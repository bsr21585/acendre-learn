CE_CourseRequirementInterface = function(
	parent, 
	modal,
	identifier,
	languages,
	initialLanguage,
	flagImagePath,
	deleteImagePath,
	addImagePath,
	data
	){
		
	this.TypeName = "CourseRequirementInterface";

	this.Parent 			= parent;
	this.ModalWindow		= modal;
	this.Identifier 		= identifier; 
	this.Languages 			= languages;
	this.InitialLanguage 	= initialLanguage;
	this.FlagImagePath 		= flagImagePath;
	this.DeleteImagePath 	= deleteImagePath;
	this.AddImagePath 		= addImagePath;
		
	this.Interface 			= this.Parent.Interface;
	
	this.Body = document.createElement("div");
		this.Body.className = "ModalPopupBody";
		
	if (typeof data == "object") {
	    this.Data = data;
	    this.DataIdentifier = this.Data.id;
	}else{
	    this.Data = new Object;
	    this.DataIdentifier = this.Parent.GetNextDataIdentifier();
		this.Data.Labels = [];
		this.Data.Course = new Object;
			this.Data.Course.Items = [];
	}
	
	// content here
	
	this.Label = new FormInputField(
		this, 					//object
		this.Body,				// DOM node to append to
		"input",				// type (input|textarea)
		this.Interface.GetDictionaryTerm("Description"),  				// text
		true, 					// bit
		this.Languages, 		// array
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		this.Data.Labels, 					// JSON
		"InputMedium",						// className to add to the input
		this.Interface.GetDictionaryTerm("New Requirement")		// default label if unable to extract
		);
		
	this.Courses = new FormInputField(
		this, 					//object
		this.Body,				// DOM node to append to
		"custom",				// type (input|textarea)
		this.Interface.GetDictionaryTerm("Learner must complete"),  		// text
		true, 					// bit
		this.InitialLanguage.split(","), 		// array
		this.InitialLanguage,	// initialLanguage
		this.FlagImagePath,		// path to flag images
		"", 					// JSON
		"InputXShort",			// className to add to the input
		""						// default label if unable to extract
		);
		
	
	this.AnyCourseSelector = new HTMLRadioOptionWithLabel(
		this.Identifier + "-IsAnyCourse", 
		this.Identifier + "-AnyCourse", 
		"1", 
		this.Interface.GetDictionaryTerm("any selected course") + ":", //jp colon
		(this.Data.Course.isAny == true),
		false
		);
		
	this.AllCourseSelector = new HTMLRadioOptionWithLabel(
		this.Identifier + "-IsAnyCourse", 
		this.Identifier + "-AllCourses", 
		"0", 
		this.Interface.GetDictionaryTerm("all selected course(s)") + ":", //jp colon
		(this.Data.Course.isAny != true),
		false
		);
		
	
	this.HTMLCoursesList = new HTMLListOfCheckBoxWithLabel(
		this.Identifier + "-CourseList",
		this.Identifier + "-CourseList",
		"", 
		"CheckBoxList",
		this
		);
	
	this.Courses.FormFieldInputContainer.append(this.AnyCourseSelector.Container);
	this.Courses.FormFieldInputContainer.append(this.AllCourseSelector.Container);

	//accumulate the selected Ids
	var ai = [];
	for (var i = 0; i < this.Data.Course.Items.length; i++){
		ai[this.Data.Course.Items[i].id] = true;
	}
	
	
	// process the course list
	var ac = this.Interface.Data.ApplicableCourses;
	for (var i = 0; i < ac.length; i++) {
		//first, process the number of credits string
		var numCreditsStr = this.Interface.GetDictionaryTerm("x credits");//i.e. ##NumberOfCredits## credits
		numCreditsStr = numCreditsStr.split("##NumberOfCredits##").join(ac[i].Credits);
		
		//ac[i].Name + " (" + ac[i].Credits + " credits)",
		this.HTMLCoursesList.AddItem(
			this.Identifier + "-CourseList-Items", 
			this.Identifier + "-CourseList-Item" + ac[i].id,
			ac[i].id, 
			ac[i].Name + " (" +  numCreditsStr + ")", 
			(ai[ac[i].id] == true),
			false
		);
	}
	
	this.Courses.FormFieldInputContainer.append(this.HTMLCoursesList.Panel);
	
	// do not attach. 
	this.Separator = document.createElement("div");
		this.Separator.className = "ModalPopupSeparator";
		
	// do not attach
	this.ButtonContainer = document.createElement("div");
		this.ButtonContainer.className = "ModalPopupButtons";
	
	this.OKButton = document.createElement("input");
		this.OKButton.Parent = this;
		this.OKButton.type = "button";
		this.OKButton.className = "Button ActionButton";
		this.OKButton.value = this.Interface.GetDictionaryTerm("OK");
		
		this.OKButton.onclick = function(){
			if(this.Parent.IsValidOrShowErrors()){
				this.Parent.ModalWindow.Parent.PushRequirementInterface(this.Parent.GetDataAsObject());
				//this.Parent.ModalWindow.Panel.style.visibility = "hidden";
				this.Parent.ModalWindow.Close();
				
				//hide the containing window
				this.Parent.Interface.ShowAsentiaModalContainer(false);
			}
		}
		
	this.CancelButton = document.createElement("input");
		this.CancelButton.Parent = this;
		this.CancelButton.type = "button";
		this.CancelButton.className = "Button NonActionButton";
		this.CancelButton.value = this.Interface.GetDictionaryTerm("Cancel");
		
		this.CancelButton.onclick = function(){
			//this.Parent.ModalWindow.Panel.style.visibility = "hidden";
			this.Parent.ModalWindow.Close();
			
			//hide the containing window
			this.Parent.Interface.ShowAsentiaModalContainer(false);
		}
		
	this.ButtonContainer.append(this.OKButton);
	this.ButtonContainer.append(this.CancelButton);
	
}

CE_CourseRequirementInterface.prototype.GetDataAsString = function(){
	
	var s = "";
	
	s = s + "{";
	s = s + "	\"id\":\"" + this.DataIdentifier + "\",";
	s = s + "	\"Labels\":";
	s = s + 	this.Label.GetData();
	s = s + "	,";
	s = s + "	\"Type\":\"course\",";
	s = s + "   \"Credit\": {\"Items\": [], \"Minimum\": null, \"isAny\": null },";
    s = s + "   \"Task\": { \"RequireDocumentation\": null, \"Signoff\": { \"ByAdministrators\": null, \"BySupervisor\": null, \"Required\": null } },";
	s = s + "	\"Course\":{";
	s = s + "		\"isAny\":" + this.AnyCourseSelector.Input.checked + ",";
	s = s + "		\"Items\":";
	s = s + 		this.HTMLCoursesList.GetData();
	s = s + "		";
	s = s + "	}";
	s = s + "}";
	
	return s;
	
}

CE_CourseRequirementInterface.prototype.GetDataAsObject = function(){
	
	return JSON.parse(this.GetDataAsString());
	
}

CE_CourseRequirementInterface.prototype.IsValidOrShowErrors = function(){
	
	this.InErrorState = false;
	
	this.Label.ClearError();
	this.Courses.ClearError();
	
	//be sure the description field isn't blank
	if (this.Label.Inputs[0].value == "")
	{
		this.Label.ShowError(this.Interface.GetDictionaryTerm("Description is required"));//"Description is required."
		this.InErrorState = true;
	}
	
	//be sure at least one item is selected
	var isAnItemSelected = false;
	for (var i = 0; i < this.HTMLCoursesList.Items.length; i++)
	{
		if (this.HTMLCoursesList.Items[i].Input.checked)
		{
			isAnItemSelected = true;
			break;
		}
	}
	
	if (isAnItemSelected == false)
	{
		this.Courses.ShowError(this.Interface.GetDictionaryTerm("At least one course must be selected"));
		this.InErrorState = true;
	}
	
	return !this.InErrorState;
	
}
	