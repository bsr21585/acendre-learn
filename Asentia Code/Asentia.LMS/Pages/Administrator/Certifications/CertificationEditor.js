/* 

CERTIFICATION EDITOR 

*/
CE_CertificationInterface = function(
	identifier, 
	node,  				// DOM object to attach to
	languages, 			// string list of languages for inputs
	initialLanguage,	// initial language for inputs
	flagImagePath,
	addImagePath, 
	deleteImagePath, 
	warningIconPath,
	data,				// JSON data
	activePanel,			// the ID of the panel that should be displayed
	modalDOMId,			// the DOM id of the target node we can attach the modal object to
	dictionary			//Dictionary of terms used to translate the content into different languages
	){
	
	this.DebugMode = false;
	
	this.Identifier			= identifier;
	this.Node				= node;
	this.Languages 			= languages.split(",");
	this.InitialLanguage	= initialLanguage;
	this.FlagImagePath		= flagImagePath;
	this.AddImagePath 		= addImagePath;
	this.DeleteImagePath 	= deleteImagePath;
	this.WarningIconPath    = warningIconPath;
	this.ModalDOMId			= modalDOMId;
	if (dictionary)
		this.Dictionary = JSON.parse(dictionary);
	
	this.TypeName			= "CertificationInterface";
	
	try{
		this.Data 			= JSON.parse(data);
	}catch(e){
		this.Data 			= "";
	}
	
	//instantiate the ErrorLister
	this.ErrorLister = new CE_ErrorLister(this);
	
	//create empty object for the ErrorListerInterface (will be instantiated when ErrorLister is created)
	this.ErrorListerInterface;
	
	//Listing of course objects that exist in Attached Courses window but NOT in LMS Courses
	this.AttachedCoursesNonExistentInLMS = new Array();
	
	//Event defines when number of credits a course is worth is changed
	this.OnNumberOfCreditsChangedEvent = new CustomEvent(
		"onChangeCourseCredits",
		{
			detail: {
				message: "Changed Course Credits"
			},
			bubbles: true,
			cancelable: true
		}
	);
	
	//Event defines just before number of credits a course is worth is changed
	this.OnBeforeNumberOfCreditsChangedEvent = new CustomEvent(
		"onBeforeChangeCourseCredits",
		{
			detail: {
				message: "onBefore Change Course Credits"
			},
			bubbles: true,
			cancelable: true
		}
	);
	
	//PANELS
	this.TabPanelsContentContainer = document.createElement("div");
	this.TabPanelsContentContainer.className = "TabPanelsContentContainer";
	
	this.TabFormObjects = [];//holds the CE_PropertiesTabForm and CE_RequirementsTabForm objects (as opposed to the panels)
	
	try
	{
		this.PropertiesPanel 			= new CE_PropertiesTabForm("properties", this, this.Data);
	}
	catch(e)
	{
		this.PropertiesPanel 			= new CE_PropertiesTabForm("properties", this, "");
	}
	//this.SettingsPanel 				= new CE_SettingsTabForm("settings", this, this.Data.Contact);

	try
	{
		this.InitialRequirementsPanel 	= new CE_RequirementsTabForm("initial", this, this.Data.Units.Initial);
	}
	catch(e)
	{
		this.InitialRequirementsPanel 	= new CE_RequirementsTabForm("initial", this, "");
	}
	
	try
	{
		this.RenewalRequirementsPanel	= new CE_RequirementsTabForm("renewal", this, this.Data.Units.Renewal);
	}
	catch(e)
	{
		this.RenewalRequirementsPanel	= new CE_RequirementsTabForm("renewal", this, "");
	}
	
		this.ModalObject = document.createElement("div");
		this.ModalObject.id = "modal-me";
		this.ModalObject.style.position = "absolute";
	
	this.Panels = [];
	this.Panels.push(this.PropertiesPanel);
	//this.Panels.push(this.SettingsPanel);
	this.Panels.push(this.InitialRequirementsPanel);
	this.Panels.push(this.RenewalRequirementsPanel);
	
	switch (activePanel.toLowerCase()){
		case "settings":
		case "initial":
		case "renewal":
		case "expert":
			this.PropertiesPanel.className = this.PropertiesPanel.className + " certification-editor-hide";
			break;
		default:
			activePanel = "properties";
			this.InitialRequirementsPanel.className = this.InitialRequirementsPanel.className + " certification-editor-hide";
			this.RenewalRequirementsPanel.className = this.RenewalRequirementsPanel.className + " certification-editor-hide";
			//this.SettingsPanel.className 			= this.SettingsPanel.className + " certification-editor-hide";
			break;
	}
	
	// "Pages" within the Certification
	
	this.TabPanelsContentContainer.append(this.PropertiesPanel);
	//this.TabPanelsContentContainer.append(this.SettingsPanel);
	this.TabPanelsContentContainer.append(this.InitialRequirementsPanel);
	this.TabPanelsContentContainer.append(this.RenewalRequirementsPanel);
	
	//TABS
	this.Tabs = new Tabs(this, this.Node);
	this.Tabs.UL.id = "certificationsTabsUL";
	this.Tabs.Items.push(new Tab(this.Tabs, this.Tabs.UL, this.GetDictionaryTerm("Properties"), (activePanel.toLowerCase() == "properties"), "properties"));
	this.Tabs.Items[this.Tabs.Items.length - 1].LI.id = "certificationsTabProperties";//assign an id to the Tab
	//this.Tabs.Items.push(new Tab(this.Tabs, this.Tabs.UL, "Settings", (activePanel.toLowerCase() == "settings"), "settings"));
	this.Tabs.Items.push(new Tab(this.Tabs, this.Tabs.UL, this.GetDictionaryTerm("Initial"), (activePanel.toLowerCase() == "initial"), "initial"));
	this.Tabs.Items[this.Tabs.Items.length - 1].LI.id = "certificationsTabInitial";//assign an id to the Tab
	this.Tabs.Items.push(new Tab(this.Tabs, this.Tabs.UL, this.GetDictionaryTerm("Renewal"), (activePanel.toLowerCase() == "renewal"), "renewal"));
	this.Tabs.Items[this.Tabs.Items.length - 1].LI.id = "certificationsTabRenewal";//assign an id to the Tab
	
	this.Node.append(this.TabPanelsContentContainer);
	
	//if Renewal requirements data matches Initial requirements data, set the Use Initial Requirements to true for the Renewal tab
	this.SetUseInitialRequirements();
	
	//automatically check that all currently Attached Courses exist in the system; alert user if conflict(s) occur
	//IDEALLY THIS WILL MOVE INTO this.ValidateEntireForm()
	//this.GetCourseListingAndValidateAttachedCourses();
	//Don't want to validate entire form on initial load bc it could be a new form (therefore, blank); only check for Attached Courses against GetCourseListing
	this.GetCourseListingAndValidateAttachedCoursesAndEntireForm(false, false);
	
	//Debug mode button to call check validity of all tabs in current Certification
	if (this.DebugMode)
	{
		this.CheckValidityButton = HTML.c(
		this,										//parent
		"input",									//type
		"CheckValidityButton",						//name
		"CheckValidityButton", 						//id
		"Button ActionButton", 						//classname
		this.Node									//appendTo
		);
		
		this.CheckValidityButton.setAttribute("type","button");
		this.CheckValidityButton.setAttribute("value","CHECK VALIDITY");
		this.CheckValidityButton.style.marginBottom = "10px";
		
		this.CheckValidityButton.onclick = function()
		{
			//recheck for any errors the were/weren't corrected
			//this.Parent.ValidateEntireForm();
			
			//ensure that courses in the Attached Courses box actually exist in the LMS environment
			//this.Parent.GetCourseListingAndValidateAttachedCourses();
			
			//check validity and do save if valid
			//this.Parent.GetCourseListingAndValidateAttachedCoursesAndEntireForm(true, true);
			
			//check validity but don't do a save
			this.Parent.GetCourseListingAndValidateAttachedCoursesAndEntireForm(false, true);
		}
		
		this.Node.append(HTML.text("CHECK VALIDITY button appears in Debug Mode only."));
	}
}

CE_CertificationInterface.prototype.SetTab = function(s){
	for (var i = 0; i < this.Panels.length; i++){
		if (this.Panels[i].id.toLowerCase() == s.toLowerCase()){
			if (this.Panels[i].className == "certification-editor-hide"){
				this.Panels[i].className = "";
			}else{
				this.Panels[i].className = this.Panels[i].className.replace(" certification-editor-hide", "");
				this.Panels[i].className = this.Panels[i].className.replace("certification-editor-hide ", "");
			}
		}else{
			if(this.Panels[i].className.indexOf("certification-editor-hide") == -1){
				this.Panels[i].className = this.Panels[i].className + " certification-editor-hide";
			}
		}
	}
}

CE_CertificationInterface.prototype.GetCourseNameFromPool = function(id){

	for (var i = 0; i < this.Data.ApplicableCourses.length; i++){
		if(this.Data.ApplicableCourses[i].id == id){
			return this.Data.ApplicableCourses[i].Name;
		}
	}
	
	return "[not found]";
}

CE_CertificationInterface.prototype.GetCourseCreditsFromPool = function(id){

	for (var i = 0; i < this.Data.ApplicableCourses.length; i++){
		if(this.Data.ApplicableCourses[i].id == id){
			return this.Data.ApplicableCourses[i].Credits;
		}
	}
	
	return "[not found]";
}

CE_CertificationInterface.prototype.AddCourseToPool = function(courseId, courseName, courseCredits)
{
	//create a new course object and add it to the ApplicableCourses pool
	var newCourse = new Object();
	
	newCourse.Name = courseName;
	newCourse.id = courseId;
	newCourse.Credits = courseCredits;
	
	this.Data.ApplicableCourses[this.Data.ApplicableCourses.length] = newCourse;
	
	if (this.DebugMode)
	{
		console.log("Course Added to Pool thru CE_CertificationInterface.prototype.AddCourseToPool:");
		console.log(this.Data.ApplicableCourses);
	}
}

CE_CertificationInterface.prototype.RemoveCourseFromPoolById = function(courseId)
{
	//remove an ApplicableCourse from the pool
	var foundIndex;
	
	for (var i = 0; i < this.Data.ApplicableCourses.length; i++){
		if(this.Data.ApplicableCourses[i].id == courseId)
		{
			foundIndex = i;
			break;
		}
	}
	
	if (foundIndex != null && foundIndex != undefined)
	{
		this.Data.ApplicableCourses.splice(foundIndex, 1);
		
		if (this.DebugMode)
		{
			console.log("Course Removed from Pool thru CE_CertificationInterface.prototype.RemoveCourseFromPoolById:");
			console.log(this.Data.ApplicableCourses);
		}
	}
}

CE_CertificationInterface.prototype.GetCourseIdListFromPool = function()
{
	//grab all id's from the courses in the pool and return them in an array
	var courseIdsArray = new Array();
	
	for (var i = 0; i < this.Data.ApplicableCourses.length; i++)
	{
		courseIdsArray.push(this.Data.ApplicableCourses[i].id);
	}
	
	return courseIdsArray;
}

CE_CertificationInterface.prototype.DoesCourseExistInPool = function(id){

	for (var i = 0; i < this.Data.ApplicableCourses.length; i++)
	{
		if(this.Data.ApplicableCourses[i].id == id)
			return true;
	}
	
	return false;
}

CE_CertificationInterface.prototype.ShowAlertModal = function(
	headerText,
	messageText,
	confirmButtonText,
	cancelButtonText,
	confirmButtonActionObject,
	cancelButtonActionObject
)
{
	/* --confirmButtonActionObject Definition--
	{
		targetObject: original object that made the call
		functionName: the name (string) of the function to be called if they click the confirm button
		functionParam: a single optional param to be used in function defined by functionName
	}
	*/
	
	//origin element (item where call originated)
	//alert box title
	//alert box message
	//confirm button text
	//confirm action
	//cancel button text
	//optional cancel action override
	
	//launch the alert modal
	this.AlertModal = new Asentia_ModalWindow(
			this,
			"asentia-js-modal-alert", 		//attach to node
			headerText, 
			"asentia-modal-alert-container", 				// containment
			this.DeleteImagePath 									// close button image path
	);
	
	//Build the interface
	this.AlertModalInterface = new CE_GeneralAlertInterface(
		this.AlertModal,
		this,
		messageText,
		confirmButtonText,
		cancelButtonText,
		confirmButtonActionObject,
		cancelButtonActionObject
	);
	
	//override the Close button (header) function to add extra functionality to it
	this.AlertModal.CloseButton.onclick = function()
	{
		this.Parent.Close();
			
		//hide the containing window
		this.Parent.Parent.ShowAsentiaModalAlertContainer(false);
	}
	
	//attach the interface
	this.AlertModal.Panel.append(this.AlertModalInterface.Body);
	this.AlertModal.Reposition();
	
	//change the modal holder interface to 'fixed' so it positions itself and stays static on the screen
	//document.getElementById(this.ModalDOMId).style.position = "fixed";
	
}

CE_CertificationInterface.prototype.UpdateCreditsForApplicableCourseWithErrorCheck = function(courseId, updatedNumberOfCredits, targetHTMLCourseItemWithCredit)
{
	
	//if they enter a non-numeric value, automatically change the value back and ignore request
	if (isNaN(updatedNumberOfCredits))
	{
		targetHTMLCourseItemWithCredit.RevertToLastAcceptableInputValue();
		return false;
	}
	
	//if the impending change will be valid, push the change through
	if (this.GetTabFormObjectByName("initial").ValidateCourseCreditChangeOnAllSegments(courseId, updatedNumberOfCredits) && this.GetTabFormObjectByName("renewal").ValidateCourseCreditChangeOnAllSegments(courseId, updatedNumberOfCredits))
	{
		return this.UpdateCreditsForApplicableCourse(courseId, updatedNumberOfCredits, targetHTMLCourseItemWithCredit);
	}
	else
	{
		//otherwise, if the impending change will be invalid due to Credit Requirements already in place, alert the user and allow them to push the change or revert to previous setting
		
		//targetHTMLCourseItemWithCredit.ShowError("Change reverted because it breaks a credit requirement already in place.");
		this.ShowAlertModal(
			this.GetDictionaryTerm("Change Credits"),
			this.GetDictionaryTerm("changing this courses credit value will affect one or more requirements"),//Changing this course's credit value will affect one or more Requirements. Make change and update all affected Requirements?
			this.GetDictionaryTerm("Yes"),
			this.GetDictionaryTerm("No"),
			{targetObject: targetHTMLCourseItemWithCredit, functionName: "ConfirmChangeCreditValueAndUpdateRequirements", functionParam:{updatedNumberOfCredits:updatedNumberOfCredits, courseId:courseId}}
		);
		
		//revert the value by default; if they click yes, it will change back to their desired value
		targetHTMLCourseItemWithCredit.RevertToLastAcceptableInputValue();
		
		return false;	
	}
}

CE_CertificationInterface.prototype.UpdateCreditsForApplicableCourse = function(courseId, updatedNumberOfCredits, targetHTMLCourseItemWithCredit)
{
	//this version (without error check) will be used to override a Requirement and push a credit change thru; any affected Requirements will be modified to fit the change
	var courseIndex;
	var currentNumberOfCredits;
	
	if (this.DebugMode)
	{
		console.log("++++++++++++++++**************+++++++++++++++++");
		console.log("courseId: " + courseId);
		console.log("updatedNumberOfCredits: " + updatedNumberOfCredits);
		console.log(targetHTMLCourseItemWithCredit);
	}
	
	//update the item's record of what the last acceptable value entered was (if they enter something invalid, the last valid entry will be restored)
	targetHTMLCourseItemWithCredit.UpdateLastAcceptableInputValue(updatedNumberOfCredits);
	
	//find the matching course id in ApplicableCourses data object
	for (var i = 0; i < this.Data.ApplicableCourses.length; i++)
	{
		var foundId = this.Data.ApplicableCourses[i].id;
		
		if (courseId == foundId)
		{
			courseIndex = i;
			break;
		}
	}
	
	if (courseIndex != undefined)
	{
		//store the number of credits before the value was changed (for error-checking)
		currentNumberOfCredits = this.Data.ApplicableCourses[i].Credits;
		
		this.Data.ApplicableCourses[i].Credits = Number(updatedNumberOfCredits);
		
		//push the change thru throughout the module
		this.InitialRequirementsPanel.dispatchEvent(this.OnNumberOfCreditsChangedEvent);
		this.RenewalRequirementsPanel.dispatchEvent(this.OnNumberOfCreditsChangedEvent);
		
		//return true if a course was updated
		return true;
	}
	
	//return false if no course was updated
	return false;
}

CE_CertificationInterface.prototype.AddTabFormObject = function(tabFormObject)
{
	this.TabFormObjects.push(tabFormObject);
}

CE_CertificationInterface.prototype.GetTabFormObjectByName = function(tabFormName)
{
	//get a specified TabForm object by Panel.id 
	for (var i = 0; i < this.TabFormObjects.length; i++)
	{
		if (this.TabFormObjects[i].Panel.id == tabFormName)
			return this.TabFormObjects[i];
	}
	
	//return if nothing found
	return;
}

CE_CertificationInterface.prototype.ValidateCourseDeleteByIdOnTabs = function(courseId)
{
	//before a course is deleted, verify the delete won't break a Requirement that is currently in place
	var isValid = true;

	if (!this.GetTabFormObjectByName("initial").ValidateCourseDeleteByIdOnTab(courseId))
		isValid = false;
		
	if (!this.GetTabFormObjectByName("renewal").ValidateCourseDeleteByIdOnTab(courseId))
		isValid = false;
		
	return isValid;
}

CE_CertificationInterface.prototype.UpdateAllRequirementInterfacesOnAllTabSegments = function()
{
	//individually call update on each tab to refresh each Segment's Requirement interfaces
	this.GetTabFormObjectByName("initial").UpdateAllRequirementInterfaces();
	this.GetTabFormObjectByName("renewal").UpdateAllRequirementInterfaces();
}

CE_CertificationInterface.prototype.ReduceMinCreditsRequiredOnAllTabSegmentsOnCourseDelete = function()
{
	//after a course delete, search thru all affected Requirements and lower the Credit Requirement where needed
	this.GetTabFormObjectByName("initial").ReduceMinCreditsRequiredOnAllSegmentsOnCourseDelete();
	this.GetTabFormObjectByName("renewal").ReduceMinCreditsRequiredOnAllSegmentsOnCourseDelete();
}

CE_CertificationInterface.prototype.DeleteRequirementsWithNoCoursesOnAllTabs = function()
{
	//after a course is deleted, Requirements left hanging with no courses will need to be removed
	this.GetTabFormObjectByName("initial").DeleteRequirementsWithNoCoursesOnAllUnits();
	this.GetTabFormObjectByName("renewal").DeleteRequirementsWithNoCoursesOnAllUnits();
}

CE_CertificationInterface.prototype.DeleteAllSegmentsWithNoRequirementsOnAllTabs = function()
{
	//after a course is deleted, Segments left hanging with no requirements will need to be removed
	this.GetTabFormObjectByName("initial").DeleteAllSegmentsWithNoRequirementsOnAllUnits();
	this.GetTabFormObjectByName("renewal").DeleteAllSegmentsWithNoRequirementsOnAllUnits();
}


CE_CertificationInterface.prototype.DoAttachedCoursesListCourseDelete = function(courseId)
{
		
	//remove the course from the global Attached Course list
	this.RemoveCourseFromPoolById(courseId);
	
	//reduce the min number of required credits in any affected Requirement where needed
	this.ReduceMinCreditsRequiredOnAllTabSegmentsOnCourseDelete();
	
	//also update any segment Requirements that may have contained the course
	this.UpdateAllRequirementInterfacesOnAllTabSegments();
	
	//Remove any requirements left hanging with no courses
	this.DeleteRequirementsWithNoCoursesOnAllTabs();
	
	//Remove any segments left hanging with no requirements
	this.DeleteAllSegmentsWithNoRequirementsOnAllTabs();
}

CE_CertificationInterface.prototype.ShowAsentiaModalContainer = function(showFlag)
{
	if (showFlag)
		document.getElementById("asentia-modal-container").style.display = "block";
	else
		document.getElementById("asentia-modal-container").style.display = "none";
}

CE_CertificationInterface.prototype.ShowAsentiaModalAlertContainer = function(showFlag)
{
	if (showFlag)
		document.getElementById("asentia-modal-alert-container").style.display = "block";
	else
		document.getElementById("asentia-modal-alert-container").style.display = "none";
}

CE_CertificationInterface.prototype.SetUseInitialRequirements = function()
{
	//if the tabs' data matches, mark the "Use Initial Requirements" box checked on Renewal Tab and hide its contents (specifically used once after everything is instantiated)
	//BUT, not if the data is blank, because then it will always match
	if (this.DoRenewalRequirementsMatchInitial() && this.GetTabFormObjectByName("renewal").Units.length > 0)
	{
		this.GetTabFormObjectByName("renewal").UseInitialRequirements = true;
		this.GetTabFormObjectByName("renewal").UpdateForUseInitialRequirements();
	}
}

CE_CertificationInterface.prototype.DoRenewalRequirementsMatchInitial = function()
{
	//returns true if all settings in the Renewal tab match the Initial tab
	var initialRequirementsStr;
	
	if (this.GetTabFormObjectByName("renewal").UseInitialRequirements == false)
		initialRequirementsStr = this.GetTabFormObjectByName("renewal").GetDataAsString();
	else if (this.GetTabFormObjectByName("renewal").UseInitialRequirements == true)
		initialRequirementsStr = this.GetTabFormObjectByName("renewal").GetDataAsStringAndUseInitialRequirements();
	
    //Check if the data for the two tabs matches (excluding Ids since they have to be different for database storage)
	var initialTabData = this.GetTabFormObjectByName("initial").GetDataAsString();
	if (this.DoesTabDataMatchExcludingIds(initialRequirementsStr, initialTabData) == true)
	    return true;
	else
	    return false;
}

CE_CertificationInterface.prototype.DoesTabDataMatchExcludingIds = function (tab1DataString, tab2DataString) {
    //Compares the JSON data from two tabs a returns whether the data is the same, minus any Ids throughout (since Ids will always be different)
    try {
        //get the json data strings with all ids set to 0
        var tab1DataStringFormattedZeroIds = this.ZeroOutAllIdsInJsonString(tab1DataString);
        var tab2DataStringFormattedZeroIds = this.ZeroOutAllIdsInJsonString(tab2DataString);

        //compare the strings
        if (tab1DataStringFormattedZeroIds == null || tab2DataStringFormattedZeroIds == null)
            return;

        if (tab1DataStringFormattedZeroIds == tab2DataStringFormattedZeroIds)
            return true;
        else
            return false;
    }
    catch (e) {
        console.log("Error comparing tab data: " + e.message);
    }

    return;
}

CE_CertificationInterface.prototype.ZeroOutAllIdsInJsonString = function (jsonString) {
    //iterates thru every item in the object and sets any property with the name "id" to 0
    try {
        //be sure data string is in parse format
        var jsonStringFormatted = this.PutDataStringInParseFormat(jsonString);

        //parse the string into a JSON object
        var jsonObject = JSON.parse(jsonStringFormatted);

        //local function that changes the ids to 0
        function changeIdsToZero(pJsonObject) {
            for (var i in pJsonObject) {
                if (i == "id") {
                    pJsonObject[i] = 0;
                }

                //if an object is found, recursively go inside that object and perform the same search/change process
                if (typeof pJsonObject[i] === "object")
                    changeIdsToZero(pJsonObject[i]);
            }

            jsonObject = pJsonObject;
        }

        //run the function
        changeIdsToZero(jsonObject);

        //change the object back to a string
        var reformattedJsonString = JSON.stringify(jsonObject);

        //console.log(jsonObject);
        //console.log(reformattedJsonString);

        return reformattedJsonString;
    }
    catch (e) {
        console.log("Error zeroing out id's in json string/object: " + e.message);
    }

    return;
}

CE_CertificationInterface.prototype.PutDataStringInParseFormat = function (jsonDataString) {
    //ensures the data string is properly wrapped in curly braces {} for JSON parsing
    var dataStr = jsonDataString;

    //change any 'undefined' to null because undefined will cause parse error
    dataStr = dataStr.split("undefined").join("null");

    if (dataStr.charAt(0) != "{")
        dataStr = "{" + dataStr + "}";

    return dataStr;
}

CE_CertificationInterface.prototype.IsValidOrShowErrors = function()
{
	//validate that all required top level fields (Titles, Descriptions, Labels) are all filled
	this.InErrorState = false;
	
	if (!this.GetTabFormObjectByName("properties").IsValidOrShowErrors())
		this.InErrorState = true;
	
	if (!this.GetTabFormObjectByName("initial").IsValidOrShowErrors())
		this.InErrorState = true;
		
	//if Renewal tab is set to UseInitialRequirements, no error-checking is needed
	if (!this.GetTabFormObjectByName("renewal").UseInitialRequirements)
	{
		if (!this.GetTabFormObjectByName("renewal").IsValidOrShowErrors())
			this.InErrorState = true;
	}
	
	return !this.InErrorState;
}


CE_CertificationInterface.prototype.AddClassToElement = function(elem,value)
{
	//adds specified class to passed element
	var rspaces = /\s+/;
	var classNames = (value || "").split( rspaces );
	var className = " " + elem.className + " ",
	setClass = elem.className;
	
	for (var c = 0, cl = classNames.length; c < cl; c++ )
	{
		if ( className.indexOf( " " + classNames[c] + " " ) < 0 )
		{
			setClass += " " + classNames[c];
		}
	}
	
	elem.className = setClass.replace(/^\s+|\s+$/g,'');
}

CE_CertificationInterface.prototype.RemoveClassFromElement = function(elem, value)
{
	//removes specified class from passed element and leaves any other class names as-is
	var rspaces = /\s+/;
	var rclass = /[\n\t]/g
	var classNames = (value || "").split( rspaces );
	var className = (" " + elem.className + " ").replace(rclass, " ");
	
	for (var c = 0, cl = classNames.length; c < cl; c++ )
	{
		className = className.replace(" " + classNames[c] + " ", " ");
	}
	 
	 elem.className = className.replace(/^\s+|\s+$/g,'');

}

CE_CertificationInterface.prototype.GetDictionaryTerm = function(termStr)
{
	//retrieve terms from the global dictionary
	if (this.Dictionary)
	{
		var thisTerm;
		//convert term to lowercase for data grab
		var termStrLower = termStr.toLowerCase();
		
		//grab the term from the dictionary
		thisTerm = this.Dictionary[this.InitialLanguage][termStrLower];
		
		//if no term found, send back what was orignally passed in (to avoid undefined)
		if (!thisTerm)
			return termStr;
		else
			return thisTerm;
	}
	else //if no Dictionary exists, return the same term that was passed in (to avoid undefined values if a term doesn't exist)
	{
		return termStr;
	}
}

CE_CertificationInterface.prototype.AddToErrorList = function(
	tabName,
	targetObject,
	errorName,
	unitObject,
	segmentObject
)
{
	//transfer the error to the ErrorLister
	this.ErrorLister.AddError(
		tabName,
		targetObject,
		errorName,
		unitObject,
		segmentObject
	);
}

CE_CertificationInterface.prototype.GetDataAsString = function()
{
	//compile all the data into a JSON string
	var s = "{";
	
	//get current Properties Tab info
	s += this.GetTabFormObjectByName("properties").GetDataAsString();
	
	//General data - Experts, isPublished, isClosed
	s += "\"Experts\":";
	s += JSON.stringify(this.Data.Experts) + ",";
	
	s += "\"isPublished\":";
	s +=  JSON.stringify(this.Data.isPublished) + ",";
	
	s += "\"isClosed\":";
	s +=  JSON.stringify(this.Data.isClosed) + ",";
	
	//get the Units info from the Tabs
	s += "\"Units\":{";
	
	//Initial Tab
	s += "\"Initial\":{";
	s += this.GetTabFormObjectByName("initial").GetDataAsString();
	
	//close the Initial object
	s += "},";
	
	//Renewal Tab: if "Use Initial Requirements" is checked, duplicate Initial Tab settings to the Renewal Tab
	s += "\"Renewal\":{";
	if (this.GetTabFormObjectByName("renewal").UseInitialRequirements == false)
		s += this.GetTabFormObjectByName("renewal").GetDataAsString();
	else if (this.GetTabFormObjectByName("renewal").UseInitialRequirements == true)
		s += this.GetTabFormObjectByName("renewal").GetDataAsStringAndUseInitialRequirements();
	
	//close the Renewal object
	s += "}";
	
	//close the Units object
	s += "},";
	
	//Add Applicable Courses Object
	s += this.GetApplicableCoursesDataAsString();
	
	//close the overall object
	s += "}";
	
	//console.log(this.Data);
	
	return s;
}

CE_CertificationInterface.prototype.GetApplicableCoursesDataAsString = function()
{
	var s = "";
	
	s += "\"ApplicableCourses\":";
	
	s += JSON.stringify(this.Data.ApplicableCourses);
	
	return s;
}

CE_CertificationInterface.prototype.GetApplicableCoursesDataJSON = function()
{
	if (this.Data.ApplicableCourses)
		return this.Data.ApplicableCourses;
	else
		return [];
}

CE_CertificationInterface.prototype.ValidateAndDoSave = function()
{
	//PAGE SAVE BUTTON SHOULD CALL THIS
	
	//checks that everything is valid; if valid, DoSave will be called
	this.GetCourseListingAndValidateAttachedCoursesAndEntireForm(true, true);
}

CE_CertificationInterface.prototype.DoSave = function()
{
	//Only reaches this point after all fields/items have been validated
	//Update the hidden field will the current JSON data
	var certificationJSONDataHiddenField = document.getElementById("CertificationJSONField");
	
	if (certificationJSONDataHiddenField) {
        certificationJSONDataHiddenField.value = this.GetDataAsString();
	}
	
	try {
	    //do the postback containing the button object that was clicked
        __doPostBack('Master$PageContentPlaceholder$CertificationSaveButton', '');
	}
	catch(e) {
		//postback failure
	}
	
	//check that everything is valid; shows errors if not
	//if (!this.ValidateEntireForm())
	//	return false;

	//ADD SAVE BEHAVIOR
	//console.log("SAVE ACTION");
	//ADD __doPostBack function(s) here
	
	//return true;
}

CE_CertificationInterface.prototype.CheckCourseListingAndValidateEntireForm = function(optionalSupressAlertModalBoolean)
{
	//clear the list of any previously logged errors
	this.ErrorLister.ClearAllErrors();
	
	//hide the error lister interface
	try
	{
		this.ShowErrorListerInterface(false);
	}
	catch(e) {};
	
	this.GetCourseListingAndValidateAttachedCoursesAndEntireForm(false, true);
}

CE_CertificationInterface.prototype.ValidateEntireForm = function(optionalSupressAlertModalBoolean)
{
	//check for errors anywhere in the Certifications
	
	//clear the list of any previously logged errors
	this.ErrorLister.ClearAllErrors();
	
	//hide the error lister interface
	try
	{
		this.ShowErrorListerInterface(false);
	}
	catch(e) {};
	
	//checks the form validity, but will not Save the page
	this.GetCourseListingAndValidateAttachedCoursesAndEntireForm(false, true);
	
	//Don't save if there are still errors present
	/*if (!this.IsValidOrShowErrors())
	{
		//show an alert (unless it was manually suppressed in the call)
		if (!optionalSupressAlertModalBoolean)
		{
			this.ShowAlertModal(
				this.GetDictionaryTerm("Save Certification"),
				this.GetDictionaryTerm("certification cannot be saved until all errors are corrected"),//Certification cannot be saved until all errors are corrected.
				this.GetDictionaryTerm("OK"),
				""
			);
		}
		
		//show the error lister
		this.ShowErrorListerInterface(true);
		
		if (this.DebugMode)
			console.log(this.ErrorLister.ErrorList);
		
		return false;	
	}*/
	
	return true;
}

CE_CertificationInterface.prototype.ShowErrorListerInterface = function(showHideFlag)
{
	if (showHideFlag)
	{
		//create a new instance if none exists
		if (!this.ErrorListerInterface)
		{
			this.ErrorListerInterface = new CE_ErrorListerInterface(this);
			
			//insert it just before the tab interface area
			this.Node.insertBefore(this.ErrorListerInterface.Container, this.TabPanelsContentContainer);
			
		}
		
		//clear any errors already in the list
		try
		{
			this.ErrorListerInterface.ClearErrorList();
		}
		catch(e){};
		
		//populate the errors
		this.ErrorListerInterface.PopulateErrorList();
		
		//update the instruction/title area
		var errorListerHeaderText = this.GetDictionaryTerm("Click each error below to auto navigate to its location");//ClickEachErrorBelowToAutoNavigateToItsLocation
		this.ErrorListerInterface.UpdateHeaderText(errorListerHeaderText);
		//this.ErrorListerInterface.UpdateHeaderText("Click each error below to auto-navigate to its location.");
		
		this.ErrorListerInterface.Show();
		
		//scroll to the top of the page
		window.scrollTo(0,0);
	}
	else
	{
		this.ErrorListerInterface.Hide();
	}
}

CE_CertificationInterface.prototype.GetCourseListingAndValidateAttachedCourses = function()
{
	//pulls the current listing of courses directly from the system and validates Attached Courses to be sure they actually exist in the system
	var targetInterfaceObject = this;
	
	$.ajax({
        type: "POST",
        url: this.GetCourseListingServiceURL(),
        data: "{ searchParam: null }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
			var responseObject = response.d;
			
			//ensure that all attached courses actually exist in the course listing
			targetInterfaceObject.ValidateAttachedCoursesAgainstCourseListing(responseObject.courses);

			//console.log(JSON.stringify(responseObject.courses));
	},
        failure: function (response) {
            //alert(response.d);
        },
        error: function (response) {
            //alert(response.d);
        }
    });
}

CE_CertificationInterface.prototype.GetCourseListingAndValidateAttachedCoursesAndEntireForm = function(performSaveBoolean, validateEntireFormBoolean)
{
	//pulls the current listing of courses directly from the system and validates Attached Courses to be sure they actually exist in the system
	var targetInterfaceObject = this;
	
	//clear the list of any previously logged errors
	this.ErrorLister.ClearAllErrors();
	
	//remove any Yellow Warning from the Properties tab at the top
	this.GetTabFormObjectByName("properties").ShowTabErrorImage(false);
	
	//hide the error lister interface
	try
	{
		this.ShowErrorListerInterface(false);
	}
	catch(e) {};
	
	$.ajax({
        type: "POST",
        url: this.GetCourseListingServiceURL(),
        data: "{ searchParam: null }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(response) {
			var responseObject = response.d;
			var foundErrors = false;
			
			//ensure that all attached courses actually exist in the course listing
			if (!targetInterfaceObject.ValidateAttachedCoursesAgainstCourseListing(responseObject.courses))
				foundErrors = true;
			//console.log(JSON.stringify(responseObject.courses));
			
			//ensure that the tabs' setup is valid
			if (validateEntireFormBoolean)
			{
				if (!targetInterfaceObject.IsValidOrShowErrors())
					foundErrors = true;
			}
				
			//if errors are found, show the alert
			if (foundErrors)
				targetInterfaceObject.ShowAlertOnFormErrorsFound();
			
			//otherwise, do the save if prompted
			if (performSaveBoolean && !foundErrors)
				targetInterfaceObject.DoSave();
	},
        failure: function (response) {
            //alert(response.d);
			
			console.log("Call to verify Attached Courses against GetCourseListing service failed. Proceeding to check for all other errors.");
			
			//add general error that the GetCourseListing call failed
			targetInterfaceObject.AddToErrorList(
				"properties",
				targetInterfaceObject.GetTabFormObjectByName("properties"),
				targetInterfaceObject.ErrorLister.GETCOURSELISTING_ERROR
			);
			
			//if the call fails, still evaluate the tabs data; either way, show the GetCoursListingError
			if (!targetInterfaceObject.IsValidOrShowErrors())
				targetInterfaceObject.ShowAlertOnFormErrorsFound();
			else
			{
				targetInterfaceObject.GetTabFormObjectByName("properties").ShowTabErrorImage(true);
				targetInterfaceObject.ShowAlertOnFormErrorsFound();
			}
			//otherwise, do the save
			/*else
			{
				if (performSaveBoolean)
					targetInterfaceObject.DoSave();
			}*/
        },
        error: function (response) {
            //alert(response.d);
			
			console.log("Call to verify Attached Courses against GetCourseListing service errored. Proceeding to check for all other errors.");
			
			//add general error that the GetCourseListing call failed
			targetInterfaceObject.AddToErrorList(
				"properties",
				targetInterfaceObject.GetTabFormObjectByName("properties"),
				targetInterfaceObject.ErrorLister.GETCOURSELISTING_ERROR
			);
			
			//if the call fails, still evaluate the tabs data; either way, show the GetCoursListingError
			if (!targetInterfaceObject.IsValidOrShowErrors())
				targetInterfaceObject.ShowAlertOnFormErrorsFound();
			else
			{
				targetInterfaceObject.GetTabFormObjectByName("properties").ShowTabErrorImage(true);
				targetInterfaceObject.ShowAlertOnFormErrorsFound();
			}
			//otherwise, do the save
			/*else
			{
				if (performSaveBoolean)
					targetInterfaceObject.DoSave();
			}*/
        }
    });
}

CE_CertificationInterface.prototype.ShowAlertOnFormErrorsFound = function(optionalSupressAlertModalBoolean)
{
	//show an alert (unless it was manually suppressed in the call)
	if (!optionalSupressAlertModalBoolean)
	{
		this.ShowAlertModal(
			this.GetDictionaryTerm("Save Certification"),
			this.GetDictionaryTerm("certification cannot be saved until all errors are corrected"),//Certification cannot be saved until all errors are corrected.
			this.GetDictionaryTerm("OK"),
			""
		);
	}
	
	//show the error lister
	this.ShowErrorListerInterface(true);
	
	if (this.DebugMode)
		console.log(this.ErrorLister.ErrorList);
}

CE_CertificationInterface.prototype.GetCourseListingServiceURL = function()
{
	//builds the URL for GetCourseListing web service
	
	//OLD: url: "http://www.asentiaics.com/_util/UtilityServices.asmx/GetCourseListing",
	//construct the URL (i.e. http://www.asentia-dev03.local/_util/UtilityServices.asmx/GetCourseListing)
	var serviceURL = "";
	serviceURL += window.location.protocol + "//"; //http:
	serviceURL += window.location.hostname; //www.asentia-dev03.local
	
	//add trailing slash if there's none
	if (serviceURL.charAt(serviceURL.length - 1) != "/")
		serviceURL += "/";
	
	//FOR DEBUG: If running locally (file://), target the dev server
	//if (serviceURL == "file://")
	if (this.DebugMode)
		serviceURL = "http://www.asentia-dev03.local/";
	
	//add service location
	serviceURL += "_util/UtilityServices.asmx/GetCourseListing";
	
	return serviceURL;
}

CE_CertificationInterface.prototype.ValidateAttachedCoursesAgainstCourseListing = function(courseListing)
{
	//Finds any Attached Courses that have no matching course in the system and adds error to ErrorLister; does NOT show the ErrorList -- just adds the errors to it
	var foundErrors = false;
	
	if (this.DebugMode)
		console.log(courseListing);
	
	//check that each course in Attached Courses actually exists in the LMS/system
	this.AttachedCoursesNonExistentInLMS = new Array();
	
	//get the courses currently in the Attached Courses box
	var attachedCoursesObjectArr = this.GetApplicableCoursesDataJSON();
	
	//verify each course Id against the courses in the system
	for (var i = 0; i < attachedCoursesObjectArr.length; i++)
	{
		var thisAttachedCourseId = attachedCoursesObjectArr[i].id;
		
		//if course doesn't exist, add it to an array of missing courses
		if (!this.DoesAttachedCourseExistInCourseListing(thisAttachedCourseId, courseListing))
		{
			if (!foundErrors)
				foundErrors = true;
			
			this.AttachedCoursesNonExistentInLMS.push(attachedCoursesObjectArr[i]);
		}
	}
	
	//add the Yellow Alert icon image to the Properties tab
	if (foundErrors)
		this.GetTabFormObjectByName("properties").ShowTabErrorImage(true);
	
	
	//add an error in the ErrorLister for each course that does not exist
	
	//ShowError for each HTMLCourseItemWithCredit in the Attached Courses list that doesn't exist in the LMS system
    //get the list of objects for presently Attached Courses		
	//var attachedCoursesObjectList = this.GetTabFormObjectByName("properties").AttachedCoursesList.Items;
	var attachedCoursesObjectList = this.GetTabFormObjectByName("properties").Parent.PropertiesPanel.Parent.AttachedCoursesList.Items; // JC edit, this appears to be where the object is located
                                                                                                                                       // as the code above is throwing an undefined error
	
	//iterate thru courses that already have been marked as not existing
	for (var i = 0; i < this.AttachedCoursesNonExistentInLMS.length; i++)
	{	
		var idOfMissingCourse = this.AttachedCoursesNonExistentInLMS[i].id;
		
		//match each missing course id with the matching element in the Attached Courses list
		for (var j = 0; j < attachedCoursesObjectList.length; j++)
		{
			var currentItemId = attachedCoursesObjectList[j].Identifier;
			
			//show the error on the on-screen element
			if (currentItemId == idOfMissingCourse)
			{	
				attachedCoursesObjectList[j].ShowError(this.GetDictionaryTerm("This course no longer exists in the system"));//This course no longer exists in the system. | ThisCourseNoLongerExistsInTheSystem
				
				//store info about the error to the error lister
				this.AddToErrorList(
					"properties",
					attachedCoursesObjectList[j],
					this.ErrorLister.COURSE_DOES_NOT_EXIST
				);
			}
		}

	}
	
	//return true if everything is fine; false otherwise
	return !foundErrors;
}

CE_CertificationInterface.prototype.DoesAttachedCourseExistInCourseListing = function(attachedCourseId, courseListing)
{
	//iterate thru course listing to see if the specified attachedCourse exists
	for (var i = 0; i < courseListing.length; i++)
	{
		var thisCourseId = courseListing[i].Id;
		
		if (attachedCourseId == thisCourseId)
			return true;
	}
	
	return false;
}









/* REQUIREMENTS TAB OBJECT */

CE_RequirementsTabForm = function(
	name, 
	parent, 
	data
	)
{	
	this.Parent = parent;
	this.Panel 	= HTML.c(this, "div", "", name, "TabPanelContainer");
	this.TypeName = "RequirementsTabForm";
	this.Identifier = this.Parent.Identifier;
	this.TabName = name;
	
	this.Parent.AddTabFormObject(this);
	
	//Event to listen for when user changes the number of credits a course is worth
	this.Panel.addEventListener("onChangeCourseCredits",
		function(e){
			this.Parent.UpdateAllRequirementInterfaces();
		}
	);
	
	this.UnitCounter		= 0;
	
	if (typeof data == "object") {
		this.Data = data;
	}else{
		this.Data = new Object;
		this.Data.Items = [];
	}
	
	//if this is the Renewal Requirements tab, add the "Use Initial Requirements" checkbox, which (when checked) will use all settings on the Initial Requirements tab
	if (name == "renewal")
	{
		//instantiate this.UseInitialRequirements from Data if it exists
		if (this.Data.UseInitialRequirements != undefined)
			this.UseInitialRequirements = this.Data.UseInitialRequirements;
		else
			this.UseInitialRequirements = false;
		
		this.UseInitialRequirementsCheckbox = new HTMLCheckBoxOptionWithLabel(
			"UseInitialRequirementsCheckbox",
			"UseInitialRequirementsCheckbox",
			"useInitialRequirements",
			this.Parent.GetDictionaryTerm("Use Initial Requirements"),
			this.UseInitialRequirements,
			false
		);
		
		//add a class name to the item's container so CSS styles can be applied
		this.UseInitialRequirementsCheckbox.Container.className = "UseInitialRequirementsCheckboxContainer";
		
		//set the parent to this tab
		this.UseInitialRequirementsCheckbox.Parent = this;
		
		//set a variable to track whether the Renewal tab items are visible (in relation to Use Initial Requirements)
		this.TabItemsVisible = true;
		
		//add check/uncheck actions to the box
		this.UseInitialRequirementsCheckbox.Input.onclick = function()
		{
			//toggle the UseInitialRequirements flag
			this.Parent.Parent.ToggleUseInitialRequirements();
		}
		
		//finally, append the item to the tab
		this.Panel.append(this.UseInitialRequirementsCheckbox.Container);
	}
	
	this.Title = document.createElement("div");
		this.Title.className = "Title";
	this.UnitCount = document.createElement("span");
		this.UnitCount.className = "UnitCount";
		this.UnitCount.append(HTML.text(this.Data.Items.length));

	this.Title.append(this.UnitCount);
	//this.Title.append(HTML.text(" Unit(s):"));
	this.Title.append(HTML.text(" " + this.Parent.GetDictionaryTerm("Unit(s)")));

	this.Panel.append(this.Title);

	/* ADD UNIT Button */

	this.AddUnitButton = new IconButton(
		this,
		this.Title,
		this.Parent.AddImagePath,
		this.Parent.GetDictionaryTerm("New Unit"),
		true,
		"AddItemButton"
		)

	this.AddUnitButton.onclick = function(){
		this.Parent.NewUnit("");
		this.Parent.UnitsCarousel.GoToLastItem();
		
		//update the Delete button enabled/disabled on all Units
		this.Parent.UpdateDeleteButtonEnabledOnAllUnits();
	}
	
	//Units AND/OR
	try {this.UnitsIsAny = this.Data.isAny;}catch(e){this.UnitsIsAny = false};
	
	//add the Units single AND/OR button
	this.UnitAndOrButton = new CE_UnitAndOr(this);
	this.Title.append(this.UnitAndOrButton);
	
	/* MODULES Navigation / Dots */

	this.UnitsNavigationContainer = document.createElement("div");
		this.UnitsNavigationContainer.className = "UnitNavigationContainer"
		this.Panel.append(this.UnitsNavigationContainer);

	/* MODULES (Carousel) */

	this.Units 			= []; 
	this.UnitsCarousel = new AsentiaCarousel(
		"UnitsCarousel",
		this.Panel
		);
		
		// Add Units from Data
		try{
			for (var i = 0; i < this.Data.Items.length; i++){
				this.NewUnit(this.Data.Items[i]);
			}
		} catch(e){
			this.NewUnit("");
		} 

		// Configure the Carousel Navigation
		
		var pButtonContainer = document.createElement("div");
			pButtonContainer.style.float = "left";
			pButtonContainer.append(this.UnitsCarousel.PreviousItemButton.Button);
			
		var nButtonContainer = document.createElement("div");
			nButtonContainer.style.float = "right";
			nButtonContainer.append(this.UnitsCarousel.NextItemButton.Button);
			
		this.UnitsNavigationContainer.append(pButtonContainer);
		this.UnitsNavigationContainer.append(nButtonContainer);
		this.UnitsNavigationContainer.append(this.UnitsCarousel.Dots.Panel);

	//this.Panel.append(this.ModalObject);
	
	//update the Delete(X) button enabled/disabled
	this.UpdateDeleteButtonEnabledOnAllUnits();
	
	//update the visibility of the Unit AND/OR button based on if there's more than 1 unit present
	this.UpdateUnitAndOr();
	
	//when the item first loads, be sure the Renewal tab interface is hidden/shown based on "Use Initial Requirements" being checked/unchecked
	if (name == "renewal")
		this.UpdateForUseInitialRequirements();
	
	return this.Panel;
}
	
CE_RequirementsTabForm.prototype.UpdateUnitsTitleCount = function(){
	
	// update the counter on the page
	this.UnitCount.removeChild(this.UnitCount.childNodes[0]);
	this.UnitCount.append(HTML.text(this.Units.length));
	
}

CE_RequirementsTabForm.prototype.NewUnit = function(
	data, 
	block
){

	this.Units.push(new CE_UnitInterface(
		this,
		this.Identifier + "-Unit" + this.UnitCounter,
		null, 
		this.Parent.Languages,				// languages
		this.Parent.InitialLanguage,		// initialLanguage
		this.Parent.FlagImagePath,			// path to flag images
		this.Parent.DeleteImagePath,		// path to delete image
		this.Parent.AddImagePath,			// path to add image
		data								// JSON data or string
	))
	
	this.UpdateUnitsTitleCount();
	
	this.UnitCounter++; // only used for DOM Ids
	
	this.UnitsCarousel.AttachItem(this.Units[this.Units.length - 1].Panel);
	
	this.FixUnitsCarouselIndexingAfterAddOrDelete();
	
	//show the AND/OR button if there's more than 1 Unit
	this.UpdateUnitAndOr();
	
	return this.Units[this.Units.length - 1].Panel;
	
}

CE_RequirementsTabForm.prototype.PromptToDeleteUnit = function(unitHTMLDOM)
{	
	this.Parent.ShowAlertModal(
	 this.Parent.GetDictionaryTerm("Delete Unit"),
	 this.Parent.GetDictionaryTerm("Are you sure you want to delete this unit"),//Are you sure you want to delete this unit?
	 this.Parent.GetDictionaryTerm("Yes"),
	 this.Parent.GetDictionaryTerm("No"),
	 {targetObject: this, functionName: "DeleteUnit", functionParam: unitHTMLDOM}
	);
}

CE_RequirementsTabForm.prototype.DeleteUnit = function(unitHTMLDOM){
	
	var index = -1;
	
	// find it
	for (var i = 0; i < this.Units.length; i++){
		if (this.Units[i].Panel.id == unitHTMLDOM.id) {index = i;}
	}
	
	// not found, exit
	if (index == -1) { alert("not found"); return true;}
	
	// remove from the carousel
	this.UnitsCarousel.RemoveItemAtIndex(index);
	
	// remove from the array
	this.Units.splice(index, 1);
	
	this.UpdateUnitsTitleCount();
	
	//update the Delete button enabled/disabled on all Units
	this.UpdateDeleteButtonEnabledOnAllUnits();
	
	//show/hide the AND/OR button based on number of remaining Units
	this.UpdateUnitAndOr();
	
	this.FixUnitsCarouselIndexingAfterAddOrDelete();
	
	// disable the delete unit button from index 0 if its the last unit remaining
	//alert("still needs work - DeleteUnit");
	//if (this.GetEffectiveUnitCount() == 1){
	//	for (var i = 0; i < this.Units.length; i++){
	//		this.Units[i].DeleteButton.Enable(false);
	//	}
	//}

}

CE_RequirementsTabForm.prototype.FixUnitsCarouselIndexingAfterAddOrDelete = function()
{
	//manually adjust certain paramters of the Carousel after an add or delete happens
	this.UnitsCarousel.LastViewableIndex = this.Units.length - 1;
	//this.UnitsCarousel.ActualItemIndex--;
	//this.UnitsCarousel.EffectiveItemIndex++;
	//this.UnitsCarousel.EffectiveItemCountCount--;
	//this.UnitsCarousel.LastViewableIndex--;
	//this.UnitsCarousel.ItemCounter--;
	
	if (this.Parent.DebugMode)
	{
		console.log(this.UnitsCarousel);
	}
}

CE_RequirementsTabForm.prototype.UpdateAllRequirementInterfaces = function()
{
	//traverse the Units then Segments to update each Requirement Interface
	for (var i = 0; i < this.Units.length; i++)
	{
		for (var j = 0; j < this.Units[i].Segments.length; j++)
		{
			this.Units[i].Segments[j].UpdateAllRequirementInterfaces();
		}
	}

}

CE_RequirementsTabForm.prototype.ValidateCourseCreditChangeOnAllSegments = function(courseId, updatedNumberOfCredits)
{
	for (var i = 0; i < this.Units.length; i++)
	{
		
		for (var j = 0; j < this.Units[i].Segments.length; j++)
		{
			
			if (this.Parent.DebugMode)
			{
				console.log(this.Units[i].Segments[j].Data);
				console.log("**");
				console.log(this.Parent.Data);
			}
			
			//check each Segment's current Requirements to be sure impending course Credit change is legal
			if (!this.Units[i].Segments[j].ValidateCourseCreditChange(courseId, updatedNumberOfCredits))
				return false;
			
			/*for (var k = 0; k < this.Units[i].Segments.length; k++)
			{
				//check each Segment's current Requirements to be sure impending course Credit change is legal
				if (!this.Units[i].Segments[k].ValidateCourseCreditChange(courseId, updatedNumberOfCredits))
					return false;
			}*/
		}
	}
	
	return true;
}

CE_RequirementsTabForm.prototype.ValidateCourseDeleteByIdOnTab = function(courseId)
{
	//before a course is deleted, verify the delete won't break a Requirement that is currently in place
	var allTabsValid = true;
	
	for (var i = 0; i < this.Units.length; i++)
	{
		for (var j = 0; j < this.Units[i].Segments.length; j++)
		{
			if (!this.Units[i].Segments[j].ValidateSegmentCourseDeleteById(courseId))
			{
				allTabsValid = false;
				
				//stop the process as soon as a conflicting requirement is found for this course id
				break;
			}
		}
	}
	
	return allTabsValid;
}

CE_RequirementsTabForm.prototype.ReduceMinCreditsRequiredOnAllSegmentsOnCourseDelete = function()
{
	//when a course is deleted, reduce a Credit Requirement's min credits where needed
	for (var i = 0; i < this.Units.length; i++)
	{
		for (var j = 0; j < this.Units[i].Segments.length; j++)
		{
			this.Units[i].Segments[j].ReduceMinCreditsRequiredOnCourseDelete();
		}
	}
}

CE_RequirementsTabForm.prototype.DeleteRequirementsWithNoCoursesOnAllUnits = function()
{
	for (var i = 0; i < this.Units.length; i++)
	{
		this.Units[i].DeleteRequirementsWithNoCoursesOnAllSegments();
	}
}

CE_RequirementsTabForm.prototype.DeleteAllSegmentsWithNoRequirementsOnAllUnits = function()
{
	for (var i = 0; i < this.Units.length; i++)
	{
		this.Units[i].DeleteAllSegmentsWithNoRequirements();
	}
}

CE_RequirementsTabForm.prototype.UpdateDeleteButtonEnabledOnAllUnits = function()
{
	//goes thru each Unit to determine if its Delete button (X) should be enabled/disabled
	for (var i = 0; i < this.Units.length; i++)
	{
		this.Units[i].UpdateDeleteButtonEnabled();
	}
}

CE_RequirementsTabForm.prototype.UpdateUnitAndOr = function()
{
	//show the AND/OR button if there's more than 1 Unit
	if (this.Units.length > 1)
	{
		this.ShowUnitAndOr(true);
	}
	else
	{
		this.ShowUnitAndOr(false);
	}
}

CE_RequirementsTabForm.prototype.ShowUnitAndOr = function(showFlag)
{
	//show/hide the Unit level AND/OR button
	if (showFlag)
		this.UnitAndOrButton.style.visibility = "visible";
	else
		this.UnitAndOrButton.style.visibility = "hidden";

}

CE_RequirementsTabForm.prototype.ToggleUnitsAndOr = function()
{
	//change the AND/OR button from AND to OR and vice-versa
	this.UnitsIsAny = !this.UnitsIsAny;
	
	//remove the current button text
	this.UnitAndOrButton.removeChild(this.UnitAndOrButton.childNodes[0]);
	
	//insert the applicable text
	this.UnitAndOrButton.append(HTML.text(  (this.UnitsIsAny) ? this.Parent.GetDictionaryTerm("OR") : this.Parent.GetDictionaryTerm("AND") ));
	
}

CE_RequirementsTabForm.prototype.ShowTabContents = function(showFlag)
{	
	//show/hide all the contents on this tab (used specifically for "Use Initial Requirements" checkbox)
	for (var i = 0; i < this.Panel.children.length; i++)
	{
		var currentElement = this.Panel.children[i];
		
		//hide everything except the Use Initial Requirements checkbox
		if (currentElement.className != "UseInitialRequirementsCheckboxContainer")
		{
			if (showFlag)
			{
				this.Parent.RemoveClassFromElement(currentElement, "RequirementsTabItemHidden");
				this.TabItemsVisible = true;
			}
			else
			{
				this.Parent.AddClassToElement(currentElement, "RequirementsTabItemHidden");
				this.TabItemsVisible = false;
			}
		}
	}

}

CE_RequirementsTabForm.prototype.ToggleUseInitialRequirements = function()
{
	//strictly for the "Renewal" tab to handle the "Use Initial Requirements" checkbox action
	
	//toggle both data items
	this.UseInitialRequirements = !this.UseInitialRequirements
	this.Data.UseInitialRequirements = this.UseInitialRequirements;
	
	//show or hide the Renewal tab body depending on checked status (checked/use = hide; unchecked/don't use = show)
	this.ShowTabContents(!this.UseInitialRequirements);
}

CE_RequirementsTabForm.prototype.UpdateForUseInitialRequirements = function()
{
	//syncs the UI to match what this.UseInitialRequirements is set to
	if (this.UseInitialRequirements == true && this.TabItemsVisible == true)
	{
		this.ShowTabContents(false);
		this.UseInitialRequirementsCheckbox.Input.checked = true;
	}
	else if (this.UseInitialRequirements == false && this.TabItemsVisible == false)
	{
		this.ShowTabContents(true);
		this.UseInitialRequirementsCheckbox.Input.checked = false;
	}
}

CE_RequirementsTabForm.prototype.IsValidOrShowErrors = function()
{
	//validate the required fields on this tab
	this.InErrorState = false;
	
	//hide presently showing Tab warning icons
	this.ShowTabErrorImage(false);
	
	//check all units for Title, Description, and Segment Label completion
	for (var i = 0; i < this.Units.length; i++)
	{
		if (!this.Units[i].IsValidOrShowErrors())
		{
			this.InErrorState = true;
			
			//show error/warning image on this Tab at top
			this.ShowTabErrorImage(true);
		}
	}
	
	//add error if no Units exist on this Tab
	if (this.Units.length == 0)
	{
		var errorType;
		if (this.TabName == "initial")
			errorType = this.Parent.ErrorLister.INITIAL_NO_UNITS;
		else if (this.TabName == "renewal")
			errorType = this.Parent.ErrorLister.RENEWAL_NO_UNITS;
		
		if (errorType)
		{
			this.Parent.AddToErrorList(
				this.TabName,
				this.Parent.GetTabFormObjectByName(this.TabName),
				errorType
			);
		}
		
		//show error/warning image on this Tab at top
		this.ShowTabErrorImage(true);
		
		this.InErrorState = true;
	}
	
	return !this.InErrorState;
}

CE_RequirementsTabForm.prototype.ShowTabErrorImage = function(showFlagBoolean)
{
	if (showFlagBoolean)
	{
		//add the proper error class to the tab itself (TabbedListLIError)
		if (this.TabName == "initial")
		{
			this.AddTabErrorImageToTab(document.getElementById("certificationsTabInitial"));
		}
		else if (this.TabName == "renewal")
		{
			this.AddTabErrorImageToTab(document.getElementById("certificationsTabRenewal"));
		}
	}
	else
	{
		//remove the error stylings/image
		if (this.TabName == "initial")
		{
			this.RemoveTabErrorImageFromTab(document.getElementById("certificationsTabInitial"));
		}
		else if (this.TabName == "renewal")
		{
			this.RemoveTabErrorImageFromTab(document.getElementById("certificationsTabRenewal"));
		}
	}
}

CE_RequirementsTabForm.prototype.AddTabErrorImageToTab = function(tabElement)
{
	//Add the LIError class to the Tab and insert the visual Warning image
	this.Parent.AddClassToElement(tabElement, "TabbedListLIError");
	
	var imageId = this.TabName + "_TabLIErrorImage";
	var attachToElement = tabElement;
	var imageSrc = this.Parent.WarningIconPath;
	
	//add the image inside the Tab at the top
	this.TabLIErrorImage = HTML.c(
		this, 						//parent
		"img", 						//type
		null, 						//name
		imageId, 					//id
		"SmallIcon", 				//class
		attachToElement 			//appendTo
	);
	
	this.TabLIErrorImage.setAttribute("src", imageSrc);
}

CE_RequirementsTabForm.prototype.RemoveTabErrorImageFromTab = function(tabElement)
{
	this.Parent.RemoveClassFromElement(tabElement, "TabbedListLIError");
	
	//remove the image itself
	var imageElement = document.getElementById(this.TabName + "_TabLIErrorImage");

	if (imageElement)
		tabElement.removeChild(imageElement);
}

CE_RequirementsTabForm.prototype.GetDataAsString = function()
{
	//returns the data within as a string in JSON format
	var s = "";
		
    //top level items
    if (this.UnitsIsAny != null) {
	    s += "\"isAny\":" + this.UnitsIsAny + ",";
    }
    else {
	    s += "\"isAny\":false,";
	}

    // get expiration data from the inputs on the properties tab
	if (this.TabName == "initial") {
	    if (this.Data.Expiration != null) {
	        this.Data.Expiration.Interval = document.getElementById("CertificationInitialAwardExpirationInterval").value;
	        this.Data.Expiration.Timeframe = document.getElementById("CertificationInitialAwardExpirationTimeframe").value;
	    }
	}
	else if (this.TabName == "renewal") {
	    if (this.Data.Expiration != null) {
	        this.Data.Expiration.Interval = document.getElementById("CertificationRenewalExpirationInterval").value;
	        this.Data.Expiration.Timeframe = document.getElementById("CertificationRenewalExpirationTimeframe").value;
	    }
	}
	else {
	}

	s += "\"Expiration\":" + JSON.stringify(this.Data.Expiration) + ",";
	
	//added item: needed to determine whether we are copying the settings from the Initial Tab
	//if (this.Panel.id == "renewal")
	//	s += "\"UseInitialRequirements\":" + this.UseInitialRequirements + ",";
	
	//Units definitions (Items)
	s += "\"Items\":[";
	
	for (var i = 0; i < this.Units.length; i++)
	{
		//build data for this Unit
		
		//needs a comma separator if this is a subsequent item
		if (i > 0)
			s += ",";
			
		s += this.Units[i].GetDataAsString();
	}
	
	//close the Units (Items)
	 s += "]";
	
	return s;
}

CE_RequirementsTabForm.prototype.GetDataAsStringAndUseInitialRequirements = function()
{
	//for use with a Renewal tab that uses "Use Initial Requirements" and copies those settings over
	//returns the data within as a string in JSON format
	var initialTabObject = this.Parent.GetTabFormObjectByName("initial");
	var s = "";
	
	//top level items
    if (initialTabObject.UnitsIsAny != null) {
	    s += "\"isAny\":" + String(initialTabObject.UnitsIsAny) + ",";
	}
	else {
	    s += "\"isAny\":false,";
	}

    // get expiration data from the inputs on the properties tab	
	if (this.Data.Expiration != null) {
	    this.Data.Expiration.Interval = document.getElementById("CertificationRenewalExpirationInterval").value;
	    this.Data.Expiration.Timeframe = document.getElementById("CertificationRenewalExpirationTimeframe").value;
	}
	
	s += "\"Expiration\":" + JSON.stringify(this.Data.Expiration) + ",";
	
	//added item: needed to determine whether we are copying the settings from the Initial Tab
	//s += "\"UseInitialRequirements\":" + this.UseInitialRequirements + ",";
	
	//Units definitions (Items)
	s += "\"Items\":[";
	
	for (var i = 0; i < initialTabObject.Units.length; i++)
	{
		//build data for this Unit
		
		//needs a comma separator if this is a subsequent item
		if (i > 0)
			s += ",";
			
		s += initialTabObject.Units[i].GetDataAsString();
	}
	
	//close the Units (Items)
	 s += "]";
	
    //edit the initial tab data to eliminate id duplicates on Units, Segments, and Requirements
	 var useInitialRequirementsData = this.RemoveUnitSegmentRequirementIdDuplications(s);

	 return useInitialRequirementsData;

	 //return s;
}

CE_RequirementsTabForm.prototype.RemoveUnitSegmentRequirementIdDuplications = function (tabDataString) {
    //prepare and parse the data string into JSON
    try {
        //replace any undefined's with 'null' so JSON can parse the string
        var modifiedDataString = tabDataString.split("undefined").join("null");

        //make the data json parse-able by surrounding with brackets
        var dataJSONString = "{" + modifiedDataString + "}";

        //create the JSON object
        var tabJSON = JSON.parse(dataJSONString);

        //iterate thru to change the ids for Units, Segments, Requirements to 0

        //Units
        for (var i = 0; i < tabJSON.Items.length; i++) {
            var thisUnit = tabJSON.Items[i];

            thisUnit.id = 0;

            //Segments
            for (var j = 0; j < thisUnit.Segments.Items.length; j++) {
                var thisSegment = thisUnit.Segments.Items[j];

                thisSegment.id = 0;

                //Requirements
                for (var k = 0; k < thisSegment.Requirements.Items.length; k++) {
                    var thisRequirement = thisSegment.Requirements.Items[k];

                    //use the letter requirements since these are new items
                    var newReqId = "A" + (k + 1).toString();
                    thisRequirement.id = newReqId;
                }
            }
        }

        //change the JSON object back into a string
        var finalJSONString = JSON.stringify(tabJSON);

        //remove the surrounding { } braces that were added before parsing
        //start brace removal
        finalJSONString = finalJSONString.substr(1);
        //end brace removal
        finalJSONString = finalJSONString.substring(0, finalJSONString.length - 1);

        return finalJSONString;

    }
    catch (e) {
        console.log("error parsing JSON for Use Initial Requirements: " + e.message);
    }

    return;
}

/* SETTINGS TAB OBJECT*/

CE_SettingsTabForm = function(
	name, 
	parent, 
	data){

	this.Parent = parent;
	this.Panel 	= HTML.c(this, "div", "", name, "TabPanelContainer");
	this.TypeName = "SettingsTabForm";
	this.Identifier = this.Parent.Identifier;
	
	if (typeof data == "object") {
		this.Data 	= data;
	}
	
	/* SETTINGS */
	
	this.IsPublishedFormField = new FormInputField(
		this, 								// parent
		this.Panel,					// attach to
		"custom",							// type
		"Published", 						// label
		false, 								// isRequired
		this.Parent.InitialLanguage.split(","),	// languages
		this.Parent.InitialLanguage,				// initialLanguage
		"",									// path to flag images
		"",									// JSON data or string
		""									// input class
	)
	
		this.IsPublishedYes = new HTMLRadioOptionWithLabel(
			"IsPublished", 
			"IsPublished_Yes", 
			true, 
			"Yes", 
			this.Data.isPublished,
			false,
			"ToggleInput"
			);
			
		this.IsPublishedNo = new HTMLRadioOptionWithLabel(
			"IsPublished", 
			"IsPublished_No", 
			false, 
			"No", 
			!this.Data.isPublished,
			false,
			"ToggleInput"
			);
			
		this.IsPublishedFormField.FormFieldInputContainer.append(this.IsPublishedYes.Container);
		this.IsPublishedFormField.FormFieldInputContainer.append(this.IsPublishedNo.Container);
	
	this.IsClosedFormField = new FormInputField(
		this, 									// parent
		this.Panel,								// attach to
		"custom",								// type
		"Closed", 								// label
		false, 									// isRequired
		this.Parent.InitialLanguage.split(","),	// languages
		this.Parent.InitialLanguage,			// initialLanguage
		"",										// path to flag images
		"",										// JSON data or string
		""										// input class
	)
	
		this.IsClosedYes = new HTMLRadioOptionWithLabel(
			"IsClosed", 
			"IsClosed_Yes", 
			true, 
			"Yes", 
			this.Data.isClosed,
			false,
			"ToggleInput"
			);
			
		this.IsClosedNo = new HTMLRadioOptionWithLabel(
			"IsClosed", 
			"IsClosed_No", 
			false, 
			"No", 
			!this.Data.isClosed,
			false,
			"ToggleInput"
			);
			
		this.IsClosedFormField.FormFieldInputContainer.append(this.IsClosedYes.Container);
		this.IsClosedFormField.FormFieldInputContainer.append(this.IsClosedNo.Container);

	return this.Panel;
	
}


/* PROPERTIES TAB OBJECT */

CE_PropertiesTabForm = function(
	name, 
	parent, 
	data){
		
	this.Parent = parent;
	this.Panel 	= HTML.c(this, "div", "", name, "TabPanelContainer");
	this.TypeName = "PropertiesTabForm";
	this.Identifier = this.Parent.Identifier;
	this.Interface = this.Parent;
	this.TabName = name;
	
	this.Parent.AddTabFormObject(this);
	
	this.LaunchAttachCoursesModalButtonImagePath = CertificationsCourseImagePath;
	
	if (typeof data == "object") {
		this.Data 	= data;
	}else{
		this.Data = new Object;
		this.Data.Titles = [];
		this.Data.Descriptions = [];
		this.Data.Accreditor = new Object;
		this.Data.Tag = "";
	}
	
	this.Title = new FormInputField(
		this, 					// parent
		this.Panel,				// attach to
		"input",				// type
		this.Parent.GetDictionaryTerm("Title"),  				// label
		true, 					// isRequired
		this.Parent.Languages, 		// languages
		this.Parent.InitialLanguage,	// initialLanguage
		this.Parent.FlagImagePath,		// path to flag images
		this.Data.Titles,		// JSON data or string
		"InputLong"				// input class
	);
	
	this.Description = new FormInputField(
		this, 					// parent
		this.Panel,	// attach to
		"textarea",				// type
		this.Parent.GetDictionaryTerm("Description"),  		// label
		true, 					// isRequired
		this.Parent.Languages, 		// languages
		this.Parent.InitialLanguage,	// initialLanguage
		this.Parent.FlagImagePath,		// path to flag images
		this.Data.Descriptions,	// JSON data or string
		""						// input class
	);
    
    // INITIAL AWARD EXPIRATION
	this.InitialAwardExpiresInterval = new FormInputField(
        this,
        this.Panel,
        "custom",
        this.Parent.GetDictionaryTerm("Initial Award Expiration"),
        true,
        "",
        "",
        "",
        "",
        "",
        ""
    );

    // interval
	var initialIntervalTextbox = document.createElement("input");
	initialIntervalTextbox.setAttribute("id", "CertificationInitialAwardExpirationInterval");
	initialIntervalTextbox.className = "InputXShort";

	this.InitialAwardExpiresInterval.FormFieldInputContainer.append(initialIntervalTextbox);

	if (this.Data.Units != null) {
	    if (this.Data.Units.Initial.Expiration.Interval != null) {
	        initialIntervalTextbox.value = this.Data.Units.Initial.Expiration.Interval;
	    }
	}

    // timeframe
	var initialTimeframeSelector = document.createElement("select");
	initialTimeframeSelector.setAttribute("id", "CertificationInitialAwardExpirationTimeframe");

	var initialTimeframeSelectorOption1 = document.createElement("option");
	initialTimeframeSelectorOption1.setAttribute("value", "d")
	initialTimeframeSelectorOption1.append(HTML.text(this.Parent.GetDictionaryTerm("day_s")));
	initialTimeframeSelector.append(initialTimeframeSelectorOption1);

	var initialTimeframeSelectorOption2 = document.createElement("option");
	initialTimeframeSelectorOption2.setAttribute("value", "ww")
	initialTimeframeSelectorOption2.append(HTML.text(this.Parent.GetDictionaryTerm("week_s")));
	initialTimeframeSelector.append(initialTimeframeSelectorOption2);

	var initialTimeframeSelectorOption3 = document.createElement("option");
	initialTimeframeSelectorOption3.setAttribute("value", "m")
	initialTimeframeSelectorOption3.append(HTML.text(this.Parent.GetDictionaryTerm("month_s")));
	initialTimeframeSelector.append(initialTimeframeSelectorOption3);

	var initialTimeframeSelectorOption4 = document.createElement("option");
	initialTimeframeSelectorOption4.setAttribute("value", "yyyy")
	initialTimeframeSelectorOption4.append(HTML.text(this.Parent.GetDictionaryTerm("year_s")));
	initialTimeframeSelector.append(initialTimeframeSelectorOption4);

	this.InitialAwardExpiresInterval.FormFieldInputContainer.append(initialTimeframeSelector);

	if (this.Data.Units != null) {
	    if (this.Data.Units.Initial.Expiration.Timeframe != null) {
	        initialTimeframeSelector.value = this.Data.Units.Initial.Expiration.Timeframe;
	    }
	}
    // END INITIAL AWARD EXPIRATION

    // RENEWAL EXPIRATION
	this.RenewalExpiresInterval = new FormInputField(
        this,
        this.Panel,
        "custom",
        this.Parent.GetDictionaryTerm("Renewal Expiration"),
        true,
        "",
        "",
        "",
        "",
        "",
        ""
    );

    // interval
	var renewalIntervalTextbox = document.createElement("input");
	renewalIntervalTextbox.setAttribute("id", "CertificationRenewalExpirationInterval");
	renewalIntervalTextbox.className = "InputXShort";
	
	this.RenewalExpiresInterval.FormFieldInputContainer.append(renewalIntervalTextbox);

	if (this.Data.Units != null) {
	    if (this.Data.Units.Renewal.Expiration.Interval != null) {
	        renewalIntervalTextbox.value = this.Data.Units.Renewal.Expiration.Interval;
	    }
	}
	
    // timeframe
	var renewalTimeframeSelector = document.createElement("select");
	renewalTimeframeSelector.setAttribute("id", "CertificationRenewalExpirationTimeframe");

	var renewalTimeframeSelectorOption1 = document.createElement("option");
	renewalTimeframeSelectorOption1.setAttribute("value", "d")
	renewalTimeframeSelectorOption1.append(HTML.text(this.Parent.GetDictionaryTerm("day_s")));
	renewalTimeframeSelector.append(renewalTimeframeSelectorOption1);

	var renewalTimeframeSelectorOption2 = document.createElement("option");
	renewalTimeframeSelectorOption2.setAttribute("value", "ww")
	renewalTimeframeSelectorOption2.append(HTML.text(this.Parent.GetDictionaryTerm("week_s")));
	renewalTimeframeSelector.append(renewalTimeframeSelectorOption2);

	var renewalTimeframeSelectorOption3 = document.createElement("option");
	renewalTimeframeSelectorOption3.setAttribute("value", "m")
	renewalTimeframeSelectorOption3.append(HTML.text(this.Parent.GetDictionaryTerm("month_s")));
	renewalTimeframeSelector.append(renewalTimeframeSelectorOption3);

	var renewalTimeframeSelectorOption4 = document.createElement("option");
	renewalTimeframeSelectorOption4.setAttribute("value", "yyyy")
	renewalTimeframeSelectorOption4.append(HTML.text(this.Parent.GetDictionaryTerm("year_s")));
	renewalTimeframeSelector.append(renewalTimeframeSelectorOption4);

	this.RenewalExpiresInterval.FormFieldInputContainer.append(renewalTimeframeSelector);

	if (this.Data.Units != null) {
	    if (this.Data.Units.Renewal.Expiration.Timeframe != null) {
	        renewalTimeframeSelector.value = this.Data.Units.Renewal.Expiration.Timeframe;
	    }
	}
    // END RENEWAL EXPIRATION
	
	this.Accreditor = new FormInputField(
		this, 								// parent
		this.Panel,							// attach to
		"custom",							// type
		this.Parent.GetDictionaryTerm("Accrediting Organization"),			// label
		false, 								// isRequired
		"",	// languages
		"",				// initialLanguage
		"",					// path to flag images
		"",				// JSON data or string
		"", 			// input class
		""
	);
	
	//alert(typeof this.Data.Accreditor.Company);
		this.AccreditorCompany = new FormInputField(
			this, 										// parent
			this.Accreditor.FormFieldInputContainer,	// attach to
			"input",									// type
			this.Parent.GetDictionaryTerm("Company"),									// label
			false, 										// isRequired
			this.Parent.InitialLanguage.split(","), 	// languages
			this.Parent.InitialLanguage,				// initialLanguage
			this.Parent.FlagImagePath,					// path to flag images
			(this.Data.Accreditor.Company ? this.Data.Accreditor.Company.toString() : ""),				// JSON data or string
			"InputLong",									// input class
			""												//default label when data is blank (prevents showing 'undefined' as the blank value)
		);
		
		this.AccreditorName = new FormInputField(
			this, 										// parent
			this.Accreditor.FormFieldInputContainer,	// attach to
			"input",									// type
			this.Parent.GetDictionaryTerm("Name"),									// label
			false, 										// isRequired
			this.Parent.InitialLanguage.split(","), 	// languages
			this.Parent.InitialLanguage,				// initialLanguage
			this.Parent.FlagImagePath,					// path to flag images
			(this.Data.Accreditor.Name ? this.Data.Accreditor.Name.toString() : ""),				// JSON data or string
			"InputMedium",									// input class
			""												//default label when data is blank (prevents showing 'undefined' as the blank value)
		);
		
		this.AccreditorEmail = new FormInputField(
			this, 										// parent
			this.Accreditor.FormFieldInputContainer,	// attach to
			"input",									// type
			this.Parent.GetDictionaryTerm("Email"),									// label
			false, 										// isRequired
			this.Parent.InitialLanguage.split(","), 	// languages
			this.Parent.InitialLanguage,				// initialLanguage
			this.Parent.FlagImagePath,					// path to flag images
			(this.Data.Accreditor.Email ? this.Data.Accreditor.Email.toString() : ""),				// JSON data or string
			"InputLong",									// input class
			""												//default label when data is blank (prevents showing 'undefined' as the blank value)
		);
		
		this.AccreditorPhone = new FormInputField(
			this, 										// parent
			this.Accreditor.FormFieldInputContainer,	// attach to
			"input",									// type
			this.Parent.GetDictionaryTerm("Phone"),									// label
			false, 										// isRequired
			this.Parent.InitialLanguage.split(","), 	// languages
			this.Parent.InitialLanguage,				// initialLanguage
			this.Parent.FlagImagePath,					// path to flag images
			(this.Data.Accreditor.Phone ? this.Data.Accreditor.Phone.toString() : ""),				// JSON data or string
			"InputMedium",									// input class
			""												//default label when data is blank (prevents showing 'undefined' as the blank value)
		);
	

	
	this.Tags = new FormInputField(
		this, 								// parent
		this.Panel,							// attach to
		"textarea",							// type
		this.Parent.GetDictionaryTerm("Search Tags"),  					// label
		false, 								// isRequired
		this.Parent.InitialLanguage.split(","),	// languages
		this.Parent.InitialLanguage,		// initialLanguage
		this.Parent.FlagImagePath,			// path to flag images
		this.Data.Tags,						// JSON data or string
		"",									// input class
		""
	);	
	
	this.AttachedCourses = new FormInputField(
		this, 								// parent
		this.Panel,							// attach to
		"custom",							// type
		this.Parent.GetDictionaryTerm("Attached Courses"),			// label
		false, 								// isRequired
		"",				// languages
		"",				// initialLanguage
		"",					// path to flag images
		"",				// JSON data or string
		"", 			// input class
		""
	);
	
	//add button panel to hold the Attach Course(s) link/button
	this.AttachCourses_ButtonsPanel = HTML.c(
		this, 						//parent
		"div", 						//type
		null, 							//name
		"AttachCourses_ButtonsPanel", 	//id
		null, 						//class
		this.AttachedCourses.FormFieldContainer  				//appendTo
	);
	
	//add the anchor tag that will serve as Attach Courses link/button
	this.LaunchAttachCoursesModal = HTML.c(
		this, 						//parent
		"a", 						//type
		null, 							//name
		"LaunchAttachCoursesModal", 	//id
		"ImageLink", 						//class
		this.AttachCourses_ButtonsPanel 				//appendTo
	);
	
	this.LaunchAttachCoursesModal.onclick = function(){
		
		//show the modal container 
		this.Parent.Interface.ShowAsentiaModalContainer(true);
		
		//launch Attach Courses modal
		this.Parent.ModalWindow = new Asentia_ModalWindow(
			this.Parent,						//parent
			this.Parent.Interface.ModalDOMId,	//node to attach to
			this.Parent.Interface.GetDictionaryTerm("Attach Course(s)"),					//title
			"asentia-modal-container",							//containment node
			this.Parent.Interface.DeleteImagePath				//close button icon path
		);

		//override the close button function to add extra functionality to it	
		this.Parent.ModalWindow.CloseButton.onclick = function()
		{
			this.Parent.Close();
			
			//hide the containing window
			this.Parent.Parent.Interface.ShowAsentiaModalContainer(false);
		}

		this.Parent.AttachCoursesInterface = new CE_AttachCoursesInterface(
			this.Parent.ModalWindow,
			this.Parent
			);
		
		//attach the interface
		this.Parent.ModalWindow.Panel.append(this.Parent.AttachCoursesInterface.Body);
		this.Parent.ModalWindow.Panel.append(this.Parent.AttachCoursesInterface.ButtonContainer);
		this.Parent.ModalWindow.Reposition();
		
		//change the modal holder interface to 'fixed' so it positions itself and stays static on the screen
		//document.getElementById(this.Parent.Interface.ModalDOMId).style.position = "fixed";
		
		//grab the courses that will appear in the list box
		this.Parent.AttachCoursesInterface.GetCourseListing();
	};
	
	//add the icon that will be beside the link
	this.LaunchAttachCoursesModalImage = HTML.c(
		this, 						//parent
		"img", 						//type
		null, 							//name
		"LaunchAttachCoursesModalImage", 	//id
		"MediumIcon", 						//class
		this.LaunchAttachCoursesModal 				//appendTo
	);
	
	this.LaunchAttachCoursesModalImage.setAttribute("src", this.LaunchAttachCoursesModalButtonImagePath);
	
	//add the link text
	this.LaunchAttachCoursesModal.append(document.createTextNode(this.Parent.GetDictionaryTerm("Attach Course(s)")));
	
	this.AttachedCoursesList = new HTMLCourseListBoxWithCredits(this.Interface.DeleteImagePath, this.Data.ApplicableCourses, this);
	this.AttachedCourses.FormFieldInputContainer.append(this.AttachedCoursesList.Container);
	
	return this.Panel;
}

CE_PropertiesTabForm.prototype.IsValidOrShowErrors = function()
{
	//validate the required fields on this tab
	this.InErrorState = false;
	
	//hide any previous errors
	this.Title.ClearError();
	this.Description.ClearError();
	
	if (this.Title.IsInitialLanguageFieldEmpty())
	{
		this.Title.ShowError(this.Parent.GetDictionaryTerm("Title is required in the default language"));
		this.InErrorState = true;
		
		//store info about the error to the error lister
		this.Parent.AddToErrorList(
			this.TabName,
			this.Title,
			this.Parent.ErrorLister.PROPERTIES_TITLE_REQUIRED
		);
	}
	
	if (this.Description.IsInitialLanguageFieldEmpty())
	{
		this.Description.ShowError(this.Parent.GetDictionaryTerm("Description is required in the default language"));
		this.InErrorState = true;
		
		//store info about the error to the error lister
		this.Parent.AddToErrorList(
			this.TabName,
			this.Description,
			this.Parent.ErrorLister.PROPERTIES_DESCRIPTION_REQUIRED
		);
	}
	
	//show error/warning image on this Tab at top
	if (this.InErrorState)
		this.ShowTabErrorImage(true);
	
	return !this.InErrorState;
}

CE_PropertiesTabForm.prototype.ShowTabErrorImage = function(showFlagBoolean)
{
	if (showFlagBoolean)
	{
		//add the proper error class to the tab itself
		if (this.TabName == "properties")
		{
			this.AddTabErrorImageToTab(document.getElementById("certificationsTabProperties"));
		}
	}
	else
	{
		//remove the error stylings/image
		if (this.TabName == "properties")
		{
			this.RemoveTabErrorImageFromTab(document.getElementById("certificationsTabProperties"));
		}
	}
}

CE_PropertiesTabForm.prototype.AddTabErrorImageToTab = function(tabElement)
{
	//Add the LIError class to the Tab and insert the visual Warning image
	this.Parent.AddClassToElement(tabElement, "TabbedListLIError");
	
	var imageId = this.TabName + "_TabLIErrorImage";
	var attachToElement = tabElement;
	var imageSrc = this.Parent.WarningIconPath;
	
	//don't add it again if it already exists
	if (document.getElementById(imageId))
		return;
	
	//add the image inside the Tab at the top
	this.TabLIErrorImage = HTML.c(
		this, 						//parent
		"img", 						//type
		null, 						//name
		imageId, 					//id
		"SmallIcon", 				//class
		attachToElement 			//appendTo
	);
	
	this.TabLIErrorImage.setAttribute("src", imageSrc);
	
}

CE_PropertiesTabForm.prototype.RemoveTabErrorImageFromTab = function(tabElement)
{
	this.Parent.RemoveClassFromElement(tabElement, "TabbedListLIError");
	
	//remove the image itself
	var imageElement = document.getElementById(this.TabName + "_TabLIErrorImage");
	
	if (imageElement)
		tabElement.removeChild(imageElement);
}

CE_PropertiesTabForm.prototype.GetDataAsString = function()
{
	var s = "";
	
	//Properties Tab - Title form input field (returns all languages)
	s += "\"Titles\":";
	
	s += this.Title.GetData() + ",";
	
	//Properties Tab - Description form input field (returns all languages)
	s += "\"Descriptions\":";
	
	s += this.Description.GetData() + ",";

	//Properties Tab - Accreditor info
	s += "\"Accreditor\":{";
	
	s += "\"Company\":";
	s += this.AccreditorCompany.GetDataForSingleInputValue() + ",";
	s += "\"Name\":";
	s += this.AccreditorName.GetDataForSingleInputValue() + ",";
	s += "\"Email\":";
	s += this.AccreditorEmail.GetDataForSingleInputValue() + ",";
	s += "\"Phone\":";
	s += this.AccreditorPhone.GetDataForSingleInputValue();
	s += "},";
	
	//Properties Tab - Tags
	s += "\"Tags\":";
	s += this.Tags.GetDataForSingleInputValue();
	
	//add comma so that new data can be appended
	s += ",";
	
	return s;
}

/* CONTACT TAB OBJECT */


CE_ContactTabForm = function(
	name, 
	parent, 
	data
	){
	
	this.Parent = parent;
	this.Panel 	= HTML.c(this, "div", "", name, "TabPanelContainer");
	this.TypeName = "ContactTabForm";
	this.Identifier = this.Parent.Identifier;
	
	if (typeof data == "object") {
		this.Data 	= data;
	}else{
		this.Data = new Object;
		this.Data.id = "";
		this.Data.Name = "";
		this.Data.Email = "";
		this.Data.Phone = "";
	}
	
	/* CONTACT */
	
	this.FormField = new FormInputField(
		this, 								// parent
		this.Panel,							// attach to
		"custom",							// type
		this.Parent.GetDictionaryTerm("Contact"), 							// label
		false, 								// isRequired
		this.Parent.InitialLanguage.split(","),	// languages
		this.Parent.InitialLanguage,				// initialLanguage
		"",									// path to flag images
		"",									// JSON data or string
		""									// input class
		);
	
		this.ContactInternalSelector = new HTMLRadioOptionWithLabel(
			"ContactType", 
			"ContactType_Internal", 
			true, 
			this.Parent.GetDictionaryTerm("User") + ": ", 
			(Number.isInteger(this.Data.id)),
			false,
			"ToggleInput"
			);
			this.ContactInternalSelector.Input.Parent = this;
			this.ContactInternalSelector.Input.onclick = function(){
				this.Parent.ChangeContactTypeUpdateForm();
				}
				
		this.ContactExternalSelector = new HTMLRadioOptionWithLabel(
			"ContactType", 
			"ContactType_External", 
			false, 
			this.Parent.GetDictionaryTerm("Other") + ":", 
			(!Number.isInteger(this.Data.id)),
			false,
			"ToggleInput"
			);
			this.ContactExternalSelector.Input.Parent = this;
			this.ContactExternalSelector.Input.onclick = function(){
				this.Parent.ChangeContactTypeUpdateForm();
				}
				
		/*  INTERNAL */
		// connect the radio
		this.FormField.FormFieldInputContainer.append(this.ContactInternalSelector.Container);
		
		// span holding the selected user's name
		this.InternalContactSelectedLabel = HTML.c(this, "span", "", "InternalContactSelectedLabel", "", this.ContactInternalSelector.Label);
		this.InternalContactSelectedLabel.append(HTML.text(this.Data.Name));
			
		// container
		this.InternalContactContainer = HTML.c(this, "div", "", "InternalContactContainer", "FormFieldInputContainer", this.FormField.FormFieldInputContainer);
		
		this.InternalContactSelectLink = new HTML.c(this, "a", "", "", "ImageLink", this.InternalContactContainer);
		this.InternalContactSelectIcon = new HTML.c(this, "img", "", "", "MediumIcon", this.InternalContactSelectLink);		
		this.InternalContactSelectLink.append(HTML.text("Select Contact"));
			
		/* EXTERNAL */
		// connect the radio
		this.FormField.FormFieldInputContainer.append(this.ContactExternalSelector.Container);
		//container
		this.ExternalContactContainer = HTML.c(this, "div", "", "ExternalContactContainer", "FormFieldInputContainer", this.FormField.FormFieldInputContainer	);
		
			this.ExternalContactNameLabel = HTML.c(this, "div", "", "", "", this.ExternalContactContainer);
			this.ExternalContactNameLabel.append(HTML.text(this.Parent.GetDictionaryTerm("Name") + ":"));
			this.ExternalContactName = HTML.c(this, "input", "ExternalContactName", "ExternalContactName", "InputMedium", this.ExternalContactContainer);
			this.ExternalContactName.value = this.Data.Name;
					
			this.ExternalContactEmailLabel = HTML.c(this, "div", "", "", "", this.ExternalContactContainer);
			this.ExternalContactEmailLabel.append(HTML.text(this.Parent.GetDictionaryTerm("Email") + ":"));
			this.ExternalContactEmail = HTML.c(this, "input", "ExternalContactEmail", "ExternalContactEmail", "InputLong", this.ExternalContactContainer);
			this.ExternalContactEmail.value = this.Data.Email;
			
			this.ExternalContactPhoneLabel = HTML.c(this, "div", "", "", "", this.ExternalContactContainer);
			this.ExternalContactPhoneLabel.append(HTML.text(this.Parent.GetDictionaryTerm("Phone") + ":"));
			this.ExternalContactPhone = HTML.c(this, "input", "ExternalContactName", "ExternalContactName", "InputMedium", this.ExternalContactContainer);
			this.ExternalContactPhone.value = this.Data.Phone;
		
	//set the input states
	this.ChangeContactTypeUpdateForm();
	
	return this.Panel;
		
}

CE_ContactTabForm.prototype.ChangeContactTypeUpdateForm = function(){

	this.ExternalContactName.disabled = this.ContactInternalSelector.Input.checked;
	this.ExternalContactEmail.disabled = this.ContactInternalSelector.Input.checked;
	this.ExternalContactPhone.disabled = this.ContactInternalSelector.Input.checked;
	
}

/* REQUIREMENT CLICKER */

CE_NewRequirementClicker = function(
	parent
	){

	this.Parent = parent;
	this.TypeName = "NewRequirementClicker";
	
	this.DataIdentifierCounter = 0;
	
	this.Interface = this.Parent.Interface;
	
	this.Container = document.createElement("div");
	this.Container.className = "Button centered";
	
	this.Container.append(HTML.text(this.Interface.GetDictionaryTerm("Add New Requirement")));
	this.Container.Parent = this;
	
	this.Container.onclick = function(){

		//show the modal container
		this.Parent.Parent.Interface.ShowAsentiaModalContainer(true);

		// launch the modal
		this.Parent.ModalWindow = new Asentia_ModalWindow(
			this.Parent,
			this.Parent.Interface.ModalDOMId, 		//attach to node
			this.Parent.Interface.GetDictionaryTerm("New Requirement"), 
			"asentia-modal-container", //this.Parent.Panel.id,  				// containment
			this.Parent.Parent.DeleteImagePath 							// close button image path
			)
		
		//override the close button function to add extra functionality to it	
		this.Parent.ModalWindow.CloseButton.onclick = function()
		{
			this.Parent.Close();
			
			//hide the containing window
			this.Parent.Parent.Interface.ShowAsentiaModalContainer(false);
		}
		
		// create the interface
		
		this.Parent.EditWindowInterface = new CE_RequirementSelectTypeInterface(
			this.Parent.ModalWindow
			);
		
		// attach the interface
		this.Parent.ModalWindow.Panel.append(this.Parent.EditWindowInterface.Body);
		this.Parent.ModalWindow.Panel.append(this.Parent.EditWindowInterface.Separator);
		this.Parent.ModalWindow.Panel.append(this.Parent.EditWindowInterface.ButtonContainer);
		this.Parent.ModalWindow.Reposition();
		
	}
	
}

CE_NewRequirementClicker.prototype.GetNextDataIdentifier = function(){

	this.DataIdentifierCounter++;
	return "A" + this.DataIdentifierCounter.toString();
	
}

CE_NewRequirementClicker.prototype.ClearModal = function(){
	
	// remove all 
	this.ModalWindow.Panel.removeChild(this.EditWindowInterface.Body);
	this.ModalWindow.Panel.removeChild(this.EditWindowInterface.Separator);
	this.ModalWindow.Panel.removeChild(this.EditWindowInterface.ButtonContainer);
		
}

CE_NewRequirementClicker.prototype.ChooseCourse = function(){
	
	this.ClearModal();
	
	// create the interface
	this.EditWindowInterface = new CE_CourseRequirementInterface(
		this,
		this.ModalWindow,
		"", //this.Identifier + "NewCourseRequirementWindow",
		this.Interface.Languages,
		this.Interface.InitialLanguage,
		this.Interface.FlagImagePath,
		this.Interface.DeleteImagePath,
		this.Interface.AddImagePath,
		""
		);
	
	// attach the interface
	this.ModalWindow.Panel.append(this.EditWindowInterface.Body);
	this.ModalWindow.Panel.append(this.EditWindowInterface.Separator);
	this.ModalWindow.Panel.append(this.EditWindowInterface.ButtonContainer);
	this.ModalWindow.Reposition();
	
}

CE_NewRequirementClicker.prototype.ChooseCredit = function(){
	
	this.ClearModal();
	
	// create the interface
	this.EditWindowInterface = new CE_CreditRequirementInterface(
		this,
		this.ModalWindow,
		"", //this.Identifier + "NewCourseRequirementWindow",
		this.Interface.Languages,
		this.Interface.InitialLanguage,
		this.Interface.FlagImagePath,
		this.Interface.DeleteImagePath,
		this.Interface.AddImagePath,
		""
		);
	
	// attach the interface
	this.ModalWindow.Panel.append(this.EditWindowInterface.Body);
	this.ModalWindow.Panel.append(this.EditWindowInterface.Separator);
	this.ModalWindow.Panel.append(this.EditWindowInterface.ButtonContainer);
	this.ModalWindow.Reposition();
	
}

CE_NewRequirementClicker.prototype.ChooseTask = function(){
	
	this.ClearModal();
	
	// create the interface
	this.EditWindowInterface = new CE_TaskRequirementInterface(
		this,
		this.ModalWindow,
		"", //this.Identifier + "NewCourseRequirementWindow",
		this.Interface.Languages,
		this.Interface.InitialLanguage,
		this.Interface.FlagImagePath,
		this.Interface.DeleteImagePath,
		this.Interface.AddImagePath,
		""
		);
	
	// attach the interface
	this.ModalWindow.Panel.append(this.EditWindowInterface.Body);
	this.ModalWindow.Panel.append(this.EditWindowInterface.Separator);
	this.ModalWindow.Panel.append(this.EditWindowInterface.ButtonContainer);
	this.ModalWindow.Reposition();
	
}

CE_NewRequirementClicker.prototype.PushRequirementInterface = function(
	data
	){
	
	this.Parent.PushRequirementInterface(data);
	
}