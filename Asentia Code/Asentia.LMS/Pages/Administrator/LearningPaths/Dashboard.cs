﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.LearningPaths
{
    public class Dashboard : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel LearningPathDashboardFormContentWrapperContainer;
        public Panel LearningPathObjectMenuContainer;
        public Panel LearningPathDashboardWrapperContainer;
        public Panel LearningPathDashboardInstructionsPanel;
        public Panel LearningPathDashboardFeedbackContainer;
        #endregion

        #region Private Properties
        private LearningPath _LearningPathObject;
        private EcommerceSettings _EcommerceSettings;
        private ObjectDashboard _LearningPathObjectDashboard;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            //csm.RegisterClientScriptResource(typeof(Dashboard), "Asentia.LMS.Pages.Administrator.LearningPaths.Dashboard.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Load event of the page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathContentManager) &&
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("ObjectDashboard.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/learningpaths/Dashboard.css");

            // get the learning path object
            this._GetLearningPathObject();

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetLearningPathObject
        /// <summary>
        /// Gets a learning path object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetLearningPathObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._LearningPathObject = new LearningPath(id); }
                }
                catch
                { Response.Redirect("~/administrator/learningpath"); }
            }
            else { Response.Redirect("~/administrator/learningpath"); }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.LearningPathDashboardFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.LearningPathDashboardWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the learning path object menu
            if (this._LearningPathObject != null)
            {
                LearningPathObjectMenu learningPathObjectMenu = new LearningPathObjectMenu(this._LearningPathObject);
                learningPathObjectMenu.SelectedItem = LearningPathObjectMenu.MenuObjectItem.LearningPathDashboard;

                this.LearningPathObjectMenuContainer.Controls.Add(learningPathObjectMenu);
            }            

            // build the learning path object dashboard
            this._LearningPathObjectDashboard = new ObjectDashboard("LearningPathObjectDashboard");

            this._BuildInformationWidget();
            this._BuildActionsWidget();
            this._BuildEnrollmentsWidget();
            this._BuildCourseStatsWidget();

            if (this._EcommerceSettings.IsEcommerceSetAndVerified)
            { this._BuildEcommerceWidget(); }

            // attach object widget to wrapper container
            this.LearningPathDashboardWrapperContainer.Controls.Add(this._LearningPathObjectDashboard);
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get learning path title information
            string learningPathTitleInInterfaceLanguage = this._LearningPathObject.Name;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (LearningPath.LanguageSpecificProperty learningPathLanguageSpecificProperty in this._LearningPathObject.LanguageSpecificProperties)
                {
                    if (learningPathLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { learningPathTitleInInterfaceLanguage = learningPathLanguageSpecificProperty.Name; }
                }
            }

            // get learning path avatar information
            string learningPathImagePath;
            string imageCssClass = null;

            if (this._LearningPathObject.Avatar != null)
            {
                learningPathImagePath = SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + this._LearningPathObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                imageCssClass = "AvatarImage";
            }
            else
            {
                learningPathImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD, ImageFiles.EXT_PNG);
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.LearningPaths, "/administrator/learningpaths"));
            breadCrumbLinks.Add(new BreadcrumbLink(learningPathTitleInInterfaceLanguage));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, learningPathTitleInInterfaceLanguage, learningPathImagePath, imageCssClass);
        }
        #endregion

        #region _BuildInformationWidget
        /// <summary>
        /// Builds the information widget.
        /// </summary>
        private void _BuildInformationWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> informationWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // CREATED DATE
            string formattedCreatedDate = TimeZoneInfo.ConvertTimeFromUtc(this._LearningPathObject.DtCreated, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("CreatedDate", _GlobalResources.Created + ":", formattedCreatedDate, ObjectDashboard.WidgetItemType.Text));

            // MODIFIED DATE
            string formattedModifiedDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._LearningPathObject.DtModified, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.LongDatePattern.Replace("dddd,", "").Replace("dddd", ""));
            informationWidgetItems.Add(new ObjectDashboard.WidgetItem("LastModifiedDate", _GlobalResources.LastModified + ":", formattedModifiedDate, ObjectDashboard.WidgetItemType.Text));

            // add the widget
            this._LearningPathObjectDashboard.AddWidget("InformationWidget", _GlobalResources.Information, informationWidgetItems);
        }
        #endregion

        #region _BuildActionsWidget
        /// <summary>
        /// Builds the actions widget.
        /// </summary>
        private void _BuildActionsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> actionsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // build links for material, certificate, and email notification if the user has learning path content manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathContentManager)) {
                // NEW LEARNING PATH MATERIAL LINK

                Panel createLearningPathMaterialLinkWrapper = new Panel();

                HyperLink createLearningPathMaterialLink = new HyperLink();
                createLearningPathMaterialLink.NavigateUrl = "Modify.aspx?id=" + this._LearningPathObject.Id.ToString() + "&action=NewLearningPathMaterial";
                createLearningPathMaterialLinkWrapper.Controls.Add(createLearningPathMaterialLink);

                Image createLearningPathMaterialLinkImage = new Image();
                createLearningPathMaterialLinkImage.CssClass = "SmallIcon";
                createLearningPathMaterialLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
                createLearningPathMaterialLink.Controls.Add(createLearningPathMaterialLinkImage);

                Image createLearningPathMaterialOverlayLinkImage = new Image();
                createLearningPathMaterialOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                createLearningPathMaterialOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                createLearningPathMaterialLink.Controls.Add(createLearningPathMaterialOverlayLinkImage);

                Literal createLearningPathMaterialLinkText = new Literal();
                createLearningPathMaterialLinkText.Text = _GlobalResources.CreateANewLearningPathMaterial;
                createLearningPathMaterialLink.Controls.Add(createLearningPathMaterialLinkText);

                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateLearningPathMaterial", null, createLearningPathMaterialLinkWrapper, ObjectDashboard.WidgetItemType.Link));

                // NEW CERTIFICATE LINK

                Panel createCertificateLinkWrapper = new Panel();

                HyperLink createCertificateLink = new HyperLink();
                createCertificateLink.NavigateUrl = "certificates/Modify.aspx?lpid=" + this._LearningPathObject.Id.ToString();
                createCertificateLinkWrapper.Controls.Add(createCertificateLink);

                Image createCertificateLinkImage = new Image();
                createCertificateLinkImage.CssClass = "SmallIcon";
                createCertificateLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE, ImageFiles.EXT_PNG);
                createCertificateLink.Controls.Add(createCertificateLinkImage);

                Image createCertificateOverlayLinkImage = new Image();
                createCertificateOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                createCertificateOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                createCertificateLink.Controls.Add(createCertificateOverlayLinkImage);

                Literal createCertificateLinkText = new Literal();
                createCertificateLinkText.Text = _GlobalResources.CreateANewCertificate;
                createCertificateLink.Controls.Add(createCertificateLinkText);

                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateCertificate", null, createCertificateLinkWrapper, ObjectDashboard.WidgetItemType.Link));

                // NEW EMAIL NOTIFICATION LINK

                Panel createEmailNotificationLinkWrapper = new Panel();

                HyperLink createEmailNotificationLink = new HyperLink();
                createEmailNotificationLink.NavigateUrl = "emailnotifications/Modify.aspx?lpid=" + this._LearningPathObject.Id.ToString();
                createEmailNotificationLinkWrapper.Controls.Add(createEmailNotificationLink);

                Image createEmailNotificationLinkImage = new Image();
                createEmailNotificationLinkImage.CssClass = "SmallIcon";
                createEmailNotificationLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
                createEmailNotificationLink.Controls.Add(createEmailNotificationLinkImage);

                Image createEmailNotificationOverlayLinkImage = new Image();
                createEmailNotificationOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                createEmailNotificationOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                createEmailNotificationLink.Controls.Add(createEmailNotificationOverlayLinkImage);

                Literal createEmailNotificationLinkText = new Literal();
                createEmailNotificationLinkText.Text = _GlobalResources.CreateANewEmailNotification;
                createEmailNotificationLink.Controls.Add(createEmailNotificationLinkText);

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OBJECTSPECIFICEMAILNOTIFICATIONS_ENABLE) ?? false)
                {
                    actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateEmailNotification", null, createEmailNotificationLinkWrapper, ObjectDashboard.WidgetItemType.Link));
                }
            }

            // build the enrollment link if the user has learning path enrollment manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager))
            {
                // NEW ENROLLMENT LINK

                Panel createEnrollmentLinkWrapper = new Panel();

                HyperLink createEnrollmentLink = new HyperLink();
                createEnrollmentLink.NavigateUrl = "rulesetenrollments/Modify.aspx?lpid=" + this._LearningPathObject.Id.ToString();
                createEnrollmentLinkWrapper.Controls.Add(createEnrollmentLink);

                Image createEnrollmentLinkImage = new Image();
                createEnrollmentLinkImage.CssClass = "SmallIcon";
                createEnrollmentLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG);
                createEnrollmentLink.Controls.Add(createEnrollmentLinkImage);

                Image createEnrollmentOverlayLinkImage = new Image();
                createEnrollmentOverlayLinkImage.CssClass = "XSmallIcon OverlayIcon";
                createEnrollmentOverlayLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
                createEnrollmentLink.Controls.Add(createEnrollmentOverlayLinkImage);

                Literal createEnrollmentLinkText = new Literal();
                createEnrollmentLinkText.Text = _GlobalResources.CreateANewEnrollment;
                createEnrollmentLink.Controls.Add(createEnrollmentLinkText);

                actionsWidgetItems.Add(new ObjectDashboard.WidgetItem("CreateEnrollment", null, createEnrollmentLinkWrapper, ObjectDashboard.WidgetItemType.Link));
            }

            // add the widget
            this._LearningPathObjectDashboard.AddWidget("ActionsWidget", _GlobalResources.Actions, actionsWidgetItems);
        }
        #endregion        

        #region _BuildEnrollmentsWidget
        /// <summary>
        /// Builds the enrollments widget.
        /// </summary>
        private void _BuildEnrollmentsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> enrollmentsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and build a chart
            DataTable learningPathEnrollmentsStatusDt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            Chart learningPathEnrollmentsPieChart;

            // chart colors
            List<string> learningPathEnrollmentsPieChartColors = new List<string>();
            learningPathEnrollmentsPieChartColors.Add("#2E7DBD");
            learningPathEnrollmentsPieChartColors.Add("#55BD86");
            learningPathEnrollmentsPieChartColors.Add("#FED155");
            learningPathEnrollmentsPieChartColors.Add("#EC6E61");

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idLearningPath", this._LearningPathObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.EnrollmentStatistics]", true);

                // load recordset
                learningPathEnrollmentsStatusDt.Load(sdr);

                sdr.Close();

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // draw the chart
                learningPathEnrollmentsPieChart = new Chart("LearningPathEnrollmentsPieChart", ChartType.Doughnut, learningPathEnrollmentsStatusDt, learningPathEnrollmentsPieChartColors, _GlobalResources.Enrollments, null);
                learningPathEnrollmentsPieChart.ShowTotalInTitle = true;
                learningPathEnrollmentsPieChart.ShowTotalAndPercentageInLegend = true;
                learningPathEnrollmentsPieChart.IsResponsive = false;
                learningPathEnrollmentsPieChart.CanvasWidth = 125;
                learningPathEnrollmentsPieChart.CanvasHeight = 125;
            }
            catch // on errors, just draw an empty chart
            {
                // draw an empty chart
                DataTable emptyDataTable = new DataTable();
                emptyDataTable.Columns.Add("_Total_", typeof(int));
                emptyDataTable.Rows.Add(0);

                learningPathEnrollmentsPieChart = new Chart("LearningPathEnrollmentsPieChart", ChartType.Doughnut, emptyDataTable, learningPathEnrollmentsPieChartColors, _GlobalResources.Enrollments, null);
                learningPathEnrollmentsPieChart.ShowTotalInTitle = true;
                learningPathEnrollmentsPieChart.ShowTotalAndPercentageInLegend = false;
                learningPathEnrollmentsPieChart.IsResponsive = false;
                learningPathEnrollmentsPieChart.CanvasWidth = 125;
                learningPathEnrollmentsPieChart.CanvasHeight = 125;
            }
            finally
            { databaseObject.Dispose(); }

            // add the chart to the widget item list
            enrollmentsWidgetItems.Add(new ObjectDashboard.WidgetItem("EnrollmentStats", null, learningPathEnrollmentsPieChart, ObjectDashboard.WidgetItemType.Object));

            // add the widget
            this._LearningPathObjectDashboard.AddWidget("EnrollmentsWidget", _GlobalResources.Enrollments, enrollmentsWidgetItems);
        }
        #endregion

        #region _BuildCourseStatsWidget
        /// <summary>
        /// Builds the course stats widget.
        /// </summary>
        private void _BuildCourseStatsWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> courseStatsWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // get the data and add it to the widget items            
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            string totalCourses = "-";
            string easiestCourseTitle = "-";
            string hardestCourseTitle = "-";
            string averageTimePerCourse = "-";

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idLearningPath", this._LearningPathObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@totalCourses", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@easiestCourse", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
                databaseObject.AddParameter("@hardestCourse", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
                databaseObject.AddParameter("@averageTimePerCourse", null, SqlDbType.NVarChar, 8, ParameterDirection.Output);

                databaseObject.ExecuteNonQuery("[LearningPath.CourseStatistics]", true);

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // format the data strings
                totalCourses = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@totalCourses"].Value).ToString();

                if (!String.IsNullOrWhiteSpace(AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@easiestCourse"].Value)))
                { easiestCourseTitle = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@easiestCourse"].Value); }

                if (!String.IsNullOrWhiteSpace(AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@hardestCourse"].Value)))
                { hardestCourseTitle = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@hardestCourse"].Value); }

                if (!String.IsNullOrWhiteSpace(AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@averageTimePerCourse"].Value)))
                { averageTimePerCourse = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@averageTimePerCourse"].Value); }
            }
            catch // on errors, just bury it
            { }
            finally
            { databaseObject.Dispose(); }

            // add the items to the widget item list
            courseStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("TotalCourses", _GlobalResources.TotalCourses + ":", totalCourses, ObjectDashboard.WidgetItemType.Text));
            courseStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("EasiestCourse", _GlobalResources.EasiestCourse + ":", easiestCourseTitle, ObjectDashboard.WidgetItemType.Text));
            courseStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("HardestCourse", _GlobalResources.HardestCourse + ":", hardestCourseTitle, ObjectDashboard.WidgetItemType.Text));
            courseStatsWidgetItems.Add(new ObjectDashboard.WidgetItem("AverageTimePerCourse", _GlobalResources.AverageTimeCourse + ":", averageTimePerCourse, ObjectDashboard.WidgetItemType.Text));

            // add the widget
            this._LearningPathObjectDashboard.AddWidget("ModuleStatsWidget", _GlobalResources.CourseStats, courseStatsWidgetItems);
        }
        #endregion        

        #region _BuildEcommerceWidget
        /// <summary>
        /// Builds the ecommerce widget.
        /// </summary>
        private void _BuildEcommerceWidget()
        {
            // create a list for widget items
            List<ObjectDashboard.WidgetItem> ecommerceWidgetItems = new List<ObjectDashboard.WidgetItem>();

            // COST
            if (this._LearningPathObject.Cost > 0)
            {
                string formattedCourseCost = String.Format("{0}{1:N2} ({2})", this._EcommerceSettings.CurrencySymbol, this._LearningPathObject.Cost, this._EcommerceSettings.CurrencyCode);
                ecommerceWidgetItems.Add(new ObjectDashboard.WidgetItem("Cost", _GlobalResources.Cost + ":", formattedCourseCost, ObjectDashboard.WidgetItemType.Text));
            }
            else
            { ecommerceWidgetItems.Add(new ObjectDashboard.WidgetItem("Cost", _GlobalResources.Cost + ":", _GlobalResources.Free, ObjectDashboard.WidgetItemType.Text)); }

            // TOTAL EARNINGS

            // get the data and add it to the widget items            
            AsentiaDatabase databaseObject = new AsentiaDatabase();
            double? totalEarnings = 0;

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idObject", this._LearningPathObject.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@itemType", PurchaseItemType.LearningPath, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@totalEarnings", null, SqlDbType.Float, 8, ParameterDirection.Output);

                databaseObject.ExecuteNonQuery("[TransactionItem.GetTotalEarningsForObject]", true);

                // check for errors
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // format the data strings
                totalEarnings = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@totalEarnings"].Value);

                if (totalEarnings == null)
                { totalEarnings = 0; }
            }
            catch // on errors, just bury it
            { }
            finally
            { databaseObject.Dispose(); }

            // add the earnings to the widget items
            string formattedCourseEarnings = String.Format("{0}{1:N2} ({2})", this._EcommerceSettings.CurrencySymbol, totalEarnings, this._EcommerceSettings.CurrencyCode);
            ecommerceWidgetItems.Add(new ObjectDashboard.WidgetItem("TotalEarnings", _GlobalResources.TotalEarnings + ":", formattedCourseEarnings, ObjectDashboard.WidgetItemType.Text));

            // add the widget
            this._LearningPathObjectDashboard.AddWidget("EcommerceWidget", _GlobalResources.ECommerce, ecommerceWidgetItems);
        }
        #endregion
    }
}