﻿function LoadUnpublishLearningPathModalContent(idLearningPath) {
    $("#LearningPathUnpublishData").val(idLearningPath);
    $("#UnpublishLearningPathConfirmationModalLaunchButton").click();
    $("#UnpublishLearningPathConfirmationModalLoadButton").click();
}

function LoadPublishLearningPathModalContent(idLearningPath) {
    $("#LearningPathPublishData").val(idLearningPath);

    $("#PublishLearningPathConfirmationModalLaunchButton").click();
    $("#PublishLearningPathConfirmationModalLoadButton").click();
}

function LoadOpenLearningPathModalContent(idLearningPath) {
    $("#LearningPathOpenData").val(idLearningPath);

    $("#OpenLearningPathConfirmationModalLaunchButton").click();
    $("#OpenLearningPathConfirmationModalLoadButton").click();
}

function LoadCloseLearningPathModalContent(idLearningPath) {
    $("#LearningPathCloseData").val(idLearningPath);

    $("#CloseLearningPathConfirmationModalLaunchButton").click();
    $("#CloseLearningPathConfirmationModalLoadButton").click();
}