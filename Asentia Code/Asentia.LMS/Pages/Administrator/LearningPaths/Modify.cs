﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Administrator.LearningPaths
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel LearningPathPropertiesFormContentWrapperContainer;
        public Panel LearningPathObjectMenuContainer;
        public Panel LearningPathPropertiesWrapperContainer;
        public Panel LearningPathPropertiesInstructionsPanel;
        public Panel LearningPathPropertiesFeedbackContainer;
        public Panel LearningPathPropertiesContainer;
        public Panel LearningPathTabPanelsContainer;
        public Panel LearningPathPropertiesActionsPanel;
        #endregion

        #region Private Properties
        private HiddenField _PageAction;

        // LEARNING PATH MODIFY

        private LearningPath _LearningPathObject;
        private EcommerceSettings _EcommerceSettings;

        private UploaderAsync _LearningPathAvatar;

        private TextBox _LearningPathName;
        private TextBox _LearningPathShortDescription;
        private TextBox _LearningPathLongDescription;
        private TextBox _LearningPathSearchTags;
        private TextBox _LearningPathCost;

        private RadioButtonList _LearningPathPublished;
        private RadioButtonList _LearningPathClosed;

        private CheckBox _LearningPathForceCompletionInOrder;
        private CheckBox _LearningPathSelfEnrollmentIsOneTimeOnly;

        private Button _LearningPathPropertiesSaveButton;
        private Button _LearningPathPropertiesCancelButton;

        private DataTable _LearningPathCourses;
        private DataTable _EligibleLerarningPathCoursesForSelectList;
        private DynamicListBox _SelectEligibleLearningPathCourses;

        private HiddenField _SelectedLearningPathCourses;        
        private Panel _LearningPathCoursesListContainer;
        private ModalPopup _SelectCoursesForLearningPath;

        private Image _AvatarImage;
        private HiddenField _ClearAvatar;

        private TextBox _Shortcode;

        // LEARNING PATH MATERIALS GRID

        private Grid _LearningPathMaterialGrid;
        private ModalPopup _LearningPathMaterialGridConfirmAction;
        private LinkButton _LearningPathMaterialGridDeleteButton;

        private ModalPopup _LearningPathMaterialMakePrivateConfirmationModal;
        private Button _LearningPathMaterialMakePrivateConfirmationModalLaunchButton;
        private Button _LearningPathMaterialMakePrivateConfirmationModalLoadButton;

        private ModalPopup _LearningPathMaterialMakePublicConfirmationModal;
        private Button _LearningPathMaterialMakePublicConfirmationModalLaunchButton;
        private Button _LearningPathMaterialMakePublicConfirmationModalLoadButton;

        // LEARNING PATH MATERIAL MODIFY

        private DocumentRepositoryItem _LearningPathMaterialObject;

        private ModalPopup _LearningPathMaterialModifyModal;
        private Button _LearningPathMaterialModifyModalLaunchButton;
        private Button _LearningPathMaterialModifyModalLoadButton;
        private HiddenField _LearningPathMaterialModifyData;

        private UploaderAsync _LearningPathMaterialUploader;
        private LanguageSelector _LearningPathMaterialLanguage;

        private TextBox _LearningPathMaterialLabel;
        private TextBox _LearningPathMaterialSearchTags;

        private CheckBox _LearningPathMaterialIsPrivate;
        private CheckBox _LearningPathMaterialIsAllLanguages;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {            
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoadCKEditor.js");
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.LearningPaths.Modify.js");

            // build start up call for MCE and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", false));
            multipleStartUpCallsScript.AppendLine(Asentia.Common.Utility.GetCSSFilePathForCKEditor("Layout.css", true));

            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine(" LoadCKEditor();");
            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine(" // initialize sortable on course list container");
            multipleStartUpCallsScript.AppendLine(" $(\"#\" + LearningPathCoursesListContainer).sortable();");

            // javascript for page action querystring, i.e. New Learning Path Material
            if (this._PageAction.Value == "NewLearningPathMaterial" && !Page.IsPostBack)
            {
                multipleStartUpCallsScript.AppendLine("");
                multipleStartUpCallsScript.AppendLine(" if ($(\"#PageAction\").val() == \"NewLearningPathMaterial\") {");
                multipleStartUpCallsScript.AppendLine("     $(\"#PageAction\").val(\"\");");
                multipleStartUpCallsScript.AppendLine("     Helper.ToggleTab('LearningPathMaterials', 'LearningPathProperties');");
                multipleStartUpCallsScript.AppendLine("     LearningPathPropertiesTabChange('LearningPathMaterials', 'LearningPathProperties');");
                multipleStartUpCallsScript.AppendLine("     LoadLearningPathMaterialModifyModalContent(0, false);");
                multipleStartUpCallsScript.AppendLine(" }");
            }
            else
            { }

            multipleStartUpCallsScript.AppendLine("");
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);

            // build global JS variables
            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("LearningPathCoursesListContainer = \"" + this._LearningPathCoursesListContainer.ID + "\";");
            globalJS.AppendLine("SelectedLearningPathCourses = \"" + this._SelectedLearningPathCourses.ID + "\";");

            csm.RegisterClientScriptBlock(typeof(Modify), "GlobalJS", globalJS.ToString(), true);

            base.OnPreRender(e);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathContentManager) &&
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/learningpaths/Modify.css");

            // get learning path object
            this._GetLearningPathObject();

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetLearningPathObject
        /// <summary>
        /// Gets a learning path object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetLearningPathObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._LearningPathObject = new LearningPath(id); }
                }
                catch
                { Response.Redirect("~/administrator/learningpaths"); }
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to build the controls on the page.
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // set container classes
            this.LearningPathPropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.LearningPathPropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the learning path object menu if we're editing a learning path
            if (this._LearningPathObject != null)
            {
                LearningPathObjectMenu learningPathObjectMenu = new LearningPathObjectMenu(this._LearningPathObject);
                learningPathObjectMenu.SelectedItem = LearningPathObjectMenu.MenuObjectItem.LearningPathProperties;

                this.LearningPathObjectMenuContainer.Controls.Add(learningPathObjectMenu);
            }

            // build the learning path properties form
            this._BuildLearningPathPropertiesForm();

            // build the learning path properties form actions panel
            this._BuildLearningPathPropertiesActionsPanel();

            // build learning path material grid actions modals if there is a learning path object
            if (this._LearningPathObject != null)
            {
                // build the learning path material grid actions modal
                this._BuildLearningPathMaterialGridActionsModal();

                // instansiate learning path material make private controls
                this._LearningPathMaterialMakePrivateConfirmationModalLaunchButton = new Button();
                this._LearningPathMaterialMakePrivateConfirmationModalLaunchButton.ID = "LearningPathMaterialMakePrivateConfirmationModalLaunchButton";
                this._LearningPathMaterialMakePrivateConfirmationModalLaunchButton.Style.Add("display", "none");
                this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathMaterialMakePrivateConfirmationModalLaunchButton);

                this._LearningPathMaterialMakePrivateConfirmationModalLoadButton = new Button();
                this._LearningPathMaterialMakePrivateConfirmationModalLoadButton.ID = "LearningPathMaterialMakePrivateConfirmationModalLoadButton";
                this._LearningPathMaterialMakePrivateConfirmationModalLoadButton.Style.Add("display", "none");
                this._LearningPathMaterialMakePrivateConfirmationModalLoadButton.Click += this._LoadLearningPathMaterialMakePrivateConfirmationModalContent;

                // instansiate learning path material make public controls
                this._LearningPathMaterialMakePublicConfirmationModalLaunchButton = new Button();
                this._LearningPathMaterialMakePublicConfirmationModalLaunchButton.ID = "LearningPathMaterialMakePublicConfirmationModalLaunchButton";
                this._LearningPathMaterialMakePublicConfirmationModalLaunchButton.Style.Add("display", "none");
                this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathMaterialMakePublicConfirmationModalLaunchButton);

                this._LearningPathMaterialMakePublicConfirmationModalLoadButton = new Button();
                this._LearningPathMaterialMakePublicConfirmationModalLoadButton.ID = "LearningPathMaterialMakePublicConfirmationModalLoadButton";
                this._LearningPathMaterialMakePublicConfirmationModalLoadButton.Style.Add("display", "none");
                this._LearningPathMaterialMakePrivateConfirmationModalLoadButton.Click += this._LoadLearningPathMaterialMakePublicConfirmationModalContent;

                // build the confirmation modals for making learning path materials private/public
                this._BuildLearningPathMaterialMakePrivateConfirmationModal();
                this._BuildLearningPathMaterialMakePublicConfirmationModal();

                // instansiate learning path material modify modal controls
                this._LearningPathMaterialModifyModalLaunchButton = new Button();
                this._LearningPathMaterialModifyModalLaunchButton.ID = "LearningPathMaterialModifyModalLaunchButton";
                this._LearningPathMaterialModifyModalLaunchButton.Style.Add("display", "none");
                this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathMaterialModifyModalLaunchButton);

                this._LearningPathMaterialModifyModalLoadButton = new Button();
                this._LearningPathMaterialModifyModalLoadButton.ID = "LearningPathMaterialModifyModalLoadButton";
                this._LearningPathMaterialModifyModalLoadButton.Style.Add("display", "none");
                this._LearningPathMaterialModifyModalLoadButton.Click += this._LoadLearningPathMaterialModifyModalContent;

                // build the learning path material modify modal
                this._BuildLearningPathMaterialModifyModal();
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string learningPathImagePath;
            string imageCssClass = null;
            string pageTitle;

            if (this._LearningPathObject != null)
            {
                string learningPathTitleInInterfaceLanguage = this._LearningPathObject.Name;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (LearningPath.LanguageSpecificProperty learningPathLanguageSpecificProperty in this._LearningPathObject.LanguageSpecificProperties)
                    {
                        if (learningPathLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { learningPathTitleInInterfaceLanguage = learningPathLanguageSpecificProperty.Name; }
                    }
                }

                breadCrumbPageTitle = learningPathTitleInInterfaceLanguage;

                if (this._LearningPathObject.Avatar != null)
                {
                    learningPathImagePath = SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + this._LearningPathObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    imageCssClass = "AvatarImage";
                }
                else
                {
                    learningPathImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
                }

                pageTitle = learningPathTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewLearningPath;
                learningPathImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
                pageTitle = _GlobalResources.NewLearningPath;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.LearningPaths, "/administrator/learningpaths"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, pageTitle, learningPathImagePath, imageCssClass);
        }
        #endregion

        #region _BuildLearningPathPropertiesForm
        /// <summary>
        /// Builds the Learning Path Properties form.
        /// </summary>
        private void _BuildLearningPathPropertiesForm()
        {            
            // clear controls from container
            this.LearningPathPropertiesContainer.Controls.Clear();

            // build the learning path properties form tabs
            this._BuildLearningPathPropertiesFormTabs();

            // add learning path material link container
            Panel addLearningPathMaterialLinkContainer = new Panel();
            addLearningPathMaterialLinkContainer.ID = "AddLearningPathMaterialLinkContainer";
            addLearningPathMaterialLinkContainer.CssClass = "ObjectOptionsPanel";
            addLearningPathMaterialLinkContainer.Style.Add("display", "none");

            // NEW LEARNING PATH MATERIAL
            addLearningPathMaterialLinkContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddLearningPathMaterialLink",
                                                null,
                                                "javascript: void(0);",
                                                "LoadLearningPathMaterialModifyModalContent(0, true);",
                                                _GlobalResources.NewLearningPathMaterial,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.LearningPathPropertiesContainer.Controls.Add(addLearningPathMaterialLinkContainer);

            // form instructions panel - "Properties" is the first tab so do not hide this
            Panel formInstructionsPanel = new Panel();
            formInstructionsPanel.ID = "LearningPathFormInstructionsPanel";
            this.FormatPageInformationPanel(formInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfThisLearningPath, true);
            this.LearningPathPropertiesContainer.Controls.Add(formInstructionsPanel);

            // tab panels container
            this.LearningPathTabPanelsContainer = new Panel();
            this.LearningPathTabPanelsContainer.ID = "LearningPathProperties_TabPanelsContainer";
            this.LearningPathTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.LearningPathPropertiesContainer.Controls.Add(this.LearningPathTabPanelsContainer);

            // build the "properties" panel of the learning path properties form
            this._BuildLearningPathPropertiesFormPropertiesPanel();

            // build the "courses" panel of the learning path properties form
            this._BuildLearningPathPropertiesFormCoursesPanel();

            if (this._LearningPathObject != null)
            {                
                // build the "learning path materials" panel of the learning path properties form
                this._BuildLearningPathPropertiesFormLearningPathMaterialsPanel();
            }

            // populate the form input elements
            this._PopulateLearningPathPropertiesInputElements();
        }
        #endregion

        #region _BuildLearningPathPropertiesFormTabs
        /// <summary>
        /// Builds the tabs of learning path properties form.
        /// </summary>
        private void _BuildLearningPathPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Courses", _GlobalResources.Courses));

            if (this._LearningPathObject != null)
            {                
                tabs.Enqueue(new KeyValuePair<string, string>("LearningPathMaterials", _GlobalResources.Materials));
            }

            // build and attach the tabs
            this.LearningPathPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("LearningPathProperties", tabs, null, this.Page, "LearningPathPropertiesTabChange"));
        }
        #endregion

        #region _BuildLearningPathPropertiesFormPropertiesPanel
        /// <summary>
        /// Builds the learning path properties form fields under properties tab.
        /// </summary>
        private void _BuildLearningPathPropertiesFormPropertiesPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "LearningPathProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.CssClass = "TabPanelContainer";
            propertiesPanel.Attributes.Add("style", "display: block;");

            // learning path name field
            this._LearningPathName = new TextBox();
            this._LearningPathName.ID = "LearningPathName_Field";
            this._LearningPathName.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathName",
                                                             _GlobalResources.Name,
                                                             this._LearningPathName.ID,
                                                             this._LearningPathName,
                                                             true,
                                                             true,
                                                             true));

            // catalog link shortcode            
            List<Control> shortcodeInputControls = new List<Control>();

            this._Shortcode = new TextBox();
            this._Shortcode.ID = "LearningPathShortcode_Field";
            this._Shortcode.CssClass = "InputShort";
            this._Shortcode.MaxLength = 10;
            shortcodeInputControls.Add(this._Shortcode);

            if (this._LearningPathObject != null)
            {
                if (!String.IsNullOrWhiteSpace(this._LearningPathObject.Shortcode) && (bool)this._LearningPathObject.IsPublished)
                {
                    // build the shortcode link
                    Panel shortcodeUrlLinkContainer = new Panel();
                    shortcodeUrlLinkContainer.ID = "ShortcodeUrlLinkContainer";

                    string shortcodeUrl = String.Empty;

                    shortcodeUrl = "https://" + AsentiaSessionState.GlobalSiteObject.Hostname + "." + Config.AccountSettings.BaseDomain + "/catalog/?type=learningpath&sc=" + this._LearningPathObject.Shortcode;

                    HyperLink shortcodeLink = new HyperLink();
                    shortcodeLink.ID = "ShortcodeLink";
                    shortcodeLink.NavigateUrl = shortcodeUrl;
                    shortcodeLink.Text = shortcodeUrl;
                    shortcodeLink.Target = "_blank";

                    shortcodeUrlLinkContainer.Controls.Add(shortcodeLink);
                    shortcodeInputControls.Add(shortcodeUrlLinkContainer);

                    // attach an onchange event to the shortcode text box so the link gets updated as it is being changed
                    this._Shortcode.Attributes.Add("onkeyup", "UpdateShortcodeLink(this.id);");
                }
            }

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LearningPathShortcode",
                                                                                 _GlobalResources.CatalogShortcode,
                                                                                 shortcodeInputControls,
                                                                                 false,
                                                                                 true));

            // cost field            
            if (this._EcommerceSettings.IsEcommerceSet)
            {
                List<Control> learningPathCostInputControls = new List<Control>();

                Label learningPathCostSymbolLabel = new Label();
                learningPathCostSymbolLabel.Text = this._EcommerceSettings.CurrencySymbol + " ";
                learningPathCostInputControls.Add(learningPathCostSymbolLabel);

                this._LearningPathCost = new TextBox();
                this._LearningPathCost.ID = "LearningPathCost_Field";
                this._LearningPathCost.CssClass = "InputXShort";
                learningPathCostInputControls.Add(this._LearningPathCost);

                Label learningPathCostCodeLabel = new Label();
                learningPathCostCodeLabel.Text = " (" + this._EcommerceSettings.CurrencyCode + ") ";
                learningPathCostInputControls.Add(learningPathCostCodeLabel);

                propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LearningPathCost",
                                                                                     _GlobalResources.Cost,
                                                                                     learningPathCostInputControls,
                                                                                     false,
                                                                                     true));
            }
             
            // learning path avatar field
            List<Control> avatarInputControls = new List<Control>();
           
            if (this._LearningPathObject != null)
            {
                if (this._LearningPathObject.Avatar != null)
                {
                    // learning path avatar image panel
                    Panel avatarImageContainer = new Panel();
                    avatarImageContainer.ID = "LearningPathAvatar_Field_ImageContainer";
                    avatarImageContainer.CssClass = "AvatarImageContainer";

                    // avatar image
                    this._AvatarImage = new Image();
                    avatarImageContainer.Controls.Add(this._AvatarImage);
                    avatarInputControls.Add(avatarImageContainer);

                    Panel avatarImageDeleteButtonContainer = new Panel();
                    avatarImageDeleteButtonContainer.ID = "AvatarImageDeleteButtonContainer";
                    avatarImageDeleteButtonContainer.CssClass = "AvatarDeleteButtonContainer";
                    avatarImageContainer.Controls.Add(avatarImageDeleteButtonContainer);

                    // delete course avatar image
                    Image deleteAvatarImage = new Image();
                    deleteAvatarImage.ID = "AvatarImageDeleteButton";
                    deleteAvatarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    deleteAvatarImage.CssClass = "SmallIcon";
                    deleteAvatarImage.Attributes.Add("onclick", "javascript:DeleteLearningPathAvatar();");
                    deleteAvatarImage.Style.Add("cursor", "pointer");

                    avatarImageDeleteButtonContainer.Controls.Add(deleteAvatarImage);

                    // clear avatar hidden field
                    this._ClearAvatar = new HiddenField();
                    this._ClearAvatar.ID = "ClearAvatar_Field";
                    this._ClearAvatar.Value = "false";
                    avatarInputControls.Add(this._ClearAvatar);
                }
            }

            // learning path avatar image upload
            this._LearningPathAvatar = new UploaderAsync("LearningPathAvatar_Field", UploadType.Avatar, "LearningPathAvatar_ErrorContainer");

            // set params to resize the learning path avatar upon upload
            this._LearningPathAvatar.ResizeOnUpload = true;
            this._LearningPathAvatar.ResizeMaxWidth = 256;
            this._LearningPathAvatar.ResizeMaxHeight = 256;

            avatarInputControls.Add(this._LearningPathAvatar);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LearningPathAvatar",
                                                                                 _GlobalResources.Avatar,
                                                                                 avatarInputControls,
                                                                                 false,
                                                                                 true));            

            // learning path is published field
            this._LearningPathPublished = new RadioButtonList();
            this._LearningPathPublished.ID = "LearningPathIsPublished_Field";
            this._LearningPathPublished.Items.Add(new ListItem(_GlobalResources.Yes, "True"));
            this._LearningPathPublished.Items.Add(new ListItem(_GlobalResources.No, "False"));
            this._LearningPathPublished.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this._LearningPathPublished.SelectedIndex = 0;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathPublished",
                                                             _GlobalResources.Published,
                                                             this._LearningPathPublished.ID,
                                                             this._LearningPathPublished,
                                                             false,
                                                             true,
                                                             false));

            // learning path is closed field
            this._LearningPathClosed = new RadioButtonList();
            this._LearningPathClosed.ID = "LearningPathClosed_Field";
            this._LearningPathClosed.Items.Add(new ListItem(_GlobalResources.Yes, "True"));
            this._LearningPathClosed.Items.Add(new ListItem(_GlobalResources.No, "False"));
            this._LearningPathClosed.RepeatDirection = global::System.Web.UI.WebControls.RepeatDirection.Horizontal;
            this._LearningPathClosed.SelectedIndex = 1;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathClosed",
                                                             _GlobalResources.Closed,
                                                             this._LearningPathClosed.ID,
                                                             this._LearningPathClosed,
                                                             false,
                                                             true,
                                                             false));

            // self-enrollment one-time only field
            this._LearningPathSelfEnrollmentIsOneTimeOnly = new CheckBox();
            this._LearningPathSelfEnrollmentIsOneTimeOnly.ID = "IsSelfEnrollmentOneTimeOnly_Field";
            this._LearningPathSelfEnrollmentIsOneTimeOnly.Text = _GlobalResources.LearnersMaySelfEnrollInThisLearningPathOneTimeOnlyRegardlessOfAnyPreviousEnrollmentCompletionsOfThisLearningPath;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("IsSelfEnrollmentOneTimeOnly",
                                               _GlobalResources.SelfEnrollmentOneTimeOnly,
                                               this._LearningPathSelfEnrollmentIsOneTimeOnly.ID,
                                               this._LearningPathSelfEnrollmentIsOneTimeOnly,
                                               false,
                                               true,
                                               false));

            // learning path short description field
            this._LearningPathShortDescription = new TextBox();
            this._LearningPathShortDescription.ID = "LearningPathShortDescription_Field";
            this._LearningPathShortDescription.Style.Add("width", "98%");
            this._LearningPathShortDescription.TextMode = TextBoxMode.MultiLine;
            this._LearningPathShortDescription.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathShortDescription",
                                                             _GlobalResources.ShortDescription,
                                                             this._LearningPathShortDescription.ID,
                                                             this._LearningPathShortDescription,
                                                             true,
                                                             true,
                                                             true));

            // learning path long description field
            this._LearningPathLongDescription = new TextBox();
            this._LearningPathLongDescription.ID = "LearningPathLongDescription_Field";
            this._LearningPathLongDescription.CssClass = "ckeditor";
            this._LearningPathLongDescription.Style.Add("width", "98%");
            this._LearningPathLongDescription.TextMode = TextBoxMode.MultiLine;
            this._LearningPathLongDescription.Rows = 10;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathLongDescription",
                                                             _GlobalResources.Description,
                                                             this._LearningPathLongDescription.ID,
                                                             this._LearningPathLongDescription,
                                                             false,
                                                             true,
                                                             true));

            // learning path search tags field
            this._LearningPathSearchTags = new TextBox();
            this._LearningPathSearchTags.ID = "LearningPathSearchTags_Field";
            this._LearningPathSearchTags.Style.Add("width", "98%");
            this._LearningPathSearchTags.TextMode = TextBoxMode.MultiLine;
            this._LearningPathSearchTags.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathSearchTags",
                                                             _GlobalResources.SearchTags,
                                                             this._LearningPathSearchTags.ID,
                                                             this._LearningPathSearchTags,
                                                             false,
                                                             false,
                                                             true));

            // attach panel to container
            this.LearningPathTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildLearningPathPropertiesFormCoursesPanel
        /// <summary>
        /// Builds the learning path properties form fields under courses tab.
        /// </summary>
        private void _BuildLearningPathPropertiesFormCoursesPanel()
        {
            Panel coursesPanel = new Panel();
            coursesPanel.ID = "LearningPathProperties_" + "Courses" + "_TabPanel";
            coursesPanel.CssClass = "TabPanelContainer";
            coursesPanel.Attributes.Add("style", "display: none;");

            // learning path courses field instructions panel
            Panel learningPathCoursesFieldInstructionsPanel = new Panel();
            learningPathCoursesFieldInstructionsPanel.ID = "LearningPathCoursesFieldInstructionsPanel";
            this.FormatFormInformationPanel(learningPathCoursesFieldInstructionsPanel, _GlobalResources.YouCanSetCourseOrderByDraggingTheCoursesInTheBoxBelow, false);
            coursesPanel.Controls.Add(learningPathCoursesFieldInstructionsPanel);

            // learning path courses field
            List<Control> learningPathCoursesInputControls = new List<Control>();

            // populate datatables with lists of courses that are part of this learning path and ones that are not
            if (this._LearningPathObject != null)
            {
                this._LearningPathCourses = this._LearningPathObject.GetCourses(null);
                this._EligibleLerarningPathCoursesForSelectList = Course.IdsAndNamesForLearningPathSelectList(this._LearningPathObject.Id, null);
            }
            else
            {
                this._EligibleLerarningPathCoursesForSelectList = Course.IdsAndNamesForLearningPathSelectList(0, null);
            }
            
            // force course completion in order field
            this._LearningPathForceCompletionInOrder = new CheckBox();
            this._LearningPathForceCompletionInOrder.ID = "LearningPathForceCompletionInOrder_Field";
            this._LearningPathForceCompletionInOrder.Text = _GlobalResources.ForceCoursesToBeCompletedInOrder;
            learningPathCoursesInputControls.Add(this._LearningPathForceCompletionInOrder);

            // learning path selected courses hidden field
            this._SelectedLearningPathCourses = new HiddenField();
            this._SelectedLearningPathCourses.ID = "SelectedLearningPathCourses_Field";
            learningPathCoursesInputControls.Add(this._SelectedLearningPathCourses);

            // build a container for the course listing
            this._LearningPathCoursesListContainer = new Panel();
            this._LearningPathCoursesListContainer.ID = "LearningPathCoursesList_Container";
            this._LearningPathCoursesListContainer.CssClass = "ItemListingContainer";
            learningPathCoursesInputControls.Add(this._LearningPathCoursesListContainer);

            Panel learningPathCoursesButtonsPanel = new Panel();
            learningPathCoursesButtonsPanel.ID = "LearningPathCourses_ButtonsPanel";

            // select courses button

            // link
            Image selectCoursesImageForLink = new Image();
            selectCoursesImageForLink.ID = "LaunchSelectCoursesModalImage";
            selectCoursesImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            selectCoursesImageForLink.CssClass = "MediumIcon";

            Localize selectCoursesTextForLink = new Localize();
            selectCoursesTextForLink.Text = _GlobalResources.SelectCourse_s;

            // required asterisk
            Label coursesRequiredAsterisk = new Label();
            coursesRequiredAsterisk.Text = " *";
            coursesRequiredAsterisk.CssClass = "RequiredAsterisk";
            

            LinkButton selectCoursesLink = new LinkButton();
            selectCoursesLink.ID = "LaunchSelectExpertsModal";
            selectCoursesLink.CssClass = "ImageLink";
            selectCoursesLink.Controls.Add(selectCoursesImageForLink);
            selectCoursesLink.Controls.Add(selectCoursesTextForLink);
            selectCoursesLink.Controls.Add(coursesRequiredAsterisk);            
            learningPathCoursesButtonsPanel.Controls.Add(selectCoursesLink);

            // attach the buttons panel to the container
            learningPathCoursesInputControls.Add(learningPathCoursesButtonsPanel);

            // build modals for adding and removing courses to/from the learning path
            this._BuildSelectCoursesModal(selectCoursesLink.ID);

            // add controls to container
            coursesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LearningPathCourses",
                                                                                     String.Empty,
                                                                                     learningPathCoursesInputControls,
                                                                                     true,
                                                                                     true));

            // attach panel to container
            this.LearningPathTabPanelsContainer.Controls.Add(coursesPanel);
        }
        #endregion

        #region _BuildSelectCoursesModal
        /// <summary>
        /// Builds the modal for selecting courses to add to the learning path.
        /// </summary>
        private void _BuildSelectCoursesModal(string targetControlId)
        {
            // set modal properties
            this._SelectCoursesForLearningPath = new ModalPopup("SelectCoursesForLearningPathModal");
            this._SelectCoursesForLearningPath.Type = ModalPopupType.Form;
            this._SelectCoursesForLearningPath.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            this._SelectCoursesForLearningPath.HeaderIconAlt = _GlobalResources.SelectCourse_s;
            this._SelectCoursesForLearningPath.HeaderText = _GlobalResources.SelectCourse_s;
            this._SelectCoursesForLearningPath.TargetControlID = targetControlId;
            this._SelectCoursesForLearningPath.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCoursesForLearningPath.SubmitButtonCustomText = _GlobalResources.AddCourse_s;
            this._SelectCoursesForLearningPath.SubmitButton.OnClientClick = "javascript:AddCoursesToLearningPath(); return false;";
            this._SelectCoursesForLearningPath.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCoursesForLearningPath.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectCoursesForLearningPath.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleLearningPathCourses = new DynamicListBox("SelectEligibleCourses");
            this._SelectEligibleLearningPathCourses.NoRecordsFoundMessage = _GlobalResources.NoCoursesFound;
            this._SelectEligibleLearningPathCourses.SearchButton.Command += new CommandEventHandler(this._SearchSelectCoursesButton_Command);
            this._SelectEligibleLearningPathCourses.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectCoursesButton_Command);
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataSource = this._EligibleLerarningPathCoursesForSelectList;
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataBind();

            // add controls to body
            this._SelectCoursesForLearningPath.AddControlToBody(this._SelectEligibleLearningPathCourses);

            // add modal to container
            this.LearningPathTabPanelsContainer.Controls.Add(this._SelectCoursesForLearningPath);
        }
        #endregion

        #region _SearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Course(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCoursesForLearningPath.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleLearningPathCourses.ListBoxControl.Items.Clear();

            // do the search
            if (this._LearningPathObject != null)
            { this._EligibleLerarningPathCoursesForSelectList = Course.IdsAndNamesForLearningPathSelectList(this._LearningPathObject.Id, this._SelectEligibleLearningPathCourses.SearchTextBox.Text); }
            else
            { this._EligibleLerarningPathCoursesForSelectList = Course.IdsAndNamesForLearningPathSelectList(0, this._SelectEligibleLearningPathCourses.SearchTextBox.Text); }

            this._SelectEligibleLearningPathCourses.ListBoxControl.DataSource = this._EligibleLerarningPathCoursesForSelectList;
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataBind();

            // if no records are available then disable the submit button
            if (this._SelectEligibleLearningPathCourses.ListBoxControl.Items.Count == 0)
            {
                this._SelectCoursesForLearningPath.SubmitButton.Enabled = false;
                this._SelectCoursesForLearningPath.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectCoursesForLearningPath.SubmitButton.Enabled = true;
                this._SelectCoursesForLearningPath.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Courses" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCoursesForLearningPath.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleLearningPathCourses.ListBoxControl.Items.Clear();
            this._SelectEligibleLearningPathCourses.SearchTextBox.Text = "";

            // clear the search
            if (this._LearningPathObject != null)
            { this._EligibleLerarningPathCoursesForSelectList = Course.IdsAndNamesForLearningPathSelectList(this._LearningPathObject.Id, null); }
            else
            { this._EligibleLerarningPathCoursesForSelectList = Course.IdsAndNamesForLearningPathSelectList(0, null); }

            this._SelectEligibleLearningPathCourses.ListBoxControl.DataSource = this._EligibleLerarningPathCoursesForSelectList;
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleLearningPathCourses.ListBoxControl.DataBind();

            // if records available then enable the submit button
            if (this._SelectEligibleLearningPathCourses.ListBoxControl.Items.Count > 0)
            {
                this._SelectCoursesForLearningPath.SubmitButton.Enabled = true;
                this._SelectCoursesForLearningPath.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectCoursesForLearningPath.SubmitButton.Enabled = false;
                this._SelectCoursesForLearningPath.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _BuildLearningPathPropertiesFormLearningPathMaterialsPanel
        /// <summary>
        /// Builds the learning path properties form fields under learning path materials tab.
        /// </summary>
        private void _BuildLearningPathPropertiesFormLearningPathMaterialsPanel()
        {
            UpdatePanel learningPathMaterialsPanel = new UpdatePanel();
            learningPathMaterialsPanel.ID = "LearningPathProperties_" + "LearningPathMaterials" + "_TabPanel";
            learningPathMaterialsPanel.Attributes.Add("class", "TabPanelContainer");
            learningPathMaterialsPanel.Attributes.Add("style", "display: none;");

            // build the grid
            this._LearningPathMaterialGrid = new Grid();
            this._LearningPathMaterialGrid.ID = "LearningPathMaterialGrid";
            this._LearningPathMaterialGrid.AllowPaging = false;
            this._LearningPathMaterialGrid.ShowSearchBox = false;
            this._LearningPathMaterialGrid.ShowRecordsPerPageSelectbox = false;
            this._LearningPathMaterialGrid.PageSize = 1000;

            this._LearningPathMaterialGrid.StoredProcedure = DocumentRepositoryItem.GridProcedure;
            this._LearningPathMaterialGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._LearningPathMaterialGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._LearningPathMaterialGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._LearningPathMaterialGrid.AddFilter("@idDocumentRepositoryObjectType", SqlDbType.Int, 4, DocumentRepositoryObjectType.LearningPath);
            this._LearningPathMaterialGrid.AddFilter("@idObject", SqlDbType.Int, 4, this._LearningPathObject.Id);
            this._LearningPathMaterialGrid.IdentifierField = "idDocumentRepositoryItem";
            this._LearningPathMaterialGrid.DefaultSortColumn = "name";

            // data key names
            this._LearningPathMaterialGrid.DataKeyNames = new string[] { "idDocumentRepositoryItem" };

            // columns
            GridColumn nameFileSize = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.File + " (" + _GlobalResources.Size + ")", null); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this._LearningPathMaterialGrid.AddColumn(nameFileSize);
            this._LearningPathMaterialGrid.AddColumn(options);

            // add row data bound event
            this._LearningPathMaterialGrid.RowDataBound += new GridViewRowEventHandler(this._LearningPathMaterialGrid_RowDataBound);

            // bind the grid
            this._LearningPathMaterialGrid.BindData();

            // attach the grid to the update panel
            learningPathMaterialsPanel.ContentTemplateContainer.Controls.Add(this._LearningPathMaterialGrid);

            // attach panel to container
            this.LearningPathTabPanelsContainer.Controls.Add(learningPathMaterialsPanel);
        }
        #endregion

        #region _LearningPathMaterialGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the learning path material grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LearningPathMaterialGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idDocumentRepositoryItem = Convert.ToInt32(rowView["idDocumentRepositoryItem"]);

                // AVATAR, TITLE, FILE (SIZE) COLUMN

                string title = rowView["label"].ToString();
                string fileName = rowView["fileName"].ToString();
                string kb = rowView["kb"].ToString();
                bool isPrivate = Convert.ToBoolean(rowView["isPrivate"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = title;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // title
                Label titleLabel = new Label();
                titleLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(titleLabel);

                HyperLink titleLink = new HyperLink();
                titleLink.NavigateUrl = "javascript: void(0);";
                titleLink.Attributes.Add("onclick", "LoadLearningPathMaterialModifyModalContent(" + idDocumentRepositoryItem.ToString() + ", true);");
                titleLink.Text = title;
                titleLabel.Controls.Add(titleLink);

                // file name
                Label fileNameLabel = new Label();
                fileNameLabel.CssClass = "GridSecondaryLine";
                e.Row.Cells[1].Controls.Add(fileNameLabel);

                HyperLink learningPathMaterialLink = new HyperLink();
                learningPathMaterialLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LEARNINGPATH + this._LearningPathObject.Id.ToString() + "/" + fileName;
                learningPathMaterialLink.Target = "_blank";
                learningPathMaterialLink.Text = fileName;
                fileNameLabel.Controls.Add(learningPathMaterialLink);

                Literal learningPathMaterialSize = new Literal();
                //learningPathMaterialSize.Text = " (" + Asentia.Common.Utility.GetSizeStringFromKB(kb) + ")";
                learningPathMaterialSize.Text = " (" + kb + ")";
                fileNameLabel.Controls.Add(learningPathMaterialSize);

                // OPTIONS COLUMN 

                // publish/unpublish
                if (isPrivate)
                {
                    Label learningPathMaterialPrivatePublicSpan = new Label();
                    learningPathMaterialPrivatePublicSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(learningPathMaterialPrivatePublicSpan);

                    LinkButton learningPathMaterialMakePublicLink = new LinkButton();
                    learningPathMaterialMakePublicLink.ID = "LearningPathMaterialMakePublicLink_" + idDocumentRepositoryItem.ToString();
                    learningPathMaterialMakePublicLink.OnClientClick = "LoadMakeLearningPathMaterialPublicModalContent(" + idDocumentRepositoryItem + "); return false;";
                    learningPathMaterialMakePublicLink.ToolTip = _GlobalResources.LearningPathMaterialIsPrivateClickToMakeItPublic;

                    Image learningPathMaterialMakePublicLinkImage = new Image();
                    learningPathMaterialMakePublicLinkImage.ID = "LearningPathMaterialMakePublicLinkImage_" + idDocumentRepositoryItem.ToString();
                    learningPathMaterialMakePublicLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
                    learningPathMaterialMakePublicLinkImage.AlternateText = _GlobalResources.Private;

                    learningPathMaterialMakePublicLink.Controls.Add(learningPathMaterialMakePublicLinkImage);

                    learningPathMaterialPrivatePublicSpan.Controls.Add(learningPathMaterialMakePublicLink);
                }
                else
                {
                    Label learningPathMaterialPrivatePublicSpan = new Label();
                    learningPathMaterialPrivatePublicSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(learningPathMaterialPrivatePublicSpan);

                    LinkButton learningPathMaterialMakePrivateLink = new LinkButton();
                    learningPathMaterialMakePrivateLink.ID = "LearningPathMaterialMakePrivateLink_" + idDocumentRepositoryItem.ToString();
                    learningPathMaterialMakePrivateLink.OnClientClick = "LoadMakeLearningPathMaterialPrivateModalContent(" + idDocumentRepositoryItem + "); return false;";
                    learningPathMaterialMakePrivateLink.ToolTip = _GlobalResources.LearningPathMaterialIsPublicClickToMakeItPrivate;

                    Image learningPathMaterialMakePrivateLinkImage = new Image();
                    learningPathMaterialMakePrivateLinkImage.ID = "LearningPathMaterialMakePrivateLinkImage_" + idDocumentRepositoryItem.ToString();
                    learningPathMaterialMakePrivateLinkImage.CssClass = "DimIcon";
                    learningPathMaterialMakePrivateLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
                    learningPathMaterialMakePrivateLinkImage.AlternateText = _GlobalResources.Public;

                    learningPathMaterialMakePrivateLink.Controls.Add(learningPathMaterialMakePrivateLinkImage);

                    learningPathMaterialPrivatePublicSpan.Controls.Add(learningPathMaterialMakePrivateLink);
                }
            }
        }
        #endregion

        #region _BuildLearningPathPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for Learning Path Properties actions.
        /// </summary>
        private void _BuildLearningPathPropertiesActionsPanel()
        {
            // clear controls from container
            this.LearningPathPropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.LearningPathPropertiesActionsPanel.CssClass = "ActionsPanel";

            // LEARNING PATH SAVE BUTTON

            this._LearningPathPropertiesSaveButton = new Button();
            this._LearningPathPropertiesSaveButton.ID = "LearningPathSaveButton";
            this._LearningPathPropertiesSaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._LearningPathObject == null)
            { this._LearningPathPropertiesSaveButton.Text = _GlobalResources.CreateLearningPath; }
            else
            { this._LearningPathPropertiesSaveButton.Text = _GlobalResources.SaveChanges; }

            this._LearningPathPropertiesSaveButton.Command += new CommandEventHandler(this._LearningPathPropertiesSaveButton_Command);
            this._LearningPathPropertiesSaveButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicLearningPathElements();");

            this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathPropertiesSaveButton);

            // set the default button for the learning path properties container to the id of the properties save button
            this.LearningPathPropertiesContainer.DefaultButton = this._LearningPathPropertiesSaveButton.ID;

            // CANCEL BUTTON

            this._LearningPathPropertiesCancelButton = new Button();
            this._LearningPathPropertiesCancelButton.ID = "CancelButton";
            this._LearningPathPropertiesCancelButton.CssClass = "Button NonActionButton";
            this._LearningPathPropertiesCancelButton.Text = _GlobalResources.Cancel;
            this._LearningPathPropertiesCancelButton.Command += new CommandEventHandler(this._LearningPathPropertiesCancelButton_Command);

            this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathPropertiesCancelButton);

            if (this._LearningPathObject != null)
            {
                // LEARNING PATH MATERIALS GRID DELETE BUTTON

                this._LearningPathMaterialGridDeleteButton = new LinkButton();
                this._LearningPathMaterialGridDeleteButton.ID = "LearningPathMaterialGridDeleteButton";
                this._LearningPathMaterialGridDeleteButton.CssClass = "GridDeleteButton";
                this._LearningPathMaterialGridDeleteButton.Style.Add("display", "none");

                // delete button image
                Image learningPathMaterialGridDeleteImage = new Image();
                learningPathMaterialGridDeleteImage.ID = "LearningPathMaterialGridDeleteButtonImage";
                learningPathMaterialGridDeleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                learningPathMaterialGridDeleteImage.CssClass = "MediumIcon";
                learningPathMaterialGridDeleteImage.AlternateText = _GlobalResources.Delete;
                this._LearningPathMaterialGridDeleteButton.Controls.Add(learningPathMaterialGridDeleteImage);

                // delete button text
                Literal learningPathMaterialGridDeleteText = new Literal();
                learningPathMaterialGridDeleteText.Text = _GlobalResources.DeleteSelectedLearningPathMaterial_s;
                this._LearningPathMaterialGridDeleteButton.Controls.Add(learningPathMaterialGridDeleteText);

                this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathMaterialGridDeleteButton);
            }

            // PAGE ACTION HIDDEN FIELD

            this._PageAction = new HiddenField();
            this._PageAction.ID = "PageAction";
            this._PageAction.Value = this.QueryStringString("action", String.Empty);
            this.LearningPathPropertiesActionsPanel.Controls.Add(this._PageAction);
        }
        #endregion

        #region _PopulateLearningPathPropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateLearningPathPropertiesInputElements()
        {
            if (this._LearningPathObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // name, short description, long description
                bool isDefaultPopulated = false;

                foreach (LearningPath.LanguageSpecificProperty learningPathLanguageSpecificProperty in this._LearningPathObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (learningPathLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._LearningPathName.Text = learningPathLanguageSpecificProperty.Name;
                        this._LearningPathShortDescription.Text = learningPathLanguageSpecificProperty.ShortDescription;
                        this._LearningPathLongDescription.Text = learningPathLanguageSpecificProperty.LongDescription;
                        this._LearningPathSearchTags.Text = learningPathLanguageSpecificProperty.SearchTags;

                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificPathNameTextBox = (TextBox)this.LearningPathPropertiesContainer.FindControl(this._LearningPathName.ID + "_" + learningPathLanguageSpecificProperty.LangString);
                        TextBox languageSpecificPathShortDescriptionTextBox = (TextBox)this.LearningPathPropertiesContainer.FindControl(this._LearningPathShortDescription.ID + "_" + learningPathLanguageSpecificProperty.LangString);
                        TextBox languageSpecificPathLongDescriptionTextBox = (TextBox)this.LearningPathPropertiesContainer.FindControl(this._LearningPathLongDescription.ID + "_" + learningPathLanguageSpecificProperty.LangString);
                        TextBox languageSpecificPathSearchTagsTextBox = (TextBox)this.LearningPathPropertiesContainer.FindControl(this._LearningPathSearchTags.ID + "_" + learningPathLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificPathNameTextBox != null)
                        { languageSpecificPathNameTextBox.Text = learningPathLanguageSpecificProperty.Name; }

                        if (languageSpecificPathShortDescriptionTextBox != null)
                        { languageSpecificPathShortDescriptionTextBox.Text = learningPathLanguageSpecificProperty.ShortDescription; }

                        if (languageSpecificPathLongDescriptionTextBox != null)
                        { languageSpecificPathLongDescriptionTextBox.Text = learningPathLanguageSpecificProperty.LongDescription; }

                        if (languageSpecificPathSearchTagsTextBox != null)
                        { languageSpecificPathSearchTagsTextBox.Text = learningPathLanguageSpecificProperty.SearchTags; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._LearningPathName.Text = this._LearningPathObject.Name;
                    this._LearningPathShortDescription.Text = this._LearningPathObject.ShortDescription;
                    this._LearningPathLongDescription.Text = this._LearningPathObject.LongDescription;
                    this._LearningPathSearchTags.Text = this._LearningPathObject.SearchTags;
                }

                // NON-LANGUAGE SPECIFIC PROPERTIES

                // catalog link shortcode
                this._Shortcode.Text = this._LearningPathObject.Shortcode;

                // cost
                if (this._EcommerceSettings.IsEcommerceSet)
                { this._LearningPathCost.Text = Convert.ToString(this._LearningPathObject.Cost); }

                // avatar image
                if (this._LearningPathObject.Avatar != null)
                {
                    this._AvatarImage.ImageUrl = SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + this._LearningPathObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                }                                

                // is published
                if (this._LearningPathObject.IsPublished == true)
                { this._LearningPathPublished.SelectedValue = "True"; }
                else
                { this._LearningPathPublished.SelectedValue = "False"; }

                // is closed
                if (this._LearningPathObject.IsClosed == true)
                { this._LearningPathClosed.SelectedValue = "True"; }
                else
                { this._LearningPathClosed.SelectedValue = "False"; }

                // self-enrollment is one-time only
                if (this._LearningPathObject.SelfEnrollmentIsOneTimeOnly == true)
                { this._LearningPathSelfEnrollmentIsOneTimeOnly.Checked = true; }
                else
                { this._LearningPathSelfEnrollmentIsOneTimeOnly.Checked = false; }

                // force course completion in order
                if (this._LearningPathObject.ForceCompletionInOrder == true)
                { this._LearningPathForceCompletionInOrder.Checked = true; }
                else
                { this._LearningPathForceCompletionInOrder.Checked = false; }                

                /* LEARNING PATH COURSES */

                // loop through the datatable and add each course to the listing container
                foreach (DataRow row in this._LearningPathCourses.Rows)
                {
                    // container
                    Panel courseNameContainer = new Panel();
                    courseNameContainer.ID = "Course_" + row["idCourse"].ToString();

                    // remove course button
                    Image removeCourseImage = new Image();
                    removeCourseImage.ID = "Course_" + row["idCourse"].ToString() + "_RemoveImage";
                    removeCourseImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    removeCourseImage.CssClass = "SmallIcon";
                    removeCourseImage.Attributes.Add("onclick", "javascript:RemoveCoursesFromLearningPath('" + row["idCourse"].ToString() + "');");
                    removeCourseImage.Style.Add("cursor", "pointer");

                    // course name
                    Literal courseName = new Literal();
                    courseName.Text = row["title"].ToString();

                    // add controls to container
                    courseNameContainer.Controls.Add(removeCourseImage);
                    courseNameContainer.Controls.Add(courseName);
                    this._LearningPathCoursesListContainer.Controls.Add(courseNameContainer);
                }
            }
        }
        #endregion

        #region _ValidatePropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidatePropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;
            bool coursesTabHasErrors = false;

            // TITLE - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._LearningPathName.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.LearningPathPropertiesContainer, "LearningPathName", _GlobalResources.Name + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // SHORT DESCRIPTION - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._LearningPathShortDescription.Text))
            {
                isValid = false;
                propertiesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.LearningPathPropertiesContainer, "LearningPathShortDescription", _GlobalResources.ShortDescription + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // COST - MUST BE DOUBLE
            if (this._EcommerceSettings.IsEcommerceSet)
            {
                double cost;

                if ((!String.IsNullOrWhiteSpace(this._LearningPathCost.Text) && !double.TryParse(this._LearningPathCost.Text, out cost)))
                {
                    isValid = false;
                    propertiesTabHasErrors = true;
                    this.ApplyErrorMessageToFieldErrorPanel(this.LearningPathPropertiesContainer, "LearningPathCost", _GlobalResources.Cost + " " + _GlobalResources.IsInvalid);
                }
            }

            // MAKE SURE THAT AT LEAST ONE COURSE HAS BEEN SELECTED
            if (String.IsNullOrWhiteSpace(this._SelectedLearningPathCourses.Value))
            {
                isValid = false;
                coursesTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this.LearningPathPropertiesContainer, "LearningPathCourses", _GlobalResources.AtLeastOneCourseMustBeSelected);
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.LearningPathPropertiesContainer, "LearningPathProperties_Properties_TabLI"); }

            if (coursesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.LearningPathPropertiesContainer, "LearningPathProperties_Courses_TabLI"); }

            // if this is invalid, we need to pre-populate multi-select fields with the objects that were selected
            if (!isValid)
            {
                /* LEARNING PATH COURSES */

                if (!String.IsNullOrWhiteSpace(this._SelectedLearningPathCourses.Value))
                {
                    // split the "value" of the hidden field to get an array of course expert ids
                    string[] selectedCourses = this._SelectedLearningPathCourses.Value.Split(',');

                    // loop through the array and add each course to the listing container
                    foreach (string courseId in selectedCourses)
                    {
                        if (this._LearningPathCoursesListContainer.FindControl("Course_" + courseId) == null)
                        {
                            // user object
                            Library.Course courseobject = new Library.Course(Convert.ToInt32(courseId));

                            // container
                            Panel courseNameContainer = new Panel();
                            courseNameContainer.ID = "Course_" + courseobject.Id.ToString();

                            // remove course button
                            Image removeCourseImage = new Image();
                            removeCourseImage.ID = "Course_" + courseobject.Id.ToString() + "_RemoveImage";
                            removeCourseImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            removeCourseImage.CssClass = "SmallIcon";
                            removeCourseImage.Attributes.Add("onclick", "javascript:RemoveCoursesFromLearningPath('" + courseobject.Id.ToString() + "');");
                            removeCourseImage.Style.Add("cursor", "pointer");

                            // course name
                            Literal courseName = new Literal();
                            courseName.Text = courseobject.Title.ToString();

                            // add controls to container
                            courseNameContainer.Controls.Add(removeCourseImage);
                            courseNameContainer.Controls.Add(courseName);
                            this._LearningPathCoursesListContainer.Controls.Add(courseNameContainer);
                        }
                    }
                }
            }

            return isValid;
        }
        #endregion

        #region _LearningPathPropertiesSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for Learning Path Properties.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _LearningPathPropertiesSaveButton_Command(object sender, EventArgs e)
        {
            try
            {
                if (!this._ValidatePropertiesForm())
                { throw new AsentiaException(); }

                // if there is no learning path object, create one
                if (this._LearningPathObject == null)
                { this._LearningPathObject = new LearningPath(); }

                int id;
                
                // populate the object
                this._LearningPathObject.Name = this._LearningPathName.Text;

                if (!String.IsNullOrWhiteSpace(this._Shortcode.Text))
                { this._LearningPathObject.Shortcode = this._Shortcode.Text; }
                else
                { this._LearningPathObject.Shortcode = null; }

                if (this._EcommerceSettings.IsEcommerceSet)
                {
                    if (!String.IsNullOrWhiteSpace(this._LearningPathCost.Text))
                    { this._LearningPathObject.Cost = Convert.ToDouble(this._LearningPathCost.Text); }
                    else
                    { this._LearningPathObject.Cost = null; }
                }
                else
                { this._LearningPathObject.Cost = null; }

                this._LearningPathObject.IsPublished = Convert.ToBoolean(this._LearningPathPublished.SelectedValue);
                this._LearningPathObject.IsClosed = Convert.ToBoolean(this._LearningPathClosed.SelectedValue);
                
                this._LearningPathObject.ShortDescription = this._LearningPathShortDescription.Text;

                if (!String.IsNullOrWhiteSpace(this._LearningPathLongDescription.Text))
                { this._LearningPathObject.LongDescription = HttpUtility.HtmlDecode(this._LearningPathLongDescription.Text); }
                else
                { this._LearningPathObject.LongDescription = null; }

                if (!String.IsNullOrWhiteSpace(this._LearningPathSearchTags.Text))
                { this._LearningPathObject.SearchTags = this._LearningPathSearchTags.Text; }
                else
                { this._LearningPathObject.SearchTags = null; }

                // course order settings
                this._LearningPathObject.ForceCompletionInOrder = this._LearningPathForceCompletionInOrder.Checked;

                // do avatar if existing learning path, if its a new learning path, we'll do it after initial save
                bool isNewLearningPath = true;

                if (this._LearningPathObject.Id > 0)
                {
                    isNewLearningPath = false;

                    if (this._LearningPathAvatar.SavedFilePath != null)
                    {
                        // check user folder existence and create if necessary
                        if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id)))
                        { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id)); }

                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._LearningPathAvatar.SavedFilePath)))
                        {
                            fullSavedFilePath = SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar" + Path.GetExtension(this._LearningPathAvatar.SavedFilePath);
                            fullSavedFilePathSmall = SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar.small" + Path.GetExtension(this._LearningPathAvatar.SavedFilePath);

                            // delete existing avatar images if any
                            if (this._LearningPathObject.Avatar != null)
                            {
                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar" + Path.GetExtension(this._LearningPathObject.Avatar))))
                                { File.Delete(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar" + Path.GetExtension(this._LearningPathObject.Avatar))); }

                                if (File.Exists(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar.small" + Path.GetExtension(this._LearningPathObject.Avatar))))
                                { File.Delete(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar.small" + Path.GetExtension(this._LearningPathObject.Avatar))); }
                            }

                            // move the uploaded file into the learning path's folder
                            File.Copy(Server.MapPath(this._LearningPathAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._LearningPathAvatar.SavedFilePath));
                            this._LearningPathObject.Avatar = "avatar" + Path.GetExtension(this._LearningPathAvatar.SavedFilePath);

                            // create a smaller version of the avatar fo use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            this._LearningPathObject.SaveAvatar();
                        }
                        else
                        {
                            this._LearningPathObject.Avatar = null;
                        }
                    }
                    else if (this._ClearAvatar != null)
                    {
                        if (this._ClearAvatar.Value == "true")
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar" + Path.GetExtension(this._LearningPathObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar" + Path.GetExtension(this._LearningPathObject.Avatar))); }

                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar.small" + Path.GetExtension(this._LearningPathObject.Avatar))))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar.small" + Path.GetExtension(this._LearningPathObject.Avatar))); }

                            this._LearningPathObject.Avatar = null;

                            this._LearningPathObject.SaveAvatar();
                        }
                    }
                    else
                    { }
                }

                // self-enrollment is one-time only
                this._LearningPathObject.SelfEnrollmentIsOneTimeOnly = this._LearningPathSelfEnrollmentIsOneTimeOnly.Checked;

                // save the learning path, save its returned id to viewstate, and set the current learning path object's id
                id = this._LearningPathObject.Save();
                this.ViewState["id"] = id;
                this._LearningPathObject.Id = id;

                // if this was a new learning path we just saved, we now have the id so that
                // we can create the learning path's folder and update the avatar if necessary.
                if (isNewLearningPath)
                {
                    // check learning path folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id)))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id)); }

                    // if there is an avatar, move it and update the database record
                    if (this._LearningPathAvatar.SavedFilePath != null)
                    {
                        string fullSavedFilePath = null;
                        string fullSavedFilePathSmall = null;

                        if (File.Exists(Server.MapPath(this._LearningPathAvatar.SavedFilePath)))
                        {
                            fullSavedFilePath = SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar" + Path.GetExtension(this._LearningPathAvatar.SavedFilePath);
                            fullSavedFilePathSmall = SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + "avatar.small" + Path.GetExtension(this._LearningPathAvatar.SavedFilePath);

                            // move the uploaded file into the user's folder
                            File.Copy(Server.MapPath(this._LearningPathAvatar.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                            File.Delete(Server.MapPath(this._LearningPathAvatar.SavedFilePath));
                            this._LearningPathObject.Avatar = "avatar" + Path.GetExtension(this._LearningPathAvatar.SavedFilePath);

                            // create a smaller version of the avatar fo use in wall feeds
                            ImageResizer resizer = new ImageResizer();
                            resizer.MaxX = 40;
                            resizer.MaxY = 40;
                            resizer.TrimImage = true;
                            resizer.Resize(MapPathSecure(fullSavedFilePath), MapPathSecure(fullSavedFilePathSmall));

                            // save the avatar path to the database
                            this._LearningPathObject.SaveAvatar();
                        }
                    }
                }

                // do learning path language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string learningPathName = null;
                        string learningPathShortDescription = null;
                        string learningPathLongDescription = null;
                        string learningPathSearchTags = null;

                        // get text boxes
                        TextBox languageSpecificLearningPathNameTextBox = (TextBox)this.LearningPathPropertiesContainer.FindControl(this._LearningPathName.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificLearningPathShortDescriptionTextBox = (TextBox)this.LearningPathPropertiesContainer.FindControl(this._LearningPathShortDescription.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificLearningPathLongDescriptionTextBox = (TextBox)this.LearningPathPropertiesContainer.FindControl(this._LearningPathLongDescription.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificLearningPathSearchTagsTextBox = (TextBox)this.LearningPathPropertiesContainer.FindControl(this._LearningPathSearchTags.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificLearningPathNameTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLearningPathNameTextBox.Text))
                            { learningPathName = languageSpecificLearningPathNameTextBox.Text; }
                        }

                        if (languageSpecificLearningPathShortDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLearningPathShortDescriptionTextBox.Text))
                            { learningPathShortDescription = languageSpecificLearningPathShortDescriptionTextBox.Text; }
                        }

                        if (languageSpecificLearningPathLongDescriptionTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLearningPathLongDescriptionTextBox.Text))
                            { learningPathLongDescription = languageSpecificLearningPathLongDescriptionTextBox.Text; }
                        }

                        if (languageSpecificLearningPathSearchTagsTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLearningPathSearchTagsTextBox.Text))
                            { learningPathSearchTags = languageSpecificLearningPathSearchTagsTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(learningPathName) ||
                            !String.IsNullOrWhiteSpace(learningPathShortDescription) ||
                            !String.IsNullOrWhiteSpace(learningPathLongDescription) ||
                            !String.IsNullOrWhiteSpace(learningPathSearchTags))
                        {
                            this._LearningPathObject.SaveLang(cultureInfo.Name,
                                                              learningPathName,
                                                              learningPathShortDescription,
                                                              HttpUtility.HtmlDecode(learningPathLongDescription),
                                                              learningPathSearchTags);
                        }
                    }
                }

                // do course to learning path links

                // delcare data table
                DataTable coursesToBeAssigned = new DataTable();
                coursesToBeAssigned.Columns.Add("id", typeof(int));
                coursesToBeAssigned.Columns.Add("order", typeof(int));

                int order = 0;                

                if (!String.IsNullOrWhiteSpace(this._SelectedLearningPathCourses.Value))
                {
                    // split the "value" of the hidden field to get an array of course ids
                    string[] selectedLearningPathCourseIds = this._SelectedLearningPathCourses.Value.Split(',');

                    // put ids into datatable 
                    foreach (string courseId in selectedLearningPathCourseIds)
                    {
                        order++;
                        coursesToBeAssigned.Rows.Add(Convert.ToInt32(courseId), order);
                    }
                }

                // save the learning path courses
                this._LearningPathObject.SaveCourses(coursesToBeAssigned);

                // load the saved learning path object
                this._LearningPathObject = new LearningPath(id);

                // clear controls for containers that have dynamically added elements
                this.LearningPathObjectMenuContainer.Controls.Clear();

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, _GlobalResources.LearningPathHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _LearningPathPropertiesCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for Learning Path Properties.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _LearningPathPropertiesCancelButton_Command(object sender, EventArgs e)
        {
            Response.Redirect("~/administrator/learningpaths");
        }
        #endregion

        #region _BuildLearningPathMaterialGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on the Learning Path Material Grid data.
        /// </summary>
        private void _BuildLearningPathMaterialGridActionsModal()
        {
            this._LearningPathMaterialGridConfirmAction = new ModalPopup("LearningPathMaterialGridConfirmAction");

            // set modal properties
            this._LearningPathMaterialGridConfirmAction.Type = ModalPopupType.Confirm;
            this._LearningPathMaterialGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._LearningPathMaterialGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._LearningPathMaterialGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedLearningPathMaterial_s;
            this._LearningPathMaterialGridConfirmAction.TargetControlID = this._LearningPathMaterialGridDeleteButton.ClientID;
            this._LearningPathMaterialGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._LearningPathMaterialGridDeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "LearningPathMaterialGridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseLearningPathMaterial_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._LearningPathMaterialGridConfirmAction.AddControlToBody(body1Wrapper);

            this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathMaterialGridConfirmAction);
        }
        #endregion

        #region _LearningPathMaterialGridDeleteButton_Command
        /// <summary>
        /// Performs the delete action on Learning Path Material Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _LearningPathMaterialGridDeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._LearningPathMaterialGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._LearningPathMaterialGrid.Rows[i].FindControl(this._LearningPathMaterialGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    DocumentRepositoryItem.Delete(recordsToDelete);

                    // display the success message                    
                    this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, _GlobalResources.TheSelectedLearningPathMaterial_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message                    
                    this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, _GlobalResources.NoLearningPathMaterial_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.LearningPathPropertiesFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // clear controls for containers that have dynamically added elements
                this.LearningPathObjectMenuContainer.Controls.Clear();                

                // build the page controls
                this._BuildControls();
            }
        }
        #endregion

        #region _BuildLearningPathMaterialMakePrivateConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the make private action performed on a learning path material.
        /// </summary>
        private void _BuildLearningPathMaterialMakePrivateConfirmationModal()
        {
            // set modal properties
            this._LearningPathMaterialMakePrivateConfirmationModal = new ModalPopup("LearningPathMaterialMakePrivateConfirmationModal");
            this._LearningPathMaterialMakePrivateConfirmationModal.Type = ModalPopupType.Form;
            this._LearningPathMaterialMakePrivateConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
            this._LearningPathMaterialMakePrivateConfirmationModal.HeaderIconAlt = _GlobalResources.MakeLearningPathMaterialPrivate;
            this._LearningPathMaterialMakePrivateConfirmationModal.HeaderText = _GlobalResources.MakeLearningPathMaterialPrivate;
            this._LearningPathMaterialMakePrivateConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._LearningPathMaterialMakePrivateConfirmationModal.TargetControlID = this._LearningPathMaterialMakePrivateConfirmationModalLaunchButton.ID;
            this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._LearningPathMaterialMakePrivateSubmit_Command);

            // build the modal form panel
            Panel learningPathMaterialMakePrivateConfirmationModalFormPanel = new Panel();
            learningPathMaterialMakePrivateConfirmationModalFormPanel.ID = "LearningPathMaterialMakePrivateConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmLearningPathMaterialMakePrivateActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToMakeThisLearningPathMaterialPrivate;

            body1Wrapper.Controls.Add(body1);
            learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the data hidden field
            HiddenField learningPathMaterialMakePrivateData = new HiddenField();
            learningPathMaterialMakePrivateData.ID = "LearningPathMaterialMakePrivateData";

            // build the modal body
            this._LearningPathMaterialMakePrivateConfirmationModal.AddControlToBody(learningPathMaterialMakePrivateConfirmationModalFormPanel);
            this._LearningPathMaterialMakePrivateConfirmationModal.AddControlToBody(this._LearningPathMaterialMakePrivateConfirmationModalLoadButton);
            this._LearningPathMaterialMakePrivateConfirmationModal.AddControlToBody(learningPathMaterialMakePrivateData);

            // add modal to container
            this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathMaterialMakePrivateConfirmationModal);
        }
        #endregion

        #region _LoadLearningPathMaterialMakePrivateConfirmationModalContent
        /// <summary>
        /// Loads content for learning path material make private confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadLearningPathMaterialMakePrivateConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel learningPathMaterialMakePrivateConfirmationModalFormPanel = (Panel)this._LearningPathMaterialMakePrivateConfirmationModal.FindControl("LearningPathMaterialMakePrivateConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._LearningPathMaterialMakePrivateConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = true;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(dEx.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(ex.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _LearningPathMaterialMakePrivateSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the learning path material make private confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _LearningPathMaterialMakePrivateSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            UpdatePanel learningPathMaterialGridUpdatePanel = (UpdatePanel)this.LearningPathTabPanelsContainer.FindControl("LearningPathProperties_LearningPathMaterials_TabPanel");
            HiddenField learningPathMaterialMakePrivateData = (HiddenField)this._LearningPathMaterialMakePrivateConfirmationModal.FindControl("LearningPathMaterialMakePrivateData");
            Panel learningPathMaterialMakePrivateConfirmationModalFormPanel = (Panel)this._LearningPathMaterialMakePrivateConfirmationModal.FindControl("LearningPathMaterialMakePrivateConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._LearningPathMaterialMakePrivateConfirmationModal.ClearFeedback();

                // get the document repository item id
                int idDocumentRepositoryItem = Convert.ToInt32(learningPathMaterialMakePrivateData.Value);

                // make the course material private
                DocumentRepositoryItem documentRepositoryItemObject = new DocumentRepositoryItem(idDocumentRepositoryItem);
                documentRepositoryItemObject.IsPrivate = true;
                documentRepositoryItemObject.Save();

                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(_GlobalResources.TheLearningPathMaterialHasBeenMarkedAsPrivate, false);

                // rebind the learning path material grid and update
                this._LearningPathMaterialGrid.BindData();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(dEx.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LearningPathMaterialMakePrivateConfirmationModal.DisplayFeedback(ex.Message, true);
                learningPathMaterialMakePrivateConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePrivateConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePrivateConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildLearningPathMaterialMakePublicConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the make public action performed on a learning path material.
        /// </summary>
        private void _BuildLearningPathMaterialMakePublicConfirmationModal()
        {
            // set modal properties
            this._LearningPathMaterialMakePublicConfirmationModal = new ModalPopup("LearningPathMaterialMakePublicConfirmationModal");
            this._LearningPathMaterialMakePublicConfirmationModal.Type = ModalPopupType.Form;
            this._LearningPathMaterialMakePublicConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
            this._LearningPathMaterialMakePublicConfirmationModal.HeaderIconAlt = _GlobalResources.MakeLearningPathMaterialPublic;
            this._LearningPathMaterialMakePublicConfirmationModal.HeaderText = _GlobalResources.MakeLearningPathMaterialPublic;
            this._LearningPathMaterialMakePublicConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._LearningPathMaterialMakePublicConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._LearningPathMaterialMakePublicConfirmationModal.TargetControlID = this._LearningPathMaterialMakePublicConfirmationModalLaunchButton.ID;
            this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._LearningPathMaterialMakePublicSubmit_Command);

            // build the modal form panel
            Panel learningPathMaterialMakePublicConfirmationModalFormPanel = new Panel();
            learningPathMaterialMakePublicConfirmationModalFormPanel.ID = "LearningPathMaterialMakePublicConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmLearningPathMaterialMakePublicActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToMakeThisLearningPathMaterialPublic;

            body1Wrapper.Controls.Add(body1);
            learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the data hidden field
            HiddenField learningPathMaterialMakePublicData = new HiddenField();
            learningPathMaterialMakePublicData.ID = "LearningPathMaterialMakePublicData";

            // build the modal body
            this._LearningPathMaterialMakePublicConfirmationModal.AddControlToBody(learningPathMaterialMakePublicConfirmationModalFormPanel);
            this._LearningPathMaterialMakePublicConfirmationModal.AddControlToBody(this._LearningPathMaterialMakePublicConfirmationModalLoadButton);
            this._LearningPathMaterialMakePublicConfirmationModal.AddControlToBody(learningPathMaterialMakePublicData);

            // add modal to container
            this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathMaterialMakePublicConfirmationModal);
        }
        #endregion

        #region _LoadLearningPathMaterialMakePublicConfirmationModalContent
        /// <summary>
        /// Loads content for learning path material make public confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadLearningPathMaterialMakePublicConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel learningPathMaterialMakePublicConfirmationModalFormPanel = (Panel)this._LearningPathMaterialMakePublicConfirmationModal.FindControl("LearningPathMaterialMakePublicConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._LearningPathMaterialMakePublicConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = true;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(dEx.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(ex.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _LearningPathMaterialMakePublicSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the learning path material make public confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _LearningPathMaterialMakePublicSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            UpdatePanel learningPathMaterialGridUpdatePanel = (UpdatePanel)this.LearningPathTabPanelsContainer.FindControl("LearningPathProperties_CourseMaterials_TabPanel");
            HiddenField learningPathMaterialMakePublicData = (HiddenField)this._LearningPathMaterialMakePublicConfirmationModal.FindControl("LearningPathMaterialMakePublicData");
            Panel learningPathMaterialMakePublicConfirmationModalFormPanel = (Panel)this._LearningPathMaterialMakePublicConfirmationModal.FindControl("LearningPathMaterialMakePublicConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._LearningPathMaterialMakePublicConfirmationModal.ClearFeedback();

                // get the document repository item id
                int idDocumentRepositoryItem = Convert.ToInt32(learningPathMaterialMakePublicData.Value);

                // make the course material private
                DocumentRepositoryItem documentRepositoryItemObject = new DocumentRepositoryItem(idDocumentRepositoryItem);
                documentRepositoryItemObject.IsPrivate = false;
                documentRepositoryItemObject.Save();

                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(_GlobalResources.TheLearningPathMaterialHasBeenMarkedAsPublic, false);

                // rebind the learning path material grid and update
                this._LearningPathMaterialGrid.BindData();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(dEx.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LearningPathMaterialMakePublicConfirmationModal.DisplayFeedback(ex.Message, true);
                learningPathMaterialMakePublicConfirmationModalFormPanel.Controls.Clear();
                this._LearningPathMaterialMakePublicConfirmationModal.SubmitButton.Visible = false;
                this._LearningPathMaterialMakePublicConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region Learning Path Material Modify
        #region _BuildLearningPathMaterialModifyModal
        /// <summary>
        /// Builds the learning path material modify modal
        /// </summary>
        private void _BuildLearningPathMaterialModifyModal()
        {
            // set modal properties
            this._LearningPathMaterialModifyModal = new ModalPopup("LearningPathMaterialModifyModal");
            this._LearningPathMaterialModifyModal.Type = ModalPopupType.Form;
            this._LearningPathMaterialModifyModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE_MATERIALS, ImageFiles.EXT_PNG);
            this._LearningPathMaterialModifyModal.HeaderIconAlt = _GlobalResources.LearningPathMaterials;
            this._LearningPathMaterialModifyModal.HeaderText = _GlobalResources.NewLearningPathMaterial;
            this._LearningPathMaterialModifyModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._LearningPathMaterialModifyModal.SubmitButtonCustomText = _GlobalResources.CreateLearningPathMaterial;
            this._LearningPathMaterialModifyModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._LearningPathMaterialModifyModal.TargetControlID = this._LearningPathMaterialModifyModalLaunchButton.ID;
            this._LearningPathMaterialModifyModal.SubmitButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicLearningPathMaterialElements();");
            this._LearningPathMaterialModifyModal.SubmitButton.Command += new CommandEventHandler(this._LearningPathMaterialModifySubmit_Command);

            // build and format a page information panel with instructions
            Panel learningPathMaterialPropertiesInstructionsPanel = new Panel();
            learningPathMaterialPropertiesInstructionsPanel.ID = "LearningPathMaterialModifyModal_LearningPathMaterialPropertiesInstructionsPanel";
            this.FormatPageInformationPanel(learningPathMaterialPropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateThePropertiesOfTheLearningPathMaterial, true);
            this._LearningPathMaterialModifyModal.AddControlToBody(learningPathMaterialPropertiesInstructionsPanel);

            // build the form properties container
            Panel learningPathMaterialPropertiesContainer = new Panel();
            learningPathMaterialPropertiesContainer.ID = "LearningPathMaterialModifyModal_LearningPathMaterialPropertiesContainer";
            learningPathMaterialPropertiesContainer.CssClass = "FormContentContainer";
            this._LearningPathMaterialModifyModal.AddControlToBody(learningPathMaterialPropertiesContainer);

            // build the learning path material properties form content
            learningPathMaterialPropertiesContainer.Controls.Add(this._BuildLearningPathMaterialFormProperties());

            // build the learning path material data hidden field - this will hold the id of the learning path material being edited
            this._LearningPathMaterialModifyData = new HiddenField();
            this._LearningPathMaterialModifyData.ID = "LearningPathMaterialModifyData";

            // add the load button and hidden input to the modal                        
            this._LearningPathMaterialModifyModal.AddControlToBody(this._LearningPathMaterialModifyModalLoadButton);
            this._LearningPathMaterialModifyModal.AddControlToBody(this._LearningPathMaterialModifyData);

            // add modal to container
            this.LearningPathPropertiesActionsPanel.Controls.Add(this._LearningPathMaterialModifyModal);
        }
        #endregion

        #region _BuildLearningPathMaterialFormProperties
        /// <summary>
        /// Method to build learning path material form properties controls
        /// </summary>
        private Panel _BuildLearningPathMaterialFormProperties()
        {
            // build the modal form panel
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "LearningPathMaterialModifyModal_LearningPathMaterialProperties";

            // learning path material label field
            this._LearningPathMaterialLabel = new TextBox();
            this._LearningPathMaterialLabel.ID = "LearningPathMaterialModifyModal_LearningPathMaterialLabel_Field";
            this._LearningPathMaterialLabel.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathMaterialModifyModal_LearningPathMaterialLabel",
                                                             _GlobalResources.Name,
                                                             this._LearningPathMaterialLabel.ID,
                                                             this._LearningPathMaterialLabel,
                                                             true,
                                                             true,
                                                             true));

            // learning path material file field
            List<Control> fileControls = new List<Control>();

            Panel learningPathMaterialFileNameFieldContainer = new Panel();
            learningPathMaterialFileNameFieldContainer.ID = "LearningPathMaterialModifyModal_FileNameFieldContainer";

            Label learningPathMaterialFileNameField = new Label();
            learningPathMaterialFileNameField.ID = "LearningPathMaterialModifyModal_FileNameField";
            learningPathMaterialFileNameFieldContainer.Controls.Add(learningPathMaterialFileNameField);

            fileControls.Add(learningPathMaterialFileNameFieldContainer);

            // uploader
            this._LearningPathMaterialUploader = new UploaderAsync("LearningPathMaterialModifyModal_LearningPathMaterialFile_Field", UploadType.LearningPathMaterial, "LearningPathMaterialModifyModal_LearningPathMaterialFile_ErrorContainer");
            fileControls.Add(this._LearningPathMaterialUploader);

            propertiesPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("LearningPathMaterialModifyModal_LearningPathMaterialFile",
                                                                                 _GlobalResources.File,
                                                                                 fileControls,
                                                                                 false,
                                                                                 true));
            // learning path material file size field
            Label learningPathMaterialFileSizeFieldStaticValue = new Label();
            learningPathMaterialFileSizeFieldStaticValue.ID = "LearningPathMaterialModifyModal_FileSizeField";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathMaterialModifyModal_LearningPathMaterialFileSize",
                                                             _GlobalResources.Size,
                                                             learningPathMaterialFileSizeFieldStaticValue.ID,
                                                             learningPathMaterialFileSizeFieldStaticValue,
                                                             false,
                                                             false,
                                                             false));

            // learning path material is private field 
            this._LearningPathMaterialIsPrivate = new CheckBox();
            this._LearningPathMaterialIsPrivate.ID = "LearningPathMaterialModifyModal_LearningPathMaterialIsPrivate_Field";
            this._LearningPathMaterialIsPrivate.Text = _GlobalResources.ThisLearningPathMaterialIsOnlyAvailableToUsersWhoAreEnrolledInTheLearningPath;
            this._LearningPathMaterialIsPrivate.Checked = false;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathMaterialModifyModal_LearningPathMaterialIsPrivate",
                                                             _GlobalResources.Private,
                                                             this._LearningPathMaterialIsPrivate.ID,
                                                             this._LearningPathMaterialIsPrivate,
                                                             false,
                                                             false,
                                                             false));

            // learning path material is all languages field            
            this._LearningPathMaterialIsAllLanguages = new CheckBox();
            this._LearningPathMaterialIsAllLanguages.ID = "LearningPathMaterialModifyModal_LearningPathMaterialIsAllLanguages_Field";
            this._LearningPathMaterialIsAllLanguages.Text = _GlobalResources.ThisLearningPathMaterialIsForAllLanguagesOrIsNotLanguageSpecific;
            this._LearningPathMaterialIsAllLanguages.Checked = false;
            this._LearningPathMaterialIsAllLanguages.Attributes.Add("onclick", "LearningPathMaterialLanguageSelectionClick(this)");

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathMaterialModifyModal_LearningPathMaterialIsAllLanguages",
                                                             _GlobalResources.AllLanguages,
                                                             this._LearningPathMaterialIsAllLanguages.ID,
                                                             this._LearningPathMaterialIsAllLanguages,
                                                             false,
                                                             false,
                                                             false));

            // learning path material language field
            this._LearningPathMaterialLanguage = new LanguageSelector(LanguageDropDownType.DataOnly);
            this._LearningPathMaterialLanguage.ID = "LearningPathMaterialModifyModal_LearningPathMaterialLanguage_Field";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathMaterialModifyModal_LearningPathMaterialLanguage",
                                                             _GlobalResources.Language,
                                                             this._LearningPathMaterialLanguage.ID,
                                                             this._LearningPathMaterialLanguage,
                                                             false,
                                                             false,
                                                             false));

            // learning path material search tags field
            this._LearningPathMaterialSearchTags = new TextBox();
            this._LearningPathMaterialSearchTags.ID = "LearningPathMaterialModifyModal_LearningPathMaterialSearchTags_Field";
            this._LearningPathMaterialSearchTags.Style.Add("width", "98%");
            this._LearningPathMaterialSearchTags.TextMode = TextBoxMode.MultiLine;
            this._LearningPathMaterialSearchTags.Rows = 5;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("LearningPathMaterialModifyModal_LearningPathMaterialSearchTags",
                                                             _GlobalResources.SearchTags,
                                                             this._LearningPathMaterialSearchTags.ID,
                                                             this._LearningPathMaterialSearchTags,
                                                             false,
                                                             false,
                                                             true));

            // return the properties panel
            return propertiesPanel;
        }
        #endregion

        #region _LoadLearningPathMaterialModifyModalContent
        /// <summary>
        /// Loads content for learning path material modify modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadLearningPathMaterialModifyModalContent(object sender, EventArgs e)
        {
            try
            {
                // clear the modal feedback
                this._LearningPathMaterialModifyModal.ClearFeedback();

                // make the submit and close buttons visible
                this._LearningPathMaterialModifyModal.SubmitButton.Visible = true;
                this._LearningPathMaterialModifyModal.CloseButton.Visible = true;

                // get the header and submit button
                Label modalHeader = (Label)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModalModalPopupHeaderText");
                modalHeader.Text = _GlobalResources.NewLearningPathMaterial;

                Button submitButton = (Button)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModalModalPopupSubmitButton");
                submitButton.Text = _GlobalResources.CreateLearningPathMaterial;

                // get the learning path material id from the hidden input                
                int idLearningPathMaterial = Convert.ToInt32(this._LearningPathMaterialModifyData.Value);

                // reset the form fields
                this._ResetLearningPathMaterialModifyFormFields();

                // populate the form if the learning path material id is greater than 0
                if (idLearningPathMaterial > 0)
                {
                    // set the submit button text to "Save Changes"
                    submitButton.Text = _GlobalResources.SaveChanges;

                    // get the learning path material object
                    this._LearningPathMaterialObject = new DocumentRepositoryItem(idLearningPathMaterial);

                    // set the modal header
                    string learningPathMaterialTitleInInterfaceLanguage = this._LearningPathMaterialObject.Label;

                    if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        foreach (DocumentRepositoryItem.LanguageSpecificProperty learningPathMaterialLanguageSpecificProperty in this._LearningPathMaterialObject.LanguageSpecificProperties)
                        {
                            if (learningPathMaterialLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                            { learningPathMaterialTitleInInterfaceLanguage = learningPathMaterialLanguageSpecificProperty.Label; }
                        }
                    }

                    modalHeader.Text = learningPathMaterialTitleInInterfaceLanguage;

                    // LANGUAGE SPECIFIC PROPERTIES

                    // title, search tags
                    bool isDefaultPopulated = false;

                    foreach (DocumentRepositoryItem.LanguageSpecificProperty learningPathMaterialLanguageSpecificProperty in this._LearningPathMaterialObject.LanguageSpecificProperties)
                    {
                        // if the language is the default language, populate the control directly attached to this page,
                        // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                        // it; note that if we cannot populate the controls directly attached to this page (default) from
                        // language-specific properties, we will use the values in the properties that come from the base table
                        if (learningPathMaterialLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            this._LearningPathMaterialLabel.Text = learningPathMaterialLanguageSpecificProperty.Label;
                            this._LearningPathMaterialSearchTags.Text = learningPathMaterialLanguageSpecificProperty.SearchTags;

                            isDefaultPopulated = true;
                        }
                        else
                        {
                            // get text boxes
                            TextBox languageSpecificLearningPathMaterialTitleTextBox = (TextBox)this._LearningPathMaterialModifyModal.FindControl(this._LearningPathMaterialLabel.ID + "_" + learningPathMaterialLanguageSpecificProperty.LangString);
                            TextBox languageSpecificLearningPathMaterialSearchTagsTextBox = (TextBox)this._LearningPathMaterialModifyModal.FindControl(this._LearningPathMaterialSearchTags.ID + "_" + learningPathMaterialLanguageSpecificProperty.LangString);

                            // if the text boxes were found, set the text box values to the language-specific value
                            if (languageSpecificLearningPathMaterialTitleTextBox != null)
                            { languageSpecificLearningPathMaterialTitleTextBox.Text = learningPathMaterialLanguageSpecificProperty.Label; }

                            if (languageSpecificLearningPathMaterialSearchTagsTextBox != null)
                            { languageSpecificLearningPathMaterialSearchTagsTextBox.Text = learningPathMaterialLanguageSpecificProperty.SearchTags; }
                        }
                    }

                    if (!isDefaultPopulated)
                    {
                        this._LearningPathMaterialLabel.Text = this._LearningPathMaterialObject.Label;
                        this._LearningPathMaterialSearchTags.Text = this._LearningPathMaterialObject.SearchTags;
                    }

                    // NON-LANGUAGE SPECIFIC PROPERTIES

                    // populate and show the learning path material file name field                    
                    Panel learningPathMaterialModifyFileNameFieldContainer = (Panel)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_FileNameFieldContainer");
                    learningPathMaterialModifyFileNameFieldContainer.Style.Add("display", "block");

                    Label learningPathMaterialModifyFileNameField = (Label)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_FileNameField");
                    learningPathMaterialModifyFileNameField.Text = this._LearningPathMaterialObject.FileName;

                    // populate and show the learning path material file size field                    
                    Panel learningPathMaterialModifyFileSizeFieldContainer = (Panel)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_LearningPathMaterialFileSize_Container");
                    learningPathMaterialModifyFileSizeFieldContainer.Style.Add("display", "block");

                    Label learningPathMaterialModifyFileSizeField = (Label)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_FileSizeField");
                    learningPathMaterialModifyFileSizeField.Text = Asentia.Common.Utility.GetSizeStringFromKB(this._LearningPathMaterialObject.Kb);

                    // is private
                    if (this._LearningPathMaterialObject.IsPrivate == true)
                    { this._LearningPathMaterialIsPrivate.Checked = true; }
                    else
                    { this._LearningPathMaterialIsPrivate.Checked = false; }

                    // is all languages
                    if (this._LearningPathMaterialObject.IsAllLanguages == true)
                    { this._LearningPathMaterialIsAllLanguages.Checked = true; }
                    else
                    { this._LearningPathMaterialIsAllLanguages.Checked = false; }

                    // learning path material language
                    if (this._LearningPathMaterialObject.LanguageString != null && this._LearningPathMaterialLanguage.Items.FindByValue(this._LearningPathMaterialObject.LanguageString) != null)
                    { this._LearningPathMaterialLanguage.SelectedValue = this._LearningPathMaterialObject.LanguageString; }
                }

                // build a startup script for handling JS control initialization
                StringBuilder multipleStartUpCallsScript = new StringBuilder();

                multipleStartUpCallsScript.AppendLine(" // initialize learning path material modify controls");
                multipleStartUpCallsScript.AppendLine(" LearningPathMaterialLanguageSelectionClick(document.getElementById(\"LearningPathMaterialModifyModal_LearningPathMaterialIsAllLanguages_Field\"));");
                multipleStartUpCallsScript.AppendLine(" ResetLearningPathMaterialUploaderField();");

                ScriptManager.RegisterStartupScript(this, this.GetType(), this._LearningPathMaterialModifyModal.ID + "StartupScript", multipleStartUpCallsScript.ToString(), true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(dnfEx.Message, true);
                this._LearningPathMaterialModifyModal.SubmitButton.Visible = false;
                this._LearningPathMaterialModifyModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(fnuEx.Message, true);
                this._LearningPathMaterialModifyModal.SubmitButton.Visible = false;
                this._LearningPathMaterialModifyModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(cpeEx.Message, true);
                this._LearningPathMaterialModifyModal.SubmitButton.Visible = false;
                this._LearningPathMaterialModifyModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(dEx.Message, true);
                this._LearningPathMaterialModifyModal.SubmitButton.Visible = false;
                this._LearningPathMaterialModifyModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(ex.Message, true);
                this._LearningPathMaterialModifyModal.SubmitButton.Visible = false;
                this._LearningPathMaterialModifyModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _ResetLearningPathMaterialModifyFormFields
        /// <summary>
        /// Resets the form fields for the learning path material modify modal.
        /// </summary>
        private void _ResetLearningPathMaterialModifyFormFields()
        {
            // reset the error containers
            Panel learningPathMaterialModifyModalFormPropertiesPanel = (Panel)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_LearningPathMaterialProperties");
            AsentiaPage.ClearErrorClassFromFieldsRecursive(learningPathMaterialModifyModalFormPropertiesPanel);

            // LANGUAGE SPECIFIC PROPERTIES

            // title, search tags
            this._LearningPathMaterialLabel.Text = "";
            this._LearningPathMaterialSearchTags.Text = "";

            // loop through each language available to the site and clear the text boxes for it
            foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
            {
                // get text boxes
                TextBox languageSpecificLearningPathMaterialTitleTextBox = (TextBox)this._LearningPathMaterialModifyModal.FindControl(this._LearningPathMaterialLabel.ID + "_" + language);
                TextBox languageSpecificLearningPathMaterialSearchTagsTextBox = (TextBox)this._LearningPathMaterialModifyModal.FindControl(this._LearningPathMaterialSearchTags.ID + "_" + language);

                // if the text boxes were found, set the text box values to the language-specific value
                if (languageSpecificLearningPathMaterialTitleTextBox != null)
                { languageSpecificLearningPathMaterialTitleTextBox.Text = ""; }

                if (languageSpecificLearningPathMaterialSearchTagsTextBox != null)
                { languageSpecificLearningPathMaterialSearchTagsTextBox.Text = ""; }
            }

            // NON-LANGUAGE SPECIFIC PROPERTIES

            // clear and hide the learning path material file name field                    
            Panel learningPathMaterialModifyFileNameFieldContainer = (Panel)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_FileNameFieldContainer");
            learningPathMaterialModifyFileNameFieldContainer.Style.Add("display", "none");

            Label learningPathMaterialModifyFileNameField = (Label)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_FileNameField");
            learningPathMaterialModifyFileNameField.Text = "";

            // clear and hide the learning path material file size field                    
            Panel learningPathMaterialModifyFileSizeFieldContainer = (Panel)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_LearningPathMaterialFileSize_Container");
            learningPathMaterialModifyFileSizeFieldContainer.Style.Add("display", "none");

            Label learningPathMaterialModifyFileSizeField = (Label)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_FileSizeField");
            learningPathMaterialModifyFileSizeField.Text = "";

            // is private            
            this._LearningPathMaterialIsPrivate.Checked = false;

            // is all languages
            this._LearningPathMaterialIsAllLanguages.Checked = false;

            // learning path material language
            this._LearningPathMaterialLanguage.SelectedIndex = 0;
        }
        #endregion

        #region _ValidateLearningPathMaterialPropertiesForm
        /// <summary>
        /// Validates the learning path material properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateLearningPathMaterialPropertiesForm()
        {
            Panel learningPathMaterialModifyModalFormPropertiesPanel = (Panel)this._LearningPathMaterialModifyModal.FindControl("LearningPathMaterialModifyModal_LearningPathMaterialProperties");

            bool isValid = true;

            // LABEL - DEFAULT LANGUAGE REQUIRED
            if (String.IsNullOrWhiteSpace(this._LearningPathMaterialLabel.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(learningPathMaterialModifyModalFormPropertiesPanel, "LearningPathMaterialModifyModal_LearningPathMaterialLabel", _GlobalResources.Name + " " + _GlobalResources.IsRequiredInThePortalsDefaultLanguage);
            }

            // LEARNING PATH MATERIAL FILE, required if saving new learning path material            
            if (Convert.ToInt32(this._LearningPathMaterialModifyData.Value) == 0)
            {
                if (String.IsNullOrWhiteSpace(this._LearningPathMaterialUploader.FileSavedName) || !File.Exists(Server.MapPath(this._LearningPathMaterialUploader.SavedFilePath)))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(learningPathMaterialModifyModalFormPropertiesPanel, "LearningPathMaterialModifyModal_LearningPathMaterialFile", _GlobalResources.PleaseSelectAFileToUpload);
                }
            }

            return isValid;
        }
        #endregion

        #region _LearningPathMaterialModifySubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the learning path material modify modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _LearningPathMaterialModifySubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // clear the modal feedback
                this._LearningPathMaterialModifyModal.ClearFeedback();

                // validate the form
                if (!this._ValidateLearningPathMaterialPropertiesForm())
                { throw new AsentiaException(); }

                // instansiate the learning path material object for new and existing learning path material
                int idLearningPathMaterial = Convert.ToInt32(this._LearningPathMaterialModifyData.Value);

                if (idLearningPathMaterial > 0)
                { this._LearningPathMaterialObject = new DocumentRepositoryItem(idLearningPathMaterial); }
                else
                { this._LearningPathMaterialObject = new DocumentRepositoryItem(); }

                // populate the object
                this._LearningPathMaterialObject.IdDocumentRepositoryObjectType = DocumentRepositoryObjectType.LearningPath;
                this._LearningPathMaterialObject.IdObject = this._LearningPathObject.Id;
                this._LearningPathMaterialObject.IdOwner = AsentiaSessionState.IdSiteUser;

                this._LearningPathMaterialObject.IsPrivate = this._LearningPathMaterialIsPrivate.Checked;

                this._LearningPathMaterialObject.IsAllLanguages = this._LearningPathMaterialIsAllLanguages.Checked;

                if (!this._LearningPathMaterialIsAllLanguages.Checked)
                { this._LearningPathMaterialObject.LanguageString = this._LearningPathMaterialLanguage.SelectedValue; }
                else
                { this._LearningPathMaterialObject.LanguageString = null; }

                this._LearningPathMaterialObject.Label = this._LearningPathMaterialLabel.Text;

                if (String.IsNullOrWhiteSpace(this._LearningPathMaterialSearchTags.Text))
                { this._LearningPathMaterialObject.SearchTags = null; }
                else
                { this._LearningPathMaterialObject.SearchTags = this._LearningPathMaterialSearchTags.Text; }

                if (this._LearningPathMaterialObject.Id > 0 && this._LearningPathMaterialUploader.SavedFilePath != null)
                {
                    // get the learning path's document folder path
                    string fullSavedFilePath = null;
                    fullSavedFilePath = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LEARNINGPATH + this._LearningPathObject.Id + "/";

                    // remove the existing file for this learning path material
                    if (File.Exists(Server.MapPath(fullSavedFilePath + this._LearningPathMaterialObject.FileName)))
                    { File.Delete(Server.MapPath(fullSavedFilePath + this._LearningPathMaterialObject.FileName)); }

                    // save the uploaded file
                    string fileOriginalName = String.Empty;
                    fullSavedFilePath = DocumentRepositoryItem.GetCorrectFileName(fullSavedFilePath + this._LearningPathMaterialUploader.FileOriginalName, out fileOriginalName);

                    File.Copy(Server.MapPath(this._LearningPathMaterialUploader.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                    File.Delete(Server.MapPath(this._LearningPathMaterialUploader.SavedFilePath));

                    this._LearningPathMaterialObject.FileName = fileOriginalName;
                    this._LearningPathMaterialObject.Kb = Convert.ToInt32(this._LearningPathMaterialUploader.FileSize) / 1000;
                }
                else if (this._LearningPathMaterialObject.Id == 0 && this._LearningPathMaterialUploader.SavedFilePath != null)
                {
                    // check learning path's documents folder existence and create if necessary
                    if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LEARNINGPATH + this._LearningPathObject.Id)))
                    { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LEARNINGPATH + this._LearningPathObject.Id)); }

                    string fullSavedFilePath = null;

                    if (File.Exists(Server.MapPath(this._LearningPathMaterialUploader.SavedFilePath)))
                    {
                        fullSavedFilePath = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LEARNINGPATH + this._LearningPathObject.Id + "/" + this._LearningPathMaterialUploader.FileOriginalName;

                        string fileOriginalName = String.Empty;
                        fullSavedFilePath = DocumentRepositoryItem.GetCorrectFileName(fullSavedFilePath, out fileOriginalName);

                        // move the uploaded file into the learning path material's folder
                        File.Copy(Server.MapPath(this._LearningPathMaterialUploader.SavedFilePath), Server.MapPath(fullSavedFilePath), true);
                        File.Delete(Server.MapPath(this._LearningPathMaterialUploader.SavedFilePath));

                        this._LearningPathMaterialObject.FileName = fileOriginalName;
                        this._LearningPathMaterialObject.Kb = Convert.ToInt32(this._LearningPathMaterialUploader.FileSize) / 1000;
                    }
                    else
                    { this._LearningPathMaterialObject.FileName = null; }
                }

                // save the learning path material, save its returned id to viewstate, and set the current learning path material object's id
                idLearningPathMaterial = this._LearningPathMaterialObject.Save();
                this._LearningPathMaterialObject.Id = idLearningPathMaterial;
                this._LearningPathMaterialModifyData.Value = idLearningPathMaterial.ToString();

                // do learning path material language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string learningPathMaterialLabel = null;
                        string learningPathMaterialSearchTags = null;

                        // get text boxes
                        TextBox languageSpecificLearningPathMaterialLabelTextBox = (TextBox)this._LearningPathMaterialModifyModal.FindControl(this._LearningPathMaterialLabel.ID + "_" + cultureInfo.Name);
                        TextBox languageSpecificLearningPathMaterialSearchTagsTextBox = (TextBox)this._LearningPathMaterialModifyModal.FindControl(this._LearningPathMaterialSearchTags.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificLearningPathMaterialLabelTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLearningPathMaterialLabelTextBox.Text))
                            { learningPathMaterialLabel = languageSpecificLearningPathMaterialLabelTextBox.Text; }
                        }

                        if (languageSpecificLearningPathMaterialSearchTagsTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificLearningPathMaterialSearchTagsTextBox.Text))
                            { learningPathMaterialSearchTags = languageSpecificLearningPathMaterialSearchTagsTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(learningPathMaterialLabel) ||
                            !String.IsNullOrWhiteSpace(learningPathMaterialSearchTags))
                        {
                            this._LearningPathMaterialObject.SaveLang(cultureInfo.Name,
                                                                      learningPathMaterialLabel,
                                                                      learningPathMaterialSearchTags);
                        }
                    }
                }

                // rebind the learning path material grid
                this._LearningPathMaterialGrid.BindData();

                // reload the saved learning path material
                this._LoadLearningPathMaterialModifyModalContent(sender, e);

                // display the feedback
                this._LearningPathMaterialModifyModal.DisplayFeedback(_GlobalResources.LearningPathMaterialHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._LearningPathMaterialModifyModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion
        #endregion
    }
}
