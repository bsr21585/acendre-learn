﻿//Page-specific tab change method
function LearningPathPropertiesTabChange(selectedTabId, tabContainerId) {
    switch (selectedTabId) {
        case "Properties":
        case "Courses":            
            $("#AddLearningPathMaterialLinkContainer").hide();
            
            $("#LearningPathFormInstructionsPanel").show();
            
            $("#LearningPathMaterialGridDeleteButton").hide();
            $("#LearningPathSaveButton").show();
            $("#CancelButton").show();
            break;        
        case "LearningPathMaterials":            
            $("#AddLearningPathMaterialLinkContainer").show();

            $("#LearningPathFormInstructionsPanel").hide();            

            $("#LearningPathSaveButton").hide();
            $("#CancelButton").hide();            
            $("#LearningPathMaterialGridDeleteButton").show();
            break;
        default:
            break;
    }
}

//Method to add courses from modal popup to the learning path courses listing container
function AddCoursesToLearningPath() {
    var learningPathCoursesListContainer = $("#" + LearningPathCoursesListContainer);
    var selectedCourses = $('select#SelectEligibleCoursesListBox').val();

    if (selectedCourses != null) {
        for (var i = 0; i < selectedCourses.length; i++) {
            if (!isNaN(Number(selectedCourses[i])) && !$("#Course_" + selectedCourses[i]).length) {
                // add the selected user to the course expert list container
                var itemContainerDiv = $("<div id=\"Course_" + selectedCourses[i] + "\">" + "<img onclick=\"javascript:RemoveCoursesFromLearningPath('" + selectedCourses[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" class=\"SmallIcon\" />" + $("#SelectEligibleCoursesListBox option[value='" + selectedCourses[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(learningPathCoursesListContainer);

                // remove the user from the select list
                $("#SelectEligibleCoursesListBox option[value='" + selectedCourses[i] + "']").remove();
            }
        }
    }
}

//Method to remove selected courses from the learning path courses listing container
function RemoveCoursesFromLearningPath(courseId) {
    $("#Course_" + courseId).remove();
}

//Gets the selected courses for the learning path
function GetSelectedCoursesForHiddenField() {
    var learningPathCoursesListContainer = $("#" + LearningPathCoursesListContainer);
    var selectedLearningPathCoursesField = $("#" + SelectedLearningPathCourses);
    var selectedLearningPathCourses = "";

    learningPathCoursesListContainer.children().each(function () {
        selectedLearningPathCourses = selectedLearningPathCourses + $(this).prop("id").replace("Course_", "") + ",";
    });

    if (selectedLearningPathCourses.length > 0)
    { selectedLearningPathCourses = selectedLearningPathCourses.substring(0, selectedLearningPathCourses.length - 1); }

    selectedLearningPathCoursesField.val(selectedLearningPathCourses);
}

//Method to populate hidden fields for dynamically denerated/selected items
function PopulateHiddenFieldsForDynamicLearningPathElements() {
    GetSelectedCoursesForHiddenField();
}

//Method to remove a learning path avatar
function DeleteLearningPathAvatar() {
    $("#LearningPathAvatar_Field_ImageContainer").remove();
    $("#ClearAvatar_Field").val("true");
}

//Updates shortcode link as you type
function UpdateShortcodeLink(shortcodeFieldId) {
    var shortcodeLinkText = $("#ShortcodeLink").text();
    var shortcodeLinkTextSplit = shortcodeLinkText.split("?");
    var shortcodeQSPrefix = "?type=learningpath&sc=";

    shortcodeLinkText = shortcodeLinkTextSplit[0] + shortcodeQSPrefix + $("#LearningPathShortcode_Field").val();
    $("#ShortcodeLink").text(shortcodeLinkText);
}

/* LEARNING PATH MATERIAL GRID */

function LoadMakeLearningPathMaterialPrivateModalContent(idDocumentRepositoryItem) {
    $("#LearningPathMaterialMakePrivateData").val(idDocumentRepositoryItem);

    $("#LearningPathMaterialMakePrivateConfirmationModalLaunchButton").click();
    $("#LearningPathMaterialMakePrivateConfirmationModalLoadButton").click();
}

function LoadMakeLearningPathMaterialPublicModalContent(idDocumentRepositoryItem) {
    $("#LearningPathMaterialMakePublicData").val(idDocumentRepositoryItem);

    $("#LearningPathMaterialMakePublicConfirmationModalLaunchButton").click();
    $("#LearningPathMaterialMakePublicConfirmationModalLoadButton").click();
}

/* LEARNING PATH MATERIAL MODIFY */

function LoadLearningPathMaterialModifyModalContent(idLearningPathMaterial, doPostbackLoad) {
    $("#LearningPathMaterialModifyData").val(idLearningPathMaterial);
    $("#LearningPathMaterialModifyModalLaunchButton").click();

    if (doPostbackLoad) {
        $("#LearningPathMaterialModifyModalLoadButton").click();
    }
    else {
        LearningPathMaterialLanguageSelectionClick(document.getElementById("LearningPathMaterialModifyModal_LearningPathMaterialIsAllLanguages_Field"));
        ResetLearningPathMaterialUploaderField();
    }
}

function PopulateHiddenFieldsForDynamicLearningPathMaterialElements() {
}

//Method to reset the learning path material uploader
function ResetLearningPathMaterialUploaderField() {
    $("#LearningPathMaterialModifyModal_LearningPathMaterialFile_Field_CompletedPanel").html("");
    $("#LearningPathMaterialModifyModal_LearningPathMaterialFile_ErrorContainer").html("");
    $("#LearningPathMaterialModifyModal_LearningPathMaterialFile_Field_UploadControl input").attr("style", "");
    $("#LearningPathMaterialModifyModal_LearningPathMaterialFile_Field_UploadHiddenFieldOriginalFileName").val("");
    $("#LearningPathMaterialModifyModal_LearningPathMaterialFile_Field_UploadHiddenField").val("");
}

//Method to enable/disable the learning path material language selector based on the "all languages" checkbox
function LearningPathMaterialLanguageSelectionClick(ele) {
    if (ele.checked == true) {
        $("#LearningPathMaterialModifyModal_LearningPathMaterialLanguage_Field").attr("disabled", "disabled")
    }
    else {
        $("#LearningPathMaterialModifyModal_LearningPathMaterialLanguage_Field").removeAttr("disabled");
    }
}