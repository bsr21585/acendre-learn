﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.LearningPaths.RulesetEnrollments
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel RuleSetEnrollmentPropertiesFormContentWrapperContainer;
        public Panel LearningPathObjectMenuContainer;
        public Panel RuleSetLearningPathEnrollmentPropertiesWrapperContainer;
        public Panel RuleSetLearningPathEnrollmentPropertiesInstructionsPanel;
        public Panel RuleSetLearningPathEnrollmentPropertiesFeedbackContainer;
        public Panel RuleSetLearningPathEnrollmentPropertiesContainer;
        public Panel RuleSetLearningPathEnrollmentPropertiesActionsPanel;
        public Panel RuleSetLearningPathEnrollmentPropertiesTabPanelsContainer;
        public Panel ObjectOptionsPanel;
        #endregion

        #region Private Properties
        private LearningPath _LearningPathObject;
        private RuleSetLearningPathEnrollment _RuleSetLearningPathEnrollmentObject;        
        private AutoJoinRuleSet _AutoJoinRuleSet;
        private RuleSet _RuleSetObject;

        private bool _IsStartDatePassed = false;
        private bool _RuleSetControlLoaded;

        private TimeZoneSelector _Timezone;
        private DatePicker _DtStart;
        private DatePicker _DtEnd;

        private DateIntervalSelector _Delay;
        private DateIntervalSelector _Due;
        private DateIntervalSelector _ExpiresFromStart;
        private DateIntervalSelector _ExpiresFromFirstLaunch;

        private TextBox _Label;
        private TextBox _LabelTextbox;

        private DataTable _Rules;
        private Grid _LearningPathRuleSetsGrid;

        private RadioButton _MatchAny;
        private RadioButton _MatchAll;

        private HiddenField _RulesData;
        private HiddenField _IdRulesetModalHidden;
        private HiddenField _IdRulesetEnrollmentHiddenField;

        private Button _AddModifyRuleSetModalButton;
        private Button _SubmitButtonMadal;
        private Button _CloseButtonMadal;
        private Button _RuleSetLearningPathEnrollmentPropertiesSaveButton;
        private Button _RuleSetLearningPathEnrollmentPropertiesCancelButton;

        private LinkButton _AddModifyRuleSetHiddenButton;
        private LinkButton _DeleteButton;

        private Panel _RuleSetModalPropertiesContainer;
        private Panel _RuleSetsPanel;
        private Panel _RuleSetGridActionsPanel;
        private PlaceHolder _RuleSetControlPlaceHolder;

        private ModalPopup _GridConfirmAction;
        private ModalPopup _AddModifyRuleSetModal;

        private DataTable _EligibleLearningPathsForCheckBoxList;
        private DynamicCheckBoxList __SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication;
        private ModalPopup _SelectLearningPathsForRuleSetLearningPathEnrollmentReplication;

        private HiddenField _ModifyRulesetId;
        #endregion

        #region Page Init Event
        /// <summary>
        /// Page_Init event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Init(object sender, EventArgs e)
        {
            this._RulesData = new HiddenField();
            this._RulesData.ID = "RulesData";
            this._IdRulesetModalHidden = new HiddenField();
            this._AddModifyRuleSetHiddenButton = new LinkButton();
            this._AddModifyRuleSetModalButton = new Button();

            this._RuleSetControlPlaceHolder = new PlaceHolder();
            this._LabelTextbox = new TextBox();
            this._RuleSetControlLoaded = false;            
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LMS.Pages.Administrator.LearningPaths.RulesetEnrollments.Modify.js");

            
            csm.RegisterStartupScript(typeof(Modify), "JsVariables", "var AddRuleSetModalHeader ='" + _GlobalResources.NewRuleset
            + "';var IdRulesetModalHidden ='" + this._IdRulesetModalHidden.ID
            + "';var AddModifyRuleSetHiddenButton ='" + this._AddModifyRuleSetHiddenButton.ID
            + "';var ModifyRuleSetModalButton ='" + this._AddModifyRuleSetModalButton.ID
            + "';var CreateRulsetText ='" + _GlobalResources.CreateRuleset
            + "';var RuleSetLabel_Field ='" + this._LabelTextbox.ID
            + "';", true);

            StringBuilder startUpCallsScript = new StringBuilder();

            if (this._ModifyRulesetId != null)
            {
                if (this._ModifyRulesetId.Value != String.Empty)
                {
                    this._AddModifyRuleSetModal.ShowModal();
                    startUpCallsScript.AppendLine("OnRuleSetModifyLoad('" + this._ModifyRulesetId.Value + "');");
                }
            }

            csm.RegisterStartupScript(typeof(Modify), "startUpCalls", startUpCallsScript.ToString(), true);

            if (!this._RuleSetControlLoaded)
            {
                if (this._RuleSetObject == null)
                { this._AutoJoinRuleSet = new AutoJoinRuleSet(); }
                else
                { this._AutoJoinRuleSet = new AutoJoinRuleSet(this._RuleSetObject.Id); }

                this._AutoJoinRuleSet.ID = "Rule_Field";
                this._AutoJoinRuleSet.ClientIDMode = ClientIDMode.Static;

                this._RuleSetControlPlaceHolder.Controls.Clear();
                this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
                EnsureChildControls();
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager))
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("page-specific/administrator/learningPaths/rulesetenrollments/Modify.css");

            // get the ruleset enrollment and learning path objects
            this._GetRuleSetLearningPathEnrollmentAndLearningPathObjects();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // build the controls for the page
            this._BuildControls();
        }
        #endregion        

        #region _GetRuleSetLearningPathEnrollmentAndLearningPathObjects
        /// <summary>
        /// Gets the learning path and ruleset enrollment objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetRuleSetLearningPathEnrollmentAndLearningPathObjects()
        {
            // get the id querystring parameter
            int qsLPId = this.QueryStringInt("lpid", 0);
            int vsLPId = this.ViewStateInt(this.ViewState, "lpid", 0);
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            // get learning path object - learning path object MUST be specified and exist for this page to load
            if (qsLPId > 0 || vsLPId > 0)
            {
                int lpid = 0;

                if (qsLPId > 0)
                { lpid = qsLPId; }

                if (vsLPId > 0)
                { lpid = vsLPId; }

                try
                {
                    if (lpid > 0)
                    { this._LearningPathObject = new LearningPath(lpid); }
                }
                catch
                { Response.Redirect("~/administrator/learningpaths"); }
            }
            else
            { Response.Redirect("~/administrator/learningpaths"); }

            // get ruleset enrollment object (if exists), and set the ruleset enrollment type based on either properties of
            // the ruleset enrollment, or the type specified in a querystring parameter, if no ruleset enrollment object
            // or type cannot be set from querystring parameter, redirect because an invalid ruleset enrollment would be created
            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    {
                        this._RuleSetLearningPathEnrollmentObject = new RuleSetLearningPathEnrollment(id);

                        if (this._RuleSetLearningPathEnrollmentObject.DtStart <= AsentiaSessionState.UtcNow)
                        { this._IsStartDatePassed = true; }
                    }
                }
                catch
                { Response.Redirect("~/administrator/learningpaths/Modify.aspx?id=" + this._LearningPathObject.Id.ToString()); }
            }

        }
        #endregion

        #region _PopulatePropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulatePropertiesInputElements()
        {
            if (this._RuleSetObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                //rule set title
                bool isDefaultPopulated = false;

                foreach (RuleSet.LanguageSpecificProperty ruleSetLanguageSpecificProperty in this._RuleSetObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (ruleSetLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._LabelTextbox.Text = ruleSetLanguageSpecificProperty.Label;
                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificRuleSetLabelTextBox = (TextBox)this._RuleSetModalPropertiesContainer.FindControl(this._LabelTextbox.ID + "_" + ruleSetLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificRuleSetLabelTextBox != null)
                        { languageSpecificRuleSetLabelTextBox.Text = ruleSetLanguageSpecificProperty.Label; }

                    }
                }

                if (!isDefaultPopulated)
                {
                    this._LabelTextbox.Text = this._RuleSetObject.Label;
                }

                //populating language independent properties
                if (this._RuleSetObject.IsAny)
                {
                    this._MatchAny.Checked = true;
                }
                else
                {
                    this._MatchAll.Checked = true;
                }
            }
            else
            {
                this._LabelTextbox.Text = string.Empty;
                this._MatchAny.Checked = true;
            }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Build controls
        /// </summary>
        private void _BuildControls()
        {
            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.RuleSetEnrollmentPropertiesFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";            
            this.RuleSetLearningPathEnrollmentPropertiesWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the learning path object menu            
            if (this._LearningPathObject != null)
            {
                LearningPathObjectMenu learningPathObjectMenu = new LearningPathObjectMenu(this._LearningPathObject);
                learningPathObjectMenu.SelectedItem = LearningPathObjectMenu.MenuObjectItem.RulesetEnrollments;

                this.LearningPathObjectMenuContainer.Controls.Add(learningPathObjectMenu);
            }            

            // build the ruleset enrollment properties form
            this._BuildRuleSetLearningPathEnrollmentPropertiesForm();

            // build the ruleset enrollment properties form actions panel
            this._BuildRuleSetLearningPathEnrollmentPropertiesActionsPanel();
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get learning path title information
            string learningPathTitleInInterfaceLanguage = this._LearningPathObject.Name;
            string learningPathImagePath;
            string learningPathImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (LearningPath.LanguageSpecificProperty learningPathLanguageSpecificProperty in this._LearningPathObject.LanguageSpecificProperties)
                {
                    if (learningPathLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { learningPathTitleInInterfaceLanguage = learningPathLanguageSpecificProperty.Name; }
                }
            }

            if (this._LearningPathObject.Avatar != null)
            {
                learningPathImagePath = SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + this._LearningPathObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                learningPathImageCssClass = "AvatarImage";
            }
            else
            {
                learningPathImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
            }

            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;            
            string pageTitle;

            if (this._RuleSetLearningPathEnrollmentObject != null)
            {
                string ruleSetEnrollmentTitleInInterfaceLanguage = this._RuleSetLearningPathEnrollmentObject.Label;

                if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    foreach (RuleSetLearningPathEnrollment.LanguageSpecificProperty ruleSetEnrollmentLanguageSpecificProperty in this._RuleSetLearningPathEnrollmentObject.LanguageSpecificProperties)
                    {
                        if (ruleSetEnrollmentLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                        { ruleSetEnrollmentTitleInInterfaceLanguage = ruleSetEnrollmentLanguageSpecificProperty.Label; }
                    }
                }

                breadCrumbPageTitle = ruleSetEnrollmentTitleInInterfaceLanguage;
                pageTitle = ruleSetEnrollmentTitleInInterfaceLanguage;
            }
            else
            {
                breadCrumbPageTitle = _GlobalResources.NewRulesetEnrollment;
                pageTitle = _GlobalResources.NewRulesetEnrollment;
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.LearningPaths, "/administrator/learningpaths"));
            breadCrumbLinks.Add(new BreadcrumbLink(learningPathTitleInInterfaceLanguage, "/administrator/learningpaths/Dashboard.aspx?id=" + this._LearningPathObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.RulesetEnrollments, "/administrator/learningpaths/rulesetenrollments/Default.aspx?lpid=" + this._LearningPathObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, learningPathTitleInInterfaceLanguage, learningPathImagePath, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG), learningPathImageCssClass);
        }
        #endregion

        #region _BuildRuleSetLearningPathEnrollmentPropertiesForm
        /// <summary>
        /// Builds the ruleset enrollment Properties form.
        /// </summary>
        private void _BuildRuleSetLearningPathEnrollmentPropertiesForm()
        {
            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.RuleSetLearningPathEnrollmentPropertiesInstructionsPanel, _GlobalResources.CompleteTheFormBelowToCreateOrUpdateRulesetEnrollment, true);

            // clear controls from container
            this.RuleSetLearningPathEnrollmentPropertiesContainer.Controls.Clear();

            // build the ruleset enrollment properties form tabs
            this._BuildRuleSetLearningPathEnrollmentPropertiesFormTabs();

            this.RuleSetLearningPathEnrollmentPropertiesTabPanelsContainer = new Panel();
            this.RuleSetLearningPathEnrollmentPropertiesTabPanelsContainer.ID = "RuleSetLearningPathEnrollmentProperties_TabPanelsContainer";
            this.RuleSetLearningPathEnrollmentPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.RuleSetLearningPathEnrollmentPropertiesContainer.Controls.Add(this.RuleSetLearningPathEnrollmentPropertiesTabPanelsContainer);

            // build the "properties" panel of the ruleset enrollment properties form
            this._BuildRuleSetLearningPathEnrollmentPropertiesFormPropertiesPanel();

            

            if (this._RuleSetLearningPathEnrollmentObject != null)
            {
                // build the rulesets form
                this._BuildRuleSetLearningPathEnrollmentPropertiesFormRulesetsPanel();

                this._BuildRulesetGridActionsPanel();

                this._BuildRulesetGridActionsModal();
            }

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulateRuleSetLearningPathEnrollmentPropertiesInputElements();
        }
        #endregion

        #region _BuildRuleSetLearningPathEnrollmentPropertiesFormTabs
        /// <summary>
        /// Builds learning path enrollment form tabs
        /// </summary>
        private void _BuildRuleSetLearningPathEnrollmentPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));

            if (this._RuleSetLearningPathEnrollmentObject != null)
            { tabs.Enqueue(new KeyValuePair<string, string>("RuleSets", _GlobalResources.Rulesets)); }

            // build and attach the tabs
            this.RuleSetLearningPathEnrollmentPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("RuleSetLearningPathEnrollmentProperties", tabs));
        }
        #endregion

        #region _BuildRuleSetLearningPathEnrollmentPropertiesFormPropertiesPanel
        /// <summary>
        ///  Builds learning path enrollment form properties
        /// </summary>
        private void _BuildRuleSetLearningPathEnrollmentPropertiesFormPropertiesPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            Panel propertiesPanel = new Panel();
            propertiesPanel.ID = "RuleSetLearningPathEnrollmentProperties_" + "Properties" + "_TabPanel";
            propertiesPanel.Attributes.Add("style", "display: block;");

            Panel propertiesOptionsPanel = new Panel();
            propertiesOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "PropertiesOptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // REPLICATE RULESET ENROLLMENT
            if (this._RuleSetLearningPathEnrollmentObject != null)
            {
                if (this._RuleSetLearningPathEnrollmentObject.IdParentRuleSetLearningPathEnrollment == 0)
                {
                    // populate datatables with lists of courses that ruleset enrollment can be replicated to and those it has already been replicated to
                    this._EligibleLearningPathsForCheckBoxList = RuleSetLearningPathEnrollment.IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList(this._RuleSetLearningPathEnrollmentObject.Id, null);

                    Panel replicateRulesetLearningPathEnrollmentLink = this.BuildOptionsPanelImageLink("ReplicateRulesetLearningPathEnrollmentLink",
                                                                                           null,
                                                                                           "javascript: void(0);",
                                                                                           null,
                                                                                           _GlobalResources.ReplicateRulesetEnrollmentToOtherLearningPaths,
                                                                                           null,
                                                                                           ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG),
                                                                                           ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG));

                    optionsPanelLinksContainer.Controls.Add(replicateRulesetLearningPathEnrollmentLink);

                    propertiesOptionsPanel.Controls.Add(optionsPanelLinksContainer);

                    this._BuildSelectLearningPathsModal(replicateRulesetLearningPathEnrollmentLink.ID);

                    propertiesPanel.Controls.Add(propertiesOptionsPanel);
                }
            }

            // ruleset enrollment label field
            this._Label = new TextBox();
            this._Label.ID = "RuleSetLearningPathEnrollmentLabel_Field";
            this._Label.CssClass = "InputLong";

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetLearningPathEnrollmentLabel",
                                                             _GlobalResources.Label,
                                                             this._Label.ID,
                                                             this._Label,
                                                             true,
                                                             true,
                                                             true));

            // ruleset enrollment date start field
            // if the start date has not passed, its still editable, otherwise it is not
            if (!this._IsStartDatePassed)
            {
                this._DtStart = new DatePicker("RuleSetLearningPathEnrollmentDtStart_Field", false, true, true);
                this._DtStart.NowCheckboxText = _GlobalResources.StartEnrollmentImmediately;
                this._DtStart.Value = TimeZoneInfo.ConvertTimeFromUtc(AsentiaSessionState.UtcNow.AddMinutes(1), TimeZoneInfo.FindSystemTimeZoneById(new Timezone(AsentiaSessionState.GlobalSiteObject.IdTimezone).dotNetName));

                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetLearningPathEnrollmentDtStart",
                                                             _GlobalResources.LifespanStart,
                                                             this._DtStart.ID,
                                                             this._DtStart,
                                                             true,
                                                             true,
                                                             false));
            }
            else
            {
                DateTime dateValue = TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetLearningPathEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetLearningPathEnrollmentObject.IdTimezone).dotNetName));
                Label ruleSetEnrollmentDtStartFieldStaticValue = new Label();

                ruleSetEnrollmentDtStartFieldStaticValue.Text = dateValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern) + " @ " + dateValue.ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern);

                propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetLearningPathEnrollmentDtStart",
                                                             _GlobalResources.LifespanStart,
                                                             null,
                                                             ruleSetEnrollmentDtStartFieldStaticValue,
                                                             true,
                                                             true,
                                                             false));
            }

            // ruleset enrollment date end field
            this._DtEnd = new DatePicker("RuleSetLearningPathEnrollmentDtEnd_Field", true, false, true);
            this._DtEnd.NoneCheckboxText = _GlobalResources.NoEndDate;
            this._DtEnd.NoneCheckBoxChecked = true;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetLearningPathEnrollmentDtEnd",
                                                             _GlobalResources.LifespanEnd,
                                                             this._DtEnd.ID,
                                                             this._DtEnd,
                                                             false,
                                                             true,
                                                             false));

            // ruleset enrollment timezone field
            this._Timezone = new TimeZoneSelector();
            this._Timezone.ID = "RuleSetLearningPathEnrollmentTimezone_Field";
            this._Timezone.SelectedValue = Convert.ToString(AsentiaSessionState.GlobalSiteObject.IdTimezone);

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetLearningPathEnrollmentTimezone",
                                                             _GlobalResources.LifespanTimezone,
                                                             this._Timezone.ID,
                                                             this._Timezone,
                                                             true,
                                                             true,
                                                             false));

            // ruleset enrollment delay field
            this._Delay = new DateIntervalSelector("RuleSetLearningPathEnrollmentDelay_Field", true);
            this._Delay.NoneCheckBoxChecked = true;
            this._Delay.TextBeforeSelector = _GlobalResources.LearnersInheritThisEnrollment;
            this._Delay.TextAfterSelector = _GlobalResources.AfterMeetingRulesetCriteria;
            this._Delay.NoneCheckboxText = _GlobalResources.NoDelay;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetLearningPathEnrollmentDelay",
                                                             _GlobalResources.Delay,
                                                             this._Delay.ID,
                                                             this._Delay,
                                                             false,
                                                             true,
                                                             false));

            // ruleset enrollment due field
            this._Due = new DateIntervalSelector("RuleSetLearningPathEnrollmentDue_Field", true);
            this._Due.NoneCheckboxText = _GlobalResources.NoDueDate;
            this._Due.NoneCheckBoxChecked = true;

            this._Due.TextBeforeSelector = _GlobalResources.LearnersMustCompleteTheLearningPathWithin;
            this._Due.TextAfterSelector = _GlobalResources.AfterEnrollment;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetLearningPathEnrollmentDue",
                                                             _GlobalResources.Due,
                                                             this._Due.ID,
                                                             this._Due,
                                                             false,
                                                             true,
                                                             false));

            // ruleset enrollment expires from start field
            this._ExpiresFromStart = new DateIntervalSelector("RuleSetEnrollmentExpiresFromStart_Field", true);
            this._ExpiresFromStart.NoneCheckboxText = _GlobalResources.Indefinite;
            this._ExpiresFromStart.NoneCheckBoxChecked = true;

            this._ExpiresFromStart.TextBeforeSelector = _GlobalResources.LearnersHaveAccessToTheLearningPathFor;
            this._ExpiresFromStart.TextAfterSelector = _GlobalResources.AfterEnrollment;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentExpiresFromStart",
                                                             _GlobalResources.AccessFromStart,
                                                             this._ExpiresFromStart.ID,
                                                             this._ExpiresFromStart,
                                                             false,
                                                             true,
                                                             false));


            // ruleset enrollment expires from first launch field
            this._ExpiresFromFirstLaunch = new DateIntervalSelector("RuleSetEnrollmentExpiresFromFirstLaunch_Field", true);
            this._ExpiresFromFirstLaunch.NoneCheckboxText = _GlobalResources.Indefinite;
            this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = true;
            this._ExpiresFromFirstLaunch.TextBeforeSelector = _GlobalResources.LearnersHaveAccessToTheLearningPathFor;
            this._ExpiresFromFirstLaunch.TextAfterSelector = _GlobalResources.AfterFirstLaunchOfLearningPath;

            propertiesPanel.Controls.Add(AsentiaPage.BuildFormField("RuleSetEnrollmentExpiresFromFirstLaunch",
                                                             _GlobalResources.AccessFromFirstLaunch,
                                                             this._ExpiresFromFirstLaunch.ID,
                                                             this._ExpiresFromFirstLaunch,
                                                             false,
                                                             true,
                                                             false));

            // attach panel to container
            this.RuleSetLearningPathEnrollmentPropertiesTabPanelsContainer.Controls.Add(propertiesPanel);
        }
        #endregion

        #region _BuildSelectLearningPathsModal
        /// <summary>
        /// Builds the modal for selecting learning paths to replicate ruleset enrollment to.
        /// </summary>
        private void _BuildSelectLearningPathsModal(string targetControlId)
        {
            // set modal properties
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication = new ModalPopup("SelectLearningPathsForRuleSetLearningPathEnrollmentReplicationModal");
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.Type = ModalPopupType.Form;
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.HeaderIconAlt = _GlobalResources.SelectLearningPath_s;
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.HeaderText = _GlobalResources.SelectLearningPath_s;
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.TargetControlID = targetControlId;
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButtonCustomText = _GlobalResources.ReplicateToLearningPath_s;
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButton.Command += new CommandEventHandler(this._ReplicateToLearningPathsSelectLearningPathsButton_Command);
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication = new DynamicCheckBoxList("SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication");
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.NoRecordsFoundMessage = _GlobalResources.NoCoursesFound;
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.SearchButton.Command += new CommandEventHandler(this._SearchSelectLearningPathsButton_Command);
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectLearningPathsButton_Command);
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataSource = this._EligibleLearningPathsForCheckBoxList;
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataTextField = "name";
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataValueField = "idLearningPath";
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataBound += this._SelectLearningPathsModal_RowDataBound;
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataBind();

            // add controls to body
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.AddControlToBody(this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication);

            // add modal to container
            this.RuleSetLearningPathEnrollmentPropertiesTabPanelsContainer.Controls.Add(this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication);
        }
        #endregion

        #region _SelectLearningPathsModal_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the "Select Learning Path(s)" searchable checkboxlist control.
        /// </summary>
        public void _SelectLearningPathsModal_RowDataBound(Object sender, EventArgs e)
        {
            // create hyperlink for those courses that the ruleset enrollment has already been replicated to and is synced with
            foreach (DataRow row in this._EligibleLearningPathsForCheckBoxList.Rows)
            {
                if (!Convert.IsDBNull(row["idParentRuleSetLearningPathEnrollment"]))
                {
                    if (Convert.ToInt32(row["idParentRuleSetLearningPathEnrollment"]) == this._RuleSetLearningPathEnrollmentObject.Id)
                    {
                        ListItem learningPathName = this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.Items.FindByValue(row["idLearningPath"].ToString());
                        learningPathName.Selected = true;

                        learningPathName.Text = "<a href = 'Modify.aspx?lpid=" + row["idLearningPath"].ToString() + "&id=" + row["idRuleSetLearningPathEnrollment"].ToString() + "'>" + row["name"].ToString();
                    }
                }

            }
        }
        #endregion

        #region _SearchSelectLearningPathsButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Learning Path(s)" searchable checkboxlist control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectLearningPathsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.ClearFeedback();

            // clear the checkboxlist control
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.Items.Clear();

            // do the search
            this._EligibleLearningPathsForCheckBoxList = RuleSetLearningPathEnrollment.IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList(this._RuleSetLearningPathEnrollmentObject.Id, this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.SearchTextBox.Text);

            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataSource = this._EligibleLearningPathsForCheckBoxList;
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataTextField = "name";
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataValueField = "idLearningPath";
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataBound += this._SelectLearningPathsModal_RowDataBound;
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataBind();

            // if no records are available then disable the submit button
            if (this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.Items.Count == 0)
            {
                this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButton.Enabled = false;
                this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
            else
            {
                this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButton.Enabled = true;
                this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButton.CssClass = "Button ActionButton";
            }
        }
        #endregion

        #region _ClearSearchSelectLearningPathsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Learning Path(s)" searchable checkboxlist control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectLearningPathsButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.ClearFeedback();

            // clear the checkboxlist control and search text box
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.Items.Clear();
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.SearchTextBox.Text = "";

            // clear the search
            this._EligibleLearningPathsForCheckBoxList = RuleSetLearningPathEnrollment.IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList(this._RuleSetLearningPathEnrollmentObject.Id, null);

            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataSource = this._EligibleLearningPathsForCheckBoxList;
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataTextField = "name";
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataValueField = "idLearningPath";
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataBound += this._SelectLearningPathsModal_RowDataBound;
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataBind();

            // if records available then enable the submit button
            if (this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.Items.Count > 0)
            {
                this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButton.Enabled = true;
                this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButton.CssClass = "Button ActionButton";
            }
            else
            {
                this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButton.Enabled = false;
                this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            }
        }
        #endregion

        #region _ReplicateToLearningPathsSelectLearningPathsButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Learning Path(s)" searchable checkboxlist control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ReplicateToLearningPathsSelectLearningPathsButton_Command(object sender, CommandEventArgs e)
        {
            // delcare data tables to hold selected and not selected courses
            DataTable selectedLearningPaths = new DataTable();
            selectedLearningPaths.Columns.Add("id", typeof(int));

            DataTable notSelectedLearningPaths = new DataTable();
            notSelectedLearningPaths.Columns.Add("id", typeof(int));

            List<string> selectedValues = new List<string>();
            selectedValues = this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.GetSelectedValues();

            List<string> notSelectedValues = new List<string>();
            notSelectedValues = this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.GetNotSelectedValues();

            // put ids into datatable 
            foreach (string learningPathId in selectedValues)
            {
                selectedLearningPaths.Rows.Add(Convert.ToInt32(learningPathId));
            }

            foreach (string learningPathId in notSelectedValues)
            {
                notSelectedLearningPaths.Rows.Add(Convert.ToInt32(learningPathId));
            }

            RuleSetLearningPathEnrollment.Replicate(this._RuleSetLearningPathEnrollmentObject.Id, selectedLearningPaths, notSelectedLearningPaths);

            this._SelectLearningPathsForRuleSetLearningPathEnrollmentReplication.DisplayFeedback(_GlobalResources.RuleSetEnrollmentReplicated, false);

            this._EligibleLearningPathsForCheckBoxList = RuleSetLearningPathEnrollment.IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList(this._RuleSetLearningPathEnrollmentObject.Id, null);

            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataSource = this._EligibleLearningPathsForCheckBoxList;
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataTextField = "name";
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataValueField = "idLearningPath";
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataBound += this._SelectLearningPathsModal_RowDataBound;
            this.__SelectEligibleLearningPathsForRuleSetLearningPathEnrollmentReplication.CheckBoxListControl.DataBind();
        }
        #endregion

        #region _BuildRuleSetLearningPathEnrollmentPropertiesFormRulesetsPanel
        /// <summary>
        /// Build rule set enrollment properties form rulesets panel
        /// </summary>
        private void _BuildRuleSetLearningPathEnrollmentPropertiesFormRulesetsPanel()
        {
            ObjectOptionsPanel.Controls.Clear();

            this._RuleSetsPanel = new Panel();
            this._RuleSetsPanel.ID = "RuleSetLearningPathEnrollmentProperties_" + "RuleSets" + "_TabPanel";
            this._RuleSetsPanel.Attributes.Add("style", "display: none;");

            UpdatePanel ruleSetsUpdatePanel = new UpdatePanel();
            ruleSetsUpdatePanel.ID = "RuleSetsUpdatePanel";

            Panel ruleSetsOptionsPanel = new Panel();
            ruleSetsOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD RULESET
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddRuleSetLink",
                                                null,
                                                "javascript: void(0);",
                                                "OnRuleSetModifyClick(0); return false;",
                                                _GlobalResources.NewRuleset,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            ruleSetsOptionsPanel.Controls.Add(optionsPanelLinksContainer);

            this._AddModifyRuleSetHiddenButton.ClientIDMode = ClientIDMode.Static;
            this._AddModifyRuleSetHiddenButton.ID = "AddModifyRuleSetHiddenButton";
            this._AddModifyRuleSetHiddenButton.Style.Add("display", "none");

            this._BuildAddModifyRuleSetModal(this._AddModifyRuleSetHiddenButton.ID, this.ObjectOptionsPanel);

            this._IdRulesetEnrollmentHiddenField = new HiddenField();
            this._IdRulesetEnrollmentHiddenField.ID = "IdRulesetHiddenField";
            this._IdRulesetEnrollmentHiddenField.Value = Convert.ToString(this._RuleSetLearningPathEnrollmentObject.Id);

            this.ObjectOptionsPanel.Controls.Add(this._IdRulesetEnrollmentHiddenField);
            this.ObjectOptionsPanel.Controls.Add(_AddModifyRuleSetHiddenButton);

            ruleSetsUpdatePanel.ContentTemplateContainer.Controls.Add(ruleSetsOptionsPanel);

            // GRID

            this._LearningPathRuleSetsGrid = new Grid();
            this._LearningPathRuleSetsGrid.ID = "RuleSetsGrid";

            this._LearningPathRuleSetsGrid.ShowSearchBox = false;
            this._LearningPathRuleSetsGrid.StoredProcedure = Library.RuleSet.GridProcedureForRuleSetLearningPathEnrollment;
            this._LearningPathRuleSetsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this._LearningPathRuleSetsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this._LearningPathRuleSetsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this._LearningPathRuleSetsGrid.AddFilter("@idRuleSetLearningPathEnrollment", SqlDbType.Int, 4, this._RuleSetLearningPathEnrollmentObject.Id);
            this._LearningPathRuleSetsGrid.IdentifierField = "idRuleSet";
            this._LearningPathRuleSetsGrid.DefaultSortColumn = "label";

            // data key names
            this._LearningPathRuleSetsGrid.DataKeyNames = new string[] { "idRuleSet" };

            // columns
            GridColumn label = new GridColumn(_GlobalResources.Label, null); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this._LearningPathRuleSetsGrid.AddColumn(label);

            // add row data bound event
            this._LearningPathRuleSetsGrid.RowDataBound += new GridViewRowEventHandler(this._RuleSetsGrid_RowDataBound);

            // attach the grid to the update panel
            ruleSetsUpdatePanel.ContentTemplateContainer.Controls.Add(this._LearningPathRuleSetsGrid);

            this._ModifyRulesetId = new HiddenField();
            this._ModifyRulesetId.ID = "ModifyRulesetId";
            this._ModifyRulesetId.Value = this.QueryStringString("rsid", String.Empty);
            this._RuleSetsPanel.Controls.Add(this._ModifyRulesetId);

            // bind the grid
            this._LearningPathRuleSetsGrid.BindData();
            this._RuleSetsPanel.Controls.Add(ruleSetsUpdatePanel);

            this.RuleSetLearningPathEnrollmentPropertiesTabPanelsContainer.Controls.Add(this._RuleSetsPanel);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildRulesetGridActionsPanel()
        {
            this._RuleSetGridActionsPanel = new Panel();
            this._RuleSetGridActionsPanel.ID = "RuleSetGridActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedRuleset_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this._RuleSetGridActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _RuleSetsGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the rulesets grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RuleSetsGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idRuleSet = Convert.ToInt32(rowView["idRuleSet"]);

                // AVATAR, LABEL

                string label = rowView["label"].ToString();

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = label;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // label
                Label labelLabelWrapper = new Label();
                labelLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(labelLabelWrapper);

                HyperLink labelLink = new HyperLink();
                labelLink.NavigateUrl = "javascript: void(0);";
                labelLink.Attributes.Add("onclick", "OnRuleSetModifyClick(" + idRuleSet.ToString() + ");");
                labelLabelWrapper.Controls.Add(labelLink);

                Literal labelLabel = new Literal();
                labelLabel.Text = label;
                labelLink.Controls.Add(labelLabel);
            }
        }
        #endregion

        #region _BuildRulesetGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildRulesetGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmActionModal");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedRuleset_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseRuleset_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this._RuleSetGridActionsPanel.Controls.Add(this._GridConfirmAction);

            this._RuleSetsPanel.Controls.Add(this._RuleSetGridActionsPanel);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._LearningPathRuleSetsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._LearningPathRuleSetsGrid.Rows[i].FindControl(this._LearningPathRuleSetsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.RuleSet.Delete(recordsToDelete);

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, _GlobalResources.TheSelectedRuleset_sHaveBeenDeletedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this._LearningPathRuleSetsGrid.BindData();

                // clear the learning path object menu
                this.LearningPathObjectMenuContainer.Controls.Clear();

                // rebuild the controls
                this._BuildControls();
            }
        }
        #endregion        

        #region _BuildAddModifyRuleSetModal
        /// <summary>
        /// Builds Create/Modify ruleset modal
        /// </summary>
        private void _BuildAddModifyRuleSetModal(string targetControlId, Panel modalContainer)
        {
            EnsureChildControls();
            this._AddModifyRuleSetModal = new ModalPopup("AddModifyRuleSetModal");
            this._AddModifyRuleSetModal.ID = "AddModifyRuleSetModal";
            this._AddModifyRuleSetModal.CssClass += " AddModifyRuleSetModal";
            this._AddModifyRuleSetModal.Type = ModalPopupType.Form;
            this._AddModifyRuleSetModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT,
                                                                                ImageFiles.EXT_PNG);
            this._AddModifyRuleSetModal.HeaderIconAlt = _GlobalResources.ModifyRuleset;
            this._AddModifyRuleSetModal.HeaderText = _GlobalResources.ModifyRuleset;

            this._AddModifyRuleSetModal.SubmitButton.Visible = false;
            this._AddModifyRuleSetModal.CloseButton.Visible = false;

            //adding custom  own submit button to cahnge text 
            this._SubmitButtonMadal = new Button();
            this._SubmitButtonMadal.Text = _GlobalResources.CreateRuleset;
            this._SubmitButtonMadal.OnClientClick = "getRules(this,'" + this._RulesData.ClientID + "')";
            this._SubmitButtonMadal.Command += new CommandEventHandler(this._AddModifyRuleSetModalSubmitButton_Command);
            this._SubmitButtonMadal.CssClass = "Button ActionButton";

            //adding custom  own close button to cahnge text 
            this._CloseButtonMadal = new Button();
            this._CloseButtonMadal.Text = _GlobalResources.Close;
            this._CloseButtonMadal.CssClass = "Button NonActionButton";
            this._CloseButtonMadal.OnClientClick = "HideModalPopup();return false;";

            this._AddModifyRuleSetModal.TargetControlID = targetControlId;
            this._AddModifyRuleSetModal.ReloadPageOnClose = false;

            //adding maintenance submit button to simulate button click.  
            this._AddModifyRuleSetModalButton.ClientIDMode = ClientIDMode.Static;
            this._AddModifyRuleSetModalButton.ID = "ModifyRuleSetModalButton";
            this._AddModifyRuleSetModalButton.Command += new CommandEventHandler(this._AddModifyRulesetModalButton_Command);
            this._AddModifyRuleSetModalButton.Style.Add("display", "none");

            this._RuleSetModalPropertiesContainer = new Panel();
            this._RuleSetModalPropertiesContainer.ID = "RuleSetModalPropertiesContainer";

            //clear controls from container
            this._RuleSetModalPropertiesContainer.Controls.Add(this._AddModifyRuleSetModalButton);

            // add hidden field for ruleset id
            this._IdRulesetModalHidden.ClientIDMode = ClientIDMode.Static;
            this._IdRulesetModalHidden.ID = "IdRulesetModalHidden";
            this._RuleSetModalPropertiesContainer.Controls.Add(this._IdRulesetModalHidden);

            // name container

            this._LabelTextbox.ID = "Label_Field";
            this._LabelTextbox.CssClass = "InputMedium";

            this._RuleSetModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Label",
                                                             _GlobalResources.Label,
                                                             this._LabelTextbox.ID,
                                                             this._LabelTextbox,
                                                             true,
                                                             true,
                                                             true));
         
            // Match any/all field
            Panel matchAnyFieldInputContainer = new Panel();
            matchAnyFieldInputContainer.ID = "MatchAnyInputContainer";

            this._MatchAny = new RadioButton();
            this._MatchAny.ID = "MatchAnyRadioButton";
            this._MatchAny.Text = _GlobalResources.MatchAny;
            this._MatchAny.Checked = true;
            this._MatchAny.GroupName = "matchAny";

            this._MatchAll = new RadioButton();
            this._MatchAll.ID = "MatchAllRadioButton";
            this._MatchAll.Text = _GlobalResources.MatchAll;
            this._MatchAll.GroupName = "matchAny";

            matchAnyFieldInputContainer.Controls.Add(this._MatchAny);
            matchAnyFieldInputContainer.Controls.Add(this._MatchAll);

            this._RuleSetModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("MatchAny",
                                                             _GlobalResources.Match,
                                                             matchAnyFieldInputContainer.ID,
                                                             matchAnyFieldInputContainer,
                                                             false,
                                                             true,
                                                             false));

            // rules field
            this._RuleSetModalPropertiesContainer.Controls.Add(AsentiaPage.BuildFormField("Rule",
                                                             _GlobalResources.Rules,
                                                             this._RuleSetControlPlaceHolder.ID,
                                                             this._RuleSetControlPlaceHolder,
                                                             true,
                                                             true,
                                                             false));

            this._RuleSetModalPropertiesContainer.Controls.Add(this._RulesData);

            this._AddModifyRuleSetModal.AddControlToBody(this._RuleSetModalPropertiesContainer);
            this._AddModifyRuleSetModal.AddControlToBody(this._SubmitButtonMadal);
            this._AddModifyRuleSetModal.AddControlToBody(this._CloseButtonMadal);

            modalContainer.Controls.Add(this._AddModifyRuleSetModal);

            EnsureChildControls();

            // POPULATE THE FORM INPUT ELEMENTS
            this._PopulatePropertiesInputElements();
        }
        #endregion

        #region _AddmodifyRulesetModalButton_Command
        /// <summary>
        /// Handles "Add/Modify" ruleset madal hidden button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _AddModifyRulesetModalButton_Command(object sender, CommandEventArgs e)
        {
            this._AddModifyRuleSetModal.ClearFeedback();
            int idRuleSet = Convert.ToInt32(this._IdRulesetModalHidden.Value);
            if (!this._RuleSetControlLoaded)
            {
                if (idRuleSet > 0)
                {
                    this._RuleSetObject = new RuleSet(idRuleSet);
                    this._AutoJoinRuleSet = new AutoJoinRuleSet(idRuleSet);
                    this._SubmitButtonMadal.Text = _GlobalResources.SaveChanges;
                    this._AddModifyRuleSetModal.HeaderText = _GlobalResources.ModifyRuleset;
                }
                else
                {
                    this._RuleSetObject = new RuleSet();
                    this._AutoJoinRuleSet = new AutoJoinRuleSet();
                    this._SubmitButtonMadal.Text = _GlobalResources.CreateRuleset;
                    this._AddModifyRuleSetModal.HeaderText = _GlobalResources.NewRuleset;
                }
                this._AutoJoinRuleSet.ID = "Rule_Field";
                this._AutoJoinRuleSet.ClientIDMode = ClientIDMode.Static;
                this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
                EnsureChildControls();
                this._PopulatePropertiesInputElements();
            }

        }
        #endregion

        #region _AddModifyRuleSetModalSubmitButton_Command
        /// <summary>
        /// Handles "Add/Modify" ruleset madal submit button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"> </param>
        private void _AddModifyRuleSetModalSubmitButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable rulesData = new DataTable();

                if (!String.IsNullOrWhiteSpace(this._RulesData.Value))
                {
                    rulesData.Columns.Add("idRule", typeof(int));
                    rulesData.Columns.Add("userField", typeof(string));
                    rulesData.Columns.Add("operator", typeof(string));
                    rulesData.Columns.Add("textValue", typeof(string));

                    List<RuleObject> allRules = new JavaScriptSerializer().Deserialize<List<RuleObject>>(this._RulesData.Value);
                    foreach (RuleObject rule in allRules)
                    {
                        int idRule = 0;
                        int.TryParse(rule.IdRule, out idRule);
                        rulesData.Rows.Add(idRule, rule.UserField, rule.Operator, rule.Value);
                    }

                    if (rulesData.Rows.Count > 0)
                    {
                        this._AutoJoinRuleSet = new AutoJoinRuleSet(rulesData);
                        this._AutoJoinRuleSet.ID = "Rule_Field";
                        this._RuleSetControlPlaceHolder.Controls.Add(this._AutoJoinRuleSet);
                        this._RuleSetControlLoaded = true;
                    }

                    this._RulesData.Value = string.Empty;
                }
                int idRuleSet = Convert.ToInt32(this._IdRulesetModalHidden.Value);

                if (idRuleSet > 0)
                    this._RuleSetObject = new RuleSet(idRuleSet);

                // if there is no rule set object, create one
                if (this._RuleSetObject == null)
                { this._RuleSetObject = new RuleSet(); }

                // validate the form
                if (!this._ValidateRuleSetModalPropertiesForm())
                { throw new AsentiaException(); }

                // if this is an existing rule set, and not the default language, save language properties only
                // otherwise, save the whole object in its default language
                int id;

                // populate the object
                this._RuleSetObject.Label = this._LabelTextbox.Text;

                if (_MatchAny.Checked)
                {
                    this._RuleSetObject.IsAny = true;
                }
                else
                {
                    this._RuleSetObject.IsAny = false;
                }

                // save the user, save its returned id to viewstate, and 
                // instansiate a new user object with the id

                this._RuleSetObject.LinkedObjectId = this._RuleSetLearningPathEnrollmentObject.Id;
                this._RuleSetObject.LinkedObjectType = RuleSetLinkedObjectType.RuleSetLearningPathEnrollment;

                id = this._RuleSetObject.Save();

                Asentia.LMS.Library.Rule ruleob = new Asentia.LMS.Library.Rule();
                ruleob.IdRuleSet = id;
                if (this._Rules.Rows.Count > 0)
                {
                    ruleob.Save(this._Rules);
                }

                this._IdRulesetModalHidden.Value = id.ToString();
                this._RuleSetObject.Id = id;

                // load the saved user object
                this._RuleSetObject = new RuleSet(id);

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string ruleSetTitle = null;

                        // get text boxes
                        TextBox languageSpecificRuleSetTitleTextBox = (TextBox)this._RuleSetModalPropertiesContainer.FindControl(this._LabelTextbox.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificRuleSetTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificRuleSetTitleTextBox.Text))
                            { ruleSetTitle = languageSpecificRuleSetTitleTextBox.Text; }
                        }

                        // save the property if property is populated
                        if (!String.IsNullOrWhiteSpace(ruleSetTitle))
                        {
                            this._RuleSetObject.SaveLang(cultureInfo.Name,
                                                        ruleSetTitle);
                        }
                    }
                }

                /* REMOVED IN FAVOR OF GLOBAL WAY TO ASSIGN ENROLLMENTS
                // ASSIGN RULESET ENROLLMENTS FOR THE COURSE THIS SAVE AFFECTS

                // build DataTable for Course filter
                DataTable assignRuleSetEnrollmentsCourseFilter = new DataTable();
                assignRuleSetEnrollmentsCourseFilter.Columns.Add("id", typeof(int));
                assignRuleSetEnrollmentsCourseFilter.Rows.Add(this._RuleSetLearningPathEnrollmentObject.IdLearningPath);

                // build DataTable for "Filters" filter - even though it will be empty, it is still required to be passed
                DataTable assignRuleSetEnrollmentsFiltersFilter = new DataTable();
                assignRuleSetEnrollmentsFiltersFilter.Columns.Add("id", typeof(int));

                RuleSetLearningPathEnrollment.AssignEnrollments(assignRuleSetEnrollmentsCourseFilter, assignRuleSetEnrollmentsFiltersFilter, null);
                */

                // rebind the grid and other controls;
                //this._BuildControls();
                this._LearningPathRuleSetsGrid.BindData();
                this._SubmitButtonMadal.Text = _GlobalResources.SaveChanges;

                // display the saved feedback
                this._AddModifyRuleSetModal.DisplayFeedback(_GlobalResources.RulesetPropertiesHaveBeenSavedSuccessfully, false);

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException dfnuEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dfnuEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._AddModifyRuleSetModal.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ValidateRuleSetModalPropertiesForm
        /// <summary>
        /// Validates the properties form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateRuleSetModalPropertiesForm()
        {
            bool isValid = true;

            // name field
            if (String.IsNullOrWhiteSpace(this._LabelTextbox.Text.Trim()))
            {
             
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "Label", _GlobalResources.Label + " " + _GlobalResources.IsRequired);
            }

            // rules control
            if (this._AutoJoinRuleSet != null)
            {
                this._Rules = this._AutoJoinRuleSet.GetRulesAfterValidatingData();
                if (this._Rules == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "Rule", _GlobalResources.OneOrMoreRulesAreInvalidHoverOverField_sToViewSpecificError_s);
                }
                else if (this._Rules.Rows.Count < 1)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this._RuleSetModalPropertiesContainer, "Rule", _GlobalResources.YouMustHaveAtLeastOneRule);
                }
            }

            return isValid;
        }
        #endregion

        #region _BuildRuleSetLearningPathEnrollmentPropertiesActionsPanel
        /// <summary>
        /// Builds the container and buttons for ruleset enrollment properties actions.
        /// </summary>
        private void _BuildRuleSetLearningPathEnrollmentPropertiesActionsPanel()
        {
            this.RuleSetLearningPathEnrollmentPropertiesActionsPanel = new Panel();
            this.RuleSetLearningPathEnrollmentPropertiesActionsPanel.ID = "RuleSetLearningPathEnrollmentPropertiesActionsPanel";

            // clear controls from container
            this.RuleSetLearningPathEnrollmentPropertiesActionsPanel.Controls.Clear();

            // style actions panel
            this.RuleSetLearningPathEnrollmentPropertiesActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._RuleSetLearningPathEnrollmentPropertiesSaveButton = new Button();
            this._RuleSetLearningPathEnrollmentPropertiesSaveButton.ID = "RuleSetLearningPathEnrollmentSaveButton";
            this._RuleSetLearningPathEnrollmentPropertiesSaveButton.CssClass = "Button ActionButton SaveButton";

            // if the object is null, it's a new object, so make button text say "Create"
            if (this._RuleSetLearningPathEnrollmentObject == null)
            { this._RuleSetLearningPathEnrollmentPropertiesSaveButton.Text = _GlobalResources.CreateRulesetEnrollment; }
            else
            { this._RuleSetLearningPathEnrollmentPropertiesSaveButton.Text = _GlobalResources.SaveChanges; }

            this._RuleSetLearningPathEnrollmentPropertiesSaveButton.Command += new CommandEventHandler(this._RuleSetLearningPathEnrollmentPropertiesSaveButton_Command);
            this.RuleSetLearningPathEnrollmentPropertiesActionsPanel.Controls.Add(this._RuleSetLearningPathEnrollmentPropertiesSaveButton);

            // cancel button
            this._RuleSetLearningPathEnrollmentPropertiesCancelButton = new Button();
            this._RuleSetLearningPathEnrollmentPropertiesCancelButton.ID = "CancelButton";
            this._RuleSetLearningPathEnrollmentPropertiesCancelButton.CssClass = "Button NonActionButton";
            this._RuleSetLearningPathEnrollmentPropertiesCancelButton.Text = _GlobalResources.Cancel;
            this._RuleSetLearningPathEnrollmentPropertiesCancelButton.Command += new CommandEventHandler(this._RuleSetLearningPathEnrollmentPropertiesCancelButton_Command);
            this.RuleSetLearningPathEnrollmentPropertiesActionsPanel.Controls.Add(this._RuleSetLearningPathEnrollmentPropertiesCancelButton);

            Panel propertiesFormPanel = (Panel)this.RuleSetLearningPathEnrollmentPropertiesContainer.FindControl("RuleSetLearningPathEnrollmentProperties_" + "Properties" + "_TabPanel");
            propertiesFormPanel.Controls.Add(this.RuleSetLearningPathEnrollmentPropertiesActionsPanel);
        }
        #endregion

        #region _PopulateRuleSetLearningPathEnrollmentPropertiesInputElements
        /// <summary>
        /// Populates the input elements in the properties panel with values from the object.
        /// </summary>
        private void _PopulateRuleSetLearningPathEnrollmentPropertiesInputElements()
        {
            if (this._RuleSetLearningPathEnrollmentObject != null)
            {
                // LANGUAGE SPECIFIC PROPERTIES

                // label
                bool isDefaultPopulated = false;

                foreach (RuleSetLearningPathEnrollment.LanguageSpecificProperty ruleSetEnrollmentLanguageSpecificProperty in this._RuleSetLearningPathEnrollmentObject.LanguageSpecificProperties)
                {
                    // if the language is the default language, populate the control directly attached to this page,
                    // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                    // it; note that if we cannot populate the controls directly attached to this page (default) from
                    // language-specific properties, we will use the values in the properties that come from the base table
                    if (ruleSetEnrollmentLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        this._Label.Text = ruleSetEnrollmentLanguageSpecificProperty.Label;

                        isDefaultPopulated = true;
                    }
                    else
                    {
                        // get text boxes
                        TextBox languageSpecificRuleSetEnrollmentLabelTextBox = (TextBox)this.RuleSetLearningPathEnrollmentPropertiesContainer.FindControl(this._Label.ID + "_" + ruleSetEnrollmentLanguageSpecificProperty.LangString);

                        // if the text boxes were found, set the text box values to the language-specific value
                        if (languageSpecificRuleSetEnrollmentLabelTextBox != null)
                        { languageSpecificRuleSetEnrollmentLabelTextBox.Text = ruleSetEnrollmentLanguageSpecificProperty.Label; }
                    }
                }

                if (!isDefaultPopulated)
                {
                    this._Label.Text = this._RuleSetLearningPathEnrollmentObject.Label;
                }

                // NON-LANGUAGE SPECIFIC PROPERTIES

                // timezone
                this._Timezone.SelectedValue = Convert.ToString(this._RuleSetLearningPathEnrollmentObject.IdTimezone);

                // date start
                if (!this._IsStartDatePassed)
                { this._DtStart.Value = TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetLearningPathEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetLearningPathEnrollmentObject.IdTimezone).dotNetName)); }

                // date end
                if (this._RuleSetLearningPathEnrollmentObject.DtEnd != null)
                {
                    this._DtEnd.NoneCheckBoxChecked = false;
                    this._DtEnd.Value = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._RuleSetLearningPathEnrollmentObject.DtEnd, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetLearningPathEnrollmentObject.IdTimezone).dotNetName));
                }
                else
                {
                    this._DtEnd.NoneCheckBoxChecked = true;
                    this._DtEnd.Value = null;
                }

                if (this._RuleSetLearningPathEnrollmentObject.DelayInterval != null)
                {
                    this._Delay.NoneCheckBoxChecked = false;
                    this._Delay.IntervalValue = this._RuleSetLearningPathEnrollmentObject.DelayInterval.ToString();
                    this._Delay.TimeframeValue = this._RuleSetLearningPathEnrollmentObject.DelayTimeframe;
                }
                else
                {
                    this._Delay.NoneCheckBoxChecked = true;
                    this._Delay.IntervalValue = null;
                    this._Delay.TimeframeValue = null;
                }

                // due
                if (this._RuleSetLearningPathEnrollmentObject.DueInterval != null)
                {
                    this._Due.NoneCheckBoxChecked = false;
                    this._Due.IntervalValue = this._RuleSetLearningPathEnrollmentObject.DueInterval.ToString();
                    this._Due.TimeframeValue = this._RuleSetLearningPathEnrollmentObject.DueTimeframe;
                }
                else
                {
                    this._Due.NoneCheckBoxChecked = true;
                    this._Due.IntervalValue = null;
                    this._Due.TimeframeValue = null;
                }

                // expires from start
                if (this._RuleSetLearningPathEnrollmentObject.ExpiresFromStartInterval != null)
                {
                    this._ExpiresFromStart.NoneCheckBoxChecked = false;
                    this._ExpiresFromStart.IntervalValue = this._RuleSetLearningPathEnrollmentObject.ExpiresFromStartInterval.ToString();
                    this._ExpiresFromStart.TimeframeValue = this._RuleSetLearningPathEnrollmentObject.ExpiresFromStartTimeframe;
                }
                else
                {
                    this._ExpiresFromStart.NoneCheckBoxChecked = true;
                    this._ExpiresFromStart.IntervalValue = null;
                    this._ExpiresFromStart.TimeframeValue = null;
                }

                // expires from first launch
                if (this._RuleSetLearningPathEnrollmentObject.ExpiresFromFirstLaunchInterval != null)
                {
                    this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = false;
                    this._ExpiresFromFirstLaunch.IntervalValue = this._RuleSetLearningPathEnrollmentObject.ExpiresFromFirstLaunchInterval.ToString();
                    this._ExpiresFromFirstLaunch.TimeframeValue = this._RuleSetLearningPathEnrollmentObject.ExpiresFromFirstLaunchTimeframe;
                }
                else
                {
                    this._ExpiresFromFirstLaunch.NoneCheckBoxChecked = true;
                    this._ExpiresFromFirstLaunch.IntervalValue = null;
                    this._ExpiresFromFirstLaunch.TimeframeValue = null;
                }
            }
        }
        #endregion

        #region _ValidateRuleSetLearningPathEnrollmentPropertiesForm
        /// <summary>
        /// Validates learning path ruleset enrollment form properties
        /// </summary>
        /// <returns></returns>
        private bool _ValidateRuleSetLearningPathEnrollmentPropertiesForm()
        {
            bool isValid = true;
            bool propertiesTabHasErrors = false;
         
            /* BASIC VALIDATIONS */

            // label field - default language required
            if (String.IsNullOrWhiteSpace(this._Label.Text))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentLabel", _GlobalResources.Label + " " + _GlobalResources.IsRequired);
            }

            // is locked by prerequisites - no validation needed

            // timezone - always required, must be a number, and if not valid, its a showstopper, which means return right away
            if (String.IsNullOrWhiteSpace(this._Timezone.SelectedValue))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentTimezone", _GlobalResources.LifespanTimezone + " " + _GlobalResources.IsRequired);
                return isValid;
            }

            int idTimezone;
            if (!int.TryParse(this._Timezone.SelectedValue, out idTimezone))
            {
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentTimezone", _GlobalResources.LifespanTimezone + " " + _GlobalResources.IsInvalid);
                return isValid;
            }

            // get the timezone's "dotnetname" from a timezone object.
            string tzDotNetName = new Timezone(Convert.ToInt32(idTimezone)).dotNetName;

            // date start - required, if its not specified its a showstopper, which means return right away
            // only validate if start has not passed
            if (!this._IsStartDatePassed)
            {
                if (this._DtStart.Value == null)
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentDtStart", _GlobalResources.LifespanStart + " " + _GlobalResources.IsRequired);
                    return isValid;
                }
            }

            // date end - if the "none" checkbox is not checked, ensure a valid date has been entered, showstopper, return immeadiately
            if (!this._DtEnd.NoneCheckBoxChecked && this._DtEnd.Value == null)
            {                
                propertiesTabHasErrors = true;
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentDtEnd", _GlobalResources.LifespanEnd + " " + _GlobalResources.IsInvalid);
                return isValid;
            }

            // delay - only exists for relative dates, never required, interval must be integer
            if (!this._Delay.NoneCheckBoxChecked)
            {
                int delayInterval;
                if (String.IsNullOrWhiteSpace(this._Delay.IntervalValue) || !int.TryParse(this._Delay.IntervalValue, out delayInterval))
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentDelay", _GlobalResources.Delay + " " + _GlobalResources.IsInvalid);
                }
            }

            // due - always exists, only required for recurring, interval must be integer
            if (!this._Due.NoneCheckBoxChecked)
            {
                int dueInterval;
                if (String.IsNullOrWhiteSpace(this._Due.IntervalValue) || !int.TryParse(this._Due.IntervalValue, out dueInterval))
                {
                    propertiesTabHasErrors = true;
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.IsInvalid);
                }
            }

            // access from start - always exists, never required, interval must be integer
            if (!this._ExpiresFromStart.NoneCheckBoxChecked)
            {
                int expiresFromStartInterval;
                if (String.IsNullOrWhiteSpace(this._ExpiresFromStart.IntervalValue) || !int.TryParse(this._ExpiresFromStart.IntervalValue, out expiresFromStartInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetEnrollmentExpiresFromStart", _GlobalResources.AccessFromStart + " " + _GlobalResources.IsInvalid);
                }
            }
            

            // access from first launch - always exists, never required, interval must be integer
            if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
            {
                int expiresFromFirstLaunchInterval;
                if (String.IsNullOrWhiteSpace(this._ExpiresFromFirstLaunch.IntervalValue) || !int.TryParse(this._ExpiresFromFirstLaunch.IntervalValue, out expiresFromFirstLaunchInterval))
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetEnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.IsInvalid);
                }
            }

            /* CONSTRAINT VALIDATIONS BASED ON TYPE */

            // only apply constraint validations if the other validations have passed
            if (isValid)
            {
                // date start - must be in future, only validate if start has not passed
                DateTime dtStart;

                if (!this._IsStartDatePassed)
                {
                    if (this._DtStart.NowCheckBoxChecked)
                    { dtStart = DateTime.UtcNow.AddMinutes(1); }
                    else
                    { dtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName)); }

                    if (dtStart < AsentiaSessionState.UtcNow)
                    {
                        propertiesTabHasErrors = true;
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentDtStart", _GlobalResources.LifespanStart + " " + _GlobalResources.MustBeInFuture);
                    }
                }
                else // this assumes that the RuleSetLearningPathEnrollment object exists and is populated, otherwise, this couldn't be fired due to logic in other places
                {
                    // if applying the the selected timezone to the unchanged original date start results in a
                    // new start date that has already passed, fail
                    // this only needs to be done when the timezone has been modified

                    // this is utc
                    dtStart = this._RuleSetLearningPathEnrollmentObject.DtStart;

                    if (this._RuleSetLearningPathEnrollmentObject.IdTimezone != Convert.ToInt32(this._Timezone.SelectedValue))
                    {
                        // convert start back to local (from original timezone)
                        DateTime originalLocalDtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetLearningPathEnrollmentObject.IdTimezone).dotNetName));

                        // convert local start to new utc
                        dtStart = TimeZoneInfo.ConvertTimeToUtc(originalLocalDtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(Convert.ToInt32(this._Timezone.SelectedValue)).dotNetName));

                        if (dtStart <= AsentiaSessionState.UtcNow)
                        {
                            propertiesTabHasErrors = true;
                            isValid = false;
                            Timezone selectedTimezone = new Timezone(Convert.ToInt32(this._Timezone.SelectedValue));
                            string currentAdjustedStartValue = TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetLearningPathEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(selectedTimezone.dotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern)
                                                               + " @ "
                                                               + TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetLearningPathEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(selectedTimezone.dotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortTimePattern)
                                                               + " "
                                                               + selectedTimezone.displayName;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentDtStart", _GlobalResources.LifespanStart + " " + String.Format(_GlobalResources.CannotBeMovedEarlierThanCurrentValue, currentAdjustedStartValue));
                        }
                    }
                }

                // date end - if set, cannot be less than date start
                DateTime? dtEnd = null;

                if (!this._DtEnd.NoneCheckBoxChecked && this._DtEnd.Value != null)
                {
                    dtEnd = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtEnd.Value, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));

                    if (dtEnd <= dtStart)
                    {
                        propertiesTabHasErrors = true;
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentDtEnd", _GlobalResources.LifespanEnd + " " + _GlobalResources.CannotBeBeforeStartDate);
                    }
                }

                // due - interval cannot exceed access from start and access from first launch intervals
                if (!this._Due.NoneCheckBoxChecked)
                {
                    // compare to expires from start interval
                    if (!this._ExpiresFromStart.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtDue;
                        DateTime calculatedDtExpiresFromStart;
                        int dueInterval = Convert.ToInt32(this._Due.IntervalValue);
                        int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                        switch (this._Due.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtDue = dtStart.AddDays(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtDue = dtStart.AddMonths(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtDue = dtStart.AddYears(dueInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtDue = DateTime.MinValue;
                                break;
                        }

                        switch (this._ExpiresFromStart.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromStart = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date due to calculated expires from start
                        if (calculatedDtDue > calculatedDtExpiresFromStart)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromStart);
                        }
                    }

                    // compare to expires from first launch interval
                    if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtDue;
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        int dueInterval = Convert.ToInt32(this._Due.IntervalValue);
                        int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);

                        switch (this._Due.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtDue = dtStart.AddDays(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtDue = dtStart.AddDays(dueInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtDue = dtStart.AddMonths(dueInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtDue = dtStart.AddYears(dueInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtDue = DateTime.MinValue;
                                break;
                        }

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        // compare calculated date due to calculated expires from first launch
                        if (calculatedDtDue > calculatedDtExpiresFromFirstLaunch)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetEnrollmentDue", _GlobalResources.Due + " " + _GlobalResources.CannotExceedAccessFromFirstLaunch);
                        }
                    }
                }

                // expires from first launch - interval cannot exceed expires from start interval
                if (!this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                {
                    // compare to expires from start interval
                    if (!this._ExpiresFromStart.NoneCheckBoxChecked)
                    {
                        // calculate dates from intervals for comparison by adding interval to utcnow
                        DateTime calculatedDtExpiresFromFirstLaunch;
                        DateTime calculatedDtExpiresFromStart;
                        int expiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);
                        int expiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);

                        switch (this._ExpiresFromFirstLaunch.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddDays(expiresFromFirstLaunchInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddMonths(expiresFromFirstLaunchInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromFirstLaunch = dtStart.AddYears(expiresFromFirstLaunchInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromFirstLaunch = DateTime.MinValue;
                                break;
                        }

                        switch (this._ExpiresFromStart.TimeframeValue)
                        {
                            case DateIntervalSelector.INTERVAL_VALUE_DAY:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_WEEK:
                                calculatedDtExpiresFromStart = dtStart.AddDays(expiresFromStartInterval * 7);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_MONTH:
                                calculatedDtExpiresFromStart = dtStart.AddMonths(expiresFromStartInterval);
                                break;
                            case DateIntervalSelector.INTERVAL_VALUE_YEAR:
                                calculatedDtExpiresFromStart = dtStart.AddYears(expiresFromStartInterval);
                                break;
                            default:
                                // will never fire but needed to avoid "unassigned variable" compiler error
                                calculatedDtExpiresFromStart = DateTime.MinValue;
                                break;
                        }

                        // compare calculated expires from first launch to calculated expires from start
                        if (calculatedDtExpiresFromFirstLaunch > calculatedDtExpiresFromStart)
                        {
                            isValid = false;
                            this.ApplyErrorMessageToFieldErrorPanel(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetEnrollmentExpiresFromFirstLaunch", _GlobalResources.AccessFromFirstLaunch + " " + _GlobalResources.CannotExceedAccessFromStart);
                        }
                    }
                }
            }

            // apply error image and class to tabs if they have errors
            if (propertiesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this.RuleSetLearningPathEnrollmentPropertiesContainer, "RuleSetLearningPathEnrollmentProperties_Properties_TabLI"); }

            return isValid;
        }
        #endregion

        #region _RuleSetLearningPathEnrollmentPropertiesSaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click for ruleset enrollment properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RuleSetLearningPathEnrollmentPropertiesSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateRuleSetLearningPathEnrollmentPropertiesForm())
                { throw new AsentiaException(); }

                // if there is no ruleset enrollment object, create one
                if (this._RuleSetLearningPathEnrollmentObject == null)
                { this._RuleSetLearningPathEnrollmentObject = new RuleSetLearningPathEnrollment(); }
                
                int id;
                int lpid = this._LearningPathObject.Id;

                // populate the object

                // label
                this._RuleSetLearningPathEnrollmentObject.Label = this._Label.Text;

                // learningPath id
                this._RuleSetLearningPathEnrollmentObject.IdLearningPath = this._LearningPathObject.Id;

                // timezone
                int oldTimezoneId = 0;

                if (this._RuleSetLearningPathEnrollmentObject.IdTimezone > 0)
                { oldTimezoneId = this._RuleSetLearningPathEnrollmentObject.IdTimezone; }

                this._RuleSetLearningPathEnrollmentObject.IdTimezone = Convert.ToInt32(this._Timezone.SelectedValue);

                // date start
                if (!this._IsStartDatePassed)
                {
                    if (this._DtStart.NowCheckBoxChecked)
                    { this._RuleSetLearningPathEnrollmentObject.DtStart = DateTime.UtcNow.AddMinutes(1); }
                    else
                    { this._RuleSetLearningPathEnrollmentObject.DtStart = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtStart.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetLearningPathEnrollmentObject.IdTimezone).dotNetName)); }
                }
                else
                {
                    // if the timezones have changed, convert start to utc from the original date start
                    if (oldTimezoneId > 0 && oldTimezoneId != this._RuleSetLearningPathEnrollmentObject.IdTimezone)
                    {
                        // convert start back to local
                        DateTime originalLocalDtStart = TimeZoneInfo.ConvertTimeFromUtc(this._RuleSetLearningPathEnrollmentObject.DtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(oldTimezoneId).dotNetName));

                        // convert local start to new utc
                        this._RuleSetLearningPathEnrollmentObject.DtStart = TimeZoneInfo.ConvertTimeToUtc(originalLocalDtStart, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetLearningPathEnrollmentObject.IdTimezone).dotNetName));
                    }
                }

                // date end
                if (this._DtEnd.NoneCheckBoxChecked)
                { this._RuleSetLearningPathEnrollmentObject.DtEnd = null; }
                else
                { this._RuleSetLearningPathEnrollmentObject.DtEnd = TimeZoneInfo.ConvertTimeToUtc((DateTime)this._DtEnd.Value, TimeZoneInfo.FindSystemTimeZoneById(new Timezone(this._RuleSetLearningPathEnrollmentObject.IdTimezone).dotNetName)); }

                // delay
                if (!this._Delay.NoneCheckBoxChecked)
                {
                    this._RuleSetLearningPathEnrollmentObject.DelayInterval = Convert.ToInt32(this._Delay.IntervalValue);
                    this._RuleSetLearningPathEnrollmentObject.DelayTimeframe = this._Delay.TimeframeValue;
                }
                else
                {
                    this._RuleSetLearningPathEnrollmentObject.DelayInterval = null;
                    this._RuleSetLearningPathEnrollmentObject.DelayTimeframe = null;
                }

                // due
                if (this._Due.NoneCheckBoxChecked)
                {
                    this._RuleSetLearningPathEnrollmentObject.DueInterval = null;
                    this._RuleSetLearningPathEnrollmentObject.DueTimeframe = null;
                }
                else
                {
                    this._RuleSetLearningPathEnrollmentObject.DueInterval = Convert.ToInt32(this._Due.IntervalValue);
                    this._RuleSetLearningPathEnrollmentObject.DueTimeframe = this._Due.TimeframeValue;
                }

                // expires from start
                if (this._ExpiresFromStart.NoneCheckBoxChecked)
                {
                    this._RuleSetLearningPathEnrollmentObject.ExpiresFromStartInterval = null;
                    this._RuleSetLearningPathEnrollmentObject.ExpiresFromStartTimeframe = null;
                }
                else
                {
                    this._RuleSetLearningPathEnrollmentObject.ExpiresFromStartInterval = Convert.ToInt32(this._ExpiresFromStart.IntervalValue);
                    this._RuleSetLearningPathEnrollmentObject.ExpiresFromStartTimeframe = this._ExpiresFromStart.TimeframeValue;
                }

                // expires from first launch
                if (this._ExpiresFromFirstLaunch.NoneCheckBoxChecked)
                {
                    this._RuleSetLearningPathEnrollmentObject.ExpiresFromFirstLaunchInterval = null;
                    this._RuleSetLearningPathEnrollmentObject.ExpiresFromFirstLaunchTimeframe = null;
                }
                else
                {
                    this._RuleSetLearningPathEnrollmentObject.ExpiresFromFirstLaunchInterval = Convert.ToInt32(this._ExpiresFromFirstLaunch.IntervalValue);
                    this._RuleSetLearningPathEnrollmentObject.ExpiresFromFirstLaunchTimeframe = this._ExpiresFromFirstLaunch.TimeframeValue;
                }


                // save the ruleset enrollment, save its returned id to viewstate, and set the current ruleset learning path enrollment object's id
                id = this._RuleSetLearningPathEnrollmentObject.Save();
                this.ViewState["id"] = id;
                this._RuleSetLearningPathEnrollmentObject.Id = id;

                // do ruleset learning path enrollment language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string ruleSetEnrollmentLabel = null;

                        // get text boxes
                        TextBox languageSpecificRuleSetEnrollmentLabelTextBox = (TextBox)this.RuleSetLearningPathEnrollmentPropertiesContainer.FindControl(this._Label.ID + "_" + cultureInfo.Name);

                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificRuleSetEnrollmentLabelTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificRuleSetEnrollmentLabelTextBox.Text))
                            { ruleSetEnrollmentLabel = languageSpecificRuleSetEnrollmentLabelTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(ruleSetEnrollmentLabel))
                        {
                            this._RuleSetLearningPathEnrollmentObject.SaveLang(cultureInfo.Name,
                                                                               ruleSetEnrollmentLabel);
                        }
                    }
                }                

                // load the saved ruleset enrollment object
                this._RuleSetLearningPathEnrollmentObject = new RuleSetLearningPathEnrollment(id);

                // determine if the start date has passed based on the saved enrollment information
                if (this._RuleSetLearningPathEnrollmentObject.DtStart <= AsentiaSessionState.UtcNow)
                { this._IsStartDatePassed = true; }
                else
                { this._IsStartDatePassed = false; }

                // clear controls for containers that have dynamically added elements
                this.LearningPathObjectMenuContainer.Controls.Clear();

                // build the page controls
                this._BuildControls();

                // display the saved feedback
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, _GlobalResources.RulesetEnrollmentHasBeenSavedSuccessfully, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.RuleSetLearningPathEnrollmentPropertiesFeedbackContainer, _GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _RuleSetLearningPathEnrollmentPropertiesCancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click for RuleSetLearningPathEnrollment Properties.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RuleSetLearningPathEnrollmentPropertiesCancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("Default.aspx?lpid=" + this._LearningPathObject.Id.ToString());
        }
        #endregion
    }
}
