﻿/*
METHOD: OnRuleSetModifyClick
*/
function OnRuleSetModifyClick(IdRuleset) {
    $('#' + IdRulesetModalHidden).val(IdRuleset);

    // click the add modify ruleset hidden link button to show modal
    document.getElementById(AddModifyRuleSetHiddenButton).click();

    if ($('#' + IdRulesetModalHidden).val() == "" || $('#' + IdRulesetModalHidden).val() == "0") {
        $('#AddModifyRuleSetModalModalPopupHeaderText').text(AddRuleSetModalHeader);
    }

    // clear the name field
    $("input[id^='" + RuleSetLabel_Field + "']").val("");

    // fire click for existing Ruleset to Modify else show as new RuleSet
    // click the hidden button to fill the controls on update modal
    document.getElementById(ModifyRuleSetModalButton).click();
    var ruleRows = $("tr.RuleRow");

    if (ruleRows != null && ruleRows.length <= 1) {
        $(".RemoveRuleButton").hide();
    }

    return false;
}

function OnRuleSetModifyLoad(IdRuleset) {
    Helper.ToggleTab('RuleSets', 'LearningPathEnrollmentProperties');

    $('#' + IdRulesetModalHidden).val(IdRuleset);

    if ($('#' + IdRulesetModalHidden).val() == "" || $('#' + IdRulesetModalHidden).val() == "0") {
        $('#AddModifyRuleSetModalModalPopupHeaderText').text(AddRuleSetModalHeader);
    }

    // clear the name field
    $("input[id^='" + RuleSetLabel_Field + "']").val("");

    // fire click for existing Ruleset to Modify else show as new RuleSet
    // click the hidden button to fill the controls on update modal
    document.getElementById(ModifyRuleSetModalButton).click();
    var ruleRows = $("tr.RuleRow");

    if (ruleRows != null && ruleRows.length <= 1) {
        $(".RemoveRuleButton").hide();
    }

    return false;
}

//Hides the ruleset modal
function HideModalPopup() {
    $("#AddModifyRuleSetModal").hide();
    $('#AddModifyRuleSetModalModalPopupExtender_backgroundElement').css("display", "none");

    return false;
}

// Page load method
function pageLoad() {
    if ($('#' + IdRulesetModalHidden).val() == "" || $('#' + IdRulesetModalHidden).val() == "0") {
        $('#AddModifyRuleSetModalModalPopupHeaderText').text(AddRuleSetModalHeader);
    }
}