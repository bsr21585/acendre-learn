﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.LearningPaths.RulesetEnrollments
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel RuleSetEnrollmentsFormContentWrapperContainer;
        public Panel LearningPathObjectMenuContainer;
        public Panel RuleSetEnrollmentsWrapperContainer;
        public Panel ObjectOptionsPanel;
        public Panel RuleSetEnrollmentGridInstructionsPanel;
        public UpdatePanel RuleSetEnrollmentGridUpdatePanel;
        public Grid RuleSetEnrollmentGrid;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private LearningPath _LearningPathObject;

        private HiddenField _RuleSetEnrollmentOrderHiddenField;

        private ModalPopup _GridConfirmAction;
        private LinkButton _DeleteButton;        
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.Courses.RulesetEnrollments.Default.js");            

            // for multiple "start up" scripts, we need to build start up calls
            // and add them to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();
            multipleStartUpCallsScript.AppendLine("Sys.Application.add_load(");
            multipleStartUpCallsScript.AppendLine("function() { ");
            multipleStartUpCallsScript.AppendLine(" // initialize sortable on ruleset enrollment grid");
            multipleStartUpCallsScript.AppendLine(" InitializeSortableOnRuleSetEnrollmentGrid();");  
            multipleStartUpCallsScript.AppendLine("});");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/administrator/learningpaths/rulesetenrollments/Default.css");

            // get the learning path object
            this._GetLearningPathObject();

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();
            
            this.RuleSetEnrollmentsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.RuleSetEnrollmentsWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // build the learning path object menu
            if (this._LearningPathObject != null)
            {
                LearningPathObjectMenu learningPathObjectMenu = new LearningPathObjectMenu(this._LearningPathObject);
                learningPathObjectMenu.SelectedItem = LearningPathObjectMenu.MenuObjectItem.RulesetEnrollments;

                this.LearningPathObjectMenuContainer.Controls.Add(learningPathObjectMenu);
            }

            // instansiate the hidden field for ruleset enrollment order (priority)
            this._RuleSetEnrollmentOrderHiddenField = new HiddenField();
            this._RuleSetEnrollmentOrderHiddenField.ID = "RuleSetEnrollmentOrderHiddenField";
            this._RuleSetEnrollmentOrderHiddenField.Value = "";
            this.ActionsPanel.Controls.Add(this._RuleSetEnrollmentOrderHiddenField);

            // build the grid, actions panel, and modal
            this._BuildObjectOptionsPanel();
            this._BuildGrid();
            this._BuildActionsPanel();
            this._BuildGridActionsModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.RuleSetEnrollmentGrid.BindData();

                // after we got the ruleset enrollment ordering into the input field from the databound event, strip the last character which is a |
                if (!String.IsNullOrWhiteSpace(this._RuleSetEnrollmentOrderHiddenField.Value))
                { this._RuleSetEnrollmentOrderHiddenField.Value = this._RuleSetEnrollmentOrderHiddenField.Value.Substring(0, this._RuleSetEnrollmentOrderHiddenField.Value.Length - 1); }
            }
        }
        #endregion        

        #region _GetLearningPathObject
        /// <summary>
        /// Gets a learning path object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetLearningPathObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("lpid", 0);

            if (qsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                try
                {
                    if (id > 0)
                    { this._LearningPathObject = new LearningPath(id); }
                }
                catch
                { Response.Redirect("~/administrator/learningpaths"); }
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get learning path title information
            string learningPathTitleInInterfaceLanguage = this._LearningPathObject.Name;
            string learningPathImagePath;
            string learningPathImageCssClass = null;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (LearningPath.LanguageSpecificProperty learningPathLanguageSpecificProperty in this._LearningPathObject.LanguageSpecificProperties)
                {
                    if (learningPathLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { learningPathTitleInInterfaceLanguage = learningPathLanguageSpecificProperty.Name; }
                }
            }

            if (this._LearningPathObject.Avatar != null)
            {
                learningPathImagePath = SitePathConstants.SITE_LEARNINGPATHS_ROOT + this._LearningPathObject.Id + "/" + this._LearningPathObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                learningPathImageCssClass = "AvatarImage";
            }
            else
            {
                learningPathImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
            }

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.LearningPaths, "/administrator/learningpaths"));
            breadCrumbLinks.Add(new BreadcrumbLink(learningPathTitleInInterfaceLanguage, "/administrator/learningpaths/Dashboard.aspx?id=" + this._LearningPathObject.Id.ToString()));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.RulesetEnrollments));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, learningPathTitleInInterfaceLanguage, learningPathImagePath, _GlobalResources.RulesetEnrollments, ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_SERIES, ImageFiles.EXT_PNG), learningPathImageCssClass);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // ADD NEW LEARNING PATH ENROLLMENT
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("AddLearningPathEnrollmentLink",
                                                null,
                                                "Modify.aspx?lpid=" + this._LearningPathObject.Id.ToString(),
                                                null,
                                                _GlobalResources.NewEnrollment,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                );

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion        

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // build instructions panel for grid
            this.FormatPageInformationPanel(this.RuleSetEnrollmentGridInstructionsPanel, _GlobalResources.YouCanSetEnrollmentPriorityByDraggingEnrollmentRowsInTheTableBelow, true);

            // build grid
            this.RuleSetEnrollmentGrid.AllowPaging = false;
            this.RuleSetEnrollmentGrid.ShowSearchBox = false;
            this.RuleSetEnrollmentGrid.ShowRecordsPerPageSelectbox = false;
            this.RuleSetEnrollmentGrid.PageSize = 1000;

            this.RuleSetEnrollmentGrid.StoredProcedure = Library.RuleSetLearningPathEnrollment.GridProcedure;
            this.RuleSetEnrollmentGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.RuleSetEnrollmentGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.RuleSetEnrollmentGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.RuleSetEnrollmentGrid.AddFilter("@idLearningPath", SqlDbType.Int, 4, this._LearningPathObject.Id);
            this.RuleSetEnrollmentGrid.IdentifierField = "idRuleSetLearningPathEnrollment";
            this.RuleSetEnrollmentGrid.DefaultSortColumn = "label";

            // data key names
            this.RuleSetEnrollmentGrid.DataKeyNames = new string[] { "idRuleSetLearningPathEnrollment" };

            // columns
            GridColumn label = new GridColumn(_GlobalResources.Label, null); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.RuleSetEnrollmentGrid.AddColumn(label);

            // add row data bound event
            this.RuleSetEnrollmentGrid.RowDataBound += new GridViewRowEventHandler(this._RuleSetEnrollmentGrid_RowDataBound);            
        }
        #endregion

        #region _RuleSetEnrollmentGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the ruleset enrollment grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RuleSetEnrollmentGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idRuleSetLearningPathEnrollment = Convert.ToInt32(rowView["idRuleSetLearningPathEnrollment"]);

                // add the ruleset learning path enrollment id to the ordering hidden field
                this._RuleSetEnrollmentOrderHiddenField.Value += idRuleSetLearningPathEnrollment.ToString() + "|";

                // AVATAR, TITLE

                string label = Server.HtmlEncode(rowView["label"].ToString());
                int enrollmentPriority = Convert.ToInt32(rowView["priority"]);                
                string enrollmentTypeText = String.Empty;                

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_SERIES, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = label;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // label
                Label labelLabelWrapper = new Label();
                labelLabelWrapper.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(labelLabelWrapper);

                HyperLink labelLink = new HyperLink();
                labelLink.NavigateUrl = "Modify.aspx?lpid=" + this._LearningPathObject.Id.ToString() + "&id=" + idRuleSetLearningPathEnrollment.ToString();
                labelLabelWrapper.Controls.Add(labelLink);

                Label orderLabel = new Label();
                orderLabel.CssClass = "RuleSetEnrollmentOrderLabel";
                orderLabel.Text = enrollmentPriority.ToString();
                labelLink.Controls.Add(orderLabel);

                Literal labelLabel = new Literal();
                labelLabel.Text = ". " + label;
                labelLink.Controls.Add(labelLabel);
            }
        }
        #endregion        

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedRulesetEnrollment_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add controls to panel
            this.ActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedRulesetEnrollment_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl bodyWrapper = new HtmlGenericControl("p");
            Literal body = new Literal();

            bodyWrapper.ID = "GridConfirmActionModalBody1";
            body.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseRulesetEnrollment_s;

            bodyWrapper.Controls.Add(body);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(bodyWrapper);

            // add modal to actions panel
            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.RuleSetEnrollmentGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.RuleSetEnrollmentGrid.Rows[i].FindControl(this.RuleSetEnrollmentGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.RuleSetLearningPathEnrollment.Delete(recordsToDelete, this._LearningPathObject.Id);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedRulesetEnrollment_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoRulesetEnrollment_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.RuleSetEnrollmentGrid.BindData();
            }
        }
        #endregion

        #region _SaveRuleSetEnrollmentOrderingJsonDataStruct
        /// <summary>
        /// Data structure for ruleset enrollment ordering web method return.
        /// </summary>
        public struct _SaveRuleSetEnrollmentOrderingJsonData
        {
            public bool actionSuccessful;
            public string html;
            public string exception;
        }
        #endregion

        #region SaveRuleSetEnrollmentOrdering
        [WebMethod(EnableSession = true)]
        /// <summary>
        /// Web method to save lesson ordering after drag and drop.
        /// </summary>
        public static _SaveRuleSetEnrollmentOrderingJsonData SaveRuleSetEnrollmentOrdering(string rulesetEnrollmentOrdering)
        {
            _SaveRuleSetEnrollmentOrderingJsonData jsonData = new _SaveRuleSetEnrollmentOrderingJsonData();

            try
            {
                if (String.IsNullOrWhiteSpace(rulesetEnrollmentOrdering))
                { throw new Exception(""); }

                DataTable rulesetEnrollmentIdsWithOrdering = new DataTable();
                rulesetEnrollmentIdsWithOrdering.Columns.Add("id", typeof(int));
                rulesetEnrollmentIdsWithOrdering.Columns.Add("order", typeof(int));

                string[] rulesetEnrollmentOrderingInputItems = rulesetEnrollmentOrdering.Split('|');

                int ordinal = 1;

                for (int i = 0; i < rulesetEnrollmentOrderingInputItems.Length; i++)
                {
                    rulesetEnrollmentIdsWithOrdering.Rows.Add(Convert.ToInt32(rulesetEnrollmentOrderingInputItems[i]), ordinal);
                    ordinal++;
                }

                RuleSetLearningPathEnrollment.UpdateOrder(rulesetEnrollmentIdsWithOrdering);

                jsonData.actionSuccessful = true;
                jsonData.html = String.Empty;
                jsonData.exception = String.Empty;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;

                // return jsonData
                return jsonData;
            }
        }
        #endregion        
    }    
}
