﻿using Asentia.Common;
using Asentia.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Administrator.LearningPaths
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel LearningPathsFormContentWrapperContainer;
        public Panel GridAnalyticPanel;
        public UpdatePanel LearningPathsGridUpdatePanel;
        public Grid LearningPathsGrid;
        public Panel GridActionsPanel;
        #endregion

        #region Private Properties
        private LinkButton _DeleteButton;
        private ModalPopup _GridConfirmAction;

        // unpublish learning path confirmation modal
        private ModalPopup _UnpublishLearningPathConfirmationModal;
        private Button _UnpublishLearningPathConfirmationModalLaunchButton;
        private Button _UnpublishLearningPathConfirmationModalLoadButton;

        // publish learning path confirmation modal
        private ModalPopup _PublishLearningPathConfirmationModal;
        private Button _PublishLearningPathConfirmationModalLaunchButton;
        private Button _PublishLearningPathConfirmationModalLoadButton;

        // open learning path confirmation modal
        private ModalPopup _OpenLearningPathConfirmationModal;
        private Button _OpenLearningPathConfirmationModalLaunchButton;
        private Button _OpenLearningPathConfirmationModalLoadButton;

        // close learning path confirmation modal
        private ModalPopup _CloseLearningPathConfirmationModal;
        private Button _CloseLearningPathConfirmationModalLaunchButton;
        private Button _CloseLearningPathConfirmationModalLoadButton;
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // check to ensure learning paths are enabled on the portal
            if (!AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE) ?? false)
            { Response.Redirect("/"); }

            // check permissions
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathContentManager) && 
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("GridAnalytic.css");
            this.IncludePageSpecificCssFile("page-specific/administrator/learningpaths/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.LearningPaths));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, _GlobalResources.LearningPaths, ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG));

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.LearningPathsFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // instansiate unpublish learning path controls
            this._UnpublishLearningPathConfirmationModalLaunchButton = new Button();
            this._UnpublishLearningPathConfirmationModalLaunchButton.ID = "UnpublishLearningPathConfirmationModalLaunchButton";
            this._UnpublishLearningPathConfirmationModalLaunchButton.Style.Add("display", "none");

            this._UnpublishLearningPathConfirmationModalLoadButton = new Button();
            this._UnpublishLearningPathConfirmationModalLoadButton.ID = "UnpublishLearningPathConfirmationModalLoadButton";
            this._UnpublishLearningPathConfirmationModalLoadButton.Style.Add("display", "none");
            this._UnpublishLearningPathConfirmationModalLoadButton.Click += this._LoadUnpublishLearningPathConfirmationModalContent;

            // instansiate publish learning path controls
            this._PublishLearningPathConfirmationModalLaunchButton = new Button();
            this._PublishLearningPathConfirmationModalLaunchButton.ID = "PublishLearningPathConfirmationModalLaunchButton";
            this._PublishLearningPathConfirmationModalLaunchButton.Style.Add("display", "none");

            this._PublishLearningPathConfirmationModalLoadButton = new Button();
            this._PublishLearningPathConfirmationModalLoadButton.ID = "PublishLearningPathConfirmationModalLoadButton";
            this._PublishLearningPathConfirmationModalLoadButton.Style.Add("display", "none");
            this._PublishLearningPathConfirmationModalLoadButton.Click += this._LoadPublishLearningPathConfirmationModalContent;

            // instansiate open learning path controls
            this._OpenLearningPathConfirmationModalLaunchButton = new Button();
            this._OpenLearningPathConfirmationModalLaunchButton.ID = "OpenLearningPathConfirmationModalLaunchButton";
            this._OpenLearningPathConfirmationModalLaunchButton.Style.Add("display", "none");

            this._OpenLearningPathConfirmationModalLoadButton = new Button();
            this._OpenLearningPathConfirmationModalLoadButton.ID = "OpenLearningPathConfirmationModalLoadButton";
            this._OpenLearningPathConfirmationModalLoadButton.Style.Add("display", "none");
            this._OpenLearningPathConfirmationModalLoadButton.Click += this._LoadOpenLearningPathConfirmationModalContent;

            // instansiate close learning path controls
            this._CloseLearningPathConfirmationModalLaunchButton = new Button();
            this._CloseLearningPathConfirmationModalLaunchButton.ID = "CloseLearningPathConfirmationModalLaunchButton";
            this._CloseLearningPathConfirmationModalLaunchButton.Style.Add("display", "none");

            this._CloseLearningPathConfirmationModalLoadButton = new Button();
            this._CloseLearningPathConfirmationModalLoadButton.ID = "CloseLearningPathConfirmationModalLoadButton";
            this._CloseLearningPathConfirmationModalLoadButton.Style.Add("display", "none");
            this._CloseLearningPathConfirmationModalLoadButton.Click += this._LoadCloseLearningPathConfirmationModalContent;

            // attach the modal launch controls            
            this.LearningPathsFormContentWrapperContainer.Controls.Add(this._UnpublishLearningPathConfirmationModalLaunchButton);
            this.LearningPathsFormContentWrapperContainer.Controls.Add(this._PublishLearningPathConfirmationModalLaunchButton);
            this.LearningPathsFormContentWrapperContainer.Controls.Add(this._OpenLearningPathConfirmationModalLaunchButton);
            this.LearningPathsFormContentWrapperContainer.Controls.Add(this._CloseLearningPathConfirmationModalLaunchButton);

            // build grid
            this._BuildGridAnalytics();
            this._BuildGrid();
            this._BuildObjectOptionsPanel();
            this._BuildGridActionsPanel();
            this._BuildGridActionsModal();

            // build modals for publish, unpublish, open, close
            this._BuildPublishLearningPathConfirmationModal();
            this._BuildUnpublishLearningPathConfirmationModal();
            this._BuildOpenLearningPathConfirmationModal();
            this._BuildCloseLearningPathConfirmationModal();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.LearningPathsGrid.BindData();
            }
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Default), "Asentia.LMS.Pages.Administrator.LearningPaths.Default.js");

            base.OnPreRender(e);
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            // build new learning path link if the user has learning path content manager permissions
            if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathContentManager))
            {
                Panel optionsPanelLinksContainer = new Panel();
                optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
                optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

                // ADD LEARNING PATH
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("AddLearningPathLink",
                                                    null,
                                                    "Modify.aspx",
                                                    null,
                                                    _GlobalResources.NewLearningPath,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                    );

                this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
            }
        }
        #endregion

        #region _BuildGridAnalytics
        /// <summary>
        /// Builds the grid analytics for the page.
        /// </summary>
        private void _BuildGridAnalytics()
        {
            // get the analytic data
            DataTable analyticData = GridAnalyticData.LearningPaths();

            int total = Convert.ToInt32(analyticData.Rows[0]["total"]);
            int published = Convert.ToInt32(analyticData.Rows[0]["published"]);
            int nonPublished = Convert.ToInt32(analyticData.Rows[0]["nonPublished"]);
            int closed = Convert.ToInt32(analyticData.Rows[0]["closed"]);
            int createdThisWeek = Convert.ToInt32(analyticData.Rows[0]["createdThisWeek"]);
            int createdThisMonth = Convert.ToInt32(analyticData.Rows[0]["createdThisMonth"]);
            int createdThisYear = Convert.ToInt32(analyticData.Rows[0]["createdThisYear"]);

            // build title for column 1
            GridAnalytic.DataBlock column1Title = new GridAnalytic.DataBlock(total.ToString("N0"), _GlobalResources.LearningPaths);

            // build a DataBlock list for the column 1 data
            List<GridAnalytic.DataBlock> dataBlocksColumn1 = new List<GridAnalytic.DataBlock>();
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(published.ToString("N0"), _GlobalResources.Published.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(nonPublished.ToString("N0"), _GlobalResources.NonPublished.ToLower()));
            dataBlocksColumn1.Add(new GridAnalytic.DataBlock(closed.ToString("N0"), _GlobalResources.Closed.ToLower()));

            // build title for column 2
            GridAnalytic.DataBlock column2Title = new GridAnalytic.DataBlock(null, _GlobalResources.NewLearningPaths);

            // build a DataBlock list for the column 2 data
            List<GridAnalytic.DataBlock> dataBlocksColumn2 = new List<GridAnalytic.DataBlock>();
            dataBlocksColumn2.Add(new GridAnalytic.DataBlock(createdThisWeek.ToString("N0"), _GlobalResources.thisweek_lower.ToLower()));
            dataBlocksColumn2.Add(new GridAnalytic.DataBlock(createdThisMonth.ToString("N0"), _GlobalResources.thismonth_lower.ToLower()));
            dataBlocksColumn2.Add(new GridAnalytic.DataBlock(createdThisYear.ToString("N0"), _GlobalResources.thisyear_lower.ToLower()));

            // build the GridAnalytic object
            GridAnalytic gridAnalyticObject = new GridAnalytic("LearningPathStatistics", column1Title, dataBlocksColumn1, column2Title, dataBlocksColumn2);

            // attach the object to the container
            this.GridAnalyticPanel.Controls.Add(gridAnalyticObject);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            // apply css class to container
            this.LearningPathsGridUpdatePanel.Attributes.Add("class", "FormContentContainer");

            this.LearningPathsGrid.StoredProcedure = "[LearningPath.GetGrid]";
            this.LearningPathsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.LearningPathsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.LearningPathsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.LearningPathsGrid.IdentifierField = "idLearningPath";
            this.LearningPathsGrid.DefaultSortColumn = "dtCreated";
            this.LearningPathsGrid.SearchBoxPlaceholderText = _GlobalResources.SearchLearningPaths;

            // data key names
            this.LearningPathsGrid.DataKeyNames = new string[] { "idLearningPath" };

            // columns
            GridColumn nameNumberCourses = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.Courses, null, "name"); // this is calculated dynamically in the RowDataBound method

            GridColumn options = new GridColumn(_GlobalResources.Options, null, true); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.LearningPathsGrid.AddColumn(nameNumberCourses);
            this.LearningPathsGrid.AddColumn(options);

            // add row data bound event
            this.LearningPathsGrid.RowDataBound += new GridViewRowEventHandler(this._LearningPathGrid_RowDataBound);                      
        }
        #endregion

        #region _LearningPathGrid_RowDataBound
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LearningPathGrid_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idLearningPath = Convert.ToInt32(rowView["idLearningPath"]);

                // AVATAR, NAME, COURSE COUNT

                string avatar = rowView["avatar"].ToString();
                string name = Server.HtmlEncode(rowView["name"].ToString());
                int courseCount = Convert.ToInt32(rowView["courseCount"]);
                bool isPublished = Convert.ToBoolean(rowView["isPublished"]);
                bool isClosed = Convert.ToBoolean(rowView["isClosed"]);

                // avatar
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                if (!String.IsNullOrWhiteSpace(avatar))
                {
                    avatarImagePath = SitePathConstants.SITE_LEARNINGPATHS_ROOT + idLearningPath.ToString() + "/" + avatar;
                    avatarImageClass += " AvatarImage";
                }

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = name;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // title
                Label titleLabel = new Label();
                titleLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(titleLabel);

                HyperLink titleLink = new HyperLink();
                titleLink.NavigateUrl = "Dashboard.aspx?id=" + idLearningPath.ToString();
                titleLink.Text = name;
                titleLabel.Controls.Add(titleLink);

                // number of courses
                Label coursesLabel = new Label();
                coursesLabel.CssClass = "GridSecondaryLine";
                coursesLabel.Text = courseCount + " " + ((courseCount != 1) ? _GlobalResources.Courses : _GlobalResources.Course);
                e.Row.Cells[1].Controls.Add(coursesLabel);

                // OPTIONS COLUMN 

                // publish/unpublish
                if (isPublished)
                {
                    Label learningPathPublishUnPublishSpan = new Label();
                    learningPathPublishUnPublishSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(learningPathPublishUnPublishSpan);

                    LinkButton learningPathUnpublishLink = new LinkButton();
                    learningPathUnpublishLink.ID = "LearningPathUnpublishLink_" + idLearningPath.ToString();
                    learningPathUnpublishLink.OnClientClick = "LoadUnpublishLearningPathModalContent(" + idLearningPath + "); return false;";
                    learningPathUnpublishLink.ToolTip = _GlobalResources.UnpublishLearningPath;

                    Image learningPathUnpublishLinkImage = new Image();
                    learningPathUnpublishLinkImage.ID = "LearningPathUnpublishLinkImage_" + idLearningPath.ToString();
                    learningPathUnpublishLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    learningPathUnpublishLinkImage.AlternateText = _GlobalResources.Published;

                    learningPathUnpublishLink.Controls.Add(learningPathUnpublishLinkImage);

                    learningPathPublishUnPublishSpan.Controls.Add(learningPathUnpublishLink);
                }
                else
                {
                    Label learningPathPublishUnPublishSpan = new Label();
                    learningPathPublishUnPublishSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(learningPathPublishUnPublishSpan);

                    LinkButton learningPathPublishLink = new LinkButton();
                    learningPathPublishLink.ID = "LearningPathPublishLink_" + idLearningPath.ToString();
                    learningPathPublishLink.OnClientClick = "LoadPublishLearningPathModalContent(" + idLearningPath + "); return false;";
                    learningPathPublishLink.ToolTip = _GlobalResources.PublishLearningPath;

                    Image learningPathPublishLinkImage = new Image();
                    learningPathPublishLinkImage.ID = "LearningPathPublishLinkImage_" + idLearningPath.ToString();
                    learningPathPublishLinkImage.CssClass = "DimIcon";
                    learningPathPublishLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    learningPathPublishLinkImage.AlternateText = _GlobalResources.NonPublished;

                    learningPathPublishLink.Controls.Add(learningPathPublishLinkImage);

                    learningPathPublishUnPublishSpan.Controls.Add(learningPathPublishLink);
                }

                // open/close
                if (isClosed)
                {
                    Label learningPathOpenCloseSpan = new Label();
                    learningPathOpenCloseSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(learningPathOpenCloseSpan);

                    LinkButton learningPathOpenLink = new LinkButton();
                    learningPathOpenLink.ID = "LearningPathOpenLink_" + idLearningPath.ToString();
                    learningPathOpenLink.OnClientClick = "LoadOpenLearningPathModalContent(" + idLearningPath + "); return false;";
                    learningPathOpenLink.ToolTip = _GlobalResources.OpenLearningPath;

                    Image learningPathOpenLinkImage = new Image();
                    learningPathOpenLinkImage.ID = "LearningPathOpenLinkImage_" + idLearningPath.ToString();
                    learningPathOpenLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
                    learningPathOpenLinkImage.AlternateText = _GlobalResources.Closed;

                    learningPathOpenLink.Controls.Add(learningPathOpenLinkImage);

                    learningPathOpenCloseSpan.Controls.Add(learningPathOpenLink);
                }
                else
                {
                    Label learningPathOpenCloseSpan = new Label();
                    learningPathOpenCloseSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(learningPathOpenCloseSpan);

                    LinkButton learningPathCloseLink = new LinkButton();
                    learningPathCloseLink.ID = "LearningPathCloseLink_" + idLearningPath.ToString();
                    learningPathCloseLink.OnClientClick = "LoadCloseLearningPathModalContent(" + idLearningPath + "); return false;";
                    learningPathCloseLink.ToolTip = _GlobalResources.CloseLearningPath;

                    Image learningPathCloseLinkImage = new Image();
                    learningPathCloseLinkImage.ID = "LearningPathCloseLinkImage_" + idLearningPath.ToString();
                    learningPathCloseLinkImage.CssClass = "DimIcon";
                    learningPathCloseLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
                    learningPathCloseLinkImage.AlternateText = _GlobalResources.Open;

                    learningPathCloseLink.Controls.Add(learningPathCloseLinkImage);

                    learningPathOpenCloseSpan.Controls.Add(learningPathCloseLink);
                }

                // show modify link if the user has learning path content manager permissions
                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathContentManager))
                {
                    // learning path modify
                    Label learningPathModifySpan = new Label();
                    learningPathModifySpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(learningPathModifySpan);

                    HyperLink learningPathModifyLink = new HyperLink();
                    learningPathModifyLink.NavigateUrl = "Modify.aspx?id=" + idLearningPath.ToString();
                    learningPathModifySpan.Controls.Add(learningPathModifyLink);

                    Image learningPathModifyIcon = new Image();
                    learningPathModifyIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MODIFY, ImageFiles.EXT_PNG);
                    learningPathModifyIcon.AlternateText = _GlobalResources.Modify;
                    learningPathModifyIcon.ToolTip = _GlobalResources.LearningPathProperties;
                    learningPathModifyLink.Controls.Add(learningPathModifyIcon);
                }

                // show enrollments link if the user has learning path enrollment manager permissions
                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager))
                {
                    // enrollments
                    Label enrollmentsSpan = new Label();
                    enrollmentsSpan.CssClass = "GridImageLink";
                    e.Row.Cells[2].Controls.Add(enrollmentsSpan);

                    HyperLink enrollmentsLink = new HyperLink();
                    enrollmentsLink.NavigateUrl = "rulesetenrollments/Default.aspx?lpid=" + idLearningPath.ToString();
                    enrollmentsSpan.Controls.Add(enrollmentsLink);

                    Image enrollmentsIcon = new Image();
                    enrollmentsIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT_BUTTON, ImageFiles.EXT_PNG);
                    enrollmentsIcon.AlternateText = _GlobalResources.Enrollments;
                    enrollmentsIcon.ToolTip = _GlobalResources.Enrollments;
                    enrollmentsLink.Controls.Add(enrollmentsIcon);
                }
            }
        }
        #endregion

        #region _BuildGridActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsPanel()
        {
            this.GridActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this._DeleteButton = new LinkButton();
            this._DeleteButton.ID = "GridDeleteButton";
            this._DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this._DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedLearningPath_s;
            this._DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.GridActionsPanel.Controls.Add(this._DeleteButton);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                            ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedLearningPath_s;
            this._GridConfirmAction.TargetControlID = this._DeleteButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseLearningPath_s;
            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.GridActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.LearningPathsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.LearningPathsGrid.Rows[i].FindControl(this.LearningPathsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.LearningPath.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedLearningPath_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoLearningPath_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.LearningPathsGrid.BindData();
            }
        }
        #endregion

        #region _BuildUnpublishLearningPathConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the unpublish action performed on a LearningPath.
        /// </summary>
        private void _BuildUnpublishLearningPathConfirmationModal()
        {
            // set modal properties
            this._UnpublishLearningPathConfirmationModal = new ModalPopup("UnpublishLearningPathConfirmationModal");
            this._UnpublishLearningPathConfirmationModal.Type = ModalPopupType.Form;
            this._UnpublishLearningPathConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
            this._UnpublishLearningPathConfirmationModal.HeaderIconAlt = _GlobalResources.UnpublishLearningPath;
            this._UnpublishLearningPathConfirmationModal.HeaderText = _GlobalResources.UnpublishLearningPath;
            this._UnpublishLearningPathConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._UnpublishLearningPathConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._UnpublishLearningPathConfirmationModal.TargetControlID = this._UnpublishLearningPathConfirmationModalLaunchButton.ID;
            this._UnpublishLearningPathConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._UnpublishLearningPathSubmit_Command);

            // build the modal form panel
            Panel unpublishLearningPathConfirmationModalFormPanel = new Panel();
            unpublishLearningPathConfirmationModalFormPanel.ID = "UnpublishLearningPathConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmUnpublishActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToUnpublishThisLearningPath;

            body1Wrapper.Controls.Add(body1);
            unpublishLearningPathConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the unpublish LearningPath data hidden field
            HiddenField LearningPathUnpublishData = new HiddenField();
            LearningPathUnpublishData.ID = "LearningPathUnpublishData";

            // build the modal body
            this._UnpublishLearningPathConfirmationModal.AddControlToBody(unpublishLearningPathConfirmationModalFormPanel);
            this._UnpublishLearningPathConfirmationModal.AddControlToBody(this._UnpublishLearningPathConfirmationModalLoadButton);
            this._UnpublishLearningPathConfirmationModal.AddControlToBody(LearningPathUnpublishData);

            // add modal to container
            this.LearningPathsFormContentWrapperContainer.Controls.Add(this._UnpublishLearningPathConfirmationModal);
        }
        #endregion

        #region _LoadUnpublishLearningPathConfirmationModalContent
        /// <summary>
        /// Loads content for LearningPath unpublish confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadUnpublishLearningPathConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel unpublishLearningPathConfirmationModalFormPanel = (Panel)this._UnpublishLearningPathConfirmationModal.FindControl("UnpublishLearningPathConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._UnpublishLearningPathConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = true;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(dEx.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(ex.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _UnpublishLearningPathSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the unpublish LearningPath confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _UnpublishLearningPathSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField LearningPathUnpublishData = (HiddenField)this._UnpublishLearningPathConfirmationModal.FindControl("LearningPathUnpublishData");
            Panel unpublishLearningPathConfirmationModalFormPanel = (Panel)this._UnpublishLearningPathConfirmationModal.FindControl("UnpublishLearningPathConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._UnpublishLearningPathConfirmationModal.ClearFeedback();

                // get the LearningPath id
                int idLearningPath = Convert.ToInt32(LearningPathUnpublishData.Value);

                // unpublish the LearningPath
                LearningPath LearningPathObject = new LearningPath(idLearningPath);
                LearningPathObject.IsPublished = false;
                LearningPathObject.Save();

                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(_GlobalResources.TheLearningPathHasBeenUnpublishedSuccessfully, false);

                // rebind the LearningPath grid and update
                this.LearningPathsGrid.BindData();
                this.LearningPathsGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(dEx.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._UnpublishLearningPathConfirmationModal.DisplayFeedback(ex.Message, true);
                unpublishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._UnpublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._UnpublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildPublishLearningPathConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the publish action performed on a LearningPath.
        /// </summary>
        private void _BuildPublishLearningPathConfirmationModal()
        {
            // set modal properties
            this._PublishLearningPathConfirmationModal = new ModalPopup("PublishLearningPathConfirmationModal");
            this._PublishLearningPathConfirmationModal.Type = ModalPopupType.Form;
            this._PublishLearningPathConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
            this._PublishLearningPathConfirmationModal.HeaderIconAlt = _GlobalResources.PublishLearningPath;
            this._PublishLearningPathConfirmationModal.HeaderText = _GlobalResources.PublishLearningPath;
            this._PublishLearningPathConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._PublishLearningPathConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._PublishLearningPathConfirmationModal.TargetControlID = this._PublishLearningPathConfirmationModalLaunchButton.ID;
            this._PublishLearningPathConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._PublishLearningPathSubmit_Command);

            // build the modal form panel
            Panel publishLearningPathConfirmationModalFormPanel = new Panel();
            publishLearningPathConfirmationModalFormPanel.ID = "PublishLearningPathConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmPublishActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToPublishThisLearningPath;

            body1Wrapper.Controls.Add(body1);
            publishLearningPathConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the publish LearningPath data hidden field
            HiddenField LearningPathPublishData = new HiddenField();
            LearningPathPublishData.ID = "LearningPathPublishData";

            // build the modal body
            this._PublishLearningPathConfirmationModal.AddControlToBody(publishLearningPathConfirmationModalFormPanel);
            this._PublishLearningPathConfirmationModal.AddControlToBody(this._PublishLearningPathConfirmationModalLoadButton);
            this._PublishLearningPathConfirmationModal.AddControlToBody(LearningPathPublishData);

            // add modal to container
            this.LearningPathsFormContentWrapperContainer.Controls.Add(this._PublishLearningPathConfirmationModal);
        }
        #endregion

        #region _LoadPublishLearningPathConfirmationModalContent
        /// <summary>
        /// Loads content for LearningPath publish confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadPublishLearningPathConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel publishLearningPathConfirmationModalFormPanel = (Panel)this._PublishLearningPathConfirmationModal.FindControl("PublishLearningPathConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._PublishLearningPathConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = true;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(dEx.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(ex.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _PublishLearningPathSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the publish LearningPath confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _PublishLearningPathSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField LearningPathPublishData = (HiddenField)this._PublishLearningPathConfirmationModal.FindControl("LearningPathPublishData");
            Panel publishLearningPathConfirmationModalFormPanel = (Panel)this._PublishLearningPathConfirmationModal.FindControl("PublishLearningPathConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._PublishLearningPathConfirmationModal.ClearFeedback();

                // get the LearningPath id
                int idLearningPath = Convert.ToInt32(LearningPathPublishData.Value);

                // publish the LearningPath
                LearningPath LearningPathObject = new LearningPath(idLearningPath);
                LearningPathObject.IsPublished = true;
                LearningPathObject.Save();

                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
                this._PublishLearningPathConfirmationModal.DisplayFeedback(_GlobalResources.TheLearningPathHasBeenPublishedSuccessfully, false);

                // rebind the LearningPath grid and update
                this.LearningPathsGrid.BindData();
                this.LearningPathsGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(dEx.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._PublishLearningPathConfirmationModal.DisplayFeedback(ex.Message, true);
                publishLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._PublishLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._PublishLearningPathConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildOpenLearningPathConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the open action performed on a LearningPath.
        /// </summary>
        private void _BuildOpenLearningPathConfirmationModal()
        {
            // set modal properties
            this._OpenLearningPathConfirmationModal = new ModalPopup("OpenLearningPathConfirmationModal");
            this._OpenLearningPathConfirmationModal.Type = ModalPopupType.Form;
            this._OpenLearningPathConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_OPEN, ImageFiles.EXT_PNG);
            this._OpenLearningPathConfirmationModal.HeaderIconAlt = _GlobalResources.OpenLearningPath;
            this._OpenLearningPathConfirmationModal.HeaderText = _GlobalResources.OpenLearningPath;
            this._OpenLearningPathConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._OpenLearningPathConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._OpenLearningPathConfirmationModal.TargetControlID = this._OpenLearningPathConfirmationModalLaunchButton.ID;
            this._OpenLearningPathConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._OpenLearningPathSubmit_Command);

            // build the modal form panel
            Panel openLearningPathConfirmationModalFormPanel = new Panel();
            openLearningPathConfirmationModalFormPanel.ID = "OpenLearningPathConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmOpenActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToOpenThisLearningPath;

            body1Wrapper.Controls.Add(body1);
            openLearningPathConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the open LearningPath data hidden field
            HiddenField LearningPathOpenData = new HiddenField();
            LearningPathOpenData.ID = "LearningPathOpenData";

            // build the modal body
            this._OpenLearningPathConfirmationModal.AddControlToBody(openLearningPathConfirmationModalFormPanel);
            this._OpenLearningPathConfirmationModal.AddControlToBody(this._OpenLearningPathConfirmationModalLoadButton);
            this._OpenLearningPathConfirmationModal.AddControlToBody(LearningPathOpenData);

            // add modal to container
            this.LearningPathsFormContentWrapperContainer.Controls.Add(this._OpenLearningPathConfirmationModal);
        }
        #endregion

        #region _LoadOpenLearningPathConfirmationModalContent
        /// <summary>
        /// Loads content for LearningPath open confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadOpenLearningPathConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel openLearningPathConfirmationModalFormPanel = (Panel)this._OpenLearningPathConfirmationModal.FindControl("OpenLearningPathConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._OpenLearningPathConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = true;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(dEx.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(ex.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _OpenLearningPathSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the open LearningPath confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _OpenLearningPathSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField LearningPathOpenData = (HiddenField)this._OpenLearningPathConfirmationModal.FindControl("LearningPathOpenData");
            Panel openLearningPathConfirmationModalFormPanel = (Panel)this._OpenLearningPathConfirmationModal.FindControl("OpenLearningPathConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._OpenLearningPathConfirmationModal.ClearFeedback();

                // get the LearningPath id
                int idLearningPath = Convert.ToInt32(LearningPathOpenData.Value);

                // open the LearningPath
                LearningPath LearningPathObject = new LearningPath(idLearningPath);
                LearningPathObject.IsClosed = false;
                LearningPathObject.Save();

                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
                this._OpenLearningPathConfirmationModal.DisplayFeedback(_GlobalResources.TheLearningPathHasBeenOpenedSuccessfully, false);

                // rebind the LearningPath grid and update
                this.LearningPathsGrid.BindData();
                this.LearningPathsGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(dEx.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._OpenLearningPathConfirmationModal.DisplayFeedback(ex.Message, true);
                openLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._OpenLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._OpenLearningPathConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildCloseLearningPathConfirmationModal
        /// <summary>
        /// Builds the confirmation modal for the close action performed on a LearningPath.
        /// </summary>
        private void _BuildCloseLearningPathConfirmationModal()
        {
            // set modal properties
            this._CloseLearningPathConfirmationModal = new ModalPopup("CloseLearningPathConfirmationModal");
            this._CloseLearningPathConfirmationModal.Type = ModalPopupType.Form;
            this._CloseLearningPathConfirmationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSED, ImageFiles.EXT_PNG);
            this._CloseLearningPathConfirmationModal.HeaderIconAlt = _GlobalResources.CloseLearningPath;
            this._CloseLearningPathConfirmationModal.HeaderText = _GlobalResources.CloseLearningPath;
            this._CloseLearningPathConfirmationModal.CloseButtonTextType = ModalPopupButtonText.No;
            this._CloseLearningPathConfirmationModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._CloseLearningPathConfirmationModal.TargetControlID = this._CloseLearningPathConfirmationModalLaunchButton.ID;
            this._CloseLearningPathConfirmationModal.SubmitButton.Command += new CommandEventHandler(this._CloseLearningPathSubmit_Command);

            // build the modal form panel
            Panel closeLearningPathConfirmationModalFormPanel = new Panel();
            closeLearningPathConfirmationModalFormPanel.ID = "CloseLearningPathConfirmationModalFormPanel";

            // body text
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "ConfirmCloseActionBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToCloseThisLearningPath;

            body1Wrapper.Controls.Add(body1);
            closeLearningPathConfirmationModalFormPanel.Controls.Add(body1Wrapper);

            // build the close LearningPath data hidden field
            HiddenField LearningPathCloseData = new HiddenField();
            LearningPathCloseData.ID = "LearningPathCloseData";

            // build the modal body
            this._CloseLearningPathConfirmationModal.AddControlToBody(closeLearningPathConfirmationModalFormPanel);
            this._CloseLearningPathConfirmationModal.AddControlToBody(this._CloseLearningPathConfirmationModalLoadButton);
            this._CloseLearningPathConfirmationModal.AddControlToBody(LearningPathCloseData);

            // add modal to container
            this.LearningPathsFormContentWrapperContainer.Controls.Add(this._CloseLearningPathConfirmationModal);
        }
        #endregion

        #region _LoadCloseLearningPathConfirmationModalContent
        /// <summary>
        /// Loads content for LearningPath close confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadCloseLearningPathConfirmationModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            Panel closeLearningPathConfirmationModalFormPanel = (Panel)this._CloseLearningPathConfirmationModal.FindControl("CloseLearningPathConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._CloseLearningPathConfirmationModal.ClearFeedback();

                // make the submit and close buttons visible
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = true;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(dEx.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(ex.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _CloseLearningPathSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the close LearningPath confirmation modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _CloseLearningPathSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField LearningPathCloseData = (HiddenField)this._CloseLearningPathConfirmationModal.FindControl("LearningPathCloseData");
            Panel closeLearningPathConfirmationModalFormPanel = (Panel)this._CloseLearningPathConfirmationModal.FindControl("CloseLearningPathConfirmationModalFormPanel");

            try
            {
                // clear the modal feedback
                this._CloseLearningPathConfirmationModal.ClearFeedback();

                // get the LearningPath id
                int idLearningPath = Convert.ToInt32(LearningPathCloseData.Value);

                // close the LearningPath
                LearningPath LearningPathObject = new LearningPath(idLearningPath);
                LearningPathObject.IsClosed = true;
                LearningPathObject.Save();

                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
                this._CloseLearningPathConfirmationModal.DisplayFeedback(_GlobalResources.TheLearningPathHasBeenClosedSuccessfully, false);

                // rebind the LearningPath grid and update
                this.LearningPathsGrid.BindData();
                this.LearningPathsGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(dnfEx.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(fnuEx.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(cpeEx.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(dEx.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._CloseLearningPathConfirmationModal.DisplayFeedback(ex.Message, true);
                closeLearningPathConfirmationModalFormPanel.Controls.Clear();
                this._CloseLearningPathConfirmationModal.SubmitButton.Visible = false;
                this._CloseLearningPathConfirmationModal.CloseButton.Visible = false;
            }
        }
        #endregion
    }
}
