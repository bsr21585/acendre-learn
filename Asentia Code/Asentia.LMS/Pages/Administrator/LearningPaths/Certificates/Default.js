﻿function RedirectToCertificateTemplate(idLearningPath) {
    var idCertificate = $('#CertificateTemplateListingListBox :selected').val();

    window.location.replace("/administrator/learningpaths/certificates/Modify.aspx?lpid=" + idLearningPath + "&id=" + idCertificate);
}