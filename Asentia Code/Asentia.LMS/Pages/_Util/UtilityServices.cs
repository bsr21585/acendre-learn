﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Xml;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages._Util
{
    /// <summary>
    /// Utility web services for Asentia.
    /// </summary>
    [WebService(Description = "Utility web services for Asentia.", Namespace = "http://default.asentialms.com/UtilityServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class UtilityServices : WebService
    {
        #region SSOFromCM
        #region SSOFromCMJsonDataStruct
        public struct SSOFromCMJsonDataStruct
        {
            public bool actionSuccessful;            
            public string token;
            public string exception;
        }
        #endregion

        #region GetSSOFromCMToken
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public SSOFromCMJsonDataStruct GetSSOFromCMToken(string secretKey)
        {            
            SSOFromCMJsonDataStruct jsonData = new SSOFromCMJsonDataStruct();            

            try
            {
                // validate the secret key to ensure this request comes from the customer manager
                if (secretKey != Config.ApplicationSettings.CustomerManagerSSOSecretKey)
                { throw new AsentiaException(_GlobalResources.TheSecretKeyDoesNotMatchTheStoredSecretKey); }

                // generate the token
                string token = Utility.GenerateTokenForSSO(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, 1, 1);

                // return the information
                if (!String.IsNullOrWhiteSpace(token))
                {
                    jsonData.actionSuccessful = true;
                    jsonData.token = token;
                    jsonData.exception = String.Empty;
                }
                else
                {
                    jsonData.actionSuccessful = false;
                    jsonData.token = String.Empty;
                    jsonData.exception = _GlobalResources.UnableToAcquireSSOToken;
                }                

                // return jsonData
                return jsonData;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                jsonData.actionSuccessful = false;
                jsonData.token = String.Empty;
                jsonData.exception = dnfEx.Message;

                // return jsonData
                return jsonData;
            }
            catch (AsentiaException aEx)
            {
                jsonData.actionSuccessful = false;
                jsonData.token = String.Empty;
                jsonData.exception = aEx.Message;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.token = String.Empty;
                jsonData.exception = ex.Message + " | " + ex.StackTrace;

                // return jsonData
                return jsonData;
            }
        }
        #endregion
        #endregion

        #region _EventEmailDetailJsonDataStruct
        public struct _EventEmailDetailJsonDataStruct
        {
            public string emailBody;
            public string sentTo;
            public string sentDate;
            public string emailSubject;
        }
        #endregion

        #region GetEventEmailDetail
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public _EventEmailDetailJsonDataStruct GetEventEmailDetail(int idEventEmailQueue)
        {
            _EventEmailDetailJsonDataStruct jsonData = new _EventEmailDetailJsonDataStruct();

            try
            {
                DataRow queueItem = EventEmailQueue.GetEventEmailById(idEventEmailQueue).Rows[0];

                // get the data from the row
                int idSite = Convert.ToInt32(queueItem["idSite"]);
                string hostname = queueItem["hostname"].ToString();
                int idEventLog = Convert.ToInt32(queueItem["idEventLog"]);
                int idEventType = Convert.ToInt32(queueItem["idEventType"]);
                int idEventEmailNotification = Convert.ToInt32(queueItem["idEventEmailNotification"]);
                int idEventTypeRecipient = Convert.ToInt32(queueItem["idEventTypeRecipient"]);
                int idObject = Convert.ToInt32(queueItem["idObject"]);
                int idObjectRelated = Convert.ToInt32(queueItem["idObjectRelated"]);
                string objectType = queueItem["objectType"].ToString();
                int? idObjectUser = null;
                string objectUserFullName = queueItem["objectUserFullName"].ToString();
                string objectUserFirstName = queueItem["objectUserFirstName"].ToString();
                string objectUserLogin = queueItem["objectUserLogin"].ToString();
                string objectUserEmail = queueItem["objectUserEmail"].ToString();
                int idRecipient = Convert.ToInt32(queueItem["idRecipient"]);
                string recipientLangString = queueItem["recipientLangString"].ToString();
                string recipientFullName = queueItem["recipientFullName"].ToString();
                string recipientFirstName = queueItem["recipientFirstName"].ToString();
                string recipientLogin = queueItem["recipientLogin"].ToString();
                string recipientEmail = queueItem["recipientEmail"].ToString();
                int recipientTimezone = Convert.ToInt32(queueItem["recipientTimezone"]);
                string recipientTimezoneDotNetName = queueItem["recipientTimezoneDotNetName"].ToString();
                string from = queueItem["from"].ToString();
                string copyTo = queueItem["copyTo"].ToString();
                string systemFromAddressOverride = queueItem["systemFromAddressOverride"].ToString();
                string systemSmtpServerNameOverride = queueItem["systemSmtpServerNameOverride"].ToString();
                string systemSmtpServerPortOverrideStr = queueItem["systemSmtpServerPortOverride"].ToString();
                string systemSmtpServerUseSslOverrideStr = queueItem["systemSmtpServerUseSslOverride"].ToString();
                string systemSmtpServerUsernameOverride = queueItem["systemSmtpServerUsernameOverride"].ToString();
                string systemSmtpServerPasswordOverride = queueItem["systemSmtpServerPasswordOverride"].ToString();
                int? priority = null;
                bool isHTMLBased = false;
                int? attachmentType = null;
                DateTime dtEvent = Convert.ToDateTime(queueItem["dtEvent"]);
                DateTime dtAction = Convert.ToDateTime(queueItem["dtAction"]);
                DateTime dtActivation = Convert.ToDateTime(queueItem["dtActivation"]);
                string objectInformation = queueItem["objectInformation"].ToString();

                if (!String.IsNullOrWhiteSpace(queueItem["idObjectUser"].ToString()))
                { idObjectUser = Convert.ToInt32(queueItem["idObjectUser"]); }

                if (!String.IsNullOrWhiteSpace(queueItem["priority"].ToString()))
                { priority = Convert.ToInt32(queueItem["priority"]); }

                if (!String.IsNullOrWhiteSpace(queueItem["isHTMLBased"].ToString()) && queueItem["isHTMLBased"].ToString() == "1")
                { isHTMLBased = true; }

                if (!String.IsNullOrWhiteSpace(queueItem["attachmentType"].ToString()))
                { attachmentType = Convert.ToInt32(queueItem["attachmentType"]); }

                int systemSmtpServerPortOverride = 0;

                if (!String.IsNullOrWhiteSpace(systemSmtpServerPortOverrideStr))
                { Int32.TryParse(systemSmtpServerPortOverrideStr, out systemSmtpServerPortOverride); }

                bool systemSmtpServerUseSslOverride = false;

                if (!String.IsNullOrWhiteSpace(systemSmtpServerUseSslOverrideStr))
                { Boolean.TryParse(systemSmtpServerUseSslOverrideStr, out systemSmtpServerUseSslOverride); }

                // instansiate an EventEmail
                EventEmail eventEmail = new EventEmail(idEventEmailNotification, hostname, from, copyTo, systemFromAddressOverride, systemSmtpServerNameOverride, systemSmtpServerPortOverride, systemSmtpServerUseSslOverride, systemSmtpServerUsernameOverride, systemSmtpServerPasswordOverride, priority, isHTMLBased, recipientLangString, null);

                // fill in the common elements that do not need an object binding
                CultureInfo culture = new CultureInfo(recipientLangString);

                eventEmail.LmsUrl = "http://" + hostname + "." + Config.AccountSettings.BaseDomain;
                eventEmail.LmsHostname = hostname;

                eventEmail.EventDate = TimeZoneInfo.ConvertTimeFromUtc(dtEvent, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern);
                eventEmail.EventTime = TimeZoneInfo.ConvertTimeFromUtc(dtEvent, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortTimePattern);

                eventEmail.UserFullName = objectUserFullName;
                eventEmail.UserFirstName = objectUserFirstName;
                eventEmail.UserLogin = objectUserLogin;
                eventEmail.UserEmail = objectUserEmail;

                eventEmail.RecipientFullName = recipientFullName;
                eventEmail.RecipientFirstName = recipientFirstName;
                eventEmail.RecipientLogin = recipientLogin;
                eventEmail.RecipientEmail = recipientEmail;

                // if we have object information, load as xml and populate for placeholder replacement
                if (!String.IsNullOrWhiteSpace(objectInformation))
                {
                    bool isXmlLoaded = false;
                    XmlDocument objectInformationXml = new XmlDocument();

                    try
                    {
                        objectInformationXml.LoadXml(objectInformation);
                        isXmlLoaded = true;
                    }
                    catch // do nothing with any caught exception, just move along
                    { }

                    // fill in elements that need an object binding if we have the xml for the object information
                    if (isXmlLoaded)
                    {
                        // object name is common to all object types
                        string objectName = null;
                        objectName = objectInformationXml.SelectSingleNode("//objectName").InnerText;

                        int courseEstimatedLengthInMinutes;
                        string courseEstimatedLength = String.Empty;

                        switch (objectType)
                        {
                            case "user":
                                eventEmail.ObjectName = objectName;
                                eventEmail.UserRegistrationRejectionComments = objectInformationXml.SelectSingleNode("//rejectionComments").InnerText;

                                break;
                            case "courseenrollment":
                                eventEmail.ObjectName = objectName;
                                eventEmail.EnrollmentCourseDescription = objectInformationXml.SelectSingleNode("//enrollmentDescription").InnerText;

                                // enrollment due date
                                DateTime dtDue;

                                if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentDueDate").InnerText))
                                {
                                    if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//enrollmentDueDate").InnerText, out dtDue))
                                    { eventEmail.EnrollmentDueDate = TimeZoneInfo.ConvertTimeFromUtc(dtDue, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern); }
                                }

                                // enrollment expire date
                                DateTime dtExpired;

                                if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentExpireDate").InnerText))
                                {
                                    if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//enrollmentExpireDate").InnerText, out dtExpired))
                                    { eventEmail.EnrollmentExpireDate = TimeZoneInfo.ConvertTimeFromUtc(dtExpired, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern); }
                                }

                                // course estimated length in minutes                                    
                                if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentEstCompletionTime").InnerText))
                                {
                                    if (int.TryParse(objectInformationXml.SelectSingleNode("//enrollmentEstCompletionTime").InnerText, out courseEstimatedLengthInMinutes))
                                    {
                                        if (courseEstimatedLengthInMinutes < 60)
                                        { courseEstimatedLength = String.Format(_GlobalResources.XMinute_s, courseEstimatedLengthInMinutes.ToString()); }
                                        else
                                        {
                                            int estimatedLengthHours = courseEstimatedLengthInMinutes / 60;
                                            int estimatedLengthMinutes = courseEstimatedLengthInMinutes - (estimatedLengthHours * 60);

                                            if (estimatedLengthMinutes > 0)
                                            { courseEstimatedLength = String.Format(_GlobalResources.XHour_sAndXMinute_s, estimatedLengthHours, estimatedLengthMinutes); }
                                            else
                                            { courseEstimatedLength = String.Format(_GlobalResources.XHour_s, estimatedLengthHours); }
                                        }

                                        eventEmail.EnrollmentCourseEstCompletionTime = courseEstimatedLength;
                                    }
                                }

                                break;
                            case "courseenrollmentrequest":
                                eventEmail.ObjectName = objectName;
                                eventEmail.EnrollmentCourseDescription = objectInformationXml.SelectSingleNode("//enrollmentDescription").InnerText;
                                eventEmail.EnrollmentRequestRejectionComments = objectInformationXml.SelectSingleNode("//rejectionComments").InnerText;

                                // course estimated length in minutes                                    
                                if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentEstCompletionTime").InnerText))
                                {
                                    if (int.TryParse(objectInformationXml.SelectSingleNode("//enrollmentEstCompletionTime").InnerText, out courseEstimatedLengthInMinutes))
                                    {
                                        if (courseEstimatedLengthInMinutes < 60)
                                        { courseEstimatedLength = String.Format(_GlobalResources.XMinute_s, courseEstimatedLengthInMinutes.ToString()); }
                                        else
                                        {
                                            int estimatedLengthHours = courseEstimatedLengthInMinutes / 60;
                                            int estimatedLengthMinutes = courseEstimatedLengthInMinutes - (estimatedLengthHours * 60);

                                            if (estimatedLengthMinutes > 0)
                                            { courseEstimatedLength = String.Format(_GlobalResources.XHour_sAndXMinute_s, estimatedLengthHours, estimatedLengthMinutes); }
                                            else
                                            { courseEstimatedLength = String.Format(_GlobalResources.XHour_s, estimatedLengthHours); }
                                        }

                                        eventEmail.EnrollmentCourseEstCompletionTime = courseEstimatedLength;
                                    }
                                }

                                break;
                            case "lesson":
                                eventEmail.ObjectName = objectName;

                                break;
                            case "standuptraininginstance":
                                eventEmail.ObjectName = objectName;

                                // session start date
                                DateTime dtSessionStart;

                                if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//sessionDateTime").InnerText))
                                {
                                    if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//sessionDateTime").InnerText, out dtSessionStart))
                                    {
                                        if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//sessionTimezoneDotNetName").InnerText))
                                        {
                                            string sessionTimeZoneDotNetName = objectInformationXml.SelectSingleNode("//sessionTimezoneDotNetName").InnerText;
                                            eventEmail.StandupTrainingSessionMeetingTime = TimeZoneInfo.ConvertTimeFromUtc(dtSessionStart, TimeZoneInfo.FindSystemTimeZoneById(sessionTimeZoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern);
                                            eventEmail.StandupTrainingSessionMeetingTime += " @ " + TimeZoneInfo.ConvertTimeFromUtc(dtSessionStart, TimeZoneInfo.FindSystemTimeZoneById(sessionTimeZoneDotNetName)).ToString(culture.DateTimeFormat.ShortTimePattern);
                                        }
                                    }
                                }

                                // session timezone
                                eventEmail.StandupTrainingSessionTimezone = objectInformationXml.SelectSingleNode("//sessionTimezoneFriendlyName").InnerText;

                                // session location
                                eventEmail.StandupTrainingSessionLocation = objectInformationXml.SelectSingleNode("//sessionLocation").InnerText;

                                // session location description
                                eventEmail.StandupTrainingSessionLocationDescription = objectInformationXml.SelectSingleNode("//sessionLocationDescription").InnerText;

                                // session instructor full name
                                eventEmail.StandupTrainingSessionInstructorFullName = objectInformationXml.SelectSingleNode("//sessionInstructorFullName").InnerText;

                                // session drop link
                                // if the user is allowed to drop the ilt, populate the stand up training session drop link                                    
                                bool isRestrictedDrop = Convert.ToBoolean(objectInformationXml.SelectSingleNode("//sessionIsRestrictedDrop").InnerText);
                                if (!isRestrictedDrop)
                                {
                                    AsentiaAESEncryption dropSessionCryptoURL = new AsentiaAESEncryption();
                                    string encryptedDropSessionURL = WebUtility.UrlEncode(dropSessionCryptoURL.Encrypt(idObjectRelated.ToString() + "|" + idRecipient.ToString() + "|" + idSite.ToString()));

                                    eventEmail.StandupTrainingSessionDropLink = "http://" + hostname + "." + Config.AccountSettings.BaseDomain + "/_util/DropSession/?token=" + encryptedDropSessionURL;
                                }
                                else
                                {
                                    eventEmail.StandupTrainingSessionDropLink = "";
                                }

                                break;
                            case "learningpathenrollment":
                                eventEmail.ObjectName = objectName;
                                eventEmail.EnrollmentLearningPathDescription = objectInformationXml.SelectSingleNode("//enrollmentDescription").InnerText;

                                // learning path enrollment due date
                                DateTime dtDueLPE;

                                if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentDueDate").InnerText))
                                {
                                    if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//enrollmentDueDate").InnerText, out dtDueLPE))
                                    { eventEmail.EnrollmentDueDate = TimeZoneInfo.ConvertTimeFromUtc(dtDueLPE, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern); }
                                }

                                // learning path enrollment expire date
                                DateTime dtExpiredLPE;

                                if (!String.IsNullOrWhiteSpace(objectInformationXml.SelectSingleNode("//enrollmentExpireDate").InnerText))
                                {
                                    if (DateTime.TryParse(objectInformationXml.SelectSingleNode("//enrollmentExpireDate").InnerText, out dtExpiredLPE))
                                    { eventEmail.EnrollmentExpireDate = TimeZoneInfo.ConvertTimeFromUtc(dtExpiredLPE, TimeZoneInfo.FindSystemTimeZoneById(recipientTimezoneDotNetName)).ToString(culture.DateTimeFormat.ShortDatePattern); }
                                }

                                break;
                            case "certificate":
                                eventEmail.ObjectName = objectName;

                                break;
                            case "certification":
                                eventEmail.ObjectName = objectName;
                                eventEmail.CertificationDescription = objectInformationXml.SelectSingleNode("//certificationDescription").InnerText;

                                break;
                            case "coursediscussionmessage":
                            case "groupdiscussionmessage":
                                eventEmail.ObjectName = objectName;
                                eventEmail.DiscussionMessage = objectInformationXml.SelectSingleNode("//discussionMessage").InnerText;

                                break;
                            default:
                                break;
                        }
                    }
                }

                eventEmail.FormatForView();

                // return the information
                if (eventEmail != null)
                {
                    jsonData.emailBody = eventEmail.Body.ToString();
                    jsonData.sentTo = eventEmail.RecipientFullName.ToString() + " (" + eventEmail.RecipientLogin.ToString() + ")";
                    jsonData.sentDate = eventEmail.EventDate.ToString();
                    jsonData.emailSubject = eventEmail.Subject.ToString();
                }

                // return jsonData
                return jsonData;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                jsonData.emailSubject = "DatabaseDetailsNotFoundException";
                jsonData.emailBody = dnfEx.Message;

                // return jsonData
                return jsonData;
            }
            catch (AsentiaException aEx)
            {
                jsonData.emailSubject = "AsentiaException";
                jsonData.emailBody = aEx.Message;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.emailSubject = "Exception";
                jsonData.emailBody = ex.Message + " | " + ex.StackTrace;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region CourseListing
        #region CourseListingObject
        public class CourseListingObject
        {
            public int Id;
            public string Title;
            public double? Credits;
        }
        #endregion

        #region CourseListingJsonDataStruct
        public struct CourseListingJsonData
        {
            public bool actionSuccessful;
            public bool hasRecords;
            public List<CourseListingObject> courses;
            public string exception;            
        }
        #endregion

        #region GetCourseListing
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public CourseListingJsonData GetCourseListing(string searchParam)
        {                        
            CourseListingJsonData jsonData = new CourseListingJsonData();

            DataTable courses = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                // get the messages from the database object
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", 1, SqlDbType.Int, 4, ParameterDirection.Input);

                if (String.IsNullOrEmpty(searchParam))
                { databaseObject.AddParameter("@searchParam", DBNull.Value, SqlDbType.NVarChar, 4000, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@searchParam", searchParam, SqlDbType.NVarChar, 4000, ParameterDirection.Input); }

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.IdsAndNamesForSelectList]", true);
                courses.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);                                

                if (courses.Rows.Count > 0)
                { 
                    jsonData.hasRecords = true;
                    jsonData.courses = new List<CourseListingObject>();
                }
                else
                { jsonData.hasRecords = false; }

                foreach (DataRow row in courses.Rows)
                {
                    CourseListingObject course = new CourseListingObject();
                    int idCourse = Convert.ToInt32(row["idCourse"]);
                    string title = row["title"].ToString();
                    double? credits;

                    if (!String.IsNullOrWhiteSpace(row["credits"].ToString()))
                    { credits = Convert.ToDouble(row["credits"]); }
                    else
                    { credits = null; }

                    course.Id = idCourse;
                    course.Title = title;
                    course.Credits = credits;

                    jsonData.courses.Add(course);                                        
                }

                jsonData.actionSuccessful = true;
                jsonData.exception = String.Empty;                

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.hasRecords = false;
                jsonData.exception = ex.Message + " | " + ex.StackTrace;                

                // return jsonData
                return jsonData;
            }            
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region SessionKeepAlive
        /// <summary>
        /// SessionKeepAlive
        /// </summary>
        /// <param name="idUser">the user id of the session user</param>
        /// <returns>true/false</returns>        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool SessionKeepAlive(int idUser)
        {
            if (AsentiaSessionState.IdSiteUser == idUser)
            {
                // update user session expiration, and return true
                AsentiaAuthenticatedPage.UpdateUserSessionExpiration(AsentiaSessionState.DtExpires);                
                return true;
            }
            else
            { return false; }
        }
        #endregion

        #region SearchRecipient
        #region RecipientListingObject
        public class RecipientListingObject
        {
            public string RecipientID;
            public string RecipientImagePath;
            public string RecipientName;
            public string RecipientType;
        }
        #endregion

        #region RecipientListingJsonData
        public struct RecipientListingJsonData
        {
            public bool actionSuccessful;
            public bool hasRecords;
            public List<RecipientListingObject> recipients;
            public string exception;  
        }
        #endregion


        #region SearchRecipient
        /// <summary>
        /// SearchRecipient
        /// </summary>
        /// <param name="idUser">searchParam</param>
        /// <returns>true/false</returns>        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public RecipientListingJsonData SearchRecipient(string searchParam)
        {
            RecipientListingJsonData jsonData = new RecipientListingJsonData();

            try
            {
                DataTable recipients = InboxMessage.SearchRecipient(searchParam);

                if (recipients.Rows.Count > 0)
                {
                    jsonData.hasRecords = true;
                    jsonData.recipients = new List<RecipientListingObject>();
                }
                else
                { jsonData.hasRecords = false; }

                foreach (DataRow row in recipients.Rows)
                {
                    RecipientListingObject recipient = new RecipientListingObject();
                    recipient.RecipientID = row["idObject"].ToString();
                    recipient.RecipientName = row["objectName"].ToString();
                    recipient.RecipientType = row["objectType"].ToString();

                    if (recipient.RecipientType == "group")
                    { recipient.RecipientImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG); }
                    else
                    { recipient.RecipientImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }

                    jsonData.recipients.Add(recipient);
                }

                jsonData.actionSuccessful = true;
                jsonData.exception = String.Empty;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.hasRecords = false;
                jsonData.exception = ex.Message + " | " + ex.StackTrace;

                // return jsonData
                return jsonData;
            }
        }
        #endregion
        #endregion
    }
}
