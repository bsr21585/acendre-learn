﻿#region Includes
using System;
using System.Collections.Generic;
#endregion

namespace Asentia.LMS.Pages._Util
{
    #region scorm12CmiDataModel cmi data model
    /// <summary>
    /// CmiDataModel for scorm 1.2
    /// </summary>
    public class CmiDataModelScorm12
    {
        public string credit { get; set; }                       //Indicates whether the student’s performance with the SCO is to be credited.
        public string lesson_mode { get; set; }                  //Identifies the modes in which the SCO may be presented to the student.
        public string completion_status { get; set; }           //Indicates whether the student has completed the SCO.        
        public string exit { get; set; }                        //Contains information as to why and how the student exited from the SCO.
        public string entry { get; set; }                       //Contains information as how the student entered in the SCO.
        public string student_id { get; set; }                  //Indicates the student’s id.
        public string student_name { get; set; }                //Indicates the student’s name.
        public string launch_data { get; set; }                 //Indicates the launch data value.
        public string lesson_status { get; set; }               //Data model element to defines the status of the activity. 
        public string suspend_data { get; set; }                //Provides additional space for storing and retrieving information relating to the suspension of an SCO.
        public string lesson_location { get; set; }             //Represents a location in the SCO. Its value and meaning are determined by the SCO.
        public Score score { get; set; }                        //Identifies the learner’s score for the SCO.
        public string success_status { get; set; }              //Indicates whether the learner has mastered the SCO.        
        public double total_time { get; set; }                  //Defines information concerning  rte Total_time.
        public List<Interaction> interactions { get; set; }          //Defines information concerning an interaction for the purpose of measurement or assessment.
        public List<Objective> objectives { get; set; }            //Defines information concerning an objectives for the purpose of measurement or assessment.
    }
    #endregion

    #region Score cmi data model object
    /// <summary>
    /// Score model  for scorm 1.2
    /// </summary>
    public class Score
    {
        public string max { get; set; }                         //Real number with 7 significant decimal digits.
        public string min { get; set; }                         //Real number with 7 significant decimal digits.
        public string raw { get; set; }                         //Real number with 7 significant decimal digits.
    }

    #endregion

    #region CorrectResponse Model
    /// <summary>
    /// CorrectResponse model
    /// </summary>
    public class CorrectResponse
    {
        public string pattern { get; set; }
    }
    #endregion

    #region Interaction Model
    /// <summary>
    /// Interaction model
    /// </summary>
    public class Interaction
    {
        public string id { get; set; }
        public string type { get; set; }
        public string timestamp { get; set; }
        public string timestampConverted { get; set; }
        public string weighting { get; set; }
        public string student_response { get; set; }
        public string result { get; set; }
        public string description { get; set; }
        public float latency { get; set; }
        public List<object> objectives { get; set; }
        public List<CorrectResponse> correct_responses { get; set; }
    }
    #endregion

    #region Objective Model
    /// <summary>
    /// Objective model
    /// </summary>
    public class Objective
    {
        public string id { get; set; }
        public string completion_status { get; set; }
        public string success_status { get; set; }
        public Score score { get; set; }
    }
    #endregion
}
