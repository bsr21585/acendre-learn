﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Linq;
using System.Xml.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Xml;
using Asentia.Common;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using Asentia.LMS.Library;
using System.Data;

namespace Asentia.LMS.Pages._Util
{
    /// <summary>
    /// Web service for PayPal Instance Payment Notification (IPN Listener).
    /// </summary>
    [WebService(Description = "Web service for PayPal Instance Payment Notification (IPN Listener).", Namespace = "https://default.asentia.com/PayPalIPNListener")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]    
    public class PayPalIPNListener : WebService
    {
        #region ReceiveIPN
        /// <summary>
        /// The entry point for the listener. Returns empty 200 response per PayPal requirement.
        /// </summary>
        [WebMethod(Description = "PayPal Instance Payment Notification (IPN) Listener.", EnableSession = true)]        
        public void ReceiveIPN()
        {
            try
            {
                // NOTE: MESSAGES SENT TO LOGS SHOULD BE IN ENGLISH, THEY DO NOT NEED TO BE MULTI-LINGUAL SINCE END USERS NEVER SEE THEM

                // get the global site object so we have access to site parameters
                AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);

                // allow this service to act as administrator
                AsentiaSessionState.IdSiteUser = 1;

                // set security protocol to TLS 1.2 as PayPal requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // ensure that the portal's processing method is PayPal; if not, record an error
                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD) == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
                {
                    // verify the transaction
                    this._VerifyTransaction(HttpContext.Current.Request);
                }
                else
                { this._LogIPNResponse("PayPal is not the portal's ecommerce processing method.", "_IPN_REQUEST_FAILED_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow), true); }                
            }
            catch (Exception ex)
            {
                this._LogIPNResponse(ex.Message + " | " + ex.StackTrace, "_IPN_REQUEST_FATALEXCEPTION_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow), true);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                AsentiaSessionState.EndSession(false);
            }
        }
        #endregion

        #region _VerifyTransaction
        /// <summary>
        /// Verifies the transaction from PayPal.
        /// 
        /// NOTE: ANY "KNOWN" EXCEPTION CAUGHT FROM THE TRY AREA OF THIS METHOD WILL BE HANDLED AND LOGGED
        /// IN AN ATTEMPT TO MARK THE PURCHASE OBJECT AS FAILED. GENERIC EXCEPTIONS AND ANY EXCEPTION THROWN
        /// FROM "FINALLY" WILL BE CAUGHT AND HANDLED BY THE CALLING METHOD.
        /// </summary>
        /// <param name="request">the HTTP request</param>
        private void _VerifyTransaction(HttpRequest request)
        {
            // declare global values reported by PayPal
            const string _PAYPAL_RESPONSE_VERIFIED = "VERIFIED";
            const string _PAYPAL_RESPONSE_INVALID = "INVALID";

            const string _PAYPAL_STATUS_COMPLETED = "Completed";
            const string _PAYPAL_STATUS_DENIED = "Denied";
            const string _PAYPAL_STATUS_FAILED = "Failed";
            const string _PAYPAL_STATUS_PENDING = "Pending";

            // declare the paypal transaction id here so that it is available to exception handlers for writing log files
            // also, initialize it as undefined so that we can know if we hit an exception before getting the id
            string payPalTransactionId = "_UNDEFINED_TXN";

            // declare a purchase object, and a failed flag so that "finally" can handle marking the purchase as failed if it needs to
            Purchase purchaseObject = new Purchase();            
            bool markPurchaseAsFailed = false;

            try
            {
                // get the request as a string
                byte[] requestBytes = request.BinaryRead(request.ContentLength);
                string requestString = Encoding.ASCII.GetString(requestBytes);

                // convert that string into a NameValueCollection
                NameValueCollection requestVariables = HttpUtility.ParseQueryString(requestString);

                // get the PayPal transaction ID
                payPalTransactionId = requestVariables["txn_id"];

                // log the request
                this._LogIPNRequest(requestVariables, payPalTransactionId + "_IPN_REQUEST_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow));

                // send request back to PayPal for validation
                string payPalUrl = String.Empty;

                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PAYPAL_MODE) == EcommerceSettings.PROCESSING_MODE_LIVE)
                { payPalUrl = Config.EcommerceSettings.PayPalIPNReturnLiveURL; }
                else
                { payPalUrl = Config.EcommerceSettings.PayPalIPNReturnTestURL; }

                // build the request, send the request, get the response
                HttpWebRequest requestToPayPal = (HttpWebRequest)WebRequest.Create(payPalUrl);

                // encode payload into bytes
                byte[] requestToPayPalBytes;
                requestToPayPalBytes = Encoding.ASCII.GetBytes("cmd=_notify-validate&" + requestString);

                // set the payload type, length, and method
                requestToPayPal.ContentType = "application/x-www-form-urlencoded";
                requestToPayPal.ContentLength = requestToPayPalBytes.Length;
                requestToPayPal.Method = "POST";

                // write the request stream (payload)
                Stream requestToPayPalStream = requestToPayPal.GetRequestStream();
                requestToPayPalStream.Write(requestToPayPalBytes, 0, requestToPayPalBytes.Length);
                requestToPayPalStream.Close();

                // get the response - which will be either VERIFIED or INVALID
                string responseFromPayPalString = String.Empty;
                bool responseFatalError = false;

                using (HttpWebResponse responseFromPayPal = (HttpWebResponse)requestToPayPal.GetResponse())
                {
                    if (responseFromPayPal.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream responseFromPayPalStream = responseFromPayPal.GetResponseStream())
                        { responseFromPayPalString = new StreamReader(responseFromPayPalStream).ReadToEnd(); }
                    }
                    else
                    { responseFatalError = true; }
                }

                // read the response and perform actions based on the response                
                
                if (!responseFatalError)
                {
                    // if the transaction is VERIFIED, proceed to validating elements of the purchase and granting enrollments
                    if (responseFromPayPalString == _PAYPAL_RESPONSE_VERIFIED)
                    {
                        // get the request variables that we need to verify
                        string itemNumber = requestVariables["item_number"];
                        string itemNumber1 = requestVariables["item_number1"];
                        string paymentStatus = requestVariables["payment_status"];
                        string mcGross = requestVariables["mc_gross"];

                        // parse the request variables into their respective types
                        int idTransactionItem = 0;

                        // item_number or item_number1 - PayPal could use either one
                        if (!String.IsNullOrWhiteSpace(itemNumber))
                        {
                            if (!Int32.TryParse(itemNumber, out idTransactionItem))
                            { throw new AsentiaException("The transaction item id could not be retrieved from the IPN request."); }
                        }
                        else
                        {
                            if (!Int32.TryParse(itemNumber1, out idTransactionItem))
                            { throw new AsentiaException("The transaction item id could not be retrieved from the IPN request."); }
                        }

                        double amountPaid = 0;
                        if (!Double.TryParse(mcGross, out amountPaid))
                        { throw new AsentiaException("The gross invoice amount could not be retrieved from the IPN request."); }

                        // ensure the idTransactionItem is greater than 0, this may be unnecessary but do it anyway
                        if (idTransactionItem <= 0)
                        { throw new AsentiaException("The transaction item id is invalid."); }

                        // if PayPal reports "Denied" or "Failed", fail the transaction
                        if (paymentStatus == _PAYPAL_STATUS_FAILED || paymentStatus == _PAYPAL_STATUS_DENIED)
                        { throw new AsentiaException("PayPal reported that the transaction's status was \"" + paymentStatus + "\"."); }

                        // if PayPal reports "Completed", then continue with validating, enrolling, and confirming the transaction
                        // otherwise, ("Pending") do nothing, and just report that the transaction is still pending
                        if (paymentStatus == _PAYPAL_STATUS_COMPLETED)
                        {
                            // instansiate the transaction item object
                            TransactionItem transactionItemObject = new TransactionItem(idTransactionItem);

                            // compare the amount PayPal is reporting to the transaction item amount, ensure that it as at least what PayPal reported
                            if (transactionItemObject.Paid > amountPaid)
                            { throw new AsentiaException("The amount paid through PayPal is less than the item's amount in the transaction item record."); }

                            // validate the purchase id from the transaction item                        
                            if (transactionItemObject.IdPurchase == null || transactionItemObject.IdPurchase == 0)
                            { throw new AsentiaException("Could not find a purchase associated with the transaction item id."); }

                            // instansiate the purchase object
                            purchaseObject = new Purchase((int)transactionItemObject.IdPurchase);

                            // ensure that the purchase has not already been confirmed
                            if (purchaseObject.TimeStamp != null) // throw a generic exception here so we do not fail an already confirmed transaction item
                            { throw new Exception("The purchase associated with the item has already been confirmed."); }

                            // IF WE GET HERE, ALL VALIDATIONS ARE DONE, NOW LETS PROCEED WITH CONFIRMING THE PURCHASE WHICH WILL DO ALL NECESSARY ENROLLMENTS
                            purchaseObject.TimeStamp = DateTime.UtcNow;
                            purchaseObject.CreditCardLastFour = "XXXX";
                            purchaseObject.TransactionId = payPalTransactionId + " (PayPal)";

                            purchaseObject.Confirm(purchaseObject.IdUser);

                            // IF WE GET HERE, ALL IS CONFIRMED, LOG THE SUCCESS
                            this._LogIPNResponse("The transaction has been successfully processed and confirmed.", payPalTransactionId + "_RESPONSE_SUCCESS_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow), false);
                        }
                        else // "Pending" - log that it is pending
                        { this._LogIPNResponse("The transaction is marked as \"Pending\" by PayPal.", payPalTransactionId + "_RESPONSE_PENDING_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow), false); }                       
                    }
                    else // INVALID
                    { throw new AsentiaException("PayPal returned an INVALID response for the transaction."); }
                }
                else
                { throw new AsentiaException("A fatal error occurred when receiving the VERIFIED/INVALID response from PayPal."); }                
            }
            // it might be overkill, but catch any possible exception here so that we can log it
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                markPurchaseAsFailed = true;
                this._LogIPNResponse(dnfEx.Message, payPalTransactionId + "_RESPONSE_FAILED_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow), true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                markPurchaseAsFailed = true;
                this._LogIPNResponse(fnuEx.Message, payPalTransactionId + "_RESPONSE_FAILED_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow), true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                markPurchaseAsFailed = true;
                this._LogIPNResponse(cpeEx.Message, payPalTransactionId + "_RESPONSE_FAILED_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow), true);
            }
            catch (DatabaseException dEx)
            {
                markPurchaseAsFailed = true;
                this._LogIPNResponse(dEx.Message, payPalTransactionId + "_RESPONSE_FAILED_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow), true);
            }
            catch (AsentiaException ex)
            {
                markPurchaseAsFailed = true;
                this._LogIPNResponse(ex.Message, payPalTransactionId + "_RESPONSE_FAILED_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.UtcNow), true);
            }
            catch (Exception) // pass any other unhandled exception up as it is truly a fatal exception
            {
                throw;
            }
            finally // do not try to handle exceptions from here, they are fatal and will be caught by the caller
            {
                // if we need to mark the purchase as failed and we have a purchase object, mark it as failed
                if (markPurchaseAsFailed && purchaseObject.Id > 0)
                {
                    purchaseObject.TimeStamp = null;
                    purchaseObject.DtPending = null;
                    purchaseObject.CreditCardLastFour = null;
                    purchaseObject.TransactionId = null;
                    purchaseObject.Save();
                }
            }
        }
        #endregion

        #region _LogIPNRequest
        /// <summary>
        /// Saves IPN request to a log file.
        /// </summary>
        /// <param name="requestVariables">the request variables</param>
        /// <param name="fileName">the name of the file, minus extension</param>
        private void _LogIPNRequest(NameValueCollection requestVariables, string fileName)
        {
            // create the log directory if it doesn't exist
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_TRANSACTION_PAYPAL)))
            { Directory.CreateDirectory(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_TRANSACTION_PAYPAL)); }

            // build file path
            string filePath = SitePathConstants.SITE_LOG_TRANSACTION_PAYPAL + fileName + ".txt";
            
            // write the request to a file
            using (StreamWriter sw = new StreamWriter(Server.MapPath(filePath)))
            {
                foreach (string key in requestVariables)
                { sw.WriteLine(key + " : " + requestVariables[key]); }
            }
        }
        #endregion

        #region _LogIPNResponse
        /// <summary>
        /// Saves IPN response to a log file.
        /// </summary>
        /// <param name="responseMessage">the message to record</param>
        /// <param name="fileName">the name of the file, minus extension</param>
        /// <param name="isError">is this response an error</param>
        private void _LogIPNResponse(string responseMessage, string fileName, bool isError)
        {
            // create the log directory if it doesn't exist
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_TRANSACTION_PAYPAL)))
            { Directory.CreateDirectory(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_TRANSACTION_PAYPAL)); }

            // build file path
            string filePath = SitePathConstants.SITE_LOG_TRANSACTION_PAYPAL + fileName + ".xml";

            // build xml document for response log
            string statusText = "SUCCESS";

            if (isError)
            { statusText = "FAILED"; }

            XDocument xmlDocument =
                new XDocument(
                    new XElement("IPNResponse",
                        new XElement("Status", statusText),
                        new XElement("Message", new XCData(responseMessage))
                    )
                );

            // save the document
            xmlDocument.Save(Server.MapPath(filePath));
        }
        #endregion
    }
}