﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Asentia.Common;
using Asentia.Controls;
using Lesson = Asentia.LMS.Library.Lesson;

namespace Asentia.LMS.Pages._Util
{

    /// <summary>
    /// Web Services to Retrieve Manifest Information, Retrieve User RTE Data, and Save User RTE Data
    /// Server-side implementation of the SCORM API
    /// </summary>
    [WebService(Description = "Web services for use in Asentia SCORM API.", Namespace = "http://default.asentialms.com/SCORMServices")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class SCORMServices : System.Web.Services.WebService
    {
        #region Properties
        int organizationCounter;
        ManifestDataModel manifest;
        #endregion

        #region Constructor
        public SCORMServices()
        {
            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }
        #endregion

        #region Interfaces

        #region RetrieveManifestObject
        /// <summary>
        /// Web Service method to retrive the imsmanifest.xml, parse it into the Manifest Data Model,
        /// serialize it to JSON, and return the JSON
        /// </summary>
        /// <param name="manifestId">the database identifier of the course's manifest location</param>
        /// <returns>Manifest Data Model as a JSON string</returns>
        [WebMethod(Description = "Gets specified manifest, parses, and returns a manifest data model in JSON.", EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string RetrieveManifestObject(string contentFolder)
        {
            //Added By Chetu 
            var manifesttext = "imsmanifest.xml";
            string manifestDataJSON = null;

            manifestDataJSON = ProcessManifest(Path.Combine(contentFolder, manifesttext));

            return manifestDataJSON;
        }
        #endregion

        #region RetrieveRTEData
        /// <summary>
        /// Retrieves saved user interaction data for a specified SCO
        /// </summary>
        /// <param name="lessonIdentifier">the identifier of the lesson</param>
        /// <param name="scoIdentifier">the identifier of the sco</param>
        /// <returns>JSON string containing a representation of the RTE data model</returns>
        [WebMethod(Description = "Retrieves saved user RTE data and returns it in JSON form.", EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string RetrieveRTEData(string learnerId, string lessonIdentifier, string scoIdentifier)
        {
            var fileList = new List<KeyValuePair<string, string>>();
            try
            {
                string jsonFloderPath = Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + learnerId + "/" + lessonIdentifier + "/");
                string jsonString;

                if (!Directory.Exists(jsonFloderPath))
                {
                    return "fail";
                }

                //string[] filePaths = Directory.GetFiles(@"c:\MyDir\");
                string[] filePaths = Directory.GetFiles(jsonFloderPath, "*.json", SearchOption.TopDirectoryOnly);

                foreach (string filePath in filePaths)
                {
                    jsonString = File.ReadAllText(filePath);

                    // clean the JSON string by removing tab and new line characters
                    jsonString = jsonString.Replace(System.Environment.NewLine, "");
                    jsonString = jsonString.Replace("\t", "");

                    fileList.Add(new KeyValuePair<string, string>(Path.GetFileNameWithoutExtension(filePath), jsonString));
                }
            }
            catch (Exception ex)
            {
                return "fail";
            }

            string result = JsonConvert.SerializeObject(fileList);
            return result;
        }
        #endregion

        #region SaveRTEData
        /// <summary>
        /// Retrieves saved user interaction data for a specified SCO
        /// </summary>
        /// <param name="lessonIdentifier">the identifier of the lesson</param>
        /// <param name="scoIdentifier">the identifier of the sco</param>
        /// <param name="jsonString">JSON string of RTE data to save</param>
        /// <returns>string indicating success/failure</returns>
        [WebMethod(Description = "Takes in JSON object of user RTE data and saves it.", EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SaveRTEData(string learnerId, int lessonIdentifier, string scoIdentifier, string jsonString, string schemaVersion, string launchType, int idDataSco, bool saveToDB = false)
        {
            try
            {
                if (!String.IsNullOrWhiteSpace(scoIdentifier) && scoIdentifier != "|__NonLeafDataItems__|")
                {
                    string folderPath = Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + learnerId + "/" + lessonIdentifier.ToString());

                    if (!Directory.Exists(folderPath))
                    { Directory.CreateDirectory(folderPath); }

                    string filePath = Path.Combine(folderPath, scoIdentifier + ".json");

                    // clean the JSON string by removing tab and new line characters
                    jsonString = jsonString.Replace(System.Environment.NewLine, "");
                    jsonString = jsonString.Replace("\t", "");

                    // write the data file no matter what
                    File.WriteAllText(filePath, jsonString);

                    //This portion will be executed only on termination of the sco package
                    #region Saving scorm status data into database
                    #region Saving global data
                    if (scoIdentifier == "global_data")
                    {
                        if (saveToDB)
                        {
                            //string content;
                            //CmiDataModel fileContentModel = new CmiDataModel();
                            //CmiDataModelScorm12 fileContentModelForSCorm12 = new CmiDataModelScorm12();

                            //if (File.Exists(filePath))
                            //{
                                //content = File.ReadAllText(filePath);
                                //if (schemaVersion != "1.2")
                                    //fileContentModel = JsonHelper.JsonDeserialize<CmiDataModel>(content);
                                //else
                                //{
                                    //fileContentModelForSCorm12 = JsonHelper.JsonDeserialize<CmiDataModelScorm12>(content);
                                //}
                            //}

                            #region Save global for scorm 2004

                            if (schemaVersion != "1.2")
                            {                                
                                //File.WriteAllText(filePath, jsonString);

                                //Deserialize data from Json to CmiDataModel
                                CmiDataModel cmiModelData = JsonHelper.JsonDeserialize<CmiDataModel>(jsonString);

                                ///Call SaveTableSCOData Method to save data in database
                                Library.DataSCO objectDataSco = new Library.DataSCO();
                                objectDataSco.Id = idDataSco;
                                objectDataSco.IdDataLesson = lessonIdentifier;
                                objectDataSco.ManifestIdentifier = null;
                                objectDataSco.LaunchType = launchType;
                                objectDataSco.CompletionStatus = (int)((Lesson.ScormCompletionStatus)Enum.Parse(typeof(Lesson.ScormCompletionStatus), cmiModelData.completion_status));
                                objectDataSco.SuccessStatus = (int)((Lesson.ScormSuccessStatus)Enum.Parse(typeof(Lesson.ScormSuccessStatus), cmiModelData.success_status));

                                if (!String.IsNullOrWhiteSpace(Convert.ToString(cmiModelData.score)) && !String.IsNullOrWhiteSpace(cmiModelData.score.scaled))
                                {
                                    objectDataSco.ScoreScaled = float.Parse(cmiModelData.score.scaled);
                                }
                                else
                                {
                                    objectDataSco.ScoreScaled = null;
                                }

                                float result = !String.IsNullOrWhiteSpace(Convert.ToString(cmiModelData.total_time)) ? (float)cmiModelData.total_time : 0;

                                if (float.IsPositiveInfinity(result))
                                {
                                    result = float.MaxValue;
                                }
                                else if (float.IsNegativeInfinity(result))
                                {
                                    result = float.MinValue;
                                }
                                objectDataSco.TotalTime = result;
                                objectDataSco.ActualAttemptCount = cmiModelData.actualAttemptCount;
                                objectDataSco.EffectiveAttemptCount = cmiModelData.effectiveAttemptCount;

                                //Save the sco status
                                objectDataSco.Save();
                            }
                            #endregion

                            #region Save global for scorm 1.2
                            else
                            {
                                //File.WriteAllText(filePath, jsonString);

                                //Deserialize data from Json to CmiDataModel
                                CmiDataModelScorm12 cmiModelData = JsonHelper.JsonDeserialize<CmiDataModelScorm12>(jsonString);

                                ///Call SaveTableSCOData Method to save data in database
                                Library.DataSCO objectDataSco = new Library.DataSCO();
                                objectDataSco.Id = idDataSco;
                                objectDataSco.IdDataLesson = lessonIdentifier;
                                objectDataSco.ManifestIdentifier = string.Empty;
                                objectDataSco.LaunchType = launchType;
                                objectDataSco.CompletionStatus = (int)((Lesson.ScormCompletionStatus)Enum.Parse(typeof(Lesson.ScormCompletionStatus), cmiModelData.completion_status));
                                objectDataSco.SuccessStatus = (int)((Lesson.ScormSuccessStatus)Enum.Parse(typeof(Lesson.ScormSuccessStatus), cmiModelData.success_status));

                                if (!String.IsNullOrWhiteSpace(Convert.ToString(cmiModelData.score)) && !String.IsNullOrWhiteSpace(cmiModelData.score.raw))
                                {
                                    objectDataSco.ScoreScaled = float.Parse(cmiModelData.score.raw);
                                }
                                else
                                {
                                    objectDataSco.ScoreScaled = null;
                                }

                                float result = !String.IsNullOrWhiteSpace(Convert.ToString(cmiModelData.total_time)) ? (float)cmiModelData.total_time : 0;
                                if (float.IsPositiveInfinity(result))
                                {
                                    result = float.MaxValue;
                                }
                                else if (float.IsNegativeInfinity(result))
                                {
                                    result = float.MinValue;
                                }
                                objectDataSco.TotalTime = result;
                                objectDataSco.ActualAttemptCount = 0;
                                objectDataSco.EffectiveAttemptCount = 0;
                                objectDataSco.Save();
                            }
                            #endregion
                        }
                    }
                    #endregion

                    #region Saving the interactions data
                    else
                    {
                        #region Save scorm 2004 interaction data
                        if (schemaVersion != "1.2")
                        {
                            //File.WriteAllText(filePath, jsonString);

                            if (saveToDB)
                            {
                                //Deserialize data from Json to CmiDataModel
                                CmiDataModel cmiModelData = JsonHelper.JsonDeserialize<CmiDataModel>(jsonString);

                                //interactions table
                                DataTable interactionTable = new DataTable();
                                interactionTable.Columns.Add("id");
                                interactionTable.Columns.Add("result");
                                interactionTable.Columns.Add("latency");

                                //flag to check the intraction item with id already exists
                                bool isIntExist = false;

                                if (cmiModelData.interactions.Count > 0)
                                {
                                    for (int i = cmiModelData.interactions.Count - 1; i >= 0; i--)
                                    {
                                        for (int j = 0; j < interactionTable.Rows.Count; j++)
                                        {
                                            //if intraction with id already exists then break the loop
                                            if (interactionTable.Rows[j]["id"].ToString() == cmiModelData.interactions[i].id)
                                            {
                                                isIntExist = true;
                                                break;
                                            }
                                        }

                                        if (isIntExist == false)
                                        {
                                            DataRow rowInteraction = interactionTable.NewRow();
                                            rowInteraction["id"] = cmiModelData.interactions[i].id;
                                            rowInteraction["result"] = cmiModelData.interactions[i].result;
                                            rowInteraction["latency"] = cmiModelData.interactions[i].latency;
                                            interactionTable.Rows.Add(rowInteraction);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }

                                    //save the interactions value in the database
                                    Library.DataSCO objectDataSco = new Library.DataSCO();
                                    objectDataSco.SaveInteractions(lessonIdentifier, scoIdentifier, interactionTable);
                                }
                            }
                        }
                        #endregion
                        #region Save scorm1.2 interaction data
                        else
                        {
                            //File.WriteAllText(filePath, jsonString);

                            if (saveToDB)
                            {
                                //Deserialize data from Json to CmiDataModel
                                CmiDataModelScorm12 cmiModelData12 = JsonHelper.JsonDeserialize<CmiDataModelScorm12>(jsonString);

                                //interactions table
                                DataTable interactionTable = new DataTable();
                                interactionTable.Columns.Add("id");
                                interactionTable.Columns.Add("result");
                                interactionTable.Columns.Add("latency");

                                //flag to check the intraction item with id already exists
                                bool isIntExist = false;

                                if (cmiModelData12.interactions.Count > 0)
                                {
                                    for (int i = cmiModelData12.interactions.Count - 1; i >= 0; i--)
                                    {
                                        for (int j = 0; j < interactionTable.Rows.Count; j++)
                                        {
                                            //if intraction with id already exists then break the loop
                                            if (interactionTable.Rows[j]["id"].ToString() == cmiModelData12.interactions[i].id)
                                            {
                                                isIntExist = true;
                                                break;
                                            }
                                        }

                                        if (isIntExist == false)
                                        {
                                            DataRow rowInteraction = interactionTable.NewRow();
                                            rowInteraction["id"] = cmiModelData12.interactions[i].id;
                                            rowInteraction["result"] = cmiModelData12.interactions[i].result;
                                            rowInteraction["latency"] = cmiModelData12.interactions[i].latency;
                                            interactionTable.Rows.Add(rowInteraction);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }

                                    //save the interactions value in the database
                                    Library.DataSCO objectDataSco = new Library.DataSCO();
                                    objectDataSco.SaveInteractions(lessonIdentifier, scoIdentifier, interactionTable);
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                    #endregion
                }                
                // SAVES NON-LEAF DATA ITEMS FOR SCORM 2004 SEQUENCING
                else if (!String.IsNullOrWhiteSpace(scoIdentifier) && scoIdentifier == "|__NonLeafDataItems__|")
                {
                    var jsonList = JArray.Parse(jsonString);
                    foreach (dynamic obj in jsonList)
                    {
                        string scoIdentifierName = obj.Key;
                        string jsonStr = Convert.ToString(obj.Value);
                        SaveRTEData(learnerId, lessonIdentifier, scoIdentifierName, jsonStr, schemaVersion, launchType, idDataSco, false);
                    }
                }
                else // NO SCO IDENTIFIER TO SAVE, DIE QUIETLY
                { }
            }
            catch (Exception ex)
            {
                return "fail";
            }

            return "success";
        }
        #endregion

        #region KeepAliveSession
        /// <summary>
        /// KeepAliveSession
        /// </summary>
        /// <param name="learnerId"></param>
        /// <returns></returns>
        [WebMethod(Description = "Update session", EnableSession = true)]
        public bool KeepAliveSession(int learnerId)
        {
            if (AsentiaSessionState.IdSiteUser == learnerId)
            {
                // update user session expiration
                AsentiaAuthenticatedPage.UpdateUserSessionExpiration(AsentiaSessionState.DtExpires);

                return true;
            }
            else
            { return false; }
        }
        #endregion
        #endregion

        #region Methods

        #region ProcessManifest
        /// <summary>
        /// Processes the imsmanifest.xml into the Manifest Data Model and serializes it to JSON
        /// </summary>
        /// <param name="pathToManifest">path to imsmanifest.xml</param>
        /// <returns>Manifest Data Model for the imsmanifest.xml serialized to JSON string</returns>
        private string ProcessManifest(string pathToManifest)
        {
            string manifestDataJSON = null;

            // instansiate the manifest data model
            this.manifest = new ManifestDataModel();

            // retrieve the imsmanifest.xml
            XmlDocument manifestDocument = OpenManifestDocument(pathToManifest);

            // parse the manifest into the data model
            bool isManifestParsed = ParseManifest(manifestDocument.DocumentElement, 0, null, null, null);

            // if the manifest was parsed, then serialize to JSON
            if (isManifestParsed)
            { manifestDataJSON = SerializeManifestToJSON(); }

            return manifestDataJSON;
        }
        #endregion

        #region OpenManifestDocument
        /// <summary>
        /// Opens and loads the imsmanifest.xml file
        /// </summary>
        /// <param name="documentPath">path to imsmanifest.xml</param>
        /// <returns>XmlDocument of the manifest</returns>
        private XmlDocument OpenManifestDocument(string documentPath)
        {
            XmlDocument xmlDocument = new XmlDocument();
            try
            {
                XmlTextReader xmlReader = new XmlTextReader(Server.MapPath(documentPath));
                xmlReader.WhitespaceHandling = WhitespaceHandling.None;
                xmlDocument.Load(xmlReader);
                xmlReader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return xmlDocument;
        }
        #endregion

        #region ParseManifest
        /// <summary>
        /// Recursive function to parse the manifest and build the Manifest Data Model
        /// </summary>
        /// <param name="xmlNode">the XmlNode to read</param>
        /// <param name="itemTreeLevel">the level at which the node is at - used when parsing <item>s</param>
        /// <param name="itemParentIdentifier">the identifier attribute of the <item>'s parent <item> node</param>
        /// <param name="objectIdentifier">the identifier of an object that contains a supporting collection - used when adding <item>s
        /// to <organization>s, and <sequencing> to <manifest>, <organization>, and/or <item></param>
        /// <param name="objectType">the type of object that the objectIdentifier references; see description of objectIdentifier param</param>
        /// <returns>true/false</returns>
        private bool ParseManifest(XmlNode xmlNode, int itemTreeLevel, string itemParentIdentifier, string objectIdentifier, string objectType)
        {
            bool returnResult = true;


            if (xmlNode == null)
            {
                returnResult = false;
                return returnResult;
            }

            switch (xmlNode.NodeType)
            {
                case XmlNodeType.ProcessingInstruction:
                    break;
                case XmlNodeType.Element:

                    String parentNodeName = xmlNode.Name;

                    // <manifest> node
                    #region <manifest>
                    if (parentNodeName == "manifest")
                    {
                        // get the identifier and xml:base attributes of the <manifest> node
                        manifest.identifier = GetStringAttribute(xmlNode, "identifier", null);
                        manifest.xmlBase = GetStringAttribute(xmlNode, "xml:base", null);

                        // process direct children of the <manifest>
                        XmlNodeList manifestChildren = xmlNode.ChildNodes;

                        XmlNode metadataNode = FindNode(manifestChildren, "metadata");
                        XmlNode organizationsNode = FindNode(manifestChildren, "organizations");
                        XmlNode resourcesNode = FindNode(manifestChildren, "resources");
                        XmlNode sequencingCollectionNode = FindNode(manifestChildren, "imsss:sequencingCollection");

                        //Boolean flag for chacking the schema version is set or not
                        bool isSchemaversionFound = false;

                        // process <metadata> node - 0 or 1                        
                        if (metadataNode != null)
                        {
                            XmlNodeList metadataChildren = metadataNode.ChildNodes;
                            XmlNode schemaversionNode = FindNode(metadataChildren, "schemaversion");

                            if (schemaversionNode != null)
                            {
                                manifest.schemaversion = schemaversionNode.InnerText;
                                isSchemaversionFound = true;
                            }                            
                        }                        
                        if (!isSchemaversionFound & organizationsNode != null)
                        {
                            XmlNodeList organizationsNodeListing = organizationsNode.ChildNodes;
                            XmlNode organizationNode = FindNode(organizationsNodeListing, "organization");
                            if (organizationNode != null)
                            {
                                XmlNodeList organizationChildNodes = organizationNode.ChildNodes;
                                XmlNode metadataNodeInsideOrganization = FindNode(organizationChildNodes, "metadata");
                                if (metadataNodeInsideOrganization != null)
                                {
                                    XmlNodeList metadataChildrenForSchemaVersion = metadataNodeInsideOrganization.ChildNodes;
                                    XmlNode schemaversionNodeWithinOrganization = FindNode(metadataChildrenForSchemaVersion, "schemaversion");

                                    if (schemaversionNodeWithinOrganization != null)
                                    {
                                        manifest.schemaversion = schemaversionNodeWithinOrganization.InnerText;
                                        isSchemaversionFound = true;
                                    }
                                }
                            }
                        }
                        if (!isSchemaversionFound & resourcesNode != null)
                        {
                            XmlNodeList resourcesNodeListing = resourcesNode.ChildNodes;
                            XmlNode resourceForSchemaVersion = FindNode(resourcesNodeListing, "resource");
                            if (resourceForSchemaVersion != null)
                            {
                                XmlNodeList resourceChildForSchemaVersion = resourceForSchemaVersion.ChildNodes;
                                XmlNode metadataNodeInResourceForSchemaVersion = FindNode(resourceChildForSchemaVersion, "metadata");
                                if (metadataNodeInResourceForSchemaVersion != null)
                                {
                                    XmlNodeList metadataChildrenForSchemaVersion = metadataNodeInResourceForSchemaVersion.ChildNodes;
                                    XmlNode schemaversionNodeWithinResource = FindNode(metadataChildrenForSchemaVersion, "schemaversion");

                                    if (schemaversionNodeWithinResource != null)
                                    {
                                        manifest.schemaversion = schemaversionNodeWithinResource.InnerText;
                                        isSchemaversionFound = true;
                                    }
                                }
                            }
                        }
                        if (!isSchemaversionFound) // schemaversion not found at all, so assume it is 1.2 because 1.3 (2004) requires <schemaversion>
                        {                            
                            manifest.schemaversion = "1.2";
                            isSchemaversionFound = true;
                        }

                        // process <organizations> - 1 and only 1
                        if (organizationsNode != null)
                        {
                            // get the default attribute of the organizations node
                            manifest.defaultOrganization = GetStringAttribute(organizationsNode, "default", null);

                            XmlNodeList organizationsChildren = organizationsNode.ChildNodes;
                            ArrayList organizationNodes = FindNodes(organizationsChildren, "organization");

                            if (organizationNodes != null)
                            {
                                foreach (XmlNode organizationNode in organizationNodes)
                                {
                                    returnResult = ParseManifest(organizationNode, 0, null, null, null) && returnResult;
                                }
                            }
                        }

                        // process <resources> - 1 and only 1
                        if (resourcesNode != null)
                        {
                            // get the xml:base attribute of the organizations node
                            manifest.resourcesXmlBase = GetStringAttribute(resourcesNode, "xml:base", null);

                            XmlNodeList resourcesChildren = resourcesNode.ChildNodes;
                            ArrayList resourceNodes = FindNodes(resourcesChildren, "resource");

                            if (resourceNodes != null)
                            {
                                foreach (XmlNode resourceNode in resourceNodes)
                                {
                                    returnResult = ParseManifest(resourceNode, 0, null, null, null) && returnResult;
                                }
                            }
                        }

                        // process <imsss:sequencingCollection> - 0 or 1
                        if (sequencingCollectionNode != null)
                        {
                            XmlNodeList sequencingCollectionChildren = sequencingCollectionNode.ChildNodes;
                            ArrayList imsssSequencingNodes = FindNodes(sequencingCollectionChildren, "imsss:sequencing");

                            if (imsssSequencingNodes != null)
                            {
                                foreach (XmlNode imsssSequencingNode in imsssSequencingNodes)
                                {
                                    // no objectIdentifier needed, as this would be a direct child of our manifest object
                                    returnResult = ParseManifest(imsssSequencingNode, 0, null, null, "manifest") && returnResult;
                                }
                            }
                        }
                    }
                    #endregion
                    // <organization> node
                    #region <organization>
                    else if (parentNodeName == "organization")
                    {
                        // instansiate a new OrganizationDataModel item and add it
                        // to the manifest.organizations ArrayList
                        OrganizationDataModel organizationData = new OrganizationDataModel();

                        manifest.organizations.Add(organizationData);

                        // get the children of this <organization>
                        XmlNodeList organizationChildren = xmlNode.ChildNodes;

                        // get <organization> attributes
                        organizationData.identifier = GetStringAttribute(xmlNode, "identifier", null);
                        organizationData.objectivesGlobalToSystem = GetBoolAttribute(xmlNode, "adlseq:objectivesGlobalToSystem", true);
                        organizationData.sharedDataGlobalToSystem = GetBoolAttribute(xmlNode, "adlcp:sharedDataGlobalToSystem", true);
                        organizationData.structure = GetStringAttribute(xmlNode, "structure", "hierarchical");

                        // get <organization> -> <title> - 1 and only 1
                        XmlNode titleNode = FindNode(organizationChildren, "title");

                        if (titleNode != null)
                        {
                            organizationData.title = titleNode.InnerText;
                        }

                        // get <organization> -> <adlcp:completionThreshold> - 0 or 1
                        XmlNode adlcpCompletionThresholdNode = FindNode(organizationChildren, "adlcp:completionThreshold");

                        if (adlcpCompletionThresholdNode != null)
                        {
                            // instansiate a new CompletionThresholdDataModel
                            organizationData.adlcpCompletionThreshold = new CompletionThresholdDataModel();

                            organizationData.adlcpCompletionThreshold.isDefined = true; // Go back to this... We may need to redefine.

                            // SCORM 2004 3rd edition and lower uses the node value
                            // SCORM 2004 4th edition uses attribute values
                            if (adlcpCompletionThresholdNode.InnerText != "")
                            {
                                organizationData.adlcpCompletionThreshold.completionThreshold = Convert.ToDouble(adlcpCompletionThresholdNode.InnerText);
                            }
                            else
                            {
                                organizationData.adlcpCompletionThreshold.completedByMeasure = GetBoolAttribute(adlcpCompletionThresholdNode, "completedByMeasure", false);
                                organizationData.adlcpCompletionThreshold.minProgressMeasure = GetDoubleAttribute(adlcpCompletionThresholdNode, "minProgressMeasure", 1.0);
                                organizationData.adlcpCompletionThreshold.progressWeight = GetDoubleAttribute(adlcpCompletionThresholdNode, "progressWeight", 1.0);
                            }
                        }

                        //Added by chetu to create organzation as item in manfiest data model
                        returnResult = this.CreateItemStructure(xmlNode, itemTreeLevel, itemParentIdentifier, objectIdentifier, objectType, "organization", returnResult);
                        organizationCounter++;

                        // get <organization> -> <item> nodes - 1 or more
                        ArrayList itemNodes = FindNodes(organizationChildren, "item");

                        if (itemNodes != null)
                        {
                            foreach (XmlNode itemNode in itemNodes)
                            {
                                //changes done by chetu to send parentIdenfier as organizationData.identifier for level 1 parent items
                                returnResult = ParseManifest(itemNode, itemTreeLevel + 1, organizationData.identifier, organizationData.identifier, "organization") && returnResult;
                            }
                        }

                        // write treeHTML property
                        organizationData.treeHTML = WriteOrganizationTreeview(organizationData);

                        // get <organization> -> <imsss:sequencing> - 0 or 1
                        XmlNode imsssSequencingNode = FindNode(organizationChildren, "imsss:sequencing");

                        if (imsssSequencingNode != null)
                        {
                            // instansiate a new SequencingDataModel object
                            organizationData.imsssSequencing = new SequencingDataModel();

                            returnResult = ParseManifest(imsssSequencingNode, 0, null, organizationData.identifier, "organization") && returnResult;
                        }
                    }
                    #endregion
                    // <item> node
                    #region <item>
                    else if (parentNodeName == "item")
                    {
                        returnResult = this.CreateItemStructure(xmlNode, itemTreeLevel, itemParentIdentifier, objectIdentifier, objectType, "item", returnResult) && returnResult;
                    }
                    #endregion
                    // <resource> node
                    #region <resource>
                    else if (parentNodeName == "resource")
                    {
                        // we only care about <resource>s with href attributes
                        // so, only add those
                        if (GetStringAttribute(xmlNode, "href", null) != null)
                        {
                            // instansiate a new ResourceDataModel item and add it
                            // to the manifest.resources ArrayList
                            ResourceDataModel resourceData = new ResourceDataModel();

                            manifest.resources.Add(resourceData);

                            // get <resource> attributes
                            resourceData.adlcpScormType = GetStringAttribute(xmlNode, "adlcp:scormType", null);
                            resourceData.href = GetStringAttribute(xmlNode, "href", null);
                            resourceData.identifier = GetStringAttribute(xmlNode, "identifier", null);
                            resourceData.type = GetStringAttribute(xmlNode, "type", null);
                            resourceData.xmlBase = GetStringAttribute(xmlNode, "xml:base", null);
                        }
                    }
                    #endregion
                    // <sequencing> node
                    #region <imsss:sequencing>
                    else if (parentNodeName == "imsss:sequencing")
                    {
                        // instansiate a new SequencingDataModel item and add it
                        // to the ArrayList object for sequencing for the object 
                        // defined in the objectType and objectIdentifier parameters 
                        // of this function
                        SequencingDataModel sequencingData = new SequencingDataModel();

                        switch (objectType)
                        {
                            case "manifest":
                                manifest.sequencingCollection.Add(sequencingData);
                                break;
                            case "organization":
                                foreach (OrganizationDataModel organizationDataModel in manifest.organizations)
                                {
                                    if (organizationDataModel.identifier == objectIdentifier)
                                    {
                                        organizationDataModel.imsssSequencing = sequencingData;
                                    }
                                }
                                break;
                            case "item":
                                foreach (OrganizationDataModel organizationDataModel in manifest.organizations)
                                {
                                    foreach (ItemDataModel itemDataModel in organizationDataModel.items)
                                    {
                                        if (itemDataModel.identifier == objectIdentifier)
                                        {
                                            itemDataModel.imsssSequencing = sequencingData;
                                        }
                                    }
                                }
                                break;
                        }

                        // get the children of this <imsss:sequencing>
                        XmlNodeList sequencingChildren = xmlNode.ChildNodes;

                        // get <imsss:sequencing> attributes
                        sequencingData.id = GetStringAttribute(xmlNode, "ID", null);
                        sequencingData.idRef = GetStringAttribute(xmlNode, "IDRef", null);

                        // get <imsss:sequencing> -> <imsss:controlMode>
                        XmlNode controlModeNode = FindNode(sequencingChildren, "imsss:controlMode");

                        if (controlModeNode != null)
                        {
                            // instansiate a new ControlModeDataModel object
                            sequencingData.controlMode = new ControlModeDataModel();

                            sequencingData.controlMode.choice = GetBoolAttribute(controlModeNode, "choice", true);
                            sequencingData.controlMode.choiceExit = GetBoolAttribute(controlModeNode, "choiceExit", true);
                            sequencingData.controlMode.flow = GetBoolAttribute(controlModeNode, "flow", false);
                            sequencingData.controlMode.forwardOnly = GetBoolAttribute(controlModeNode, "forwardOnly", false);
                            sequencingData.controlMode.useCurrentAttemptObjectiveInfo = GetBoolAttribute(controlModeNode, "useCurrentAttemptObjectiveInfo", true);
                            sequencingData.controlMode.useCurrentAttemptProgressInfo = GetBoolAttribute(controlModeNode, "useCurrentAttemptProgressInfo", true);
                        }

                        // get <imsss:sequencing> -> <imsss:sequencingRules> then process 
                        // <imsss:preConditionRule>s, <imsss:exitConditionRule>s, and 
                        // <imsss:postConditionRule>s
                        XmlNode sequencingRulesNode = FindNode(sequencingChildren, "imsss:sequencingRules");

                        if (sequencingRulesNode != null)
                        {
                            XmlNodeList sequencingRulesChildren = sequencingRulesNode.ChildNodes;
                            ArrayList preConditionRuleNodes = FindNodes(sequencingRulesChildren, "imsss:preConditionRule");
                            ArrayList exitConditionRuleNodes = FindNodes(sequencingRulesChildren, "imsss:exitConditionRule");
                            ArrayList postConditionRuleNodes = FindNodes(sequencingRulesChildren, "imsss:postConditionRule");

                            // <imsss:preConditionRule>s
                            if (preConditionRuleNodes != null)
                            {
                                // loop through each <imsss:preConditionRule>
                                foreach (XmlNode preConditionRuleNode in preConditionRuleNodes)
                                {
                                    // instansiate a new SequencingDataModel item for this rule
                                    // and add it to the preConditionRules ArrayList
                                    SequencingRuleDataModel sequencingRuleData = new SequencingRuleDataModel();
                                    sequencingData.preConditionRules.Add(sequencingRuleData);

                                    // get the children of <imsss:preConditionRule>
                                    XmlNodeList preConditionRuleChildren = preConditionRuleNode.ChildNodes;

                                    // get the <imsss:ruleAction> node - 1 and only 1
                                    XmlNode ruleActionNode = FindNode(preConditionRuleChildren, "imsss:ruleAction");

                                    if (ruleActionNode != null)
                                    {
                                        sequencingRuleData.ruleAction = GetStringAttribute(ruleActionNode, "action", null);
                                    }

                                    // get the <imsss:ruleConditions> node - 1 and only 1
                                    XmlNode ruleConditionsNode = FindNode(preConditionRuleChildren, "imsss:ruleConditions");

                                    if (ruleConditionsNode != null)
                                    {
                                        // get attributes of <imsss:ruleConditions> and add to the sequencingRuleData object
                                        sequencingRuleData.conditionCombination = GetStringAttribute(ruleConditionsNode, "conditionCombination", "all");

                                        // get the children of <imsss:ruleConditions>
                                        XmlNodeList ruleConditionsChildren = ruleConditionsNode.ChildNodes;

                                        // get the <imsss:ruleCondition> nodes  - 1 or many
                                        ArrayList ruleConditionNodes = FindNodes(ruleConditionsChildren, "imsss:ruleCondition");

                                        // get the <imsss:ruleCondition>s and add them to the ruleConditions ArrayList
                                        if (ruleConditionNodes != null)
                                        {
                                            foreach (XmlNode ruleConditionNode in ruleConditionNodes)
                                            {
                                                RuleConditionDataModel ruleCondition = new RuleConditionDataModel();
                                                sequencingRuleData.ruleConditions.Add(ruleCondition);

                                                ruleCondition.condition = GetStringAttribute(ruleConditionNode, "condition", null);
                                                ruleCondition.conditionOperator = GetStringAttribute(ruleConditionNode, "operator", "noOp");
                                                ruleCondition.measureThreshold = GetDoubleAttribute(ruleConditionNode, "measureThreshold", -1.0000); // there is no default here, we need to figure something out
                                                ruleCondition.referencedObjective = GetStringAttribute(ruleConditionNode, "referencedObjective", null);
                                            }
                                        }
                                    }
                                }
                            }

                            // <imsss:exitConditionRule>s
                            if (exitConditionRuleNodes != null)
                            {
                                // loop through each <imsss:exitConditionRule>
                                foreach (XmlNode exitConditionRuleNode in exitConditionRuleNodes)
                                {
                                    // instansiate a new SequencingDataModel item for this rule
                                    // and add it to the exitConditionRules ArrayList
                                    SequencingRuleDataModel sequencingRuleData = new SequencingRuleDataModel();
                                    sequencingData.exitConditionRules.Add(sequencingRuleData);

                                    // get the children of <exitConditionRule>
                                    XmlNodeList exitConditionRuleChildren = exitConditionRuleNode.ChildNodes;

                                    // get the <imsss:ruleAction> node - 1 and only 1
                                    XmlNode ruleActionNode = FindNode(exitConditionRuleChildren, "imsss:ruleAction");

                                    if (ruleActionNode != null)
                                    {
                                        sequencingRuleData.ruleAction = GetStringAttribute(ruleActionNode, "action", null);
                                    }

                                    // get the <imsss:ruleConditions> node - 1 and only 1
                                    XmlNode ruleConditionsNode = FindNode(exitConditionRuleChildren, "imsss:ruleConditions");

                                    if (ruleConditionsNode != null)
                                    {
                                        // get attributes of <imsss:ruleConditions> and add to the sequencingRuleData object
                                        sequencingRuleData.conditionCombination = GetStringAttribute(ruleConditionsNode, "conditionCombination", "all");

                                        // get the children of <imsss:ruleConditions>
                                        XmlNodeList ruleConditionsChildren = ruleConditionsNode.ChildNodes;

                                        // get the <imsss:ruleCondition> nodes  - 1 or many
                                        ArrayList ruleConditionNodes = FindNodes(ruleConditionsChildren, "imsss:ruleCondition");

                                        // get the <imsss:ruleCondition>s and add them to the ruleConditions ArrayList
                                        if (ruleConditionNodes != null)
                                        {
                                            foreach (XmlNode ruleConditionNode in ruleConditionNodes)
                                            {
                                                RuleConditionDataModel ruleCondition = new RuleConditionDataModel();
                                                sequencingRuleData.ruleConditions.Add(ruleCondition);

                                                ruleCondition.condition = GetStringAttribute(ruleConditionNode, "condition", null);
                                                ruleCondition.conditionOperator = GetStringAttribute(ruleConditionNode, "operator", "noOp");
                                                ruleCondition.measureThreshold = GetDoubleAttribute(ruleConditionNode, "measureThreshold", -1.0000); // there is no default here, we need to figure something out
                                                ruleCondition.referencedObjective = GetStringAttribute(ruleConditionNode, "referencedObjective", null);
                                            }
                                        }
                                    }
                                }
                            }

                            // <imsss:postConditionRule>s
                            if (postConditionRuleNodes != null)
                            {
                                // loop through each <imsss:postConditionRule>
                                foreach (XmlNode postConditionRuleNode in postConditionRuleNodes)
                                {
                                    // instansiate a new SequencingDataModel item for this rule
                                    // and add it to the postConditionRules ArrayList
                                    SequencingRuleDataModel sequencingRuleData = new SequencingRuleDataModel();
                                    sequencingData.postConditionRules.Add(sequencingRuleData);

                                    // get the children of <imsss:postConditionRule>
                                    XmlNodeList postConditionRuleChildren = postConditionRuleNode.ChildNodes;

                                    // get the <imsss:ruleAction> node - 1 and only 1
                                    XmlNode ruleActionNode = FindNode(postConditionRuleChildren, "imsss:ruleAction");

                                    if (ruleActionNode != null)
                                    {
                                        sequencingRuleData.ruleAction = GetStringAttribute(ruleActionNode, "action", null);
                                    }

                                    // get the <imsss:ruleConditions> node - 1 and only 1
                                    XmlNode ruleConditionsNode = FindNode(postConditionRuleChildren, "imsss:ruleConditions");

                                    if (ruleConditionsNode != null)
                                    {
                                        // get attributes of <imsss:ruleConditions> and add to the sequencingRuleData object
                                        sequencingRuleData.conditionCombination = GetStringAttribute(ruleConditionsNode, "conditionCombination", "all");

                                        // get the children of <imsss:ruleConditions>
                                        XmlNodeList ruleConditionsChildren = ruleConditionsNode.ChildNodes;

                                        // get the <imsss:ruleCondition> nodes - 1 or many
                                        ArrayList ruleConditionNodes = FindNodes(ruleConditionsChildren, "imsss:ruleCondition");

                                        // get the <imsss:ruleCondition>s and add them to the ruleConditions ArrayList
                                        if (ruleConditionNodes != null)
                                        {
                                            foreach (XmlNode ruleConditionNode in ruleConditionNodes)
                                            {
                                                RuleConditionDataModel ruleCondition = new RuleConditionDataModel();
                                                sequencingRuleData.ruleConditions.Add(ruleCondition);

                                                ruleCondition.condition = GetStringAttribute(ruleConditionNode, "condition", null);
                                                ruleCondition.conditionOperator = GetStringAttribute(ruleConditionNode, "operator", "noOp");
                                                ruleCondition.measureThreshold = GetDoubleAttribute(ruleConditionNode, "measureThreshold", -1.0000); // there is no default here, we need to figure something out
                                                ruleCondition.referencedObjective = GetStringAttribute(ruleConditionNode, "referencedObjective", null);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // get <imsss:sequencing> -> <imsss:limitConditions> - 0 or 1
                        XmlNode limitConditionsNode = FindNode(sequencingChildren, "imsss:limitConditions");

                        if (limitConditionsNode != null)
                        {
                            // instansiate a new LimitConditionsDataModel object
                            sequencingData.limitConditions = new LimitConditionsDataModel();

                            sequencingData.limitConditions.attemptAbsoluteDurationLimit = GetStringAttribute(limitConditionsNode, "attemptAbsoluteDurationLimit", null);
                            sequencingData.limitConditions.attemptLimit = GetIntAttribute(limitConditionsNode, "attemptLimit", 0); // there is no default here, we need to figure something out                        
                            //Added by chetu 
                            sequencingData.limitConditions.attemptExperiencedDurationLimit = GetStringAttribute(limitConditionsNode, "attemptExperiencedDurationLimit", null);
                            sequencingData.limitConditions.activityAbsoluteDurationLimit = GetStringAttribute(limitConditionsNode, "activityAbsoluteDurationLimit", null);
                            sequencingData.limitConditions.activityExperiencedDurationLimit = GetStringAttribute(limitConditionsNode, "activityExperiencedDurationLimit", null);
                            sequencingData.limitConditions.beginTimeLimit = GetIntAttribute(limitConditionsNode, "beginTimeLimit", 0);
                            sequencingData.limitConditions.endTimeLimit = GetIntAttribute(limitConditionsNode, "endTimeLimit", 0);
                        }

                        // <imsss:sequencing> -> <imsss:auxiliaryResources> - WILL NOT BE IMPLEMENTED

                        // <imsss:sequencing> -> <imsss:rollupRules> - 0 or 1
                        XmlNode rollupRulesNode = FindNode(sequencingChildren, "imsss:rollupRules");

                        if (rollupRulesNode != null)
                        {
                            // instansiate a new RollupRulesDataModel object
                            sequencingData.rollupRules = new RollupRulesDataModel();

                            // get attributes of <imsss:rollupRules>
                            sequencingData.rollupRules.objectiveMeasureWeight = GetDoubleAttribute(rollupRulesNode, "objectiveMeasureWeight", 1.0000);
                            sequencingData.rollupRules.rollupObjectiveSatisfied = GetBoolAttribute(rollupRulesNode, "rollupObjectiveSatisfied", true);
                            sequencingData.rollupRules.rollupProgressCompletion = GetBoolAttribute(rollupRulesNode, "rollupProgressCompletion", true);

                            // get the children of <imsss:rollupRules>
                            XmlNodeList rollupRulesChildren = rollupRulesNode.ChildNodes;

                            // get <imsss:rollupRule> nodes - 0 or many
                            ArrayList rollupRuleNodes = FindNodes(rollupRulesChildren, "imsss:rollupRule");

                            if (rollupRuleNodes != null)
                            {
                                foreach (XmlNode rollupRuleNode in rollupRuleNodes)
                                {
                                    RollupRuleDataModel rollupRule = new RollupRuleDataModel();
                                    sequencingData.rollupRules.rollupRules.Add(rollupRule);

                                    // get attributes of <imsss:rollupRule>
                                    rollupRule.childActivitySet = GetStringAttribute(rollupRuleNode, "childActivitySet", "all");
                                    rollupRule.minimumCount = GetIntAttribute(rollupRuleNode, "minimumCount", 0);
                                    rollupRule.minimumPercent = GetDoubleAttribute(rollupRuleNode, "minimumPercent", 0.0000);

                                    // get the children of <imsss:rollupRule>
                                    XmlNodeList rollupRuleChildren = rollupRuleNode.ChildNodes;

                                    // get the <imsss:rollupAction> node - 1 and only 1
                                    XmlNode rollupActionNode = FindNode(rollupRuleChildren, "imsss:rollupAction");

                                    if (rollupActionNode != null)
                                    {
                                        rollupRule.rollupAction = GetStringAttribute(rollupActionNode, "action", null);
                                    }

                                    // get the <imsss:rollupConditions> node - 1 and only 1
                                    XmlNode rollupConditionsNode = FindNode(rollupRuleChildren, "imsss:rollupConditions");

                                    if (rollupConditionsNode != null)
                                    {
                                        // get attributes of <imsss:rollupConditions> and add to the rollupRule object
                                        rollupRule.conditionCombination = GetStringAttribute(rollupConditionsNode, "conditionCombination", "any");

                                        // get the children of <imsss:rollupConditions>
                                        XmlNodeList rollupConditionsChildren = rollupConditionsNode.ChildNodes;

                                        // get the <imsss:rollupCondition> nodes - 1 or many
                                        ArrayList rollupConditionNodes = FindNodes(rollupConditionsChildren, "imsss:rollupCondition");

                                        // get the <imsss:rollupCondition>s and add them to the rollupConditions ArrayList
                                        if (rollupConditionNodes != null)
                                        {
                                            foreach (XmlNode rollupConditionNode in rollupConditionNodes)
                                            {
                                                RollupConditionDataModel rollupCondition = new RollupConditionDataModel();
                                                rollupRule.rollupConditions.Add(rollupCondition);

                                                rollupCondition.condition = GetStringAttribute(rollupConditionNode, "condition", null);
                                                rollupCondition.conditionOperator = GetStringAttribute(rollupConditionNode, "operator", "noOp");
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // <imsss:sequencing> -> <imsss:objectives> - 0 or 1
                        XmlNode imsssObjectivesNode = FindNode(sequencingChildren, "imsss:objectives");

                        if (imsssObjectivesNode != null)
                        {
                            // instansiate a new IMSSSObjectivesDataModel object
                            sequencingData.imsssObjectives = new IMSSSObjectivesDataModel();

                            // get the children of <imsss:objectives>
                            XmlNodeList imsssObjectivesChildren = imsssObjectivesNode.ChildNodes;

                            // <imsss:primaryObjective> - 1 and only 1
                            XmlNode primaryObjectiveNode = FindNode(imsssObjectivesChildren, "imsss:primaryObjective");

                            if (primaryObjectiveNode != null && primaryObjectiveNode.Attributes.Count > 0)
                            {
                                // instansiate a new IMSSSObjectiveDataModel object
                                sequencingData.imsssObjectives.primaryObjective = new IMSSSObjectiveDataModel();

                                // get attributes of <imsss:primaryObjective>
                                sequencingData.imsssObjectives.primaryObjective.satisfiedByMeasure = GetBoolAttribute(primaryObjectiveNode, "satisfiedByMeasure", false);
                                sequencingData.imsssObjectives.primaryObjective.objectiveId = GetStringAttribute(primaryObjectiveNode, "objectiveID", "");
                                //added by chetu
                                sequencingData.imsssObjectives.primaryObjective.objectiveContributesToRollup = GetBoolAttribute(primaryObjectiveNode, "objectiveContributesToRollup", true);
                                // get the children of <imsss:primaryObjective>
                                XmlNodeList primaryObjectiveChildren = primaryObjectiveNode.ChildNodes;

                                // get <imsss:primaryObjective> -> <imsss:minNormalizedMeasure> - 0 or 1
                                XmlNode minNormalizedMeasureNode = FindNode(primaryObjectiveChildren, "imsss:minNormalizedMeasure");

                                if (minNormalizedMeasureNode != null)
                                {
                                    if (!String.IsNullOrWhiteSpace(minNormalizedMeasureNode.InnerText))
                                    { sequencingData.imsssObjectives.primaryObjective.minNormalizedMeasure = Convert.ToDouble(minNormalizedMeasureNode.InnerText); }
                                }

                                // get <imsss:mapInfo> nodes
                                ArrayList mapInfoNodes = FindNodes(primaryObjectiveChildren, "imsss:mapInfo");

                                // get the <imsss:mapInfo>s and add them to the mapInfo ArrayList
                                if (mapInfoNodes != null)
                                {
                                    foreach (XmlNode mapInfoNode in mapInfoNodes)
                                    {
                                        IMSSSMapInfoDataModel mapInfoData = new IMSSSMapInfoDataModel();
                                        sequencingData.imsssObjectives.primaryObjective.mapInfo.Add(mapInfoData);

                                        // get attributes of <imsss:mapInfo>
                                        mapInfoData.readNormalizedMeasure = GetBoolAttribute(mapInfoNode, "readNormalizedMeasure", true);
                                        mapInfoData.readSatisfiedStatus = GetBoolAttribute(mapInfoNode, "readSatisfiedStatus", true);
                                        mapInfoData.targetObjectiveId = GetStringAttribute(mapInfoNode, "targetObjectiveId", null);
                                        mapInfoData.writeNormalizedMeasure = GetBoolAttribute(mapInfoNode, "writeNormalizedMeasure", false);
                                        mapInfoData.writeSatisfiedStatus = GetBoolAttribute(mapInfoNode, "writeSatisfiedStatus", false);
                                    }
                                }
                            }

                            // <imsss:objective> - 0 or more
                            ArrayList objectiveNodes = FindNodes(imsssObjectivesChildren, "imsss:objective");

                            if (objectiveNodes != null)
                            {
                                foreach (XmlNode objectiveNode in objectiveNodes)
                                {
                                    IMSSSObjectiveDataModel objectiveData = new IMSSSObjectiveDataModel();
                                    sequencingData.imsssObjectives.objectives.Add(objectiveData);

                                    // get attributes of <imsss:objective>
                                    objectiveData.satisfiedByMeasure = GetBoolAttribute(objectiveNode, "satisfiedByMeasure", false);
                                    objectiveData.objectiveId = GetStringAttribute(objectiveNode, "objectiveID", "");
                                    //added by chetu
                                    objectiveData.objectiveContributesToRollup = GetBoolAttribute(objectiveNode, "objectiveContributesToRollup", false);
                                    // get the children of <imsss:objective>
                                    XmlNodeList objectiveChildren = objectiveNode.ChildNodes;

                                    // get <imsss:objective> -> <imsss:minNormalizedMeasure> - 0 or 1
                                    XmlNode minNormalizedMeasureNode = FindNode(objectiveChildren, "imsss:minNormalizedMeasure");

                                    if (minNormalizedMeasureNode != null)
                                    {
                                        if (!String.IsNullOrWhiteSpace(minNormalizedMeasureNode.InnerText))
                                        { objectiveData.minNormalizedMeasure = Convert.ToDouble(minNormalizedMeasureNode.InnerText); }
                                    }

                                    // get <imsss:mapInfo> nodes
                                    ArrayList mapInfoNodes = FindNodes(objectiveChildren, "imsss:mapInfo");

                                    // get the <imsss:mapInfo>s and add them to the mapInfo ArrayList
                                    if (mapInfoNodes != null)
                                    {
                                        foreach (XmlNode mapInfoNode in mapInfoNodes)
                                        {
                                            IMSSSMapInfoDataModel mapInfoData = new IMSSSMapInfoDataModel();
                                            objectiveData.mapInfo.Add(mapInfoData);

                                            // get attributes of <imsss:mapInfo>
                                            mapInfoData.readNormalizedMeasure = GetBoolAttribute(mapInfoNode, "readNormalizedMeasure", true);
                                            mapInfoData.readSatisfiedStatus = GetBoolAttribute(mapInfoNode, "readSatisfiedStatus", true);
                                            mapInfoData.targetObjectiveId = GetStringAttribute(mapInfoNode, "targetObjectiveId", null);
                                            mapInfoData.writeNormalizedMeasure = GetBoolAttribute(mapInfoNode, "writeNormalizedMeasure", false);
                                            mapInfoData.writeSatisfiedStatus = GetBoolAttribute(mapInfoNode, "writeSatisfiedStatus", false);
                                        }
                                    }
                                }
                            }
                        }

                        // get <imsss:sequencing> -> <imsss:randomizationControls> - 0 or 1
                        XmlNode randomizationControlsNode = FindNode(sequencingChildren, "imsss:randomizationControls");

                        if (randomizationControlsNode != null)
                        {
                            // instansiate a new RandomizationControlsDataModel object
                            sequencingData.randomizationControls = new RandomizationControlsDataModel();

                            sequencingData.randomizationControls.randomizationTiming = GetStringAttribute(randomizationControlsNode, "randomizationTiming", "never");
                            sequencingData.randomizationControls.reorderChildren = GetBoolAttribute(randomizationControlsNode, "reorderChildren", false);
                            sequencingData.randomizationControls.selectCount = GetIntAttribute(randomizationControlsNode, "selectCount", 0);  // there is no default here, we need to figure something out
                            sequencingData.randomizationControls.selectionTiming = GetStringAttribute(randomizationControlsNode, "selectionTiming", "never");
                        }

                        // get <imsss:sequencing> -> <imsss:deliveryControls> - 0 or 1
                        XmlNode deliveryControlsNode = FindNode(sequencingChildren, "imsss:deliveryControls");

                        if (deliveryControlsNode != null)
                        {
                            // instansiate a new DeliveryControlsDataModel object
                            sequencingData.deliveryControls = new DeliveryControlsDataModel();

                            sequencingData.deliveryControls.completionSetByContent = GetBoolAttribute(deliveryControlsNode, "completionSetByContent", false);
                            sequencingData.deliveryControls.objectiveSetByContent = GetBoolAttribute(deliveryControlsNode, "objectiveSetByContent", false);
                            sequencingData.deliveryControls.tracked = GetBoolAttribute(deliveryControlsNode, "tracked", true);
                        }

                        // get <imsss:sequencing> -> <adlseq:constrainedChoiceConsiderations> - 0 or 1
                        XmlNode constrainedChoiceConsiderationsNode = FindNode(sequencingChildren, "adlseq:constrainedChoiceConsiderations");

                        if (constrainedChoiceConsiderationsNode != null)
                        {
                            // instansiate a new ConstrainedChoiceConsiderationsDataModel object
                            sequencingData.adlseqConstrainedChoiceConsiderations = new ConstrainedChoiceConsiderationsDataModel();

                            sequencingData.adlseqConstrainedChoiceConsiderations.constrainChoice = GetBoolAttribute(constrainedChoiceConsiderationsNode, "constrainChoice", false);
                            sequencingData.adlseqConstrainedChoiceConsiderations.preventActivation = GetBoolAttribute(constrainedChoiceConsiderationsNode, "preventActivation", false);
                        }

                        // get <imsss:sequencing> -> <adlseq:rollupConsiderations> - 0 or 1
                        XmlNode rollupConsiderationsNode = FindNode(sequencingChildren, "adlseq:rollupConsiderations");

                        if (rollupConsiderationsNode != null)
                        {
                            // instansiate a new RollupConsiderationsDataModel object
                            sequencingData.adlseqRollupConsiderations = new RollupConsiderationsDataModel();

                            sequencingData.adlseqRollupConsiderations.measureSatisfactionIfActive = GetBoolAttribute(rollupConsiderationsNode, "measureSatisfactionIfActive", true);
                            sequencingData.adlseqRollupConsiderations.requiredForCompleted = GetStringAttribute(rollupConsiderationsNode, "requiredForCompleted", "always");
                            sequencingData.adlseqRollupConsiderations.requiredForIncomplete = GetStringAttribute(rollupConsiderationsNode, "requiredForIncomplete", "always");
                            sequencingData.adlseqRollupConsiderations.requiredForNotSatisfied = GetStringAttribute(rollupConsiderationsNode, "requiredForNotSatisfied", "always");
                            sequencingData.adlseqRollupConsiderations.requiredForSatisfied = GetStringAttribute(rollupConsiderationsNode, "requiredForSatisfied", "always");
                        }

                        // <imsss:sequencing> -> <adlseq:objectives> - 0 or 1
                        XmlNode adlseqObjectivesNode = FindNode(sequencingChildren, "adlseq:objectives");

                        if (adlseqObjectivesNode != null)
                        {
                            // instansiate a new ADLSEQObjectivesDataModel object
                            sequencingData.adlseqObjectives = new ADLSEQObjectivesDataModel();

                            // get the children of <adlseq:objectives>
                            XmlNodeList adlseqObjectivesChildren = adlseqObjectivesNode.ChildNodes;

                            // <adlseq:objective> - 0 or more
                            ArrayList objectiveNodes = FindNodes(adlseqObjectivesChildren, "adlseq:objective");

                            if (objectiveNodes != null)
                            {
                                foreach (XmlNode objectiveNode in objectiveNodes)
                                {
                                    ADLSEQObjectiveDataModel objectiveData = new ADLSEQObjectiveDataModel();
                                    sequencingData.adlseqObjectives.objectives.Add(objectiveData);

                                    // get attributes of <adlseq:objective>
                                    objectiveData.objectiveId = GetStringAttribute(objectiveNode, "objectiveID", null);

                                    // get the children of <adlseq:objective>
                                    XmlNodeList objectiveChildren = objectiveNode.ChildNodes;

                                    // get <adlseq:mapInfo> nodes
                                    ArrayList mapInfoNodes = FindNodes(objectiveChildren, "adlseq:mapInfo");

                                    // get the <adlseq:mapInfo>s and add them to the mapInfo ArrayList
                                    if (mapInfoNodes != null)
                                    {
                                        foreach (XmlNode mapInfoNode in mapInfoNodes)
                                        {
                                            ADLSEQMapInfoDataModel mapInfoData = new ADLSEQMapInfoDataModel();
                                            objectiveData.mapInfo.Add(mapInfoData);

                                            // get attributes of <adlseq:mapInfo>
                                            mapInfoData.readCompletionStatus = GetBoolAttribute(mapInfoNode, "readCompletionStatus", true);
                                            mapInfoData.readMaxScore = GetBoolAttribute(mapInfoNode, "readMaxScore", true);
                                            mapInfoData.readMinScore = GetBoolAttribute(mapInfoNode, "readMinScore", true);
                                            mapInfoData.readProgressMeasure = GetBoolAttribute(mapInfoNode, "readProgressMeasure", true);
                                            mapInfoData.readRawScore = GetBoolAttribute(mapInfoNode, "readRawScore", true);
                                            mapInfoData.targetObjectiveId = GetStringAttribute(mapInfoNode, "targetObjectiveId", null);
                                            mapInfoData.writeCompletionStatus = GetBoolAttribute(mapInfoNode, "writeCompletionStatus", false);
                                            mapInfoData.writeMaxScore = GetBoolAttribute(mapInfoNode, "writeMaxScore", false);
                                            mapInfoData.writeMinScore = GetBoolAttribute(mapInfoNode, "writeMinScore", false);
                                            mapInfoData.writeProgressMeasure = GetBoolAttribute(mapInfoNode, "writeProgressMeasure", false);
                                            mapInfoData.writeRawScore = GetBoolAttribute(mapInfoNode, "writeRawScore", false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case XmlNodeType.EntityReference:
                    break;
                case XmlNodeType.Comment:
                    break;
                case XmlNodeType.CDATA:
                    break;
                case XmlNodeType.Text:
                    break;
            }

            return returnResult;
        }
        #endregion

        #region WriteOrganizationTreeview
        /// <summary>
        /// Writes HTML <ul> structure for an organization's activity tree.
        /// </summary>
        /// <param name="organization">the organization to create the tree for</param>
        /// <returns>string of HTML</returns>
        private string WriteOrganizationTreeview(OrganizationDataModel organization)
        {
            string treeHTML = String.Empty;

            //Modified  By Chetu
            treeHTML += "<ul rel=\"open\" id=\"__ROOT__\" class=\"treeMenu\">";

            foreach (ItemDataModel item in organization.items)
            {
                //changes done by chetu so that organization.identifier is used instead of null coz now level 0 item is organization in manifest data model
                if (item.parentIdentifier == organization.identifier)
                    treeHTML += WriteTreeItems(organization.items, item);
            }

            treeHTML += "</ul>";

            return treeHTML;
        }
        #endregion

        #region WriteTreeItems
        /// <summary>
        /// Function to recursively write the HTML tree structure <ul> > <li>
        /// </summary>
        /// <param name="items">List of items</param>
        /// <param name="item">a sub item</param>
        /// <returns>string HTML</returns>
        private string WriteTreeItems(ArrayList items, ItemDataModel item)
        {
            string treeHTML = String.Empty;
            ArrayList subElements = new ArrayList();
            string activityStatusIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);

            foreach (ItemDataModel subItem in items)
            {
                if (item.identifier == subItem.parentIdentifier && subItem.isvisible == true)
                {
                    subElements.Add(subItem);
                }
                else if (item.identifier == subItem.parentIdentifier && subItem.isvisible == false)
                {
                    foreach (ItemDataModel childItem in items)
                    {
                        if (subItem.identifier == childItem.parentIdentifier && childItem.isvisible == true)
                        {
                            subElements.Add(childItem);
                        }
                    }
                }
            }

            if (subElements.Count > 0)
            {

                if (item.isvisible == true)
                {                    
                    var imgScoStatus = String.Format("<img id=\"img_status_" + item.identifier + "\" src=\"" + activityStatusIconPath + "\" />");

                    treeHTML += String.Format("<li>{0}<span id=\"__leaf__{1}\">{2}</span>", imgScoStatus, item.identifier, item.title);
                }

                if (item.isvisible == true)
                { treeHTML += "<ul>"; }

                foreach (ItemDataModel subItem in subElements)
                {
                    int subCount = 0;
                    foreach (ItemDataModel item1 in items)
                    {
                        if (item1.parentIdentifier == subItem.identifier)
                        {
                            subCount++;
                        }
                    }

                    if (subCount > 0)
                    {
                        treeHTML += WriteTreeItems(items, subItem);
                    }
                    else
                    {
                        if (subItem.isvisible == true)
                        {
                            var imgScoStatus = String.Format("<img id=\"img_status_" + subItem.identifier + "\"  src=\"" + activityStatusIconPath + "\" />");

                            treeHTML += String.Format("<li>{0}<span id=\"__leaf__{1}\">{2}</span></li>", imgScoStatus, subItem.identifier, subItem.title);
                        }
                    }
                }

                if (item.isvisible == true)
                { treeHTML += "</ul>"; }
            }
            else
            {
                if (item.isvisible == true)
                {
                    var imgScoStatus = String.Format("<img id=\"img_status_" + item.identifier + "\" src=\"" + activityStatusIconPath + "\" />");

                    treeHTML += String.Format("<li>{0}<span id=\"__leaf__{1}\">{2}</span>", imgScoStatus, item.identifier, item.title);
                }
            }

            if (item.isvisible == true)
            { treeHTML += "</li>"; }

            return treeHTML;
        }
        #endregion

        #region FindNode
        /// <summary>
        /// Locates a specific node form a node list and returns the node if found.
        /// Used in cases where SCORM CAM specifies 0 or 1 instances of a node
        /// </summary>
        /// <param name="xmlNodeList">the node list to look through</param>
        /// <param name="nodeName">the node to look for</param>
        /// <returns>XmlNode or null</returns>
        private XmlNode FindNode(XmlNodeList xmlNodeList, string nodeName)
        {
            for (int i = 0; i < xmlNodeList.Count; i++)
            {
                if (xmlNodeList.Item(i).Name == nodeName)
                {
                    return xmlNodeList.Item(i);
                }
            }

            return null;
        }
        #endregion

        #region FindNodes
        /// <summary>
        /// Locates a specific node form a node list and returns an array list of those nodes, if found.
        /// Used in cases where SCORM CAM specifies 0 or many instances of a node
        /// </summary>
        /// <param name="xmlNodeList">the node list to look through</param>
        /// <param name="nodeName">the node to look for</param>
        /// <returns>ArrayList of XmlNodes or null</returns>
        private ArrayList FindNodes(XmlNodeList xmlNodeList, string nodeName)
        {
            ArrayList xmlNodes = new ArrayList();

            for (int i = 0; i < xmlNodeList.Count; i++)
            {
                if (xmlNodeList.Item(i).Name == nodeName)
                {
                    xmlNodes.Add(xmlNodeList.Item(i));
                }
            }

            if (xmlNodes.Count > 0)
            { return xmlNodes; }
            else
            { return null; }
        }
        #endregion

        #region GetStringAttribute
        /// <summary>
        /// Gets a string value of an XmlNode attribute
        /// </summary>
        /// <param name="xmlNode">the XmlNode</param>
        /// <param name="attributeName">the name of the attribute</param>
        /// <param name="defaultValue">the default value</param>
        /// <returns>string value of attribute</returns>
        private string GetStringAttribute(XmlNode xmlNode, string attributeName, string defaultValue)
        {
            if (attributeName.Equals("targetObjectiveId"))
            {
                attributeName = "targetObjectiveID";
            }

            if (xmlNode.Attributes[attributeName] != null)
            {
                string dirtyString = System.Web.HttpContext.Current.Server.UrlDecode(xmlNode.Attributes.GetNamedItem(attributeName).Value).Trim();
                return System.Text.RegularExpressions.Regex.Replace(dirtyString, @"\s+", " ");
            }

            return defaultValue;
        }
        #endregion

        #region GetBoolAttribute
        /// <summary>
        /// Gets a boolean value of an XmlNode attribute
        /// </summary>
        /// <param name="xmlNode">the XmlNode</param>
        /// <param name="attributeName">the name of the attribute</param>
        /// <param name="defaultValue">the default value</param>
        /// <returns>noolean value of attribute</returns>
        private bool GetBoolAttribute(XmlNode xmlNode, string attributeName, bool defaultValue)
        {
            if (xmlNode.Attributes[attributeName] != null)
            {
                return Convert.ToBoolean(xmlNode.Attributes.GetNamedItem(attributeName).Value);
            }

            return defaultValue;
        }
        #endregion

        #region GetDoubleAttribute
        /// <summary>
        /// Gets a double value of an XmlNode attribute
        /// </summary>
        /// <param name="xmlNode">the XmlNode</param>
        /// <param name="attributeName">the name of the attribute</param>
        /// <param name="defaultValue">the default value</param>
        /// <returns>double value of attribute</returns>
        private double GetDoubleAttribute(XmlNode xmlNode, string attributeName, double defaultValue)
        {
            if (xmlNode.Attributes[attributeName] != null)
            {
                return Convert.ToDouble(xmlNode.Attributes.GetNamedItem(attributeName).Value);
            }

            return defaultValue;
        }
        #endregion

        #region GetIntAttribute
        /// <summary>
        /// Gets an integer value of an XmlNode attribute
        /// </summary>
        /// <param name="xmlNode">the XmlNode</param>
        /// <param name="attributeName">the name of the attribute</param>
        /// <param name="defaultValue">the default value</param>
        /// <returns>integer value of attribute</returns>
        private int GetIntAttribute(XmlNode xmlNode, string attributeName, int defaultValue)
        {
            if (xmlNode.Attributes[attributeName] != null)
            {

                return Convert.ToInt32(xmlNode.Attributes.GetNamedItem(attributeName).Value);


            }

            return defaultValue;
        }
        #endregion

        #region SerializeManifestToJSON
        /// <summary>
        /// Serializes the Manifest Data Model object to JSON
        /// </summary>
        /// <returns>JSON string</returns>
        private string SerializeManifestToJSON()
        {
            MemoryStream outputStream = new MemoryStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(ManifestDataModel));
            serializer.WriteObject(outputStream, manifest);

            outputStream.Position = 0;

            StreamReader sr = new StreamReader(outputStream);
            string serializedData = sr.ReadToEnd();

            outputStream.Close();
            sr.Close();

            return serializedData;
        }
        #endregion

        #region CreateItemStructure
        /// <summary>
        /// This function is used to initialize item data model values. this function is used for creating normal items as well as organzation element as level 0 item
        /// </summary>
        /// <param name="xmlNode"></param>
        /// <param name="itemTreeLevel"></param>
        /// <param name="itemParentIdentifier"></param>
        /// <param name="objectIdentifier"></param>
        /// <param name="objectType"></param>
        /// <param name="itemType"></param>
        /// <param name="returnResult"></param>
        /// <returns></returns>
        private bool CreateItemStructure(XmlNode xmlNode, int itemTreeLevel, string itemParentIdentifier, string objectIdentifier, string objectType, string itemType, bool returnResult)
        {

            // instansiate a new ItemDataModel item and add it
            // to the manifest.organizations.items ArrayList
            // where the manifest.organizations ArrayList item
            // has the identifier property equal to the value
            // of the objectIdentifier parameter
            ItemDataModel itemData = new ItemDataModel();

            if (itemType == "organization")
            {
                OrganizationDataModel org = (OrganizationDataModel)manifest.organizations[organizationCounter];
                org.items.Add(itemData);

                itemData.identifier = GetStringAttribute(xmlNode, "identifier", null);
                itemData.level = itemTreeLevel;
                itemData.isvisible = GetBoolAttribute(xmlNode, "isvisible", true);
                itemData.parentIdentifier = itemParentIdentifier;

            }
            else
            {
                foreach (OrganizationDataModel organizationDataModel in manifest.organizations)
                {
                    if (organizationDataModel.identifier == objectIdentifier)
                    {
                        organizationDataModel.items.Add(itemData);
                    }
                }
            }
            // get the children of this <item>
            XmlNodeList itemChildren = xmlNode.ChildNodes;

            // get <item> attributes
            itemData.identifier = GetStringAttribute(xmlNode, "identifier", null);
            itemData.identifierref = GetStringAttribute(xmlNode, "identifierref", null);
            itemData.isvisible = GetBoolAttribute(xmlNode, "isvisible", true);
            itemData.parameters = GetStringAttribute(xmlNode, "parameters", null);

            // populate itemData properties that we use (not SCORM spec)
            itemData.level = itemTreeLevel;
            itemData.parentIdentifier = itemParentIdentifier;

            // get <item> -> <title> - 1 and only 1
            XmlNode titleNode = FindNode(itemChildren, "title");

            if (titleNode != null)
            {
                itemData.title = titleNode.InnerText;
            }

            if (manifest.schemaversion != null && manifest.schemaversion == "1.2")
            {
                // get <item> -> <title> - 1 and only 1
                XmlNode prerequisitesNode = FindNode(itemChildren, "adlcp:prerequisites");

                if (prerequisitesNode != null)
                {
                    itemData.adlcpPrerequisites = prerequisitesNode.InnerText;
                }
            }

            // get <item> -> <adlcp:timeLimitAction> - 0 or 1
            XmlNode adlcpTimeLimitActionNode = FindNode(itemChildren, "adlcp:timeLimitAction");

            if (adlcpTimeLimitActionNode != null)
            {
                itemData.adlcpTimeLimitAction = adlcpTimeLimitActionNode.InnerText;
            }

            // get <item> -> <adlcp:dataFromLMS> - 0 or 1
            XmlNode adlcpDataFromLMSNode = FindNode(itemChildren, "adlcp:datafromlms");

            if (adlcpDataFromLMSNode != null)
            {
                itemData.adlcpDataFromLMS = adlcpDataFromLMSNode.InnerText;
            }

            // get <item> -> <adlcp:completionThreshold> - 0 or 1
            XmlNode adlcpCompletionThresholdNode = FindNode(itemChildren, "adlcp:completionThreshold");

            if (adlcpCompletionThresholdNode != null)
            {
                // instansiate a new CompletionThresholdDataModel object
                itemData.adlcpCompletionThreshold = new CompletionThresholdDataModel();

                itemData.adlcpCompletionThreshold.isDefined = true; // Go back to this... We may need to redefine.

                // SCORM 2004 3rd edition and lower uses the node value
                // SCORM 2004 4th edition uses attribute values
                if (adlcpCompletionThresholdNode.InnerText != "")
                {
                    itemData.adlcpCompletionThreshold.completionThreshold = Convert.ToDouble(adlcpCompletionThresholdNode.InnerText);
                }
                else
                {
                    itemData.adlcpCompletionThreshold.completedByMeasure = GetBoolAttribute(adlcpCompletionThresholdNode, "completedByMeasure", false);
                    itemData.adlcpCompletionThreshold.minProgressMeasure = GetDoubleAttribute(adlcpCompletionThresholdNode, "minProgressMeasure", 1.0);
                    itemData.adlcpCompletionThreshold.progressWeight = GetDoubleAttribute(adlcpCompletionThresholdNode, "progressWeight", 1.0);
                }
            }

            // get <item> -> <adlcp:data> - 0 or 1
            XmlNode adlcpDataNode = FindNode(itemChildren, "adlcp:data");

            if (adlcpDataNode != null)
            {
                XmlNodeList adlcpDataChildren = adlcpDataNode.ChildNodes;

                ArrayList adlcpDataMapNodes = FindNodes(adlcpDataChildren, "adlcp:map");

                if (adlcpDataMapNodes != null)
                {
                    foreach (XmlNode adlcpDataMapNode in adlcpDataMapNodes)
                    {
                        // instansiate a new ADLCPDataMapDataModel object
                        ADLCPDataMapDataModel adlcpDataMapData = new ADLCPDataMapDataModel();

                        // get the attributes
                        adlcpDataMapData.targetId = GetStringAttribute(adlcpDataMapNode, "targetID", null);
                        adlcpDataMapData.readSharedData = GetBoolAttribute(adlcpDataMapNode, "readSharedData", true);
                        adlcpDataMapData.writeSharedData = GetBoolAttribute(adlcpDataMapNode, "writeSharedData", true);

                        itemData.adlcpData.Add(adlcpDataMapData);
                    }
                }
            }

            // get <item> -> <adlnav:presentation> - 0 or 1
            XmlNode adlnavPresentationNode = FindNode(itemChildren, "adlnav:presentation");

            if (adlnavPresentationNode != null)
            {
                // instansiate a new PresentationDataModel object
                itemData.adlnavPresentation = new PresentationDataModel();

                XmlNodeList adlnavPresentationChildren = adlnavPresentationNode.ChildNodes;

                XmlNode navigationInterfaceNode = FindNode(adlnavPresentationChildren, "adlnav:navigationInterface");

                if (navigationInterfaceNode != null)
                {
                    XmlNodeList navigationInterfaceChildren = navigationInterfaceNode.ChildNodes;

                    ArrayList hideLMSUINodes = FindNodes(navigationInterfaceChildren, "adlnav:hideLMSUI");

                    if (hideLMSUINodes != null)
                    {
                        foreach (XmlNode hideLMSUINode in hideLMSUINodes)
                        {
                            itemData.adlnavPresentation.hideLMSUI.Add(hideLMSUINode.InnerText);
                        }
                    }
                }
            }

            // get <item> -> <imsss:sequencing> - 0 or 1
            XmlNode imsssSequencingNode = FindNode(itemChildren, "imsss:sequencing");

            if (imsssSequencingNode != null)
            {
                // instansiate a new SequencingDataModel object
                itemData.imsssSequencing = new SequencingDataModel();

                returnResult = ParseManifest(imsssSequencingNode, 0, null, itemData.identifier, "item") && returnResult;
            }

            if (itemType == "item")
            {
                // get <item> -> <item> nodes - 0 or more - CHILD <item>s
                ArrayList itemNodes = FindNodes(itemChildren, "item");

                if (itemNodes != null)
                {
                    foreach (XmlNode itemNode in itemNodes)
                    {
                        returnResult = ParseManifest(itemNode, itemTreeLevel + 1, itemData.identifier, objectIdentifier, "organization") && returnResult;
                    }
                }
            }

            return returnResult;
        }
        #endregion

        #endregion
    }

}