﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Xml;
using Asentia.Common;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages._Util
{
    /// <summary>
    /// Web services for Asentia Widgets.
    /// </summary>
    [WebService(Description = "Web services for Asentia Widgets.", Namespace = "http://default.asentialms.com/WidgetService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class WidgetService : WebService
    {
        #region SaveDashboardConfiguration
        [WebMethod(Description = "Saves a user's dashboard configuration file.", EnableSession=true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SaveDashboardConfiguration(string layout, string column1Width, string column2Width, string[] columnOneWidgets, string[] columnTwoWidgets)
        {
            // initialize return message to "fail"
            string returnMessage = "fail";

            // dashboard configuration file name      
            string dashboard_config_file = "Dashboard.xml";

            // retired configuration file names
            string administrator_config_file = "AdministratorDashboard.xml";
            string learner_config_file = "LearnerDashboard.xml";
            string reporter_config_file = "ReporterDashboard.xml";

            // if there is no session, return with "fail" message
            if (AsentiaSessionState.IdSiteUser == 0 || AsentiaSessionState.IdSite == 0)
            { return returnMessage; }

            // build the xml from the JSON that was passed and save it
            try
            {
                // create xml document
                XmlDocument xmlDocument = new XmlDocument();

                // build dashboard node and attributes
                XmlElement dashboardElement = xmlDocument.CreateElement("dashboard");

                // layout attribute
                dashboardElement.SetAttribute("layout", layout);

                // if the column width variables are set, write xml attributes for those
                if (!String.IsNullOrWhiteSpace(column1Width) && !String.IsNullOrWhiteSpace(column2Width))
                {
                    dashboardElement.SetAttribute("column1Width", column1Width);
                    dashboardElement.SetAttribute("column2Width", column2Width);
                }

                xmlDocument.AppendChild(dashboardElement);

                // params for parsing the JSON arrays
                int i;
                char[] paramDelimiter = { '|' };

                // loop through column 1 array, create xml, and append
                for (i = 0; i < columnOneWidgets.Length; i++)
                {
                    string[] widgetParams = columnOneWidgets[i].ToString().Split(paramDelimiter);
                    XmlElement columnOneWidgetElement = xmlDocument.CreateElement("widget");
                    columnOneWidgetElement.SetAttribute("id", widgetParams[0]);
                    columnOneWidgetElement.SetAttribute("column", "1");
                    columnOneWidgetElement.SetAttribute("isCollapsed", widgetParams[1]);
                    dashboardElement.AppendChild(columnOneWidgetElement);
                }

                // loop through column 2 array, create xml, and append
                for (i = 0; i < columnTwoWidgets.Length; i++)
                {
                    string[] widgetParams = columnTwoWidgets[i].ToString().Split(paramDelimiter);
                    XmlElement columnTwoWidgetElement = xmlDocument.CreateElement("widget");
                    columnTwoWidgetElement.SetAttribute("id", widgetParams[0]);
                    columnTwoWidgetElement.SetAttribute("column", "2");
                    columnTwoWidgetElement.SetAttribute("isCollapsed", widgetParams[1]);
                    dashboardElement.AppendChild(columnTwoWidgetElement);
                }

                // get the path to save the xml file to and save it
                string filePath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_USERS_ROOT
                                                                     + AsentiaSessionState.IdSiteUser
                                                                     + "/" + dashboard_config_file);
                xmlDocument.Save(filePath);

                // delete retired learner dashboard config file
                string learnerFilePath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_USERS_ROOT
                                                                     + AsentiaSessionState.IdSiteUser
                                                                     + "/" + learner_config_file);

                if (File.Exists(learnerFilePath))
                { File.Delete(learnerFilePath); }

                // delete retired administrator dashboard config file
                string adminFilePath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_USERS_ROOT
                                                                     + AsentiaSessionState.IdSiteUser
                                                                     + "/" + administrator_config_file);
                if (File.Exists(adminFilePath))
                { File.Delete(adminFilePath); }

                // delete retired reporter dashboard config file
                string reportFilePath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_USERS_ROOT
                                                                     + AsentiaSessionState.IdSiteUser
                                                                     + "/" + reporter_config_file);

                if (File.Exists(reportFilePath))
                { File.Delete(reportFilePath); }

                // return
                returnMessage = "success";
                return returnMessage;
            }
            // catch exception and return failure
            catch(Exception ex)
            { return returnMessage; }
        }
        #endregion

        #region _WallMessagesJsonDataStruct
        public struct _WallMessagesJsonData
        {
            public bool actionSuccessful;
            public string html;
            public string exception;
            public string lastRecord;
            public int? idMessage;
            public int? idParentMessage;
            public string feedType;
            public bool hasRecords;
        }
        #endregion

        #region BuildFeedWidgetMessages
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public _WallMessagesJsonData BuildFeedWidgetMessages(DateTime dtQuery, bool getMessagesNewerThanDtQuery, bool isInitialLoad)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            DateTime lastRecord;
            DataTable messages = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                // get the messages from the database object
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@pageSize", 10, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@dtQuery", dtQuery, SqlDbType.DateTime, 8, ParameterDirection.Input);
                databaseObject.AddParameter("@getMessagesNewerThanDtQuery", getMessagesNewerThanDtQuery, SqlDbType.Bit, 1, ParameterDirection.Input);
                databaseObject.AddParameter("@dtLastRecord", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Widget.Feed.GetMessagesForUser]", true);
                messages.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                lastRecord = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtLastRecord"].Value);

                // if there are no messages and this is the initial load, display a "no messages" message
                if (messages.Rows.Count == 0 && isInitialLoad)
                {
                    Panel noMessagesMessageContainer = new Panel();
                    noMessagesMessageContainer.ID = "FeedWidget_NoMessagesMessageContainer";
                    noMessagesMessageContainer.CssClass = "centered";

                    Literal noMessagesMessage = new Literal();
                    noMessagesMessage.Text = _GlobalResources.ThereAreNoMessagesInYourFeed;
                    noMessagesMessageContainer.Controls.Add(noMessagesMessage);

                    // write the data to a text writer for json output
                    TextWriter textWriter = new StringWriter();
                    HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                    noMessagesMessageContainer.RenderControl(htmlTextWriter);

                    jsonData.html += textWriter.ToString();
                }

                foreach (DataRow row in messages.Rows)
                {
                    int idFeedMessage = Convert.ToInt32(row["idFeedMessage"]);
                    int feedObjectId = Convert.ToInt32(row["feedObjectId"]);
                    string feedType = row["feedType"].ToString();
                    int idMessageAuthor = Convert.ToInt32(row["idAuthor"]);

                    Panel messageContainer = new Panel();
                    messageContainer.ID = "FeedWidget_MessageContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageContainer.CssClass = "MessageContainer";

                    // MESSAGE HEADER

                    Panel messageHeaderContainer = new Panel();
                    messageHeaderContainer.ID = "FeedWidget_MessageHeaderContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageHeaderContainer.CssClass = "MessageHeaderContainer";

                    // avatar
                    Panel messageSenderAvatarContainer = new Panel();
                    messageSenderAvatarContainer.ID = "FeedWidget_MessageSenderAvatarContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageSenderAvatarContainer.CssClass = "MessageAvatar";

                    Image messageSenderAvatar = new Image();
                    messageSenderAvatar.ID = "FeedWidget_MessageSenderAvatar_" + feedType + "_" + idFeedMessage.ToString();
                    messageSenderAvatar.CssClass = "MediumIcon";
                    messageSenderAvatar.AlternateText = row["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                    if (!String.IsNullOrWhiteSpace(row["avatar"].ToString()))
                    { messageSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(row["idAuthor"]) + "/" + row["avatar"].ToString(); }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(row["gender"].ToString()) && row["gender"].ToString() == "f")
                        { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                        else
                        { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                    }

                    messageSenderAvatarContainer.Controls.Add(messageSenderAvatar);

                    // attach avatar to header container
                    messageHeaderContainer.Controls.Add(messageSenderAvatarContainer);

                    // sender information
                    Panel messageSenderInformationContainer = new Panel();
                    messageSenderInformationContainer.ID = "FeedWidget_MessageSenderInformationContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageSenderInformationContainer.CssClass = "MessageSenderInformationContainer";

                    Panel messageSenderNameContainer = new Panel();
                    messageSenderNameContainer.ID = "FeedWidget_MessageSenderNameContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageSenderNameContainer.CssClass = "MessageSenderNameContainer";

                    Literal messageSenderName = new Literal();
                    messageSenderName.Text = row["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                    messageSenderNameContainer.Controls.Add(messageSenderName);
                    messageSenderInformationContainer.Controls.Add(messageSenderNameContainer);

                    Panel messageSentDateTimeContainer = new Panel();
                    messageSentDateTimeContainer.ID = "FeedWidget_MessageSentDateTimeContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageSentDateTimeContainer.CssClass = "MessageSentDateTimeContainer";

                    DateTime messageTimestamp = Convert.ToDateTime(row["timestamp"]);
                    messageTimestamp = TimeZoneInfo.ConvertTimeFromUtc(messageTimestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                    Literal messageSentDateTime = new Literal();
                    messageSentDateTime.Text = messageTimestamp.ToString();


                    messageSentDateTimeContainer.Controls.Add(messageSentDateTime);
                    messageSenderInformationContainer.Controls.Add(messageSentDateTimeContainer);

                    Panel messagePostedInContainer = new Panel();
                    messagePostedInContainer.ID = "FeedWidget_MessagePostedInContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messagePostedInContainer.CssClass = "MessageSentDateTimeContainer";

                    Literal messagePostedIn = new Literal();
                    messagePostedIn.Text = _GlobalResources.PostedIn + ": ";
                    messagePostedInContainer.Controls.Add(messagePostedIn);

                    HyperLink messagePostedInLink = new HyperLink();
                    messagePostedInLink.ID = "FeedWidget_MessagePostedInLink_" + feedType + "_" + idFeedMessage.ToString();

                    if (feedType == "group")
                    {
                        messagePostedInLink.Text = _GlobalResources.Group + " - " + row["feedObjectName"].ToString();
                        messagePostedInLink.NavigateUrl = "/groups/Wall.aspx?id=" + feedObjectId.ToString();
                        messagePostedInContainer.Controls.Add(messagePostedInLink);
                    }
                    else if (feedType == "course")
                    {
                        messagePostedInLink.Text = _GlobalResources.Course + " - " + row["feedObjectName"].ToString();
                        messagePostedInLink.NavigateUrl = "/courses/Wall.aspx?id=" + feedObjectId.ToString();
                        messagePostedInContainer.Controls.Add(messagePostedInLink);
                    }
                    else
                    { }

                    messageSenderInformationContainer.Controls.Add(messagePostedInContainer);

                    // attach sender information to header container
                    messageHeaderContainer.Controls.Add(messageSenderInformationContainer);

                    // approve and delete/dis-approve buttons
                    Panel messageActionButtonsContainer = new Panel();
                    messageActionButtonsContainer.ID = "FeedWidget_MessageActionButtonsContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                    // NO APPROVE BUTTON HERE

                    // delete button - only the message's author can delete from here
                    if (idMessageAuthor == AsentiaSessionState.IdSiteUser)
                    {
                        Panel messageDeleteButtonContainer = new Panel();
                        messageDeleteButtonContainer.ID = "FeedWidget_MessageDeleteButtonContainer_" + feedType + "_" + idFeedMessage.ToString();

                        Image messageDeleteButton = new Image();
                        messageDeleteButton.ID = "FeedWidget_MessageDeleteButton_" + feedType + "_" + idFeedMessage.ToString();
                        messageDeleteButton.CssClass = "XSmallIcon";
                        messageDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        messageDeleteButton.AlternateText = _GlobalResources.Delete;
                        messageDeleteButton.Style.Add("cursor", "pointer");
                        messageDeleteButton.Attributes.Add("onclick", "DeleteFeedWidgetMessage(\"" + feedType + "\", \"" + idFeedMessage.ToString() + "\");");

                        messageDeleteButtonContainer.Controls.Add(messageDeleteButton);
                        messageActionButtonsContainer.Controls.Add(messageDeleteButtonContainer);
                    }

                    // attach buttons container to header container
                    messageHeaderContainer.Controls.Add(messageActionButtonsContainer);

                    // attach header to message container
                    messageContainer.Controls.Add(messageHeaderContainer);

                    // MESSAGE DATA

                    Panel messageDataContainer = new Panel();
                    messageDataContainer.ID = "FeedWidget_MessageDataContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageDataContainer.CssClass = "MessageDataContainer";

                    Literal messageData = new Literal();
                    messageData.Text = row["message"].ToString();

                    messageDataContainer.Controls.Add(messageData);
                    messageContainer.Controls.Add(messageDataContainer);

                    // COMMENTS

                    Panel messageCommentsContainer = new Panel();
                    messageCommentsContainer.ID = "FeedWidget_MessageCommentsContainer_" + feedType + "_" + idFeedMessage.ToString();

                    DataTable messageComments = new DataTable();

                    if (feedType == "group")
                    { messageComments = LMS.Library.GroupFeedMessage.GetComments(idFeedMessage, true); }
                    else if (feedType == "course")
                    { messageComments = LMS.Library.CourseFeedMessage.GetComments(idFeedMessage, true); }
                    else
                    { }

                    foreach (DataRow commentRow in messageComments.Rows)
                    {
                        int idFeedMessageComment = 0;

                        if (feedType == "group")
                        { idFeedMessageComment = Convert.ToInt32(commentRow["idGroupFeedMessage"]); }
                        else if (feedType == "course")
                        { idFeedMessageComment = Convert.ToInt32(commentRow["idCourseFeedMessage"]); }
                        else
                        { }

                        int idCommentAuthor = Convert.ToInt32(commentRow["idAuthor"]);

                        Panel messageCommentContainer = new Panel();
                        messageCommentContainer.ID = "FeedWidget_MessageCommentContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                        messageCommentContainer.CssClass = "MessageCommentContainer";

                        // avatar
                        Panel messageCommentSenderAvatarContainer = new Panel();
                        messageCommentSenderAvatarContainer.ID = "FeedWidget_MessageCommentSenderAvatarContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                        messageCommentSenderAvatarContainer.CssClass = "MessageCommentAvatar";

                        Image messageCommentSenderAvatar = new Image();
                        messageCommentSenderAvatar.ID = "FeedWidget_MessageCommentSenderAvatar_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                        messageCommentSenderAvatar.CssClass = "SmallIcon";
                        messageCommentSenderAvatar.AlternateText = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                        if (!String.IsNullOrWhiteSpace(commentRow["avatar"].ToString()))
                        { messageCommentSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(commentRow["idAuthor"]) + "/" + commentRow["avatar"].ToString(); }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(commentRow["gender"].ToString()) && commentRow["gender"].ToString() == "f")
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                            else
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                        }

                        messageCommentSenderAvatarContainer.Controls.Add(messageCommentSenderAvatar);

                        // attach avatar to container
                        messageCommentContainer.Controls.Add(messageCommentSenderAvatarContainer);

                        // comment data
                        Panel messageCommentDataContainer = new Panel();
                        messageCommentDataContainer.ID = "FeedWidget_MessageCommentDataContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                        messageCommentDataContainer.CssClass = "MessageCommentDataContainer";

                        Panel messageCommentData = new Panel();
                        messageCommentData.ID = "FeedWidget_MessageCommentData_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                        messageCommentData.CssClass = "MessageCommentData";

                        Label messageCommentSenderName = new Label();
                        messageCommentSenderName.CssClass = "MessageCommentSenderName";
                        messageCommentSenderName.Text = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);
                        messageCommentData.Controls.Add(messageCommentSenderName);

                        Label messageCommentMessage = new Label();
                        messageCommentMessage.Text = commentRow["message"].ToString();
                        messageCommentData.Controls.Add(messageCommentMessage);

                        // approve and delete/dis-approve buttons
                        Panel messageCommentActionButtonsContainer = new Panel();
                        messageCommentActionButtonsContainer.ID = "FeedWidget_MessageCommentActionButtonsContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                        messageCommentActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                        // NO APPROVE BUTTON HERE

                        // delete button
                        if (idCommentAuthor == AsentiaSessionState.IdSiteUser)
                        {
                            Panel messageCommentDeleteButtonContainer = new Panel();
                            messageCommentDeleteButtonContainer.ID = "FeedWidget_MessageCommentDeleteButtonContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();

                            Image messageCommentDeleteButton = new Image();
                            messageCommentDeleteButton.ID = "FeedWidget_MessageCommentDeleteButton_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                            messageCommentDeleteButton.CssClass = "XSmallIcon";
                            messageCommentDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            messageCommentDeleteButton.AlternateText = _GlobalResources.Delete;
                            messageCommentDeleteButton.Style.Add("cursor", "pointer");
                            messageCommentDeleteButton.Attributes.Add("onclick", "DeleteFeedWidgetComment(\"" + feedType + "\", \"" + idFeedMessageComment.ToString() + "\", \"" + idFeedMessage.ToString() + "\");");

                            messageCommentDeleteButtonContainer.Controls.Add(messageCommentDeleteButton);
                            messageCommentActionButtonsContainer.Controls.Add(messageCommentDeleteButtonContainer);
                        }

                        // attach buttons container to header container
                        messageCommentData.Controls.Add(messageCommentActionButtonsContainer);

                        messageCommentDataContainer.Controls.Add(messageCommentData);

                        // timestamp
                        Panel messageCommentTimestampContainer = new Panel();
                        messageCommentTimestampContainer.ID = "FeedWidget_MessageCommentTimestampContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                        messageCommentTimestampContainer.CssClass = "MessageCommentTimestamp";

                        DateTime messageCommentTimestampDt = Convert.ToDateTime(commentRow["timestamp"]);
                        messageCommentTimestampDt = TimeZoneInfo.ConvertTimeFromUtc(messageCommentTimestampDt, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        Literal messageCommentTimestamp = new Literal();
                        messageCommentTimestamp.Text = messageCommentTimestampDt.ToString();
                        messageCommentTimestampContainer.Controls.Add(messageCommentTimestamp);

                        messageCommentDataContainer.Controls.Add(messageCommentTimestampContainer);

                        // attach comment data to comment container
                        messageCommentContainer.Controls.Add(messageCommentDataContainer);

                        // attach comment container to comments container
                        messageCommentsContainer.Controls.Add(messageCommentContainer);
                    }

                    // attach comments container to message container
                    messageContainer.Controls.Add(messageCommentsContainer);

                    // REPLY WITH COMMENT

                    Panel messageReplyCommentContainer = new Panel();
                    messageReplyCommentContainer.ID = "FeedWidget_MessageReplyCommentContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageReplyCommentContainer.CssClass = "MessageReplyCommentContainer";

                    // avatar
                    Panel messageReplyCommentSenderAvatarContainer = new Panel();
                    messageReplyCommentSenderAvatarContainer.ID = "FeedWidget_MessageReplyCommentSenderAvatarContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageReplyCommentSenderAvatarContainer.CssClass = "MessageReplyCommentAvatar";

                    Image messageReplyCommentSenderAvatar = new Image();
                    messageReplyCommentSenderAvatar.ID = "FeedWidget_MessageReplyCommentSenderAvatar_" + feedType + "_" + idFeedMessage.ToString();
                    messageReplyCommentSenderAvatar.CssClass = "SmallIcon";
                    messageReplyCommentSenderAvatar.AlternateText = String.Empty;
                    messageReplyCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);

                    messageReplyCommentSenderAvatarContainer.Controls.Add(messageReplyCommentSenderAvatar);

                    // attach avatar to reply comment container
                    messageReplyCommentContainer.Controls.Add(messageReplyCommentSenderAvatarContainer);

                    // comment field
                    TextBox messageCommentField = new TextBox();
                    messageCommentField.ID = "FeedWidget_MessageCommentField_" + feedType + "_" + idFeedMessage.ToString();
                    messageCommentField.CssClass = "MessageReplyCommentField";
                    messageCommentField.Attributes.Add("placeholder", _GlobalResources.PostComment);
                    messageCommentField.Attributes.Add("onkeyup", "PostFeedWidgetComment(\"" + feedType + "\", \"" + feedObjectId + "\", this, event);");
                    messageCommentField.AutoPostBack = false;

                    // attach field to reply comment container
                    messageReplyCommentContainer.Controls.Add(messageCommentField);

                    // attach reply comment container to message container
                    messageContainer.Controls.Add(messageReplyCommentContainer);

                    // write the data to a text writer for json output
                    TextWriter textWriter = new StringWriter();
                    HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                    messageContainer.RenderControl(htmlTextWriter);

                    jsonData.html += textWriter.ToString();
                }

                jsonData.actionSuccessful = true;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = lastRecord.ToString();
                jsonData.idMessage = null;
                jsonData.idParentMessage = null;
                jsonData.feedType = null;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = dtQuery.ToString();
                jsonData.idMessage = null;
                jsonData.idParentMessage = null;
                jsonData.feedType = null;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region _CertificateContentJsonDataStruct
        public struct _CertificateContentJsonData
        {
            public string certificateTitle;
            public string certificateLayoutJson;
            public string certificateAvailableFieldsJson;
            public string certificateAwardDataJson;
        }
        #endregion

        #region BuildCertificateViewerModal
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public _CertificateContentJsonData BuildCertificateViewerModal(int idCertificateRecord)
        {
            string certificateName = String.Empty;

            _CertificateContentJsonData jsonData = new _CertificateContentJsonData();

            try
            {
                CertificateRecord crObject = new CertificateRecord(idCertificateRecord);
                jsonData.certificateLayoutJson = crObject.BuildCertificateLayoutJSON();
                jsonData.certificateAvailableFieldsJson = crObject.BuildCertificateAvailableFieldsJSON(true);
                jsonData.certificateAwardDataJson = crObject.BuildCertificateAwardDataJSON(true);

                Certificate certificateObject = new Certificate(crObject.IdCertificate);

                // get the certificate name
                jsonData.certificateTitle = certificateObject.Name;
    
                foreach (Certificate.LanguageSpecificProperty certificateLanguageSpecificProperty in certificateObject.LanguageSpecificProperties)
                {
                    if (certificateLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    {  certificateName = certificateLanguageSpecificProperty.Name; }
                }                

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.certificateLayoutJson = ex.Message + ex.StackTrace;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region SaveFeedWidgetMessage
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public _WallMessagesJsonData SaveFeedWidgetMessage(string feedType, int feedObjectId, string message, int? idParentMessage)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            // messages from admin are never subject to moderation
            // "admin" will probably never access this particular method, but leave it here anyway
            bool isModerated = true;

            if (AsentiaSessionState.IdSiteUser > 1)
            {
                // declare a local feed object to get moderation information

                if (feedType == "group")
                {
                    Asentia.UMS.Library.Group groupObject = new Asentia.UMS.Library.Group(feedObjectId);
                    isModerated = (bool)groupObject.IsFeedModerated;
                }
                else if (feedType == "course")
                {
                    Asentia.LMS.Library.Course courseObject = new Asentia.LMS.Library.Course(feedObjectId);
                    isModerated = (bool)courseObject.IsFeedModerated;
                }
            }
            else
            { isModerated = false; }

            try
            {
                // build and save the feed message
                int idFeedMessage = 0;

                if (feedType == "group")
                {
                    Asentia.LMS.Library.GroupFeedMessage groupFeedMessage = new Asentia.LMS.Library.GroupFeedMessage();
                    groupFeedMessage.IdGroup = feedObjectId;
                    groupFeedMessage.IdAuthor = AsentiaSessionState.IdSiteUser;

                    if (idParentMessage != null)
                    { groupFeedMessage.IdParentGroupFeedMessage = idParentMessage; }

                    groupFeedMessage.Message = message;

                    idFeedMessage = groupFeedMessage.Save(isModerated);
                }
                else if (feedType == "course")
                {
                    Asentia.LMS.Library.CourseFeedMessage courseFeedMessage = new Asentia.LMS.Library.CourseFeedMessage();
                    courseFeedMessage.IdCourse = feedObjectId;
                    courseFeedMessage.IdAuthor = AsentiaSessionState.IdSiteUser;

                    if (idParentMessage != null)
                    { courseFeedMessage.IdParentCourseFeedMessage = idParentMessage; }

                    courseFeedMessage.Message = message;

                    idFeedMessage = courseFeedMessage.Save(isModerated);
                }
                else
                { }

                if (!isModerated)
                {
                    // get author information or just hard code it if user is "administrator"
                    // note that the author is always the currently logged in user
                    string authorName = String.Empty;
                    string authorAvatar = String.Empty;
                    string authorGender = String.Empty;

                    if (AsentiaSessionState.IdSiteUser == 1)
                    { authorName = _GlobalResources.Administrator; }
                    else
                    {
                        authorName = AsentiaSessionState.UserFirstName + " " + AsentiaSessionState.UserLastName;
                        authorAvatar = AsentiaSessionState.UserAvatar;
                        authorGender = AsentiaSessionState.UserGender;
                    }

                    if (idParentMessage == null) // new message -- this should never get executed as any message posted from the feed widget would be a comment, 
                    //                but lets leave it in case we develop a way to post a new message from here
                    {
                        Panel messageContainer = new Panel();
                        messageContainer.ID = "FeedWidget_MessageContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageContainer.CssClass = "MessageContainer";

                        // MESSAGE HEADER

                        Panel messageHeaderContainer = new Panel();
                        messageHeaderContainer.ID = "FeedWidget_MessageHeaderContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageHeaderContainer.CssClass = "MessageHeaderContainer";

                        // avatar
                        Panel messageSenderAvatarContainer = new Panel();
                        messageSenderAvatarContainer.ID = "FeedWidget_MessageSenderAvatarContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageSenderAvatarContainer.CssClass = "MessageAvatar";

                        Image messageSenderAvatar = new Image();
                        messageSenderAvatar.ID = "FeedWidget_MessageSenderAvatar_" + feedType + "_" + idFeedMessage.ToString();
                        messageSenderAvatar.CssClass = "MediumIcon";
                        messageSenderAvatar.AlternateText = authorName;

                        if (!String.IsNullOrWhiteSpace(authorAvatar))
                        { messageSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + authorAvatar; }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(authorGender) && authorGender == "f")
                            { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                            else
                            { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                        }

                        messageSenderAvatarContainer.Controls.Add(messageSenderAvatar);

                        // attach avatar to header container
                        messageHeaderContainer.Controls.Add(messageSenderAvatarContainer);

                        // sender information
                        Panel messageSenderInformationContainer = new Panel();
                        messageSenderInformationContainer.ID = "FeedWidget_MessageSenderInformationContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageSenderInformationContainer.CssClass = "MessageSenderInformationContainer";

                        Panel messageSenderNameContainer = new Panel();
                        messageSenderNameContainer.ID = "FeedWidget_MessageSenderNameContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageSenderNameContainer.CssClass = "MessageSenderNameContainer";

                        Literal messageSenderName = new Literal();
                        messageSenderName.Text = authorName;

                        messageSenderNameContainer.Controls.Add(messageSenderName);
                        messageSenderInformationContainer.Controls.Add(messageSenderNameContainer);

                        Panel messageSentDateTimeContainer = new Panel();
                        messageSentDateTimeContainer.ID = "FeedWidget_MessageSentDateTimeContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageSentDateTimeContainer.CssClass = "MessageSentDateTimeContainer";

                        DateTime messageTimestamp = DateTime.UtcNow;
                        messageTimestamp = TimeZoneInfo.ConvertTimeFromUtc(messageTimestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        Literal messageSentDateTime = new Literal();
                        messageSentDateTime.Text = messageTimestamp.ToString();


                        messageSentDateTimeContainer.Controls.Add(messageSentDateTime);
                        messageSenderInformationContainer.Controls.Add(messageSentDateTimeContainer);

                        Panel messagePostedInContainer = new Panel();
                        messagePostedInContainer.ID = "FeedWidget_MessagePostedInContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messagePostedInContainer.CssClass = "MessageSentDateTimeContainer";

                        Literal messagePostedIn = new Literal();
                        messagePostedIn.Text = _GlobalResources.PostedIn + ": ";
                        messagePostedInContainer.Controls.Add(messagePostedIn);

                        HyperLink messagePostedInLink = new HyperLink();
                        messagePostedInLink.ID = "FeedWidget_MessagePostedInLink_" + feedType + "_" + idFeedMessage.ToString();

                        if (feedType == "group")
                        {
                            messagePostedInLink.Text = _GlobalResources.Group + " - ";
                            messagePostedInLink.NavigateUrl = "/groups/Wall.aspx?id=" + feedObjectId.ToString();
                            messagePostedInContainer.Controls.Add(messagePostedInLink);
                        }
                        else if (feedType == "course")
                        {
                            messagePostedInLink.Text = _GlobalResources.Course + " - ";
                            messagePostedInLink.NavigateUrl = "/courses/Wall.aspx?id=" + feedObjectId.ToString();
                            messagePostedInContainer.Controls.Add(messagePostedInLink);
                        }
                        else
                        { }

                        messageSenderInformationContainer.Controls.Add(messagePostedInContainer);

                        // attach sender information to header container
                        messageHeaderContainer.Controls.Add(messageSenderInformationContainer);

                        // approve and delete/dis-approve buttons
                        Panel messageActionButtonsContainer = new Panel();
                        messageActionButtonsContainer.ID = "FeedWidget_MessageActionButtonsContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                        // NO APPROVE BUTTON HERE

                        // delete button - the author IS the current user, so no if statement needed
                        Panel messageDeleteButtonContainer = new Panel();
                        messageDeleteButtonContainer.ID = "FeedWidget_MessageDeleteButtonContainer_" + feedType + "_" + idFeedMessage.ToString();

                        Image messageDeleteButton = new Image();
                        messageDeleteButton.ID = "FeedWidget_MessageDeleteButton_" + feedType + "_" + idFeedMessage.ToString();
                        messageDeleteButton.CssClass = "XSmallIcon";
                        messageDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        messageDeleteButton.AlternateText = _GlobalResources.Delete;
                        messageDeleteButton.Style.Add("cursor", "pointer");
                        messageDeleteButton.Attributes.Add("onclick", "DeleteFeedWidgetMessage(\"" + feedType + "\", \"" + idFeedMessage.ToString() + "\");");

                        messageDeleteButtonContainer.Controls.Add(messageDeleteButton);
                        messageActionButtonsContainer.Controls.Add(messageDeleteButtonContainer);

                        // attach buttons container to header container
                        messageHeaderContainer.Controls.Add(messageActionButtonsContainer);

                        // attach header to message container
                        messageContainer.Controls.Add(messageHeaderContainer);

                        // MESSAGE DATA

                        Panel messageDataContainer = new Panel();
                        messageDataContainer.ID = "FeedWidget_MessageDataContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageDataContainer.CssClass = "MessageDataContainer";

                        Literal messageData = new Literal();
                        messageData.Text = message;

                        messageDataContainer.Controls.Add(messageData);
                        messageContainer.Controls.Add(messageDataContainer);

                        // COMMENTS

                        Panel messageCommentsContainer = new Panel();
                        messageCommentsContainer.ID = "FeedWidget_MessageCommentsContainer_" + feedType + "_" + idFeedMessage.ToString();

                        DataTable messageComments = new DataTable();

                        if (feedType == "group")
                        { messageComments = LMS.Library.GroupFeedMessage.GetComments(idFeedMessage, true); }
                        else if (feedType == "course")
                        { messageComments = LMS.Library.CourseFeedMessage.GetComments(idFeedMessage, true); }
                        else
                        { }

                        foreach (DataRow commentRow in messageComments.Rows)
                        {
                            int idFeedMessageComment = 0;

                            if (feedType == "group")
                            { idFeedMessageComment = Convert.ToInt32(commentRow["idGroupFeedMessage"]); }
                            else if (feedType == "course")
                            { idFeedMessageComment = Convert.ToInt32(commentRow["idCourseFeedMessage"]); }
                            else
                            { }

                            int idCommentAuthor = Convert.ToInt32(commentRow["idAuthor"]);

                            Panel messageCommentContainer = new Panel();
                            messageCommentContainer.ID = "FeedWidget_MessageCommentContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                            messageCommentContainer.CssClass = "MessageCommentContainer";

                            // avatar
                            Panel messageCommentSenderAvatarContainer = new Panel();
                            messageCommentSenderAvatarContainer.ID = "FeedWidget_MessageCommentSenderAvatarContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                            messageCommentSenderAvatarContainer.CssClass = "MessageCommentAvatar";

                            Image messageCommentSenderAvatar = new Image();
                            messageCommentSenderAvatar.ID = "FeedWidget_MessageCommentSenderAvatar_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                            messageCommentSenderAvatar.CssClass = "SmallIcon";
                            messageCommentSenderAvatar.AlternateText = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                            if (!String.IsNullOrWhiteSpace(commentRow["avatar"].ToString()))
                            { messageCommentSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(commentRow["idAuthor"]) + "/" + commentRow["avatar"].ToString(); }
                            else
                            {
                                if (!String.IsNullOrWhiteSpace(commentRow["gender"].ToString()) && commentRow["gender"].ToString() == "f")
                                { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                                else
                                { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                            }

                            messageCommentSenderAvatarContainer.Controls.Add(messageCommentSenderAvatar);

                            // attach avatar to container
                            messageCommentContainer.Controls.Add(messageCommentSenderAvatarContainer);

                            // comment data
                            Panel messageCommentDataContainer = new Panel();
                            messageCommentDataContainer.ID = "FeedWidget_MessageCommentDataContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                            messageCommentDataContainer.CssClass = "MessageCommentDataContainer";

                            Panel messageCommentData = new Panel();
                            messageCommentData.ID = "FeedWidget_MessageCommentData_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                            messageCommentData.CssClass = "MessageCommentData";

                            Label messageCommentSenderName = new Label();
                            messageCommentSenderName.CssClass = "MessageCommentSenderName";
                            messageCommentSenderName.Text = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);
                            messageCommentData.Controls.Add(messageCommentSenderName);

                            Label messageCommentMessage = new Label();
                            messageCommentMessage.Text = commentRow["message"].ToString();
                            messageCommentData.Controls.Add(messageCommentMessage);

                            // approve and delete/dis-approve buttons
                            Panel messageCommentActionButtonsContainer = new Panel();
                            messageCommentActionButtonsContainer.ID = "FeedWidget_MessageCommentActionButtonsContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                            messageCommentActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                            // NO APPROVE BUTTON HERE

                            // delete button
                            if (idCommentAuthor == AsentiaSessionState.IdSiteUser)
                            {
                                Panel messageCommentDeleteButtonContainer = new Panel();
                                messageCommentDeleteButtonContainer.ID = "FeedWidget_MessageCommentDeleteButtonContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();

                                Image messageCommentDeleteButton = new Image();
                                messageCommentDeleteButton.ID = "FeedWidget_MessageCommentDeleteButton_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                                messageCommentDeleteButton.CssClass = "XSmallIcon";
                                messageCommentDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                                messageCommentDeleteButton.AlternateText = _GlobalResources.Delete;
                                messageCommentDeleteButton.Style.Add("cursor", "pointer");
                                messageCommentDeleteButton.Attributes.Add("onclick", "DeleteFeedWidgetComment(\"" + feedType + "\", \"" + idFeedMessageComment.ToString() + "\", \"" + idFeedMessage.ToString() + "\");");

                                messageCommentDeleteButtonContainer.Controls.Add(messageCommentDeleteButton);
                                messageCommentActionButtonsContainer.Controls.Add(messageCommentDeleteButtonContainer);
                            }

                            // attach buttons container to header container
                            messageCommentData.Controls.Add(messageCommentActionButtonsContainer);

                            messageCommentDataContainer.Controls.Add(messageCommentData);

                            // timestamp
                            Panel messageCommentTimestampContainer = new Panel();
                            messageCommentTimestampContainer.ID = "FeedWidget_MessageCommentTimestampContainer_" + feedType + "_" + idFeedMessage.ToString() + "_" + idFeedMessageComment.ToString();
                            messageCommentTimestampContainer.CssClass = "MessageCommentTimestamp";

                            DateTime messageCommentTimestampDt = Convert.ToDateTime(commentRow["timestamp"]);
                            messageCommentTimestampDt = TimeZoneInfo.ConvertTimeFromUtc(messageCommentTimestampDt, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                            Literal messageCommentTimestamp = new Literal();
                            messageCommentTimestamp.Text = messageCommentTimestampDt.ToString();
                            messageCommentTimestampContainer.Controls.Add(messageCommentTimestamp);

                            messageCommentDataContainer.Controls.Add(messageCommentTimestampContainer);

                            // attach comment data to comment container
                            messageCommentContainer.Controls.Add(messageCommentDataContainer);

                            // attach comment container to comments container
                            messageCommentsContainer.Controls.Add(messageCommentContainer);
                        }

                        // attach comments container to message container
                        messageContainer.Controls.Add(messageCommentsContainer);

                        // REPLY WITH COMMENT

                        Panel messageReplyCommentContainer = new Panel();
                        messageReplyCommentContainer.ID = "FeedWidget_MessageReplyCommentContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageReplyCommentContainer.CssClass = "MessageReplyCommentContainer";

                        // avatar
                        Panel messageReplyCommentSenderAvatarContainer = new Panel();
                        messageReplyCommentSenderAvatarContainer.ID = "FeedWidget_MessageReplyCommentSenderAvatarContainer_" + feedType + "_" + idFeedMessage.ToString();
                        messageReplyCommentSenderAvatarContainer.CssClass = "MessageReplyCommentAvatar";

                        Image messageReplyCommentSenderAvatar = new Image();
                        messageReplyCommentSenderAvatar.ID = "FeedWidget_MessageReplyCommentSenderAvatar_" + feedType + "_" + idFeedMessage.ToString();
                        messageReplyCommentSenderAvatar.CssClass = "SmallIcon";
                        messageReplyCommentSenderAvatar.AlternateText = String.Empty;
                        messageReplyCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);

                        messageReplyCommentSenderAvatarContainer.Controls.Add(messageReplyCommentSenderAvatar);

                        // attach avatar to reply comment container
                        messageReplyCommentContainer.Controls.Add(messageReplyCommentSenderAvatarContainer);

                        // comment field
                        TextBox messageCommentField = new TextBox();
                        messageCommentField.ID = "FeedWidget_MessageCommentField_" + feedType + "_" + idFeedMessage.ToString();
                        messageCommentField.CssClass = "MessageReplyCommentField";
                        messageCommentField.Attributes.Add("placeholder", _GlobalResources.PostComment);
                        messageCommentField.Attributes.Add("onkeyup", "PostComment(this, event);");
                        messageCommentField.AutoPostBack = false;

                        // attach field to reply comment container
                        messageReplyCommentContainer.Controls.Add(messageCommentField);

                        // attach reply comment container to message container
                        messageContainer.Controls.Add(messageReplyCommentContainer);

                        // write the data to a text writer for json output
                        TextWriter textWriter = new StringWriter();
                        HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                        messageContainer.RenderControl(htmlTextWriter);

                        jsonData.html += textWriter.ToString();
                    }
                    else // just a comment -- this is what gets executed in all cases, for now
                    {
                        Panel messageCommentContainer = new Panel();
                        messageCommentContainer.ID = "FeedWidget_MessageCommentContainer_" + feedType + "_" + idParentMessage.ToString() + "_" + idFeedMessage.ToString();
                        messageCommentContainer.CssClass = "MessageCommentContainer";

                        // avatar
                        Panel messageCommentSenderAvatarContainer = new Panel();
                        messageCommentSenderAvatarContainer.ID = "FeedWidget_MessageCommentSenderAvatarContainer_" + feedType + "_" + idParentMessage.ToString() + "_" + idFeedMessage.ToString();
                        messageCommentSenderAvatarContainer.CssClass = "MessageCommentAvatar";

                        Image messageCommentSenderAvatar = new Image();
                        messageCommentSenderAvatar.ID = "FeedWidget_MessageCommentSenderAvatar_" + feedType + "_" + idParentMessage.ToString() + "_" + idFeedMessage.ToString();
                        messageCommentSenderAvatar.CssClass = "SmallIcon";
                        messageCommentSenderAvatar.AlternateText = authorName;

                        if (!String.IsNullOrWhiteSpace(authorAvatar))
                        { messageCommentSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser + "/" + authorAvatar; }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(authorGender) && authorGender == "f")
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                            else
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                        }

                        messageCommentSenderAvatarContainer.Controls.Add(messageCommentSenderAvatar);

                        // attach avatar to container
                        messageCommentContainer.Controls.Add(messageCommentSenderAvatarContainer);

                        // comment data
                        Panel messageCommentDataContainer = new Panel();
                        messageCommentDataContainer.ID = "FeedWidget_MessageCommentDataContainer_" + feedType + "_" + idParentMessage.ToString() + "_" + idFeedMessage.ToString();
                        messageCommentDataContainer.CssClass = "MessageCommentDataContainer";

                        Panel messageCommentData = new Panel();
                        messageCommentData.ID = "FeedWidget_MessageCommentData_" + feedType + "_" + idParentMessage.ToString() + "_" + idFeedMessage.ToString();
                        messageCommentData.CssClass = "MessageCommentData";

                        Label messageCommentSenderName = new Label();
                        messageCommentSenderName.CssClass = "MessageCommentSenderName";
                        messageCommentSenderName.Text = authorName;
                        messageCommentData.Controls.Add(messageCommentSenderName);

                        Label messageCommentMessage = new Label();
                        messageCommentMessage.Text = message;
                        messageCommentData.Controls.Add(messageCommentMessage);

                        // approve and delete/dis-approve buttons
                        Panel messageCommentActionButtonsContainer = new Panel();
                        messageCommentActionButtonsContainer.ID = "FeedWidget_MessageCommentActionButtonsContainer_" + feedType + "_" + idParentMessage.ToString() + "_" + idFeedMessage.ToString();
                        messageCommentActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                        // NO APPROVE BUTTON HERE

                        // delete button - the author IS the current user, so no if statement needed
                        Panel messageCommentDeleteButtonContainer = new Panel();
                        messageCommentDeleteButtonContainer.ID = "FeedWidget_MessageCommentDeleteButtonContainer_" + feedType + "_" + idParentMessage.ToString() + "_" + idFeedMessage.ToString();

                        Image messageCommentDeleteButton = new Image();
                        messageCommentDeleteButton.ID = "FeedWidget_MessageCommentDeleteButton_" + feedType + "_" + idParentMessage.ToString() + "_" + idFeedMessage.ToString();
                        messageCommentDeleteButton.CssClass = "XSmallIcon";
                        messageCommentDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        messageCommentDeleteButton.AlternateText = _GlobalResources.Delete;
                        messageCommentDeleteButton.Style.Add("cursor", "pointer");
                        messageCommentDeleteButton.Attributes.Add("onclick", "DeleteFeedWidgetComment(\"" + feedType + "\", \"" + idFeedMessage.ToString() + "\", \"" + idParentMessage.ToString() + "\");");

                        messageCommentDeleteButtonContainer.Controls.Add(messageCommentDeleteButton);
                        messageCommentActionButtonsContainer.Controls.Add(messageCommentDeleteButtonContainer);

                        // attach buttons container to header container
                        messageCommentData.Controls.Add(messageCommentActionButtonsContainer);

                        messageCommentDataContainer.Controls.Add(messageCommentData);

                        // timestamp
                        Panel messageCommentTimestampContainer = new Panel();
                        messageCommentTimestampContainer.ID = "FeedWidget_MessageCommentTimestampContainer_" + feedType + "_" + idParentMessage.ToString() + "_" + idFeedMessage.ToString();
                        messageCommentTimestampContainer.CssClass = "MessageCommentTimestamp";

                        DateTime messageCommentTimestampDt = DateTime.UtcNow;
                        messageCommentTimestampDt = TimeZoneInfo.ConvertTimeFromUtc(messageCommentTimestampDt, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        Literal messageCommentTimestamp = new Literal();
                        messageCommentTimestamp.Text = messageCommentTimestampDt.ToString();
                        messageCommentTimestampContainer.Controls.Add(messageCommentTimestamp);

                        messageCommentDataContainer.Controls.Add(messageCommentTimestampContainer);

                        // attach comment data to comment container
                        messageCommentContainer.Controls.Add(messageCommentDataContainer);

                        // write the data to a text writer for json output
                        TextWriter textWriter = new StringWriter();
                        HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                        messageCommentContainer.RenderControl(htmlTextWriter);

                        jsonData.html += textWriter.ToString();
                    }
                }
                else // empty html json data, used to just show a "message saved for moderation" modal
                { jsonData.html = String.Empty; }

                jsonData.actionSuccessful = true;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = null;
                jsonData.idMessage = idFeedMessage;
                jsonData.idParentMessage = idParentMessage;
                jsonData.feedType = feedType;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = null;
                jsonData.idMessage = null;
                jsonData.idParentMessage = idParentMessage;
                jsonData.feedType = feedType;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region DeleteFeedWidgetMessage
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public _WallMessagesJsonData DeleteFeedWidgetMessage(string feedType, int idMessage, int? idParentMessage)
        {
            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            try
            {
                if (feedType == "group")
                { Asentia.LMS.Library.GroupFeedMessage.Delete(idMessage); }
                else if (feedType == "course")
                { Asentia.LMS.Library.CourseFeedMessage.Delete(idMessage); }
                else
                { }

                jsonData.actionSuccessful = true;
                jsonData.html = String.Empty;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;
                jsonData.feedType = feedType;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;
                jsonData.feedType = feedType;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region BuildWallModerationWidgetMessages
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public _WallMessagesJsonData BuildWallModerationWidgetMessages(DateTime dtQuery, bool getMessagesNewerThanDtQuery, bool isInitialLoad)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            DateTime lastRecord;
            DataTable messages = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                // get the messages from the database object
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@pageSize", 10, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@dtQuery", dtQuery, SqlDbType.DateTime, 8, ParameterDirection.Input);
                databaseObject.AddParameter("@getMessagesNewerThanDtQuery", getMessagesNewerThanDtQuery, SqlDbType.Bit, 1, ParameterDirection.Input);
                databaseObject.AddParameter("@dtLastRecord", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Widget.WallModeration.GetMessages]", true);
                messages.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                lastRecord = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtLastRecord"].Value);

                // if there are no messages and this is the initial load, display a "no messages" message
                if (messages.Rows.Count == 0 && isInitialLoad)
                {
                    Panel noMessagesMessageContainer = new Panel();
                    noMessagesMessageContainer.ID = "WallModerationWidget_NoMessagesMessageContainer";
                    noMessagesMessageContainer.CssClass = "centered";

                    Literal noMessagesMessage = new Literal();
                    noMessagesMessage.Text = _GlobalResources.ThereAreNoMessagesCurrentlyAwaitingModeration;
                    noMessagesMessageContainer.Controls.Add(noMessagesMessage);

                    // write the data to a text writer for json output
                    TextWriter textWriter = new StringWriter();
                    HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                    noMessagesMessageContainer.RenderControl(htmlTextWriter);

                    jsonData.html += textWriter.ToString();
                    jsonData.hasRecords = false;
                }
                else
                {
                    jsonData.hasRecords = true;
                }

                foreach (DataRow row in messages.Rows)
                {
                    int idFeedMessage = Convert.ToInt32(row["idFeedMessage"]);

                    int? idParentFeedMessage = null;
                    if (!String.IsNullOrWhiteSpace(row["idParentFeedMessage"].ToString()))
                    { idParentFeedMessage = Convert.ToInt32(row["idParentFeedMessage"]); }

                    int feedObjectId = Convert.ToInt32(row["feedObjectId"]);
                    string feedType = row["feedType"].ToString();
                    int idMessageAuthor = Convert.ToInt32(row["idAuthor"]);

                    Panel messageContainer = new Panel();
                    messageContainer.ID = "WallModerationWidget_MessageContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageContainer.CssClass = "MessageContainer";

                    // MESSAGE HEADER

                    Panel messageHeaderContainer = new Panel();
                    messageHeaderContainer.ID = "WallModerationWidget_MessageHeaderContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageHeaderContainer.CssClass = "MessageHeaderContainer";

                    // avatar
                    Panel messageSenderAvatarContainer = new Panel();
                    messageSenderAvatarContainer.ID = "WallModerationWidget_MessageSenderAvatarContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageSenderAvatarContainer.CssClass = "MessageAvatar";

                    Image messageSenderAvatar = new Image();
                    messageSenderAvatar.ID = "WallModerationWidget_MessageSenderAvatar_" + feedType + "_" + idFeedMessage.ToString();
                    messageSenderAvatar.CssClass = "MediumIcon";
                    messageSenderAvatar.AlternateText = row["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                    if (!String.IsNullOrWhiteSpace(row["avatar"].ToString()))
                    { messageSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(row["idAuthor"]) + "/" + row["avatar"].ToString(); }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(row["gender"].ToString()) && row["gender"].ToString() == "f")
                        { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                        else
                        { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                    }

                    messageSenderAvatarContainer.Controls.Add(messageSenderAvatar);

                    // attach avatar to header container
                    messageHeaderContainer.Controls.Add(messageSenderAvatarContainer);

                    // sender information
                    Panel messageSenderInformationContainer = new Panel();
                    messageSenderInformationContainer.ID = "WallModerationWidget_MessageSenderInformationContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageSenderInformationContainer.CssClass = "MessageSenderInformationContainer";

                    Panel messageSenderNameContainer = new Panel();
                    messageSenderNameContainer.ID = "WallModerationWidget_MessageSenderNameContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageSenderNameContainer.CssClass = "MessageSenderNameContainer";

                    Literal messageSenderName = new Literal();
                    messageSenderName.Text = row["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                    messageSenderNameContainer.Controls.Add(messageSenderName);
                    messageSenderInformationContainer.Controls.Add(messageSenderNameContainer);

                    Panel messageSentDateTimeContainer = new Panel();
                    messageSentDateTimeContainer.ID = "WallModerationWidget_MessageSentDateTimeContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageSentDateTimeContainer.CssClass = "MessageSentDateTimeContainer";

                    DateTime messageTimestamp = Convert.ToDateTime(row["timestamp"]);
                    messageTimestamp = TimeZoneInfo.ConvertTimeFromUtc(messageTimestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                    Literal messageSentDateTime = new Literal();
                    messageSentDateTime.Text = messageTimestamp.ToString();


                    messageSentDateTimeContainer.Controls.Add(messageSentDateTime);
                    messageSenderInformationContainer.Controls.Add(messageSentDateTimeContainer);

                    Panel messagePostedInContainer = new Panel();
                    messagePostedInContainer.ID = "WallModerationWidget_MessagePostedInContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messagePostedInContainer.CssClass = "MessageSentDateTimeContainer";

                    Literal messagePostedIn = new Literal();

                    if (idParentFeedMessage != null)
                    { messagePostedIn.Text = _GlobalResources.PostedAsCommentIn + ": "; }
                    else
                    { messagePostedIn.Text = _GlobalResources.PostedAsNewMessageIn + ": "; }

                    messagePostedInContainer.Controls.Add(messagePostedIn);

                    HyperLink messagePostedInLink = new HyperLink();
                    messagePostedInLink.ID = "WallModerationWidget_MessagePostedInLink_" + feedType + "_" + idFeedMessage.ToString();

                    if (feedType == "group")
                    {
                        messagePostedInLink.Text = _GlobalResources.Group + " - " + row["feedObjectName"].ToString();
                        messagePostedInLink.NavigateUrl = "/groups/Wall.aspx?id=" + feedObjectId.ToString();
                        messagePostedInContainer.Controls.Add(messagePostedInLink);
                    }
                    else if (feedType == "course")
                    {
                        messagePostedInLink.Text = _GlobalResources.Course + " - " + row["feedObjectName"].ToString();
                        messagePostedInLink.NavigateUrl = "/courses/Wall.aspx?id=" + feedObjectId.ToString();
                        messagePostedInContainer.Controls.Add(messagePostedInLink);
                    }
                    else
                    { }

                    messageSenderInformationContainer.Controls.Add(messagePostedInContainer);

                    // attach sender information to header container
                    messageHeaderContainer.Controls.Add(messageSenderInformationContainer);

                    // approve and delete/dis-approve buttons
                    Panel messageActionButtonsContainer = new Panel();
                    messageActionButtonsContainer.ID = "WallModerationWidget_MessageActionButtonsContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                    // approve button
                    Panel messageApproveButtonContainer = new Panel();
                    messageApproveButtonContainer.ID = "WallModerationWidget_MessageApproveButtonContainer_" + feedType + "_" + idFeedMessage.ToString();

                    Image messageApproveButton = new Image();
                    messageApproveButton.ID = "WallModerationWidget_MessageApproveButton_" + feedType + "_" + idFeedMessage.ToString();
                    messageApproveButton.CssClass = "XSmallIcon";
                    messageApproveButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
                    messageApproveButton.AlternateText = _GlobalResources.Approve;
                    messageApproveButton.Style.Add("cursor", "pointer");
                    messageApproveButton.Attributes.Add("onclick", "ApproveWallModerationWidgetMessage(\"" + feedType + "\", \"" + idFeedMessage.ToString() + "\");");

                    messageApproveButtonContainer.Controls.Add(messageApproveButton);
                    messageActionButtonsContainer.Controls.Add(messageApproveButtonContainer);

                    // delete button
                    Panel messageDeleteButtonContainer = new Panel();
                    messageDeleteButtonContainer.ID = "WallModerationWidget_MessageDeleteButtonContainer_" + feedType + "_" + idFeedMessage.ToString();

                    Image messageDeleteButton = new Image();
                    messageDeleteButton.ID = "WallModerationWidget_MessageDeleteButton_" + feedType + "_" + idFeedMessage.ToString();
                    messageDeleteButton.CssClass = "XSmallIcon";
                    messageDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                    messageDeleteButton.AlternateText = _GlobalResources.Delete;
                    messageDeleteButton.Style.Add("cursor", "pointer");
                    messageDeleteButton.Attributes.Add("onclick", "DeleteWallModerationWidgetMessage(\"" + feedType + "\", \"" + idFeedMessage.ToString() + "\");");

                    messageDeleteButtonContainer.Controls.Add(messageDeleteButton);
                    messageActionButtonsContainer.Controls.Add(messageDeleteButtonContainer);

                    // attach buttons container to header container
                    messageHeaderContainer.Controls.Add(messageActionButtonsContainer);

                    // attach header to message container
                    messageContainer.Controls.Add(messageHeaderContainer);

                    // MESSAGE DATA

                    Panel messageDataContainer = new Panel();
                    messageDataContainer.ID = "WallModerationWidget_MessageDataContainer_" + feedType + "_" + idFeedMessage.ToString();
                    messageDataContainer.CssClass = "MessageDataContainer";

                    Literal messageData = new Literal();
                    messageData.Text = row["message"].ToString();

                    messageDataContainer.Controls.Add(messageData);
                    messageContainer.Controls.Add(messageDataContainer);

                    // write the data to a text writer for json output
                    TextWriter textWriter = new StringWriter();
                    HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                    messageContainer.RenderControl(htmlTextWriter);

                    jsonData.html += textWriter.ToString();
                }

                jsonData.actionSuccessful = true;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = lastRecord.ToString();
                jsonData.idMessage = null;
                jsonData.idParentMessage = null;
                jsonData.feedType = null;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = dtQuery.ToString();
                jsonData.idMessage = null;
                jsonData.idParentMessage = null;
                jsonData.feedType = null;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region ApproveWallModerationWidgetMessage
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public _WallMessagesJsonData ApproveWallModerationWidgetMessage(string feedType, int idMessage, int? idParentMessage)
        {
            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            try
            {
                if (feedType == "group")
                { Asentia.LMS.Library.GroupFeedMessage.Approve(idMessage); }
                else if (feedType == "course")
                { Asentia.LMS.Library.CourseFeedMessage.Approve(idMessage); }
                else
                { }

                jsonData.actionSuccessful = true;
                jsonData.html = String.Empty;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;
                jsonData.feedType = feedType;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;
                jsonData.feedType = feedType;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region DeleteWallModerationWidgetMessage
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public _WallMessagesJsonData DeleteWallModerationWidgetMessage(string feedType, int idMessage, int? idParentMessage)
        {
            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            try
            {
                if (feedType == "group")
                { Asentia.LMS.Library.GroupFeedMessage.Delete(idMessage); }
                else if (feedType == "course")
                { Asentia.LMS.Library.CourseFeedMessage.Delete(idMessage); }
                else
                { }

                jsonData.actionSuccessful = true;
                jsonData.html = String.Empty;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;
                jsonData.feedType = feedType;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;
                jsonData.feedType = feedType;

                // return jsonData
                return jsonData;
            }
        }
        #endregion
    }
}
