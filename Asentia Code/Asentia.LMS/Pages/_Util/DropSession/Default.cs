﻿using System;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages._Util.DropSession
{
    public class Default : AsentiaPage
    {
        #region Private Properties
        private string _Token;
        private ModalPopup _DropSessionConfirmModal;
        private Button _DropSessionConfirmHiddenButton;
        #endregion

        #region Page_Load
        public void Page_Load()
        {
            try
            {
                // get the token querystring parameter
                this._Token = this.QueryStringString("token");

                // build up the confirm modal popup
                this._DropSessionConfirmModal = new ModalPopup("DropSessionConfirmAction");
                this._DropSessionConfirmHiddenButton = new Button();
                this._DropSessionConfirmHiddenButton.ID = "DropSessionConfirmHiddenButton";
                this._DropSessionConfirmHiddenButton.Style.Add("display", "none");

                // add button to container
                this.PageContentContainer.Controls.Add(this._DropSessionConfirmHiddenButton);

                // set modal properties
                this._DropSessionConfirmModal.Type = ModalPopupType.Confirm;
                this._DropSessionConfirmModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_SESSION, ImageFiles.EXT_PNG);
                this._DropSessionConfirmModal.HeaderIconAlt = _GlobalResources.InstructorLedTrainingSession;
                this._DropSessionConfirmModal.HeaderText = _GlobalResources.InstructorLedTrainingSession;
                this._DropSessionConfirmModal.TargetControlID = this._DropSessionConfirmHiddenButton.ClientID;
                this._DropSessionConfirmModal.SubmitButton.Command += new CommandEventHandler(this._ConfirmButton_Command);
                this._DropSessionConfirmModal.ShowModal();

                // build the modal body
                HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
                body1Wrapper.ID = "GridConfirmActionModalBody1";

                Literal body1 = new Literal();
                body1.Text = _GlobalResources.AreYouSureYouWantToDropOutOfThisSession;
                body1Wrapper.Controls.Add(body1);

                // add controls to body
                this._DropSessionConfirmModal.AddControlToBody(body1Wrapper);

                // add modal to container
                this.PageContentContainer.Controls.Add(this._DropSessionConfirmModal);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, true);
            }

        }
        #endregion

        #region _ConfirmButton_Command
        /// <summary>
        /// Performs the confirm action.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _ConfirmButton_Command(object sender, CommandEventArgs e)
        {
            try
            {

                // redirect if the token is invalid
                if (this._Token == null)
                { Response.Redirect("/"); }

                AsentiaAESEncryption cryptoURL = new AsentiaAESEncryption();
                string decryptedURL = WebUtility.UrlDecode(cryptoURL.Decrypt(this._Token));

                string[] parameters = decryptedURL.Split('|');

                int idSession = Convert.ToInt32(parameters[0]);

                int idUser = Convert.ToInt32(parameters[1]);

                int idSite = Convert.ToInt32(parameters[2]);

                if (idSession > 0 && idUser > 0 && idSite > 0)
                {
                        // close confirm modal popup
                        this._DropSessionConfirmModal.HideModal();

                        // delete the session to user link
                        StandUpTrainingInstanceToUserLink.DropUserFromSession(idSession, idUser, idSite);

                        // display the saved feedback
                        this.DisplayFeedback(_GlobalResources.YouHaveBeenDroppedFromTheInstructorLedTrainingSession, false);
                }
                else
                {
                    this.DisplayFeedback(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, true);
                }

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }

        }
        #endregion        
    }


}
