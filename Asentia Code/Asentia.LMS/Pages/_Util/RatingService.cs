﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Xml;
using Asentia.Common;

namespace Asentia.LMS.Pages._Util
{
    /// <summary>
    /// Web services for Asentia Ratings.
    /// </summary>
    [WebService(Description = "Web services for Asentia Ratings.", Namespace = "http://default.asentia.com/RatingService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    public class RatingService : WebService
    {
        #region SaveCourseRating
        [WebMethod(Description = "Saves a user's rating for a course.", EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SaveCourseRating(string idObject, string rating)
        {
            // initialize return message to "fail"
            string returnMessage = "fail";

            // if there is no session, return with "fail" message
            if (AsentiaSessionState.IdSiteUser == 0 || AsentiaSessionState.IdSite == 0)
            { return returnMessage; }

            try
            {
                int idCourse;
                int courseRating;

                if (Int32.TryParse(idObject, out idCourse) && Int32.TryParse(rating, out courseRating))
                { Library.Course.SaveRating(idCourse, courseRating); }
                else
                { throw new AsentiaException("Object Id and/or Rating are not valid integers."); }

                // return
                returnMessage = "success";
                return returnMessage;
            }
            // catch exception and return failure
            catch (Exception ex)
            { return returnMessage; }
        }
        #endregion

        #region ResetCourseRating
        [WebMethod(Description = "Resets ratings for a course.", EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ResetCourseRating(string idObject)
        {
            // initialize return message to "fail"
            string returnMessage = "fail";

            // if there is no session, return with "fail" message
            if (AsentiaSessionState.IdSiteUser == 0 || AsentiaSessionState.IdSite == 0)
            { return returnMessage; }

            try
            {
                int idCourse;

                if (Int32.TryParse(idObject, out idCourse))
                { Library.Course.ResetRating(idCourse); }
                else
                { throw new AsentiaException("Object Id is not a valid integer."); }

                // return
                returnMessage = "success";
                return returnMessage;
            }
            // catch exception and return failure
            catch (Exception ex)
            { return returnMessage; }
        }
        #endregion

        /*
        #region SaveLearningPathRating
        [WebMethod(Description = "Saves a user's rating for a learning path.", EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SaveLearningPathRating(string idObject, string rating)
        {
            // initialize return message to "fail"
            string returnMessage = "fail";

            // if there is no session, return with "fail" message
            if (AsentiaSessionState.IdSiteUser == 0 || AsentiaSessionState.IdSite == 0)
            { return returnMessage; }

            try
            {
                int idLearningPath;
                int courseRating;

                if (Int32.TryParse(idObject, out idLearningPath) && Int32.TryParse(rating, out courseRating))
                { Library.LearningPath.SaveRating(idLearningPath, courseRating); }
                else
                { throw new AsentiaException("Object Id and/or Rating are not valid integers."); }

                // return
                returnMessage = "success";
                return returnMessage;
            }
            // catch exception and return failure
            catch (Exception ex)
            { return returnMessage; }
        }
        #endregion

        #region ResetLearningPathRating
        [WebMethod(Description = "Resets ratings for a learning path.", EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ResetLearningPathRating(string idObject)
        {
            // initialize return message to "fail"
            string returnMessage = "fail";

            // if there is no session, return with "fail" message
            if (AsentiaSessionState.IdSiteUser == 0 || AsentiaSessionState.IdSite == 0)
            { return returnMessage; }

            try
            {
                int idLearningPath;

                if (Int32.TryParse(idLearningPath, out idCourse))
                { Library.LearningPath.ResetRating(idLearningPath); }
                else
                { throw new AsentiaException("Object Id is not a valid integer."); }

                // return
                returnMessage = "success";
                return returnMessage;
            }
            // catch exception and return failure
            catch (Exception ex)
            { return returnMessage; }
        }
        #endregion
        */
    }
}
