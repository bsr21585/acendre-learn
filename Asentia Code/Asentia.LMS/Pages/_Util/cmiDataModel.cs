﻿#region Includes
using System.Collections.Generic;
#endregion

#region    CmiData Model
/// <summary>
/// CmiDataModel 
/// </summary>
public class CmiDataModel
{
    public string credit { get; set; }                    //Indicates whether the learner’s performance with the SCO is to be credited.
    public string mode { get; set; }                      //Identifies the modes in which the SCO may be presented to the learner.
    public string completion_status { get; set; }         //Indicates whether the learner has completed the SCO.
    public string exit { get; set; }                      //Contains information as to why and how the learner exited from the SCO.
    public string location { get; set; }                  //Represents a location in the SCO. Its value and meaning are determined by the SCO.
    public string progress_measure { get; set; }          //Identifies a measure of the progress the learner has made toward completing the SCO.
    public Score score { get; set; }                      //Identifies the learner’s score for the SCO.
    public string success_status { get; set; }            //Indicates whether the learner has mastered the SCO.
    public string suspend_data { get; set; }              //Provides additional space for storing and retrieving information relating to the suspension of an SCO.
    public double total_time { get; set; }                //Provides the sum of all of the learner’s session times accumulated in the learner’s current attempt.
    public List<Interaction> interactions { get; set; }   //Defines information concerning an interaction for the purpose of measurement or assessment.
    public List<Objective> objectives { get; set; }       //Defines information concerning an objectives for the purpose of measurement or assessment.
    public List<Objective> globalObjectives { get; set; }  //Defines information concerning an globalObjectives for the purpose of measurement or assessment.  
    //Added newly 
    public dynamic  activityAttemptCount { get; set; }          //Defines information concerning an acivity's number of Attempt Count.
    public int activityEffectiveAttemptCount { get; set; }  //Defines information concerning an acivity's number of Effective Attempt Count.
    public bool isActive { get; set; }                      //Defines information concerning an acivity is Active or not.                             
    public bool? isSuspended { get; set; }                   //Defines information concerning an acivity is Suspended or not.      
    public string attemptCompletionAmount { get; set; }      //Defines information concerning an acivity's attemptCompletionAmount.
    public bool objectiveProgressStatus { get; set; }        //Defines information concerning an acivity's objectiveProgressStatus.   
    public bool objectiveSatisfiedStatus { get; set; }      //Defines information concerning an acivity's objectiveSatisfiedStatus. 
    public bool objectiveMeasureStatus { get; set; }        //Defines information concerning an acivity's objectiveSatisfiedStatus. 
    public decimal objectiveNormalizedMeasure { get; set; } //Defines information concerning an acivity's objectiveNormalizedMeasure. 
    public bool attemptProgressStatus { get; set; }         //Defines information concerning an acivity's attemptProgressStatus. 
    public bool attemptCompletionStatus { get; set; }       //Defines information concerning an acivity's attemptCompletionStatus. 
    public bool activityProgressStatus { get; set; }        //Defines information concerning an acivity's activityProgressStatus. 
    public bool attemptCompletionAmountStatus { get; set; } //Defines information concerning an acivity's attemptCompletionAmountStatus. 
    //for global.json
    public int actualAttemptCount { get; set; }             //Defines information concerning  rte actualAttemptCount.
    public int effectiveAttemptCount { get; set; }          //Defines information concerning  rte actualAttemptCount.
}
#endregion

#region Score Model
/// <summary>
/// Score model
/// </summary>
public class Score
{
    public string max { get; set; }                       //Real number with 7 significant decimal digits.
    public string min { get; set; }                       //Real number with 7 significant decimal digits.                 
    public string raw { get; set; }                       //Real number with 7 significant decimal digits.                    
    public string scaled { get; set; }                    //Real number with 7 significant decimal digits.                    
}
#endregion

#region CorrectRespons Model
/// <summary>
/// CorrectRespons model
/// </summary>
public class CorrectResponse
{
    public string pattern { get; set; }
}
#endregion

#region Interaction Model
/// <summary>
/// Interaction model
/// </summary>
public class Interaction
{
    public string id { get; set; }                              //Initialized with the value defined as the identifier for <imsss:object tives> in the manifestfile.
    public string type { get; set; }                            //The correct_response and learner_response elements are dependent on this data model element, and this must be set before these two dependent elements are used.
    public object timestamp { get; set; }                       //records the interactions timestamp
    public object timestampConverted { get; set; }
    public object weighting { get; set; }                       //Real with 7 significant decimal digits.
    public string learner_response { get; set; }                //learner_response elements are dependent on this data model element, and this must be set before these two dependent elements are used.  
    public string result { get; set; }
    public string description { get; set; }                     //The data model elements are used mainly for storing the learner’s performance and progress data with an SCO.
    public float latency { get; set; }
    public List<Objective> objectives { get; set; }             //Defines information concerning an objectives for the purpose of measurement or assessment.
    public List<CorrectResponse> correct_responses { get; set; } //correct_responses elements are dependent on this data model element, and this must be set before these two dependent elements are used.  
}
#endregion

#region Objective Model
/// <summary>
/// Objective model
/// </summary>
public class Objective
{
    public string id { get; set; }                              //Provided by the LMS 
    public string completion_status { get; set; }               // holds Completion status of each deliverable SCO values are like “completed”,“incomplete”,“not_attempted”,“unknown”.
    public string success_status { get; set; }                  //The data model element affects the Objective Progress Status for the activity associated with the SCO.
    public string progress_measure { get; set; }                //Real number with 7 significant decimal digits within the range between 0 and 1.0.
    public object description { get; set; }                     //Localized string type (ISO-10646-1).
    public Score score { get; set; }                            //Identifies the learner’s score for the SCO.
    //Added newly
    public bool objectiveProgressStatus { get; set; }           //Defines information concerning  each SCO objectiveProgressStatus.
    public bool objectiveSatisfiedStatus { get; set; }          //Defines information concerning  each SCO objectiveSatisfiedStatus.
    public bool objectiveMeasureStatus { get; set; }             //Defines information concerning  each SCO objectiveMeasureStatus.    
    public decimal objectiveNormalizedMeasure { get; set; }          //Defines information concerning  each SCO objectiveNormalizedMeasure.
    public bool attemptProgressStatus { get; set; }              //Defines information concerning  each SCO attemptProgressStatus.
    public bool attemptCompletionStatus { get; set; }            //Defines information concerning  each SCO attemptCompletionStatus.   
    public object objectiveContributestoRollup { get; set; }     //Defines information concerning  each SCO objectiveContributestoRollup.  
    public bool attemptCompletionAmountStatus { get; set; }      //Defines information concerning  each SCO attemptCompletionAmountStatus.
}
#endregion