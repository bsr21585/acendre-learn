﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages._Util.API
{
    #region WebServiceType [ENUM]
    public enum WebServiceType
    {
        GetUser = 1,
        GetCourse = 2,
        GetGroup = 3,
        GetEnrollment = 4,
        GetCourseCatalog = 5,
        SaveUser = 6,
        SaveCourse = 7,
        SaveGroup = 8,
        EnrollCourse = 9,
        AddUser = 10,
        EnrollLearningPath = 11,
        AttachUserToGroups = 12,
        RemoveUserFromGroups = 13,
        GetSSOToken = 14,
        GetLearningPath = 15
    }
    #endregion

    [WebService(Namespace = "http://default.asentia.com/_util/API/default.asmx/")]
    [ScriptService]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [global::System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Default : WebService
    {
        #region Public Data Types
        public struct ValidationError
        {
            public int LineNumber;
            public int PositionNumber;
            public string Desc;
        }
        #endregion

        #region Private Data Members

        private ArrayList _Errors;
        private AsentiaSite_ReadOnly _GSObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
        private string _XMLDefaultReturnString = "<response><status><message>##message##</message><description>##description##</description></status><payload>##payload##</payload></response>";
        private string _JSONDefaultReturnString = "{\"response\": {\"status\": {\"message\": \"##message##\", \"description\": \"##description##\"}, \"payload\": ##payload##}}";
        private string _XMLNamespaceAttributeA = "p2:nil=\"true\" xmlns:p2=\"http://www.w3.org/2001/XMLSchema-instance\"";
        private string _XMLNamespaceAttributeB = "xmlns:p3=\"http://www.w3.org/2001/XMLSchema-instance\"";
        private string _XMLNamespaceAttributeC = "<anyType p3:type=\"LanguageSpecificProperty\" >";
        private string _XMLNamespaceAttributeD = "</anyType>";
        private string _NotCorrectFormat = "Not Correct Format In XML Or JSON";
        #endregion

        #region WebService Methods

        #region Course Methods
        #region WebService: GetCourse
        /// <summary>
        /// Web Services : GetCourse
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string GetCourse(string payload)
        {
                AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
                AsentiaSessionState.IdSiteUser = 1;

                int courseID;
                StringBuilder returnString = new StringBuilder();

                string apiKey = string.Empty;
                bool returnXML = true;
                string data = string.Empty;
                string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
                string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                    throw new AsentiaException(_NotCorrectFormat);

                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                int i = 0;

                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.GetCourse))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        JObject o = JObject.Parse(@data);
                        if (!String.IsNullOrWhiteSpace((string)o["id"]))
                        {
                            courseID = (int)o["id"];
                            returnString.Append(_GetCourseSerialized(courseID, returnXML));
                        }
                        else if (!String.IsNullOrWhiteSpace((string)o["code"]))
                        {
                            foreach (int id in Course.GetCoursesAPI("code", (string)o["code"]))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetCourseSerialized(id, returnXML));
                            }
                        }
                        else if (!String.IsNullOrWhiteSpace((string)o["fts"]))
                        {
                            foreach (int id in Course.GetCoursesAPI("fts", (string)o["fts"]))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetCourseSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, code, or fts");
                        }
                    }
                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.GetCourse);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            returnString.Append(" ");
                            returnString.Append(error.Desc); 
                        }
                        throw new AsentiaException(returnString.ToString());
                    }
                    else
                    {
                        XDocument doc = XDocument.Parse(data);
                        if (doc.Element("getCourse").Element("id") != null)
                        {
                            courseID = Convert.ToInt32(doc.Element("getCourse").Element("id").Value);
                            returnString.Append(_GetCourseSerialized(courseID, returnXML));
                        }
                        else if (doc.Element("getCourse").Element("code") != null)
                        {
                            foreach (int id in Course.GetCoursesAPI("code", doc.Element("getCourse").Element("code").Value.Trim()))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetCourseSerialized(id, returnXML));
                            }
                        }
                        else if (doc.Element("getCourse").Element("fts") != null)
                        {
                            foreach (int id in Course.GetCoursesAPI("fts", doc.Element("getCourse").Element("fts").Value.Trim()))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetCourseSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, code, or fts");
                        }
                    }
                }

                returnString = _GetRootElement(returnXML, returnString, "Course", i);

                return _ReturnSuccess(returnString.ToString(), returnXML, true);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #region WebService: GetCourseCatalog
        /// <summary>
        /// Web Services : GetCourseCatalog
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string GetCourseCatalog(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            int idCatalog;
            StringBuilder returnString = new StringBuilder();

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                int i = 0;

                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.GetCourseCatalog))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        JObject o = JObject.Parse(@data);
                        if (!String.IsNullOrWhiteSpace((string)o["id"]))
                        {
                            idCatalog = (int)o["id"];
                            returnString.Append(_GetCourseCatalogSerialized(idCatalog, returnXML));
                        }
                        else if (!String.IsNullOrWhiteSpace((string)o["fts"]))
                        {
                            foreach (int id in Asentia.LMS.Library.Catalog.GetCatalogAPI((string)o["fts"]))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetCourseCatalogSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, or fts");
                        }
                    }
                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.GetCourseCatalog);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            returnString.Append(" ");
                            returnString.Append(error.Desc); 
                        }

                        throw new AsentiaException(returnString.ToString());
                    }
                    else
                    {
                        XDocument doc = XDocument.Parse(data);
                        if (doc.Element("getCourseCatalog").Element("id") != null)
                        {
                            idCatalog = Convert.ToInt16(doc.Element("getCourseCatalog").Element("id").Value);
                            returnString.Append(_GetCourseCatalogSerialized(idCatalog, returnXML));
                        }
                        else if (doc.Element("getCourseCatalog").Element("fts").Value != null)
                        {
                            foreach (int id in Asentia.LMS.Library.Catalog.GetCatalogAPI(doc.Element("getCourseCatalog").Element("fts").Value.Trim()))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetCourseCatalogSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, or fts");
                        }
                    }
                }

                returnString = _GetRootElement(returnXML, returnString, "Catalog", i);

                return _ReturnSuccess(returnString.ToString(), returnXML, true);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #region WebService: GetLearningPath
        /// <summary>
        /// Web Services : GetLearningPath
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string GetLearningPath(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            int idLearningPath;
            StringBuilder returnString = new StringBuilder();

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                    throw new AsentiaException(_NotCorrectFormat);

                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                int i = 0;

                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.GetLearningPath))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        JObject o = JObject.Parse(@data);
                        if (!String.IsNullOrWhiteSpace((string)o["id"]))
                        {
                            idLearningPath = (int)o["id"];
                            returnString.Append(_GetLearningPathSerialized(idLearningPath, returnXML));
                        }
                        else if (!String.IsNullOrWhiteSpace((string)o["fts"]))
                        {
                            foreach (int id in LearningPath.GetLearningPathsAPI((string)o["fts"]))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetLearningPathSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, or fts");
                        }
                    }
                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.GetLearningPath);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            returnString.Append(" ");
                            returnString.Append(error.Desc);
                        }
                        throw new AsentiaException(returnString.ToString());
                    }
                    else
                    {
                        XDocument doc = XDocument.Parse(data);
                        if (doc.Element("getLearningPath").Element("id") != null)
                        {
                            idLearningPath = Convert.ToInt32(doc.Element("getLearningPath").Element("id").Value);
                            returnString.Append(_GetLearningPathSerialized(idLearningPath, returnXML));
                        }
                        else if (doc.Element("getLearningPath").Element("fts") != null)
                        {
                            foreach (int id in LearningPath.GetLearningPathsAPI(doc.Element("getLearningPath").Element("fts").Value.Trim()))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetLearningPathSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, or fts");
                        }
                    }
                }

                returnString = _GetRootElement(returnXML, returnString, "LearningPath", i);

                return _ReturnSuccess(returnString.ToString(), returnXML, true);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #endregion

        #region User Methods
        #region WebService: GetUser
        /// <summary>
        /// Web Services : GetUser
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string GetUser(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            int idUser;
            StringBuilder returnString = new StringBuilder();

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                int i = 0;
                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.GetUser))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        JObject o = JObject.Parse(@data);
                        if (!String.IsNullOrWhiteSpace((string)o["id"]))
                        {
                            idUser = (int)o["id"];
                            returnString.Append(_GetUserSerialized( idUser, returnXML));
                        }
                        else if (!String.IsNullOrWhiteSpace((string)o["username"]))
                        {
                            foreach (int id in Asentia.UMS.Library.User.GetUsersAPI("username", (string)o["username"]))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetUserSerialized(id, returnXML));
                            }
                        }
                        else if (!String.IsNullOrWhiteSpace((string)o["email"]))
                        {
                            foreach (int id in Asentia.UMS.Library.User.GetUsersAPI("email", (string)o["email"]))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetUserSerialized(id, returnXML));
                            }
                        }
                        else if (!String.IsNullOrWhiteSpace((string)o["fts"]))
                        {
                            foreach (int id in Asentia.UMS.Library.User.GetUsersAPI("fts", (string)o["fts"]))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetUserSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, username, or fts");
                        }
                    }
                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.GetUser);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            returnString.Append(" ");
                            returnString.Append(error.Desc); 
                        }

                        throw new AsentiaException(returnString.ToString());
                    }
                    else
                    {
                        XDocument doc = XDocument.Parse(data);
                        if (doc.Element("getUser").Element("id") != null)
                        {
                            idUser = Convert.ToInt32(doc.Element("getUser").Element("id").Value);
                            returnString.Append(_GetUserSerialized( idUser, returnXML));
                        }
                        else if (doc.Element("getUser").Element("username") != null)
                        {
                            foreach (int id in Asentia.UMS.Library.User.GetUsersAPI("username", doc.Element("getUser").Element("username").Value.Trim()))
                            {
                                i++;
                                returnString.Append(((!returnXML && i>1) ? "," : string.Empty) + _GetUserSerialized(id, returnXML));
                            }
                        }
                        else if (doc.Element("getUser").Element("email") != null)
                        {
                            foreach (int id in Asentia.UMS.Library.User.GetUsersAPI("email", doc.Element("getUser").Element("email").Value.Trim()))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetUserSerialized(id, returnXML));
                            }
                        }
                        else if (doc.Element("getUser").Element("fts") != null)
                        {
                            foreach (int id in Asentia.UMS.Library.User.GetUsersAPI("fts", doc.Element("getUser").Element("fts").Value.Trim()))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetUserSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, username, or fts");
                        }
                    }
                }

                returnString = _GetRootElement(returnXML, returnString, "User", i);

                return _ReturnSuccess(returnString.ToString(), returnXML, true);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #region WebService: SaveUser
        /// <summary>
        /// Web Services : SaveUser
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string SaveUser(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            User userObject;
            AsentiaSessionState.IdSiteUser = 1;
            XDocument doc;
            string userID = string.Empty;
            StringBuilder returnString = new StringBuilder();

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                if (data.Substring(0, 1) == "{")
                {
                    JObject o = JObject.Parse(@data);
                    bool passJasonSchema = true;
                    if ((int)o["-id"] == 0)
                    {
                        userObject = new User();
                        passJasonSchema = _ValidateJsonData(data, WebServiceType.AddUser);
                    }
                    else
                    {
                        userObject = new User((int)o["id"]);
                        passJasonSchema = _ValidateJsonData(data, WebServiceType.SaveUser);
                    }

                    if (!passJasonSchema)
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {

                        if (!String.IsNullOrWhiteSpace((string)o["firstName"]))
                            userObject.FirstName = (string)o["firstName"];

                        if (!String.IsNullOrWhiteSpace((string)o["middleName"]))
                            userObject.MiddleName = (string)o["middleName"];

                        if (!String.IsNullOrWhiteSpace((string)o["lastName"]))
                            userObject.LastName = (string)o["lastName"];

                        if (!String.IsNullOrWhiteSpace((string)o["email"]))
                            userObject.Email = (string)o["email"];

                        if (!String.IsNullOrWhiteSpace((string)o["username"]))
                            userObject.Username = (string)o["username"];

                        if (!String.IsNullOrWhiteSpace((string)o["password"]))
                            userObject.Password = (string)o["password"];

                        if (!String.IsNullOrWhiteSpace((string)o["mustChangePassword"]))
                            userObject.MustChangePassword = (bool)o["mustChangePassword"];

                        if (!String.IsNullOrWhiteSpace((string)o["idTimezone"]))
                            userObject.IdTimezone = (int)o["idTimezone"];

                        if (!String.IsNullOrWhiteSpace((string)o["languageString"]))
                            userObject.LanguageString = (string)o["languageString"];

                        if (!String.IsNullOrWhiteSpace((string)o["dtExpires"]))
                            userObject.DtExpires = (DateTime)o["dtExpires"];

                        if (!String.IsNullOrWhiteSpace((string)o["isActive"]))
                            userObject.IsActive = (bool)o["isActive"];

                        if (!String.IsNullOrWhiteSpace((string)o["company"]))
                            userObject.Company = (string)o["company"];

                        if (!String.IsNullOrWhiteSpace((string)o["address"]))
                            userObject.Address = (string)o["address"];

                        if (!String.IsNullOrWhiteSpace((string)o["city"]))
                            userObject.City = (string)o["city"];

                        if (!String.IsNullOrWhiteSpace((string)o["province"]))
                            userObject.Province = (string)o["province"];

                        if (!String.IsNullOrWhiteSpace((string)o["postalcode"]))
                            userObject.PostalCode = (string)o["postalcode"];

                        if (!String.IsNullOrWhiteSpace((string)o["country"]))
                            userObject.Country = (string)o["country"];

                        if (!String.IsNullOrWhiteSpace((string)o["phonePrimary"]))
                            userObject.PhonePrimary = (string)o["phonePrimary"];

                        if (!String.IsNullOrWhiteSpace((string)o["phoneHome"]))
                            userObject.PhoneHome = (string)o["phoneHome"];

                        if (!String.IsNullOrWhiteSpace((string)o["phoneWork"]))
                            userObject.PhoneWork = (string)o["phoneWork"];

                        if (!String.IsNullOrWhiteSpace((string)o["phoneFax"]))
                            userObject.PhoneFax = (string)o["phoneFax"];

                        if (!String.IsNullOrWhiteSpace((string)o["phoneMobile"]))
                            userObject.PhoneMobile = (string)o["phoneMobile"];

                        if (!String.IsNullOrWhiteSpace((string)o["phonePager"]))
                            userObject.PhonePager = (string)o["phonePager"];

                        if (!String.IsNullOrWhiteSpace((string)o["phoneOther"]))
                            userObject.PhoneOther = (string)o["phoneOther"];

                        if (!String.IsNullOrWhiteSpace((string)o["employeeID"]))
                            userObject.EmployeeId = (string)o["employeeID"];

                        if (!String.IsNullOrWhiteSpace((string)o["jobTitle"]))
                            userObject.JobTitle = (string)o["jobTitle"];
                    
                        if (!String.IsNullOrWhiteSpace((string)o["jobClass"]))
                            userObject.JobClass = (string)o["jobClass"];

                        if (!String.IsNullOrWhiteSpace((string)o["division"]))
                            userObject.Division = (string)o["division"];

                        if (!String.IsNullOrWhiteSpace((string)o["region"]))
                            userObject.Region = (string)o["region"];

                        if (!String.IsNullOrWhiteSpace((string)o["department"]))
                            userObject.Department = (string)o["department"];

                        if (!String.IsNullOrWhiteSpace((string)o["dtHire"]))
                            userObject.DtHire = (DateTime)o["dtHire"];

                        if (!String.IsNullOrWhiteSpace((string)o["dtTerm"]))
                            userObject.DtTerm = (DateTime)o["dtTerm"];

                        if (!String.IsNullOrWhiteSpace((string)o["gender"]))
                            userObject.Gender = (string)o["gender"];

                        if (!String.IsNullOrWhiteSpace((string)o["race"]))
                            userObject.Race = (string)o["race"];

                        if (!String.IsNullOrWhiteSpace((string)o["dtDOB"]))
                            userObject.DtDOB = (DateTime)o["dtDOB"];

                        if (!String.IsNullOrWhiteSpace((string)o["field00"]))
                            userObject.Field00 = (string)o["field00"];

                        if (!String.IsNullOrWhiteSpace((string)o["field01"]))
                            userObject.Field01 = (string)o["field01"];

                        if (!String.IsNullOrWhiteSpace((string)o["field02"]))
                            userObject.Field02 = (string)o["field02"];

                        if (!String.IsNullOrWhiteSpace((string)o["field03"]))
                            userObject.Field03 = (string)o["field03"];

                        if (!String.IsNullOrWhiteSpace((string)o["field04"]))
                            userObject.Field04 = (string)o["field04"];

                        if (!String.IsNullOrWhiteSpace((string)o["field05"]))
                            userObject.Field05 = (string)o["field05"];

                        if (!String.IsNullOrWhiteSpace((string)o["field06"]))
                            userObject.Field06 = (string)o["field06"];

                        if (!String.IsNullOrWhiteSpace((string)o["field07"]))
                            userObject.Field07 = (string)o["field07"];

                        if (!String.IsNullOrWhiteSpace((string)o["field08"]))
                            userObject.Field08 = (string)o["field08"];

                        if (!String.IsNullOrWhiteSpace((string)o["field09"]))
                            userObject.Field09 = (string)o["field09"];

                        if (!String.IsNullOrWhiteSpace((string)o["field10"]))
                            userObject.Field10 = (string)o["field10"];

                        if (!String.IsNullOrWhiteSpace((string)o["field11"]))
                            userObject.Field11 = (string)o["field11"];

                        if (!String.IsNullOrWhiteSpace((string)o["field12"]))
                            userObject.Field12 = (string)o["field12"];

                        if (!String.IsNullOrWhiteSpace((string)o["field13"]))
                            userObject.Field13 = (string)o["field13"];

                        if (!String.IsNullOrWhiteSpace((string)o["field14"]))
                            userObject.Field14 = (string)o["field14"];

                        if (!String.IsNullOrWhiteSpace((string)o["field15"]))
                            userObject.Field15 = (string)o["field15"];

                        if (!String.IsNullOrWhiteSpace((string)o["field16"]))
                            userObject.Field16 = (string)o["field16"];

                        if (!String.IsNullOrWhiteSpace((string)o["field17"]))
                            userObject.Field17 = (string)o["field17"];

                        if (!String.IsNullOrWhiteSpace((string)o["field18"]))
                            userObject.Field18 = (string)o["field18"];

                        if (!String.IsNullOrWhiteSpace((string)o["field19"]))
                            userObject.Field19 = (string)o["field19"];

                        userObject.Id = userObject.Save();

                        returnString.Append(_GlobalResources.UserProfileHasBeenSavedSuccessfully);
                    }

                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors;
                    string successMsg;
                    doc = XDocument.Parse(data);
                    if ((int)doc.Element("user").Attribute("id") == 0)
                    {
                        userObject = new User();
                        xmlErrors = _ValidateXMLData(data, WebServiceType.AddUser);
                        successMsg = _GlobalResources.UserProfileHasBeenSavedSuccessfully;
                    }
                    else
                    {
                        userObject = new User(Convert.ToInt32(doc.Element("user").Attribute("id").Value.Trim()));
                        xmlErrors = _ValidateXMLData(data, WebServiceType.SaveUser);
                        successMsg = _GlobalResources.UserProfileHasBeenSavedSuccessfully;
                    }

                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            returnString.Append(" ");
                            returnString.Append(error.Desc); 
                        }                        
                        throw new AsentiaException(returnString.ToString());
                    }
                    else
                    {
                        if (doc.Element("user").Element("firstName") != null)
                            userObject.FirstName = doc.Element("user").Element("firstName").Value.Trim();

                        if (doc.Element("user").Element("middleName") != null)
                            userObject.MiddleName = doc.Element("user").Element("middleName").Value.Trim();

                        if (doc.Element("user").Element("lastName") != null)
                            userObject.LastName = doc.Element("user").Element("lastName").Value.Trim();

                        if (doc.Element("user").Element("email") != null)
                            userObject.Email = doc.Element("user").Element("email").Value.Trim();

                        if (doc.Element("user").Element("username") != null)
                            userObject.Username = doc.Element("user").Element("username").Value.Trim();

                        if (doc.Element("user").Element("password") != null)
                            userObject.Password = doc.Element("user").Element("password").Value.Trim();

                        if (doc.Element("user").Element("mustChangePassword") != null)
                            userObject.MustChangePassword = (bool)doc.Element("user").Element("mustChangePassword");

                        if (doc.Element("user").Element("idTimezone") != null)
                            userObject.IdTimezone = (int)doc.Element("user").Element("idTimezone");

                        if (doc.Element("user").Element("languageString") != null)
                            userObject.LanguageString = doc.Element("user").Element("languageString").Value.Trim();

                        if (doc.Element("user").Element("dtExpires") != null)
                            userObject.DtExpires = (DateTime)doc.Element("user").Element("dtExpires");

                        if (doc.Element("user").Element("isActive") != null)
                            userObject.IsActive = (bool)doc.Element("user").Element("isActive");

                        if (doc.Element("user").Element("company") != null)
                            userObject.Company = doc.Element("user").Element("company").Value.Trim();

                        if (doc.Element("user").Element("address") != null)
                            userObject.Address = doc.Element("user").Element("address").Value.Trim();

                        if (doc.Element("user").Element("city") != null)
                            userObject.City = doc.Element("user").Element("city").Value.Trim();

                        if (doc.Element("user").Element("province") != null)
                            userObject.Province = doc.Element("user").Element("province").Value.Trim();

                        if (doc.Element("user").Element("postalcode") != null)
                            userObject.PostalCode = doc.Element("user").Element("postalcode").Value.Trim();

                        if (doc.Element("user").Element("country") != null)
                            userObject.Country = doc.Element("user").Element("country").Value.Trim();

                        if (doc.Element("user").Element("phonePrimary") != null)
                            userObject.PhonePrimary = doc.Element("user").Element("phonePrimary").Value.Trim();

                        if (doc.Element("user").Element("phoneHome") != null)
                            userObject.PhoneHome = doc.Element("user").Element("phoneHome").Value.Trim();

                        if (doc.Element("user").Element("phoneWork") != null)
                            userObject.PhoneWork = doc.Element("user").Element("phoneWork").Value.Trim();

                        if (doc.Element("user").Element("phoneFax") != null)
                            userObject.PhoneFax = doc.Element("user").Element("phoneFax").Value.Trim();

                        if (doc.Element("user").Element("phoneMobile") != null)
                            userObject.PhoneMobile = doc.Element("user").Element("phoneMobile").Value.Trim();

                        if (doc.Element("user").Element("phonePager") != null)
                            userObject.PhonePager = doc.Element("user").Element("phonePager").Value.Trim();

                        if (doc.Element("user").Element("phoneOther") != null)
                            userObject.PhoneOther = doc.Element("user").Element("phoneOther").Value.Trim();

                        if (doc.Element("user").Element("employeeID") != null)
                            userObject.EmployeeId = doc.Element("user").Element("employeeID").Value.Trim();

                        if (doc.Element("user").Element("jobTitle") != null)
                            userObject.JobTitle = doc.Element("user").Element("jobTitle").Value.Trim();

                        if (doc.Element("user").Element("jobClass") != null)
                            userObject.JobClass = doc.Element("user").Element("jobClass").Value.Trim();

                        if (doc.Element("user").Element("division") != null)
                            userObject.Division = doc.Element("user").Element("division").Value.Trim();

                        if (doc.Element("user").Element("region") != null)
                            userObject.Region = doc.Element("user").Element("region").Value.Trim();

                        if (doc.Element("user").Element("department") != null)
                            userObject.Department = doc.Element("user").Element("department").Value.Trim();

                        if (doc.Element("user").Element("dtHire") != null)
                            userObject.DtHire = (DateTime)doc.Element("user").Element("dtHire");

                        if (doc.Element("user").Element("dtTerm") != null)
                            userObject.DtTerm = (DateTime)doc.Element("user").Element("dtTerm");

                        if (doc.Element("user").Element("gender") != null)
                            userObject.Gender = doc.Element("user").Element("gender").Value.Trim();

                        if (doc.Element("user").Element("race") != null)
                            userObject.Race = doc.Element("user").Element("race").Value.Trim();

                        if (doc.Element("user").Element("dtDOB") != null)
                            userObject.DtDOB = (DateTime)doc.Element("user").Element("dtDOB");

                        if (doc.Element("user").Element("field00") != null)
                            userObject.Field00 = doc.Element("user").Element("field00").Value.Trim();

                        if (doc.Element("user").Element("field01") != null)
                            userObject.Field01 = doc.Element("user").Element("field01").Value.Trim();

                        if (doc.Element("user").Element("field02") != null)
                            userObject.Field02 = doc.Element("user").Element("field02").Value.Trim();

                        if (doc.Element("user").Element("field03") != null)
                            userObject.Field03 = doc.Element("user").Element("field03").Value.Trim();

                        if (doc.Element("user").Element("field04") != null)
                            userObject.Field04 = doc.Element("user").Element("field04").Value.Trim();

                        if (doc.Element("user").Element("field05") != null)
                            userObject.Field05 = doc.Element("user").Element("field05").Value.Trim();

                        if (doc.Element("user").Element("field06") != null)
                            userObject.Field06 = doc.Element("user").Element("field06").Value.Trim();

                        if (doc.Element("user").Element("field07") != null)
                            userObject.Field07 = doc.Element("user").Element("field07").Value.Trim();

                        if (doc.Element("user").Element("field08") != null)
                            userObject.Field08 = doc.Element("user").Element("field08").Value.Trim();

                        if (doc.Element("user").Element("field09") != null)
                            userObject.Field09 = doc.Element("user").Element("field09").Value.Trim();

                        if (doc.Element("user").Element("field10") != null)
                            userObject.Field10 = doc.Element("user").Element("field10").Value.Trim();

                        if (doc.Element("user").Element("field11") != null)
                            userObject.Field11 = doc.Element("user").Element("field11").Value.Trim();

                        if (doc.Element("user").Element("field12") != null)
                            userObject.Field12 = doc.Element("user").Element("field12").Value.Trim();

                        if (doc.Element("user").Element("field13") != null)
                            userObject.Field13 = doc.Element("user").Element("field13").Value.Trim();

                        if (doc.Element("user").Element("field14") != null)
                            userObject.Field14 = doc.Element("user").Element("field14").Value.Trim();

                        if (doc.Element("user").Element("field15") != null)
                            userObject.Field15 = doc.Element("user").Element("field15").Value.Trim();

                        if (doc.Element("user").Element("field16") != null)
                            userObject.Field16 = doc.Element("user").Element("field16").Value.Trim();

                        if (doc.Element("user").Element("field17") != null)
                            userObject.Field17 = doc.Element("user").Element("field17").Value.Trim();

                        if (doc.Element("user").Element("field18") != null)
                            userObject.Field18 = doc.Element("user").Element("field18").Value.Trim();

                        if (doc.Element("user").Element("field19") != null)
                            userObject.Field19 = doc.Element("user").Element("field19").Value.Trim();

                            userObject.Id = userObject.Save();

                        returnString.Append(successMsg);
                    }
                }

                return _ReturnSuccess(returnString.ToString(), returnXML, false);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #endregion

        #region Group Methods
        #region WebService: GetGroup
        /// <summary>
        /// Web Services : GetGroup
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string GetGroup(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            int idGroup;
            StringBuilder returnString = new StringBuilder();

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                int i = 0;
                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.GetGroup))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        JObject o = JObject.Parse(@data);
                        if (!String.IsNullOrWhiteSpace((string)o["id"]))
                        {
                            idGroup = (int)o["id"];
                            returnString.Append(_GetGroupSerialized(idGroup, returnXML));
                        }
                        else if (!String.IsNullOrWhiteSpace((string)o["fts"]))
                        {
                            foreach (int id in Group.GetGroupsAPI((string)o["fts"]))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetGroupSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            return defaultReturnString.Replace("##description##", _GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, or fts").Replace("##message##", _GlobalResources.Error).Replace("##payload##", "");
                        }
                    }
                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.GetGroup);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            returnString.Append(" ");
                            returnString.Append(error.Desc); 
                        }
                        throw new AsentiaException(returnString.ToString());
                    }
                    else
                    {
                        XDocument doc = XDocument.Parse(data);
                            if (doc.Element("getGroup").Element("id") != null)
                            {
                                idGroup = Convert.ToInt32(doc.Element("getGroup").Element("id").Value);
                                returnString.Append(_GetGroupSerialized(idGroup, returnXML));
                            }
                            else if (doc.Element("getGroup").Element("fts") != null)
                            {
                                foreach (int id in Group.GetGroupsAPI(doc.Element("getGroup").Element("fts").Value.Trim()))
                                {
                                    i++;
                                    returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetGroupSerialized(id, returnXML));
                                }
                            }
                            else
                            {
                                return defaultReturnString.Replace("##description##", _GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id, or fts").Replace("##message##", _GlobalResources.Error).Replace("##payload##", "");
                            }
                    }
                }

                returnString = _GetRootElement(returnXML, returnString, "Group", i);

                return _ReturnSuccess(returnString.ToString(), returnXML, true);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #region WebService: SaveGroup
        /// <summary>
        /// Web Services : SaveGroup
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string SaveGroup(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            Group groupObject;
            AsentiaSessionState.IdSiteUser = 1;
            XDocument doc;
            string groupID = string.Empty;
            string returnString = string.Empty;

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.SaveGroup))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        JObject o = JObject.Parse(@data);
                        if ((int)o["-id"] == 0)
                            groupObject = new Group();
                        else
                            groupObject = new Group((int)o["-id"]);                        

                        foreach (JObject t in o["name"])
                        {
                            if ((string)t["-lang"] == AsentiaSessionState.UserCulture)
                                groupObject.Name = (string)t["#text"];
                        }

                        if (!String.IsNullOrWhiteSpace((string)o["isSelfJoinAllowed"]))
                            groupObject.IsSelfJoinAllowed = (bool)o["isSelfJoinAllowed"];

                        if (o["shortDescription"] != null)
                        {
                            foreach (JObject s in o["shortDescription"])
                            {
                                if ((string)s["-lang"] == AsentiaSessionState.UserCulture)
                                    groupObject.ShortDescription = (string)s["#text"];
                            }
                        }

                        if (o["longDescription"] != null)
                        {
                            foreach (JObject l in o["longDescription"])
                            {
                                if ((string)l["-lang"] == AsentiaSessionState.UserCulture)
                                    groupObject.LongDescription = (string)l["#text"];
                            }
                        }

                        if (!String.IsNullOrWhiteSpace((string)o["wallIsActive"]))
                            groupObject.IsFeedActive = (bool)o["wallIsActive"];

                        if (!String.IsNullOrWhiteSpace((string)o["wallIsModerated"]))
                            groupObject.IsFeedModerated = (bool)o["wallIsModerated"];

                            groupObject.Id = groupObject.Save();

                            foreach (string language in _GSObject.AvailableLanguages)
                            {
                                string name = string.Empty;
                                string shortDescription = string.Empty;
                                string longDescription = string.Empty;

                                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in groupObject.LanguageSpecificProperties)
                                {
                                    if (groupLanguageSpecificProperty.LangString == language)
                                    {
                                        name = groupLanguageSpecificProperty.Name;
                                        shortDescription = groupLanguageSpecificProperty.ShortDescription;
                                        longDescription = groupLanguageSpecificProperty.LongDescription;
                                    }
                                }

                                foreach (JObject t in o["name"])
                                {
                                    if ((string)t["-lang"] == language)
                                        name = (string)t["#text"];
                                }

                                if (o["shortDescription"] != null)
                                {
                                    foreach (JObject s in o["shortDescription"])
                                    {
                                        if ((string)s["-lang"] == language)
                                            shortDescription = (string)s["#text"];
                                    }
                                }

                                if (o["longDescription"] != null)
                                {
                                    foreach (JObject l in o["longDescription"])
                                    {
                                        if ((string)l["-lang"] == language)
                                            longDescription = (string)l["#text"];
                                    }
                                }

                                if (!String.IsNullOrWhiteSpace(name))
                                {
                                    groupObject.SaveLang(language, name, shortDescription, longDescription, null);
                                }
                            }

                        returnString += _GlobalResources.GroupPropertiesHaveBeenSavedSuccessfully;
                    }

                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.SaveGroup);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            if (String.IsNullOrWhiteSpace(returnString))
                            { returnString += error.Desc; }
                            else
                            { returnString += " " + error.Desc; }
                        }

                        throw new AsentiaException(returnString);
                    }
                    else
                    {
                        doc = XDocument.Parse(data);
                        if ((int)doc.Element("group").Attribute("id") == 0)
                        {
                            groupObject = new Group();
                        }
                        else
                        {
                            groupObject = new Group(Convert.ToInt32(doc.Element("group").Attribute("id").Value.Trim()));
                        }

                        if (doc.Element("group").Element("name").Element("value").Attribute("lang").Value.Trim() == AsentiaSessionState.UserCulture)
                            groupObject.Name = doc.Element("group").Element("name").Element("value").Value.Trim();

                        if (doc.Element("group").Element("isSelfJoinAllowed") != null)
                            groupObject.IsSelfJoinAllowed = (bool)doc.Element("group").Element("isSelfJoinAllowed");
                    
                        if (doc.Element("group").Element("wallIsActive") != null)
                            groupObject.IsFeedActive = (bool)doc.Element("group").Element("wallIsActive");

                        if (doc.Element("group").Element("wallIsModerated") != null)
                            groupObject.IsFeedModerated = (bool)doc.Element("group").Element("wallIsModerated");

                        if (doc.Element("group").Element("shortDescription") != null)
                        {
                            if (doc.Element("group").Element("shortDescription").Element("value").Attribute("lang").Value.Trim() == AsentiaSessionState.UserCulture)
                                groupObject.ShortDescription = doc.Element("group").Element("shortDescription").Element("value").Value.Trim();
                        }

                        if (doc.Element("group").Element("longDescription") != null)
                        {
                            if (doc.Element("group").Element("longDescription").Element("value").Attribute("lang").Value.Trim() == AsentiaSessionState.UserCulture)
                                groupObject.LongDescription = (string)doc.Element("group").Element("longDescription").Element("value").Value.Trim();
                        }

                        groupObject.Id = groupObject.Save();

                        foreach (string language in _GSObject.AvailableLanguages)
                        {
                            string name = string.Empty;
                            string shortDescription = string.Empty;
                            string longDescription = string.Empty;

                            foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in groupObject.LanguageSpecificProperties)
                            {
                                if (groupLanguageSpecificProperty.LangString == language)
                                {
                                    name = groupLanguageSpecificProperty.Name;
                                    shortDescription = groupLanguageSpecificProperty.ShortDescription;
                                    longDescription = groupLanguageSpecificProperty.LongDescription;
                                }
                            }

                            foreach (XElement xt in doc.Element("group").Element("name").Descendants("value"))
                            {
                                if (xt.Attribute("lang").Value.Trim() == language)
                                    name = xt.Value.Trim();
                            }

                            if (doc.Element("group").Element("shortDescription") != null)
                            {
                                foreach (XElement xs in doc.Element("group").Element("shortDescription").Descendants("value"))
                                {
                                    if (xs.Attribute("lang").Value.Trim() == language)
                                        shortDescription = xs.Value.Trim();
                                }
                            }

                            if (doc.Element("group").Element("longDescription") != null)
                            {
                                foreach (XElement xl in doc.Element("group").Element("longDescription").Descendants("value"))
                                {
                                    if (xl.Attribute("lang").Value.Trim() == language)
                                        longDescription = xl.Value.Trim();
                                }
                            }

                            if (!String.IsNullOrWhiteSpace(name))
                            {
                                groupObject.SaveLang(language, name, shortDescription, longDescription, null);
                            }
                        }

                        returnString += _GlobalResources.GroupPropertiesHaveBeenSavedSuccessfully;
                    }
                }

                return _ReturnSuccess(returnString, returnXML, false);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #region WebService: AttachUserToGroups
        /// <summary>
        /// Web Services : AttachUserToGroups
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string AttachUserToGroups(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            User userObject;
            AsentiaSessionState.IdSiteUser = 1;
            XDocument doc;
            DataTable dtGroup = new DataTable();
            DataColumn dcGroup = new DataColumn();
            dtGroup.Columns.Add(dcGroup);
            string groupID = string.Empty;
            string returnString = string.Empty;

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                     XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.AttachUserToGroups))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        JObject o = JObject.Parse(@data);
                        userObject = new User((int)o["user"]["-id"]);

                        JObject g = o["user"]["groups"] as JObject;

                        foreach (JObject t in g["group"])
                        {
                            DataRow drGroup = dtGroup.NewRow();
                            drGroup[0] = (int)t["-id"];
                            dtGroup.Rows.Add(drGroup);
                        }

                        userObject.JoinGroups(dtGroup);  
                        returnString += _GlobalResources.User_sAddedSuccessfully;
                    }

                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.AttachUserToGroups);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            if (String.IsNullOrWhiteSpace(returnString))
                            { returnString += error.Desc; }
                            else
                            { returnString += " " + error.Desc; }
                        }

                        throw new AsentiaException(returnString);
                    }
                    else
                    {
                        doc = XDocument.Parse(data);
                        userObject = new User(Convert.ToInt32(doc.Element("user").Attribute("id").Value.Trim()));

                        foreach (XElement xt in doc.Element("user").Element("groups").Descendants("group"))
                        {
                            DataRow drGroup = dtGroup.NewRow();
                            drGroup[0] = xt.Attribute("id").Value.Trim();
                            dtGroup.Rows.Add(drGroup);
                        }
                        userObject.JoinGroups(dtGroup);
                        returnString += _GlobalResources.User_sAddedSuccessfully;
                    }
                }

                return _ReturnSuccess(returnString, returnXML, false);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #region WebService: RemoveUserFromGroups
        /// <summary>
        /// Web Services : RemoveUserFromGroups
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string RemoveUserFromGroups(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            User userObject;
            AsentiaSessionState.IdSiteUser = 1;
            XDocument doc;
            DataTable dtGroup = new DataTable();
            DataColumn dcGroup = new DataColumn();
            dtGroup.Columns.Add(dcGroup);
            string groupID = string.Empty;
            string returnString = string.Empty;

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.AttachUserToGroups))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        JObject o = JObject.Parse(@data);
                        userObject = new User((int)o["user"]["-id"]);

                        JObject g = o["user"]["groups"] as JObject;

                        foreach (JObject t in g["group"])
                        {
                            DataRow drGroup = dtGroup.NewRow();
                            drGroup[0] = (int)t["-id"];
                            dtGroup.Rows.Add(drGroup);
                        }

                        userObject.RemoveGroups(dtGroup);
                        returnString += _GlobalResources.User_sRemovedSuccessfully;
                    }

                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.AttachUserToGroups);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            if (String.IsNullOrWhiteSpace(returnString))
                            { returnString += error.Desc; }
                            else
                            { returnString += " " + error.Desc; }
                        }

                        throw new AsentiaException(returnString);
                    }
                    else
                    {
                        doc = XDocument.Parse(data);
                        userObject = new User(Convert.ToInt32(doc.Element("user").Attribute("id").Value.Trim()));

                        foreach (XElement xt in doc.Element("user").Element("groups").Descendants("group"))
                        {
                            DataRow drGroup = dtGroup.NewRow();
                            drGroup[0] = xt.Attribute("id").Value.Trim();
                            dtGroup.Rows.Add(drGroup);
                        }
                        userObject.RemoveGroups(dtGroup);
                        returnString += _GlobalResources.User_sRemovedSuccessfully;
                    }
                }

                return _ReturnSuccess(returnString, returnXML, false);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion
        #endregion

        #region Enrollment Methods
        #region WebService: GetEnrollment
        /// <summary>
        /// Web Services : GetEnrollment
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string GetEnrollment(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            int idEnrollment;
            StringBuilder returnString = new StringBuilder();

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                int i = 0;

                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.GetEnrollment))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        JObject o = JObject.Parse(@data);
                        if (!String.IsNullOrWhiteSpace((string)o["idEnrollment"]))
                        {
                            if (String.IsNullOrWhiteSpace((string)o["idUser"]) && String.IsNullOrWhiteSpace((string)o["idCourse"]))
                            {
                                idEnrollment = (int)o["idEnrollment"];
                                returnString.Append(_GetEnrollmentSerialized(idEnrollment, returnXML));
                            }
                            else
                            {
                                returnString.Append(_GlobalResources.IdCourseAndOrIdUserCannotBeUsedWhenIdEnrollmentIsUsed);
                            }
                        }
                        else if (!String.IsNullOrWhiteSpace((string)o["idUser"]) || !String.IsNullOrWhiteSpace((string)o["idCourse"]))
                        {
                            int idUser = (o["idUser"] == null) ? 0 : Convert.ToInt32((string)o["idUser"]);
                            int idCourse = (o["idCourse"] == null) ? 0 : Convert.ToInt32((string)o["idCourse"]);
                            foreach (int id in Enrollment.GetEnrollmentsAPI(idUser, idCourse))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetEnrollmentSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": idEnrollment, idUser, or idCourse");
                        }
                    }
                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.GetEnrollment);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            returnString.Append(" ");
                            returnString.Append(error.Desc); 
                        }
                        throw new AsentiaException(returnString.ToString());
                    }
                    else
                    {
                        XDocument doc = XDocument.Parse(data);

                        if (doc.Element("getEnrollment").Element("idEnrollment") != null)
                        {
                                idEnrollment = Convert.ToInt32(doc.Element("getEnrollment").Element("idEnrollment").Value);
                                returnString.Append(_GetEnrollmentSerialized(idEnrollment, returnXML));
                        }
                        else if (doc.Element("getEnrollment").Element("idUser") != null || doc.Element("getEnrollment").Element("idCourse") != null )
                        {
                            int idUser = (doc.Element("getEnrollment").Element("idUser") == null)? 0 : Convert.ToInt32(doc.Element("getEnrollment").Element("idUser").Value);
                            int idCourse = (doc.Element("getEnrollment").Element("idCourse") == null)? 0 : Convert.ToInt32(doc.Element("getEnrollment").Element("idCourse").Value.Trim());
                            foreach (int id in Enrollment.GetEnrollmentsAPI(idUser, idCourse))
                            {
                                i++;
                                returnString.Append(((!returnXML && i > 1) ? "," : string.Empty) + _GetEnrollmentSerialized(id, returnXML));
                            }
                        }
                        else
                        {
                            throw new AsentiaException(_GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": idEnrollment, idUser, or idCourse");
                        }
                    }
                }

                returnString = _GetRootElement(returnXML, returnString, "Enrollment", i);

                return _ReturnSuccess(returnString.ToString(), returnXML, true);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #region WebService: EnrollCourse
        /// <summary>
        /// Web Services : EnrollCourse
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string EnrollCourse(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            Enrollment enrollmentObject = new Enrollment();
            AsentiaSessionState.IdSiteUser = 1;
            XDocument doc;
            string successMsg = _GlobalResources.EnrollmentHasBeenSavedSuccessfully;
            string returnString = string.Empty;

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                if (data.Substring(0,1) == "{" )
                {
                    JObject o = JObject.Parse(@data);
                    bool passJasonSchema = true;
                    passJasonSchema = _ValidateJsonData(data, WebServiceType.EnrollCourse);

                    if (!passJasonSchema)
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace((string)o["idUser"]))
                            enrollmentObject.IdUser = (int)o["idUser"];

                        if (!String.IsNullOrWhiteSpace((string)o["idCourse"]))
                            enrollmentObject.IdCourse = (int)o["idCourse"];

                        if (!String.IsNullOrWhiteSpace((string)o["isLockedByPrerequisites"]))
                        { enrollmentObject.IsLockedByPrerequisites = (bool)o["isLockedByPrerequisites"]; }
                        else
                        { enrollmentObject.IsLockedByPrerequisites = true; }                         

                        if (!String.IsNullOrWhiteSpace((string)o["dtStart"]))
                            enrollmentObject.DtStart = (DateTime)o["dtStart"];

                        if (!String.IsNullOrWhiteSpace((string)o["idTimezone"]))
                            enrollmentObject.IdTimezone = (int)o["idTimezone"];

                        if (!String.IsNullOrWhiteSpace((string)o["dueInterval"]))
                            enrollmentObject.DueInterval = (int)o["dueInterval"];

                        if (!String.IsNullOrWhiteSpace((string)o["dueTimeframe"]))
                            enrollmentObject.DueTimeframe = (string)o["dueTimeframe"];

                        if (!String.IsNullOrWhiteSpace((string)o["expiresFromStartInterval"]))
                            enrollmentObject.ExpiresFromStartInterval = (int)o["expiresFromStartInterval"];

                        if (!String.IsNullOrWhiteSpace((string)o["expiresFromStartTimeframe"]))
                            enrollmentObject.ExpiresFromStartTimeframe = (string)o["expiresFromStartTimeframe"];

                        if (!String.IsNullOrWhiteSpace((string)o["expiresFromFirstLaunchInterval"]))
                            enrollmentObject.ExpiresFromFirstLaunchInterval = (int)o["expiresFromFirstLaunchInterval"];

                        if (!String.IsNullOrWhiteSpace((string)o["expiresFromFirstLaunchTimeframe"]))
                            enrollmentObject.ExpiresFromFirstLaunchTimeframe = (string)o["expiresFromFirstLaunchTimeframe"];
                    }
                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors;
                    doc = XDocument.Parse(data);

                    xmlErrors = _ValidateXMLData(data, WebServiceType.EnrollCourse);                

                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            if (String.IsNullOrWhiteSpace(returnString))
                            { returnString += error.Desc; }
                            else
                            { returnString += " " + error.Desc; }
                        }

                        throw new AsentiaException(returnString);
                    }
                    else
                    {
                        if (doc.Element("courseEnrollment").Element("idUser") != null)
                            enrollmentObject.IdUser = (int)doc.Element("courseEnrollment").Element("idUser");

                        if (doc.Element("courseEnrollment").Element("idCourse") != null)
                            enrollmentObject.IdCourse = (int)doc.Element("courseEnrollment").Element("idCourse");

                        if (doc.Element("courseEnrollment").Element("isLockedByPrerequisites") != null)
                            { enrollmentObject.IsLockedByPrerequisites = (bool)doc.Element("courseEnrollment").Element("isLockedByPrerequisites"); }
                        else
                            { enrollmentObject.IsLockedByPrerequisites = true; }

                        if (doc.Element("courseEnrollment").Element("dtStart") != null)
                            enrollmentObject.DtStart = (DateTime)doc.Element("courseEnrollment").Element("dtStart");

                        if (doc.Element("courseEnrollment").Element("idTimezone") != null)
                            enrollmentObject.IdTimezone = (int)doc.Element("courseEnrollment").Element("idTimezone");

                        if (doc.Element("courseEnrollment").Element("dueInterval") != null)
                            enrollmentObject.DueInterval = (int)doc.Element("courseEnrollment").Element("dueInterval");

                        if (doc.Element("courseEnrollment").Element("dueTimeframe") != null)
                            enrollmentObject.DueTimeframe = doc.Element("courseEnrollment").Element("dueTimeframe").Value.Trim();

                        if (doc.Element("courseEnrollment").Element("expiresFromStartInterval") != null)
                            enrollmentObject.ExpiresFromStartInterval = (int)doc.Element("courseEnrollment").Element("expiresFromStartInterval");

                        if (doc.Element("courseEnrollment").Element("expiresFromStartTimeframe") != null)
                            enrollmentObject.ExpiresFromStartTimeframe = doc.Element("courseEnrollment").Element("expiresFromStartTimeframe").Value.Trim();

                        if (doc.Element("courseEnrollment").Element("expiresFromFirstLaunchInterval") != null)
                            enrollmentObject.ExpiresFromFirstLaunchInterval = (int)doc.Element("courseEnrollment").Element("expiresFromFirstLaunchInterval");

                        if (doc.Element("courseEnrollment").Element("expiresFromFirstLaunchTimeframe") != null)
                            enrollmentObject.ExpiresFromFirstLaunchTimeframe = doc.Element("courseEnrollment").Element("expiresFromFirstLaunchTimeframe").Value.Trim();
                    }
                }

                enrollmentObject.Id = enrollmentObject.Save();

                returnString += successMsg;

                return _ReturnSuccess(returnString, returnXML, false);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion

        #region WebService: EnrollLearningPath
        /// <summary>
        /// Web Services : EnrollLearningPath
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string EnrollLearningPath(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            LearningPathEnrollment enrollmentObject = new LearningPathEnrollment();
            AsentiaSessionState.IdSiteUser = 1;
            XDocument doc;
            string successMsg = _GlobalResources.EnrollmentHasBeenSavedSuccessfully;
            string returnString = string.Empty;

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                        throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

                _CheckAPIkey(apiKey);

                if (data.Substring(0, 1) == "{")
                {
                    JObject o = JObject.Parse(@data);
                    bool passJasonSchema = true;
                    passJasonSchema = _ValidateJsonData(data, WebServiceType.EnrollLearningPath);

                    if (!passJasonSchema)
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace((string)o["idUser"]))
                            enrollmentObject.IdUser = (int)o["idUser"];

                        if (!String.IsNullOrWhiteSpace((string)o["idLearningPath"]))
                            enrollmentObject.IdLearningPath = (int)o["idLearningPath"];

                        if (!String.IsNullOrWhiteSpace((string)o["dtStart"]))
                            enrollmentObject.DtStart = (DateTime)o["dtStart"];

                        if (!String.IsNullOrWhiteSpace((string)o["idTimezone"]))
                            enrollmentObject.IdTimezone = (int)o["idTimezone"];

                        if (!String.IsNullOrWhiteSpace((string)o["dueInterval"]))
                            enrollmentObject.DueInterval = (int)o["dueInterval"];

                        if (!String.IsNullOrWhiteSpace((string)o["dueTimeframe"]))
                            enrollmentObject.DueTimeframe = (string)o["dueTimeframe"];
                    }
                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors;
                    doc = XDocument.Parse(data);

                    xmlErrors = _ValidateXMLData(data, WebServiceType.EnrollLearningPath);

                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            if (String.IsNullOrWhiteSpace(returnString))
                            { returnString += error.Desc; }
                            else
                            { returnString += " " + error.Desc; }
                        }

                        throw new AsentiaException(returnString);
                    }
                    else
                    {
                        if (doc.Element("learningPathEnrollment").Element("idUser") != null)
                            enrollmentObject.IdUser = (int)doc.Element("learningPathEnrollment").Element("idUser");

                        if (doc.Element("learningPathEnrollment").Element("idLearningPath") != null)
                            enrollmentObject.IdLearningPath = (int)doc.Element("learningPathEnrollment").Element("idLearningPath");

                        if (doc.Element("learningPathEnrollment").Element("dtStart") != null)
                            enrollmentObject.DtStart = (DateTime)doc.Element("learningPathEnrollment").Element("dtStart");

                        if (doc.Element("learningPathEnrollment").Element("idTimezone") != null)
                            enrollmentObject.IdTimezone = (int)doc.Element("learningPathEnrollment").Element("idTimezone");

                        if (doc.Element("learningPathEnrollment").Element("dueInterval") != null)
                            enrollmentObject.DueInterval = (int)doc.Element("learningPathEnrollment").Element("dueInterval");

                        if (doc.Element("learningPathEnrollment").Element("dueTimeframe") != null)
                            enrollmentObject.DueTimeframe = doc.Element("learningPathEnrollment").Element("dueTimeframe").Value.Trim();
                    }
                }

                enrollmentObject.Id = enrollmentObject.Save();

                returnString += successMsg;

                return _ReturnSuccess(returnString, returnXML, false);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion
        #endregion

        #endregion

        #region XML Private Methods


        #region ValidateXMLData - Returns only error list
        /// <summary>
        /// Validates the imsmanifest.xml file.
        /// </summary>
        /// <exception>
        /// </exception>
        /// <param name="imsmanifestFile">This is the imsmanifest.xml file to validate. It could be a relative or an absolute path.</param>
        /// <param name="isResource">It will be true if the type of the SCORM package is Resource and false if it is Content.</param>
        private ArrayList _ValidateXMLData(string xmlData, WebServiceType webServiceType)
        {
            string xsdFilePath = string.Empty;
            try
            {
                //Errors Collection
                _Errors = new ArrayList();

                XmlUrlResolver resolver = new XmlUrlResolver();
                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;
                settings.XmlResolver = resolver;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

                XmlReaderSettings schemaSettings = new XmlReaderSettings();
                schemaSettings.DtdProcessing = DtdProcessing.Parse;


                switch (webServiceType)
                {
                    case WebServiceType.GetUser:
                        xsdFilePath = SitePathConstants.API_Schema + "GetUser.xsd";
                        break;
                    case WebServiceType.GetCourse:
                        xsdFilePath = SitePathConstants.API_Schema + "GetCourse.xsd";
                        break;
                    case WebServiceType.GetGroup:
                        xsdFilePath = SitePathConstants.API_Schema + "GetGroup.xsd";
                        break;
                    case WebServiceType.GetEnrollment:
                        xsdFilePath = SitePathConstants.API_Schema + "GetEnrollment.xsd";
                        break;
                    case WebServiceType.SaveUser:
                    case WebServiceType.AddUser:
                        xsdFilePath = SitePathConstants.API_Schema + "SaveUser.xsd";
                        break;
                    case WebServiceType.SaveCourse:
                        xsdFilePath = SitePathConstants.API_Schema + "SaveCourse.xsd";
                        break;
                    case WebServiceType.SaveGroup:
                        xsdFilePath = SitePathConstants.API_Schema + "SaveGroup.xsd";
                        break;
                    case WebServiceType.EnrollCourse:
                        xsdFilePath = SitePathConstants.API_Schema + "EnrollCourse.xsd";
                        break;
                    case WebServiceType.EnrollLearningPath:
                        xsdFilePath = SitePathConstants.API_Schema + "EnrollLearningPath.xsd";
                        break;
                    case WebServiceType.AttachUserToGroups:
                    case WebServiceType.RemoveUserFromGroups:
                        xsdFilePath = SitePathConstants.API_Schema + "AttachUserToGroups.xsd";
                        break;
                    case WebServiceType.GetCourseCatalog:
                        xsdFilePath = SitePathConstants.API_Schema + "GetCourseCatalog.xsd";
                        break;
                    case WebServiceType.GetSSOToken:
                        xsdFilePath = SitePathConstants.API_Schema + "GetSSOToken.xsd";
                        break;
                    case WebServiceType.GetLearningPath:
                        xsdFilePath = SitePathConstants.API_Schema + "GetLearningPath.xsd";
                        break;

                    default:
                        break;
                }

                settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath(xsdFilePath), schemaSettings));

                settings.Schemas.Compile();
                settings.ValidationType = ValidationType.Schema;
                //Set the handler for catching xml validation errors
                settings.ValidationEventHandler += new System.Xml.Schema.ValidationEventHandler(_ValidationEventHandler);
                XDocument doc;
                using (StringReader s = new StringReader(xmlData))
                {
                    doc = XDocument.Load(s);
                }

                // Create the XmlReader object
                using (XmlReader reader = XmlReader.Create(new StringReader(xmlData), settings))
                {
                    // Parse the file.
                    while (reader.Read()) // Read and validate against the relevant xsd file (if an error comes the error catching event is called)
                    { }
                }

                return _Errors;
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion

        #region _ValidationEventHandler
        /// <summary>
        /// It is called when the xml file fails to vaildate against the given xsd files.
        /// </summary>
        /// <param name=""></param>
        private void _ValidationEventHandler(object sender, System.Xml.Schema.ValidationEventArgs e)
        {
            try
            {
                IXmlLineInfo lineInfo = sender as IXmlLineInfo;
                ValidationError error = new ValidationError();
                if (lineInfo != null)
                {
                    error.LineNumber = lineInfo.LineNumber;
                    error.PositionNumber = lineInfo.LinePosition;
                }
                error.Desc = e.Message;

                _Errors.Add(error);
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }
        #endregion

        #endregion

        #region Get Serialized Objects Private Methods

        private string _GetCourseSerialized(int idCourse, bool returnXML)
        {
            Course courseObject = new Course(idCourse);

            if (returnXML)
            {
                StringBuilder returnDocument = new StringBuilder();
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Course));
                XmlDocument targetDocument = new XmlDocument();
                var nav = targetDocument.CreateNavigator();

                //Create our own namespaces for the output
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                //Add an empty namespace and empty value
                ns.Add("", "");

                using (XmlWriter writer = nav.AppendChild())
                {
                    xmlSerializer.Serialize(writer, courseObject, ns);
                }

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;

                StringWriter stringWriter = new StringWriter();
                using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
                {
                    targetDocument.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                }

                string returnString = _ReturnSimplifiedFormattedXML(stringWriter.GetStringBuilder().ToString());
                string[] cDataTagNames = new string[] { "Title", "ShortDescription", "LongDescription", "Objectives", "SearchTags" };
                returnString = _WriteCData(returnString, cDataTagNames);

                return returnString;
            }
            else // JSON
            {
                return JsonConvert.SerializeObject(courseObject).Replace("\\n", string.Empty).Replace("\\r", string.Empty);
            }
        }

        private string _GetCourseCatalogSerialized(int idCatalog, bool returnXML)
        {
            StringBuilder idCourseStr = new StringBuilder();
            Asentia.LMS.Library.Catalog catalogObject = new Asentia.LMS.Library.Catalog(idCatalog);

            DataTable dtCourse = catalogObject.GetCoursesInDataTable(idCatalog);
            int rowNum = 0;
            foreach (DataRow row in dtCourse.Rows)
            {
                idCourseStr.Append(Convert.ToString(row["idCourse"]) + ",");
                rowNum++;
            }
            if (rowNum > 0)
            {
                idCourseStr.Length--;
            }

            if (returnXML)
            {
                StringBuilder returnDocument = new StringBuilder();
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Asentia.LMS.Library.Catalog));
                XmlDocument targetDocument = new XmlDocument();
                var nav = targetDocument.CreateNavigator();

                //Create our own namespaces for the output
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                //Add an empty namespace and empty value
                ns.Add("", "");

                idCourseStr.Append("</CourseIds></Catalog>");

                using (XmlWriter writer = nav.AppendChild())
                {
                    xmlSerializer.Serialize(writer, catalogObject, ns);
                }

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;

                StringWriter stringWriter = new StringWriter();
                using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
                {
                    targetDocument.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                }

                string returnString = _ReturnSimplifiedFormattedXML(stringWriter.GetStringBuilder().ToString());
                returnString = returnString.Replace("</Catalog>", "<CourseIds>" + idCourseStr.ToString());
                string[] cDataTagNames = new string[] { "Title", "ShortDescription", "LongDescription", "SearchTags" };
                returnString = _WriteCData(returnString, cDataTagNames);

                return returnString;

            }
            else // JSON
            {
                StringBuilder stringReturnJSON = new StringBuilder();
                stringReturnJSON.Append(JsonConvert.SerializeObject(catalogObject));
                stringReturnJSON.Length--;
                stringReturnJSON.Append(",\"CourseIds\":\"");
                stringReturnJSON.Append(idCourseStr.ToString());
                stringReturnJSON.Append("\"}");

                return stringReturnJSON.ToString().Replace("\\n", string.Empty).Replace("\\r", string.Empty);
            }

        }

        private string _GetLearningPathSerialized(int idLearningPath, bool returnXML)
        {
            LearningPath learningPathObject = new LearningPath(idLearningPath);

            if (returnXML)
            {
                StringBuilder returnDocument = new StringBuilder();
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(LearningPath));
                XmlDocument targetDocument = new XmlDocument();
                var nav = targetDocument.CreateNavigator();

                //Create our own namespaces for the output
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                //Add an empty namespace and empty value
                ns.Add("", "");

                using (XmlWriter writer = nav.AppendChild())
                {
                    xmlSerializer.Serialize(writer, learningPathObject, ns);
                }

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;

                StringWriter stringWriter = new StringWriter();
                using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
                {
                    targetDocument.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                }

                string returnString = _ReturnSimplifiedFormattedXML(stringWriter.GetStringBuilder().ToString());
                string[] cDataTagNames = new string[] { "Name", "ShortDescription", "LongDescription", "SearchTags" };
                returnString = _WriteCData(returnString, cDataTagNames);

                return returnString;
            }
            else // JSON
            {
                return JsonConvert.SerializeObject(learningPathObject).Replace("\\n", string.Empty).Replace("\\r", string.Empty);
            }

        }

        private string _GetEnrollmentSerialized(int idEnrollment, bool returnXML)
        {
            Enrollment enrollmentObject = new Enrollment(idEnrollment);

            if (returnXML)
            {
                StringBuilder returnDocument = new StringBuilder();
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Enrollment));
                XmlDocument targetDocument = new XmlDocument();
                var nav = targetDocument.CreateNavigator();

                //Create our own namespaces for the output
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                //Add an empty namespace and empty value
                ns.Add("", "");

                using (XmlWriter writer = nav.AppendChild())
                {
                    xmlSerializer.Serialize(writer, enrollmentObject, ns);
                }

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;

                StringWriter stringWriter = new StringWriter();
                using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
                {
                    targetDocument.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                }

                string returnString = _ReturnSimplifiedFormattedXML(stringWriter.GetStringBuilder().ToString());
                string[] cDataTagNames = new string[] { "Title" };
                returnString = _WriteCData(returnString, cDataTagNames);

                return returnString;
            }
            else // JSON
            {
                return JsonConvert.SerializeObject(enrollmentObject).Replace("\\n", string.Empty).Replace("\\r", string.Empty);
            }

        }

        private string _GetGroupSerialized(int idGroup, bool returnXML)
        {
            Group groupObject = new Group(idGroup);

            if (returnXML)
            {
                StringBuilder returnDocument = new StringBuilder();
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Group));
                XmlDocument targetDocument = new XmlDocument();
                var nav = targetDocument.CreateNavigator();

                //Create our own namespaces for the output
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                //Add an empty namespace and empty value
                ns.Add("", "");

                using (XmlWriter writer = nav.AppendChild())
                {
                    xmlSerializer.Serialize(writer, groupObject, ns);
                }

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;

                StringWriter stringWriter = new StringWriter();
                using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
                {
                    targetDocument.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                }

                string returnString = _ReturnSimplifiedFormattedXML(stringWriter.GetStringBuilder().ToString());
                string[] cDataTagNames = new string[] {"Name", "ShortDescription", "LongDescription", "SearchTags"};
                returnString = _WriteCData(returnString, cDataTagNames);

                return returnString;
            }
            else // JSON
            {
                return JsonConvert.SerializeObject(groupObject).Replace("\\n", string.Empty).Replace("\\r", string.Empty);
            }

        }

        private string _GetUserSerialized(int idUser, bool returnXML)
        {
            User userObject = new User(idUser);

            if (returnXML)
            {
                StringBuilder returnDocument = new StringBuilder();
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(User));
                XmlDocument targetDocument = new XmlDocument();
                var nav = targetDocument.CreateNavigator();

                //Create our own namespaces for the output
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                //Add an empty namespace and empty value
                ns.Add("", "");

                using (XmlWriter writer = nav.AppendChild())
                {
                    xmlSerializer.Serialize(writer, userObject, ns);
                }

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;

                StringWriter stringWriter = new StringWriter();
                using (var xmlTextWriter = XmlWriter.Create(stringWriter, settings))
                {
                    targetDocument.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                }

                string returnString = _ReturnSimplifiedFormattedXML(stringWriter.GetStringBuilder().ToString());
                string[] cDataTagNames = new string[] { "FirstName", "MiddleName", "LastName", "DisplayName", 
                    "DistinguishedName", "Email", "Username", "Password", "Company", "Address", "City", 
                    "Province", "Country", "Department", "Division", "Region", "JobTitle", "JobClass", 
                    "Race", "Field00", "Field01", "Field02", "Field03", "Field04", "Field05", "Field06", "Field07", "Field08", 
                    "Field09", "Field10", "Field11", "Field12", "Field13", "Field14", "Field15", "Field16", "Field17", "Field18", "Field19"};
                returnString = _WriteCData(returnString, cDataTagNames);
                returnString = returnString.Replace("<int>", "<Id>").Replace("</int>", "</Id>");

                return returnString;
            }
            else // JSON
            {
                return JsonConvert.SerializeObject(userObject).Replace("\\n", ",").Replace("\\r", string.Empty);
            }

        }

        #endregion

        #region JSON Validation
        /// <summary>
        /// Validate JSon Data
        /// </summary>
        private bool _ValidateJsonData(string jsonData, WebServiceType webServiceType)
        {
            string jsonFilePath = string.Empty;

            switch (webServiceType)
            {
                case WebServiceType.GetUser:
                    jsonFilePath = SitePathConstants.API_Schema + "GetUser.json";
                    break;
                case WebServiceType.GetCourse:
                    jsonFilePath = SitePathConstants.API_Schema + "GetCourse.json";
                    break;
                case WebServiceType.GetGroup:
                    jsonFilePath = SitePathConstants.API_Schema + "GetGroup.json";
                    break;
                case WebServiceType.GetEnrollment:
                    jsonFilePath = SitePathConstants.API_Schema + "GetEnrollment.json";
                    break;
                case WebServiceType.AddUser:
                case WebServiceType.SaveUser:
                    jsonFilePath = SitePathConstants.API_Schema + "SaveUser.json";
                    break;
                case WebServiceType.SaveCourse:
                    jsonFilePath = SitePathConstants.API_Schema + "SaveCourse.json";
                    break;
                case WebServiceType.SaveGroup:
                    jsonFilePath = SitePathConstants.API_Schema + "SaveGroup.json";
                    break;
                case WebServiceType.EnrollCourse:
                    jsonFilePath = SitePathConstants.API_Schema + "EnrollCourse.json";
                    break;
                case WebServiceType.EnrollLearningPath:
                    jsonFilePath = SitePathConstants.API_Schema + "EnrollLearningPath.json";
                    break;
                case WebServiceType.AttachUserToGroups:
                case WebServiceType.RemoveUserFromGroups:
                    jsonFilePath = SitePathConstants.API_Schema + "AttachUserToGroups.json";
                    break;
                case WebServiceType.GetCourseCatalog:
                    jsonFilePath = SitePathConstants.API_Schema + "GetCourseCatalog.json";
                    break;
                case WebServiceType.GetSSOToken:
                    jsonFilePath = SitePathConstants.API_Schema + "GetSSOToken.json";
                    break;
                case WebServiceType.GetLearningPath:
                    jsonFilePath = SitePathConstants.API_Schema + "GetLearningPath.json";
                    break;
                default:
                    break;
            }
            string schemaJson = File.ReadAllText(HttpContext.Current.Server.MapPath(jsonFilePath));

            JsonSchema schema = JsonSchema.Parse(schemaJson);
            try
            {
                JObject person = JObject.Parse(jsonData);
                return person.IsValid(schema);
            }
            catch (Exception e)
            {
                return true;
            }
        }
        #endregion

        #region WebService: GetSSOToken
        /// <summary>
        /// Web Services : GetSSOToken
        /// </summary>
        [WebMethod(EnableSession = true)]
        public string GetSSOToken(string payload)
        {
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);
            AsentiaSessionState.IdSiteUser = 1;

            int idGroup;
            StringBuilder returnString = new StringBuilder();

            string apiKey = string.Empty;
            bool returnXML = true;
            string data = string.Empty;
            string payloadNoSpace = System.Text.RegularExpressions.Regex.Replace(payload, @"\s+", "");
            string defaultReturnString = string.Empty;

            try
            {
                if (payloadNoSpace.Length < 12)
                    throw new AsentiaException(_NotCorrectFormat);
                if (payloadNoSpace.Substring(0, 11) == "{\"request\":")
                {
                    JObject o = JObject.Parse(@payload);
                    apiKey = (string)o["request"]["securityContext"]["key"];
                    returnXML = ((string)o["request"]["responseFormat"]).ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("\"payload\""));
                    data = data.Substring(data.IndexOf("{"));
                    data = data.Remove(data.LastIndexOf("}"), 1);
                    data = data.Remove(data.LastIndexOf("}"), 1).Trim();
                }
                else if (payloadNoSpace.Substring(0, 9) == "<request>")
                {
                    XDocument request = XDocument.Parse(payload);
                    apiKey = request.Element("request").Element("securityContext").Element("key").Value.Trim();
                    returnXML = request.Element("request").Element("responseFormat").Value.ToLower() == "xml";
                    data = payload.Substring(payload.IndexOf("<payload>") + 9).Replace("</payload>", "").Replace("</request>", "").Trim();
                }
                else
                {
                    throw new AsentiaException(_NotCorrectFormat);
                }

                defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;


                if (data.Substring(0, 1) == "{")
                {
                    if (!_ValidateJsonData(data, WebServiceType.GetSSOToken))
                    {
                        throw new AsentiaException(_GlobalResources.JSONFormatIsNotValid);
                    }
                    else
                    {
                        returnString.Append((returnXML) ? "<SSOToken><Token>" : "{\"Token\":\"");
                        JObject o = JObject.Parse(@data);
                        if (!String.IsNullOrWhiteSpace((string)o["id"]))
                        {
                            idGroup = (int)o["id"];
                            returnString.Append(Utility.GenerateTokenForSSO(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, 1, (int)o["id"]));
                        }
                        else
                        {
                            return defaultReturnString.Replace("##description##", _GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id").Replace("##message##", _GlobalResources.Error).Replace("##payload##", "");
                        }
                        returnString.Append((returnXML) ? "</Token></SSOToken>" : "\"}");
                    }
                }
                else if (data.Substring(0, 1) == "<")
                {
                    ArrayList xmlErrors = _ValidateXMLData(data, WebServiceType.GetSSOToken);
                    if (xmlErrors != null && xmlErrors.Count > 0)
                    {
                        foreach (ValidationError error in xmlErrors)
                        {
                            returnString.Append(" ");
                            returnString.Append(error.Desc);
                        }
                        throw new AsentiaException(returnString.ToString());
                    }
                    else
                    {
                        returnString.Append((returnXML) ? "<SSOToken><Token>" : "{\"Token\":\"");
                        XDocument doc = XDocument.Parse(data);

                        if (doc.Element("getSSOToken").Element("id") != null)
                        {
                            returnString.Append(Utility.GenerateTokenForSSO(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, 1, Convert.ToInt32(doc.Element("getSSOToken").Element("id").Value)));
                        }
                        else
                        {
                            return defaultReturnString.Replace("##description##", _GlobalResources.OneOfTheFollowingPropertiesNeedsToBeAssignedAValue + ": id").Replace("##message##", _GlobalResources.Error).Replace("##payload##", "");
                        }
                        returnString.Append((returnXML) ? "</Token></SSOToken>" : "\"}");
                    }
                }

                return _ReturnSuccess(returnString.ToString(), returnXML, true);
            }
            catch (Exception e)
            {
                return _ReturnError(e.Message, returnXML);
            }
            finally
            {
                // kill the session so we are not leaving an administrator session open
                HttpContext.Current.Session.Abandon();
            }
        }
        #endregion


        private void _CheckAPIAccess()
        {
            if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_API_ENABLED))
            {
                throw new AsentiaException("API Access For This Site Is Not Open");
            }
        }

        private void _CheckAPIkey(string inputAPIKey)
        {
            _CheckAPIAccess();

            if (inputAPIKey != AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_API_KEY))
            {
                throw new AsentiaException(_GlobalResources.APIKeyIsNotValid);
            }
        }

        private string _ReturnError(string message, bool returnXML)
        {
            string defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

            string formatedErrorMessage = string.Empty;

            formatedErrorMessage = defaultReturnString.Replace("##payload##", returnXML? string.Empty: "null").Replace("##message##", _GlobalResources.Error).Replace("##description##", message);

            return formatedErrorMessage;
        }

        private string _ReturnSuccess(string message, bool returnXML, bool getMethod)
        {
            string defaultReturnString = returnXML ? _XMLDefaultReturnString : _JSONDefaultReturnString;

            string formatedErrorMessage = string.Empty;

            if (getMethod)
            {
                formatedErrorMessage = defaultReturnString.Replace("##payload##", message).Replace("##message##", _GlobalResources.Success).Replace("##description##", returnXML ? string.Empty : "null");
            }
            else
            {
                formatedErrorMessage = defaultReturnString.Replace("##payload##", returnXML ? string.Empty : "null").Replace("##message##", _GlobalResources.Success).Replace("##description##", message);
            }

            return formatedErrorMessage;
        }


        private string _ReturnSimplifiedFormattedXML(string xmlString)
        {
            string returnSimplifiedFormattedXMLString = string.Empty;

            returnSimplifiedFormattedXMLString = xmlString.Replace(_XMLNamespaceAttributeA, string.Empty);
            returnSimplifiedFormattedXMLString = returnSimplifiedFormattedXMLString.Replace(_XMLNamespaceAttributeB, string.Empty);
            returnSimplifiedFormattedXMLString = returnSimplifiedFormattedXMLString.Replace(_XMLNamespaceAttributeC, string.Empty);
            returnSimplifiedFormattedXMLString = returnSimplifiedFormattedXMLString.Replace(_XMLNamespaceAttributeD, string.Empty);

            return returnSimplifiedFormattedXMLString;
        }

        private string _WriteCData(string xmlString, string[] tagNames)
        {
            string returnString = xmlString;

            foreach (string tagName in tagNames)
            {
                returnString = returnString.Replace("<" + tagName + ">", "<" + tagName + "><![CDATA[");
                returnString = returnString.Replace("</" + tagName + ">", "]]></" + tagName + ">");
            }
            return returnString;
        }

        private StringBuilder _GetRootElement(bool returnXML, StringBuilder returnString, string objectString, int i)
        {
            if (returnXML)
            {
                string pattern =  @"(<anyType p3:type=)[^>]*>";
                System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(pattern);
                string result = rgx.Replace(returnString.ToString(), string.Empty);
                returnString.Clear();
                returnString.Append(result);
                returnString.Insert(0, "<" + objectString + "s>").Append("</" + objectString + "s>");
            }
            else
            {
                if (i > 1)
                {
                    returnString.Insert(0, "{\"" + objectString + "s\":{\"" + objectString + "\":[").Append("]}}");
                }
                else
                {
                    returnString.Insert(0, "{\"" + objectString + "s\":{\"" + objectString + "\":").Append("}}");
                }
            }
            return returnString;
        }

    }
}
