﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages._Util
{
    public class DoSSO : AsentiaPage
    {
        #region Private Properties
        private string _Token;        
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // ensure we do not already have a logged in user
            if (AsentiaSessionState.IdSiteUser > 0)
            { Response.Redirect("/"); }

            // get the token, bounces if there is no token
            this._GetToken();

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/_util/DoSSO.css");

            // validate the token and do the sso, errors throw exceptions, and exceptions will be caught and displayed
            try
            {
                int idUser = 0;

                // validate the token
                idUser = Utility.ValidateTokenForSSO(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, 1, this._Token);
                
                if (idUser == 0)
                { throw new AsentiaException(_GlobalResources.UnableToFindUserForSSO); }

                // IF WE GET HERE, THE TOKEN IS VALID, DO THE SSO

                // if the user id is 1, do SSO as admin user
                // otherwise, do it as a normal user, which means load a user object and do proper validations before logging in
                if (idUser == 1)
                {
                    // set the session state properties
                    AsentiaSessionState.IdSiteUser = 1;
                    AsentiaSessionState.UserFirstName = "ADMINISTRATOR";
                    AsentiaSessionState.UserLastName = String.Empty;
                    AsentiaSessionState.UserAvatar = null;
                    AsentiaSessionState.UserGender = null;
                    AsentiaSessionState.UserCulture = AsentiaSessionState.GlobalSiteObject.LanguageString;
                    AsentiaSessionState.IsUserACourseExpert = true;
                    AsentiaSessionState.IsUserACourseApprover = true;
                    AsentiaSessionState.IsUserASupervisor = true;
                    AsentiaSessionState.IsUserAWallModerator = true;
                    AsentiaSessionState.IsUserAnILTInstructor = true;
                    AsentiaSessionState.UserMustChangePassword = false;                    
                }
                else
                {
                    AsentiaSessionState.IdSiteUser = idUser;

                    // get the user object
                    User userObject = new User(idUser);

                    // ensure that the session state site matches the user site
                    if (AsentiaSessionState.IdSite != userObject.IdSite)
                    { throw new AsentiaException(_GlobalResources.TheUserAttemptingToSignInDoesNotBelongToThisPortal); }

                    // ensure the user is not deleted
                    if (userObject.IsDeleted)
                    { throw new AsentiaException(_GlobalResources.TheUserAttemptingToSignInDoesNotBelongToThisPortal); }

                    // ensure the user is active
                    if (!userObject.IsActive)
                    { throw new AsentiaException(_GlobalResources.TheUserAttemptingToSignInIsNotCurrentlyActive); }

                    // ensure the user has not expired
                    if (userObject.DtExpires != null && userObject.DtExpires < AsentiaSessionState.UtcNow)
                    { throw new AsentiaException(_GlobalResources.TheUserAttemptingToSignInHasAnExpiredAccount); }

                    // set the session state properties                    
                    AsentiaSessionState.UserFirstName = userObject.FirstName;
                    AsentiaSessionState.UserLastName = userObject.LastName;
                    AsentiaSessionState.UserAvatar = userObject.Avatar;
                    AsentiaSessionState.UserGender = userObject.Gender;
                    AsentiaSessionState.UserCulture = userObject.LanguageString;
                    AsentiaSessionState.IsUserACourseExpert = userObject.IsUserACourseExpert;
                    AsentiaSessionState.IsUserACourseApprover = userObject.IsUserACourseApprover;
                    AsentiaSessionState.IsUserASupervisor = userObject.IsUserASupervisor;
                    AsentiaSessionState.IsUserAWallModerator = userObject.IsUserAWallModerator;
                    AsentiaSessionState.IsUserAnILTInstructor = userObject.IsUserAnILTInstructor;
                    AsentiaSessionState.UserMustChangePassword = userObject.MustChangePassword;

                    // get the user's effective permissions
                    DataTable effectivePermissions = UMS.Library.User.GetEffectivePermissions();
                    List<AsentiaPermissionWithScope> effectivePermissionsList = new List<AsentiaPermissionWithScope>();

                    foreach (DataRow row in effectivePermissions.Rows)
                    {
                        AsentiaPermissionWithScope permissionWithScope = new AsentiaPermissionWithScope((AsentiaPermission)Convert.ToInt32(row["idPermission"]), row["scope"].ToString());
                        effectivePermissionsList.Add(permissionWithScope);
                    }

                    AsentiaSessionState.UserEffectivePermissions = effectivePermissionsList;
                }

                // if we get here without throwing an exception, it means a successful login, so redirect to the landing page
                Response.Redirect(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_DEFAULTLANDINGPAGE_LOCALPATH));
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                this.DisplayFeedback(dnfEx.Message, true, false);
            }
            catch (AsentiaException aEx)
            {
                this.DisplayFeedback(aEx.Message, true, false);
            }
            catch (Exception ex)
            {
                this.DisplayFeedback(ex.Message + " | " + ex.StackTrace, true, false);
            }            
        }
        #endregion

        #region _GetToken
        /// <summary>
        /// Gets the token based on querystring if exists.
        /// </summary>
        private void _GetToken()
        {
            // get the token querystring parameter
            this._Token = this.QueryStringString("token", "");

            // redirect if the token is invalid
            if (String.IsNullOrWhiteSpace(this._Token))
            { Response.Redirect("/"); }            
        }
        #endregion
    }
}