﻿#region Includes

using System;
using System.Collections;
using System.Runtime.Serialization;

#endregion

#region ManifestDataModel

// KnownTypes for JSON Serialization
[KnownType(typeof(OrganizationDataModel))]
[KnownType(typeof(ItemDataModel))]
[KnownType(typeof(CompletionThresholdDataModel))]
[KnownType(typeof(ADLCPDataMapDataModel))]
[KnownType(typeof(ResourceDataModel))]
[KnownType(typeof(SequencingDataModel))]
[KnownType(typeof(ControlModeDataModel))]
[KnownType(typeof(SequencingRuleDataModel))]
[KnownType(typeof(RuleConditionDataModel))]
[KnownType(typeof(LimitConditionsDataModel))]
[KnownType(typeof(RollupRulesDataModel))]
[KnownType(typeof(RollupRuleDataModel))]
[KnownType(typeof(RollupConditionDataModel))]
[KnownType(typeof(IMSSSObjectivesDataModel))]
[KnownType(typeof(IMSSSObjectiveDataModel))]
[KnownType(typeof(IMSSSMapInfoDataModel))]
[KnownType(typeof(RandomizationControlsDataModel))]
[KnownType(typeof(DeliveryControlsDataModel))]
[KnownType(typeof(ConstrainedChoiceConsiderationsDataModel))]
[KnownType(typeof(RollupConsiderationsDataModel))]
[KnownType(typeof(ADLSEQObjectivesDataModel))]
[KnownType(typeof(ADLSEQObjectiveDataModel))]
[KnownType(typeof(ADLSEQMapInfoDataModel))]
[KnownType(typeof(PresentationDataModel))]

public class ManifestDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public string       defaultOrganization;    // the default organization to use - attribute of <organizations> node
    
    public string       identifier;             // the manifest identifier - attribute of <manifest> node
    
    public ArrayList    organizations;          // ArrayList of OrganizationDataModel objects - <organization> nodes
    
    public ArrayList    resources;              // ArrayList of ResourceDataModel objects - <resource> nodes

    public string       resourcesXmlBase;       // relative path offset for content - attribute of <resources> node
    
    public string       schemaversion;          // the version of scorm that this manifest is targeted to - <schemaversion> node
    
    public ArrayList    sequencingCollection;   // ArrayList of SequencingDataModel objects - <sequencingCollection> nodes (global)
    
    public string       xmlBase;                // relative path offset for content - attribute of <manifest> node

    // Default Constructor
    public ManifestDataModel()
    {
        this.defaultOrganization    = null;
        
        this.identifier             = null;
        
        this.organizations          = new ArrayList();
        
        this.resources              = new ArrayList();

        this.resourcesXmlBase       = null;
        
        this.schemaversion          = null;             // we'll default this to null, but valid values for SCORM 2004
                                                        // are "CAM 1.3" (2nd edition), "2004 3rd Edition" (3rd edition),
                                                        // and "2004 4th Edition", 1st edition is not accounted for
                                                        // TODO: ENUMERATE THIS!!!
        
        this.sequencingCollection   = new ArrayList();
        
        this.xmlBase                = null;
    }
}
#endregion

#region OrganizationDataModel
public class OrganizationDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public CompletionThresholdDataModel adlcpCompletionThreshold;   // CompletionThresholdDataModel object for completion threshold for 
                                                                    // this SCO, CAM 3.4.1.15 - further explanation in the class definition
                                                                    // for CompletionThresholdDataModel
    
    public ArrayList                    items;                      // ArrayList of ItemDataModel objects - <item> nodes
    
    public string                       identifier;                 // the organization identifier - attribute of <organization> node

    public SequencingDataModel          imsssSequencing;            // SequencingDataModel object - <imsss:sequencing> (global to <item>s/SCOs
                                                                    // within this <organization>), for <organization> it is 0 or 1
    
    
    public bool?                        objectivesGlobalToSystem;   // indicates that any mapped shared global objectives either apply to all organizations
                                                                    // within the manifest (true), or only this organization (false) - attribute of <organization> node
    
    public bool?                        sharedDataGlobalToSystem;   // indicates that any shared data mapped across SCOs apply to all organizations within the
                                                                    // manifest (true), or only this organization (false) - attribute of <organization> node
    
    public string                       structure;                  // the structure of the organization - attribute of <organization> node
    
    public string                       title;                      // the title of the organization - <title> node (1 and only 1)
    
    public string                       treeHTML;                   // a nested HTML <ul> structure that represents the activity tree for this organization

    // Default Constructor
    public OrganizationDataModel()
    {
        this.adlcpCompletionThreshold   = null; //new CompletionThresholdDataModel();
        
        this.items                      = new ArrayList();
        
        this.identifier                 = null;
        
        this.imsssSequencing            = null; //new SequencingDataModel();
        
        this.objectivesGlobalToSystem   = null;             // default is true, CAM 3.4.1.7
        
        this.sharedDataGlobalToSystem   = null;             // default is true, CAM 3.4.1.7
        
        this.structure                  = null; //"hierarchical";   // right now it's always "hierarchical" because SCORM
                                                            // doesn't currently specify any other value; so we
                                                            // will just set it here in the constructor and not
                                                            // bother to parse the attribute
        
        this.title                      = null;
        
        this.treeHTML                   = null;
    }
}
#endregion

#region ItemDataModel
public class ItemDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public CompletionThresholdDataModel adlcpCompletionThreshold;   // CompletionThresholdDataModel object for completion threshold for 
                                                                    // this SCO, CAM 3.4.1.15 - further explanation in the class definition
                                                                    // for CompletionThresholdDataModel
    
    public ArrayList                    adlcpData;                  // ArrayList of ADLCPDataMapDataModel objects - <adlcp:map> nodes 
                                                                    // within <adlcp:Data>
    
    public string                       adlcpDataFromLMS;           // provides initialization data expected by the SCO, CAM 3.4.1.14
    
    public string                       adlcpTimeLimitAction;       // defines the action the SCO should take when the maximum
                                                                    // time allowed in the current attempt of the activity is
                                                                    // exceeded, valid values are "exit, message", "exit, no message",
                                                                    // "continue, message", and "continue, no message" - CAM 3.4.1.13

    public PresentationDataModel        adlnavPresentation;         // PresentationDataModel object for controlling the display of
                                                                    // LMS navigation elements for this SCO. Determines what LMS
                                                                    // supplied SCO navigation to show/hide - <adlnav:presentation>
    
    public string                       identifier;                 // the item (SCO) identifier - attribute of <item> node
    
    public string                       identifierref;              // reference to a resource identifier - attribute of <item> node
    
    public bool?                        isvisible;                  // indicates whether or not this item should be displayed in the
                                                                    // activity tree - attribute of <item> node
    
    public int                          level;                      // the level of the tree in which this item is nested - this is
                                                                    // used internally to help build the treeHTML for the organization
                                                                    // and is not part of the manifest
    
    public string                       parentIdentifier;           // the identifier of the parent <item> of this <item> - this is
                                                                    // used internally to help build the treeHTML for the organization
                                                                    // and is not part of the manifest
    
    public string                       parameters;                 // querystring parameters that should be appended to the href of
                                                                    // the resource identified in this item's identifierref property -
                                                                    // attribute of <item> node

    public SequencingDataModel          imsssSequencing;            // SequencingDataModel object - <imsss:sequencing> (local to this <item>/SCO)
                                                                    // for <item> it is 0 or 1

    public string                       adlcpPrerequisites;         // the Pre-requisites rules for item - <adlcp:prerequisites> (available for scorm 1.2 only)

    public string                       title;                      // the title of the item - <title> node (1 and only 1)


    // NOTE: FOR SEQUENCING AND ROLLUP STUFF, WE MAY WANT TO ELIMINATE LEVEL AND 
    // PARENT (WHICH KEEPS THIS AS ONE ARRAY LIST IN THE ORG DM) IN FAVOR OF ANOTHER
    // ARRAY LIST OF ITEMS (ITEM DM) HERE (INSTANTIATION OF THIS CLASS WITHIN THIS CLASS
    // AND POTENTIALLY ON AND ON AND ON)


    // Default Constructor
    public ItemDataModel()
    {
        this.adlcpCompletionThreshold   = null; //new CompletionThresholdDataModel();

        this.adlcpData                  = new ArrayList();

        this.adlcpDataFromLMS           = null;

        this.adlcpTimeLimitAction       = null;

        this.adlnavPresentation         = null; //new PresentationDataModel();
        
        this.identifier                 = null;
        
        this.identifierref              = null;
        
        this.isvisible                  = null; //true; // default is true, CAM 3.4.1.9
        
        this.level                      = 0;
        
        this.parentIdentifier           = null;
        
        this.parameters                 = null;

        this.imsssSequencing            = null; //new SequencingDataModel();
        
        this.title                      = null;
    }
}
#endregion

#region CompletionThresholdDataModel
public class CompletionThresholdDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    // This entire model is outlined in CAM 3.4.1.15 (both 3rd and 4th edition) 
    // The properties that we will use will depend on whether this is actually defined
    // (isDefined = true), and the SCORM edition as determined by the manifest's <schemaversion>
    public bool?     completedByMeasure;         // determines whether or not to use measure to determine completion, 
                                                // attribute of <adlcp:completionThreshold> in 4th edition
    
    public double?   completionThreshold;        // completion threshold value from completionThreshold element, 2nd/3rd editions
    
    public bool?     isDefined;                  // determines whether <adlcp:completionThreshold> exists in the
                                                // <organization> or <item> where this class is instantiated
                                                // this is used internally and is not part of the manifest
    
    public double?   minProgressMeasure;         // minimum progress the user must pass for completion, attribute of
                                                // <adlcp:CompletionThreshold> in 4th edition
    
    public double?   progressWeight;             // the weight of the measure in relation to the completion of other SCOs,
                                                // contributes to rollup, attribute of <adlcp:CompletionThreshold> in 4th edition

    // Default Constructor
    public CompletionThresholdDataModel()
    {
        this.completedByMeasure     = null; // false;    // default is false 
        
        this.completionThreshold    = null; //0.0;      // used in 2nd/3rd edition, 4th edition uses the attributes completedByMeasure,
                                                // minProgressMeasure, and progressWeight - valid range 0.0 - 1.0, no default
                                                // value, so we'll just set it to the minimum valid value
        
        this.isDefined              = null; //false;    // we'll default this to false until we determine whether <adlcp:completionThreshold> exists
        
        this.minProgressMeasure     = null; //1.0;      // valid range 0.0000 - 1.0000, default is 1.0
        
        this.progressWeight         = null; //1.0;      // valid range 0.0000 - 1.0000, default is 1.0
    }
}
#endregion

#region ADLCPDataMapDataModel
public class ADLCPDataMapDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public bool?    readSharedData;     // indicates that shared data can be read by the SCO
    
    public string   targetId;           // the identifier of the shared data (global objective)
    
    public bool?    writeSharedData;    // indicates that shared data can be written to by the SCO

    // Default Constructor
    public ADLCPDataMapDataModel()
    {
        this.readSharedData     = null;     // default is true, CAM 3.4.1.19
        
        this.targetId           = null;
        
        this.writeSharedData    = null;     // default is true, CAM 3.4.1.19
    }
}
#endregion

#region ResourceDataModel
public class ResourceDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public string   adlcpScormType;     // the type of resource "sco" or "asset" - attribute of <resource> node 
    
    public string   href;               // the url to the resource, offset by xmlBase properties - attribute of <resource> node
    
    public string   identifier;         // the resource identifier - attribute of <resource> node 
    
    public string   type;               // the type of resource - most of the time "webcontent" - attribute of <resource> node
    
    public string   xmlBase;            // relative path offset for content - attribute of <resource> node

    // <file>s and <dependency>s might have to be included, but right now I don't see a reason to include them
    // as all resource files and dependencies should be resolved in the content, if we don't HAVE to include this,
    // we shouldn't because it will only increase the size of the JSON string
    
    // Default Constructor
    public ResourceDataModel()
    {
        this.adlcpScormType     = null;
        
        this.href               = null;
        
        this.identifier         = null;
        
        this.type               = null;
        
        this.xmlBase            = null;
    }
}
#endregion

#region SequencingDataModel
public class SequencingDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public ConstrainedChoiceConsiderationsDataModel     adlseqConstrainedChoiceConsiderations;  //
    public ADLSEQObjectivesDataModel                    adlseqObjectives;                       //
    public RollupConsiderationsDataModel                adlseqRollupConsiderations;             //
    public string                                       auxiliaryResources;                     // NOT IMPLEMENTED
    public ControlModeDataModel                         controlMode;                            //
    public DeliveryControlsDataModel                    deliveryControls;                       //
    public ArrayList                                    exitConditionRules;                     // <sequencingRules>
    public string                                       id;                                     //
    public string                                       idRef;                                  //
    public IMSSSObjectivesDataModel                     imsssObjectives;                        //
    public LimitConditionsDataModel                     limitConditions;                        //
    public ArrayList                                    postConditionRules;                     // <sequencingRules>
    public ArrayList                                    preConditionRules;                      // <sequencingRules>
    public RandomizationControlsDataModel               randomizationControls;                  //
    public RollupRulesDataModel                         rollupRules;                            //
    
    // Default Constructor
    public SequencingDataModel()
    {
        this.adlseqConstrainedChoiceConsiderations  = null; //new ConstrainedChoiceConsiderationsDataModel();
        this.adlseqObjectives                       = null; //new ADLSEQObjectivesDataModel();                        
        this.adlseqRollupConsiderations             = null; //new RollupConsiderationsDataModel();
        this.auxiliaryResources                     = null;                                             // NOT IMPLEMENTED
        this.controlMode                            = null; //new ControlModeDataModel();
        this.deliveryControls                       = null; //new DeliveryControlsDataModel();
        this.exitConditionRules                     = new ArrayList();
        this.id                                     = null;
        this.idRef                                  = null;
        this.imsssObjectives                        = null; //new IMSSSObjectivesDataModel();
        this.limitConditions                        = null; //new LimitConditionsDataModel();
        this.postConditionRules                     = new ArrayList();
        this.preConditionRules                      = new ArrayList();
        this.randomizationControls                  = null; //new RandomizationControlsDataModel();
        this.rollupRules                            = null; //new RollupRulesDataModel();
    }
}
#endregion

#region ControlModeDataModel
public class ControlModeDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public bool?     choice;
    public bool?     choiceExit;
    public bool?     flow;
    public bool?     forwardOnly;
    public bool?     useCurrentAttemptObjectiveInfo;
    public bool?     useCurrentAttemptProgressInfo;

    // Default Constructor
    public ControlModeDataModel()
    {
        this.choice                             = null; //true;     // default is true CAM 5.1.2
        this.choiceExit                         = null; //true;     // default is true CAM 5.1.2
        this.flow                               = null; //false;    // default is false CAM 5.1.2
        this.forwardOnly                        = null; //false;    // default is false CAM 5.1.2
        this.useCurrentAttemptObjectiveInfo     = null; //true;     // default is true CAM 5.1.2
        this.useCurrentAttemptProgressInfo      = null; //true;     // default is true CAM 5.1.2
    }
}
#endregion

#region SequencingRuleDataModel
public class SequencingRuleDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public string      conditionCombination;
    public string      ruleAction;
    public ArrayList   ruleConditions;

    // Default Constructor
    public SequencingRuleDataModel()
    {
        this.conditionCombination   = null; //"all";
        this.ruleAction             = null;
        this.ruleConditions         = new ArrayList();
    }
}
#endregion

#region RuleConditionDataModel
public class RuleConditionDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public string    condition;
    public string    conditionOperator;
    public double?   measureThreshold;
    public string    referencedObjective;

    // Default Constructor
    public RuleConditionDataModel()
    {
        this.condition              = null;
        this.conditionOperator      = null; //"noOp";
        this.measureThreshold       = null; //-1.0000;
        this.referencedObjective    = null;
    }

}
#endregion

#region LimitConditionsDataModel
public class LimitConditionsDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public string attemptAbsoluteDurationLimit;
    public int?      attemptLimit;    
    //Added by chetu 
    public string attemptExperiencedDurationLimit;
    public string activityAbsoluteDurationLimit;
    public string activityExperiencedDurationLimit;
    public int? beginTimeLimit;
    public int? endTimeLimit;
    // Default Constructor
    public LimitConditionsDataModel()
    {
        this.attemptAbsoluteDurationLimit   = null;
        this.attemptLimit                   = null; //0; // there is no default
        //Added by chetu
        this.attemptExperiencedDurationLimit = null;
        this.activityAbsoluteDurationLimit = null;
        this.activityExperiencedDurationLimit=null;
        this.beginTimeLimit = null;
        this.endTimeLimit = null;
    }

}
#endregion

#region RollupRulesDataModel
public class RollupRulesDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public double?       objectiveMeasureWeight;
    public bool?         rollupObjectiveSatisfied;
    public bool?         rollupProgressCompletion;
    public ArrayList     rollupRules;

    // Default Constructor
    public RollupRulesDataModel()
    {
        this.objectiveMeasureWeight     = null; //1.0000;
        this.rollupObjectiveSatisfied   = null; //true;
        this.rollupProgressCompletion   = null; //true;
        this.rollupRules                = new ArrayList();
    }
}
#endregion

#region RollupRuleDataModel
public class RollupRuleDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public string        childActivitySet;
    public string        conditionCombination;
    public int?          minimumCount;
    public double?       minimumPercent;
    public string        rollupAction;
    public ArrayList     rollupConditions;

    // Default Constructor
    public RollupRuleDataModel()
    {
        this.childActivitySet       = null; //"all";
        this.conditionCombination   = null; //"any";
        this.minimumCount           = null; //0;
        this.minimumPercent         = null; //0.0000;
        this.rollupAction           = null;
        this.rollupConditions       = new ArrayList();
    }
}
#endregion

#region RollupConditionDataModel
public class RollupConditionDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public string   condition;
    public string   conditionOperator;

    // Default Constructor
    public RollupConditionDataModel()
    {
        this.condition          = null;
        this.conditionOperator  = null; //"noOp";
    }
}
#endregion

#region IMSSSObjectivesDataModel
public class IMSSSObjectivesDataModel
{
    public ArrayList                objectives;
    public IMSSSObjectiveDataModel  primaryObjective;
    
    public IMSSSObjectivesDataModel()
    {
        this.objectives         = new ArrayList();
        this.primaryObjective   = null; //new IMSSSObjectiveDataModel();
    }
}
#endregion

#region IMSSSObjectiveDataModel
public class IMSSSObjectiveDataModel
{
    public ArrayList     mapInfo;
    public double?       minNormalizedMeasure;
    public string        objectiveId;
    public bool?         satisfiedByMeasure;
    //added by chetu
    public bool?         objectiveContributesToRollup;

    public IMSSSObjectiveDataModel()
    {
        this.mapInfo                 = new ArrayList();
        this.minNormalizedMeasure    = null; //1.0000;
        this.objectiveId             = null;
        this.satisfiedByMeasure      = null; //false;
        //added by chetu
        this.objectiveContributesToRollup = null;
    }
}
#endregion

#region IMSSSMapInfoDataModel
public class IMSSSMapInfoDataModel
{
    public bool?     readNormalizedMeasure;
    public bool?     readSatisfiedStatus;
    public string    targetObjectiveId;
    public bool?     writeNormalizedMeasure;
    public bool?     writeSatisfiedStatus;
    
    public IMSSSMapInfoDataModel()
    {
        this.readNormalizedMeasure      = null; //true;
        this.readSatisfiedStatus        = null; //true;
        this.targetObjectiveId          = null;
        this.writeNormalizedMeasure     = null; //false;
        this.writeSatisfiedStatus       = null; //false;
    }
}
#endregion

#region RandomizationControlsDataModel
public class RandomizationControlsDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public string    randomizationTiming;
    public bool?     reorderChildren;
    public int?      selectCount;
    public string    selectionTiming;

    // Default Constructor
    public RandomizationControlsDataModel()
    {
        this.randomizationTiming    = null; //"never";
        this.reorderChildren        = null; //false;
        this.selectCount            = null; //0;
        this.selectionTiming        = null; //"never";
    }
}
#endregion

#region DeliveryControlsDataModel
public class DeliveryControlsDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public bool?     completionSetByContent;
    public bool?     objectiveSetByContent;
    public bool?     tracked;

    // Default Constructor
    public DeliveryControlsDataModel()
    {
        this.completionSetByContent     = null; //false;
        this.objectiveSetByContent      = null; //false;
        this.tracked                    = null; //true;
    }
}
#endregion

#region ConstrainedChoiceConsiderationsDataModel
public class ConstrainedChoiceConsiderationsDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public bool?     constrainChoice;
    public bool?     preventActivation;

    // Default Constructor
    public ConstrainedChoiceConsiderationsDataModel()
    {
        this.constrainChoice    = null; //false;
        this.preventActivation  = null; //false;
    }
}
#endregion

#region RollupConsiderationsDataModel
public class RollupConsiderationsDataModel
{
    // Properties (in alphabetical order because JSON serialization will do that anyway)
    public bool?     measureSatisfactionIfActive;
    public string    requiredForCompleted;
    public string    requiredForIncomplete;
    public string    requiredForNotSatisfied;
    public string    requiredForSatisfied;

    // Default Constructor
    public RollupConsiderationsDataModel()
    {
        this.measureSatisfactionIfActive    = null; //true;
        this.requiredForCompleted           = null; //"always";
        this.requiredForIncomplete          = null; //"always";
        this.requiredForNotSatisfied        = null; //"always";
        this.requiredForSatisfied           = null; //"always";
    }
}
#endregion

#region ADLSEQObjectivesDataModel
public class ADLSEQObjectivesDataModel
{
    public ArrayList    objectives;

    public ADLSEQObjectivesDataModel()
    {
        this.objectives     = new ArrayList();
    }
}
#endregion

#region ADLSEQObjectiveDataModel
public class ADLSEQObjectiveDataModel
{
    public ArrayList    mapInfo;
    public string       objectiveId;        

    public ADLSEQObjectiveDataModel()
    {
        this.mapInfo        = new ArrayList();
        this.objectiveId    = null;
    }
}
#endregion

#region ADLSEQMapInfoDataModel
public class ADLSEQMapInfoDataModel
{
    public bool?     readCompletionStatus;
    public bool?     readMaxScore;
    public bool?     readMinScore;
    public bool?     readProgressMeasure;
    public bool?     readRawScore;
    public string    targetObjectiveId;
    public bool?     writeCompletionStatus;
    public bool?     writeMaxScore;
    public bool?     writeMinScore;
    public bool?     writeProgressMeasure;
    public bool?     writeRawScore;

    public ADLSEQMapInfoDataModel()
    {
        this.readCompletionStatus   = null; //true;
        this.readMaxScore           = null; //true;
        this.readMinScore           = null; //true;
        this.readProgressMeasure    = null; //true;
        this.readRawScore           = null; //true;
        this.targetObjectiveId      = null;
        this.writeCompletionStatus  = null; //false;
        this.writeMaxScore          = null; //false;
        this.writeMinScore          = null; //false;
        this.writeProgressMeasure   = null; //false;
        this.writeRawScore          = null; //false;
    }
}
#endregion

#region PresentationDataModel
public class PresentationDataModel
{
    public ArrayList    hideLMSUI;          // <hideLMSUI> element values - child of <navigationInterface>

    public PresentationDataModel()
    {
        this.hideLMSUI  = new ArrayList();  // valid values: "previous", "continue", "exit", "exitAll", 
                                            // "abandon", "abandonAll", "suspendAll"
    }
}
#endregion