﻿using System;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;

namespace Asentia.LMS.Pages._Util.EmailOptOut
{
    public class Default : AsentiaPage
    {
        #region Private Properties
        private string _Token;
        private ModalPopup _EmailOptOutConfirmModal;
        private Button _EmailOptOutConfirmHiddenButton;
        #endregion

        #region Page_Load
        public void Page_Load()
        {
            // get the token querystring parameter
            this._Token = this.QueryStringString("token");

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/_util/EmailOptOut/Default.css");

            // build up the confirm modal popup
            this._EmailOptOutConfirmModal = new ModalPopup("EmailOptOutConfirmAction");
            this._EmailOptOutConfirmHiddenButton = new Button();
            this._EmailOptOutConfirmHiddenButton.ID = "EmailOptOutConfirmHiddenButton";
            this._EmailOptOutConfirmHiddenButton.Style.Add("display", "none");

            // add button to container
            this.PageContentContainer.Controls.Add(this._EmailOptOutConfirmHiddenButton);

            // set modal properties
            this._EmailOptOutConfirmModal.Type = ModalPopupType.Confirm;
            this._EmailOptOutConfirmModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG);
            this._EmailOptOutConfirmModal.HeaderIconAlt = _GlobalResources.EmailNotifications;
            this._EmailOptOutConfirmModal.HeaderText = _GlobalResources.EmailNotifications;
            this._EmailOptOutConfirmModal.TargetControlID = this._EmailOptOutConfirmHiddenButton.ClientID;
            this._EmailOptOutConfirmModal.SubmitButton.Command += new CommandEventHandler(this._ConfirmButton_Command);
            this._EmailOptOutConfirmModal.ShowModal();

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            body1Wrapper.ID = "GridConfirmActionModalBody1";

            Literal body1 = new Literal();
            body1.Text = _GlobalResources.AreYouSureYouWantToOptOutOfEmailNotifications;
            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._EmailOptOutConfirmModal.AddControlToBody(body1Wrapper);

            // add modal to container
            this.PageContentContainer.Controls.Add(this._EmailOptOutConfirmModal);
        }

        #endregion

        #region _ConfirmButton_Command
        /// <summary>
        /// Performs the confirm action.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _ConfirmButton_Command(object sender, CommandEventArgs e)
        {
            int idUser = 0;
            try
            {
                // redirect if the token is invalid
                if (String.IsNullOrWhiteSpace(this._Token))
                { Response.Redirect("/"); }

                AsentiaAESEncryption cryptoURL = new AsentiaAESEncryption();
                string decryptedURL = WebUtility.UrlDecode(cryptoURL.Decrypt(this._Token));

                idUser = Convert.ToInt32(decryptedURL.Substring(0, decryptedURL.IndexOf("|")));

                if (idUser > 0)
                {
                    // close confirm modal popup
                    this._EmailOptOutConfirmModal.HideModal();

                    Asentia.UMS.Library.User.OptOutEmailNotifications(idUser);
                    // display the saved feedback
                    this.DisplayFeedback(_GlobalResources.YouHaveBeenOptedOutOfEmailNotificationsYouWillNoLongerReceiveEmailsFromThisSystem, false);
                }
                else
                {
                    this.DisplayFeedback(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, true);
                }
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, true);
            }

        }
        #endregion
    }
}
