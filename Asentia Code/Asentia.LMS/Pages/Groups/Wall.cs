﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.Groups
{
    public class Wall : AsentiaAuthenticatedPage
    {
        #region Public Properties
        public Panel WallFormContentWrapperContainer;
        public Panel WallWrapperContainer;
        public Panel WallPropertiesFeedbackContainer;
        public Panel WallPropertiesContainer;
        public Panel WallPropertiesTabPanelsContainer;
        #endregion

        #region Private Properties
        private Group _GroupObject;

        // wall controls
        private const int _WallMessagesPageSize = 10;

        private Panel _WallMessagesTabPanel;
        private Panel _WallMessagesContainer;
        private Button _HiddenButtonForPostedForModerationModal;
        private ModalPopup _MessagePostedForModerationModal;
        private Button _HiddenButtonForMessageDeletedModal;
        private ModalPopup _MessageDeletedModal;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Wall), "Asentia.LMS.Pages.Groups.Wall.js");

            // build start up and global js           
            StringBuilder globalJs = new StringBuilder();

            globalJs.AppendLine("var LastRecord = \"" + AsentiaSessionState.UtcNow + "\";");
            globalJs.AppendLine("var GroupId = " + this._GroupObject.Id + ";");
            globalJs.AppendLine("var IsLoading = false;");
            globalJs.AppendLine("GetRecords();");
            globalJs.AppendLine("$(window).scroll(function () {");
            globalJs.AppendLine("    if ($(window).scrollTop() == $(document).height() - $(window).height()) {");
            globalJs.AppendLine("       if (!IsLoading) {");
            globalJs.AppendLine("           GetRecords();");
            globalJs.AppendLine("       }");
            globalJs.AppendLine("    }");
            globalJs.AppendLine("});");

            csm.RegisterStartupScript(typeof(Wall), "GlobalJs", globalJs.ToString(), true);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("Wall.css");
            this.IncludePageSpecificCssFile("page-specific/groups/Wall.css");

            // get the group object
            this._GetGroupObject();

            // if group discussion and group documents are disabled, bounce
            if (!(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE) &&
                !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
            {
                Response.Redirect("~/dashboard");
            }

            // if the user is not admin, validate that they are a member of the group, if not, bounce
            if (AsentiaSessionState.IdSiteUser > 1)
            {
                if (!Asentia.UMS.Library.User.IsMemberOfGroup(this._GroupObject.Id))
                { Response.Redirect("~/dashboard"); }
            }

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.WallFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.WallWrapperContainer.CssClass = "FormContentContainer";

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetGroupObject
        /// <summary>
        /// Gets a group object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetGroupObject()
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { this._GroupObject = new Group(id); }
                }
                catch
                { Response.Redirect("~/dashboard"); }
            }
            else { Response.Redirect("~/dashboard"); }
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // clear controls from wrapper container
            this.WallPropertiesContainer.Controls.Clear();

            // build the group properties form tabs
            this._BuildWallPropertiesFormTabs();

            this.WallPropertiesTabPanelsContainer = new Panel();
            this.WallPropertiesTabPanelsContainer.ID = "WallProperties_TabPanelsContainer";
            this.WallPropertiesTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.WallPropertiesContainer.Controls.Add(this.WallPropertiesTabPanelsContainer);

            // build the wall messages panel, only if the group has a wall
            if ((bool)this._GroupObject.IsFeedActive && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
            {
                this._BuildMessageFeedPanel();
            }

            // build the documents panel
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
            {
                this._BuildDocumentsPanel();
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        private void _BuildBreadcrumbAndPageTitle()
        {
            // get group name information
            string groupNameInInterfaceLanguage = this._GroupObject.Name;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Group.LanguageSpecificProperty groupLanguageSpecificProperty in this._GroupObject.LanguageSpecificProperties)
                {
                    if (groupLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { groupNameInInterfaceLanguage = groupLanguageSpecificProperty.Name; }
                }
            }

            // evaluate for breadcrumb and page title information
            string groupWallImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION,
                                                               ImageFiles.EXT_PNG);

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(groupNameInInterfaceLanguage + ": " + _GlobalResources.Discussion));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.UsersAndGroups, groupNameInInterfaceLanguage + ": " + _GlobalResources.Discussion, groupWallImagePath);
        }
        #endregion

        #region _BuildWallPropertiesFormTabs
        /// <summary>
        /// Method to build the wall properties form tabs
        /// </summary>
        private void _BuildWallPropertiesFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            if ((bool)this._GroupObject.IsFeedActive && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
            {
                tabs.Enqueue(new KeyValuePair<string, string>("MessageFeed", _GlobalResources.MessageFeed));
            }

            if (this._GroupObject != null && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DOCUMENTS_ENABLE))
            { tabs.Enqueue(new KeyValuePair<string, string>("Documents", _GlobalResources.Documents)); }

            // build and attach the tabs
            this.WallPropertiesContainer.Controls.Add(AsentiaPage.BuildTabListPanel("WallProperties", tabs));
        }
        #endregion

        #region _BuildMessageFeedPanel
        /// <summary>
        /// Builds the message feed panel.
        /// </summary>
        private void _BuildMessageFeedPanel()
        {
            // "Message Feed" is the default tab, so this is visible on page load.
            this._WallMessagesTabPanel = new Panel();
            this._WallMessagesTabPanel.ID = "WallProperties_" + "MessageFeed" + "_TabPanel";
            this._WallMessagesTabPanel.Attributes.Add("style", "display: block;");

            // make an unclickable button that will be the default button for the wall messages panel
            // so that we can control postbacks when enter is pressed in the comment fields
            Button wallMessagesContainerDefaultButton = new Button();
            wallMessagesContainerDefaultButton.ID = "WallMessagesContainerDefaultButton";
            wallMessagesContainerDefaultButton.OnClientClick = "return false;";
            wallMessagesContainerDefaultButton.Style.Add("display", "none");
            this._WallMessagesTabPanel.Controls.Add(wallMessagesContainerDefaultButton);

            // NEW MESSAGE

            Panel newMessageContainer = new Panel();
            newMessageContainer.ID = "NewMessageContainer";
            newMessageContainer.CssClass = "NewMessageContainer";
            newMessageContainer.DefaultButton = wallMessagesContainerDefaultButton.ID;

            // avatar
            Panel newMessageSenderAvatarContainer = new Panel();
            newMessageSenderAvatarContainer.ID = "NewMessageSenderAvatarContainer";
            newMessageSenderAvatarContainer.CssClass = "NewMessageAvatar";

            Image newMessageSenderAvatar = new Image();
            newMessageSenderAvatar.ID = "NewMessageSenderAvatar";
            newMessageSenderAvatar.CssClass = "MediumIcon";
            newMessageSenderAvatar.AlternateText = String.Empty;
            newMessageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);

            newMessageSenderAvatarContainer.Controls.Add(newMessageSenderAvatar);

            // attach avatar to new message container
            newMessageContainer.Controls.Add(newMessageSenderAvatarContainer);

            // message field
            TextBox newMessageField = new TextBox();
            newMessageField.ID = "NewMessageField";
            newMessageField.CssClass = "NewMessageField";
            newMessageField.Attributes.Add("placeholder", _GlobalResources.PostMessage);
            newMessageField.Attributes.Add("onkeyup", "PostNewMessage(event);");

            // attach field to reply comment container
            newMessageContainer.Controls.Add(newMessageField);

            // post message button
            Panel postMessageButtonContainer = new Panel();
            postMessageButtonContainer.ID = "PostMessageButtonContainer";
            postMessageButtonContainer.CssClass = "PostMessageButtonContainer";

            Button postMessageButton = new Button();
            postMessageButton.ID = "PostMessageButton";
            postMessageButton.CssClass = "Button ActionButton";
            postMessageButton.Text = _GlobalResources.PostMessage;
            postMessageButton.OnClientClick = "PostNewMessage(event); return false;";

            postMessageButtonContainer.Controls.Add(postMessageButton);

            // attach post message button to new messgae container
            newMessageContainer.Controls.Add(postMessageButtonContainer);

            // attach new message container to panel
            this._WallMessagesTabPanel.Controls.Add(newMessageContainer);

            // MESSAGES

            this._WallMessagesContainer = new Panel();
            this._WallMessagesContainer.ID = "WallMessagesContainer";
            this._WallMessagesContainer.DefaultButton = wallMessagesContainerDefaultButton.ID;
            this._WallMessagesTabPanel.Controls.Add(this._WallMessagesContainer);

            // LOADING PANEL - OLDER MESSAGES

            Panel olderMessagesLoadingPanel = new Panel();
            olderMessagesLoadingPanel.ID = "OlderMessagesLoadingPanel";
            olderMessagesLoadingPanel.CssClass = "WallLoadingPanel";
            olderMessagesLoadingPanel.Style.Add("display", "none");

            Image olderMessagesLoadingImage = new Image();
            olderMessagesLoadingImage.ID = "OlderMessagesLoadingImage";
            olderMessagesLoadingImage.CssClass = "MediumIcon";
            olderMessagesLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING, ImageFiles.EXT_GIF);
            olderMessagesLoadingImage.AlternateText = _GlobalResources.Loading;

            Label olderMessagesLoadingText = new Label();
            olderMessagesLoadingText.Text = _GlobalResources.LoadingMessages;

            olderMessagesLoadingPanel.Controls.Add(olderMessagesLoadingImage);
            olderMessagesLoadingPanel.Controls.Add(olderMessagesLoadingText);

            this._WallMessagesTabPanel.Controls.Add(olderMessagesLoadingPanel);

            // MODALS - FOR CONFIRMATION OF MESSAGE POSTING, APPROVAL, AND DELETION

            // message posted for moderation
            this._HiddenButtonForPostedForModerationModal = new Button();
            this._HiddenButtonForPostedForModerationModal.ID = "HiddenButtonForPostedForModerationModal";
            this._HiddenButtonForPostedForModerationModal.Style.Add("display", "none");
            this._WallMessagesTabPanel.Controls.Add(this._HiddenButtonForPostedForModerationModal);

            this._BuildMessagePostedForModerationModal();

            // message deleted
            this._HiddenButtonForMessageDeletedModal = new Button();
            this._HiddenButtonForMessageDeletedModal.ID = "HiddenButtonForMessageDeletedModal";
            this._HiddenButtonForMessageDeletedModal.Style.Add("display", "none");
            this._WallMessagesTabPanel.Controls.Add(this._HiddenButtonForMessageDeletedModal);

            this._BuildMessageDeletedModal();

            // attach update panel to container
            this.WallPropertiesTabPanelsContainer.Controls.Add(this._WallMessagesTabPanel);
        }
        #endregion

        #region _BuildMessagePostedForModerationModal
        private void _BuildMessagePostedForModerationModal()
        {
            // set modal properties
            this._MessagePostedForModerationModal = new ModalPopup("MessagePostedForModerationModal");
            this._MessagePostedForModerationModal.Type = ModalPopupType.Information;
            this._MessagePostedForModerationModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
            this._MessagePostedForModerationModal.HeaderIconAlt = _GlobalResources.Discussion;
            this._MessagePostedForModerationModal.HeaderText = _GlobalResources.MessagePosted;
            this._MessagePostedForModerationModal.TargetControlID = this._HiddenButtonForPostedForModerationModal.ID;

            // build the modal body
            this._MessagePostedForModerationModal.DisplayFeedback(_GlobalResources.YourMessageHasBeenPostedForModerationOnceApprovedByAModeratorYourMessageWillBeDisplayedInTheDiscussion, false);

            // add modal to container
            this._WallMessagesTabPanel.Controls.Add(this._MessagePostedForModerationModal);
        }
        #endregion

        #region _BuildMessageDeletedModal
        private void _BuildMessageDeletedModal()
        {
            // set modal properties
            this._MessageDeletedModal = new ModalPopup("MessageDeletedModal");
            this._MessageDeletedModal.Type = ModalPopupType.Information;
            this._MessageDeletedModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION, ImageFiles.EXT_PNG);
            this._MessageDeletedModal.HeaderIconAlt = _GlobalResources.Discussion;
            this._MessageDeletedModal.HeaderText = _GlobalResources.MessageDeleted;
            this._MessageDeletedModal.TargetControlID = this._HiddenButtonForMessageDeletedModal.ID;

            // build the modal body
            this._MessageDeletedModal.DisplayFeedback(_GlobalResources.TheMessageHasBeenDeletedSuccessfully, false);

            // add modal to container
            this._WallMessagesTabPanel.Controls.Add(this._MessageDeletedModal);
        }
        #endregion

        #region _WallMessagesJsonDataStruct
        public struct _WallMessagesJsonData
        {
            public bool actionSuccessful;
            public string html;
            public string exception;
            public string lastRecord;
            public int? idMessage;
            public int? idParentMessage;
        }
        #endregion

        #region BuildFeedMessages
        [WebMethod(EnableSession = true)]
        public static _WallMessagesJsonData BuildFeedMessages(int idGroup, DateTime dtQuery, bool getMessagesNewerThanDtQuery)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            try
            {
                DateTime lastRecord;
                DataTable messages = UMS.Library.Group.GetFeedMessages(idGroup, _WallMessagesPageSize, dtQuery, getMessagesNewerThanDtQuery, true, out lastRecord);

                foreach (DataRow row in messages.Rows)
                {
                    int idGroupFeedMessage = Convert.ToInt32(row["idGroupFeedMessage"]);
                    bool isMessageApproved = Convert.ToBoolean(row["isApproved"]);
                    int idMessageAuthor = Convert.ToInt32(row["idAuthor"]);

                    Panel messageContainer = new Panel();
                    messageContainer.ID = "MessageContainer_" + idGroupFeedMessage.ToString();
                    messageContainer.CssClass = "MessageContainer";

                    // MESSAGE HEADER

                    Panel messageHeaderContainer = new Panel();
                    messageHeaderContainer.ID = "MessageHeaderContainer_" + idGroupFeedMessage.ToString();
                    messageHeaderContainer.CssClass = "MessageHeaderContainer";

                    // avatar
                    Panel messageSenderAvatarContainer = new Panel();
                    messageSenderAvatarContainer.ID = "MessageSenderAvatarContainer_" + idGroupFeedMessage.ToString();
                    messageSenderAvatarContainer.CssClass = "MessageAvatar";

                    Image messageSenderAvatar = new Image();
                    messageSenderAvatar.ID = "MessageSenderAvatar_" + idGroupFeedMessage.ToString();
                    messageSenderAvatar.CssClass = "MediumIcon";
                    messageSenderAvatar.AlternateText = row["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                    if (!String.IsNullOrWhiteSpace(row["avatar"].ToString()))
                    { messageSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(row["idAuthor"]) + "/" + row["avatar"].ToString(); }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(row["gender"].ToString()) && row["gender"].ToString() == "f")
                        { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                        else
                        { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                    }

                    messageSenderAvatarContainer.Controls.Add(messageSenderAvatar);

                    // attach avatar to header container
                    messageHeaderContainer.Controls.Add(messageSenderAvatarContainer);

                    // sender information
                    Panel messageSenderInformationContainer = new Panel();
                    messageSenderInformationContainer.ID = "MessageSenderInformationContainer_" + idGroupFeedMessage.ToString();
                    messageSenderInformationContainer.CssClass = "MessageSenderInformationContainer";

                    Panel messageSenderNameContainer = new Panel();
                    messageSenderNameContainer.ID = "MessageSenderNameContainer_" + idGroupFeedMessage.ToString();
                    messageSenderNameContainer.CssClass = "MessageSenderNameContainer";

                    Literal messageSenderName = new Literal();
                    messageSenderName.Text = row["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                    messageSenderNameContainer.Controls.Add(messageSenderName);
                    messageSenderInformationContainer.Controls.Add(messageSenderNameContainer);

                    Panel messageSentDateTimeContainer = new Panel();
                    messageSentDateTimeContainer.ID = "MessageSentDateTimeContainer_" + idGroupFeedMessage.ToString();
                    messageSentDateTimeContainer.CssClass = "MessageSentDateTimeContainer";

                    DateTime messageTimestamp = Convert.ToDateTime(row["timestamp"]);
                    messageTimestamp = TimeZoneInfo.ConvertTimeFromUtc(messageTimestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                    Literal messageSentDateTime = new Literal();
                    messageSentDateTime.Text = messageTimestamp.ToString();


                    messageSentDateTimeContainer.Controls.Add(messageSentDateTime);
                    messageSenderInformationContainer.Controls.Add(messageSentDateTimeContainer);

                    // attach sender information to header container
                    messageHeaderContainer.Controls.Add(messageSenderInformationContainer);

                    // approve and delete/dis-approve buttons
                    Panel messageActionButtonsContainer = new Panel();
                    messageActionButtonsContainer.ID = "MessageActionButtonsContainer_" + idGroupFeedMessage.ToString();
                    messageActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                    // NO APPROVE BUTTON HERE

                    // delete button - only the message's author can delete from here
                    if (idMessageAuthor == AsentiaSessionState.IdSiteUser)
                    {
                        Panel messageDeleteButtonContainer = new Panel();
                        messageDeleteButtonContainer.ID = "MessageDeleteButtonContainer_" + idGroupFeedMessage.ToString();

                        Image messageDeleteButton = new Image();
                        messageDeleteButton.ID = "MessageDeleteButton_" + idGroupFeedMessage.ToString();
                        messageDeleteButton.CssClass = "XSmallIcon";
                        messageDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        messageDeleteButton.AlternateText = _GlobalResources.Delete;
                        messageDeleteButton.Style.Add("cursor", "pointer");
                        messageDeleteButton.Attributes.Add("onclick", "DeleteMessage(\"" + idGroupFeedMessage.ToString() + "\");");

                        messageDeleteButtonContainer.Controls.Add(messageDeleteButton);
                        messageActionButtonsContainer.Controls.Add(messageDeleteButtonContainer);
                    }

                    // attach buttons container to header container
                    messageHeaderContainer.Controls.Add(messageActionButtonsContainer);

                    // attach header to message container
                    messageContainer.Controls.Add(messageHeaderContainer);

                    // MESSAGE DATA

                    Panel messageDataContainer = new Panel();
                    messageDataContainer.ID = "MessageDataContainer_" + idGroupFeedMessage.ToString();
                    messageDataContainer.CssClass = "MessageDataContainer";

                    Literal messageData = new Literal();
                    messageData.Text = row["message"].ToString();

                    messageDataContainer.Controls.Add(messageData);
                    messageContainer.Controls.Add(messageDataContainer);

                    // COMMENTS

                    Panel messageCommentsContainer = new Panel();
                    messageCommentsContainer.ID = "MessageCommentsContainer_" + idGroupFeedMessage.ToString();

                    DataTable messageComments = LMS.Library.GroupFeedMessage.GetComments(idGroupFeedMessage, true);

                    foreach (DataRow commentRow in messageComments.Rows)
                    {
                        int idGroupFeedMessageComment = Convert.ToInt32(commentRow["idGroupFeedMessage"]);
                        bool isCommentApproved = Convert.ToBoolean(commentRow["isApproved"]);
                        int idCommentAuthor = Convert.ToInt32(commentRow["idAuthor"]);

                        Panel messageCommentContainer = new Panel();
                        messageCommentContainer.ID = "MessageCommentContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentContainer.CssClass = "MessageCommentContainer";

                        // avatar
                        Panel messageCommentSenderAvatarContainer = new Panel();
                        messageCommentSenderAvatarContainer.ID = "MessageCommentSenderAvatarContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentSenderAvatarContainer.CssClass = "MessageCommentAvatar";

                        Image messageCommentSenderAvatar = new Image();
                        messageCommentSenderAvatar.ID = "MessageCommentSenderAvatar_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentSenderAvatar.CssClass = "SmallIcon";
                        messageCommentSenderAvatar.AlternateText = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                        if (!String.IsNullOrWhiteSpace(commentRow["avatar"].ToString()))
                        { messageCommentSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(commentRow["idAuthor"]) + "/" + commentRow["avatar"].ToString(); }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(commentRow["gender"].ToString()) && commentRow["gender"].ToString() == "f")
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                            else
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                        }

                        messageCommentSenderAvatarContainer.Controls.Add(messageCommentSenderAvatar);

                        // attach avatar to container
                        messageCommentContainer.Controls.Add(messageCommentSenderAvatarContainer);

                        // comment data
                        Panel messageCommentDataContainer = new Panel();
                        messageCommentDataContainer.ID = "MessageCommentDataContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentDataContainer.CssClass = "MessageCommentDataContainer";

                        Panel messageCommentData = new Panel();
                        messageCommentData.ID = "MessageCommentData_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentData.CssClass = "MessageCommentData";

                        Label messageCommentSenderName = new Label();
                        messageCommentSenderName.CssClass = "MessageCommentSenderName";
                        messageCommentSenderName.Text = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);
                        messageCommentData.Controls.Add(messageCommentSenderName);

                        Label messageCommentMessage = new Label();
                        messageCommentMessage.Text = HttpContext.Current.Server.HtmlEncode(commentRow["message"].ToString());
                        messageCommentData.Controls.Add(messageCommentMessage);

                        // approve and delete/dis-approve buttons
                        Panel messageCommentActionButtonsContainer = new Panel();
                        messageCommentActionButtonsContainer.ID = "MessageCommentActionButtonsContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                        // NO APPROVE BUTTON HERE

                        // delete button
                        if (idCommentAuthor == AsentiaSessionState.IdSiteUser)
                        {
                            Panel messageCommentDeleteButtonContainer = new Panel();
                            messageCommentDeleteButtonContainer.ID = "MessageCommentDeleteButtonContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();

                            Image messageCommentDeleteButton = new Image();
                            messageCommentDeleteButton.ID = "MessageCommentDeleteButton_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentDeleteButton.CssClass = "XSmallIcon";
                            messageCommentDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                            messageCommentDeleteButton.AlternateText = _GlobalResources.Delete;
                            messageCommentDeleteButton.Style.Add("cursor", "pointer");
                            messageCommentDeleteButton.Attributes.Add("onclick", "DeleteComment(\"" + idGroupFeedMessageComment.ToString() + "\", \"" + idGroupFeedMessage.ToString() + "\");");

                            messageCommentDeleteButtonContainer.Controls.Add(messageCommentDeleteButton);
                            messageCommentActionButtonsContainer.Controls.Add(messageCommentDeleteButtonContainer);
                        }

                        // attach buttons container to header container
                        messageCommentData.Controls.Add(messageCommentActionButtonsContainer);

                        messageCommentDataContainer.Controls.Add(messageCommentData);

                        // timestamp
                        Panel messageCommentTimestampContainer = new Panel();
                        messageCommentTimestampContainer.ID = "MessageCommentTimestampContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                        messageCommentTimestampContainer.CssClass = "MessageCommentTimestamp";

                        DateTime messageCommentTimestampDt = Convert.ToDateTime(commentRow["timestamp"]);
                        messageCommentTimestampDt = TimeZoneInfo.ConvertTimeFromUtc(messageCommentTimestampDt, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        Literal messageCommentTimestamp = new Literal();
                        messageCommentTimestamp.Text = messageCommentTimestampDt.ToString();
                        messageCommentTimestampContainer.Controls.Add(messageCommentTimestamp);

                        messageCommentDataContainer.Controls.Add(messageCommentTimestampContainer);

                        // attach comment data to comment container
                        messageCommentContainer.Controls.Add(messageCommentDataContainer);

                        // attach comment container to comments container
                        messageCommentsContainer.Controls.Add(messageCommentContainer);
                    }

                    // attach comments container to message container
                    messageContainer.Controls.Add(messageCommentsContainer);

                    // REPLY WITH COMMENT

                    Panel messageReplyCommentContainer = new Panel();
                    messageReplyCommentContainer.ID = "MessageReplyCommentContainer_" + idGroupFeedMessage.ToString();
                    messageReplyCommentContainer.CssClass = "MessageReplyCommentContainer";

                    // avatar
                    Panel messageReplyCommentSenderAvatarContainer = new Panel();
                    messageReplyCommentSenderAvatarContainer.ID = "MessageReplyCommentSenderAvatarContainer_" + idGroupFeedMessage.ToString();
                    messageReplyCommentSenderAvatarContainer.CssClass = "MessageReplyCommentAvatar";

                    Image messageReplyCommentSenderAvatar = new Image();
                    messageReplyCommentSenderAvatar.ID = "MessageReplyCommentSenderAvatar_" + idGroupFeedMessage.ToString();
                    messageReplyCommentSenderAvatar.CssClass = "SmallIcon";
                    messageReplyCommentSenderAvatar.AlternateText = String.Empty;
                    messageReplyCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);

                    messageReplyCommentSenderAvatarContainer.Controls.Add(messageReplyCommentSenderAvatar);

                    // attach avatar to reply comment container
                    messageReplyCommentContainer.Controls.Add(messageReplyCommentSenderAvatarContainer);

                    // comment field
                    TextBox messageCommentField = new TextBox();
                    messageCommentField.ID = "MessageCommentField_" + idGroupFeedMessage.ToString();
                    messageCommentField.CssClass = "MessageReplyCommentField";
                    messageCommentField.Attributes.Add("placeholder", _GlobalResources.PostComment);
                    messageCommentField.Attributes.Add("onkeyup", "PostComment(this, event);");
                    messageCommentField.AutoPostBack = false;

                    // attach field to reply comment container
                    messageReplyCommentContainer.Controls.Add(messageCommentField);

                    // attach reply comment container to message container
                    messageContainer.Controls.Add(messageReplyCommentContainer);

                    // write the data to a text writer for json output
                    TextWriter textWriter = new StringWriter();
                    HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                    messageContainer.RenderControl(htmlTextWriter);

                    jsonData.html += textWriter.ToString();
                }

                jsonData.actionSuccessful = true;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = lastRecord.ToString();
                jsonData.idMessage = null;
                jsonData.idParentMessage = null;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = dtQuery.ToString();
                jsonData.idMessage = null;
                jsonData.idParentMessage = null;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region SaveMessage
        [WebMethod(EnableSession = true)]
        public static _WallMessagesJsonData SaveMessage(int idGroup, string message, int? idParentMessage)
        {
            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            // messages from admin are never subject to moderation
            bool isModerated;

            if (AsentiaSessionState.IdSiteUser > 1)
            {
                // since this is a static method, we need to have a local group object
                Group groupObject = new Group(idGroup);
                isModerated = (bool)groupObject.IsFeedModerated;
                
                // fetch group wall moderator
                DataTable dtGroupModerator = groupObject.GetWallModerators(null);
                if (dtGroupModerator.Rows.Count > 0)
                {
                    foreach (DataRow drGroupModerator in dtGroupModerator.Rows)
                    {
                        // if logged in user is present in Moderator list then set isModerator to false i.e. 
                        // no need for any approval of post from admin
                        if ((Int32)drGroupModerator["idUser"] == AsentiaSessionState.IdSiteUser)
                        {
                            isModerated = false;
                        }
                    }
                }
            }
            else
            { isModerated = false; }

            try
            {
                // build and save the group feed message
                GroupFeedMessage groupFeedMessage = new GroupFeedMessage();
                groupFeedMessage.IdGroup = idGroup;
                groupFeedMessage.IdAuthor = AsentiaSessionState.IdSiteUser;

                if (idParentMessage != null)
                { groupFeedMessage.IdParentGroupFeedMessage = idParentMessage; }

                groupFeedMessage.Message = message;

                int idGroupFeedMessage = groupFeedMessage.Save(isModerated);

                if (!isModerated)
                {
                    // get author information or just hard code it if user is "administrator"
                    // note that the author is always the currently logged in user
                    string authorName = String.Empty;
                    string authorAvatar = String.Empty;
                    string authorGender = String.Empty;

                    if (groupFeedMessage.IdAuthor == 1)
                    { authorName = _GlobalResources.Administrator; }
                    else
                    {
                        authorName = AsentiaSessionState.UserFirstName + " " + AsentiaSessionState.UserLastName;
                        authorAvatar = AsentiaSessionState.UserAvatar;
                        authorGender = AsentiaSessionState.UserGender;
                    }

                    if (groupFeedMessage.IdParentGroupFeedMessage == null) // new message
                    {
                        Panel messageContainer = new Panel();
                        messageContainer.ID = "MessageContainer_" + idGroupFeedMessage.ToString();
                        messageContainer.CssClass = "MessageContainer";

                        // MESSAGE HEADER

                        Panel messageHeaderContainer = new Panel();
                        messageHeaderContainer.ID = "MessageHeaderContainer_" + idGroupFeedMessage.ToString();
                        messageHeaderContainer.CssClass = "MessageHeaderContainer";

                        // avatar
                        Panel messageSenderAvatarContainer = new Panel();
                        messageSenderAvatarContainer.ID = "MessageSenderAvatarContainer_" + idGroupFeedMessage.ToString();
                        messageSenderAvatarContainer.CssClass = "MessageAvatar";

                        Image messageSenderAvatar = new Image();
                        messageSenderAvatar.ID = "MessageSenderAvatar_" + idGroupFeedMessage.ToString();
                        messageSenderAvatar.CssClass = "MediumIcon";
                        messageSenderAvatar.AlternateText = authorName;

                        if (!String.IsNullOrWhiteSpace(authorAvatar))
                        { messageSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + groupFeedMessage.IdAuthor + "/" + authorAvatar; }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(authorGender) && authorGender == "f")
                            { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                            else
                            { messageSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                        }

                        messageSenderAvatarContainer.Controls.Add(messageSenderAvatar);

                        // attach avatar to header container
                        messageHeaderContainer.Controls.Add(messageSenderAvatarContainer);

                        // sender information
                        Panel messageSenderInformationContainer = new Panel();
                        messageSenderInformationContainer.ID = "MessageSenderInformationContainer_" + idGroupFeedMessage.ToString();
                        messageSenderInformationContainer.CssClass = "MessageSenderInformationContainer";

                        Panel messageSenderNameContainer = new Panel();
                        messageSenderNameContainer.ID = "MessageSenderNameContainer_" + idGroupFeedMessage.ToString();
                        messageSenderNameContainer.CssClass = "MessageSenderNameContainer";

                        Literal messageSenderName = new Literal();
                        messageSenderName.Text = authorName;

                        messageSenderNameContainer.Controls.Add(messageSenderName);
                        messageSenderInformationContainer.Controls.Add(messageSenderNameContainer);

                        Panel messageSentDateTimeContainer = new Panel();
                        messageSentDateTimeContainer.ID = "MessageSentDateTimeContainer_" + idGroupFeedMessage.ToString();
                        messageSentDateTimeContainer.CssClass = "MessageSentDateTimeContainer";

                        DateTime messageTimestamp = DateTime.UtcNow;
                        messageTimestamp = TimeZoneInfo.ConvertTimeFromUtc(messageTimestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        Literal messageSentDateTime = new Literal();
                        messageSentDateTime.Text = messageTimestamp.ToString();


                        messageSentDateTimeContainer.Controls.Add(messageSentDateTime);
                        messageSenderInformationContainer.Controls.Add(messageSentDateTimeContainer);

                        // attach sender information to header container
                        messageHeaderContainer.Controls.Add(messageSenderInformationContainer);

                        // approve and delete/dis-approve buttons
                        Panel messageActionButtonsContainer = new Panel();
                        messageActionButtonsContainer.ID = "MessageActionButtonsContainer_" + idGroupFeedMessage.ToString();
                        messageActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                        // NO APPROVE BUTTON HERE

                        // delete button - the author IS the current user, so no if statement needed
                        Panel messageDeleteButtonContainer = new Panel();
                        messageDeleteButtonContainer.ID = "MessageDeleteButtonContainer_" + idGroupFeedMessage.ToString();

                        Image messageDeleteButton = new Image();
                        messageDeleteButton.ID = "MessageDeleteButton_" + idGroupFeedMessage.ToString();
                        messageDeleteButton.CssClass = "XSmallIcon";
                        messageDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        messageDeleteButton.AlternateText = _GlobalResources.Delete;
                        messageDeleteButton.Style.Add("cursor", "pointer");
                        messageDeleteButton.Attributes.Add("onclick", "DeleteMessage(\"" + idGroupFeedMessage.ToString() + "\");");

                        messageDeleteButtonContainer.Controls.Add(messageDeleteButton);
                        messageActionButtonsContainer.Controls.Add(messageDeleteButtonContainer);

                        // attach buttons container to header container
                        messageHeaderContainer.Controls.Add(messageActionButtonsContainer);

                        // attach header to message container
                        messageContainer.Controls.Add(messageHeaderContainer);

                        // MESSAGE DATA

                        Panel messageDataContainer = new Panel();
                        messageDataContainer.ID = "MessageDataContainer_" + idGroupFeedMessage.ToString();
                        messageDataContainer.CssClass = "MessageDataContainer";

                        Literal messageData = new Literal();
                        messageData.Text = message;

                        messageDataContainer.Controls.Add(messageData);
                        messageContainer.Controls.Add(messageDataContainer);

                        // COMMENTS

                        Panel messageCommentsContainer = new Panel();
                        messageCommentsContainer.ID = "MessageCommentsContainer_" + idGroupFeedMessage.ToString();

                        DataTable messageComments = LMS.Library.GroupFeedMessage.GetComments(idGroupFeedMessage, true);

                        foreach (DataRow commentRow in messageComments.Rows)
                        {
                            int idGroupFeedMessageComment = Convert.ToInt32(commentRow["idGroupFeedMessage"]);
                            bool isCommentApproved = Convert.ToBoolean(commentRow["isApproved"]);
                            int idCommentAuthor = Convert.ToInt32(commentRow["idAuthor"]);

                            Panel messageCommentContainer = new Panel();
                            messageCommentContainer.ID = "MessageCommentContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentContainer.CssClass = "MessageCommentContainer";

                            // avatar
                            Panel messageCommentSenderAvatarContainer = new Panel();
                            messageCommentSenderAvatarContainer.ID = "MessageCommentSenderAvatarContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentSenderAvatarContainer.CssClass = "MessageCommentAvatar";

                            Image messageCommentSenderAvatar = new Image();
                            messageCommentSenderAvatar.ID = "MessageCommentSenderAvatar_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentSenderAvatar.CssClass = "SmallIcon";
                            messageCommentSenderAvatar.AlternateText = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);

                            if (!String.IsNullOrWhiteSpace(commentRow["avatar"].ToString()))
                            { messageCommentSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + Convert.ToInt32(commentRow["idAuthor"]) + "/" + commentRow["avatar"].ToString(); }
                            else
                            {
                                if (!String.IsNullOrWhiteSpace(commentRow["gender"].ToString()) && commentRow["gender"].ToString() == "f")
                                { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                                else
                                { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                            }

                            messageCommentSenderAvatarContainer.Controls.Add(messageCommentSenderAvatar);

                            // attach avatar to container
                            messageCommentContainer.Controls.Add(messageCommentSenderAvatarContainer);

                            // comment data
                            Panel messageCommentDataContainer = new Panel();
                            messageCommentDataContainer.ID = "MessageCommentDataContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentDataContainer.CssClass = "MessageCommentDataContainer";

                            Panel messageCommentData = new Panel();
                            messageCommentData.ID = "MessageCommentData_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentData.CssClass = "MessageCommentData";

                            Label messageCommentSenderName = new Label();
                            messageCommentSenderName.CssClass = "MessageCommentSenderName";
                            messageCommentSenderName.Text = commentRow["authorName"].ToString().Replace("##administrator##", _GlobalResources.Administrator);
                            messageCommentData.Controls.Add(messageCommentSenderName);

                            Label messageCommentMessage = new Label();
                            messageCommentMessage.Text = commentRow["message"].ToString();
                            messageCommentData.Controls.Add(messageCommentMessage);

                            // approve and delete/dis-approve buttons
                            Panel messageCommentActionButtonsContainer = new Panel();
                            messageCommentActionButtonsContainer.ID = "MessageCommentActionButtonsContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                            // NO APPROVE BUTTON HERE

                            // delete button
                            if (idCommentAuthor == AsentiaSessionState.IdSiteUser)
                            {
                                Panel messageCommentDeleteButtonContainer = new Panel();
                                messageCommentDeleteButtonContainer.ID = "MessageCommentDeleteButtonContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();

                                Image messageCommentDeleteButton = new Image();
                                messageCommentDeleteButton.ID = "MessageCommentDeleteButton_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                                messageCommentDeleteButton.CssClass = "XSmallIcon";
                                messageCommentDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                                messageCommentDeleteButton.AlternateText = _GlobalResources.Delete;
                                messageCommentDeleteButton.Style.Add("cursor", "pointer");
                                messageCommentDeleteButton.Attributes.Add("onclick", "DeleteComment(\"" + idGroupFeedMessageComment.ToString() + "\", \"" + idGroupFeedMessage.ToString() + "\");");

                                messageCommentDeleteButtonContainer.Controls.Add(messageCommentDeleteButton);
                                messageCommentActionButtonsContainer.Controls.Add(messageCommentDeleteButtonContainer);
                            }

                            // attach buttons container to header container
                            messageCommentData.Controls.Add(messageCommentActionButtonsContainer);

                            messageCommentDataContainer.Controls.Add(messageCommentData);

                            // timestamp
                            Panel messageCommentTimestampContainer = new Panel();
                            messageCommentTimestampContainer.ID = "MessageCommentTimestampContainer_" + idGroupFeedMessage.ToString() + "_" + idGroupFeedMessageComment.ToString();
                            messageCommentTimestampContainer.CssClass = "MessageCommentTimestamp";

                            DateTime messageCommentTimestampDt = Convert.ToDateTime(commentRow["timestamp"]);
                            messageCommentTimestampDt = TimeZoneInfo.ConvertTimeFromUtc(messageCommentTimestampDt, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                            Literal messageCommentTimestamp = new Literal();
                            messageCommentTimestamp.Text = messageCommentTimestampDt.ToString();
                            messageCommentTimestampContainer.Controls.Add(messageCommentTimestamp);

                            messageCommentDataContainer.Controls.Add(messageCommentTimestampContainer);

                            // attach comment data to comment container
                            messageCommentContainer.Controls.Add(messageCommentDataContainer);

                            // attach comment container to comments container
                            messageCommentsContainer.Controls.Add(messageCommentContainer);
                        }

                        // attach comments container to message container
                        messageContainer.Controls.Add(messageCommentsContainer);

                        // REPLY WITH COMMENT

                        Panel messageReplyCommentContainer = new Panel();
                        messageReplyCommentContainer.ID = "MessageReplyCommentContainer_" + idGroupFeedMessage.ToString();
                        messageReplyCommentContainer.CssClass = "MessageReplyCommentContainer";

                        // avatar
                        Panel messageReplyCommentSenderAvatarContainer = new Panel();
                        messageReplyCommentSenderAvatarContainer.ID = "MessageReplyCommentSenderAvatarContainer_" + idGroupFeedMessage.ToString();
                        messageReplyCommentSenderAvatarContainer.CssClass = "MessageReplyCommentAvatar";

                        Image messageReplyCommentSenderAvatar = new Image();
                        messageReplyCommentSenderAvatar.ID = "MessageReplyCommentSenderAvatar_" + idGroupFeedMessage.ToString();
                        messageReplyCommentSenderAvatar.CssClass = "SmallIcon";
                        messageReplyCommentSenderAvatar.AlternateText = String.Empty;
                        messageReplyCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG);

                        messageReplyCommentSenderAvatarContainer.Controls.Add(messageReplyCommentSenderAvatar);

                        // attach avatar to reply comment container
                        messageReplyCommentContainer.Controls.Add(messageReplyCommentSenderAvatarContainer);

                        // comment field
                        TextBox messageCommentField = new TextBox();
                        messageCommentField.ID = "MessageCommentField_" + idGroupFeedMessage.ToString();
                        messageCommentField.CssClass = "MessageReplyCommentField";
                        messageCommentField.Attributes.Add("placeholder", _GlobalResources.PostComment);
                        messageCommentField.Attributes.Add("onkeyup", "PostComment(this, event);");
                        messageCommentField.AutoPostBack = false;

                        // attach field to reply comment container
                        messageReplyCommentContainer.Controls.Add(messageCommentField);

                        // attach reply comment container to message container
                        messageContainer.Controls.Add(messageReplyCommentContainer);

                        // write the data to a text writer for json output
                        TextWriter textWriter = new StringWriter();
                        HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                        messageContainer.RenderControl(htmlTextWriter);

                        jsonData.html += textWriter.ToString();
                    }
                    else // just a comment
                    {
                        Panel messageCommentContainer = new Panel();
                        messageCommentContainer.ID = "MessageCommentContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentContainer.CssClass = "MessageCommentContainer";

                        // avatar
                        Panel messageCommentSenderAvatarContainer = new Panel();
                        messageCommentSenderAvatarContainer.ID = "MessageCommentSenderAvatarContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentSenderAvatarContainer.CssClass = "MessageCommentAvatar";

                        Image messageCommentSenderAvatar = new Image();
                        messageCommentSenderAvatar.ID = "MessageCommentSenderAvatar_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentSenderAvatar.CssClass = "SmallIcon";
                        messageCommentSenderAvatar.AlternateText = authorName;

                        if (!String.IsNullOrWhiteSpace(authorAvatar))
                        { messageCommentSenderAvatar.ImageUrl = SitePathConstants.SITE_USERS_ROOT + groupFeedMessage.IdAuthor + "/" + authorAvatar; }
                        else
                        {
                            if (!String.IsNullOrWhiteSpace(authorGender) && authorGender == "f")
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERF, ImageFiles.EXT_PNG); }
                            else
                            { messageCommentSenderAvatar.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG); }
                        }

                        messageCommentSenderAvatarContainer.Controls.Add(messageCommentSenderAvatar);

                        // attach avatar to container
                        messageCommentContainer.Controls.Add(messageCommentSenderAvatarContainer);

                        // comment data
                        Panel messageCommentDataContainer = new Panel();
                        messageCommentDataContainer.ID = "MessageCommentDataContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentDataContainer.CssClass = "MessageCommentDataContainer";

                        Panel messageCommentData = new Panel();
                        messageCommentData.ID = "MessageCommentData_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentData.CssClass = "MessageCommentData";

                        Label messageCommentSenderName = new Label();
                        messageCommentSenderName.CssClass = "MessageCommentSenderName";
                        messageCommentSenderName.Text = authorName;
                        messageCommentData.Controls.Add(messageCommentSenderName);

                        Label messageCommentMessage = new Label();
                        messageCommentMessage.Text = message;
                        messageCommentData.Controls.Add(messageCommentMessage);

                        // approve and delete/dis-approve buttons
                        Panel messageCommentActionButtonsContainer = new Panel();
                        messageCommentActionButtonsContainer.ID = "MessageCommentActionButtonsContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentActionButtonsContainer.CssClass = "MessageActionButtonsContainer";

                        // NO APPROVE BUTTON HERE

                        // delete button - the author IS the current user, so no if statement needed
                        Panel messageCommentDeleteButtonContainer = new Panel();
                        messageCommentDeleteButtonContainer.ID = "MessageCommentDeleteButtonContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();

                        Image messageCommentDeleteButton = new Image();
                        messageCommentDeleteButton.ID = "MessageCommentDeleteButton_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentDeleteButton.CssClass = "XSmallIcon";
                        messageCommentDeleteButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        messageCommentDeleteButton.AlternateText = _GlobalResources.Delete;
                        messageCommentDeleteButton.Style.Add("cursor", "pointer");
                        messageCommentDeleteButton.Attributes.Add("onclick", "DeleteComment(\"" + idGroupFeedMessage.ToString() + "\", \"" + idParentMessage.ToString() + "\");");

                        messageCommentDeleteButtonContainer.Controls.Add(messageCommentDeleteButton);
                        messageCommentActionButtonsContainer.Controls.Add(messageCommentDeleteButtonContainer);

                        // attach buttons container to header container
                        messageCommentData.Controls.Add(messageCommentActionButtonsContainer);

                        messageCommentDataContainer.Controls.Add(messageCommentData);

                        // timestamp
                        Panel messageCommentTimestampContainer = new Panel();
                        messageCommentTimestampContainer.ID = "MessageCommentTimestampContainer_" + idParentMessage.ToString() + "_" + idGroupFeedMessage.ToString();
                        messageCommentTimestampContainer.CssClass = "MessageCommentTimestamp";

                        DateTime messageCommentTimestampDt = DateTime.UtcNow;
                        messageCommentTimestampDt = TimeZoneInfo.ConvertTimeFromUtc(messageCommentTimestampDt, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        Literal messageCommentTimestamp = new Literal();
                        messageCommentTimestamp.Text = messageCommentTimestampDt.ToString();
                        messageCommentTimestampContainer.Controls.Add(messageCommentTimestamp);

                        messageCommentDataContainer.Controls.Add(messageCommentTimestampContainer);

                        // attach comment data to comment container
                        messageCommentContainer.Controls.Add(messageCommentDataContainer);

                        // write the data to a text writer for json output
                        TextWriter textWriter = new StringWriter();
                        HtmlTextWriter htmlTextWriter = new HtmlTextWriter(textWriter);
                        messageCommentContainer.RenderControl(htmlTextWriter);

                        jsonData.html += textWriter.ToString();
                    }
                }
                else // empty html json data, used to just show a "message saved for moderation" modal
                { jsonData.html = String.Empty; }

                jsonData.actionSuccessful = true;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = null;
                jsonData.idMessage = idGroupFeedMessage;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = null;
                jsonData.idMessage = null;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region DeleteMessage
        [WebMethod(EnableSession = true)]
        public static _WallMessagesJsonData DeleteMessage(int idMessage, int? idParentMessage)
        {
            _WallMessagesJsonData jsonData = new _WallMessagesJsonData();

            try
            {
                GroupFeedMessage.Delete(idMessage);

                jsonData.actionSuccessful = true;
                jsonData.html = String.Empty;
                jsonData.exception = String.Empty;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
            catch (Exception ex)
            {
                jsonData.actionSuccessful = false;
                jsonData.html = String.Empty;
                jsonData.exception = ex.Message;
                jsonData.lastRecord = null;
                jsonData.idMessage = idMessage;
                jsonData.idParentMessage = idParentMessage;

                // return jsonData
                return jsonData;
            }
        }
        #endregion

        #region _BuildDocumentsPanel
        /// <summary>
        /// Builds the documents panel.
        /// </summary>
        private void _BuildDocumentsPanel()
        {
            Panel documentsPanel = new Panel();
            documentsPanel.ID = "WallProperties_" + "Documents" + "_TabPanel";
            
            // if the feed is active, documents panel needs to be hidden, since feed is shown by default
            if ((bool)this._GroupObject.IsFeedActive && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.GROUPS_DISCUSSION_ENABLE))
            {
                documentsPanel.Attributes.Add("style", "display: none;");
            }

            TreeView documentsTree = new TreeView();
            documentsTree.ID = "DocumentsTree";

            DataTable documentFolders = new DataTable();
            documentFolders = DocumentRepositoryFolder.IdsAndNamesForDocumentRepositorySelectList(this._GroupObject.Id, Convert.ToInt32(DocumentRepositoryObjectType.Group));

            foreach (DataRow folder in documentFolders.Rows)
            { documentsTree.Nodes.Add(this._BuildDocumentsTreeNode(NodeSubType.Folder, Convert.ToInt32(folder["idDocumentRepositoryFolder"]), folder["name"].ToString(), null)); }

            DataTable documents = new DataTable();
            documents = DocumentRepositoryItem.GetObjectDocumentsInFolder(this._GroupObject.Id, Convert.ToInt32(DocumentRepositoryObjectType.Group));

            foreach (DataRow document in documents.Rows)
            { documentsTree.Nodes.Add(this._BuildDocumentsTreeNode(NodeSubType.Document, Convert.ToInt32(document["idDocumentRepositoryItem"]), document["fileName"].ToString(), document["label"].ToString())); }

            documentsTree.CollapseAll();

            documentsPanel.Controls.Add(documentsTree);

            // attach panel to container
            this.WallPropertiesTabPanelsContainer.Controls.Add(documentsPanel);
        }
        #endregion

        #region _BuildDocumentsTreeNode
        private TreeNode _BuildDocumentsTreeNode(NodeSubType nodeType, int id, string fileName, string label)
        {
            TreeNode node = new TreeNode(string.IsNullOrEmpty(label)? fileName : label, String.Empty);

            if (nodeType == NodeSubType.Folder)
            {
                DataTable documents = new DataTable();
                documents = DocumentRepositoryItem.GetObjectDocumentsInFolder(this._GroupObject.Id, Convert.ToInt32(DocumentRepositoryObjectType.Group), id);

                foreach (DataRow document in documents.Rows)
                { node.ChildNodes.Add(this._BuildDocumentsTreeNode(NodeSubType.Document, Convert.ToInt32(document["idDocumentRepositoryItem"]), document["fileName"].ToString(), document["label"].ToString())); }

                node.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FOLDER, ImageFiles.EXT_PNG);
                node.SelectAction = TreeNodeSelectAction.Expand;
            }
            if (nodeType == NodeSubType.Document)
            {
                node.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DOCUMENT, ImageFiles.EXT_PNG);
                node.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_GROUP + this._GroupObject.Id.ToString() + "/" + fileName;
                node.Target = "_blank";
                node.SelectAction = TreeNodeSelectAction.Select;
            }

            return node;
        }
        #endregion
    }
}