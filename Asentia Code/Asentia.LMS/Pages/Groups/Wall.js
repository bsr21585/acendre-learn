﻿function GetRecords() {
    IsLoading = true;
    $("#OlderMessagesLoadingPanel").show();

    $.ajax({
        type: "POST",
        url: "Wall.aspx/BuildFeedMessages",
        data: "{idGroup: " + GroupId + ", dtQuery: \"" + LastRecord + "\", getMessagesNewerThanDtQuery: " + false + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnGetRecordsSuccess,
        failure: function (response) {
            $("#OlderMessagesLoadingPanel").hide();
            IsLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            $("#OlderMessagesLoadingPanel").hide();
            IsLoading = false;
            //alert(response.d);
        }
    });
}

function OnGetRecordsSuccess(response) {
    var responseObject = response.d;

    $(responseObject.html).hide().appendTo("#WallMessagesContainer").fadeIn(1000);
    LastRecord = responseObject.lastRecord;

    $("#OlderMessagesLoadingPanel").hide();
    IsLoading = false;
}

function PostNewMessage(e) {
    var enterKey = 13;
    var leftMouseClick = 1; // for click of post message button

    if (e.which == enterKey || e.which == leftMouseClick) {
        var textBoxValue = $("#NewMessageField").val().replace(/\"/g, '\\\"');

        if (textBoxValue != "") {

            IsLoading = true;

            $.ajax({
                type: "POST",
                url: "Wall.aspx/SaveMessage",
                data: "{idGroup: " + GroupId + ", message: \"" + textBoxValue + "\", idParentMessage: " + null + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPostNewMessageSuccess,
                failure: function (response) {
                    IsLoading = false;
                    //alert(response.d);
                },
                error: function (response) {
                    IsLoading = false;
                    //alert(response.d);
                }
            });

            $("#NewMessageField").val("");
        }
    }
}

function OnPostNewMessageSuccess(response) {
    var responseObject = response.d;

    if (responseObject.html != "") {
        $(responseObject.html).hide().prependTo("#WallMessagesContainer").fadeIn(1000);
    }
    else {
        $("#HiddenButtonForPostedForModerationModal").click();
    }

    IsLoading = false;
}

function PostComment(sender, e) {
    var enterKey = 13;

    if (e.which == enterKey) {
        var textBoxValue = $("#" + sender.id).val();

        if (textBoxValue != "") {
            var idParentMessage = sender.id.replace("MessageCommentField_", "");

            IsLoading = true;

            $.ajax({
                type: "POST",
                url: "Wall.aspx/SaveMessage",
                data: "{idGroup: " + GroupId + ", message: \"" + textBoxValue + "\", idParentMessage: " + idParentMessage + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnPostCommentSuccess,
                failure: function (response) {
                    IsLoading = false;
                    //alert(response.d);
                },
                error: function (response) {
                    IsLoading = false;
                    //alert(response.d);
                }
            });

            $("#" + sender.id).val("");
        }
    }
}

function OnPostCommentSuccess(response) {
    var responseObject = response.d;

    if (responseObject.html != "") {
        $(responseObject.html).hide().appendTo("#MessageCommentsContainer_" + responseObject.idParentMessage).fadeIn(1000);
    }
    else {
        $("#HiddenButtonForPostedForModerationModal").click();
    }

    IsLoading = false;
}

function DeleteMessage(idMessage) {
    IsLoading = true;

    var idParentMessage = null;

    $.ajax({
        type: "POST",
        url: "Wall.aspx/DeleteMessage",
        data: "{idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnDeleteMessageSuccess,
        failure: function (response) {
            IsLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsLoading = false;
            //alert(response.d);
        }
    });
}

function OnDeleteMessageSuccess(response) {
    var responseObject = response.d;

    $("#MessageContainer_" + responseObject.idMessage).remove();
    $("#HiddenButtonForMessageDeletedModal").click();
    IsLoading = false;
}

function DeleteComment(idMessage, idParentMessage) {
    IsLoading = true;

    $.ajax({
        type: "POST",
        url: "Wall.aspx/DeleteMessage",
        data: "{idMessage: " + idMessage + ", idParentMessage: " + idParentMessage + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnDeleteCommentSuccess,
        failure: function (response) {
            IsLoading = false;
            //alert(response.d);
        },
        error: function (response) {
            IsLoading = false;
            //alert(response.d);
        }
    });
}

function OnDeleteCommentSuccess(response) {
    var responseObject = response.d;

    $("#MessageCommentContainer_" + responseObject.idParentMessage + "_" + responseObject.idMessage).remove();
    $("#HiddenButtonForMessageDeletedModal").click();
    IsLoading = false;
}