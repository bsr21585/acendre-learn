﻿// before unload event
function BeforeUnloadEvent(e) {
    (e || window.event).returnValue = ConfirmationMessage; //Gecko + IE
    return ConfirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
}

// on load event
function OnLoad() {
    var domain = window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    var endpoint = domain + EndPoint;
    var actor = { "mbox": "mailto:" + ActorEmail, "name": ActorName };
    var path = domain + PckagePath;
    path = path + '/' + LaunchPage;

    var link = path +
        "?endpoint=" + encodeURIComponent(endpoint) +
        "&actor=" + encodeURIComponent(JSON.stringify(actor)) +
        "&registration=" + encodeURIComponent(RegistrationKey) +
        "&activity_id=" + ActivityID;

     // assign the lesson link to ifreame
    document.getElementById("LessonFrame").setAttribute('src', link);

}

// Remove removeEventListener 
function RemoveBeforeUnloadEvent() {
    window.removeEventListener("beforeunload", BeforeUnloadEvent);
}     

// on ready event 
$(document).ready(function () {
    window.addEventListener("beforeunload", BeforeUnloadEvent);
    
    // timeout is 1 min
    setTimeout(KeepAliveSession, 500 * 60);
});

// Method to keep session alive
function KeepAliveSession() {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: GLOBAL.WEB_SERVICES.KeepAliveSessionURL,
        data: "{learnerId: " + LearnerId + "}",
        dataType: "json",
        onSuccess: function () { },
        onError: function () {
            alert(SessionLostErrorMessage);
        }
    });
    setTimeout(KeepAliveSession, 500 * 60);
}
