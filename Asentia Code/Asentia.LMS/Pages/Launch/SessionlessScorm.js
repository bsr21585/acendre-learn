﻿var startPOSTop;
var startPOSLeft;
var stopPOSTop;
var stopPOSLeft;
var isDragged = false;
var scheemaVersion = "";
var API_Asentia;
var API_1484_11_Asentia;
var courseLaunchController;

function isNullOrUndefined(element) {
    return (element === null) || (element === undefined);
};

$(document).ready(function () {

    // bind event to the launcher window
    // window.addEventListener("beforeunload", BeforeUnloadEvent); -- temporarily commented 

    var idEnrollment = EnrollmentId;
    var idEnrolledUser = IdEnrolledUser;
    var launchType = LaunchType;

    GLOBAL.COURSE_INFO.ContentFolder = ContentFolder + '/';
    GLOBAL.COURSE_INFO.PersistedDataFileName = PersistedDataFileName;
    if (launchType == 'ojt') {
        GLOBAL.COURSE_INFO.ReturnUrl = "/administrator/users/enrollments/Activity.aspx?uid=" + idEnrolledUser + "&id=" + idEnrollment;
    }
    else if (launchType == 'learner') {
        GLOBAL.COURSE_INFO.ReturnUrl = "/dashboard/Enrollment.aspx?idEnrollment=" + idEnrollment;
    }
    else if (launchType == 'ojtWidget') {
        GLOBAL.COURSE_INFO.ReturnUrl = "/dashboard";
    }
    else {
        GLOBAL.COURSE_INFO.ReturnUrl = null;
    }
    GLOBAL.COURSE_INFO.LearnerName = LearnerName;
    GLOBAL.COURSE_INFO.LearnerId = LearnerId;
    GLOBAL.COURSE_INFO.EnrollmentIdentifier = EnrollmentIdentifier;

    courseLaunchController = new CourseLaunchController(GLOBAL.COURSE_INFO.ManifestId, GLOBAL.COURSE_INFO.ReturnUrl, "Wait");
    API_Asentia = courseLaunchController.getScorm12Api(); // SCORM 1.2 API
    API_1484_11_Asentia = courseLaunchController.getScorm2004Api();

    scheemaVersion = API_1484_11_Asentia.schemaversion;

    // make loader image visible on loader modal popup always
    $("#LoadAndSaveScoDataModalModalPopupPostbackLoadingPlaceholder").show();;

    setTimeout(KeepAliveSession, 5000 * 60);

});

// Page load method
function pageLoad() {
    // display the loading content message in the loader modal popup
    $("#LoadAndSaveScoDataModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").html(LoadingContentStr);

}


// Method to set the url for Lesson frame
function SetContentFrameUrl(href) {

    document.getElementById("LessonFrame").src = href;
}

// Making Draggable to loggerContainer
$(function () {

    var counts = 0;
    var distance = 30;
    $('#LoggerContainer').draggable({

        // find original position of dragged image.
        start: function (event, ui) {

            // show start dragged position of image.
            var Startpos = $(this).position();
            startPOSTop = Startpos.top;
            startPOSLeft = Startpos.left;
        },

        // find position where image is dropped.
        stop: function (event, ui) {

            // show dropped position.
            var Stoppos = $(this).position();

            stopPOSTop = Stoppos.top;
            stopPOSLeft = Stoppos.left;

            isDragged = false;
        },

        drag: function () {
            isDragged = true;
        }
    });
});

// Managing tree button visibility
function ManageTreeButtonsAndSCOStatus() {

    if (API_1484_11_Asentia.schemaversion.indexOf("1.2") > -1) {
        API_Asentia.UpdateStateDisplay();
        API_Asentia.courseLaunchController.EnableTreeItems();
    }
    else {

        API_1484_11_Asentia.scorm2004ADLNav.UpdateStateDisplay();
        API_1484_11_Asentia.scorm2004ADLNav.CheckMenu();
    }    
}

// Method to toggle content menu containing activity tree
function ToggleContentMenu() {
    if ($("#SCOTreeMenuContainer").css("left") == "0px") {
        $("#SCOTreeMenuContainer").css("left", "-301px");
        $("#LessonFrame").css("visibility", "visible");
    }
    else {
        $("#SCOTreeMenuContainer").css("left", "0px");
        $("#LessonFrame").css("visibility", "hidden");
    }
}

// Method to close content menu
function CloseContentMenu(makeFrameVisible) {
    if ($("#SCOTreeMenuContainer").css("left") == "0px") {
        $("#SCOTreeMenuContainer").css("left", "-301px");

        if (makeFrameVisible) {
            $("#LessonFrame").css("visibility", "visible");
        }
    }
}

// Method to keep user session alive troupgh out the package learning
function KeepAliveSession() {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: GLOBAL.WEB_SERVICES.KeepAliveSessionURL,
        data: "{learnerId: " + LearnerId + "}",
        dataType: "json",
        onSuccess: function () { },
        onError: function () {
            alert(SessionLostErrorMessage);
        }
    });
    setTimeout(KeepAliveSession, 5000 * 60);
}

// Method to make loader modal popup visible and setting its header and message value
function ShowLoaderModalPopup(msg) {
    // open loader modal popup 
    $("#" + HiddenLoadAndSaveContentTargetButton + "").click();

    // set the value of loader modal popup header
    $("#LoadAndSaveScoDataModalModalPopupHeaderText").html(PackageName);
    $("#LoadAndSaveScoDataModalModalPopupPostbackLoadingPlaceholderContentLoadingImageWrapper").show();
    if (msg == SavingContentStr) {
        // display the saving content message
        $("#LoadAndSaveScoDataModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").html(SavingContentStr);
    }
    else {
        // display the loading content message
        $("#LoadAndSaveScoDataModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").html(LoadingContentStr);
    }
}

// Hide the loader modal popup
function HideLoadAndSaveContentModalPopup() {
    $find('LoadAndSaveScoDataModalModalPopupExtender').hide();
}

// Add event BeforeUnloadEvent
function BeforeUnloadEvent(e) {
    //(e || window.event).returnValue = ConfirmationMessage; //Gecko + IE
    //return ConfirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
}

// Remove removeEventListener 
function RemoveBeforeUnloadEvent() {
    window.removeEventListener("beforeunload", BeforeUnloadEvent);
}


// METHOD: to hide the Logger container

function HideLoggerContainer() {
    $("#LoggerContainer").css("display", "none");
}

/*
* METHOD: Collapse Logger container
* Method that is called hide the widget and save the configuration.
*/
function CollapseLoggerContainer(element) {

    var widgetCollapseExpandButtonId = $(element).attr("id");
    var widgetContentId = widgetCollapseExpandButtonId.toString().replace("CollapseExpandIcon", "Content");

    // collapse/expand the Logger container widget
    if ($(".logger").is(":visible")) {
        $("#LoggerContainer > div:not(:first-child)").css("display", "none");
        $("#" + widgetCollapseExpandButtonId + " img").attr("src", ExpandImagePath);
    }
    else {
        $("#LoggerContainer > div:not(:first-child)").css("display", "block");
        $("#" + widgetCollapseExpandButtonId + " img").attr("src", CollapseImagePath);
    }
}


// Display the message to user for unavailable activity
function ShowUserInstruction() {
    $("#LoadAndSaveScoDataModalModalPopupPostbackLoadingPlaceholderContentLoadingImageWrapper").hide();
    $("#LoadAndSaveScoDataModalModalPopupPostbackLoadingPlaceholderContentLoadingTextWrapper").html(ThisActivityIsNotAvailablePleaseOpenTheMenuAndSelectAnotherActivityToContinue);
}