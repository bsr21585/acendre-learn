﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Text;
using Asentia.LMS;
using System.Collections;
using Asentia.Common;
using Asentia.LMS.Library;
using Asentia.UMS.Library;
using Asentia.Controls;
using System.Web.UI.HtmlControls;
using System.Xml;
using AjaxControlToolkit;


namespace Asentia.LMS.Pages.Launch
{
    public partial class SessionlessScorm : Page
    {
        #region Public Properties
        public Panel LauncherContainer = new Panel();
        public ToolkitScriptManager asm;
        #endregion

        #region Private Properties
        private Panel _ContentHeaderAndNavigationContainer;

        private int _IdDataLesson;
        private int _IdContentPackage;
        private string _LaunchType;
        private int _IdEnrolledUser;
        private int _IdDataSco;
        private bool _SaveToDB = false;
        private string _CmiMode = "normal";
        private DataLesson _DataLessonObject;
        private ContentPackage _ContentPackageObject;

        private ImageButton _BtnPrevious = new ImageButton();
        private ImageButton _BtnContinue = new ImageButton();
        private ImageButton _BtnExitAll = new ImageButton();
        private ImageButton _BtnExitThisSCO = new ImageButton();
        private ImageButton _BtnAbandon = new ImageButton();
        private ImageButton _BtnAbandonAll = new ImageButton();
        private ImageButton _BtnSuspendAll = new ImageButton();

        private string _PackageName = string.Empty;
        private string _PackageOrganizationIdentifier = string.Empty;
        private static string _LoaderImageUrl = string.Empty;
        
        private ModalPopup _LoadAndSaveScoDataModal;        
        private Button _HiddenLoadAndSaveContentTargetButton;
        private bool _debugMode = false;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery-1.10.2.min.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery-ui.js");

            #region Attach all related js files            
            csm.RegisterClientScriptResource(typeof(Scorm), "Asentia.LMS.Pages.Launch.SessionlessScorm.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.util.Util), "Asentia.LMS.SCORM_API.util.JSON.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.treeutil.TreeUtil), "Asentia.LMS.SCORM_API.treeutil.TreeMenu.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.util.Util), "Asentia.LMS.SCORM_API.util.Function.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.util.Util), "Asentia.LMS.SCORM_API.util.Event.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.util.Util), "Asentia.LMS.SCORM_API.util.EventDispatcher.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.util.Util), "Asentia.LMS.SCORM_API.util.ArrayList.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.util.Util), "Asentia.LMS.SCORM_API.util.Queue.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.util.Util), "Asentia.LMS.SCORM_API.util.Logger.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.util.Util), "Asentia.LMS.SCORM_API.util.JSONLoader.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.util.Util), "Asentia.LMS.SCORM_API.util.StringHelper.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.LessonLaunch), "Asentia.LMS.SCORM_API.lessonLaunch.Globals.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.LessonLaunch), "Asentia.LMS.SCORM_API.lessonLaunch.CourseLaunchController.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.LessonLaunch), "Asentia.LMS.SCORM_API.lessonLaunch.ManifestDataLoader.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.LessonLaunch), "Asentia.LMS.SCORM_API.lessonLaunch.RTEDataLoader.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.LessonLaunch), "Asentia.LMS.SCORM_API.lessonLaunch.RTEDataSaver.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004Api.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004DataItem.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004DataModel.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004Interaction.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004InteractionCorrectResponse.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004InteractionObjective.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004Objective.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004Comment.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004ADLNavChoice.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Scorm2004), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm2004.Enums.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12Api.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12DataItem.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12DataModel.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12Interaction.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12InteractionCorrectResponse.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12InteractionObjective.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12), "Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12Objective.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.scorm.scorm12.Scorm12), "Asentia.LMS.SCORM_API.lessonLaunch.ServerSessionPersister.js");

            #endregion

            // instansiate a user object so we can get the learner's name
            User userObject = new User(this._IdEnrolledUser);

            // build global javascript variables
            StringBuilder jsVariables = new StringBuilder();

            jsVariables.AppendLine("var CurrentPageLanguage = \"" + AsentiaSessionState.UserCulture + "\";");
            jsVariables.AppendLine("var EnrollmentId = \"" + this._DataLessonObject.IdEnrollment + "\";");
            jsVariables.AppendLine("var DataScoId = \"" + this._IdDataSco + "\";");
            jsVariables.AppendLine("var SaveToDB = \"" + this._SaveToDB + "\";");
            jsVariables.AppendLine("var CmiMode = \"" + this._CmiMode + "\";");
            jsVariables.AppendLine("var IdEnrolledUser = \"" + this._IdEnrolledUser + "\";");
            jsVariables.AppendLine("var LaunchType = \"" + this._LaunchType + "\";");
            jsVariables.AppendLine("var ContentFolder = \"" + this._ContentPackageObject.Path + "\";");
            jsVariables.AppendLine("var PersistedDataFileName = \"" + this._ContentPackageObject.Name + "\";");
            jsVariables.AppendLine("var LearnerName = \"" + userObject.FirstName + " " + userObject.LastName + "\";");
            jsVariables.AppendLine("var LearnerId = \"" + this._IdEnrolledUser + "\";");
            jsVariables.AppendLine("var EnrollmentIdentifier = \"" + this._DataLessonObject.Id + "\";");
            jsVariables.AppendLine("var ConfirmationMessage = \"" + _GlobalResources.IfYouLeaveModuleBeforeSavingStatusWillNotBeSaved + "\";");
            jsVariables.AppendLine("var PackageName = \"" + this._PackageName + "\";");
            jsVariables.AppendLine("var HiddenLoadAndSaveContentTargetButton = \"" + this._HiddenLoadAndSaveContentTargetButton.ClientID + "\";");
            jsVariables.AppendLine("var LoadingContentStr  = \"" + _GlobalResources.LoadingContent + "\";");
            jsVariables.AppendLine("var SavingContentStr = \"" + _GlobalResources.SavingData + "\";");
            jsVariables.AppendLine("var ThisActivityIsNotAvailablePleaseOpenTheMenuAndSelectAnotherActivityToContinue = \"" + _GlobalResources.ThisActivityIsNotAvailablePleaseOpenTheMenuAndSelectAnotherActivityToContinue + "\";");
            jsVariables.AppendLine("var SessionLostErrorMessage = \"" + _GlobalResources.YourSessionHasExpired + "\";");
            jsVariables.AppendLine("var PleaseClickTheMenuAndSelectAItemToContinueMessage = \"" + _GlobalResources.PleaseClickTheMenuAndSelectAnItemToContinue + "\";");
            jsVariables.AppendLine("var LoaderImageUrl = \"" + _LoaderImageUrl + "\";");
            jsVariables.AppendLine("var DebugMode = \"" + this._debugMode + "\";");
            jsVariables.AppendLine("var ExpandImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_EXPAND, ImageFiles.EXT_PNG) + "\";");
            jsVariables.AppendLine("var CollapseImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_COLLAPSE, ImageFiles.EXT_PNG) + "\";");

            csm.RegisterStartupScript(typeof(Scorm), "JsVariables", jsVariables.ToString(), true);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {            
            // set session context
            AsentiaSessionState.IdSiteUser = 152629;
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);

            User userObject = new User(AsentiaSessionState.IdSiteUser);

            AsentiaSessionState.UserFirstName = userObject.FirstName;
            AsentiaSessionState.UserLastName = userObject.LastName;
            AsentiaSessionState.UserAvatar = userObject.Avatar;
            AsentiaSessionState.UserGender = userObject.Gender;
            AsentiaSessionState.UserCulture = userObject.LanguageString;
            AsentiaSessionState.IsUserACourseExpert = userObject.IsUserACourseExpert;
            AsentiaSessionState.IsUserACourseApprover = userObject.IsUserACourseApprover;
            AsentiaSessionState.IsUserASupervisor = userObject.IsUserASupervisor;
            AsentiaSessionState.IsUserAWallModerator = userObject.IsUserAWallModerator;
            AsentiaSessionState.IsUserAnILTInstructor = userObject.IsUserAnILTInstructor;
            AsentiaSessionState.UserMustChangePassword = userObject.MustChangePassword;

            // get the user's effective permissions
            DataTable effectivePermissions = UMS.Library.User.GetEffectivePermissions();
            List<AsentiaPermissionWithScope> effectivePermissionsList = new List<AsentiaPermissionWithScope>();

            foreach (DataRow row in effectivePermissions.Rows)
            {
                AsentiaPermissionWithScope permissionWithScope = new AsentiaPermissionWithScope((AsentiaPermission)Convert.ToInt32(row["idPermission"]), row["scope"].ToString());
                effectivePermissionsList.Add(permissionWithScope);
            }

            AsentiaSessionState.UserEffectivePermissions = effectivePermissionsList;

            // get portal title in language for "browser title"
            string portalTitleInInterfaceLanguage = AsentiaSessionState.GlobalSiteObject.Title;
            this.Page.Title = portalTitleInInterfaceLanguage;

            // get the form parameters if they aren't empty, otherwise get querystring
            string variableType = String.Empty;

            if (!String.IsNullOrWhiteSpace(Request.Form["id"]) && !String.IsNullOrWhiteSpace(Request.Form["cpid"]))
            {
                variableType = "FORM POST";
                this._IdDataLesson = Convert.ToInt32(Request.Form["id"]);
                this._IdContentPackage = Convert.ToInt32(Request.Form["cpid"]);
                this._LaunchType = HttpContext.Current.Request.Form["launchType"];
                Boolean.TryParse(Request.Form["DebugMode"], out this._debugMode);
            }
            else
            {
                variableType = "QUERYSTRING";
                this._IdDataLesson = Convert.ToInt32(Request.QueryString["id"]);
                this._IdContentPackage = Convert.ToInt32(Request.QueryString["cpid"]);
                this._LaunchType = HttpContext.Current.Request.QueryString["launchType"];
                Boolean.TryParse(Request.QueryString["DebugMode"], out this._debugMode);
            }

            Response.Write("THE FOLLOWING ARE " + variableType + " VARIABLES: <br />");
            Response.Write("ID: " + Request.Form["id"] + "<br />");
            Response.Write("CPID: " + Request.Form["cpid"] + "<br />");            

            _LoaderImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING, ImageFiles.EXT_GIF);

            // validate the launch type - bounce the user out of here if it isn't right
            //if (this._LaunchType != "learner" && this._LaunchType != "ojt" && this._LaunchType != "ojtWidget")
            //{ Response.Redirect("/"); }

            // instansiate data lesson and content package objects, and get the user's id this enrollment is for
            this._DataLessonObject = new DataLesson(this._IdDataLesson);
            this._ContentPackageObject = new ContentPackage(this._IdContentPackage);

            // note, we obtain the enrolled user's id from the enrollment object because the launching user might not be
            // the user the enrollment belongs to because of ojt
            Enrollment enrollmentObject = new Enrollment(this._DataLessonObject.IdEnrollment);
            this._IdEnrolledUser = enrollmentObject.IdUser;

            // if this a learner launch, validate that the enrollment belongs to the launching user - bounce them if not
            if (this._LaunchType == "learner" && AsentiaSessionState.IdSiteUser != this._IdEnrolledUser)
            { Response.Redirect("/"); }

            // initialize sco data in the database
            this._IdDataSco = DataLesson.InitializeSCOData(this._IdDataLesson);

            DataSCO objectDataSCO = new DataSCO(this._IdDataSco);

            // set the cmi mode variable to "review" if the status is already marked as completed
            if (objectDataSCO.CompletionStatus == (int)Lesson.ScormCompletionStatus.completed)
            {
                this._CmiMode = "review";
            }

            // include page specific and site specific css class
            this._IncludeCSSFiles();

            // initialize the navigation buttons and assign css classes
            this._InitializeButtons();

            // build the launcher iframe
            this._BuildLauncherContainer();            
        }
        #endregion

        #region _IncludeCSSFiles
        /// <summary>
        /// Method to include all css files.
        /// </summary>
        private void _IncludeCSSFiles()
        {
            // tag format for the stylesheet link
            const string STYLE_SHEET_LINK_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}?ver={1}\" />\r\n";

            // style sheets that aren't site configurable - located in the root css folder
            ArrayList notSiteConfigurableStyleSheets = new ArrayList();

            // add stylesheets
            notSiteConfigurableStyleSheets.Add("Reset.css");

            // stylesheets that are site configurable - located in css folders under _config
            // defaults will always be loaded first, followed by client configured ones
            ArrayList defaultSiteConfigurableStyleSheets = new ArrayList();

            // add stylesheets
            defaultSiteConfigurableStyleSheets.Add("page-specific/launch/Scorm.css");
            defaultSiteConfigurableStyleSheets.Add("Layout.css");
            defaultSiteConfigurableStyleSheets.Add("Modal.css");

            // attach references to "not site configurable" stylesheets
            foreach (string stylesheet in notSiteConfigurableStyleSheets)
            {
                string pathToStylesheet = MapPathSecure(SitePathConstants.CSS + stylesheet);

                if (File.Exists(pathToStylesheet))
                {
                    this.Page.Header.Controls.Add(
                        new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                    );
                }
            }

            // attach references to "default site configurable" stylesheets
            if (AsentiaSessionState.IsThemePreviewMode)
            {
                foreach (string stylesheet in defaultSiteConfigurableStyleSheets)
                {
                    string pathToStylesheet = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet);

                    if (File.Exists(pathToStylesheet))
                    {
                        this.Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
            }
            else
            {
                foreach (string stylesheet in defaultSiteConfigurableStyleSheets)
                {
                    string pathToStylesheet = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet);

                    if (File.Exists(pathToStylesheet))
                    {
                        this.Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
            }

            // style sheets that are site configurable but not of default site
            ArrayList siteConfigurableStyleSheets = new ArrayList();
            siteConfigurableStyleSheets.Add("page-specific/launch/Scorm.css");
            siteConfigurableStyleSheets.Add("Layout.css");
            siteConfigurableStyleSheets.Add("Modal.css");

            // attach references to  site configurable stylesheets
            if (AsentiaSessionState.IsThemePreviewMode)
            {
                foreach (string stylesheet in siteConfigurableStyleSheets)
                {
                    string pathToStylesheet = MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet);

                    if (File.Exists(pathToStylesheet))
                    {
                        this.Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
            }
            else
            {
                foreach (string stylesheet in siteConfigurableStyleSheets)
                {
                    string pathToStylesheet = MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet);

                    if (File.Exists(pathToStylesheet))
                    {
                        this.Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
            }
        }
        #endregion

        #region _InitializeButtons
        /// <summary>
        /// Initializes naviagation control buttons.
        /// Abandon, AbandonAll, Suspend, SuspendAll, and ExitThisSCO buttons are not added in the page. 
        /// To add these buttons, you need to uncomment the commented code below in the _InitializeButtons() method to 
        /// add them to the page and also uncomment code from the CourseLaunchController.js file within the functions
        /// 'evaluateLMSUIButtonStates' and 'evaluateLMSUIButtonStatesForSco12'.
        /// </summary>
        private void _InitializeButtons()
        {
            // attach content header and navigation container
            this._ContentHeaderAndNavigationContainer = new Panel();
            this._ContentHeaderAndNavigationContainer.ID = "ContentHeaderAndNavigationContainer";
            this.LauncherContainer.Controls.Add(this._ContentHeaderAndNavigationContainer);

            // assign ids to slide and navigation control buttons as these ids are used in js files
            this._BtnPrevious.ID = "BtnPrevious";
            this._BtnContinue.ID = "BtnContinue";
            this._BtnExitAll.ID = "BtnExitAll";
            //this._BtnExitThisSCO.ID = "BtnExitThisSCO";
            //this._BtnAbandon.ID = "BtnAbandon";
            //this._BtnAbandonAll.ID = "BtnAbandonAll";
            //this._BtnSuspendAll.ID = "BtnSuspendAll";

            // assign titles to the navigation buttons
            this._BtnPrevious.ToolTip = _GlobalResources.Previous;
            this._BtnContinue.ToolTip = _GlobalResources.Next;
            this._BtnExitAll.ToolTip = _GlobalResources.Exit;
            //this._BtnExitThisSCO.ToolTip = _GlobalResources.ExitThisSco;
            //this._BtnAbandon.ToolTip = _GlobalResources.Abandon;
            //this._BtnAbandonAll.ToolTip = _GlobalResources.AbandonAll;
            //this._BtnSuspendAll.ToolTip = _GlobalResources.Suspend;

            // hidden "loader/saver" modal target button 
            this._HiddenLoadAndSaveContentTargetButton = new Button();
            this._HiddenLoadAndSaveContentTargetButton.ID = "HiddenLoadAndSaveContentTargetButton";
            this._HiddenLoadAndSaveContentTargetButton.Style.Add("display", "none");
            this._HiddenLoadAndSaveContentTargetButton.ClientIDMode = ClientIDMode.Static;
            this._ContentHeaderAndNavigationContainer.Controls.Add(this._HiddenLoadAndSaveContentTargetButton);

            // set image url properties for naviagation buttons
            this._BtnPrevious.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_BACK, ImageFiles.EXT_PNG);
            this._BtnContinue.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FORWARD, ImageFiles.EXT_PNG);
            this._BtnExitAll.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE, ImageFiles.EXT_PNG);
            //this._BtnExitThisSCO.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE, ImageFiles.EXT_PNG);
            //this._BtnAbandon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE, ImageFiles.EXT_PNG);
            //this._BtnAbandonAll.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE, ImageFiles.EXT_PNG);
            //this._BtnSuspendAll.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE, ImageFiles.EXT_PNG);

            // set css class for navigation buttons
            this._BtnPrevious.CssClass = "ContentNavigationButton";
            this._BtnContinue.CssClass = "ContentNavigationButton";
            this._BtnExitAll.CssClass = "ContentNavigationButton";
            //this._BtnExitThisSCO.CssClass = "ContentNavigationButton";
            //this._BtnAbandon.CssClass = "ContentNavigationButton";
            //this._BtnAbandonAll.CssClass = "ContentNavigationButton";
            //this._BtnSuspendAll.CssClass = "ContentNavigationButton";

            // set onClientClick property of navigation buttons
            this._BtnPrevious.OnClientClick = "courseLaunchController.processLMSNavRequest('previous'); return false;";
            this._BtnContinue.OnClientClick = "courseLaunchController.processLMSNavRequest('continue'); return false;";
            this._BtnExitAll.OnClientClick = "courseLaunchController.processLMSNavRequest('exitAll'); return false;";
            //this._BtnExitThisSCO.OnClientClick = "courseLaunchController.processLMSNavRequest('exit'); return false;";
            //this._BtnAbandon.OnClientClick = "courseLaunchController.processLMSNavRequest('exitAll'); return false;";
            //this._BtnAbandonAll.OnClientClick = "courseLaunchController.processLMSNavRequest('exitAll'); return false;";
            //this._BtnSuspendAll.OnClientClick = "courseLaunchController.processLMSNavRequest('suspendAll'); return false;";

            // sub panel for navigation buttons
            Panel contentNavigationButtonsContainer = new Panel();
            contentNavigationButtonsContainer.ID = "ContentNavigationButtonsContainer";

            // add navigation buttons to container
            contentNavigationButtonsContainer.Controls.Add(this._BtnPrevious);
            contentNavigationButtonsContainer.Controls.Add(this._BtnContinue);
            contentNavigationButtonsContainer.Controls.Add(this._BtnExitAll);
            //contentNavigationButtonsContainer.Controls.Add(this._BtnExitThisSCO);
            //contentNavigationButtonsContainer.Controls.Add(this._BtnAbandon);
            //contentNavigationButtonsContainer.Controls.Add(this._BtnAbandonAll);
            //contentNavigationButtonsContainer.Controls.Add(this._BtnSuspendAll);

            // add buttons container to the header and navigation container
            this._ContentHeaderAndNavigationContainer.Controls.Add(contentNavigationButtonsContainer);
        }
        #endregion

        #region OnInit
        /// <summary>
        /// OnInit event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            // add control bundle to tooklitscriptmanager
            ControlBundle masterBundle = new ControlBundle();
            masterBundle.Name = "MasterBundle";
            asm.ControlBundles.Add(masterBundle);
        }
        #endregion

        #region _BuildLauncherContainer
        /// <summary>
        /// Builds the scorm content launcher container and logger container.
        /// </summary>
        private void _BuildLauncherContainer()
        {
            #region HEADER CONTAINER

            // content menu toggle link
            HyperLink contentMenuToggle = new HyperLink();
            contentMenuToggle.ID = "ContentMenuToggle";
            contentMenuToggle.Attributes.Add("onclick", "ToggleContentMenu();");

            // create span to add inside the toggle menu area just for design purpose
            HtmlGenericControl span1 = new HtmlGenericControl("span");
            HtmlGenericControl span2 = new HtmlGenericControl("span");
            HtmlGenericControl span3 = new HtmlGenericControl("span");

            // add spans to menu icon
            contentMenuToggle.Controls.Add(span1);
            contentMenuToggle.Controls.Add(span2);
            contentMenuToggle.Controls.Add(span3);

            // get the manifest so we can retrieve the title and identifier
            string manifestXML = this._ContentPackageObject.Manifest;

            // get the title and identifier from the manifest
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(manifestXML);

            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("organization");

            foreach (XmlNode xmlNode in nodeList)
            {
                // this._PackageName = xmlNode["title"].InnerText; // we're going to use the lesson name as the title
                this._PackageOrganizationIdentifier = xmlNode.Attributes["identifier"].Value;
            }

            // get the lesson name to use as the _PackageName in the title
            Lesson lessonObject = new Lesson(this._DataLessonObject.IdLesson);

            this._PackageName = lessonObject.Title; // set the default title in case we don't find a language-specific one

            foreach (Lesson.LanguageSpecificProperty lessonLSP in lessonObject.LanguageSpecificProperties)
            {
                if (lessonLSP.LangString == AsentiaSessionState.UserCulture)
                { this._PackageName = lessonLSP.Title; }
            }

            // package's over all status indicator image
            Image contentCompletionStatusImage = new Image();
            contentCompletionStatusImage.ID = "img_status_" + this._PackageOrganizationIdentifier; ;
            contentCompletionStatusImage.CssClass = "ContentCompletionStatusImage";
            contentCompletionStatusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);

            // package's title
            Label packageTitle = new Label();
            packageTitle.ID = "ContentTitle";
            packageTitle.Text = this._PackageName;

            #endregion

            // add controls to main panel on header 
            this._ContentHeaderAndNavigationContainer.Controls.Add(contentMenuToggle);
            this._ContentHeaderAndNavigationContainer.Controls.Add(contentCompletionStatusImage);
            this._ContentHeaderAndNavigationContainer.Controls.Add(packageTitle);

            #region ACTIVITY TREE CONTAINER

            // sco tree menu container
            Panel scoTreeMenuContainer = new Panel();
            scoTreeMenuContainer.ID = "SCOTreeMenuContainer";

            // sco tree menu close button container
            Panel scoTreeMenuCloseButtonContainer = new Panel();
            scoTreeMenuCloseButtonContainer.ID = "SCOTreeMenuCloseButtonContainer";

            // close slider menu indicator image 
            Image closeMenu = new Image();
            closeMenu.CssClass = "CloseMenu";
            closeMenu.ToolTip = _GlobalResources.Close;
            closeMenu.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PREVIOUS, ImageFiles.EXT_PNG);
            closeMenu.Attributes.Add("onclick", "CloseContentMenu(true);");

            // add toggle panel close button
            scoTreeMenuCloseButtonContainer.Controls.Add(closeMenu);
            scoTreeMenuContainer.Controls.Add(scoTreeMenuCloseButtonContainer);

            // activity tree will be placed inside this container
            Panel activityTreeContainer = new Panel();
            activityTreeContainer.ID = "ActivityTreeContainer";

            // add panel to tree container body
            scoTreeMenuContainer.Controls.Add(activityTreeContainer);

            #endregion

            // add toggle container to the main panel
            this.LauncherContainer.Controls.Add(scoTreeMenuContainer);

            #region LOGGER CONTAINER

            Panel loggerContainer = new Panel();
            loggerContainer.ID = "LoggerContainer";
            loggerContainer.CssClass = "LoggerContainer";


            if (this._debugMode)
            { loggerContainer.Attributes.Add("style", "display:block !important;"); }
            else
            { loggerContainer.Attributes.Add("style", "display:none !important;"); }

            // login container header
            Panel loggerContainerHeader = new Panel();
            loggerContainerHeader.ID = "LoggerContainerHeader";
            loggerContainerHeader.CssClass = "LoggerContainerHeader";


            // header text
            HtmlGenericControl headerWrapper = new HtmlGenericControl("p");
            headerWrapper.ID = "LoggerContainerHeaderWrapper";

            // logger header test 
            Localize headerText = new Localize();
            headerText.Text = _GlobalResources.DebuggingInformation;

            // add logger text header
            headerWrapper.Controls.Add(headerText);
            loggerContainerHeader.Controls.Add(headerWrapper);

            // close icon
            HyperLink closeIcon = new HyperLink();
            closeIcon.CssClass = "WidgetControlIcon";
            closeIcon.ID = "LoggerContainerCloseIcon";
            closeIcon.Attributes.Add("onclick", "HideLoggerContainer();");
            closeIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE, ImageFiles.EXT_PNG);
            loggerContainerHeader.Controls.Add(closeIcon);

            // collapse/expand icon
            HyperLink collapseExpandIcon = new HyperLink();
            collapseExpandIcon.CssClass = "WidgetControlIcon";
            collapseExpandIcon.ID = "LoggerContainerCollapseExpandIcon";
            collapseExpandIcon.Attributes.Add("onclick", "CollapseLoggerContainer(this);");
            collapseExpandIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COLLAPSE, ImageFiles.EXT_PNG);
            loggerContainerHeader.Controls.Add(collapseExpandIcon);

            loggerContainer.Controls.Add(loggerContainerHeader);
            this.LauncherContainer.Controls.Add(loggerContainer);

            #endregion

            #region LESSON CONTENT FRAME

            Panel lessonContentFrameContainer = new Panel();
            lessonContentFrameContainer.ID = "LessonContentFrameContainer";

            // lesson frame
            HtmlIframe lessonFrame = new HtmlIframe();
            lessonFrame.ID = "LessonFrame";
            lessonFrame.Src = "../launch/ContentPlaceHolder.html";

            lessonContentFrameContainer.Controls.Add(lessonFrame);
            this.LauncherContainer.Controls.Add(lessonContentFrameContainer);

            #endregion

            // build navigation modal
            this._BuildLoadAndSaveSCOContentModal();

            // show loading modal in the begining
            this._LoadAndSaveScoDataModal.ShowModal();
        }
        #endregion

        #region _BuildLoadAndSaveSCOContentModal
        /// <summary>
        /// Builds loading or saving information, with loader, modal popup.
        /// </summary>
        private void _BuildLoadAndSaveSCOContentModal()
        {
            this._LoadAndSaveScoDataModal = new ModalPopup("LoadAndSaveScoDataModal");

            // set modal properties
            this._LoadAndSaveScoDataModal.Type = ModalPopupType.Confirm;
            this._LoadAndSaveScoDataModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);
            this._LoadAndSaveScoDataModal.HeaderIconAlt = this._PackageName;
            this._LoadAndSaveScoDataModal.HeaderText = this._PackageName;
            this._LoadAndSaveScoDataModal.TargetControlID = this._HiddenLoadAndSaveContentTargetButton.ClientID;
            this._LoadAndSaveScoDataModal.SubmitButton.Visible = false;
            this._LoadAndSaveScoDataModal.CloseButton.Visible = false;
            this._LoadAndSaveScoDataModal.ShowCloseIcon = false;
            this._LoadAndSaveScoDataModal.ShowLoadingPlaceholder = true;

            // add modal popup to main container
            this._ContentHeaderAndNavigationContainer.Controls.Add(this._LoadAndSaveScoDataModal);
        }
        #endregion
    }
}
