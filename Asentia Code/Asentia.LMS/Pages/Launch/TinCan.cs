﻿using AjaxControlToolkit;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

namespace Asentia.LMS.Pages.Launch
{
    public partial class TinCan : Page
    {
        #region Public Properties
        public Panel LessonContentFrameContainer;
        public ToolkitScriptManager Asm;
        public Panel ContentHeaderAndNavigationContainer;

        #endregion

        #region Private Properties
        private string _ActivityID;
        private string _LaunchPage;
        private int _IdDataLesson;
        private int _IdContentPackage;
        private string _LaunchType;
        private int _IdEnrolledUser;

        private DataLesson _DataLessonObject;
        private ContentPackage _ContentPackageObject;
        private ImageButton _ExitActionButton;  
        private string _PackageName = string.Empty;
        private string _PackageOrganizationIdentifier = string.Empty;
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery-1.10.2.min.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery-ui.js");
            csm.RegisterClientScriptResource(typeof(Asentia.LMS.SCORM_API.lessonLaunch.LessonLaunch), "Asentia.LMS.SCORM_API.lessonLaunch.Globals.js");

            // xAPI API
            csm.RegisterClientScriptResource(typeof(TinCan), "Asentia.LMS.Pages.Launch.TinCan.js");

            // instansiate a user object so we can get the learner's name
            User userObject = new User(this._IdEnrolledUser);

            // build global JS variables
            StringBuilder jsVariables = new StringBuilder();

            jsVariables.AppendLine("EndPoint = \"" + Config.TinCanApiSettings.EndPoint + "\";");
            jsVariables.AppendLine("RegistrationKey = \"" + Config.TinCanApiSettings.RegistrationKey + "\";");
            jsVariables.AppendLine("Auth = \"" + HttpUtility.UrlEncode(Config.TinCanApiSettings.AuthUser + "=" + Config.TinCanApiSettings.AuthPassword) + "\";");
            jsVariables.AppendLine("ActorName = \"" + userObject.DisplayName + "\";");
            jsVariables.AppendLine("ActorEmail = \"" + userObject.Email + "\";");
            jsVariables.AppendLine("var LaunchType = \"" + this._LaunchType + "\";");
            jsVariables.AppendLine("PckagePath = \"" + this._ContentPackageObject.Path + "\";");
            jsVariables.AppendLine("ActivityID = \"" + this._ActivityID + "\";");
            jsVariables.AppendLine("LaunchPage = \"" + this._LaunchPage + "\";");
            jsVariables.AppendLine("var LearnerId = \"" + this._IdEnrolledUser + "\";");
            jsVariables.AppendLine("var SessionLostErrorMessage = \"" + _GlobalResources.YourSessionHasExpired + "\";");
            jsVariables.AppendLine("ConfirmationMessage = \"" + _GlobalResources.IfYouLeaveModuleBeforeSavingStatusWillNotBeSaved + "\";");

            csm.RegisterClientScriptBlock(typeof(TinCan), "JsVariables", jsVariables.ToString(), true);
        }
        #endregion

        #region OnInit
        /// <summary>
        /// Page iniitalization.
        /// </summary>
        /// <param name="e">Event arguments</param>
        protected override void OnInit(EventArgs e)
        {
            // add control bundle to tooklitscriptmanager
            ControlBundle masterBundle = new ControlBundle();
            masterBundle.Name = "MasterBundle";
            Asm.ControlBundles.Add(masterBundle);

            // call the base
            base.OnInit(e);
        }
        #endregion
        #endregion

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // if the user isn't logged in, bounce the user
            if (AsentiaSessionState.IdSiteUser == 0)
            { Response.Redirect("/"); }

            // get portal title in language for "browser title"
            string portalTitleInInterfaceLanguage = AsentiaSessionState.GlobalSiteObject.Title;
            this.Page.Title = portalTitleInInterfaceLanguage;

            // get the querystring parameters
            this._IdDataLesson = Convert.ToInt32(Request.QueryString["id"]);
            this._IdContentPackage = Convert.ToInt32(Request.QueryString["cpid"]);
            this._LaunchType = HttpContext.Current.Request.QueryString["launchType"];

            // validate the launch type - bounce the user out of here if it isn't right
            if (this._LaunchType != "learner" && this._LaunchType != "ojt" && this._LaunchType != "ojtWidget")
            { Response.Redirect("/"); }

            // instansiate data lesson and content package objects, and get the user's id this enrollment is for
            this._DataLessonObject = new DataLesson(this._IdDataLesson);
            this._ContentPackageObject = new ContentPackage(this._IdContentPackage);

            // note, we obtain the enrolled user's id from the enrollment object because the launching user might not be
            // the user the enrollment belongs to because of ojt
            Enrollment enrollmentObject = new Enrollment(this._DataLessonObject.IdEnrollment);

            this._IdEnrolledUser = enrollmentObject.IdUser;

            // initialize sco data in the database
            DataLesson.InitializeSCOData(this._IdDataLesson);

            // if this a learner launch, validate that the enrollment belongs to the launching user - bounce them if not
            if (this._LaunchType == "learner" && AsentiaSessionState.IdSiteUser != this._IdEnrolledUser)
            { Response.Redirect("/"); }

            // set the session state parameter for currently launched lesson data
            AsentiaSessionState.CurrentlyLaunchedDataLessonId = this._IdDataLesson;

            // include page specific and site specific css class
            this._IncludeCSSFiles();

            // build the launcher
            this._GetActivityIdAndLaunchPage();

            // build lesson container
            this._BuildLessonContainer();

            // build launcher Coantainer
            this._BuildContentHeaderAndNavigationContainer();
        }
        #endregion

        #region _IncludeCSSFiles
        /// <summary>
        /// Method to include all css files.
        /// </summary>
        private void _IncludeCSSFiles()
        {
            // tag format for the stylesheet link
            const string STYLE_SHEET_LINK_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}?ver={1}\" />\r\n";

            // style sheets that aren't site configurable - located in the root css folder
            ArrayList notSiteConfigurableStyleSheets = new ArrayList();

            // add stylesheets
            notSiteConfigurableStyleSheets.Add("Reset.css");

            // stylesheets that are site configurable - located in css folders under _config
            // defaults will always be loaded first, followed by client configured ones
            ArrayList defaultSiteConfigurableStyleSheets = new ArrayList();

            // add stylesheets
            defaultSiteConfigurableStyleSheets.Add("page-specific/launch/TinCan.css");
            defaultSiteConfigurableStyleSheets.Add("Layout.css");
            defaultSiteConfigurableStyleSheets.Add("Modal.css");

            // attach references to "not site configurable" stylesheets
            foreach (string stylesheet in notSiteConfigurableStyleSheets)
            {
                string pathToStylesheet = MapPathSecure(SitePathConstants.CSS + stylesheet);

                if (File.Exists(pathToStylesheet))
                {
                    this.Page.Header.Controls.Add(
                        new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                    );
                }
            }

            // attach references to "default site configurable" stylesheets
            if (AsentiaSessionState.IsThemePreviewMode)
            {
                foreach (string stylesheet in defaultSiteConfigurableStyleSheets)
                {
                    string pathToStylesheet = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet);

                    if (File.Exists(pathToStylesheet))
                    {
                        this.Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
            }
            else
            {
                foreach (string stylesheet in defaultSiteConfigurableStyleSheets)
                {
                    string pathToStylesheet = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet);

                    if (File.Exists(pathToStylesheet))
                    {
                        this.Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
            }

            // style sheets that are site configurable but not of default site
            ArrayList siteConfigurableStyleSheets = new ArrayList();
            siteConfigurableStyleSheets.Add("page-specific/launch/TinCan.css");
            siteConfigurableStyleSheets.Add("Layout.css");
            siteConfigurableStyleSheets.Add("Modal.css");

            // attach references to  site configurable stylesheets
            if (AsentiaSessionState.IsThemePreviewMode)
            {
                foreach (string stylesheet in siteConfigurableStyleSheets)
                {
                    string pathToStylesheet = MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet);

                    if (File.Exists(pathToStylesheet))
                    {
                        this.Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
            }
            else
            {
                foreach (string stylesheet in siteConfigurableStyleSheets)
                {
                    string pathToStylesheet = MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet);

                    if (File.Exists(pathToStylesheet))
                    {
                        this.Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
            }
        }
        #endregion

        #region _GetActivityIdAndLaunchPage
        /// <summary>
        /// Get Activity ID value by parsing the package manifest file
        /// Use first id value.
        /// Launch page will be the page which will be launched by this launcher page.
        /// </summary>
        /// <returns></returns>
        private void _GetActivityIdAndLaunchPage()
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(this._ContentPackageObject.Manifest);

                if ((ContentPackage.ContentPackageType)this._ContentPackageObject.IdContentPackageType == ContentPackage.ContentPackageType.xAPI)
                {
                    XmlNode node = xmlDoc.GetElementsByTagName("activity")[0];

                    if (node.Attributes[0].Name == "id")
                    {
                        this._ActivityID = node.Attributes[0].Value;
                    }

                    for (int i = 0; i < node.ChildNodes.Count; i++)
                    {
                        if (node.ChildNodes[i].Name == "launch")
                        {
                            this._LaunchPage = node.ChildNodes[i].InnerText;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // display the failure message
                throw (ex);
            }
        }
        #endregion
        
        #region _BuildLessonContainer
        /// <summary>
        ///  Builds lesson container.
        /// </summary>
        private void _BuildLessonContainer()
        {
            HtmlIframe lessonFrame = new HtmlIframe();
            lessonFrame.ID = "LessonFrame";
            lessonFrame.Src = "../launch/ContentPlaceHolder.html?val=" + this._DataLessonObject.Title + "&msg=" + _GlobalResources.LoadingContent + "&imgUrl=" + ImageFiles.GetIconPath(ImageFiles.ICON_LOADING,
                                                   ImageFiles.EXT_GIF);
            this.LessonContentFrameContainer.Controls.Add(lessonFrame);
        }
        #endregion

        #region _TincanPropertiesSaveButton_Command
        /// <summary>
        /// Handles the Command event of the _TincanPropertiesSaveButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
        private void _TincanPropertiesSaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                Asentia.LMS.Library.TinCan._UpdateLessonStatus(this._IdDataLesson, this._LaunchType, this._ActivityID);
            }
            catch (AsentiaException ex)
            {
                Response.Redirect("/dashboard");
            }

            if (this._DataLessonObject.IdEnrollment > 0)
            {
                if (this._LaunchType == "learner")
                {
                    Response.Redirect("/dashboard/Enrollment.aspx?idEnrollment=" + this._DataLessonObject.IdEnrollment);
                }
                else if (this._LaunchType == "ojt")
                {
                    Enrollment enrollmentObject = new Enrollment(this._DataLessonObject.IdEnrollment);
                    Response.Redirect("/administrator/users/enrollments/Activity.aspx?uid=" + enrollmentObject.IdUser + "&id=" + enrollmentObject.Id);
                }
                else if (this._LaunchType == "ojtWidget")
                {
                    Response.Redirect("/dashboard");
                }
                else
                {
                    Response.Redirect("/");
                }
            }
        }
        #endregion

        #region _BuildContentHeaderAndNavigationContainer
        /// <summary>
        /// Builds the xAPI content header and navigation containers.
        /// </summary>
        private void _BuildContentHeaderAndNavigationContainer()
        {
            // content menu toggle link
            HyperLink contentMenuToggle = new HyperLink();
            contentMenuToggle.ID = "ContentMenuToggle";

            // create span to add inside the toggle menu area
            HtmlGenericControl span1 = new HtmlGenericControl("span");
            HtmlGenericControl span2 = new HtmlGenericControl("span");
            HtmlGenericControl span3 = new HtmlGenericControl("span");

            //add spans to menu icon
            contentMenuToggle.Controls.Add(span1);
            contentMenuToggle.Controls.Add(span2);
            contentMenuToggle.Controls.Add(span3);

            // get the manifest so we can retrieve the title and identifier
            string manifestXML = this._ContentPackageObject.Manifest;

            // get the title and identifier from the manifest
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(manifestXML);
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("organization");

            foreach (XmlNode xmlNode in nodeList)
            {
                this._PackageOrganizationIdentifier = xmlNode.Attributes["identifier"].Value;
            }

            // get the lesson name to use as the _PackageName in the title
            Lesson lessonObject = new Lesson(this._DataLessonObject.IdLesson);

            this._PackageName = lessonObject.Title; // set the default title in case we don't find a language-specific one

            foreach (Lesson.LanguageSpecificProperty lessonLSP in lessonObject.LanguageSpecificProperties)
            {
                if (lessonLSP.LangString == AsentiaSessionState.UserCulture)
                { this._PackageName = lessonLSP.Title; }
            }

            // package's over all status indicator image
            Image contentCompletionStatusImage = new Image();
            contentCompletionStatusImage.ID = "img_status_" + this._PackageOrganizationIdentifier; ;
            contentCompletionStatusImage.CssClass = "ContentCompletionStatusImage";
            contentCompletionStatusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);

            // package's title
            Label packageTitle = new Label();
            packageTitle.ID = "ContentTitle";
            packageTitle.Text = this._PackageName;

            // add controls to main panel on header 
            this.ContentHeaderAndNavigationContainer.Controls.Add(contentMenuToggle);
            this.ContentHeaderAndNavigationContainer.Controls.Add(contentCompletionStatusImage);
            this.ContentHeaderAndNavigationContainer.Controls.Add(packageTitle);

            // sub panel for navigation buttons
            Panel contentNavigationButtonsContainer = new Panel();
            contentNavigationButtonsContainer.ID = "ContentNavigationButtonsContainer";

            // exit navigation button
            this._ExitActionButton = new ImageButton();
            this._ExitActionButton.ID = "ExitActionButton";

            // assign title to the navigation button
            this._ExitActionButton.ToolTip = _GlobalResources.Exit;

            // set image url properties of naviagation button
            this._ExitActionButton.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE, ImageFiles.EXT_PNG);

            // set css class of slide and navigation button
            this._ExitActionButton.CssClass = "ContentNavigationButton";

            // set onClientClick property of navigation button
            this._ExitActionButton.OnClientClick = "javascript:RemoveBeforeUnloadEvent();";
            this._ExitActionButton.Command += new CommandEventHandler(this._TincanPropertiesSaveButton_Command);

            // add controls to panel
            contentNavigationButtonsContainer.Controls.Add(this._ExitActionButton);

            // add sub panel to main header panel
            this.ContentHeaderAndNavigationContainer.Controls.Add(contentNavigationButtonsContainer);
        }
        #endregion
    }
}
