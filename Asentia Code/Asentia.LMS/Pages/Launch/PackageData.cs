﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asentia.LMS.Pages.Launch
{
    public class PackageData
    {
        public int ManifestID
        {
            get;
            set;
        }
        public string ContentFolder { get; set; }
        public string PersistedDataFileName { get; set; }
        public string LearnerName { get; set; }
        public string LearnerId { get; set; }
        public string EnrollmentIdentifier { get; set; }
    }
}