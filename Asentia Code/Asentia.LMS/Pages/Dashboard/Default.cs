﻿using System;
using System.Collections;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using System.Web.UI;
using System.Text;

namespace Asentia.LMS.Pages.Dashboard
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Private Properties
        private EcommerceSettings _EcommerceSettings;
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard));
            this.BuildBreadcrumb(breadCrumbLinks);

            // include page-specific css files
            this.IncludePageSpecificCssFile("Wall.css");
            this.IncludePageSpecificCssFile("page-specific/Dashboard/DashboardWidget.css");
            this.IncludePageSpecificCssFile("page-specific/Dashboard/Default.css");
            this.IncludePageSpecificCssFile("Certificate.css");

            // DO NOT BUILD A PAGE TITLE FOR THIS

            // initialize the administrator menu
            this.InitializeAdminMenu();

            // load and attach the dashboard
            this._LoadDashboard();
        }
        #endregion

        #region _LoadDashboard
        /// <summary>
        /// Loads the widget dashboard.
        /// </summary>
        private void _LoadDashboard()
        {            
            // load the dashboard
            DashboardWidgetContainer dashboardWidgets = new DashboardWidgetContainer();

            dashboardWidgets.ID = (AsentiaSessionState.UserDevice == AsentiaSessionState.UserDeviceType.Desktop) ? "DashboardWidgets" : "MobileDashboardWidgets";
            this.PageContentContainer.Controls.Add(dashboardWidgets);
        }
        #endregion
    }
}