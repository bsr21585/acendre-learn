﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Dashboard
{
    public class Enrollment : AsentiaAuthenticatedPage
    {
        #region Private Properties
        private Library.Enrollment _EnrollmentObject;
        private Library.Course _CourseObject;
        private bool _ReferredFromContentLauncher = false;
        private bool _LessonDataResetDueToContentChange;

        // enroll in standup training modal
        private ModalPopup _EnrollInStandupTrainingModal;
        private Button _EnrollInStandupTrainingModalHiddenLaunchButton;
        private Button _EnrollInStandupTrainingModalHiddenLoadButton;
        private Panel _EnrollInStandupTrainingDetailsPanel;
        private HiddenField _EnrollInStandupTrainingData;
        private HiddenField _EnrollInStandupTrainingSelectedStandupTrainingInstanceData;

        // view enrolled standup training modal
        private ModalPopup _ViewEnrolledStandupTrainingModal;
        private Button _ViewEnrolledStandupTrainingModalHiddenLaunchButton;
        private Button _ViewEnrolledStandupTrainingModalHiddenLoadButton;
        private Panel _EnrolledStandupTrainingSessionInformationPanel;
        private HiddenField _EnrolledStandupTrainingData;

        // remove enrolled standup training modal - uses data from _EnrolledStandupTrainingData hidden field
        private ModalPopup _RemoveEnrolledStandupTrainingModal;
        private Button _RemoveEnrolledStandupTrainingModalHiddenLaunchButton;
        private Button _RemoveEnrolledStandupTrainingModalHiddenLoadButton;
        private Panel _RemoveEnrolledStandupTrainingSessionConfirmationPanel;

        // task modal
        private ModalPopup _TaskModal;
        private Button _TaskModalLaunchButton;
        private Button _TaskModalLoadButton;
        private Panel _TaskModalFormPanel;
        private Panel _TaskModalUploaderPanel;
        private UploaderAsync _TaskUploader;
        private HiddenField _TaskData;
        private HiddenField _TaskRemoveUploadedFile;

        // ojt modal
        private ModalPopup _OJTModal;
        private Button _OJTCancelRequestButton;
        private Button _OJTModalLaunchButton;
        private Button _OJTModalLoadButton;
        private Panel _OJTModalFormPanel;
        private HiddenField _OJTData;

        private HiddenField _ICalendarContent = new HiddenField();
        #endregion

        #region Public Properties
        public Panel EnrollmentFormContentWrapperContainer;
        public Panel CourseInformationContainer;
        public Grid CourseLessonsGrid;
        public Panel EnrollmentInformationWrapperContainer;
        public UpdatePanel CourseLessonsGridUpdatePanel = new UpdatePanel();
        public Button _HiddenDownloadCalendarFileButton = new Button();
        #endregion

        #region Page_Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("UserRating.css");
            this.IncludePageSpecificCssFile("page-specific/Dashboard/Enrollment.css");

            // get the enrollment object, which will also synchronize any lesson data
            this._GetEnrollmentObject();

            // process content change flags on lesson data
            this._LessonDataResetDueToContentChange = Library.Enrollment.ProcessContentChangeLessonDataFlags(this._EnrollmentObject.IdUser, this._EnrollmentObject.Id);

            // check for unsynchronized lesson data when returning from SCO
            this._CheckForReturnSCOUnsynchronizedData();

            // check and fill in lesson data completions for standup training, only if the enrollment has not been completed
            if (this._EnrollmentObject.DtCompleted == null)
            { this._EnrollmentObject.SetLessonDataCompletionForStandupTraining(); }

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.EnrollmentFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.EnrollmentInformationWrapperContainer.CssClass = "FormContentContainer";

            // build page controls
            this._BuildControls();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.CourseLessonsGrid.BindData();

                // if this is a single lesson course and that course's only content delivery method is a content package, and the learner
                // did not just return from content launch, automatically redirect them to the content
                /*
                Grid lessonGrid = (Grid)PageContentContainer.FindControl("CourseLessonsGrid");

                if (lessonGrid != null && lessonGrid.Rows.Count == 1 && !this._ReferredFromContentLauncher)
                {
                    string idDataLessonQS = lessonGrid.DataKeys[0][0].ToString();
                    string idContentPackageQS = lessonGrid.DataKeys[0][1].ToString();

                    Response.Redirect("/launch/Scorm.aspx?id=" + idDataLessonQS + "&cpid=" + idContentPackageQS + "&launchType=learner");
                }
                */
            }
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Enrollment), "Asentia.LMS.Pages.Dashboard.Enrollment.js");

            // register global js variables for modal popup control ids
            StringBuilder jsVariables = new StringBuilder();

            jsVariables.AppendLine("var EnrollInStandupTrainingModalHiddenLaunchButtonId = \"" + this._EnrollInStandupTrainingModalHiddenLaunchButton.ID + "\";");
            jsVariables.AppendLine("var EnrollInStandupTrainingModalHiddenLoadButtonId = \"" + this._EnrollInStandupTrainingModalHiddenLoadButton.ID + "\";");
            jsVariables.AppendLine("var EnrollInStandupTrainingDataId = \"" + this._EnrollInStandupTrainingData.ID + "\";");
            jsVariables.AppendLine("var EnrollInStandupTrainingSelectedStandupTrainingInstanceDataId = \"" + this._EnrollInStandupTrainingSelectedStandupTrainingInstanceData.ID + "\";");
            jsVariables.AppendLine("var ViewEnrolledStandupTrainingModalHiddenLaunchButtonId = \"" + this._ViewEnrolledStandupTrainingModalHiddenLaunchButton.ID + "\";");
            jsVariables.AppendLine("var ViewEnrolledStandupTrainingModalHiddenLoadButtonId = \"" + this._ViewEnrolledStandupTrainingModalHiddenLoadButton.ID + "\";");
            jsVariables.AppendLine("var RemoveEnrolledStandupTrainingModalHiddenLaunchButtonId = \"" + this._RemoveEnrolledStandupTrainingModalHiddenLaunchButton.ID + "\";");
            jsVariables.AppendLine("var RemoveEnrolledStandupTrainingModalHiddenLoadButtonId = \"" + this._RemoveEnrolledStandupTrainingModalHiddenLoadButton.ID + "\";");
            jsVariables.AppendLine("var EnrolledStandupTrainingDataId = \"" + this._EnrolledStandupTrainingData.ID + "\";");
            jsVariables.AppendLine("var TaskModalLaunchButtonId = \"" + this._TaskModalLaunchButton.ID + "\";");
            jsVariables.AppendLine("var TaskModalLoadButtonId = \"" + this._TaskModalLoadButton.ID + "\";");
            jsVariables.AppendLine("var TaskDataId = \"" + this._TaskData.ID + "\";");
            jsVariables.AppendLine("var TaskRemoveUploadedFile = \"" + this._TaskRemoveUploadedFile.ID + "\";");
            jsVariables.AppendLine("var RemoveImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            jsVariables.AppendLine("var OJTModalLaunchButtonId = \"" + this._OJTModalLaunchButton.ID + "\";");
            jsVariables.AppendLine("var OJTModalLoadButtonId = \"" + this._OJTModalLoadButton.ID + "\";");
            jsVariables.AppendLine("var OJTDataId = \"" + this._OJTData.ID + "\";");

            csm.RegisterStartupScript(typeof(Enrollment), "JsVariables", jsVariables.ToString(), true);
        }
        #endregion

        #region _GetEnrollmentObject
        /// <summary>
        /// Gets a Enrollment object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetEnrollmentObject()
        {
            // get the id querystring parameter
            int idEnrollment = this.QueryStringInt("idEnrollment", 0);            

            try
            {                
                if (idEnrollment > 0)
                {
                    this._EnrollmentObject = new Library.Enrollment(idEnrollment);
                    this._CourseObject = new Library.Course(this._EnrollmentObject.IdCourse);

                    // if the course is locked, get out of here
                    if ((bool)this._CourseObject.IsLocked)
                    { Response.Redirect("~/dashboard"); }
                }
            }
            catch
            {
                Response.Redirect("~/dashboard");
            }
        }
        #endregion

        #region _CheckForReturnSCOUnsynchronizedData
        /// <summary>
        /// If we have returned back to this page after exiting a SCO (rsdid QS param will be populated), check
        /// to see if the data is unsynchronized; and if it is, synchronize it.
        /// 
        /// This is used as a safeguard against data not being saved to the database.
        /// </summary>
        private void _CheckForReturnSCOUnsynchronizedData()
        {
            // get the sco data id from the querystring
            int idDataSCO = this.QueryStringInt("rsdid", 0);

            // if we dont have a SCO data id, return
            if (idDataSCO == 0)
            { return; }

            // if we get here, we know that we were referred back from the content launcher
            this._ReferredFromContentLauncher = true;

            // get the sco data object, wrap in try/catch and die quietly on error
            try
            {
                DataSCO scoDataObject = new DataSCO(idDataSCO);

                // if the SCO data is already completed, there is nothing to do, return
                if ((Lesson.LessonCompletionStatus)scoDataObject.CompletionStatus == Lesson.LessonCompletionStatus.Completed)
                { return; }

                // attempt to get the global_data file for the SCO data
                string folderPath = Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + AsentiaSessionState.IdSiteUser.ToString() + "/" + scoDataObject.IdDataLesson.ToString());
                string filePath = Path.Combine(folderPath, "global_data.json");

                // if there is no file, nothing to do, return
                if (!File.Exists(filePath))
                { return; }

                // read and deserialize the data from the file
                string fileContent = File.ReadAllText(filePath);
                string completionStatusFromFile;
                string successStatusFromFile;
                string scoreFromFile;
                double totalTimeFromFile;
                int actualAttemptCountFromFile;
                int effectiveAttemptCountFromFile;

                if (fileContent.Contains("\"globalObjectives\":")) // globalObjectives are SCORM 2004, so if they exist in the file, it is SCORM 2004 data model
                {
                    CmiDataModel scorm2004Cmi = new CmiDataModel();
                    scorm2004Cmi = JsonHelper.JsonDeserialize<CmiDataModel>(fileContent);

                    completionStatusFromFile = scorm2004Cmi.completion_status;
                    successStatusFromFile = scorm2004Cmi.success_status;
                    scoreFromFile = scorm2004Cmi.score.scaled;
                    totalTimeFromFile = scorm2004Cmi.total_time;
                    actualAttemptCountFromFile = scorm2004Cmi.actualAttemptCount;
                    effectiveAttemptCountFromFile = scorm2004Cmi.effectiveAttemptCount;
                }
                else // SCORM 1.2
                {
                    Asentia.LMS.Pages._Util.CmiDataModelScorm12 scorm12Cmi = new Asentia.LMS.Pages._Util.CmiDataModelScorm12();
                    scorm12Cmi = JsonHelper.JsonDeserialize<Asentia.LMS.Pages._Util.CmiDataModelScorm12>(fileContent);

                    completionStatusFromFile = scorm12Cmi.completion_status;
                    successStatusFromFile = scorm12Cmi.success_status;
                    scoreFromFile = scorm12Cmi.score.raw;
                    totalTimeFromFile = scorm12Cmi.total_time;
                    actualAttemptCountFromFile = 0;
                    effectiveAttemptCountFromFile = 0;
                }

                // now, compare completion and success from file to completion and success from database, if any difference save the values from file to database
                if (
                    scoDataObject.CompletionStatus != (int)((Lesson.ScormCompletionStatus)Enum.Parse(typeof(Lesson.ScormCompletionStatus), completionStatusFromFile))
                    || scoDataObject.SuccessStatus != (int)((Lesson.ScormSuccessStatus)Enum.Parse(typeof(Lesson.ScormSuccessStatus), successStatusFromFile))
                   )
                {
                    scoDataObject.CompletionStatus = (int)((Lesson.ScormCompletionStatus)Enum.Parse(typeof(Lesson.ScormCompletionStatus), completionStatusFromFile));
                    scoDataObject.SuccessStatus = (int)((Lesson.ScormSuccessStatus)Enum.Parse(typeof(Lesson.ScormSuccessStatus), successStatusFromFile));
                    
                    if (!String.IsNullOrWhiteSpace(scoreFromFile))
                    { scoDataObject.ScoreScaled = float.Parse(scoreFromFile); }
                    else
                    { scoDataObject.ScoreScaled = null; }

                    float totalTime = !String.IsNullOrWhiteSpace(totalTimeFromFile.ToString()) ? (float)totalTimeFromFile : 0;

                    if (float.IsPositiveInfinity(totalTime))
                    { totalTime = float.MaxValue; }
                    else if (float.IsNegativeInfinity(totalTime))
                    { totalTime = float.MinValue; }

                    scoDataObject.TotalTime = totalTime;
                    scoDataObject.ActualAttemptCount = actualAttemptCountFromFile;
                    scoDataObject.EffectiveAttemptCount = effectiveAttemptCountFromFile;

                    scoDataObject.Save();
                }
            }
            catch
            {
                return;
            }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string courseImagePath;
            string imageCssClass = null;
            string pageTitle;

            string courseCode = this._CourseObject.CourseCode;
            string courseTitleInInterfaceLanguage = this._CourseObject.Title;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (Course.LanguageSpecificProperty courseLanguageSpecificProperty in this._CourseObject.LanguageSpecificProperties)
                {
                    if (courseLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { courseTitleInInterfaceLanguage = courseLanguageSpecificProperty.Title; }
                }
            }

            // attach course code if not hidden and there is a course code
            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_HIDE_COURSE_CODES) != true && !String.IsNullOrWhiteSpace(courseCode))
            { courseTitleInInterfaceLanguage = courseCode + ": " + courseTitleInInterfaceLanguage; }

            breadCrumbPageTitle = courseTitleInInterfaceLanguage;

            if (this._CourseObject.Avatar != null)
            {
                courseImagePath = SitePathConstants.SITE_COURSES_ROOT + this._CourseObject.Id + "/" + this._CourseObject.Avatar + "?" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                imageCssClass = "AvatarImage";
            }
            else
            {
                courseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT,
                                                         ImageFiles.EXT_PNG);
            }

            pageTitle = courseTitleInInterfaceLanguage;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, pageTitle, courseImagePath, imageCssClass);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        ///  Builds the page controls related to enrollment
        /// </summary>
        private void _BuildControls()
        {
            // if the enrollment has had some lesson data reset due to content changes, display that message
            if (this._LessonDataResetDueToContentChange)
            {
                Panel lessonDataResetWarningPanel = new Panel();
                lessonDataResetWarningPanel.ID = "EnrollmentLessonDataResetWarning";
                this.FormatPageInformationPanel(lessonDataResetWarningPanel, _GlobalResources.SomeOfTheModuleContentForThisCourseHasChangedSinceTheLastTimeYouHaveAccessedThisCourseEnrollment, true, ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG));
                this.EnrollmentInformationWrapperContainer.Controls.AddAt(0, lessonDataResetWarningPanel);
            }

            // instansiate enroll in standup training controls
            this._EnrollInStandupTrainingModalHiddenLaunchButton = new Button();
            this._EnrollInStandupTrainingModalHiddenLaunchButton.ID = "EnrollInStandupTrainingModalHiddenLaunchButton";
            this._EnrollInStandupTrainingModalHiddenLaunchButton.Style.Add("display", "none");

            this._EnrollInStandupTrainingModalHiddenLoadButton = new Button();
            this._EnrollInStandupTrainingModalHiddenLoadButton.ID = "EnrollInStandupTrainingModalHiddenLoadButton";
            this._EnrollInStandupTrainingModalHiddenLoadButton.Style.Add("display", "none");
            this._EnrollInStandupTrainingModalHiddenLoadButton.Click += this._LoadEnrollInStandupTrainingModalContent;

            this._EnrollInStandupTrainingDetailsPanel = new Panel();
            this._EnrollInStandupTrainingDetailsPanel.ID = "EnrollInStandupTrainingDetailsPanel";

            this._EnrollInStandupTrainingData = new HiddenField();
            this._EnrollInStandupTrainingData.ID = "EnrollInStandupTrainingData";

            this._EnrollInStandupTrainingSelectedStandupTrainingInstanceData = new HiddenField();
            this._EnrollInStandupTrainingSelectedStandupTrainingInstanceData.ID = "EnrollInStandupTrainingSelectedStandupTrainingInstanceData";

            // instansiate view enrolled standup training controls
            this._ViewEnrolledStandupTrainingModalHiddenLaunchButton = new Button();
            this._ViewEnrolledStandupTrainingModalHiddenLaunchButton.ID = "ViewEnrolledStandupTrainingModalHiddenLaunchButton";
            this._ViewEnrolledStandupTrainingModalHiddenLaunchButton.Style.Add("display", "none");

            this._ViewEnrolledStandupTrainingModalHiddenLoadButton = new Button();
            this._ViewEnrolledStandupTrainingModalHiddenLoadButton.ID = "ViewEnrolledStandupTrainingModalHiddenLoadButton";
            this._ViewEnrolledStandupTrainingModalHiddenLoadButton.Style.Add("display", "none");
            this._ViewEnrolledStandupTrainingModalHiddenLoadButton.Click += this._LoadViewEnrolledStandupTrainingModalContent;

            this._EnrolledStandupTrainingSessionInformationPanel = new Panel();
            this._EnrolledStandupTrainingSessionInformationPanel.ID = "EnrolledStandupTrainingSessionInformationPanel";

            // instansiate remove enrolled standup training controls
            this._RemoveEnrolledStandupTrainingModalHiddenLaunchButton = new Button();
            this._RemoveEnrolledStandupTrainingModalHiddenLaunchButton.ID = "RemoveEnrolledStandupTrainingModalHiddenLaunchButton";
            this._RemoveEnrolledStandupTrainingModalHiddenLaunchButton.Style.Add("display", "none");

            this._RemoveEnrolledStandupTrainingModalHiddenLoadButton = new Button();
            this._RemoveEnrolledStandupTrainingModalHiddenLoadButton.ID = "RemoveEnrolledStandupTrainingModalHiddenLoadButton";
            this._RemoveEnrolledStandupTrainingModalHiddenLoadButton.Style.Add("display", "none");
            this._RemoveEnrolledStandupTrainingModalHiddenLoadButton.Click += this._LoadRemoveEnrolledStandupTrainingModalContent;

            this._RemoveEnrolledStandupTrainingSessionConfirmationPanel = new Panel();
            this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.ID = "RemoveEnrolledStandupTrainingSessionConfirmationPanel";

            // enrolled standup training data hidden field is used by both the view and remove controls
            this._EnrolledStandupTrainingData = new HiddenField();
            this._EnrolledStandupTrainingData.ID = "EnrolledStandupTrainingData";

            // instansiate task controls
            this._TaskModalLaunchButton = new Button();
            this._TaskModalLaunchButton.ID = "TaskModalLaunchButton";
            this._TaskModalLaunchButton.Style.Add("display", "none");

            this._TaskModalLoadButton = new Button();
            this._TaskModalLoadButton.ID = "TaskModalLoadButton";
            this._TaskModalLoadButton.Style.Add("display", "none");
            this._TaskModalLoadButton.Click += this._LoadTaskModalContent;

            this._TaskModalFormPanel = new Panel();
            this._TaskModalFormPanel.ID = "TaskModalFormPanel";

            this._TaskModalUploaderPanel = new Panel();
            this._TaskModalUploaderPanel.ID = "TaskModalUploaderPanel";

            this._TaskUploader = new UploaderAsync("TaskUploader_Field", UploadType.Task, "TaskUploader_ErrorContainer");
            this._TaskUploader.ClientSideCompleteJSMethod = "TaskUploaded";

            this._TaskData = new HiddenField();
            this._TaskData.ID = "TaskData";

            this._TaskRemoveUploadedFile = new HiddenField();
            this._TaskRemoveUploadedFile.ID = "TaskRemoveUploadedFile";

            // instansiate ojt controls
            this._OJTModalLaunchButton = new Button();
            this._OJTModalLaunchButton.ID = "OJTModalLaunchButton";
            this._OJTModalLaunchButton.Style.Add("display", "none");

            this._OJTModalLoadButton = new Button();
            this._OJTModalLoadButton.ID = "OJTModalLoadButton";
            this._OJTModalLoadButton.Style.Add("display", "none");
            this._OJTModalLoadButton.Click += this._LoadOJTModalContent;

            this._OJTModalFormPanel = new Panel();
            this._OJTModalFormPanel.ID = "OJTModalFormPanel";

            this._OJTData = new HiddenField();
            this._OJTData.ID = "OJTData";

            // hidden button to download 
            this._HiddenDownloadCalendarFileButton.ID = "HiddenDownloadCalendarFileButton";
            this._HiddenDownloadCalendarFileButton.ClientIDMode = ClientIDMode.Static;
            this._HiddenDownloadCalendarFileButton.Style.Add("display", "none");
            this._HiddenDownloadCalendarFileButton.Command += this._DownloadCalendarFile;

            // attach necessary global elements to page
            this.CourseInformationContainer.Controls.Add(this._EnrollInStandupTrainingModalHiddenLaunchButton);
            this.CourseInformationContainer.Controls.Add(this._ViewEnrolledStandupTrainingModalHiddenLaunchButton);
            this.CourseInformationContainer.Controls.Add(this._RemoveEnrolledStandupTrainingModalHiddenLaunchButton);
            this.CourseInformationContainer.Controls.Add(this._TaskModalLaunchButton);
            this.CourseInformationContainer.Controls.Add(this._OJTModalLaunchButton);
            this.CourseInformationContainer.Controls.Add(this._HiddenDownloadCalendarFileButton);
            this.CourseInformationContainer.Controls.Add(this._ICalendarContent);

            // build lesson listing grid
            this._BuildGrid();

            // build left side course details panel
            this._BuildCourseDetailsContainer();

            // build modal popup panels
            this._BuildEnrollInStandupTrainingInstanceModal();
            this._BuildViewEnrolledStandupTrainingModal();
            this._BuildRemoveEnrolledStandupTrainingModal();
            this._BuildTaskModal();
            this._BuildOJTModal();
        }
        #endregion

        #region _BuildCourseDetailsContainer
        /// <summary>
        /// Builds the course details container.
        /// </summary>
        private void _BuildCourseDetailsContainer()
        {
            if (this._CourseObject != null)
            {
                this.CourseInformationContainer.CssClass = "CourseInformationContainer";

                // get the course's language-specific title and description
                string title = this._CourseObject.Title;
                string shortDescription = this._CourseObject.ShortDescription;
                string description = HttpUtility.HtmlDecode(this._CourseObject.LongDescription);
                string objectives = HttpUtility.HtmlDecode(this._CourseObject.Objectives);

                foreach (Library.Course.LanguageSpecificProperty courseLanguageSpecific in this._CourseObject.LanguageSpecificProperties)
                {
                    if (courseLanguageSpecific.LangString == AsentiaSessionState.UserCulture)
                    {
                        title = courseLanguageSpecific.Title;
                        shortDescription = courseLanguageSpecific.ShortDescription;
                        description = HttpUtility.HtmlDecode(courseLanguageSpecific.LongDescription);
                        objectives = HttpUtility.HtmlDecode(courseLanguageSpecific.Objectives);
                        break;
                    }
                }

                // COURSE BASIC INFORMATION 

                Panel courseBasicInformationContainer = new Panel();
                courseBasicInformationContainer.ID = "CourseBasicInformationContainer";
                courseBasicInformationContainer.CssClass = "DetailsViewBasicInformationContainer";

                // RATINGS

                if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_ENABLE) == true)
                {
                    if (!this._CourseObject.DisallowRating)
                    {
                        Panel courseRatingsContainer = new Panel();
                        courseRatingsContainer.ID = "CourseRatingsContainer";
                        courseRatingsContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                        UserRating courseRatingControl = new UserRating("CourseRatingControl");
                        courseRatingControl.IdObject = this._CourseObject.Id;

                        if (this._CourseObject.Rating != null)
                        { courseRatingControl.UsersRating = (double)this._CourseObject.Rating; }

                        if (this._CourseObject.Votes != null)
                        { courseRatingControl.Votes = (int)this._CourseObject.Votes; }

                        courseRatingControl.YourRating = this._CourseObject.GetUsersRating(AsentiaSessionState.IdSiteUser);
                        courseRatingControl.ShowYourRating = true;
                        courseRatingControl.ShowUserRating = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.RATINGS_COURSE_PUBLICIZE);

                        if (this._EnrollmentObject.DtCompleted != null)
                        { courseRatingControl.RateUrl = "/_util/RatingService.asmx/SaveCourseRating"; }

                        courseRatingsContainer.Controls.Add(courseRatingControl);
                        courseBasicInformationContainer.Controls.Add(courseRatingsContainer);
                    }
                }

                // CREDITS, ESTIMATED TIME, AND STATUS

                Panel courseCreditsEstTimeStatusWrapperContainer = new Panel();
                courseCreditsEstTimeStatusWrapperContainer.ID = "CourseCreditsEstTimeStatusWrapperContainer";
                courseCreditsEstTimeStatusWrapperContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                // credits
                if (this._CourseObject.Credits != null)
                {
                    Panel courseCreditsContainer = new Panel();
                    courseCreditsContainer.ID = "CourseCreditsContainer";

                    Label courseCreditsLabel = new Label();
                    courseCreditsLabel.CssClass = "bold";
                    courseCreditsLabel.Text = _GlobalResources.Credits + ": ";
                    courseCreditsContainer.Controls.Add(courseCreditsLabel);

                    Label courseCreditsValueLabel = new Label();
                    courseCreditsValueLabel.Text = this._CourseObject.Credits.ToString();
                    courseCreditsContainer.Controls.Add(courseCreditsValueLabel);

                    courseCreditsEstTimeStatusWrapperContainer.Controls.Add(courseCreditsContainer);
                }

                // estimated time
                if (this._CourseObject.EstimatedLengthInMinutes != null && this._CourseObject.EstimatedLengthInMinutes > 0)
                {
                    string lengthData = null;

                    if (this._CourseObject.EstimatedLengthInMinutes < 60)
                    { lengthData = String.Format(_GlobalResources.XMinute_s, this._CourseObject.EstimatedLengthInMinutes.ToString()); }
                    else
                    {
                        int estimatedLengthHours = (int)this._CourseObject.EstimatedLengthInMinutes / 60;
                        int estimatedLengthMinutes = (int)this._CourseObject.EstimatedLengthInMinutes - (estimatedLengthHours * 60);

                        if (estimatedLengthMinutes > 0)
                        { lengthData = String.Format(_GlobalResources.XHour_sAndXMinute_s, estimatedLengthHours, estimatedLengthMinutes); }
                        else
                        { lengthData = String.Format(_GlobalResources.XHour_s, estimatedLengthHours); }
                    }

                    Panel courseEstimatedLengthContainer = new Panel();
                    courseEstimatedLengthContainer.ID = "CourseEstimatedLengthContainer";

                    Label courseEstimatedLengthLabel = new Label();
                    courseEstimatedLengthLabel.CssClass = "bold";
                    courseEstimatedLengthLabel.Text = _GlobalResources.EstimatedLength + ": ";
                    courseEstimatedLengthContainer.Controls.Add(courseEstimatedLengthLabel);

                    Label courseEstimatedLengthValueLabel = new Label();
                    courseEstimatedLengthValueLabel.Text = lengthData;
                    courseEstimatedLengthContainer.Controls.Add(courseEstimatedLengthValueLabel);

                    courseCreditsEstTimeStatusWrapperContainer.Controls.Add(courseEstimatedLengthContainer);
                }

                // status
                Panel courseStatusContainer = new Panel();
                courseStatusContainer.ID = "CourseStatusContainer";

                Label courseStatusLabel = new Label();
                courseStatusLabel.CssClass = "bold";
                courseStatusLabel.Text = _GlobalResources.Status + ": ";
                courseStatusContainer.Controls.Add(courseStatusLabel);

                Label courseStatusValueLabel = new Label();

                if (this._EnrollmentObject.DtStart > AsentiaSessionState.UtcNow) // we should never see this
                { courseStatusValueLabel.Text = _GlobalResources.Future; }
                else
                {
                    if (this._EnrollmentObject.DtCompleted != null ) // completed
                    { courseStatusValueLabel.Text = _GlobalResources.Completed; }
                    else
                    {
                        if (this._EnrollmentObject.DtDue < AsentiaSessionState.UtcNow && this._EnrollmentObject.DtEffectiveExpires > AsentiaSessionState.UtcNow) // overdue
                        { courseStatusValueLabel.Text = _GlobalResources.Overdue; }
                        else
                        {
                            if (this._EnrollmentObject.DtEffectiveExpires < AsentiaSessionState.UtcNow) // expired, should also not ever see this
                            { courseStatusValueLabel.Text = _GlobalResources.Expired; }
                            else // enrolled
                            { courseStatusValueLabel.Text = _GlobalResources.Enrolled; }
                        }
                    }
                }

                courseStatusContainer.Controls.Add(courseStatusValueLabel);

                courseCreditsEstTimeStatusWrapperContainer.Controls.Add(courseStatusContainer);

                // attach segment container to basic information container
                courseBasicInformationContainer.Controls.Add(courseCreditsEstTimeStatusWrapperContainer);

                // WALL
                if (this._CourseObject.IsFeedActive == true && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGASSETS_COURSEDISCUSSION_ENABLE))
                {
                    Panel courseWallLinkContainer = new Panel();
                    courseWallLinkContainer.ID = "CourseWallLinkContainer";
                    courseWallLinkContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                    HyperLink courseWallLink = new HyperLink();
                    courseWallLink.ID = "CourseWallLink";
                    courseWallLink.CssClass = "ImageLink";
                    courseWallLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DISCUSSION,
                                                                     ImageFiles.EXT_PNG);
                    courseWallLink.NavigateUrl = "/courses/Wall.aspx?id=" + this._CourseObject.Id.ToString();
                    courseWallLink.ToolTip = _GlobalResources.JumpToCourseDiscussion;
                    courseWallLinkContainer.Controls.Add(courseWallLink);

                    // attach wall link wrapper to basic information container
                    courseBasicInformationContainer.Controls.Add(courseWallLinkContainer);
                }

                // SOCIAL MEDIA
                
                if (this._CourseObject.SocialMediaProperties.Count > 0)
                {
                    Panel courseSocialMediaLinksContainer = new Panel();
                    courseSocialMediaLinksContainer.ID = "CourseSocialMediaLinksContainer";
                    courseSocialMediaLinksContainer.CssClass = "DetailsViewInlineInformationWrapperContainer";

                    int socialMediaItemsCount = 0;

                    foreach (Course.SocialMediaProperty socialMediaProperty in this._CourseObject.SocialMediaProperties)
                    {
                        socialMediaItemsCount++;

                        HyperLink socialMediaLink = new HyperLink();
                        socialMediaLink.ID = "SocialMediaLink_" + socialMediaItemsCount.ToString();
                        socialMediaLink.CssClass = "ImageLink";
                        socialMediaLink.NavigateUrl = socialMediaProperty.Protocol + socialMediaProperty.Href;
                        socialMediaLink.Target = "_blank";
                        socialMediaLink.ToolTip = socialMediaProperty.Protocol + socialMediaProperty.Href;

                        switch (socialMediaProperty.IconType)
                        {
                            case "facebook":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FACEBOOK,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            case "linkedin":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LINKEDIN,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            case "twitter":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_TWITTER,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            case "google":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOOGLE,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            case "youtube":
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_YOUTUBE,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                            default:
                                socialMediaLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CUSTOMFORUM,
                                                                                  ImageFiles.EXT_PNG);
                                break;
                        }

                        courseSocialMediaLinksContainer.Controls.Add(socialMediaLink);
                    }

                    // attach social media links wrapper to basic information container
                    courseBasicInformationContainer.Controls.Add(courseSocialMediaLinksContainer);
                }

                // attach basic information container to course information container
                this.CourseInformationContainer.Controls.Add(courseBasicInformationContainer);

                // DESCRIPTION

                Panel courseDescriptionContainer = new Panel();
                courseDescriptionContainer.ID = "CourseDescriptionContainer";
                courseDescriptionContainer.CssClass = "DetailsViewSectionContainer";

                // label
                Panel courseDescriptionLabelContainer = new Panel();
                courseDescriptionLabelContainer.ID = "CourseDescriptionLabelContainer";
                courseDescriptionLabelContainer.CssClass = "DetailsViewSectionLabel";
                courseDescriptionContainer.Controls.Add(courseDescriptionLabelContainer);

                Literal courseDescriptionLabel = new Literal();
                courseDescriptionLabel.Text = _GlobalResources.Description;
                courseDescriptionLabelContainer.Controls.Add(courseDescriptionLabel);

                // description
                Panel courseDescriptionContentContainer = new Panel();
                courseDescriptionContentContainer.ID = "CourseDescriptionContentContainer";
                courseDescriptionContentContainer.CssClass = "DetailsViewSectionContentContainer";
                courseDescriptionContainer.Controls.Add(courseDescriptionContentContainer);

                Label courseDescription = new Label();
                courseDescription.ID = "CourseDescription";

                if (!String.IsNullOrWhiteSpace(description))
                { courseDescription.Text = description; }
                else
                { courseDescription.Text = shortDescription; }

                courseDescriptionContentContainer.Controls.Add(courseDescription);

                // attach description container to course information container
                this.CourseInformationContainer.Controls.Add(courseDescriptionContainer);

                // OBJECTIVES

                if (!String.IsNullOrWhiteSpace(this._CourseObject.Objectives))
                {
                    Panel courseObjectivesContainer = new Panel();
                    courseObjectivesContainer.ID = "CourseObjectivesContainer";
                    courseObjectivesContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseObjectivesLabelContainer = new Panel();
                    courseObjectivesLabelContainer.ID = "CourseObjectivesLabelContainer";
                    courseObjectivesLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseObjectivesContainer.Controls.Add(courseObjectivesLabelContainer);

                    Literal courseObjectivesLabel = new Literal();
                    courseObjectivesLabel.Text = _GlobalResources.Objectives;
                    courseObjectivesLabelContainer.Controls.Add(courseObjectivesLabel);

                    // objectives
                    Panel courseObjectivesContentContainer = new Panel();
                    courseObjectivesContentContainer.ID = "CourseObjectivesContentContainer";
                    courseObjectivesContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseObjectivesContainer.Controls.Add(courseObjectivesContentContainer);

                    Label courseObjectives = new Label();
                    courseObjectives.ID = "CourseObjectives";
                    courseObjectives.Text = objectives;

                    courseObjectivesContentContainer.Controls.Add(courseObjectives);

                    // attach objectives container to course information container
                    this.CourseInformationContainer.Controls.Add(courseObjectivesContainer);
                }

                // COURSE MATERIALS

                // get the course materials into a DataTable
                DataTable courseMaterials = this._CourseObject.GetCourseMaterials(null);

                if (courseMaterials.Rows.Count > 0)
                {
                    Panel courseMaterialsContainer = new Panel();
                    courseMaterialsContainer.ID = "CourseMaterialsContainer";
                    courseMaterialsContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseMaterialsLabelContainer = new Panel();
                    courseMaterialsLabelContainer.ID = "CourseMaterialsLabelContainer";
                    courseMaterialsLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseMaterialsContainer.Controls.Add(courseMaterialsLabelContainer);

                    Literal courseMaterialsLabel = new Literal();
                    courseMaterialsLabel.Text = _GlobalResources.CourseMaterials;
                    courseMaterialsLabelContainer.Controls.Add(courseMaterialsLabel);

                    // course materials
                    Panel courseMaterialsContentContainer = new Panel();
                    courseMaterialsContentContainer.ID = "CourseMaterialsContentContainer";
                    courseMaterialsContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseMaterialsContainer.Controls.Add(courseMaterialsContentContainer);

                    foreach (DataRow row in courseMaterials.Rows)
                    {
                        Panel courseMaterialLinkContainer = new Panel();
                        courseMaterialLinkContainer.ID = "CourseMaterialLinkContainer_" + row["idDocumentRepositoryItem"].ToString();
                        courseMaterialLinkContainer.CssClass = "DetailsViewListItemContainer";
                        courseMaterialsContentContainer.Controls.Add(courseMaterialLinkContainer);

                        HyperLink courseMaterialLink = new HyperLink();
                        courseMaterialLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_COURSE + this._CourseObject.Id + "/" + Convert.ToString(row["fileName"]);
                        courseMaterialLink.Target = "_blank";
                        courseMaterialLink.Text = row["label"].ToString();
                        courseMaterialLinkContainer.Controls.Add(courseMaterialLink);

                        Literal courseMaterialSize = new Literal();
                        courseMaterialSize.Text = " (" + Asentia.Common.Utility.GetSizeStringFromKB(Convert.ToInt32(row["kb"])) + ")";
                        courseMaterialLinkContainer.Controls.Add(courseMaterialSize);
                    }

                    // attach materials container to course information container
                    this.CourseInformationContainer.Controls.Add(courseMaterialsContainer);
                }

                // COURSE EXPERTS

                DataTable courseExperts = this._CourseObject.GetExperts(null);

                if (courseExperts.Rows.Count > 0)
                {
                    Panel courseExpertsContainer = new Panel();
                    courseExpertsContainer.ID = "CourseExpertsContainer";
                    courseExpertsContainer.CssClass = "DetailsViewSectionContainer";

                    // label
                    Panel courseExpertsLabelContainer = new Panel();
                    courseExpertsLabelContainer.ID = "CourseExpertsLabelContainer";
                    courseExpertsLabelContainer.CssClass = "DetailsViewSectionLabel";
                    courseExpertsContainer.Controls.Add(courseExpertsLabelContainer);

                    Literal courseExpertsLabel = new Literal();
                    courseExpertsLabel.Text = _GlobalResources.Experts;
                    courseExpertsLabelContainer.Controls.Add(courseExpertsLabel);

                    // course experts
                    Panel courseExpertsContentContainer = new Panel();
                    courseExpertsContentContainer.ID = "CourseExpertsContentContainer";
                    courseExpertsContentContainer.CssClass = "DetailsViewSectionContentContainer";
                    courseExpertsContainer.Controls.Add(courseExpertsContentContainer);

                    foreach (DataRow row in courseExperts.Rows)
                    {
                        Panel courseExpertContainer = new Panel();
                        courseExpertContainer.ID = "CourseExpertContainer_" + row["idUser"].ToString();
                        courseExpertContainer.CssClass = "DetailsViewListItemContainer";
                        courseExpertsContentContainer.Controls.Add(courseExpertContainer);

                        Literal courseExpert = new Literal();
                        courseExpert.Text = row["displayName"].ToString();
                        courseExpertContainer.Controls.Add(courseExpert);

                        // course expert email address
                        if (!String.IsNullOrWhiteSpace(row["email"].ToString()))
                        {
                            Literal openParen = new Literal();
                            openParen.Text = " (";
                            courseExpertContainer.Controls.Add(openParen);

                            HyperLink expertEmail = new HyperLink();
                            expertEmail.Text = row["email"].ToString();
                            expertEmail.NavigateUrl = "mailto:" + row["email"].ToString();
                            courseExpertContainer.Controls.Add(expertEmail);

                            Literal closeParen = new Literal();
                            closeParen.Text = ")";
                            courseExpertContainer.Controls.Add(closeParen);
                        }
                    }

                    // attach experts container to course information container
                    this.CourseInformationContainer.Controls.Add(courseExpertsContainer);
                }
            }
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.CourseLessonsGrid.AddCheckboxColumn = false;
            this.CourseLessonsGrid.ShowSearchBox = false;
            this.CourseLessonsGrid.AllowPaging = false;
            this.CourseLessonsGrid.ShowRecordsPerPageSelectbox = false;

            this.CourseLessonsGrid.StoredProcedure = "[Lesson.GetGridForEnrollment]";
            this.CourseLessonsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.CourseLessonsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.CourseLessonsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.CourseLessonsGrid.AddFilter("@idEnrollment", SqlDbType.Int, 4, this._EnrollmentObject.Id);
            this.CourseLessonsGrid.IdentifierField = "idLesson";
            this.CourseLessonsGrid.DefaultSortColumn = "title";

            // data key names
            this.CourseLessonsGrid.DataKeyNames = new string[] { "idData-Lesson", "idContentPackage" };

            // columns
            GridColumn lessonTitle = new GridColumn(_GlobalResources.Lesson, "title");
            GridColumn lessonStatus = new GridColumn(_GlobalResources.Status, null, true); // contents of this column will be populated by the method that handles RowDataBound
            GridColumn lessonScore = new GridColumn(_GlobalResources.Score, "contentScore", true);
            GridColumn lessonLaunch = new GridColumn(_GlobalResources.Action, null, true); // contents of this column will be populated by the method that handles RowDataBound

            // add columns to data grid 
            this.CourseLessonsGrid.AddColumn(lessonTitle);
            this.CourseLessonsGrid.AddColumn(lessonStatus);
            this.CourseLessonsGrid.AddColumn(lessonScore);
            this.CourseLessonsGrid.AddColumn(lessonLaunch);
            this.CourseLessonsGrid.RowDataBound += new GridViewRowEventHandler(this._CourseLessonsGridView_RowDataBound);
        }
        #endregion

        #region _CourseLessonsGridView_RowDataBound
        /// <summary>
        /// Handles course lesson grid row data bind event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _CourseLessonsGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                int idDataLesson = Convert.ToInt32(rowView["idData-Lesson"]); // will never be null
                int idLesson = Convert.ToInt32(rowView["idLesson"]); // will never be null
                bool allowLessonLaunch = Convert.ToBoolean(rowView["allowLaunch"]); // will never be null
                bool preventPostCompletionLaunchForContentChange = Convert.ToBoolean(rowView["preventPostCompletionLaunchForContentChange"]); // will never be null

                Lesson.LessonContentType? contentTypeCommittedTo = null;
                if (!String.IsNullOrWhiteSpace(rowView["contentTypeCommittedTo"].ToString()))
                { contentTypeCommittedTo = (Lesson.LessonContentType)Convert.ToInt32(rowView["contentTypeCommittedTo"]); }

                // STATUS
                string contentCompletionStatus = rowView["contentCompletionStatus"].ToString();
                string contentSuccessStatus = rowView["contentSuccessStatus"].ToString();

                if (
                    contentSuccessStatus == Lesson.LessonSuccessStatus.Passed.ToString().ToLower()
                    && (String.IsNullOrWhiteSpace(contentCompletionStatus) || contentCompletionStatus == Lesson.LessonCompletionStatus.Completed.ToString().ToLower() || contentCompletionStatus == Lesson.LessonCompletionStatus.Unknown.ToString().ToLower())
                   )
                { e.Row.Cells[1].Text = _GlobalResources.Passed; }
                else if (
                         contentSuccessStatus == Lesson.LessonSuccessStatus.Failed.ToString().ToLower()
                        )
                { e.Row.Cells[1].Text = _GlobalResources.Failed; }
                else if (
                            (String.IsNullOrWhiteSpace(contentSuccessStatus) || (contentSuccessStatus == Lesson.LessonSuccessStatus.Unknown.ToString().ToLower()))
                            && contentCompletionStatus == Lesson.LessonCompletionStatus.Completed.ToString().ToLower()
                        )
                { e.Row.Cells[1].Text = _GlobalResources.Completed; }
                else if (
                         (
                          (contentCompletionStatus == Lesson.LessonCompletionStatus.Incomplete.ToString().ToLower() && contentSuccessStatus != Lesson.LessonSuccessStatus.Failed.ToString().ToLower())
                          ||
                          (
                           (String.IsNullOrWhiteSpace(contentSuccessStatus) || contentSuccessStatus == Lesson.LessonSuccessStatus.Unknown.ToString().ToLower())
                           &&
                           (String.IsNullOrWhiteSpace(contentCompletionStatus) || contentCompletionStatus == Lesson.LessonCompletionStatus.Unknown.ToString().ToLower())
                          )
                         )
                         && allowLessonLaunch == true
                        )                   
                { e.Row.Cells[1].Text = _GlobalResources.IncompleteInProgress; }
                else if (contentCompletionStatus == Lesson.LessonCompletionStatus.Unknown.ToString().ToLower() && contentSuccessStatus == Lesson.LessonSuccessStatus.Unknown.ToString().ToLower()
                         && allowLessonLaunch == false)
                { e.Row.Cells[1].Text = _GlobalResources.NotAttempted; }
                else
                { e.Row.Cells[1].Text = _GlobalResources.Unknown; }

                // SCORE
                // if no score, replace with "-"; otherwise, append 
                string contentScore = rowView["contentScore"].ToString();

                if (String.IsNullOrWhiteSpace(contentScore))
                { e.Row.Cells[2].Text = string.Empty; }
                else
                { e.Row.Cells[2].Text += "%"; }

                // ACTION
                bool hasMultipleContentDeliveryMethods = Convert.ToBoolean(rowView["hasMultipleContentMethods"]); // will never be null
                bool hasContentPackage = Convert.ToBoolean(rowView["hasContentPackage"]); // will never be null
                bool hasStandupTraining = Convert.ToBoolean(rowView["hasStandupTraining"]); // will never be null
                bool hasTask = Convert.ToBoolean(rowView["hasTask"]); // will never be null
                bool hasOJT = Convert.ToBoolean(rowView["hasOJT"]); // will never be null
                string taskResourcePath = rowView["taskResourcePath"].ToString();
                string uploadedTaskFilename = rowView["uploadedTaskFilename"].ToString();
                string taskUploadedDate = rowView["taskUploadedDate"].ToString();
                string idTaskDocumentType = rowView["idTaskDocumentType"].ToString();

                if (!allowLessonLaunch && contentCompletionStatus != Lesson.LessonCompletionStatus.Completed.ToString().ToLower()) // if launch is not allowed due to forceCompletionInOrder AND not completed, lock it
                                                                                                                                   // note: not completed check is for when/if an admin overrides the completion of a lesson
                {
                    HyperLink launchLockedLink = new HyperLink();
                    launchLockedLink.ID = "LaunchLockedLink_" + idDataLesson.ToString();
                    launchLockedLink.ToolTip = _GlobalResources.ModuleLockedPreviousModulesMustBeCompletedFirst;

                    Image launchLockedLinkImage = new Image();
                    launchLockedLinkImage.ID = "LaunchLockedLinkImage_" + idDataLesson.ToString();
                    launchLockedLinkImage.CssClass = "SmallIcon";
                    launchLockedLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD, ImageFiles.EXT_PNG);
                    launchLockedLinkImage.AlternateText = _GlobalResources.Locked;

                    launchLockedLink.Controls.Add(launchLockedLinkImage);

                    e.Row.Cells[3].Controls.Add(launchLockedLink);
                }
                else 
                {
                    if (hasMultipleContentDeliveryMethods)
                    {
                        Panel multipleContentMethodLaunchContainer = new Panel();
                        multipleContentMethodLaunchContainer.ID = "MultipleContentMethodLaunchContainer_" + idDataLesson.ToString();
                        multipleContentMethodLaunchContainer.CssClass = "MultipleContentMethodLaunchContainer";

                        Image multipleContentMethodLaunchMainImage = new Image();
                        multipleContentMethodLaunchMainImage.ID = "MultipleContentMethodLaunchMainImage_" + idDataLesson.ToString();
                        multipleContentMethodLaunchMainImage.CssClass = "MultipleContentMethodLaunchImage SmallIcon";
                        multipleContentMethodLaunchMainImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DROPDOWN, ImageFiles.EXT_PNG);
                        multipleContentMethodLaunchMainImage.AlternateText = _GlobalResources.Lesson;
                        multipleContentMethodLaunchMainImage.Attributes.Add("onclick", "ToggleMultipleContentMenu(\"" + idDataLesson + "\");");
                        multipleContentMethodLaunchContainer.Controls.Add(multipleContentMethodLaunchMainImage);

                        //Image multipleContentMethodLaunchDownArrowImage = new Image();
                        //multipleContentMethodLaunchDownArrowImage.ID = "MultipleContentMethodLaunchDownArrowImage_" + idDataLesson.ToString();
                        //multipleContentMethodLaunchDownArrowImage.CssClass = "MultipleContentMethodLaunchImage XSmallIcon";
                        //multipleContentMethodLaunchDownArrowImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DROPDOWN, ImageFiles.EXT_PNG);
                        //multipleContentMethodLaunchDownArrowImage.AlternateText = _GlobalResources.Lesson;
                        //multipleContentMethodLaunchDownArrowImage.Attributes.Add("onclick", "ToggleMultipleContentMenu(\"" + idDataLesson + "\");");
                        //multipleContentMethodLaunchContainer.Controls.Add(multipleContentMethodLaunchDownArrowImage);

                        Panel multipleContentMethodLaunchItemContainer = new Panel();
                        multipleContentMethodLaunchItemContainer.ID = "MultipleContentMethodLaunchItemContainer_" + idDataLesson.ToString();
                        multipleContentMethodLaunchItemContainer.CssClass = "MultipleContentMethodLaunchItemContainer";

                        if (hasContentPackage)
                        {
                            Panel contentPackageLaunchLinkContainer = new Panel();
                            contentPackageLaunchLinkContainer.ID = "ContentPackageLaunchLinkContainer_" + idDataLesson.ToString();

                            ContentPackage.ContentPackageType contentPackageType = (ContentPackage.ContentPackageType)Convert.ToInt32(rowView["contentPackageType"]); // will never be null, if we get here
                            int idContentPackage = Convert.ToInt32(rowView["idContentPackage"]); // will never be null, if we get here

                            if (preventPostCompletionLaunchForContentChange)
                            {
                                HyperLink launchPackageLink = new HyperLink();
                                launchPackageLink.ID = "LaunchPackageLink_" + idDataLesson.ToString();
                                launchPackageLink.NavigateUrl = null;
                                launchPackageLink.Enabled = false;
                                launchPackageLink.ToolTip = _GlobalResources.LaunchHasBeenDisabledDueToModuleContentChange;

                                Image launchPackageLinkImage = new Image();
                                launchPackageLinkImage.ID = "LaunchPackageLinkImage_" + idDataLesson.ToString();
                                launchPackageLinkImage.CssClass = "SmallIcon DimIcon";
                                launchPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE_BUTTON, ImageFiles.EXT_PNG);
                                launchPackageLinkImage.AlternateText = _GlobalResources.View;

                                launchPackageLink.Controls.Add(launchPackageLinkImage);

                                contentPackageLaunchLinkContainer.Controls.Add(launchPackageLink);
                            }
                            else
                            {
                                if (contentPackageType == ContentPackage.ContentPackageType.SCORM)
                                {
                                    HyperLink launchPackageLink = new HyperLink();
                                    launchPackageLink.ID = "LaunchPackageLink_" + idDataLesson.ToString();
                                    launchPackageLink.NavigateUrl = "/launch/Scorm.aspx?id=" + idDataLesson.ToString() + "&cpid=" + idContentPackage.ToString() + "&launchType=learner";
                                    launchPackageLink.ToolTip = _GlobalResources.LaunchContentPackage;

                                    Image launchPackageLinkImage = new Image();
                                    launchPackageLinkImage.ID = "LaunchPackageLinkImage_" + idDataLesson.ToString();
                                    launchPackageLinkImage.CssClass = "SmallIcon";
                                    launchPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE_BUTTON, ImageFiles.EXT_PNG);
                                    launchPackageLinkImage.AlternateText = _GlobalResources.View;

                                    launchPackageLink.Controls.Add(launchPackageLinkImage);

                                    contentPackageLaunchLinkContainer.Controls.Add(launchPackageLink);

                                    // disable the link if another content type has been committed to
                                    if (contentTypeCommittedTo != null && contentTypeCommittedTo != Lesson.LessonContentType.ContentPackage)
                                    {
                                        launchPackageLink.Enabled = false;
                                        launchPackageLink.NavigateUrl = null;
                                        launchPackageLinkImage.CssClass += " DimIcon";
                                    }
                                }
                                else if (contentPackageType == ContentPackage.ContentPackageType.xAPI)
                                {
                                    HyperLink launchPackageLink = new HyperLink();
                                    launchPackageLink.ID = "LaunchPackageLink_" + idDataLesson.ToString();
                                    launchPackageLink.NavigateUrl = "/launch/Tincan.aspx?id=" + idDataLesson.ToString() + "&cpid=" + idContentPackage.ToString() + "&launchType=learner";
                                    launchPackageLink.ToolTip = _GlobalResources.LaunchContentPackage;

                                    Image launchPackageLinkImage = new Image();
                                    launchPackageLinkImage.ID = "LaunchPackageLinkImage_" + idDataLesson.ToString();
                                    launchPackageLinkImage.CssClass = "SmallIcon";
                                    launchPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE_BUTTON, ImageFiles.EXT_PNG);
                                    launchPackageLinkImage.AlternateText = _GlobalResources.View;

                                    launchPackageLink.Controls.Add(launchPackageLinkImage);

                                    contentPackageLaunchLinkContainer.Controls.Add(launchPackageLink);

                                    // disable the link if another content type has been committed to
                                    if (contentTypeCommittedTo != null && contentTypeCommittedTo != Lesson.LessonContentType.ContentPackage)
                                    {
                                        launchPackageLink.Enabled = false;
                                        launchPackageLink.NavigateUrl = null;
                                        launchPackageLinkImage.CssClass += " DimIcon";
                                    }
                                }
                            }

                            multipleContentMethodLaunchItemContainer.Controls.Add(contentPackageLaunchLinkContainer);
                        }

                        if (hasStandupTraining)
                        {
                            Panel standupTrainingLaunchLinkContainer = new Panel();
                            standupTrainingLaunchLinkContainer.ID = "StandupTrainingLaunchLinkContainer_" + idDataLesson.ToString();

                            int idStandupTraining = Convert.ToInt32(rowView["idStandupTraining"]); // will never be null, if we get here
                            int? idStandupTrainingInstance = null; // null if the user has not enrolled in a session

                            if (!String.IsNullOrWhiteSpace(rowView["idStandupTrainingInstance"].ToString()))
                            { idStandupTrainingInstance = Convert.ToInt32(rowView["idStandupTrainingInstance"]); }

                            // get the standup training object
                            StandupTraining standupTrainingObject = new StandupTraining(idStandupTraining);

                            if (idStandupTrainingInstance == null) // no session enrolled
                            {
                                // button
                                LinkButton launchStandupTrainingInstanceLink = new LinkButton();
                                launchStandupTrainingInstanceLink.ID = "LaunchStandupTrainingInstanceLink_" + idDataLesson.ToString();
                                launchStandupTrainingInstanceLink.OnClientClick = "LoadEnrollInStandupTrainingModalContent(" + idDataLesson + ", " + idStandupTraining + "); return false;";
                                launchStandupTrainingInstanceLink.ToolTip = _GlobalResources.EnrollInInstructorLedTrainingSession;

                                Image launchStandupTrainingInstanceLinkImage = new Image();
                                launchStandupTrainingInstanceLinkImage.ID = "LaunchStandupTrainingInstanceLinkImage_" + idDataLesson.ToString();
                                launchStandupTrainingInstanceLinkImage.CssClass = "SmallIcon";
                                //launchStandupTrainingInstanceLinkImage.Style.Add("margin-left", "5px");
                                launchStandupTrainingInstanceLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_BUTTON, ImageFiles.EXT_PNG);
                                launchStandupTrainingInstanceLinkImage.AlternateText = _GlobalResources.InstructorLedTraining;

                                //Image launchStandupTrainingInstanceLinkOverlayImage = new Image();
                                //launchStandupTrainingInstanceLinkOverlayImage.ID = "LaunchStandupTrainingInstanceLinkOverlayImage_" + idDataLesson.ToString();
                                //launchStandupTrainingInstanceLinkOverlayImage.CssClass = "XSmallIcon";
                                //launchStandupTrainingInstanceLinkOverlayImage.Style.Add("position", "relative");
                                //launchStandupTrainingInstanceLinkOverlayImage.Style.Add("left", "-5px");
                                //launchStandupTrainingInstanceLinkOverlayImage.Style.Add("opacity", "1.0");
                                //launchStandupTrainingInstanceLinkOverlayImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG);
                                //launchStandupTrainingInstanceLinkOverlayImage.AlternateText = _GlobalResources.Add;

                                launchStandupTrainingInstanceLink.Controls.Add(launchStandupTrainingInstanceLinkImage);
                                //launchStandupTrainingInstanceLink.Controls.Add(launchStandupTrainingInstanceLinkOverlayImage);

                                // disable the button if the standup training is restricted enroll or another content type has been committed to
                                if (standupTrainingObject.IsRestrictedEnroll || (contentTypeCommittedTo != null && contentTypeCommittedTo != Lesson.LessonContentType.StandupTrainingModule))
                                {
                                    launchStandupTrainingInstanceLink.Enabled = false;
                                    launchStandupTrainingInstanceLink.OnClientClick = "return false;";
                                    launchStandupTrainingInstanceLinkImage.CssClass += " DimIcon";
                                    //launchStandupTrainingInstanceLinkOverlayImage.CssClass += " DimIcon";
                                }

                                standupTrainingLaunchLinkContainer.Controls.Add(launchStandupTrainingInstanceLink);
                            }
                            else // session is enrolled
                            {
                                // add enrolled standup training information to lesson name column
                                Literal lessonName = new Literal();
                                lessonName.Text = e.Row.Cells[0].Text;
                                e.Row.Cells[0].Controls.Add(lessonName);

                                Panel enrolledStandupTrainingInstanceInformationContainer = new Panel();
                                enrolledStandupTrainingInstanceInformationContainer.ID = "EnrolledStandupTrainingInstanceInformationContainer_" + idDataLesson.ToString();
                                enrolledStandupTrainingInstanceInformationContainer.CssClass = "EnrolledStandupTrainingInstanceTitleLinkContainer";

                                // standup training instance label
                                Label standupTrainingInstanceLabel = new Label();
                                standupTrainingInstanceLabel.Text = _GlobalResources.InstructorLedTrainingSession + ": ";
                                enrolledStandupTrainingInstanceInformationContainer.Controls.Add(standupTrainingInstanceLabel);

                                // standup training instance information link
                                HyperLink enrolledStandupTrainingInstanceInformationLink = new HyperLink();
                                enrolledStandupTrainingInstanceInformationLink.ID = "EnrolledStandupTrainingInstanceInformationLink_" + idDataLesson.ToString();
                                enrolledStandupTrainingInstanceInformationLink.NavigateUrl = "javascript: LoadViewEnrolledStandupTrainingModalContent(" + idDataLesson + ", " + idStandupTraining + ", " + idStandupTrainingInstance + ");";

                                // get the standup training instance information
                                StandupTrainingInstance standupTrainingInstanceObject = new StandupTrainingInstance((int)idStandupTrainingInstance);

                                // get the instance title for the link
                                foreach (StandupTrainingInstance.LanguageSpecificProperty standupTrainingInstanceLSP in standupTrainingInstanceObject.LanguageSpecificProperties)
                                {
                                    if (standupTrainingInstanceLSP.LangString == AsentiaSessionState.UserCulture)
                                    {
                                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceLSP.Title))
                                        { enrolledStandupTrainingInstanceInformationLink.Text = standupTrainingInstanceLSP.Title; }
                                    }
                                }

                                // attach link to container
                                enrolledStandupTrainingInstanceInformationContainer.Controls.Add(enrolledStandupTrainingInstanceInformationLink);

                                // if this standup training is not restricted drop, not already completed, and has not already started/or is starting within 15 minutes, allow it to be dropped
                                bool standupTrainingInstanceHasStarted = false;

                                foreach (StandupTrainingInstance.MeetingTimeProperty standupTrainingInstanceMTP in standupTrainingInstanceObject.MeetingTimes)
                                {
                                    if (standupTrainingInstanceMTP.DtStart.AddMinutes(-15) <= AsentiaSessionState.UtcNow)
                                    { standupTrainingInstanceHasStarted = true; }
                                }

                                if (!standupTrainingObject.IsRestrictedDrop && contentCompletionStatus != "completed" && !standupTrainingInstanceHasStarted)
                                {
                                    // remove standup training instance link button
                                    LinkButton removeStandupTrainingInstanceLink = new LinkButton();
                                    removeStandupTrainingInstanceLink.ID = "RemoveStandupTrainingInstanceLink_" + idDataLesson.ToString();
                                    removeStandupTrainingInstanceLink.OnClientClick = "LoadRemoveEnrolledStandupTrainingModalContent(" + idDataLesson + ", " + idStandupTrainingInstance + "); return false;";


                                    Image removeStandupTrainingInstanceLinkImage = new Image();
                                    removeStandupTrainingInstanceLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                                    removeStandupTrainingInstanceLinkImage.CssClass = "XSmallIcon";
                                    removeStandupTrainingInstanceLink.Controls.Add(removeStandupTrainingInstanceLinkImage);

                                    // attach link to container
                                    enrolledStandupTrainingInstanceInformationContainer.Controls.Add(removeStandupTrainingInstanceLink);
                                }

                                // attach container to row
                                e.Row.Cells[0].Controls.Add(enrolledStandupTrainingInstanceInformationContainer);

                                // add "launch" button for standup training instance
                                LinkButton launchStandupTrainingInstanceLink = new LinkButton();
                                launchStandupTrainingInstanceLink.ID = "LaunchStandupTrainingInstanceLink_" + idDataLesson.ToString();
                                launchStandupTrainingInstanceLink.OnClientClick = "LoadViewEnrolledStandupTrainingModalContent(" + idDataLesson + ", " + idStandupTraining + ", " + idStandupTrainingInstance + "); return false;";
                                launchStandupTrainingInstanceLink.ToolTip = _GlobalResources.ViewInstructorLedTrainingSessionInformation;

                                Image launchStandupTrainingInstanceLinkImage = new Image();
                                launchStandupTrainingInstanceLinkImage.ID = "LaunchStandupTrainingInstanceLinkImage_" + idDataLesson.ToString();
                                launchStandupTrainingInstanceLinkImage.CssClass = "SmallIcon";
                                launchStandupTrainingInstanceLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_BUTTON, ImageFiles.EXT_PNG);
                                launchStandupTrainingInstanceLinkImage.AlternateText = _GlobalResources.InstructorLedTraining;

                                launchStandupTrainingInstanceLink.Controls.Add(launchStandupTrainingInstanceLinkImage);

                                standupTrainingLaunchLinkContainer.Controls.Add(launchStandupTrainingInstanceLink);

                                // disable the button if another content type has been committed to - this particular one should never be dim
                                // since in order to see this button, we'd already have to be committed to the content type
                                if (contentTypeCommittedTo != null && contentTypeCommittedTo != Lesson.LessonContentType.StandupTrainingModule)
                                {
                                    launchStandupTrainingInstanceLink.Enabled = false;
                                    launchStandupTrainingInstanceLink.OnClientClick = "return false;";
                                    launchStandupTrainingInstanceLinkImage.CssClass += " DimIcon";
                                }
                            }

                            multipleContentMethodLaunchItemContainer.Controls.Add(standupTrainingLaunchLinkContainer);
                        }

                        if (hasTask)
                        {
                            Panel taskLaunchLinkContainer = new Panel();
                            taskLaunchLinkContainer.ID = "TaskLaunchLinkContainer_" + idDataLesson.ToString();

                            // button
                            LinkButton launchTaskLink = new LinkButton();
                            launchTaskLink.ID = "LaunchTaskLink_" + idDataLesson.ToString();
                            launchTaskLink.OnClientClick = "LoadTaskModalContent(" + idDataLesson + ", " + idLesson + ", \"" + Convert.ToInt32(contentTypeCommittedTo) + "\", \"" + contentCompletionStatus + "\", \"" + taskResourcePath + "\", \"" + uploadedTaskFilename + "\", \"" + taskUploadedDate + "\", \"" + idTaskDocumentType + "\"); return false;";
                            launchTaskLink.ToolTip = _GlobalResources.SubmitViewTask;

                            Image launchTaskLinkImage = new Image();
                            launchTaskLinkImage.ID = "LaunchTaskLinkImage_" + idDataLesson.ToString();
                            launchTaskLinkImage.CssClass = "SmallIcon";
                            launchTaskLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_TASK_BUTTON, ImageFiles.EXT_PNG);
                            launchTaskLinkImage.AlternateText = _GlobalResources.Task;

                            launchTaskLink.Controls.Add(launchTaskLinkImage);

                            taskLaunchLinkContainer.Controls.Add(launchTaskLink);
                            multipleContentMethodLaunchItemContainer.Controls.Add(taskLaunchLinkContainer);

                            // disable the button if another content type has been committed to
                            if (contentTypeCommittedTo != null && contentTypeCommittedTo != Lesson.LessonContentType.Task)
                            {
                                launchTaskLink.Enabled = false;
                                launchTaskLink.OnClientClick = "return false;";
                                launchTaskLinkImage.CssClass += " DimIcon";
                            }
                        }

                        if (hasOJT)
                        {
                            Panel ojtLaunchLinkContainer = new Panel();
                            ojtLaunchLinkContainer.ID = "OJTLaunchLinkContainer_" + idDataLesson.ToString();

                            // button
                            LinkButton launchOJTLink = new LinkButton();
                            launchOJTLink.ID = "LaunchOJTLink_" + idDataLesson.ToString();
                            launchOJTLink.OnClientClick = "LoadOJTModalContent(" + idDataLesson + ", \"" + Convert.ToInt32(contentTypeCommittedTo) + "\", \"" + contentCompletionStatus + "\"); return false;";
                            launchOJTLink.ToolTip = _GlobalResources.RequestCancelOJT;

                            Image launchOJTLinkImage = new Image();
                            launchOJTLinkImage.ID = "LaunchOJTLinkImage_" + idDataLesson.ToString();
                            launchOJTLinkImage.CssClass = "SmallIcon";
                            launchOJTLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OJT_BUTTON, ImageFiles.EXT_PNG);
                            launchOJTLinkImage.AlternateText = _GlobalResources.OJT;

                            launchOJTLink.Controls.Add(launchOJTLinkImage);

                            ojtLaunchLinkContainer.Controls.Add(launchOJTLink);
                            multipleContentMethodLaunchItemContainer.Controls.Add(ojtLaunchLinkContainer);

                            // disable the button if another content type has been committed to
                            if (contentTypeCommittedTo != null && contentTypeCommittedTo != Lesson.LessonContentType.OJT)
                            {
                                launchOJTLink.Enabled = false;
                                launchOJTLink.OnClientClick = "return false;";
                                launchOJTLinkImage.CssClass += " DimIcon";
                            }
                        }

                        // attach multiple content method launch item container to multiple content method launch container
                        multipleContentMethodLaunchContainer.Controls.Add(multipleContentMethodLaunchItemContainer);

                        // attach the multiple content method launch container to the table cell
                        e.Row.Cells[3].Controls.Add(multipleContentMethodLaunchContainer);
                    }
                    else // single content delivery method
                    {
                        if (hasContentPackage)
                        {
                            ContentPackage.ContentPackageType contentPackageType = (ContentPackage.ContentPackageType)Convert.ToInt32(rowView["contentPackageType"]); // will never be null, if we get here
                            int idContentPackage = Convert.ToInt32(rowView["idContentPackage"]); // will never be null, if we get here

                            if (preventPostCompletionLaunchForContentChange)
                            {
                                HyperLink launchPackageLink = new HyperLink();
                                launchPackageLink.ID = "LaunchPackageLink_" + idDataLesson.ToString();
                                launchPackageLink.NavigateUrl = null;
                                launchPackageLink.Enabled = false;
                                launchPackageLink.ToolTip = _GlobalResources.LaunchHasBeenDisabledDueToModuleContentChange;

                                Image launchPackageLinkImage = new Image();
                                launchPackageLinkImage.ID = "LaunchPackageLinkImage_" + idDataLesson.ToString();
                                launchPackageLinkImage.CssClass = "SmallIcon DimIcon";
                                launchPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE_BUTTON, ImageFiles.EXT_PNG);
                                launchPackageLinkImage.AlternateText = _GlobalResources.View;

                                launchPackageLink.Controls.Add(launchPackageLinkImage);

                                e.Row.Cells[3].Controls.Add(launchPackageLink);
                            }
                            else
                            {
                                if (contentPackageType == ContentPackage.ContentPackageType.SCORM)
                                {
                                    HyperLink launchPackageLink = new HyperLink();
                                    launchPackageLink.ID = "LaunchPackageLink_" + idDataLesson.ToString();
                                    launchPackageLink.NavigateUrl = "/launch/Scorm.aspx?id=" + idDataLesson.ToString() + "&cpid=" + idContentPackage.ToString() + "&launchType=learner";
                                    launchPackageLink.ToolTip = _GlobalResources.LaunchContentPackage;

                                    Image launchPackageLinkImage = new Image();
                                    launchPackageLinkImage.ID = "LaunchPackageLinkImage_" + idDataLesson.ToString();
                                    launchPackageLinkImage.CssClass = "SmallIcon";
                                    launchPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE_BUTTON, ImageFiles.EXT_PNG);
                                    launchPackageLinkImage.AlternateText = _GlobalResources.View;

                                    launchPackageLink.Controls.Add(launchPackageLinkImage);

                                    e.Row.Cells[3].Controls.Add(launchPackageLink);
                                }
                                else if (contentPackageType == ContentPackage.ContentPackageType.xAPI)
                                {
                                    HyperLink launchPackageLink = new HyperLink();
                                    launchPackageLink.ID = "LaunchPackageLink_" + idDataLesson.ToString();
                                    launchPackageLink.NavigateUrl = "/launch/Tincan.aspx?id=" + idDataLesson.ToString() + "&cpid=" + idContentPackage.ToString() + "&launchType=learner";
                                    launchPackageLink.ToolTip = _GlobalResources.LaunchContentPackage;

                                    Image launchPackageLinkImage = new Image();
                                    launchPackageLinkImage.ID = "LaunchPackageLinkImage_" + idDataLesson.ToString();
                                    launchPackageLinkImage.CssClass = "SmallIcon";
                                    launchPackageLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE_BUTTON, ImageFiles.EXT_PNG);
                                    launchPackageLinkImage.AlternateText = _GlobalResources.View;

                                    launchPackageLink.Controls.Add(launchPackageLinkImage);

                                    e.Row.Cells[3].Controls.Add(launchPackageLink);
                                }
                            }
                        }

                        if (hasStandupTraining)
                        {
                            int idStandupTraining = Convert.ToInt32(rowView["idStandupTraining"]); // will never be null, if we get here
                            int? idStandupTrainingInstance = null; // null if the user has not enrolled in a session

                            if (!String.IsNullOrWhiteSpace(rowView["idStandupTrainingInstance"].ToString()))
                            { idStandupTrainingInstance = Convert.ToInt32(rowView["idStandupTrainingInstance"]); }

                            // get the standup training object
                            StandupTraining standupTrainingObject = new StandupTraining(idStandupTraining);

                            if (idStandupTrainingInstance == null) // no session enrolled
                            {
                                // button
                                LinkButton launchStandupTrainingInstanceLink = new LinkButton();
                                launchStandupTrainingInstanceLink.ID = "LaunchStandupTrainingInstanceLink_" + idDataLesson.ToString();
                                launchStandupTrainingInstanceLink.OnClientClick = "LoadEnrollInStandupTrainingModalContent(" + idDataLesson + ", " + idStandupTraining + "); return false;";
                                launchStandupTrainingInstanceLink.ToolTip = _GlobalResources.EnrollInInstructorLedTrainingSession;

                                Image launchStandupTrainingInstanceLinkImage = new Image();
                                launchStandupTrainingInstanceLinkImage.ID = "LaunchStandupTrainingInstanceLinkImage_" + idDataLesson.ToString();
                                launchStandupTrainingInstanceLinkImage.CssClass = "SmallIcon";
                                //launchStandupTrainingInstanceLinkImage.Style.Add("margin-left", "5px");
                                launchStandupTrainingInstanceLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_BUTTON, ImageFiles.EXT_PNG);
                                launchStandupTrainingInstanceLinkImage.AlternateText = _GlobalResources.InstructorLedTraining;

                                //Image launchStandupTrainingInstanceLinkOverlayImage = new Image();
                                //launchStandupTrainingInstanceLinkOverlayImage.ID = "LaunchStandupTrainingInstanceLinkOverlayImage_" + idDataLesson.ToString();
                                //launchStandupTrainingInstanceLinkOverlayImage.CssClass = "XSmallIcon";
                                //launchStandupTrainingInstanceLinkOverlayImage.Style.Add("position", "relative");
                                //launchStandupTrainingInstanceLinkOverlayImage.Style.Add("left", "-5px");
                                //launchStandupTrainingInstanceLinkOverlayImage.Style.Add("opacity", "1.0");
                                //launchStandupTrainingInstanceLinkOverlayImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG);
                                //launchStandupTrainingInstanceLinkOverlayImage.AlternateText = _GlobalResources.Add;

                                launchStandupTrainingInstanceLink.Controls.Add(launchStandupTrainingInstanceLinkImage);
                                //launchStandupTrainingInstanceLink.Controls.Add(launchStandupTrainingInstanceLinkOverlayImage);

                                // disable the button if the standup training is restricted enroll
                                if (standupTrainingObject.IsRestrictedEnroll)
                                {
                                    launchStandupTrainingInstanceLink.Enabled = false;
                                    launchStandupTrainingInstanceLink.OnClientClick = "return false;";
                                    launchStandupTrainingInstanceLinkImage.CssClass += " DimIcon";
                                    //launchStandupTrainingInstanceLinkOverlayImage.CssClass += " DimIcon";
                                }

                                e.Row.Cells[3].Controls.Add(launchStandupTrainingInstanceLink);
                            }
                            else // session is enrolled
                            {
                                // add enrolled standup training information to lesson name column
                                Literal lessonName = new Literal();
                                lessonName.Text = e.Row.Cells[0].Text;
                                e.Row.Cells[0].Controls.Add(lessonName);

                                Panel enrolledStandupTrainingInstanceInformationContainer = new Panel();
                                enrolledStandupTrainingInstanceInformationContainer.ID = "EnrolledStandupTrainingInstanceInformationContainer_" + idDataLesson.ToString();
                                enrolledStandupTrainingInstanceInformationContainer.CssClass = "EnrolledStandupTrainingInstanceTitleLinkContainer";

                                // standup training instance label
                                Label standupTrainingInstanceLabel = new Label();
                                standupTrainingInstanceLabel.Text = _GlobalResources.InstructorLedTrainingSession + ": ";
                                enrolledStandupTrainingInstanceInformationContainer.Controls.Add(standupTrainingInstanceLabel);

                                // standup training instance information link
                                HyperLink enrolledStandupTrainingInstanceInformationLink = new HyperLink();
                                enrolledStandupTrainingInstanceInformationLink.ID = "EnrolledStandupTrainingInstanceInformationLink_" + idDataLesson.ToString();
                                enrolledStandupTrainingInstanceInformationLink.NavigateUrl = "javascript: LoadViewEnrolledStandupTrainingModalContent(" + idDataLesson + ", " + idStandupTraining + ", " + idStandupTrainingInstance + ");";

                                // get the standup training instance information
                                StandupTrainingInstance standupTrainingInstanceObject = new StandupTrainingInstance((int)idStandupTrainingInstance);

                                // get the instance title for the link
                                foreach (StandupTrainingInstance.LanguageSpecificProperty standupTrainingInstanceLSP in standupTrainingInstanceObject.LanguageSpecificProperties)
                                {
                                    if (standupTrainingInstanceLSP.LangString == AsentiaSessionState.UserCulture)
                                    {
                                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceLSP.Title))
                                        { enrolledStandupTrainingInstanceInformationLink.Text = standupTrainingInstanceLSP.Title; }
                                    }
                                }

                                // attach link to container
                                enrolledStandupTrainingInstanceInformationContainer.Controls.Add(enrolledStandupTrainingInstanceInformationLink);

                                // if this standup training is not restricted drop, not already completed, and has not already started/or is starting within 15 minutes, allow it to be dropped
                                bool standupTrainingInstanceHasStarted = false;

                                foreach (StandupTrainingInstance.MeetingTimeProperty standupTrainingInstanceMTP in standupTrainingInstanceObject.MeetingTimes)
                                {
                                    if (standupTrainingInstanceMTP.DtStart.AddMinutes(-15) <= AsentiaSessionState.UtcNow)
                                    { standupTrainingInstanceHasStarted = true; }
                                }

                                if (!standupTrainingObject.IsRestrictedDrop && contentCompletionStatus != "completed" && !standupTrainingInstanceHasStarted)
                                {
                                    // remove standup training instance link button
                                    LinkButton removeStandupTrainingInstanceLink = new LinkButton();
                                    removeStandupTrainingInstanceLink.ID = "RemoveStandupTrainingInstanceLink_" + idDataLesson.ToString();
                                    removeStandupTrainingInstanceLink.OnClientClick = "LoadRemoveEnrolledStandupTrainingModalContent(" + idDataLesson + ", " + idStandupTrainingInstance + "); return false;";


                                    Image removeStandupTrainingInstanceLinkImage = new Image();
                                    removeStandupTrainingInstanceLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                                    removeStandupTrainingInstanceLinkImage.CssClass = "XSmallIcon";
                                    removeStandupTrainingInstanceLink.Controls.Add(removeStandupTrainingInstanceLinkImage);

                                    // attach link to container
                                    enrolledStandupTrainingInstanceInformationContainer.Controls.Add(removeStandupTrainingInstanceLink);
                                }

                                // attach container to row
                                e.Row.Cells[0].Controls.Add(enrolledStandupTrainingInstanceInformationContainer);

                                // add "launch" button for standup training instance
                                LinkButton launchStandupTrainingInstanceLink = new LinkButton();
                                launchStandupTrainingInstanceLink.ID = "LaunchStandupTrainingInstanceLink_" + idDataLesson.ToString();
                                launchStandupTrainingInstanceLink.OnClientClick = "LoadViewEnrolledStandupTrainingModalContent(" + idDataLesson + ", " + idStandupTraining + ", " + idStandupTrainingInstance + "); return false;";
                                launchStandupTrainingInstanceLink.ToolTip = _GlobalResources.ViewInstructorLedTrainingSessionInformation;

                                Image launchStandupTrainingInstanceLinkImage = new Image();
                                launchStandupTrainingInstanceLinkImage.ID = "LaunchStandupTrainingInstanceLinkImage_" + idDataLesson.ToString();
                                launchStandupTrainingInstanceLinkImage.CssClass = "SmallIcon";
                                launchStandupTrainingInstanceLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_BUTTON, ImageFiles.EXT_PNG);
                                launchStandupTrainingInstanceLinkImage.AlternateText = _GlobalResources.InstructorLedTraining;

                                launchStandupTrainingInstanceLink.Controls.Add(launchStandupTrainingInstanceLinkImage);

                                e.Row.Cells[3].Controls.Add(launchStandupTrainingInstanceLink);
                            }
                        }

                        if (hasTask)
                        {
                            // button
                            LinkButton launchTaskLink = new LinkButton();
                            launchTaskLink.ID = "LaunchTaskLink_" + idDataLesson.ToString();
                            launchTaskLink.OnClientClick = "LoadTaskModalContent(" + idDataLesson + ", " + idLesson + ", \"" + Convert.ToInt32(contentTypeCommittedTo) + "\", \"" + contentCompletionStatus + "\", \"" + taskResourcePath + "\", \"" + uploadedTaskFilename + "\", \"" + taskUploadedDate + "\", \"" + idTaskDocumentType + "\"); return false;";
                            launchTaskLink.ToolTip = _GlobalResources.SubmitViewTask;

                            Image launchTaskLinkImage = new Image();
                            launchTaskLinkImage.ID = "LaunchTaskLinkImage_" + idDataLesson.ToString();
                            launchTaskLinkImage.CssClass = "SmallIcon";
                            launchTaskLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_TASK_BUTTON, ImageFiles.EXT_PNG);
                            launchTaskLinkImage.AlternateText = _GlobalResources.Task;

                            launchTaskLink.Controls.Add(launchTaskLinkImage);

                            e.Row.Cells[3].Controls.Add(launchTaskLink);
                        }

                        if (hasOJT)
                        {
                            // button
                            LinkButton launchOJTLink = new LinkButton();
                            launchOJTLink.ID = "LaunchOJTLink_" + idDataLesson.ToString();
                            launchOJTLink.OnClientClick = "LoadOJTModalContent(" + idDataLesson + ", \"" + Convert.ToInt32(contentTypeCommittedTo) + "\", \"" + contentCompletionStatus + "\"); return false;";
                            launchOJTLink.ToolTip = _GlobalResources.RequestCancelOJT;

                            Image launchOJTLinkImage = new Image();
                            launchOJTLinkImage.ID = "LaunchOJTLinkImage_" + idDataLesson.ToString();
                            launchOJTLinkImage.CssClass = "SmallIcon";
                            launchOJTLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OJT_BUTTON, ImageFiles.EXT_PNG);
                            launchOJTLinkImage.AlternateText = _GlobalResources.OJT;

                            launchOJTLink.Controls.Add(launchOJTLinkImage);

                            e.Row.Cells[3].Controls.Add(launchOJTLink);
                        }
                    }
                }
            }
        }
        #endregion

        #region _BuildEnrollInStandupTrainingInstanceModal
        /// <summary>
        /// Builds the modal popup for enrolling into a standup training session.
        /// </summary>
        private void _BuildEnrollInStandupTrainingInstanceModal()
        {
            //set modal properties
            this._EnrollInStandupTrainingModal = new ModalPopup("EnrollInStandupTrainingModal");
            this._EnrollInStandupTrainingModal.Type = ModalPopupType.Form;
            this._EnrollInStandupTrainingModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            this._EnrollInStandupTrainingModal.HeaderIconAlt = _GlobalResources.EnrollInInstructorLedTrainingSession;
            this._EnrollInStandupTrainingModal.HeaderText = _GlobalResources.EnrollInInstructorLedTrainingSession;
            this._EnrollInStandupTrainingModal.SubmitButtonTextType = ModalPopupButtonText.Ok;
            this._EnrollInStandupTrainingModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._EnrollInStandupTrainingModal.TargetControlID = this._EnrollInStandupTrainingModalHiddenLaunchButton.ID;
            this._EnrollInStandupTrainingModal.SubmitButton.Command += new CommandEventHandler(this._EnrollInStandupTraining_Command);
            this._EnrollInStandupTrainingModal.SubmitButton.OnClientClick = "return GetSelectedStandupTrainingInstanceListItem();";

            // build the modal body
            this._EnrollInStandupTrainingModal.AddControlToBody(this._EnrollInStandupTrainingDetailsPanel);
            this._EnrollInStandupTrainingModal.AddControlToBody(this._EnrollInStandupTrainingModalHiddenLoadButton);
            this._EnrollInStandupTrainingModal.AddControlToBody(this._EnrollInStandupTrainingData);
            this._EnrollInStandupTrainingModal.AddControlToBody(this._EnrollInStandupTrainingSelectedStandupTrainingInstanceData);

            // add modal to container
            this.CourseInformationContainer.Controls.Add(this._EnrollInStandupTrainingModal);
        }
        #endregion

        #region _LoadEnrollInStandupTrainingModalContent
        /// <summary>
        /// Loads content for enroll in standup training modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadEnrollInStandupTrainingModalContent(object sender, EventArgs e)
        {
            try
            {
                // clear the modal
                this._EnrollInStandupTrainingModal.ClearFeedback();

                // make the submit and close buttons visible
                this._EnrollInStandupTrainingModal.SubmitButton.Visible = true;
                this._EnrollInStandupTrainingModal.CloseButton.Visible = true;

                // get the lesson data and standup training ids
                string[] enrollInStandupTrainingData = this._EnrollInStandupTrainingData.Value.Split('|');
                int idDataLesson = Convert.ToInt32(enrollInStandupTrainingData[0]);
                int idStandupTraining = Convert.ToInt32(enrollInStandupTrainingData[1]);

                // get the available instances
                StandupTraining standupTrainingObject = new StandupTraining(idStandupTraining);

                // add standup training module information to modal
                foreach (StandupTraining.LanguageSpecificProperty standupTrainingLSP in standupTrainingObject.LanguageSpecificProperties)
                {
                    if (standupTrainingLSP.LangString == AsentiaSessionState.UserCulture)
                    {
                        if (!String.IsNullOrWhiteSpace(standupTrainingLSP.Title))
                        {
                            Panel standupTrainingTitleContainer = new Panel();
                            standupTrainingTitleContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingTitleContainer";
                            standupTrainingTitleContainer.CssClass = "SelectStandupTrainingTitleContainer";

                            Literal standupTrainingTitle = new Literal();
                            standupTrainingTitle.Text = standupTrainingLSP.Title;
                            standupTrainingTitleContainer.Controls.Add(standupTrainingTitle);

                            this._EnrollInStandupTrainingDetailsPanel.Controls.Add(standupTrainingTitleContainer);
                        }

                        if (!String.IsNullOrWhiteSpace(standupTrainingLSP.Description))
                        {
                            Panel standupTrainingDescriptionContainer = new Panel();
                            standupTrainingDescriptionContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingDescriptionContainer";
                            standupTrainingDescriptionContainer.CssClass = "SelectStandupTrainingDescriptionContainer";

                            Literal standupTrainingDescription = new Literal();
                            standupTrainingDescription.Text = standupTrainingLSP.Description;
                            standupTrainingDescriptionContainer.Controls.Add(standupTrainingDescription);

                            this._EnrollInStandupTrainingDetailsPanel.Controls.Add(standupTrainingDescriptionContainer);
                        }

                        break;
                    }
                }

                // get the enrollable instances and put them in a radio button list
                DataTable enrollableInstances = standupTrainingObject.GetEnrollableInstances();

                if (enrollableInstances.Rows.Count > 0)
                {
                    // add instructions to modal
                    Panel selectSessionInstructionsContainer = new Panel();
                    selectSessionInstructionsContainer.ID = "SelectSessionInstructionsContainer";
                    this.FormatFormInformationPanel(selectSessionInstructionsContainer, _GlobalResources.PleaseSelectASessionToAttendAndClickOK, false);
                    this._EnrollInStandupTrainingDetailsPanel.Controls.Add(selectSessionInstructionsContainer);

                    // create a panel to house the available sessions
                    Panel availableStandupTrainingSessionsListingPanel = new Panel();
                    availableStandupTrainingSessionsListingPanel.ID = "AvailableStandupTrainingSessionsListingPanel";

                    // loop through available sessions creating a list item for each and listing all meeting times for each
                    int idStandupTrainingInstance = 0;
                    int meetingTimesCount = 0;                    

                    foreach (DataRow row in enrollableInstances.Rows)
                    {
                        if (idStandupTrainingInstance != Convert.ToInt32(row["idStandupTrainingInstance"]))
                        {
                            meetingTimesCount = 0;

                            // instance id
                            idStandupTrainingInstance = Convert.ToInt32(row["idStandupTrainingInstance"]);

                            // radio button selector
                            Panel standupTrainingInstanceSelectContainer = new Panel();
                            standupTrainingInstanceSelectContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingInstanceSelectContainer_" + idStandupTrainingInstance.ToString();
                            standupTrainingInstanceSelectContainer.CssClass = "SelectStandupTrainingSessionTitleContainer";

                            RadioButton standupTrainingInstanceSelect = new RadioButton();
                            standupTrainingInstanceSelect.ID = "EnrollInStandupTrainingModal_StandupTrainingInstanceSelect_" + idStandupTrainingInstance.ToString();
                            standupTrainingInstanceSelect.GroupName = "EnrollInStandupTrainingModal_StandupTrainingInstanceSelect";
                            standupTrainingInstanceSelect.Text = row["title"].ToString();

                            standupTrainingInstanceSelectContainer.Controls.Add(standupTrainingInstanceSelect);
                            availableStandupTrainingSessionsListingPanel.Controls.Add(standupTrainingInstanceSelectContainer);

                            // status label (seat(s), waiting seat(s))
                            Panel standupTrainingInstanceStatusLabelContainer = new Panel();
                            standupTrainingInstanceStatusLabelContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingInstanceStatusLabelContainer_" + idStandupTrainingInstance.ToString();
                            standupTrainingInstanceStatusLabelContainer.CssClass = "SelectStandupTrainingPropertyContainer";

                            Literal standupTrainingInstanceStatusLabel = new Literal();
                            standupTrainingInstanceStatusLabel.Text = _GlobalResources.Status + ": ";

                            Label standupTrainingInstanceSeats = new Label();
                            standupTrainingInstanceSeats.CssClass = "SelectStandupTrainingAvailableSeatsLabel";

                            int availableSeats = Convert.ToInt32(row["availableSeats"]);
                            int seats = Convert.ToInt32(row["seats"]);
                            int availableWaitingSeats = Convert.ToInt32(row["availableWaitingSeats"]);
                            int waitingSeats = Convert.ToInt32(row["waitingSeats"]);

                            if (availableSeats > 0)
                            {
                                standupTrainingInstanceSeats.Text = _GlobalResources.Seat_sAvailable + " (" + availableSeats + "/" + seats + ")";
                                standupTrainingInstanceSelect.InputAttributes.Add("waitinglist", "false");
                            }
                            else
                            {
                                standupTrainingInstanceSeats.Text = _GlobalResources.WaitingListAvailable + " (" + availableWaitingSeats + "/" + waitingSeats + ")";
                                standupTrainingInstanceSelect.InputAttributes.Add("waitinglist", "true");
                            }

                            standupTrainingInstanceStatusLabelContainer.Controls.Add(standupTrainingInstanceStatusLabel);
                            standupTrainingInstanceStatusLabelContainer.Controls.Add(standupTrainingInstanceSeats);
                            availableStandupTrainingSessionsListingPanel.Controls.Add(standupTrainingInstanceStatusLabelContainer);

                            // location label
                            Panel standupTrainingInstanceLocationLabelContainer = new Panel();
                            standupTrainingInstanceLocationLabelContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingInstanceLocationLabelContainer_" + idStandupTrainingInstance.ToString();
                            standupTrainingInstanceLocationLabelContainer.CssClass = "SelectStandupTrainingPropertyContainer";

                            Literal standupTrainingInstanceLocationLabel = new Literal();
                            standupTrainingInstanceLocationLabel.Text = _GlobalResources.Location + ": ";
                            
                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                if (!String.IsNullOrWhiteSpace(row["city"].ToString()) || !String.IsNullOrWhiteSpace(row["province"].ToString()))
                                {
                                    if (!String.IsNullOrWhiteSpace(row["city"].ToString()))
                                    { standupTrainingInstanceLocationLabel.Text += row["city"].ToString(); }

                                    if (!String.IsNullOrWhiteSpace(row["province"].ToString()))
                                    { standupTrainingInstanceLocationLabel.Text += ", " + row["province"].ToString(); }
                                }
                            }
                            else if (Convert.ToInt32(row["type"]) == 2)
                            { standupTrainingInstanceLocationLabel.Text += _GlobalResources.GoToMeeting; }
                            else if (Convert.ToInt32(row["type"]) == 3)
                            { standupTrainingInstanceLocationLabel.Text += _GlobalResources.GoToWebinar; }
                            else if (Convert.ToInt32(row["type"]) == 4)
                            { standupTrainingInstanceLocationLabel.Text += _GlobalResources.GoToTraining; }
                            else if (Convert.ToInt32(row["type"]) == 5)
                            { standupTrainingInstanceLocationLabel.Text += _GlobalResources.WebEx; }
                            else
                            { standupTrainingInstanceLocationLabel.Text += _GlobalResources.Online; }

                            standupTrainingInstanceLocationLabelContainer.Controls.Add(standupTrainingInstanceLocationLabel);
                            availableStandupTrainingSessionsListingPanel.Controls.Add(standupTrainingInstanceLocationLabelContainer);
                            
                            // instructors
                            if (!String.IsNullOrWhiteSpace(row["instructorDisplayNames"].ToString()))
                            {
                                // instructors label
                                Panel standupTrainingInstanceInstructorLabelContainer = new Panel();
                                standupTrainingInstanceInstructorLabelContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingInstanceInstructorLabelContainer_" + idStandupTrainingInstance.ToString();
                                standupTrainingInstanceInstructorLabelContainer.CssClass = "SelectStandupTrainingPropertyContainer";

                                Literal standupTrainingInstanceInstructorLabel = new Literal();
                                standupTrainingInstanceInstructorLabel.Text = _GlobalResources.Instructor_s + ":";

                                standupTrainingInstanceInstructorLabelContainer.Controls.Add(standupTrainingInstanceInstructorLabel);
                                availableStandupTrainingSessionsListingPanel.Controls.Add(standupTrainingInstanceInstructorLabelContainer);

                                // instructors
                                string[] instructors = row["instructorDisplayNames"].ToString().Split('|');

                                for (int i = 0; i < instructors.Length; i++)
                                {
                                    Panel standupTrainingInstanceInstructorContainer = new Panel();
                                    standupTrainingInstanceInstructorContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingInstanceInstructorContainer_" + idStandupTrainingInstance.ToString() + "_" + i.ToString();
                                    standupTrainingInstanceInstructorContainer.CssClass = "SelectStandupTrainingInstructorContainer";

                                    Literal standupTrainingInstanceInstructor = new Literal();
                                    standupTrainingInstanceInstructor.Text = instructors[i];

                                    standupTrainingInstanceInstructorContainer.Controls.Add(standupTrainingInstanceInstructor);
                                    availableStandupTrainingSessionsListingPanel.Controls.Add(standupTrainingInstanceInstructorContainer);
                                }
                            }

                            // meeting times label
                            Panel standupTrainingInstanceMeetingTimesLabelContainer = new Panel();
                            standupTrainingInstanceMeetingTimesLabelContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingInstanceMeetingTimesLabelContainer_" + idStandupTrainingInstance.ToString();
                            standupTrainingInstanceMeetingTimesLabelContainer.CssClass = "SelectStandupTrainingPropertyContainer";

                            Literal standupTrainingInstanceMeetingTimesLabel = new Literal();
                            standupTrainingInstanceMeetingTimesLabel.Text = _GlobalResources.MeetingTime_s + ":";

                            standupTrainingInstanceMeetingTimesLabelContainer.Controls.Add(standupTrainingInstanceMeetingTimesLabel);
                            availableStandupTrainingSessionsListingPanel.Controls.Add(standupTrainingInstanceMeetingTimesLabelContainer);

                            // meeting time
                            Panel standupTrainingInstanceMeetingTimeContainer = new Panel();
                            standupTrainingInstanceMeetingTimeContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingInstanceMeetingTimeContainer_" + idStandupTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                            standupTrainingInstanceMeetingTimeContainer.CssClass = "SelectStandupTrainingMeetingTimeContainer";

                            DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                            DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                            // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                int idTimezone = Convert.ToInt32(row["idTimezone"]);
                                string tzDotNetName = new Timezone(idTimezone).dotNetName;

                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            }
                            else
                            {
                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            }

                            Literal standupTrainingInstanceMeetingTime = new Literal();
                            standupTrainingInstanceMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                            standupTrainingInstanceMeetingTimeContainer.Controls.Add(standupTrainingInstanceMeetingTime);
                            availableStandupTrainingSessionsListingPanel.Controls.Add(standupTrainingInstanceMeetingTimeContainer);

                        }
                        else // additional meeting times
                        {
                            // meeting time
                            Panel standupTrainingInstanceMeetingTimeContainer = new Panel();
                            standupTrainingInstanceMeetingTimeContainer.ID = "EnrollInStandupTrainingModal_StandupTrainingInstanceMeetingTimeContainer_" + idStandupTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                            standupTrainingInstanceMeetingTimeContainer.CssClass = "SelectStandupTrainingMeetingTimeContainer";

                            DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                            DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                            // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                            if (Convert.ToInt32(row["type"]) == 1)
                            {
                                int idTimezone = Convert.ToInt32(row["idTimezone"]);
                                string tzDotNetName = new Timezone(idTimezone).dotNetName;

                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            }
                            else
                            {
                                dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            }

                            Literal standupTrainingInstanceMeetingTime = new Literal();
                            standupTrainingInstanceMeetingTime.Text = dtStart.ToString() + " - " + dtEnd.ToString();

                            standupTrainingInstanceMeetingTimeContainer.Controls.Add(standupTrainingInstanceMeetingTime);
                            availableStandupTrainingSessionsListingPanel.Controls.Add(standupTrainingInstanceMeetingTimeContainer);
                        }

                        meetingTimesCount++;
                    }

                    this._EnrollInStandupTrainingDetailsPanel.Controls.Add(availableStandupTrainingSessionsListingPanel);
                }
                else // no available sessions
                {
                    Panel noSessionsAvailableContainer = new Panel();
                    noSessionsAvailableContainer.ID = "EnrollInStandupTrainingModal_NoSessionsAvailableContainer";

                    Literal nosessionsAvailable = new Literal();
                    nosessionsAvailable.Text = _GlobalResources.ThereAreNoSessionsAvailable;
                    noSessionsAvailableContainer.Controls.Add(nosessionsAvailable);

                    this._EnrollInStandupTrainingDetailsPanel.Controls.Add(noSessionsAvailableContainer);
                    this._EnrollInStandupTrainingModal.SubmitButton.Visible = false;
                    this._EnrollInStandupTrainingModal.CloseButton.Visible = false;
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(dnfEx.Message, true);
                this._EnrollInStandupTrainingDetailsPanel.Controls.Clear();
                this._EnrollInStandupTrainingModal.SubmitButton.Visible = false;
                this._EnrollInStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(fnuEx.Message, true);
                this._EnrollInStandupTrainingDetailsPanel.Controls.Clear();
                this._EnrollInStandupTrainingModal.SubmitButton.Visible = false;
                this._EnrollInStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(cpeEx.Message, true);
                this._EnrollInStandupTrainingDetailsPanel.Controls.Clear();
                this._EnrollInStandupTrainingModal.SubmitButton.Visible = false;
                this._EnrollInStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(dEx.Message, true);
                this._EnrollInStandupTrainingDetailsPanel.Controls.Clear();
                this._EnrollInStandupTrainingModal.SubmitButton.Visible = false;
                this._EnrollInStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(ex.Message, true);
                this._EnrollInStandupTrainingDetailsPanel.Controls.Clear();
                this._EnrollInStandupTrainingModal.SubmitButton.Visible = false;
                this._EnrollInStandupTrainingModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _EnrollInStandupTraining_Command
        /// <summary>
        /// Handles the event initiated by the submit button in the enroll in standup training modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _EnrollInStandupTraining_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the lesson data and standup training ids
                string[] enrollInStandupTrainingData = this._EnrollInStandupTrainingData.Value.Split('|');
                int idDataLesson = Convert.ToInt32(enrollInStandupTrainingData[0]);
                int idStandupTraining = Convert.ToInt32(enrollInStandupTrainingData[1]);

                // get the selected standup training instance id and waitinglist bool
                string[] enrollInStandupTrainingSelectedStandupTrainingInstanceData = this._EnrollInStandupTrainingSelectedStandupTrainingInstanceData.Value.Split('|');
                int idStandupTrainingInstance = Convert.ToInt32(enrollInStandupTrainingSelectedStandupTrainingInstanceData[0]);
                bool isWaitingList = Convert.ToBoolean(enrollInStandupTrainingSelectedStandupTrainingInstanceData[1]);

                StandupTrainingInstance standupTrainingInstance = new StandupTrainingInstance(idStandupTrainingInstance);

                if (!isWaitingList)
                {
                    if (standupTrainingInstance.SeatsRemaining > 0)
                    {
                        // enroll in standup training instance
                        standupTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, false);
                        DataLesson.CommitToContentType(idDataLesson, Lesson.LessonContentType.StandupTrainingModule);

                        // display feedback, rebind lesson data grid, and update the update panel
                        this._EnrollInStandupTrainingDetailsPanel.Controls.Clear();
                        this._EnrollInStandupTrainingModal.SubmitButton.Visible = false;
                        this._EnrollInStandupTrainingModal.CloseButton.Visible = false;
                        this._EnrollInStandupTrainingModal.DisplayFeedback(_GlobalResources.YouHaveBeenJoinedToTheInstructorLedTrainingSessionSuccessfully, false);
                        this.CourseLessonsGrid.BindData();
                        this.CourseLessonsGridUpdatePanel.Update();
                    }
                    else
                    {
                        // clear the enroll in standup training panel and rebuild it
                        this._EnrollInStandupTrainingDetailsPanel.Controls.Clear();
                        this._LoadEnrollInStandupTrainingModalContent(sender, e);

                        // display error
                        this._EnrollInStandupTrainingModal.DisplayFeedback(_GlobalResources.ThereAreNoSeatsAvailableInTheSelectedInstructorLedTrainingSessionPleaseSelectAnotherSession, true);
                    }
                }
                else
                {
                    if (standupTrainingInstance.WaitingSeatsRemaining > 0)
                    {
                        // enroll in standup training instance waiting list
                        standupTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, true);
                        DataLesson.CommitToContentType(idDataLesson, Lesson.LessonContentType.StandupTrainingModule);

                        // display feedback, rebind lesson data grid, and update the update panel
                        this._EnrollInStandupTrainingDetailsPanel.Controls.Clear();
                        this._EnrollInStandupTrainingModal.SubmitButton.Visible = false;
                        this._EnrollInStandupTrainingModal.CloseButton.Visible = false;
                        this._EnrollInStandupTrainingModal.DisplayFeedback(_GlobalResources.YouHaveBeenJoinedToTheInstructorLedTrainingSession_sWaitingListSuccessfully, false);
                        this.CourseLessonsGrid.BindData();
                        this.CourseLessonsGridUpdatePanel.Update();
                    }
                    else
                    {
                        // clear the enroll in standup training panel and rebuild it
                        this._EnrollInStandupTrainingDetailsPanel.Controls.Clear();
                        this._LoadEnrollInStandupTrainingModalContent(sender, e);

                        // display error
                        this._EnrollInStandupTrainingModal.DisplayFeedback(_GlobalResources.ThereAreNoWaitingListSeatsAvailableInTheSelectedInstructorLedTrainingSessionPleaseSelectAnotherSession, true);
                    }
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EnrollInStandupTrainingModal.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _BuildViewEnrolledStandupTrainingModal
        /// <summary>
        /// Builds the modal popup for displaying the session details.
        /// </summary>
        private void _BuildViewEnrolledStandupTrainingModal()
        {
            //set modal properties
            this._ViewEnrolledStandupTrainingModal = new ModalPopup("ViewEnrolledStandupTrainingModal");
            this._ViewEnrolledStandupTrainingModal.Type = ModalPopupType.Form;
            this._ViewEnrolledStandupTrainingModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            this._ViewEnrolledStandupTrainingModal.HeaderIconAlt = _GlobalResources.EnrolledSessionInformation;
            this._ViewEnrolledStandupTrainingModal.HeaderText = _GlobalResources.EnrolledSessionInformation;
            this._ViewEnrolledStandupTrainingModal.TargetControlID = this._ViewEnrolledStandupTrainingModalHiddenLaunchButton.ID;
            this._ViewEnrolledStandupTrainingModal.SubmitButton.Visible = false;
            this._ViewEnrolledStandupTrainingModal.CloseButton.Visible = false;

            // build the modal body
            this._ViewEnrolledStandupTrainingModal.AddControlToBody(this._EnrolledStandupTrainingSessionInformationPanel);
            this._ViewEnrolledStandupTrainingModal.AddControlToBody(this._ViewEnrolledStandupTrainingModalHiddenLoadButton);
            this._ViewEnrolledStandupTrainingModal.AddControlToBody(this._EnrolledStandupTrainingData);

            // add modal to container
            this.CourseInformationContainer.Controls.Add(this._ViewEnrolledStandupTrainingModal);
        }
        #endregion

        #region _LoadViewEnrolledStandupTrainingModalContent
        /// <summary>
        /// Loads enrolled standup training instance information.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LoadViewEnrolledStandupTrainingModalContent(object sender, EventArgs e)
        {
            try
            {
                // clear the modal
                this._ViewEnrolledStandupTrainingModal.ClearFeedback();

                // make the submit and close buttons invisible
                this._ViewEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._ViewEnrolledStandupTrainingModal.CloseButton.Visible = false;

                // get the lesson data, standup training, and standup training instance ids
                string[] viewEnrolledStandupTrainingData = this._EnrolledStandupTrainingData.Value.Split('|');
                int idDataLesson = Convert.ToInt32(viewEnrolledStandupTrainingData[0]);
                int idStandupTraining = Convert.ToInt32(viewEnrolledStandupTrainingData[1]);
                int idStandupTrainingInstance = Convert.ToInt32(viewEnrolledStandupTrainingData[2]);

                // instansiate standup training and standup training instance objects
                StandupTraining standupTrainingObject = new StandupTraining(idStandupTraining);
                StandupTrainingInstance standupTrainingInstanceObject = new StandupTrainingInstance(idStandupTrainingInstance);
                StandUpTrainingInstanceToUserLink standupTrainingInstanceToUserLinkObject = new StandUpTrainingInstanceToUserLink(idStandupTrainingInstance, AsentiaSessionState.IdSiteUser);

                // get the standup training instance instructors
                DataTable standupTrainingInstanceInstructors = standupTrainingInstanceObject.GetInstructors(null);

                // determine if the instance has started (or is starting in 15 minutes)
                bool standupTrainingInstanceHasStarted = false;

                foreach (StandupTrainingInstance.MeetingTimeProperty standupTrainingInstanceMTP in standupTrainingInstanceObject.MeetingTimes)
                {
                    if (standupTrainingInstanceMTP.DtStart.AddMinutes(-15) <= AsentiaSessionState.UtcNow)
                    { standupTrainingInstanceHasStarted = true; }
                }

                // needed for icalendar later
                string standupTrainingTitleInLanguage = String.Empty;
                string standupTrainingDescriptionInLanguage = String.Empty;
                string standupTrainingInstanceTitleInLanguage = String.Empty;
                string standupTrainingInstanceLocationDescriptionInLanguage = String.Empty;

                // get standup training module information for iCalendar
                foreach (StandupTraining.LanguageSpecificProperty standupTrainingLSP in standupTrainingObject.LanguageSpecificProperties)
                {
                    if (standupTrainingLSP.LangString == AsentiaSessionState.UserCulture)
                    {
                        if (!String.IsNullOrWhiteSpace(standupTrainingLSP.Title))
                        { standupTrainingTitleInLanguage = standupTrainingLSP.Title; }

                        if (!String.IsNullOrWhiteSpace(standupTrainingLSP.Description))
                        { standupTrainingDescriptionInLanguage = standupTrainingLSP.Description; }

                        break;
                    }
                }

                // add enrolled standup training instance information to modal

                #region TitleAndDescription
                // title and description
                foreach (StandupTrainingInstance.LanguageSpecificProperty standupTrainingInstanceLSP in standupTrainingInstanceObject.LanguageSpecificProperties)
                {
                    if (standupTrainingInstanceLSP.LangString == AsentiaSessionState.UserCulture)
                    {
                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceLSP.Title))
                        {
                            standupTrainingInstanceTitleInLanguage = standupTrainingInstanceLSP.Title;

                            Panel standupTrainingInstanceTitleContainer = new Panel();
                            standupTrainingInstanceTitleContainer.ID = "ViewEnrolledStandupTrainingModal_StandupTrainingInstanceTitleContainer_" + idStandupTrainingInstance.ToString();
                            standupTrainingInstanceTitleContainer.CssClass = "SelectStandupTrainingSessionTitleContainer";

                            Literal standupTrainingInstanceTitle = new Literal();
                            standupTrainingInstanceTitle.Text = standupTrainingInstanceTitleInLanguage;
                            standupTrainingInstanceTitleContainer.Controls.Add(standupTrainingInstanceTitle);

                            this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(standupTrainingInstanceTitleContainer);
                        }

                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceLSP.Description))
                        {                            
                            Literal standupTrainingInstanceDescription = new Literal();
                            standupTrainingInstanceDescription.Text = standupTrainingInstanceLSP.Description;                            

                            // attach the controls
                            this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildFormField("ViewEnrolledStandupTrainingModal_StandupTrainingInstanceDescriptionContainer_" + idStandupTrainingInstance.ToString(),
                                                                                             _GlobalResources.Description,
                                                                                             standupTrainingInstanceDescription.ID,
                                                                                             standupTrainingInstanceDescription,
                                                                                             false,
                                                                                             false,
                                                                                             false));
                        }

                        break;
                    }
                }
                #endregion

                #region Location
                if (standupTrainingInstanceObject.Type == StandupTrainingInstance.MeetingType.Online) // online
                {
                    List<Control> onlineSessionControls = new List<Control>();

                    // location label
                    Panel locationContainer = new Panel();
                    locationContainer.Style.Add("margin-bottom", "5px");

                    Literal locationText = new Literal();
                    locationText.Text = _GlobalResources.Online;
                    locationContainer.Controls.Add(locationText);

                    onlineSessionControls.Add(locationContainer);

                    // show registration/attendance URLs only if the caller is enrolled
                    if (!standupTrainingInstanceToUserLinkObject.IsWaitingList)
                    {
                        // url register -- show until 15 minutes prior to session starting
                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceObject.UrlRegistration) && !standupTrainingInstanceHasStarted)
                        {
                            Panel urlRegisterContainer = new Panel();

                            Label urlRegisterLabel = new Label();
                            urlRegisterLabel.Text = _GlobalResources.Register + ": ";
                            urlRegisterContainer.Controls.Add(urlRegisterLabel);

                            HyperLink urlRegisterLink = new HyperLink();
                            urlRegisterLink.NavigateUrl = standupTrainingInstanceObject.UrlRegistration;
                            urlRegisterLink.Target = "_blank";
                            urlRegisterLink.Text = standupTrainingInstanceObject.UrlRegistration;
                            urlRegisterContainer.Controls.Add(urlRegisterLink);

                            onlineSessionControls.Add(urlRegisterContainer);
                        }

                        // url attend - only show 15 minutes prior to session starting                        
                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceObject.UrlAttend) && standupTrainingInstanceHasStarted)
                        {
                            Panel urlAttendContainer = new Panel();

                            Label urlAttendLabel = new Label();
                            urlAttendLabel.Text = _GlobalResources.Attend + ": ";
                            urlAttendContainer.Controls.Add(urlAttendLabel);

                            HyperLink urlAttendLink = new HyperLink();
                            urlAttendLink.NavigateUrl = standupTrainingInstanceObject.UrlAttend;
                            urlAttendLink.Target = "_blank";
                            urlAttendLink.Text = standupTrainingInstanceObject.UrlAttend;
                            urlAttendContainer.Controls.Add(urlAttendLink);

                            onlineSessionControls.Add(urlAttendContainer);
                        }
                    }

                    // attach the controls
                    this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ViewEnrolledStandupTrainingModal_" + "Location_" + idStandupTrainingInstance.ToString(),
                                                                                                                               _GlobalResources.Location,
                                                                                                                               onlineSessionControls,
                                                                                                                               false,
                                                                                                                               false));
                }
                else if (standupTrainingInstanceObject.Type == StandupTrainingInstance.MeetingType.GoToMeeting) // GTM
                {
                    List<Control> gtmSessionControls = new List<Control>();

                    // location label
                    Panel locationContainer = new Panel();
                    locationContainer.Style.Add("margin-bottom", "5px");

                    Image gtmImage = new Image();
                    gtmImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOMEETING, ImageFiles.EXT_PNG);
                    gtmImage.Style.Add("margin-right", "6px");
                    gtmImage.CssClass = "SmallIcon";
                    locationContainer.Controls.Add(gtmImage);

                    Literal locationText = new Literal();
                    locationText.Text = _GlobalResources.GoToMeeting;
                    locationContainer.Controls.Add(locationText);

                    gtmSessionControls.Add(locationContainer);

                    // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                    if (!standupTrainingInstanceToUserLinkObject.IsWaitingList)
                    {
                        // join link - only show 15 minutes prior to session starting                        
                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceObject.GenericJoinUrl) && standupTrainingInstanceHasStarted)
                        {
                            Panel joinLinkContainer = new Panel();

                            HyperLink joinLink = new HyperLink();
                            joinLink.NavigateUrl = standupTrainingInstanceObject.GenericJoinUrl;
                            joinLink.Target = "_blank";
                            joinLink.Text = _GlobalResources.JoinMeeting;
                            joinLinkContainer.Controls.Add(joinLink);

                            gtmSessionControls.Add(joinLinkContainer);
                        }
                    }

                    // attach the controls
                    this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                                               _GlobalResources.Location,
                                                                                                                               gtmSessionControls,
                                                                                                                               false,
                                                                                                                               false));
                }
                else if (standupTrainingInstanceObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar) // GTW
                {
                    List<Control> gtwSessionControls = new List<Control>();

                    // location label
                    Panel locationContainer = new Panel();
                    locationContainer.Style.Add("margin-bottom", "5px");

                    Image gtwImage = new Image();
                    gtwImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOWEBINAR, ImageFiles.EXT_PNG);
                    gtwImage.Style.Add("margin-right", "6px");
                    gtwImage.CssClass = "SmallIcon";
                    locationContainer.Controls.Add(gtwImage);

                    Literal locationText = new Literal();
                    locationText.Text = _GlobalResources.GoToWebinar;
                    locationContainer.Controls.Add(locationText);

                    gtwSessionControls.Add(locationContainer);

                    // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                    if (!standupTrainingInstanceToUserLinkObject.IsWaitingList)
                    {
                        // join link - only show 15 minutes prior to session starting                        
                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceToUserLinkObject.JoinUrl) && standupTrainingInstanceHasStarted)
                        {
                            Panel joinLinkContainer = new Panel();

                            HyperLink joinLink = new HyperLink();
                            joinLink.NavigateUrl = standupTrainingInstanceToUserLinkObject.JoinUrl;
                            joinLink.Target = "_blank";
                            joinLink.Text = _GlobalResources.JoinWebinar;
                            joinLinkContainer.Controls.Add(joinLink);

                            gtwSessionControls.Add(joinLinkContainer);
                        }
                    }

                    // attach the controls
                    this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                                               _GlobalResources.Location,
                                                                                                                               gtwSessionControls,
                                                                                                                               false,
                                                                                                                               false));
                }
                else if (standupTrainingInstanceObject.Type == StandupTrainingInstance.MeetingType.GoToTraining) // GTT
                {
                    List<Control> gttSessionControls = new List<Control>();

                    // location label
                    Panel locationContainer = new Panel();
                    locationContainer.Style.Add("margin-bottom", "5px");

                    Image gttImage = new Image();
                    gttImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOTRAINING, ImageFiles.EXT_PNG);
                    gttImage.Style.Add("margin-right", "6px");
                    gttImage.CssClass = "SmallIcon";
                    locationContainer.Controls.Add(gttImage);

                    Literal locationText = new Literal();
                    locationText.Text = _GlobalResources.GoToTraining;
                    locationContainer.Controls.Add(locationText);

                    gttSessionControls.Add(locationContainer);

                    // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                    if (!standupTrainingInstanceToUserLinkObject.IsWaitingList)
                    {
                        // join link - only show 15 minutes prior to session starting                        
                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceToUserLinkObject.JoinUrl) && standupTrainingInstanceHasStarted)
                        {
                            Panel joinLinkContainer = new Panel();

                            HyperLink joinLink = new HyperLink();
                            joinLink.NavigateUrl = standupTrainingInstanceToUserLinkObject.JoinUrl;
                            joinLink.Target = "_blank";
                            joinLink.Text = _GlobalResources.JoinTraining;
                            joinLinkContainer.Controls.Add(joinLink);

                            gttSessionControls.Add(joinLinkContainer);
                        }
                    }

                    // attach the controls
                    this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                                               _GlobalResources.Location,
                                                                                                                               gttSessionControls,
                                                                                                                               false,
                                                                                                                               false));
                }
                else if (standupTrainingInstanceObject.Type == StandupTrainingInstance.MeetingType.WebEx) // WebEx
                {
                    List<Control> wbxSessionControls = new List<Control>();

                    // location label
                    Panel locationContainer = new Panel();
                    locationContainer.Style.Add("margin-bottom", "5px");

                    Image wbxImage = new Image();
                    wbxImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_WEBEX, ImageFiles.EXT_PNG);
                    wbxImage.Style.Add("margin-right", "6px");
                    wbxImage.CssClass = "SmallIcon";
                    locationContainer.Controls.Add(wbxImage);

                    Literal locationText = new Literal();
                    locationText.Text = _GlobalResources.WebEx;
                    locationContainer.Controls.Add(locationText);

                    wbxSessionControls.Add(locationContainer);

                    // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                    if (!standupTrainingInstanceToUserLinkObject.IsWaitingList)
                    {
                        // join link - only show 15 minutes prior to session starting                        
                        if ((!String.IsNullOrWhiteSpace(standupTrainingInstanceToUserLinkObject.JoinUrl) || !String.IsNullOrWhiteSpace(standupTrainingInstanceObject.GenericJoinUrl)) && standupTrainingInstanceHasStarted)
                        {
                            Panel joinLinkContainer = new Panel();

                            HyperLink joinLink = new HyperLink();

                            if (!String.IsNullOrWhiteSpace(standupTrainingInstanceToUserLinkObject.JoinUrl))
                            { joinLink.NavigateUrl = standupTrainingInstanceToUserLinkObject.JoinUrl; }
                            else
                            { joinLink.NavigateUrl = standupTrainingInstanceObject.GenericJoinUrl; }

                            joinLink.Target = "_blank";
                            joinLink.Text = _GlobalResources.JoinMeeting;
                            joinLinkContainer.Controls.Add(joinLink);

                            if (!String.IsNullOrWhiteSpace(standupTrainingInstanceObject.MeetingPassword))
                            {
                                Literal meetingPasswordLit = new Literal();
                                meetingPasswordLit.Text = " (" + _GlobalResources.Password + ": " + standupTrainingInstanceObject.MeetingPassword + ")";
                                joinLinkContainer.Controls.Add(meetingPasswordLit);
                            }

                            wbxSessionControls.Add(joinLinkContainer);
                        }
                    }

                    // attach the controls
                    this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                                               _GlobalResources.Location,
                                                                                                                               wbxSessionControls,
                                                                                                                               false,
                                                                                                                               false));
                }
                else // classroom
                {
                    Literal locationText = new Literal();                    

                    if (!String.IsNullOrWhiteSpace(standupTrainingInstanceObject.City) || !String.IsNullOrWhiteSpace(standupTrainingInstanceObject.Province))
                    {
                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceObject.City))
                        { locationText.Text += standupTrainingInstanceObject.City; }

                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceObject.Province))
                        { locationText.Text += ", " + standupTrainingInstanceObject.Province; }
                    }

                    // attach the controls
                    this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildFormField("Location",
                                                                                                           _GlobalResources.Location,
                                                                                                           locationText.ID,
                                                                                                           locationText,
                                                                                                           false,
                                                                                                           false,
                                                                                                           false));
                }
                #endregion

                #region Location Description
                // location description
                foreach (StandupTrainingInstance.LanguageSpecificProperty standupTrainingInstanceLSP in standupTrainingInstanceObject.LanguageSpecificProperties)
                {
                    if (standupTrainingInstanceLSP.LangString == AsentiaSessionState.UserCulture)
                    {
                        if (!String.IsNullOrWhiteSpace(standupTrainingInstanceLSP.LocationDescription) && !standupTrainingInstanceToUserLinkObject.IsWaitingList)
                        {
                            standupTrainingInstanceLocationDescriptionInLanguage = standupTrainingInstanceLSP.LocationDescription;

                            Literal locationDescriptionText = new Literal();
                            locationDescriptionText.Text = standupTrainingInstanceLocationDescriptionInLanguage;

                            // attach the controls
                            this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildFormField("ViewEnrolledStandupTrainingModal_" + "LocationDescription_" + idStandupTrainingInstance.ToString(),
                                                                                                                   _GlobalResources.LocationDescription,
                                                                                                                   locationDescriptionText.ID,
                                                                                                                   locationDescriptionText,
                                                                                                                   false,
                                                                                                                   false,
                                                                                                                   false));
                        }

                        break;
                    }
                }
                #endregion

                #region Instructor
                if (standupTrainingInstanceInstructors.Rows.Count > 0)
                {
                    List<Control> instructorsContainer = new List<Control>();

                    int instructorsCount = 0;

                    foreach (DataRow standupTrainingInstanceInstructor in standupTrainingInstanceInstructors.Rows)
                    {
                        Panel standupTrainingInstanceInstructorContainer = new Panel();
                        standupTrainingInstanceInstructorContainer.ID = "ViewEnrolledStandupTrainingModal_StandupTrainingInstanceInstructorContainer_" + idStandupTrainingInstance.ToString() + "_" + instructorsCount.ToString();                        

                        Literal standupTrainingInstanceInstructorText = new Literal();
                        standupTrainingInstanceInstructorText.Text = standupTrainingInstanceInstructor["displayName"].ToString();

                        standupTrainingInstanceInstructorContainer.Controls.Add(standupTrainingInstanceInstructorText);
                        instructorsContainer.Add(standupTrainingInstanceInstructorContainer);

                        instructorsCount++;
                    }

                    // attach the controls
                    this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ViewEnrolledStandupTrainingModal_" + "Instructors_" + idStandupTrainingInstance.ToString(),
                                                                                                                               _GlobalResources.Instructor_s,
                                                                                                                               instructorsContainer,
                                                                                                                               false,
                                                                                                                               false));
                }
                #endregion

                #region Meeting Time(s)
                List<Control> meetingTimesContainer = new List<Control>();

                int meetingTimesCount = 0;

                foreach (StandupTrainingInstance.MeetingTimeProperty standupTrainingInstanceMTP in standupTrainingInstanceObject.MeetingTimes)
                {
                    Panel meetingTimePanel = new Panel();
                    meetingTimePanel.ID = "ViewEnrolledStandupTrainingModal_StandupTrainingInstanceMeetingTimeContainer_" + idStandupTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();

                    DateTime dtStart = Convert.ToDateTime(standupTrainingInstanceMTP.DtStart);
                    DateTime dtEnd = Convert.ToDateTime(standupTrainingInstanceMTP.DtEnd);                    

                    // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                    if (standupTrainingInstanceObject.Type == StandupTrainingInstance.MeetingType.Classroom)
                    {
                        int idTimezone = Convert.ToInt32(standupTrainingInstanceMTP.IdTimezone);
                        string tzDotNetName = new Timezone(idTimezone).dotNetName;

                        dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                        dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                    }
                    else
                    {
                        dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                        dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                    }

                    // meeting time values
                    Literal meetingTimesText = new Literal();
                    meetingTimesText.Text = Convert.ToString(dtStart) + " - " + Convert.ToString(dtEnd);
                    meetingTimePanel.Controls.Add(meetingTimesText);

                    // icalendar link
                    string icalendarContent = Utility.GetICalendarFormatedString(dtStart.ToString("o"), dtEnd.ToString("o"), standupTrainingTitleInLanguage + " - " + standupTrainingInstanceTitleInLanguage, standupTrainingInstanceLocationDescriptionInLanguage, standupTrainingDescriptionInLanguage);

                    LinkButton standupTrainingInstanceMeetingTimeICalendarLink = new LinkButton();
                    standupTrainingInstanceMeetingTimeICalendarLink.ID = "ViewEnrolledStandupTrainingModal_StandupTrainingInstanceMeetingTimeICalendarLink_" + idStandupTrainingInstance.ToString() + "_" + meetingTimesCount.ToString();
                    standupTrainingInstanceMeetingTimeICalendarLink.CssClass = "ICalendarLink";
                    standupTrainingInstanceMeetingTimeICalendarLink.OnClientClick = "Helper.CreateICalendarObject(); return false;";

                    Image icalendarImage = new Image();
                    icalendarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR, ImageFiles.EXT_PNG);
                    icalendarImage.CssClass = "XSmallIcon";

                    standupTrainingInstanceMeetingTimeICalendarLink.Controls.Add(icalendarImage);
                    meetingTimePanel.Controls.Add(standupTrainingInstanceMeetingTimeICalendarLink);

                    meetingTimesContainer.Add(meetingTimePanel);

                    meetingTimesCount++;
                }

                // attach the controls
                this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ViewEnrolledStandupTrainingModal_" + "MeetingTimes_" + idStandupTrainingInstance.ToString(),
                                                                                                                           _GlobalResources.MeetingTime_s,
                                                                                                                           meetingTimesContainer,
                                                                                                                           false,
                                                                                                                           false));
                #endregion

                #region Session Additional Information
                // if the user is waitlisted, let them know
                if (standupTrainingInstanceToUserLinkObject.IsWaitingList)
                {
                    List<Control> sessionAdditionalInfoControls = new List<Control>();

                    // on waitlist text
                    Panel waitlistTextContainer = new Panel();                    

                    Literal waitlistText = new Literal();
                    waitlistText.Text = _GlobalResources.YouAreCurrentlyOnTheWaitlistForThisSession;
                    waitlistTextContainer.Controls.Add(waitlistText);

                    sessionAdditionalInfoControls.Add(waitlistTextContainer);

                    // attach the controls
                    this._EnrolledStandupTrainingSessionInformationPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ViewEnrolledStandupTrainingModal_" + "SessionAdditionalInformation_" + idStandupTrainingInstance.ToString(),
                                                                                                                               String.Empty,
                                                                                                                               sessionAdditionalInfoControls,
                                                                                                                               false,
                                                                                                                               false));
                }
                #endregion
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._ViewEnrolledStandupTrainingModal.DisplayFeedback(dnfEx.Message, true);
                this._ViewEnrolledStandupTrainingModal.Controls.Clear();
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._ViewEnrolledStandupTrainingModal.DisplayFeedback(fnuEx.Message, true);
                this._ViewEnrolledStandupTrainingModal.Controls.Clear();
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._ViewEnrolledStandupTrainingModal.DisplayFeedback(cpeEx.Message, true);
                this._ViewEnrolledStandupTrainingModal.Controls.Clear();
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._ViewEnrolledStandupTrainingModal.DisplayFeedback(dEx.Message, true);
                this._ViewEnrolledStandupTrainingModal.Controls.Clear();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._ViewEnrolledStandupTrainingModal.DisplayFeedback(ex.Message, true);
                this._ViewEnrolledStandupTrainingModal.Controls.Clear();
            }
        }
        #endregion

        #region _DownloadCalendarFile
        /// <summary>
        /// Download the calendar file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DownloadCalendarFile(object sender, CommandEventArgs e)
        {
            // split the ICalendarContent to remove the new line characters
            string[] splitString = this._ICalendarContent.Value.Split(new string[] { @"\n" }, StringSplitOptions.RemoveEmptyEntries);

            // append each line separately to the calendar file
            StringBuilder sb = new StringBuilder();
            foreach (string s in splitString)
            {
                sb.AppendLine(s);
            }

            // download the calendar file
            HttpContext.Current.Response.ContentType = "text/calendar;charset-utf8";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=AsentiaMeeting.ics");
            HttpContext.Current.Response.Write(sb.ToString());
            HttpContext.Current.Response.End();
        }
        #endregion

        #region _BuildRemoveEnrolledStandupTrainingModal
        /// <summary>
        /// Builds the modal popup for removing an enrolled standup training session.
        /// </summary>
        private void _BuildRemoveEnrolledStandupTrainingModal()
        {
            //set modal properties
            this._RemoveEnrolledStandupTrainingModal = new ModalPopup("RemoveEnrolledStandupTrainingModal");
            this._RemoveEnrolledStandupTrainingModal.Type = ModalPopupType.Form;
            this._RemoveEnrolledStandupTrainingModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);
            this._RemoveEnrolledStandupTrainingModal.HeaderIconAlt = _GlobalResources.DropInstructorLedTrainingSession;
            this._RemoveEnrolledStandupTrainingModal.HeaderText = _GlobalResources.DropInstructorLedTrainingSession;
            this._RemoveEnrolledStandupTrainingModal.SubmitButtonTextType = ModalPopupButtonText.Yes;
            this._RemoveEnrolledStandupTrainingModal.CloseButtonTextType = ModalPopupButtonText.No;

            this._RemoveEnrolledStandupTrainingModal.TargetControlID = this._RemoveEnrolledStandupTrainingModalHiddenLaunchButton.ID;
            this._RemoveEnrolledStandupTrainingModal.SubmitButton.Command += new CommandEventHandler(this._RemoveEnrolledStandupTraining_Command);

            // build the modal body
            this._RemoveEnrolledStandupTrainingModal.AddControlToBody(this._RemoveEnrolledStandupTrainingSessionConfirmationPanel);
            this._RemoveEnrolledStandupTrainingModal.AddControlToBody(this._RemoveEnrolledStandupTrainingModalHiddenLoadButton);

            // add modal to container
            this.CourseInformationContainer.Controls.Add(this._RemoveEnrolledStandupTrainingModal);
        }
        #endregion

        #region _LoadRemoveEnrolledStandupTrainingModalContent
        /// <summary>
        /// Loads content for remove enrolled standup training modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadRemoveEnrolledStandupTrainingModalContent(object sender, EventArgs e)
        {
            try
            {
                // clear the modal
                this._RemoveEnrolledStandupTrainingModal.ClearFeedback();

                // make the submit and close buttons visible
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = true;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = true;

                // build the confirmation message
                HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
                Literal body1 = new Literal();
                body1.Text = _GlobalResources.AreYouSureYouWantToDropOutOfThisSession;
                body1Wrapper.Controls.Add(body1);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Add(body1Wrapper);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(dnfEx.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(fnuEx.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(cpeEx.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(dEx.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(ex.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _RemoveEnrolledStandupTraining_Command
        /// <summary>
        /// Handles the event initiated by the button click for removing learner from standup training instance.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _RemoveEnrolledStandupTraining_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the lesson data and standup training instance ids
                string[] viewEnrolledStandupTrainingData = this._EnrolledStandupTrainingData.Value.Split('|');
                int idDataLesson = Convert.ToInt32(viewEnrolledStandupTrainingData[0]);
                int idStandupTrainingInstance = Convert.ToInt32(viewEnrolledStandupTrainingData[1]);

                // remove learner from standup training instance
                StandupTrainingInstance standupTrainingInstance = new StandupTrainingInstance(idStandupTrainingInstance);
                standupTrainingInstance.RemoveUser(AsentiaSessionState.IdSiteUser);
                DataLesson.CommitToContentType(idDataLesson, null);

                // display feedback, rebind lesson data grid, and update the update panel
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(_GlobalResources.YouHaveBeenRemovedFromTheInstructorLedTrainingSessionSuccessfully, false);
                this.CourseLessonsGrid.BindData();
                this.CourseLessonsGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(dnfEx.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(fnuEx.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(cpeEx.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(dEx.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._RemoveEnrolledStandupTrainingModal.DisplayFeedback(ex.Message, true);
                this._RemoveEnrolledStandupTrainingSessionConfirmationPanel.Controls.Clear();
                this._RemoveEnrolledStandupTrainingModal.SubmitButton.Visible = false;
                this._RemoveEnrolledStandupTrainingModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildTaskModal
        /// <summary>
        /// Builds the modal popup for task.
        /// </summary>
        private void _BuildTaskModal()
        {
            //set modal properties
            this._TaskModal = new ModalPopup("TaskModal");
            this._TaskModal.Type = ModalPopupType.Form;
            this._TaskModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_TASK, ImageFiles.EXT_PNG);
            this._TaskModal.HeaderIconAlt = _GlobalResources.Task;
            this._TaskModal.HeaderText = _GlobalResources.Task;
            this._TaskModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._TaskModal.TargetControlID = this._TaskModalLaunchButton.ID;
            this._TaskModal.SubmitButton.Command += new CommandEventHandler(this._TaskSubmit_Command);

            // build the upload control here instead of within the "load" method for this modal
            // the uploader will be maintained within its own panel and it all needs to be done here for page lifecycle reasons

            // uploader error container
            Panel taskUploaderErrorContainer = new Panel();
            taskUploaderErrorContainer.ID = "TaskUploader_ErrorContainer";
            taskUploaderErrorContainer.CssClass = "FormFieldErrorContainer";
            this._TaskModalUploaderPanel.Controls.Add(taskUploaderErrorContainer);

            // attach the task uploader
            this._TaskModalUploaderPanel.Controls.Add(this._TaskUploader);

            // build the modal body
            this._TaskModal.AddControlToBody(this._TaskModalFormPanel);
            this._TaskModal.AddControlToBody(this._TaskModalUploaderPanel);
            this._TaskModal.AddControlToBody(this._TaskModalLoadButton);
            this._TaskModal.AddControlToBody(this._TaskData);
            this._TaskModal.AddControlToBody(this._TaskRemoveUploadedFile);

            // add modal to container
            this.CourseInformationContainer.Controls.Add(this._TaskModal);
        }
        #endregion

        #region _LoadTaskModalContent
        /// <summary>
        /// Loads content for task modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadTaskModalContent(object sender, EventArgs e)
        {
            try
            {
                // clear the modal
                this._TaskModal.ClearFeedback();

                // make the submit and close buttons visible
                this._TaskModal.SubmitButton.Visible = true;
                this._TaskModal.CloseButton.Visible = true;

                // get the lesson data id, lesson id, content type committed to, content completion status, task resource path, uploaded task filename, and task uploaded date
                string[] taskData = this._TaskData.Value.Split('|');
                int idDataLesson = Convert.ToInt32(taskData[0]);
                int idLesson = Convert.ToInt32(taskData[1]);

                Lesson.LessonContentType? contentTypeCommittedTo = null;
                if (!String.IsNullOrWhiteSpace(taskData[2]))
                { contentTypeCommittedTo = (Lesson.LessonContentType)Convert.ToInt32(taskData[2]); }

                string contentCompletionStatus = taskData[3];
                string taskResourcePath = taskData[4];
                string uploadedTaskFilename = taskData[5];

                DateTime? taskUploadedDate = null;
                if (!String.IsNullOrWhiteSpace(taskData[6]))
                {
                    taskUploadedDate = Convert.ToDateTime(taskData[6]);

                    // convert to user's local time
                    taskUploadedDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)taskUploadedDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                }

                Lesson.TaskDocumentType? taskDocumentType = null;
                if (!String.IsNullOrWhiteSpace(taskData[7]))
                { taskDocumentType = (Lesson.TaskDocumentType)Convert.ToInt32(taskData[7]); }

                // TASK RESOUCRE

                if (!String.IsNullOrWhiteSpace(taskResourcePath))
                {
                    // task resource label
                    Panel taskResourceLabelContainer = new Panel();
                    taskResourceLabelContainer.ID = "TaskModal_TaskResourceLabelContainer";

                    Label taskResourceLabel = new Label();
                    taskResourceLabel.Text = _GlobalResources.TaskResource + ": ";
                    taskResourceLabelContainer.Controls.Add(taskResourceLabel);

                    // task resource link
                    Panel taskResourceLinkContainer = new Panel();
                    taskResourceLinkContainer.ID = "TaskModal_TaskResourceLinkContainer";

                    HyperLink taskResourceLink = new HyperLink();
                    taskResourceLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LESSON + idLesson + "/" + taskResourcePath;
                    taskResourceLink.Target = "_blank";
                    taskResourceLink.Text = taskResourcePath;
                    taskResourceLinkContainer.Controls.Add(taskResourceLink);

                    this._TaskModalFormPanel.Controls.Add(taskResourceLabelContainer);
                    this._TaskModalFormPanel.Controls.Add(taskResourceLinkContainer);
                }

                // TASK UPLOAD

                // upload task label
                Panel uploadTaskLabelContainer = new Panel();
                uploadTaskLabelContainer.ID = "TaskModal_UploadTaskLabelContainer";

                Label uploadTaskLabel = new Label();
                uploadTaskLabel.Text = _GlobalResources.UploadTask + ": ";
                uploadTaskLabelContainer.Controls.Add(uploadTaskLabel);

                this._TaskModalFormPanel.Controls.Add(uploadTaskLabelContainer);

                // uploaded task
                Panel uploadedTaskContainer = new Panel();
                uploadedTaskContainer.ID = "TaskModal_UploadedTaskContainer";

                if (!String.IsNullOrWhiteSpace(uploadedTaskFilename))
                {
                    // uploaded task link
                    HyperLink uploadedTaskLink = new HyperLink();
                    uploadedTaskLink.NavigateUrl = SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/Tasks/" + idDataLesson.ToString() + "/" + uploadedTaskFilename;
                    uploadedTaskLink.Target = "_blank";
                    uploadedTaskLink.Text = uploadedTaskFilename;
                    uploadedTaskContainer.Controls.Add(uploadedTaskLink);

                    // allow removal if the lesson is not completed
                    if (contentCompletionStatus != "completed")
                    {
                        Image uploadedTaskRemoveLink = new Image();
                        uploadedTaskRemoveLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
                        uploadedTaskRemoveLink.CssClass = "XSmallIcon";
                        uploadedTaskRemoveLink.Style.Add("cursor", "pointer");
                        uploadedTaskRemoveLink.Attributes.Add("onclick", "javascript:RemoveUploadedTask();");
                        uploadedTaskContainer.Controls.Add(uploadedTaskRemoveLink);
                    }

                    // uploaded on
                    Panel uploadedTaskUploadDateContainer = new Panel();
                    uploadedTaskUploadDateContainer.ID = "TaskModal_UploadedTaskUploadDateContainer";

                    Literal uploadedTaskUploadDate = new Literal();
                    uploadedTaskUploadDate.Text = _GlobalResources.YourTaskWasSubmittedOn + " " + taskUploadedDate.ToString();
                    uploadedTaskUploadDateContainer.Controls.Add(uploadedTaskUploadDate);

                    uploadedTaskContainer.Controls.Add(uploadedTaskUploadDateContainer);
                }

                this._TaskModalFormPanel.Controls.Add(uploadedTaskContainer);

                // upload task - allow only if lesson is not completed
                if (contentCompletionStatus != "completed")
                {
                    this._TaskModalUploaderPanel.Visible = true;

                    Panel allowedDocumentTypeInstructionsPanel = new Panel();
                    allowedDocumentTypeInstructionsPanel.ID = "TaskModal_AllowedDocumentTypeInstructionsPanel";

                    // set the allowed upload file type
                    switch (taskDocumentType)
                    {
                        case Lesson.TaskDocumentType.Excel:
                            this._TaskUploader.TaskValidExtensions = "xls, xlsx";
                            this.FormatFormInformationPanel(allowedDocumentTypeInstructionsPanel, String.Format(_GlobalResources.YourFileMustBeUploadedInXFormat, _GlobalResources.MicrosoftOfficeExcelXLSXLSX));
                            break;
                        case Lesson.TaskDocumentType.Image:
                            this._TaskUploader.TaskValidExtensions = "jpeg, jpg, png, gif";
                            this.FormatFormInformationPanel(allowedDocumentTypeInstructionsPanel, String.Format(_GlobalResources.YourFileMustBeUploadedInXFormat, _GlobalResources.ImageJPEGJPGPNGGIF));
                            break;
                        case Lesson.TaskDocumentType.PDF:
                            this._TaskUploader.TaskValidExtensions = "pdf";
                            this.FormatFormInformationPanel(allowedDocumentTypeInstructionsPanel, String.Format(_GlobalResources.YourFileMustBeUploadedInXFormat, _GlobalResources.AdobeAcrobatPDF));
                            break;
                        case Lesson.TaskDocumentType.PowerPoint:
                            this._TaskUploader.TaskValidExtensions = "ppt, pptx";
                            this.FormatFormInformationPanel(allowedDocumentTypeInstructionsPanel, String.Format(_GlobalResources.YourFileMustBeUploadedInXFormat, _GlobalResources.MicrosoftOfficePowerPointPPTPPTX));
                            break;
                        case Lesson.TaskDocumentType.Text:
                            this._TaskUploader.TaskValidExtensions = "txt";
                            this.FormatFormInformationPanel(allowedDocumentTypeInstructionsPanel, String.Format(_GlobalResources.YourFileMustBeUploadedInXFormat, _GlobalResources.TextTXT));
                            break;
                        case Lesson.TaskDocumentType.Word:
                            this._TaskUploader.TaskValidExtensions = "doc, docx";
                            this.FormatFormInformationPanel(allowedDocumentTypeInstructionsPanel, String.Format(_GlobalResources.YourFileMustBeUploadedInXFormat, _GlobalResources.MicrosoftOfficeWordDOCDOCX));
                            break;
                        case Lesson.TaskDocumentType.Zip:
                            this._TaskUploader.TaskValidExtensions = "zip";
                            this.FormatFormInformationPanel(allowedDocumentTypeInstructionsPanel, String.Format(_GlobalResources.YourFileMustBeUploadedInXFormat, _GlobalResources.ZipZIP));
                            break;
                        case Lesson.TaskDocumentType.Mp4:
                            this._TaskUploader.TaskValidExtensions = "mp4";
                            this.FormatFormInformationPanel(allowedDocumentTypeInstructionsPanel, String.Format(_GlobalResources.YourFileMustBeUploadedInXFormat, _GlobalResources.VideoMP4));
                            break;
                    }

                    // attach uploader instructions
                    this._TaskModalUploaderPanel.Controls.AddAt(0, allowedDocumentTypeInstructionsPanel);

                    // build instructions for the modal - we do this here because by now we have a clear picture of exactly what set of instructions to display
                    Panel modalInstructions = new Panel();
                    modalInstructions.ID = "TaskModal_ModalInstructions";
                    this.FormatFormInformationPanel(modalInstructions, _GlobalResources.YouMaySubmitOrWithdrawYourTaskByUsingTheFormBelowBeSureToClickTheSubmitButtonToSubmitYourChanges);
                    this._TaskModalFormPanel.Controls.AddAt(0, modalInstructions);
                }
                else
                {
                    // hide anything that would allow user to change data
                    this._TaskModalUploaderPanel.Visible = false;
                    this._TaskModal.SubmitButton.Visible = false;
                    this._TaskModal.CloseButton.Visible = false;

                    // build instructions for the modal - we do this here because by now we have a clear picture of exactly what set of instructions to display
                    Panel modalInstructions = new Panel();
                    modalInstructions.ID = "TaskModal_ModalInstructions";
                    this.FormatFormInformationPanel(modalInstructions, _GlobalResources.YourTaskHasBeenCompletedYouMayReviewYourTaskAndOrTheTaskResourceBelow);
                    this._TaskModalFormPanel.Controls.AddAt(0, modalInstructions);
                }

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(dnfEx.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(fnuEx.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(cpeEx.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(dEx.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(ex.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _TaskSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the task modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _TaskSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the lesson data id and uploaded task filename
                string[] taskData = this._TaskData.Value.Split('|');
                int idDataLesson = Convert.ToInt32(taskData[0]);
                string uploadedTaskFilename = taskData[5];

                // get the remove uploaded file flag
                bool removeUploadedFile = Convert.ToBoolean(this._TaskRemoveUploadedFile.Value);

                // if the uploader doesn't have a file and we're not flagged to remove an existing file, then no changes were made, error
                if (
                    (this._TaskUploader.SavedFilePath == null && removeUploadedFile == false)
                    || (this._TaskUploader.SavedFilePath == null && removeUploadedFile == true && String.IsNullOrWhiteSpace(uploadedTaskFilename))
                   )
                {
                    // clear the task form panel and rebuild it
                    this._TaskModalFormPanel.Controls.Clear();
                    this._LoadTaskModalContent(sender, e);

                    // display error
                    this._TaskModal.DisplayFeedback(_GlobalResources.YouHaveNotMadeAnyChangesPleaseTryAgain, true);
                }
                else // there were changes made, figure them out and commit them
                {
                    if (removeUploadedFile)
                    {
                        // uncommit from the task lesson, and remove the uploaded file
                        DataLesson.ClearTask(idDataLesson);
                        DataLesson.CommitToContentType(idDataLesson, null);

                        if (File.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/Tasks/" + idDataLesson.ToString() + "/" + uploadedTaskFilename)))
                        { File.Delete(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/Tasks/" + idDataLesson.ToString() + "/" + uploadedTaskFilename)); }

                        // display feedback, rebind lesson data grid, and update the update panel
                        this._TaskModalFormPanel.Controls.Clear();
                        this._TaskModalUploaderPanel.Visible = false;
                        this._TaskModal.SubmitButton.Visible = false;
                        this._TaskModal.CloseButton.Visible = false;
                        this._TaskModal.DisplayFeedback(_GlobalResources.YouHaveWithdrawnYourTaskSuccessfully, false);
                        this.CourseLessonsGrid.BindData();
                        this.CourseLessonsGridUpdatePanel.Update();
                    }
                    else // it is implied that the uploader has a file because the other case in which removeUploadedFile could be false throws an error above
                    {
                        // check for folder existence and create if necessary
                        if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/Tasks/" + idDataLesson.ToString())))
                        { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/Tasks/" + idDataLesson.ToString())); }

                        // remove old file if it exists
                        if (!String.IsNullOrWhiteSpace(uploadedTaskFilename))
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/Tasks/" + idDataLesson.ToString() + "/" + uploadedTaskFilename)))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/Tasks/" + idDataLesson.ToString() + "/" + uploadedTaskFilename)); }
                        }

                        // save the new file, and remove the uploaded file from the upload folder
                        if (File.Exists(Server.MapPath(this._TaskUploader.SavedFilePath)))
                        {
                            File.Copy(Server.MapPath(this._TaskUploader.SavedFilePath), Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/Tasks/" + idDataLesson.ToString() + "/" + this._TaskUploader.FileOriginalName), true);
                            File.Delete(Server.MapPath(this._TaskUploader.SavedFilePath));
                            uploadedTaskFilename = this._TaskUploader.FileOriginalName;
                        }
                        else
                        { throw new AsentiaException(_GlobalResources.AnUnknownErrorOccurredWhileProcessingYourRequest); }

                        // save the task data and commit to the task
                        DataLesson.SaveTask(idDataLesson, uploadedTaskFilename);
                        DataLesson.CommitToContentType(idDataLesson, Lesson.LessonContentType.Task);

                        // display feedback, rebind lesson data grid, and update the update panel
                        this._TaskModalFormPanel.Controls.Clear();
                        this._TaskModalUploaderPanel.Visible = false;
                        this._TaskModal.SubmitButton.Visible = false;
                        this._TaskModal.CloseButton.Visible = false;
                        this._TaskModal.DisplayFeedback(_GlobalResources.YouHaveSubmittedYourTaskSuccessfully, false);
                        this.CourseLessonsGrid.BindData();
                        this.CourseLessonsGridUpdatePanel.Update();
                    }
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(dnfEx.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(fnuEx.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(cpeEx.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(dEx.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._TaskModal.DisplayFeedback(ex.Message, true);
                this._TaskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskModal.SubmitButton.Visible = false;
                this._TaskModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _BuildOJTModal
        /// <summary>
        /// Builds the modal popup for OJT.
        /// </summary>
        private void _BuildOJTModal()
        {
            // set modal properties
            this._OJTModal = new ModalPopup("OJTModal");
            this._OJTModal.Type = ModalPopupType.Form;
            this._OJTModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_OJT, ImageFiles.EXT_PNG);
            this._OJTModal.HeaderIconAlt = _GlobalResources.OJT;
            this._OJTModal.HeaderText = _GlobalResources.OJT;

            this._OJTModal.TargetControlID = this._OJTModalLaunchButton.ID;
            this._OJTModal.SubmitButton.Command += new CommandEventHandler(this._OJTRequest_Command);
            this._OJTModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._OJTModal.SubmitButtonCustomText = _GlobalResources.SubmitRequest;
            this._OJTModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            // add a button to cancel the ojt request - this remains hidden until needed
            this._OJTCancelRequestButton = new Button();
            this._OJTCancelRequestButton.ID = this._OJTModal.ID + "_CancelRequestButton";
            this._OJTCancelRequestButton.CssClass = "Button ActionButton";
            this._OJTCancelRequestButton.Text = _GlobalResources.CancelRequest;
            this._OJTCancelRequestButton.Visible = false;
            this._OJTCancelRequestButton.Command += new CommandEventHandler(this._OJTCancel_Command);

            this._OJTModal.AddControlToButtonsContainer(this._OJTCancelRequestButton);

            // build the modal body
            this._OJTModal.AddControlToBody(this._OJTModalFormPanel);

            this._OJTModal.AddControlToBody(this._OJTModalLoadButton);
            this._OJTModal.AddControlToBody(this._OJTData);

            // add modal to container
            this.CourseInformationContainer.Controls.Add(this._OJTModal);
        }
        #endregion

        #region _LoadOJTModalContent
        /// <summary>
        /// Loads content for OJT modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadOJTModalContent(object sender, EventArgs e)
        {
            try
            {
                // clear the modal
                this._OJTModal.ClearFeedback();

                // make the submit and close buttons visible
                this._OJTModal.SubmitButton.Visible = true;
                this._OJTModal.CloseButton.Visible = true;
                this._OJTCancelRequestButton.Visible = false;

                // get the lesson data id, content type committed to, and contentCompletionStatus
                string[] ojtData = this._OJTData.Value.Split('|');
                int idDataLesson = Convert.ToInt32(ojtData[0]);

                Lesson.LessonContentType? contentTypeCommittedTo = null;
                if (!String.IsNullOrWhiteSpace(ojtData[1]))
                { contentTypeCommittedTo = (Lesson.LessonContentType)Convert.ToInt32(ojtData[1]); }

                string contentCompletionStatus = ojtData[2];

                if (contentTypeCommittedTo == Lesson.LessonContentType.OJT) // ojt has already been committed to
                {

                    if (contentCompletionStatus != "completed") // allow cancellation of ojt request
                    {
                        // hide the submit and close buttons 
                        this._OJTModal.SubmitButton.Visible = false;
                        this._OJTModal.CloseButton.Visible = false;
                        this._OJTCancelRequestButton.Visible = true;

                        // modal body
                        HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
                        Literal body1 = new Literal();
                        body1.Text = _GlobalResources.YouHaveRequestedOJTForThisModuleIfYouWishToCancelThisRequestPleaseClickTheCancelRequestButtonBelow;
                        body1Wrapper.Controls.Add(body1);

                        this._OJTModalFormPanel.Controls.Add(body1Wrapper);


                    }
                    else // let the learner know their ojt is done
                    {
                        // hide the submit and close buttons 
                        this._OJTModal.SubmitButton.Visible = false;
                        this._OJTModal.CloseButton.Visible = false;
                        this._OJTCancelRequestButton.Visible = false;

                        // modal body
                        HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
                        Literal body1 = new Literal();
                        body1.Text = _GlobalResources.YourOJTForThisModuleHasBeenCompleted;
                        body1Wrapper.Controls.Add(body1);
                        this._OJTModalFormPanel.Controls.Add(body1Wrapper);
                    }
                }
                else // prompt the user to commit to ojt
                {
                    // modal body
                    HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
                    Literal body1 = new Literal();
                    body1.Text = _GlobalResources.PleaseClickTheSubmitRequestButtonToRequestOJTForThisModule;
                    body1Wrapper.Controls.Add(body1);
                    this._OJTModalFormPanel.Controls.Add(body1Wrapper);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(dnfEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(fnuEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(cpeEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(dEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(ex.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
        }
        #endregion

        #region _OJTRequest_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the OJT modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _OJTRequest_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the lesson data id
                string[] ojtData = this._OJTData.Value.Split('|');
                int idDataLesson = Convert.ToInt32(ojtData[0]);

                // all we have to do to request ojt is set the learner's commitment to it, so do that
                DataLesson.CommitToContentType(idDataLesson, Lesson.LessonContentType.OJT);

                // display feedback, rebind lesson data grid, and update the update panel
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
                this._OJTModal.DisplayFeedback(_GlobalResources.YourOJTRequestForThisModuleHasBeenSubmittedSuccessfully, false);
                this.CourseLessonsGrid.BindData();
                this.CourseLessonsGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(dnfEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(fnuEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(cpeEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(dEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(ex.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
        }
        #endregion

        #region _OJTCancel_Command
        /// <summary>
        /// Handles the event initiated by the "Cancel Request" button click on the OJT modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _OJTCancel_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the lesson data id
                string[] ojtData = this._OJTData.Value.Split('|');
                int idDataLesson = Convert.ToInt32(ojtData[0]);

                // all we have to do to cancel ojt is remove the learner's commitment to it, so do that
                DataLesson.CommitToContentType(idDataLesson, null);

                // display feedback, rebind lesson data grid, and update the update panel
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
                this._OJTModal.DisplayFeedback(_GlobalResources.YouHaveCanceledYourOJTRequestForThisModuleSuccessfully, false);
                this.CourseLessonsGrid.BindData();
                this.CourseLessonsGridUpdatePanel.Update();
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(dnfEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(fnuEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(cpeEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(dEx.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._OJTModal.DisplayFeedback(ex.Message, true);
                this._OJTModalFormPanel.Controls.Clear();
                this._OJTModal.SubmitButton.Visible = false;
                this._OJTModal.CloseButton.Visible = false;
                this._OJTCancelRequestButton.Visible = false;
            }
        }
        #endregion
    }
}