﻿function LoadCertificationTaskModalContent(idCertificationToUserLink, idDataCertificationModuleRequirement, isDocumentationUploadRequired, uploadedTaskFilename) {
    $("#TaskData").val(idCertificationToUserLink + "|" + idDataCertificationModuleRequirement + "|" + isDocumentationUploadRequired + "|" + uploadedTaskFilename);

    $("#TaskSubmitModalLaunchButton").click();
    $("#TaskSubmitModalLoadButton").click();    

    // clear out the uploader's values
    $("#TaskUploader_Field_CompletedPanel").html("");
    $("#TaskUploader_ErrorContainer").html("");
    $("#TaskUploader_Field_UploadControl input").removeAttr("style");
    $("#TaskUploader_Field_UploadHiddenFieldOriginalFileName").val("");
    $("#TaskUploader_Field_UploadHiddenField").val("");
}

function TaskUploaded() {
    $("#TaskModal_UploadedTaskContainer").html("");
    $("#TaskModal_UploadedTaskContainer").html($("#TaskUploader_Field_UploadHiddenFieldOriginalFileName").val());    
}