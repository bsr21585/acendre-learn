﻿$(document).ready(function () {
    $(".MultipleContentMethodLaunchContainer").each(function () {
        $("#" + this.id).click(function (event) {
            event.stopPropagation();
        });
    });

    $(document).click(function () {
        if ($(".MultipleContentMethodLaunchItemContainer").is(":visible")) {
            $(".MultipleContentMethodLaunchItemContainer").hide();
        }
    });
});

function ToggleMultipleContentMenu(idDataLesson) {
    $(".MultipleContentMethodLaunchContainer").each(function () {
        $("#" + this.id).click(function (event) {
            event.stopPropagation();
        });
    });

    var launchItemContainerId = "MultipleContentMethodLaunchItemContainer_" + idDataLesson;

    $(".MultipleContentMethodLaunchItemContainer").each(function () {
        if (this.id != launchItemContainerId) {
            $("#" + this.id).hide();
        }
    });

    $("#" + launchItemContainerId).toggle();
}

function LoadEnrollInStandupTrainingModalContent(idDataLesson, idStandupTraining) {
    $("#" + EnrollInStandupTrainingDataId).val(idDataLesson + "|" + idStandupTraining);
    $("#" + EnrollInStandupTrainingModalHiddenLaunchButtonId).click();
    $("#" + EnrollInStandupTrainingModalHiddenLoadButtonId).click();
}

function GetSelectedStandupTrainingInstanceListItem() {
    $("input[name=\"Master$PageContentPlaceholder$EnrollInStandupTrainingModal$EnrollInStandupTrainingModal_StandupTrainingInstanceSelect\"]").each(function () {
        if (this.checked) {
            var idStandupTrainingInstance = this.id.replace("EnrollInStandupTrainingModal_StandupTrainingInstanceSelect_", "");
            var isWaitingList = $(this).attr("waitinglist");
            $("#" + EnrollInStandupTrainingSelectedStandupTrainingInstanceDataId).val(idStandupTrainingInstance + "|" + isWaitingList);
        }
    });
}

function LoadViewEnrolledStandupTrainingModalContent(idDataLesson, idStandupTraining, idStandupTrainingInstance) {
    $("#" + EnrolledStandupTrainingDataId).val(idDataLesson + "|" + idStandupTraining + "|" + idStandupTrainingInstance);

    $("#" + ViewEnrolledStandupTrainingModalHiddenLaunchButtonId).click();
    $("#" + ViewEnrolledStandupTrainingModalHiddenLoadButtonId).click();
}

function LoadRemoveEnrolledStandupTrainingModalContent(idDataLesson, idStandupTrainingInstance) {
    $("#" + EnrolledStandupTrainingDataId).val(idDataLesson + "|" + idStandupTrainingInstance);

    $("#" + RemoveEnrolledStandupTrainingModalHiddenLaunchButtonId).click();
    $("#" + RemoveEnrolledStandupTrainingModalHiddenLoadButtonId).click();
}

function LoadTaskModalContent(idDataLesson, idLesson, contentTypeCommittedTo, contentCompletionStatus, taskResourcePath, uploadedTaskFilename, taskUploadedDate, idTaskDocumentType) {
    $("#" + TaskDataId).val(idDataLesson + "|" + idLesson + "|" + contentTypeCommittedTo + "|" + contentCompletionStatus + "|" + taskResourcePath + "|" + uploadedTaskFilename + "|" + taskUploadedDate + "|" + idTaskDocumentType);

    $("#" + TaskModalLaunchButtonId).click();
    $("#" + TaskModalLoadButtonId).click();

    // clear the remove uploaded file flag
    $("#" + TaskRemoveUploadedFile).val("false");

    // clear out the uploader's values
    $("#TaskUploader_Field_CompletedPanel").html("");
    $("#TaskUploader_ErrorContainer").html("");
    $("#TaskUploader_Field_UploadControl input").removeAttr("style");
    $("#TaskUploader_Field_UploadHiddenFieldOriginalFileName").val("");
    $("#TaskUploader_Field_UploadHiddenField").val("");
}

function RemoveUploadedTask() {
    // set the remove uploaded file flag
    $("#" + TaskRemoveUploadedFile).val("true");

    $("#TaskModal_UploadedTaskContainer").html("");
    $("#TaskUploader_Field_CompletedPanel").html("");
    $("#TaskUploader_ErrorContainer").html("");
    $("#TaskUploader_Field_UploadControl input").removeAttr("style");
    $("#TaskUploader_Field_UploadHiddenFieldOriginalFileName").val("");
    $("#TaskUploader_Field_UploadHiddenField").val("");
}

function TaskUploaded() {
    $("#TaskModal_UploadedTaskContainer").html("");
    $("#TaskModal_UploadedTaskContainer").html($("#TaskUploader_Field_UploadHiddenFieldOriginalFileName").val() + "<img class=\"XSmallIcon\" onclick=\"javascript:RemoveUploadedTask();\" src=\"" + RemoveImagePath + "\" style=\"cursor:pointer;\">");

    // clear the remove uploaded file flag
    $("#" + TaskRemoveUploadedFile).val("false");
}

function LoadOJTModalContent(idDataLesson, contentTypeCommittedTo, contentCompletionStatus) {
    $("#" + OJTDataId).val(idDataLesson + "|" + contentTypeCommittedTo + "|" + contentCompletionStatus);

    $("#" + OJTModalLaunchButtonId).click();
    $("#" + OJTModalLoadButtonId).click();
}