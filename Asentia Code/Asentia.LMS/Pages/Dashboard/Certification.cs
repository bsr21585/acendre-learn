﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Controls;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.Dashboard
{
    public class Certification : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel CertificationFormContentWrapperContainer;
        public Panel CertificationFormWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel CertificationFormContainer;
        public Panel CertificationTabPanelsContainer;
        #endregion

        #region Private Properties
        // objects
        private User _UserObject;
        private LMS.Library.Certification _CertificationObject;
        private int _IdCertificationToUserLink;

        // user certification details
        private DateTime? _InitialAwardDate = null;
        private int _CertificationTrack = 0;
        private DateTime? _LastExpirationDate = null;
        private DateTime? _CurrentExpirationDate = null;
        private string _CertificationIdentifier;

        // update panels
        private UpdatePanel _CertificationPropertiesTabPanel;
        private UpdatePanel _CertificationInitialTabPanel;
        private UpdatePanel _CertificationRenewalTabPanel;

        // task submit modal
        private ModalPopup _TaskSubmitModal;
        private Panel _TaskModalUploaderPanel;
        private UploaderAsync _TaskUploader;
        private Button _TaskSubmitModalLaunchButton;
        private Button _TaskSubmitModalLoadButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Certification), "Asentia.LMS.Pages.Dashboard.Certification.js");

            base.OnPreRender(e);
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // get the certification and user objects
            this._GetCertificationAndUserObjects();            

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/dashboard/Certification.css");

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CertificationFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            // get user certification details
            this._GetUserCertificationDetails();

            // synchronize certification requirement task data
            LMS.Library.Certification.SynchronizeTaskRequirementData(this._IdCertificationToUserLink);            

            // build the controls for the page
            this._BuildControls();
        }
        #endregion

        #region _GetCertificationAndUserObjects
        /// <summary>
        /// Gets the user and certification objects based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetCertificationAndUserObjects()
        {
            // get the id querystring parameter            
            int qsCId = this.QueryStringInt("cid", 0);
            int vsCId = this.ViewStateInt(this.ViewState, "cid", 0);
            int qsCULId = this.QueryStringInt("culid", 0);
            int vsCULId = this.ViewStateInt(this.ViewState, "culid", 0);

            // get user object - user object MUST exist for this page to load            
            try
            {
                this._UserObject = new User(AsentiaSessionState.IdSiteUser);
            }
            catch
            { Response.Redirect("~/dashboard"); }

            // get certificaiton object - certification object MUST be specified and exist for this page to load
            if (qsCId > 0 || vsCId > 0)
            {
                int id = 0;

                if (qsCId > 0)
                { id = qsCId; }

                if (vsCId > 0)
                { id = vsCId; }

                try
                {
                    if (id > 0)
                    { this._CertificationObject = new LMS.Library.Certification(id); }
                }
                catch
                { Response.Redirect("~/dashboard"); }
            }
            else
            { Response.Redirect("~/dashboard"); }

            // get certification to user link id - MUST be specified for this page to load
            if (qsCULId > 0 || vsCULId > 0)
            {
                int id = 0;

                if (qsCULId > 0)
                { id = qsCULId; }

                if (vsCULId > 0)
                { id = vsCULId; }

                this._IdCertificationToUserLink = id;
            }
            else
            { Response.Redirect("~/dashboard"); }
        }
        #endregion

        #region _GetUserCertificationDetails
        private void _GetUserCertificationDetails()
        {
            DataTable userCertificationDetails = LMS.Library.Certification.GetUserCertificationDetails(this._IdCertificationToUserLink);
            int idUser = 0; // we're going to get the user id from the certification user link so that we can verify that it actually belongs to the calling user

            foreach (DataRow userCertificationDetail in userCertificationDetails.Rows)
            {
                if (userCertificationDetail["idUser"] != DBNull.Value)
                { idUser = Convert.ToInt32(userCertificationDetail["idUser"]); }

                if (userCertificationDetail["initialAwardDate"] != DBNull.Value)
                { this._InitialAwardDate = Convert.ToDateTime(userCertificationDetail["initialAwardDate"]); }

                if (userCertificationDetail["certificationTrack"] != DBNull.Value)
                { this._CertificationTrack = Convert.ToInt32(userCertificationDetail["certificationTrack"]); }

                if (userCertificationDetail["lastExpirationDate"] != DBNull.Value)
                { this._LastExpirationDate = Convert.ToDateTime(userCertificationDetail["lastExpirationDate"]); }

                if (userCertificationDetail["currentExpirationDate"] != DBNull.Value)
                { this._CurrentExpirationDate = Convert.ToDateTime(userCertificationDetail["currentExpirationDate"]); }

                if (userCertificationDetail["certificationId"] != DBNull.Value)
                { this._CertificationIdentifier = userCertificationDetail["certificationId"].ToString(); }
            }

            // if the certification user link id doesnt belong to this user, exit
            if (idUser != AsentiaSessionState.IdSiteUser)
            { Response.Redirect("~/dashboard"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {            
            // evaluate for breadcrumb and page title information
            string breadCrumbPageTitle;
            string imageCssClass = null;
            string pageTitle;

            string certificationTitleInInterfaceLanguage = this._CertificationObject.Title;

            if (AsentiaSessionState.UserCulture != AsentiaSessionState.GlobalSiteObject.LanguageString)
            {
                foreach (LMS.Library.Certification.LanguageSpecificProperty certificationLanguageSpecificProperty in this._CertificationObject.LanguageSpecificProperties)
                {
                    if (certificationLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                    { certificationTitleInInterfaceLanguage = certificationLanguageSpecificProperty.Title; }
                }
            }

            breadCrumbPageTitle = certificationTitleInInterfaceLanguage;
            pageTitle = certificationTitleInInterfaceLanguage;

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(breadCrumbPageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG), imageCssClass);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Method to create the page Breadcrumb, user action menu controls and other controls on page.
        /// </summary>
        private void _BuildControls()
        {
            // instansiate task submit controls
            this._TaskSubmitModalLaunchButton = new Button();
            this._TaskSubmitModalLaunchButton.ID = "TaskSubmitModalLaunchButton";
            this._TaskSubmitModalLaunchButton.Style.Add("display", "none");

            this._TaskSubmitModalLoadButton = new Button();
            this._TaskSubmitModalLoadButton.ID = "TaskSubmitModalLoadButton";
            this._TaskSubmitModalLoadButton.Style.Add("display", "none");
            this._TaskSubmitModalLoadButton.Click += this._LoadTaskSubmitModalContent;

            this._TaskModalUploaderPanel = new Panel();
            this._TaskModalUploaderPanel.ID = "TaskModalUploaderPanel";

            this._TaskUploader = new UploaderAsync("TaskUploader_Field", UploadType.Task, "TaskUploader_ErrorContainer");
            this._TaskUploader.ClientSideCompleteJSMethod = "TaskUploaded";

            // attach the modal launch controls
            this.CertificationFormWrapperContainer.Controls.Add(this._TaskSubmitModalLaunchButton);

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            this.CertificationFormWrapperContainer.CssClass = "xd-12 xm-12 FormContentContainer";

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.UseTheFollowingFormToViewYourCertificationActivityAndUploadDocumentationForCertificationTasksThatRequireDocumentation, true);

            // build the certification form
            this._BuildCertificationForm();

            // build modal for task submit
            this._BuildTaskSubmitModal();
        }
        #endregion

        #region _BuildCertificationForm
        private void _BuildCertificationForm()
        {
            // clear certification from container
            this.CertificationFormContainer.Controls.Clear();

            // build the tabs
            this._BuildCertificationFormTabs();

            this.CertificationTabPanelsContainer = new Panel();
            this.CertificationTabPanelsContainer.ID = "Certification_TabPanelsContainer";
            this.CertificationTabPanelsContainer.CssClass = "TabPanelsContentContainer";
            this.CertificationFormContainer.Controls.Add(this.CertificationTabPanelsContainer);

            // build the tab panels
            this._BuildCertificationPropertiesTabPanel();
            this._BuildCertificationInitialTabPanel();

            if (this._CertificationTrack == 1)
            { this._BuildCertificationRenewalTabPanel(); }
        }
        #endregion

        #region _BuildCertificationFormTabs
        /// <summary>
        /// Builds the tabs for the certification.
        /// </summary>
        private void _BuildCertificationFormTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Properties", _GlobalResources.Properties));
            tabs.Enqueue(new KeyValuePair<string, string>("Initial", _GlobalResources.Initial));

            if (this._CertificationTrack == 1)
            { tabs.Enqueue(new KeyValuePair<string, string>("Renewal", _GlobalResources.Renewal)); }

            // build and attach the tabs
            this.CertificationFormContainer.Controls.Add(AsentiaPage.BuildTabListPanel("Certification", tabs));
        }
        #endregion

        #region _BuildCertificationPropertiesTabPanel
        /// <summary>
        /// Builds the certification properties tab panel.
        /// </summary>
        private void _BuildCertificationPropertiesTabPanel()
        {
            // "Properties" is the default tab, so this is visible on page load.
            this._CertificationPropertiesTabPanel = new UpdatePanel();
            this._CertificationPropertiesTabPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._CertificationPropertiesTabPanel.ID = "Certification_" + "Properties" + "_TabPanel";
            this._CertificationPropertiesTabPanel.Attributes.Add("class", "TabPanelContainer");
            this._CertificationPropertiesTabPanel.Attributes.Add("style", "display: block;");

            string certificationTitleInLanguage = this._CertificationObject.Title;
            string certificationShortDescriptionInLanguage = this._CertificationObject.ShortDescription;

            foreach (LMS.Library.Certification.LanguageSpecificProperty cLSP in this._CertificationObject.LanguageSpecificProperties)
            {
                if (cLSP.LangString == AsentiaSessionState.UserCulture)
                {
                    certificationTitleInLanguage = cLSP.Title;
                    certificationShortDescriptionInLanguage = cLSP.ShortDescription;
                    break;
                }
            }

            // certification title field            
            Literal certificationTitle = new Literal();
            certificationTitle.Text = certificationTitleInLanguage;

            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildFormField("CertificationTitle",
                                                                                        _GlobalResources.Certification,
                                                                                        null,
                                                                                        certificationTitle,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // certification description field            
            Literal certificationDescription = new Literal();
            certificationDescription.Text = certificationShortDescriptionInLanguage;

            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildFormField("CertificationDescription",
                                                                                        _GlobalResources.Description,
                                                                                        null,
                                                                                        certificationDescription,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // certification track field            
            Literal certificationTrack = new Literal();

            if (this._CertificationTrack == 0)
            { certificationTrack.Text = _GlobalResources.Initial; }

            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildFormField("CertificationTrack",
                                                                                        _GlobalResources.CertificationTrack,
                                                                                        null,
                                                                                        certificationTrack,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // initial award date          
            Literal initialAwardDate = new Literal();

            if (this._InitialAwardDate != null)
            {
                DateTime initialAwardDateLocal = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._InitialAwardDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                initialAwardDate.Text = initialAwardDateLocal.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
            }
            else
            { initialAwardDate.Text = _GlobalResources.None; }

            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildFormField("InitialAwardDate",
                                                                                        _GlobalResources.InitialAwardDate,
                                                                                        null,
                                                                                        initialAwardDate,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // last expiration date          
            Literal lastExpirationDate = new Literal();

            if (this._LastExpirationDate != null)
            {
                DateTime lastExpirationDateLocal = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._LastExpirationDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                lastExpirationDate.Text = lastExpirationDateLocal.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
            }
            else
            { lastExpirationDate.Text = _GlobalResources.None; }

            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildFormField("LastExpirationDate",
                                                                                        _GlobalResources.LastExpirationDate,
                                                                                        null,
                                                                                        lastExpirationDate,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // current expiration date          
            Literal currentExpirationDate = new Literal();

            if (this._CurrentExpirationDate != null)
            {
                DateTime currentExpirationDateLocal = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._CurrentExpirationDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                currentExpirationDate.Text = currentExpirationDateLocal.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
            }
            else
            { currentExpirationDate.Text = _GlobalResources.None; }

            this._CertificationPropertiesTabPanel.ContentTemplateContainer.Controls.Add(AsentiaPage.BuildFormField("CurrentExpirationDate",
                                                                                        _GlobalResources.CurrentExpirationDate,
                                                                                        null,
                                                                                        currentExpirationDate,
                                                                                        false,
                                                                                        false,
                                                                                        false));

            // attach panel to container
            this.CertificationTabPanelsContainer.Controls.Add(this._CertificationPropertiesTabPanel);
        }
        #endregion

        #region _BuildCertificationInitialTabPanel
        /// <summary>
        /// Builds the certification initial tab panel.
        /// </summary>
        private void _BuildCertificationInitialTabPanel()
        {
            // declare the panel
            this._CertificationInitialTabPanel = new UpdatePanel();
            this._CertificationInitialTabPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._CertificationInitialTabPanel.ID = "Certification_" + "Initial" + "_TabPanel";
            this._CertificationInitialTabPanel.Attributes.Add("class", "TabPanelContainer");
            this._CertificationInitialTabPanel.Attributes.Add("style", "display: none;");

            // build the panel content
            this._BuildCertificationRequirementTabPanelContent(true, this._CertificationInitialTabPanel);

            // attach panel to container
            this.CertificationTabPanelsContainer.Controls.Add(this._CertificationInitialTabPanel);
        }
        #endregion

        #region _BuildCertificationRenewalTabPanel
        /// <summary>
        /// Builds the certification renewal tab panel.
        /// </summary>
        private void _BuildCertificationRenewalTabPanel()
        {
            this._CertificationRenewalTabPanel = new UpdatePanel();
            this._CertificationRenewalTabPanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._CertificationRenewalTabPanel.ID = "Certification_" + "Renewal" + "_TabPanel";
            this._CertificationRenewalTabPanel.Attributes.Add("class", "TabPanelContainer");
            this._CertificationRenewalTabPanel.Attributes.Add("style", "display: none;");

            // build the panel content
            this._BuildCertificationRequirementTabPanelContent(false, this._CertificationRenewalTabPanel);

            // attach panel to container
            this.CertificationTabPanelsContainer.Controls.Add(this._CertificationRenewalTabPanel);
        }
        #endregion

        #region _BuildCertificationRequirementTabPanelContent
        private void _BuildCertificationRequirementTabPanelContent(bool isInitialRequirements, UpdatePanel tabUpdatePanelContainer)
        {
            // clear the controls
            tabUpdatePanelContainer.ContentTemplateContainer.Controls.Clear();

            // begin building
            DataTable requirementStatuses = LMS.Library.Certification.GetRequirementStatusesForUser(this._UserObject.Id, this._IdCertificationToUserLink, isInitialRequirements);
            DataTable certificationModules = this._CertificationObject.GetModuleIds();
            bool isCertificationComplete = false;

            // INITIAL REQUIREMENTS STATUS
            Panel certificationTitleContainer = new Panel();
            certificationTitleContainer.ID = tabUpdatePanelContainer.ID + "_CertificationTitleContainer";
            certificationTitleContainer.CssClass = "CertificationTitleContainer";

            foreach (DataRow row in requirementStatuses.Rows)
            {
                if (row["objectType"].ToString() == "certification" && Convert.ToInt32(row["idObject"]) == this._CertificationObject.Id)
                {
                    isCertificationComplete = Convert.ToBoolean(row["isRequirementMet"]);
                    break;
                }
            }

            Label certificationCompletionStatusImageContainer = new Label();
            certificationCompletionStatusImageContainer.Controls.Add(this._GetCompletionStatusImage(isCertificationComplete, "SmallIcon"));
            certificationTitleContainer.Controls.Add(certificationCompletionStatusImageContainer);

            Label certificationTitle = new Label();

            if (isInitialRequirements)
            { certificationTitle.Text = _GlobalResources.InitialRequirements; }
            else
            { certificationTitle.Text = _GlobalResources.RenewalRequirements; }

            certificationTitleContainer.Controls.Add(certificationTitle);
            tabUpdatePanelContainer.ContentTemplateContainer.Controls.Add(certificationTitleContainer);

            // CERTIFICATION MODULES            
            foreach (DataRow moduleRow in certificationModules.Rows)
            {
                int idCertificationModule = Convert.ToInt32(moduleRow["idCertificationModule"]);
                CertificationModule certificationModuleObject = new CertificationModule(idCertificationModule);
                bool isCertificationModuleComplete = false;

                if ((certificationModuleObject.IsInitialRequirement && isInitialRequirements) || (!certificationModuleObject.IsInitialRequirement && !isInitialRequirements))
                {
                    // container
                    Panel certificationModuleContainer = new Panel();
                    certificationModuleContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleContainer_" + certificationModuleObject.Id.ToString();
                    certificationModuleContainer.CssClass = "CertificationModuleContainer";

                    // language-specific properties
                    string certificationModuleTitleInLanguage = certificationModuleObject.Title;
                    string certificationModuleShortDescriptionInLanguage = certificationModuleObject.ShortDescription;

                    foreach (CertificationModule.LanguageSpecificProperty certModuleLSP in certificationModuleObject.LanguageSpecificProperties)
                    {
                        if (certModuleLSP.LangString == AsentiaSessionState.UserCulture)
                        {
                            certificationModuleTitleInLanguage = certModuleLSP.Title;
                            certificationModuleShortDescriptionInLanguage = certModuleLSP.ShortDescription;
                            break;
                        }
                    }

                    // title
                    Panel certificationModuleTitleContainer = new Panel();
                    certificationModuleTitleContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleTitleContainer_" + certificationModuleObject.Id.ToString();
                    certificationModuleTitleContainer.CssClass = "CertificationModuleTitleContainer";

                    foreach (DataRow row in requirementStatuses.Rows)
                    {
                        if (row["objectType"].ToString() == "module" && Convert.ToInt32(row["idObject"]) == certificationModuleObject.Id)
                        {
                            isCertificationModuleComplete = Convert.ToBoolean(row["isRequirementMet"]);
                            break;
                        }
                    }

                    Label certificationModuleCompletionStatusImageContainer = new Label();
                    certificationModuleCompletionStatusImageContainer.Controls.Add(this._GetCompletionStatusImage(isCertificationModuleComplete, "SmallIcon"));
                    certificationModuleTitleContainer.Controls.Add(certificationModuleCompletionStatusImageContainer);

                    Label certificationModuleTitle = new Label();
                    certificationModuleTitle.Text = _GlobalResources.Unit + ": " + certificationModuleTitleInLanguage;
                    certificationModuleTitle.CssClass = "CertificationModuleTitle";
                    certificationModuleTitleContainer.Controls.Add(certificationModuleTitle);

                    certificationModuleContainer.Controls.Add(certificationModuleTitleContainer);

                    // short description
                    Panel certificationModuleShortDescriptionContainer = new Panel();
                    certificationModuleShortDescriptionContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleShortDescriptionContainer_" + certificationModuleObject.Id.ToString();
                    certificationModuleShortDescriptionContainer.CssClass = "CertificationModuleShortDescriptionContainer";

                    Label certificationModuleShortDescription = new Label();
                    certificationModuleShortDescription.Text = certificationModuleShortDescriptionInLanguage;
                    certificationModuleShortDescriptionContainer.Controls.Add(certificationModuleShortDescription);

                    certificationModuleTitleContainer.Controls.Add(certificationModuleShortDescriptionContainer); // put the short description in the title container

                    // CERTIFICATION REQUIREMENT SETS
                    DataTable certificationRequirementSets = certificationModuleObject.GetRequirementSetIds();
                    int moduleRequirementSetIndex = 0;

                    foreach (DataRow requirementSetRow in certificationRequirementSets.Rows)
                    {
                        int idCertificationModuleRequirementSet = Convert.ToInt32(requirementSetRow["idCertificationModuleRequirementSet"]);
                        CertificationModuleRequirementSet certificationModuleRequirementSetObject = new CertificationModuleRequirementSet(idCertificationModuleRequirementSet);
                        bool isCertificationModuleRequirementSetComplete = false;
                        moduleRequirementSetIndex++;

                        // container
                        Panel certificationModuleRequirementSetContainer = new Panel();
                        certificationModuleRequirementSetContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementSetContainer_" + certificationModuleRequirementSetObject.Id.ToString();
                        certificationModuleRequirementSetContainer.CssClass = "CertificationModuleRequirementSetContainer";

                        // language-specific properties
                        string certificationModuleRequirementSetLabelInLanguage = certificationModuleRequirementSetObject.Label;

                        foreach (CertificationModuleRequirementSet.LanguageSpecificProperty certModuleRSLSP in certificationModuleRequirementSetObject.LanguageSpecificProperties)
                        {
                            if (certModuleRSLSP.LangString == AsentiaSessionState.UserCulture)
                            {
                                certificationModuleRequirementSetLabelInLanguage = certModuleRSLSP.Label;
                                break;
                            }
                        }

                        // label
                        Panel certificationModuleRequirementSetLabelContainer = new Panel();
                        certificationModuleRequirementSetLabelContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementSetLabelContainer_" + certificationModuleRequirementSetObject.Id.ToString();
                        certificationModuleRequirementSetLabelContainer.CssClass = "CertificationModuleRequirementSetLabelContainer";

                        foreach (DataRow row in requirementStatuses.Rows)
                        {
                            if (row["objectType"].ToString() == "requirementset" && Convert.ToInt32(row["idObject"]) == certificationModuleRequirementSetObject.Id)
                            {
                                isCertificationModuleRequirementSetComplete = Convert.ToBoolean(row["isRequirementMet"]);
                                break;
                            }
                        }

                        Label certificationModuleRequirementSetCompletionStatusImageContainer = new Label();
                        certificationModuleRequirementSetCompletionStatusImageContainer.Controls.Add(this._GetCompletionStatusImage(isCertificationModuleRequirementSetComplete, "SmallIcon"));
                        certificationModuleRequirementSetLabelContainer.Controls.Add(certificationModuleRequirementSetCompletionStatusImageContainer);

                        Label certificationModuleRequirementSetLabel = new Label();
                        certificationModuleRequirementSetLabel.Text = _GlobalResources.Segment + ": " + certificationModuleRequirementSetLabelInLanguage;
                        certificationModuleRequirementSetLabelContainer.Controls.Add(certificationModuleRequirementSetLabel);

                        certificationModuleRequirementSetContainer.Controls.Add(certificationModuleRequirementSetLabelContainer);

                        // CERTIFICATION REQUIREMENTS
                        DataTable certificationRequirements = certificationModuleRequirementSetObject.GetRequirementIds();
                        int moduleRequirementIndex = 0;

                        foreach (DataRow requirementRow in certificationRequirements.Rows)
                        {
                            moduleRequirementIndex++;

                            int idCertificationModuleRequirement = Convert.ToInt32(requirementRow["idCertificationModuleRequirement"]);
                            CertificationModuleRequirement certificationModuleRequirementObject = new CertificationModuleRequirement(idCertificationModuleRequirement);
                            bool isCertificationModuleRequirementComplete = false;
                            int? requirementType = null;
                            bool? courseCompletionIsAny = null;
                            double? numberCreditsRequired = null;
                            double? numberCreditsEarned = null;
                            bool? courseCreditEligibleCourseIsAll = null;
                            bool? documentationApprovalRequired = null;
                            bool? allowSupervisorsToApproveDocumentation = null;
                            string completionDocumentationFilePath = null;
                            DateTime? dtSubmitted = null;
                            DateTime? dtApproved = null;
                            int? idDataCertificationModuleRequirement = null;
                            string[] idsOfCoursesCompleted = new string[0];

                            // container
                            Panel certificationModuleRequirementContainer = new Panel();
                            certificationModuleRequirementContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementContainer_" + certificationModuleRequirementObject.Id.ToString();
                            certificationModuleRequirementContainer.CssClass = "CertificationModuleRequirementContainer";

                            // language-specific properties
                            string certificationModuleRequirementLabelInLanguage = certificationModuleRequirementObject.Label;

                            foreach (CertificationModuleRequirement.LanguageSpecificProperty certModuleRLSP in certificationModuleRequirementObject.LanguageSpecificProperties)
                            {
                                if (certModuleRLSP.LangString == AsentiaSessionState.UserCulture)
                                {
                                    certificationModuleRequirementLabelInLanguage = certModuleRLSP.Label;
                                    break;
                                }
                            }

                            // label
                            Panel certificationModuleRequirementLabelContainer = new Panel();
                            certificationModuleRequirementLabelContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementLabelContainer_" + certificationModuleRequirementObject.Id.ToString();
                            certificationModuleRequirementLabelContainer.CssClass = "CertificationModuleRequirementLabelContainer";

                            foreach (DataRow row in requirementStatuses.Rows)
                            {
                                if (row["objectType"].ToString() == "requirement" && Convert.ToInt32(row["idObject"]) == certificationModuleRequirementObject.Id)
                                {
                                    isCertificationModuleRequirementComplete = Convert.ToBoolean(row["isRequirementMet"]);

                                    if (row["requirementType"] != null)
                                    { requirementType = Convert.ToInt32(row["requirementType"]); }

                                    if (row["courseCompletionIsAny"] != DBNull.Value)
                                    { courseCompletionIsAny = Convert.ToBoolean(row["courseCompletionIsAny"]); }

                                    if (row["numberCreditsRequired"] != DBNull.Value)
                                    { numberCreditsRequired = Convert.ToDouble(row["numberCreditsRequired"]); }

                                    if (row["numberCreditsEarned"] != DBNull.Value)
                                    { numberCreditsEarned = Convert.ToDouble(row["numberCreditsEarned"]); }

                                    if (row["courseCreditEligibleCourseIsAll"] != DBNull.Value)
                                    { courseCreditEligibleCourseIsAll = Convert.ToBoolean(row["courseCreditEligibleCourseIsAll"]); }

                                    if (row["documentationApprovalRequired"] != DBNull.Value)
                                    { documentationApprovalRequired = Convert.ToBoolean(row["documentationApprovalRequired"]); }

                                    if (row["allowSupervisorsToApproveDocumentation"] != DBNull.Value)
                                    { allowSupervisorsToApproveDocumentation = Convert.ToBoolean(row["allowSupervisorsToApproveDocumentation"]); }

                                    if (row["completionDocumentationFilePath"] != DBNull.Value)
                                    { completionDocumentationFilePath = row["completionDocumentationFilePath"].ToString(); }

                                    if (row["dtSubmitted"] != DBNull.Value)
                                    { dtSubmitted = Convert.ToDateTime(row["dtSubmitted"]); }

                                    if (row["dtApproved"] != DBNull.Value)
                                    { dtApproved = Convert.ToDateTime(row["dtApproved"]); }

                                    if (row["idDataCertificationModuleRequirement"] != DBNull.Value)
                                    { idDataCertificationModuleRequirement = Convert.ToInt32(row["idDataCertificationModuleRequirement"]); }

                                    if (row["idsOfCoursesCompleted"] != DBNull.Value)
                                    { idsOfCoursesCompleted = row["idsOfCoursesCompleted"].ToString().Split(','); }

                                    break;
                                }
                            }

                            Label certificationModuleRequirementCompletionStatusImageContainer = new Label();
                            certificationModuleRequirementCompletionStatusImageContainer.Controls.Add(this._GetCompletionStatusImage(isCertificationModuleRequirementComplete, "SmallIcon"));
                            certificationModuleRequirementLabelContainer.Controls.Add(certificationModuleRequirementCompletionStatusImageContainer);

                            Label certificationModuleRequirementLabel = new Label();
                            certificationModuleRequirementLabel.Text = _GlobalResources.Requirement + ": " + certificationModuleRequirementLabelInLanguage;
                            certificationModuleRequirementLabelContainer.Controls.Add(certificationModuleRequirementLabel);

                            certificationModuleRequirementContainer.Controls.Add(certificationModuleRequirementLabelContainer);

                            // specifics of the requirement including document links and submission of tasks                            
                            Panel requirementDetailsContainer = new Panel();
                            requirementDetailsContainer.ID = tabUpdatePanelContainer.ID + "_CertificationModuleRequirementDetailsContainer_" + certificationModuleRequirementObject.Id.ToString();
                            requirementDetailsContainer.CssClass = "CertificationModuleRequirementDetailsContainer";

                            if (requirementType == 0) // course
                            {
                                Literal completeFollowingCourses = new Literal();
                                requirementDetailsContainer.Controls.Add(completeFollowingCourses);

                                if ((bool)courseCompletionIsAny)
                                { completeFollowingCourses.Text = _GlobalResources.CompleteAnyOfTheseCourse_s; }
                                else
                                { completeFollowingCourses.Text = _GlobalResources.CompleteAllOfTheseCourse_s; }

                                DataTable requirementCourses = certificationModuleRequirementObject.GetCourseRequirementCourseIds();

                                HtmlGenericControl courseUL = new HtmlGenericControl("ul");
                                requirementDetailsContainer.Controls.Add(courseUL);

                                foreach (DataRow requirementCourse in requirementCourses.Rows)
                                {                                    
                                    HtmlGenericControl courseLI = new HtmlGenericControl("li");

                                    if (Array.IndexOf(idsOfCoursesCompleted, requirementCourse["idCourse"].ToString()) > -1)
                                    { courseLI.Controls.Add(this._GetCompletionStatusImage(true, "XSmallIcon")); }
                                    else
                                    { courseLI.Controls.Add(this._GetCompletionStatusImage(false, "XSmallIcon")); }

                                    Literal courseLIText = new Literal();
                                    courseLIText.Text = requirementCourse["title"].ToString();
                                    courseLI.Controls.Add(courseLIText);

                                    courseUL.Controls.Add(courseLI);
                                }
                            }
                            else if (requirementType == 1) // credit
                            {
                                Literal creditsEarnedOfCreditsRequired = new Literal();
                                creditsEarnedOfCreditsRequired.Text = String.Format(_GlobalResources.XOfXCreditsEarnedOfTheFollowingCourse_s, numberCreditsEarned.ToString(), numberCreditsRequired.ToString());
                                requirementDetailsContainer.Controls.Add(creditsEarnedOfCreditsRequired);

                                DataTable requirementCourses;

                                if ((bool)courseCreditEligibleCourseIsAll)
                                { requirementCourses = this._CertificationObject.GetAttachedCourses(); }
                                else
                                { requirementCourses = certificationModuleRequirementObject.GetCourseRequirementCourseIds(); }

                                HtmlGenericControl courseUL = new HtmlGenericControl("ul");
                                requirementDetailsContainer.Controls.Add(courseUL);

                                foreach (DataRow requirementCourse in requirementCourses.Rows)
                                {
                                    HtmlGenericControl courseLI = new HtmlGenericControl("li");

                                    if (Array.IndexOf(idsOfCoursesCompleted, requirementCourse["idCourse"].ToString()) > -1)
                                    { courseLI.Controls.Add(this._GetCompletionStatusImage(true, "XSmallIcon")); }
                                    else
                                    { courseLI.Controls.Add(this._GetCompletionStatusImage(false, "XSmallIcon")); }

                                    Literal courseLIText = new Literal();
                                    courseLIText.Text = requirementCourse["title"].ToString() + " (" + requirementCourse["credits"].ToString() + " " + _GlobalResources.Credits.ToLower() + ")";
                                    courseLI.Controls.Add(courseLIText);

                                    courseUL.Controls.Add(courseLI);
                                }
                            }
                            else if (requirementType == 2) // task
                            {
                                if (dtSubmitted != null && (bool)documentationApprovalRequired && !String.IsNullOrEmpty(completionDocumentationFilePath))
                                {
                                    Panel uploadedTaskDocumentLinkContainer = new Panel();
                                    requirementDetailsContainer.Controls.Add(uploadedTaskDocumentLinkContainer);

                                    Literal uploadedDocumentText = new Literal();
                                    uploadedDocumentText.Text = _GlobalResources.UploadedDocument;

                                    HyperLink uploadedTaskDocumentLink = new HyperLink();
                                    uploadedTaskDocumentLink.NavigateUrl = SitePathConstants.SITE_USERS_ROOT + this._UserObject.Id.ToString() + "/CertificationTasks/" + idDataCertificationModuleRequirement.ToString() + "/" + completionDocumentationFilePath;
                                    uploadedTaskDocumentLink.Target = "_blank";
                                    uploadedTaskDocumentLink.Text = completionDocumentationFilePath;
                                    uploadedTaskDocumentLinkContainer.Controls.Add(uploadedTaskDocumentLink);
                                }

                                if (dtApproved == null)
                                {
                                    Panel submitTaskLinkContainer = new Panel();
                                    requirementDetailsContainer.Controls.Add(submitTaskLinkContainer);

                                    HyperLink submitTaskLink = new HyperLink();
                                    submitTaskLink.NavigateUrl = "javascript: LoadCertificationTaskModalContent(" + this._IdCertificationToUserLink + ", " + idDataCertificationModuleRequirement + ", " + documentationApprovalRequired.ToString().ToLower() + ", \"" + completionDocumentationFilePath + "\");";
                                    submitTaskLink.Text = _GlobalResources.SubmitTask;
                                    submitTaskLinkContainer.Controls.Add(submitTaskLink);
                                }
                            }
                            else
                            { }

                            // attach the requirement details to the requirement container
                            certificationModuleRequirementContainer.Controls.Add(requirementDetailsContainer);

                            // attach module requirement container to module requirement set panel
                            certificationModuleRequirementSetContainer.Controls.Add(certificationModuleRequirementContainer);

                            // create and attach and/or label for module requirement
                            if (moduleRequirementIndex < certificationRequirements.Rows.Count)
                            {
                                Panel certificationModuleRequirementAndOrContainer = new Panel();
                                certificationModuleRequirementAndOrContainer.CssClass = "CertificationAndOrContainer";

                                Literal certificationModuleRequirementAndOrText = new Literal();
                                certificationModuleRequirementAndOrContainer.Controls.Add(certificationModuleRequirementAndOrText);

                                if (certificationModuleRequirementSetObject.IsAny)
                                { certificationModuleRequirementAndOrText.Text = _GlobalResources.OR_UPPER; }
                                else
                                { certificationModuleRequirementAndOrText.Text = _GlobalResources.AND_UPPER; }

                                certificationModuleRequirementSetContainer.Controls.Add(certificationModuleRequirementAndOrContainer);
                            }
                        }

                        // attach module requirement set container to module panel
                        certificationModuleContainer.Controls.Add(certificationModuleRequirementSetContainer);

                        // create and attach and/or label for module requirement set
                        if (moduleRequirementSetIndex < certificationRequirementSets.Rows.Count)
                        {
                            Panel certificationModuleRequirementSetAndOrContainer = new Panel();
                            certificationModuleRequirementSetAndOrContainer.CssClass = "CertificationAndOrContainer";

                            Literal certificationModuleRequirementSetAndOrText = new Literal();
                            certificationModuleRequirementSetAndOrContainer.Controls.Add(certificationModuleRequirementSetAndOrText);

                            if (certificationModuleObject.IsAnyRequirementSet)
                            { certificationModuleRequirementSetAndOrText.Text = _GlobalResources.OR_UPPER; }
                            else
                            { certificationModuleRequirementSetAndOrText.Text = _GlobalResources.AND_UPPER; }

                            certificationModuleContainer.Controls.Add(certificationModuleRequirementSetAndOrContainer);
                        }
                    }

                    // attach module container to tab panel
                    tabUpdatePanelContainer.ContentTemplateContainer.Controls.Add(certificationModuleContainer);

                    // create and attach and/or label for module
                    Panel certificationModuleAndOrContainer = new Panel();
                    certificationModuleAndOrContainer.CssClass = "CertificationAndOrContainer";

                    Literal certificationModuleAndOrText = new Literal();
                    certificationModuleAndOrContainer.Controls.Add(certificationModuleAndOrText);

                    if (this._CertificationObject.IsAnyModule)
                    { certificationModuleAndOrText.Text = _GlobalResources.OR_UPPER; }
                    else
                    { certificationModuleAndOrText.Text = _GlobalResources.AND_UPPER; }

                    tabUpdatePanelContainer.ContentTemplateContainer.Controls.Add(certificationModuleAndOrContainer);
                }
            }

            // take out the last control, which should be the last and/or container
            tabUpdatePanelContainer.ContentTemplateContainer.Controls.RemoveAt(tabUpdatePanelContainer.ContentTemplateContainer.Controls.Count - 1);
        }
        #endregion

        #region _GetCompletionStatusImage
        private Image _GetCompletionStatusImage(bool isRequirementMet, string iconClass)
        {
            Image completionStatusImage = new Image();
            completionStatusImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);
            completionStatusImage.CssClass = iconClass + " CertificationCompletionStatusImage";
            completionStatusImage.AlternateText = _GlobalResources.Status;

            if (!isRequirementMet)
            { completionStatusImage.CssClass += " DimIcon"; }

            return completionStatusImage;
        }
        #endregion

        #region _BuildTaskSubmitModal
        /// <summary>
        /// 
        /// </summary>
        private void _BuildTaskSubmitModal()
        {
            // set modal properties
            this._TaskSubmitModal = new ModalPopup("TaskSubmitModal");
            this._TaskSubmitModal.Type = ModalPopupType.Form;
            this._TaskSubmitModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG);
            this._TaskSubmitModal.HeaderIconAlt = _GlobalResources.SubmitTask;
            this._TaskSubmitModal.HeaderText = _GlobalResources.SubmitTask;
            this._TaskSubmitModal.CloseButtonTextType = ModalPopupButtonText.Cancel;

            this._TaskSubmitModal.TargetControlID = this._TaskSubmitModalLaunchButton.ID;
            this._TaskSubmitModal.SubmitButton.Command += new CommandEventHandler(this._TaskSubmitSubmit_Command);

            // build the modal form panel
            Panel taskModalFormPanel = new Panel();
            taskModalFormPanel.ID = "TaskSubmitModalFormPanel";
            taskModalFormPanel.Visible = false; // don't render this, we will render in the _LoadTaskSubmitModalContent method

            // note, we need to add the control for setting approval, it registers properly in the control stack

            // build the upload control here instead of within the "load" method for this modal
            // the uploader will be maintained within its own panel and it all needs to be done here for page lifecycle reasons

            // uploader error container
            Panel taskUploaderErrorContainer = new Panel();
            taskUploaderErrorContainer.ID = "TaskUploader_ErrorContainer";
            taskUploaderErrorContainer.CssClass = "FormFieldErrorContainer";
            this._TaskModalUploaderPanel.Controls.Add(taskUploaderErrorContainer);

            // attach the task uploader
            this._TaskModalUploaderPanel.Controls.Add(this._TaskUploader);

            // build the task data hidden field
            HiddenField taskData = new HiddenField();
            taskData.ID = "TaskData";

            // build the modal body
            this._TaskSubmitModal.AddControlToBody(taskModalFormPanel);
            this._TaskSubmitModal.AddControlToBody(this._TaskModalUploaderPanel);
            this._TaskSubmitModal.AddControlToBody(this._TaskSubmitModalLoadButton);
            this._TaskSubmitModal.AddControlToBody(taskData);

            // add modal to container
            this.CertificationFormWrapperContainer.Controls.Add(this._TaskSubmitModal);
        }
        #endregion

        #region _LoadTaskSubmitModalContent
        /// <summary>
        /// Loads content for task submit modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void _LoadTaskSubmitModalContent(object sender, EventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField taskData = (HiddenField)this._TaskSubmitModal.FindControl("TaskData");
            Panel taskModalFormPanel = (Panel)this._TaskSubmitModal.FindControl("TaskSubmitModalFormPanel");

            try
            {
                // clear the modal feedback
                this._TaskSubmitModal.ClearFeedback();

                // make the submit and close buttons visible
                this._TaskSubmitModal.SubmitButton.Visible = true;
                this._TaskSubmitModal.CloseButton.Visible = true;

                // get the certification user link id, certification requirement data id, and whether or not upload is required
                string[] taskDataItems = taskData.Value.Split('|');
                int idCertificationToUserLink = Convert.ToInt32(taskDataItems[0]);
                int idDataCertificationModuleRequirement = Convert.ToInt32(taskDataItems[1]);
                bool isDocumentationUploadRequired = Convert.ToBoolean(taskDataItems[2]);

                // TASK UPLOAD -- ONLY IF REQUIRED

                if (isDocumentationUploadRequired)
                {
                    // build instructions for the modal
                    Panel modalInstructions = new Panel();
                    modalInstructions.ID = "TaskModal_ModalInstructions";
                    this.FormatFormInformationPanel(modalInstructions, _GlobalResources.YouMustUploadDocumentationForThisTaskOnceDocumentationIsApprovedTheTaskWillBeCompleted);
                    taskModalFormPanel.Controls.AddAt(0, modalInstructions);

                    // upload task label
                    Panel uploadTaskLabelContainer = new Panel();
                    uploadTaskLabelContainer.ID = "TaskModal_UploadTaskLabelContainer";

                    Label uploadTaskLabel = new Label();
                    uploadTaskLabel.Text = _GlobalResources.UploadTaskDocumentation + ": ";
                    uploadTaskLabelContainer.Controls.Add(uploadTaskLabel);

                    taskModalFormPanel.Controls.Add(uploadTaskLabelContainer);

                    // show the uploader
                    this._TaskModalUploaderPanel.Visible = true;
                }
                else
                {
                    // build instructions for the modal
                    Panel modalInstructions = new Panel();
                    modalInstructions.ID = "TaskModal_ModalInstructions";
                    this.FormatFormInformationPanel(modalInstructions, _GlobalResources.YouAreNotRequiredToUploadDocumentationForThisTaskOnceSubmissionIsApprovedTheTaskWillBeCompleted);
                    taskModalFormPanel.Controls.AddAt(0, modalInstructions);

                    // hide the uploader
                    this._TaskModalUploaderPanel.Visible = false;
                }

                // set the form panel so it will render
                taskModalFormPanel.Visible = true;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
        }
        #endregion

        #region _TaskSubmitSubmit_Command
        /// <summary>
        /// Handles the event initiated by the submit button click on the task submit modal.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _TaskSubmitSubmit_Command(object sender, CommandEventArgs e)
        {
            // get the controls from the widget that we need to work with
            HiddenField taskData = (HiddenField)this._TaskSubmitModal.FindControl("TaskData");
            Panel taskModalFormPanel = (Panel)this._TaskSubmitModal.FindControl("TaskSubmitModalFormPanel");

            try
            {
                // clear the modal feedback
                this._TaskSubmitModal.ClearFeedback();

                // get the certification user link id, certification requirement data id, and whether or not upload is required
                string[] taskDataItems = taskData.Value.Split('|');
                int idCertificationToUserLink = Convert.ToInt32(taskDataItems[0]);
                int idDataCertificationModuleRequirement = Convert.ToInt32(taskDataItems[1]);
                bool isDocumentationUploadRequired = Convert.ToBoolean(taskDataItems[2]);
                string uploadedTaskFilename = taskDataItems[3].ToString();

                // if upload of documentation is required, get the data, otherwise just move on
                if (isDocumentationUploadRequired)
                {
                    // if the uploader doesn't have a file and we're not flagged to remove an existing file, then no changes were made, error
                    if (this._TaskUploader.SavedFilePath == null)
                    {
                        // clear the task form panel and rebuild it
                        taskModalFormPanel.Controls.Clear();
                        this._LoadTaskSubmitModalContent(sender, e);

                        // display error
                        this._TaskSubmitModal.DisplayFeedback(_GlobalResources.YourUploadedFileCouldNotBeProcessedPleaseContactAnAdministrator, true);
                    }
                    else
                    {
                        // check for folder existence and create if necessary
                        if (!Directory.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/CertificationTasks/" + idDataCertificationModuleRequirement.ToString())))
                        { Directory.CreateDirectory(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/CertificationTasks/" + idDataCertificationModuleRequirement.ToString())); }

                        // remove old file if it exists
                        if (!String.IsNullOrWhiteSpace(uploadedTaskFilename))
                        {
                            if (File.Exists(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/CertificationTasks/" + idDataCertificationModuleRequirement.ToString() + "/" + uploadedTaskFilename)))
                            { File.Delete(Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/CertificationTasks/" + idDataCertificationModuleRequirement.ToString() + "/" + uploadedTaskFilename)); }
                        }

                        // save the new file, and remove the uploaded file from the upload folder
                        if (File.Exists(Server.MapPath(this._TaskUploader.SavedFilePath)))
                        {
                            File.Copy(Server.MapPath(this._TaskUploader.SavedFilePath), Server.MapPath(SitePathConstants.SITE_USERS_ROOT + AsentiaSessionState.IdSiteUser.ToString() + "/CertificationTasks/" + idDataCertificationModuleRequirement.ToString() + "/" + this._TaskUploader.FileOriginalName), true);
                            File.Delete(Server.MapPath(this._TaskUploader.SavedFilePath));
                            uploadedTaskFilename = this._TaskUploader.FileOriginalName;
                        }
                        else
                        { throw new AsentiaException(_GlobalResources.AnUnknownErrorOccurredWhileProcessingYourRequest); }
                    }
                }
                else
                { uploadedTaskFilename = null; }

                // save the data
                CertificationModuleRequirement.SaveTask(idDataCertificationModuleRequirement, uploadedTaskFilename);

                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
                this._TaskSubmitModal.DisplayFeedback(_GlobalResources.YouHaveSubmittedYourTaskSuccessfully, false);

                // rebind and update
                this._BuildCertificationRequirementTabPanelContent(true, this._CertificationInitialTabPanel);                
                this._CertificationInitialTabPanel.Update();

                if (this._CertificationTrack == 1)
                { 
                    this._CertificationRenewalTabPanel.Update();
                    this._BuildCertificationRequirementTabPanelContent(false, this._CertificationRenewalTabPanel);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(dnfEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(fnuEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(cpeEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(dEx.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(ex.Message, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
            catch (Exception ee)
            {
                // display the failure message
                this._TaskSubmitModal.DisplayFeedback(ee.Message + " | " + ee.StackTrace, true);
                taskModalFormPanel.Controls.Clear();
                this._TaskModalUploaderPanel.Visible = false;
                this._TaskSubmitModal.SubmitButton.Visible = false;
                this._TaskSubmitModal.CloseButton.Visible = false;
            }
        }
        #endregion
    }
}
