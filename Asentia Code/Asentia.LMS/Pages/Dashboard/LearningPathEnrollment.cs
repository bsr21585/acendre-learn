﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.LMS.Pages.Dashboard
{
    public class LearningPathEnrollment : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel LearningPathEnrollmentFormContentWrapperContainer;
        public UpdatePanel LearningPathEnrollmentCoursesGridUpdatePanel;
        public Panel LearningPathEnrollmentWrapperContainer;
        public Panel LearningPathDetailsContainer;
        public Grid LearningPathEnrollmentCoursesGrid;
        #endregion

        #region Private Properties
        private Library.LearningPathEnrollment _LearningPathEnrollmentObject;
        private LearningPath _LearningPathObject;
        #endregion

        #region Page_Load
        /// <summary>
        /// Page_Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/dashboard/LearningPathEnrollment.css");

            this._GetLearningPathEnrollmentAndLearningPathObjects();

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.LearningPathEnrollmentFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.LearningPathEnrollmentWrapperContainer.CssClass = "FormContentContainer";

            // build page controls
            this._BuildControls();

            // if not postback
            if (!IsPostBack)
            {
                // bind data grid
                this.LearningPathEnrollmentCoursesGrid.BindData();
            }
        }
        #endregion

        #region _GetLearningPathEnrollmentAndLearningPathObjects
        /// <summary>
        /// Gets the learning path enrollment and learning path objects.
        /// </summary>
        private void _GetLearningPathEnrollmentAndLearningPathObjects()
        {
            // get the id querystring parameter
            int id = this.QueryStringInt("id", 0);

            try
            {
                if (id > 0)
                {
                    this._LearningPathEnrollmentObject = new Library.LearningPathEnrollment(id);
                    this._LearningPathObject = new LearningPath(this._LearningPathEnrollmentObject.IdLearningPath);
                }
                else
                { Response.Redirect("~/dashboard"); }
            }
            catch
            { Response.Redirect("~/dashboard"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(this._LearningPathEnrollmentObject.Title));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.LearningAssets, this._LearningPathEnrollmentObject.Title, ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH,
                                                                                                                                      ImageFiles.EXT_PNG));
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds page controls
        /// </summary>
        private void _BuildControls()
        {
            // build left side description panel
            this._BuildDescriptionContainer();

            // build learning path enrollment grid
            this._BuildGrid();
        }
        #endregion

        #region _BuildDescriptionContainer
        /// <summary>
        /// Builds description container
        /// </summary>
        private void _BuildDescriptionContainer()
        {
            this.LearningPathDetailsContainer.CssClass = "LearningPathDetailsContainer";

            // DESCRIPTION

            Panel descriptionContainer = new Panel();
            descriptionContainer.ID = "DescriptionContainer";

            // label
            Panel descriptionLabelContainer = new Panel();
            descriptionLabelContainer.ID = "DescriptionLabelContainer";
            descriptionLabelContainer.CssClass = "FormFieldLabelContainer";

            Label descriptionLabel = new Label();
            descriptionLabel.Text = _GlobalResources.Description;

            descriptionLabelContainer.Controls.Add(descriptionLabel);
            descriptionContainer.Controls.Add(descriptionLabelContainer); 

            // description text
            Panel descriptionTextContainer = new Panel();
            descriptionTextContainer.ID = "DescriptionLabelContainer";

            Literal descriptionText = new Literal();
            descriptionText.Text = Server.HtmlDecode(this._LearningPathObject.LongDescription);

            descriptionTextContainer.Controls.Add(descriptionText);
            descriptionContainer.Controls.Add(descriptionTextContainer); 

            // LEARNING PATH MATERIALS

            Panel learningPathMaterialsContainer = new Panel();
            learningPathMaterialsContainer.ID = "LearningPathMaterialsContainer";

            // label
            Panel learningPathMaterialsLabelContainer = new Panel();
            learningPathMaterialsLabelContainer.ID = "LearningPathMaterialsLabelContainer";
            learningPathMaterialsLabelContainer.CssClass = "FormFieldLabelContainer";

            Label learningPathMaterialsLabel = new Label();
            learningPathMaterialsLabel.Text = _GlobalResources.LearningPathMaterials;

            learningPathMaterialsLabelContainer.Controls.Add(learningPathMaterialsLabel);
            learningPathMaterialsContainer.Controls.Add(learningPathMaterialsLabelContainer);

            // learning path materials files listing
            Panel learningPathMaterialsFilesContainer = new Panel();
            learningPathMaterialsFilesContainer.ID = "LearningPathMaterialsFilesContainer";

            DataTable learningPathMaterialsListing = this._LearningPathObject.GetLearningPathMaterials(null);

            if (learningPathMaterialsListing.Rows.Count > 0)
            {
                int count = 0;

                foreach (DataRow row in learningPathMaterialsListing.Rows)
                {
                    Panel learningPathMaterialFileLinkContainer = new Panel();
                    learningPathMaterialFileLinkContainer.ID = "LearningPathMaterialFileLinkContainer_" + count.ToString();
                    learningPathMaterialFileLinkContainer.CssClass = "LearningPathMaterialFileLinkContainer";

                    HyperLink learningPathMaterialFileLink = new HyperLink();
                    learningPathMaterialFileLink.Text = Convert.ToString(row["label"]);
                    learningPathMaterialFileLink.NavigateUrl = SitePathConstants.SITE_WAREHOUSE_DOCUMENTS_LEARNINGPATH + this._LearningPathObject.Id + "/" + Convert.ToString(row["filename"]);

                    learningPathMaterialFileLinkContainer.Controls.Add(learningPathMaterialFileLink);
                    learningPathMaterialsFilesContainer.Controls.Add(learningPathMaterialFileLinkContainer);
                }
            }

            learningPathMaterialsContainer.Controls.Add(learningPathMaterialsFilesContainer);

            this.LearningPathDetailsContainer.Controls.Add(descriptionContainer);
            this.LearningPathDetailsContainer.Controls.Add(learningPathMaterialsContainer);
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.LearningPathEnrollmentCoursesGrid.AddCheckboxColumn = false;
            this.LearningPathEnrollmentCoursesGrid.ShowSearchBox = false;
            this.LearningPathEnrollmentCoursesGrid.AllowPaging = false;
            this.LearningPathEnrollmentCoursesGrid.ShowRecordsPerPageSelectbox = false;

            this.LearningPathEnrollmentCoursesGrid.StoredProcedure = Library.LearningPathEnrollment.GridProcedureForLearnersStatus;
            this.LearningPathEnrollmentCoursesGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.LearningPathEnrollmentCoursesGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.LearningPathEnrollmentCoursesGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.LearningPathEnrollmentCoursesGrid.AddFilter("@idLearningPathEnrollment", SqlDbType.Int, 4, this._LearningPathEnrollmentObject.Id);
            this.LearningPathEnrollmentCoursesGrid.IdentifierField = "idLearningPathEnrollment";
            this.LearningPathEnrollmentCoursesGrid.DefaultSortColumn = "order";

            // data key names
            this.LearningPathEnrollmentCoursesGrid.DataKeyNames = new string[] { "idLearningPathEnrollment" };

            // columns
            GridColumn courseTitle = new GridColumn(_GlobalResources.CourseTitle, "courseTitle");
            GridColumn courseCompletionStatus = new GridColumn(_GlobalResources.Status, null, true); // contents of this column will be populated by the method that handles RowDataBound
            GridColumn courseLaunch = new GridColumn(_GlobalResources.Action, null, true); // contents of this column will be populated by the method that handles RowDataBound

            // add columns to data grid 
            this.LearningPathEnrollmentCoursesGrid.AddColumn(courseTitle);
            this.LearningPathEnrollmentCoursesGrid.AddColumn(courseCompletionStatus);
            this.LearningPathEnrollmentCoursesGrid.AddColumn(courseLaunch);
            this.LearningPathEnrollmentCoursesGrid.RowDataBound += new GridViewRowEventHandler(this._LearningPathEnrollmentCoursesGridView_RowDataBound);
        }
        #endregion

        #region _LearningPathEnrollmentCoursesGridView_RowDataBound
        /// <summary>
        /// Handles learning path enrollment courses grid row data bind event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LearningPathEnrollmentCoursesGridView_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                bool isCompleted = Convert.ToBoolean(rowView["isCompleted"]); // will never be null                
                int? idEnrollmentLaunchable = rowView["idEnrollmentLaunchable"] != DBNull.Value ? Convert.ToInt32(rowView["idEnrollmentLaunchable"]) : (int?)null;
                int? idEnrollmentLocked = rowView["idEnrollmentLocked"] != DBNull.Value ? Convert.ToInt32(rowView["idEnrollmentLocked"]) : (int?)null;

                if (isCompleted)
                {
                    // set the status text
                    e.Row.Cells[1].Text = _GlobalResources.Completed;

                    // if there is a launchable enrollment, add a launch button for review
                    if (idEnrollmentLaunchable != null)
                    {
                        HyperLink launchLink = new HyperLink();
                        launchLink.ID = "LaunchLink_" + idEnrollmentLaunchable.ToString();
                        launchLink.NavigateUrl = "Enrollment.aspx?idEnrollment=" + idEnrollmentLaunchable.ToString();
                        launchLink.ToolTip = _GlobalResources.ViewCourse;

                        Image launchLinkImage = new Image();
                        launchLinkImage.ID = "LaunchLinkImage_" + idEnrollmentLaunchable.ToString();
                        launchLinkImage.CssClass = "SmallIcon";
                        launchLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                        launchLinkImage.AlternateText = _GlobalResources.View;

                        launchLink.Controls.Add(launchLinkImage);

                        e.Row.Cells[2].Controls.Add(launchLink);     
                    }
                    else // no launchable enrollment, do not check for locked enrollment, just display a dim launch icon
                    {
                        Image noLaunchLinkImage = new Image();
                        noLaunchLinkImage.ID = "NoLaunchLinkImage_" + e.Row.RowIndex.ToString();
                        noLaunchLinkImage.CssClass = "SmallIcon DimIcon";
                        noLaunchLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                        noLaunchLinkImage.ToolTip = _GlobalResources.ViewCourseUnavailable;
                        noLaunchLinkImage.AlternateText = _GlobalResources.View;

                        e.Row.Cells[2].Controls.Add(noLaunchLinkImage);
                    }
                }
                else
                {
                    // set the status text
                    e.Row.Cells[1].Text = _GlobalResources.IncompleteInProgress;

                    // if there is a launchable enrollment, add a launch button
                    if (idEnrollmentLaunchable != null)
                    {                        
                        HyperLink launchLink = new HyperLink();
                        launchLink.ID = "LaunchLink_" + idEnrollmentLaunchable.ToString();
                        launchLink.NavigateUrl = "Enrollment.aspx?idEnrollment=" + idEnrollmentLaunchable.ToString();
                        launchLink.ToolTip = _GlobalResources.ViewCourse;

                        Image launchLinkImage = new Image();
                        launchLinkImage.ID = "LaunchLinkImage_" + idEnrollmentLaunchable.ToString();
                        launchLinkImage.CssClass = "SmallIcon";
                        launchLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                        launchLinkImage.AlternateText = _GlobalResources.View;

                        launchLink.Controls.Add(launchLinkImage);

                        e.Row.Cells[2].Controls.Add(launchLink);                        
                    }
                    else // no launchable enrollment, check for a locked enrollment so we can put a lock icon here
                    {
                        if (idEnrollmentLocked != null)
                        {                            
                            Image launchLockedLinkImage = new Image();
                            launchLockedLinkImage.ID = "LaunchLockedLinkImage_" + idEnrollmentLocked.ToString();
                            launchLockedLinkImage.CssClass = "SmallIcon";
                            launchLockedLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOCKED_BUTTON_GOLD, ImageFiles.EXT_PNG);
                            launchLockedLinkImage.ToolTip = _GlobalResources.Locked;
                            launchLockedLinkImage.AlternateText = _GlobalResources.Locked;

                            e.Row.Cells[2].Controls.Add(launchLockedLinkImage);
                        }
                        else // no locked enrollment, so put a dim launch icon here
                        {                            
                            Image noLaunchLinkImage = new Image();
                            noLaunchLinkImage.ID = "NoLaunchLinkImage_" + e.Row.RowIndex.ToString();
                            noLaunchLinkImage.CssClass = "SmallIcon DimIcon";
                            noLaunchLinkImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG);
                            noLaunchLinkImage.ToolTip = _GlobalResources.ViewCourseUnavailable;
                            noLaunchLinkImage.AlternateText = _GlobalResources.View;

                            e.Row.Cells[2].Controls.Add(noLaunchLinkImage);
                        }
                    }
                }
            }
        }
        #endregion
    }
}