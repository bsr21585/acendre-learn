﻿// Function to kick off the Authorize.net process
function SubmitOrder() {
    // combine first name and last name fields for Authorize.net Accept name field
    var firstName = $("#CardholderFirstName_Field").val();
    var lastName = $("#CardholderLastName_Field").val();
    $("#AuthorizeAcceptCardholderNameField").val(firstName + " " + lastName);
    
    // scroll to top
    $(document).scrollTop(0);

    // hide the form
    $("#CheckoutFormWrapperContainer").hide();

    // show the processing message
    $("#CheckoutFormProcessingContainer").show();

    // validate that the fields Authorize.net Accept marks as "optional" have been filled out
    // note, this may go away in a real production setting, but in Developer Mode (which is used to test this) it takes anything
    if ($("#CardholderFirstName_Field").val() == "") {
        $("#AuthorizeAcceptResponseStatus").val("ERROR");
        $("#AuthorizeAcceptResponseErrorMessages").val("E_WC_17 - " + acceptJSErrorMessages["E_WC_17"]);
        $("#AuthorizeNetHiddenPostbackButton").click();
    }
    else if ($("#CardholderLastName_Field").val() == "") {
        $("#AuthorizeAcceptResponseStatus").val("ERROR");
        $("#AuthorizeAcceptResponseErrorMessages").val("E_WC_17 - " + acceptJSErrorMessages["E_WC_17"]);
        $("#AuthorizeNetHiddenPostbackButton").click();
    }
    else if ($("#CreditCardCVV2_Field").val() == "") {
        $("#AuthorizeAcceptResponseStatus").val("ERROR");
        $("#AuthorizeAcceptResponseErrorMessages").val("E_WC_15 - " + acceptJSErrorMessages["E_WC_15"]);
        $("#AuthorizeNetHiddenPostbackButton").click();
    }
    else {
        // send the payment data
        objAuthorizeNetAccept.SendPaymentData();
    }
}

// Callback function to initiate Authorize.net postback after client-side process is finished.
function AuthorizeNetResponseCallback() {
    // clear the credit card information from the fields so that it doesnt hit the server on postback
    $("#CreditCardNumber_Field").val("");
    $("#CreditCardExpirationMonth_Field").val("");
    $("#CreditCardExpirationYear_Field").val("");
    $("#CreditCardCVV2_Field").val("");

    // initiate the postback
    $("#AuthorizeNetHiddenPostbackButton").click();
}