﻿using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.MyProfile
{
    public class MyCart : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel MyCartFormContentWrapperContainer;
        public Panel MyCartFormContentContainer;
        public UpdatePanel MyCartGridUpdatePanel;        
        public Grid MyCartGrid;
        public Panel ActionsPanel;        
        #endregion

        #region Private Properties
        private EcommerceSettings _EcommerceSettings;

        private Purchase _PurchaseObject;
        private HiddenField _HiddenTransactionItemId;

        private double _PurchaseTotal;
        private UpdatePanel _PurchaseTotalUpdatePanel;        
        
        private ModalPopup _ApplyCouponModal;
        private Button _ApplyCouponCodeModalButton;
        private TextBox _ApplyCouponCodeTextBox;                

        private ModalPopup _RemoveCouponModal;
        private Button _RemoveCouponCodeModalButton;

        private LinkButton _RemoveItemsButton;
        private ModalPopup _GridConfirmAction;
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(CouponCode), "Asentia.LMS.Pages.MyProfile.MyCart.js");
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get e-commerce settings, then check to ensure ecommerce is set and verified on the portal, and that the processor is not paypal
            this._EcommerceSettings = new EcommerceSettings();

            if (!this._EcommerceSettings.IsEcommerceSetAndVerified || (_EcommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL))
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/myprofile/MyCart.css");            

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.MyCartFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.MyCartFormContentContainer.CssClass = "FormContentContainer";

            // get the purchase object
            this._GetPurchaseObject();

            // build the object options panel
            this._BuildObjectOptionsPanel();

            // build the page controls
            this._BuildControls();

            // if not postback
            if (!Page.IsPostBack)
            {                
                // bind data grid
                this.MyCartGrid.BindData();
            }

            // show or hide the remove items button based on whether or not there are transaction items
            if (this.MyCartGrid.RowCount > 0)
            { this._RemoveItemsButton.Visible = true; }
            else
            { this._RemoveItemsButton.Visible = false; }
        }
        #endregion

        #region _GetPurchaseObject
        /// <summary>
        /// Gets the purchase object for the user's active cart.
        /// </summary>
        private void _GetPurchaseObject()
        {            
            this._PurchaseObject = new Purchase();
            this._PurchaseObject.GetActiveCartForUser(AsentiaSessionState.IdSiteUser, false);            
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.ShoppingCart));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.General, _GlobalResources.ShoppingCart, ImageFiles.GetIconPath(ImageFiles.ICON_CART, ImageFiles.EXT_PNG));
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.Controls.Clear();
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // CONTINUE SHOPPING
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("ContinueShoppingLink",
                                                null,
                                                "/catalog/",
                                                null,
                                                _GlobalResources.ContinueShopping,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG))
                                                );

            // CHECKOUT
            if (this._PurchaseObject != null && this._PurchaseObject.Id > 0)
            {
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("CheckoutLink",
                                                    null,
                                                    "Checkout.aspx",
                                                    null,
                                                    _GlobalResources.Checkout,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_CART, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_CART, ImageFiles.EXT_PNG))
                    );
            }

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {                
            // initialize _PurchaseTotal to 0
            this._PurchaseTotal = 0;

            // build the cart items grid
            this._BuildGrid();

            // build the actions panel for the cart items grid
            this._BuildActionsPanel();

            // build the grid action modal
            this._BuildGridActionsModal();

            // build the apply coupon code modal
            this._BuildApplyCouponCodeModal();

            // build the remove coupon code modal
            this._BuildRemoveCouponCodeModal();
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {            
            this.MyCartGrid.StoredProcedure = TransactionItem.GetGridForPurchase;
            this.MyCartGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.MyCartGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.MyCartGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.MyCartGrid.AddFilter("@idPurchase", SqlDbType.Int, 4, this._PurchaseObject.Id);
            this.MyCartGrid.IdentifierField = "idTransactionItem";
            this.MyCartGrid.DefaultSortColumn = "idTransactionItem";
            this.MyCartGrid.AllowPaging = false;
            this.MyCartGrid.ShowSearchBox = false;
            this.MyCartGrid.ShowRecordsPerPageSelectbox = false;
            this.MyCartGrid.EmptyDataText = _GlobalResources.YouDoNotCurrentlyHaveAnyItemsInYourCart;

            // data key names
            this.MyCartGrid.DataKeyNames = new string[] { "idTransactionItem", "couponCode" };

            // columns
            GridColumn cartItemType = new GridColumn(_GlobalResources.Item, "itemType");
            cartItemType.AddProperty(new GridColumnProperty("1", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.Course + "\" />"));
            cartItemType.AddProperty(new GridColumnProperty("2", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.Catalog + "\" />"));
            cartItemType.AddProperty(new GridColumnProperty("3", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.LearningPath + "\" />"));
            cartItemType.AddProperty(new GridColumnProperty("4", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.InstructorLedTraining + "\" />"));

            GridColumn item = new GridColumn(String.Empty, "itemName");
            GridColumn cost = new GridColumn(_GlobalResources.Price, "cost");

            GridColumn couponCode = new GridColumn(_GlobalResources.CouponCode, "isCouponCodeApplied", true);
            couponCode.AddProperty(new GridColumnProperty("0", ""));
            couponCode.AddProperty(new GridColumnProperty("1", "<a href=\"javascript:void(0);\" class=\"ImageLink\" onclick=\"RemoveCouponCodeClick(##idTransactionItem##); return false;\">"
                                                               + "<img class=\"XSmallIcon\" src=\""
                                                               + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG)
                                                               + "\" alt=\"" + _GlobalResources.RemoveCouponCode + "\" title=\"" + _GlobalResources.RemoveCouponCode + "\" />"
                                                               + "</a> ##couponCode##"));           

            GridColumn paid = new GridColumn(_GlobalResources.Amount, "paid");

            GridColumn applyCouponCode = new GridColumn(_GlobalResources.ApplyCouponCode, "idCouponCode", true);
            applyCouponCode.AddProperty(new GridColumnProperty("True", "<a href=\"javascript:void(0);\" onclick=\"ApplyCouponCodeClick(##idTransactionItem##); return false;\">"
                                                                       + "<img class=\"SmallIcon\" src=\""
                                                                       + ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG)
                                                                       + "\" alt=\"" + _GlobalResources.ApplyCouponCode + "\" />"
                                                                       + "</a>"));            

            // add columns to data grid 
            this.MyCartGrid.AddColumn(cartItemType);
            this.MyCartGrid.AddColumn(item);
            this.MyCartGrid.AddColumn(cost);
            this.MyCartGrid.AddColumn(couponCode);
            this.MyCartGrid.AddColumn(paid);
            this.MyCartGrid.AddColumn(applyCouponCode);

            // apply data bound events
            this.MyCartGrid.RowDataBound += this._MyCartGrid_RowDataBound;
            this.MyCartGrid.DataBound += this._MyCartGrid_DataBound;
        }
        #endregion

        #region _MyCartGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the my cart grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MyCartGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // set the currency symbol for NumberFormat so we can use it to format currency
            CultureInfo ci = new CultureInfo(AsentiaSessionState.UserCulture);
            ci.NumberFormat.CurrencySymbol = this._EcommerceSettings.CurrencySymbol;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                // ADD ITEM TITLE, DESCRIPTION, AND "INCLUDES COURSES" (IF CATALOG OR LEARNING PATH) TO ITEM COLUMN

                int itemType = Convert.ToInt32(rowView["itemType"]);
                string itemTitle = rowView["itemName"].ToString();
                string itemDescription = rowView["description"].ToString();
                string includedCourses = rowView["includedCourses"].ToString();

                Panel itemInformationContainer = new Panel();
                itemInformationContainer.CssClass = "ItemInformationContainer";

                // title
                Panel itemTitleContainer = new Panel();
                itemTitleContainer.CssClass = "ItemTitleContainer";
                itemInformationContainer.Controls.Add(itemTitleContainer);

                Literal itemTitleLit = new Literal();
                itemTitleLit.Text = itemTitle;
                itemTitleContainer.Controls.Add(itemTitleLit);

                // description
                Panel itemDescriptionContainer = new Panel();
                itemDescriptionContainer.CssClass = "ItemDescriptionContainer";
                itemInformationContainer.Controls.Add(itemDescriptionContainer);

                Literal itemDescriptionLit = new Literal();
                itemDescriptionLit.Text = itemDescription;
                itemDescriptionContainer.Controls.Add(itemDescriptionLit);

                // "includes courses"
                if (itemType == (int)PurchaseItemType.Catalog || itemType == (int)PurchaseItemType.LearningPath)
                {
                    Panel itemIncludesContainer = new Panel();
                    itemIncludesContainer.CssClass = "ItemIncludesContainer";
                    itemInformationContainer.Controls.Add(itemIncludesContainer);

                    Literal itemIncludesLit = new Literal();
                    itemIncludesLit.Text = _GlobalResources.IncludesTheFollowingCourse_s + ": " + includedCourses;
                    itemIncludesContainer.Controls.Add(itemIncludesLit);
                }

                // attach the item information container to the item cell
                e.Row.Cells[2].Controls.Add(itemInformationContainer);
                

                // FORMAT "PRICE" (COST) AND "AMOUNT" (PAID) COLUMNS

                double cost = 0;
                double paid = 0;

                // parse out cost and paid
                double.TryParse(rowView["cost"].ToString(), out cost);
                double.TryParse(rowView["paid"].ToString(), out paid);

                // add the item "paid" price to the total price
                this._PurchaseTotal += paid;
                
                // add cost and paid to their correct columns in the table
                e.Row.Cells[3].Text = cost.ToString("C", ci);
                e.Row.Cells[5].Text = paid.ToString("C", ci);                
            }
        }
        #endregion

        #region _MyCartGrid_DataBound
        /// <summary>
        /// Handles the complete data bound event for the my cart grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MyCartGrid_DataBound(object sender, EventArgs e)
        {
            // set the currency symbol for NumberFormat so we can use it to format currency
            CultureInfo ci = new CultureInfo(AsentiaSessionState.UserCulture);
            ci.NumberFormat.CurrencySymbol = this._EcommerceSettings.CurrencySymbol;

            // put the total amount of the purchase into a label control
            Label purchaseTotalLabel = new Label();
            purchaseTotalLabel.ID = "PurchaseTotalLabel";
            purchaseTotalLabel.CssClass = "PurchaseTotalLabel";
            purchaseTotalLabel.Text = _GlobalResources.Total + " (" + this._EcommerceSettings.CurrencyCode + "): " + this._PurchaseTotal.ToString("C", ci);

            // put the label inside an update panel so we can affect it on partial postbacks
            this._PurchaseTotalUpdatePanel = new UpdatePanel();
            this._PurchaseTotalUpdatePanel.ID = "PurchaseTotalUpdatePanel";
            this._PurchaseTotalUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            this._PurchaseTotalUpdatePanel.ContentTemplateContainer.Controls.Add(purchaseTotalLabel);

            // attach the update panel to the actions panel, note that we are attaching this outside of the 
            // update panel that holds the grid because an update panel inside another update panel is a no-no
            if (this.ActionsPanel != null)
            { this.ActionsPanel.Controls.Add(this._PurchaseTotalUpdatePanel); }
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Grid data.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.CssClass = "ActionsPanel";

            // remove items button
            this._RemoveItemsButton = new LinkButton();
            this._RemoveItemsButton.ID = "GridRemoveItemsButton";
            this._RemoveItemsButton.CssClass = "GridDeleteButton";

            // remove items button image
            Image removeItemsImage = new Image();
            removeItemsImage.ID = "GridRemoveItemsButtonImage";
            removeItemsImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            removeItemsImage.CssClass = "MediumIcon";
            removeItemsImage.AlternateText = _GlobalResources.Delete;
            this._RemoveItemsButton.Controls.Add(removeItemsImage);

            // remove items button text
            Literal removeItemsText = new Literal();
            removeItemsText.Text = _GlobalResources.RemoveSelectedItem_s;
            this._RemoveItemsButton.Controls.Add(removeItemsText);

            // add remove items button to panel
            this.ActionsPanel.Controls.Add(this._RemoveItemsButton);


            // add hidden field to hold the transaction item id when applying or removing a coupon code
            this._HiddenTransactionItemId = new HiddenField();            
            this._HiddenTransactionItemId.ID = "HiddenTransactionItemId";
            this.ActionsPanel.Controls.Add(this._HiddenTransactionItemId);

            // add hidden button to use as the apply coupon code modal launch trigger
            this._ApplyCouponCodeModalButton = new Button();
            this._ApplyCouponCodeModalButton.ID = "ApplyCouponCodeModalButton";
            this._ApplyCouponCodeModalButton.Style.Add("display", "none");
            this.ActionsPanel.Controls.Add(this._ApplyCouponCodeModalButton);

            // add hidden button to use as the remove coupon code modal launch trigger
            this._RemoveCouponCodeModalButton = new Button();
            this._RemoveCouponCodeModalButton.ID = "RemoveCouponCodeModalButton";
            this._RemoveCouponCodeModalButton.Style.Add("display", "none");
            this.ActionsPanel.Controls.Add(this._RemoveCouponCodeModalButton);
        }
        #endregion        

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for remove item action performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {            
            this._GridConfirmAction = new ModalPopup("GridConfirmAction");

            // set modal properties
            this._GridConfirmAction.Type = ModalPopupType.Confirm;
            this._GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG);
            this._GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this._GridConfirmAction.HeaderText = _GlobalResources.RemoveSelectedItem_s;
            this._GridConfirmAction.TargetControlID = this._RemoveItemsButton.ClientID;
            this._GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._RemoveItemsButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();
            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToRemoveTheseItem_sFromYourCart;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ActionsPanel.Controls.Add(this._GridConfirmAction);
        }
        #endregion

        #region _RemoveItemsButton_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _RemoveItemsButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable();
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.MyCartGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.MyCartGrid.Rows[i].FindControl(this.MyCartGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    TransactionItem.Delete(recordsToDelete);

                    // display the success message
                    this.DisplayFeedback(_GlobalResources.TheSelectedItem_sHaveBeenRemovedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedback(_GlobalResources.NoItem_sSelectedForRemoval, true);
                }                
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the cart data
                this.MyCartGrid.BindData();

                // show or hide the remove items button based on whether or not there are transaction items
                if (this.MyCartGrid.RowCount > 0)
                { this._RemoveItemsButton.Visible = true; }
                else
                { this._RemoveItemsButton.Visible = false; }

                // update the update panels
                this.MyCartGridUpdatePanel.Update();
                this._PurchaseTotalUpdatePanel.Update();
            }
        }
        #endregion

        #region _BuildApplyCouponCodeModal
        /// <summary>
        /// Builds the apply coupon code modal.
        /// </summary>
        private void _BuildApplyCouponCodeModal()
        {
            // set modal properties
            this._ApplyCouponModal = new ModalPopup("ApplyCouponCodeModal");
            this._ApplyCouponModal.Type = ModalPopupType.Confirm;
            this._ApplyCouponModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG);
            this._ApplyCouponModal.HeaderIconAlt = _GlobalResources.ApplyCouponCode;
            this._ApplyCouponModal.HeaderText = _GlobalResources.ApplyCouponCode;
            this._ApplyCouponModal.TargetControlID = this._ApplyCouponCodeModalButton.ID;
            this._ApplyCouponModal.SubmitButton.Command += new CommandEventHandler(this._ApplyCouponModalSubmit_Command);
            this._ApplyCouponModal.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._ApplyCouponModal.SubmitButtonCustomText = _GlobalResources.ApplyCouponCode;
            this._ApplyCouponModal.CloseButtonTextType = ModalPopupButtonText.Close;            

            // build the modal body  
            this._ApplyCouponCodeTextBox = new TextBox();
            this._ApplyCouponCodeTextBox.ID = "CouponCodeToApply_Field";
            this._ApplyCouponCodeTextBox.CssClass = "InputMedium";
            this._ApplyCouponCodeTextBox.MaxLength = 10;

            // give focus to the text box control when modal is launched
            this._ApplyCouponModal.FocusControlIdOnLaunch = this._ApplyCouponCodeTextBox.ID;

            // add controls to body                       
            this._ApplyCouponModal.AddControlToBody(AsentiaPage.BuildFormField("CouponCode",
                                                                        _GlobalResources.CouponCode,
                                                                        this._ApplyCouponCodeTextBox.ID,
                                                                        this._ApplyCouponCodeTextBox,
                                                                        false,
                                                                        false,
                                                                        false));

            // add modal to container
            this.ActionsPanel.Controls.Add(this._ApplyCouponModal);
        }
        #endregion

        #region _ApplyCouponModalSubmit_Command
        /// <summary>
        /// Applies coupon code (if applicable) to item, enrolls in item if coupon makes it free, and adjusts item cost if item still has a cost.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ApplyCouponModalSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate that the processor is set and verified and is not PayPal - this is just for extreme safety
                if (!this._EcommerceSettings.IsEcommerceSetAndVerified || this._EcommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
                { throw new AsentiaException(_GlobalResources.CouldNotConnectToPaymentProcessorPleaseContactAnAdministrator); }

                // get the transaction item id, die if not valid
                int idTransactionItem;

                if (String.IsNullOrEmpty(this._HiddenTransactionItemId.Value) || !Int32.TryParse(this._HiddenTransactionItemId.Value, out idTransactionItem))
                { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingTheTransactionPleaseContactAnAdministrator); }

                // if the coupon code field was empty, throw an error
                if (String.IsNullOrWhiteSpace(this._ApplyCouponCodeTextBox.Text))
                { throw new AsentiaException(_GlobalResources.YouMustEnterACouponCode); }

                // if we get here, apply the coupon code
                TransactionItem.ApplyCoupon(idTransactionItem, this._ApplyCouponCodeTextBox.Text);

                // get the transaction item object
                TransactionItem transactionItemObject = new TransactionItem(idTransactionItem);

                // if the effective price is 0, just enroll the user, confirm and detach the item from purchase, and report success
                if (transactionItemObject.Paid == 0)
                {
                    // enroll based on item type
                    switch (transactionItemObject.ItemType)
                    {
                        case PurchaseItemType.Catalog:
                            // enroll the user in the catalog
                            LMS.Library.Enrollment catalogEnrollmentObject = new LMS.Library.Enrollment();
                            catalogEnrollmentObject.EnrollCatalog((int)transactionItemObject.ItemId, AsentiaSessionState.IdSiteUser, transactionItemObject.Id);

                            // display feedback
                            this.DisplayFeedback(String.Format(_GlobalResources.YouHaveBeenEnrolledInAllCoursesWithinThisCatalogSuccessfullyClickHereToGoToYourDashboardToViewYourCourseEnrollments, "<a href=\"/dashboard\">", "</a>"), false);

                            break;
                        case PurchaseItemType.Course:
                            // enroll the user in the course
                            LMS.Library.Enrollment courseEnrollmentObject = new LMS.Library.Enrollment();
                            courseEnrollmentObject.EnrollCourse((int)transactionItemObject.ItemId, AsentiaSessionState.IdSiteUser, transactionItemObject.Id);

                            // if there is an ilt session linked to this course, enroll the user into the session
                            if (transactionItemObject.IdIltSession != null)
                            {
                                StandupTrainingInstance session = new StandupTrainingInstance(Convert.ToInt32(transactionItemObject.IdIltSession));

                                if (session.SeatsRemaining > 0)
                                { session.JoinUser(AsentiaSessionState.IdSiteUser, false); }
                                else if (session.WaitingSeatsRemaining > 0)
                                { session.JoinUser(AsentiaSessionState.IdSiteUser, true); }

                            }

                            // display feedback                            
                            this.DisplayFeedback(String.Format(_GlobalResources.YouHaveBeenEnrolledInThisCourseSuccessfullyClickHereToGoToYourCourseEnrollment, "<a href=\"/dashboard/Enrollment.aspx?idEnrollment=" + courseEnrollmentObject.Id.ToString() + "\">", "</a>"), false);

                            break;
                        case PurchaseItemType.LearningPath:
                            // enroll the user in the learning path
                            LMS.Library.LearningPathEnrollment learningPathEnrollmentObject = new LMS.Library.LearningPathEnrollment();
                            learningPathEnrollmentObject.EnrollLearningPath((int)transactionItemObject.ItemId, AsentiaSessionState.IdSiteUser, transactionItemObject.Id);

                            // display feedback                            
                            this.DisplayFeedback(String.Format(_GlobalResources.YouHaveBeenEnrolledInThisLearningPathSuccessfullyClickHereToGoToYourLearningPathEnrollment, "<a href=\"/dashboard/LearningPathEnrollment.aspx?id=" + learningPathEnrollmentObject.Id + "\">", "</a>"), false);

                            break;
                        case PurchaseItemType.InstructorLedTraining:
                            // enroll in instructor led training instance
                            StandupTrainingInstance instructorLedTrainingInstance = new StandupTrainingInstance((int)transactionItemObject.ItemId);

                            if (instructorLedTrainingInstance.SeatsRemaining > 0)
                            { instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, false); }
                            else if (instructorLedTrainingInstance.WaitingSeatsRemaining > 0)
                            { instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, true); }
                            else
                            { throw new AsentiaException(_GlobalResources.ThereAreNoSeatsAvailableInTheSelectedInstructorLedTrainingSessionPleaseSelectAnotherSession); }

                            // display feedback
                            this.DisplayFeedback(_GlobalResources.YouHaveBeenJoinedToTheInstructorLedTrainingSessionSuccessfully, false);

                            break;
                        default:
                            throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingTheTransactionPleaseContactAnAdministrator);
                    }

                    // confirm and detach the item from purchase
                    DataTable transactionIdsToConfirm = new DataTable();
                    transactionIdsToConfirm.Columns.Add("id", typeof(int));

                    transactionIdsToConfirm.Rows.Add(transactionItemObject.Id);

                    TransactionItem.ConfirmTransactionItems(transactionIdsToConfirm, true);
                }
                else // show confirmation that the coupon code has been applied
                { this.DisplayFeedback(_GlobalResources.TheCouponCodeHasBeenAppliedToTheSelectedItem, false); }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the cart data
                this.MyCartGrid.BindData();

                // show or hide the remove items button based on whether or not there are transaction items
                if (this.MyCartGrid.RowCount > 0)
                { this._RemoveItemsButton.Visible = true; }
                else
                { this._RemoveItemsButton.Visible = false; }

                // update the update panels
                this.MyCartGridUpdatePanel.Update();
                this._PurchaseTotalUpdatePanel.Update();
            }
        }
        #endregion

        #region _BuildRemoveCouponCodeModal
        /// <summary>
        /// Builds the remove coupon code modal.
        /// </summary>
        private void _BuildRemoveCouponCodeModal()
        {
            // set modal properties
            this._RemoveCouponModal = new ModalPopup("RemoveCouponCodeModal");
            this._RemoveCouponModal.Type = ModalPopupType.Confirm;
            this._RemoveCouponModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG);
            this._RemoveCouponModal.HeaderIconAlt = _GlobalResources.RemoveCouponCode;
            this._RemoveCouponModal.HeaderText = _GlobalResources.RemoveCouponCode;
            this._RemoveCouponModal.TargetControlID = this._RemoveCouponCodeModalButton.ID;
            this._RemoveCouponModal.SubmitButton.Command += new CommandEventHandler(this._RemoveCouponModalSubmit_Command);            

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");

            Literal body1 = new Literal();

            body1Wrapper.ID = "RemoveCouponCodeModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToRemoveTheCouponFromThisItem;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this._RemoveCouponModal.AddControlToBody(body1Wrapper);

            // add modal to container
            this.ActionsPanel.Controls.Add(this._RemoveCouponModal);
        }
        #endregion

        #region _RemoveCouponModalSubmit_Command
        /// <summary>
        /// handles remove coupon code process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RemoveCouponModalSubmit_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate that the processor is set and verified and is not PayPal - this is just for extreme safety
                if (!this._EcommerceSettings.IsEcommerceSetAndVerified || this._EcommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
                { throw new AsentiaException(_GlobalResources.CouldNotConnectToPaymentProcessorPleaseContactAnAdministrator); }

                // get the transaction item id, die if not valid
                int idTransactionItem;

                if (String.IsNullOrEmpty(this._HiddenTransactionItemId.Value) || !Int32.TryParse(this._HiddenTransactionItemId.Value, out idTransactionItem))
                { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingTheTransactionPleaseContactAnAdministrator); }

                // remove the coupon code
                TransactionItem.RemoveCoupon(idTransactionItem);

                // display success message
                this.DisplayFeedback(_GlobalResources.TheCouponCodeHasBeenRemovedFromTheSelectedItem, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            finally
            {
                // rebind the cart data
                this.MyCartGrid.BindData();

                // show or hide the remove items button based on whether or not there are transaction items
                if (this.MyCartGrid.RowCount > 0)
                { this._RemoveItemsButton.Visible = true; }
                else
                { this._RemoveItemsButton.Visible = false; }

                // update the update panels
                this.MyCartGridUpdatePanel.Update();
                this._PurchaseTotalUpdatePanel.Update();
            }
        }
        #endregion
    }
}