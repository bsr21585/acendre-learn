﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;
using Asentia.UMS.Library;

namespace Asentia.LMS.Pages.MyProfile
{
    public class Receipt : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ReceiptPageContentWrapperContainer;
        public Panel ReceiptPageContentContainer;
        public Panel PurchaseBaseInformationContainer;
        public Grid PurchaseItemsGrid;
        public Panel PurchaseTotalContainer;        
        #endregion

        #region Private Properties
        private EcommerceSettings _EcommerceSettings;
        private Purchase _PurchaseObject;
        private User _UserObject;
        private double _PurchaseTotal;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get e-commerce settings, then check to ensure ecommerce is set and verified on the portal
            this._EcommerceSettings = new EcommerceSettings();

            if (!this._EcommerceSettings.IsEcommerceSetAndVerified)
            { Response.Redirect("/"); }

            // get the purchase and user objects
            this._GetPurchaseAndUserObjects();

            // if this is not a completed purchase, bounce
            if (this._PurchaseObject.TimeStamp == null)
            { Response.Redirect("/"); }

            // check permissions, if the caller is not a user manager, then the purchase must belong to the logged in user
            if (!AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager, this._UserObject.GroupMemberships))
            {
                if (this._PurchaseObject.IdUser != AsentiaSessionState.IdSiteUser)
                { Response.Redirect("/"); }
            }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/myprofile/Receipt.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.ReceiptPageContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ReceiptPageContentContainer.CssClass = "FormContentContainer";

            // build the page controls
            this._BuildControls();

            // if not postback
            if (!Page.IsPostBack)
            {
                // bind data grid
                this.PurchaseItemsGrid.BindData();
            }
        }
        #endregion

        #region _GetPurchaseAndUserObjects
        /// <summary>
        /// Gets the purchase object from querystring and the user object from Purchase.IdUser.
        /// </summary>
        private void _GetPurchaseAndUserObjects()
        {            
            // get the id querystring parameter
            int qsId = this.QueryStringInt("id", 0);
            int vsId = this.ViewStateInt(this.ViewState, "id", 0);

            if (qsId > 0 || vsId > 0)
            {
                int id = 0;

                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }

                try
                {
                    if (id > 0)
                    { 
                        // get the purchase object
                        this._PurchaseObject = new Purchase(id);

                        // get the user object
                        this._UserObject = new User(this._PurchaseObject.IdUser);
                    }
                }
                catch
                { Response.Redirect("/"); }
            }
            else
            { Response.Redirect("/"); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Receipt));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.General, _GlobalResources.Receipt, ImageFiles.GetIconPath(ImageFiles.ICON_RECEIPT, ImageFiles.EXT_PNG));
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // initialize _PurchaseTotal to 0
            this._PurchaseTotal = 0;

            // build the receipt base information container
            this._BuildReceiptBaseInformationContainer();

            // build the cart items grid
            this._BuildGrid();
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.PurchaseItemsGrid.StoredProcedure = TransactionItem.GetGridForPurchase;
            this.PurchaseItemsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.PurchaseItemsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.PurchaseItemsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.PurchaseItemsGrid.AddFilter("@idPurchase", SqlDbType.Int, 4, this._PurchaseObject.Id);
            this.PurchaseItemsGrid.IdentifierField = "idTransactionItem";
            this.PurchaseItemsGrid.DefaultSortColumn = "idTransactionItem";
            this.PurchaseItemsGrid.AddCheckboxColumn = false;
            this.PurchaseItemsGrid.AllowPaging = false;
            this.PurchaseItemsGrid.ShowSearchBox = false;
            this.PurchaseItemsGrid.ShowRecordsPerPageSelectbox = false;
            this.PurchaseItemsGrid.EmptyDataText = _GlobalResources.ThereAreNoItemsAssociatedWithThisPurchase;

            // data key names
            this.PurchaseItemsGrid.DataKeyNames = new string[] { "idTransactionItem" };

            // columns
            GridColumn cartItemType = new GridColumn(_GlobalResources.Item, "itemType");
            cartItemType.AddProperty(new GridColumnProperty("1", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.Course + "\" />"));
            cartItemType.AddProperty(new GridColumnProperty("2", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.Catalog + "\" />"));
            cartItemType.AddProperty(new GridColumnProperty("3", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.LearningPath + "\" />"));
            cartItemType.AddProperty(new GridColumnProperty("4", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.InstructorLedTraining + "\" />"));

            GridColumn item = new GridColumn(String.Empty, "itemName");
            GridColumn cost = new GridColumn(_GlobalResources.Price, "cost");
            GridColumn paid = new GridColumn(_GlobalResources.Amount, "paid");

            // add columns to data grid 
            this.PurchaseItemsGrid.AddColumn(cartItemType);
            this.PurchaseItemsGrid.AddColumn(item);
            this.PurchaseItemsGrid.AddColumn(cost);
            this.PurchaseItemsGrid.AddColumn(paid);

            // apply data bound events
            this.PurchaseItemsGrid.RowDataBound += this._PurchaseItemsGrid_RowDataBound;
            this.PurchaseItemsGrid.DataBound += this._PurchaseItemsGrid_DataBound;
        }
        #endregion

        #region _PurchaseItemsGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the purchase items grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _PurchaseItemsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // set the currency symbol for NumberFormat so we can use it to format currency
            CultureInfo ci = new CultureInfo(AsentiaSessionState.UserCulture);
            ci.NumberFormat.CurrencySymbol = this._EcommerceSettings.CurrencySymbol;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                // ADD ITEM TITLE, DESCRIPTION, AND "INCLUDES COURSES" (IF CATALOG OR LEARNING PATH) TO ITEM COLUMN

                int itemType = Convert.ToInt32(rowView["itemType"]);
                string itemTitle = rowView["itemName"].ToString();
                string itemDescription = rowView["description"].ToString();
                string includedCourses = rowView["includedCourses"].ToString();

                Panel itemInformationContainer = new Panel();
                itemInformationContainer.CssClass = "ItemInformationContainer";

                // title
                Panel itemTitleContainer = new Panel();
                itemTitleContainer.CssClass = "ItemTitleContainer";
                itemInformationContainer.Controls.Add(itemTitleContainer);

                Literal itemTitleLit = new Literal();
                itemTitleLit.Text = itemTitle;
                itemTitleContainer.Controls.Add(itemTitleLit);

                // description
                Panel itemDescriptionContainer = new Panel();
                itemDescriptionContainer.CssClass = "ItemDescriptionContainer";
                itemInformationContainer.Controls.Add(itemDescriptionContainer);

                Literal itemDescriptionLit = new Literal();
                itemDescriptionLit.Text = itemDescription;
                itemDescriptionContainer.Controls.Add(itemDescriptionLit);

                // "includes courses"
                if (itemType == (int)PurchaseItemType.Catalog || itemType == (int)PurchaseItemType.LearningPath)
                {
                    Panel itemIncludesContainer = new Panel();
                    itemIncludesContainer.CssClass = "ItemIncludesContainer";
                    itemInformationContainer.Controls.Add(itemIncludesContainer);

                    Literal itemIncludesLit = new Literal();
                    itemIncludesLit.Text = _GlobalResources.IncludesTheFollowingCourse_s + ": " + includedCourses;
                    itemIncludesContainer.Controls.Add(itemIncludesLit);
                }

                // attach the item information container to the item cell
                e.Row.Cells[1].Controls.Add(itemInformationContainer);


                // FORMAT "PRICE" (COST) AND "AMOUNT" (PAID) COLUMNS

                double cost = 0;
                double paid = 0;

                // parse out cost and paid
                double.TryParse(rowView["cost"].ToString(), out cost);
                double.TryParse(rowView["paid"].ToString(), out paid);

                // add the item "paid" price to the total price
                this._PurchaseTotal += paid;

                // add cost and paid to their correct columns in the table
                e.Row.Cells[2].Text = cost.ToString("C", ci);
                e.Row.Cells[3].Text = paid.ToString("C", ci);
            }
        }
        #endregion

        #region _PurchaseItemsGrid_DataBound
        /// <summary>
        /// Handles the complete data bound event for the purchase items grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _PurchaseItemsGrid_DataBound(object sender, EventArgs e)
        {
            // set the currency symbol for NumberFormat so we can use it to format currency
            CultureInfo ci = new CultureInfo(AsentiaSessionState.UserCulture);
            ci.NumberFormat.CurrencySymbol = this._EcommerceSettings.CurrencySymbol;

            // put the total amount of the purchase into a label control
            Label purchaseTotalLabel = new Label();
            purchaseTotalLabel.ID = "PurchaseTotalLabel";
            purchaseTotalLabel.CssClass = "PurchaseTotalLabel";
            purchaseTotalLabel.Text = _GlobalResources.Total + " (" + this._EcommerceSettings.CurrencyCode + "): " + this._PurchaseTotal.ToString("C", ci);

            // put the label inside the container panel            
            this.PurchaseTotalContainer.Controls.Add(purchaseTotalLabel);
        }
        #endregion

        #region _BuildReceiptBaseInformationContainer
        /// <summary>
        /// Builds the container that displays the base purchase information for the receipt.
        /// </summary>
        private void _BuildReceiptBaseInformationContainer()
        {
            // "Purchase Company Information" container

            Panel purchaseCompanyInformationContainer = new Panel();
            purchaseCompanyInformationContainer.ID = "PurchaseCompanyInformationContainer";
            this.PurchaseBaseInformationContainer.Controls.Add(purchaseCompanyInformationContainer);

            // company image
            Panel companyImageContainer = new Panel();
            companyImageContainer.ID = "CompanyImageContainer";
            purchaseCompanyInformationContainer.Controls.Add(companyImageContainer);

            Image companyImage = new Image();
            companyImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY, ImageFiles.EXT_PNG);
            companyImage.AlternateText = _GlobalResources.Company;
            companyImage.CssClass = "LargeIcon";
            companyImageContainer.Controls.Add(companyImage);

            // company portal information
            Panel companyPortalInformationContainer = new Panel();
            companyPortalInformationContainer.ID = "CompanyPortalInformationContainer";
            purchaseCompanyInformationContainer.Controls.Add(companyPortalInformationContainer);

            // portal name
            Panel portalNameContainer = new Panel();
            portalNameContainer.ID = "PortalNameContainer";
            companyPortalInformationContainer.Controls.Add(portalNameContainer);

            Literal portalName = new Literal();
            portalName.Text = AsentiaSessionState.GlobalSiteObject.Title;
            portalNameContainer.Controls.Add(portalName);

            // company name
            Panel companyNameContainer = new Panel();
            companyNameContainer.ID = "CompanyNameContainer";
            companyPortalInformationContainer.Controls.Add(companyNameContainer);

            Literal companyName = new Literal();
            companyName.Text = AsentiaSessionState.GlobalSiteObject.Company;
            companyNameContainer.Controls.Add(companyName);

            // "Purchase Information" table

            // set the currency symbol for NumberFormat so we can use it to format currency
            CultureInfo ci = new CultureInfo(AsentiaSessionState.UserCulture);
            ci.NumberFormat.CurrencySymbol = this._EcommerceSettings.CurrencySymbol;

            Panel purchaseInformationTableContainer = new Panel();
            purchaseInformationTableContainer.ID = "PurchaseInformationTableContainer";
            this.PurchaseBaseInformationContainer.Controls.Add(purchaseInformationTableContainer);

            Table purchaseInformationTable = new Table();
            purchaseInformationTableContainer.Controls.Add(purchaseInformationTable);

            // header
            TableHeaderRow purchaseInformationTableHeaderRow = new TableHeaderRow();
            purchaseInformationTable.Rows.Add(purchaseInformationTableHeaderRow);

            TableHeaderCell purchaseInformationTableHeaderSoldTo = new TableHeaderCell();
            purchaseInformationTableHeaderSoldTo.Text = _GlobalResources.SoldTo;
            purchaseInformationTableHeaderRow.Cells.Add(purchaseInformationTableHeaderSoldTo);

            TableHeaderCell purchaseInformationTableHeaderOrderNumber = new TableHeaderCell();
            purchaseInformationTableHeaderOrderNumber.Text = _GlobalResources.OrderNumber;
            purchaseInformationTableHeaderRow.Cells.Add(purchaseInformationTableHeaderOrderNumber);

            TableHeaderCell purchaseInformationTableHeaderOrderDate = new TableHeaderCell();
            purchaseInformationTableHeaderOrderDate.Text = _GlobalResources.OrderDate;
            purchaseInformationTableHeaderRow.Cells.Add(purchaseInformationTableHeaderOrderDate);

            TableHeaderCell purchaseInformationTableHeaderPaymentMethod = new TableHeaderCell();
            purchaseInformationTableHeaderPaymentMethod.Text = _GlobalResources.PaymentMethod;
            purchaseInformationTableHeaderRow.Cells.Add(purchaseInformationTableHeaderPaymentMethod);

            // data
            TableRow purchaseInformationTableRow = new TableRow();
            purchaseInformationTable.Rows.Add(purchaseInformationTableRow);

            TableCell purchaseInformationTableSoldTo = new TableCell();
            purchaseInformationTableSoldTo.Text = this._UserObject.DisplayName;
            purchaseInformationTableRow.Cells.Add(purchaseInformationTableSoldTo);

            TableCell purchaseInformationTableOrderNumber = new TableCell();
            purchaseInformationTableOrderNumber.Text = this._PurchaseObject.OrderNumber;
            purchaseInformationTableRow.Cells.Add(purchaseInformationTableOrderNumber);

            TableCell purchaseInformationTableOrderDate = new TableCell();
            purchaseInformationTableOrderDate.Text = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._PurchaseObject.TimeStamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern);
            purchaseInformationTableRow.Cells.Add(purchaseInformationTableOrderDate);

            TableCell purchaseInformationTablePaymentMethod = new TableCell();
            if (this._EcommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL)
            { purchaseInformationTablePaymentMethod.Text = _GlobalResources.PayPalPurchase + ": XXXX-XXXX-XXXX-" + this._PurchaseObject.CreditCardLastFour; }
            else
            { purchaseInformationTablePaymentMethod.Text = _GlobalResources.CreditCard + ": XXXX-XXXX-XXXX-" + this._PurchaseObject.CreditCardLastFour; }
            purchaseInformationTableRow.Cells.Add(purchaseInformationTablePaymentMethod);
        }
        #endregion
    }
}
