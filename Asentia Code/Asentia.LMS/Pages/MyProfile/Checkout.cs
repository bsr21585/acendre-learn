﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LMS.Library;

namespace Asentia.LMS.Pages.MyProfile
{
    public class Checkout : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel CheckoutPageContentWrapperContainer;
        public Panel CheckoutPageContentContainer;
        public Panel CheckoutFormWrapperContainer;
        public Grid MyCartGrid;
        public Panel PurchaseTotalContainer;
        public Panel CheckoutFormContainer;
        public Panel CheckoutFormActionsPanel;
        public Panel CheckoutFormProcessingContainer;
        public Panel CheckoutReceiptContainer;
        #endregion

        #region Private Properties
        private EcommerceSettings _EcommerceSettings;
        private Purchase _PurchaseObject;        
        private double _PurchaseTotal;

        private TextBox _CardholderFirstName;
        private TextBox _CardholderLastName;
        private TextBox _CardholderCompany;
        private TextBox _CardholderBillingAddress;
        private TextBox _CardholderBillingCity;
        private TextBox _CardholderBillingStateProvince;
        private TextBox _CardholderBillingPostalCode;
        private TextBox _CardholderBillingCountry;
        private TextBox _CardholderEmail;
        private TextBox _CreditCardNumber;
        private DropDownList _CreditCardExpirationMonth;
        private DropDownList _CreditCardExpirationYear;
        private TextBox _CreditCardCVV2;

        private HiddenField _AuthorizeAcceptCardholderNameField;
        private HiddenField _AuthorizeAcceptResponseStatus;
        private HiddenField _AuthorizeAcceptResponseErrorMessages;
        private HiddenField _AuthorizeAcceptResponseDataToken;
        private HiddenField _AuthorizeAcceptResponseDataDescriptor;

        private Label _SubmitPaymentMessageLabel;

        private Button _SubmitOrderButton;
        private Button _AuthorizeNetHiddenPostbackButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(CouponCode), "Asentia.LMS.Pages.MyProfile.Checkout.js");

            // if processing method is Authorize.net, include JS resources for Authorize.net accept
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD) == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE)
            {
                string authorizeAcceptJSURL = String.Empty;

                if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_MODE) == EcommerceSettings.PROCESSING_MODE_LIVE) // live
                { authorizeAcceptJSURL = Config.EcommerceSettings.AuthorizeNetAcceptJSLiveURL; }
                else // developer/sandbox
                { authorizeAcceptJSURL = Config.EcommerceSettings.AuthorizeNetAcceptJSTestURL; }

                csm.RegisterClientScriptInclude(authorizeAcceptJSURL, authorizeAcceptJSURL);
                csm.RegisterClientScriptResource(typeof(Asentia.Common.ClientScript), "Asentia.Common.AuthorizeNetAccept.js");
            }

            // build start up calls and add to the Page_Load            
            StringBuilder multipleStartUpCallsScript = new StringBuilder();

            // if processing method is Authorize.net and is unverified, instansiate JS resources for validating Authorize.net
            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_PROCESSING_METHOD) == EcommerceSettings.PROCESSING_METHOD_AUTHORIZE)
            {
                // dictionary of error messages for Accept.js
                multipleStartUpCallsScript.AppendLine(" var acceptJSErrorMessages = {");
                multipleStartUpCallsScript.AppendLine("     \"I_WC_01\" : \"" + _GlobalResources.Success + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_01\" : \"" + _GlobalResources.AcceptJSLibraryIsNotSourcedFromCDNPleaseContactAnAdministrator + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_02\" : \"" + _GlobalResources.TransactionsMustBeProcessedOverHTTPS + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_03\" : \"" + _GlobalResources.AcceptJSLibraryHasNotBeenLoadedCorrectlyPleaseContactAnAdministrator + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_04\" : \"" + _GlobalResources.CreditCardNumberExpirationDateAndCVV2AreRequired + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_05\" : \"" + _GlobalResources.CreditCardNumberIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_06\" : \"" + _GlobalResources.ExpirationMonthIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_07\" : \"" + _GlobalResources.ExpirationYearIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_08\" : \"" + _GlobalResources.ExpirationDateMustBeInTheFuture + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_10\" : \"" + _GlobalResources.AuthorizeNetAPILoginIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_14\" : \"" + _GlobalResources.AcceptJSEncryptionFailedPleaseContactAnAdministrator + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_15\" : \"" + _GlobalResources.CVV2IsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_16\" : \"" + _GlobalResources.CardholderZipCodeIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_17\" : \"" + _GlobalResources.CardholderNameIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_18\" : \"" + _GlobalResources.AuthorizeNetPublicKeyIsInvalid + "\",");
                multipleStartUpCallsScript.AppendLine("     \"E_WC_A0\" : \"" + _GlobalResources.AuthenticationFailedDueToInvalidAPILoginAndOrPublicKey + "\"");
                multipleStartUpCallsScript.AppendLine(" };");
                multipleStartUpCallsScript.AppendLine("");

                // Accept.js object
                multipleStartUpCallsScript.AppendLine(" var objAuthorizeNetAccept = new AuthorizeNetAccept (");
                multipleStartUpCallsScript.AppendLine(" \"ProcessTransaction\",");
                multipleStartUpCallsScript.AppendLine(" Accept,");
                multipleStartUpCallsScript.AppendLine(" \"HandleAuthorizeAcceptResponse\",");
                multipleStartUpCallsScript.AppendLine(" acceptJSErrorMessages,");
                multipleStartUpCallsScript.AppendLine(" \"" + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN) + "\",");
                multipleStartUpCallsScript.AppendLine(" \"" + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_PUBLICKEY) + "\",");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"CreditCardNumber_Field\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"CreditCardExpirationMonth_Field\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"CreditCardExpirationYear_Field\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"CreditCardCVV2_Field\"),");                
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"CardholderBillingPostalCode_Field\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AuthorizeAcceptCardholderNameField\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AuthorizeAcceptResponseStatus\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AuthorizeAcceptResponseErrorMessages\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AuthorizeAcceptResponseDataDescriptor\"),");
                multipleStartUpCallsScript.AppendLine(" document.getElementById(\"AuthorizeAcceptResponseDataToken\"),");
                multipleStartUpCallsScript.AppendLine(" AuthorizeNetResponseCallback");
                multipleStartUpCallsScript.AppendLine(" );");
                multipleStartUpCallsScript.AppendLine("");
                multipleStartUpCallsScript.AppendLine(" window.HandleAuthorizeAcceptResponse = function(response) {");
                multipleStartUpCallsScript.AppendLine("     objAuthorizeNetAccept.HandleResponse(response);");
                multipleStartUpCallsScript.AppendLine(" }");
                multipleStartUpCallsScript.AppendLine("");
            }            

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Page_Load", multipleStartUpCallsScript.ToString(), true);
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get e-commerce settings, then check to ensure ecommerce is set and verified on the portal, and that the processor is not paypal
            this._EcommerceSettings = new EcommerceSettings();

            if (!this._EcommerceSettings.IsEcommerceSetAndVerified || (_EcommerceSettings.ProcessingMethod == EcommerceSettings.PROCESSING_METHOD_PAYPAL))
            { Response.Redirect("/"); }

            // get the purchase object - if no purchase, this will redirect back to my cart
            this._GetPurchaseObject();

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/myprofile/Checkout.css");

            // build the breadcrumb and page title
            this._BuildBreadcrumbAndPageTitle();

            // initialize the administrator menu
            this.InitializeAdminMenu();

            this.CheckoutPageContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.CheckoutPageContentContainer.CssClass = "FormContentContainer";

            // build the object options panel
            this._BuildObjectOptionsPanel();

            // build the page controls
            this._BuildControls();

            // if not postback
            if (!Page.IsPostBack)
            {
                // bind data grid
                this.MyCartGrid.BindData();
            }

            // if there are no items for purchase, this will redirect back to my cart
            if (this.MyCartGrid.RowCount == 0)
            { Response.Redirect("MyCart.aspx"); }            
        }
        #endregion

        #region _GetPurchaseObject
        /// <summary>
        /// Gets the purchase object for the user's active cart.
        /// </summary>
        private void _GetPurchaseObject()
        {
            this._PurchaseObject = new Purchase();
            this._PurchaseObject.GetActiveCartForUser(AsentiaSessionState.IdSiteUser, false);

            // redirect if no purchase
            if (this._PurchaseObject.Id == 0)
            { Response.Redirect("MyCart.aspx"); }  

            // if somehow this is a purchase that has already been confirmed, redirect to the receipt page
            if (this._PurchaseObject.TimeStamp != null)
            { Response.Redirect("Receipt.aspx?id=" + this._PurchaseObject.Id.ToString()); }
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Checkout));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.General, _GlobalResources.Checkout, ImageFiles.GetIconPath(ImageFiles.ICON_CART, ImageFiles.EXT_PNG));
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.Controls.Clear();
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            // RETURN TO MY CART
            optionsPanelLinksContainer.Controls.Add(
                this.BuildOptionsPanelImageLink("ReturnToCartLink",
                                                null,
                                                "MyCart.aspx",
                                                null,
                                                _GlobalResources.ReturnToShoppingCart,
                                                null,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_BACK, ImageFiles.EXT_PNG),
                                                ImageFiles.GetIconPath(ImageFiles.ICON_BACK, ImageFiles.EXT_PNG))
                                                );            

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildControls
        /// <summary>
        /// Builds the controls for the page.
        /// </summary>
        private void _BuildControls()
        {
            // initialize _PurchaseTotal to 0
            this._PurchaseTotal = 0;

            // build the cart items grid
            this._BuildGrid();

            // build the checkout form
            this._BuildCheckoutForm();

            // build the actions panel for the checkout form
            this._BuildCheckoutFormActionsPanel();

            // build the processing container
            this._BuildFormProcessingContainer();

            // leave the receipt container empty and hide it
            this.CheckoutReceiptContainer.Style.Add("display", "none");
        }
        #endregion

        #region _BuildGrid
        /// <summary>
        /// Builds the Grid for the page.
        /// </summary>
        private void _BuildGrid()
        {
            this.MyCartGrid.StoredProcedure = TransactionItem.GetGridForPurchase;
            this.MyCartGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.MyCartGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.MyCartGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.MyCartGrid.AddFilter("@idPurchase", SqlDbType.Int, 4, this._PurchaseObject.Id);
            this.MyCartGrid.IdentifierField = "idTransactionItem";
            this.MyCartGrid.DefaultSortColumn = "idTransactionItem";
            this.MyCartGrid.AddCheckboxColumn = false;
            this.MyCartGrid.AllowPaging = false;
            this.MyCartGrid.ShowSearchBox = false;
            this.MyCartGrid.ShowRecordsPerPageSelectbox = false;
            this.MyCartGrid.EmptyDataText = _GlobalResources.YouDoNotCurrentlyHaveAnyItemsInYourCart;

            // data key names
            this.MyCartGrid.DataKeyNames = new string[] { "idTransactionItem", "couponCode" };

            // columns
            GridColumn cartItemType = new GridColumn(_GlobalResources.Item, "itemType");
            cartItemType.AddProperty(new GridColumnProperty("1", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.Course + "\" />"));
            cartItemType.AddProperty(new GridColumnProperty("2", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.Catalog + "\" />"));
            cartItemType.AddProperty(new GridColumnProperty("3", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.LearningPath + "\" />"));
            cartItemType.AddProperty(new GridColumnProperty("4", "<img class=\"SmallIcon\" src=\""
                                                                 + ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG)
                                                                 + "\" alt=\"" + _GlobalResources.InstructorLedTraining + "\" />"));

            GridColumn item = new GridColumn(String.Empty, "itemName");
            GridColumn cost = new GridColumn(_GlobalResources.Price, "cost");
            GridColumn paid = new GridColumn(_GlobalResources.Amount, "paid");

            // add columns to data grid 
            this.MyCartGrid.AddColumn(cartItemType);
            this.MyCartGrid.AddColumn(item);
            this.MyCartGrid.AddColumn(cost);            
            this.MyCartGrid.AddColumn(paid);

            // apply data bound events
            this.MyCartGrid.RowDataBound += this._MyCartGrid_RowDataBound;
            this.MyCartGrid.DataBound += this._MyCartGrid_DataBound;
        }
        #endregion

        #region _MyCartGrid_RowDataBound
        /// <summary>
        /// Handles the row data bound event for the my cart grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MyCartGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
            // set the currency symbol for NumberFormat so we can use it to format currency
            CultureInfo ci = new CultureInfo(AsentiaSessionState.UserCulture);
            ci.NumberFormat.CurrencySymbol = this._EcommerceSettings.CurrencySymbol;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;

                // ADD ITEM TITLE, DESCRIPTION, AND "INCLUDES COURSES" (IF CATALOG OR LEARNING PATH) TO ITEM COLUMN

                int itemType = Convert.ToInt32(rowView["itemType"]);
                string itemTitle = rowView["itemName"].ToString();
                string itemDescription = rowView["description"].ToString();
                string includedCourses = rowView["includedCourses"].ToString();

                Panel itemInformationContainer = new Panel();
                itemInformationContainer.CssClass = "ItemInformationContainer";

                // title
                Panel itemTitleContainer = new Panel();
                itemTitleContainer.CssClass = "ItemTitleContainer";
                itemInformationContainer.Controls.Add(itemTitleContainer);

                Literal itemTitleLit = new Literal();
                itemTitleLit.Text = itemTitle;
                itemTitleContainer.Controls.Add(itemTitleLit);

                // description
                Panel itemDescriptionContainer = new Panel();
                itemDescriptionContainer.CssClass = "ItemDescriptionContainer";
                itemInformationContainer.Controls.Add(itemDescriptionContainer);

                Literal itemDescriptionLit = new Literal();
                itemDescriptionLit.Text = itemDescription;
                itemDescriptionContainer.Controls.Add(itemDescriptionLit);

                // "includes courses"
                if (itemType == (int)PurchaseItemType.Catalog || itemType == (int)PurchaseItemType.LearningPath)
                {
                    Panel itemIncludesContainer = new Panel();
                    itemIncludesContainer.CssClass = "ItemIncludesContainer";
                    itemInformationContainer.Controls.Add(itemIncludesContainer);

                    Literal itemIncludesLit = new Literal();
                    itemIncludesLit.Text = _GlobalResources.IncludesTheFollowingCourse_s + ": " + includedCourses;
                    itemIncludesContainer.Controls.Add(itemIncludesLit);
                }

                // attach the item information container to the item cell
                e.Row.Cells[1].Controls.Add(itemInformationContainer);


                // FORMAT "PRICE" (COST) AND "AMOUNT" (PAID) COLUMNS

                double cost = 0;
                double paid = 0;

                // parse out cost and paid
                double.TryParse(rowView["cost"].ToString(), out cost);
                double.TryParse(rowView["paid"].ToString(), out paid);

                // add the item "paid" price to the total price
                this._PurchaseTotal += paid;

                // add cost and paid to their correct columns in the table
                e.Row.Cells[2].Text = cost.ToString("C", ci);
                e.Row.Cells[3].Text = paid.ToString("C", ci);
            }
        }
        #endregion

        #region _MyCartGrid_DataBound
        /// <summary>
        /// Handles the complete data bound event for the my cart grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MyCartGrid_DataBound(object sender, EventArgs e)
        {
            // set the currency symbol for NumberFormat so we can use it to format currency
            CultureInfo ci = new CultureInfo(AsentiaSessionState.UserCulture);
            ci.NumberFormat.CurrencySymbol = this._EcommerceSettings.CurrencySymbol;

            // put the total amount of the purchase into a label control
            Label purchaseTotalLabel = new Label();
            purchaseTotalLabel.ID = "PurchaseTotalLabel";
            purchaseTotalLabel.CssClass = "PurchaseTotalLabel";
            purchaseTotalLabel.Text = _GlobalResources.Total + " (" + this._EcommerceSettings.CurrencyCode + "): " + this._PurchaseTotal.ToString("C", ci);

            // put the label inside the container panel            
            this.PurchaseTotalContainer.Controls.Add(purchaseTotalLabel);

            // format submit payment message for end of form
            string formattedPrice = this._PurchaseTotal.ToString("C", ci) + " (" + this._EcommerceSettings.CurrencyCode + ")";
            this._SubmitPaymentMessageLabel.Text = String.Format(_GlobalResources.ByClickingSubmitOrderYourCreditCardWillBeChargedXForTheItemsListedAbove, formattedPrice);

            if (!String.IsNullOrWhiteSpace(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_CHARGEDESCRIPTION)))
            { this._SubmitPaymentMessageLabel.Text += "<br /><br />" + _GlobalResources.ThisChargeWillAppearOnYourCreditCardAs + ": " + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_CHARGEDESCRIPTION); }
        }
        #endregion

        #region _BuildCheckoutForm
        /// <summary>
        /// Builds the checkout form.
        /// </summary>
        private void _BuildCheckoutForm()
        {
            // NOTE: MaxLength properties on fields are set according to Authorize.net constraints.

            // WE ACCEPT FIELD

            Panel cardsAcceptedContainer = new Panel();
            cardsAcceptedContainer.ID = "CardsAcceptedContainer";            

            Label weAcceptLabel = new Label();
            weAcceptLabel.Text = _GlobalResources.WeAccept + ": ";
            cardsAcceptedContainer.Controls.Add(weAcceptLabel);

            // visa
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_VISA_ENABLED))
            {
                Label visaLogoLabel = new Label();
                cardsAcceptedContainer.Controls.Add(visaLogoLabel);

                Image visaLogo = new Image();
                visaLogo.ImageUrl = EcommerceSettings.CREDITCARDICON_VISA;
                visaLogoLabel.Controls.Add(visaLogo);
            }

            // mastercard
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_MASTERCARD_ENABLED))
            {
                Label mastercardLogoLabel = new Label();
                cardsAcceptedContainer.Controls.Add(mastercardLogoLabel);

                Image mastercardLogo = new Image();
                mastercardLogo.ImageUrl = EcommerceSettings.CREDITCARDICON_MASTERCARD;
                mastercardLogoLabel.Controls.Add(mastercardLogo);
            }

            // amex
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_AMEX_ENABLED))
            {
                Label amexLogoLabel = new Label();
                cardsAcceptedContainer.Controls.Add(amexLogoLabel);

                Image amexLogo = new Image();
                amexLogo.ImageUrl = EcommerceSettings.CREDITCARDICON_AMEX;
                amexLogoLabel.Controls.Add(amexLogo);
            }

            // discover card
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_DISCOVERCARD_ENABLED))
            {
                Label discoverCardLogoLabel = new Label();
                cardsAcceptedContainer.Controls.Add(discoverCardLogoLabel);

                Image discoverCardLogo = new Image();
                discoverCardLogo.ImageUrl = EcommerceSettings.CREDITCARDICON_DISCOVERCARD;
                discoverCardLogoLabel.Controls.Add(discoverCardLogo);
            }

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("WeAccept",
                                                                        _GlobalResources.CreditCardInformation,
                                                                        null,
                                                                        cardsAcceptedContainer,
                                                                        false,
                                                                        false,
                                                                        false));

            // CARDHOLDER FIRST NAME FIELD
            this._CardholderFirstName = new TextBox();
            this._CardholderFirstName.ID = "CardholderFirstName_Field";
            this._CardholderFirstName.CssClass = "InputMedium";
            this._CardholderFirstName.MaxLength = 50;

            bool cardholderFirstNameRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_NAME_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CardholderFirstName",
                                                                        _GlobalResources.FirstName,
                                                                        this._CardholderFirstName.ID,
                                                                        this._CardholderFirstName,
                                                                        cardholderFirstNameRequired,
                                                                        true,
                                                                        false));

            // CARDHOLDER LAST NAME FIELD
            this._CardholderLastName = new TextBox();
            this._CardholderLastName.ID = "CardholderLastName_Field";
            this._CardholderLastName.CssClass = "InputMedium";
            this._CardholderLastName.MaxLength = 50;

            bool cardholderLastNameRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_NAME_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CardholderLastName",
                                                                        _GlobalResources.LastName,
                                                                        this._CardholderLastName.ID,
                                                                        this._CardholderLastName,
                                                                        cardholderLastNameRequired,
                                                                        true,
                                                                        false));

            // CARD NUMBER FIELD
            this._CreditCardNumber = new TextBox();
            this._CreditCardNumber.ID = "CreditCardNumber_Field";
            this._CreditCardNumber.CssClass = "InputMedium";
            this._CreditCardNumber.MaxLength = 16;

            bool creditCardNumberRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_CREDITCARDNUM_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CreditCardNumber",
                                                                        _GlobalResources.CardNumber,
                                                                        this._CreditCardNumber.ID,
                                                                        this._CreditCardNumber,
                                                                        creditCardNumberRequired,
                                                                        true,
                                                                        false));            

            // EXPIRATION DATE FIELD
            List<Control> expirationDateInputControls = new List<Control>();

            bool expirationDateRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_EXPIRATIONDATE_REQUIRED) ? true : false;

            // month
            this._CreditCardExpirationMonth = new DropDownList();
            this._CreditCardExpirationMonth.ID = "CreditCardExpirationMonth_Field";

            this._CreditCardExpirationMonth.Items.Add(new ListItem("01"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("02"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("03"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("04"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("05"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("06"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("07"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("08"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("09"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("10"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("11"));
            this._CreditCardExpirationMonth.Items.Add(new ListItem("12"));

            expirationDateInputControls.Add(this._CreditCardExpirationMonth);

            // year
            this._CreditCardExpirationYear = new DropDownList();
            this._CreditCardExpirationYear.ID = "CreditCardExpirationYear_Field";

            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(1).Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(2).Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(3).Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(4).Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(5).Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(6).Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(7).Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(8).Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(9).Year.ToString()));
            this._CreditCardExpirationYear.Items.Add(new ListItem(AsentiaSessionState.UtcNow.AddYears(10).Year.ToString()));

            expirationDateInputControls.Add(this._CreditCardExpirationYear);

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CreditCardExpiration", 
                                                                                            _GlobalResources.ExpirationDate, 
                                                                                            expirationDateInputControls, 
                                                                                            expirationDateRequired,
                                                                                            true));

            // CVV2 FIELD
            this._CreditCardCVV2 = new TextBox();
            this._CreditCardCVV2.ID = "CreditCardCVV2_Field";
            this._CreditCardCVV2.CssClass = "InputXShort";
            this._CreditCardCVV2.MaxLength = 4;

            bool cvv2Required = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_CVV2_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CreditCardCVV2",
                                                                        _GlobalResources.CVV2,
                                                                        this._CreditCardCVV2.ID,
                                                                        this._CreditCardCVV2,
                                                                        cvv2Required,
                                                                        true,
                                                                        false));                       

            // COMPANY FIELD
            this._CardholderCompany = new TextBox();
            this._CardholderCompany.ID = "CardholderCompany_Field";
            this._CardholderCompany.CssClass = "InputMedium";
            this._CardholderCompany.MaxLength = 50;

            bool companyRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_COMPANY_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CardholderCompany",
                                                                        _GlobalResources.Company,
                                                                        this._CardholderCompany.ID,
                                                                        this._CardholderCompany,
                                                                        companyRequired,
                                                                        true,
                                                                        false));

            // BILLING ADDRESS FIELD
            this._CardholderBillingAddress = new TextBox();
            this._CardholderBillingAddress.ID = "CardholderBillingAddress_Field";
            this._CardholderBillingAddress.CssClass = "InputMedium";
            this._CardholderBillingAddress.TextMode = TextBoxMode.MultiLine;
            this._CardholderBillingAddress.MaxLength = 60;
            this._CardholderBillingAddress.Rows = 3;

            bool addressRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_ADDRESS_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CardholderBillingAddress",
                                                                        _GlobalResources.BillingAddress,
                                                                        this._CardholderBillingAddress.ID,
                                                                        this._CardholderBillingAddress,
                                                                        addressRequired,
                                                                        true,
                                                                        false));

            // BILLING CITY FIELD
            this._CardholderBillingCity = new TextBox();
            this._CardholderBillingCity.ID = "CardholderBillingCity_Field";
            this._CardholderBillingCity.CssClass = "InputMedium";
            this._CardholderBillingCity.MaxLength = 40;

            bool cityRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_CITY_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CardholderBillingCity",
                                                                        _GlobalResources.City,
                                                                        this._CardholderBillingCity.ID,
                                                                        this._CardholderBillingCity,
                                                                        cityRequired,
                                                                        true,
                                                                        false));

            // BILLING STATE PROVINCE FIELD
            this._CardholderBillingStateProvince = new TextBox();
            this._CardholderBillingStateProvince.ID = "CardholderBillingStateProvince_Field";
            this._CardholderBillingStateProvince.CssClass = "InputMedium";
            this._CardholderBillingStateProvince.MaxLength = 40;

            bool stateProvinceRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_STATEPROVINCE_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CardholderBillingStateProvince",
                                                                        _GlobalResources.StateProvince,
                                                                        this._CardholderBillingStateProvince.ID,
                                                                        this._CardholderBillingStateProvince,
                                                                        stateProvinceRequired,
                                                                        true,
                                                                        false));

            // BILLING POSTAL CODE FIELD
            this._CardholderBillingPostalCode = new TextBox();
            this._CardholderBillingPostalCode.ID = "CardholderBillingPostalCode_Field";
            this._CardholderBillingPostalCode.CssClass = "InputShort";
            this._CardholderBillingPostalCode.MaxLength = 20;

            bool postalCodeRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_POSTALCODE_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CardholderBillingPostalCode",
                                                                        _GlobalResources.PostalCode,
                                                                        this._CardholderBillingPostalCode.ID,
                                                                        this._CardholderBillingPostalCode,
                                                                        postalCodeRequired,
                                                                        true,
                                                                        false));

            // BILLING COUNTRY FIELD
            this._CardholderBillingCountry = new TextBox();
            this._CardholderBillingCountry.ID = "CardholderBillingCountry_Field";
            this._CardholderBillingCountry.CssClass = "InputMedium";
            this._CardholderBillingCountry.MaxLength = 60;

            bool countryRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_COUNTRY_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CardholderBillingCountry",
                                                                        _GlobalResources.Country,
                                                                        this._CardholderBillingCountry.ID,
                                                                        this._CardholderBillingCountry,
                                                                        countryRequired,
                                                                        true,
                                                                        false));

            // EMAIL ADDRESS FIELD
            this._CardholderEmail = new TextBox();
            this._CardholderEmail.ID = "CardholderEmail_Field";
            this._CardholderEmail.CssClass = "InputMedium";
            this._CardholderEmail.MaxLength = 255;

            bool emailRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_EMAIL_REQUIRED) ? true : false;

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("CardholderEmail",
                                                                        _GlobalResources.Email,
                                                                        this._CardholderEmail.ID,
                                                                        this._CardholderEmail,
                                                                        emailRequired,
                                                                        true,
                                                                        false));

            // TERMS AND CONDITIONS FIELD
            if (File.Exists(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + EcommerceSettings.TERMS_AND_CONDITIONS_FILENAME)))
            {
                Panel termsAndConditionsContainer = new Panel();
                termsAndConditionsContainer.ID = "TermsAndConditions_Field";
                termsAndConditionsContainer.CssClass = "TermsAndConditionsContainer";

                Literal termsAndConditions = new Literal();

                using (StreamReader reader = new StreamReader(Server.MapPath(SitePathConstants.SITE_CONFIG_ROOT + EcommerceSettings.TERMS_AND_CONDITIONS_FILENAME)))
                { termsAndConditions.Text = reader.ReadToEnd(); }

                termsAndConditionsContainer.Controls.Add(termsAndConditions);

                this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("TermsAndConditions",
                                                                        _GlobalResources.TermsAndConditions,
                                                                        null,
                                                                        termsAndConditionsContainer,
                                                                        false,
                                                                        false,
                                                                        false));
            }

            // SUBMIT PAYMENT MESSAGE                        
            this._SubmitPaymentMessageLabel = new Label();
            this._SubmitPaymentMessageLabel.ID = "SubmitPaymentMessageLabel";            

            this.CheckoutFormContainer.Controls.Add(AsentiaPage.BuildFormField("SubmitPaymentMessage",
                                                                        String.Empty,
                                                                        null,
                                                                        this._SubmitPaymentMessageLabel,
                                                                        false,
                                                                        false,
                                                                        false));

            // HIDDEN FIELDS FOR AUTHORIZE ACCEPT.JS            
            this._AuthorizeAcceptCardholderNameField = new HiddenField();
            this._AuthorizeAcceptCardholderNameField.ID = "AuthorizeAcceptCardholderNameField";
            this.CheckoutFormContainer.Controls.Add(this._AuthorizeAcceptCardholderNameField);

            this._AuthorizeAcceptResponseStatus = new HiddenField();
            this._AuthorizeAcceptResponseStatus.ID = "AuthorizeAcceptResponseStatus";
            this.CheckoutFormContainer.Controls.Add(this._AuthorizeAcceptResponseStatus);

            this._AuthorizeAcceptResponseErrorMessages = new HiddenField();
            this._AuthorizeAcceptResponseErrorMessages.ID = "AuthorizeAcceptResponseErrorMessages";
            this.CheckoutFormContainer.Controls.Add(this._AuthorizeAcceptResponseErrorMessages);

            this._AuthorizeAcceptResponseDataToken = new HiddenField();
            this._AuthorizeAcceptResponseDataToken.ID = "AuthorizeAcceptResponseDataToken";
            this.CheckoutFormContainer.Controls.Add(this._AuthorizeAcceptResponseDataToken);

            this._AuthorizeAcceptResponseDataDescriptor = new HiddenField();
            this._AuthorizeAcceptResponseDataDescriptor.ID = "AuthorizeAcceptResponseDataDescriptor";
            this.CheckoutFormContainer.Controls.Add(this._AuthorizeAcceptResponseDataDescriptor);
        }
        #endregion

        #region _BuildCheckoutFormActionsPanel
        /// <summary>
        /// Builds the container and buttons for checkout actions.
        /// </summary>
        private void _BuildCheckoutFormActionsPanel()
        {
            // clear controls from container
            this.CheckoutFormActionsPanel.Controls.Clear();

            // style the actions panel
            this.CheckoutFormActionsPanel.CssClass = "ActionsPanel";

            // save button
            this._SubmitOrderButton = new Button();
            this._SubmitOrderButton.ID = "SubmitOrderButton";
            this._SubmitOrderButton.CssClass = "Button ActionButton";
            this._SubmitOrderButton.Text = _GlobalResources.SubmitOrder;            
            this._SubmitOrderButton.Attributes.Add("onclick", "SubmitOrder(); return false;");
            this.CheckoutFormActionsPanel.Controls.Add(this._SubmitOrderButton);

            // set the default button for the course properties container to the id of the properties save button
            this.CheckoutFormWrapperContainer.DefaultButton = this._SubmitOrderButton.ID;

            // hidden postback button
            this._AuthorizeNetHiddenPostbackButton = new Button();
            this._AuthorizeNetHiddenPostbackButton.ID = "AuthorizeNetHiddenPostbackButton";
            this._AuthorizeNetHiddenPostbackButton.Style.Add("display", "none");
            this._AuthorizeNetHiddenPostbackButton.Command += new CommandEventHandler(this._SubmitOrderButton_Command);
            this.CheckoutFormActionsPanel.Controls.Add(this._AuthorizeNetHiddenPostbackButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.CheckoutFormActionsPanel.Controls.Add(this._CancelButton);            
        }
        #endregion

        #region _BuildFormProcessingContainer
        /// <summary>
        /// Builds the container that holds the processing message after submit is clicked.
        /// </summary>
        private void _BuildFormProcessingContainer()
        {
            // make the container hidden - it gets shown only on submit button click
            this.CheckoutFormProcessingContainer.Style.Add("display", "none");

            // "Processing Transaction" message
            Panel processingTransactionMessageContainer = new Panel();
            processingTransactionMessageContainer.ID = "ProcessingTransactionMessageContainer";
            this.CheckoutFormProcessingContainer.Controls.Add(processingTransactionMessageContainer);

            Literal processingTransactionMessage = new Literal();
            processingTransactionMessage.Text = _GlobalResources.ProcessingTransaction;
            processingTransactionMessageContainer.Controls.Add(processingTransactionMessage);

            // "Please wait..." message
            Panel pleaseWaitMessageContainer = new Panel();
            pleaseWaitMessageContainer.ID = "PleaseWaitMessageContainer";
            this.CheckoutFormProcessingContainer.Controls.Add(pleaseWaitMessageContainer);

            Literal pleaseWaitMessage = new Literal();
            pleaseWaitMessage.Text = _GlobalResources.PleaseWaitDoNotClickTheStopOrBackButtonsOnYourBrowser;
            pleaseWaitMessageContainer.Controls.Add(pleaseWaitMessage);

            // loading image
            Panel loadingImageContainer = new Panel();
            loadingImageContainer.ID = "LoadingImageContainer";
            this.CheckoutFormProcessingContainer.Controls.Add(loadingImageContainer);

            Image loadingImage = new Image();
            loadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING, ImageFiles.EXT_GIF);
            loadingImage.AlternateText = _GlobalResources.Loading;
            loadingImageContainer.Controls.Add(loadingImage);
        }
        #endregion

        #region _BuildCheckoutReceiptContainer
        /// <summary>
        /// Builds the container that displays the successful transaction summary and a link to the receipt.
        /// </summary>
        private void _BuildCheckoutReceiptContainer()
        {
            // make the container visible by removing the original display property and replacing it with display: block
            this.CheckoutReceiptContainer.Style.Remove("display");
            this.CheckoutReceiptContainer.Style.Add("display", "block");

            // "Transaction Completed" message
            Panel transactionCompletedMessageContainer = new Panel();
            transactionCompletedMessageContainer.ID = "TransactionCompletedMessageContainer";
            this.CheckoutReceiptContainer.Controls.Add(transactionCompletedMessageContainer);

            Literal transactionCompletedMessage = new Literal();
            transactionCompletedMessage.Text = _GlobalResources.TransactionCompleted;
            transactionCompletedMessageContainer.Controls.Add(transactionCompletedMessage);

            // "Transaction Information" table

            // set the currency symbol for NumberFormat so we can use it to format currency
            CultureInfo ci = new CultureInfo(AsentiaSessionState.UserCulture);
            ci.NumberFormat.CurrencySymbol = this._EcommerceSettings.CurrencySymbol;

            Panel transactionInformationTableContainer = new Panel();
            transactionInformationTableContainer.ID = "TransactionInformationTableContainer";
            this.CheckoutReceiptContainer.Controls.Add(transactionInformationTableContainer);

            Table transactionInformationTable = new Table();
            transactionInformationTableContainer.Controls.Add(transactionInformationTable);

            // header
            TableHeaderRow transactionInformationTableHeaderRow = new TableHeaderRow();
            transactionInformationTable.Rows.Add(transactionInformationTableHeaderRow);

            TableHeaderCell transactionInformationTableHeaderOrderNumber = new TableHeaderCell();
            transactionInformationTableHeaderOrderNumber.Text = _GlobalResources.OrderNumber;
            transactionInformationTableHeaderRow.Cells.Add(transactionInformationTableHeaderOrderNumber);

            TableHeaderCell transactionInformationTableHeaderOrderDate = new TableHeaderCell();
            transactionInformationTableHeaderOrderDate.Text = _GlobalResources.OrderDate;
            transactionInformationTableHeaderRow.Cells.Add(transactionInformationTableHeaderOrderDate);

            TableHeaderCell transactionInformationTableHeaderPaymentMethod = new TableHeaderCell();
            transactionInformationTableHeaderPaymentMethod.Text = _GlobalResources.PaymentMethod;
            transactionInformationTableHeaderRow.Cells.Add(transactionInformationTableHeaderPaymentMethod);

            TableHeaderCell transactionInformationTableHeaderTotal = new TableHeaderCell();
            transactionInformationTableHeaderTotal.Text = _GlobalResources.Total + " (" + this._EcommerceSettings.CurrencyCode + ")";
            transactionInformationTableHeaderRow.Cells.Add(transactionInformationTableHeaderTotal);

            TableHeaderCell transactionInformationTableHeaderStatus = new TableHeaderCell();
            transactionInformationTableHeaderStatus.Text = _GlobalResources.Status;
            transactionInformationTableHeaderRow.Cells.Add(transactionInformationTableHeaderStatus);

            TableHeaderCell transactionInformationTableHeaderReceipt = new TableHeaderCell();
            transactionInformationTableHeaderReceipt.Text = _GlobalResources.Receipt;
            transactionInformationTableHeaderRow.Cells.Add(transactionInformationTableHeaderReceipt);

            // data
            TableRow transactionInformationTableRow = new TableRow();
            transactionInformationTable.Rows.Add(transactionInformationTableRow);

            TableCell transactionInformationTableOrderNumber = new TableCell();
            transactionInformationTableOrderNumber.Text = this._PurchaseObject.OrderNumber;
            transactionInformationTableRow.Cells.Add(transactionInformationTableOrderNumber);

            TableCell transactionInformationTableOrderDate = new TableCell();
            transactionInformationTableOrderDate.Text = TimeZoneInfo.ConvertTimeFromUtc((DateTime)this._PurchaseObject.TimeStamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)).ToString(Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern);
            transactionInformationTableRow.Cells.Add(transactionInformationTableOrderDate);

            TableCell transactionInformationTablePaymentMethod = new TableCell();
            transactionInformationTablePaymentMethod.Text = _GlobalResources.CreditCard + ": XXXX-XXXX-XXXX-" + this._PurchaseObject.CreditCardLastFour;
            transactionInformationTableRow.Cells.Add(transactionInformationTablePaymentMethod);

            TableCell transactionInformationTableTotal = new TableCell();
            transactionInformationTableTotal.Text = this._PurchaseTotal.ToString("C", ci);
            transactionInformationTableRow.Cells.Add(transactionInformationTableTotal);

            TableCell transactionInformationTableStatus = new TableCell();

            Image completedImage = new Image();
            completedImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);            
            completedImage.AlternateText = _GlobalResources.Completed;
            completedImage.CssClass = "SmallIcon";
            completedImage.Style.Add("margin-right", "6px");
            completedImage.Style.Add("vertical-align", "middle");
            transactionInformationTableStatus.Controls.Add(completedImage);

            Literal completedText = new Literal();
            completedText.Text = _GlobalResources.Completed;
            transactionInformationTableStatus.Controls.Add(completedText);
            
            transactionInformationTableRow.Cells.Add(transactionInformationTableStatus);

            TableCell transactionInformationTableReceipt = new TableCell();

            HyperLink receiptLink = new HyperLink();
            receiptLink.CssClass = "ImageLink";
            receiptLink.NavigateUrl = "Receipt.aspx?id=" + this._PurchaseObject.Id.ToString();
            transactionInformationTableReceipt.Controls.Add(receiptLink);

            Image receiptImage = new Image();
            receiptImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_RECEIPT, ImageFiles.EXT_PNG);
            receiptImage.AlternateText = _GlobalResources.Receipt;
            receiptLink.Controls.Add(receiptImage);

            Literal receiptText = new Literal();
            receiptText.Text = _GlobalResources.View;
            receiptLink.Controls.Add(receiptText);

            transactionInformationTableRow.Cells.Add(transactionInformationTableReceipt);
        }
        #endregion

        #region _ValidateCheckoutForm
        /// <summary>
        /// Validates the checkout form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateCheckoutForm()
        {
            // NOTE, DO NOT VALIDATE PAYMENT INFORMATION (CARD, EXPIRATION, CVV2) AS THAT INFORMATION DOES NOT POST BACK AND IS VALIDATED BY THE PAYMENT PROCESSOR
            // ADDITIONALLY, FOR FIELDS WE DO VALIDATE, JUST VALIDATE EMPTY OR NOT EMPTY, THE PAYMENT PROCESSOR DOES THE REST

            bool isValid = true;
            
            // get the field requirements
            bool cardholderFirstNameRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_NAME_REQUIRED) ? true : false;
            bool cardholderLastNameRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_NAME_REQUIRED) ? true : false;
            bool companyRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_COMPANY_REQUIRED) ? true : false;
            bool addressRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_ADDRESS_REQUIRED) ? true : false;
            bool cityRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_CITY_REQUIRED) ? true : false;
            bool stateProvinceRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_STATEPROVINCE_REQUIRED) ? true : false;
            bool postalCodeRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_POSTALCODE_REQUIRED) ? true : false;
            bool countryRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_COUNTRY_REQUIRED) ? true : false;
            bool emailRequired = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ECOMMERCE_FIELD_EMAIL_REQUIRED) ? true : false;

            // FIRST NAME 
            if (cardholderFirstNameRequired && String.IsNullOrWhiteSpace(this._CardholderFirstName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CheckoutFormContainer, "CardholderFirstName", _GlobalResources.FirstName + " " + _GlobalResources.IsRequired);
            }

            // LAST NAME 
            if (cardholderLastNameRequired && String.IsNullOrWhiteSpace(this._CardholderLastName.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CheckoutFormContainer, "CardholderLastName", _GlobalResources.LastName + " " + _GlobalResources.IsRequired);
            }

            // COMPANY
            if (companyRequired && String.IsNullOrWhiteSpace(this._CardholderCompany.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CheckoutFormContainer, "CardholderCompany", _GlobalResources.Company + " " + _GlobalResources.IsRequired);
            }

            // BILLING ADDRESS
            if (addressRequired && String.IsNullOrWhiteSpace(this._CardholderBillingAddress.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CheckoutFormContainer, "CardholderBillingAddress", _GlobalResources.BillingAddress + " " + _GlobalResources.IsRequired);
            }

            // CITY
            if (cityRequired && String.IsNullOrWhiteSpace(this._CardholderBillingCity.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CheckoutFormContainer, "CardholderBillingCity", _GlobalResources.City + " " + _GlobalResources.IsRequired);
            }

            // STATE/PROVINCE
            if (stateProvinceRequired && String.IsNullOrWhiteSpace(this._CardholderBillingStateProvince.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CheckoutFormContainer, "CardholderBillingStateProvince", _GlobalResources.StateProvince + " " + _GlobalResources.IsRequired);
            }

            // POSTAL CODE
            if (postalCodeRequired && String.IsNullOrWhiteSpace(this._CardholderBillingPostalCode.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CheckoutFormContainer, "CardholderBillingPostalCode", _GlobalResources.PostalCode + " " + _GlobalResources.IsRequired);
            }

            // COUNTRY
            if (countryRequired && String.IsNullOrWhiteSpace(this._CardholderBillingCountry.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CheckoutFormContainer, "CardholderBillingCountry", _GlobalResources.Country + " " + _GlobalResources.IsRequired);
            }

            // EMAIL
            if (emailRequired && String.IsNullOrWhiteSpace(this._CardholderEmail.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.CheckoutFormContainer, "CardholderEmail", _GlobalResources.Email + " " + _GlobalResources.IsRequired);
            }

            return isValid;
        }
        #endregion

        #region _SubmitOrderButton_Command
        /// <summary>
        /// Handles the "Submit Order" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SubmitOrderButton_Command(object sender, CommandEventArgs e)
        {
            bool transactionProcessed = false;
            string transactionErrors = String.Empty;

            // ensure the client-side process to Accept.js came back successful
            if (this._AuthorizeAcceptResponseStatus.Value == "SUCCESS")
            {
                // getting here means we have a payment token from Authorize.net Accept, so begin to process an AIM transaction
                // any falure in the process should throw an exception back, and we should capture it to display the error
                try
                {
                    // first, re-bind the my cart grid as it does calculations for total price that we need                    
                    this.MyCartGrid.BindData();

                    // validate that we have a token and descriptor
                    if (String.IsNullOrEmpty(this._AuthorizeAcceptResponseDataToken.Value) || String.IsNullOrEmpty(this._AuthorizeAcceptResponseDataDescriptor.Value))
                    { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingTheTransactionPleaseContactAnAdministrator); }

                    // validate that there is a purchase
                    if (this._PurchaseObject.Id == 0)
                    { throw new AsentiaException(_GlobalResources.TheShoppingCartForThisTransactionCouldNotBeFound); }

                    // validate that there is a total purchase price
                    if (this._PurchaseTotal == 0)
                    { throw new AsentiaException(_GlobalResources.TheTotalPurchaseAmountForThisTransactionIsInvalid); }

                    // validate the form fields
                    if (!this._ValidateCheckoutForm())
                    { throw new AsentiaException(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain); }

                    // PROCESS THE TRANSACTION, IF IT FAILS, AN EXCEPTION WILL BE THROWN AND CAUGHT

                    // build a datatable of transaction items to submit with the transaction
                    DataTable transactionItems = TransactionItem.GetItemsForPurchase(this._PurchaseObject.Id);

                    DataTable purchaseItemsForTransaction = new DataTable();
                    purchaseItemsForTransaction.Columns.Add("itemId", typeof(string));
                    purchaseItemsForTransaction.Columns.Add("itemName", typeof(string));
                    purchaseItemsForTransaction.Columns.Add("itemDescription", typeof(string));
                    purchaseItemsForTransaction.Columns.Add("effectivePrice", typeof(double));

                    foreach (DataRow row in transactionItems.Rows)
                    {
                        DataRow item = purchaseItemsForTransaction.NewRow();
                        item["itemId"] = row["idTransactionItem"].ToString();
                        item["itemName"] = row["itemName"].ToString();
                        item["itemDescription"] = row["description"].ToString();
                        item["effectivePrice"] = Convert.ToDouble(row["paid"]);
                        purchaseItemsForTransaction.Rows.Add(item);
                    }                    

                    // submit the transaction
                    string transactionReturnInformation = AuthorizeDotNetAIM.SubmitTransaction(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN),
                                                                                               AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_TRANSACTIONKEY),
                                                                                               this._PurchaseObject.OrderNumber,
                                                                                               this._AuthorizeAcceptResponseDataDescriptor.Value,
                                                                                               this._AuthorizeAcceptResponseDataToken.Value,
                                                                                               this._PurchaseTotal,
                                                                                               this._CardholderFirstName.Text,
                                                                                               this._CardholderLastName.Text,
                                                                                               this._CardholderCompany.Text,
                                                                                               this._CardholderBillingAddress.Text.Replace(System.Environment.NewLine, " "),
                                                                                               this._CardholderBillingCity.Text,
                                                                                               this._CardholderBillingStateProvince.Text,
                                                                                               this._CardholderBillingPostalCode.Text,
                                                                                               this._CardholderBillingCountry.Text,
                                                                                               this._CardholderEmail.Text,
                                                                                               purchaseItemsForTransaction
                    );
                    
                    // if we get here, the transaction was processed, confirm the purchase
                    
                    // get the transaction id and card last 4 information out of the transaction return string
                    string[] transactionReturnInformationArr = transactionReturnInformation.Split('|');
                    string transactionId = transactionReturnInformationArr[0];
                    string cardLast4 = transactionReturnInformationArr[1];

                    // set the properties for confirming the purchase and confirm it
                    this._PurchaseObject.TimeStamp = AsentiaSessionState.UtcNow;
                    this._PurchaseObject.CreditCardLastFour = cardLast4;
                    this._PurchaseObject.TransactionId = transactionId;

                    this._PurchaseObject.Confirm();

                    // if we get here without an exception, the transaction was processed and the purchase confirmed
                    transactionProcessed = true;
                }                    
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    transactionProcessed = false;
                    transactionErrors = dnfEx.Message.Replace("|", "<br />");
                }
                catch (DatabaseFieldNotUniqueException fnuEx)
                {
                    transactionProcessed = false;
                    transactionErrors = fnuEx.Message.Replace("|", "<br />");
                }
                catch (DatabaseCallerPermissionException cpeEx)
                {
                    transactionProcessed = false;
                    transactionErrors = cpeEx.Message.Replace("|", "<br />");
                }
                catch (DatabaseException dEx)
                {
                    transactionProcessed = false;
                    transactionErrors = dEx.Message.Replace("|", "<br />");
                }
                catch (AsentiaException ex)
                {
                    transactionProcessed = false;
                    transactionErrors = ex.Message.Replace("|", "<br />");
                }
                catch (Exception exc) // other exception that hasn't otherwise been handled
                {
                    transactionProcessed = false;
                    transactionErrors = exc.Message;
                }
            }
            else // client-side Accept.js failed
            {
                transactionErrors = this._AuthorizeAcceptResponseErrorMessages.Value.Replace("|", "<br />");
            }            
                
            // if successful, display feedback, hide the form, and build a receipt summary and link
            // if not successful, display feedback, and wipe the values from the Authorize.net Accept.js hidden inputs
            if (transactionProcessed)
            { 
                // display feedback
                this.DisplayFeedback(_GlobalResources.TheTransactionHasBeenProcessedSuccessfully + "<br /><br />" + _GlobalResources.YourPurchasedItem_sWillBeAvailableOnYourDashboardPageShortly, false);

                // hide the form and build the receipt container
                this.CheckoutFormWrapperContainer.Style.Add("display", "none");
                this._BuildCheckoutReceiptContainer();
            }
            else
            { 
                // display the feedback
                this.DisplayFeedback(_GlobalResources.TheTransactionFailedDueToTheFollowingError_s + "<br /><br />" + transactionErrors, true);  
              
                // clear the Authorize.net Accept.js inputs
                this._AuthorizeAcceptCardholderNameField.Value = "";
                this._AuthorizeAcceptResponseStatus.Value = "";
                this._AuthorizeAcceptResponseErrorMessages.Value = "";
                this._AuthorizeAcceptResponseDataToken.Value = "";
                this._AuthorizeAcceptResponseDataDescriptor.Value = "";                
            } 
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("MyCart.aspx");
        }
        #endregion
    }
}
