﻿function ApplyCouponCodeClick(idTransactionItem) {
    $("#ApplyCouponCodeModalModalPopupFeedbackContainer").hide();
    $("#CouponCodeToApply_Field").val("");
    $("#HiddenTransactionItemId").val(idTransactionItem);

    document.getElementById("ApplyCouponCodeModalButton").click();
}

function RemoveCouponCodeClick(idTransactionItem) {
    $("#RemoveCouponCodeModalModalPopupFeedbackContainer").hide();
    $("#HiddenTransactionItemId").val(idTransactionItem);

    document.getElementById("RemoveCouponCodeModalButton").click();
}