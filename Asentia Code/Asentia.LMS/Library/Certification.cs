﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class Certification
    {
        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Certification()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idCertification">Certification Id</param>
        public Certification(int idCertification)
        {
            this.LanguageSpecificProperties = new ArrayList();

            this._Details(idCertification);
            this._GetPropertiesInLanguages(idCertification);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Certification.GetGrid]";

        /// <summary>
        /// Stored procedure used to populate a grid for this object for a user. 
        /// </summary>
        public static readonly string GridProcedureForUser = "[Certification.GetGridForUser]";

        /// <summary>
        /// Stored procedure used to populate a grid for this object for the current session user's dashboard.
        /// </summary>
        public static readonly string GridProcedureForUserDashboard = "[Certification.GetGridForUserDashboard]";

        /// <summary>
        /// Certification Id
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id
        /// </summary>
        public int IdSite;        

        /// <summary>
        /// Title
        /// </summary>
        public string Title;

        /// <summary>
        /// Short Description
        /// </summary>
        public string ShortDescription;

        /// <summary>
        /// Search Tags
        /// </summary>
        public string SearchTags;

        /// <summary>
        /// Date Created
        /// </summary>
        public DateTime DtCreated;
        
        /// <summary>
        /// Date Modified
        /// </summary>
        public DateTime? DtModified;
        
        /// <summary>
        /// Is Deleted
        /// </summary>
        public bool IsDeleted;
        
        /// <summary>
        /// Date Deleted
        /// </summary>
        public DateTime? DtDeleted;
        
        /// <summary>
        /// Initial Award Expiration Interval
        /// </summary>
        public int? InitialAwardExpiresInterval;
        
        /// <summary>
        /// Initial Award Expiration Timeframe
        /// </summary>
        public string InitialAwardExpiresTimeframe;
        
        /// <summary>
        /// Renewal Expiration Interval
        /// </summary>
        public int? RenewalExpiresInterval;
        
        /// <summary>
        /// Renewal Expiration Timeframe
        /// </summary>
        public string RenewalExpiresTimeframe;
        
        /// <summary>
        /// Accrediting Organization
        /// </summary>
        public string AccreditingOrganization;               
        
        /// <summary>
        /// The name of the person that serves as contact for this certification, if not a user in the system.
        /// </summary>
        public string CertificationContactName;
        
        /// <summary>
        /// The email of the person that serves as contact for this certification, if not a user in the system.
        /// </summary>
        public string CertificationContactEmail;
        
        /// <summary>
        /// The phone number of the person that serves as contact for this certification, if not a user in the system.
        /// </summary>
        public string CertificationContactPhoneNumber;

        /// <summary>
        /// Is Published
        /// </summary>
        public bool? IsPublished;

        /// <summary>
        /// Is Closed
        /// </summary>
        public bool? IsClosed;

        /// <summary>
        /// Is Any Module - Can the user complete any module (true), or all modules (false)?
        /// </summary>
        public bool IsAnyModule;

        /// <summary>
        /// Is Renewal Any Module - Can the user complete any module (true), or all modules (false) for renewal?
        /// </summary>
        public bool IsRenewalAnyModule;

        /// <summary>
        /// Is the calling user (current session user) currently enrolled
        /// in this certification?
        /// </summary>
        public bool IsCallingUserEnrolled;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Title;
            public string ShortDescription;
            public string SearchTags;

            /// <summary>
            /// parameterless constructor for serialization purpose
            /// </summary>
            public LanguageSpecificProperty()
            { ;}

            public LanguageSpecificProperty(string langString, string title, string shortDescription, string searchTags)
            {
                this.LangString = langString;
                this.Title = title;
                this.ShortDescription = shortDescription;
                this.SearchTags = searchTags;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves certification data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves certification data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this certification.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="title">certification title</param>
        /// <param name="shortDescription">certification short description</param>
        /// <param name="searchTags">certification search tags</param>
        public void SaveLang(string languageString, string title, string shortDescription, string searchTags)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureCertificationSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertification", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@title", title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", shortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", searchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Certification.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetEmailNotifications
        /// <summary>
        /// Gets a listing of email notification ids and names that belong to this certification.
        /// </summary>
        /// <returns>DataTable of email notification ids and names that belong to this certification</returns>
        public DataTable GetEmailNotifications(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCertification", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certification.GetEmailNotifications]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetAttachedCourses
        /// <summary>
        /// Returns a recordset of course ids, names, and credits that belong to this certification.
        /// </summary>
        /// <returns>DataTable of course ids, names, and credits that belong to this certification</returns>
        public DataTable GetAttachedCourses()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCertification", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);                

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certification.GetAttachedCourses]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetModuleIds
        /// <summary>
        /// Returns a recordset of module ids that belong to this certification.
        /// </summary>
        /// <returns>DataTable of midule ids that belong to this certification</returns>
        public DataTable GetModuleIds()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCertification", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certification.GetModuleIds]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveAttachedCourses
        /// <summary>
        /// Saves course and credit information for courses attached to this certification.
        /// </summary>
        /// <param name="attachedCourses">DataTable of attached courses</param>
        public void SaveAttachedCourses(DataTable attachedCourses)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCertification", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@CertificationAttachedCourses", attachedCourses, SqlDbType.Structured, null, ParameterDirection.Input);

                databaseObject.ExecuteNonQuery("[Certification.SaveAttachedCourses]", true);                

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DeleteRemovedObjects
        /// <summary>
        /// Deletes ulderlying module, requirement set, and requirement objects that were removed in the editor interface.
        /// This works by giving a list of objects that should be kept, and removing the ones not in the list. 
        /// </summary>
        /// <param name="attachedCourses">DataTable of attached courses</param>
        public void DeleteRemovedObjects(DataTable modules, DataTable moduleRequirementSets, DataTable moduleRequirements)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCertification", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@Modules", modules, SqlDbType.Structured, null, ParameterDirection.Input);
                databaseObject.AddParameter("@ModuleRequirementSets", moduleRequirementSets, SqlDbType.Structured, null, ParameterDirection.Input);
                databaseObject.AddParameter("@ModuleRequirements", moduleRequirements, SqlDbType.Structured, null, ParameterDirection.Input);

                databaseObject.ExecuteNonQuery("[Certification.DeleteRemovedObjects]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Certification(s).
        /// </summary>
        /// <param name="deletees">DataTable of certifications to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Certifications", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Certification.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DeleteForUser
        /// <summary>
        /// Deletes Certification(s) that are attached to a user.
        /// </summary>
        /// <param name="deletees">DataTable of certification to user links to delete</param>
        public static void DeleteForUser(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@CertificationToUserLinks", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Certification.DeleteForUser]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ResetForUser
        /// <summary>
        /// Resets a certification that is attached to a user.
        /// </summary>
        public static void ResetForUser(int idCertificationToUserLink)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificationToUserLink", idCertificationToUserLink, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Certification.ResetForUser]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForUserSelectList
        /// <summary>
        /// Gets a listing of certification ids and names to populate into a select list.
        /// </summary>
        /// <param name="idUser">user id</param>
        /// <returns>DataTable of certification ids and names not currently attached to the specified user.</returns>
        public static DataTable IdsAndNamesForUserSelectList(int idUser, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certification.IdsAndNamesForUserSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region JoinUser
        /// <summary>
        /// Joins a certification to a user.
        /// </summary>
        public static void JoinUser(int idCertification, int idUser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertification", idCertification, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Certification.JoinUser]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SynchronizeTaskRequirementData
        /// <summary>
        /// Synchronizes task requirement data for a user's certification.
        /// </summary>
        public static void SynchronizeTaskRequirementData(int idCertificationToUserLink)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificationToUserLink", idCertificationToUserLink, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Certification.SynchronizeTaskRequirementData]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetUserCertificationDetails
        /// <summary>
        /// Gets base details of a user's certification such as expiration dates and certification track.
        /// </summary>
        public static DataTable GetUserCertificationDetails(int idCertificationToUserLink)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            { 
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCertificationToUserLink", idCertificationToUserLink, SqlDbType.Int, 4, ParameterDirection.Input);                

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certification.GetUserCertificationDetails]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetRequirementStatusesForUser
        /// <summary>
        /// Gets a recordset of requirement statuses for a user certification.
        /// </summary>
        /// <returns>DataTable</returns>
        public static DataTable GetRequirementStatusesForUser(int idUser, int idCertificationToUserLink, bool isInitialRequirement)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idCertificationToUserLink", idCertificationToUserLink, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@isInitialRequirement", isInitialRequirement, SqlDbType.Bit, 1, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certification.GetRequirementStatusesForUser]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region UpdateUserCertificationDates
        /// <summary>
        /// Updates the user certification initial award date.
        /// </summary>
        public static void UpdateUserCertificationDates(int idCertificationToUserLink, DateTime? dateToUpdate, int dateFlag)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificationToUserLink", idCertificationToUserLink, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@dateToUpdate", dateToUpdate, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@dateFlag", dateFlag, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Certification.UpdateUserCertificationDates]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves certification data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved certification</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertification", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);            
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", this.ShortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", this.SearchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            if (this.InitialAwardExpiresInterval == null)
            { databaseObject.AddParameter("@initialAwardExpiresInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@initialAwardExpiresInterval", this.InitialAwardExpiresInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@initialAwardExpiresTimeframe", this.InitialAwardExpiresTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.RenewalExpiresInterval == null)
            { databaseObject.AddParameter("@renewalExpiresInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@renewalExpiresInterval", this.RenewalExpiresInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@renewalExpiresTimeframe", this.RenewalExpiresTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@accreditingOrganization", this.AccreditingOrganization, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@certificationContactName", this.CertificationContactName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@certificationContactEmail", this.CertificationContactEmail, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@certificationContactPhoneNumber", this.CertificationContactPhoneNumber, SqlDbType.NVarChar, 20, ParameterDirection.Input);

            if (this.IsPublished == null)
            { databaseObject.AddParameter("@isPublished", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isPublished", this.IsPublished, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsClosed == null)
            { databaseObject.AddParameter("@isClosed", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isClosed", this.IsClosed, SqlDbType.Bit, 1, ParameterDirection.Input); }

            databaseObject.AddParameter("@isAnyModule", this.IsAnyModule, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@isRenewalAnyModule", this.IsRenewalAnyModule, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Certification.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved certification
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idCertification"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified certification's properties.
        /// </summary>
        /// <param name="idCertification">Certification Id</param>
        private void _Details(int idCertification)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertification", idCertification, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);            
            databaseObject.AddParameter("@title", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@shortDescription", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@searchTags", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtModified", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@initialAwardExpiresInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@initialAwardExpiresTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@renewalExpiresInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@renewalExpiresTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@accreditingOrganization", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);            
            databaseObject.AddParameter("@certificationContactName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@certificationContactEmail", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@certificationContactPhoneNumber", null, SqlDbType.NVarChar, 20, ParameterDirection.Output);
            databaseObject.AddParameter("@isPublished", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isClosed", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isAnyModule", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isRenewalAnyModule", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isCallingUserEnrolled", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Certification.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idCertification;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);                
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.ShortDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@shortDescription"].Value);
                this.SearchTags = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@searchTags"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.InitialAwardExpiresInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@initialAwardExpiresInterval"].Value);
                this.InitialAwardExpiresTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@initialAwardExpiresTimeframe"].Value);
                this.RenewalExpiresInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@renewalExpiresInterval"].Value);
                this.RenewalExpiresTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@renewalExpiresTimeframe"].Value);
                this.AccreditingOrganization = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@accreditingOrganization"].Value); ;                
                this.CertificationContactName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@certificationContactName"].Value);
                this.CertificationContactEmail = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@certificationContactEmail"].Value);
                this.CertificationContactPhoneNumber = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@certificationContactPhoneNumber"].Value);
                this.IsPublished = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isPublished"].Value);
                this.IsClosed = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isClosed"].Value);
                this.IsAnyModule = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isAnyModule"].Value);
                this.IsRenewalAnyModule = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isRenewalAnyModule"].Value);
                this.IsCallingUserEnrolled = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isCallingUserEnrolled"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a certification into a collection.
        /// </summary>
        /// <param name="idCertification">course id</param>
        private void _GetPropertiesInLanguages(int idCertification)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertification", idCertification, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certification.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["title"].ToString(),
                            sdr["shortDescription"].ToString(),
                            sdr["searchTags"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
