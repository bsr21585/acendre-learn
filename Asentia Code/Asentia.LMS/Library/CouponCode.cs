﻿using Asentia.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
namespace Asentia.LMS.Library
{
    public class CouponCode
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public CouponCode()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idPurchase">CouponCode Id</param>
        public CouponCode(int idCouponCode)
        {
            this._Details(idCouponCode);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[CouponCode.GetGrid]";

        /// <summary>
        /// CouponCode Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// User Allowed.
        /// </summary>  
        public int UsesAllowed;

        /// <summary>
        /// Discount Type.
        /// </summary>
        public int DiscountType;

        /// <summary>
        /// Value.
        /// </summary>  
        public double Discount;

        /// <summary>
        /// Code.
        /// </summary>  
        public string Code;

        /// <summary>
        /// Is Deleted.
        /// </summary>  
        public bool? IsDeleted;

        /// <summary>
        /// Comments.
        /// </summary>  
        public string Comments;

        /// <summary>
        /// Catalog.
        /// </summary>  
        public int ForCatalog;

        /// <summary>
        /// Course.
        /// </summary>  
        public int ForCourse;

        /// <summary>
        /// LearningPath.
        /// </summary>  
        public int ForLearningPath;

        /// <summary>
        /// Instructor Led Training
        /// </summary>  
        public int ForStandupTraining;        

        /// <summary>
        /// Start Date.
        /// </summary>  
        public DateTime? DtStart;

        /// <summary>
        /// Single User Per User.
        /// </summary>  
        public bool? IsSingleUsePerUser;

        /// <summary>
        /// End Date.
        /// </summary>  
        public DateTime? DtEnd;
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Coupon Code(s).
        /// </summary>
        /// <param name="deletees">DataTable of coupon codes to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@CouponCodes", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CouponCode.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves coupon code data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves coupon code data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region GetCatalogs
        /// <summary>
        /// Gets a listing of catalog ids, titles, and order that belong to this coupon code.
        /// </summary>
        /// <returns>DataTable of catalog ids, titles, and order that belong to this coupon code</returns>
        public DataTable GetCatalogs(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCouponCode", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[CouponCode.GetCatalogs]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCourses
        /// <summary>
        /// Gets a listing of course ids, titles, and order that belong to this coupon code.
        /// </summary>
        /// <returns>DataTable of course ids, titles, and order that belong to this coupon code</returns>
        public DataTable GetCourses(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCouponCode", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[CouponCode.GetCourses]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetLearningPaths
        /// <summary>
        /// Gets a listing of learning path ids, titles, and order that belong to this coupon code.
        /// </summary>
        /// <returns>DataTable of learning path ids, titles, and order that belong to this coupon code</returns>
        public DataTable GetLearningPaths(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCouponCode", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[CouponCode.GetLearningPaths]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion        

        #region GetInstructorLedTraining
        /// <summary>
        /// Gets a listing of instructor led training ids, titles, and order that belong to this coupon code.
        /// </summary>
        /// <returns>DataTable of instructor led training ids, titles, and order that belong to this coupon code</returns>
        public DataTable GetInstructorLedTraining(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCouponCode", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[CouponCode.GetInstructorLedTraining]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion        

        #region SaveCatalogs
        /// <summary>
        /// Save catalogs for coupon code.
        /// </summary>
        /// <param name="catalogs"></param>
        public void SaveCatalogs(DataTable catalogs)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCouponCode", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@forCatalog", this.ForCatalog, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCatalogList", catalogs, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CouponCode.SaveCatalogs]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveCourses
        /// <summary>
        /// Save Courses for coupon code 
        /// </summary>
        /// <param name="courses"></param>
        public void SaveCourses(DataTable courses)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCouponCode", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@forCourse", this.ForCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCourseList", courses, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CouponCode.SaveCourses]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveLearningPaths
        /// <summary>
        /// Save learning paths for coupon code.
        /// </summary>
        /// <param name="learningPaths"></param>
        public void SaveLearningPaths(DataTable learningPaths)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCouponCode", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@forLearningPath", this.ForLearningPath, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idLearningPathList", learningPaths, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CouponCode.SaveLearningPaths]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveInstructorLedTraining
        /// <summary>
        /// Save instructor led training for coupon code
        /// </summary>
        /// <param name="instructorLedTraining"></param>
        public void SaveInstructorLedTraining(DataTable instructorLedTraining)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCouponCode", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@forStandupTraining", this.ForStandupTraining, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idStandupTrainingList", instructorLedTraining, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CouponCode.SaveInstructorLedTraining]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves coupon code data to the database.
        /// </summary>        
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCouponCode", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@usesAllowed", this.UsesAllowed, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@discountType", this.DiscountType, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@discount", this.Discount, SqlDbType.Float, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@code", this.Code.Trim(), SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@isDeleted", this.IsDeleted, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@comments", this.Comments, SqlDbType.NVarChar, Int32.MaxValue, ParameterDirection.Input);
            databaseObject.AddParameter("@forCatalog", this.ForCatalog, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@forCourse", this.ForCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@forLearningPath", this.ForLearningPath, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@forStandupTraining", this.ForStandupTraining, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@isSingleUsePerUser", this.IsSingleUsePerUser, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtEnd", this.DtEnd, SqlDbType.DateTime, 8, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CouponCode.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idCouponCode"].Value);

                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="IdCouponCode">Coupon Code Id</param>
        private void _Details(int idCouponCode)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCouponCode", idCouponCode, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@usesAllowed", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@discountType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@discount", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@code", null, SqlDbType.NVarChar, 10, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@comments", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@forCatalog", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@forCourse", null, SqlDbType.Int, 4, ParameterDirection.Output);            
            databaseObject.AddParameter("@forLearningPath", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@forStandupTraining", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dtStart", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isSingleUsePerUser", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtEnd", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[CouponCode.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idCouponCode;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.UsesAllowed = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@usesAllowed"].Value);
                this.DiscountType = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@discountType"].Value);
                this.Discount = AsentiaDatabase.ParseDbParamDouble(databaseObject.Command.Parameters["@discount"].Value);
                this.Code = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@code"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.Comments = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@comments"].Value);
                this.ForCatalog = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@forCatalog"].Value);
                this.ForCourse = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@forCourse"].Value);                
                this.ForLearningPath = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@forLearningPath"].Value);
                this.ForStandupTraining = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@forStandupTraining"].Value);
                this.DtStart = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtStart"].Value);
                this.IsSingleUsePerUser = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isSingleUsePerUser"].Value);
                this.DtEnd = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtEnd"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }

    #region Enums
    public enum DiscountType
    {
        Free = 1,
        NewPrice = 2,
        AmountOff = 3,
        PercentOff = 4
    }
    public enum CouponCodeValidFor
    {
        No = 1,
        All = 2,
        ListedBelow = 3,
    }
#endregion
}