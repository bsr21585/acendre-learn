﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlTypes;

namespace Asentia.LMS.Library
{
    public class InboxMessage
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public InboxMessage()
        {; }

        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[InboxMessage.GetGrid]"; //populates recipient grid
        public static readonly string DraftsGridProcedure = "[InboxMessage.DraftsGetGrid]"; //populates drafts which were created but not yet sent
        public static readonly string SentItemsProcedure = "[InboxMessage.SentItemsGetGrid]"; //populates items which are already sent

        /// <summary>
        /// InboxMessage Id.
        /// </summary>
        public int IdInboxMessage = 0;

        /// <summary>
        /// ParentInboxMessage Id.
        /// </summary>
        public int? IdParentInboxMessage; //nullable
        /// <summary>
        /// Recipient Id.
        /// </summary>
        public int IdRecipient;

        /// <summary>
        /// Sender Id.
        /// </summary>
        public int IdSender;

        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject;
        /// <summary>
        /// Message.
        /// </summary>
        //public string Message;
        public string Message;
        /// <summary>
        /// Is Draft.
        /// </summary>
        public bool? IsDraft;

        /// <summary>
        /// Is Sent.
        /// </summary>
        public bool? IsSent;

        /// <summary>
        /// Is Read.
        /// </summary>
        public bool? IsRead;

        /// <summary>
        /// Is RecipientDeleted.
        /// </summary>
        public bool? IsRecipientDeleted;

        /// <summary>
        /// Is SenderDeleted.
        /// </summary>
        public bool? IsSenderDeleted;

        /// <summary>
        /// Timestamp
        /// </summary>
        // public DateTime? Timestamp;
        public DateTime? DateCreated;
        /// <summary>
        /// IdSite
        /// </summary>
        public int IdSite;

        // public string recipientName; //added by Sai for testing purposes

        // insert more properties
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves object data to the database.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idCaller">The User Id of the user calling this method.</param>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            //Return Code
            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            //Input
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idInboxMessage", this.IdInboxMessage, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            if (this.IdParentInboxMessage.HasValue)
            {
                databaseObject.AddParameter("@idParentInboxMessage", this.IdParentInboxMessage, SqlDbType.Int, 4, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@idParentInboxMessage", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input);
            }
            databaseObject.AddParameter("@idRecipient", this.IdRecipient, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSender", this.IdSender, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@subject", this.Subject, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@message", this.Message, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);


            // add additional parameters

            try
            {
                databaseObject.ExecuteNonQuery("[InboxMessage.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdInboxMessage = Convert.ToInt32(databaseObject.Command.Parameters["@idInboxMessage"].Value);

                return this.IdInboxMessage;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Send
        /// <summary>
        /// Sends the message to the sender
        /// </summary>

        public void Send(int idInboxMessage)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            //Return Code
            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            //Input
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idInboxMessage", this.IdInboxMessage, SqlDbType.Int, 4, ParameterDirection.InputOutput);

            try
            {
                databaseObject.ExecuteNonQuery("[InboxMessage.Send]", true);
                this.IdInboxMessage = idInboxMessage;

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }


        }

        #endregion

        #region DeleteInbox
        /// <summary>
        /// Delete the message from the Inbox
        /// </summary>

        public static void DeleteInbox(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            databaseObject.AddParameter("@InboxMessages", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[InboxMessage.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DeleteOutgoing
        /// <summary>
        /// Delete the message from sent items grid
        /// </summary>
        public static void DeleteOutgoing(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            databaseObject.AddParameter("@InboxMessages", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[InboxMessage.DeleteOutgoing]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete Draft Message
        /// <summary>
        /// Deletes the draft message(s).
        /// </summary>
        /// <param name="recordsToDelete">The records to delete.</param>
        public static void DeleteDraft(DataTable recordsToDelete)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            databaseObject.AddParameter("@InboxMessages", recordsToDelete, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[InboxMessage.DeleteDraft]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion

        #region SearchRecipient
        /// <summary>
        /// Searches available message recipients based on search string.
        /// Used for AutoComplete search box when composing a message center message. 
        /// </summary>        
        public static DataTable SearchRecipient(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[InboxMessage.SearchRecipient]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;                
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods


        #region _Details
        public enum MessageTypes { Draft = 1, Sent = 2, Inbox = 3 }
        public static InboxMessage _Details(int idInboxMessage, MessageTypes type)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idInboxMessage", idInboxMessage, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@type", type.ToString(), SqlDbType.NVarChar, 5, ParameterDirection.Input);

            try
            {
                DataSet ds = databaseObject.ExecuteDataSet("[InboxMessage.Details]", true);
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    InboxMessage objMessage = new InboxMessage();
                    objMessage.IdInboxMessage = (int)dt.Rows[0][0];
                    objMessage.IdRecipient = (int)dt.Rows[0][1];
                    objMessage.IdSender = (int)dt.Rows[0][2];
                    objMessage.Subject = dt.Rows[0][3].ToString();
                    objMessage.Message = dt.Rows[0][4].ToString();
                    objMessage.DateCreated = (dt.Rows[0][5] != DBNull.Value) ? Convert.ToDateTime(dt.Rows[0][5]) : (DateTime?)null;

                    if (type == MessageTypes.Draft)
                    {
                        objMessage.sender = dt.Rows[0][6].ToString();
                        objMessage.RecipientName = dt.Rows[0][7].ToString();
                        objMessage.IdParentInboxMessage = (dt.Rows[0][8] != DBNull.Value) ? Convert.ToInt32(dt.Rows[0][8]) : (int?)null;
                    }
                    else if (type == MessageTypes.Sent)
                    {
                        objMessage.senderFrom = dt.Rows[0][6].ToString();
                        objMessage.RecipientDelivered = dt.Rows[0][7].ToString();
                        objMessage.IdParentInboxMessage = (dt.Rows[0][8] != DBNull.Value) ? Convert.ToInt32(dt.Rows[0][8]) : (int?)null;
                    }
                    else if (type == MessageTypes.Inbox)
                    {
                        objMessage.displayName = dt.Rows[0][6].ToString();
                        objMessage.RecipientDelivered = dt.Rows[0][7].ToString();
                        objMessage.IdParentInboxMessage = (dt.Rows[0][8] != DBNull.Value) ? Convert.ToInt32(dt.Rows[0][8]) : (int?)null;
                    }
                    return objMessage;
                }
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return null;
        }
        #endregion


        public string sender;
        public string RecipientName;
        public string senderFrom;
        public string RecipientDelivered;
        public string displayName;
        public string Recipient;

        #endregion

        #region ThreadMessages()
        /// <summary>
        /// Gets the message thread(s) when clicked on an inbox row. 
        /// </summary>
        public string senderName { get; set; }
        public string recipientName;

        internal static List<InboxMessage> ThreadMessages(int idInboxMessage)
        {

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idMessage", idInboxMessage, SqlDbType.Int, 4, ParameterDirection.Input);
            List<InboxMessage> list = null;
            try
            {
                DataSet ds = databaseObject.ExecuteDataSet("[InboxMessage.GetThread]", true);
                DataTable dt = ds.Tables[0];

                if (dt.Rows.Count > 0)
                {

                    list = new List<InboxMessage>();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        // needs to modify the return variable to string instead of int
                        list.Add(new InboxMessage
                        {
                            IdInboxMessage = (int)dt.Rows[i][0],
                            IdParentInboxMessage = (dt.Rows[i][1] != DBNull.Value) ? Convert.ToInt32(dt.Rows[i][1]) : (int?)null,
                            senderName = dt.Rows[i][2].ToString(),
                            recipientName = dt.Rows[i][3].ToString(),
                            Message = dt.Rows[i][4].ToString(),
                            DateCreated = (dt.Rows[i][5] != DBNull.Value) ? Convert.ToDateTime(dt.Rows[i][5]) : (DateTime?)null,
                            Subject = dt.Rows[i][6].ToString()
                        });
                    }

                }

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return list;

        }
        #endregion
    }
}

