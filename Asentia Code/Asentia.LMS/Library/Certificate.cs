﻿using System;
using Asentia.Common;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Web;

namespace Asentia.LMS.Library
{
    public enum CertificateObjectType
    {
        Course = 1,
        LearningPath = 2,
        Certification = 3,
    }

    [Serializable]
    public class Certificate
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Certificate()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idCertificate">Certificate Id</param>
        public Certificate(int idCertificate)
        {
            this.LanguageSpecificProperties = new ArrayList();
            this._Details(idCertificate);
            this._GetPropertiesInLanguages(idCertificate);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The default width for certificate image.
        /// </summary>
        public static readonly int CertificateImageDefaultWidth = 672;

        /// <summary>
        /// The default height for certificate image.
        /// </summary>
        public static readonly int CertificateImageDefaultHeight = 500;

        /// <summary>
        /// The stored procedure used to populate a certificate templates grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Certificate.GetGrid]";

        /// <summary>
        /// The stored procedure used to populate a object certificates grid for this object.
        /// </summary>
        public static readonly string ObjectCertificatesGridProcedure = "[Certificate.GetGridForObjectCertificates]";

        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedureForUser = "[Certificate.GetGridForUser]";

        /// <summary>
        /// Certificate Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Name.
        /// </summary>
        public string Name;

        /// <summary>
        /// Issuing Organization.
        /// </summary>
        public string IssuingOrganization;

        /// <summary>
        /// Description.
        /// </summary>
        public string Description;

        /// <summary>
        /// Credits
        /// </summary>
        public double? Credits;

        /// <summary>
        /// Code
        /// </summary>
        public string Code;

        /// <summary>
        /// File Name.
        /// </summary>
        public string FileName;

        /// <summary>
        /// Is Active?
        /// </summary>
        public bool? IsActive;

        /// <summary>
        /// Activation Date.
        /// </summary>
        public DateTime? ActivationDate;

        /// <summary>
        /// Expires Interval.
        /// </summary>
        public int? ExpiresInterval;

        /// <summary>
        /// Expires Timeframe.
        /// </summary>
        public string ExpiresTimeframe;

        /// <summary>
        /// Object Type.
        /// </summary>
        public CertificateObjectType? ObjectType;

        /// <summary>
        /// Id Object
        /// </summary>
        public int? IdObject;

        /// <summary>
        /// Is Deleted
        /// </summary>
        public bool IsDeleted;

        /// <summary>
        /// Date Deleted
        /// </summary>
        public DateTime? DtDeleted;

        /// <summary>
        /// Reissue unexpired certificate awards based on the most recent [object] completion date?
        /// </summary>
        public bool? ReissueBasedOnMostRecentCompletion;

        /// <summary>
        /// ArrayList of language specific properties of type SiteLanguage
        /// </summary>
        public ArrayList LanguageSpecificProperties;
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties of a Site.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Name;
            public string Description;
            public string Filename;

            public LanguageSpecificProperty(string langString, string name, string description, string filename)
            {
                this.LangString = langString;
                this.Name = name;
                this.Description = description;
                this.Filename = filename;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves certificate data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves course data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this certificate.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="name">certificate name</param>
        /// <param name="description">certificate description</param>
        /// <param name="filename">certificate filename</param>
        public void SaveLang(string languageString, string name, string description, string filename = null)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureCertificateSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificate", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@name", name, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@description", description, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@filename", filename, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Certificate.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Certificate(s).
        /// </summary>
        /// <param name="deletees">DataTable of certificates to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Certificates", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Certificate.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion

        #region GetImageHeightWidth
        /// <summary>
        /// get the proportionate decreased width and height for a bigger image as per defualt width and height provided.
        /// </summary>
        /// <param name="imageWidth"></param>
        /// <param name="imageHeight"></param>
        /// <param name="defaultWidth"></param>
        /// <param name="defaultHeight"></param>
        /// <param name="newWidth"></param>
        /// <param name="newHeight"></param>
        public static void GetImageHeightWidth(int imageWidth, int imageHeight, int defaultWidth, int defaultHeight, out int newWidth, out int newHeight)
        {
            newWidth = imageWidth;
            newHeight = imageHeight;

            if (imageWidth != CertificateImageDefaultWidth)
            {
                newWidth = CertificateImageDefaultWidth;
                float percentChanged = ((float)newWidth / imageWidth) * 100;
                newHeight = Convert.ToInt32((imageHeight * percentChanged) / 100);
            }
        }
        #endregion

        #region IdsAndNamesForObject
        /// <summary>
        /// Gets a listing of certificate ids and titles to populate into a select list 
        /// for an object, i.e. Course, Learning Path, etc.
        /// </summary>
        /// <returns>DataTable of certificate ids and titles.</returns>
        public static DataTable IdsAndNamesForObject(string objectType, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@objectType", objectType, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certificate.IdsAndNamesForObject]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesEligibleForUserAward
        /// <summary>
        /// Gets a list of certificate ids and names that have not been awarded to
        /// a specified user or have been previously awarded by are expired.
        /// </summary>
        public static DataTable IdsAndNamesEligibleForUserAward(int idUser)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certificate.IdsAndNamesEligibleForUserAward]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves certificate data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved certificate</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificate", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            if (String.IsNullOrWhiteSpace(this.IssuingOrganization))
            { databaseObject.AddParameter("@issuingOrganization", DBNull.Value, SqlDbType.NVarChar, 255, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@issuingOrganization", this.IssuingOrganization, SqlDbType.NVarChar, 255, ParameterDirection.Input); }
                        
            databaseObject.AddParameter("@description", this.Description, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@credits", this.Credits, SqlDbType.Float, 8, ParameterDirection.Input);

            if (String.IsNullOrWhiteSpace(this.Code))
            { databaseObject.AddParameter("@code", DBNull.Value, SqlDbType.NVarChar, 25, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@code", this.Code, SqlDbType.NVarChar, 25, ParameterDirection.Input); }

            if (String.IsNullOrWhiteSpace(this.FileName))
            { databaseObject.AddParameter("@filename", DBNull.Value, SqlDbType.NVarChar, 255, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@filename", this.FileName, SqlDbType.NVarChar, 255, ParameterDirection.Input); }

            databaseObject.AddParameter("@isActive", this.IsActive, SqlDbType.Bit, 1, ParameterDirection.Input);            
            databaseObject.AddParameter("@expiresInterval", this.ExpiresInterval, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@expiresTimeframe", this.ExpiresTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@objectType", this.ObjectType, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idObject", this.IdObject, SqlDbType.Int, 4, ParameterDirection.Input);

            if (this.ReissueBasedOnMostRecentCompletion == null)
            { databaseObject.AddParameter("@reissueBasedOnMostRecentCompletion", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@reissueBasedOnMostRecentCompletion", this.ReissueBasedOnMostRecentCompletion, SqlDbType.Bit, 1, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[Certificate.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idCertificate"].Value);

                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified certificate's properties.
        /// </summary>
        /// <param name="idCertificate">Certificate Id</param>
        private void _Details(int idCertificate)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificate", idCertificate, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@name", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@issuingOrganization", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@description", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@credits", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@code", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@filename", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isActive", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@activationDate", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@objectType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idObject", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@reissueBasedOnMostRecentCompletion", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            
            try
            {
                databaseObject.ExecuteNonQuery("[Certificate.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idCertificate;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.Name = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@name"].Value);
                this.IssuingOrganization = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@issuingOrganization"].Value);
                this.Description = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@description"].Value);
                this.Credits = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@credits"].Value);
                this.Code = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@code"].Value);
                this.FileName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@filename"].Value);
                this.IsActive = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isActive"].Value);
                this.ActivationDate = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@activationDate"].Value);
                this.ExpiresInterval = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@expiresInterval"].Value);
                this.ExpiresTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresTimeframe"].Value);
                this.ObjectType = (CertificateObjectType)AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@objectType"].Value);
                this.IdObject = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idObject"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.ReissueBasedOnMostRecentCompletion = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@reissueBasedOnMostRecentCompletion"].Value);

                // if filename is null, let's look for a path to a certificate in the certificate's JSON
                // this covers a scenario where the filename is in JSON but not in the database
                if (String.IsNullOrWhiteSpace(this.FileName))
                {                    
                    if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + this.Id.ToString() + "/Certificate.json"))) // ensure the certificate JSON exists
                    {
                        using(StreamReader sr = new StreamReader(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + this.Id.ToString() + "/Certificate.json")))
                        {
                            string fileContents = sr.ReadToEnd(); // read the certificate JSON
                            
                            if (fileContents.Contains("\"ImageSource\":\"" + SitePathConstants.SITE_CERTIFICATES_ROOT + this.Id.ToString() + "/")) // look for an image path
                            {                                
                                // parse the string of JSON to get the path of the image
                                string beginOfImagePath = fileContents.Substring(fileContents.IndexOf("\"ImageSource\":\"" + SitePathConstants.SITE_CERTIFICATES_ROOT + this.Id.ToString() + "/"));
                                beginOfImagePath = beginOfImagePath.Replace("\"ImageSource\":\"" + SitePathConstants.SITE_CERTIFICATES_ROOT + this.Id.ToString() + "/", "");

                                // from here our JSON string should begin with the name of the file, parse characters in string until met with an " which signifies the end of the filename variable
                                string fileName = String.Empty;

                                foreach (char chr in beginOfImagePath.ToCharArray())
                                {                                    
                                    if (!chr.Equals('"'))
                                    { fileName += chr.ToString(); }
                                    else
                                    { break; }
                                }
                                
                                // ensure the file exist, and set it to the FileName property
                                if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + this.Id.ToString() + "/" + fileName)))
                                { this.FileName = fileName; }
                            }
                        }
                    }
                }
            }
            catch
            {                
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a certificate into a collection.
        /// </summary>
        /// <param name="idCertificate">Certificate Id</param>
        private void _GetPropertiesInLanguages(int idCertificate)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificate", idCertificate, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certificate.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["name"].ToString(),
                            sdr["description"].ToString(),
                            sdr["filename"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
