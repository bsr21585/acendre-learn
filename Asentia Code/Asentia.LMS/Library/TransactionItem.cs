﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class TransactionItem
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public TransactionItem()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idTransactionItem">Transaction Item Id</param>
        public TransactionItem(int idTransactionItem)
        {
            this._Details(idTransactionItem);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a grid of all transaction items for a purchase id. Used for My Cart, Checkout, and Purchase Receipts. 
        /// </summary>
        public static readonly string GetGridForPurchase = "[TransactionItem.GetGridForPurchase]";

        /// <summary>
        /// TransactionItem Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Parent Transaction Id.
        /// </summary>        
        public int? IdParentTransactionItem;

        /// <summary>
        /// Purchase Id.
        /// </summary>        
        public int? IdPurchase;

        /// <summary>
        /// Enrollment Id.
        /// </summary>        
        public int? IdEnrollment;

        /// <summary>
        /// User Id.
        /// </summary>        
        public int? IdUser;

        /// <summary>
        /// User Name.
        /// </summary>        
        public string UserName;

        /// <summary>
        /// Assigner Id.
        /// </summary>        
        public int? IdAssigner;

        /// <summary>
        /// Assigner Name.
        /// </summary>        
        public string AssignerName;

        /// <summary>
        /// Item Id.
        /// </summary>        
        public int? ItemId;

        /// <summary>
        /// Item Type. Keyed to ItemType ENUM
        /// </summary>
        public PurchaseItemType? ItemType;

        /// <summary>
        /// Item Name - in language.
        /// </summary>        
        public string ItemName;

        /// <summary>
        /// Description - short description, in language.
        /// </summary>        
        public string Description;

        /// <summary>
        /// Cost.
        /// </summary>        
        public double? Cost;

        /// <summary>
        /// Paid - cost after coupon application.
        /// </summary>        
        public double? Paid;

        /// <summary>
        /// Coupon Code Id.
        /// </summary>        
        public int? IdCouponCode;

        /// <summary>
        /// Coupon Code.
        /// </summary>        
        public string CouponCode;

        /// <summary>
        /// Added Date.
        /// </summary>        
        public DateTime? DtAdded;

        /// <summary>
        /// Confirmed.
        /// </summary>        
        public bool? Confirmed;

        /// <summary>
        /// ILT Session ID linked to the course for course enrollment
        /// </summary>
        public int? IdIltSession;
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves transaction item data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves transaction item data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion
        #endregion
       
        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes transaction item(s).
        /// </summary>
        /// <param name="deletees">DataTable of transaction items to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@TransactionItems", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[TransactionItem.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ApplyCoupon
        /// <summary>
        /// Validates the coupon and applies on the transaction item if validated successfully.
        /// </summary>
        /// <param name="idTransactionItem">transaction item id</param>
        /// <param name="couponCode">coupon code</param>
        /// <returns>the cost of the item after the coupon code has been applied</returns>
        public static double ApplyCoupon(int idTransactionItem, string couponCode)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idTransactionItem", idTransactionItem, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@couponCode", couponCode, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@effectiveCost", null, SqlDbType.Float, 8, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[TransactionItem.ApplyCoupon]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // return the effective cost
                return AsentiaDatabase.ParseDbParamDouble(databaseObject.Command.Parameters["@effectiveCost"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region RemoveCoupon
        /// <summary>
        /// Removes applied coupon from a transaction item.
        /// </summary>
        /// <param name="idTransactionItem">transaction item id</param>
        public static void RemoveCoupon(int idTransactionItem)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idTransactionItem", idTransactionItem, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[TransactionItem.RemoveCoupon]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ConfirmTransactionItems
        /// <summary>
        /// Confirms transaction item(s).
        /// </summary>
        /// <param name="confirmees">DataTable of transaction items to confirm</param>
        /// <param name="disassociateFromPurchase">should the items be disassociated from the purchase? this happens in the event a coupon code makes the item(s) free</param>
        public static void ConfirmTransactionItems(DataTable confirmees, bool disassociateFromPurchase = false)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@TransactionItems", confirmees, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@disassociateFromPurchase", disassociateFromPurchase, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[TransactionItem.ConfirmTransactionItems]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetItemsForPurchase
        /// <summary>
        /// Gets a DataTable of all transaction items associated with a purchase.
        /// </summary>
        /// <param name="idPurchase">purchase id</param>
        /// <returns>DataTable</returns>
        public static DataTable GetItemsForPurchase(int idPurchase)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            
                databaseObject.AddParameter("@idPurchase", idPurchase, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[TransactionItem.GetItemsForPurchase]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ClearAllUnconfirmedForPurchase
        /// <summary>
        /// Clears all unconfirmed transaction items for a purchase.
        /// 
        /// Used when the processor is PayPal and Buy Now is clicked for an item. This is because PayPal 
        /// does not have a cart per se. So we need to ensure only one item is linked to a purchase and nothing
        /// stale is left behind. This is really a safeguard for if someone doesn't follow through with a PayPal 
        /// purchase, and tries to purchase something else prior to us cleaning up the PayPal active carts. It's
        /// necessary since we have no control over what happens when they are taken to the PayPal payment page.
        /// </summary>
        public static void ClearAllUnconfirmedForPurchase(int idPurchase)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idPurchase", idPurchase, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[TransactionItem.ClearAllUnconfirmedForPurchase]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves transaction item data to the database.
        /// </summary>        
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>id of the saved transaction item</returns>
        public int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idTransactionItem", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@userName", this.UserName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@assignerName", this.AssignerName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@itemName", this.ItemName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@description", this.Description, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@couponCode", this.CouponCode, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            if (this.IdParentTransactionItem == null)
            { databaseObject.AddParameter("@idParentTransactionItem", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idParentTransactionItem", this.IdParentTransactionItem, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.IdPurchase == null)
            { databaseObject.AddParameter("@idPurchase", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idPurchase", this.IdPurchase, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.IdEnrollment == null)
            { databaseObject.AddParameter("@idEnrollment", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idEnrollment", this.IdEnrollment, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.IdUser == null)
            { databaseObject.AddParameter("@idUser", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.IdAssigner == null)
            { databaseObject.AddParameter("@idAssigner", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idAssigner", this.IdAssigner, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.ItemId == null)
            { databaseObject.AddParameter("@itemId", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@itemId", this.ItemId, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.ItemType == null)
            { databaseObject.AddParameter("@itemType", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@itemType", Convert.ToInt32(this.ItemType), SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.Cost == null)
            { databaseObject.AddParameter("@cost", DBNull.Value, SqlDbType.Float, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@cost", this.Cost, SqlDbType.Float, 8, ParameterDirection.Input); }

            if (this.Paid == null)
            { databaseObject.AddParameter("@paid", DBNull.Value, SqlDbType.Float, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@paid", this.Paid, SqlDbType.Float, 8, ParameterDirection.Input); }

            if (this.IdCouponCode == null)
            { databaseObject.AddParameter("@idCouponCode", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idCouponCode", this.IdCouponCode, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.DtAdded == null)
            { databaseObject.AddParameter("@dtAdded", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dtAdded", this.DtAdded, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            if (this.Confirmed == null)
            { databaseObject.AddParameter("@confirmed", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@confirmed", this.Confirmed, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if(this.IdIltSession == null)
            { databaseObject.AddParameter("@idIltSession", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idIltSession", this.IdIltSession, SqlDbType.Int, 4, ParameterDirection.Input); }
            
            try
            {
                databaseObject.ExecuteNonQuery("[TransactionItem.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved transaction item
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idTransactionItem"].Value);                

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {                
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="IdTransactionItem">Transaction Item Id</param>
        private void _Details(int idTransactionItem)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idTransactionItem", idTransactionItem, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idParentTransactionItem", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idPurchase", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idEnrollment", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@userName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@idAssigner", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@assignerName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@itemId", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@itemName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@itemType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@description", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@cost", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@paid", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@idCouponCode", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@couponCode", null, SqlDbType.NVarChar, 10, ParameterDirection.Output);
            databaseObject.AddParameter("@dtAdded", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@confirmed", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@idIltSession", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[TransactionItem.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idTransactionItem;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdParentTransactionItem = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idParentTransactionItem"].Value);
                this.IdPurchase = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idPurchase"].Value);
                this.IdEnrollment = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idEnrollment"].Value);
                this.IdUser = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.UserName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@userName"].Value);
                this.IdAssigner = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idAssigner"].Value);
                this.AssignerName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@assignerName"].Value);
                this.ItemId = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@itemId"].Value);
                this.ItemName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@itemName"].Value);
                this.ItemType = (PurchaseItemType?)AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@itemType"].Value);
                this.Description = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@description"].Value);
                this.Cost = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@cost"].Value);
                this.Paid = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@paid"].Value);
                this.IdCouponCode = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idCouponCode"].Value);
                this.CouponCode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@couponCode"].Value);
                this.DtAdded = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtAdded"].Value);
                this.Confirmed = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@confirmed"].Value);
                this.IdIltSession = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idIltSession"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
