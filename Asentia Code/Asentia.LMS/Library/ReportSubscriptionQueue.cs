﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class ReportSubscriptionQueue
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ReportSubscriptionQueue()
        { ;}
        #endregion

        #region Properties
        public static readonly string SentGridProcedure = "[ReportSubscriptionQueue.GetSentGrid]";
        public static readonly string FailedGridProcedure = "[ReportSubscriptionQueue.GetFailedGrid]";
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes report subscription queue item(s).
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@ReportSubscriptionQueueItems", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[ReportSubscriptionQueue.Delete]", true);

                // get the return code
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Retry
        /// <summary>
        /// Retry report subscription queue item(s).
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Retry(DataTable retryids)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@ReportSubscriptionQueueItems", retryids, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[ReportSubscriptionQueue.Retry]", true);

                // get the return code
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCurrentItems
        /// <summary>
        /// Gets the current (not yet sent) items in the report subscription queue.
        /// 
        /// All the params below, which are usually pulled from session state, need to be explicitly defined here
        /// because this method gets called from the Asentia Job Processor.
        /// </summary>
        /// <param name="idCallerSite">caller's site</param>
        /// <param name="callerLangString">caller's language string</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idSite">site id</param>
        /// <param name="numItems">number of items to get</param>
        /// <param name="accountWebConfigPath">path to web.config containing the account database information</param>
        /// <returns>DataTable of current report subscription queue items</returns>
        public static DataTable GetCurrentItems(int idCallerSite, string callerLangString, int idCaller, int? idSite, int numItems, string accountWebConfigPath = null)
        {
            DataTable currentQueueItems = new DataTable();
            AsentiaDatabase databaseObject;

            if (!String.IsNullOrWhiteSpace(accountWebConfigPath))
            { databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath); }
            else
            { databaseObject = new AsentiaDatabase(); }

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

                if (idSite == null)
                { databaseObject.AddParameter("@idSite", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@idSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input); }

                databaseObject.AddParameter("@numItems", numItems, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader currentQueueItemsReader = databaseObject.ExecuteDataReader("[ReportSubscriptionQueue.GetCurrentItems]", true);
                currentQueueItems.Load(currentQueueItemsReader);
                currentQueueItemsReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return currentQueueItems;
        }
        #endregion

        #region UpdateStatus
        /// <summary>
        /// Updates a report subscription queue item's status as either sent or failed.
        /// 
        /// All the params below, which are usually pulled from session state, need to be explicitly defined here
        /// because this method gets called from the Asentia Job Processor.
        /// </summary>
        /// <param name="idCallerSite">caller's site</param>
        /// <param name="callerLangString">caller's language string</param>
        /// <param name="idCaller">caller id</param>
        /// <param name="idReportSubscriptionQueue">queue item's id</param>
        /// <param name="isFailed">is the status failed</param>
        /// <param name="statusDescription">failure description</param>
        /// <param name="accountWebConfigPath">path to web.config containing the account database information</param>
        public static void UpdateStatus(int idCallerSite, string callerLangString, int idCaller, int idReportSubscriptionQueue, bool isFailed, string statusDescription, string accountWebConfigPath = null)
        {
            AsentiaDatabase databaseObject;

            if (!String.IsNullOrWhiteSpace(accountWebConfigPath))
            { databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath); }
            else
            { databaseObject = new AsentiaDatabase(); }

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", idCallerSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", callerLangString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReportSubscriptionQueue", idReportSubscriptionQueue, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isFailed", isFailed, SqlDbType.Bit, 1, ParameterDirection.Input);

            if (String.IsNullOrWhiteSpace(statusDescription))
            { databaseObject.AddParameter("@statusDescription", DBNull.Value, SqlDbType.NVarChar, 512, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@statusDescription", statusDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[ReportSubscriptionQueue.UpdateStatus]", true);

                // get the return code
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
