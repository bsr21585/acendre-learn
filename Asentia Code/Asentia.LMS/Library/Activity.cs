﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Data;
using Asentia.UMS.Library;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class Activity
    {
        #region Enumerations

        #region LessonCompletionTypes
        public enum LessonCompletionTypes
        {
            /// <summary>
            /// Completed
            /// </summary>
            [Description("Completed")]
            Completed = 0,

            /// <summary>
            /// Incomplete.
            /// </summary>
            [Description("Incomplete")]
            Incomplete = 1,

            /// <summary>
            /// Unknown.
            /// </summary>
            [Description("Unknown")]
            Unknown = 2,
        }
        #endregion

        #region LessonSuccessTypes
        public enum LessonSuccessTypes
        {
            /// <summary>
            /// Passed
            /// </summary>
            [Description("Passed")]
            Passed = 0,

            /// <summary>
            /// Failed.
            /// </summary>
            [Description("Failed")]
            Failed = 1,

            /// <summary>
            /// Unknown.
            /// </summary>
            [Description("Unknown")]
            Unknown = 2,
        }
        #endregion
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Activity.GetGrid]";

        /// <summary>
        /// Field Identifier
        /// </summary>
        public string Identifier;

        /// <summary>
        /// Field Name
        /// </summary>
        public string ColumnName;

        /// <summary>
        /// Is the field always required?
        /// </summary>
        public bool AlwaysRequired;

        /// <summary>
        /// The data type of the field.
        /// </summary>
        public Type DataType;

        /// <summary>
        /// The maximum length of the field, per database constraint.
        /// </summary>
        public int MaxLength;

        /// <summary>
        /// Format
        /// </summary>
        public string Format;
        #endregion

        #region Methods

        #region ImportRecord
        /// <summary>
        /// Record Import activity to table tblActivityImport
        /// </summary>
        /// <param name="">Ac</param>
        public static int ImportRecord(string idUserImported, string importFileName)
        {
            int idActivityImport = 0;
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idActivityImport", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUserImported", Convert.ToInt32(idUserImported), SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@importFileName", importFileName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Activity.ImportRecord]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                idActivityImport = Convert.ToInt32(databaseObject.Command.Parameters["@idActivityImport"].Value);
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return idActivityImport;
        }
        #endregion

        #region Import
        /// <summary>
        /// Import a DataTable of Activity Data
        /// </summary>
        /// <param name="ActivityImport">DataTable of activity and their properties</param>
        public static void Import(int idActivityImport, DataTable activityImportTable)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idActivityImport", idActivityImport, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@ActivityImportTable", activityImportTable, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Activity.Import]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes Activities.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Activities", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Activity.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetActivityColumnsInfo
        /// <summary>
        /// Loads all column info from XML file and returns them.
        /// </summary>
        /// <returns>Returns list of activity columns.</returns>
        public static List<Activity> GetActivityColumnsInfo()
        {
            List<Activity> columnList = new List<Activity>();
            string dateTimeFormat = _GlobalResources.ISODatetime + ": " + DateTime.UtcNow.ToString("yyyy-MM-dd") + " 00:00:00";

            // Add Username Column
            Activity columnUserName = new Activity();
            columnUserName.Identifier = "UserName";
            columnUserName.ColumnName = _GlobalResources.Username;
            columnUserName.AlwaysRequired = true;
            columnUserName.DataType = typeof(string);
            columnUserName.MaxLength = 255;
            columnUserName.Format = String.Empty;
            columnList.Add(columnUserName);

            // Add Course Code Column
            Activity columnCourseCode = new Activity();
            columnCourseCode.Identifier = "Course Code";
            columnCourseCode.ColumnName = _GlobalResources.CourseCode;
            columnCourseCode.AlwaysRequired = false;
            columnCourseCode.DataType = typeof(string);
            columnCourseCode.MaxLength = 255;
            columnCourseCode.Format = String.Empty;
            columnList.Add(columnCourseCode);

            // Add Course Name Column
            Activity columnCourseName = new Activity();
            columnCourseName.Identifier = "Course Name";
            columnCourseName.ColumnName = _GlobalResources.CourseName;
            columnCourseName.AlwaysRequired = true;
            columnCourseName.DataType = typeof(string);
            columnCourseName.MaxLength = 255;
            columnCourseName.Format = String.Empty;
            columnList.Add(columnCourseName);

            // Add Course Credit Column
            Activity columnCourseCredit = new Activity();
            columnCourseCredit.Identifier = "Course Credits";
            columnCourseCredit.ColumnName = _GlobalResources.CourseCredits;
            columnCourseCredit.AlwaysRequired = false;
            columnCourseCredit.DataType = typeof(double);
            columnCourseCredit.Format = "0-999";
            columnList.Add(columnCourseCredit);

            // Add Course Completion Date Column
            Activity columnCourseCompletionDate = new Activity();
            columnCourseCompletionDate.Identifier = "Course Completion Date";
            columnCourseCompletionDate.ColumnName = _GlobalResources.CourseCompletionDate;
            columnCourseCompletionDate.AlwaysRequired = true;
            columnCourseCompletionDate.DataType = typeof(DateTime);
            columnCourseCompletionDate.Format = dateTimeFormat;
            columnList.Add(columnCourseCompletionDate);

            // Add Lesson Name Column
            Activity columnLessonName = new Activity();
            columnLessonName.Identifier = "Lesson Name";
            columnLessonName.ColumnName = _GlobalResources.LessonName;
            columnLessonName.AlwaysRequired = false;
            columnLessonName.DataType = typeof(string);
            columnLessonName.MaxLength = 255;
            columnLessonName.Format = String.Empty;
            columnList.Add(columnLessonName);

            // Add Lesson Completion Status Column
            Activity columnLessonCompletionStatus = new Activity();
            columnLessonCompletionStatus.Identifier = "Lesson Completion Status";
            columnLessonCompletionStatus.ColumnName = _GlobalResources.ModuleCompletionStatus;
            columnLessonCompletionStatus.AlwaysRequired = false;
            columnLessonCompletionStatus.DataType = typeof(string);
            columnLessonCompletionStatus.MaxLength = 10;
            columnLessonCompletionStatus.Format = LessonCompletionTypes.Completed.ToString() + " | " + LessonCompletionTypes.Incomplete.ToString() +" | " + LessonCompletionTypes.Unknown.ToString();
            columnList.Add(columnLessonCompletionStatus);


            // Add Lesson Success Column
            Activity columnLessonSuccess = new Activity();
            columnLessonSuccess.Identifier = "Lesson Success";
            columnLessonSuccess.ColumnName = _GlobalResources.LessonSuccess;
            columnLessonSuccess.AlwaysRequired = false;
            columnLessonSuccess.DataType = typeof(string);
            columnLessonSuccess.MaxLength = 7;
            columnLessonSuccess.Format = LessonSuccessTypes.Passed.ToString() + " | " + LessonSuccessTypes.Failed.ToString() + " | " + LessonSuccessTypes.Unknown.ToString();
            columnList.Add(columnLessonSuccess);

            // Add Lesson Success Column
            Activity columnLessonScore = new Activity();
            columnLessonScore.Identifier = "Lesson Score";
            columnLessonScore.ColumnName = _GlobalResources.ModuleScore;
            columnLessonScore.AlwaysRequired = false;
            columnLessonScore.DataType = typeof(double);
            columnLessonScore.Format = "0-100";
            columnList.Add(columnLessonScore);

            // Add Course Completion Date Column
            Activity columnLessonActivityTimestamp = new Activity();
            columnLessonActivityTimestamp.Identifier = "Lesson Activity Timestamp";
            columnLessonActivityTimestamp.ColumnName = _GlobalResources.ModuleActivityTimestamp;
            columnLessonActivityTimestamp.AlwaysRequired = false;
            columnLessonActivityTimestamp.DataType = typeof(DateTime);
            columnLessonActivityTimestamp.Format = dateTimeFormat;
            columnList.Add(columnLessonActivityTimestamp);

            // Add Row End Column
            Activity columnRowEnd = new Activity();
            columnRowEnd.Identifier = "RowEnd";
            columnRowEnd.ColumnName = _GlobalResources.ROWEND_UPPER;
            columnRowEnd.AlwaysRequired = true;
            columnRowEnd.Format = _GlobalResources.RowEndInfo;
            columnList.Add(columnRowEnd);

            return columnList;
        }
        #endregion

        #endregion
    }
}
