﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class GoToTrainingAPI
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// No arguments passed, and no authentication call. 
        /// AuthenticateUser would need to be called after instansiation using this constructor if
        /// API methods are to be called.
        /// </summary>
        public GoToTrainingAPI()
        { ; }

        /// <summary>
        /// Constructor that takes credentials and authenticates the user for API calls.
        /// </summary>
        /// <param name="consumerKey">GTT application key</param>
        /// <param name="organizerLogin">GTT organizer login</param>
        /// <param name="organizerPassword">GTT organizer password</param>
        public GoToTrainingAPI(string consumerKey, string organizerLogin, string organizerPassword, string consumerSecret)
        {
            this.AuthenticateUser(consumerKey, organizerLogin, organizerPassword, consumerSecret);
        }
        #endregion

        #region Properties
        public GTT_Organizer_Response OrganizerInformation { get { return this._Organizer; } }

        public static readonly string STARTER_PLAN_NAME = "Starter";
        public static readonly string PRO_PLAN_NAME = "Pro";
        public static readonly string PLUS_PLAN_NAME = "Plus";

        public static readonly int STARTER_PLAN_MAXATTENDEES = 25;
        public static readonly int PRO_PLAN_MAXATTENDEES = 50;
        public static readonly int PLUS_PLAN_MAXATTENDEES = 200;
        #endregion

        #region Private Properties
        private string _AccessToken;
        private Int64 _OrganizerKey = 0;
        private GTT_Organizer_Response _Organizer;
        #endregion

        #region Structs
        #region GTT_Organizer_Response
        public struct GTT_Organizer_Response
        {
            // user personal information
            public string FirstName;
            public string LastName;
            public string Email;
        }
        #endregion

        #region GTT_Training_Response
        public struct GTT_Training_Response
        {
            public Int64 TrainingKey; // key is the main identifier
            public string TrainingId; // not sure why this exists if "key" is the identifier, but it does
            
            public string Name;
            public string Description;

            public List<Tuple<DateTime, DateTime>> MeetingTimes;
            public string Timezone;

            public string StartUrl;
        }
        #endregion

        #region GTT_Registrant_Response
        public struct GTT_Registrant_Response
        {
            // list of Registrant objects returned
            public List<Registrant> Registrants;

            // Registrant object
            public class Registrant
            {
                public Int64 TrainingKey;
                public Int64 RegistrantKey;
                public string FirstName;
                public string LastName;
                public string Email;
                public DateTime RegistrationDate;
                public string Status;
                public string JoinUrl;
                public string ConfirmationUrl;
            }
        }
        #endregion

        #region GTT_Attendee_Response
        public struct GTT_Attendee_Response
        {
            // list of Attendee objects returned
            public List<Attendee> Attendees;

            // Attendee object
            public class Attendee
            {
                public Int64 TrainingKey;
                public List<Int64> SessionKeys;
                public string FirstName;
                public string LastName;
                public string Email;
                public List<DateTime> JoinTime;
                public List<DateTime> LeaveTime;
                public int? TotalDuration; // in minutes
                public int NumberMeetingTimesAttended;
                public bool AllMeetingTimesAttended;
            }
        }
        #endregion
        #endregion

        #region Methods
        #region AuthenticateUser
        /// <summary>
        /// Authenticates the GTT credentials and returns a token to be used with subsequent GTT API calls.
        /// </summary>
        /// <param name="consumerKey">GTT application key</param>
        /// <param name="organizerLogin">GTT organizer login</param>
        /// <param name="organizerPassword">GTT organizer password</param>
        /// <param name="consumerSecret">GTW consumer secret (new for OAuth changes - 10/2019)</param>
        public void AuthenticateUser(string consumerKey, string organizerLogin, string organizerPassword, string consumerSecret)
        {
            try
            {
                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // depending on if there is a consumer secret, create a web request for either the new method (consumer secret), or the old method (no consumer secret)
                string requestUrl = String.Empty;
                string authorization = String.Empty;
                string payload = String.Empty;

                if (!String.IsNullOrWhiteSpace(consumerSecret))
                {
                    requestUrl = Config.ApplicationSettings.GoToTrainingAuthenticationURLv2;
                    authorization = consumerKey + ":" + consumerSecret;
                    payload = String.Format("grant_type=password&username={0}&password={1}", organizerLogin, HttpUtility.UrlEncode(organizerPassword));
                }
                else
                { requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingAuthenticationURL + "?grant_type=password&user_id={0}&password={1}&client_id={2}", organizerLogin, HttpUtility.UrlEncode(organizerPassword), consumerKey); }
                
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);

                if (!String.IsNullOrWhiteSpace(consumerSecret))
                { 
                    request.Method = "POST";
                    request.Accept = "application/json";
                    request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(authorization)));
                    request.ContentType = "application/x-www-form-urlencoded";

                    byte[] payloadBytes = Encoding.UTF8.GetBytes(payload);
                    request.ContentLength = payloadBytes.Length;

                    // send the POST payload
                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(payloadBytes, 0, payloadBytes.Length);
                        requestStream.Close();
                    }
                }
                else
                {
                    request.Accept = "application/json";
                    request.ContentType = "application/json";
                }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate organizer object
                this._Organizer = new GTT_Organizer_Response();

                // populate organizer information
                this._OrganizerKey = Int64.Parse(jsonTempResponseObject["organizer_key"].ToString());
                this._AccessToken = jsonTempResponseObject["access_token"].ToString();

                this._Organizer.FirstName = jsonTempResponseObject["firstName"].ToString();
                this._Organizer.LastName = jsonTempResponseObject["lastName"].ToString();
                this._Organizer.Email = jsonTempResponseObject["email"].ToString();
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.AuthenticateUser.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.AuthenticateUser.Exception")); }
        }
        #endregion

        #region CreateTraining
        /// <summary>
        /// Creates a training.
        /// </summary>
        /// <param name="name">name</param>
        /// <param name="description">description</param>
        /// <param name="startDate">start date in UTC</param>
        /// <param name="endDate">end date in UTC</param>
        /// <returns>GTT_Training_Response containing the training that was just created</returns>
        public GTT_Training_Response CreateTraining(string name, string description, List<Tuple<DateTime, DateTime>> meetingTimes, string timezoneDotNetName)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "CreateTraining")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings", this._OrganizerKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // clean name and description for JSON
                name = name.Replace(Environment.NewLine, "\\r\\n");
                name = name.Replace("\t", String.Empty);
                name = name.Replace("\"", "\\\"");

                description = description.Replace(Environment.NewLine, "\\r\\n");
                description = description.Replace("\t", String.Empty);
                description = description.Replace("\"", "\\\"");

                // get the start and end times from the meetingTimes list
                string meetingTimesJson = String.Empty;

                for (int i = 0; i < meetingTimes.Count; i++)
                {
                    if (i > 0)
                    { meetingTimesJson += ","; }

                    meetingTimesJson += "{ \"startDate\": \"" + meetingTimes[i].Item1.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\", \"endDate\": \"" + meetingTimes[i].Item2.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\" }";
                }

                // format the request body                
                string requestBody = "{ \"name\": \"" + name + "\", \"description\": \"" + description + "\", \"times\": [" + meetingTimesJson + "], \"timeZone\": \"" + GoToTrainingAPI.GetTimezoneName(timezoneDotNetName) + "\" }";
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";
                
                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create training object
                GTT_Training_Response training = new GTT_Training_Response();
                
                // get the training object from the response
                Newtonsoft.Json.Linq.JValue trainingKey = (Newtonsoft.Json.Linq.JValue)jsonTempResponseObject["Root"];
                
                // get the newly created training
                if (trainingKey != null)
                { training = this.GetTraining(Int64.Parse(trainingKey.Value.ToString())); }
                else
                { throw new AsentiaException(_GlobalResources.TrainingInformationCouldNotBeReadFromTheGetTrainingResponse); }

                return (training);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.CreateTraining.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.CreateTraining.Exception")); }
        }
        #endregion

        #region UpdateTraining
        /// <summary>
        /// Updates a training.
        /// </summary>
        /// <param name="trainingKey">GTT training id</param>
        /// <param name="name">name</param>
        /// <param name="description">description</param>
        /// <param name="startDate">start date in UTC</param>
        /// <param name="endDate">end date in UTC</param>
        public void UpdateTraining(Int64 trainingKey, string name, string description, List<Tuple<DateTime, DateTime>> meetingTimes, string timezoneDotNetName)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "UpdateTraining")); }

            try
            {
                // Note that UpdateTraining is done in two parts with the GTT API: 1) Name and Description, 2) Date/Time

                // NAME AND DESCRIPTION
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings/{1}/nameDescription", this._OrganizerKey.ToString(), trainingKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "PUT";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // clean name and description for JSON
                name = name.Replace(Environment.NewLine, "\\r\\n");
                name = name.Replace("\t", String.Empty);
                name = name.Replace("\"", "\\\"");

                description = description.Replace(Environment.NewLine, "\\r\\n");
                description = description.Replace("\t", String.Empty);
                description = description.Replace("\"", "\\\"");

                // format the request body
                string requestBody = "{ \"name\": \"" + name + "\", \"description\": \"" + description + "\" }";
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results - but do nothing with it because we don't need to
                // Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // DATE/TIME
                requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings/{1}/times", this._OrganizerKey.ToString(), trainingKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                request = WebRequest.Create(requestUrl);
                request.Method = "PUT";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the start and end times from the meetingTimes list
                string meetingTimesJson = String.Empty;

                for (int i = 0; i < meetingTimes.Count; i++)
                {
                    if (i > 0)
                    { meetingTimesJson += ","; }

                    meetingTimesJson += "{ \"startDate\": \"" + meetingTimes[i].Item1.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\", \"endDate\": \"" + meetingTimes[i].Item2.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\" }";
                }

                // format the request body
                requestBody = "{ \"times\": [" + meetingTimesJson + "], \"timeZone\": \"" + GoToTrainingAPI.GetTimezoneName(timezoneDotNetName) + "\" }";
                byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results - but do nothing with it because we don't need to
                // Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.UpdateTraining.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.UpdateTraining.Exception")); }
        }
        #endregion

        #region DeleteTraining
        /// <summary>
        /// Deletes a training.
        /// </summary>
        /// <param name="trainingKey">GTT training id</param>
        public void DeleteTraining(Int64 trainingKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "DeleteTraining")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings/{1}", this._OrganizerKey.ToString(), trainingKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "DELETE";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results - but we will do nothing with them because we don't need to
                // Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.DeleteTraining.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.DeleteTraining.Exception")); }
        }
        #endregion

        #region GetTraining
        /// <summary>
        /// Gets information about a training using the training's ID.
        /// </summary>
        /// <param name="trainingKey">GTW training id</param>
        /// <returns>GTT_Training_Response containing the requisted training's information</returns>
        public GTT_Training_Response GetTraining(Int64 trainingKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetTraining")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings/{1}", this._OrganizerKey.ToString(), trainingKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate training object
                GTT_Training_Response training = new GTT_Training_Response();

                // get the training object from the response
                Newtonsoft.Json.Linq.JObject responseTraining = (Newtonsoft.Json.Linq.JObject)jsonTempResponseObject["Root"];

                // if the response training is not null, populate the object with the training information; otherwise, throw an exception
                if (responseTraining != null)
                {
                    training.TrainingKey = Int64.Parse(responseTraining["trainingKey"].ToString());
                    training.TrainingId = responseTraining["trainingId"].ToString();
                    training.Name = HttpUtility.HtmlDecode(responseTraining["name"].ToString());

                    if (responseTraining["description"] != null)
                    { training.Description = HttpUtility.HtmlDecode(responseTraining["description"].ToString()); }
                    else
                    { training.Description = String.Empty; }

                    training.Timezone = responseTraining["timeZone"].ToString();
                    training.StartUrl = this.GetStartTrainingURL(Int64.Parse(responseTraining["trainingKey"].ToString()));

                    // note that dates get returned from the API, but we do not need to use them, use what we originally have stored from Asentia
                    Newtonsoft.Json.Linq.JArray responseTrainingTimesArray = (Newtonsoft.Json.Linq.JArray)responseTraining["times"];

                    if (responseTrainingTimesArray != null)
                    {
                        training.MeetingTimes = new List<Tuple<DateTime, DateTime>>();

                        for (int i = 0; i < responseTrainingTimesArray.Count; i++)
                        {
                            Newtonsoft.Json.Linq.JObject responseTrainingTimes = (Newtonsoft.Json.Linq.JObject)responseTrainingTimesArray[i];
                            training.MeetingTimes.Add(new Tuple<DateTime, DateTime>(DateTime.Parse(responseTrainingTimes["startDate"].ToString()), DateTime.Parse(responseTrainingTimes["endDate"].ToString())));
                        }
                    }
                    else
                    { throw new AsentiaException(_GlobalResources.TrainingMeetingTimeInformationCouldNotBeReadFromTheGetTrainingResponse); }                    
                }
                else
                { throw new AsentiaException(_GlobalResources.TrainingInformationCouldNotBeReadFromTheGetTrainingResponse); }

                return (training);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator + "|" + exp.Message + "|" + exp.StackTrace, "GoToTrainingAPI.GetTraining.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator + "|" + ex.Message + "|" + ex.StackTrace, "GoToTrainingAPI.GetTraining.Exception")); }
        }
        #endregion

        #region GetStartTrainingURL
        /// <summary>
        /// Gets and returns the "start training" (host) URL for a training.
        /// </summary>
        /// <param name="trainingKey">GTT training id</param>
        /// <returns>string</returns>
        public string GetStartTrainingURL(Int64 trainingKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetStartTrainingURL")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings/{1}/startUrl", this._OrganizerKey.ToString(), trainingKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }                                                

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);                

                // get the training object from the response
                Newtonsoft.Json.Linq.JValue startTrainingUrl = (Newtonsoft.Json.Linq.JValue)jsonTempResponseObject["Root"];

                // return the URL
                if (startTrainingUrl != null)
                { return startTrainingUrl.Value.ToString(); }
                else
                { throw new AsentiaException(_GlobalResources.TrainingInformationCouldNotBeReadFromTheGetTrainingResponse); }
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.GetStartTrainingURL.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.GetStartTrainingURL.Exception")); }
        }
        #endregion        

        #region CreateRegistrant
        /// <summary>
        /// Creates a training registrant.
        /// </summary>
        /// <param name="trainingKey">GTT training id</param>
        /// <param name="firstName">registrant first name</param>
        /// <param name="lastName">registrant last name</param>
        /// <param name="email">registrant email address</param>
        /// <returns>GTT_Registrant_Response containing the training registrant's information</returns>
        public GTT_Registrant_Response CreateRegistrant(Int64 trainingKey, string firstName, string lastName, string email)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "CreateRegistrant")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings/{1}/registrants", this._OrganizerKey.ToString(), trainingKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // format the request body                
                string requestBody = "{ \"givenName\": \"" + firstName + "\", \"surname\": \"" + lastName + "\", \"email\": \"" + email + "\" }";
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create registrant object
                GTT_Registrant_Response registrant = new GTT_Registrant_Response();

                // get the registrant object from the response
                Newtonsoft.Json.Linq.JObject responseRegistrant = (Newtonsoft.Json.Linq.JObject)jsonTempResponseObject["Root"];

                // get the newly created registrant
                if (responseRegistrant != null)
                { registrant = this.GetRegistrant(trainingKey, Int64.Parse(responseRegistrant["registrantKey"].ToString())); }
                else
                { throw new AsentiaException(_GlobalResources.RegistrantInformationCouldNotBeReadFromTheGetRegistrantResponse); }

                return (registrant);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.CreateRegistrant.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.CreateRegistrant.Exception")); }
        }
        #endregion

        #region DeleteRegistrant
        /// <summary>
        /// Deletes a training registrant.
        /// </summary>
        /// <param name="trainingKey">GTT training id</param>
        /// <param name="registrantKey">GTT training registrant id</param>        
        public void DeleteRegistrant(Int64 trainingKey, Int64 registrantKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "DeleteRegistrant")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings/{1}/registrants/{2}", this._OrganizerKey.ToString(), trainingKey.ToString(), registrantKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "DELETE";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results - but we will do nothing with them because we don't need to
                // Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.DeleteRegistrant.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.DeleteRegistrant.Exception")); }
        }
        #endregion

        #region GetRegistrant
        /// <summary>
        /// Gets information about a training registrant using the training's id and the registrant's id.
        /// </summary>
        /// <param name="trainingKey">GTT training id</param>
        /// <param name="registrantKey">GTT training registrant id</param>
        /// <returns>GTT_Registrant_Response containing the requested training registrant's information</returns>        
        public GTT_Registrant_Response GetRegistrant(Int64 trainingKey, Int64 registrantKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetRegistrant")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings/{1}/registrants/{2}", this._OrganizerKey.ToString(), trainingKey.ToString(), registrantKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate registrant object
                GTT_Registrant_Response registrant = new GTT_Registrant_Response();

                // get the registrant from the response
                Newtonsoft.Json.Linq.JObject responseRegistrant = (Newtonsoft.Json.Linq.JObject)jsonTempResponseObject["Root"];

                // if the response registrant is not null, populate the object with the registrant information; otherwise, throw an exception
                if (responseRegistrant != null)
                {
                    registrant.Registrants = new List<GTT_Registrant_Response.Registrant>();

                    registrant.Registrants.Add(new GTT_Registrant_Response.Registrant()
                    {
                        TrainingKey = trainingKey,
                        RegistrantKey = registrantKey,
                        FirstName = responseRegistrant["givenName"].ToString(),
                        LastName = responseRegistrant["surname"].ToString(),
                        Email = responseRegistrant["email"].ToString(),
                        RegistrationDate = DateTime.Parse(responseRegistrant["registrationDate"].ToString()),
                        Status = responseRegistrant["status"].ToString(),
                        JoinUrl = responseRegistrant["joinUrl"].ToString(),
                        ConfirmationUrl = responseRegistrant["confirmationUrl"].ToString()
                    });
                }
                else
                { throw new AsentiaException(_GlobalResources.RegistrantInformationCouldNotBeReadFromTheGetRegistrantResponse); }

                return (registrant);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.GetRegistrant.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.GetRegistrant.Exception")); }
        }
        #endregion

        #region GetTrainingRegistrants
        /// <summary>
        /// Gets the registrants of a training.
        /// </summary>
        /// <param name="trainingKey">GTT training id</param>
        /// <returns>GTT_Registrant_Response containing training registrants</returns>
        public GTT_Registrant_Response GetTrainingRegistrants(Int64 trainingKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetTrainingRegistrants")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "organizers/{0}/trainings/{1}/registrants", this._OrganizerKey.ToString(), trainingKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate registrants object
                GTT_Registrant_Response registrants = new GTT_Registrant_Response();

                // get an array of registrants from the response
                Newtonsoft.Json.Linq.JArray responseRegistrantsArray = (Newtonsoft.Json.Linq.JArray)jsonTempResponseObject["Root"];

                // if the response registrants array is not null, loop through it to populate the object with the registrant information; otherwise, throw an exception
                if (responseRegistrantsArray != null)
                {
                    registrants.Registrants = new List<GTT_Registrant_Response.Registrant>();

                    foreach (Newtonsoft.Json.Linq.JObject responseRegistrant in responseRegistrantsArray)
                    {                        
                        registrants.Registrants.Add(new GTT_Registrant_Response.Registrant()
                        {
                            TrainingKey = trainingKey,
                            RegistrantKey = Int64.Parse(responseRegistrant["registrantKey"].ToString()),
                            FirstName = responseRegistrant["givenName"].ToString(),
                            LastName = responseRegistrant["surname"].ToString(),
                            Email = responseRegistrant["email"].ToString(),
                            RegistrationDate = DateTime.Parse(responseRegistrant["registrationDate"].ToString()),
                            Status = responseRegistrant["status"].ToString(),
                            JoinUrl = responseRegistrant["joinUrl"].ToString(),
                            ConfirmationUrl = responseRegistrant["confirmationUrl"].ToString()
                        });
                    }
                }
                else
                { throw new AsentiaException(_GlobalResources.RegistrantInformationCouldNotBeReadFromTheGetTrainingRegistrantsResponse); }

                return (registrants);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.GetTrainingRegistrants.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.GetTrainingRegistrants.Exception")); }
        }
        #endregion

        #region GetTrainingAttendees
        /// <summary>
        /// Gets the attendees of a training. Note that the training must have occurred (not in the future), or this will throw an error.
        /// </summary>
        /// <param name="trainingKey">GTT training id</param>
        /// <returns>GTT_Attendee_Response containing training attendees</returns>
        public GTT_Attendee_Response GetTrainingAttendees(Int64 trainingKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetTrainingAttendees")); }

            try
            {
                // get all the session keys for the training
                List<Int64> sessionKeys = this._GetTrainingSessionIds(trainingKey);

                // create and populate attendees object
                GTT_Attendee_Response attendees = new GTT_Attendee_Response();
                attendees.Attendees = new List<GTT_Attendee_Response.Attendee>();

                foreach (Int64 sessionKey in sessionKeys)
                {
                    string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "/reports/organizers/{0}/sessions/{1}/attendees", this._OrganizerKey.ToString(), sessionKey.ToString());

                    // set security protocol to TLS 1.2 as LogMeIn requires
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    WebRequest request = WebRequest.Create(requestUrl);
                    request.Method = "GET";
                    request.Headers["ContentType"] = "application/json";
                    request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                    // get the response
                    string responseData = String.Empty;

                    using (WebResponse response = request.GetResponse())
                    {
                        // get the response from the stream
                        using (Stream dataStream = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(dataStream))
                            {
                                responseData = reader.ReadToEnd();
                            }
                        }
                    }

                    // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                    responseData = "{\"Root\" : " + responseData + "}";

                    // create temp json object for parsing results
                    Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);                    

                    // get an array of attendees from the response
                    Newtonsoft.Json.Linq.JArray responseAttendeesArray = (Newtonsoft.Json.Linq.JArray)jsonTempResponseObject["Root"];

                    // if the response attendees array is not null, loop through it to populate the object with the attendee information; otherwise, throw an exception
                    if (responseAttendeesArray != null)
                    {                        
                        foreach (Newtonsoft.Json.Linq.JObject responseAttendee in responseAttendeesArray)
                        {
                            // get the join time and leave time from the attendance data for the attendee
                            Newtonsoft.Json.Linq.JArray responseAttendeeAttendanceTimesArray = (Newtonsoft.Json.Linq.JArray)responseAttendee["inSessionTimes"];
                            DateTime joinTime = DateTime.MinValue;
                            DateTime leaveTime = DateTime.MinValue;

                            if (responseAttendeeAttendanceTimesArray != null)
                            {
                                if (responseAttendeeAttendanceTimesArray.Count > 0)
                                {
                                    Newtonsoft.Json.Linq.JObject responseAttendeeAttendanceTimes = (Newtonsoft.Json.Linq.JObject)responseAttendeeAttendanceTimesArray[0]; // this is an array of times, but we should only need the first set
                                    joinTime = DateTime.Parse(responseAttendeeAttendanceTimes["joinTime"].ToString());
                                    leaveTime = DateTime.Parse(responseAttendeeAttendanceTimes["leaveTime"].ToString());
                                }
                            }

                            // check for attendee existence, if exists, add session information; if not exists, add it
                            bool attendeeExists = false;

                            foreach (GTT_Attendee_Response.Attendee existingAttendee in attendees.Attendees)
                            {
                                if (existingAttendee.Email == responseAttendee["email"].ToString())
                                {
                                    attendeeExists = true;

                                    existingAttendee.SessionKeys.Add(sessionKey);
                                    existingAttendee.JoinTime.Add(joinTime);
                                    existingAttendee.LeaveTime.Add(leaveTime);
                                    existingAttendee.TotalDuration += int.Parse(responseAttendee["timeInSession"].ToString());
                                    existingAttendee.NumberMeetingTimesAttended += 1;

                                    if (existingAttendee.NumberMeetingTimesAttended == sessionKeys.Count)
                                    { existingAttendee.AllMeetingTimesAttended = true; }

                                    break;
                                }
                            }

                            if (!attendeeExists)
                            {
                                // add the attendee information to the object
                                GTT_Attendee_Response.Attendee newAttendee = new GTT_Attendee_Response.Attendee();

                                newAttendee.TrainingKey = trainingKey;
                                
                                newAttendee.SessionKeys = new List<Int64>();
                                newAttendee.SessionKeys.Add(sessionKey);

                                newAttendee.FirstName = responseAttendee["givenName"].ToString();
                                newAttendee.LastName = responseAttendee["surname"].ToString();
                                newAttendee.Email = responseAttendee["email"].ToString();

                                newAttendee.JoinTime = new List<DateTime>();
                                newAttendee.JoinTime.Add(joinTime);

                                newAttendee.LeaveTime = new List<DateTime>();
                                newAttendee.LeaveTime.Add(leaveTime);

                                newAttendee.TotalDuration = 0;
                                newAttendee.TotalDuration += int.Parse(responseAttendee["timeInSession"].ToString());

                                newAttendee.NumberMeetingTimesAttended = 1;
                                
                                newAttendee.AllMeetingTimesAttended = false;

                                if (newAttendee.NumberMeetingTimesAttended == sessionKeys.Count)
                                { newAttendee.AllMeetingTimesAttended = true; }

                                attendees.Attendees.Add(newAttendee);
                            }
                        }
                    }
                    else
                    { throw new AsentiaException(_GlobalResources.AttendeeInformationCouldNotBeReadFromTheGetTrainingAttendeesResponse); }
                }

                return (attendees);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.GetTrainingAttendees.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI.GetTrainingAttendees.Exception")); }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _GetTrainingSessionIds
        /// <summary>
        /// Gets the session ids of a training.
        /// </summary>
        /// <param name="trainingKey">GTT training id</param>
        /// <returns>session id for a training</returns>
        public List<Int64> _GetTrainingSessionIds(Int64 trainingKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "_GetTrainingSessionId")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToTrainingRestAPIURL + "/reports/organizers/{0}/trainings/{1}", this._OrganizerKey.ToString(), trainingKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";
                
                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // declare the list of session keys
                List<Int64> sessionKeys = new List<Int64>();

                // get an array of session ids from the response - note that while an array is returned, we should only have one session per training
                Newtonsoft.Json.Linq.JArray responseSessionsArray = (Newtonsoft.Json.Linq.JArray)jsonTempResponseObject["Root"];                

                // if the response sessions array is not null, get all session ids; otherwise, throw an exception
                if (responseSessionsArray != null)
                {
                    for (int i = 0; i < responseSessionsArray.Count; i++)
                    {
                        Newtonsoft.Json.Linq.JObject responseSession = (Newtonsoft.Json.Linq.JObject)responseSessionsArray[i];
                        sessionKeys.Add(Int64.Parse(responseSession["sessionKey"].ToString()));
                    }
                }
                else
                { throw new AsentiaException(_GlobalResources.SessionIdInformationCouldNotBeReadFromTheGetTrainingSessionIdResponse); }

                return (sessionKeys);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI._GetTrainingSessionId.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToTrainingAPI._GetTrainingSessionId.Exception")); }
        }
        #endregion

        #region _HandleErrorResponse
        /// <summary>
        /// Handles errors returned from the GTM API and returns a string describing the error.
        /// </summary>
        /// <param name="response">GTM response</param>
        /// <returns>string</returns>
        private string _HandleErrorResponse(HttpWebResponse response)
        {
            string errorResponseString = String.Empty;

            switch (response.StatusCode)
            {
                case HttpStatusCode.NoContent:          // 204
                case HttpStatusCode.BadRequest:         // 400
                case HttpStatusCode.Unauthorized:       // 401
                case HttpStatusCode.Forbidden:          // 403
                case HttpStatusCode.NotFound:           // 404
                case HttpStatusCode.MethodNotAllowed:   // 405
                case HttpStatusCode.Conflict:           // 409                

                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string responseString = reader.ReadToEnd();

                        Newtonsoft.Json.Linq.JObject jsonTempErrorResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseString);

                        if (jsonTempErrorResponseObject["error"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["error"].ToString(); }
                        else if (jsonTempErrorResponseObject["int_err_code"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["int_err_code"].ToString(); }
                        else if (jsonTempErrorResponseObject["int_error_code"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["int_error_code"].ToString(); }
                        else if (jsonTempErrorResponseObject["errorCode"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["errorCode"].ToString(); }
                        else if (jsonTempErrorResponseObject["error_description"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["error_description"].ToString(); }
                        else
                        { errorResponseString = _GlobalResources.AFatalErrorOccurredInTheAPIPleaseContactAnAdministrator + "(Response from API: ) " + responseString; }
                    }

                    break;
                default:                    
                    errorResponseString = _GlobalResources.AFatalErrorOccurredInTheAPIPleaseContactAnAdministrator + "(Code 2)";
                    break;
            }

            return errorResponseString;
        }
        #endregion
        #endregion

        #region Static Methods
        #region GetTimezoneName
        /// <summary>
        /// Returns a string representing the GTT/GTW timezone name as mapped to "dot net" timezone name.
        /// </summary>
        /// <param name="dotNetName">the timezone name in "dot net"</param>
        /// <returns>string representing the timezone name for GTT/GTW</returns>
        public static string GetTimezoneName(string dotNetName)
        {
            string timezoneGoToName = "America/New_York"; // default to EST

            switch (dotNetName)
            {
                case "Dateline Standard Time":
                    timezoneGoToName = "MIT";
                    break;
                case "UTC-11":
                    timezoneGoToName = "MIT";
                    break;
                case "Hawaiian Standard Time":
                    timezoneGoToName = "Pacific/Honolulu";
                    break;
                case "Alaskan Standard Time":
                    timezoneGoToName = "America/Anchorage";
                    break;
                case "Pacific Standard Time (Mexico)":
                    timezoneGoToName = "America/Los_Angeles";
                    break;
                case "Pacific Standard Time":
                    timezoneGoToName = "America/Los_Angeles";
                    break;
                case "US Mountain Standard Time":
                    timezoneGoToName = "America/Phoenix";
                    break;
                case "Mountain Standard Time (Mexico)":
                    timezoneGoToName = "America/Denver";
                    break;
                case "Mountain Standard Time":
                    timezoneGoToName = "America/Denver";
                    break;
                case "Central America Standard Time":
                    timezoneGoToName = "America/Mexico_City";
                    break;
                case "Central Standard Time":
                    timezoneGoToName = "America/Chicago";
                    break;
                case "Central Standard Time (Mexico)":
                    timezoneGoToName = "America/Mexico_City";
                    break;
                case "Canada Central Standard Time":
                    timezoneGoToName = "America/Chicago";
                    break;
                case "SA Pacific Standard Time":
                    timezoneGoToName = "America/Bogota";
                    break;
                case "Eastern Standard Time":
                    timezoneGoToName = "America/New_York";
                    break;
                case "US Eastern Standard Time":
                    timezoneGoToName = "America/Indianapolis";
                    break;
                case "Venezuela Standard Time":
                    timezoneGoToName = "America/Caracas";
                    break;
                case "Paraguay Standard Time":
                    timezoneGoToName = "America/Caracas";
                    break;
                case "Atlantic Standard Time":
                    timezoneGoToName = "America/Halifax";
                    break;
                case "Central Brazilian Standard Time":
                    timezoneGoToName = "America/Santiago";
                    break;
                case "SA Western Standard Time":
                    timezoneGoToName = "America/Caracas";
                    break;
                case "Pacific SA Standard Time":
                    timezoneGoToName = "America/Santiago";
                    break;
                case "Newfoundland Standard Time":
                    timezoneGoToName = "America/St_Johns";
                    break;
                case "E. South America Standard Time":
                    timezoneGoToName = "America/Sao_Paulo";
                    break;
                case "Argentina Standard Time":
                    timezoneGoToName = "America/Buenos_Aires";
                    break;
                case "SA Eastern Standard Time":
                    timezoneGoToName = "America/Buenos_Aires";
                    break;
                case "Greenland Standard Time":
                    timezoneGoToName = "America/St_Johns";
                    break;
                case "Montevideo Standard Time":
                    timezoneGoToName = "America/St_Johns";
                    break;
                case "Bahia Standard Time":
                    timezoneGoToName = "America/Buenos_Aires";
                    break;
                case "UTC-02":
                    timezoneGoToName = "Atlantic/Azores";
                    break;
                case "Mid-Atlantic Standard Time":
                    timezoneGoToName = "Atlantic/Azores";
                    break;
                case "Azores Standard Time":
                    timezoneGoToName = "Atlantic/Azores";
                    break;
                case "Cape Verde Standard Time":
                    timezoneGoToName = "Atlantic/Cape_Verde";
                    break;
                case "Morocco Standard Time":
                    timezoneGoToName = "Africa/Casablanca";
                    break;
                case "UTC":
                    timezoneGoToName = "GMT";
                    break;
                case "GMT Standard Time":
                    timezoneGoToName = "Europe/London";
                    break;
                case "Greenwich Standard Time":
                    timezoneGoToName = "GMT";
                    break;
                case "W. Europe Standard Time":
                    timezoneGoToName = "Europe/Amsterdam";
                    break;
                case "Central Europe Standard Time":
                    timezoneGoToName = "Europe/Prague";
                    break;
                case "Romance Standard Time":
                    timezoneGoToName = "Europe/Brussels";
                    break;
                case "Central European Standard Time":
                    timezoneGoToName = "Europe/Warsaw";
                    break;
                case "Libya Standard Time":
                    timezoneGoToName = "Africa/Malabo";
                    break;
                case "W. Central Africa Standard Time":
                    timezoneGoToName = "Africa/Malabo";
                    break;
                case "Namibia Standard Time":
                    timezoneGoToName = "Africa/Malabo";
                    break;
                case "GTB Standard Time":
                    timezoneGoToName = "Europe/Athens";
                    break;
                case "Middle East Standard Time":
                    timezoneGoToName = "Africa/Cairo";
                    break;
                case "Egypt Standard Time":
                    timezoneGoToName = "Africa/Cairo";
                    break;
                case "Syria Standard Time":
                    timezoneGoToName = "Africa/Cairo";
                    break;
                case "E. Europe Standard Time":
                    timezoneGoToName = "Europe/Helsinki";
                    break;
                case "South Africa Standard Time":
                    timezoneGoToName = "Africa/Harare";
                    break;
                case "FLE Standard Time":
                    timezoneGoToName = "Europe/Helsinki";
                    break;
                case "Turkey Standard Time":
                    timezoneGoToName = "Asia/Jerusalem";
                    break;
                case "Israel Standard Time":
                    timezoneGoToName = "Asia/Jerusalem";
                    break;
                case "Jordan Standard Time":
                    timezoneGoToName = "Asia/Baghdad";
                    break;
                case "Arabic Standard Time":
                    timezoneGoToName = "Asia/Baghdad";
                    break;
                case "Kaliningrad Standard Time":
                    timezoneGoToName = "Europe/Moscow";
                    break;
                case "Arab Standard Time":
                    timezoneGoToName = "Asia/Kuwait";
                    break;
                case "E. Africa Standard Time":
                    timezoneGoToName = "Africa/Nairobi";
                    break;
                case "Iran Standard Time":
                    timezoneGoToName = "Asia/Tehran";
                    break;
                case "Arabian Standard Time":
                    timezoneGoToName = "Asia/Muscat";
                    break;
                case "Azerbaijan Standard Time":
                    timezoneGoToName = "Asia/Tbilisi";
                    break;
                case "Russian Standard Time":
                    timezoneGoToName = "Europe/Moscow";
                    break;
                case "Mauritius Standard Time":
                    timezoneGoToName = "Asia/Tbilisi";
                    break;
                case "Georgian Standard Time":
                    timezoneGoToName = "Asia/Tbilisi";
                    break;
                case "Caucasus Standard Time":
                    timezoneGoToName = "Asia/Tbilisi";
                    break;
                case "Afghanistan Standard Time":
                    timezoneGoToName = "Asia/Kabul";
                    break;
                case "West Asia Standard Time":
                    timezoneGoToName = "Asia/Karachi";
                    break;
                case "Pakistan Standard Time":
                    timezoneGoToName = "Asia/Karachi";
                    break;
                case "India Standard Time":
                    timezoneGoToName = "Asia/Calcutta";
                    break;
                case "Sri Lanka Standard Time":
                    timezoneGoToName = "Asia/Colombo";
                    break;
                case "Nepal Standard Time":
                    timezoneGoToName = "Asia/Katmandu";
                    break;
                case "Central Asia Standard Time":
                    timezoneGoToName = "Asia/Dhaka";
                    break;
                case "Bangladesh Standard Time":
                    timezoneGoToName = "Asia/Dhaka";
                    break;
                case "Ekaterinburg Standard Time":
                    timezoneGoToName = "Asia/Dhaka";
                    break;
                case "Myanmar Standard Time":
                    timezoneGoToName = "Asia/Rangoon";
                    break;
                case "SE Asia Standard Time":
                    timezoneGoToName = "Asia/Bangkok";
                    break;
                case "N. Central Asia Standard Time":
                    timezoneGoToName = "Asia/Jakarta";
                    break;
                case "China Standard Time":
                    timezoneGoToName = "Asia/Shanghai";
                    break;
                case "North Asia Standard Time":
                    timezoneGoToName = "Asia/Shanghai";
                    break;
                case "Singapore Standard Time":
                    timezoneGoToName = "Asia/Singapore";
                    break;
                case "W. Australia Standard Time":
                    timezoneGoToName = "Australia/Perth";
                    break;
                case "Taipei Standard Time":
                    timezoneGoToName = "Asia/Taipei";
                    break;
                case "Ulaanbaatar Standard Time":
                    timezoneGoToName = "Asia/Irkutsk";
                    break;
                case "North Asia East Standard Time":
                    timezoneGoToName = "Asia/Irkutsk";
                    break;
                case "Tokyo Standard Time":
                    timezoneGoToName = "Asia/Tokyo";
                    break;
                case "Korea Standard Time":
                    timezoneGoToName = "Asia/Seoul";
                    break;
                case "Cen. Australia Standard Time":
                    timezoneGoToName = "Australia/Adelaide";
                    break;
                case "AUS Central Standard Time":
                    timezoneGoToName = "Australia/Darwin";
                    break;
                case "E. Australia Standard Time":
                    timezoneGoToName = "Australia/Brisbane";
                    break;
                case "AUS Eastern Standard Time":
                    timezoneGoToName = "Australia/Sydney";
                    break;
                case "West Pacific Standard Time":
                    timezoneGoToName = "Pacific/Guam";
                    break;
                case "Tasmania Standard Time":
                    timezoneGoToName = "Australia/Hobart";
                    break;
                case "Yakutsk Standard Time":
                    timezoneGoToName = "Asia/Vladivostok";
                    break;
                case "Central Pacific Standard Time":
                    timezoneGoToName = "Asia/Magadan";
                    break;
                case "Vladivostok Standard Time":
                    timezoneGoToName = "Asia/Vladivostok";
                    break;
                case "New Zealand Standard Time":
                    timezoneGoToName = "Pacific/Auckland";
                    break;
                case "UTC+12":
                    timezoneGoToName = "Pacific/Auckland";
                    break;
                case "Fiji Standard Time":
                    timezoneGoToName = "Pacific/Fiji";
                    break;
                case "Magadan Standard Time":
                    timezoneGoToName = "Pacific/Fiji";
                    break;
                case "Kamchatka Standard Time":
                    timezoneGoToName = "Pacific/Fiji";
                    break;
                case "Tonga Standard Time":
                    timezoneGoToName = "Pacific/Tongatapu";
                    break;
                case "Samoa Standard Time":
                    timezoneGoToName = "Pacific/Tongatapu";
                    break;
                default:
                    break;
            }

            // return
            return timezoneGoToName;
        }
        #endregion
        #endregion
    }
}