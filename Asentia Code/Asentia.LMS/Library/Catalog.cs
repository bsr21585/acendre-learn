﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    [XmlInclude(typeof(LanguageSpecificProperty))]
    [Serializable]
    public class Catalog
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Catalog()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idCatalog">Catalog Id</param>
        public Catalog(int idCatalog)
        {
            this.LanguageSpecificProperties = new ArrayList();
            this._Details(idCatalog);
            this._GetPropertiesInLanguages(idCatalog);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Catalog.GetGrid]";

        /// <summary>
        /// Catalog Id.
        /// </summary>
        public int IdCatalog = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Parent Id.
        /// </summary>
        public int? IdParent;

        /// <summary>
        /// Title.
        /// </summary>
        public string Title;

        /// <summary>
        /// Order.
        /// </summary>
        public int? Order;

        /// <summary>
        /// Short Description.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string ShortDescription;

        /// <summary>
        /// Long Description.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string LongDescription;

        /// <summary>
        /// Is Published.
        /// </summary>
        public bool? IsPublished;

        /// <summary>
        /// Is Private.
        /// </summary>
        public bool IsPrivate;

        /// <summary>
        /// Cost
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public double? Cost;

        /// <summary>
        /// The catalog cost calculated.
        /// </summary>
        public double? CalculatedCost;

        /// <summary>
        /// Cost Type
        /// </summary>
        [XmlIgnore]
        [JsonIgnore]
        public int CostType;

        /// <summary>
        /// Is Closed.
        /// </summary>
        public bool? IsClosed;

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime? DtCreated;

        /// <summary>
        /// Date Modified.
        /// </summary>
        public DateTime? DtModified;

        /// <summary>
        /// Search Tags.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string SearchTags;

        /// <summary>
        /// The number of "enrollable" (published, not closed) courses 
        /// that are a direct child of this catalog. This is used for the 
        /// "Courses Included" count in the catalog.
        /// </summary>
        public int NumberEnrollableCourses;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// Avatar
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Avatar;

        /// <summary>
        /// Shortcode for linking directly to the course through the catalog
        /// </summary>
        public string Shortcode;

        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Title;
            public string ShortDescription;
            public string LongDescription;
            public string SearchTags;

            public LanguageSpecificProperty()
            { ;}

            public LanguageSpecificProperty(string langString, string title, string shortDescription, string longDescription, string searchTags)
            {
                this.LangString = langString;
                this.Title = title;
                this.ShortDescription = shortDescription;
                this.LongDescription = longDescription;
                this.SearchTags = searchTags;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves object data to the database.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseFieldNotUniqueException">
        /// Thrown when a field must be unique.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <exception cref="DatabaseFieldConstraintException">
        /// Thrown when a value passed into a stored procedure violates a business rule.
        /// </exception>
        /// <exception cref="DatabaseSpecifiedLanguageNotDefaultException">
        /// Thrown when a Database Specified Language Not Default Exception occurs in a stored procedure.
        /// </exception>
        /// <param name=""></param>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCatalog", this.IdCatalog, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            
            if (this.IdParent == null) 
            { databaseObject.AddParameter("@idParent", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else 
            { databaseObject.AddParameter("@idParent", this.IdParent, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.Order == null) 
            { databaseObject.AddParameter("@order", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else 
            { databaseObject.AddParameter("@order", this.Order, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@shortDescription", this.ShortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@longDescription", this.LongDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@isPrivate", this.IsPrivate, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@isClosed", this.IsClosed, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@cost", this.Cost, SqlDbType.Decimal, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@costType", this.CostType, SqlDbType.SmallInt, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", this.SearchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@avatar", this.Avatar, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortcode", this.Shortcode, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Catalog.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdCatalog = Convert.ToInt32(databaseObject.Command.Parameters["@idCatalog"].Value);

                return this.IdCatalog;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveAvatar
        /// <summary>
        /// Saves catalog's avatar path to database as the current session user.
        /// </summary>
        public void SaveAvatar()
        {
            this._SaveAvatar(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves catalog's avatar path to database with a caller specified.
        /// This would be used when SaveAvatar needs to be called outside of a user session, i.e.
        /// API. Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public void SaveAvatar(int idCaller)
        {
            this._SaveAvatar(idCaller);
        }
        #endregion

        #region GetCoursesInDataTable
        /// <summary>
        /// Retrieves courses associated with a catalog.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idCatalog">Catalog Id</param>
        public DataTable GetCoursesInDataTable(int idParentCatalog)
        {
            DataTable coursesDataTable = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            if (idParentCatalog == null)
            { databaseObject.AddParameter("@idParentCatalog", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idParentCatalog", idParentCatalog, SqlDbType.Int, 4, ParameterDirection.Input); }

            try
            {
                SqlDataReader dataReader = databaseObject.ExecuteDataReader("[Catalog.GetCourses]", true);
                coursesDataTable.Load(dataReader);
                dataReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

            }
            catch
            {

            }
            finally
            {
                databaseObject.Dispose();
            }
            return coursesDataTable;
        }
        #endregion

        #region UpdateOrdering
        /// <summary>
        /// Method to update the order of catalog and courses.
        /// </summary>
        /// <param name="catalogsWithOrdering"></param>
        /// <param name="coursesWithOrdering"></param>
        public void UpdateOrdering(DataTable catalogsWithOrdering, DataTable coursesWithOrdering)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@CatalogsWithOrdering", catalogsWithOrdering, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@CoursesWithOrdering", coursesWithOrdering, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Catalog.UpdateOrder]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Join Courses
        /// <summary>
        /// Adds multiple courses to a catalog.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseFieldNotUniqueException">
        /// Thrown when a field must be unique..
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="courses">Courses to add</param>
        public void JoinCourses(DataTable courses)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCatalog", this.IdCatalog, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Courses", courses, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Catalog.JoinCourses]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Add or updates the optional language fields for a catalog
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseFieldNotUniqueException">
        /// Thrown when a field must be unique..
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name=""></param>
        public void SaveLang(string languageString)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCatalog", this.IdCatalog, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", this.ShortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@longDescription", this.LongDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", this.SearchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Catalog.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Remove Courses
        /// <summary>
        /// Removes multiple courses from catalog.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseFieldNotUniqueException">
        /// Thrown when a field must be unique..
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="users">Courses to remove</param>
        public void RemoveCourses(DataTable courses)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@IdCatalog", this.IdCatalog, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Courses", courses, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Catalog.RemoveCourses]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCatalogAPI
        /// <summary>
        /// Return a list of catalog for APIquery
        /// </summary>
        public static List<int> GetCatalogAPI(string searchParam)
        {
            List<int> returnIDs = new List<int>();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            // SEARCH PARAMETER
            // build the full text query
            FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
            string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

            // if it's not empty, use it; else make it a wildcard
            SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

            databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Catalog.IdsForAPI]", true);
                while (sdr.Read())
                {
                    returnIDs.Add(Convert.ToInt32(sdr[0]));
                }
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return returnIDs;
        }
        #endregion

        #region Get Parents Catalogs
        /// <summary>
        /// Method to get parent catalog.
        /// </summary>
        /// <param name="languageString">selected language</param>
        /// <returns></returns>
        public DataTable GetParents(string languageString, bool showPrivateCatalog = false)
        {
            DataTable catalogsDataTable = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();


            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@showPrivateCatalog", showPrivateCatalog, SqlDbType.Bit, 1, ParameterDirection.Input);

                SqlDataReader catalogsReader = databaseObject.ExecuteDataReader("[Catalog.GetParents]", true);
                catalogsDataTable.Load(catalogsReader);
                catalogsReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return catalogsDataTable;
        }
        #endregion

        #region Get Parents Catalogs for group
        /// <summary>
        /// Method to get parent catalog for the provided group id..
        /// </summary>
        /// <param name="languageString">selected language</param>
        /// <param name="idGroup">idGroup</param>
        /// <returns></returns>
        public DataTable GetParentsForGroup(string languageString, int idGroup)
        {
            DataTable catalogsDataTable = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();


            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idGroup", idGroup, SqlDbType.NVarChar, 10, ParameterDirection.Input);

                SqlDataReader catalogsReader = databaseObject.ExecuteDataReader("[Catalog.GetParentsForGroup]", true);
                catalogsDataTable.Load(catalogsReader);
                catalogsReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return catalogsDataTable;
        }
        #endregion

        #region Get Children Catalogs And Courses
        /// <summary>
        /// Method to get children catalogs and courses
        /// </summary>
        /// <param name="idParentCatalog">idParentCatalog</param>
        /// <param name="languageString">selected language</param>
        /// <returns></returns>
        public DataTable GetChildren(int idParentCatalog, string languageString, bool showPrivateCatalog = false)
        {
            DataTable catalogsDataTable = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idParentCatalog", idParentCatalog, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@showPrivateCatalog", showPrivateCatalog, SqlDbType.Bit, 1, ParameterDirection.Input);

                SqlDataReader catalogsReader = databaseObject.ExecuteDataReader("[Catalog.GetChildren]", true);
                catalogsDataTable.Load(catalogsReader);
                catalogsReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return catalogsDataTable;
        }
        #endregion

        #region Get Children Catalogs And Courses
        /// <summary>
        /// Method to get children catalogs for provided group id.
        /// </summary>
        /// <param name="idParentCatalog">idParentCatalog</param>
        /// <param name="languageString">selected language</param>
        /// <param name="idGroup">idGroup</param>
        /// <returns></returns>
        public DataTable GetChildrenForGroup(int idParentCatalog, string languageString, int idGroup)
        {
            DataTable catalogsDataTable = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idParentCatalog", idParentCatalog, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idGroup", idGroup, SqlDbType.NVarChar, 10, ParameterDirection.Input);

                SqlDataReader catalogsReader = databaseObject.ExecuteDataReader("[Catalog.GetChildrenForGroup]", true);
                catalogsDataTable.Load(catalogsReader);
                catalogsReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return catalogsDataTable;
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes a catalog.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Catalogs", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Catalog.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForCouponCodeSelectList
        /// <summary>
        /// Gets a listing of catalog ids and titles to populate into a select list.
        /// </summary>
        /// <param name="idCatalog">catalog id</param>
        /// <returns>DataTable of catalog ids and titles.</returns>
        public static DataTable IdsAndNamesForCouponCodeSelectList(int idCouponCode, string searchParam)
        {
            DataTable dt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCouponCode", idCouponCode, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Catalog.IdsAndNamesForCouponCodeSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCatalogs
        /// <summary>
        /// Returns a recordset of catalogs parented by a specified catalog, or belonging to the 
        /// catalog root if @idParentCatalog is null.
        /// </summary>
        /// <param name="idParentCatalog">parent catalog id</param>
        /// <returns></returns>
        public static DataTable GetCatalogs(int? idParentCatalog, bool? showPrivateCatalogs = true)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                if (idParentCatalog == null)
                { databaseObject.AddParameter("@idParentCatalog", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@idParentCatalog", idParentCatalog, SqlDbType.Int, 4, ParameterDirection.Input); }

                databaseObject.AddParameter("@showPrivateCatalogs", showPrivateCatalogs, SqlDbType.Bit, 1, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Catalog.GetCatalogs]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCourses
        /// <summary>
        /// Returns a recordset of courses parented by a specific catalog, or belonging to the
        /// catalog root if @idParentCatalog is null.
        /// </summary>
        /// <param name="idParentCatalog">parent catalog id</param>
        /// <returns></returns>
        public static DataTable GetCourses(int? idParentCatalog)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                if (idParentCatalog == null)
                { databaseObject.AddParameter("@idParentCatalog", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@idParentCatalog", idParentCatalog, SqlDbType.Int, 4, ParameterDirection.Input); }

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Catalog.GetCourses]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetLearningPaths
        /// <summary>
        /// Returns a recordset of learning paths parented by a specific catalog, or belonging to the
        /// catalog root if @idParentCatalog is null.
        /// 
        /// Note: Since we current do not have learning path catalogs, the procedure is going to make
        /// @idParentCatalog always null.
        /// </summary>
        /// <param name="idParentCatalog">parent catalog id</param>
        /// <returns></returns>
        public static DataTable GetLearningPaths(int? idParentCatalog)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                if (idParentCatalog == null)
                { databaseObject.AddParameter("@idParentCatalog", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@idParentCatalog", idParentCatalog, SqlDbType.Int, 4, ParameterDirection.Input); }

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Catalog.GetLearningPaths]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetInstructorLedTrainingModules
        /// <summary>
        /// Returns a recordset of standalone enrollable instructor-led training modules.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetInstructorLedTrainingModules()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Catalog.GetInstructorLedTrainingModules]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCommunities
        /// <summary>
        /// Returns a recordset of user joinable communities (groups).
        /// </summary>
        /// <returns></returns>
        public static DataTable GetCommunities()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Catalog.GetCommunities]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Search
        /// <summary>
        /// Searches the the catalog for specific object type matching the specified parameter.
        /// </summary>
        /// <param name="searchParam">search parameter</param>
        /// <param name="objectType">catalog, course, lesson, learningpath, community, ilt</param>
        /// <returns>DataTable of objects returned by search</returns>
        public static DataTable Search(string searchParam, string objectType)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                // OBJECT TYPE PARAMETER
                databaseObject.AddParameter("@objectType", objectType, SqlDbType.NVarChar, 50, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Catalog.Search]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCatalogIdByShortcode
        /// <summary>
        /// Gets a learning path ID based on the learning path shortcode
        /// </summary>
        /// <param name="shortcode">learning path shortcode</param>
        /// <returns>learning path id</returns>
        public static int GetCatalogIdByShortcode(string shortcode)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@shortcode", shortcode, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCatalog", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Catalog.GetCatalogIdByShortcode]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return Convert.ToInt32(databaseObject.Command.Parameters["@idCatalog"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion

        #region Private Methods
        #region _SaveAvatar
        /// <summary>
        /// Saves catalog's avatar path to database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved user</returns>
        private void _SaveAvatar(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCatalog", this.IdCatalog, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@avatar", this.Avatar, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Catalog.SaveAvatar]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idCatalog">Catalog Id</param>
        private void _Details(int idCatalog)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCatalog", idCatalog, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@title", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@idParent", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@order", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@shortDescription", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@longDescription", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@isPrivate", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@cost", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@calculatedCost", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isClosed", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, null, ParameterDirection.Output);
            databaseObject.AddParameter("@dtModified", null, SqlDbType.DateTime, null, ParameterDirection.Output);
            databaseObject.AddParameter("@costType", null, SqlDbType.Int, null, ParameterDirection.Output);
            databaseObject.AddParameter("@searchTags", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@numberEnrollableCourses", null, SqlDbType.Int, null, ParameterDirection.Output);
            databaseObject.AddParameter("@avatar", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@shortcode", null, SqlDbType.NVarChar, 10, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Catalog.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdCatalog = idCatalog;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.IdParent = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idParent"].Value);
                this.Order = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@order"].Value);
                this.ShortDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@shortDescription"].Value);
                this.LongDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@longDescription"].Value);
                this.IsPrivate = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isPrivate"].Value);
                this.Cost = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@cost"].Value);
                this.CalculatedCost = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@calculatedCost"].Value);
                this.IsClosed = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isClosed"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                this.CostType = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@costType"].Value);
                this.SearchTags = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@searchTags"].Value);
                this.NumberEnrollableCourses = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@numberEnrollableCourses"].Value);
                this.Avatar = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@avatar"].Value);
                this.Shortcode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@shortcode"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a catalog into a collection.
        /// </summary>
        /// <param name="idGroup">group id</param>
        private void _GetPropertiesInLanguages(int idCatalog)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCatalog", idCatalog, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Catalog.GetPropertiesInLanguages]", true);
                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["title"].ToString(),
                            sdr["shortDescription"].ToString(),
                            sdr["longDescription"].ToString(),
                            sdr["searchTags"].ToString()
                            )
                     );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }

    #region CatalogCostType
    public enum CatalogCostType
    {
        Free = 1,
        FixedPrice = 2,
        TotalAllCourse = 3,
        DiscountedTotal = 4
    }
    #endregion
}
