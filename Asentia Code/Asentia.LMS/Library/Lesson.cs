﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    [Serializable]
    public class Lesson
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Lesson()
        {
            this.LanguageSpecificProperties = new ArrayList();
            this.Content = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idLesson">Lesson Id</param>
        public Lesson(int idLesson)
        {
            this.LanguageSpecificProperties = new ArrayList();
            this.Content = new ArrayList();

            this._Details(idLesson);
            this._GetPropertiesInLanguages(idLesson);
            this._GetContent(idLesson);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Lesson.GetGrid]";

        /// <summary>
        /// Lesson Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Course Id.
        /// </summary>
        /// <seealso cref="Course" />
        public int IdCourse;

        /// <summary>
        /// Title.
        /// </summary>
        public string Title;

        /// <summary>
        /// Revision Code.
        /// </summary>
        public string RevCode;

        /// <summary>
        /// Short Description.
        /// </summary>
        public string ShortDescription;

        /// <summary>
        /// Long Description.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string LongDescription;

        /// <summary>
        /// Is Deleted.
        /// </summary>
        public bool IsDeleted;

        /// <summary>
        /// Date Deleted.
        /// </summary>
        public DateTime? DtDeleted;

        /// <summary>
        /// Order.
        /// </summary>
        public int? Order;

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Date Modified.
        /// </summary>
        public DateTime? DtModified;

        /// <summary>
        /// Search Tags.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string SearchTags;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// ArrayList of content.
        /// </summary>
        public ArrayList Content;

        /// <summary>
        /// Is Lesson Optional?
        /// </summary>
        public bool? IsOptional;
        #endregion

        #region LessonContentType ENUM
        public enum LessonContentType
        {
            AdministratorOverride = 0,
            ContentPackage = 1,
            StandupTrainingModule = 2,
            Task = 3,
            OJT = 4,
        }
        #endregion

        #region TaskDocumentType ENUM
        public enum TaskDocumentType
        {
            PDF = 1,
            Word = 2,
            Excel = 3,
            PowerPoint = 4,
            Text = 5,
            Image = 6,
            Zip = 7,
            Mp4 = 8,
        }
        #endregion

        #region LessonCompletionStatus ENUM
        public enum LessonCompletionStatus
        {
            Unknown = 0,
            NotAttempted = 1,
            Completed = 2,
            Incomplete = 3,
        }
        #endregion

        #region ScormCompletionStatus ENUM
        public enum ScormCompletionStatus
        {
            unknown = 0,
            completed = 2,
            incomplete = 3,
        }
        #endregion

        #region LessonSuccessStatus ENUM
        public enum LessonSuccessStatus
        {
            Unknown = 0,
            Passed = 4,
            Failed = 5,
        }
        #endregion

        #region ScormSuccessStatus ENUM
        public enum ScormSuccessStatus
        {
            unknown = 0,
            passed = 4,
            failed = 5,
        }
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Title;
            public string ShortDescription;
            public string LongDescription;
            public string SearchTags;

            public LanguageSpecificProperty(string langString, string title, string shortDescription, string longDescription, string searchTags)
            {
                this.LangString = langString;
                this.Title = title;
                this.ShortDescription = shortDescription;
                this.LongDescription = longDescription;
                this.SearchTags = searchTags;
            }
        }

        /// <summary>
        /// Class that represents lesson content properties.
        /// </summary>
        public class LessonContent
        {
            public int? IdObject;
            public LessonContentType IdContentType;
            public TaskDocumentType? IdTaskDocumentType;
            public string TaskResourcePath;
            public bool? AllowSupervisorsAsProctor;
            public bool? AllowCourseExpertsAsProctor;

            public LessonContent(int? idObject, LessonContentType idContentType, TaskDocumentType? idTaskDocumentType, string taskResourcePath, bool? allowSupervisorsAsProctor, bool? allowCourseExpertsAsProctor)
            {
                this.IdObject = idObject;
                this.IdContentType = idContentType;
                this.IdTaskDocumentType = idTaskDocumentType;
                this.TaskResourcePath = taskResourcePath;
                this.AllowSupervisorsAsProctor = allowSupervisorsAsProctor;
                this.AllowCourseExpertsAsProctor = allowCourseExpertsAsProctor;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves lesson data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves lesson data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this lesson.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="title">lesson title</param>
        /// <param name="shortDescription">lesson short description</param>
        /// <param name="longDescription">lesson long description</param>
        public void SaveLang(string languageString, string title, string shortDescription, string longDescription, string searchTags)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureLessonSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLesson", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@title", title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", shortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@longDescription", longDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", searchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Lesson.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveContent
        /// <summary>
        /// Saves content for this lesson.
        /// </summary>
        public void SaveContent(int? idObject, LessonContentType idContentType, TaskDocumentType? idTaskDocumentType, string taskResourcePath, bool? allowSupervisorsAsProctor, bool? allowCourseExpertsAsProctor)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureLessonSaveContentCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLesson", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

            if (idObject == null)
            { databaseObject.AddParameter("@idObject", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idObject", idObject, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@idContentType", idContentType, SqlDbType.Int, 4, ParameterDirection.Input);

            if (idTaskDocumentType == null)
            { databaseObject.AddParameter("@idTaskDocumentType", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idTaskDocumentType", idTaskDocumentType, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (String.IsNullOrWhiteSpace(taskResourcePath))
            { databaseObject.AddParameter("@taskResourcePath", DBNull.Value, SqlDbType.NVarChar, 255, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@taskResourcePath", taskResourcePath, SqlDbType.NVarChar, 255, ParameterDirection.Input); }

            if (allowSupervisorsAsProctor == null)
            { databaseObject.AddParameter("@allowSupervisorsAsProctor", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@allowSupervisorsAsProctor", allowSupervisorsAsProctor, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (allowCourseExpertsAsProctor == null)
            { databaseObject.AddParameter("@allowCourseExpertsAsProctor", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@allowCourseExpertsAsProctor", allowCourseExpertsAsProctor, SqlDbType.Bit, 1, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[Lesson.SaveContent]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SetContentChangeLessonDataFlags
        /// <summary>
        /// Sets flags for lesson data to reset or prevent content launch when a lesson's content has changed.
        /// </summary>
        public void SetContentChangeLessonDataFlags()
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureLessonSetContentChangeLessonDataFlagsCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLesson", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Lesson.SetContentChangeLessonDataFlags]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region RemoveContent
        /// <summary>
        /// Removes content for this lesson.
        /// </summary>
        public void RemoveContent(LessonContentType idContentType)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureLessonRemoveContentCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLesson", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idContentType", idContentType, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Lesson.RemoveContent]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion        
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Lesson(s).
        /// </summary>
        /// <param name="deletees">DataTable of lessons to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Lessons", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Lesson.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region UpdateOrder
        /// <summary>
        /// Method to update the order of lessons.
        /// </summary>
        /// <param name="lessonOrdering"></param>
        public static void UpdateOrder(DataTable lessonOrdering)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@LessonOrdering", lessonOrdering, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Lesson.UpdateOrder]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves lesson data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved lesson</returns>
        public int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLesson", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idCourse", this.IdCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", this.ShortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@longDescription", this.LongDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", this.SearchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@isOptional", this.IsOptional, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Lesson.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved lesson
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idLesson"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified lesson's properties.
        /// </summary>
        /// <param name="idLesson">Lesson Id</param>
        private void _Details(int idLesson)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLesson", idLesson, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idCourse", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@title", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@revcode", null, SqlDbType.NVarChar, 32, ParameterDirection.Output);
            databaseObject.AddParameter("@shortDescription", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@longDescription", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtModified", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@order", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@searchTags", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@isOptional", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Lesson.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idLesson;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdCourse = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idCourse"].Value);
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.RevCode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@revcode"].Value);
                this.ShortDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@shortDescription"].Value);
                this.LongDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@longDescription"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.Order = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@order"].Value);
                this.SearchTags = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@searchTags"].Value);
                this.IsOptional = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isOptional"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a lesson into a collection.
        /// </summary>
        /// <param name="idLesson">lesson id</param>
        private void _GetPropertiesInLanguages(int idLesson)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLesson", idLesson, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Lesson.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["title"].ToString(),
                            sdr["shortDescription"].ToString(),
                            sdr["longDescription"].ToString(),
                            sdr["searchTags"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetContent
        /// <summary>
        /// Loads the content for a lesson into a collection.
        /// </summary>
        /// <param name="idLesson">lesson id</param>
        private void _GetContent(int idLesson)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLesson", idLesson, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Lesson.GetContent]", true);

                // loop through the returned recordset, instansiate a LessonContent object
                // and add it to the Content ArrayList
                while (sdr.Read())
                {
                    int? idObject;
                    LessonContentType idContentType;
                    TaskDocumentType? idTaskDocumentType;
                    string taskResourcePath;
                    bool? allowSupervisorsAsProctor;
                    bool? allowCourseExpertsAsProctor;

                    if (String.IsNullOrWhiteSpace(sdr["idObject"].ToString()))
                    { idObject = null; }
                    else
                    { idObject = Convert.ToInt32(sdr["idObject"]); }

                    idContentType = (LessonContentType)Convert.ToInt32(sdr["idContentType"]);

                    if (String.IsNullOrWhiteSpace(sdr["idTaskDocumentType"].ToString()))
                    { idTaskDocumentType = null; }
                    else
                    { idTaskDocumentType = (TaskDocumentType)Convert.ToInt32(sdr["idTaskDocumentType"]); }

                    taskResourcePath = sdr["taskResourcePath"].ToString();

                    if (String.IsNullOrWhiteSpace(sdr["allowSupervisorsAsProctor"].ToString()))
                    { allowSupervisorsAsProctor = null; }
                    else
                    { allowSupervisorsAsProctor = Convert.ToBoolean(sdr["allowSupervisorsAsProctor"]); }

                    if (String.IsNullOrWhiteSpace(sdr["allowCourseExpertsAsProctor"].ToString()))
                    { allowCourseExpertsAsProctor = null; }
                    else
                    { allowCourseExpertsAsProctor = Convert.ToBoolean(sdr["allowCourseExpertsAsProctor"]); }

                    this.Content.Add(
                        new LessonContent(
                            idObject,
                            idContentType,
                            idTaskDocumentType,
                            taskResourcePath,
                            allowSupervisorsAsProctor,
                            allowCourseExpertsAsProctor
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}