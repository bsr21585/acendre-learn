﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class GoToMeetingAPI
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// No arguments passed, and no authentication call. 
        /// AuthenticateUser would need to be called after instansiation using this constructor if
        /// API methods are to be called.
        /// </summary>
        public GoToMeetingAPI()
        { ; }

        /// <summary>
        /// Constructor that takes credentials and authenticates the user for API calls.
        /// </summary>
        /// <param name="consumerKey">GTM application key</param>
        /// <param name="organizerLogin">GTM organizer login</param>
        /// <param name="organizerPassword">GTM organizer password</param>
        /// <param name="planName">The name of the GTM plan - used in "conference call setup"</param>
        public GoToMeetingAPI(string consumerKey, string organizerLogin, string organizerPassword, string planName, string consumerSecret)
        {
            this.AuthenticateUser(consumerKey, organizerLogin, organizerPassword, planName, consumerSecret);
        }
        #endregion

        #region Properties
        public GTM_Organizer_Response OrganizerInformation { get { return this._Organizer; } }

        public static readonly string FREE_PLAN_NAME = "Free";
        public static readonly string STARTER_PLAN_NAME = "Starter";
        public static readonly string PRO_PLAN_NAME = "Pro";
        public static readonly string PLUS_PLAN_NAME = "Plus";

        public static readonly int FREE_PLAN_MAXATTENDEES = 3;
        public static readonly int STARTER_PLAN_MAXATTENDEES = 10;
        public static readonly int PRO_PLAN_MAXATTENDEES = 50;
        public static readonly int PLUS_PLAN_MAXATTENDEES = 100;
        #endregion

        #region Private Properties
        private string _AccessToken;
        private Int64 _OrganizerKey = 0;
        private GTM_Organizer_Response _Organizer;
        private bool _IsGTMFree;
        #endregion

        #region Structs
        #region GTM_Organizer_Response
        public struct GTM_Organizer_Response
        {            
            // user personal information
            public string FirstName;
            public string LastName;
            public string Email;            
        }
        #endregion

        #region GTM_Meeting_Response
        public struct GTM_Meeting_Response
        {            
            public Int64 MeetingId;            
            public Int64 SessionId;

            public string HostURL; // specific URL given to the host
            public string JoinURL; // generic URL given to all attendees based on meeting id and a standard URL. ("https://www.gotomeeting.com/join/" + meetingId )

            public string Subject;
            public bool PasswordRequired;
            public float Duration; // in minutes
            public string Status;
            public int NumAttendees;
            public int MaxParticipants;
            public string ConferenceCallInfo;

            public DateTime StartTime;
            public DateTime EndTime;
        }
        #endregion

        #region GTM_Attendee_Response
        public struct GTM_Attendee_Response
        {
            // list of Attendee objects returned
            public List<Attendee> Attendees;            

            // Attendee object
            public class Attendee
            {
                public Int64 MeetingId;
                public string FullName; // returned from the API ##firstname## ##lastname##
                public string Email;
                public DateTime JoinTime;
                public DateTime LeaveTime;                
                public float? Duration; // in minutes (optional since this only appears for historical meetings)
            }
        }
        #endregion
        #endregion

        #region Methods
        #region AuthenticateUser
        /// <summary>
        /// Authenticates the GTM credentials and returns a token to be used with subsequent GTM API calls.
        /// </summary>
        /// <param name="consumerKey">GTM application key</param>
        /// <param name="organizerLogin">GTM organizer login</param>
        /// <param name="organizerPassword">GTM organizer password</param>
        /// <param name="planName">The name of the GTM plan - used in "conference call setup"</param>
        /// <param name="consumerSecret">GTW consumer secret (new for OAuth changes - 10/2019)</param>
        public void AuthenticateUser(string consumerKey, string organizerLogin, string organizerPassword, string planName, string consumerSecret)         
        {
            try
            {
                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // depending on if there is a consumer secret, create a web request for either the new method (consumer secret), or the old method (no consumer secret)
                string requestUrl = String.Empty;
                string authorization = String.Empty;
                string payload = String.Empty;

                if (!String.IsNullOrWhiteSpace(consumerSecret))
                {
                    requestUrl = Config.ApplicationSettings.GoToMeetingAuthenticationURLv2;
                    authorization = consumerKey + ":" + consumerSecret;
                    payload = String.Format("grant_type=password&username={0}&password={1}", organizerLogin, HttpUtility.UrlEncode(organizerPassword));
                }
                else
                { requestUrl = String.Format(Config.ApplicationSettings.GoToMeetingAuthenticationURL + "?grant_type=password&user_id={0}&password={1}&client_id={2}", organizerLogin, HttpUtility.UrlEncode(organizerPassword), consumerKey); }

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);

                if (!String.IsNullOrWhiteSpace(consumerSecret))
                {
                    request.Method = "POST";
                    request.Accept = "application/json";
                    request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(authorization)));
                    request.ContentType = "application/x-www-form-urlencoded";

                    byte[] payloadBytes = Encoding.UTF8.GetBytes(payload);
                    request.ContentLength = payloadBytes.Length;

                    // send the POST payload
                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(payloadBytes, 0, payloadBytes.Length);
                        requestStream.Close();
                    }
                }
                else
                {
                    request.Accept = "application/json";
                    request.ContentType = "application/json";
                }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate organizer object
                this._Organizer = new GTM_Organizer_Response();                    

                // populate organizer information
                this._OrganizerKey = Int64.Parse(jsonTempResponseObject["organizer_key"].ToString());
                this._AccessToken = jsonTempResponseObject["access_token"].ToString();

                this._Organizer.FirstName = jsonTempResponseObject["firstName"].ToString();
                this._Organizer.LastName = jsonTempResponseObject["lastName"].ToString();
                this._Organizer.Email = jsonTempResponseObject["email"].ToString();

                // determine if the organizer is only entitled to GTM free - this affects what kind of "conference call info" the meeting can have
                if (planName == FREE_PLAN_NAME)
                { this._IsGTMFree = true;  }
                else
                { this._IsGTMFree = false; }                
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.AuthenticateUser.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.AuthenticateUser.Exception")); }
        }
        #endregion        

        #region CreateMeeting
        /// <summary>
        /// Creates a meeting.
        /// </summary>
        /// <param name="subject">subject</param>
        /// <param name="startTime">start time in UTC</param>
        /// <param name="endTime">end time in UTC</param>
        /// <param name="passwordRequired">is a password required for the meeting?</param>
        /// <param name="conferenceCallInfo">custom conference call information - not required</param>        
        /// <returns>GTM_Meeting_Response containing the meeting that was just created</returns>
        public GTM_Meeting_Response CreateMeeting(string subject, DateTime startTime, DateTime endTime, bool passwordRequired, string conferenceCallInfo)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "CreateMeeting")); }

            try
            {                
                string requestUrl = Config.ApplicationSettings.GoToMeetingRestAPIURL + "meetings";

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);              

                // set call-in info if custom call info is not set
                // "Free" is PSTN (Toll Free Telephone) and VOIP; other options are: 
                // "PSTN" (PSTN only)
                // "Hybrid" (PSTN and VoIP)
                // "VoIP" (VoIP only)                                                
                if (String.IsNullOrWhiteSpace(conferenceCallInfo))
                {
                    if (this._IsGTMFree) // "Free" can only do VoIP
                    { conferenceCallInfo = "VoIP"; }
                    else
                    { conferenceCallInfo = "Free"; }
                }

                // format the request body
                string requestBody = "{ \"subject\": \"" + HttpUtility.HtmlEncode(subject) + "\", \"starttime\": \"" + startTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\", \"endtime\": \"" + endTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\", \"passwordrequired\": " + passwordRequired.ToString().ToLower() + ", \"conferencecallinfo\": \"" + conferenceCallInfo + "\", \"timezonekey\": \"\", \"meetingtype\": \"scheduled\" }";                
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);                
                request.ContentLength = byteArray.Length;
                
                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {                    
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";
                
                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate meetings object
                GTM_Meeting_Response meeting = new GTM_Meeting_Response();

                // get an array of meetings from the response - there should only be one, but GTM returns an array
                Newtonsoft.Json.Linq.JArray responseMeetingsArray = (Newtonsoft.Json.Linq.JArray)jsonTempResponseObject["Root"];
                
                // if the response meetings array is not null, populate the object with the newly created meeting information; otherwise, throw an exception
                if (responseMeetingsArray != null)
                {                    
                    Newtonsoft.Json.Linq.JObject responseMeeting = (Newtonsoft.Json.Linq.JObject)responseMeetingsArray[0];
                        
                    GTM_Meeting_Response individualMeetingInformation = new GTM_Meeting_Response();
                    individualMeetingInformation = this.GetMeeting(Int64.Parse(responseMeeting["meetingid"].ToString()));

                    meeting.MeetingId = Int64.Parse(responseMeeting["meetingid"].ToString());
                    meeting.ConferenceCallInfo = responseMeeting["conferenceCallInfo"].ToString();
                    meeting.JoinURL = "https://www.gotomeeting.com/join/" + responseMeeting["meetingid"].ToString();
                    meeting.MaxParticipants = int.Parse(responseMeeting["maxParticipants"].ToString());
                    meeting.Subject = subject;
                    meeting.StartTime = startTime;
                    meeting.EndTime = endTime;
                    meeting.PasswordRequired = passwordRequired;

                    // GetMeeting Information
                    meeting.Duration = individualMeetingInformation.Duration;
                    meeting.Status = individualMeetingInformation.Status;                    
                    
                    // GetStartMeetingURL Information
                    meeting.HostURL = this.GetStartMeetingURL(int.Parse(responseMeeting["meetingid"].ToString()));                    
                }
                else
                { throw new AsentiaException(_GlobalResources.MeetingInformationCouldNotBeReadFromTheCreateMeetingResponse); }
                
                return (meeting);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.CreateMeeting.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.CreateMeeting.Exception")); }
        }
        #endregion

        #region UpdateMeeting
        /// <summary>
        /// Updates a meeting.
        /// </summary>
        /// <param name="meetingId">GTM meeting id</param>
        /// <param name="subject">subject</param>
        /// <param name="startTime">start time in UTC</param>
        /// <param name="endTime">end time in UTC</param>
        /// <param name="passwordRequired">is a password required for the meeting?</param>
        /// <param name="conferenceCallInfo">custom conference call information - not required</param>           
        public void UpdateMeeting(Int64 meetingId, string subject, DateTime startTime, DateTime endTime, bool passwordRequired, string conferenceCallInfo)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "UpdateMeeting")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToMeetingRestAPIURL + "meetings/{0}", meetingId.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "PUT";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // set call-in info if custom call info is not set
                // "Free" is PSTN (Toll Free Telephone) and VOIP; other options are: 
                // "PSTN" (PSTN only)
                // "Hybrid" (PSTN and VoIP)
                // "VoIP" (VoIP only)                                                
                if (String.IsNullOrWhiteSpace(conferenceCallInfo))
                {
                    if (this._IsGTMFree) // "Free" can only do VoIP
                    { conferenceCallInfo = "VoIP"; }
                    else
                    { conferenceCallInfo = "Free"; }
                }

                // format the request body
                string requestBody = "{ \"subject\": \"" + HttpUtility.HtmlEncode(subject) + "\", \"starttime\": \"" + startTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\", \"endtime\": \"" + endTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\", \"passwordrequired\": " + passwordRequired.ToString().ToLower() + ", \"conferencecallinfo\": \"" + conferenceCallInfo + "\", \"timezonekey\": \"\", \"meetingtype\": \"scheduled\" }";                                
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }                

                // create temp json object for parsing results - but do nothing with it because we don't need to
                // Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.UpdateMeeting.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.UpdateMeeting.Exception")); }
        }
        #endregion

        #region DeleteMeeting
        /// <summary>
        /// Deletes a meeting.
        /// </summary>
        /// <param name="meetingId">GTM meeting id</param>
        public void DeleteMeeting(Int64 meetingId)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "DeleteMeeting")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToMeetingRestAPIURL + "meetings/{0}", meetingId.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "DELETE";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results - but we will do nothing with them because we don't need to
                // Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.DeleteMeeting.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.DeleteMeeting.Exception")); }
        }
        #endregion        

        #region GetMeeting
        /// <summary>
        /// Gets information about a meeting using the meeting's ID.
        /// </summary>
        /// <param name="meetingId">GTM meeting id</param>
        /// <returns>GTM_Meeting_Response containing the requested meeting's information</returns>
        public GTM_Meeting_Response GetMeeting(Int64 meetingId)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetMeeting")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToMeetingRestAPIURL + "meetings/{0}", meetingId.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);
                
                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";
                
                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate meetings object
                GTM_Meeting_Response meeting = new GTM_Meeting_Response();

                // get an array of meetings from the response - there should only be one, but GTM returns an array
                Newtonsoft.Json.Linq.JArray responseMeetingsArray = (Newtonsoft.Json.Linq.JArray)jsonTempResponseObject["Root"];

                // if the response meetings array is not null, loop through it to populate the object with the meeting information; otherwise, throw an exception
                if (responseMeetingsArray != null)
                {                    
                    Newtonsoft.Json.Linq.JObject responseMeeting = (Newtonsoft.Json.Linq.JObject)responseMeetingsArray[0];

                    meeting.Subject = HttpUtility.HtmlDecode(responseMeeting["subject"].ToString());
                    meeting.MeetingId = Int64.Parse(responseMeeting["meetingId"].ToString());
                    meeting.JoinURL = "https://www.gotomeeting.com/join/" + responseMeeting["meetingId"].ToString();
                    meeting.MaxParticipants = int.Parse(responseMeeting["maxParticipants"].ToString());
                    meeting.Duration = float.Parse(responseMeeting["duration"].ToString());
                    meeting.PasswordRequired = bool.Parse(responseMeeting["passwordRequired"].ToString());
                    meeting.ConferenceCallInfo = responseMeeting["conferenceCallInfo"].ToString();
                    
                    // note that dates get returned from the API, but we do not need to use them, use what we originally have stored from Asentia
                    meeting.StartTime = DateTime.ParseExact(responseMeeting["startTime"].ToString(), "yyyy-MM-dd'T'HH:mm:ss'.+0000'", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    meeting.EndTime = DateTime.ParseExact(responseMeeting["endTime"].ToString(), "yyyy-MM-dd'T'HH:mm:ss'.+0000'", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    
                    meeting.Status = responseMeeting["status"].ToString();
                }
                else
                { throw new AsentiaException(_GlobalResources.MeetingInformationCouldNotBeReadFromTheGetMeetingResponse); }

                return (meeting);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.GetMeeting.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.GetMeeting.Exception")); }
        }
        #endregion

        #region GetStartMeetingURL
        /// <summary>
        /// Gets and returns the "start meeting" (host) URL for a meeting.
        /// </summary>
        /// <param name="meetingId">GTM meeting id</param>
        /// <returns>string</returns>
        public string GetStartMeetingURL(Int64 meetingId)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetStartMeetingURL")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToMeetingRestAPIURL + "meetings/{0}/start", meetingId.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                return (jsonTempResponseObject["hostURL"].ToString());
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.GetStartMeetingURL.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.GetStartMeetingURL.Exception")); }
        }
        #endregion        

        #region GetMeetingAttendees
        /// <summary>
        /// Gets the attendees of a meeting. Note that the meeting must have occurred (not in the future), or this will throw an error.
        /// </summary>
        /// <param name="meetingId">GTM meeting id</param>
        /// <returns>GTM_Attendee_Response containing meeting attendees</returns>
        public GTM_Attendee_Response GetMeetingAttendees(Int64 meetingId)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetMeetingAttendees")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToMeetingRestAPIURL + "meetings/{0}/attendees", meetingId.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate attendees object
                GTM_Attendee_Response attendees = new GTM_Attendee_Response();

                // get an array of attendees from the response
                Newtonsoft.Json.Linq.JArray responseAttendeesArray = (Newtonsoft.Json.Linq.JArray)jsonTempResponseObject["Root"];

                // if the response attendees array is not null, loop through it to populate the object with the attendee information; otherwise, throw an exception
                if (responseAttendeesArray != null)
                {
                    attendees.Attendees = new List<GTM_Attendee_Response.Attendee>();

                    foreach (Newtonsoft.Json.Linq.JObject responseAttendee in responseAttendeesArray)
                    {                        
                        attendees.Attendees.Add(new GTM_Attendee_Response.Attendee()
                        {
                            MeetingId = Int64.Parse(responseAttendee["meetingId"].ToString()),
                            JoinTime = DateTime.Parse(responseAttendee["joinTime"].ToString()),
                            FullName = responseAttendee["attendeeName"].ToString(),
                            LeaveTime = DateTime.Parse(responseAttendee["leaveTime"].ToString()),
                            Email = responseAttendee["attendeeEmail"].ToString(),
                            Duration = float.Parse(responseAttendee["duration"].ToString())
                        });
                    }
                }
                else
                { throw new AsentiaException(_GlobalResources.AttendeeInformationCouldNotBeReadFromTheGetMeetingAttendeesResponse); }

                return (attendees);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.GetMeetingAttendees.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToMeetingAPI.GetMeetingAttendees.Exception")); }
        }
        #endregion        
        #endregion

        #region Private Methods
        #region _HandleErrorResponse
        /// <summary>
        /// Handles errors returned from the GTM API and returns a string describing the error.
        /// </summary>
        /// <param name="response">GTM response</param>
        /// <returns>string</returns>
        private string _HandleErrorResponse(HttpWebResponse response)
        {
            string errorResponseString = String.Empty;

            switch (response.StatusCode)
            {
                case HttpStatusCode.NoContent:          // 204
                case HttpStatusCode.BadRequest:         // 400
                case HttpStatusCode.Unauthorized:       // 401
                case HttpStatusCode.Forbidden:          // 403
                case HttpStatusCode.NotFound:           // 404
                case HttpStatusCode.MethodNotAllowed:   // 405
                case HttpStatusCode.Conflict:           // 409

                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string responseString = reader.ReadToEnd();

                        Newtonsoft.Json.Linq.JObject jsonTempErrorResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseString);

                        if (jsonTempErrorResponseObject["error"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["error"].ToString(); }
                        else if (jsonTempErrorResponseObject["int_err_code"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["int_err_code"].ToString(); }
                        else if (jsonTempErrorResponseObject["int_error_code"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["int_error_code"].ToString(); }
                        else if (jsonTempErrorResponseObject["error_description"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["error_description"].ToString(); }
                        else
                        { errorResponseString = _GlobalResources.AFatalErrorOccurredInTheAPIPleaseContactAnAdministrator + "(Code 1)"; }
                    }

                    break;
                default:
                    errorResponseString = _GlobalResources.AFatalErrorOccurredInTheAPIPleaseContactAnAdministrator + "(Code 2)";
                    break;
            }

            return errorResponseString;
        }
        #endregion
        #endregion
    }
}
