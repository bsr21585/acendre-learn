﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Linq;
using System.Xml.Linq;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class WebExAPI
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// No arguments passed, and no authentication information set. 
        /// If this instructor is called, authentication information would need to be set prior
        /// to making any API method calls.
        /// </summary>
        public WebExAPI()
        { this._MapAsentiaToWebExTimezones(); }

        /// <summary>
        /// Constructor that takes credentials and sets authentication information for API calls.
        /// </summary>
        /// <param name="accountHostname">WebEx hostname</param>
        /// <param name="organizerLogin">WebEx organizer login</param>
        /// <param name="organizerPassword">WebEx organizer password</param>
        public WebExAPI(string accountHostname, string organizerLogin, string organizerPassword)
        {
            this._MapAsentiaToWebExTimezones(); 

            this.AccountHostUrl = Config.ApplicationSettings.WebExRestAPIURL.Replace("##APPLICATION_KEY##", accountHostname);
            this.AccountHostname = accountHostname;
            this.OrganizerLogin = organizerLogin;
            this.OrganizerPassword = organizerPassword;
        }
        #endregion

        #region Properties
        public static readonly string STARTER_PLAN_NAME = "Starter";
        public static readonly string PLUS_PLAN_NAME = "Plus";
        public static readonly string BUSINESS_PLAN_NAME = "Business";
        public static readonly string ENTERPRISE_PLAN_NAME = "Enterprise";
        public static readonly string PREMIUM8_PLAN_NAME = "Premium8";
        public static readonly string PREMIUM25_PLAN_NAME = "Premium25";
        public static readonly string PREMIUM200_PLAN_NAME = "Premium200";

        public static readonly int STARTER_PLAN_MAXATTENDEES = 50;
        public static readonly int PLUS_PLAN_MAXATTENDEES = 100;
        public static readonly int BUSINESS_PLAN_MAXATTENDEES = 200;
        public static readonly int ENTERPRISE_PLAN_MAXATTENDEES = 1000;
        public static readonly int PREMIUM8_PLAN_MAXATTENDEES = 8;
        public static readonly int PREMIUM25_PLAN_MAXATTENDEES = 25;
        public static readonly int PREMIUM200_PLAN_MAXATTENDEES = 200;

        public string AccountHostUrl;
        public string AccountHostname;
        public string OrganizerLogin;
        public string OrganizerPassword;
        #endregion

        #region Private Properties
        #endregion

        #region Structs
        #region WebEx_Meeting_Response
        public struct WebEx_Meeting_Response
        {
            public Int64 MeetingKey;

            public string Subject;
            public string Password;

            public DateTime StartDate;
            public DateTime EndDate;
            public int Duration;
            public string Timezone;

            public int NumberOfAttendees;

            public string HostUrl;
            public string JoinUrl;
        }
        #endregion        

        #region WebEx_Registrant_Response
        public struct WebEx_Registrant_Response
        {
            // list of Registrant objects returned
            public List<Registrant> Registrants;

            // Registrant object
            public class Registrant
            {
                public Int64 MeetingKey;
                public Int64 AttendeeId;

                public string FirstName;
                public string LastName;
                public string Email;                
            }
        }
        #endregion

        #region WebEx_Attendee_Response
        public struct WebEx_Attendee_Response
        {
            // list of Attendee objects returned
            public List<Attendee> Attendees;

            // Attendee object
            public class Attendee
            {
                public Int64 MeetingKey;
                public Int64 AttendeeId;

                public string FirstName;
                public string LastName;
                public string Email;

                public DateTime JoinTime;
                public DateTime LeaveTime;
                public int? Duration; // in minutes
            }
        }
        #endregion
        #endregion

        #region AsentiaToWebExTimezoneDictionary
        private IDictionary<string, int> _AsentiaToWebExTimezones = new Dictionary<string, int>();

        private void _MapAsentiaToWebExTimezones()
        {
            this._AsentiaToWebExTimezones["Dateline Standard Time"] = 0;
            this._AsentiaToWebExTimezones["UTC-11"] = 1;
            this._AsentiaToWebExTimezones["Hawaiian Standard Time"] = 2;
            this._AsentiaToWebExTimezones["Alaskan Standard Time"] = 3;
            this._AsentiaToWebExTimezones["Pacific Standard Time (Mexico)"] = 4;
            this._AsentiaToWebExTimezones["Pacific Standard Time"] = 4;
            this._AsentiaToWebExTimezones["US Mountain Standard Time"] = 5;
            this._AsentiaToWebExTimezones["Mountain Standard Time (Mexico)"] = 5;
            this._AsentiaToWebExTimezones["Mountain Standard Time"] = 6;
            this._AsentiaToWebExTimezones["Central America Standard Time"] = 8;
            this._AsentiaToWebExTimezones["Central Standard Time"] = 7;
            this._AsentiaToWebExTimezones["Central Standard Time (Mexico)"] = 8;
            this._AsentiaToWebExTimezones["Canada Central Standard Time"] = 9;
            this._AsentiaToWebExTimezones["SA Pacific Standard Time"] = 10;
            this._AsentiaToWebExTimezones["Eastern Standard Time"] = 11;
            this._AsentiaToWebExTimezones["US Eastern Standard Time"] = 12;
            this._AsentiaToWebExTimezones["Venezuela Standard Time"] = 14;
            this._AsentiaToWebExTimezones["Paraguay Standard Time"] = 14;
            this._AsentiaToWebExTimezones["Atlantic Standard Time"] = 13;
            this._AsentiaToWebExTimezones["Central Brazilian Standard Time"] = 14;
            this._AsentiaToWebExTimezones["SA Western Standard Time"] = 14;
            this._AsentiaToWebExTimezones["Pacific SA Standard Time"] = 14;
            this._AsentiaToWebExTimezones["Newfoundland Standard Time"] = 15;
            this._AsentiaToWebExTimezones["E. South America Standard Time"] = 16;
            this._AsentiaToWebExTimezones["Argentina Standard Time"] = 17;
            this._AsentiaToWebExTimezones["SA Eastern Standard Time"] = 17;
            this._AsentiaToWebExTimezones["Greenland Standard Time"] = 17;
            this._AsentiaToWebExTimezones["Montevideo Standard Time"] = 17;
            this._AsentiaToWebExTimezones["Bahia Standard Time"] = 17;
            this._AsentiaToWebExTimezones["UTC-02"] = 18;
            this._AsentiaToWebExTimezones["Mid-Atlantic Standard Time"] = 18;
            this._AsentiaToWebExTimezones["Azores Standard Time"] = 19;
            this._AsentiaToWebExTimezones["Cape Verde Standard Time"] = 19;
            this._AsentiaToWebExTimezones["Morocco Standard Time"] = 20;
            this._AsentiaToWebExTimezones["UTC"] = 21;
            this._AsentiaToWebExTimezones["GMT Standard Time"] = 21;
            this._AsentiaToWebExTimezones["Greenwich Standard Time"] = 21;
            this._AsentiaToWebExTimezones["W. Europe Standard Time"] = 22;
            this._AsentiaToWebExTimezones["Central Europe Standard Time"] = 22;
            this._AsentiaToWebExTimezones["Romance Standard Time"] = 23;
            this._AsentiaToWebExTimezones["Central European Standard Time"] = 23;
            this._AsentiaToWebExTimezones["Libya Standard Time"] = 23;
            this._AsentiaToWebExTimezones["W. Central Africa Standard Time"] = 23;
            this._AsentiaToWebExTimezones["Namibia Standard Time"] = 23;
            this._AsentiaToWebExTimezones["GTB Standard Time"] = 26;
            this._AsentiaToWebExTimezones["Middle East Standard Time"] = 26;
            this._AsentiaToWebExTimezones["Egypt Standard Time"] = 28;
            this._AsentiaToWebExTimezones["Syria Standard Time"] = 29;
            this._AsentiaToWebExTimezones["E. Europe Standard Time"] = 29;
            this._AsentiaToWebExTimezones["South Africa Standard Time"] = 29;
            this._AsentiaToWebExTimezones["FLE Standard Time"] = 30;
            this._AsentiaToWebExTimezones["Turkey Standard Time"] = 31;
            this._AsentiaToWebExTimezones["Israel Standard Time"] = 31;
            this._AsentiaToWebExTimezones["Jordan Standard Time"] = 32;
            this._AsentiaToWebExTimezones["Arabic Standard Time"] = 32;
            this._AsentiaToWebExTimezones["Kaliningrad Standard Time"] = 33;
            this._AsentiaToWebExTimezones["Arab Standard Time"] = 32;
            this._AsentiaToWebExTimezones["E. Africa Standard Time"] = 34;
            this._AsentiaToWebExTimezones["Iran Standard Time"] = 35;
            this._AsentiaToWebExTimezones["Arabian Standard Time"] = 36;
            this._AsentiaToWebExTimezones["Azerbaijan Standard Time"] = 37;
            this._AsentiaToWebExTimezones["Russian Standard Time"] = 37;
            this._AsentiaToWebExTimezones["Mauritius Standard Time"] = 37;
            this._AsentiaToWebExTimezones["Georgian Standard Time"] = 37;
            this._AsentiaToWebExTimezones["Caucasus Standard Time"] = 37;
            this._AsentiaToWebExTimezones["Afghanistan Standard Time"] = 38;
            this._AsentiaToWebExTimezones["West Asia Standard Time"] = 39;
            this._AsentiaToWebExTimezones["Pakistan Standard Time"] = 40;
            this._AsentiaToWebExTimezones["India Standard Time"] = 41;
            this._AsentiaToWebExTimezones["Sri Lanka Standard Time"] = 41;
            this._AsentiaToWebExTimezones["Nepal Standard Time"] = 41;
            this._AsentiaToWebExTimezones["Central Asia Standard Time"] = 42;
            this._AsentiaToWebExTimezones["Bangladesh Standard Time"] = 42;
            this._AsentiaToWebExTimezones["Ekaterinburg Standard Time"] = 43;
            this._AsentiaToWebExTimezones["Myanmar Standard Time"] = 44;
            this._AsentiaToWebExTimezones["SE Asia Standard Time"] = 44;
            this._AsentiaToWebExTimezones["N. Central Asia Standard Time"] = 44;
            this._AsentiaToWebExTimezones["China Standard Time"] = 45;
            this._AsentiaToWebExTimezones["North Asia Standard Time"] = 45;
            this._AsentiaToWebExTimezones["Singapore Standard Time"] = 47;
            this._AsentiaToWebExTimezones["W. Australia Standard Time"] = 46;
            this._AsentiaToWebExTimezones["Taipei Standard Time"] = 48;
            this._AsentiaToWebExTimezones["Ulaanbaatar Standard Time"] = 45;
            this._AsentiaToWebExTimezones["North Asia East Standard Time"] = 51;
            this._AsentiaToWebExTimezones["Tokyo Standard Time"] = 49;
            this._AsentiaToWebExTimezones["Korea Standard Time"] = 50;
            this._AsentiaToWebExTimezones["Cen. Australia Standard Time"] = 52;
            this._AsentiaToWebExTimezones["AUS Central Standard Time"] = 53;
            this._AsentiaToWebExTimezones["E. Australia Standard Time"] = 54;
            this._AsentiaToWebExTimezones["AUS Eastern Standard Time"] = 55;
            this._AsentiaToWebExTimezones["West Pacific Standard Time"] = 56;
            this._AsentiaToWebExTimezones["Tasmania Standard Time"] = 57;
            this._AsentiaToWebExTimezones["Yakutsk Standard Time"] = 58;
            this._AsentiaToWebExTimezones["Central Pacific Standard Time"] = 59;
            this._AsentiaToWebExTimezones["Vladivostok Standard Time"] = 59;
            this._AsentiaToWebExTimezones["New Zealand Standard Time"] = 60;
            this._AsentiaToWebExTimezones["UTC+12"] = 60;
            this._AsentiaToWebExTimezones["Fiji Standard Time"] = 61;
            this._AsentiaToWebExTimezones["Magadan Standard Time"] = 60;
            this._AsentiaToWebExTimezones["Kamchatka Standard Time"] = 60;
            this._AsentiaToWebExTimezones["Tonga Standard Time"] = 0;
            this._AsentiaToWebExTimezones["Samoa Standard Time"] = 0;

        }
        #endregion

        #region Methods
        #region CreateMeeting
        /// <summary>
        /// Creates a meeting.
        /// </summary>
        /// <param name="subject">subject</param>
        /// <param name="password">password</param>
        /// <param name="startDate">start date in local time - webex takes the time as is along with a timezone</param>
        /// <param name="endDate">end date in local time - webex takes the time as is along with a timezone</param>
        /// <param name="timezoneDotNetName">the timezone dotNetName - gets mapped to a timezone id for webex</param>
        /// <param name="maxUsers">the maximum number of users that can attend the meeting</param>
        /// <returns>WebEx_Meeting_Response containing the meeting that was just created</returns>
        public WebEx_Meeting_Response CreateMeeting(string subject, string password, DateTime startDate, DateTime endDate, string timezoneDotNetName, int maxUsers)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "CreateMeeting")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // get the meeting's duration in minutes by subtracting the difference between start and end                
                double meetingDurationInMinutes = endDate.Subtract(startDate).TotalMinutes;

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");                
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.meeting.CreateMeeting\">");
                sb.AppendLine("         <accessControl>");
                sb.AppendLine("             <meetingPassword>" + password + "</meetingPassword>");
                sb.AppendLine("         </accessControl>");
                sb.AppendLine("         <metaData>");
                sb.AppendLine("             <confName>" + subject + "</confName>");
                sb.AppendLine("         </metaData>");
                sb.AppendLine("         <participants>");
                sb.AppendLine("             <maxUserNumber>" + maxUsers.ToString() + "</maxUserNumber>");
                sb.AppendLine("         </participants>");
                sb.AppendLine("         <schedule>");
                sb.AppendLine("             <startDate>" + startDate.ToString("MM/dd/yyyy HH:mm:ss") + "</startDate>");
                sb.AppendLine("             <duration>" + Convert.ToInt32(meetingDurationInMinutes).ToString() + "</duration>");
                sb.AppendLine("             <timeZoneID>" + this._AsentiaToWebExTimezones[timezoneDotNetName].ToString() + "</timeZoneID>");
                sb.AppendLine("         </schedule>");
                sb.AppendLine("         <telephony>");
                //sb.AppendLine("             <enableTSP>true</enableTSP>"); DEPRECATED
                sb.AppendLine("             <telephonySupport>CALLIN</telephonySupport>");             
                sb.AppendLine("         </telephony>");
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");                

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }
                
                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);
                
                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();
                XNamespace meet = responseDocument.Root.GetNamespaceOfPrefix("meet").ToString();
                
                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;

                WebEx_Meeting_Response meeting = new WebEx_Meeting_Response();                    

                if (responseResult == "SUCCESS")
                {
                    string meetingKey = responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(meet + "meetingkey").First().Value;
                    
                    meeting.MeetingKey = Convert.ToInt64(meetingKey);
                    meeting.Subject = subject;
                    meeting.Password = password;
                    meeting.StartDate = startDate;
                    meeting.EndDate = endDate;
                    meeting.Duration = Convert.ToInt32(meetingDurationInMinutes);
                    meeting.Timezone = timezoneDotNetName;
                    meeting.NumberOfAttendees = maxUsers;

                    // get join and host urls
                    meeting.HostUrl = this.GetMeetingHostURL(Convert.ToInt64(meetingKey));
                    meeting.JoinUrl = this.GetMeetingJoinURL(Convert.ToInt64(meetingKey));
                }
                else
                { 
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }

                return (meeting);
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.CreateMeeting.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.CreateMeeting.Exception")); }
        }
        #endregion
        
        #region UpdateMeeting
        /// <summary>
        /// Updates a meeting.
        /// </summary>
        /// <param name="meetingKey">WebEx meeting id</param>
        /// <param name="subject">subject</param>
        /// <param name="password">password</param>
        /// <param name="startDate">start date in local time - webex takes the time as is along with a timezone</param>
        /// <param name="endDate">end date in local time - webex takes the time as is along with a timezone</param>
        /// <param name="timezoneDotNetName">the timezone dotNetName - gets mapped to a timezone id for webex</param>
        /// <param name="maxUsers">the maximum number of users that can attend the meeting</param>
        public void UpdateMeeting(Int64 meetingKey, string subject, string password, DateTime startDate, DateTime endDate, string timezoneDotNetName, int maxUsers)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "UpdateMeeting")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // get the meeting's duration in minutes by subtracting the difference between start and end                
                double meetingDurationInMinutes = endDate.Subtract(startDate).TotalMinutes;

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.meeting.SetMeeting\">");
                sb.AppendLine("         <accessControl>");
                sb.AppendLine("             <meetingPassword>" + password + "</meetingPassword>");
                sb.AppendLine("         </accessControl>");
                sb.AppendLine("         <metaData>");
                sb.AppendLine("             <confName>" + subject + "</confName>");
                sb.AppendLine("         </metaData>");
                sb.AppendLine("         <participants>");
                sb.AppendLine("             <maxUserNumber>" + maxUsers.ToString() + "</maxUserNumber>");
                sb.AppendLine("         </participants>");
                sb.AppendLine("         <schedule>");
                sb.AppendLine("             <startDate>" + startDate.ToString("MM/dd/yyyy HH:mm:ss") + "</startDate>");
                sb.AppendLine("             <duration>" + Convert.ToInt32(meetingDurationInMinutes).ToString() + "</duration>");
                sb.AppendLine("             <timeZoneID>" + this._AsentiaToWebExTimezones[timezoneDotNetName].ToString() + "</timeZoneID>");
                sb.AppendLine("         </schedule>");
                sb.AppendLine("         <telephony>");
                sb.AppendLine("             <telephonySupport>CALLIN</telephonySupport>");
                sb.AppendLine("         </telephony>");
                sb.AppendLine("         <meetingkey>" + meetingKey.ToString() + "</meetingkey>");
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);

                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();

                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;

                if (responseResult == "SUCCESS")
                {
                    // just return, do nothing else with the response
                    return;
                }
                else
                {
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.UpdateMeeting.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.UpdateMeeting.Exception")); }
        }
        #endregion

        #region DeleteMeeting
        /// <summary>
        /// Deletes a meeting.
        /// </summary>
        /// <param name="meetingKey">WebEx meeting id</param>
        public void DeleteMeeting(Int64 meetingKey)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "DeleteMeeting")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.meeting.DelMeeting\">");              
                sb.AppendLine("         <meetingKey>" + meetingKey.ToString() + "</meetingKey>");
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);

                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();

                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;

                if (responseResult == "SUCCESS")
                {
                    // just return, do nothing else with the response
                    return;
                }
                else
                {
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.DeleteMeeting.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.DeleteMeeting.Exception")); }
        }
        #endregion

        #region GetMeeting
        /// <summary>
        /// Gets information about a meeting using the meeting's ID.
        /// </summary>
        /// <param name="meetingKey">WebEx meeting id</param>
        /// <returns>WebEx_Meeting_Response containing the requisted meeting's information</returns>
        public WebEx_Meeting_Response GetMeeting(Int64 meetingKey)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetMeeting")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GetMeeting\">");
                sb.AppendLine("         <meetingKey>" + meetingKey.ToString() + "</meetingKey>");                
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);

                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();
                XNamespace meet = responseDocument.Root.GetNamespaceOfPrefix("meet").ToString();

                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;

                WebEx_Meeting_Response meeting = new WebEx_Meeting_Response();

                if (responseResult == "SUCCESS")
                {                    
                    meeting.MeetingKey = Convert.ToInt64(meetingKey);
                    meeting.Subject = responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(meet + "metaData").First().Descendants(meet + "confName").First().Value;
                    meeting.Password = responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(meet + "accessControl").First().Descendants(meet + "meetingPassword").First().Value;
                    meeting.StartDate = Convert.ToDateTime(responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(meet + "schedule").First().Descendants(meet + "startDate").First().Value);
                    meeting.Duration = Convert.ToInt32(responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(meet + "schedule").First().Descendants(meet + "duration").First().Value);
                    meeting.EndDate = meeting.StartDate.AddMinutes(meeting.Duration);
                    meeting.NumberOfAttendees = Convert.ToInt32(responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(meet + "participants").First().Descendants(meet + "maxUserNumber").First().Value);

                    // map the timezone back to a Asentia (dot net) timezone
                    int webExTimezoneId = Convert.ToInt32(responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(meet + "schedule").First().Descendants(meet + "timeZoneID").First().Value);

                    foreach (string key in this._AsentiaToWebExTimezones.Keys)
                    {
                        if (this._AsentiaToWebExTimezones[key] == webExTimezoneId)
                        { 
                            meeting.Timezone = key;
                            break;
                        }
                    }

                    // get join and host urls
                    meeting.HostUrl = this.GetMeetingHostURL(meetingKey);
                    meeting.JoinUrl = this.GetMeetingJoinURL(meetingKey);
                }
                else
                {
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }

                return (meeting);
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeeting.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeeting.Exception")); }
        }
        #endregion

        #region GetMeetingHostURL
        /// <summary>
        /// Gets and returns the host URL for a meeting.
        /// </summary>
        /// <param name="meetingKey">WebEx meeting id</param>
        /// <returns>string</returns>
        public string GetMeetingHostURL(Int64 meetingKey)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetMeetingHostURL")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GethosturlMeeting\">");
                sb.AppendLine("         <sessionKey>" + meetingKey.ToString() + "</sessionKey>");
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);

                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();
                XNamespace meet = responseDocument.Root.GetNamespaceOfPrefix("meet").ToString();

                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;

                if (responseResult == "SUCCESS")
                { return responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(meet + "hostMeetingURL").First().Value; }
                else
                {
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeetingHostURL.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeetingHostURL.Exception")); }
        }
        #endregion

        #region GetMeetingJoinURL
        /// <summary>
        /// Gets and returns the join URL for a meeting.
        /// </summary>
        /// <param name="meetingKey">WebEx meeting id</param>
        /// <returns>string</returns>
        public string GetMeetingJoinURL(Int64 meetingKey)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetMeetingJoinURL")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GetjoinurlMeeting\">");
                sb.AppendLine("         <sessionKey>" + meetingKey.ToString() + "</sessionKey>");
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);

                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();
                XNamespace meet = responseDocument.Root.GetNamespaceOfPrefix("meet").ToString();

                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;

                if (responseResult == "SUCCESS")
                { return responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(meet + "joinMeetingURL").First().Value; }
                else
                {
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeetingJoinURL.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeetingJoinURL.Exception")); }
        }
        #endregion
        
        #region CreateMeetingRegistrant
        /// <summary>
        /// Creates a meeting registrant.
        /// </summary>
        /// <param name="meetingKey">WebEx meeting id</param>
        /// <param name="firstName">registrant first name</param>
        /// <param name="lastName">registrant last name</param>
        /// <param name="email">registrant email address</param>
        /// <returns>WebEx_Registrant_Response containing the meeting registrant's information</returns>
        public WebEx_Registrant_Response.Registrant CreateMeetingRegistrant(Int64 meetingKey, string firstName, string lastName, string email)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "CreateMeetingRegistrant")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.attendee.CreateMeetingAttendee\">");                
                sb.AppendLine("             <person>");
                sb.AppendLine("                 <name>" + firstName + " " + lastName + "</name>");
                sb.AppendLine("                 <email>" + email.ToLower() + "</email>");
                sb.AppendLine("                 <type>VISITOR</type>");
                sb.AppendLine("             </person>");
                sb.AppendLine("             <joinStatus>INVITE</joinStatus>");
                sb.AppendLine("             <role>ATTENDEE</role>");
                sb.AppendLine("             <emailInvitations>true</emailInvitations>");
                sb.AppendLine("             <sessionKey>" + meetingKey.ToString() + "</sessionKey>");
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }
                
                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);

                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();
                XNamespace att = responseDocument.Root.GetNamespaceOfPrefix("att").ToString();

                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;
                
                WebEx_Registrant_Response.Registrant registrant = new WebEx_Registrant_Response.Registrant();

                if (responseResult == "SUCCESS")
                {
                    string attendeeId = responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(att + "attendeeId").First().Value;

                    registrant.MeetingKey = meetingKey;
                    registrant.AttendeeId = Convert.ToInt64(responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(att + "attendeeId").First().Value);
                    registrant.FirstName = firstName;
                    registrant.LastName = lastName;
                    registrant.Email = email;
                }
                else
                {
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }

                return registrant;
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.CreateMeetingRegistrant.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.CreateMeetingRegistrant.Exception")); }
        }
        #endregion

        #region DeleteMeetingRegistrant
        /// <summary>
        /// Deletes a meeting registrant.
        /// </summary>
        /// <param name="registrantKey">WebEx attendee id</param>
        public void DeleteMeetingRegistrant(Int64 registrantKey)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "DeleteMeetingRegistrant")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.attendee.DelMeetingAttendee\">");
                sb.AppendLine("         <attendeeID>" + registrantKey.ToString() + "</attendeeID>");
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);

                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();

                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;

                if (responseResult == "SUCCESS")
                {
                    // just return, do nothing else with the response
                    return;
                }
                else
                {
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.DeleteMeetingRegistrant.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.DeleteMeetingRegistrant.Exception")); }
        }
        #endregion

        #region GetMeetingRegistrant
        /// <summary>
        /// Gets a registrant for a meeting.
        /// </summary>
        /// <param name="registrantKey">WebEx attendee id</param>
        /// <param name="meetingKey">WebEx meeting id</param>
        /// <returns>WebEx_Registrant_Response.Registrant containing the registrant's information</returns>
        public WebEx_Registrant_Response.Registrant GetMeetingRegistrant(Int64 registrantKey, Int64 meetingKey)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetMeetingRegistrant")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.attendee.LstMeetingAttendee\">");
                sb.AppendLine("         <meetingKey>" + meetingKey.ToString() + "</meetingKey>");
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);

                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();
                XNamespace att = responseDocument.Root.GetNamespaceOfPrefix("att").ToString();
                XNamespace com = responseDocument.Root.GetNamespaceOfPrefix("com").ToString();

                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;

                WebEx_Registrant_Response.Registrant registrant = new WebEx_Registrant_Response.Registrant();

                if (responseResult == "SUCCESS")
                {
                    foreach (XElement registrantNode in responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(att + "attendee"))
                    {
                        Int64 attendeeId = Convert.ToInt64(registrantNode.Descendants(att + "attendeeId").First().Value);

                        if (attendeeId == registrantKey)
                        {
                            registrant.AttendeeId = attendeeId;
                            registrant.MeetingKey = meetingKey;
                            registrant.FirstName = registrantNode.Descendants(att + "person").First().Descendants(com + "firstName").First().Value;
                            registrant.LastName = registrantNode.Descendants(att + "person").First().Descendants(com + "lastName").First().Value;
                            registrant.Email = registrantNode.Descendants(att + "person").First().Descendants(com + "email").First().Value;

                            break;
                        }
                    }
                }
                else
                {
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }

                if (registrant.AttendeeId <= 0)
                { throw new AsentiaException(_GlobalResources.RegistrantNotFound); }

                return (registrant);
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeetingRegistrant.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeetingRegistrant.Exception")); }
        }
        #endregion

        #region GetMeetingRegistrants
        /// <summary>
        /// Gets a list of registrants for a meeting.
        /// </summary>
        /// <param name="meetingKey">WebEx meeting id</param>
        /// <returns>WebEx_Registrant_Response containing the a list of registrants for the meeting</returns>
        public WebEx_Registrant_Response GetMeetingRegistrants(Int64 meetingKey)
        {
            // if there is no authentication information, throw an exception because we cannot build the authentication headers
            if (String.IsNullOrWhiteSpace(this.AccountHostname) || String.IsNullOrWhiteSpace(this.OrganizerLogin) || String.IsNullOrWhiteSpace(this.OrganizerPassword) || String.IsNullOrWhiteSpace(this.AccountHostUrl))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetMeetingRegistrants")); }

            try
            {
                string requestUrl = this.AccountHostUrl;

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "text/xml";

                // format the request body               
                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.AppendLine(this._BuildSecurityContextHeader());
                sb.AppendLine(" <body>");
                sb.AppendLine("     <bodyContent xsi:type=\"java:com.webex.service.binding.attendee.LstMeetingAttendee\">");
                sb.AppendLine("         <meetingKey>" + meetingKey.ToString() + "</meetingKey>");
                sb.AppendLine("     </bodyContent>");
                sb.AppendLine(" </body>");
                sb.AppendLine("</serv:message>");

                string requestBody = sb.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // load the response as an XDocument
                XDocument responseDocument = XDocument.Parse(responseData);

                XNamespace serv = responseDocument.Root.GetNamespaceOfPrefix("serv").ToString();
                XNamespace att = responseDocument.Root.GetNamespaceOfPrefix("att").ToString();
                XNamespace com = responseDocument.Root.GetNamespaceOfPrefix("com").ToString();

                string responseResult = responseDocument.Descendants(serv + "header").First()
                                                        .Descendants(serv + "response").First()
                                                        .Descendants(serv + "result").First().Value;

                WebEx_Registrant_Response registrants = new WebEx_Registrant_Response();

                if (responseResult == "SUCCESS")
                {
                    registrants.Registrants = new List<WebEx_Registrant_Response.Registrant>();
                    
                    foreach (XElement registrantNode in responseDocument.Descendants(serv + "body").First().Descendants(serv + "bodyContent").First().Descendants(att + "attendee"))
                    {
                        WebEx_Registrant_Response.Registrant registrant = new WebEx_Registrant_Response.Registrant();

                        registrant.AttendeeId = Convert.ToInt64(registrantNode.Descendants(att + "attendeeId").First().Value);
                        registrant.MeetingKey = meetingKey;
                        registrant.FirstName = registrantNode.Descendants(att + "person").First().Descendants(com + "firstName").First().Value;
                        registrant.LastName = registrantNode.Descendants(att + "person").First().Descendants(com + "lastName").First().Value;
                        registrant.Email = registrantNode.Descendants(att + "person").First().Descendants(com + "email").First().Value;

                        registrants.Registrants.Add(registrant);
                    }
                }
                else
                {
                    // get the error from the response and throw it
                    string errorResponse = responseDocument.Descendants(serv + "header").First()
                                                           .Descendants(serv + "response").First()
                                                           .Descendants(serv + "reason").First().Value;

                    throw new AsentiaException(errorResponse);
                }

                return (registrants);
            }
            catch (WebException exp)
            {
                throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeetingRegistrants.WebException"));
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "WebExAPI.GetMeetingRegistrants.Exception")); }
        }
        #endregion     
        #endregion

        #region Private Methods
        #region _BuildSecurityContextHeader
        /// <summary>
        /// Builds the <header> and <securityContext> xml elements required for all API calls.
        /// </summary>        
        /// <returns>string containing the header and security context XML for the API call</returns>
        private string _BuildSecurityContextHeader()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" <header>");
            sb.AppendLine("     <securityContext>");
            sb.AppendLine("         <siteName>" + this.AccountHostname + "</siteName>");
            sb.AppendLine("         <webExID>" + this.OrganizerLogin + "</webExID>");
            sb.AppendLine("         <password>" + this.OrganizerPassword + "</password>");
            sb.AppendLine("     </securityContext>");
            sb.AppendLine(" </header>");            
            
            // return the string
            return sb.ToString();
        }
        #endregion
        #endregion
    }
}
