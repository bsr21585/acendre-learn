﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    /*
    public class SCORMPackage
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public SCORMPackage()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idSCORMPackage">SCORMPackage Id</param>
        public SCORMPackage(int idSCORMPackage)
        {
            _Details(idSCORMPackage);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[SCORMPackage.GetGrid]";

        /// <summary>
        /// SCORMPackage Id.
        /// </summary>
        public int IdSCORMPackage = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        public int IdSite;

        /// <summary>
        /// Name.
        /// </summary>
        public string Name;

        /// <summary>
        /// Path.
        /// </summary>
        public string Path;

        /// <summary>
        /// Kb.
        /// </summary>
        public int Kb;

        /// <summary>
        /// SCORMPackageType Id.
        /// </summary>
        public int IdSCORMPackageType;

        /// <summary>
        /// IMS Manifest.
        /// </summary>
        public string ImsManifest;

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime? DtCreated;

        /// <summary>
        /// Date Modified.
        /// </summary>
        public DateTime? DtModified;

        // insert more properties
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves object data to the database.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idCaller">The User Id of the user calling this method.</param>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            //Return Code
            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            //Input
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            databaseObject.AddParameter("@idSCORMPackage", this.IdSCORMPackage, SqlDbType.Int, 4, ParameterDirection.Output);
            //databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@path", this.Path, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@kb", this.Kb, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSCORMPackageType", this.IdSCORMPackageType, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@imsManifest", this.ImsManifest, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtCreated", this.DtCreated, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@dtModified", this.DtModified, SqlDbType.DateTime, 8, ParameterDirection.Input);
            // add additional parameters

            try
            {
                databaseObject.ExecuteNonQuery("[SCORMPackage.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdSCORMPackage = Convert.ToInt32(databaseObject.Command.Parameters["@idSCORMPackage"].Value);

                return this.IdSCORMPackage;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            databaseObject.AddParameter("@SCORMPackages", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[SCORMPackage.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idSCORMPackage">SCORMPackage Id</param>
        private void _Details(int idSCORMPackage)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            databaseObject.AddParameter("@idSCORMPackage", idSCORMPackage, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@path", this.Path, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@kb", this.Kb, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idSCORMPackageType", this.IdSCORMPackageType, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@imsManifest", this.ImsManifest, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", this.DtCreated, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtModified", this.DtModified, SqlDbType.DateTime, 8, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[SCORMPackage.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdSCORMPackage = idSCORMPackage;
                this.IdSite = Convert.ToInt32(databaseObject.Command.Parameters["@idSite"].Value);
                this.Name = databaseObject.Command.Parameters["@name"].Value.ToString();
                this.Path = databaseObject.Command.Parameters["@path"].Value.ToString();
                this.Kb = Convert.ToInt32(databaseObject.Command.Parameters["@kb"].Value);
                this.IdSCORMPackageType = Convert.ToInt32(databaseObject.Command.Parameters["@idSCORMPackageType"].Value);
                this.ImsManifest = databaseObject.Command.Parameters["@imsManifest"].Value.ToString();
                this.DtCreated = databaseObject.Command.Parameters["@dtCreated"].Value == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = databaseObject.Command.Parameters["@dtModified"].Value == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                // add additional property value assignments
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
    */
}

