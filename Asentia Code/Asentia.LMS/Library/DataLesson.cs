﻿using System;
using System.Data;
using System.IO;
using System.Web;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    [Serializable]
    public class DataLesson
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DataLesson()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idLesson">Lesson Id</param>
        public DataLesson(int idDataLesson)
        {
            this._Details(idDataLesson);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[DataLesson.GetGrid]";

        /// <summary>
        /// Lesson Data Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Enrollment Id.
        /// </summary>
        /// <seealso cref="Enrollment" />
        public int IdEnrollment;

        /// <summary>
        /// Lesson Id.
        /// </summary>
        /// <seealso cref="Lesson" />
        public int IdLesson;

        /// <summary>
        /// Timezone Id.
        /// </summary>
        public int IdTimezone;

        /// <summary>
        /// Title.
        /// </summary>
        public string Title;

        /// <summary>
        /// Revision Code.
        /// </summary>
        public string RevCode;

        /// <summary>
        /// Order
        /// </summary>
        public int Order;

        /// <summary>
        /// Date Completed.
        /// </summary>
        public DateTime? DtCompleted;

        /// <summary>
        /// Content Type Committed To.
        /// </summary>
        public Lesson.LessonContentType? ContentTypeCommittedTo;
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves lesson data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves lesson data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion
        #endregion

        #region Static Methods
        #region CommitToContentType
        /// <summary>
        /// Sets the lesson data's contentTypeCommittedTo flag. Set when a learner launches/interacts with a specific content type for the lesson.
        /// </summary>
        /// <param name="idDataLesson"></param>
        /// <param name="contentType"></param>
        public static void CommitToContentType(int idDataLesson, Lesson.LessonContentType? contentType)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);

            if (contentType == null)
            { databaseObject.AddParameter("@contentType", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@contentType", Convert.ToInt32(contentType), SqlDbType.Int, 4, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[DataLesson.CommitToContentType]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region InitializeSCOData
        /// <summary>
        /// Creates a record in tblData-SCO for the lesson data if it doesn't already exist.
        /// </summary>
        /// <param name="idDataLesson"></param>
        public static int InitializeSCOData(int idDataLesson)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataSco", 0, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            try
            {
                databaseObject.ExecuteNonQuery("[DataLesson.InitializeSCOData]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved course
                int id = Convert.ToInt32(databaseObject.Command.Parameters["@idDataSco"].Value);

                // return
                return id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ClearTask
        /// <summary>
        /// Clears uploaded task information for a learner's task.
        /// Used when a learner decides to withdrawl their previously uploaded but ungraded task.
        /// </summary>
        /// <param name="idDataLesson"></param>
        public static void ClearTask(int idDataLesson)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[DataLesson.ClearTask]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveTask
        /// <summary>
        /// Saves task information for a learner's task.
        /// Used when a learner uploads their task.
        /// </summary>
        /// <param name="idDataLesson"></param>
        /// <param name="uploadedTaskFilename"></param>
        public static void SaveTask(int idDataLesson, string uploadedTaskFilename)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@uploadedTaskFilename", uploadedTaskFilename, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[DataLesson.SaveTask]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ProctorTask
        /// <summary>
        /// Updates completion, success, and score for a learner's task.
        /// Used when a proctor grades a learner's task.
        /// </summary>
        /// <param name="idEnrollment"></param>
        /// <param name="idDataLesson"></param>
        /// <param name="idDataTask"></param>
        /// <param name="completionStatus"></param>
        /// <param name="successStatus"></param>
        /// <param name="score"></param>
        public static void ProctorTask(int idEnrollment, int idDataLesson, int idDataTask, int completionStatus, int successStatus, int? score)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataTask", idDataTask, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@completionStatus", completionStatus, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@successStatus", successStatus, SqlDbType.Int, 4, ParameterDirection.Input);

            if (score == null)
            { databaseObject.AddParameter("@score", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@score", score, SqlDbType.Int, 4, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[DataLesson.ProctorTask]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region OverrideLessonCompletion
        /// <summary>
        /// Marks completion timestamp for a lesson data record as an "administrator override."
        /// Used in the event a learner is not committed to any lesson content type and an administrator
        /// wants to make the lesson complete for the learner.
        /// </summary>
        /// <param name="idEnrollment"></param>
        /// <param name="idDataLesson"></param>
        /// <param name="completionStatus"></param>
        /// <param name="timestamp"></param>
        public static void OverrideLessonCompletion(int idEnrollment, int idDataLesson, int completionStatus, int successStatus, double? scoreScaled, DateTime? timestamp)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@completionStatus", completionStatus, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@successStatus", successStatus, SqlDbType.Int, 4, ParameterDirection.Input);

            if (scoreScaled == null)
            { databaseObject.AddParameter("@scoreScaled", DBNull.Value, SqlDbType.Float, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@scoreScaled", scoreScaled, SqlDbType.Float, 8, ParameterDirection.Input); }

            if (timestamp == null)
            { databaseObject.AddParameter("@timestamp", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@timestamp", timestamp, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[DataLesson.OverrideLessonCompletion]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Reset
        /// <summary>
        /// Resets lesson data.
        /// </summary>
        /// <param name="idEnrollment">id of the enrollment</param>
        /// <param name="idDataLesson">id of the lesson data to reset</param>
        public static void Reset(int idEnrollment, int idDataLesson)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[DataLesson.Reset]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DeleteSCOLogDataFiles
        /// <summary>
        /// Deletes SCO data log files for a user and lesson data id. Used when resetting lesson data.
        /// </summary>
        /// <param name="idUser">id of the user</param>
        /// <param name="idDataLesson">id of the lesson data to reset</param>
        public static void DeleteSCOLogDataFiles(int idUser, int idDataLesson)
        {
            try
            {
                if (Directory.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + idUser.ToString() + "/" + idDataLesson.ToString())))
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + idUser.ToString() + "/" + idDataLesson.ToString()));

                    // delete the files
                    foreach (FileInfo file in directoryInfo.GetFiles())
                    { file.Delete(); }

                    Directory.Delete(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + idUser.ToString() + "/" + idDataLesson.ToString()));
                }
            }
            catch
            {
                throw;
            }
            finally
            {}
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves lesson data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the lesson data</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataLesson", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idEnrollment", this.IdEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idLesson", this.IdLesson, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[DataLesson.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved enrollment
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idDataLesson"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified lesson data's properties.
        /// </summary>
        /// <param name="idDataLesson">Lesson Data Id</param>
        private void _Details(int idDataLesson)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idEnrollment", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idLesson", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimezone", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@title", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@revcode", null, SqlDbType.NVarChar, 32, ParameterDirection.Output);
            databaseObject.AddParameter("@order", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCompleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@contentTypeCommittedTo", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[DataLesson.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idDataLesson;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdEnrollment = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idEnrollment"].Value);
                this.IdLesson = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idLesson"].Value);
                this.IdTimezone = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idTimezone"].Value);
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.RevCode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@revcode"].Value);
                this.Order = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@order"].Value);
                this.DtCompleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtCompleted"].Value);
                this.ContentTypeCommittedTo = (Lesson.LessonContentType?)AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@contentTypeCommittedTo"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}