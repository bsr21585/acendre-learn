﻿using System;
using System.Data;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    [Serializable]
    public class GroupEnrollment
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public GroupEnrollment()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idGroupEnrollment">Group Enrollment Id</param>
        public GroupEnrollment(int idGroupEnrollment)
        {
            this._Details(idGroupEnrollment);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[GroupEnrollment.GetGrid]";

        /// <summary>
        /// Group Enrollment Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site"/>
        public int IdSite;

        /// <summary>
        /// Course Id.
        /// </summary>
        public int IdCourse;

        /// <summary>
        /// Group Id.
        /// </summary>
        public int IdGroup;

        /// <summary>
        /// TimeZone Id.
        /// </summary>
        public int IdTimezone;

        /// <summary>
        /// Is Locked By Prerequisites.
        /// </summary>
        public bool IsLockedByPrerequisites;

        /// <summary>
        /// Date Start.
        /// </summary>
        public DateTime DtStart;        

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Due Interval.
        /// </summary>
        public int? DueInterval;

        /// <summary>
        /// Due Timeframe.
        /// </summary>
        public string DueTimeframe;

        /// <summary>
        /// Expires from Start Interval.
        /// </summary>
        public int? ExpiresFromStartInterval;

        /// <summary>
        /// Expires from Start Timeframe.
        /// </summary>
        public string ExpiresFromStartTimeframe;

        /// <summary>
        /// Expires from First Launch Interval.
        /// </summary>
        public int? ExpiresFromFirstLaunchInterval;

        /// <summary>
        /// Expires from First Launch Timeframe.
        /// </summary>
        public string ExpiresFromFirstLaunchTimeframe;
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves group enrollment data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves group enrollment data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveForMultipleCourses
        /// <summary>
        /// Saves group enrollment data for multiple courses to the database as the current session user.
        /// This is only used/called for creation of a new group enrollment where we can create the group enrollment for multiple courses.
        /// </summary>
        public void SaveForMultipleCourses(DataTable courses)
        {
            this._SaveForMultipleCourses(AsentiaSessionState.IdSiteUser, courses);
        }

        /// <summary>
        /// Overloaded method that saves group enrollment data for multiple courses to the database with a caller specified.
        /// This is only used/called for creation of a new group enrollment where we can create the group enrollment for multiple courses.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public void SaveForMultipleCourses(int idCaller, DataTable courses)
        {
            this._SaveForMultipleCourses(idCaller, courses);
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Group Enrollment(s).
        /// </summary>
        /// <param name="deletees">DataTable of group enrollments to delete</param>
        /// <param name="cascadeDeleteToUsers">Delete user-inherited enrollments of the group enrollments being deleted?</param>
        public static void Delete(DataTable deletees, bool cascadeDeleteToUsers)
        {            
            AsentiaDatabase databaseObject = new AsentiaDatabase(600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@cascadeDeleteToUsers", cascadeDeleteToUsers, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@GroupEnrollments", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[GroupEnrollment.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        
        #region TogglePrerequisiteLock
        /// <summary>
        /// Toggles the IsLockedByPrerequisites flag for a Group Enrollment.
        /// </summary>
        /// <param name="idGroupEnrollment">group enrollment id</param>
        public static void TogglePrerequisiteLock(int idGroupEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idGroupEnrollment", idGroupEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);
            
            try
            {
                databaseObject.ExecuteNonQuery("[GroupEnrollment.TogglePrerequisiteLock]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves group enrollment data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the group enrollment</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idGroupEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idCourse", this.IdCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idGroup", this.IdGroup, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isLockedByPrerequisites", this.IsLockedByPrerequisites, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);

            if (this.DueInterval == null)
            { databaseObject.AddParameter("@dueInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dueInterval", this.DueInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@dueTimeframe", this.DueTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromStartInterval == null)
            { databaseObject.AddParameter("@expiresFromStartInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromStartInterval", this.ExpiresFromStartInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromStartTimeframe", this.ExpiresFromStartTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromFirstLaunchInterval == null)
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", this.ExpiresFromFirstLaunchInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", this.ExpiresFromFirstLaunchTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[GroupEnrollment.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved group enrollment
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idGroupEnrollment"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SaveForMultipleCourses
        /// <summary>
        /// Saves group enrollment data for multiple courses to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <param name="courses">DataTable of course ids to create group enrollments for</param>
        private void _SaveForMultipleCourses(int idCaller, DataTable courses)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Courses", courses, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@idGroup", this.IdGroup, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isLockedByPrerequisites", this.IsLockedByPrerequisites, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);

            if (this.DueInterval == null)
            { databaseObject.AddParameter("@dueInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dueInterval", this.DueInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@dueTimeframe", this.DueTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromStartInterval == null)
            { databaseObject.AddParameter("@expiresFromStartInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromStartInterval", this.ExpiresFromStartInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromStartTimeframe", this.ExpiresFromStartTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromFirstLaunchInterval == null)
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", this.ExpiresFromFirstLaunchInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", this.ExpiresFromFirstLaunchTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[GroupEnrollment.SaveForMultipleCourses]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified group enrollment's properties.
        /// </summary>
        /// <param name="idGroupEnrollment">Group Enrollment Id</param>
        private void _Details(int idGroupEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idGroupEnrollment", idGroupEnrollment, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idCourse", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idGroup", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimezone", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@isLockedByPrerequisites", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtStart", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dueInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dueTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[GroupEnrollment.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idGroupEnrollment;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdCourse = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idCourse"].Value);
                this.IdGroup = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idGroup"].Value);
                this.IdTimezone = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idTimezone"].Value);
                this.IsLockedByPrerequisites = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isLockedByPrerequisites"].Value);
                this.DtStart = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtStart"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DueInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@dueInterval"].Value);
                this.DueTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@dueTimeframe"].Value);
                this.ExpiresFromStartInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromStartInterval"].Value);
                this.ExpiresFromStartTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromStartTimeframe"].Value);
                this.ExpiresFromFirstLaunchInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromFirstLaunchInterval"].Value);
                this.ExpiresFromFirstLaunchTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromFirstLaunchTimeframe"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}

