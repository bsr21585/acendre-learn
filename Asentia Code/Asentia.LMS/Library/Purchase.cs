﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class Purchase
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Purchase()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idPurchase">Purchase Id</param>
        public Purchase(int idPurchase)
        {
            this._Details(idPurchase);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Purchase.GetGrid]";

        /// <summary>
        /// Purchase Id
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// User Id
        /// </summary>
        public int IdUser;

        /// <summary>
        /// Order Number
        /// </summary>
        public string OrderNumber;

        /// <summary>
        /// TimeStamp of when the purchase was completed.
        /// </summary>
        public DateTime? TimeStamp;

        /// <summary>
        /// Credit Card Last Four Digits - PayPal gets XXXX
        /// </summary>
        public string CreditCardLastFour;

        /// <summary>
        /// Currency
        /// </summary>
        public string Currency;

        /// <summary>
        /// Site Id
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Pending Date
        /// For PayPal only, so that we can keep the record around while waiting for IPN.
        /// </summary>
        public DateTime? DtPending;

        /// <summary>
        /// Failed
        /// For PayPal only. Failure for PayPal is absolute, whereas for other processors it is not, so we keep the purchase open.
        /// </summary>
        public bool? Failed;

        /// <summary>
        /// The transaction id returned from the processor.
        /// Will be postfixed with (PayPal), or (Authorize.net)
        /// </summary>
        public string TransactionId;
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves purchase data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves purchase data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region GetActiveCartForUser
        /// <summary>
        /// Returns the user's active cart details.
        /// Active cart is defined as a purchase that has not been confirmed, is not pending, and is not failed.
        /// </summary>
        /// <param name="idUser">the id of the user to get the cart for</param>
        public void GetActiveCartForUser(int idUser, bool createCartIfNoneExists)
        {            
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idPurchase", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@orderNumber", null, SqlDbType.NVarChar, 12, ParameterDirection.Output);
            databaseObject.AddParameter("@timeStamp", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@ccnum", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@currency", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dtPending", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@failed", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@transactionId", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Purchase.GetActiveCartForUser]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                if (AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idPurchase"].Value) > 0)
                {
                    this.Id = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idPurchase"].Value);
                    this.IdUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);
                    this.OrderNumber = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@orderNumber"].Value);
                    this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                    this.TimeStamp = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@timeStamp"].Value);
                    this.CreditCardLastFour = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@ccnum"].Value);
                    this.Currency = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@currency"].Value);
                    this.DtPending = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtPending"].Value);
                    this.Failed = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@failed"].Value);
                    this.TransactionId = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@transactionId"].Value);
                }
                else
                {
                    if (createCartIfNoneExists) // a purchase needs to be created, so set base properties and create it
                    {
                        // set base properties needed to create a new cart
                        this.Id = 0;
                        this.IdUser = idUser;
                        this.IdSite = AsentiaSessionState.IdSite;
                        this.Currency = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_CURRENCY);

                        // call save to create the new cart
                        int idPurchase = this.Save();

                        // call details to load the newly saved cart
                        this._Details(idPurchase);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Confirm()
        /// <summary>
        /// Executes processes to confirm the purchase.
        /// - Loops through transaction items to enroll and confirm each item.
        /// - Confirms the purchase.
        /// </summary>
        public void Confirm()
        {
            try
            {
                // loop through each transaction item in the purchase, grant enrollment(s), then confirm it

                DataTable transactionItems = TransactionItem.GetItemsForPurchase(this.Id);

                DataTable transactionIdsToConfirm = new DataTable();
                transactionIdsToConfirm.Columns.Add("id", typeof(int));

                if (transactionItems.Rows.Count == 0)
                { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingCartItemsForEnrollmentPleaseContactAnAdministrator); }

                foreach (DataRow row in transactionItems.Rows)
                {
                    int idTransactionItem = Convert.ToInt32(row["idTransactionItem"]);
                    int itemId = Convert.ToInt32(row["itemId"]);
                    PurchaseItemType itemType = (PurchaseItemType)Convert.ToInt32(row["itemType"]);

                    // enroll based on item type
                    switch (itemType)
                    {
                        case PurchaseItemType.Catalog:
                            // enroll the user in the catalog
                            LMS.Library.Enrollment catalogEnrollmentObject = new LMS.Library.Enrollment();
                            catalogEnrollmentObject.EnrollCatalog(itemId, AsentiaSessionState.IdSiteUser, idTransactionItem);

                            break;
                        case PurchaseItemType.Course:
                            // enroll the user in the course
                            LMS.Library.Enrollment courseEnrollmentObject = new LMS.Library.Enrollment();
                            courseEnrollmentObject.EnrollCourse(itemId, AsentiaSessionState.IdSiteUser, idTransactionItem);

                            // if there is an ilt session linked to this course, enroll the user into the session
                            if (row["idIltSession"] != DBNull.Value)
                            {
                                StandupTrainingInstance session = new StandupTrainingInstance(Convert.ToInt32(row["idIltSession"]));

                                if (session.SeatsRemaining > 0)
                                { session.JoinUser(AsentiaSessionState.IdSiteUser, false); }
                                else if (session.WaitingSeatsRemaining > 0)
                                { session.JoinUser(AsentiaSessionState.IdSiteUser, true); }
                            }

                            break;
                        case PurchaseItemType.LearningPath:
                            // enroll the user in the learning path
                            LMS.Library.LearningPathEnrollment learningPathEnrollmentObject = new LMS.Library.LearningPathEnrollment();
                            learningPathEnrollmentObject.EnrollLearningPath(itemId, AsentiaSessionState.IdSiteUser, idTransactionItem);

                            break;
                        case PurchaseItemType.InstructorLedTraining:
                            // enroll in instructor led training instance
                            StandupTrainingInstance instructorLedTrainingInstance = new StandupTrainingInstance(itemId);

                            if (instructorLedTrainingInstance.SeatsRemaining > 0)
                            { instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, false); }
                            else if (instructorLedTrainingInstance.WaitingSeatsRemaining > 0)
                            { instructorLedTrainingInstance.JoinUser(AsentiaSessionState.IdSiteUser, true); }
                            else
                            {
                                // we have to just do nothing here rather than throw an exception 
                                // but this should never occur as it should have been handled prior to checkout
                            }

                            break;
                        default:
                            break;
                    }

                    // add to the list of transaction items to confirm
                    transactionIdsToConfirm.Rows.Add(idTransactionItem);
                }

                // confirm the transaction items
                TransactionItem.ConfirmTransactionItems(transactionIdsToConfirm, false);

                // all information to update the purchase should be there already, just save the purchase
                this.Save();
            }
            catch (AsentiaException)
            {
                throw;
            }
            catch (Exception ex) // throw other unhandled exception as Asentia exception so calling code only has to catch one type of exception
            {
                throw new AsentiaException(ex.Message);
            }
        }
        #endregion

        #region Confirm(int idUser)
        /// <summary>
        /// Executes processes to confirm the purchase.
        /// - Loops through transaction items to enroll and confirm each item.
        /// - Confirms the purchase.
        /// </summary>
        public void Confirm(int idUser)
        {
            try
            {
                // loop through each transaction item in the purchase, grant enrollment(s), then confirm it

                DataTable transactionItems = TransactionItem.GetItemsForPurchase(this.Id);

                DataTable transactionIdsToConfirm = new DataTable();
                transactionIdsToConfirm.Columns.Add("id", typeof(int));

                if (transactionItems.Rows.Count == 0)
                { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingCartItemsForEnrollmentPleaseContactAnAdministrator); }

                foreach (DataRow row in transactionItems.Rows)
                {
                    int idTransactionItem = Convert.ToInt32(row["idTransactionItem"]);
                    int itemId = Convert.ToInt32(row["itemId"]);
                    PurchaseItemType itemType = (PurchaseItemType)Convert.ToInt32(row["itemType"]);

                    // enroll based on item type
                    switch (itemType)
                    {
                        case PurchaseItemType.Catalog:
                            // enroll the user in the catalog
                            LMS.Library.Enrollment catalogEnrollmentObject = new LMS.Library.Enrollment();
                            catalogEnrollmentObject.EnrollCatalog(itemId, idUser, idTransactionItem);

                            break;
                        case PurchaseItemType.Course:
                            // enroll the user in the course
                            LMS.Library.Enrollment courseEnrollmentObject = new LMS.Library.Enrollment();
                            courseEnrollmentObject.EnrollCourse(itemId, idUser, idTransactionItem);

                            // if there is an ilt session linked to this course, enroll the user into the session
                            if (row["idIltSession"] != DBNull.Value)
                            {
                                StandupTrainingInstance session = new StandupTrainingInstance(Convert.ToInt32(row["idIltSession"]));

                                if (session.SeatsRemaining > 0)
                                { session.JoinUser(idUser, false); }
                                else if (session.WaitingSeatsRemaining > 0)
                                { session.JoinUser(idUser, true); }
                            }

                            break;
                        case PurchaseItemType.LearningPath:
                            // enroll the user in the learning path
                            LMS.Library.LearningPathEnrollment learningPathEnrollmentObject = new LMS.Library.LearningPathEnrollment();
                            learningPathEnrollmentObject.EnrollLearningPath(itemId, idUser, idTransactionItem);

                            break;
                        case PurchaseItemType.InstructorLedTraining:
                            // enroll in instructor led training instance
                            StandupTrainingInstance instructorLedTrainingInstance = new StandupTrainingInstance(itemId);

                            if (instructorLedTrainingInstance.SeatsRemaining > 0)
                            { instructorLedTrainingInstance.JoinUser(idUser, false); }
                            else if (instructorLedTrainingInstance.WaitingSeatsRemaining > 0)
                            { instructorLedTrainingInstance.JoinUser(idUser, true); }
                            else
                            {
                                // we have to just do nothing here rather than throw an exception 
                                // but this should never occur as it should have been handled prior to checkout
                            }

                            break;
                        default:
                            break;
                    }

                    // add to the list of transaction items to confirm
                    transactionIdsToConfirm.Rows.Add(idTransactionItem);
                }

                // confirm the transaction items
                TransactionItem.ConfirmTransactionItems(transactionIdsToConfirm, false);

                // all information to update the purchase should be there already, just save the purchase
                this.Save();
            }
            catch (AsentiaException)
            {
                throw;
            }
            catch (Exception ex) // throw other unhandled exception as Asentia exception so calling code only has to catch one type of exception
            {
                throw new AsentiaException(ex.Message);
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Purchase(s).
        /// </summary>
        /// <param name="deletees">DataTable of purchases to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Purchases", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Purchase.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ClearAllUnconfirmed
        /// <summary>
        /// Clears all unconfirmed purchases (active carts) for the site.
        /// </summary>
        public static void ClearAllUnconfirmed()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Purchase.ClearAllUnconfirmed]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves purchase data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved purchase</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idPurchase", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);            
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@orderNumber", this.OrderNumber, SqlDbType.NVarChar, 12, ParameterDirection.Input);            
            databaseObject.AddParameter("@ccnum", this.CreditCardLastFour, SqlDbType.NVarChar, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@currency", this.Currency, SqlDbType.NVarChar, 4, ParameterDirection.Input);                        
            databaseObject.AddParameter("@transactionId", this.TransactionId, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            if (this.TimeStamp == null)
            { databaseObject.AddParameter("@timeStamp", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@timeStamp", this.TimeStamp, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            if (this.DtPending == null)
            { databaseObject.AddParameter("@dtPending", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dtPending", this.DtPending, SqlDbType.DateTime, 8, ParameterDirection.Input); }            

            if (this.Failed == null)
            { databaseObject.AddParameter("@failed", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@failed", this.Failed, SqlDbType.Bit, 1, ParameterDirection.Input); }            

            try
            {
                databaseObject.ExecuteNonQuery("[Purchase.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved purchase
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idPurchase"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="IdPurchase">Purchase Id</param>
        private void _Details(int idPurchase)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idPurchase", idPurchase, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@orderNumber", null, SqlDbType.NVarChar, 12, ParameterDirection.Output);
            databaseObject.AddParameter("@timeStamp", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@ccnum", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@currency", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dtPending", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@failed", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@transactionId", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Purchase.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idPurchase;
                this.IdUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.OrderNumber = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@orderNumber"].Value);
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.TimeStamp = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@timeStamp"].Value);
                this.CreditCardLastFour = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@ccnum"].Value);
                this.Currency = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@currency"].Value);
                this.DtPending = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtPending"].Value);
                this.Failed = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@failed"].Value);
                this.TransactionId = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@transactionId"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}