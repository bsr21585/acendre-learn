﻿using System;
using System.Data;
using System.IO;
using System.Net;
using System.Web;
using System.Linq;
using System.Xml.Linq;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class AuthorizeDotNetAIM
    {
        #region AuthenticateGateway
        /// <summary>
        /// Authenticates the Authorize.net gateway using the API Login and Transaction Key.
        /// This is used to validate the gateway before any transactions can be processed on it.
        /// </summary>
        /// <param name="apiLogin">Authorize.net API Login</param>
        /// <param name="transactionKey">Authorize.net Transaction Key</param>
        public static void AuthenticateGateway(string apiLogin, string transactionKey)
        {
            // ensure apiLogin and transactionKey are not empty
            if (String.IsNullOrWhiteSpace(apiLogin))
            { throw new AsentiaException(_GlobalResources.AuthorizeNetAPILoginIsInvalid); }

            if (String.IsNullOrWhiteSpace(transactionKey))
            { throw new AsentiaException(_GlobalResources.AuthorizeNetTransactionKeyIsInvalid); }

            // build the request
            XNamespace xNamespace = "AnetApi/xml/v1/schema/AnetApiSchema.xsd";
            XDocument xmlDocument =
                new XDocument(
                    new XElement(xNamespace + "authenticateTestRequest",
                        new XElement(xNamespace + "merchantAuthentication",
                            new XElement(xNamespace + "name", AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN)),
                            new XElement(xNamespace + "transactionKey", AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_TRANSACTIONKEY))
                        )
                    )
                );
            
            // get the Authorize.net api url, live or test
            string apiUrl = String.Empty;

            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_MODE) == EcommerceSettings.PROCESSING_MODE_LIVE)
            { apiUrl = Config.EcommerceSettings.AuthorizeNetAIMLiveURL; }
            else
            { apiUrl = Config.EcommerceSettings.AuthorizeNetAIMTestURL; }

            // set security protocol to TLS 1.2 as Authorize.net requires
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // build the request, send the request, get the response
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
            
            // encode payload into bytes
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(xmlDocument.ToString());
            
            // set the payload type, length, and method
            request.ContentType = "text/xml; encoding=\"utf-8\"";
            request.ContentLength = bytes.Length;
            request.Method = "POST";

            // write the request stream (payload)
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();

            // get the response
            string responseString = String.Empty;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    { responseString = new StreamReader(responseStream).ReadToEnd(); }
                }
                else
                { throw new AsentiaException(_GlobalResources.UnableToConnectToProcessorGatewayPleaseContactAnAdministrator); }
            }

            // load the response as XML, log it, and read it
            XDocument xmlResponse = XDocument.Parse(responseString);

            // save the response to a log file
            _LogXMLResponse(xmlResponse, "_GATEWAYVALIDATION_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", AsentiaSessionState.UtcNow));

            // read the response XML
            var resultCode = xmlResponse.Descendants(xNamespace + "resultCode").FirstOrDefault().Value;

            // if the result code is ok, validation was passed, do nothing
            // if it is anything else, it's a failure, and we know it's due to transaction key, as that is the only thing that could be wrong at this point
            if (resultCode != "Ok")
            { throw new AsentiaException(_GlobalResources.AuthorizeNetTransactionKeyIsInvalid); }
        }
        #endregion

        #region SubmitTransaction
        /// <summary>
        /// Submits transaction to Authorize.net.
        /// This just submits the transaction and gets the response, all validations are done prior to getting to this point.
        /// If the transaction is successful, this method just returns and and allows calling code to move on. If the transaction 
        /// is not successful, an exception will be thrown for the calling code to catch and act accordingly.
        /// </summary>
        /// <param name="apiLogin">Authorize.net API Login</param>
        /// <param name="transactionKey">Authorize.net Transaction Key</param>
        /// <param name="purchaseIdentifier">This is the "orderNumber" field from tblPurchase.</param>
        /// <param name="paymentTokenDescriptor">This is the descriptor returned from credit card submit to Accept.js</param>
        /// <param name="paymentToken">This is the token returned from credit card submit to Accept.js</param>
        /// <param name="paymentAmount">The amount of the transaction, not currency specific.</param>
        /// <param name="purchaserFirstName">Purchaser First Name</param>
        /// <param name="purchaserLastName">Purchaser Last Name</param>
        /// <param name="purchaserCompany">Purchaser Company</param>
        /// <param name="purchaserAddress">Purchaser Address</param>
        /// <param name="purchaserCity">Purchaser City</param>
        /// <param name="purchaserStateProvince">Purchaser State/Province</param>
        /// <param name="purchaserPostalCode">Purchaser Postal Code</param>
        /// <param name="purchaserCountry">Purchaser Country</param>
        /// <param name="purchaserEmail">Purchaser Email</param>
        /// <param name="purchaseItems">DataTable of item(s) purchased</param>
        public static string SubmitTransaction(string apiLogin,
                                               string transactionKey,
                                               string purchaseIdentifier,
                                               string paymentTokenDescriptor,
                                               string paymentToken,
                                               double paymentAmount,
                                               string purchaserFirstName,
                                               string purchaserLastName,
                                               string purchaserCompany,
                                               string purchaserAddress,
                                               string purchaserCity,
                                               string purchaserStateProvince,
                                               string purchaserPostalCode,
                                               string purchaserCountry,
                                               string purchaserEmail,
                                               DataTable purchaseItems
        )
        {
            // ensure apiLogin and transactionKey are not empty
            if (String.IsNullOrWhiteSpace(apiLogin))
            { throw new AsentiaException(_GlobalResources.AuthorizeNetAPILoginIsInvalid); }

            if (String.IsNullOrWhiteSpace(transactionKey))
            { throw new AsentiaException(_GlobalResources.AuthorizeNetTransactionKeyIsInvalid); }

            // DO NOT VALIDATE OTHER FIELDS, THEY ARE VALIDATED ON THE CHECKOUT PAGE

            // build the request
            XNamespace xNamespace = "AnetApi/xml/v1/schema/AnetApiSchema.xsd";
            XDocument xmlDocument =
                new XDocument(
                    new XElement(xNamespace + "createTransactionRequest",
                        new XElement(xNamespace + "merchantAuthentication",
                            new XElement(xNamespace + "name", AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_LOGIN)),
                            new XElement(xNamespace + "transactionKey", AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_TRANSACTIONKEY))
                        ),
                        new XElement(xNamespace + "refId", purchaseIdentifier),
                        new XElement(xNamespace + "transactionRequest",
                            new XElement(xNamespace + "transactionType", "authCaptureTransaction"),
                            new XElement(xNamespace + "amount", String.Format("{0:0.00}", paymentAmount)),
                            new XElement(xNamespace + "payment",
                                new XElement(xNamespace + "opaqueData",
                                    new XElement(xNamespace + "dataDescriptor", paymentTokenDescriptor),
                                    new XElement(xNamespace + "dataValue", paymentToken)
                                )
                            ),
                            new XElement(xNamespace + "order",
                                new XElement(xNamespace + "invoiceNumber", purchaseIdentifier)
                                //new XElement(xNamespace + "description", "##REPLACE##")
                            ),
                            new XElement(xNamespace + "lineItems"
                                // line items to be added below
                            ),
                            new XElement(xNamespace + "customer",
                                new XElement(xNamespace + "email", new XCData(purchaserEmail))
                            ),
                            new XElement(xNamespace + "billTo",
                                new XElement(xNamespace + "firstName", new XCData(purchaserFirstName)),
                                new XElement(xNamespace + "lastName", new XCData(purchaserLastName)),
                                new XElement(xNamespace + "company", new XCData(purchaserCompany)),
                                new XElement(xNamespace + "address", new XCData(purchaserAddress)),
                                new XElement(xNamespace + "city", new XCData(purchaserCity)),
                                new XElement(xNamespace + "state", purchaserStateProvince),
                                new XElement(xNamespace + "zip", purchaserPostalCode),
                                new XElement(xNamespace + "country", purchaserCountry)
                            ),
                            new XElement(xNamespace + "customerIP", HttpContext.Current.Request.UserHostAddress)
                        )                        
                    )
                );

            // add lineItems
            if (purchaseItems != null)
            {
                foreach (DataRow row in purchaseItems.Rows)
                {
                    // get the items from data row
                    string itemId = row["itemId"].ToString();
                    string name = row["itemName"].ToString();
                    string description = row["itemDescription"].ToString();
                    string quantity = "1";
                    double effectivePrice = Convert.ToDouble(row["effectivePrice"]);

                    // adjust string lengths for name and description to max lengths for Authorize.net
                    int itemNameMaxLength = 31;
                    int itemDescriptionMaxLength = 255;

                    if (name.Length > itemNameMaxLength)
                    { name = name.Substring(0, itemNameMaxLength - 3) + "..."; }

                    if (description.Length > itemDescriptionMaxLength)
                    { description = description.Substring(0, itemDescriptionMaxLength - 3) + "..."; }
                    
                    // add the line item to the xml document
                    xmlDocument.Root.Element(xNamespace + "transactionRequest").Element(xNamespace + "lineItems").Add(
                        new XElement(xNamespace + "lineItem",
                            new XElement(xNamespace + "itemId", itemId),
                            new XElement(xNamespace + "name", new XCData(name)),
                            new XElement(xNamespace + "description", new XCData(description)),
                            new XElement(xNamespace + "quantity", quantity),
                            new XElement(xNamespace + "unitPrice", String.Format("{0:0.00}", effectivePrice))
                        )
                    );
                }
            }

            // save the request to a log file so that we have a record of it
            _LogXMLResponse(xmlDocument, purchaseIdentifier + "_TRANSACTION_REQUEST_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", AsentiaSessionState.UtcNow));

            // get the Authorize.net api url, live or test
            string apiUrl = String.Empty;

            if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ECOMMERCE_AUTHORIZENET_MODE) == EcommerceSettings.PROCESSING_MODE_LIVE)
            { apiUrl = Config.EcommerceSettings.AuthorizeNetAIMLiveURL; }
            else
            { apiUrl = Config.EcommerceSettings.AuthorizeNetAIMTestURL; }

            // set security protocol to TLS 1.2 as Authorize.net requires
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // build the request, send the request, get the response
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);

            // encode payload into bytes
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(xmlDocument.ToString());

            // set the payload type, length, and method
            request.ContentType = "text/xml; encoding=\"utf-8\"";
            request.ContentLength = bytes.Length;
            request.Method = "POST";

            // write the request stream (payload)
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();

            // get the response
            string responseString = String.Empty;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    { responseString = new StreamReader(responseStream).ReadToEnd(); }
                }
                else
                { throw new AsentiaException(_GlobalResources.UnableToConnectToProcessorGatewayPleaseContactAnAdministrator); }
            }

            // load the response as XML, read it, and log it
            XDocument xmlResponse = XDocument.Parse(responseString);            

            // read the response XML
            var resultCode = xmlResponse.Descendants(xNamespace + "resultCode").FirstOrDefault().Value;            

            // if the result code is ok, the transaction was transmitted, make sure it was approved
            // if it is anything else, it's a failure, and we know it's due to transaction key, as that is the only thing that could be wrong at this point
            if (resultCode != "Ok")
            {
                // save the response to a log file
                _LogXMLResponse(xmlResponse, purchaseIdentifier + "_TRANSACTION_FAILED_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", AsentiaSessionState.UtcNow));

                // get detailed error code
                var errorCode = xmlResponse.Descendants(xNamespace + "code").FirstOrDefault().Value;

                string errorMessage = String.Empty;

                switch (errorMessage)
                {
                    case "E00001":
                    case "E00002":
                    case "E00003":
                    case "E00004":
                        errorMessage = _GlobalResources.AFatalErrorOccurredWhileProcessingTheTransactionPleaseContactAnAdministrator;
                        break;
                    case "E00005":
                    case "E00006":
                    case "E00007":
                    case "E00008":
                        errorMessage = _GlobalResources.AuthenticationFailedDueToInvalidAPILoginAndOrTransactionKey;
                        break;
                    case "E00009":
                        errorMessage = _GlobalResources.ThePaymentGatewayAccountIsInTestMode;
                        break;
                    case "E00010":
                    case "E00011":
                        errorMessage = _GlobalResources.AccessToTheGatewayWasDenied;
                        break;                    
                    case "E00013":
                    case "E00014":
                        errorMessage = _GlobalResources.OneOrMoreRequiredFieldsIsMissingOrInvalid;
                        break;
                    default:
                        errorMessage = _GlobalResources.AnUnknownErrorOccurredWhileProcessingYourRequest;
                        break;
                }

                // append the code to the error message so we have something to reference it to
                errorMessage += " (" + errorCode + ")";

                // throw the exception with the error
                throw new AsentiaException(_GlobalResources.TheTransactionHasBeenDeclinedDueToTheFollowingError_s + "<br /><br />" + errorMessage); 
            }
            else // transaction was successfully submitted, make sure it was prroved, log it and return the transaction id and last 4 of the card number
            {
                // read the response code XML
                var responseCode = xmlResponse.Descendants(xNamespace + "responseCode").FirstOrDefault().Value;

                // if the response code is not 1, the transaction was not approved, log it, and throw an error
                if (responseCode != "1")
                {
                    // save the response to a log file
                    _LogXMLResponse(xmlResponse, purchaseIdentifier + "_TRANSACTION_DECLINED_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", AsentiaSessionState.UtcNow));

                    // throw the exception with the error
                    throw new AsentiaException(_GlobalResources.TheTransactionHasBeenDeclined);
                }
                else
                {
                    // save the response to a log file
                    _LogXMLResponse(xmlResponse, purchaseIdentifier + "_TRANSACTION_SUCCESSFUL_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", AsentiaSessionState.UtcNow));

                    // get the transaction id and the last 4 of the card number, put them in a string, and return
                    string returnInformation = null;

                    string transactionId = xmlResponse.Descendants(xNamespace + "transId").FirstOrDefault().Value + " (Authorize.net)";
                    string creditCardLast4 = xmlResponse.Descendants(xNamespace + "accountNumber").FirstOrDefault().Value;
                    creditCardLast4 = creditCardLast4.Substring(Math.Max(0, creditCardLast4.Length - 4));

                    returnInformation = transactionId + "|" + creditCardLast4;

                    return returnInformation;
                }
            }
        }
        #endregion

        #region _LogXMLResponse
        /// <summary>
        /// Saves response xml to a log file.
        /// </summary>
        /// <param name="responseXml">the response xml</param>
        /// <param name="fileName">the name of the file, minus extension</param>
        private static void _LogXMLResponse(XDocument xmlDocument, string fileName)
        {
            // create the log directory if it doesn't exist
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_TRANSACTION_AUTHORIZE)))
            { Directory.CreateDirectory(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_TRANSACTION_AUTHORIZE)); }

            // build file path
            string filePath = SitePathConstants.SITE_LOG_TRANSACTION_AUTHORIZE + fileName + ".xml";

            // save the response to file
            xmlDocument.Save(HttpContext.Current.Server.MapPath(filePath));
        }
        #endregion
    }
}