﻿using Asentia.Common;
using System;
using System.Data;

namespace Asentia.LMS.Library
{
    public static class TinCan
    {
        /// <summary>
        /// update lesson status.
        /// </summary>
        /// <param name="idDataLesson">The identifier data lesson.</param>
        /// <param name="packageType">Type of the package.</param>
        /// <returns></returns>
        public static bool _UpdateLessonStatus(int idDataLesson, string packageType,string activityId)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@packageType", packageType, SqlDbType.NVarChar, 50, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@activityId", activityId, SqlDbType.NVarChar, 300, ParameterDirection.Input);
          
            try
            {
                databaseObject.ExecuteNonQuery("[DataSco.UpdateStatusForTinCan]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return true;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

    }
}
