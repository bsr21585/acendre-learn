﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Threading;
using System.Xml;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class QuizSurvey
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public QuizSurvey()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idQuizSurvey">Quiz/Survey Id</param>
        public QuizSurvey(int idQuizSurvey)
        {
            this._Details(idQuizSurvey);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[QuizSurvey.GetGrid]";

        /// <summary>
        /// Quiz/Survey Id
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id
        /// </summary>
        public int IdSite;

        /// <summary>
        /// Author Id
        /// </summary>
        public int IdAuthor;

        /// <summary>
        /// Type - Quiz or Survey
        /// </summary>
        public QuizSurveyType Type;

        /// <summary>
        /// Quiz/Survey Identifier (Name)
        /// </summary>
        public string Identifier;

        /// <summary>
        /// Quiz/Survey GUID String
        /// </summary>
        public string GUIDString;

        /// <summary>
        /// This is the JSON string of data that holds all of the quiz or survey data and configuration.
        /// This will end up populating a hidden input where serialization and deserialization will occur client-side.
        /// </summary>
        public string JSONData;

        /// <summary>
        /// Is this Quiz/Survey in draft mode, not published?
        /// </summary>
        public bool IsDraft;

        /// <summary>
        /// The Content Package id this Quiz/Survey was published to.
        /// </summary>
        public int? IdContentPackage;

        /// <summary>
        /// Date Created
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Date Modified
        /// </summary>
        public DateTime? DtModified;
        #endregion

        #region QuizSurveyType ENUM
        public enum QuizSurveyType
        {
            Quiz = 1,
            Survey = 2,
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves quiz/survey data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves quiz/survey data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Quiz(zes)/Survey(s).
        /// </summary>
        /// <param name="deletees">DataTable of quizzes/surveys to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@QuizzesSurveys", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[QuizSurvey.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves quiz/survey data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved quiz/survey</returns>
        public int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idQuizSurvey", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idAuthor", this.IdAuthor, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@type", this.Type, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@identifier", this.Identifier, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@guid", this.GUIDString, SqlDbType.NVarChar, 36, ParameterDirection.Input);
            databaseObject.AddParameter("@data", this.JSONData, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@isDraft", this.IsDraft, SqlDbType.Bit, 1, ParameterDirection.Input);

            if (this.IdContentPackage == null)
            { databaseObject.AddParameter("@idContentPackage", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idContentPackage", this.IdContentPackage, SqlDbType.Int, 4, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[QuizSurvey.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved content package
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idQuizSurvey"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified content package's properties.
        /// </summary>
        /// <param name="idQuizSurvey">Quiz/Survey Id</param>
        private void _Details(int idQuizSurvey)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idQuizSurvey", idQuizSurvey, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idAuthor", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@type", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@identifier", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@guid", null, SqlDbType.NVarChar, 36, ParameterDirection.Output);
            databaseObject.AddParameter("@data", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@isDraft", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@idContentPackage", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtModified", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[QuizSurvey.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idQuizSurvey;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdAuthor = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idAuthor"].Value);
                this.Type = (QuizSurveyType)AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@type"].Value);
                this.Identifier = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@identifier"].Value);
                this.GUIDString = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@guid"].Value);
                this.JSONData = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@data"].Value);
                this.IsDraft = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDraft"].Value);
                this.IdContentPackage = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idContentPackage"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
