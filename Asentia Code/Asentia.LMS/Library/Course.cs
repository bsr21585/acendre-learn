﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    [XmlInclude(typeof(LanguageSpecificProperty))]
    [XmlInclude(typeof(SocialMediaProperty))]
    [Serializable]
    public class Course
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Course()
        {
            this.LanguageSpecificProperties = new ArrayList();
            this.SocialMediaProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idCourse">Course Id</param>
        public Course(int idCourse)
        {
            this.LanguageSpecificProperties = new ArrayList();
            this.SocialMediaProperties = new ArrayList();

            this._Details(idCourse);
            this._GetPropertiesInLanguages(idCourse);
            this._GetSocialMediaProperties(idCourse);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Course.GetGrid]";

        /// <summary>
        /// Course Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Title
        /// </summary>
        public string Title;

        /// <summary>
        /// Course Code
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string CourseCode;

        /// <summary>
        /// Revision Code
        /// </summary>
        [XmlElement(IsNullable = true)]
        [XmlIgnore]
        [JsonIgnore]
        public string RevCode;

        /// <summary>
        /// Avatar
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Avatar;

        /// <summary>
        /// Cost
        /// </summary>
        public double? Cost;

        /// <summary>
        /// Credits.
        /// </summary>
        public double? Credits;

        /// <summary>
        /// Estimated Length in Minutes (minutes)
        /// </summary>
        public int? EstimatedLengthInMinutes;

        /// <summary>
        /// Short Description
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string ShortDescription;

        /// <summary>
        /// Long Description
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string LongDescription;

        /// <summary>
        /// Objectives
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Objectives;

        /// <summary>
        /// Social Media (xml string)
        /// </summary>
        [XmlElement(IsNullable = true)]
        [XmlIgnore]
        [JsonIgnore]
        public string SocialMedia;

        /// <summary>
        /// Is Published
        /// </summary>
        public bool? IsPublished;

        /// <summary>
        /// Is Closed
        /// </summary>
        public bool? IsClosed;

        /// <summary>
        /// Is Locked
        /// </summary>
        public bool? IsLocked;

        /// <summary>
        /// Are prerequisites "match any?"
        /// </summary>
        public bool? IsPrerequisiteAny;

        /// <summary>
        /// Is course feed is "Active"
        /// </summary>
        public bool? IsFeedActive;

        /// <summary>
        /// Is Course feed is "moderated"
        /// </summary>
        public bool? IsFeedModerated;

        /// <summary>
        /// Is course feed subscription is "Open"
        /// </summary>
        public bool? IsFeedOpenSubscription;

        /// <summary>
        /// Date Created
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Date Modified
        /// </summary>
        public DateTime? DtModified;

        /// <summary>
        /// Is Deleted
        /// </summary>
        public bool IsDeleted;

        /// <summary>
        /// Date Deleted
        /// </summary>
        public DateTime? DtDeleted;

        /// <summary>
        /// Order - This is the course's order in the root catalog.
        /// </summary>
        public int? Order;

        /// <summary>
        /// Disallow Rating - Excludes this course from ratings.
        /// </summary>
        public bool DisallowRating;

        /// <summary>
        /// Rating
        /// </summary>
        public double? Rating;

        /// <summary>
        /// Votes - Number of votes that contributed to the course's rating.
        /// </summary>
        public int? Votes;

        /// <summary>
        /// ForceLessonCompletionInOrder - Forces lessons to be completed in order.
        /// </summary>
        public bool ForceLessonCompletionInOrder;

        /// <summary>
        /// RequireFirstLessonToBeCompletedBeforeOthers - Requires the first lesson to be completed prior to all others.
        /// </summary>
        public bool RequireFirstLessonToBeCompletedBeforeOthers;

        /// <summary>
        /// LockLastLessonUntilOthersCompleted - Locks the last lesson until all others have been completed.
        /// </summary>
        public bool LockLastLessonUntilOthersCompleted;

        /// <summary>
        /// Self-Enrollment One-Time Only
        /// </summary>       
        public bool? SelfEnrollmentIsOneTimeOnly;

        /// <summary>
        /// Self-Enrollment Due Interval
        /// </summary>       
        public int? SelfEnrollmentDueInterval;

        /// <summary>
        /// Self-Enrollment Due TimeFrame
        /// </summary>
        public string SelfEnrollmentDueTimeframe;

        /// <summary>
        /// Self-Enrollment Expires from Start Interval.
        /// </summary>
        public int? SelfEnrollmentExpiresFromStartInterval;

        /// <summary>
        /// Self-Enrollment Expires from Start Timeframe.
        /// </summary>
        public string SelfEnrollmentExpiresFromStartTimeframe;

        /// <summary>
        /// Self-Enrollment Expires from First Launch Interval.
        /// </summary>
        public int? SelfEnrollmentExpiresFromFirstLaunchInterval;

        /// <summary>
        /// Self-Enrollment Expires from First Launch Timeframe.
        /// </summary>
        public string SelfEnrollmentExpiresFromFirstLaunchTimeframe;

        /// <summary>
        /// Search Tags.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string SearchTags;

        /// <summary>
        /// Is the calling user (current session user) currently enrolled
        /// in this course? "Currently enrolled" is defined having a not
        /// yet completed enrollment still within its access period.
        /// This is used to determine if a user can enroll in this course
        /// via the catalog. 
        /// </summary>
        public bool IsCallingUserEnrolled;

        /// <summary>
        /// Is the calling user (current session user) eligible to 
        /// self-enroll in the course more than once? This is determined
        /// by checking if the user has has ever self-enrolled in the 
        /// course and if the 'Self-Enrollment One Time Only' property is
        /// true for the course. This is used to determine if a user can 
        /// enroll in this course via the catalog. 
        /// </summary>
        public bool IsMultipleSelfEnrollmentEligible;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// ArrayList of social media properties.
        /// </summary>
        public ArrayList SocialMediaProperties;

        /// <summary>
        /// Is self-enrollment required to be approved by the administrator
        /// </summary>
        public bool? IsSelfEnrollmentApprovalRequired;

        /// <summary>
        /// Is self-enrollment approval allowed by course experts
        /// </summary>
        public bool? IsApprovalAllowedByCourseExperts;

        /// <summary>
        /// Is self-enrollment approval allowed by user supervisor
        /// </summary>
        public bool? IsApprovalAllowedByUserSupervisor;

        /// <summary>
        /// Shortcode for linking directly to the course through the catalog
        /// </summary>
        public string Shortcode;

        /// <summary>
        /// Is self-enrollment of this course locked by prerequisites, i.e. the enrolling user has not met prerequisites.
        /// </summary>
        public bool IsSelfEnrollmentLockedByPrerequisites;
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Title;
            public string ShortDescription;
            public string LongDescription;
            public string Objectives;
            public string SearchTags;

            /// <summary>
            /// parameterless constructor for serialization purpose
            /// </summary>
            public LanguageSpecificProperty()
            { ;}

            public LanguageSpecificProperty(string langString, string title, string shortDescription, string longDescription, string objectives, string searchTags)
            {
                this.LangString = langString;
                this.Title = title;
                this.ShortDescription = shortDescription;
                this.LongDescription = longDescription;
                this.Objectives = objectives;
                this.SearchTags = searchTags;
            }
        }

        /// <summary>
        /// Class that represents social media properties.
        /// </summary>
        public class SocialMediaProperty
        {
            public string IconType;
            public string Href;
            public string Protocol;
            public bool EnrollmentRequired;

            public SocialMediaProperty()
            { ;}

            public SocialMediaProperty(string iconType, string href, string protocol, bool enrollmentRequired)
            {
                this.IconType = iconType;
                this.Href = href;
                this.Protocol = protocol;
                this.EnrollmentRequired = enrollmentRequired;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves course data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves course data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveAvatar
        /// <summary>
        /// Saves course's avatar path to database as the current session user.
        /// </summary>
        public void SaveAvatar()
        {
            this._SaveAvatar(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves course's avatar path to database with a caller specified.
        /// This would be used when SaveAvatar needs to be called outside of a user session, i.e.
        /// API. Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public void SaveAvatar(int idCaller)
        {
            this._SaveAvatar(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this course.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="title">course title</param>
        /// <param name="shortDescription">course short description</param>
        /// <param name="longDescription">course long description</param>
        /// <param name="objectives">course objectives</param>
        public void SaveLang(string languageString, string title, string shortDescription, string longDescription, string objectives, string searchTags)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureCourseSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@title", title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", shortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@longDescription", longDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@objectives", objectives, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", searchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveExperts
        /// <summary>
        /// Saves Course Experts for this Course.
        /// </summary>
        /// <param name="experts">DataTable of course experts to save</param>
        public void SaveExperts(DataTable experts)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Experts", experts, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.SaveExperts]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveApprovers
        /// <summary>
        /// Saves Enrollment Approvers for this Course.
        /// </summary>
        /// <param name="approvers">DataTable of enrollment approvers to save</param>
        public void SaveApprovers(DataTable approvers)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Approvers", approvers, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.SaveApprovers]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SavePrerequisites
        /// <summary>
        /// Saves Course Prerequisites for this Course.
        /// </summary>
        /// <param name="prerequisites">DataTable of course prerequisites to save</param>
        public void SavePrerequisites(DataTable prerequisites)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Prerequisites", prerequisites, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.SavePrerequisites]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveSampleScreens
        /// <summary>
        /// Saves Course Sample Screens for this Course.
        /// </summary>
        /// <param name="sample screens">DataTable of course sample screens to save</param>
        public void SaveSampleScreens(DataTable filename)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@fileNames", filename, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.SaveSampleScreens]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCoursesAPI
        /// <summary>
        /// Return a list of courses for APIquery
        /// </summary>
        public static List<int> GetCoursesAPI(string type, string searchParam)
        {
            List<int> returnIDs = new List<int>();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", String.Empty, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@dataType", type, SqlDbType.NVarChar, 20, ParameterDirection.Input);
            // SEARCH PARAMETER
            // build the full text query
            FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
            string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

            // if it's not empty, use it; else make it a wildcard
            SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

            databaseObject.AddParameter("@searchParam", (type == "fts") ? formattedSearchQuery : searchParam, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.IdsForAPI]", true);
                while (sdr.Read())
                {
                    returnIDs.Add(Convert.ToInt32(sdr[0]));
                }
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return returnIDs;
        }
        #endregion

        #region SerializeSocialMediaProperties
        /// <summary>
        /// Serializes the collection of SocialMediaProperties into an XML string and
        /// sets that string as the value of this.SocialMedia. 
        /// </summary>
        public void SerializeSocialMediaProperties()
        {
            if (this.SocialMediaProperties == null || this.SocialMediaProperties.Count == 0)
            {
                this.SocialMedia = null;
                return;
            }

            XmlDocument xmlDocument = new XmlDocument();

            XmlNode socialMediaNode = xmlDocument.CreateNode(XmlNodeType.Element, "socialMedia", null);

            foreach (SocialMediaProperty socialMediaProperty in this.SocialMediaProperties)
            {
                XmlNode hrefNode = xmlDocument.CreateNode(XmlNodeType.Element, "href", null);

                XmlAttribute enrollmentRequired = xmlDocument.CreateAttribute("enrollmentRequired");
                enrollmentRequired.Value = socialMediaProperty.EnrollmentRequired.ToString();
                hrefNode.Attributes.Append(enrollmentRequired);

                XmlAttribute protocol = xmlDocument.CreateAttribute("protocol");
                protocol.Value = socialMediaProperty.Protocol;
                hrefNode.Attributes.Append(protocol);

                XmlAttribute iconType = xmlDocument.CreateAttribute("iconType");
                iconType.Value = socialMediaProperty.IconType;
                hrefNode.Attributes.Append(iconType);

                XmlCDataSection href = xmlDocument.CreateCDataSection(socialMediaProperty.Href);
                hrefNode.AppendChild(href);

                socialMediaNode.AppendChild(hrefNode);
            }

            xmlDocument.AppendChild(socialMediaNode);

            this.SocialMedia = xmlDocument.OuterXml;
        }
        #endregion

        #region GetExperts
        /// <summary>
        /// Gets a listing of user ids and names that are experts of this course.
        /// </summary>
        /// <returns>DataTable of user ids and names that are experts of this course</returns>
        public DataTable GetExperts(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetExperts]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetApprovers
        /// <summary>
        /// Gets a listing of user ids and names that are enrollment approvers of this course.
        /// </summary>
        /// <returns>DataTable of user ids and names that are enrollment approvers of this course</returns>
        public DataTable GetApprovers(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetApprovers]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetPrerequisites
        /// <summary>
        /// Gets a listing of course ids and titles that are prerequisites of this course.
        /// </summary>
        /// <returns>DataTable of course ids and titles that are prerequisites of this course</returns>
        public DataTable GetPrerequisites(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetPrerequisites]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetSampleScreens
        /// <summary>
        /// Gets a listing of sample screens of this course.
        /// </summary>
        /// <returns>DataTable of course filenames of sample screens of this course</returns>
        public DataTable GetSampleScreens()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetSampleScreens]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetLessons
        /// <summary>
        /// Gets a listing of lesson ids, titles, and order that belong to this course.
        /// </summary>
        /// <returns>DataTable of lesson ids, titles, and order that belong to this course</returns>
        public DataTable GetLessons(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetLessons]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCourseMaterials
        /// <summary>
        /// Gets a listing of course material ids and names that belong to this course.
        /// </summary>
        /// <returns>DataTable of course material ids and names that belong to this course</returns>
        public DataTable GetCourseMaterials(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetCourseMaterials]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetRulesetEnrollments
        /// <summary>
        /// Gets a listing of ruleset enrollment ids, labels, and priority that belong to this course.
        /// </summary>
        /// <returns>DataTable of ruleset enrollment ids, labels, and priority that belong to this course</returns>
        public DataTable GetRulesetEnrollments(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetRulesetEnrollments]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCertificates
        /// <summary>
        /// Gets a listing of certificate ids and names that belong to this course.
        /// </summary>
        /// <returns>DataTable of certificate ids and names that belong to this course</returns>
        public DataTable GetCertificates(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetCertificates]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetEmailNotifications
        /// <summary>
        /// Gets a listing of email notification ids and names that belong to this course.
        /// </summary>
        /// <returns>DataTable of email notification ids and names that belong to this course</returns>
        public DataTable GetEmailNotifications(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetEmailNotifications]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveWallModerators
        /// <summary>
        /// Saves moderators for course wall.
        /// </summary>
        /// <param name="users">DataTable of user ids</param>
        public void SaveWallModerators(DataTable users)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Moderators", users, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.SaveWallModerators]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetWallModerators
        /// <summary>
        /// Gets listing of moderators for course wall.
        /// </summary>
        /// <param name="searchParam">The search parameter.</param>
        /// <returns>DataTable.</returns>
        public DataTable GetWallModerators(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery;

                if (String.IsNullOrWhiteSpace(fullTextSearchQuery))
                { formattedSearchQuery = new SqlString("*"); }
                else
                { formattedSearchQuery = new SqlString(fullTextSearchQuery); }

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetWallModerators]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetUsersRating
        /// <summary>
        /// Gets a specific user's rating for this course.
        /// </summary>
        /// <param name="idCourse">Course Id</param>
        public int GetUsersRating(int idUser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@rating", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.GetUsersRating]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@rating"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Course(s).
        /// </summary>
        /// <param name="deletees">DataTable of courses to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Courses", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForCatalogSelectList
        /// <summary>
        /// Gets a listing of course ids and titles to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndNamesForCatalogSelectList(int idCatalog, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCatalog", idCatalog, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.IdsAndNamesForCatalogSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForCoursePrerequisiteSelectList
        /// <summary>
        /// Gets a listing of course ids and titles to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndNamesForCoursePrerequisiteSelectList(int idCourse, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.IdsAndNamesForCoursePrerequisiteSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForCouponCodeSelectList
        /// <summary>
        /// Gets a listing of course ids and titles to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndNamesForCouponCodeSelectList(int idCouponCode, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCouponCode", idCouponCode, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.IdsAndNamesForCouponCodeSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForLearningPathSelectList
        /// <summary>
        /// Gets a listing of course ids and titles to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndNamesForLearningPathSelectList(int idLearningPath, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idLearningPath", idLearningPath, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.IdsAndNamesForLearningPathSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForSelectList
        /// <summary>
        /// Gets a listing of course ids and titles to populate into a select list.
        /// </summary>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndNamesForSelectList(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.IdsAndNamesForSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesNotMembersOfAnyCatalog
        /// <summary>
        /// Gets a listing of course ids and titles that are not members of any catalog.
        /// Used to populate the root catalog on the catalog tree.
        /// </summary>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndNamesNotMembersOfAnyCatalog(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.IdsAndNamesNotMembersOfAnyCatalog]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region CourseCodeListNotExistForCurrentSite
        /// <summary>
        /// Gets a listing of Couse Code
        /// </summary>
        public static DataTable CourseCodeListNotExistForCurrentSite(DataTable CourseCodeTable)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@CourseCodeFile", CourseCodeTable, SqlDbType.Structured, null, ParameterDirection.Input);


                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.CourseCodeListNotExistForCurrentSite]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveRating
        /// <summary>
        /// Saves a user's rating for a course.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="rating">the rating</param>
        public static void SaveRating(int idCourse, int rating)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@rating", rating, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.SaveRating]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ResetRating
        /// <summary>
        /// Resets a course's rating.
        /// </summary>
        /// <param name="idCourse">course id</param>
        public static void ResetRating(int idCourse)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.ResetRating]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetFeedMessages
        /// <summary>
        /// Gets a listing of course feed messages for a course.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="pageSize">number of records per page</param>
        /// <param name="dtQuery">the baseline date/time for the query</param>
        /// <param name="getMessagesNewerThanDtQuery">get messages that are newer than the baseline date/time</param>
        /// <param name="getApprovedMessagesOnly">get only messages that have been moderator approved?</param>
        /// <param name="dtLastRecord">out param that returns the last record's timestamp</param>
        /// <returns>DataTable of course feed messages and an out param of the last record's timestamp.</returns>
        public static DataTable GetFeedMessages(int idCourse, int pageSize, DateTime dtQuery, bool getMessagesNewerThanDtQuery, bool getApprovedMessagesOnly, out DateTime dtLastRecord)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@pageSize", pageSize, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@dtQuery", dtQuery, SqlDbType.DateTime, 8, ParameterDirection.Input);
                databaseObject.AddParameter("@getMessagesNewerThanDtQuery", getMessagesNewerThanDtQuery, SqlDbType.Bit, 1, ParameterDirection.Input);
                databaseObject.AddParameter("@getApprovedMessagesOnly", getApprovedMessagesOnly, SqlDbType.Bit, 1, ParameterDirection.Input);
                databaseObject.AddParameter("@dtLastRecord", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetFeedMessages]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                dtLastRecord = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtLastRecord"].Value);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCourseIdByShortcode
        /// <summary>
        /// Gets a course ID based on the course shortcode
        /// </summary>
        /// <param name="shortcode">course shortcode</param>
        /// <returns>course id</returns>
        public static int GetCourseIdByShortcode(string shortcode)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@shortcode", shortcode, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCourse", null, SqlDbType.Int, 4, ParameterDirection.Output);
            
            try
            {
                databaseObject.ExecuteNonQuery("[Course.GetCourseIdByShortcode]", true);
                
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
               
                return Convert.ToInt32(databaseObject.Command.Parameters["@idCourse"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves course data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved course</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@coursecode", this.CourseCode, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@avatar", this.Avatar, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            if (this.Cost == null)
            { databaseObject.AddParameter("@cost", DBNull.Value, SqlDbType.Float, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@cost", this.Cost, SqlDbType.Float, 8, ParameterDirection.Input); }

            if (this.Credits == null)
            { databaseObject.AddParameter("@credits", DBNull.Value, SqlDbType.Float, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@credits", this.Credits, SqlDbType.Float, 8, ParameterDirection.Input); }

            if (this.EstimatedLengthInMinutes == null)
            { databaseObject.AddParameter("@minutes", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@minutes", this.EstimatedLengthInMinutes, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@shortDescription", this.ShortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@longDescription", this.LongDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@objectives", this.Objectives, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@socialMedia", this.SocialMedia, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            if (this.IsPublished == null)
            { databaseObject.AddParameter("@isPublished", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isPublished", this.IsPublished, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsClosed == null)
            { databaseObject.AddParameter("@isClosed", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isClosed", this.IsClosed, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsLocked == null)
            { databaseObject.AddParameter("@isLocked", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isLocked", this.IsLocked, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsPrerequisiteAny == null)
            { databaseObject.AddParameter("@isPrerequisiteAny", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isPrerequisiteAny", this.IsPrerequisiteAny, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsFeedActive == null)
            { databaseObject.AddParameter("@isFeedActive", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isFeedActive", this.IsFeedActive, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsFeedModerated == null)
            { databaseObject.AddParameter("@isFeedModerated", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isFeedModerated", this.IsFeedModerated, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsFeedOpenSubscription == null)
            { databaseObject.AddParameter("@isFeedOpenSubscription", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isFeedOpenSubscription", this.IsFeedOpenSubscription, SqlDbType.Bit, 1, ParameterDirection.Input); }

            databaseObject.AddParameter("@disallowRating", this.DisallowRating, SqlDbType.Bit, 1, ParameterDirection.Input);

            databaseObject.AddParameter("@forceLessonCompletionInOrder", this.ForceLessonCompletionInOrder, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@requireFirstLessonToBeCompletedBeforeOthers", this.RequireFirstLessonToBeCompletedBeforeOthers, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@lockLastLessonUntilOthersCompleted", this.LockLastLessonUntilOthersCompleted, SqlDbType.Bit, 1, ParameterDirection.Input);

            if (this.SelfEnrollmentIsOneTimeOnly == null)
            { databaseObject.AddParameter("@selfEnrollmentIsOneTimeOnly", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@selfEnrollmentIsOneTimeOnly", this.SelfEnrollmentIsOneTimeOnly, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.SelfEnrollmentDueInterval == null)
            { databaseObject.AddParameter("@selfEnrollmentDueInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@selfEnrollmentDueInterval", this.SelfEnrollmentDueInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@selfEnrollmentDueTimeframe", this.SelfEnrollmentDueTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.SelfEnrollmentExpiresFromStartInterval == null)
            { databaseObject.AddParameter("@selfEnrollmentExpiresFromStartInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@selfEnrollmentExpiresFromStartInterval", this.SelfEnrollmentExpiresFromStartInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@selfEnrollmentExpiresFromStartTimeframe", this.SelfEnrollmentExpiresFromStartTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.SelfEnrollmentExpiresFromFirstLaunchInterval == null)
            { databaseObject.AddParameter("@selfEnrollmentExpiresFromFirstLaunchInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@selfEnrollmentExpiresFromFirstLaunchInterval", this.SelfEnrollmentExpiresFromFirstLaunchInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@selfEnrollmentExpiresFromFirstLaunchTimeframe", this.SelfEnrollmentExpiresFromFirstLaunchTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@searchTags", this.SearchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            if (this.IsSelfEnrollmentApprovalRequired == null)
            { databaseObject.AddParameter("@isSelfEnrollmentApprovalRequired", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isSelfEnrollmentApprovalRequired", this.IsSelfEnrollmentApprovalRequired, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsApprovalAllowedByCourseExperts == null)
            { databaseObject.AddParameter("@isApprovalAllowedByCourseExperts", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isApprovalAllowedByCourseExperts", this.IsApprovalAllowedByCourseExperts, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsApprovalAllowedByUserSupervisor == null)
            { databaseObject.AddParameter("@isApprovalAllowedByUserSupervisor", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isApprovalAllowedByUserSupervisor", this.IsApprovalAllowedByUserSupervisor, SqlDbType.Bit, 1, ParameterDirection.Input); }

            databaseObject.AddParameter("@shortcode", this.Shortcode, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved course
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idCourse"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SaveAvatar
        /// <summary>
        /// Saves course's avatar path to database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved user</returns>
        private void _SaveAvatar(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@avatar", this.Avatar, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.SaveAvatar]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified course's properties.
        /// </summary>
        /// <param name="idCourse">Course Id</param>
        private void _Details(int idCourse)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@title", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@coursecode", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@revcode", null, SqlDbType.NVarChar, 32, ParameterDirection.Output);
            databaseObject.AddParameter("@avatar", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@cost", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@credits", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@minutes", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@shortDescription", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@longDescription", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@objectives", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@socialMedia", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@isPublished", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isClosed", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isLocked", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isPrerequisiteAny", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isFeedActive", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isFeedModerated", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isFeedOpenSubscription", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtModified", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@order", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@disallowRating", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@rating", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@votes", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@forceLessonCompletionInOrder", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@requireFirstLessonToBeCompletedBeforeOthers", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@lockLastLessonUntilOthersCompleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@selfEnrollmentIsOneTimeOnly", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@selfEnrollmentDueInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@selfEnrollmentDueTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@selfEnrollmentExpiresFromStartInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@selfEnrollmentExpiresFromStartTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@selfEnrollmentExpiresFromFirstLaunchInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@selfEnrollmentExpiresFromFirstLaunchTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@searchTags", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@isCallingUserEnrolled", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isSelfEnrollmentApprovalRequired", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isApprovalAllowedByCourseExperts", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isApprovalAllowedByUserSupervisor", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@shortcode", null, SqlDbType.NVarChar, 10, ParameterDirection.Output);
            databaseObject.AddParameter("@isSelfEnrollmentLockedByPrerequisites", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isMultipleSelfEnrollmentEligible", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Course.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idCourse;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.CourseCode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@coursecode"].Value);
                this.RevCode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@revcode"].Value);
                this.Avatar = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@avatar"].Value);
                this.Cost = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@cost"].Value);
                this.Credits = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@credits"].Value);
                this.EstimatedLengthInMinutes = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@minutes"].Value);
                this.ShortDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@shortDescription"].Value);
                this.LongDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@longDescription"].Value);
                this.Objectives = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@objectives"].Value);
                this.SocialMedia = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@socialMedia"].Value);
                this.IsPublished = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isPublished"].Value);
                this.IsClosed = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isClosed"].Value);
                this.IsLocked = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isLocked"].Value);
                this.IsPrerequisiteAny = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isPrerequisiteAny"].Value);
                this.IsFeedActive = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isFeedActive"].Value);
                this.IsFeedModerated = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isFeedModerated"].Value);
                this.IsFeedOpenSubscription = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isFeedOpenSubscription"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.Order = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@order"].Value);
                this.DisallowRating = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@disallowRating"].Value);
                this.Rating = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@rating"].Value);
                this.Votes = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@votes"].Value);
                this.ForceLessonCompletionInOrder = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@forceLessonCompletionInOrder"].Value);
                this.RequireFirstLessonToBeCompletedBeforeOthers = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@requireFirstLessonToBeCompletedBeforeOthers"].Value);
                this.LockLastLessonUntilOthersCompleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@lockLastLessonUntilOthersCompleted"].Value);
                this.SelfEnrollmentIsOneTimeOnly = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@selfEnrollmentIsOneTimeOnly"].Value);
                this.SelfEnrollmentDueInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@selfEnrollmentDueInterval"].Value);
                this.SelfEnrollmentDueTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@selfEnrollmentDueTimeframe"].Value);
                this.SelfEnrollmentExpiresFromStartInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@selfEnrollmentExpiresFromStartInterval"].Value);
                this.SelfEnrollmentExpiresFromStartTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@selfEnrollmentExpiresFromStartTimeframe"].Value);
                this.SelfEnrollmentExpiresFromFirstLaunchInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@selfEnrollmentExpiresFromFirstLaunchInterval"].Value);
                this.SelfEnrollmentExpiresFromFirstLaunchTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@selfEnrollmentExpiresFromFirstLaunchTimeframe"].Value);
                this.SearchTags = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@searchTags"].Value);
                this.IsCallingUserEnrolled = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isCallingUserEnrolled"].Value);
                this.IsSelfEnrollmentApprovalRequired = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isSelfEnrollmentApprovalRequired"].Value);
                this.IsApprovalAllowedByCourseExperts = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isApprovalAllowedByCourseExperts"].Value);
                this.IsApprovalAllowedByUserSupervisor = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isApprovalAllowedByUserSupervisor"].Value);
                this.Shortcode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@shortcode"].Value);
                this.IsSelfEnrollmentLockedByPrerequisites = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isSelfEnrollmentLockedByPrerequisites"].Value);
                this.IsMultipleSelfEnrollmentEligible = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isMultipleSelfEnrollmentEligible"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a course into a collection.
        /// </summary>
        /// <param name="idCourse">course id</param>
        private void _GetPropertiesInLanguages(int idCourse)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["title"].ToString(),
                            sdr["shortDescription"].ToString(),
                            sdr["longDescription"].ToString(),
                            sdr["objectives"].ToString(),
                            sdr["searchTags"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetSocialMediaProperties
        /// <summary>
        /// Loads the social media properties for a course into a collection from its socialMedia XML.
        /// </summary>
        /// <param name="idCourse">course id</param>
        private void _GetSocialMediaProperties(int idCourse)
        {
            // if there are no social media propeties, just exit
            if (String.IsNullOrWhiteSpace(this.SocialMedia))
            { return; }

            try
            {
                XmlDocument socialMediaXml = new XmlDocument();
                socialMediaXml.LoadXml(this.SocialMedia);

                XmlNodeList hrefNodes = socialMediaXml.GetElementsByTagName("href");

                foreach (XmlNode hrefNode in hrefNodes)
                {
                    string iconType = hrefNode.Attributes["iconType"].Value;
                    string href = hrefNode.InnerText;
                    string protocol = hrefNode.Attributes["protocol"].Value; ;
                    bool enrollmentRequired = Convert.ToBoolean(hrefNode.Attributes["enrollmentRequired"].Value);

                    SocialMediaProperty socialMediaProperty = new SocialMediaProperty(iconType, href, protocol, enrollmentRequired);
                    this.SocialMediaProperties.Add(socialMediaProperty);
                }
            }
            catch
            {
                throw;
            }
            finally
            { }
        }
        #endregion
        #endregion
    }
}