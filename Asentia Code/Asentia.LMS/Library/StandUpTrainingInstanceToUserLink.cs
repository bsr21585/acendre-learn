﻿using Asentia.Common;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Asentia.UMS;

namespace Asentia.LMS.Library
{
    [Serializable]
    public class StandUpTrainingInstanceToUserLink
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public StandUpTrainingInstanceToUserLink()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idStandupTrainingInstance">Standup Training Instance Id</param>
        /// <param name="idUser">User Id</param>
        public StandUpTrainingInstanceToUserLink(int idStandUpTrainingInstance, int idUser)
        {
            this._Details(idStandUpTrainingInstance, idUser);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[StandupTrainingInstanceToUserLink.GetGrid]";

        /// <summary>
        /// Id
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Standup Training Instance Id
        /// </summary>
        public int IdStandupTrainingInstance;

        /// <summary>
        /// User Id
        /// </summary>
        public int IdUser;

        /// <summary>
        /// Completion Status
        /// </summary>
        public int CompletionStatus;

        /// <summary>
        /// Score
        /// </summary>
        public int Score;

        /// <summary>
        /// IsWaitingList
        /// </summary>
        public bool IsWaitingList;

        /// <summary>
        /// The "meeting", "webinar", or "training" registrant key that represents the registered
        /// attendee when integrated with GTM, GTW, GTT, or WebEx. This value comes from the
        /// platform's API. Only used in GTW, GTT, and WebEx.
        /// </summary>
        public Int64? RegistrantKey;

        /// <summary>
        /// The email address that was used for the user upon registration to the "meeting", 
        /// "webinar", or "training" when integrated with GTM, GTW, GTT, or WebEx. This value 
        /// comes from the user's profile, or if an email address does not exist for the user
        /// we make one up. We store this instead of pulling it dynamically to guarantee sync
        /// with the web-meeting platform's API as user profile information can change.
        /// </summary>
        public string RegistrantEmail;

        /// <summary>
        /// The "attendee specific" join url for the "meeting", "webinar", or "training" when integrated
        /// with GTM, GTW, GTT, or WebEx. This value comes from the platform's API.
        /// GTM only uses a generic join url (stored with the standup training instance). GTW and GTT use 
        /// only an "attendee specific" join url (stored here). WebEx uses an "attendee specific" join url, 
        /// but also has a generic join url.
        /// </summary>
        public string JoinUrl;

        /// <summary>
        /// Waitlist Order.
        /// The order the user is in on the waitlist.
        /// </summary>
        public int? WaitlistOrder;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;
        #endregion

        #region Methods
        #region _GetManageRosterUserList
        /// <summary>
        /// Populates this object's properties with the specified standup training's properties.
        /// </summary>
        /// <param name="idStandupTrainingInstance">Standup Training Instance Id</param>
        public DataTable _GetManageRosterUserList(int idStandupTrainingInstance, bool isWaitingList)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isWaitingList", isWaitingList, SqlDbType.Bit, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[StandUpTrainingInstanceToUserLink.GetManageRosterUserList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region UpdateUsers
        /// <summary>
        /// Method to update a standup training instance's roster.
        /// </summary>
        /// <param name="idStandupTrainingInstance">standup training instance id</param>
        /// <param name="updatedUsersRecord">datatable representing the roster</param>
        public void UpdateUsers(int idStandupTrainingInstance, DataTable updatedUsersRecord)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@updatedUsersRecord", updatedUsersRecord, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[StandUpTrainingInstanceToUserLink.UpdateUsers]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Method to delete the user from the ILT session roster.
        /// </summary>
        public void Delete()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", this.IdStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[StandUpTrainingInstanceToUserLink.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified standup training's properties.
        /// </summary>
        /// <param name="idStandupTrainingInstance">Standup Training Instance Id</param>
        private void _Details(int idStandupTrainingInstance, int idUser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandUpTrainingInstanceToUserLink", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@completionStatus", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@score", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@isWaitingList", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@registrantKey", null, SqlDbType.BigInt, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@registrantEmail", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@joinUrl", null, SqlDbType.NVarChar, 1024, ParameterDirection.Output);
            databaseObject.AddParameter("@waitlistOrder", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[StandUpTrainingInstanceToUserLink.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idStandUpTrainingInstanceToUserLink"].Value);
                this.IdUser = idUser;
                this.IdStandupTrainingInstance = idStandupTrainingInstance;
                this.CompletionStatus = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@completionStatus"].Value);
                this.Score = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@score"].Value);
                this.IsWaitingList = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isWaitingList"].Value);
                this.RegistrantKey = AsentiaDatabase.ParseDbParamNullableInt64(databaseObject.Command.Parameters["@registrantKey"].Value);
                this.RegistrantEmail = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@registrantEmail"].Value);
                this.JoinUrl = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@joinUrl"].Value);
                this.WaitlistOrder = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@waitlistOrder"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static methods
        #region GetNewRosterUserId
        /// <summary>
        /// if the user is not enrolled in the standup training session, returns the user id,
        /// otherwise returns 0
        /// </summary>
        /// <param name="idStandupTrainingInstance">Standup Training Instance Id</param>
        /// <param name="username">username</param>
        public static int GetNewRosterUserId(int idStandupTrainingInstance, string username)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@username", username, SqlDbType.NVarChar, 255, ParameterDirection.InputOutput);
            
            int idUser = 0;

            try
            {
                databaseObject.ExecuteNonQuery("[StandUpTrainingInstanceToUserLink.IsUserEnrolled]", true);
                

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                idUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);

                return idUser;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region DropUserFromSession
        /// <summary>
        /// Drops a user from a session
        /// </summary>
        /// <param name="idStandupTrainingInstance">ILT session id</param>
        /// <param name="idUser">user id</param>
        /// <param name="idSite">site id</param>
        public static void DropUserFromSession(int idStandupTrainingInstance, int idUser, int idSite)
        {
            AsentiaSessionState.IdSite = idSite;
            AsentiaSessionState.IdSiteUser = 1;
            AsentiaSessionState.UserCulture = "en-US";

            // DROP THE SESSION
            StandupTrainingInstance sessionObject = new StandupTrainingInstance(idStandupTrainingInstance);
            StandUpTrainingInstanceToUserLink sessionToUserLinkObject = new StandUpTrainingInstanceToUserLink(idStandupTrainingInstance, idUser);

            // loop through session meeting times to determine if the session has started
            bool isSessionStarted = false;

            foreach (Asentia.LMS.Library.StandupTrainingInstance.MeetingTimeProperty meetingTime in sessionObject.MeetingTimes)
            {
                if (meetingTime.DtStart < DateTime.UtcNow) { isSessionStarted = true; }
            }

            // first, if the session has not already occurred and this is a GTT, GTW, or WebEx session, use the api to delete the registrant record
            if (
                    !isSessionStarted &&
                    (sessionObject.Type == StandupTrainingInstance.MeetingType.GoToTraining
                    || sessionObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar
                    || sessionObject.Type == StandupTrainingInstance.MeetingType.WebEx
                   ))
            {

                // note that any errors thrown from this method will be caught by the method and buried, they arent showstopper errors
                if (sessionToUserLinkObject.RegistrantKey != null)
                { StandUpTrainingInstanceToUserLink.DeleteWebMeetingRegistrant(Convert.ToInt64(sessionToUserLinkObject.RegistrantKey), idStandupTrainingInstance); }

            }

            sessionToUserLinkObject.Delete();

            // end the session
            AsentiaSessionState.EndSession(false);
        }
        #endregion
        
        #region DeleteWebMeetingRegistrant
        /// <summary>
        /// Deletes a web meeting registrant from GTW, GTT, or WebEx by connecting to the appropriate API.
        /// </summary>
        /// <param name="registrantKey">the registrant key of the user</param>
        public static void DeleteWebMeetingRegistrant(Int64 registrantKey, int idStandupTrainingInstance)
        {
            try // this is wrapped in a try/catch because we dont want to die on error because an error here doesn't matter much
            {
                AsentiaAESEncryption cryptoProvider = new AsentiaAESEncryption();
                string organizerUsername = null;
                string organizerPassword = null;

                StandupTrainingInstance sessionObject = new StandupTrainingInstance(idStandupTrainingInstance);

                if (sessionObject.IdWebMeetingOrganizer != null && sessionObject.IdWebMeetingOrganizer > 0)
                {
                    Asentia.UMS.Library.WebMeetingOrganizer webMeetingOrganizer = new Asentia.UMS.Library.WebMeetingOrganizer((int)sessionObject.IdWebMeetingOrganizer);
                    organizerUsername = webMeetingOrganizer.Username;
                    organizerPassword = cryptoProvider.Decrypt(webMeetingOrganizer.Password);
                }

                if (sessionObject.Type == StandupTrainingInstance.MeetingType.GoToTraining) // GTT
                {
                    if (organizerUsername == null)
                    {
                        GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_USERNAME),
                                                                     cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PASSWORD)),
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                        gttApi.DeleteRegistrant(Convert.ToInt64(sessionObject.IntegratedObjectKey), registrantKey);
                    }
                    else
                    {
                        GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                     organizerUsername,
                                                                     organizerPassword,
                                                                     AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                        gttApi.DeleteRegistrant(Convert.ToInt64(sessionObject.IntegratedObjectKey), registrantKey);
                    }
                }
                else if (sessionObject.Type == StandupTrainingInstance.MeetingType.GoToWebinar) // GTW
                {
                    if (organizerUsername == null)
                    {
                        GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_USERNAME),
                                                                   cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PASSWORD)),
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                        gtwApi.DeleteRegistrant(Convert.ToInt64(sessionObject.IntegratedObjectKey), registrantKey);
                    }
                    else
                    {
                        GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                   organizerUsername,
                                                                   organizerPassword,
                                                                   AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                        gtwApi.DeleteRegistrant(Convert.ToInt64(sessionObject.IntegratedObjectKey), registrantKey);
                    }
                }
                else if (sessionObject.Type == StandupTrainingInstance.MeetingType.WebEx) // WebEx
                {
                    if (organizerUsername == null)
                    {
                        WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_USERNAME),
                                                       cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PASSWORD)));

                        wbxApi.DeleteMeetingRegistrant(registrantKey);
                    }
                    else
                    {
                        WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                       organizerUsername,
                                                       organizerPassword);

                        wbxApi.DeleteMeetingRegistrant(registrantKey);
                    }
                }
                else
                { }
            }
            catch // just bury the error
            { }
        }
        #endregion
        #endregion
    }
}
