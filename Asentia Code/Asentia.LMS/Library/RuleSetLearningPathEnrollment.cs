﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Xml;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    [Serializable]
    public class RuleSetLearningPathEnrollment
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public RuleSetLearningPathEnrollment()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idRuleSetLearningPathEnrollment">RuleSet Learning Path Enrollment Id</param>
        public RuleSetLearningPathEnrollment(int idRuleSetLearningPathEnrollment)
        {
            this.LanguageSpecificProperties = new ArrayList();

            this._Details(idRuleSetLearningPathEnrollment);
            this._GetPropertiesInLanguages(idRuleSetLearningPathEnrollment);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[RuleSetLearningPathEnrollment.GetGrid]";

        /// <summary>
        /// Rule Set Learning Path Enrollment Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Learning Path Id.
        /// <seealso cref="LearningPath" />
        /// </summary>
        public int IdLearningPath;

        /// <summary>
        /// Timezone Id.
        /// </summary>
        public int IdTimezone;

        /// <summary>
        /// Priority.
        /// </summary>
        public int Priority;

        /// <summary>
        /// Label.
        /// </summary>
        public string Label;

        /// <summary>
        /// Start Date.
        /// </summary>
        public DateTime DtStart;

        /// <summary>
        /// End Date.
        /// </summary>
        public DateTime? DtEnd;

        /// <summary>
        /// Delay Interval.
        /// </summary>
        public int? DelayInterval;

        /// <summary>
        /// Delay Timeframe.
        /// </summary>
        public string DelayTimeframe;

        /// <summary>
        /// Due Interval.
        /// </summary>
        public int? DueInterval;

        /// <summary>
        /// Due Timeframe.
        /// </summary>
        public string DueTimeframe;

        /// <summary>
        /// Expires from Start Interval.
        /// </summary>
        public int? ExpiresFromStartInterval;

        /// <summary>
        /// Expires from Start Timeframe.
        /// </summary>
        public string ExpiresFromStartTimeframe;

        /// <summary>
        /// Expires from First Launch Interval.
        /// </summary>
        public int? ExpiresFromFirstLaunchInterval;

        /// <summary>
        /// Expires from First Launch Timeframe.
        /// </summary>
        public string ExpiresFromFirstLaunchTimeframe;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// Parent Learning Path Ruleset Enrollment Id.
        /// </summary>
        public int IdParentRuleSetLearningPathEnrollment;
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Label;

            public LanguageSpecificProperty(string langString, string label)
            {
                this.LangString = langString;
                this.Label = label;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves ruleset learning path enrollment data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves ruleset learning path enrollment data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this ruleset learning path enrollment.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="label">label</param>
        public void SaveLang(string languageString, string label)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureRulesetLearningPathEnrollmentSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetLearningPathEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@label", label, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetLearningPathEnrollment.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion        
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes RuleSet Learning Path Enrollment(s).
        /// </summary>
        /// <param name="deletees">DataTable of ruleset learning path enrollments to delete</param>
        /// <param name="idLearningPath">id of learning path the ruleset learning path enrollments belong to</param>
        public static void Delete(DataTable deletees, int idLearningPath)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPath", idLearningPath, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@RuleSetLearningPathEnrollments", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetLearningPathEnrollment.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region AssignEnrollments
        /// <summary>
        /// Assigns RuleSet Learning Path Enrollment(s) to users.
        /// </summary>
        /// <param name="learningPaths">DataTable of learning paths to assign ruleset enrollments for</param>
        /// <param name="filters">DataTable of filters (user or group ids, see filterBy param) to assign ruleset enrollments for</param>
        /// <param name="filterBy">the type of ids the filters DataTable contains, "user" or "group"</param>
        public static void AssignEnrollments(DataTable learningPaths, DataTable filters, string filterBy)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@LearningPaths", learningPaths, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@Filters", filters, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@filterBy", filterBy, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetLearningPathEnrollment.AssignEnrollments]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region RevokeEnrollments
        /// <summary>
        /// Revokse RuleSet Learning Path Enrollment(s) from users.
        /// 
        /// DO NOT EVER CALL THIS FROM PROCEDURAL CODE!
        /// 
        /// This is only here to show the existence of a "Revoke" procedure. The calls to it are made from within
        /// the "AssignEnrollments" procedure.
        /// </summary>
        /// <param name="learningPaths">DataTable of learning paths to assign ruleset enrollments for</param>
        /// <param name="filters">DataTable of filters (user or group ids, see filterBy param) to assign ruleset enrollments for</param>
        /// <param name="filterBy">the type of ids the filters DataTable contains, "user" or "group"</param>
        public static void RevokeEnrollments(DataTable learningPaths, DataTable filters, string filterBy)
        {
            // throw an exception so that this cannot be executed directly
            throw new AsentiaException(_GlobalResources.ProcedureRulesetLearningPathEnrollmentRevokeEnrollmentsCannotBeCalledDirectlyFromProceduralCode);

            // THE CODE BELOW WILL NEVER EXECUTE

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@LearningPaths", learningPaths, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@Filters", filters, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@filterBy", filterBy, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetLearningPathEnrollment.RevokeEnrollments]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region UpdateOrder
        /// <summary>
        /// Method to update the order of ruleset learning path enrollments.
        /// </summary>
        /// <param name="enrollmentsWithOrdering"></param>
        public static void UpdateOrder(DataTable rulesetEnrollmentOrdering)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@RuleSetEnrollmentOrdering", rulesetEnrollmentOrdering, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RulesetLearningPathEnrollment.UpdateOrder]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList
        /// <summary>
        /// Gets a listing of learning path ids and titles to populate into a 
        /// "replicate to other learning paths" checkbox list for a ruleset 
        /// learning path enrollment.
        /// </summary>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of learning path ids and names.</returns>
        public static DataTable IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList(int idRuleSetLearningPathEnrollment, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idRuleSetLearningPathEnrollment", idRuleSetLearningPathEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[RuleSetLearningPathEnrollment.IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion

        #region Replicate
        /// <summary>
        /// Method to replicate a ruleset learning path enrollment to other learning paths.
        /// </summary>
        /// <param name="idRuleSetLearningPathEnrollmentToReplicate">ruleset learning path enrollment id to replicate</param>
        /// <param name="idLearningPathsToReplicateTo">datatable of learning path ids to replicate ruleset learning path enrollment to</param>
        /// <param name="idLearningPathsToUnSync">datatable of learning path ids to unsync ruleset learning path enrollment replication from</param>
        public static void Replicate(int idRuleSetLearningPathEnrollmentToReplicate, DataTable idLearningPathsToReplicateTo, DataTable idLearningPathsToUnSync)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetLearningPathEnrollmentToReplicate", idRuleSetLearningPathEnrollmentToReplicate, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idLearningPathsToReplicateTo", idLearningPathsToReplicateTo, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@idLearningPathsToUnSync", idLearningPathsToUnSync, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetLearningPathEnrollment.Replicate]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves ruleset learning path enrollment data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved ruleset learning path enrollment</returns>
        public int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetLearningPathEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idLearningPath", this.IdLearningPath, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@label", this.Label, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);

            if (this.DtEnd == null)
            { databaseObject.AddParameter("@dtEnd", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dtEnd", this.DtEnd, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            if (this.DelayInterval == null)
            { databaseObject.AddParameter("@delayInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@delayInterval", this.DelayInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@delayTimeframe", this.DelayTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.DueInterval == null)
            { databaseObject.AddParameter("@dueInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dueInterval", this.DueInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@dueTimeframe", this.DueTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromStartInterval == null)
            { databaseObject.AddParameter("@expiresFromStartInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromStartInterval", this.ExpiresFromStartInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromStartTimeframe", this.ExpiresFromStartTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromFirstLaunchInterval == null)
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", this.ExpiresFromFirstLaunchInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", this.ExpiresFromFirstLaunchTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idParentRuleSetLearningPathEnrollment", this.IdParentRuleSetLearningPathEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetLearningPathEnrollment.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved ruleset learning path enrollment
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idRuleSetLearningPathEnrollment"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified LearningPath's properties.
        /// </summary>
        /// <param name="idRuleSetLearningPathEnrollment">RuleSet Learning Path Enrollment Id</param>
        private void _Details(int idRuleSetLearningPathEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetLearningPathEnrollment", idRuleSetLearningPathEnrollment, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idLearningPath", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimezone", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@priority", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@label", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@dtStart", null, SqlDbType.DateTime, null, ParameterDirection.Output);
            databaseObject.AddParameter("@dtEnd", null, SqlDbType.DateTime, null, ParameterDirection.Output);
            databaseObject.AddParameter("@delayInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@delayTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dueInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dueTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idParentRuleSetLearningPathEnrollment", null, SqlDbType.Int, 4, ParameterDirection.Output);
            
            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetLearningPathEnrollment.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idRuleSetLearningPathEnrollment;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdLearningPath = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idLearningPath"].Value);
                this.IdTimezone = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idTimezone"].Value);
                this.Priority = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@priority"].Value);
                this.Label = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@label"].Value);
                this.DtStart = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtStart"].Value);
                this.DtEnd = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtEnd"].Value);
                this.DelayInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@delayInterval"].Value);
                this.DelayTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@delayTimeframe"].Value);
                this.DueInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@dueInterval"].Value);
                this.DueTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@dueTimeframe"].Value);
                this.ExpiresFromStartInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromStartInterval"].Value);
                this.ExpiresFromStartTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromStartTimeframe"].Value);
                this.ExpiresFromFirstLaunchInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromFirstLaunchInterval"].Value);
                this.ExpiresFromFirstLaunchTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromFirstLaunchTimeframe"].Value);
                this.IdParentRuleSetLearningPathEnrollment = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idParentRuleSetLearningPathEnrollment"].Value);
                
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a ruleset learning path enrollment into a collection.
        /// </summary>
        /// <param name="idRuleSetLearningPathEnrollment">RuleSet Learning Path Enrollment Id</param>
        private void _GetPropertiesInLanguages(int idRuleSetLearningPathEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetLearningPathEnrollment", idRuleSetLearningPathEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[RuleSetLearningPathEnrollment.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["label"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}