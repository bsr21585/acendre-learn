﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class GoToWebinarAPI
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// No arguments passed, and no authentication call. 
        /// AuthenticateUser would need to be called after instansiation using this constructor if
        /// API methods are to be called.
        /// </summary>
        public GoToWebinarAPI()
        { ; }

        /// <summary>
        /// Constructor that takes credentials and authenticates the user for API calls.
        /// </summary>
        /// <param name="consumerKey">GTW application key</param>
        /// <param name="organizerLogin">GTW organizer login</param>
        /// <param name="organizerPassword">GTW organizer password</param>
        public GoToWebinarAPI(string consumerKey, string organizerLogin, string organizerPassword, string consumerSecret)
        {
            this.AuthenticateUser(consumerKey, organizerLogin, organizerPassword, consumerSecret);
        }
        #endregion

        #region Properties
        public GTW_Organizer_Response OrganizerInformation { get { return this._Organizer; } }

        public static readonly string STARTER_PLAN_NAME = "Starter";
        public static readonly string PRO_PLAN_NAME = "Pro";
        public static readonly string PLUS_PLAN_NAME = "Plus";

        public static readonly int STARTER_PLAN_MAXATTENDEES = 100;
        public static readonly int PRO_PLAN_MAXATTENDEES = 500;
        public static readonly int PLUS_PLAN_MAXATTENDEES = 2000;
        #endregion

        #region Private Properties
        private string _AccessToken;
        private Int64 _OrganizerKey = 0;
        private GTW_Organizer_Response _Organizer;        
        #endregion

        #region Structs
        #region GTW_Organizer_Response
        public struct GTW_Organizer_Response
        {
            // user personal information
            public string FirstName;
            public string LastName;
            public string Email;            
        }
        #endregion

        #region GTW_Webinar_Response
        public struct GTW_Webinar_Response
        {
            public Int64 WebinarKey; // key is the main identifier
            public string WebinarId; // not sure why this exists if "key" is the identifier, but it does

            public int NumberOfRegistrants;
            public bool InSession;
            public string Subject;
            public string Description;

            public DateTime StartDate;
            public DateTime EndDate;
            public string Timezone;

            public string RegistrationUrl;                      
        }
        #endregion

        #region GTW_Registrant_Response
        public struct GTW_Registrant_Response
        {
            // list of Registrant objects returned
            public List<Registrant> Registrants;

            // Registrant object
            public class Registrant
            {
                public Int64 WebinarKey;
                public Int64 RegistrantKey;
                public string FirstName;
                public string LastName;
                public string Email;
                public DateTime RegistrationDate;
                public string Status;
                public string JoinUrl;
            }
        }
        #endregion

        #region GTW_Attendee_Response
        public struct GTW_Attendee_Response
        {
            // list of Attendee objects returned
            public List<Attendee> Attendees;

            // Attendee object
            public class Attendee
            {
                public Int64 WebinarKey;
                public Int64 RegistrantKey;
                public Int64 SessionKey; // this is not needed since all webinars/sessions are 1 to 1, but let's grab it anyway
                public string FirstName;
                public string LastName;
                public string Email;
                public DateTime JoinTime;
                public DateTime LeaveTime;                
                public int? Duration; // in seconds
            }
        }
        #endregion
        #endregion

        #region Methods
        #region AuthenticateUser
        /// <summary>
        /// Authenticates the GTW credentials and returns a token to be used with subsequent GTW API calls.
        /// </summary>
        /// <param name="consumerKey">GTW application key</param>
        /// <param name="organizerLogin">GTW organizer login</param>
        /// <param name="organizerPassword">GTW organizer password</param>
        /// <param name="consumerSecret">GTW consumer secret (new for OAuth changes - 10/2019)</param>
        public void AuthenticateUser(string consumerKey, string organizerLogin, string organizerPassword, string consumerSecret)
        {
            try
            {
                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // depending on if there is a consumer secret, create a web request for either the new method (consumer secret), or the old method (no consumer secret)
                string requestUrl = String.Empty;
                string authorization = String.Empty;
                string payload = String.Empty;

                if (!String.IsNullOrWhiteSpace(consumerSecret))
                {
                    requestUrl = Config.ApplicationSettings.GoToWebinarAuthenticationURLv2;
                    authorization = consumerKey + ":" + consumerSecret;
                    payload = String.Format("grant_type=password&username={0}&password={1}", organizerLogin, HttpUtility.UrlEncode(organizerPassword));
                }
                else
                { requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarAuthenticationURL + "?grant_type=password&user_id={0}&password={1}&client_id={2}", organizerLogin, HttpUtility.UrlEncode(organizerPassword), consumerKey); }

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);

                if (!String.IsNullOrWhiteSpace(consumerSecret))
                {
                    request.Method = "POST";
                    request.Accept = "application/json";
                    request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(authorization)));
                    request.ContentType = "application/x-www-form-urlencoded";

                    byte[] payloadBytes = Encoding.UTF8.GetBytes(payload);
                    request.ContentLength = payloadBytes.Length;

                    // send the POST payload
                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(payloadBytes, 0, payloadBytes.Length);
                        requestStream.Close();
                    }
                }
                else
                {
                    request.Accept = "application/json";
                    request.ContentType = "application/json";
                }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate organizer object
                this._Organizer = new GTW_Organizer_Response();

                // populate organizer information
                this._OrganizerKey = Int64.Parse(jsonTempResponseObject["organizer_key"].ToString());
                this._AccessToken = jsonTempResponseObject["access_token"].ToString();

                this._Organizer.FirstName = jsonTempResponseObject["firstName"].ToString();
                this._Organizer.LastName = jsonTempResponseObject["lastName"].ToString();
                this._Organizer.Email = jsonTempResponseObject["email"].ToString();
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.AuthenticateUser.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.AuthenticateUser.Exception")); }
        }
        #endregion

        #region CreateWebinar
        /// <summary>
        /// Creates a webinar.
        /// </summary>
        /// <param name="subject">subject</param>
        /// <param name="description">description</param>
        /// <param name="startTime">start time in UTC</param>
        /// <param name="endTime">end time in UTC</param>
        /// <param name="passwordRequired">is a password required for the webinar?</param>
        /// <returns>GTW_Webinar_Response containing the webinar that was just created</returns>
        public GTW_Webinar_Response CreateWebinar(string subject, string description, DateTime startTime, DateTime endTime, bool passwordRequired, string timezoneDotNetName)
        {            
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "CreateWebinar")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarRestAPIURL + "organizers/{0}/webinars", this._OrganizerKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // clean subject and description for JSON
                subject = subject.Replace(Environment.NewLine, "\\r\\n");
                subject = subject.Replace("\t", String.Empty);
                subject = subject.Replace("\"", "\\\"");

                description = description.Replace(Environment.NewLine, "\\r\\n");
                description = description.Replace("\t", String.Empty);
                description = description.Replace("\"", "\\\"");

                // format the request body
                string requestBody = "{ \"subject\": \"" + subject + "\", \"description\": \"" + description + "\", \"times\": [{ \"startTime\": \"" + startTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\", \"endTime\": \"" + endTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\" }], \"type\": \"single_session\", \"timeZone\": \"" + GoToTrainingAPI.GetTimezoneName(timezoneDotNetName) + "\", \"isPasswordProtected\": " + passwordRequired.ToString().ToLower() + " }";
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create webinar object
                GTW_Webinar_Response webinar = new GTW_Webinar_Response();

                // get the webinar object from the response
                Newtonsoft.Json.Linq.JObject responseWebinar = (Newtonsoft.Json.Linq.JObject)jsonTempResponseObject["Root"];                

                // get the newly created webinar
                if (responseWebinar != null)
                { webinar = this.GetWebinar(Int64.Parse(responseWebinar["webinarKey"].ToString())); }
                else
                { throw new AsentiaException(_GlobalResources.WebinarInformationCouldNotBeReadFromTheGetWebinarResponse); }

                return (webinar);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.CreateWebinar.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.CreateWebinar.Exception")); }
        }
        #endregion

        #region UpdateWebinar
        /// <summary>
        /// Updates a webinar.
        /// </summary>
        /// <param name="webinarKey">GTW webinar id</param>
        /// <param name="subject">subject</param>
        /// <param name="description">description</param>
        /// <param name="startTime">start time in UTC</param>
        /// <param name="endTime">end time in UTC</param>
        /// <param name="passwordRequired">is a password required for the webinar?</param>
        public void UpdateWebinar(Int64 webinarKey, string subject, string description, DateTime startTime, DateTime endTime, bool passwordRequired, string timezoneDotNetName)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "UpdateWebinar")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarRestAPIURL + "organizers/{0}/webinars/{1}", this._OrganizerKey.ToString(), webinarKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "PUT";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // clean subject and description for JSON
                subject = subject.Replace(Environment.NewLine, "\\r\\n");
                subject = subject.Replace("\t", String.Empty);
                subject = subject.Replace("\"", "\\\"");

                description = description.Replace(Environment.NewLine, "\\r\\n");
                description = description.Replace("\t", String.Empty);
                description = description.Replace("\"", "\\\"");

                // format the request body
                string requestBody = "{ \"subject\": \"" + subject + "\", \"description\": \"" + description + "\", \"times\": [{ \"startTime\": \"" + startTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\", \"endTime\": \"" + endTime.ToString("yyyy-MM-ddTHH:mm:ssZ") + "\" }], \"type\": \"single_session\", \"timeZone\": \"" + GoToTrainingAPI.GetTimezoneName(timezoneDotNetName) + "\", \"isPasswordProtected\": " + passwordRequired.ToString().ToLower() + " }";
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }                

                // create temp json object for parsing results - but do nothing with it because we don't need to
                // Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.UpdateWebinar.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.UpdateWebinar.Exception")); }
        }
        #endregion

        #region DeleteWebinar
        /// <summary>
        /// Deletes a webinar.
        /// </summary>
        /// <param name="webinarKey">GTW webinar id</param>
        public void DeleteWebinar(Int64 webinarKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "DeleteWebinar")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarRestAPIURL + "organizers/{0}/webinars/{1}", this._OrganizerKey.ToString(), webinarKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "DELETE";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results - but we will do nothing with them because we don't need to
                // Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.DeleteWebinar.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.DeleteWebinar.Exception")); }
        }
        #endregion

        #region GetWebinar
        /// <summary>
        /// Gets information about a webinar using the webinar's ID.
        /// </summary>
        /// <param name="webinarKey">GTW webinar id</param>
        /// <returns>GTW_Webinar_Response containing the requisted webinar's information</returns>
        public GTW_Webinar_Response GetWebinar(Int64 webinarKey)
        {            
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetWebinar")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarRestAPIURL + "organizers/{0}/webinars/{1}", this._OrganizerKey.ToString(), webinarKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate webinar object
                GTW_Webinar_Response webinar = new GTW_Webinar_Response();

                // get the webinar object from the response
                Newtonsoft.Json.Linq.JObject responseWebinar = (Newtonsoft.Json.Linq.JObject)jsonTempResponseObject["Root"];

                // if the response webinar is not null, populate the object with the webinar information; otherwise, throw an exception
                if (responseWebinar != null)
                {                    
                    webinar.WebinarKey = Int64.Parse(responseWebinar["webinarKey"].ToString());
                    webinar.WebinarId = responseWebinar["webinarID"].ToString();
                    webinar.NumberOfRegistrants = int.Parse(responseWebinar["numberOfRegistrants"].ToString());
                    webinar.InSession = bool.Parse(responseWebinar["inSession"].ToString());
                    webinar.Subject = HttpUtility.HtmlDecode(responseWebinar["subject"].ToString());

                    if (responseWebinar["description"] != null)
                    { webinar.Description = HttpUtility.HtmlDecode(responseWebinar["description"].ToString()); }
                    else
                    { webinar.Description = String.Empty; }

                    webinar.Timezone = responseWebinar["timeZone"].ToString();
                    webinar.RegistrationUrl = responseWebinar["registrationUrl"].ToString();

                    // note that dates get returned from the API, but we do not need to use them, use what we originally have stored from Asentia
                    Newtonsoft.Json.Linq.JArray responseWebinarTimesArray = (Newtonsoft.Json.Linq.JArray)responseWebinar["times"];

                    if (responseWebinarTimesArray != null)
                    {
                        Newtonsoft.Json.Linq.JObject responseWebinarTimes = (Newtonsoft.Json.Linq.JObject)responseWebinarTimesArray[0];                        
                        webinar.StartDate = DateTime.Parse(responseWebinarTimes["startTime"].ToString());
                        webinar.EndDate = DateTime.Parse(responseWebinarTimes["endTime"].ToString());                        
                    }
                    else
                    { throw new AsentiaException(_GlobalResources.WebinarMeetingTimeInformationCouldNotBeReadFromTheGetWebinarResponse); }                   
                }
                else
                { throw new AsentiaException(_GlobalResources.WebinarInformationCouldNotBeReadFromTheGetWebinarResponse); }

                return (webinar);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.GetWebinar.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.GetWebinar.Exception")); }
        }
        #endregion

        #region CreateRegistrant
        /// <summary>
        /// Creates a webinar registrant.
        /// </summary>
        /// <param name="webinarKey">GTW webinar id</param>
        /// <param name="firstName">registrant first name</param>
        /// <param name="lastName">registrant last name</param>
        /// <param name="email">registrant email address</param>
        /// <returns>GTW_Registrant_Response containing the webinar registrant's information</returns>
        public GTW_Registrant_Response CreateRegistrant(Int64 webinarKey, string firstName, string lastName, string email)
        {            
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "CreateRegistrant")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarRestAPIURL + "organizers/{0}/webinars/{1}/registrants", this._OrganizerKey.ToString(), webinarKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "POST";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // format the request body                
                string requestBody = "{ \"firstName\": \"" + firstName + "\", \"lastName\": \"" + lastName + "\", \"email\": \"" + email + "\" }";
                byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);
                request.ContentLength = byteArray.Length;

                using (Stream dataStream = request.GetRequestStream())
                { dataStream.Write(byteArray, 0, byteArray.Length); }

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create registrant object
                GTW_Registrant_Response registrant = new GTW_Registrant_Response();

                // get the registrant object from the response
                Newtonsoft.Json.Linq.JObject responseRegistrant = (Newtonsoft.Json.Linq.JObject)jsonTempResponseObject["Root"];

                // get the newly created registrant
                if (responseRegistrant != null)
                { registrant = this.GetRegistrant(webinarKey, Int64.Parse(responseRegistrant["registrantKey"].ToString())); }
                else
                { throw new AsentiaException(_GlobalResources.RegistrantInformationCouldNotBeReadFromTheGetRegistrantResponse); }

                return (registrant);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.CreateRegistrant.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.CreateRegistrant.Exception")); }
        }
        #endregion

        #region DeleteRegistrant
        /// <summary>
        /// Deletes a webinar registrant.
        /// </summary>
        /// <param name="webinarKey">GTW webinar id</param>
        /// <param name="registrantKey">GTW webinar registrant id</param>        
        public void DeleteRegistrant(Int64 webinarKey, Int64 registrantKey)
        {
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "DeleteRegistrant")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarRestAPIURL + "organizers/{0}/webinars/{1}/registrants/{2}", this._OrganizerKey.ToString(), webinarKey.ToString(), registrantKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "DELETE";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // create temp json object for parsing results - but we will do nothing with them because we don't need to
                // Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.DeleteRegistrant.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.DeleteRegistrant.Exception")); }
        }
        #endregion

        #region GetRegistrant
        /// <summary>
        /// Gets information about a webinar registrant using the webinar's id and the registrant's id.
        /// </summary>
        /// <param name="webinarKey">GTW webinar id</param>
        /// <param name="registrantKey">GTW webinar registrant id</param>
        /// <returns>GTW_Registrant_Response containing the requested webinar registrant's information</returns>        
        public GTW_Registrant_Response GetRegistrant(Int64 webinarKey, Int64 registrantKey)
        {            
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetRegistrant")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarRestAPIURL + "organizers/{0}/webinars/{1}/registrants/{2}", this._OrganizerKey.ToString(), webinarKey.ToString(), registrantKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate registrant object
                GTW_Registrant_Response registrant = new GTW_Registrant_Response();

                // get the registrant from the response
                Newtonsoft.Json.Linq.JObject responseRegistrant = (Newtonsoft.Json.Linq.JObject)jsonTempResponseObject["Root"];

                // if the response registrant is not null, populate the object with the registrant information; otherwise, throw an exception
                if (responseRegistrant != null)
                {
                    registrant.Registrants = new List<GTW_Registrant_Response.Registrant>();

                    registrant.Registrants.Add(new GTW_Registrant_Response.Registrant()
                    {
                        WebinarKey = webinarKey,
                        RegistrantKey = registrantKey,
                        FirstName = responseRegistrant["firstName"].ToString(),
                        LastName = responseRegistrant["lastName"].ToString(),
                        Email = responseRegistrant["email"].ToString(),
                        RegistrationDate = DateTime.Parse(responseRegistrant["registrationDate"].ToString()),
                        Status = responseRegistrant["status"].ToString(),
                        JoinUrl = responseRegistrant["joinUrl"].ToString()
                    });
                }
                else
                { throw new AsentiaException(_GlobalResources.RegistrantInformationCouldNotBeReadFromTheGetRegistrantResponse); }

                return (registrant);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.GetRegistrant.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator + "|" + ex.StackTrace + "|" + ex.Message, "GoToWebinarAPI.GetRegistrant.Exception")); }
        }
        #endregion

        #region GetWebinarRegistrants
        /// <summary>
        /// Gets the registrants of a webinar.
        /// </summary>
        /// <param name="webinarKey">GTW webinar id</param>
        /// <returns>GTW_Registrant_Response containing webinar registrants</returns>
        public GTW_Registrant_Response GetWebinarRegistrants(Int64 webinarKey)
        {            
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetWebinarRegistrants")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarRestAPIURL + "organizers/{0}/webinars/{1}/registrants", this._OrganizerKey.ToString(), webinarKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate registrants object
                GTW_Registrant_Response registrants = new GTW_Registrant_Response();

                // get an array of registrants from the response
                Newtonsoft.Json.Linq.JArray responseRegistrantsArray = (Newtonsoft.Json.Linq.JArray)jsonTempResponseObject["Root"];

                // if the response registrants array is not null, loop through it to populate the object with the registrant information; otherwise, throw an exception
                if (responseRegistrantsArray != null)
                {
                    registrants.Registrants = new List<GTW_Registrant_Response.Registrant>();

                    foreach (Newtonsoft.Json.Linq.JObject responseRegistrant in responseRegistrantsArray)
                    {
                        registrants.Registrants.Add(new GTW_Registrant_Response.Registrant()
                        {
                            WebinarKey = webinarKey,
                            RegistrantKey = Int64.Parse(responseRegistrant["registrantKey"].ToString()),
                            FirstName = responseRegistrant["firstName"].ToString(),
                            LastName = responseRegistrant["lastName"].ToString(),
                            Email = responseRegistrant["email"].ToString(),
                            RegistrationDate = DateTime.Parse(responseRegistrant["registrationDate"].ToString()),
                            Status = responseRegistrant["status"].ToString(),
                            JoinUrl = responseRegistrant["joinUrl"].ToString()
                        });
                    }
                }
                else
                { throw new AsentiaException(_GlobalResources.RegistrantInformationCouldNotBeReadFromTheGetWebinarRegistrantsResponse); }

                return (registrants);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.GetWebinarRegistrants.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.GetWebinarRegistrants.Exception")); }
        }
        #endregion

        #region GetWebinarAttendees
        /// <summary>
        /// Gets the attendees of a webinar. Note that the webinar must have occurred (not in the future), or this will throw an error.
        /// </summary>
        /// <param name="webinarKey">GTW webinar id</param>
        /// <returns>GTW_Attendee_Response containing webinar attendees</returns>
        public GTW_Attendee_Response GetWebinarAttendees(Int64 webinarKey)
        {            
            // if there is no organizer or access token, throw an exception because the caller is not authenticated
            if (this._OrganizerKey == 0 || String.IsNullOrWhiteSpace(this._AccessToken))
            { throw new AsentiaException(String.Format(_GlobalResources.CannotBeCalledBecauseTheAPICallerHasNotBeenAuthenticatedPleaseContactAnAdministrator, "GetWebinarAttendees")); }

            try
            {
                string requestUrl = String.Format(Config.ApplicationSettings.GoToWebinarRestAPIURL + "organizers/{0}/webinars/{1}/attendees", this._OrganizerKey.ToString(), webinarKey.ToString());

                // set security protocol to TLS 1.2 as LogMeIn requires
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest request = WebRequest.Create(requestUrl);
                request.Method = "GET";
                request.Headers["ContentType"] = "application/json";
                request.Headers["Authorization"] = String.Format("OAuth oauth_token={0}", this._AccessToken);

                // get the response
                string responseData = String.Empty;

                using (WebResponse response = request.GetResponse())
                {
                    // get the response from the stream
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            responseData = reader.ReadToEnd();
                        }
                    }
                }

                // since the system returns back [{JSON}] newtonsoft cannot parse this as it thinks its missing a key and is just given a value, so what we do is we tack on the key to the call so that the JSON is valid and then we parse the string to JSON
                responseData = "{\"Root\" : " + responseData + "}";

                // create temp json object for parsing results
                Newtonsoft.Json.Linq.JObject jsonTempResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseData);

                // create and populate attendees object
                GTW_Attendee_Response attendees = new GTW_Attendee_Response();

                // get an array of attendees from the response
                Newtonsoft.Json.Linq.JArray responseAttendeesArray = (Newtonsoft.Json.Linq.JArray)jsonTempResponseObject["Root"];

                // if the response attendees array is not null, loop through it to populate the object with the attendee information; otherwise, throw an exception
                if (responseAttendeesArray != null)
                {
                    attendees.Attendees = new List<GTW_Attendee_Response.Attendee>();

                    foreach (Newtonsoft.Json.Linq.JObject responseAttendee in responseAttendeesArray)
                    {
                        // get the join time and leave time from the attendance data for the attendee
                        Newtonsoft.Json.Linq.JArray responseAttendeeAttendanceTimesArray = (Newtonsoft.Json.Linq.JArray)responseAttendee["attendance"];
                        DateTime joinTime = DateTime.MinValue;
                        DateTime leaveTime = DateTime.MinValue; 

                        if (responseAttendeeAttendanceTimesArray != null)
                        {
                            if (responseAttendeeAttendanceTimesArray.Count > 0)
                            {
                                Newtonsoft.Json.Linq.JObject responseAttendeeAttendanceTimes = (Newtonsoft.Json.Linq.JObject)responseAttendeeAttendanceTimesArray[0]; // this is an array of times, but we should only need the first set
                                joinTime = DateTime.Parse(responseAttendeeAttendanceTimes["joinTime"].ToString());
                                leaveTime = DateTime.Parse(responseAttendeeAttendanceTimes["leaveTime"].ToString());
                            }
                        }                        

                        // add the attendee information to the object
                        attendees.Attendees.Add(new GTW_Attendee_Response.Attendee()
                        {
                            WebinarKey = webinarKey,
                            RegistrantKey = Int64.Parse(responseAttendee["registrantKey"].ToString()),
                            SessionKey = Int64.Parse(responseAttendee["sessionKey"].ToString()),
                            FirstName = responseAttendee["firstName"].ToString(),
                            LastName = responseAttendee["lastName"].ToString(),
                            Email = responseAttendee["email"].ToString(),
                            JoinTime = joinTime,
                            LeaveTime = leaveTime,
                            Duration = int.Parse(responseAttendee["attendanceTimeInSeconds"].ToString())
                        });
                    }
                }
                else
                { throw new AsentiaException(_GlobalResources.AttendeeInformationCouldNotBeReadFromTheGetWebinarAttendeesResponse); }

                return (attendees);
            }
            catch (WebException exp)
            {
                if (exp.Response != null)
                { throw new AsentiaException(this._HandleErrorResponse((HttpWebResponse)exp.Response)); }
                else
                { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.GetWebinarAttendees.WebException")); }
            }
            catch (AsentiaException aEx)
            { throw new AsentiaException(aEx.Message); }
            catch (Exception ex)
            { throw new AsentiaException(String.Format(_GlobalResources.AnUnknownErrorOccurredInWhileProcessingYourRequestPleaseContactAnAdministrator, "GoToWebinarAPI.GetWebinarAttendees.Exception")); }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _HandleErrorResponse
        /// <summary>
        /// Handles errors returned from the GTM API and returns a string describing the error.
        /// </summary>
        /// <param name="response">GTM response</param>
        /// <returns>string</returns>
        private string _HandleErrorResponse(HttpWebResponse response)
        {
            string errorResponseString = String.Empty;

            switch (response.StatusCode)
            {
                case HttpStatusCode.NoContent:          // 204
                case HttpStatusCode.BadRequest:         // 400
                case HttpStatusCode.Unauthorized:       // 401
                case HttpStatusCode.Forbidden:          // 403
                case HttpStatusCode.NotFound:           // 404
                case HttpStatusCode.MethodNotAllowed:   // 405
                case HttpStatusCode.Conflict:           // 409

                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string responseString = reader.ReadToEnd();

                        Newtonsoft.Json.Linq.JObject jsonTempErrorResponseObject = Newtonsoft.Json.Linq.JObject.Parse(responseString);

                        if (jsonTempErrorResponseObject["error"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["error"].ToString(); }
                        else if (jsonTempErrorResponseObject["int_err_code"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["int_err_code"].ToString(); }
                        else if (jsonTempErrorResponseObject["int_error_code"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["int_error_code"].ToString(); }
                        else if (jsonTempErrorResponseObject["error_description"] != null)
                        { errorResponseString = jsonTempErrorResponseObject["error_description"].ToString(); }
                        else
                        { errorResponseString = _GlobalResources.AFatalErrorOccurredInTheAPIPleaseContactAnAdministrator + "(Code 1)"; }
                    }

                    break;
                default:
                    errorResponseString = _GlobalResources.AFatalErrorOccurredInTheAPIPleaseContactAnAdministrator + "(Code 2)";
                    break;
            }

            return errorResponseString;
        }
        #endregion
        #endregion
    }
}
