﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Serialization;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    
    [Serializable]
    public class EnrollmentRequest
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public EnrollmentRequest()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idEnrollmentRequest">Enrollment Request Id</param>
        public EnrollmentRequest(int idEnrollmentRequest)
        {
          
            // load details
            this._Details(idEnrollmentRequest);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[EnrollmentRequest.GetGrid]";

        /// <summary>
        /// Stored procedure used to populate a grid for this object for the current session user's dashboard.
        /// </summary>
        public static readonly string GridProcedureForUserDashboard = "[EnrollmentRequest.GetGridForUserDashboard]";
        
        /// <summary>
        /// Enrollment Request Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Course Id.
        /// </summary>
        public int IdCourse;

        /// <summary>
        /// User Id.
        /// </summary>
        public int IdUser;

        /// <summary>
        /// Timestamp
        /// </summary>
        public DateTime Timestamp;

        /// <summary>
        /// Date Approved
        /// </summary>
        public DateTime? DtApproved;

        /// <summary>
        /// Date Denied
        /// </summary>
        public DateTime? DtDenied;

        /// <summary>
        /// Approver ID
        /// </summary>       
        public int? IdApprover;
        #endregion

        #region Methods
        #region Submit
        /// <summary>
        /// Submits an enrollment request to the database as the current session user.
        /// </summary>
        /// <param name="idCourse">Course ID</param>
        /// <param name="idUser">User ID</param>
        public int Submit(int idCourse, int idUser)
        {
            return this._Submit(AsentiaSessionState.IdSiteUser, idCourse, idUser);
        }

        /// <summary>
        /// Overloaded method that submits enrollment request to the database with a caller specified.
        /// This would be used when Submit needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Submit(int idCaller, int idCourse, int idUser)
        {
            return this._Submit(idCaller, idCourse, idUser);
        }
        #endregion
        #endregion

        #region Static Methods
        #region IsEnrollmentRequestPending
        /// <summary>
        /// Determines if the user has a pending enrollment request for the course
        /// </summary>
        /// <param name="idCourse">Course ID</param>
        /// <param name="idUser">User ID</param>
        /// <returns></returns>
        public static bool IsEnrollmentRequestPending(int idCourse, int idUser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@isEnrollmentRequestPending", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[EnrollmentRequest.IsEnrollmentRequestPending]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                
                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isEnrollmentRequestPending"].Value);


            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return false;
        }
        #endregion

        #region GetEnrollmentRequestDataTableForUser
        /// <summary>
        /// Gets a data table for a list of enrollment requests for a user
        /// </summary>
        /// <returns></returns>
        public static DataTable GetEnrollmentRequestDataTableForUser()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@searchParam", "", SqlDbType.NVarChar, 4000, ParameterDirection.Input);
            databaseObject.AddParameter("@pageNum", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@pageSize", 1000, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@orderColumn", "title", SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@orderAsc", 1, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                DataSet ds = databaseObject.ExecuteDataSet("[EnrollmentRequest.GetGridForUserDashboard]", true);

                //DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                //string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                //AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return ds.Tables[1];
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion
        #endregion

        #region Private Methods
        #region _Submit
        /// <summary>
        /// Submits an enrollment request to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the enrollment request</returns>
        private int _Submit(int idCaller, int idCourse, int idUser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();            

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollmentRequest", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
            
            try
            {
                databaseObject.ExecuteNonQuery("[EnrollmentRequest.Submit]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved enrollment request
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idEnrollmentRequest"].Value);                

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified enrollment request's properties.
        /// </summary>
        /// <param name="idEnrollment">Enrollment Request Id</param>
        private void _Details(int idEnrollmentRequest)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollmentRequest", idEnrollmentRequest, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idCourse", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@timestamp", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtApproved", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDenied", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@idApprover", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[EnrollmentRequest.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idEnrollmentRequest;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdCourse = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idCourse"].Value);
                this.IdUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.Timestamp = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@timestamp"].Value);
                this.DtApproved = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtApproved"].Value);
                this.DtDenied = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDenied"].Value);
                this.IdApprover = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idApprover"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}