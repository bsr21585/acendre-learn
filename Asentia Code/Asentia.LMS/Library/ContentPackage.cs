﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Threading;
using System.Xml;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class ContentPackage
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ContentPackage()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idContentPackage">Content Package Id</param>
        public ContentPackage(int idContentPackage)
        {
            this._Details(idContentPackage);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[ContentPackage.GetGrid]";

        /// <summary>
        /// Content Package Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        public int IdSite;

        /// <summary>
        /// Package Name.
        /// </summary>
        public string Name;

        /// <summary>
        /// Path to package content.
        /// </summary>
        public string Path;

        /// <summary>
        /// Size (in Kb) of package.
        /// </summary>
        public int Kb;

        /// <summary>
        /// Content Package Type.
        /// </summary>
        public ContentPackageType IdContentPackageType;

        /// <summary>
        /// SCORM Package Type (if content type is SCORM).
        /// </summary>
        public SCORMPackageType? IdSCORMPackageType;

        /// <summary>
        /// Manifest.
        /// </summary>
        public string Manifest;

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Date Modified.
        /// </summary>
        public DateTime? DtModified;

        // PROPERTIES RELATED TO VIDEO, POWERPOINT, AND PDF IMPORT

        /// <summary>
        /// Is this a media content (Video, PowerPoint, PDF) upload?
        /// </summary>
        public bool? IsMediaUpload;

        /// <summary>
        /// The media type of the uploaded package.
        /// </summary>
        public MediaType? IdMediaType;

        /// <summary>
        /// The content title for the "<title>s" in the manifest file. - Video, PowerPoint, & PDF
        /// </summary>
        public string ContentTitle;

        /// <summary>
        /// The original filename of the file that was uploaded.
        /// </summary>
        public string OriginalMediaFilename;

        /// <summary>
        /// Is the video media 3rd party (YouTube, Vimeo).
        /// </summary>
        public bool? IsVideoMedia3rdParty;

        /// <summary>
        /// The embed code, if 3rd party video.
        /// </summary>
        public string VideoMediaEmbedCode;

        /// <summary>
        /// Enable Autoplay - Video & PowerPoint
        /// </summary>
        public bool? EnableAutoplay;

        /// <summary>
        /// Allow Rewind - Video Only
        /// </summary>
        public bool? AllowRewind;

        /// <summary>
        /// Allow Fast Forward - Video Only
        /// </summary>
        public bool? AllowFastForward;

        /// <summary>
        /// Allow Navigation - PowerPoint Only
        /// </summary>
        public bool? AllowNavigation;

        /// <summary>
        /// Allow Resume - Video & PowerPoint
        /// </summary>
        public bool? AllowResume;

        /// <summary>
        /// Minumum progress required for completion. Scaled 0.0 - 1.0
        /// </summary>
        public double? MinProgressForCompletion;

        /// <summary>
        /// Is the package being processed? - PowerPoint & PDF (Word Upload)
        /// </summary>
        public bool? IsProcessing;

        /// <summary>
        /// Has the package been processed - PowerPoint & PDF (Word Upload)
        /// </summary>
        public bool? IsProcessed;

        /// <summary>
        /// Date/Time package was processed - PowerPoint & PDF (Word Upload)
        /// </summary>
        public DateTime? DtProcessed;

        /// <summary>
        /// Does the manifest have "non-fatal" SCORM compliance errors?
        /// </summary>
        public bool HasManifestComplianceErrors;

        /// <summary>
        /// String containing new-line separated list of "non-fatal" manifest SCORM compliance errors.
        /// </summary>
        public string ManifestComplianceErrors;

        /// <summary>
        /// String containing the Open Sesame GUID if this package was synched from Open Sesame.
        /// </summary>
        public string OpenSesameGUID;

        // PROPERTIES RELATED TO QUIZ/SURVEY

        /// <summary>
        /// The Quiz/Survey this package was published from.
        /// </summary>
        public int? IdQuizSurvey;
        #endregion

        #region ContentPackageType ENUM
        public enum ContentPackageType
        {
            SCORM = 1,
            xAPI = 2,
        }
        #endregion

        #region SCORMPackageType ENUM
        public enum SCORMPackageType
        {
            Content = 1,
            Resource = 2,
        }
        #endregion

        #region MediaType ENUM
        public enum MediaType
        {
            Video = 1,
            PowerPoint = 2,
            PDF = 3,
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves content package data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves content package data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region GetUtilizationInformation
        /// <summary>
        /// Gets a DataTable of lessons that are utilizing the specified content package.
        /// </summary>
        public DataTable GetUtilizationInformation()
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureContentPackageGetUtilizationInformationCannotBeCalledWithoutAValidID); }

            DataTable dt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idContentPackage", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[ContentPackage.GetUtilizationInformation]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Content Package(s).
        /// </summary>
        /// <param name="deletees">DataTable of content packages to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@ContentPackages", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[ContentPackage.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetPaths
        /// <summary>
        /// Retrieves the folder paths of the specified content package ids.
        /// </summary>
        /// <param name="contentPackageIds">Content Package Ids</param>
        public static DataTable GetPaths(DataTable contentPackageIds)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@ContentPackages", contentPackageIds, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                SqlDataReader reader = databaseObject.ExecuteDataReader("[ContentPackage.GetPaths]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = (databaseObject.Command.Parameters["@Error_Description_Code"].Value == DBNull.Value || databaseObject.Command.Parameters["@Error_Description_Code"].Value == null) ? null : databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                DataTable dt = new DataTable();
                dt.Load(reader);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForSelectList
        /// <summary>
        /// Gets a listing of content package ids and names to populate into a select list.
        /// </summary>
        /// <returns>DataTable of content package ids and names.</returns>
        public static DataTable IdsAndNamesForSelectList(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[ContentPackage.IdsAndNamesForSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // loop through the datatable and append the formatted dtCreated to the name
                // halt this for now
                /*
                foreach (DataRow row in dt.Rows)
                {
                    DateTime dtCreated;
                    if (DateTime.TryParse(row["dtCreated"].ToString(), out dtCreated))
                    {
                        // convert to local datetime
                        dtCreated = TimeZoneInfo.ConvertTimeFromUtc(dtCreated, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                        // apend to name column
                        row["name"] += " - [" + _GlobalResources.DateUploaded + ": " + dtCreated.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern) + "]";
                    }
                }
                */

                // return the datatable
                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetOpenSesameGUIDs
        /// <summary>
        /// Retrieves a list of packages and their Open Sesame GUIDs for packages synched with Open Sesame.
        /// </summary>
        public static DataTable GetOpenSesameGUIDs()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader reader = databaseObject.ExecuteDataReader("[ContentPackage.GetOpenSesameGUIDs]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = (databaseObject.Command.Parameters["@Error_Description_Code"].Value == DBNull.Value || databaseObject.Command.Parameters["@Error_Description_Code"].Value == null) ? null : databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                DataTable dt = new DataTable();
                dt.Load(reader);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves content package data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved course</returns>
        public int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idContentPackage", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@path", this.Path, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@kb", this.Kb, SqlDbType.Int, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@manifest", this.Manifest, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@idContentPackageType", this.IdContentPackageType, SqlDbType.Int, 4, ParameterDirection.Input);

            if (this.IdSCORMPackageType == null)
            { databaseObject.AddParameter("@idSCORMPackageType", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idSCORMPackageType", this.IdSCORMPackageType, SqlDbType.Int, 4, ParameterDirection.Input); }
            
            if (this.IsMediaUpload == null)
            { databaseObject.AddParameter("@isMediaUpload", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isMediaUpload", this.IsMediaUpload, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IdMediaType == null)
            { databaseObject.AddParameter("@idMediaType", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idMediaType", this.IdMediaType, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@contentTitle", this.ContentTitle, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            databaseObject.AddParameter("@originalMediaFilename", this.OriginalMediaFilename, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            if (this.IsVideoMedia3rdParty == null)
            { databaseObject.AddParameter("@isVideoMedia3rdParty", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isVideoMedia3rdParty", this.IsVideoMedia3rdParty, SqlDbType.Bit, 1, ParameterDirection.Input); }

            databaseObject.AddParameter("@videoMediaEmbedCode", this.VideoMediaEmbedCode, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            if (this.EnableAutoplay == null)
            { databaseObject.AddParameter("@enableAutoplay", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@enableAutoplay", this.EnableAutoplay, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.AllowRewind == null)
            { databaseObject.AddParameter("@allowRewind", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@allowRewind", this.AllowRewind, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.AllowFastForward == null)
            { databaseObject.AddParameter("@allowFastForward", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@allowFastForward", this.AllowFastForward, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.AllowNavigation == null)
            { databaseObject.AddParameter("@allowNavigation", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@allowNavigation", this.AllowNavigation, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.AllowResume == null)
            { databaseObject.AddParameter("@allowResume", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@allowResume", this.AllowResume, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.MinProgressForCompletion == null)
            { databaseObject.AddParameter("@minProgressForCompletion", DBNull.Value, SqlDbType.Float, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@minProgressForCompletion", this.MinProgressForCompletion, SqlDbType.Float, 8, ParameterDirection.Input); }

            if (this.IsProcessing == null)
            { databaseObject.AddParameter("@isProcessing", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isProcessing", this.IsProcessing, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsProcessed == null)
            { databaseObject.AddParameter("@isProcessed", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isProcessed", this.IsProcessed, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.DtProcessed == null)
            { databaseObject.AddParameter("@dtProcessed", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dtProcessed", this.DtProcessed, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            if (this.IdQuizSurvey == null)
            { databaseObject.AddParameter("@idQuizSurvey", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idQuizSurvey", this.IdQuizSurvey, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.HasManifestComplianceErrors == null)
            { databaseObject.AddParameter("@hasManifestComplianceErrors", false, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@hasManifestComplianceErrors", this.HasManifestComplianceErrors, SqlDbType.Bit, 1, ParameterDirection.Input); }

            databaseObject.AddParameter("@manifestComplianceErrors", this.ManifestComplianceErrors, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@openSesameGUID", this.OpenSesameGUID, SqlDbType.NVarChar, 36, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[ContentPackage.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved content package
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idContentPackage"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified content package's properties.
        /// </summary>
        /// <param name="idContentPackage">Content Package Id</param>
        private void _Details(int idContentPackage)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idContentPackage", idContentPackage, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@name", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@path", null, SqlDbType.NVarChar, 1024, ParameterDirection.Output);
            databaseObject.AddParameter("@kb", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idContentPackageType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idSCORMPackageType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@manifest", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output); // confirm null part
            databaseObject.AddParameter("@dtModified", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isMediaUpload", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@idMediaType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@contentTitle", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@originalMediaFilename", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isVideoMedia3rdParty", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@videoMediaEmbedCode", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@enableAutoplay", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@allowRewind", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@allowFastForward", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@allowNavigation", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@allowResume", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@minProgressForCompletion", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isProcessing", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isProcessed", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtProcessed", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@idQuizSurvey", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@hasManifestComplianceErrors", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@manifestComplianceErrors", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@openSesameGUID", null, SqlDbType.NVarChar, 36, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[ContentPackage.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idContentPackage;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.Name = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@name"].Value);
                this.Path = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@path"].Value);
                this.Kb = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@kb"].Value);
                this.IdContentPackageType = (ContentPackageType)AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idContentPackageType"].Value);
                this.IdSCORMPackageType = (SCORMPackageType?)AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idSCORMPackageType"].Value);
                this.Manifest = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@manifest"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                this.IsMediaUpload = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isMediaUpload"].Value);
                this.IdMediaType = (MediaType?)AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idMediaType"].Value);
                this.ContentTitle = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@contentTitle"].Value);
                this.OriginalMediaFilename = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@originalMediaFilename"].Value);
                this.IsVideoMedia3rdParty = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isVideoMedia3rdParty"].Value);
                this.VideoMediaEmbedCode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@videoMediaEmbedCode"].Value);
                this.EnableAutoplay = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@enableAutoplay"].Value);
                this.AllowRewind = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@allowRewind"].Value);
                this.AllowFastForward = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@allowFastForward"].Value);
                this.AllowNavigation = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@allowNavigation"].Value);
                this.AllowResume = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@allowResume"].Value);
                this.MinProgressForCompletion = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@minProgressForCompletion"].Value);
                this.IsProcessing = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isProcessing"].Value);
                this.IsProcessed = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isProcessed"].Value);
                this.DtProcessed = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtProcessed"].Value);
                this.IdQuizSurvey = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idQuizSurvey"].Value);
                this.HasManifestComplianceErrors = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@hasManifestComplianceErrors"].Value);
                this.ManifestComplianceErrors = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@manifestComplianceErrors"].Value);
                this.OpenSesameGUID = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@openSesameGUID"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
