﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class EventEmailNotification
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public EventEmailNotification()
        {
            this.LanguageSpecificProperties = new ArrayList();             
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idEventEmailNotification">Event Email Notification Id</param>
        public EventEmailNotification(int idEventEmailNotification, string languageString)
        {
            this.LanguageSpecificProperties = new ArrayList();
            this._Details(idEventEmailNotification, languageString);
            this._GetPropertiesInLanguages(idEventEmailNotification);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[EventEmailNotification.GetGrid]";

        /// <summary>
        /// Event Email Notification Id.
        /// </summary>
        public int IdEventEmailNotification { get; set; }

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Event Type Id.
        /// </summary>
        public int IdEventType { get; set; }

        /// <summary>
        /// Event Type Recipient Id.
        /// </summary>
        public int IdEventTypeRecipient { get; set; }

        /// <summary>
        /// Object Id.
        /// </summary>
        public int? IdObject { get; set; }       
        
        /// <summary>
        /// From.
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// CopyTo.
        /// </summary>
        public string CopyTo { get; set; }

        /// <summary>
        /// Specific Email Address
        /// </summary>
        public string SpecificEmailAddress { get; set; }

        /// <summary>
        /// Is Active.
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Activation Date.
        /// </summary>
        public DateTime? DtActivation { get; set; }

        /// <summary>
        /// Interval.
        /// </summary>
        public int? Interval { get; set; }

        /// <summary>
        /// Time Frame.
        /// </summary>
        public string TimeFrame { get; set; }

        /// <summary>
        /// Priority.
        /// </summary>
        public int? Priority { get; set; }

        /// <summary>
        /// User Id.
        /// </summary>
        public int? IdUser { get; set; }

        /// <summary>
        /// Is HTMLBased.
        /// </summary>
        public bool? IsHTMLBased { get; set; }

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// Attachment Type
        /// </summary>
        public int? AttachmentType { get; set; }
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Name;

            public LanguageSpecificProperty(string langString, string name)
            {
                this.LangString = langString;
                this.Name = name;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves object data to the database.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseFieldNotUniqueException">
        /// Thrown when a field must be unique.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <exception cref="DatabaseFieldConstraintException">
        /// Thrown when a value passed into a stored procedure violates a business rule.
        /// </exception>
        /// <exception cref="DatabaseSpecifiedLanguageNotDefaultException">
        /// Thrown when a Database Specified Language Not Default Exception occurs in a stored procedure.
        /// </exception>
        /// <param name=""></param>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEventEmailNotification", this.IdEventEmailNotification, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idEventType", this.IdEventType, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idEventTypeRecipient", this.IdEventTypeRecipient, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@from", this.From, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@copyTo", this.CopyTo, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            
            if (String.IsNullOrWhiteSpace(this.SpecificEmailAddress))
            {
                databaseObject.AddParameter("@specificEmailAddress", DBNull.Value, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@specificEmailAddress", this.SpecificEmailAddress, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            }           

            if (this.IdObject == null)
            {
                databaseObject.AddParameter("@idObject", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@idObject", this.IdObject, SqlDbType.Int, 4, ParameterDirection.Input);
            }

            if (this.IsActive == null)
            {
                databaseObject.AddParameter("@isActive", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@isActive", this.IsActive, SqlDbType.Bit, 1, ParameterDirection.Input);
            }

            if (this.Interval == null)
            {
                databaseObject.AddParameter("@interval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@interval", this.Interval, SqlDbType.Int, 4, ParameterDirection.Input);
            }

            if (this.TimeFrame == null)
            {
                databaseObject.AddParameter("@timeFrame", DBNull.Value, SqlDbType.NVarChar, 4, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@timeFrame", this.TimeFrame, SqlDbType.NVarChar, 4, ParameterDirection.Input);
            }

            if (this.Priority == null)
            {
                databaseObject.AddParameter("@priority", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@priority", this.Priority, SqlDbType.Int, 4, ParameterDirection.Input);
            }

            databaseObject.AddParameter("@isHTMLBased", this.IsHTMLBased, SqlDbType.Bit, 1, ParameterDirection.Input);

            if (this.AttachmentType == null)
            {
                databaseObject.AddParameter("@attachmentType", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@attachmentType", this.AttachmentType, SqlDbType.Int, 4, ParameterDirection.Input);
            }

            try
            {
                databaseObject.ExecuteNonQuery("[EventEmailNotification.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
             
                this.IdEventEmailNotification = Convert.ToInt32(databaseObject.Command.Parameters["@idEventEmailNotification"].Value);

                return this.IdEventEmailNotification;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this group.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="name">group name</param>
        public void SaveLang(string languageString, string name)
        {
            if (this.IdEventEmailNotification == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureEventEmailNotificationCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEventEmailNotification", this.IdEventEmailNotification, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@name", name, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[EventEmailNotification.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes email notifications.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@EmailNotifications", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[EventEmailNotification.Delete]", true);

                // get the return code
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idEventEmailNotification">Event Email Notification Id</param>
        private void _Details(int idEventEmailNotification, string languageString)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEventEmailNotification", idEventEmailNotification, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@idEventType", this.IdEventType, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idEventTypeRecipient", this.IdEventTypeRecipient, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idObject", this.IdObject, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@from", this.From, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@copyTo", this.CopyTo, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@specificEmailAddress", this.CopyTo, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isActive", this.IsActive, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtActivation", this.DtActivation, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@interval", this.Interval, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@timeFrame", this.TimeFrame, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@priority", this.Priority, SqlDbType.Int, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isHTMLBased", this.IsHTMLBased, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@attachmentType", this.AttachmentType, SqlDbType.Int, 8, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[EventEmailNotification.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdEventEmailNotification = Convert.ToInt32(databaseObject.Command.Parameters["@idEventEmailNotification"].Value);
                this.IdSite = Convert.ToInt32(databaseObject.Command.Parameters["@idSite"].Value);
                this.Name = databaseObject.Command.Parameters["@name"].Value.ToString();
                this.IdEventType = Convert.ToInt32(databaseObject.Command.Parameters["@idEventType"].Value);
                this.IdEventTypeRecipient = Convert.ToInt32(databaseObject.Command.Parameters["@idEventTypeRecipient"].Value);
                this.IdObject = databaseObject.Command.Parameters["@idObject"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@idObject"].Value);
                this.From = databaseObject.Command.Parameters["@from"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@from"].Value.ToString();
                this.CopyTo = databaseObject.Command.Parameters["@copyTo"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@copyTo"].Value.ToString();
                this.SpecificEmailAddress = databaseObject.Command.Parameters["@specificEmailAddress"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@specificEmailAddress"].Value.ToString();
                this.IsActive = databaseObject.Command.Parameters["@isActive"].Value == DBNull.Value ? (bool?)null : Convert.ToBoolean(databaseObject.Command.Parameters["@isActive"].Value);
                this.DtActivation = databaseObject.Command.Parameters["@dtActivation"].Value == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(databaseObject.Command.Parameters["@dtActivation"].Value);
                this.Interval = databaseObject.Command.Parameters["@Interval"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@interval"].Value);
                this.TimeFrame = databaseObject.Command.Parameters["@timeFrame"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@timeFrame"].Value.ToString();
                this.Priority = databaseObject.Command.Parameters["@priority"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@priority"].Value);
                this.IsHTMLBased = databaseObject.Command.Parameters["@isHTMLBased"].Value == DBNull.Value ? (bool?)null : Convert.ToBoolean(databaseObject.Command.Parameters["@isHTMLBased"].Value);
                this.AttachmentType = databaseObject.Command.Parameters["@attachmentType"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@attachmentType"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for an email notification into a collection.
        /// </summary>
        /// <param name="idGroup">email notification id</param>
        private void _GetPropertiesInLanguages(int idEventEmailNotification)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEventEmailNotification", idEventEmailNotification, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[EventEmailNotification.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["name"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion       
        #endregion
    }
}