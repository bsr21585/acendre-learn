﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Schema;
using Asentia.Common;
using System.Reflection;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using System.Web;

namespace Asentia.LMS.Library.SCORM
{
    public class ManifestValidator
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ManifestValidator()
        { ;}
        #endregion

        #region Public Data Types
        public struct ValidationError
        {
            public int LineNumber;
            public int PositionNumber;
            public string Desc;
        }
        #endregion

        #region Properties

        #endregion

        #region Private Data Members

        private ArrayList _Errors;

        #endregion

        #region Methods
        #region ValidateManifestFileForFatalErrors
        /// <summary>
        /// Validates the imsmanifest.xml file for fatal errors. Fatal errors are errors that will not allow the package to run in the SCORM API.
        /// </summary>
        /// <exception>
        /// </exception>
        /// <param name="imsmanifestFile">This is the imsmanifest.xml file to validate. It could be a relative or an absolute path.</param>
        public ArrayList ValidateManifestFileForFatalErrors(string imsmanifestFile)
        {
            // array list of fatal errors
            ArrayList fatalErrors = new ArrayList();

            try
            {
                XDocument manifestXml = XDocument.Load(imsmanifestFile);

                // must contain a root <manifest> element
                XElement manifestNode = manifestXml.Root;
                string rootNodeName = manifestNode.Name.LocalName;
                XNamespace manifestNS = manifestNode.GetDefaultNamespace();                                

                if (manifestNode == null || rootNodeName != "manifest")
                { fatalErrors.Add("Manifest does not contain required root node &lt;manifest&gt;."); }
                else
                {
                    // <manifest> must contain an identifier attribute
                    if (manifestNode.Attributes("identifier") == null)
                    { fatalErrors.Add("&lt;manifest&gt; element does not contain the required attribute \"identifier\"."); }
                }

                // must contain 1 and only 1 organizations node
                int organizationsCount = manifestNode.Descendants(manifestNS + "organizations").Count();
                XElement organizationsNode = manifestNode.Descendants(manifestNS + "organizations").FirstOrDefault();

                if (organizationsNode == null || organizationsCount > 1)
                { fatalErrors.Add("Manifest must contain exactly 1 &lt;organizations&gt; node."); }
                else
                {
                    // <organizations> must contain a default attribute
                    if (organizationsNode.Attributes("default") == null)
                    { fatalErrors.Add("&lt;organizations&gt; element does not contain the required attribute \"default\"."); }

                    // <organizations> must contain at least 1 <organization> node
                    XElement organizationNode = organizationsNode.Descendants(manifestNS + "organization").FirstOrDefault();

                    if (organizationNode == null)
                    { fatalErrors.Add("&lt;organizations&gt; must contain at least 1 &lt;organization&gt; node."); }
                    else
                    {
                        // <organization> must contain an identifier attribute
                        if (organizationNode.Attributes("identifier") == null)
                        { fatalErrors.Add("&lt;organization&gt; element does not contain the required attribute \"identifier\"."); }

                        // <organization> must contain 1 and only 1 <title> node
                        int organizationTitleCount = organizationNode.Descendants(manifestNS + "title").Count();
                        
                        if (organizationTitleCount == 0)
                        { fatalErrors.Add("&lt;organization&gt; must contain a &lt;title&gt; node."); }

                        // <organization> must contain at least 1 <item> node
                        XElement organizationItemNode = organizationNode.Descendants(manifestNS + "item").FirstOrDefault();

                        if (organizationItemNode == null)
                        { fatalErrors.Add("&lt;organization&gt; must contain at least 1 &lt;item&gt; node."); }
                        else
                        {
                            // <item> must contain an identifier attribute
                            if (organizationItemNode.Attributes("identifier") == null)
                            { fatalErrors.Add("&lt;item&gt; element does not contain the required attribute \"identifier\"."); }

                            // <item> must contain an identifierref attribute
                            if (organizationItemNode.Attributes("identifierref") == null)
                            { fatalErrors.Add("&lt;item&gt; element does not contain the required attribute \"identifierref\"."); }

                            // <item> must contain 1 and only 1 <title> node
                            int itemTitleCount = organizationItemNode.Descendants(manifestNS + "title").Count();

                            if (itemTitleCount != 1)
                            { fatalErrors.Add("&lt;item&gt; must contain exactly 1 &lt;title&gt; node."); }
                        }
                    }
                }

                // must contain 1 and only 1 resources node
                int resourcesCount = manifestNode.Descendants(manifestNS + "resources").Count();
                XElement resourcesNode = manifestNode.Descendants(manifestNS + "resources").FirstOrDefault();

                if (resourcesNode == null || resourcesCount > 1)
                { fatalErrors.Add("Manifest must contain exactly 1 &lt;resources&gt; node."); }
                else
                {
                    // <resources> must contain at least 1 <resource> node
                    XElement resourceNode = resourcesNode.Descendants(manifestNS + "resource").FirstOrDefault();

                    if (resourceNode == null)
                    { fatalErrors.Add("&lt;resources&gt; must contain at least 1 &lt;resource&gt; node."); }
                    else
                    {
                        // <resource> must contain an identifier attribute
                        if (resourceNode.Attributes("identifier") == null)
                        { fatalErrors.Add("&lt;resource&gt; element does not contain the required attribute \"identifier\"."); }

                        // <resource> must contain an type attribute
                        //if (resourceNode.Attributes("type") == null)
                        //{ fatalErrors.Add("&lt;resource&gt; element does not contain the required attribute \"type\"."); }
                    }
                }

                // return
                return fatalErrors;
            }
            catch
            {
                fatalErrors.Add("Manifest is malformed or not a valid XML file.");
                return fatalErrors;
            }            
        }
        #endregion

        #region ValidateManifestFile - Returns only error list
        /// <summary>
        /// Validates the imsmanifest.xml file.
        /// </summary>
        /// <exception>
        /// </exception>
        /// <param name="imsmanifestFile">This is the imsmanifest.xml file to validate. It could be a relative or an absolute path.</param>
        /// <param name="isResource">It will be true if the type of the SCORM package is Resource and false if it is Content.</param>
        public ArrayList ValidateManifestFileToXSD(string imsmanifestFile, out bool isResource)
        {
            try
            {
                //Errors Collection
                _Errors = new ArrayList();

                //Identify the SCORM version
                string scormVersion = _IdentifyScormVersionFromFile(imsmanifestFile);

                XmlUrlResolver resolver = new XmlUrlResolver();
                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;
                settings.XmlResolver = resolver;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

                XmlReaderSettings schemaSettings = new XmlReaderSettings();
                schemaSettings.DtdProcessing = DtdProcessing.Parse;

                switch (scormVersion)
                {
                    case "1.2":
                        settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath("~" + SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/imscp_rootv1p1p2.xsd"), schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath("~" + SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/imsmd_rootv1p2p1.xsd"), schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath("~" + SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/adlcp_rootv1p2.xsd"), schemaSettings));
                        break;
                    default:
                        settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath("~" + SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/imscp_v1p1.xsd"), schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath("~" + SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlcp_v1p3.xsd"), schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath("~" + SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlseq_v1p3.xsd"), schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath("~" + SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlnav_v1p3.xsd"), schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath("~" + SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/imsss_v1p0.xsd"), schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(HttpContext.Current.Server.MapPath("~" + SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/lom.xsd"), schemaSettings));
                        break;
                }

                settings.Schemas.Compile();
                settings.ValidationType = ValidationType.Schema;
                //Set the handler for catching xml validation errors
                settings.ValidationEventHandler += new ValidationEventHandler(_ValidationEventHandler);

                // Create the XmlReader object
                using (XmlReader reader = XmlReader.Create(imsmanifestFile, settings))
                {
                    isResource = true;

                    // Parse the file.
                    while (reader.Read()) // Read and validate against the relevant xsd file (if an error comes the error catching event is called)
                    {
                        //Determine the type of the SCORM Package.
                        //If the organization tag exists then it is a Content otherwise it is a Resource.
                        if (reader.NodeType == XmlNodeType.Element && reader.Name == "organization")
                        {
                            isResource = false;
                            //Don't break here, continue reading/validating the file.
                        }
                    }
                }

                return _Errors;
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion

        #region ValidateManifestFile - Returns error list & identified SCORM version
        /// <summary>
        /// Validate function overload. Validates the imsmanifest.xml file.
        /// </summary>
        /// <exception > 
        /// </exception>        
        /// <param name="imsmanifestFile">This is the imsmanifest.xml file to validate. It could be a relative or an absolute path.</param>
        /// <param name="scormVersion">Returns the scorm version identified.</param>
        public ArrayList ValidateManifestFileToXSD(string imsmanifestFile, out string scormVersion)
        {
            try
            {
                //Errors Collection
                _Errors = new ArrayList();

                //Identify the SCORM version
                scormVersion = _IdentifyScormVersionFromFile(imsmanifestFile);

                XmlUrlResolver resolver = new XmlUrlResolver();
                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;
                settings.XmlResolver = resolver;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

                XmlReaderSettings schemaSettings = new XmlReaderSettings();
                schemaSettings.DtdProcessing = DtdProcessing.Parse;

                switch (scormVersion)
                {
                    case "1.2":
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/imscp_rootv1p1p2.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/imsmd_rootv1p2p1.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/adlcp_rootv1p2.xsd", schemaSettings));
                        break;
                    default:
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/imscp_v1p1.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlcp_v1p3.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlseq_v1p3.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlnav_v1p3.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/imsss_v1p0.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/lom.xsd", schemaSettings));
                        break;
                }

                settings.Schemas.Compile();
                settings.ValidationType = ValidationType.Schema;
                //Set the handler for catching xml validation errors
                settings.ValidationEventHandler += new ValidationEventHandler(_ValidationEventHandler);

                // Create the XmlReader object
                using (XmlReader reader = XmlReader.Create(imsmanifestFile, settings))
                {
                    // Parse the file 
                    while (reader.Read()) ;
                }

                return _Errors;
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion

        #region Validates Manifest String

        #region ValidateManifestString - Returns only error list
        /// <summary>
        /// Validates the imsmanifest.xml string.
        /// </summary>
        /// <exception > 
        /// </exception>        
        /// <param name="imsmanifestString">The string representing the contents of the imsmanifest.xml file.</param>
        public ArrayList ValidateManifestString(string imsmanifestString)
        {
            try
            {
                //Errors Collection
                _Errors = new ArrayList();

                //Identify the SCORM version
                string scormVersion = _IdentifyScormVersionFromString(imsmanifestString);

                XmlUrlResolver resolver = new XmlUrlResolver();
                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;
                settings.XmlResolver = resolver;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

                XmlReaderSettings schemaSettings = new XmlReaderSettings();
                schemaSettings.DtdProcessing = DtdProcessing.Parse;

                switch (scormVersion)
                {
                    case "1.2":
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/imscp_rootv1p1p2.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/imsmd_rootv1p2p1.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/adlcp_rootv1p2.xsd", schemaSettings));
                        break;
                    default:
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/imscp_v1p1.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlcp_v1p3.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlseq_v1p3.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlnav_v1p3.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/imsss_v1p0.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/lom.xsd", schemaSettings));
                        break;
                }

                settings.Schemas.Compile();
                settings.ValidationType = ValidationType.Schema;
                //Set the handler for catching xml validation errors
                settings.ValidationEventHandler += new ValidationEventHandler(_ValidationEventHandler);

                // Create the XmlReader object
                // XmlReader reader = XmlReader.Create(imsmanifestString, settings);
                using (XmlReader reader = XmlReader.Create(new System.IO.StringReader(imsmanifestString)))
                {
                    // Parse the file 
                    while (reader.Read()) ;
                }

                return _Errors;
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion

        #region ValidateManifestString - Returns error list & identified SCORM version
        /// <summary>
        /// Validates the imsmanifest.xml string.
        /// </summary>
        /// <exception > 
        /// </exception>        
        /// <param name="imsmanifestString">The string representing the contents of the imsmanifest.xml file.</param>
        public ArrayList ValidateManifestString(string imsmanifestString, out string scormVersion)
        {
            try
            {
                //Errors Collection
                _Errors = new ArrayList();

                //Identify the SCORM version
                scormVersion = _IdentifyScormVersionFromString(imsmanifestString);

                XmlUrlResolver resolver = new XmlUrlResolver();
                resolver.Credentials = System.Net.CredentialCache.DefaultCredentials;

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Parse;
                settings.XmlResolver = resolver;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;

                XmlReaderSettings schemaSettings = new XmlReaderSettings();
                schemaSettings.DtdProcessing = DtdProcessing.Parse;

                switch (scormVersion)
                {
                    case "1.2":
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/imscp_rootv1p1p2.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/imsmd_rootv1p2p1.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + "1.2/adlcp_rootv1p2.xsd", schemaSettings));
                        break;
                    default:
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/imscp_v1p1.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlcp_v1p3.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlseq_v1p3.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/adlnav_v1p3.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/imsss_v1p0.xsd", schemaSettings));
                        settings.Schemas.Add(null, XmlReader.Create(SitePathConstants.SCORM_MANIFEST_XSD_DIRECTORY + scormVersion + "/lom.xsd", schemaSettings));
                        break;
                }

                settings.Schemas.Compile();
                settings.ValidationType = ValidationType.Schema;
                //Set the handler for catching xml validation errors
                settings.ValidationEventHandler += new ValidationEventHandler(_ValidationEventHandler);

                // Create the XmlReader object
                // XmlReader reader = XmlReader.Create(imsmanifestString, settings);
                using (XmlReader reader = XmlReader.Create(new System.IO.StringReader(imsmanifestString)))
                {
                    // Parse the file 
                    while (reader.Read()) ;
                }

                return _Errors;
            }
            catch
            {
                throw;
            }
            finally
            {

            }
        }
        #endregion

        #endregion
        #endregion

        #region Private Methods

        #region _IdentifyScormVersionFromFile
        /// <summary>
        /// Reads the contents of the xml file and identifies the scorm version against which to validate the imsmanifest file.
        /// </summary>
        /// <exception System.ArgumentException>
        /// The url/path for the file is empty i.e., the argument string imsmanifestFile is an empty/null string.
        /// </exception>
        /// <exception System.IO.FileNotFoundException>
        /// Unable to locate the file at the specified path.
        /// </exception>
        /// <exception System.NullReferenceException>
        /// When the xml file is defected & does not contain a metadata->schemaversion node.
        /// </exception>
        /// <param name="imsmanifestFile">This is the imsmanifest.xml file to validate. It could be a relative or an absolute path.</param>
        private string _IdentifyScormVersionFromFile(string imsmanifestFile)
        {
            string scormVersion = string.Empty;
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(imsmanifestFile);

                // if the xml has a <schemaversion> node under a <metadata> node, it is SCORM 2004, get the edition
                if (xmlDocument.DocumentElement["metadata"] != null && xmlDocument.DocumentElement["metadata"]["schemaversion"] != null)
                { scormVersion = xmlDocument.DocumentElement["metadata"]["schemaversion"].InnerText; }
                else // else, assume SCORM 1.2
                { scormVersion = "1.2"; }
            }
            catch
            {
                throw;
            }

            return scormVersion;
        }
        #endregion

        #region _IdentifyScormVersionFromString
        /// <summary>
        /// Reads the contents of the xml string and identifies the scorm version against which to validate the imsmanifest file.
        /// </summary>
        /// <exception System.ArgumentException>
        /// The argument string imsmanifestString is an empty/null string.
        /// </exception>
        /// <exception System.NullReferenceException>
        /// When the xml string is defected & does not contain a metadata->schemaversion node.
        /// </exception>
        /// <param name="imsmanifestString">The string representing the contents of the imsmanifest.xml file.</param>
        private string _IdentifyScormVersionFromString(string imsmanifestString)
        {
            string scormVersion = string.Empty;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(imsmanifestString);
                scormVersion = doc.DocumentElement["metadata"]["schemaversion"].InnerText;

            }
            catch (Exception e)
            {
                //System.NullReferenceException - When the xml file is defected & does not contain metadata->schemaversion node.
                //System.ArgumentException - The url/path for the file is empty i.e., the string imsmanifestFile is an empty/null string.
                throw;

            }
            finally
            {

            }
            return scormVersion;
        }
        #endregion

        #region _ValidationEventHandler
        /// <summary>
        /// It is called when the xml file fails to vaildate against the given xsd files.
        /// </summary>
        /// <param name=""></param>
        private void _ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            try
            {
                IXmlLineInfo lineInfo = sender as IXmlLineInfo;
                ValidationError error = new ValidationError();
                if (lineInfo != null)
                {
                    error.LineNumber = lineInfo.LineNumber;
                    error.PositionNumber = lineInfo.LinePosition;
                }
                error.Desc = e.Message;

                _Errors.Add(error);
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }
        #endregion

        #endregion
    }
}