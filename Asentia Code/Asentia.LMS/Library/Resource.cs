﻿using Asentia.Common;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Asentia.LMS.Library
{
    public class Resource
    {
        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        public Resource()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }
        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idCourse">Course Id</param>
        public Resource(int resourceId)
        {
            this.LanguageSpecificProperties = new ArrayList();

            this._Details(resourceId);
            this._GetPropertiesInLanguages(resourceId);
        }

        #endregion

        #region public properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Resource.GetGrid]";

        /// <summary>
        /// Id Resource
        /// </summary>
        public int IdResource;

        /// <summary>
        /// Id Resource
        /// </summary>
        public int IdOwner;

        /// <summary>
        /// Resource Name
        /// </summary>
        public string Name;

        /// <summary>
        /// Id Resource Type
        /// </summary>
        public int IdResourceType;

        /// <summary>
        /// Resource Type
        /// </summary>
        public string ResourceType = null;

        /// <summary>
        /// Description
        /// </summary>
        public string Description = null;

        /// <summary>
        /// Objectives
        /// </summary>
        public string Objectives = null;


        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;
        /// <summary>
        /// Resource Location Room
        /// </summary>
        public string LocationRoom;
        /// <summary>
        /// Resource Location Building
        /// </summary>
        public string LocationBuilding;
        /// <summary>
        /// Resource Location City
        /// </summary>
        public string LocationCity;
        /// <summary>
        /// Resource State Province
        /// </summary>
        public string LocationProvince;
        /// <summary>
        /// Resource Location Country
        /// </summary>
        public string LocationCountry;
        /// <summary>
        /// Resource Specific  Owner Name
        /// </summary>
        public string OwnerName;
        /// <summary>
        /// Resource Specific  Owner Email
        /// </summary>
        public string OwnerEmail;
        /// <summary>
        /// Resource Specific  Owner Contact Information
        /// </summary>
        public string OwnerContactInfo;
        /// <summary>
        /// Resource is UserWithinSystem
        /// </summary>
        public Boolean OwnerWithinSystem;
        /// <summary>
        /// Resource Identifier
        /// </summary>
        public string Identifier;
        public int IdParentResource;
        /// <summary>
        /// Resource Is Movable
        /// </summary>
        public bool IsMovable { get; set; }
        /// <summary>
        /// Resource Is Available
        /// </summary>
        public bool IsAvailable { get; set; }
        /// <summary>
        /// Resource Capacity
        /// </summary>
        public int? Capacity { get; set; }

        /// <summary>
        /// IsOutsideUse
        /// </summary>
        public bool? IsOutsideUse { get; set; }
        /// <summary>
        /// IsMaintenance
        /// </summary>
        public bool? IsMaintenance { get; set; }

        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Name;
            public string Description;

            public LanguageSpecificProperty(string langString, string name, string description)
            {
                this.LangString = langString;
                this.Name = name;
                this.Description = description;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves resource data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save();
        }
        #endregion

        #region GetResourceList
        /// <summary>
        /// Gets a listing of Resource type ids and names that are members of this group.
        /// </summary>
        /// <returns>DataTable of Resource type ids and names that are members of this group</returns>
        public DataTable GetResourceTypeList(bool excludeAutoJoinedUsers, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[ResourceType.IDsAndNamesForSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this resource.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="name">resource name</param>
        public void SaveLang(string languageString, string name, string description)
        {
            if (this.IdResource == 0) //
            { throw new AsentiaException(_GlobalResources.ProcedureResourceSaveLangCannotBeCalledWithoutValidGroupID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idResource", this.IdResource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@description", description, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@name", name, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Resource.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetOwners
        /// <summary>
        /// Gets a listing of user ids and names that are experts of this course.
        /// </summary>
        /// <returns>DataTable of user ids and names that are experts of this course</returns>
        public DataTable GetOwners(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idResource", this.IdResource, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Resource.GetOwners]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetParent
        /// <summary>
        /// Gets a listing of Parent Resource ids and names that is parent of this resource.
        /// </summary>
        /// <returns>DataTable of Parent Resource ids and names that are parent of this resource</returns>
        public DataTable GetParent(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idResource", this.IdResource, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Resource.GetParent]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Resource(s).
        /// </summary>
        /// <param name="deletees">DataTable of resources to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Resources", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Resource.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForResourceParentSelectList
        /// <summary>
        /// Gets a listing of user ids and names to populate into a select list.
        /// </summary>
        /// <param name="idResource">resource id</param>
        /// <returns>DataTable of user (owner) ids and names for specified resource excluding current user (owner).</returns>
        public static DataTable IdsAndNamesForResourceParentSelectList(int idResource, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idResource", idResource, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Resource.IdsAndNamesForResourceParentSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForStandupTrainingInstanceSelectList
        /// <summary>
        /// Gets a listing of resource ids and names to populate into a select list.
        /// </summary>
        /// <param name="idStandupTrainingInstance">standup training instance id</param>
        /// <returns>DataTable of resource ids and names not currently attached to the specified session.</returns>
        public static DataTable IdsAndNamesForStandupTrainingInstanceSelectList(int idStandupTrainingInstance, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Resource.IdsAndNamesForStandupTrainingInstanceSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region CheckForResourceAvailability
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idObject"></param>
        /// <param name="idResource"></param>
        /// <param name="timeInterVals"></param>
        /// <returns></returns>
        public static bool CheckForResourceAvailability(int idObject, int idResource, DataTable timeInterVals)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idObject", idObject, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idResource", idResource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@MeetingTimes", timeInterVals, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@isAvailable", 0, SqlDbType.Bit, 1, ParameterDirection.Output);
            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Resource.CheckAvailability]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isAvailable"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Saves resource data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved resource</returns>
        private void _Details(int resourceId)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idResource", resourceId, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@description", this.Description, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@idResourceType", this.IdResourceType, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@resourceType", this.ResourceType, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@identifier", this.Identifier, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@isMovable", this.IsMovable, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isAvailable", this.IsAvailable, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@capacity", this.Capacity, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@room", this.LocationRoom, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@building", this.LocationBuilding, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@city", this.LocationCity, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@province", this.LocationProvince, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@country", this.LocationCountry, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@idOwner", resourceId, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@ownerName", this.OwnerName, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@ownerEmail", this.OwnerEmail, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@ownerContactInfo", this.OwnerContactInfo, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@ownerWithinSystem", this.OwnerWithinSystem, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@idParentResource", resourceId, SqlDbType.Int, 4, ParameterDirection.Output);
            try
            {
                databaseObject.ExecuteNonQuery("[Resource.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved resource
                this.IdResource = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idResource"].Value);
                this.Name = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@name"].Value);
                this.Description = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@description"].Value);
                this.IdResourceType = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idResourceType"].Value);
                this.Identifier = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@identifier"].Value);
                this.ResourceType = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@resourceType"].Value);
                this.IsMovable = Convert.ToBoolean(databaseObject.Command.Parameters["@isMovable"].Value);
                this.IsAvailable = Convert.ToBoolean(databaseObject.Command.Parameters["@isAvailable"].Value);
                this.Capacity = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@capacity"].Value);
                this.LocationRoom = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@room"].Value);
                this.LocationBuilding = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@building"].Value);
                this.LocationCity = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@city"].Value);
                this.LocationProvince = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@province"].Value);
                this.LocationCountry = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@country"].Value);
                this.OwnerName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@ownerName"].Value);
                this.OwnerEmail = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@ownerEmail"].Value);
                this.OwnerContactInfo = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@ownerContactInfo"].Value);
                this.OwnerWithinSystem = Convert.ToBoolean(databaseObject.Command.Parameters["@ownerWithinSystem"].Value);
                this.IdOwner = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idOwner"].Value);
                this.IdParentResource = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idParentResource"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Save
        /// <summary>
        /// Saves resource data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved resource</returns>
        private int _Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idParentResource", this.IdParentResource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idResource", this.IdResource, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idOwner", this.IdOwner, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idResourceType", this.IdResourceType, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@description", this.Description, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@identifier", this.Identifier, SqlDbType.NVarChar, 50, ParameterDirection.Input);
            databaseObject.AddParameter("@isMovable", this.IsMovable, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@isAvailable", this.IsAvailable, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@capacity", this.Capacity, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@room", this.LocationRoom, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@building", this.LocationBuilding, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@city", this.LocationCity, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@province", this.LocationProvince, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@country", this.LocationCountry, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@ownerWithinSystem", this.OwnerWithinSystem, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@ownerName", this.OwnerName, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@ownerEmail", this.OwnerEmail, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@ownerContactInfo", this.OwnerContactInfo, SqlDbType.NVarChar, 512, ParameterDirection.Input);


            try
            {
                databaseObject.ExecuteNonQuery("[Resource.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved resource
                this.IdResource = Convert.ToInt32(databaseObject.Command.Parameters["@idResource"].Value);

                // return
                return this.IdResource;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a group into a collection.
        /// </summary>
        /// <param name="idGroup">group id</param>
        private void _GetPropertiesInLanguages(int idResource)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idResource", idResource, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Resource.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["name"].ToString(),
                            sdr["description"].ToString()
                            )
                    );
                }

                sdr.Close();

                //sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
