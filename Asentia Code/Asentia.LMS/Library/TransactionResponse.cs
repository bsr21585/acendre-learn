﻿using System;
using System.Data;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class TransactionResponse
    {
        #region Constructor
        public TransactionResponse(int idResponse)
        {
            this._Details(idResponse);

        }
        public TransactionResponse()
        {
        }
        #endregion

        #region  Public properties
        /// <summary>
        /// Response Id
        /// </summary>
        public int Id;

        /// <summary>
        /// IdUser
        /// </summary>
        public int IdUser;

        /// <summary>
        /// Order number or Invoice number passed by merchant
        /// </summary>
        public string OrderNumber;

        /// <summary>
        /// Card Type (Visa,MasterCrad,Amex etc)
        /// </summary>
        public string CardType;

        /// <summary>
        /// Card number
        /// </summary>
        public string cardNumber;

        /// <summary>
        /// Transaction Id return by payment processor
        /// </summary>
        public string TransactionId;

        /// <summary>
        /// Transaction Amount
        /// </summary>
        public decimal Amount;

        /// <summary>
        /// Response Code
        /// </summary>
        public string ResponseCode;

        /// <summary>
        /// Response Message
        /// </summary>
        public string ResponseMessage;

        /// <summary>
        /// Transaction status (isApproved)
        /// </summary>
        public bool IsApproved;

        /// <summary>
        /// IsValid
        /// </summary>
        public bool IsValid;

        /// <summary>
        /// Transaction Date
        /// </summary>
        public DateTime DtTransaction;
        #endregion

        #region Methods

        #region Save
        /// <summary>
        /// Saves resource data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved resource</returns>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@id", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            //here passing userid (used in DPM service) it is not AsentiaSessionState.IdSiteUser because there it becomes zero
            if (AsentiaSessionState.IdSiteUser > 0)
            {
                this.IdUser = AsentiaSessionState.IdSiteUser;
            }
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@invoiceNumber", this.OrderNumber, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@cardType", this.CardType, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@cardNumber", this.cardNumber, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idTransaction", this.TransactionId, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@responseCode", this.ResponseCode, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@amount", this.Amount, SqlDbType.Float, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@responseMessage", this.ResponseMessage, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtTransaction", this.DtTransaction, SqlDbType.NVarChar, null, ParameterDirection.Input);
            databaseObject.AddParameter("@isApproved", this.IsApproved, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@isValidated", this.IsValid, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[TransactionResponse.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved resource
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@id"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion

        #region _Details
        /// <summary>
        /// Saves resource data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved resource</returns>
        private void _Details(int idResponse)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@id", idResponse, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@invoiceNumber", this.OrderNumber, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@cardType", this.CardType, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@cardNumber", this.cardNumber, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@idTransaction", this.TransactionId, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@responseCode", this.ResponseCode, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@amount", this.Amount, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@responseMessage", this.ResponseMessage, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtTransaction", this.DtTransaction, SqlDbType.DateTime, null, ParameterDirection.Output);
            databaseObject.AddParameter("@isApproved", this.IsApproved, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isValidated", this.IsValid, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[TransactionResponse.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved resource
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@id"].Value);
                this.OrderNumber = Convert.ToString(databaseObject.Command.Parameters["@invoiceNumber"].Value);
                this.cardNumber = Convert.ToString(databaseObject.Command.Parameters["@cardNumber"].Value);
                this.TransactionId = Convert.ToString(databaseObject.Command.Parameters["@idTransaction"].Value);
                this.ResponseCode = Convert.ToString(databaseObject.Command.Parameters["@responseCode"].Value);
                this.ResponseMessage = Convert.ToString(databaseObject.Command.Parameters["@responseMessage"].Value);
                this.Amount = Convert.ToDecimal(databaseObject.Command.Parameters["@amount"].Value);
                this.IsApproved = Convert.ToBoolean(databaseObject.Command.Parameters["@isApproved"].Value);
                this.IsValid = Convert.ToBoolean(databaseObject.Command.Parameters["@isValidated"].Value);
                this.DtTransaction = Convert.ToDateTime(databaseObject.Command.Parameters["@dtTransaction"].Value);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion

        #endregion
    }
}
