﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Serialization;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    #region EnrollmentType ENUM
    public enum EnrollmentType
    {
        RuleSetInheritedOneTime = 1,
        RuleSetInheritedRecurring = 2,
        GroupInheritedOneTime = 3,
        ManuallyAssignedOneTime = 4,
    }
    #endregion

    #region EnrollmentStatus ENUM
    public enum EnrollmentStatus
    {
        Enrolled = 1,
        Completed = 2,
        Overdue = 3,
        Expired = 4,
        Future = 5,
    }
    #endregion    

    [Serializable]
    public class Enrollment
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Enrollment()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idEnrollment">Enrollment Id</param>
        public Enrollment(int idEnrollment)
        {
            // synchronize lesson data first
            this._SynchronizeLessonData(idEnrollment);

            // load details
            this._Details(idEnrollment);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Enrollment.GetGrid]";

        /// <summary>
        /// Stored procedure used to populate a grid for this object for the current session user's dashboard.
        /// </summary>
        public static readonly string GridProcedureForUserDashboard = "[Enrollment.GetGridForUserDashboard]";

        /// <summary>
        /// Enrollment Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Course Id.
        /// </summary>
        public int IdCourse;

        /// <summary>
        /// User Id.
        /// </summary>
        public int IdUser;

        /// <summary>
        /// Group Enrollment Id.
        /// </summary>
        public int? IdGroupEnrollment;

        /// <summary>
        /// Rule Set Enrollment Id.
        /// </summary>
        public int? IdRuleSetEnrollment;

        /// <summary>
        /// Learning Path Enrollment Id.
        /// </summary>
        public int? IdLearningPathEnrollment;

        /// <summary>
        /// Time Zone Id.
        /// </summary>
        public int IdTimezone;

        /// <summary>
        /// Is Locked By Prerequisites.
        /// </summary>
        public bool IsLockedByPrerequisites;

        /// <summary>
        /// Code.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Code;

        /// <summary>
        /// Revision Code.
        /// </summary>
        public string RevCode;

        /// <summary>
        /// Title.
        /// </summary>
        public string Title;

        /// <summary>
        /// Date Start.
        /// </summary>
        public DateTime DtStart;

        /// <summary>
        /// Date Due.
        /// </summary>
        public DateTime? DtDue;

        /// <summary>
        /// Date Expires from Start.
        /// </summary>
        public DateTime? DtExpiresFromStart;

        /// <summary>
        /// Date Expires from First Launch.
        /// </summary>
        public DateTime? DtExpiresFromFirstLaunch;

        /// <summary>
        /// The effective date of enrollment expiration. 
        /// Calculated and populated in _Details for an Enrollment by determining the first date to occur.
        /// </summary>
        public DateTime? DtEffectiveExpires;

        /// <summary>
        /// Date First Launch.
        /// </summary>
        public DateTime? DtFirstLaunch;

        /// <summary>
        /// Date Completed.
        /// </summary>
        public DateTime? DtCompleted;

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Date Last Synchronized.
        /// </summary>
        public DateTime? DtLastSynchronized;

        /// <summary>
        /// Due Interval
        /// </summary>       
        public int? DueInterval;

        /// <summary>
        /// Due TimeFrame
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string DueTimeframe;

        /// <summary>
        /// Expires from Start Interval.
        /// </summary>
        public int? ExpiresFromStartInterval;

        /// <summary>
        /// Expires from Start Timeframe.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string ExpiresFromStartTimeframe;

        /// <summary>
        /// Expires from First Launch Interval.
        /// </summary>
        public int? ExpiresFromFirstLaunchInterval;

        /// <summary>
        /// Expires from First Launch Timeframe.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string ExpiresFromFirstLaunchTimeframe;

        /// <summary>
        /// Activity Import Id.
        /// </summary>
        public int? IdActivityImport;
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves enrollment data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves enrollment data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveForMultipleCourses
        /// <summary>
        /// Saves enrollment data for multiple courses to the database as the current session user.
        /// This is used/called for creation of new enrollments where we create enrollments for multiple courses.
        /// </summary>
        public void SaveForMultipleCourses(DataTable courses)
        {
            this._SaveForMultipleCourses(AsentiaSessionState.IdSiteUser, courses);
        }
        #endregion

        #region SaveCompletionStatus
        /// <summary>
        /// Saves enrollment completion status to the database as the current session user.
        /// </summary>
        public int SaveCompletionStatus()
        {
            return this._SaveCompletionStatus(AsentiaSessionState.IdSiteUser);
        }
        #endregion

        #region EnrollCatalog
        /// <summary>
        /// Handles catalog enrollment for self-enrolled free and purchased catalogs.
        /// 
        /// - This makes calls to EnrollCourse for each course enrollment.
        /// - This will create a transaction item if one is to be created, i.e. the catalog is free.
        /// - For purchased catalogs, this will fire off the process to create all enrollments, but
        ///   it will not confirm transaction items or disassociate transaction items from purchase
        ///   if a coupon code makes it free. That will be done by the purchase confirmation processes.
        /// </summary>
        /// <param name="idCatalog">catalog id</param>
        /// <param name="idUser">the id of the user to enroll</param>
        /// <param name="idTransactionItem">existing transaction item id to link this to, passed through to EnrollCourse, or 0 if we are to create a new transaction item</param>
        public void EnrollCatalog(int idCatalog, int idUser, int idTransactionItem)
        {
            if (AsentiaSessionState.IdSite == 0)
            { throw new AsentiaException(_GlobalResources.YouMustBeLoggedInToEnrollInThisCatalog); }

            //if (AsentiaSessionState.IdSiteUser == 1)
            //{ throw new AsentiaException(_GlobalResources.TheSystemAdministratorCannotEnrollInCatalogs); }

            try
            {
                // get user and catalog objects
                Asentia.UMS.Library.User userObject = new Asentia.UMS.Library.User(idUser);
                Asentia.LMS.Library.Catalog catalogObject = new Asentia.LMS.Library.Catalog(idCatalog);

                // check the published and closed status of the catalog
                if (catalogObject.IsPublished == false || catalogObject.IsClosed == true)
                { throw new AsentiaException(_GlobalResources.YouCannotEnrollInThisCatalog); }                

                // get a datatable of courses that belong to this catalog
                DataTable catalogCourses = Asentia.LMS.Library.Catalog.GetCourses(idCatalog);

                if (catalogCourses.Rows.Count == 0)
                { throw new AsentiaException(_GlobalResources.ThisCatalogDoesNotContainAnyCourses); }
                
                // insert a record into tblTransactionItem for the catalog enrollment if idTransactionItem = 0
                if (idTransactionItem == 0)
                {
                    TransactionItem transactionItemObject = new TransactionItem();
                    transactionItemObject.Id = 0;
                    transactionItemObject.IdParentTransactionItem = null;
                    transactionItemObject.IdPurchase = null;
                    transactionItemObject.IdEnrollment = null;
                    transactionItemObject.IdUser = userObject.Id;
                    transactionItemObject.UserName = userObject.DisplayName;
                    transactionItemObject.IdAssigner = userObject.Id;
                    transactionItemObject.AssignerName = userObject.DisplayName;
                    transactionItemObject.ItemId = catalogObject.IdCatalog;
                    transactionItemObject.ItemName = catalogObject.Title;
                    transactionItemObject.ItemType = PurchaseItemType.Catalog;
                    transactionItemObject.Description = catalogObject.ShortDescription;
                    transactionItemObject.Cost = 0;
                    transactionItemObject.Paid = 0;
                    transactionItemObject.IdCouponCode = null;
                    transactionItemObject.CouponCode = null;
                    transactionItemObject.Confirmed = true;

                    // save the transaction item object
                    idTransactionItem = transactionItemObject.Save();

                    // if we didnt get an id back from the transaction item, do not continue
                    if (idTransactionItem == 0)
                    { throw new AsentiaException(_GlobalResources.AFatalErrorOccurredWhileProcessingThisEnrollmentPleaseContactAnAdministrator); }
                }

                // loop through each course to enroll
                foreach (DataRow row in catalogCourses.Rows)
                {
                    bool isClosed = false;

                    if (!String.IsNullOrWhiteSpace(row["isClosed"].ToString()))
                    { isClosed = Convert.ToBoolean(row["isClosed"]); }

                    Course courseObject = new Course(Convert.ToInt32(row["idCourse"]));

                    if (!isClosed)
                    {
                        // Check if self-enrollment approval is required for the course
                        if ((bool)courseObject.IsSelfEnrollmentApprovalRequired)
                        {
                            // submit an enrollment request
                            EnrollmentRequest enrollmentRequestObject = new EnrollmentRequest();
                            int enrollmentId = enrollmentRequestObject.Submit(courseObject.Id, idUser);
                        }
                        else
                        {
                            // enroll the user in the course
                            this.EnrollCourse(courseObject.Id, idUser, 0, idTransactionItem);
                        }                         
                    }
                }                
            }
            catch
            {                
                throw;
            }            
        }
        #endregion

        #region EnrollCourse
        /// <summary>
        /// Handles course enrollment for self-enrolled free and purchased courses, as well as the course 
        /// enrollments cascaded from a catalog enrollment (EnrollCatalog).
        /// 
        /// - This will create a transaction item if one is to be created, i.e. the course is free or this 
        ///   is fired by EnrollCatalog .
        /// - For purchased courses, this will create the enrollment and link it to the transaction item,
        ///   but it will not confirm transaction items or disassociate transaction items from purchase
        ///   if a coupon code makes it free. That will be done by the purchase confirmation processes.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="idUser">the id of the user to enroll</param>
        /// <param name="idTransactionItem">existing transaction item id to link this to, 0 if we are to create a new transaction item</param>
        /// <param name="idParentTransactionItem">parent transaction item id, a parent transaction item would be the transaction item of the catalog item</param>
        public void EnrollCourse(int idCourse, int idUser, int idTransactionItem, int idParentTransactionItem = 0)
        {
            if (AsentiaSessionState.IdSite == 0)
            { throw new AsentiaException(_GlobalResources.YouMustBeLoggedInToEnrollInThisCourse); }

            //if (AsentiaSessionState.IdSiteUser == 1)
            //{ throw new AsentiaException(_GlobalResources.TheSystemAdministratorCannotEnrollInCourses); }

            try
            {
                // get user and course objects
                Asentia.UMS.Library.User userObject = new Asentia.UMS.Library.User(idUser);
                Asentia.LMS.Library.Course courseObject = new Asentia.LMS.Library.Course(idCourse);

                // check the published and closed status of the course
                // note, if this is originating from a catalog enrollment we dont care about published and closed for the course
                if ((courseObject.IsPublished == false || courseObject.IsClosed == true) && idParentTransactionItem == 0) 
                { throw new AsentiaException(_GlobalResources.YouCannotEnrollInThisCourse); }                

                this.Id = 0; // enrollment id is 0 since we are creating new, this is redundant but it guarantees new
                this.IdCourse = courseObject.Id;
                this.IdUser = userObject.Id;
                this.IdTimezone = userObject.IdTimezone;
                this.IsLockedByPrerequisites = true;
                this.DtStart = AsentiaSessionState.UtcNow;

                // set self-enrollment due interval
                if (courseObject.SelfEnrollmentDueInterval != null && courseObject.SelfEnrollmentDueTimeframe != null)
                {
                    this.DueInterval = courseObject.SelfEnrollmentDueInterval;
                    this.DueTimeframe = courseObject.SelfEnrollmentDueTimeframe;
                }

                // set self-enrollment access from start interval
                if (courseObject.SelfEnrollmentExpiresFromStartInterval != null && courseObject.SelfEnrollmentExpiresFromStartTimeframe != null)
                {
                    this.ExpiresFromStartInterval = courseObject.SelfEnrollmentExpiresFromStartInterval;
                    this.ExpiresFromStartTimeframe = courseObject.SelfEnrollmentExpiresFromStartTimeframe;
                }

                // set self-enrollment access from first launch interval
                if (courseObject.SelfEnrollmentExpiresFromFirstLaunchInterval != null && courseObject.SelfEnrollmentExpiresFromFirstLaunchTimeframe != null)
                {
                    this.ExpiresFromFirstLaunchInterval = courseObject.SelfEnrollmentExpiresFromFirstLaunchInterval;
                    this.ExpiresFromFirstLaunchTimeframe = courseObject.SelfEnrollmentExpiresFromFirstLaunchTimeframe;
                }
                
                // save the enrollment
                this.Save();

                // if the enrollment was saved, 
                // either log a transaction item for it if idTransactionItem = 0
                // or update idEnrollment for an existing transaction item if idTransactionItem > 0
                if (this.Id > 0)
                {
                    if (idTransactionItem > 0)
                    {
                        TransactionItem transactionItemObject = new TransactionItem(idTransactionItem);
                        transactionItemObject.IdEnrollment = this.Id;
                        transactionItemObject.Save();
                    }
                    else
                    {
                        TransactionItem transactionItemObject = new TransactionItem();
                        transactionItemObject.Id = 0;

                        if (idParentTransactionItem > 0)
                        { transactionItemObject.IdParentTransactionItem = idParentTransactionItem; }
                        else
                        { transactionItemObject.IdParentTransactionItem = null; }

                        transactionItemObject.IdPurchase = null;
                        transactionItemObject.IdEnrollment = this.Id;
                        transactionItemObject.IdUser = userObject.Id;
                        transactionItemObject.UserName = userObject.DisplayName;
                        transactionItemObject.IdAssigner = userObject.Id;
                        transactionItemObject.AssignerName = userObject.DisplayName;
                        transactionItemObject.ItemId = courseObject.Id;
                        transactionItemObject.ItemName = courseObject.Title;
                        transactionItemObject.ItemType = PurchaseItemType.Course;
                        transactionItemObject.Description = courseObject.ShortDescription;
                        transactionItemObject.Cost = 0;
                        transactionItemObject.Paid = 0;
                        transactionItemObject.IdCouponCode = null;
                        transactionItemObject.CouponCode = null;
                        transactionItemObject.Confirmed = true;

                        // save the transaction item object
                        transactionItemObject.Save();
                    }
                }
            }
            catch
            {                
                throw;
            }            
        }
        #endregion

        #region SetLessonDataCompletionForStandupTraining
        /// <summary>
        /// Looks for standup training instances a learner has taken prior to being enrolled in a course
        /// that can be counted as credit toward completion of the enrollment, and marks the appropriate
        /// lesson data record(s) as completed for them.
        ///
        /// This looks back 30 days prior to the enrollment start and applies any standup training instances
        /// that a user has completed within that time period as credit for lesson data where that standup 
        /// training module is a requirement.
        ///
        /// Note that if a user enrolls in a standup training while currently enrolled in a course the 
        /// standup training applies to, the manage roster functionality for the standup training instance
        /// takes care of completing lesson data. This just fills in the gap where the standup training
        /// instance was completed prior to the user having the enrollment.
        /// </summary>
        /// <param name="idEnrollment">the enrollment's id</param>
        public void SetLessonDataCompletionForStandupTraining()
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureEnrollmentSetLessonDataCompletionForStandupTrainingCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Enrollment.SetLessonDataCompletionForStandupTraining]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Enrollment(s).
        /// </summary>
        /// <param name="deletees">DataTable of enrollments to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Enrollments", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Enrollment.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region TogglePrerequisiteLock
        /// <summary>
        /// Toggles the IsLockedByPrerequisites flag for an Enrollment.
        /// </summary>
        /// <param name="idEnrollment">enrollment id</param>
        public static void TogglePrerequisiteLock(int idEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Enrollment.TogglePrerequisiteLock]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Reset
        /// <summary>
        /// Resets an enrollment.
        /// </summary>
        /// <param name="idEnrollment">id of the enrollment to reset</param>
        public static void Reset(int idEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Enrollment.Reset]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetLessonDataIds
        /// <summary>
        /// Returns a recordset of lesson data ids along with the user id the enrollment belongs to.
        /// This is used for the purpose of deleting SCO data for lesson data from the filesystem for enrollment reset.
        /// </summary>
        /// <param name="idEnrollment">id of the enrollment to get lesson data ids for</param>
        public static DataTable GetLessonDataIds(int idEnrollment)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Enrollment.GetLessonDataIds]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetEnrollmentsAPI
        /// <summary>
        /// Return a list of enrollments for API
        /// </summary>
        public static List<int> GetEnrollmentsAPI(int idUser, int idCourse)
        {
            List<int> returnIDs = new List<int>();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Enrollment.IdsForAPI]", true);
                while (sdr.Read())
                {
                    returnIDs.Add(Convert.ToInt32(sdr[0]));
                }
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return returnIDs;
        }
        #endregion

        #region ProcessContentChangeLessonDataFlags
        /// <summary>
        /// Processes the lesson data content change flags for an enrollment.
        ///
        /// This procedure will:
        ///
        /// 1) Reset lesson data for lesson data records that have the resetForContentChange flag set and 
        /// the lesson data and enrollment are not completed. Completion check is requred due to the fact 
        /// that the learner may have been interacting with the original lesson content when the flag was 
        /// set and subsequently completed it.
        ///
        /// 2) Unsets the resetForContentChange flag and sets the preventPostCompletionLaunchForContentChange
        /// flag for lesson data that has the resetForContentChange flag set and has been completed or has had
        /// the enrollment completed. This covers the scenario described above where the learner was interacting 
        /// with the original content when the resetForContentChange was set.
        ///
        /// 3) The procedure will return a recordset of lesson data ids that it has reset. This is so that
        /// we can delete any lesson data files on the file system.
        ///
        /// Note: Checks on enrollment completion where lesson data may not be completed are necessary because
        /// an administrator has the ability to mark an enrollment as completed which does not mark the lesson
        /// data as completed, and we do not want to reset lesson data for those cases because the enrollment's
        /// completion would be unset as a result. 
        /// </summary>
        /// <param name="idEnrollment">the enrollment's id</param>
        public static bool ProcessContentChangeLessonDataFlags(int idUser, int idEnrollment)
        {
            bool recordsWereReset = false;
            DataTable dt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Enrollment.ProcessContentChangeLessonDataFlags]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // if there were lesson data records reset, remove SCO data files for them and return true
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        // remove the sco data log files
                        DataLesson.DeleteSCOLogDataFiles(idUser, Convert.ToInt32(row["idData-Lesson"]));
                    }

                    recordsWereReset = true;
                }
                else // otherwise, do nothing and we will return false
                { }

                return recordsWereReset;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetEnrollmentDataTableForUser
        /// <summary>
        /// Gets a data table for a list of enrollments for a user
        /// </summary>
        /// <param name="enrollmentStatus">enrollment status</param>
        /// <returns></returns>
        public static DataTable GetEnrollmentDataTableForUser(string enrollmentStatus)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@searchParam", "", SqlDbType.NVarChar, 4000, ParameterDirection.Input);
            databaseObject.AddParameter("@pageNum", 1, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@pageSize", 1000, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@orderColumn", "title", SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@orderAsc", 1, SqlDbType.Bit, 1, ParameterDirection.Input);

            databaseObject.AddParameter("@enrollmentStatus", enrollmentStatus, SqlDbType.NVarChar, 20, ParameterDirection.Input);

            try
            {
                DataSet ds = databaseObject.ExecuteDataSet("[Enrollment.GetGridForUserDashboard]", true);

                //DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                //string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                //AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return ds.Tables[1];
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves enrollment data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the enrollment</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();            

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idCourse", this.IdCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);

            if (this.IdRuleSetEnrollment == null)
            { databaseObject.AddParameter("@idRuleSetEnrollment", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idRuleSetEnrollment", this.IdRuleSetEnrollment, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.IdGroupEnrollment == null)
            { databaseObject.AddParameter("@idGroupEnrollment", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idGroupEnrollment", this.IdGroupEnrollment, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.IdLearningPathEnrollment == null)
            { databaseObject.AddParameter("@idLearningPathEnrollment", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idLearningPathEnrollment", this.IdLearningPathEnrollment, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isLockedByPrerequisites", this.IsLockedByPrerequisites, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);

            if (this.DueInterval == null)
            { databaseObject.AddParameter("@dueInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dueInterval", this.DueInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@dueTimeframe", this.DueTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromStartInterval == null)
            { databaseObject.AddParameter("@expiresFromStartInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromStartInterval", this.ExpiresFromStartInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromStartTimeframe", this.ExpiresFromStartTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromFirstLaunchInterval == null)
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", this.ExpiresFromFirstLaunchInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", this.ExpiresFromFirstLaunchTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Enrollment.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved enrollment
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idEnrollment"].Value);                

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SaveForMultipleCourses
        /// <summary>
        /// Saves enrollment data for multiple courses to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <param name="courses">DataTable of course ids to create enrollments for</param>
        private void _SaveForMultipleCourses(int idCaller, DataTable courses)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Courses", courses, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isLockedByPrerequisites", this.IsLockedByPrerequisites, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);

            if (this.DueInterval == null)
            { databaseObject.AddParameter("@dueInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dueInterval", this.DueInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@dueTimeframe", this.DueTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromStartInterval == null)
            { databaseObject.AddParameter("@expiresFromStartInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromStartInterval", this.ExpiresFromStartInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromStartTimeframe", this.ExpiresFromStartTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromFirstLaunchInterval == null)
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", this.ExpiresFromFirstLaunchInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", this.ExpiresFromFirstLaunchTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Enrollment.SaveForMultipleCourses]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SaveCompletionStatus
        /// <summary>
        /// Saves enrollment completion status to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the enrollment</returns>
        private int _SaveCompletionStatus(int idCaller)
        {
            // make sure an enrollment id exists, if not, throw exception!

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);

            if (this.DtCompleted == null)
            { databaseObject.AddParameter("@dtCompleted", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dtCompleted", this.DtCompleted, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[Enrollment.SaveCompletionStatus]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved enrollment
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idEnrollment"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SynchronizeLessonData
        /// <summary>
        /// Synchronizes lesson data for an Enrollment.
        /// </summary>
        /// <param name="idEnrollment">the enrollment's id</param>
        private void _SynchronizeLessonData(int idEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Enrollment.SynchronizeLessonData]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified enrollment's properties.
        /// </summary>
        /// <param name="idEnrollment">Enrollment Id</param>
        private void _Details(int idEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idCourse", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idGroupEnrollment", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idRuleSetEnrollment", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idLearningPathEnrollment", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimezone", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@isLockedByPrerequisites", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@code", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@revcode", null, SqlDbType.NVarChar, 32, ParameterDirection.Output);
            databaseObject.AddParameter("@title", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@dtStart", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDue", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtExpiresFromStart", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtExpiresFromFirstLaunch", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtFirstLaunch", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCompleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtLastSynchronized", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dueInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dueTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idActivityImport", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Enrollment.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idEnrollment;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdCourse = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idCourse"].Value);
                this.IdUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.IdGroupEnrollment = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idGroupEnrollment"].Value);
                this.IdRuleSetEnrollment = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idRuleSetEnrollment"].Value);
                this.IdLearningPathEnrollment = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idLearningPathEnrollment"].Value);
                this.IdTimezone = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idTimezone"].Value);
                this.IsLockedByPrerequisites = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isLockedByPrerequisites"].Value);
                this.Code = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@code"].Value);
                this.RevCode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@revcode"].Value);
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.DtStart = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtStart"].Value);
                this.DtDue = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDue"].Value);
                this.DtExpiresFromStart = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtExpiresFromStart"].Value);
                this.DtExpiresFromFirstLaunch = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtExpiresFromStart"].Value);
                this.DtFirstLaunch = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtFirstLaunch"].Value);
                this.DtCompleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtCompleted"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtLastSynchronized = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtLastSynchronized"].Value);
                this.DueInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@dueInterval"].Value);
                this.DueTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@dueTimeframe"].Value);
                this.ExpiresFromStartInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromStartInterval"].Value);
                this.ExpiresFromStartTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromStartTimeframe"].Value);
                this.ExpiresFromFirstLaunchInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromFirstLaunchInterval"].Value);
                this.ExpiresFromFirstLaunchTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromFirstLaunchTimeframe"].Value);
                this.IdActivityImport = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idActivityImport"].Value);

                // calculate the effective expiration date
                if (this.DtExpiresFromStart != null || this.DtExpiresFromFirstLaunch != null)
                {
                    if (this.DtExpiresFromStart == null)
                    { this.DtEffectiveExpires = this.DtExpiresFromFirstLaunch; }
                    else if (this.DtExpiresFromFirstLaunch == null)
                    { this.DtEffectiveExpires = this.DtExpiresFromStart; }
                    else
                    {
                        if (this.DtExpiresFromStart > this.DtExpiresFromFirstLaunch)
                        { this.DtEffectiveExpires = this.DtExpiresFromFirstLaunch; }
                        else
                        { this.DtEffectiveExpires = this.DtExpiresFromStart; }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}