﻿using Asentia.Common;
using System;
using System.Data;

namespace Asentia.LMS.Library
{
    public class EventLog
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public EventLog()
        { ;}
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string SystemEventLogGridProcedure = "[EventLog.GetGrid]";
        #endregion

        #region Static Methods
        #endregion
    }
}