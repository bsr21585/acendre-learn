﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Asentia.UMS.Library;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class CertificateImport
    {
        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[CertificateImport.GetGrid]";

        /// <summary>
        /// Field Identifier
        /// </summary>
        public string Identifier;

        /// <summary>
        /// Field Name
        /// </summary>
        public string ColumnName;

        /// <summary>
        /// Is the field always required?
        /// </summary>
        public bool AlwaysRequired;

        /// <summary>
        /// The data type of the field.
        /// </summary>
        public UserAccountData.DataType DataType;

        /// <summary>
        /// The maximum length of the field, per database constraint.
        /// </summary>
        public int MaxLength;

        /// <summary>
        /// Format
        /// </summary>
        public string Format;
        #endregion

        #region Methods

        #region ImportRecord
        /// <summary>
        /// Record Import CertificateImport to table tblCertificateImport
        /// </summary>
        /// <param name="">Ac</param>
        public static int ImportRecord(string idUserImported, string importFileName)
        {
            int idCertificateImport = 0;
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificateImport", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUserImported", Convert.ToInt32(idUserImported), SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@importFileName", importFileName, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CertificateImport.ImportRecord]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                idCertificateImport = Convert.ToInt32(databaseObject.Command.Parameters["@idCertificateImport"].Value);
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return idCertificateImport;
        }
        #endregion

        #region Import
        /// <summary>
        /// Import a DataTable of CertificateImport Data
        /// </summary>
        /// <param name="CertificateImport">DataTable of CertificateImport and their properties</param>
        public static void Import(int idCertificateImport, DataTable CertificateImportTable)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificateImport", idCertificateImport, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@CertificateImportTable", CertificateImportTable, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CertificateImport.Import]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes Activities.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@CertificatesImport", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[CertificateImport.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCertificateImportColumnsInfo
        /// <summary>
        /// Loads all column info from XML file and returns them.
        /// </summary>
        /// <returns>Returns list of CertificateImport columns.</returns>
        public static List<CertificateImport> GetCertificateImportColumnsInfo()
        {
            List<CertificateImport> columnList = new List<CertificateImport>();
            string dateTimeFormat = _GlobalResources.ISODatetime + ": " + DateTime.UtcNow.ToString("yyyy-MM-dd") + " 00:00:00";

            // Add Username Column
            CertificateImport columnUserName = new CertificateImport();
            columnUserName.Identifier = "UserName";
            columnUserName.ColumnName = _GlobalResources.Username;
            columnUserName.AlwaysRequired = true;
            columnUserName.DataType = (UserAccountData.DataType)Enum.Parse(typeof(UserAccountData.DataType), "String");
            columnUserName.MaxLength = 255;
            columnUserName.Format = String.Empty;
            columnList.Add(columnUserName);

            // Add Certificate Code Column
            CertificateImport columnCertificateCode = new CertificateImport();
            columnCertificateCode.Identifier = "CertificateCode";
            columnCertificateCode.ColumnName = _GlobalResources.CertificateCode;
            columnCertificateCode.AlwaysRequired = true;
            columnCertificateCode.DataType = (UserAccountData.DataType)Enum.Parse(typeof(UserAccountData.DataType), "String");
            columnCertificateCode.MaxLength = 255;
            columnCertificateCode.Format = String.Empty;
            columnList.Add(columnCertificateCode);

            // Add Certificate Award Date Column
            CertificateImport columnCertificateAwardDate = new CertificateImport();
            columnCertificateAwardDate.Identifier = "CertificateAwardDate";
            columnCertificateAwardDate.ColumnName = _GlobalResources.CertificateAwardDate;
            columnCertificateAwardDate.AlwaysRequired = true;
            columnCertificateAwardDate.DataType = (UserAccountData.DataType)Enum.Parse(typeof(UserAccountData.DataType), "Date");
            columnCertificateAwardDate.Format = dateTimeFormat;
            columnList.Add(columnCertificateAwardDate);

            // Add Row End Column
            CertificateImport columnRowEnd = new CertificateImport();
            columnRowEnd.Identifier = "RowEnd";
            columnRowEnd.ColumnName = _GlobalResources.ROWEND_UPPER;
            columnRowEnd.AlwaysRequired = true;
            columnRowEnd.Format = _GlobalResources.RowEndInfo;
            columnList.Add(columnRowEnd);

            return columnList;
        }
        #endregion

        #endregion

        #region Static Methods

        #region CodeExistForCurrentSite
        /// <summary>
        /// Gets a list of codes that is not exist for current site
        /// </summary>
        public static string[] CodeListExistForCurrentSite()
        {
            string[] codelist;
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[CertificateImport.CheckCodes]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                codelist = dt.AsEnumerable().Select(r => r.Field<string>("code")).ToArray();
                return codelist;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
