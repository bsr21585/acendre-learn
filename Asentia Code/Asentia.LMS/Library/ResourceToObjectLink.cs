﻿using Asentia.Common;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.LMS.Library
{
    public class ResourceToObjectLink
    {
        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        public ResourceToObjectLink()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }
        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idCourse">Course Id</param>
        public ResourceToObjectLink(int resourceToObjectId)
        {
            this.LanguageSpecificProperties = new ArrayList();

            this._Details(resourceToObjectId);
            this._GetPropertiesInLanguages(this.IdResource);
        }

        #endregion

        #region public properties
        /// <summary>
        /// Id Resource
        /// </summary>
        public int IdResource;
        /// <summary>
        /// Resource Name
        /// </summary>
        public string Name;
        /// <summary>
        /// Id Resource To Object Link
        /// </summary>
        public int IdResourceToObject;
        /// <summary>
        /// Object Id
        /// </summary>
        public int IdObject;
        /// <summary>
        /// Object Type
        /// </summary>
        public string ObjectType;
        /// <summary>
        /// Start Time
        /// </summary>
        public DateTime? DtStart;
        /// <summary>
        /// End time
        /// </summary>
        public DateTime? DtEnd;
        /// <summary>
        /// IsOutsideUse
        /// </summary>
        public bool? IsOutsideUse { get; set; }
        /// <summary>
        /// IsMaintenance
        /// </summary>
        public bool? IsMaintenance { get; set; }
        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Name;
            public string Description;

            public LanguageSpecificProperty(string langString, string name, string description)
            {
                this.LangString = langString;
                this.Name = name;
                this.Description = description;
            }
        }
        #endregion

        #region Methods
        #region Static Methods
        /// <summary>
        /// Deletes Resource(s).
        /// </summary>
        /// <param name="deletees">DataTable of resources to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@ResourceToObjectLinkIds", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[ResourceToObjectLink.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #region GetItemsForCalendar
        /// <summary>
        /// Returns the list of calendar events for a particular resource.
        /// </summary>
        public static DataTable GetItemsForCalendar(int idResource)
        {
            DataTable calendarItems = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idResource", idResource, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[ResourceToObjectLink.GetItemsForCalendar]", true);
                calendarItems.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return calendarItems;
        }
        #endregion

        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Saves resource data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved resource</returns>
        private void _Details(int resourceToObjectId)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idResourceToObject", resourceToObjectId, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idResource", this.IdResource, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@idObject", this.IdObject, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@objectType", this.ObjectType, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtEnd", this.DtEnd, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isOutsideUse", this.IsOutsideUse, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isMaintenance", this.IsMaintenance, SqlDbType.Bit, 1, ParameterDirection.Output);
            try
            {
                databaseObject.ExecuteNonQuery("[ResourceToObjectLink.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved resource
                this.IdResourceToObject = Convert.ToInt32(databaseObject.Command.Parameters["@idResourceToObject"].Value);
                this.IdResource = Convert.ToInt32(databaseObject.Command.Parameters["@idResource"].Value);
                this.Name = Convert.ToString(databaseObject.Command.Parameters["@name"].Value);

                this.IdObject = Convert.ToInt32(databaseObject.Command.Parameters["@idObject"].Value);
                this.ObjectType = Convert.ToString(databaseObject.Command.Parameters["@ObjectType"].Value);
                this.DtStart =Convert.ToDateTime(databaseObject.Command.Parameters["@dtStart"].Value);
                this.DtEnd = Convert.ToDateTime(databaseObject.Command.Parameters["@dtEnd"].Value);
                this.IsOutsideUse = Convert.ToBoolean(databaseObject.Command.Parameters["@isOutsideUse"].Value);
                this.IsMaintenance= Convert.ToBoolean(databaseObject.Command.Parameters["@isMaintenance"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a group into a collection.
        /// </summary>
        /// <param name="idGroup">group id</param>
        private void _GetPropertiesInLanguages(int idResource)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idResource", idResource, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Resource.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["name"].ToString(),
                            sdr["description"].ToString()
                            )
                    );
                }

                sdr.Close();

                //sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Save
        /// <summary>
        /// Save with ResourceToObject id
        /// </summary>
        /// <returns></returns>
        private int _Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idResourceToObject", this.IdResourceToObject, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idResource", this.IdResource, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idObject", this.IdObject, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@objectType", this.ObjectType, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@dtEnd", this.DtEnd, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@isOutsideUse", this.IsOutsideUse, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@isMaintenance", this.IsMaintenance, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[ResourceToObjectLink.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the idResourceToObject
                this.IdResourceToObject = Convert.ToInt32(databaseObject.Command.Parameters["@idResourceToObject"].Value);

                // return
                return this.IdResourceToObject;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion

        #region Public Methods
        #region Save
        /// <summary>
        /// Saves resource data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save();
        }

        #endregion

        #endregion
        
        #endregion
    }
    public enum ObjectType
    {
        standuptraining = 1
    }
}
