﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;
using System.ComponentModel;
using System.Collections;

namespace Asentia.LMS.Library
{
    public class RuleSet
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public RuleSet()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idRuleSet">Rule Set Id</param>
        public RuleSet(int idRuleSet)
        {
            this.LanguageSpecificProperties = new ArrayList();

            _Details(idRuleSet);
            this._GetPropertiesInLanguages(idRuleSet);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[RuleSet.GetGrid]";

        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedureForGroup = "[RuleSet.GetGridForGroup]";

        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedureForRoles = "[RuleSet.GetGridForRole]";

        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedureForRuleSetEnrollment = "[RuleSet.GetGridForRuleSetEnrollment]";

        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedureForRuleSetLearningPathEnrollment = "[RuleSet.GetGridForRuleSetLearningPathEnrollment]";

        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedureForCertification = "[RuleSet.GetGridForCertification]";

        /// <summary>
        /// Rule Set Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite { get; set; }

        /// <summary>
        /// Is Any.
        /// </summary>
        public bool IsAny { get; set; }

        /// <summary>
        /// Rule Set Label.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// LinkedObjectId.
        /// If it is greater than zero then the rule set will be linked to rule set as per IdType-specific 
        /// </summary>
        public int LinkedObjectId { get; set; }

        /// <summary>
        /// LinkedObjectType.
        /// If it is not none then link this rule set with the appropriate type 
        /// </summary>
        public RuleSetLinkedObjectType LinkedObjectType { get; set; }

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Label;

            public LanguageSpecificProperty(string langString, string label)
            {
                this.LangString = langString;
                this.Label = label;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves object data to the database.
        /// </summary>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSet", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@id", this.LinkedObjectId, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idType", Convert.ToInt32(this.LinkedObjectType), SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isAny", this.IsAny, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@label", this.Label, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSet.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idRuleSet"].Value);
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this rule set.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="name">group name</param>
        public void SaveLang(string languageString, string label)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureRulesetSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSet", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@label", label, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSet.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes rule sets.
        /// </summary>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@RuleSets", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSet.Delete]", true);

                // get the return code
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetAssociatedRules
        /// <summary>
        /// Retrieves rules associated with a ruleset.
        /// </summary>
        /// <param name="idRuleSet">RuleSet Id</param>
        public DataTable GetAssociatedRules(int idRuleSet)
        {
            DataTable ruleSetDataTable = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSet", idRuleSet, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader dataReader = databaseObject.ExecuteDataReader("[RuleSet.GetAssociatedRules]", true);
                ruleSetDataTable.Load(dataReader);
                dataReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return ruleSetDataTable;
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <param name="idRuleSet">Rule Set Id</param>
        private void _Details(int idRuleSet)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSet", idRuleSet, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@isAny", this.IsAny, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@label", this.Label, SqlDbType.NVarChar, 255, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSet.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idRuleSet;
                this.IdSite = Convert.ToInt32(databaseObject.Command.Parameters["@idSite"].Value);
                this.IsAny = Convert.ToBoolean(databaseObject.Command.Parameters["@isAny"].Value);
                this.Label = databaseObject.Command.Parameters["@label"].Value.ToString();
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a group into a collection.
        /// </summary>
        /// <param name="idGroup">group id</param>
        private void _GetPropertiesInLanguages(int idRuleSet)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSet", idRuleSet, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[RuleSet.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["label"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }

    /// <summary>
    /// Link Object Id Type.
    /// </summary>
    /// <remarks>
    /// Used to distinguish the type of Linked Object whether it is GroupId, or RoleId etc.
    /// </remarks>
    public enum RuleSetLinkedObjectType
    {
        /// <summary>
        /// None.
        /// </summary>
        /// <remarks>
        /// Linked Object Id links to None
        /// </remarks>
        [Description("None")]
        None = 0,

        /// <summary>
        /// Group.
        /// </summary>
        /// <remarks>
        /// Linked Object Id links to Group
        /// </remarks>
        [Description("Group")]
        Group = 1,

        /// <summary>
        /// Role.
        /// </summary>
        /// <remarks>
        /// Linked Object Id links to Role
        /// </remarks>
        [Description("Role")]
        Role = 2,

        /// <summary>
        /// RuleSetEnrollment.
        /// </summary>
        /// <remarks>
        /// Linked Object Id links to RuleSet Enrollment
        /// </remarks>
        [Description("RuleSet Enrollment")]
        RuleSetEnrollment = 3,

        /// <summary>
        /// RuleSetEnrollment.
        /// </summary>
        /// <remarks>
        /// Linked Object Id links to RuleSet Enrollment
        /// </remarks>
        [Description("RuleSet LearningPath Enrollment")]
        RuleSetLearningPathEnrollment = 4,

        /// <summary>
        /// Certification.
        /// </summary>
        /// <remarks>
        /// Linked Object Id links to Certification
        /// </remarks>
        [Description("Certification")]
        Certification = 5
    }
}