﻿using System;
using System.Data;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    [Serializable]
    public class LearningPathEnrollment
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public LearningPathEnrollment()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idLearningPathEnrollment">Learning Path Enrollment Id</param>
        public LearningPathEnrollment(int idLearningPathEnrollment)
        {
            // synchronize course data first
            this._SynchronizeCourseData(idLearningPathEnrollment);

            // load details
            this._Details(idLearningPathEnrollment);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[LearningPathEnrollment.GetGrid]";

        /// <summary>
        /// Stored procedure used to populate a grid for this object for the current session user's dashboard.
        /// </summary>
        public static readonly string GridProcedureForUserDashboard = "[LearningPathEnrollment.GetGridForUserDashboard]";

        /// <summary>
        /// Stored procedure used to populate a grid of a user's enrollment statuses in courses belonging to a learning path.
        /// </summary>
        public static readonly string GridProcedureForLearnersStatus = "[LearningPathEnrollment.GetGridForLearnersStatus]";

        /// <summary>
        /// Learning Path Enrollment Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Learning Path Id.
        /// </summary>
        public int IdLearningPath;

        /// <summary>
        /// User Id.
        /// </summary>
        public int IdUser;

        /// <summary>
        /// RuleSet Learning Path Enrollment Id.
        /// </summary>
        public int? IdRuleSetLearningPathEnrollment;

        /// <summary>
        /// Timezone Id.
        /// </summary>
        public int IdTimezone;

        /// <summary>
        /// Title.
        /// </summary>
        public string Title;

        /// <summary>
        /// Start Date.
        /// </summary>
        public DateTime DtStart;

        /// <summary>
        /// Due Date.
        /// </summary>
        public DateTime? DtDue;

        /// <summary>
        /// Date Expires from Start.
        /// </summary>
        public DateTime? DtExpiresFromStart;

        /// <summary>
        /// Date Expires from First Launch.
        /// </summary>
        public DateTime? DtExpiresFromFirstLaunch;

        /// <summary>
        /// The effective date of enrollment expiration. 
        /// Calculated and populated in _Details for an Enrollment by determining the first date to occur.
        /// </summary>
        public DateTime? DtEffectiveExpires;

        /// <summary>
        /// Date First Launch.
        /// </summary>
        public DateTime? DtFirstLaunch;

        /// <summary>
        /// Completed Date.
        /// </summary>
        public DateTime? DtCompleted;

        /// <summary>
        /// Created Date.
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Due Interval.
        /// </summary>
        public int? DueInterval;

        /// <summary>
        /// Due Timeframe.
        /// </summary>
        public string DueTimeframe;

        /// <summary>
        /// Expires from Start Interval.
        /// </summary>
        public int? ExpiresFromStartInterval;

        /// <summary>
        /// Expires from Start Timeframe.
        /// </summary>
        public string ExpiresFromStartTimeframe;

        /// <summary>
        /// Expires from First Launch Interval.
        /// </summary>
        public int? ExpiresFromFirstLaunchInterval;

        /// <summary>
        /// Expires from First Launch Timeframe.
        /// </summary>
        public string ExpiresFromFirstLaunchTimeframe;
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves learning path enrollment data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser, false);
        }

        /// <summary>
        /// Saves learning path enrollment data to the database as the current session user
        /// and allows a flag to be set for cascading course enrollments.
        /// </summary>
        public int Save(bool cascadeCourseEnrollments)
        {
            return this._Save(AsentiaSessionState.IdSiteUser, cascadeCourseEnrollments);
        }

        /// <summary>
        /// Overloaded method that saves learning path enrollment data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller, false);
        }

        /// <summary>
        /// Overloaded method that saves learning path enrollment data to the database with a caller specified
        /// and allows a flag to be set for cascading course enrollments.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller, bool cascadeCourseEnrollments)
        {
            return this._Save(idCaller, cascadeCourseEnrollments);
        }
        #endregion

        #region SaveCompletionStatus
        /// <summary>
        /// Saves enrollment completion status to the database as the current session user.
        /// </summary>
        public int SaveCompletionStatus()
        {
            return this._SaveCompletionStatus(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves enrollment completion status to the database with a caller specified.
        /// This would be used when SaveCompletionStatus needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int SaveCompletionStatus(int idCaller)
        {
            return this._SaveCompletionStatus(idCaller);
        }
        #endregion

        #region SaveForMultipleLearningPaths
        /// <summary>
        /// Saves learning path enrollment data for multiple learning paths to the database as the current session user.
        /// This is used/called for creation of new learning path enrollments where we create enrollments for multiple learning paths.
        /// </summary>
        public void SaveForMultipleLearningPaths(DataTable courses)
        {
            this._SaveForMultipleLearningPaths(AsentiaSessionState.IdSiteUser, courses);
        }

        /// <summary>
        /// Overloaded method that saves learning path enrollment data for multiple learning paths to the database with a caller specified.
        /// This is used/called for creation of new learning path enrollments where we create enrollments for multiple learning paths.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public void SaveForMultipleLearningPaths(int idCaller, DataTable courses)
        {
            this._SaveForMultipleLearningPaths(idCaller, courses);
        }
        #endregion

        #region EnrollLearningPath
        /// <summary>
        /// Handles learning path enrollment for self-enrolled free and purchased learning paths.
        /// 
        /// - This will create a transaction item if one is to be created, i.e. the learning path is free.
        /// - For purchased learning paths, this will create the enrollment and link it to the transaction
        ///   item, but it will not confirm transaction items or disassociate transaction items from purchase
        ///   if a coupon code makes it free. That will be done by the purchase confirmation processes.
        /// </summary>
        /// <param name="idLearningPath">learning path id</param>
        /// <param name="idUser">the id of the user to enroll</param>
        /// <param name="idTransactionItem">existing transaction item id to link this to, 0 if we are to create a new transaction item</param>
        public void EnrollLearningPath(int idLearningPath, int idUser, int idTransactionItem)
        {
            if (AsentiaSessionState.IdSite == 0)
            { throw new AsentiaException(_GlobalResources.YouMustBeLoggedInToEnrollInThisLearningPath); }

            //if (AsentiaSessionState.IdSiteUser == 1)
            //{ throw new AsentiaException(_GlobalResources.TheSystemAdministratorCannotEnrollInLearningPaths); }

            try
            {
                // get user and course objects
                Asentia.UMS.Library.User userObject = new Asentia.UMS.Library.User(idUser);
                Asentia.LMS.Library.LearningPath learningPathObject = new Asentia.LMS.Library.LearningPath(idLearningPath);

                // check the published and closed status of the learning path
                // note, if this is originating from a catalog enrollment we dont care about published and closed for the course
                if (learningPathObject.IsPublished == false || learningPathObject.IsClosed == true)
                { throw new AsentiaException(_GlobalResources.YouCannotEnrollInThisLearningPath); }                

                this.Id = 0; // enrollment id is 0 since we are creating new, this is redundant but it guarantees new
                this.IdLearningPath = learningPathObject.Id;
                this.IdUser = userObject.Id;
                this.IdTimezone = userObject.IdTimezone;
                this.DtStart = AsentiaSessionState.UtcNow;

                // save the enrollment
                this.Save(true); // cascade the course enrollments

                // if the enrollment was saved,
                // either log a transaction item for it if idTransactionItem = 0
                // or update idEnrollment for an existing transaction item if idTransactionItem > 0
                if (this.Id > 0)
                {
                    if (idTransactionItem > 0)
                    {
                        TransactionItem transactionItemObject = new TransactionItem(idTransactionItem);
                        transactionItemObject.IdEnrollment = this.Id;
                        transactionItemObject.Save();
                    }
                    else
                    {
                        TransactionItem transactionItemObject = new TransactionItem();
                        transactionItemObject.Id = 0;
                        transactionItemObject.IdParentTransactionItem = null;
                        transactionItemObject.IdPurchase = null;
                        transactionItemObject.IdEnrollment = this.Id;
                        transactionItemObject.IdUser = userObject.Id;
                        transactionItemObject.UserName = userObject.DisplayName;
                        transactionItemObject.IdAssigner = userObject.Id;
                        transactionItemObject.AssignerName = userObject.DisplayName;
                        transactionItemObject.ItemId = learningPathObject.Id;
                        transactionItemObject.ItemName = learningPathObject.Name;
                        transactionItemObject.ItemType = PurchaseItemType.LearningPath;
                        transactionItemObject.Description = learningPathObject.ShortDescription;
                        transactionItemObject.Cost = 0;
                        transactionItemObject.Paid = 0;
                        transactionItemObject.IdCouponCode = null;
                        transactionItemObject.CouponCode = null;
                        transactionItemObject.Confirmed = true;

                        // save the transaction item object
                        transactionItemObject.Save();
                    }
                }                
            }
            catch
            {
                throw;
            }            
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes a Learning Path Enrollment(s).
        /// </summary>
        /// <param name="deletees">DataTable of learning path enrollments to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@LearningPathEnrollments", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[LearningPathEnrollment.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves learning path enrollment data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <param name="cascadeCourseEnrollments">if true, the procedure will cascade course enrollments for the learning path</param>
        /// <returns>the id of the learning path enrollment</returns>
        private int _Save(int idCaller, bool cascadeCourseEnrollments)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPathEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idLearningPath", this.IdLearningPath, SqlDbType.Int, 4, ParameterDirection.Input);

            if (this.IdRuleSetLearningPathEnrollment == null)
            { databaseObject.AddParameter("@idRuleSetLearningPathEnrollment", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idRuleSetLearningPathEnrollment", this.IdRuleSetLearningPathEnrollment, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);

            if (this.DueInterval == null)
            { databaseObject.AddParameter("@dueInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dueInterval", this.DueInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@dueTimeframe", this.DueTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromStartInterval == null)
            { databaseObject.AddParameter("@expiresFromStartInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromStartInterval", this.ExpiresFromStartInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromStartTimeframe", this.ExpiresFromStartTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromFirstLaunchInterval == null)
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", this.ExpiresFromFirstLaunchInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", this.ExpiresFromFirstLaunchTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@cascadeCourseEnrollments", cascadeCourseEnrollments, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[LearningPathEnrollment.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved learning path enrollment
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idLearningPathEnrollment"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SaveForMultipleLearningPaths
        /// <summary>
        /// Saves learning path enrollment data for multiple learning paths to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <param name="learningPaths">DataTable of learning path ids to create enrollments for</param>
        private void _SaveForMultipleLearningPaths(int idCaller, DataTable learningPaths)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@LearningPaths", learningPaths, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);

            if (this.DueInterval == null)
            { databaseObject.AddParameter("@dueInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dueInterval", this.DueInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@dueTimeframe", this.DueTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromStartInterval == null)
            { databaseObject.AddParameter("@expiresFromStartInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromStartInterval", this.ExpiresFromStartInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromStartTimeframe", this.ExpiresFromStartTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromFirstLaunchInterval == null)
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", this.ExpiresFromFirstLaunchInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", this.ExpiresFromFirstLaunchTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[LearningPathEnrollment.SaveForMultipleLearningPaths]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SaveCompletionStatus
        /// <summary>
        /// Saves enrollment completion status to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the enrollment</returns>
        private int _SaveCompletionStatus(int idCaller)
        {
            // make sure an enrollment id exists, if not, throw exception!

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPathEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);

            if (this.DtCompleted == null)
            { databaseObject.AddParameter("@dtCompleted", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dtCompleted", this.DtCompleted, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[LearningPathEnrollment.SaveCompletionStatus]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved enrollment
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idLearningPathEnrollment"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SynchronizeCourseData
        /// <summary>
        /// Synchronizes course data for a Learning Path Enrollment.
        /// </summary>
        /// <param name="idLearningPathEnrollment">the learning path enrollment's id</param>
        private void _SynchronizeCourseData(int idLearningPathEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPathEnrollment", idLearningPathEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[LearningPathEnrollment.SynchronizeCourseData]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified learning path enrollment's properties.
        /// </summary>
        /// <param name="idLearningPathEnrollment">Learning Path Enrollment Id</param>
        private void _Details(int idLearningPathEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPathEnrollment", idLearningPathEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idLearningPath", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idRuleSetLearningPathEnrollment", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimezone", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@title", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@dtStart", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDue", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtExpiresFromStart", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtExpiresFromFirstLaunch", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtFirstLaunch", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCompleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dueInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dueTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[LearningPathEnrollment.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idLearningPathEnrollment;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdLearningPath = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idLearningPath"].Value);
                this.IdUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.IdRuleSetLearningPathEnrollment = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idRuleSetLearningPathEnrollment"].Value);
                this.IdTimezone = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idTimezone"].Value);
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.DtStart = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtStart"].Value);
                this.DtDue = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDue"].Value);
                this.DtExpiresFromStart = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtExpiresFromStart"].Value);
                this.DtExpiresFromFirstLaunch = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtExpiresFromStart"].Value);
                this.DtFirstLaunch = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtFirstLaunch"].Value);
                this.DtCompleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtCompleted"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DueInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@dueInterval"].Value);
                this.DueTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@dueTimeframe"].Value);
                this.ExpiresFromStartInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromStartInterval"].Value);
                this.ExpiresFromStartTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromStartTimeframe"].Value);
                this.ExpiresFromFirstLaunchInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromFirstLaunchInterval"].Value);
                this.ExpiresFromFirstLaunchTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromFirstLaunchTimeframe"].Value);

                // calculate the effective expiration date
                if (this.DtExpiresFromStart != null || this.DtExpiresFromFirstLaunch != null)
                {
                    if (this.DtExpiresFromStart == null)
                    { this.DtEffectiveExpires = this.DtExpiresFromFirstLaunch; }
                    else if (this.DtExpiresFromFirstLaunch == null)
                    { this.DtEffectiveExpires = this.DtExpiresFromStart; }
                    else
                    {
                        if (this.DtExpiresFromStart > this.DtExpiresFromFirstLaunch)
                        { this.DtEffectiveExpires = this.DtExpiresFromFirstLaunch; }
                        else
                        { this.DtEffectiveExpires = this.DtExpiresFromStart; }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion        
        #endregion
    }
}
