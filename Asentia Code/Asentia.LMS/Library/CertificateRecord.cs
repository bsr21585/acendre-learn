﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Threading;
using Newtonsoft.Json.Linq;
using drawing = System.Drawing;
using Asentia.Common;
using Asentia.UMS.Library;
using System.Data;
using System.Reflection;

namespace Asentia.LMS.Library
{
    class CertificateRecord
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public CertificateRecord()
        {

        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idCertificate">Certificate Id</param>
        public CertificateRecord(int idCertificateRecord)
        {
            this._Details(idCertificateRecord);
        }
        #endregion

        #region Properties

        /// <summary>
        /// CertificateRecord Id.
        /// </summary>
        public int IdCertificateRecord = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Certificate Id.
        /// </summary>
        public int IdCertificate = 0;

        /// <summary>
        /// User Id
        /// </summary>
        public int IdUser;

        /// <summary>
        /// Certificate awarded by.
        /// </summary>
        public int IdAwardedBy;

        /// <summary>
        /// timestamp
        /// </summary>
        public DateTime? Timestamp;

        /// <summary>
        /// Expiry date of the certificate
        /// </summary>
        public DateTime? Expires;

        /// <summary>
        /// Time zone id
        /// </summary>
        public int? IdTimeZone;

        /// <summary>
        /// Code
        /// </summary>
        public string Code;

        /// <summary>
        /// credits
        /// </summary>
        public string Credits;

        /// <summary>
        /// Award data
        /// </summary>
        public string AwardData;

        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves object data to the database.
        /// </summary>
        public int Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificateRecord", this.IdCertificateRecord, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idCertificate", this.IdCertificate, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@timestamp", this.Timestamp, SqlDbType.DateTime, null, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimeZone, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CertificateRecord.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdCertificateRecord = Convert.ToInt32(databaseObject.Command.Parameters["@idCertificateRecord"].Value);

                return this.IdCertificateRecord;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region BuildCertificateLayoutJSON
        /// <summary>
        /// Build Certificate Layout JSON
        /// </summary>
        public string BuildCertificateLayoutJSON()
        {
            string certificateLayoutJSON = string.Empty;            

            //Build file path
            string filePath = SitePathConstants.SITE_CERTIFICATES_ROOT + this.IdCertificate + "/Certificate_" + AsentiaSessionState.UserCulture + ".json";
            string defaultFilePath = SitePathConstants.SITE_CERTIFICATES_ROOT + this.IdCertificate + "/Certificate.json";

            //Load json
            if (File.Exists(HttpContext.Current.Server.MapPath(filePath)))
            {
                certificateLayoutJSON = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(filePath));
            }
            else if (File.Exists(HttpContext.Current.Server.MapPath(defaultFilePath)))
            {
                certificateLayoutJSON = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(defaultFilePath));
            }

            return certificateLayoutJSON;
        }
        #endregion

        #region BuildCertificateAvailableFieldsJSON
        /// <summary>
        /// Build Certificate Available Fields
        /// </summary>
        public string BuildCertificateAvailableFieldsJSON(bool forWebservice)
        {
            UserAccountData userAccountData = new UserAccountData(UserAccountDataFileType.Site, true, false);
            List<UserAccountData.UserFieldForLabel> listUserFields = userAccountData.GetRuleSetEligibleFieldLabelsInLanguage(AsentiaSessionState.UserCulture);
            List<string> lstCertificateProperties = new List<string>();

            lstCertificateProperties.Add("Certificate Name");
            lstCertificateProperties.Add("Issuing Organization");
            lstCertificateProperties.Add("Award Date");
            lstCertificateProperties.Add("Expiration Date");
            lstCertificateProperties.Add("Certificate Code");
            lstCertificateProperties.Add("Certificate Credits");

            string certificateName = String.Empty;
            string issuingOrganization = String.Empty;

            string availableFieldsJSON = "{ \"AvailableFields\" : [ ";
            HtmlGenericControl certificateWrapper = new HtmlGenericControl("div");
            certificateWrapper.ID = "CertificateWrapper";

            string bgImg = string.Empty;
            int newHeight = 0;
            int newWidth = 0;
            drawing::Image objImage = null;

            if (File.Exists(HttpContext.Current.Server.MapPath(bgImg)))
            {
                objImage = drawing::Image.FromFile(HttpContext.Current.Server.MapPath(bgImg));
                Certificate.GetImageHeightWidth(objImage.Width, objImage.Height, Certificate.CertificateImageDefaultWidth, Certificate.CertificateImageDefaultHeight, out newWidth, out newHeight);
            }

            string viewerHtml = String.Empty;
            string lastNameText = String.Empty;
            string firstNameText = String.Empty;

            availableFieldsJSON += "{\"Identifier\" : \"fullName\", \"Label\" : \"" + _GlobalResources.FullName + "\", \"Value\" : \"" + _GlobalResources.FullName + "\" }, ";

            foreach (UserAccountData.UserFieldForLabel userField in listUserFields)
            {
                if (userField.Identifier.Contains("dt"))
                {
                    availableFieldsJSON += "{\"Identifier\" : \"" + userField.Identifier + "\", \"Label\" : \"" + Utility.EscapeSpecialCharactersForJSON(userField.Label, forWebservice) + "\", \"Value\" : \"" + DateTime.Now.ToString("MMMM dd, yyyy") + "\" }, ";
                }
                else
                {
                    availableFieldsJSON += "{\"Identifier\" : \"" + userField.Identifier + "\", \"Label\" : \"" + Utility.EscapeSpecialCharactersForJSON(userField.Label, forWebservice) + "\", \"Value\" : \"" + Utility.EscapeSpecialCharactersForJSON(userField.Label, forWebservice) + "\" }, ";
                }
            }

            foreach (string certificateProperty in lstCertificateProperties)
            {
                    switch (certificateProperty)
                    {
                        case "Certificate Name":
                            availableFieldsJSON += "{\"Identifier\" : \"certificateName\", \"Label\" : \"" + _GlobalResources.CertificateName + "\", \"Value\" : \"" + _GlobalResources.CertificateName + "\" }, ";
                            break;
                        case "Issuing Organization":
                            availableFieldsJSON += "{\"Identifier\" : \"issuingOrganization\", \"Label\" : \"" + _GlobalResources.IssuingOrganization + "\", \"Value\" : \"" + _GlobalResources.IssuingOrganization + "\" }, ";
                            break;
                        case "Award Date":
                            availableFieldsJSON += "{\"Identifier\" : \"dtAward\", \"Label\" : \"" + _GlobalResources.AwardDate + "\", \"Value\" : \"" + DateTime.Now.ToString("MMMM dd, yyyy") + "\" }, "; 
                            break;
                        case "Expiration Date":
                            availableFieldsJSON += "{\"Identifier\" : \"dtCertificateExpires\", \"Label\" : \"" + _GlobalResources.ExpirationDate + "\", \"Value\" : \"" + DateTime.Now.ToString("MMMM dd, yyyy") + "\" }, ";
                            break;
                        case "Certificate Code":
                            availableFieldsJSON += "{\"Identifier\" : \"certificateCode\", \"Label\" : \"" + _GlobalResources.CertificateCode + "\", \"Value\" : \"-RANDOM-UNIQUE-CERT-CODE-\" }, ";
                            break;
                        case "Certificate Credits":
                            availableFieldsJSON += "{\"Identifier\" : \"certificateCredits\", \"Label\" : \"" + _GlobalResources.CertificateCredits + "\", \"Value\" : \"00\" }, ";
                            break;
                    }
            }

            availableFieldsJSON = availableFieldsJSON.TrimEnd();
            availableFieldsJSON = availableFieldsJSON.Remove(availableFieldsJSON.Length - 1) + " ] }";
            return availableFieldsJSON;
        }
        #endregion

        #region BuildCertificateAwardDataJSON
        /// <summary>
        /// Build Certificate Award Data from certificate ID
        /// </summary>
        public string BuildCertificateAwardDataJSON(bool forWebservice)
        {
            string certificateAwardDataJSON = "{\"AwardData\": [ ";
            Certificate certificateObject = new Certificate(this.IdCertificate);
            User userObject = new User(this.IdUser);
            UserAccountData userAccountData = new UserAccountData(UserAccountDataFileType.Site, true, false);
            List<UserAccountData.UserFieldForLabel> listUserFields = userAccountData.GetRuleSetEligibleFieldLabelsInLanguage(AsentiaSessionState.UserCulture);

            string lastNameText = string.Empty;
            string firstNameText = string.Empty;
            foreach (UserAccountData.UserFieldForLabel userField in listUserFields)
            {
                FieldInfo userFieldInfo = userObject.GetType().GetField(userField.UserObjectPropertyName);

                string userFieldValue = Convert.ToString(userObject.GetType().GetField(userField.UserObjectPropertyName).GetValue(userObject));

                if (userField.Identifier == "lastName") { lastNameText = userFieldValue; }
                if (userField.Identifier == "firstName") { firstNameText = userFieldValue; }

                if (userFieldInfo != null)
                {
                    if ((userField.Identifier == "dtExpires") || (userField.Identifier == "dtCreated") || (userField.Identifier == "dtAward") || (userField.Identifier == "dtDOB") || (userField.Identifier == "dtHire") || (userField.Identifier == "dtTerm"))
                    {
                        userFieldValue = !String.IsNullOrWhiteSpace(userFieldValue) ? String.Format("{0:MMMM dd, yyyy}", TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(userFieldValue), TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName))) : String.Empty;
                    }
                    string elementAwardData = "{\"Identifier\":\"" + userField.Identifier + "\", \"Value\":\"" + Utility.EscapeSpecialCharactersForJSON(userFieldValue, forWebservice) + "\"},";
                    certificateAwardDataJSON += elementAwardData;
                }
            }

            string fullNameData = "{\"Identifier\":\"fullName\", \"Value\":\"" + Utility.EscapeSpecialCharactersForJSON(firstNameText, forWebservice) + " " + Utility.EscapeSpecialCharactersForJSON(lastNameText, forWebservice) + "\"},";
            certificateAwardDataJSON += fullNameData;

            string certificateName = certificateObject.Name;

            foreach (Certificate.LanguageSpecificProperty certificateLanguageSpecificProperty in certificateObject.LanguageSpecificProperties)
            {
                if (certificateLanguageSpecificProperty.LangString == AsentiaSessionState.UserCulture)
                { certificateName = certificateLanguageSpecificProperty.Name; }
            }

            string timeStamp = (this.Timestamp == null) ? string.Empty : String.Format("{0:MMMM dd, yyyy}", TimeZoneInfo.ConvertTimeFromUtc((DateTime)this.Timestamp, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)));
            string expires = (this.Expires == null) ?  string.Empty : String.Format("{0:MMMM dd, yyyy}", TimeZoneInfo.ConvertTimeFromUtc((DateTime)this.Expires, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName)));

            certificateAwardDataJSON += "{\"Identifier\":\"certificateName\", \"Value\":\"" + Utility.EscapeSpecialCharactersForJSON(certificateName, forWebservice) + "\"},";
            certificateAwardDataJSON += "{\"Identifier\":\"issuingOrganization\", \"Value\":\"" + Utility.EscapeSpecialCharactersForJSON(certificateObject.IssuingOrganization, forWebservice) + "\"},";
            certificateAwardDataJSON += "{\"Identifier\":\"dtAward\", \"Value\":\"" + timeStamp + "\"},";
            certificateAwardDataJSON += "{\"Identifier\":\"dtCertificateExpires\", \"Value\":\"" + expires + "\"},";
            certificateAwardDataJSON += "{\"Identifier\":\"certificateCode\", \"Value\":\"" + Utility.EscapeSpecialCharactersForJSON(this.Code, forWebservice) + "\"},";
            certificateAwardDataJSON += "{\"Identifier\":\"certificateCredits\", \"Value\":\"" + Convert.ToString(this.Credits) + "\"}]}";

            return certificateAwardDataJSON;
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes Certificate records of user.
        /// </summary>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@CertificateRecords", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[CertificateRecord.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified certificate's properties.
        /// </summary>
        /// <param name="idCertificate">Certificate Id</param>
        private void _Details(int idCertificateRecord)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificateRecord", idCertificateRecord, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idCertificate", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idAwardedBy", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@timestamp", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@expires", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimeZone", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@code", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@credits", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@awardData", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[CertificateRecord.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdCertificateRecord = idCertificateRecord;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdCertificate = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idCertificate"].Value);
                this.IdUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.IdAwardedBy = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idAwardedBy"].Value);
                this.Timestamp = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@timestamp"].Value);
                this.Expires = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@expires"].Value);
                this.IdTimeZone = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idTimeZone"].Value);
                this.Code = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@code"].Value);
                this.Credits = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@credits"].Value);
                this.AwardData = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@awardData"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
