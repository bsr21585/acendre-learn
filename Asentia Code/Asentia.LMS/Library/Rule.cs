﻿using System;
using System.Data;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class Rule
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Rule()
        { ;}

        #endregion

        #region Properties
        
        /// <summary>
        /// Rule Set Id.
        /// </summary>
        public int IdRuleSet { get; set; }

        /// <summary>
        /// Rule Id.
        /// </summary>
        public int IdRule { get; set; }

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite { get; set; }

        /// <summary>
        /// User Field.
        /// </summary>
        public string UserField { get; set; }

        /// <summary>
        /// Operator.
        /// </summary>
        public string Operator { get; set; }
         
        /// <summary>
        /// Text Value.
        /// </summary>
        public string TextValue { get; set; }

        /// <summary>
        /// Date Value.
        /// </summary>
        public DateTime DateValue { get; set; }

        /// <summary>
        /// Numeric Value.
        /// </summary>
        public int NumValue { get; set; }

        /// <summary>
        /// Bit Value.
        /// </summary>
        public bool BitValue { get; set; }

        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves object data to the database.
        /// </summary>
        /// <param name="dtRulesTable">rules data table</param>
        public void Save(DataTable dtRulesTable)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSet", this.IdRuleSet, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@rulesTable", dtRulesTable, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Rule.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
