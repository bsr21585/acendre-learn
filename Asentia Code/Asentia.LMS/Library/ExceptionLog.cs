﻿
namespace Asentia.LMS.Library
{
    public class ExceptionLog
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ExceptionLog()
        { ;}

        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[ExceptionLog.GetGrid]";

        #endregion
    }
}
