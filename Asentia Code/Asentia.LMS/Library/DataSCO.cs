﻿using System;
using System.Data;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    class DataSCO
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DataSCO()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idDataSCO">SCO Data Id</param>
        public DataSCO(int idDataSCO)
        {
            this._Details(idDataSCO);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Data SCO Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Data Lesson Id.
        /// </summary>
        /// <seealso cref="Enrollment" />
        public int IdDataLesson;

        /// <summary>
        /// Timezone Id.
        /// </summary>
        public int IdTimezone;

        /// <summary>
        /// Manifest Identifier.
        /// </summary>
        /// <seealso cref="Lesson" />
        public string ManifestIdentifier;

        /// <summary>
        /// LaunchType.
        /// </summary>
        public string LaunchType;

        /// <summary>
        /// Total Time.
        /// </summary>
        public float TotalTime;

        /// <summary>
        /// Completion Status.
        /// </summary>
        public int CompletionStatus;

        /// <summary>
        /// Success Status.
        /// </summary>
        public int SuccessStatus;

        /// <summary>
        /// Score Scaled
        /// </summary>
        public float? ScoreScaled;

        /// <summary>
        /// time stamp.
        /// </summary>
        public DateTime Timestamp;

        /// <summary>
        /// Actual Attempt Count.
        /// </summary>
        public int ActualAttemptCount;

        /// <summary>
        /// Effective Attempt Count.
        /// </summary>
        public int EffectiveAttemptCount;
        #endregion

        #region Methods
        public void Save()
        {
            this._Save();
        }
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves lesson data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the lesson data</returns>
        private int _Save()
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataSCO", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataLesson", this.IdDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@dotNetName", AsentiaSessionState.UserTimezoneDotNetName, SqlDbType.NVarChar, 510, ParameterDirection.Input);
            databaseObject.AddParameter("@manifestIdentifier", this.ManifestIdentifier, SqlDbType.NVarChar, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@launchType", this.LaunchType, SqlDbType.NVarChar, 510, ParameterDirection.Input);
            databaseObject.AddParameter("@CompletionStatus", this.CompletionStatus, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@SuccessStatus", this.SuccessStatus, SqlDbType.Int, 4, ParameterDirection.Input);

            if (this.ScoreScaled == null)
            {
                databaseObject.AddParameter("@ScoreScaled", DBNull.Value, SqlDbType.Float, 8, ParameterDirection.Input);
            }
            else
            {
                databaseObject.AddParameter("@ScoreScaled", this.ScoreScaled, SqlDbType.Float, 8, ParameterDirection.Input);
            }

            databaseObject.AddParameter("@TotalTime", this.TotalTime, SqlDbType.Float, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@ActualAttemptCount", this.ActualAttemptCount, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@EffectiveAttemptCount", this.EffectiveAttemptCount, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[DataSCO.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified lesson data's properties.
        /// </summary>
        /// <param name="idDataLesson">Lesson Data Id</param>
        private void _Details(int idDataSCO)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataSCO", idDataSCO, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idDataLesson", this.IdDataLesson, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@manifestIdentifier", this.ManifestIdentifier, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@completionStatus", this.CompletionStatus, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@successStatus", this.SuccessStatus, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@scoreScaled", this.ScoreScaled, SqlDbType.Float, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@totalTime", this.TotalTime, SqlDbType.Float, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@timestamp", this.Timestamp, SqlDbType.DateTime, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@actualAttemptCount", this.ActualAttemptCount, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@effectiveAttemptCount", this.EffectiveAttemptCount, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[DataSCO.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idDataSCO;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdDataLesson = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idDataLesson"].Value);
                this.IdTimezone = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idTimezone"].Value);
                this.ManifestIdentifier = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@manifestIdentifier"].Value);
                this.CompletionStatus = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@completionStatus"].Value);
                this.SuccessStatus = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@successStatus"].Value);
                this.ScoreScaled = AsentiaDatabase.ParseDbParamFloat(databaseObject.Command.Parameters["@scoreScaled"].Value);
                this.Timestamp = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@timestamp"].Value);
                this.ActualAttemptCount = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@actualAttemptCount"].Value);
                this.EffectiveAttemptCount = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@effectiveAttemptCount"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveInteractions
        /// <summary>
        /// SaveInteractions 
        /// This Method to Save the Interactions data to database.
        /// </summary>
        /// <param name="idLesson">idLesson</param>
        /// <param name="scoIdentifier">scoIdentifier</param>
        /// <param name="interactionsTable">table of interactions</param>
        /// <returns></returns>
        public void SaveInteractions(int idLesson, string scoIdentifier, DataTable interactionsTable)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase(600);

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataLesson", idLesson, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@scoIdentifier", scoIdentifier, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@ScoInteraction", interactionsTable, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[DataSCOInt.Save]", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion
        #endregion
    }
}
