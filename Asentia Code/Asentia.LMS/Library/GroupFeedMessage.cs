﻿using System;
using Asentia.Common;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.LMS.Library
{
    public class GroupFeedMessage
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public GroupFeedMessage()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idGroupFeedMessage">Group Feed Message Id</param>
        public GroupFeedMessage(int idGroupFeedMessage)
        {
            this._Details(idGroupFeedMessage);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Group Feed Message Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site"/>
        public int IdSite;

        /// <summary>
        /// Group Id.
        /// </summary>
        /// <seealso cref="Group"/>
        public int IdGroup;

        /// <summary>
        /// Language Id.
        /// </summary>
        public int? IdLanguage;

        /// <summary>
        /// Author Id.
        /// </summary>
        public int IdAuthor;

        /// <summary>
        /// Parent Message Id.
        /// If not null, this message is a comment of another message.
        /// </summary>
        public int? IdParentGroupFeedMessage;

        /// <summary>
        /// Message.
        /// </summary>
        public string Message;

        /// <summary>
        /// The timestamp of the message.
        /// </summary>
        public DateTime Timestamp;

        /// <summary>
        /// Date/Time the message was approved by moderator. 
        /// </summary>
        public DateTime? DtApproved;

        /// <summary>
        /// The message's approver.
        /// </summary>
        public int? IdApprover;

        /// <summary>
        /// Has the message been approved?
        /// If this is true and approver information is null, this message never underwent moderation.
        /// </summary>
        public bool? IsApproved;
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves group feed message data to the database as the current session user.
        /// </summary>
        public int Save(bool moderate)
        {
            return this._Save(AsentiaSessionState.IdSiteUser, moderate);
        }

        /// <summary>
        /// Overloaded method that saves group feed message data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller, bool moderate)
        {
            return this._Save(idCaller, moderate);
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes a single Group Feed Message.
        /// Also used when moderators do not approve a message.
        /// </summary>
        /// <param name="idGroupFeedMessage"></param>
        public static void Delete(int idGroupFeedMessage)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idGroupFeedMessage", idGroupFeedMessage, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[GroupFeedMessage.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Approve
        /// <summary>
        /// Approves a Group Feed Message.
        /// </summary>
        /// <param name="idGroupFeedMessage"></param>
        public static void Approve(int idGroupFeedMessage)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idGroupFeedMessage", idGroupFeedMessage, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[GroupFeedMessage.Approve]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetComments
        /// <summary>
        /// Gets a listing of comments for a group feed message.
        /// </summary>
        /// <param name="idGroupFeedMessage">group feed message id</param>
        /// <param name="getApprovedMessagesOnly">get only messages that have been moderator approved?</param>
        /// <returns>DataTable of group feed message comments.</returns>
        public static DataTable GetComments(int idGroupFeedMessage, bool getApprovedMessagesOnly)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idGroupFeedMessage", idGroupFeedMessage, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@getApprovedMessagesOnly", getApprovedMessagesOnly, SqlDbType.Bit, 1, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[GroupFeedMessage.GetComments]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves group feed message data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <param name="moderate">is this message moderated, if true, "isApproved" is marked as null</param>
        /// <returns>the id of the saved group feed message</returns>
        private int _Save(int idCaller, bool moderate)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idGroupFeedMessage", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idGroup", this.IdGroup, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idAuthor", this.IdAuthor, SqlDbType.Int, 4, ParameterDirection.Input);

            if (this.IdParentGroupFeedMessage == null)
            { databaseObject.AddParameter("@idParentGroupFeedMessage", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@IdParentGroupFeedMessage", this.IdParentGroupFeedMessage, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.Message == null)
            { databaseObject.AddParameter("@message", DBNull.Value, SqlDbType.NVarChar, -1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@message", this.Message, SqlDbType.NVarChar, -1, ParameterDirection.Input); }

            if (moderate)
            { databaseObject.AddParameter("@isApproved", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isApproved", true, SqlDbType.Bit, 1, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[GroupFeedMessage.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved group feed message
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idGroupFeedMessage"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified group feed message's properties.
        /// </summary>
        /// <param name="idGroupFeedMessage">Group Feed Message Id</param>
        private void _Details(int idGroupFeedMessage)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idGroupFeedMessage", idGroupFeedMessage, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idGroup", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idLanguage", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idAuthor", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idParentGroupFeedMessage", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@message", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@timestamp", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtApproved", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@idApprover", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@isApproved", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[GroupFeedMessage.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idGroupFeedMessage;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdGroup = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idGroup"].Value);
                this.IdLanguage = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idLanguage"].Value);
                this.IdAuthor = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idAuthor"].Value);
                this.IdParentGroupFeedMessage = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idParentGroupFeedMessage"].Value);
                this.Message = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@message"].Value);
                this.Timestamp = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@timestamp"].Value);
                this.DtApproved = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtApproved"].Value);
                this.IdApprover = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idApprover"].Value);
                this.IsApproved = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isApproved"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
