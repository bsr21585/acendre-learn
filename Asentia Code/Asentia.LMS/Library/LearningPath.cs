﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    [XmlInclude(typeof(LanguageSpecificProperty))]
    [Serializable]
    public class LearningPath
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public LearningPath()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idLearningPath">Learning Path Id</param>
        public LearningPath(int idLearningPath)
        {
            this.LanguageSpecificProperties = new ArrayList();

            this._Details(idLearningPath);
            this._GetPropertiesInLanguages(idLearningPath);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[LearningPath.GetGrid]";

        /// <summary>
        /// Learning Path Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Name.
        /// </summary>
        public string Name;

        /// <summary>
        /// Short Description.
        /// </summary>
        public string ShortDescription;

        /// <summary>
        /// Long Description.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string LongDescription;

        /// <summary>
        /// Cost.
        /// </summary>
        public double? Cost;

        /// <summary>
        /// Is Published.
        /// </summary>
        public bool? IsPublished;

        /// <summary>
        /// Is Closed.
        /// </summary>
        public bool? IsClosed;

        /// <summary>
        /// Self-Enrollment One-Time Only
        /// </summary>
        public bool? SelfEnrollmentIsOneTimeOnly;

        /// <summary>
        /// Force Completion of Courses in Order?
        /// </summary>
        public bool? ForceCompletionInOrder;

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Date Modified.
        /// </summary>
        public DateTime? DtModified;

        /// <summary>
        /// Is Deleted
        /// </summary>
        public bool IsDeleted;

        /// <summary>
        /// Date Deleted.
        /// </summary>
        public DateTime? DtDeleted;

        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar;

        /// <summary>
        /// Search Tags.
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string SearchTags;

        /// <summary>
        /// Is the calling user (current session user) currently enrolled
        /// in this learning path? "Currently enrolled" is defined having a not
        /// yet completed enrollment.
        /// This is used to determine if a user can enroll in this learning path
        /// via the catalog. 
        /// </summary>
        public bool IsCallingUserEnrolled;

        /// <summary>
        /// Is the calling user (current session user) eligible to 
        /// self-enroll in the learning path more than once? This is determined
        /// by checking if the user has has ever self-enrolled in the 
        /// learning path and if the 'Self-Enrollment One Time Only' property is
        /// true for the learning path. This is used to determine if a user can 
        /// enroll in this learning path via the catalog. 
        /// </summary>
        public bool IsMultipleSelfEnrollmentEligible;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// Shortcode for linking directly to the learning path through the catalog
        /// </summary>
        public string Shortcode;
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Name;
            public string ShortDescription;
            public string LongDescription;
            public string SearchTags;

            /// <summary>
            /// parameterless constructor for serialization purpose
            /// </summary>
            public LanguageSpecificProperty()
            { ;}

            public LanguageSpecificProperty(string langString, string name, string shortDescription, string longDescription, string searchTags)
            {
                this.LangString = langString;
                this.Name = name;
                this.ShortDescription = shortDescription;
                this.LongDescription = longDescription;
                this.SearchTags = searchTags;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves learning path data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves learning path data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveAvatar
        /// <summary>
        /// Saves learning path's avatar path to database as the current session user.
        /// </summary>
        public void SaveAvatar()
        {
            this._SaveAvatar(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves learning path's avatar path to database with a caller specified.
        /// This would be used when SaveAvatar needs to be called outside of a user session, i.e.
        /// API. Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public void SaveAvatar(int idCaller)
        {
            this._SaveAvatar(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this learning path.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="name">learning path name</param>
        /// <param name="shortDescription">learning path short description</param>
        /// <param name="longDescription">learning path long description</param>
        public void SaveLang(string languageString, string name, string shortDescription, string longDescription, string searchTags)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureLearningPathSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@name", name, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", shortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@longDescription", longDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", searchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[LearningPath.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveCourses
        /// <summary>
        /// Saves Courses for this Learning Path.
        /// </summary>
        /// <param name="courses">DataTable of courses to save</param>
        public void SaveCourses(DataTable courses)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Courses", courses, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[LearningPath.SaveCourses]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCourses
        /// <summary>
        /// Gets a listing of courses ids and names that belong to this learning path.
        /// </summary>
        /// <returns>DataTable of courses ids and names that belong to this learning path</returns>
        public DataTable GetCourses(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.GetCourses]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCourseSampleScreens
        /// <summary>
        /// Gets a listing of sample screens for courses belonging to this learning path.
        /// </summary>
        /// <returns>DataTable of course sample screen filenames</returns>
        public DataTable GetCourseSampleScreens()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.GetCourseSampleScreens]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetLearningPathAPI
        /// <summary>
        /// Return a list of learningpaths for APIquery
        /// </summary>
        public static List<int> GetLearningPathsAPI(string searchParam)
        {
            List<int> returnIDs = new List<int>();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", String.Empty, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            // SEARCH PARAMETER
            // build the full text query
            FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
            string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

            // if it's not empty, use it; else make it a wildcard
            SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

            databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.IdsForAPI]", true);
                while (sdr.Read())
                {
                    returnIDs.Add(Convert.ToInt32(sdr[0]));
                }
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return returnIDs;
        }
        #endregion

        #region GetLearningPaths
        /// <param>isPublished</param>
        public static DataTable GetLearningPaths(bool isPublished)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            databaseObject.AddParameter("@isPublished", isPublished, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.GetLearningPaths]", true);

                DataTable learningPaths = new DataTable();
                learningPaths.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return learningPaths;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetLearningPathMaterials
        /// <summary>
        /// Gets a listing of learning path material ids and names that belong to this learning path.
        /// </summary>
        /// <returns>DataTable of learning path material ids and names that belong to this learning path</returns>
        public DataTable GetLearningPathMaterials(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.GetLearningPathMaterials]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetRulesetEnrollments
        /// <summary>
        /// Gets a listing of ruleset enrollment ids, labels, and priority that belong to this learning path.
        /// </summary>
        /// <returns>DataTable of ruleset enrollment ids, labels, and priority that belong to this learning path</returns>
        public DataTable GetRulesetEnrollments(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.GetRuleSetEnrollments]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCertificates
        /// <summary>
        /// Gets a listing of certificate ids and names that belong to this learning path.
        /// </summary>
        /// <returns>DataTable of certificate ids and names that belong to this learning path</returns>
        public DataTable GetCertificates(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.GetCertificates]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion        

        #region GetEmailNotifications
        /// <summary>
        /// Gets a listing of email notification ids and names that belong to this learning path.
        /// </summary>
        /// <returns>DataTable of email notification ids and names that belong to this learning path</returns>
        public DataTable GetEmailNotifications(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.GetEmailNotifications]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SearchLearningPaths
        /// <summary>
        /// Gets a listing of Learning Paths available.
        /// </summary>
        /// <param name="searchParam">search Parameter</param>
        /// <returns>DataTable of Groups.</returns>
        public static DataTable SearchLearningPaths(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER               
                databaseObject.AddParameter("@searchParam", searchParam, SqlDbType.NVarChar, 50, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.SearchLearningPaths]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Learning Path(s).
        /// </summary>
        /// <param name="deletees">DataTable of learning paths to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@LearningPaths", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[LearningPath.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForSelectList
        /// <summary>
        /// Gets a listing of learning path ids and titles to populate into a select list.
        /// </summary>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndNamesForSelectList(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.IdsAndNamesForSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForCouponCodeSelectList
        /// <summary>
        /// Gets a listing of learning path ids and names to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndNamesForCouponCodeSelectList(int idCouponCode, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCouponCode", idCouponCode, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.IdsAndNamesForCouponCodeSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetLearningPathIdByShortcode
        /// <summary>
        /// Gets a learning path ID based on the learning path shortcode
        /// </summary>
        /// <param name="shortcode">learning path shortcode</param>
        /// <returns>learning path id</returns>
        public static int GetLearningPathIdByShortcode(string shortcode)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@shortcode", shortcode, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idLearningPath", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[LearningPath.GetLearningPathIdByShortcode]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return Convert.ToInt32(databaseObject.Command.Parameters["@idLearningPath"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves learning path data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved learning path</returns>
        public int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", this.ShortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@longDescription", this.LongDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@searchTags", this.SearchTags, SqlDbType.NVarChar, 512, ParameterDirection.Input);

            if (this.Cost == null)
            { databaseObject.AddParameter("@cost", DBNull.Value, SqlDbType.Float, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@cost", this.Cost, SqlDbType.Float, 8, ParameterDirection.Input); }

            if (this.IsPublished == null)
            { databaseObject.AddParameter("@isPublished", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isPublished", this.IsPublished, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.IsClosed == null)
            { databaseObject.AddParameter("@isClosed", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@isClosed", this.IsClosed, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.SelfEnrollmentIsOneTimeOnly == null)
            { databaseObject.AddParameter("@selfEnrollmentIsOneTimeOnly", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@selfEnrollmentIsOneTimeOnly", this.SelfEnrollmentIsOneTimeOnly, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.ForceCompletionInOrder == null)
            { databaseObject.AddParameter("@forceCompletionInOrder", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@forceCompletionInOrder", this.ForceCompletionInOrder, SqlDbType.Bit, 1, ParameterDirection.Input); }

            databaseObject.AddParameter("@shortcode", this.Shortcode, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            
            try
            {
                databaseObject.ExecuteNonQuery("[LearningPath.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved learning path
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idLearningPath"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SaveAvatar
        /// <summary>
        /// Saves learning path's avatar path to database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved user</returns>
        private void _SaveAvatar(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPath", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@avatar", this.Avatar, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[LearningPath.SaveAvatar]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified learning path's properties.
        /// </summary>
        /// <param name="idGroup">Group Id</param>
        private void _Details(int idLearningPath)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPath", idLearningPath, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@name", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@shortDescription", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@longDescription", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@cost", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isPublished", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isClosed", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@selfEnrollmentIsOneTimeOnly", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@forceCompletionInOrder", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtModified", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@avatar", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@searchTags", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@isCallingUserEnrolled", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@shortcode", null, SqlDbType.NVarChar, 10, ParameterDirection.Output);
            databaseObject.AddParameter("@isMultipleSelfEnrollmentEligible", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            
            try
            {
                databaseObject.ExecuteNonQuery("[LearningPath.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idLearningPath;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.Name = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@name"].Value);
                this.ShortDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@shortDescription"].Value);
                this.LongDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@longDescription"].Value);
                this.Cost = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@cost"].Value);
                this.IsPublished = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isPublished"].Value);
                this.IsClosed = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isClosed"].Value);
                this.SelfEnrollmentIsOneTimeOnly = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@selfEnrollmentIsOneTimeOnly"].Value);
                this.ForceCompletionInOrder = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@forceCompletionInOrder"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.Avatar = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@avatar"].Value);
                this.SearchTags = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@searchTags"].Value);
                this.IsCallingUserEnrolled = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isCallingUserEnrolled"].Value);
                this.Shortcode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@shortcode"].Value);
                this.IsMultipleSelfEnrollmentEligible = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isMultipleSelfEnrollmentEligible"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a learning path into a collection.
        /// </summary>
        /// <param name="idLearningPath">learning path id</param>
        private void _GetPropertiesInLanguages(int idLearningPath)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idLearningPath", idLearningPath, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[LearningPath.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["name"].ToString(),
                            sdr["shortDescription"].ToString(),
                            sdr["longDescription"].ToString(),
                            sdr["searchTags"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
 }