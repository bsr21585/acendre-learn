﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Asentia.Common;
using Asentia.UMS.Library;

namespace Asentia.LMS.Library
{
    [Serializable]
    public class StandupTrainingInstance
    {
        #region MeetingType ENUM
        public enum MeetingType
        {
            Online = 0,
            Classroom = 1,
            GoToMeeting = 2,
            GoToWebinar = 3,
            GoToTraining = 4,
            WebEx = 5,
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public StandupTrainingInstance()
        {
            this.LanguageSpecificProperties = new ArrayList();
            this.MeetingTimes = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idStandupTrainingInstance">Standup Training Instance Id</param>
        public StandupTrainingInstance(int idStandupTrainingInstance)
        {
            this.LanguageSpecificProperties = new ArrayList();
            this.MeetingTimes = new ArrayList();

            this._Details(idStandupTrainingInstance);
            this._GetPropertiesInLanguages(idStandupTrainingInstance);
            this._GetMeetingTimes(idStandupTrainingInstance);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[StandupTrainingInstance.GetGrid]";

        /// <summary>
        /// Id
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Standup Training Id
        /// </summary>
        public int IdStandupTraining;

        /// <summary>
        /// Site Id
        /// </summary>
        public int IdSite;

        /// <summary>
        /// Title
        /// </summary>
        public string Title;

        /// <summary>
        /// Description
        /// </summary>
        public string Description;

        /// <summary>
        /// Seats
        /// </summary>
        public int Seats;

        /// <summary>
        /// Seats Remaining (calculated)
        /// </summary>
        public int SeatsRemaining;

        /// <summary>
        /// Waiting Seats
        /// </summary>
        public int WaitingSeats;

        /// <summary>
        /// Waiting Seats Remaining (calculated)
        /// </summary>
        public int WaitingSeatsRemaining;

        /// <summary>
        /// Type
        /// </summary>
        public MeetingType Type;

        /// <summary>
        /// Registration Open/Closed
        /// </summary>
        public bool? IsClosed;

        /// <summary>
        /// Registration URL (for Classroom and Online)
        /// </summary>
        public string UrlRegistration;

        /// <summary>
        /// Attend URL (for Online)
        /// </summary>
        public string UrlAttend;

        /// <summary>
        /// City (for Classroom)
        /// </summary>
        public string City;

        /// <summary>
        /// Province (for Classroom)
        /// </summary>
        public string Province;

        /// <summary>
        /// Postal Code (for Classroom)
        /// </summary>
        public string PostalCode;

        /// <summary>
        /// Location Description
        /// </summary>
        public string LocationDescription;

        /// <summary>
        /// Is Deleted
        /// </summary>
        public bool? IsDeleted;

        /// <summary>
        /// Date Deleted
        /// </summary>
        public DateTime? DtDeleted;

        /// <summary>
        /// The "meeting", "webinar", or "training" key that represents the session
        /// when integrated with GTM, GTW, GTT, or WebEx. This value comes from the
        /// platform's API.
        /// </summary>
        public Int64? IntegratedObjectKey;

        /// <summary>
        /// The host url for the "meeting", "webinar", or "training" when integrated
        /// with GTM, GTW, GTT, or WebEx. This value comes from the platform's API.
        /// GTW does not have a special host url, it requires a login directly to the site.
        /// </summary>
        public string HostUrl;

        /// <summary>
        /// The generic join url for the "meeting", "webinar", or "training" when integrated
        /// with GTM, GTW, GTT, or WebEx. This value comes from the platform's API.
        /// GTM only uses a generic join url. GTW and GTT use only an "attendee specific" join url.
        /// WebEx uses an "attendee specific" join url, but also has a generic join url.
        /// </summary>
        public string GenericJoinUrl;

        /// <summary>
        /// The password for the "meeting", "webinar", or "training" when integrated
        /// with GTM, GTW, GTT, or WebEx. This value comes from the platform's API.
        /// This is currently used for only WebEx as WebEx requires a password; the others
        /// do not. So at least for the moment, they will not use one. 
        /// </summary>
        public string MeetingPassword;

        /// <summary>
        /// The id of the organizer for the GTM, GTW, GTT, or WebEx meeting.
        /// </summary>
        public int? IdWebMeetingOrganizer;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// ArrayList of meeting times.
        /// </summary>
        public ArrayList MeetingTimes;
        #endregion        

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Title;
            public string Description;
            public string LocationDescription;

            public LanguageSpecificProperty(string langString, string title, string description, string locationDescription)
            {
                this.LangString = langString;
                this.Title = title;
                this.Description = description;
                this.LocationDescription = locationDescription;
            }
        }

        /// <summary>
        /// Class that represents properties of a meeting time.
        /// </summary>
        public class MeetingTimeProperty
        {
            public DateTime DtStart;
            public DateTime DtEnd;
            public int Minutes;
            public int IdTimezone;
            public bool IsExistingDate = false;
            public MeetingTimeProperty(DateTime dtStart, DateTime dtEnd, int minutes, int idTimezone, bool isExistingDate)
            {
                this.DtStart = dtStart;
                this.DtEnd = dtEnd;
                this.Minutes = minutes;
                this.IdTimezone = idTimezone;
                this.IsExistingDate = isExistingDate;

            }

            public MeetingTimeProperty(DateTime dtStart, DateTime dtEnd, int idTimezone, bool isExistingDate)
            {
                this.DtStart = dtStart;
                this.DtEnd = dtEnd;
                this.IdTimezone = idTimezone;
                this.IsExistingDate = isExistingDate;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves standup training instance data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves course data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this standup training instance.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="title">standup training instance title</param>
        /// <param name="description">description</param>
        /// <param name="locationDescription">location description</param>
        public void SaveLang(string languageString, string title, string description, string locationDescription)
        {
            if (this.Id == 0)
            {
                throw new AsentiaException(_GlobalResources.ProcedureStandupTrainingInstanceSaveLangCannotBeCalledWithoutAValidInstanceID);
            }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@title", title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@description", description, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@locationDescription", locationDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveInstructors
        /// <summary>
        /// Saves Instructors for this Standup Training Instance.
        /// </summary>
        /// <param name="resources">DataTable of Instructors to save</param>
        public void SaveInstructors(DataTable instructors)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Instructors", instructors, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.SaveInstructors]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveResources
        /// <summary>
        /// Saves Resources for this Standup Training Instance.
        /// </summary>
        /// <param name="resources">DataTable of Resources to save</param>
        public void SaveResources(DataTable resources)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Resources", resources, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.SaveResources]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveMeetingTimes
        /// <summary>
        /// Saves Meeting Times for this Standup Training Instance.
        /// </summary>
        /// <param name="meetingTimes">DataTable of Meeting Times to save</param>
        public void SaveMeetingTimes(DataTable meetingTimes)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@MeetingTimes", meetingTimes, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.SaveMeetingTimes]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetInstructors
        /// <summary>
        /// Gets a listing of instructor ids and names that are instructors of this standup training instance.
        /// </summary>
        /// <returns>DataTable of instructor ids and names that are instructors of this standup training instance</returns>
        public DataTable GetInstructors(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idStandupTrainingInstance", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[StandupTrainingInstance.GetInstructors]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetResources
        /// <summary>
        /// Gets a listing of resource ids and names that are resources of this standup training instance.
        /// </summary>
        /// <returns>DataTable of user ids and names that are resources of this standup training instance</returns>
        public DataTable GetResources(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idStandupTrainingInstance", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[StandupTrainingInstance.GetResources]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region JoinUser
        /// <summary>
        /// Joins a user to this standup training instance.
        /// </summary>
        /// <param name="idUser"></param>
        /// <param name="isWaitingList"></param>
        /// <param name="promotedFromWaitlist"></param>
        public void JoinUser(int idUser, bool isWaitingList, bool promotedFromWaitlist = false)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureStandupTrainingInstanceJoinUserCannotBeCalledWithoutAValidInstanceID); }

            bool hasSessionAlreadyOccurred = false;

            if (this.MeetingTimes.Count > 0)
            { hasSessionAlreadyOccurred = ((Library.StandupTrainingInstance.MeetingTimeProperty)(this.MeetingTimes[0])).DtStart < DateTime.UtcNow; }
            else
            { hasSessionAlreadyOccurred = true; }

            Int64 registrantKey = 0;
            string registrantEmail = null;
            string joinUrl = null;

            try // this is wrapped in a try/catch because we dont want to die on error, we want database work to continue in case API is broken
                // note that a failure here doesnt mean absolute failure, just that no registrant gets created in the API, so no auto-tracking 
                // for this user. 
            {
                if (
                    !isWaitingList
                    && !hasSessionAlreadyOccurred
                    && (
                        this.Type == StandupTrainingInstance.MeetingType.GoToMeeting
                        || this.Type == StandupTrainingInstance.MeetingType.GoToTraining
                        || this.Type == StandupTrainingInstance.MeetingType.GoToWebinar
                        || this.Type == StandupTrainingInstance.MeetingType.WebEx
                       )
                   )
                {
                    AsentiaAESEncryption cryptoProvider = new AsentiaAESEncryption();
                    string organizerUsername = null;
                    string organizerPassword = null;
                    Asentia.UMS.Library.User userObject = new Asentia.UMS.Library.User(idUser);
                    registrantEmail = userObject.Email;

                    if (this.IdWebMeetingOrganizer != null && this.IdWebMeetingOrganizer > 0)
                    {
                        WebMeetingOrganizer webMeetingOrganizer = new WebMeetingOrganizer((int)this.IdWebMeetingOrganizer);
                        organizerUsername = webMeetingOrganizer.Username;
                        organizerPassword = cryptoProvider.Decrypt(webMeetingOrganizer.Password);
                    }

                    // if the user does not have an email address, we need to dummy one up for the API call
                    if (String.IsNullOrWhiteSpace(registrantEmail))
                    { registrantEmail = userObject.Id.ToString() + "@" + AsentiaSessionState.SiteHostname + "." + Config.AccountSettings.BaseDomain; }

                    if (this.Type == StandupTrainingInstance.MeetingType.GoToTraining) // GTT                    
                    {
                        GoToTrainingAPI.GTT_Registrant_Response response;

                        if (organizerUsername == null)
                        {
                            GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                         AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_USERNAME),
                                                                         cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PASSWORD)),
                                                                         AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                            response = gttApi.CreateRegistrant((Int64)this.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);

                            registrantKey = response.Registrants[0].RegistrantKey;
                            joinUrl = response.Registrants[0].JoinUrl;
                        }
                        else
                        {
                            GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                         organizerUsername,
                                                                         organizerPassword,
                                                                         AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                            response = gttApi.CreateRegistrant((Int64)this.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);

                            registrantKey = response.Registrants[0].RegistrantKey;
                            joinUrl = response.Registrants[0].JoinUrl;
                        }
                    }
                    else if (this.Type == StandupTrainingInstance.MeetingType.GoToWebinar) // GTW
                    {
                        GoToWebinarAPI.GTW_Registrant_Response response;

                        if (organizerUsername == null)
                        {
                            GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_USERNAME),
                                                                       cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PASSWORD)),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                            response = gtwApi.CreateRegistrant((Int64)this.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);

                            registrantKey = response.Registrants[0].RegistrantKey;
                            joinUrl = response.Registrants[0].JoinUrl;
                        }
                        else
                        {
                            GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                       organizerUsername,
                                                                       organizerPassword,
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                            response = gtwApi.CreateRegistrant((Int64)this.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);

                            registrantKey = response.Registrants[0].RegistrantKey;
                            joinUrl = response.Registrants[0].JoinUrl;
                        }
                    }
                    else if (this.Type == StandupTrainingInstance.MeetingType.WebEx) // WebEx
                    {
                        WebExAPI.WebEx_Registrant_Response.Registrant response;

                        if (organizerUsername == null)
                        {
                            WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_USERNAME),
                                                           cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PASSWORD)));

                            response = wbxApi.CreateMeetingRegistrant((Int64)this.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);

                            registrantKey = response.AttendeeId;
                            joinUrl = wbxApi.GetMeetingJoinURL(response.AttendeeId);
                        }
                        else
                        {
                            WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                           organizerUsername,
                                                           organizerPassword);

                            response = wbxApi.CreateMeetingRegistrant((Int64)this.IntegratedObjectKey, userObject.FirstName, userObject.LastName, registrantEmail);

                            registrantKey = response.AttendeeId;
                            joinUrl = wbxApi.GetMeetingJoinURL(response.AttendeeId);
                        }                        
                    }
                }
            }
            catch // just bury the error and move on
            { }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isWaitingList", isWaitingList, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@promotedFromWaitlist", promotedFromWaitlist, SqlDbType.Bit, 1, ParameterDirection.Input);

            if (registrantKey == 0)
            { databaseObject.AddParameter("@registrantKey", DBNull.Value, SqlDbType.BigInt, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@registrantKey", registrantKey, SqlDbType.BigInt, 8, ParameterDirection.Input); }

            databaseObject.AddParameter("@registrantEmail", registrantEmail, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@joinUrl", joinUrl, SqlDbType.NVarChar, 1024, ParameterDirection.Input);
            
            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.JoinUser]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region RemoveUser
        /// <summary>
        /// Removes a user from this standup training instance.
        /// </summary>
        /// <param name="idUser"></param>
        public void RemoveUser(int idUser)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureStandupTrainingInstanceRemoveUserCannotBeCalledWithoutAValidInstanceID); }

            bool hasSessionAlreadyOccurred = false;

            if (this.MeetingTimes.Count > 0)
            { hasSessionAlreadyOccurred = ((Library.StandupTrainingInstance.MeetingTimeProperty)(this.MeetingTimes[0])).DtStart < DateTime.UtcNow; }
            else
            { hasSessionAlreadyOccurred = true; }

            try // this is wrapped in a try/catch because we dont want to die on error, we want database work to continue in case API is broken
                // note that a failure here doesnt mean absolute failure, just that the registrant doesn't get deleted in the API
            {
                StandUpTrainingInstanceToUserLink stiulObject = new StandUpTrainingInstanceToUserLink(this.Id, idUser);

                if (
                    !hasSessionAlreadyOccurred
                    && (stiulObject.RegistrantKey != null && stiulObject.RegistrantKey > 0)
                    && (
                        this.Type == StandupTrainingInstance.MeetingType.GoToTraining
                        || this.Type == StandupTrainingInstance.MeetingType.GoToWebinar
                        || this.Type == StandupTrainingInstance.MeetingType.WebEx
                       )
                   )
                {
                    AsentiaAESEncryption cryptoProvider = new AsentiaAESEncryption();
                    string organizerUsername = null;
                    string organizerPassword = null;

                    if (this.IdWebMeetingOrganizer != null && this.IdWebMeetingOrganizer > 0)
                    {
                        WebMeetingOrganizer webMeetingOrganizer = new WebMeetingOrganizer((int)this.IdWebMeetingOrganizer);
                        organizerUsername = webMeetingOrganizer.Username;
                        organizerPassword = cryptoProvider.Decrypt(webMeetingOrganizer.Password);
                    }

                    if (this.Type == StandupTrainingInstance.MeetingType.GoToTraining) // GTT                    
                    {
                        if (organizerUsername == null)
                        {
                            GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                         AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_USERNAME),
                                                                         cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_PASSWORD)),
                                                                         AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                            gttApi.DeleteRegistrant(Convert.ToInt64(this.IntegratedObjectKey), (Int64)stiulObject.RegistrantKey);
                        }
                        else
                        {
                            GoToTrainingAPI gttApi = new GoToTrainingAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_APPLICATION),
                                                                         organizerUsername,
                                                                         organizerPassword,
                                                                         AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTT_CONSUMERSECRET));

                            gttApi.DeleteRegistrant(Convert.ToInt64(this.IntegratedObjectKey), (Int64)stiulObject.RegistrantKey);
                        }
                    }
                    else if (this.Type == StandupTrainingInstance.MeetingType.GoToWebinar) // GTW
                    {
                        if (organizerUsername == null)
                        {
                            GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_USERNAME),
                                                                       cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_PASSWORD)),
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                            gtwApi.DeleteRegistrant(Convert.ToInt64(this.IntegratedObjectKey), (Int64)stiulObject.RegistrantKey);
                        }
                        else
                        {
                            GoToWebinarAPI gtwApi = new GoToWebinarAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_APPLICATION),
                                                                       organizerUsername,
                                                                       organizerPassword,
                                                                       AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_GTW_CONSUMERSECRET));

                            gtwApi.DeleteRegistrant(Convert.ToInt64(this.IntegratedObjectKey), (Int64)stiulObject.RegistrantKey);
                        }
                    }
                    else if (this.Type == StandupTrainingInstance.MeetingType.WebEx) // WebEx
                    {
                        if (organizerUsername == null)
                        {
                            WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                           AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_USERNAME),
                                                           cryptoProvider.Decrypt(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_PASSWORD)));

                            wbxApi.DeleteMeetingRegistrant((Int64)stiulObject.RegistrantKey);
                        }
                        else
                        {
                            WebExAPI wbxApi = new WebExAPI(AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.WEBMEETING_WEBEX_APPLICATION),
                                                           organizerUsername,
                                                           organizerPassword);

                            wbxApi.DeleteMeetingRegistrant((Int64)stiulObject.RegistrantKey);
                        }
                    }
                }
            }
            catch // just bury the error and move on
            { }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idUserToPromote", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.RemoveUser]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // promote a user if there is one to promote
                int? idUserToPromote = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idUserToPromote"].Value);

                if (idUserToPromote != null && idUserToPromote > 0)
                { this.JoinUser((int)idUserToPromote, false, true); }                
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region FillSessionMeetingList
        /// <summary>
        /// this method will be used to rebind the date control when there any error occurs
        /// </summary>
        /// <param name="meetings"></param>
        public void FillSessionMeetingList(DataTable meetings)
        {
            this.MeetingTimes = new ArrayList();

            foreach (DataRow row in meetings.Rows)
            {
                this.MeetingTimes.Add(
                       new MeetingTimeProperty(
                           (DateTime)row["dtStart"],
                           (DateTime)row["dtEnd"],
                           (int)row["idTimezone"],
                           (bool)row["isExisting"]
                           )
                   );

            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Standup Training Instance(s).
        /// </summary>
        /// <param name="deletees">DataTable of standup training instances to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@StandupTrainingInstances", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion        

        #region GetAvailableSeats
        /// <summary>
        /// returns the number of seats currently available,0 other wise
        /// </summary>
        /// <returns>returns the number of seats currently available,0 other wise</returns>
        public static int GetAvailableSeats(int sessionId, bool isWaitingList)
        {
            int availableSeats = 0;

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idStandupTrainingInstance", sessionId, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@isWaitingList", isWaitingList, SqlDbType.Bit, 1, ParameterDirection.Input);
                databaseObject.AddParameter("@availableSeats", availableSeats, SqlDbType.Int, 4, ParameterDirection.InputOutput);


                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.GetSeatsAvailability]", true);
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return Convert.ToInt32(databaseObject.Command.Parameters["@availableSeats"].Value);

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region CheckInstructorAvailability
        /// <summary>
        /// Checks that the selected instructor is available for the session meeting times. 
        /// </summary>
        /// <param name="idStandupTrainingInstance"></param>
        /// <param name="idResource"></param>
        /// <param name="meetingTimes"></param>
        /// <returns></returns>
        public static bool CheckInstructorAvailability(int idStandupTrainingInstance, int idInstructor, DataTable meetingTimes)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idInstructor", idInstructor, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@MeetingTimes", meetingTimes, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@isAvailable", 0, SqlDbType.Bit, 1, ParameterDirection.Output);
            
            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.CheckInstructorAvailability]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isAvailable"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region CheckOrganizerAvailability
        /// <summary>
        /// Checks that the selected GTM, GTT, GTW, or WebEx organizer is able to schedule the meeting for the session meeting times. 
        /// </summary>
        /// <param name="idStandupTrainingInstance"></param>
        /// <param name="idResource"></param>
        /// <param name="meetingTimes"></param>
        /// <returns></returns>
        public static bool CheckOrganizerAvailability(int idStandupTrainingInstance, int? idWebMeetingOrganizer, int meetingType, DataTable meetingTimes)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);

            if (idWebMeetingOrganizer == null)
            { databaseObject.AddParameter("@idWebMeetingOrganizer", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idWebMeetingOrganizer", idWebMeetingOrganizer, SqlDbType.Int, 4, ParameterDirection.Input); }
            
            databaseObject.AddParameter("@meetingType", meetingType, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@MeetingTimes", meetingTimes, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@isAvailable", 0, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.CheckOrganizerAvailability]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isAvailable"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion        
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves standup training data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved standup training</returns>
        public int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idStandupTraining", this.IdStandupTraining, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@description", this.Description, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@seats", this.Seats, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@waitingSeats", this.WaitingSeats, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@type", this.Type, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isClosed", this.IsClosed, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@urlRegistration", this.UrlRegistration, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@urlAttend", this.UrlAttend, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@city", this.City, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@province", this.Province, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@postalcode", this.PostalCode, SqlDbType.NVarChar, 25, ParameterDirection.Input);
            databaseObject.AddParameter("@locationDescription", this.LocationDescription, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            if (this.IntegratedObjectKey == null)
            { databaseObject.AddParameter("@integratedObjectKey", DBNull.Value, SqlDbType.BigInt, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@integratedObjectKey", this.IntegratedObjectKey, SqlDbType.BigInt, 8, ParameterDirection.Input); }


            databaseObject.AddParameter("@hostUrl", this.HostUrl, SqlDbType.NVarChar, 1024, ParameterDirection.Input);            
            databaseObject.AddParameter("@genericJoinUrl", this.GenericJoinUrl, SqlDbType.NVarChar, 1024, ParameterDirection.Input);
            databaseObject.AddParameter("@meetingPassword", this.MeetingPassword, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            if (this.IdWebMeetingOrganizer == null)
            { databaseObject.AddParameter("@idWebMeetingOrganizer", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@idWebMeetingOrganizer", this.IdWebMeetingOrganizer, SqlDbType.Int, 4, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved standup training instance
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idStandupTrainingInstance"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified standup training's properties.
        /// </summary>
        /// <param name="idStandupTrainingInstance">Standup Training Instance Id</param>
        private void _Details(int idStandupTrainingInstance)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idStandupTraining", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@title", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@description", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@seats", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@seatsRemaining", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@waitingSeats", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@waitingSeatsRemaining", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@type", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@isClosed", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@urlRegistration", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@urlAttend", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@city", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@province", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@postalcode", null, SqlDbType.NVarChar, 25, ParameterDirection.Output);
            databaseObject.AddParameter("@locationDescription", null, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@integratedObjectKey", null, SqlDbType.BigInt, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@hostUrl", null, SqlDbType.NVarChar, 1024, ParameterDirection.Output);
            databaseObject.AddParameter("@genericJoinUrl", null, SqlDbType.NVarChar, 1024, ParameterDirection.Output);
            databaseObject.AddParameter("@meetingPassword", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@idWebMeetingOrganizer", null, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[StandupTrainingInstance.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idStandupTrainingInstance;
                this.IdStandupTraining = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idStandupTraining"].Value);
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.Description = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@description"].Value);
                this.Seats = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@seats"].Value);
                this.SeatsRemaining = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@seatsRemaining"].Value);
                this.WaitingSeats = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@waitingSeats"].Value);
                this.WaitingSeatsRemaining = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@waitingSeatsRemaining"].Value);
                this.Type = (StandupTrainingInstance.MeetingType)AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@type"].Value);
                this.IsClosed = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isClosed"].Value);
                this.UrlRegistration = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@urlRegistration"].Value);
                this.UrlAttend = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@urlAttend"].Value);
                this.City = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@city"].Value);
                this.Province = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@province"].Value);
                this.PostalCode = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@postalcode"].Value);
                this.LocationDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@locationDescription"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.IntegratedObjectKey = AsentiaDatabase.ParseDbParamNullableInt64(databaseObject.Command.Parameters["@integratedObjectKey"].Value);
                this.HostUrl = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@hostUrl"].Value);
                this.GenericJoinUrl = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@genericJoinUrl"].Value);
                this.MeetingPassword = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@meetingPassword"].Value);
                this.IdWebMeetingOrganizer = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@idWebMeetingOrganizer"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a standup training instance into a collection.
        /// </summary>
        /// <param name="idStandupTrainingInstance">standup training instance id</param>
        private void _GetPropertiesInLanguages(int idStandupTrainingInstance)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[StandupTrainingInstance.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["title"].ToString(),
                            sdr["description"].ToString(),
                            sdr["locationDescription"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetMeetingTimes
        /// <summary>
        /// Loads the meeting times for a standup training instance into a collection.
        /// </summary>
        /// <param name="idStandupTrainingInstance">standup training instance id</param>
        private void _GetMeetingTimes(int idStandupTrainingInstance)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[StandupTrainingInstance.GetMeetingTimes]", true);

                // loop through the returned recordset, instansiate a MeetingTimeProperty object
                // and add it to the MeetingTimes ArrayList
                while (sdr.Read())
                {
                    this.MeetingTimes.Add(
                        new MeetingTimeProperty(
                            (DateTime)sdr["dtStart"],
                            (DateTime)sdr["dtEnd"],
                            (int)sdr["minutes"],
                            (int)sdr["idTimezone"],
                            (bool)true
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
