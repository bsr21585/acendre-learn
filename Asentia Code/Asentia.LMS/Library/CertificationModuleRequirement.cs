﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class CertificationModuleRequirement
    {
        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CertificationModuleRequirement()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idCertificationModuleRequirement">Certification Module Requirement Id</param>
        public CertificationModuleRequirement(int idCertificationModuleRequirement)
        {
            this.LanguageSpecificProperties = new ArrayList();

            this._Details(idCertificationModuleRequirement);
            this._GetPropertiesInLanguages(idCertificationModuleRequirement);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Certification Module Requirement Id
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Certification Module Requirement Set Id
        /// </summary>
        public int IdCertificationModuleRequirementSet;

        /// <summary>
        /// Site Id
        /// </summary>
        public int IdSite;

        /// <summary>
        /// Label
        /// </summary>
        public string Label;

        /// <summary>
        /// Short Description
        /// </summary>
        public string ShortDescription;

        /// <summary>
        /// RequirementType (Course, Credit, Task)
        /// </summary>
        public int RequirementType;

        /// <summary>
        /// CourseCompletionIsAny
        /// For "Course" requirement type. Can the user complete any course?
        /// </summary>
        public bool? CourseCompletionIsAny;

        /// <summary>
        /// ForceCourseCompletionInOrder - NOT USED AT THE MOMENT
        /// </summary>
        public bool? ForceCourseCompletionInOrder;

        /// <summary>
        /// NumberCreditsRequired
        /// For "Credit" requirement type.
        /// </summary>
        public double? NumberCreditsRequired;

        /// <summary>
        /// DocumentUploadFileType - NOT USED AT THE MOMENT
        /// </summary>
        public int? DocumentUploadFileType;

        /// <summary>
        /// DocumentationApprovalRequired
        /// For "Task" requirement type. Is the completion of the task required to be signed off on. 
        /// </summary>
        public bool? DocumentationApprovalRequired;

        /// <summary>
        /// AllowSupervisorsToApproveDocumentation
        /// For "Task" requirement type. Are learner's supervisors allowed to sign off?
        /// </summary>
        public bool? AllowSupervisorsToApproveDocumentation;

        /// <summary>
        /// AllowExpertsToApproveDocumentation - NOT USED AT THE MOMENT
        /// For "Task" requirement type. Are "experts" allowed to sign off?
        /// </summary>
        public bool? AllowExpertsToApproveDocumentation;

        /// <summary>
        /// CourseCreditEligibleCoursesIsAll
        /// For "Credit" requirement type. Are all courses attached to the certification in play for credit requirements?
        /// </summary>
        public bool? CourseCreditEligibleCoursesIsAll;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Label;
            public string ShortDescription;

            /// <summary>
            /// parameterless constructor for serialization purpose
            /// </summary>
            public LanguageSpecificProperty()
            { ;}

            public LanguageSpecificProperty(string langString, string label, string shortDescription)
            {
                this.LangString = langString;
                this.Label = label;
                this.ShortDescription = shortDescription;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves certification module requirement data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves certification module requirement data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this certification module requirement.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="label">certification label</param>
        /// <param name="shortDescription">certification module requirement short description</param>        
        public void SaveLang(string languageString, string label, string shortDescription)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureCertificationModuleRequirementSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificationModuleRequirement", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@label", label, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", shortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);            

            try
            {
                databaseObject.ExecuteNonQuery("[CertificationModuleRequirement.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCourseRequirementCourseIds
        /// <summary>
        /// Returns a recordset of course requirement ids that belong to this certification.
        /// </summary>
        /// <returns>DataTable of course requirement ids that belong to this certification</returns>
        public DataTable GetCourseRequirementCourseIds()
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCertificationModuleRequirement", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[CertificationModuleRequirement.GetCourseRequirementCourseIds]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveRequirementCourses
        /// <summary>
        /// Saves course information for course requirements.
        /// </summary>
        /// <param name="attachedCourses">DataTable of requirement courses</param>
        public void SaveRequirementCourses(DataTable requirementCourses)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idCertificationModuleRequirement", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@CertificationRequirementCourses", requirementCourses, SqlDbType.Structured, null, ParameterDirection.Input);

                databaseObject.ExecuteNonQuery("[CertificationModuleRequirement.SaveRequirementCourses]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes Certification Module Requirement(s).
        /// </summary>
        /// <param name="deletees">DataTable of certification module requirements to delete</param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@CertificationModuleRequirements", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CertificationModuleRequirement.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveTask
        /// <summary>
        /// Saves task information for a learner's certification task.
        /// Used when a learner uploads their task.
        /// </summary>
        /// <param name="idDataLesson"></param>
        /// <param name="uploadedTaskFilename"></param>
        public static void SaveTask(int idDataCertificationModuleRequirement, string uploadedTaskFilename)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataCertificationModuleRequirement", idDataCertificationModuleRequirement, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@uploadedTaskFilename", uploadedTaskFilename, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[CertificationModuleRequirement.SaveTask]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region ProctorTask
        /// <summary>
        /// Updates approval (signoff) of a task requirement that needs to be signed off on.
        /// Used when a proctor marks approval or signs off on a task.
        /// </summary>
        /// <param name="idCertificationToUserLink"></param>
        /// <param name="idDataCertificationModuleRequirement"></param>
        /// <param name="approve"></param>
        public static void ProctorTask(int idCertificationToUserLink, int idDataCertificationModuleRequirement, bool approve)            
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificationToUserLink", idCertificationToUserLink, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataCertificationModuleRequirement", idDataCertificationModuleRequirement, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@approve", approve, SqlDbType.Bit, 1, ParameterDirection.Input);            

            try
            {
                databaseObject.ExecuteNonQuery("[CertificationModuleRequirement.ProctorTask]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves certification module requirement data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved certification module requirement</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificationModuleRequirement", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idCertificationModuleRequirementSet", this.IdCertificationModuleRequirementSet, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@label", this.Label, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@shortDescription", this.ShortDescription, SqlDbType.NVarChar, 512, ParameterDirection.Input);
            databaseObject.AddParameter("@requirementType", this.RequirementType, SqlDbType.Int, 4, ParameterDirection.Input);

            if (this.CourseCompletionIsAny == null)
            { databaseObject.AddParameter("@courseCompletionIsAny", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@courseCompletionIsAny", this.CourseCompletionIsAny, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.ForceCourseCompletionInOrder == null)
            { databaseObject.AddParameter("@forceCourseCompletionInOrder", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@forceCourseCompletionInOrder", this.ForceCourseCompletionInOrder, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.NumberCreditsRequired == null)
            { databaseObject.AddParameter("@numberCreditsRequired", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@numberCreditsRequired", this.NumberCreditsRequired, SqlDbType.Float, 8, ParameterDirection.Input); }

            if (this.DocumentUploadFileType == null)
            { databaseObject.AddParameter("@documentUploadFileType", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@documentUploadFileType", this.DocumentUploadFileType, SqlDbType.Int, 4, ParameterDirection.Input); }

            if (this.DocumentationApprovalRequired == null)
            { databaseObject.AddParameter("@documentationApprovalRequired", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@documentationApprovalRequired", this.DocumentationApprovalRequired, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.AllowSupervisorsToApproveDocumentation == null)
            { databaseObject.AddParameter("@allowSupervisorsToApproveDocumentation", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@allowSupervisorsToApproveDocumentation", this.AllowSupervisorsToApproveDocumentation, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.AllowExpertsToApproveDocumentation == null)
            { databaseObject.AddParameter("@allowExpertsToApproveDocumentation", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@allowExpertsToApproveDocumentation", this.AllowExpertsToApproveDocumentation, SqlDbType.Bit, 1, ParameterDirection.Input); }

            if (this.CourseCreditEligibleCoursesIsAll == null)
            { databaseObject.AddParameter("@courseCreditEligibleCoursesIsAll", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@courseCreditEligibleCoursesIsAll", this.CourseCreditEligibleCoursesIsAll, SqlDbType.Bit, 1, ParameterDirection.Input); }

            try
            {
                databaseObject.ExecuteNonQuery("[CertificationModuleRequirement.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved certification module requirement
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idCertificationModuleRequirement"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified certification module requirement's properties.
        /// </summary>
        /// <param name="idCertificationModuleRequirement">Certification Module Requirement Id</param>
        private void _Details(int idCertificationModuleRequirement)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificationModuleRequirement", idCertificationModuleRequirement, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idCertificationModuleRequirementSet", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@label", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@shortDescription", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);
            databaseObject.AddParameter("@requirementType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@courseCompletionIsAny", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@forceCourseCompletionInOrder", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@numberCreditsRequired", null, SqlDbType.Float, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@documentUploadFileType", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@documentationApprovalRequired", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@allowSupervisorsToApproveDocumentation", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@allowExpertsToApproveDocumentation", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@courseCreditEligibleCoursesIsAll", null, SqlDbType.Bit, 1, ParameterDirection.Output);           

            try
            {
                databaseObject.ExecuteNonQuery("[CertificationModuleRequirement.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idCertificationModuleRequirement;
                this.IdCertificationModuleRequirementSet = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idCertificationModuleRequirementSet"].Value);
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.Label = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@label"].Value);
                this.ShortDescription = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@shortDescription"].Value);
                this.RequirementType = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@requirementType"].Value);
                this.CourseCompletionIsAny = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@courseCompletionIsAny"].Value);
                this.ForceCourseCompletionInOrder = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@forceCourseCompletionInOrder"].Value);
                this.NumberCreditsRequired = AsentiaDatabase.ParseDbParamNullableDouble(databaseObject.Command.Parameters["@numberCreditsRequired"].Value);
                this.DocumentUploadFileType = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@documentUploadFileType"].Value);
                this.DocumentationApprovalRequired = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@documentationApprovalRequired"].Value);
                this.AllowSupervisorsToApproveDocumentation = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@allowSupervisorsToApproveDocumentation"].Value);
                this.AllowExpertsToApproveDocumentation = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@allowExpertsToApproveDocumentation"].Value);
                this.CourseCreditEligibleCoursesIsAll = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@courseCreditEligibleCoursesIsAll"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a certification module requirement into a collection.
        /// </summary>
        /// <param name="idCertificationModuleRequirement">certification module requirement id</param>
        private void _GetPropertiesInLanguages(int idCertificationModuleRequirement)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCertificationModuleRequirement", idCertificationModuleRequirement, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[CertificationModuleRequirement.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["label"].ToString(),
                            sdr["shortDescription"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
