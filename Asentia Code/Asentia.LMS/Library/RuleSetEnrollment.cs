﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Xml;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    #region RuleSetEnrollmentType ENUM
    public enum RuleSetEnrollmentType
    {
        FixedDateOneTime = 1,
        RelativeDateOneTime = 2,
        FixedDateRecurring = 3,
        RelativeDateRecurring = 4,
    }
    #endregion

    [Serializable]
    public class RuleSetEnrollment
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public RuleSetEnrollment()
        {
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idRuleSetEnrollment">RuleSet Enrollment Id</param>
        public RuleSetEnrollment(int idRuleSetEnrollment)
        {
            this.LanguageSpecificProperties = new ArrayList();

            this._Details(idRuleSetEnrollment);
            this._GetPropertiesInLanguages(idRuleSetEnrollment);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[RuleSetEnrollment.GetGrid]";

        /// <summary>
        /// Rule Set Enrollment Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Course Id.
        /// <seealso cref="Course" />
        /// </summary>
        public int IdCourse;

        /// <summary>
        /// Timezone Id.
        /// </summary>
        public int IdTimezone;

        /// <summary>
        /// Priority.
        /// </summary>
        public int Priority;

        /// <summary>
        /// Label.
        /// </summary>
        public string Label;

        /// <summary>
        /// Is Locked By Prerequisites.
        /// </summary>
        public bool IsLockedByPrerequisites;

        /// <summary>
        /// Is Fixed Date.
        /// </summary>
        public bool IsFixedDate;

        /// <summary>
        /// Start Date.
        /// </summary>
        public DateTime DtStart;

        /// <summary>
        /// End Date.
        /// </summary>
        public DateTime? DtEnd;

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime DtCreated;

        /// <summary>
        /// Delay Interval.
        /// </summary>
        public int? DelayInterval;

        /// <summary>
        /// Delay Timeframe.
        /// </summary>
        public string DelayTimeframe;

        /// <summary>
        /// Due Interval.
        /// </summary>
        public int? DueInterval;

        /// <summary>
        /// Due Timeframe.
        /// </summary>
        public string DueTimeframe;

        /// <summary>
        /// Recur Interval.
        /// </summary>
        public int? RecurInterval;

        /// <summary>
        /// Recur Timeframe.
        /// </summary>
        public string RecurTimeframe;

        /// <summary>
        /// Expires From Start Interval.
        /// </summary>
        public int? ExpiresFromStartInterval;

        /// <summary>
        /// Expires From Start Timeframe.
        /// </summary>
        public string ExpiresFromStartTimeframe;

        /// <summary>
        /// Expires From First Launch Interval.
        /// </summary>
        public int? ExpiresFromFirstLaunchInterval;

        /// <summary>
        /// Expires From First Launch Timeframe.
        /// </summary>
        public string ExpiresFromFirstLaunchTimeframe;

        /// <summary>
        /// Force Re-Assignment of this RuleSetEnrollment to Users who have completed it?
        /// Only applicable to One-Time RuleSetEnrollments
        /// </summary>
        public bool? ForceReassignment;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// Parent RuleSet Enrollment Id
        /// </summary>
        public int IdParentRuleSetEnrollment;
        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Label;

            public LanguageSpecificProperty(string langString, string label)
            {
                this.LangString = langString;
                this.Label = label;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves ruleset enrollment data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }

        /// <summary>
        /// Overloaded method that saves ruleset enrollment data to the database with a caller specified.
        /// This would be used when Save needs to be called outside of a user session.
        /// Generally this would be called with a value of 1.
        /// </summary>
        /// <param name="idCaller">the id of the caller</param>
        public int Save(int idCaller)
        {
            return this._Save(idCaller);
        }
        #endregion

        #region SaveLang
        /// <summary>
        /// Saves "language-specific" properties for this ruleset enrollment.
        /// </summary>
        /// <param name="languageString">the language</param>
        /// <param name="label">label</param>
        public void SaveLang(string languageString, string label)
        {
            if (this.Id == 0)
            { throw new AsentiaException(_GlobalResources.ProcedureRulesetEnrollmentSaveLangCannotBeCalledWithoutAValidID); }

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@label", label, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetEnrollment.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion        
        #endregion

        #region Static Methods
        #region Delete
        /// <summary>
        /// Deletes RuleSet Enrollment(s).
        /// </summary>
        /// <param name="deletees">DataTable of ruleset enrollments to delete</param>
        /// <param name="idCourse">id of course the ruleset enrollments belong to</param>
        public static void Delete(DataTable deletees, int idCourse)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idCourse", idCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@RuleSetEnrollments", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetEnrollment.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region AssignEnrollments
        /// <summary>
        /// Assigns RuleSet Enrollment(s) to users.
        /// </summary>
        /// <param name="courses">DataTable of courses to assign ruleset enrollments for</param>
        /// <param name="filters">DataTable of filters (user or group ids, see filterBy param) to assign ruleset enrollments for</param>
        /// <param name="filterBy">the type of ids the filters DataTable contains, "user" or "group"</param>
        public static void AssignEnrollments(DataTable courses, DataTable filters, string filterBy)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Courses", courses, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@Filters", filters, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@filterBy", filterBy, SqlDbType.NVarChar, 10, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetEnrollment.AssignEnrollments]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region UpdateOrder
        /// <summary>
        /// Method to update the order of ruleset enrollments.
        /// </summary>
        /// <param name="enrollmentsWithOrdering"></param>
        public static void UpdateOrder(DataTable rulesetEnrollmentOrdering)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@RuleSetEnrollmentOrdering", rulesetEnrollmentOrdering, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RulesetEnrollment.UpdateOrder]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndCourseNamesForRuleSetEnrollmentReplicationList
        /// <summary>
        /// Gets a listing of course ids and titles to populate into a 
        /// "replicate to other courses" checkbox list for a course ruleset
        /// enrollment.
        /// </summary>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndCourseNamesForRuleSetEnrollmentReplicationList(int idRuleSetEnrollment, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idRuleSetEnrollment", idRuleSetEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[RuleSetEnrollment.IdsAndCourseNamesForRuleSetEnrollmentReplicationList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }

        #endregion
        #endregion

        #region Private Methods
        #region _Save
        /// <summary>
        /// Saves ruleset enrollment data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the saved ruleset enrollment</returns>
        public int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetEnrollment", this.Id, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idCourse", this.IdCourse, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idTimezone", this.IdTimezone, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@label", this.Label, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@isLockedByPrerequisites", this.IsLockedByPrerequisites, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@isFixedDate", this.IsFixedDate, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, 8, ParameterDirection.Input);

            if (this.DtEnd == null)
            { databaseObject.AddParameter("@dtEnd", DBNull.Value, SqlDbType.DateTime, 8, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dtEnd", this.DtEnd, SqlDbType.DateTime, 8, ParameterDirection.Input); }

            if (this.DelayInterval == null)
            { databaseObject.AddParameter("@delayInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@delayInterval", this.DelayInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@delayTimeframe", this.DelayTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.DueInterval == null)
            { databaseObject.AddParameter("@dueInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@dueInterval", this.DueInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@dueTimeframe", this.DueTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.RecurInterval == null)
            { databaseObject.AddParameter("@recurInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@recurInterval", this.RecurInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@recurTimeframe", this.RecurTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromStartInterval == null)
            { databaseObject.AddParameter("@expiresFromStartInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromStartInterval", this.ExpiresFromStartInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromStartTimeframe", this.ExpiresFromStartTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ExpiresFromFirstLaunchInterval == null)
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", DBNull.Value, SqlDbType.Int, 4, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@expiresFromFirstLaunchInterval", this.ExpiresFromFirstLaunchInterval, SqlDbType.Int, 4, ParameterDirection.Input); }

            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", this.ExpiresFromFirstLaunchTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);

            if (this.ForceReassignment == null)
            { databaseObject.AddParameter("@forceReassignment", DBNull.Value, SqlDbType.Bit, 1, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@forceReassignment", this.ForceReassignment, SqlDbType.Bit, 1, ParameterDirection.Input); }

            databaseObject.AddParameter("@idParentRuleSetEnrollment", this.IdParentRuleSetEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetEnrollment.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // set the id of the saved ruleset enrollment
                this.Id = Convert.ToInt32(databaseObject.Command.Parameters["@idRuleSetEnrollment"].Value);

                // return
                return this.Id;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified ruleset enrollment's properties.
        /// </summary>
        /// <param name="idRuleSetEnrollment">RuleSet Enrollment Id</param>
        private void _Details(int idRuleSetEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetEnrollment", idRuleSetEnrollment, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idCourse", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idTimezone", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@priority", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@label", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isLockedByPrerequisites", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isFixedDate", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtStart", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtEnd", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@delayInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@delayTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dueInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dueTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@recurInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@recurTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromStartTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchInterval", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@expiresFromFirstLaunchTimeframe", null, SqlDbType.NVarChar, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@forceReassignment", null, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@idParentRuleSetEnrollment", null, SqlDbType.Int, 4, ParameterDirection.Output);
            
            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetEnrollment.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idRuleSetEnrollment;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdCourse = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idCourse"].Value);
                this.IdTimezone = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idTimezone"].Value);
                this.Priority = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@priority"].Value);
                this.Label = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@label"].Value);
                this.IsLockedByPrerequisites = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isLockedByPrerequisites"].Value);                
                this.IsFixedDate = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isFixedDate"].Value);
                this.DtStart = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtStart"].Value);
                this.DtEnd = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtEnd"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DelayInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@delayInterval"].Value);
                this.DelayTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@delayTimeframe"].Value);
                this.DueInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@dueInterval"].Value);
                this.DueTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@dueTimeframe"].Value);
                this.RecurInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@recurInterval"].Value);
                this.RecurTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@recurTimeframe"].Value);
                this.ExpiresFromStartInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromStartInterval"].Value);
                this.ExpiresFromStartTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromStartTimeframe"].Value);
                this.ExpiresFromFirstLaunchInterval = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@expiresFromFirstLaunchInterval"].Value);
                this.ExpiresFromFirstLaunchTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@expiresFromFirstLaunchTimeframe"].Value);
                this.ForceReassignment = AsentiaDatabase.ParseDbParamNullableBool(databaseObject.Command.Parameters["@forceReassignment"].Value);
                this.IdParentRuleSetEnrollment = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idParentRuleSetEnrollment"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a ruleset enrollment into a collection.
        /// </summary>
        /// <param name="idRuleSetEnrollment">RuleSet Enrollment Id</param>
        private void _GetPropertiesInLanguages(int idRuleSetEnrollment)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetEnrollment", idRuleSetEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[RuleSetEnrollment.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["label"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Replicate
        /// <summary>
        /// Method to replicate a ruleset enrollment to other courses.
        /// </summary>
        /// <param name="idRuleSetEnrollmentToReplicate">ruleset enrollment id to replicate</param>
        /// <param name="idCoursesToReplicateTo">datatable of course ids to replicate ruleset enrollment to</param>
        /// <param name="idCoursesToUnSync">datatable of course ids to unsync ruleset enrollment replication from</param>
        public static void Replicate(int idRuleSetEnrollmentToReplicate, DataTable idCoursesToReplicateTo, DataTable idCoursesToUnSync)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idRuleSetEnrollmentToReplicate", idRuleSetEnrollmentToReplicate, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idCoursesToReplicateTo", idCoursesToReplicateTo, SqlDbType.Structured, null, ParameterDirection.Input);
            databaseObject.AddParameter("@idCoursesToUnSync", idCoursesToUnSync, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[RuleSetEnrollment.Replicate]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}