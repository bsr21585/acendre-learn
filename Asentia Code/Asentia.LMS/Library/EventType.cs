﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.LMS.Library
{
    public class EventType
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public EventType()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idEventType">Event Type Id</param>
        public EventType(int idEventType)
        {
            _Details(idEventType);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Event Type Id.
        /// </summary>
        public int IdEventType { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Allow Prior Send.
        /// </summary>
        public bool? AllowPriorSend { get; set; }

        /// <summary>
        /// Allow Post Send.
        /// </summary>
        public bool? AllowPostSend { get; set; }
        #endregion

        #region Static Methods
        #region GetEventTypesInfoIntoDataTable
        public static DataTable GetEventTypesInfo(int? idEventTypePrefix = null)
        {
            DataTable eventTypesList = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idEventTypePrefix", idEventTypePrefix, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader eventTypesReader = databaseObject.ExecuteDataReader("[EventType.GetListEventTypeRecipients]", true);
                eventTypesList.Load(eventTypesReader);
                eventTypesReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                // throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return eventTypesList;
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idEventType">Event Type Id</param>
        private void _Details(int idEventType)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEventType", idEventType, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@name", this.Name, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@allowPriorSend", this.AllowPriorSend, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@allowPostSend", this.AllowPostSend, SqlDbType.Bit, 1, ParameterDirection.Output);
            
            try
            {
                databaseObject.ExecuteNonQuery("[EventType.Details]", true);
                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdEventType = idEventType;
                this.Name = databaseObject.Command.Parameters["@name"].Value.ToString();
                this.AllowPriorSend = databaseObject.Command.Parameters["@allowPriorSend"].Value == DBNull.Value ? (bool?)null : Convert.ToBoolean(databaseObject.Command.Parameters["@allowPriorSend"].Value);
                this.AllowPostSend = databaseObject.Command.Parameters["@allowPostSend"].Value == DBNull.Value ? (bool?)null : Convert.ToBoolean(databaseObject.Command.Parameters["@allowPostSend"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}