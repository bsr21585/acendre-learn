﻿function ForgotPassword_Click(modalHeaderIconPath, modalHeaderText, cancelButtonText) {
    // change header 
    $("#LoginFormModalModalPopupHeaderIcon").prop("src", modalHeaderIconPath);
    $("#LoginFormModalModalPopupHeaderText").text(modalHeaderText);

    // remove feedback
    $("#LoginFormFeedbackPanel").removeClass("PageFeedbackError");
    $("#LoginFormFeedbackPanel").removeClass("PageFeedbackSuccess");
    $("#LoginFormFeedbackPanel").html("");

    // reset forgot password username box
    $("#ForgotPasswordUsernameBox").val("");

    // enable forgot password submit button
    $("#LoginFormForgotPasswordSubmitButton").prop("disabled", false);
    $("#LoginFormForgotPasswordSubmitButton").removeClass("DisabledButton");
    $("#LoginFormForgotPasswordSubmitButton").addClass("Button");

    // change forgot password cancel button text
    $("#LoginFormForgotPasswordCancelButton").val(cancelButtonText);

    // hide form content and show forgot password panel
    $("#LoginFormFormContent").hide();
    $("#LoginFormForgotPasswordContent").show();

    // give focus to the username field for forgot password
    $get("ForgotPasswordUsernameBox").focus();

    // re-center the modal
    $find("LoginFormModalModalPopupExtender")._layout();
}

function ForgotPasswordCancel_Click(modalHeaderIconPath, modalHeaderText) {
    // change header
    $("#LoginFormModalModalPopupHeaderIcon").prop("src", modalHeaderIconPath);
    $("#LoginFormModalModalPopupHeaderText").text(modalHeaderText);

    // remove feedback
    $("#LoginFormFeedbackPanel").removeClass("PageFeedbackError");
    $("#LoginFormFeedbackPanel").removeClass("PageFeedbackSuccess");
    $("#LoginFormFeedbackPanel").html("");

    // change forgot password cancel button text
    $("#ForgotPasswordUsernameBox").val("");

    // hide forgot password panel and show form content
    $("#LoginFormForgotPasswordContent").hide();
    $("#LoginFormFormContent").show();

    // give focus to the username field for login
    $get("LoginFormUsernameBox").focus();

    // re-center the modal
    $find("LoginFormModalModalPopupExtender")._layout();
}