﻿
namespace Asentia.Controls
{
    /// <summary>
    /// Defines the properties of a <see cref="GridColumn"/>. These properties
    /// are used to define the format of the data in the column and the condition
    /// under which that format is applied.
    /// </summary>
    public class GridColumnProperty
    {
        #region Constructors
        /// <summary>
        /// Creates an instance of this <see cref="GridColumnProperty"/> using
        /// a condition, and format HTML.
        /// </summary>
        /// <param name="condition">The condition under which this <see cref="GridColumnProperty"/> will be applied.</param>
        /// <param name="html">The format template HTML to be aplied to the data.</param>
        public GridColumnProperty(string condition, string html)
        {
            this.Condition = condition;
            this.Html = html;
        }

        /// <summary>
        /// Creates an instance of this <see cref="GridColumnProperty"/> using
        /// only format HTML (no condition).
        /// </summary>
        /// <param name="html">The format template HTML to be aplied to the data.</param>
        public GridColumnProperty(string html)
        {
            this.Html = html;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The condition under which this <see cref="GridColumnProperty"/>
        /// will be used to apply formatting to the data in the <see cref="GridColumn"/>.
        /// </summary>
        public string Condition = null;

        /// <summary>
        /// The format template to be aplied to the data in the <see cref="GridColumn"/>.
        /// </summary>
        public string Html = null;
        #endregion
    }
}