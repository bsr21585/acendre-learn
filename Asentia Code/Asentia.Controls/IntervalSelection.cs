﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    public class IntervalSelection : WebControl
    {
        #region Constructor
        /// <summary>
        /// /// Initializes a new instance of the Interval Selection control.
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <param name="useNone">Use None check Box or not</param>
        /// <param name="BeforeText">Text Before Input Controls</param>
        /// <param name="AfterText">Text After Input Contols</param>
        /// <param name="CheckBoxText">None Check Box Text</param>
        public IntervalSelection(string id, bool useNone, string BeforeText,string AfterText, string CheckBoxText)
            : base(HtmlTextWriterTag.Div)
        {
            // set the properties
            this.ID = id;
            this.UseNone = useNone;
            this.NoneCheckboxText = CheckBoxText;
            this.BeforeInputText = BeforeText;
            this.AfterInputText = AfterText;
            // build the controls
            this._BuildControls();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Does this control use the "none" checkbox?
        /// </summary>
        public bool UseNone = false;

        /// <summary>
        /// The text to display for the "none" checkbox.
        /// </summary>
        public string NoneCheckboxText
        { set { this._None.Text = " " + value; } }

        /// <summary>
        /// Text Before Input Controls
        /// </summary>
        public string BeforeInputText
        { set { this._BeforeInput.Text = " " + value; } }

        /// <summary>
        /// To Check and Set the Checked States of None CheckBox
        /// </summary>
        public bool NoneCheckUnCheck
        {
            set { this._None.Checked = value; }
            get { return this._None.Checked; }
        }

        /// <summary>
        /// Text After Input Contols
        /// </summary>
        public string AfterInputText
        { set { this._AfterInput.Text = " " + value; } }
      
        /// <summary>
        /// Gets or sets Interval value.
        /// </summary>
        public string DropDownValue
        {
            get
            {
                if (this.UseNone)
                {
                    if (this._None.Checked)
                    { return null; }
                }
                return this._DropDownList.SelectedValue;
            }
            set
            {
                if (value != null)
                {
                    this._DropDownList.SelectedValue = value; 
                }
                else
                {
                    this._DropDownList.SelectedValue = "0";

                    if (this.UseNone)
                    { this._None.Checked = true; }
                }
            }
        }

        /// <summary>
        /// Drop Donw Selected Value
        /// </summary>
        public string DropDownText
        {
            get
            {
                if (this.UseNone)
                {
                    if (this._None.Checked)
                    { return null; }
                }
                return this._DropDownList.SelectedItem.Text;
            }
        }

        /// <summary>
        /// Gets string value of the input control.
        /// </summary>
        public string TextBoxValue
        {
            get
            {
                if (this.UseNone)
                {
                    if (this._None.Checked)
                    { return null; }
                }
                if (!String.IsNullOrWhiteSpace(this._InputControl.Text))
                {
                    return this._InputControl.Text; 
                }
                else
                { return null; }
            }
            set
            {
                if (value != null)
                {
                    this._InputControl.Text = value;
                }
                else
                {
                    this._InputControl.Text = "";
                }
            }
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// Dropbown Control to Hold all Interval Values
        /// </summary>
        private DropDownList _DropDownList = new DropDownList();

        /// <summary>
        /// TextBox control to hold the Entered Text value.
        /// </summary>
        private TextBox _InputControl = new TextBox();

        /// <summary>
        /// CheckBox control for "none."
        /// </summary>
        private CheckBox _None = new CheckBox();

        /// <summary>
        /// Label Control to hold Text Before Input Controls
        /// </summary>
        private Label _BeforeInput = new Label();

        /// <summary>
        /// Label Control to hold Text After Input Controls
        /// </summary>
        private Label _AfterInput = new Label();
      
        #endregion

        #region Private Methods
        #region _BuildControls
        /// <summary>
        /// Builds the text box input, Dropdown, Checkbox and label controls.
        /// </summary>
        private void _BuildControls()
        {
            //Add Before Text
            this._BeforeInput.ID = this.ID + "_BeforeInput";
            this.Controls.Add(this._BeforeInput);


            // build the input control
            this._InputControl.ID = this.ID + "_DateInputControl";
            this._InputControl.Attributes.Add("style", "width: 40px;margin:0px 5px 0px 10px");
            this.Controls.Add(this._InputControl);

            // build the DropDownSelection control
            this._DropDownList.ID = this.ID + "_DropDownList";
            this._DropDownList.Items.Add(new ListItem("- " + Asentia.Common._GlobalResources.Select, "0"));
            this._DropDownList.Items.Add(new ListItem(Asentia.Common._GlobalResources.Year_s, "yy"));
            this._DropDownList.Items.Add(new ListItem(Asentia.Common._GlobalResources.Month_s,"mm"));
            this._DropDownList.Items.Add(new ListItem(Asentia.Common._GlobalResources.Week_s, "ww"));
            this._DropDownList.Items.Add(new ListItem(Asentia.Common._GlobalResources.Day_s, "dd"));
            this._DropDownList.Attributes.Add("style", "margin:0px 10px 0px 5px");
            this.Controls.Add(this._DropDownList);
            //Add After Text
            this._AfterInput.ID = this.ID + "_AfterInput";
            this._AfterInput.Attributes.Add("style", "padding-right: 10px;");
            this.Controls.Add(this._AfterInput);

            // build the use none checkbox if we need to build it
            if (this.UseNone)
            {
                this._None.ID = this.ID + "NoneCheckbox";
                this.Controls.Add(this._None);
            }
        }
        #endregion

        #region _BuildJSStartUpScript
        /// <summary>
        /// Builds the start up javascript to apply the jQuery Checkbox Enable disable Input controls function to the
        /// controls. This gets fired OnInit.
        /// </summary>
        private void _BuildJSStartUpScript()
        {
           

            // build the javascript using a StringBuilder
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" $(document).ready(function () {");

            // ACTIONS FOR "NONE" CONTROL
            if (this.UseNone)
            {
                sb.AppendLine("     if ($(\"#" + this._None.ID + "\").is(\":checked\")) {");
                sb.AppendLine("         $(\"#" + this._InputControl.ID + "\").prop(\"disabled\", true);");
                sb.AppendLine("         $(\"#" + this._DropDownList.ID + "\").prop(\"disabled\", true);");
                sb.AppendLine("     }");

                sb.AppendLine("     $(\"#" + this._None.ID + "\").click(function () {");
                sb.AppendLine("         if ($(\"#" + this._None.ID + "\").is(\":checked\")) {");
                sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").prop(\"disabled\", true);");
                sb.AppendLine("             $(\"#" + this._DropDownList.ID + "\").prop(\"disabled\", true);");
                sb.AppendLine("             $(\"#" + this._DropDownList.ID + "\").prop('selectedIndex', 0);");
                sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").val(\"\");");
                sb.AppendLine("         }");
                sb.AppendLine("         else {");
                sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").prop(\"disabled\", false);");
                sb.AppendLine("             $(\"#" + this._DropDownList.ID + "\").prop(\"disabled\", false);");
                
                sb.AppendLine("         }");
                sb.AppendLine("     });");
            }
            sb.AppendLine(" });");

            // attach the script as a startup script
            ScriptManager.RegisterStartupScript(this, typeof(Asentia.Controls.ClientScript), this.ID, sb.ToString(), true);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnInit
        protected override void OnInit(EventArgs e)
        {
            // execute base
            base.OnInit(e);

            // build js start up script
            this._BuildJSStartUpScript();
        }
        #endregion
        #endregion
    }
}
