﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    public class GridAnalytic : WebControl
    {
        #region Constructors
        /// <summary>
        /// Constructor for 1 column analytic.
        /// </summary>
        /// <param name="id">identifier</param>
        /// <param name="column1Title">column 1 title</param>
        /// <param name="column1DataBlocks">column 1 data</param>
        public GridAnalytic(string id, DataBlock column1Title, List<DataBlock> column1DataBlocks)
        {
            this.ID = id;
            this._NumberOfColumns = 1;

            this._Column1Title = column1Title;
            this._Column1DataBlocks = column1DataBlocks;

            this._Column2Title = null;
            this._Column2DataBlocks = null;

            this._Column3Title = null;
            this._Column3DataBlocks = null;

            this._Column4Title = null;
            this._Column4DataBlocks = null;
        }

        /// <summary>
        /// Constructor for 2 column analytic.
        /// </summary>
        /// <param name="id">identifier</param>
        /// <param name="column1Title">column 1 title</param>
        /// <param name="column1DataBlocks">column 1 data</param>
        /// <param name="column2Title">column 2 title</param>
        /// <param name="column2DataBlocks">column 2 data</param>
        public GridAnalytic(string id, DataBlock column1Title, List<DataBlock> column1DataBlocks, DataBlock column2Title, List<DataBlock> column2DataBlocks)
        {
            this.ID = id;
            this._NumberOfColumns = 2;

            this._Column1Title = column1Title;
            this._Column1DataBlocks = column1DataBlocks;

            this._Column2Title = column2Title;
            this._Column2DataBlocks = column2DataBlocks;

            this._Column3Title = null;
            this._Column3DataBlocks = null;

            this._Column4Title = null;
            this._Column4DataBlocks = null;
        }

        /// <summary>
        /// Constructor for 3 column analytic.
        /// </summary>
        /// <param name="id">identifier</param>
        /// <param name="column1Title">column 1 title</param>
        /// <param name="column1DataBlocks">column 1 data</param>
        /// <param name="column2Title">column 2 title</param>
        /// <param name="column2DataBlocks">column 2 data</param>
        /// <param name="column3Title">column 3 title</param>
        /// <param name="column3DataBlocks">column 3 data</param>
        public GridAnalytic(string id, DataBlock column1Title, List<DataBlock> column1DataBlocks, DataBlock column2Title, List<DataBlock> column2DataBlocks, DataBlock column3Title, List<DataBlock> column3DataBlocks)
        {
            this.ID = id;
            this._NumberOfColumns = 3;

            this._Column1Title = column1Title;
            this._Column1DataBlocks = column1DataBlocks;

            this._Column2Title = column2Title;
            this._Column2DataBlocks = column2DataBlocks;

            this._Column3Title = column3Title;
            this._Column3DataBlocks = column3DataBlocks;

            this._Column4Title = null;
            this._Column4DataBlocks = null;
        }

        /// <summary>
        /// Constructor for 4 column analytic.
        /// </summary>
        /// <param name="id">identifier</param>
        /// <param name="column1Title">column 1 title</param>
        /// <param name="column1DataBlocks">column 1 data</param>
        /// <param name="column2Title">column 2 title</param>
        /// <param name="column2DataBlocks">column 2 data</param>
        /// <param name="column3Title">column 3 title</param>
        /// <param name="column3DataBlocks">column 3 data</param>
        /// <param name="column4Title">column 4 title</param>
        /// <param name="column4DataBlocks">column 4 data</param>
        public GridAnalytic(string id, DataBlock column1Title, List<DataBlock> column1DataBlocks, DataBlock column2Title, List<DataBlock> column2DataBlocks, DataBlock column3Title, List<DataBlock> column3DataBlocks, DataBlock column4Title, List<DataBlock> column4DataBlocks)
        {
            this.ID = id;
            this._NumberOfColumns = 4;

            this._Column1Title = column1Title;
            this._Column1DataBlocks = column1DataBlocks;

            this._Column2Title = column2Title;
            this._Column2DataBlocks = column2DataBlocks;

            this._Column3Title = column3Title;
            this._Column3DataBlocks = column3DataBlocks;

            this._Column4Title = column4Title;
            this._Column4DataBlocks = column4DataBlocks;
        }
        #endregion

        #region Properties
        #endregion

        #region Private Properties
        private int _NumberOfColumns = 1;

        DataBlock _Column1Title;
        List<DataBlock> _Column1DataBlocks;

        DataBlock _Column2Title;
        List<DataBlock> _Column2DataBlocks;

        DataBlock _Column3Title;
        List<DataBlock> _Column3DataBlocks;

        DataBlock _Column4Title;
        List<DataBlock> _Column4DataBlocks;
        #endregion

        #region Classes
        /// <summary>
        /// Class to represent a "block" (single analytic) of data, i.e. ## users.
        /// 
        /// This also includes properties for making the block's label a link, in the
        /// event a "click action" were to be applied to the data block.
        /// </summary>
        public class DataBlock
        {
            public string Data;
            public string Label;
            public string Href;
            public bool IsOnClickAction;
            public string OnClickAction;

            public DataBlock(string data, string label)
            {
                this.Data = data;
                this.Label = label;
            }

            public DataBlock(string data, string label, string href)
            {
                this.Data = data;
                this.Label = label;
                this.Href = href;
            }

            public DataBlock(string data, string label, bool isOnClickAction, string action)
            {
                this.Data = data;
                this.Label = label;
                this.IsOnClickAction = isOnClickAction;

                if (this.IsOnClickAction)
                { this.OnClickAction = action; }
                else
                { this.Href = action; }
            }
        }
        #endregion        

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        #endregion

        #region Render
        /// <summary>
        /// This is overridden so that the control doesn't render inside of a <span> tag.
        /// Only the contents of the control are rendered.
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            this.RenderContents(writer);
        }
        #endregion

        #region RenderContents
        protected override void RenderContents(HtmlTextWriter writer)
        {            
            // grid analytic container
            Panel gridAnalyticContainer = new Panel();
            gridAnalyticContainer.ID = this.ID + "GridAnalyticContainer";
            gridAnalyticContainer.CssClass = "GridAnalyticContainer" + this._NumberOfColumns.ToString() + "Column";

            // column 1
            if (this._Column1Title != null && this._Column1DataBlocks != null)
            {
                // column container
                Panel gridAnalyticColumn1 = new Panel();
                gridAnalyticColumn1.ID = this.ID + "GridAnalyticColumn1";
                gridAnalyticColumn1.CssClass = "GridAnalyticColumn";
                gridAnalyticContainer.Controls.Add(gridAnalyticColumn1);

                // analytic wrapper container
                Panel gridAnalytic1WrapperContainer = new Panel();
                gridAnalytic1WrapperContainer.ID = this.ID + "GridAnalytic1WrapperContainer";
                gridAnalytic1WrapperContainer.CssClass = "GridAnalyticWrapperContainer";
                gridAnalyticColumn1.Controls.Add(gridAnalytic1WrapperContainer);

                // analytic container
                Panel gridAnalytic1Container = new Panel();
                gridAnalytic1Container.ID = this.ID + "GridAnalytic1Container";
                gridAnalytic1Container.CssClass = "GridAnalyticContainer";
                gridAnalytic1WrapperContainer.Controls.Add(gridAnalytic1Container);

                // analytic inner container
                Panel gridAnalytic1InnerContainer = new Panel();
                gridAnalytic1InnerContainer.ID = this.ID + "GridAnalytic1InnerContainer";
                gridAnalytic1InnerContainer.CssClass = "GridAnalyticContainerInner";
                gridAnalytic1Container.Controls.Add(gridAnalytic1InnerContainer);

                // analytic title container
                Panel gridAnalytic1TitleContainer = new Panel();
                gridAnalytic1TitleContainer.ID = this.ID + "GridAnalytic1TitleContainer";
                gridAnalytic1TitleContainer.CssClass = "GridAnalyticTitleContainer";
                gridAnalytic1InnerContainer.Controls.Add(gridAnalytic1TitleContainer);

                // analytic title
                Panel gridAnalytic1Title = new Panel();
                gridAnalytic1Title.ID = this.ID + "GridAnalytic1Title";
                gridAnalytic1Title.CssClass = "GridAnalyticTitle";
                gridAnalytic1TitleContainer.Controls.Add(gridAnalytic1Title);

                // if the title should be a link, make it a link, otherwise make it static text
                if (!String.IsNullOrWhiteSpace(this._Column1Title.Href) || !String.IsNullOrWhiteSpace(this._Column1Title.OnClickAction))
                {
                    // label link
                    HyperLink gridAnalytic1TitleLink = new HyperLink();
                    gridAnalytic1TitleLink.Text = this._Column1Title.Label;

                    if (this._Column1Title.IsOnClickAction && !String.IsNullOrWhiteSpace(this._Column1Title.OnClickAction))
                    {                        
                        gridAnalytic1TitleLink.NavigateUrl = "javascript: void(0);";
                        gridAnalytic1TitleLink.Attributes.Add("onclick", this._Column1Title.OnClickAction);
                    }
                    else
                    {
                        gridAnalytic1TitleLink.NavigateUrl = this._Column1Title.Href;
                    }

                    gridAnalytic1Title.Controls.Add(gridAnalytic1TitleLink);

                    // data
                    if (!String.IsNullOrWhiteSpace(this._Column1Title.Data))
                    {
                        Literal gridAnalytic1TitleText = new Literal();
                        gridAnalytic1TitleText.Text = " (" + this._Column1Title.Data + ")";
                        gridAnalytic1Title.Controls.Add(gridAnalytic1TitleText);
                    }
                }
                else
                {
                    // label and data
                    Literal gridAnalytic1TitleText = new Literal();
                    gridAnalytic1TitleText.Text = this._Column1Title.Label;

                    if (!String.IsNullOrWhiteSpace(this._Column1Title.Data))
                    { gridAnalytic1TitleText.Text += " (" + this._Column1Title.Data + ")"; }

                    gridAnalytic1Title.Controls.Add(gridAnalytic1TitleText);
                }

                // blocks
                int i = 1;

                foreach (DataBlock blockDataItem in this._Column1DataBlocks)
                {
                    // block
                    Panel gridAnalyticBlock1 = new Panel();
                    gridAnalyticBlock1.ID = this.ID + "GridAnalyticColumn1AnalyticBlock" + i.ToString();
                    gridAnalyticBlock1.CssClass = "GridAnalyticBlock";
                    gridAnalytic1InnerContainer.Controls.Add(gridAnalyticBlock1);

                    // data
                    Panel gridAnalyticBlockData1 = new Panel();
                    gridAnalyticBlockData1.CssClass = "GridAnalyticBlockData";
                    gridAnalyticBlock1.Controls.Add(gridAnalyticBlockData1);

                    Literal gridAnalyticBlockData1Text = new Literal();
                    gridAnalyticBlockData1Text.Text = blockDataItem.Data;
                    gridAnalyticBlockData1.Controls.Add(gridAnalyticBlockData1Text);

                    // hyperlink (if applicable)
                    HyperLink gridAnalyticBlockData1Link = new HyperLink();

                    if (!String.IsNullOrWhiteSpace(blockDataItem.Href) || !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                    {
                        if (blockDataItem.IsOnClickAction && !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                        {
                            gridAnalyticBlockData1Link.NavigateUrl = "javascript: void(0);";
                            gridAnalyticBlockData1Link.Attributes.Add("onclick", blockDataItem.OnClickAction);
                        }
                        else
                        {
                            gridAnalyticBlockData1Link.NavigateUrl = blockDataItem.Href;
                        }

                        gridAnalyticBlock1.Controls.Add(gridAnalyticBlockData1Link);
                    }

                    // label
                    Panel gridAnalyticBlockLabel1 = new Panel();
                    gridAnalyticBlockLabel1.CssClass = "GridAnalyticBlockLabel";                    

                    Literal gridAnalyticBlockLabel1Text = new Literal();
                    gridAnalyticBlockLabel1Text.Text = blockDataItem.Label;
                    gridAnalyticBlockLabel1.Controls.Add(gridAnalyticBlockLabel1Text);

                    // add the label to the block hyperlink or the block
                    if (!String.IsNullOrWhiteSpace(blockDataItem.Href) || !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                    { gridAnalyticBlockData1Link.Controls.Add(gridAnalyticBlockLabel1); }
                    else
                    { gridAnalyticBlock1.Controls.Add(gridAnalyticBlockLabel1); }

                    i++;
                }
            }

            // column 2
            if (this._Column2Title != null && this._Column2DataBlocks != null)
            {
                // column container
                Panel gridAnalyticColumn2 = new Panel();
                gridAnalyticColumn2.ID = this.ID + "GridAnalyticColumn2";
                gridAnalyticColumn2.CssClass = "GridAnalyticColumn";
                gridAnalyticContainer.Controls.Add(gridAnalyticColumn2);

                // analytic wrapper container
                Panel gridAnalytic2WrapperContainer = new Panel();
                gridAnalytic2WrapperContainer.ID = this.ID + "GridAnalytic2WrapperContainer";
                gridAnalytic2WrapperContainer.CssClass = "GridAnalyticWrapperContainer";
                gridAnalyticColumn2.Controls.Add(gridAnalytic2WrapperContainer);

                // analytic container
                Panel gridAnalytic2Container = new Panel();
                gridAnalytic2Container.ID = this.ID + "GridAnalytic2Container";
                gridAnalytic2Container.CssClass = "GridAnalyticContainer";
                gridAnalytic2WrapperContainer.Controls.Add(gridAnalytic2Container);

                // analytic inner container
                Panel gridAnalytic2InnerContainer = new Panel();
                gridAnalytic2InnerContainer.ID = this.ID + "GridAnalytic2InnerContainer";
                gridAnalytic2InnerContainer.CssClass = "GridAnalyticContainerInner";
                gridAnalytic2Container.Controls.Add(gridAnalytic2InnerContainer);

                // analytic title container
                Panel gridAnalytic2TitleContainer = new Panel();
                gridAnalytic2TitleContainer.ID = this.ID + "GridAnalytic2TitleContainer";
                gridAnalytic2TitleContainer.CssClass = "GridAnalyticTitleContainer";
                gridAnalytic2InnerContainer.Controls.Add(gridAnalytic2TitleContainer);

                // analytic title
                Panel gridAnalytic2Title = new Panel();
                gridAnalytic2Title.ID = this.ID + "GridAnalytic2Title";
                gridAnalytic2Title.CssClass = "GridAnalyticTitle";
                gridAnalytic2TitleContainer.Controls.Add(gridAnalytic2Title);

                // if the title should be a link, make it a link, otherwise make it static text
                if (!String.IsNullOrWhiteSpace(this._Column2Title.Href) || !String.IsNullOrWhiteSpace(this._Column2Title.OnClickAction))
                {
                    // label link
                    HyperLink gridAnalytic2TitleLink = new HyperLink();
                    gridAnalytic2TitleLink.Text = this._Column2Title.Label;

                    if (this._Column2Title.IsOnClickAction && !String.IsNullOrWhiteSpace(this._Column2Title.OnClickAction))
                    {
                        gridAnalytic2TitleLink.NavigateUrl = "javascript: void(0);";
                        gridAnalytic2TitleLink.Attributes.Add("onclick", this._Column2Title.OnClickAction);
                    }
                    else
                    {
                        gridAnalytic2TitleLink.NavigateUrl = this._Column2Title.Href;
                    }

                    gridAnalytic2Title.Controls.Add(gridAnalytic2TitleLink);

                    // data
                    if (!String.IsNullOrWhiteSpace(this._Column2Title.Data))
                    {
                        Literal gridAnalytic2TitleText = new Literal();
                        gridAnalytic2TitleText.Text = " (" + this._Column2Title.Data + ")";
                        gridAnalytic2Title.Controls.Add(gridAnalytic2TitleText);
                    }
                }
                else
                {
                    // label and data
                    Literal gridAnalytic2TitleText = new Literal();
                    gridAnalytic2TitleText.Text = this._Column2Title.Label;

                    if (!String.IsNullOrWhiteSpace(this._Column2Title.Data))
                    { gridAnalytic2TitleText.Text += " (" + this._Column2Title.Data + ")"; }

                    gridAnalytic2Title.Controls.Add(gridAnalytic2TitleText);
                }

                // blocks
                int i = 1;

                foreach (DataBlock blockDataItem in this._Column2DataBlocks)
                {
                    // block
                    Panel gridAnalyticBlock2 = new Panel();
                    gridAnalyticBlock2.ID = this.ID + "GridAnalyticColumn2AnalyticBlock" + i.ToString();
                    gridAnalyticBlock2.CssClass = "GridAnalyticBlock";
                    gridAnalytic2InnerContainer.Controls.Add(gridAnalyticBlock2);

                    // data
                    Panel gridAnalyticBlockData2 = new Panel();
                    gridAnalyticBlockData2.CssClass = "GridAnalyticBlockData";
                    gridAnalyticBlock2.Controls.Add(gridAnalyticBlockData2);

                    Literal gridAnalyticBlockData2Text = new Literal();
                    gridAnalyticBlockData2Text.Text = blockDataItem.Data;
                    gridAnalyticBlockData2.Controls.Add(gridAnalyticBlockData2Text);

                    // hyperlink (if applicable)
                    HyperLink gridAnalyticBlockData2Link = new HyperLink();

                    if (!String.IsNullOrWhiteSpace(blockDataItem.Href) || !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                    {
                        if (blockDataItem.IsOnClickAction && !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                        {
                            gridAnalyticBlockData2Link.NavigateUrl = "javascript: void(0);";
                            gridAnalyticBlockData2Link.Attributes.Add("onclick", blockDataItem.OnClickAction);
                        }
                        else
                        {
                            gridAnalyticBlockData2Link.NavigateUrl = blockDataItem.Href;
                        }

                        gridAnalyticBlock2.Controls.Add(gridAnalyticBlockData2Link);
                    }

                    // label
                    Panel gridAnalyticBlockLabel2 = new Panel();
                    gridAnalyticBlockLabel2.CssClass = "GridAnalyticBlockLabel";                    

                    Literal gridAnalyticBlockLabel2Text = new Literal();
                    gridAnalyticBlockLabel2Text.Text = blockDataItem.Label;
                    gridAnalyticBlockLabel2.Controls.Add(gridAnalyticBlockLabel2Text);

                    // add the label to the block hyperlink or the block
                    if (!String.IsNullOrWhiteSpace(blockDataItem.Href) || !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                    { gridAnalyticBlockData2Link.Controls.Add(gridAnalyticBlockLabel2); }
                    else
                    { gridAnalyticBlock2.Controls.Add(gridAnalyticBlockLabel2); }

                    i++;
                }
            }

            // column 3
            if (this._Column3Title != null && this._Column3DataBlocks != null)
            {
                // column container
                Panel gridAnalyticColumn3 = new Panel();
                gridAnalyticColumn3.ID = this.ID + "GridAnalyticColumn3";
                gridAnalyticColumn3.CssClass = "GridAnalyticColumn";
                gridAnalyticContainer.Controls.Add(gridAnalyticColumn3);

                // analytic wrapper container
                Panel gridAnalytic3WrapperContainer = new Panel();
                gridAnalytic3WrapperContainer.ID = this.ID + "GridAnalytic3WrapperContainer";
                gridAnalytic3WrapperContainer.CssClass = "GridAnalyticWrapperContainer";
                gridAnalyticColumn3.Controls.Add(gridAnalytic3WrapperContainer);

                // analytic container
                Panel gridAnalytic3Container = new Panel();
                gridAnalytic3Container.ID = this.ID + "GridAnalytic3Container";
                gridAnalytic3Container.CssClass = "GridAnalyticContainer";
                gridAnalytic3WrapperContainer.Controls.Add(gridAnalytic3Container);

                // analytic inner container
                Panel gridAnalytic3InnerContainer = new Panel();
                gridAnalytic3InnerContainer.ID = this.ID + "GridAnalytic3InnerContainer";
                gridAnalytic3InnerContainer.CssClass = "GridAnalyticContainerInner";
                gridAnalytic3Container.Controls.Add(gridAnalytic3InnerContainer);

                // analytic title container
                Panel gridAnalytic3TitleContainer = new Panel();
                gridAnalytic3TitleContainer.ID = this.ID + "GridAnalytic3TitleContainer";
                gridAnalytic3TitleContainer.CssClass = "GridAnalyticTitleContainer";
                gridAnalytic3InnerContainer.Controls.Add(gridAnalytic3TitleContainer);

                // analytic title
                Panel gridAnalytic3Title = new Panel();
                gridAnalytic3Title.ID = this.ID + "GridAnalytic3Title";
                gridAnalytic3Title.CssClass = "GridAnalyticTitle";
                gridAnalytic3TitleContainer.Controls.Add(gridAnalytic3Title);

                // if the title should be a link, make it a link, otherwise make it static text
                if (!String.IsNullOrWhiteSpace(this._Column3Title.Href) || !String.IsNullOrWhiteSpace(this._Column3Title.OnClickAction))
                {
                    // label link
                    HyperLink gridAnalytic3TitleLink = new HyperLink();
                    gridAnalytic3TitleLink.Text = this._Column3Title.Label;

                    if (this._Column3Title.IsOnClickAction && !String.IsNullOrWhiteSpace(this._Column3Title.OnClickAction))
                    {
                        gridAnalytic3TitleLink.NavigateUrl = "javascript: void(0);";
                        gridAnalytic3TitleLink.Attributes.Add("onclick", this._Column3Title.OnClickAction);
                    }
                    else
                    {
                        gridAnalytic3TitleLink.NavigateUrl = this._Column3Title.Href;
                    }

                    gridAnalytic3Title.Controls.Add(gridAnalytic3TitleLink);

                    // data
                    if (!String.IsNullOrWhiteSpace(this._Column3Title.Data))
                    {
                        Literal gridAnalytic3TitleText = new Literal();
                        gridAnalytic3TitleText.Text = " (" + this._Column3Title.Data + ")";
                        gridAnalytic3Title.Controls.Add(gridAnalytic3TitleText);
                    }
                }
                else
                {
                    // label and data
                    Literal gridAnalytic3TitleText = new Literal();
                    gridAnalytic3TitleText.Text = this._Column3Title.Label;

                    if (!String.IsNullOrWhiteSpace(this._Column3Title.Data))
                    { gridAnalytic3TitleText.Text += " (" + this._Column3Title.Data + ")"; }

                    gridAnalytic3Title.Controls.Add(gridAnalytic3TitleText);
                }

                // blocks
                int i = 1;

                foreach (DataBlock blockDataItem in this._Column3DataBlocks)
                {
                    // block
                    Panel gridAnalyticBlock3 = new Panel();
                    gridAnalyticBlock3.ID = this.ID + "GridAnalyticColumn3AnalyticBlock" + i.ToString();
                    gridAnalyticBlock3.CssClass = "GridAnalyticBlock";
                    gridAnalytic3InnerContainer.Controls.Add(gridAnalyticBlock3);

                    // data
                    Panel gridAnalyticBlockData3 = new Panel();
                    gridAnalyticBlockData3.CssClass = "GridAnalyticBlockData";
                    gridAnalyticBlock3.Controls.Add(gridAnalyticBlockData3);

                    Literal gridAnalyticBlockData3Text = new Literal();
                    gridAnalyticBlockData3Text.Text = blockDataItem.Data;
                    gridAnalyticBlockData3.Controls.Add(gridAnalyticBlockData3Text);

                    // hyperlink (if applicable)
                    HyperLink gridAnalyticBlockData3Link = new HyperLink();

                    if (!String.IsNullOrWhiteSpace(blockDataItem.Href) || !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                    {
                        if (blockDataItem.IsOnClickAction && !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                        {
                            gridAnalyticBlockData3Link.NavigateUrl = "javascript: void(0);";
                            gridAnalyticBlockData3Link.Attributes.Add("onclick", blockDataItem.OnClickAction);
                        }
                        else
                        {
                            gridAnalyticBlockData3Link.NavigateUrl = blockDataItem.Href;
                        }

                        gridAnalyticBlock3.Controls.Add(gridAnalyticBlockData3Link);
                    }

                    // label
                    Panel gridAnalyticBlockLabel3 = new Panel();
                    gridAnalyticBlockLabel3.CssClass = "GridAnalyticBlockLabel";                    

                    Literal gridAnalyticBlockLabel3Text = new Literal();
                    gridAnalyticBlockLabel3Text.Text = blockDataItem.Label;
                    gridAnalyticBlockLabel3.Controls.Add(gridAnalyticBlockLabel3Text);

                    // add the label to the block hyperlink or the block
                    if (!String.IsNullOrWhiteSpace(blockDataItem.Href) || !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                    { gridAnalyticBlockData3Link.Controls.Add(gridAnalyticBlockLabel3); }
                    else
                    { gridAnalyticBlock3.Controls.Add(gridAnalyticBlockLabel3); }

                    i++;
                }
            }

            // column 4
            if (this._Column4Title != null && this._Column4DataBlocks != null)
            {
                // column container
                Panel gridAnalyticColumn4 = new Panel();
                gridAnalyticColumn4.ID = this.ID + "GridAnalyticColumn4";
                gridAnalyticColumn4.CssClass = "GridAnalyticColumn";
                gridAnalyticContainer.Controls.Add(gridAnalyticColumn4);

                // analytic wrapper container
                Panel gridAnalytic4WrapperContainer = new Panel();
                gridAnalytic4WrapperContainer.ID = this.ID + "GridAnalytic4WrapperContainer";
                gridAnalytic4WrapperContainer.CssClass = "GridAnalyticWrapperContainer";
                gridAnalyticColumn4.Controls.Add(gridAnalytic4WrapperContainer);

                // analytic container
                Panel gridAnalytic4Container = new Panel();
                gridAnalytic4Container.ID = this.ID + "GridAnalytic4Container";
                gridAnalytic4Container.CssClass = "GridAnalyticContainer";
                gridAnalytic4WrapperContainer.Controls.Add(gridAnalytic4Container);

                // analytic inner container
                Panel gridAnalytic4InnerContainer = new Panel();
                gridAnalytic4InnerContainer.ID = this.ID + "GridAnalytic4InnerContainer";
                gridAnalytic4InnerContainer.CssClass = "GridAnalyticContainerInner";
                gridAnalytic4Container.Controls.Add(gridAnalytic4InnerContainer);

                // analytic title container
                Panel gridAnalytic4TitleContainer = new Panel();
                gridAnalytic4TitleContainer.ID = this.ID + "GridAnalytic4TitleContainer";
                gridAnalytic4TitleContainer.CssClass = "GridAnalyticTitleContainer";
                gridAnalytic4InnerContainer.Controls.Add(gridAnalytic4TitleContainer);

                // analytic title
                Panel gridAnalytic4Title = new Panel();
                gridAnalytic4Title.ID = this.ID + "GridAnalytic4Title";
                gridAnalytic4Title.CssClass = "GridAnalyticTitle";
                gridAnalytic4TitleContainer.Controls.Add(gridAnalytic4Title);

                // if the title should be a link, make it a link, otherwise make it static text
                if (!String.IsNullOrWhiteSpace(this._Column4Title.Href) || !String.IsNullOrWhiteSpace(this._Column4Title.OnClickAction))
                {
                    // label link
                    HyperLink gridAnalytic4TitleLink = new HyperLink();
                    gridAnalytic4TitleLink.Text = this._Column4Title.Label;

                    if (this._Column4Title.IsOnClickAction && !String.IsNullOrWhiteSpace(this._Column4Title.OnClickAction))
                    {
                        gridAnalytic4TitleLink.NavigateUrl = "javascript: void(0);";
                        gridAnalytic4TitleLink.Attributes.Add("onclick", this._Column4Title.OnClickAction);
                    }
                    else
                    {
                        gridAnalytic4TitleLink.NavigateUrl = this._Column4Title.Href;
                    }

                    gridAnalytic4Title.Controls.Add(gridAnalytic4TitleLink);

                    // data
                    if (!String.IsNullOrWhiteSpace(this._Column4Title.Data))
                    {
                        Literal gridAnalytic4TitleText = new Literal();
                        gridAnalytic4TitleText.Text = " (" + this._Column4Title.Data + ")";
                        gridAnalytic4Title.Controls.Add(gridAnalytic4TitleText);
                    }
                }
                else
                {
                    // label and data
                    Literal gridAnalytic4TitleText = new Literal();
                    gridAnalytic4TitleText.Text = this._Column4Title.Label;

                    if (!String.IsNullOrWhiteSpace(this._Column4Title.Data))
                    { gridAnalytic4TitleText.Text += " (" + this._Column4Title.Data + ")"; }

                    gridAnalytic4Title.Controls.Add(gridAnalytic4TitleText);
                }

                // blocks
                int i = 1;

                foreach (DataBlock blockDataItem in this._Column4DataBlocks)
                {
                    // block
                    Panel gridAnalyticBlock4 = new Panel();
                    gridAnalyticBlock4.ID = this.ID + "GridAnalyticColumn4AnalyticBlock" + i.ToString();
                    gridAnalyticBlock4.CssClass = "GridAnalyticBlock";
                    gridAnalytic4InnerContainer.Controls.Add(gridAnalyticBlock4);

                    // data
                    Panel gridAnalyticBlockData4 = new Panel();
                    gridAnalyticBlockData4.CssClass = "GridAnalyticBlockData";
                    gridAnalyticBlock4.Controls.Add(gridAnalyticBlockData4);

                    Literal gridAnalyticBlockData4Text = new Literal();
                    gridAnalyticBlockData4Text.Text = blockDataItem.Data;
                    gridAnalyticBlockData4.Controls.Add(gridAnalyticBlockData4Text);

                    // hyperlink (if applicable)
                    HyperLink gridAnalyticBlockData4Link = new HyperLink();

                    if (!String.IsNullOrWhiteSpace(blockDataItem.Href) || !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                    {
                        if (blockDataItem.IsOnClickAction && !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                        {
                            gridAnalyticBlockData4Link.NavigateUrl = "javascript: void(0);";
                            gridAnalyticBlockData4Link.Attributes.Add("onclick", blockDataItem.OnClickAction);
                        }
                        else
                        {
                            gridAnalyticBlockData4Link.NavigateUrl = blockDataItem.Href;
                        }

                        gridAnalyticBlock4.Controls.Add(gridAnalyticBlockData4Link);
                    }

                    // label
                    Panel gridAnalyticBlockLabel4 = new Panel();
                    gridAnalyticBlockLabel4.CssClass = "GridAnalyticBlockLabel";                    

                    Literal gridAnalyticBlockLabel4Text = new Literal();
                    gridAnalyticBlockLabel4Text.Text = blockDataItem.Label;
                    gridAnalyticBlockLabel4.Controls.Add(gridAnalyticBlockLabel4Text);

                    // add the label to the block hyperlink or the block
                    if (!String.IsNullOrWhiteSpace(blockDataItem.Href) || !String.IsNullOrWhiteSpace(blockDataItem.OnClickAction))
                    { gridAnalyticBlockData4Link.Controls.Add(gridAnalyticBlockLabel4); }
                    else
                    { gridAnalyticBlock4.Controls.Add(gridAnalyticBlockLabel4); }

                    i++;
                }
            }            

            // attach grid analytic container to control
            this.Controls.Add(gridAnalyticContainer);

            // execute base
            base.RenderContents(writer);
        }
        #endregion
        #endregion
    }
}
