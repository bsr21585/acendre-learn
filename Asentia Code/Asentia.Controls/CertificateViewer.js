﻿var droppableOffset;
var init = true;

$(function () {
    init = false;
    Sys.Application.add_load(initialize);
});

$(window).load(function () {
    init = true;
    initialize();
});

function initialize() {
    if (!init) {
        return;
    }

    droppableOffset = $('#Certificate').offset();
    
    var hiddenHtml = $('#HiddenCertificateViewerHtml').val();
    if (hiddenHtml != '') {
        var decodedHtml = $('<div />').html(hiddenHtml).text();
        $('#Certificate').html(decodedHtml);
    }
    
    adjustPosition();
}

//Adjusts position of each draggable item
function adjustPosition() {
    $(".Draggable").each(function (index, item) {
        draggableLeft = parseInt($(item).css('left').replace("px", ""));
        draggableTop = parseInt($(item).css('top').replace("px", ""));
        $(item).css('left', (draggableLeft + droppableOffset.left) + "px");
        $(item).css('top', (draggableTop + droppableOffset.top) + "px");
    });
}