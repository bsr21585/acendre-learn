﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Asentia.Common;
using AjaxControlToolkit;

namespace Asentia.Controls
{
    public class AsentiaMasterPage : MasterPage
    {
        #region Properties
        public HtmlForm MasterForm;
        public static Panel PageContainer;
        public Literal GlobalJavascriptVariables;
        public ContentPlaceHolder HeadPlaceholder;

        // masthead elements
        public Panel MastheadContainer;
        public Panel MastheadColumnLeft;
        public Panel MastheadColumnRight;

        // content elements
        public Panel ContentContainer;
        public Panel MasterContentLeft;
        public Panel MasterContentRight;
        public Panel MasterContentMiddle;

        // breadcrumb elements
        public Panel BreadcrumbContainer;
        public Panel BreadcrumbColumnLeft;
        public Panel BreadcrumbColumnRight;

        // page content placeholders
        public ContentPlaceHolder BreadcrumbContentPlaceholder;
        public ContentPlaceHolder SideContentPlaceholder1;
        public ContentPlaceHolder PageContentPlaceholder;
        public ContentPlaceHolder SideContentPlaceholder2;

        // footer elements
        public Panel FooterContainer;
        public Panel FooterColumnLeft;
        public Panel FooterColumnRight;

        public ToolkitScriptManager asm;

        // login button id
        public static string LogInButtonId = null;
        
        // log in button control exists
        public static bool LogInButtonControlExists = false;
        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AsentiaMasterPage()
        {}
        #endregion

        #region Private Properties
        // dictionary of controls to attach and reference to associated method
        // to add a control, add its name and associated method to the dictionary
        // in "OnInit", then add the method to this class
        private Dictionary<string, Action<XmlNode, Panel>> _ControlMethods = new Dictionary<string, Action<XmlNode, Panel>>();

        // parameter that gets set to true if the master page instansiating this class is HomePage.master
        // if true, a different masthead will be loaded
        private bool _IsHomePage = false;
        #endregion

        #region Methods
        #region Page_Load
        /// <summary>
        /// Page load.
        /// </summary>
        /// <param name="sender">Page</param>
        /// <param name="e">Event arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // give the master page an ID so one is not auto-generated
            this.ID = "Master";

            // call functions to build page
            this._MastheadBackground();
            this._AttachControlsToContainers();

            if (!Page.IsPostBack)
            { }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _AttachControlsToContainers
        /// <summary>
        /// Method to load the controls template and build containers with the control elements.
        /// </summary>
        private void _AttachControlsToContainers()
        {
            XmlDocument globalControlsDoc = new XmlDocument();
            string globalControlsFileName = "GlobalControls.xml";

            // if the instansiating page is the home page master page, set the global controls
            // file name to the one for the home page
            if (this._IsHomePage)
            { globalControlsFileName = "HomePageControls.xml"; }

            // if site is not the default site, attempt to load its controls file
            // if that doesn't exist, load the default; if neither exist, throw an exception
            if (AsentiaSessionState.SiteHostname != "default")
            {
                string pathToGlobalControlsFile = MapPathSecure(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + globalControlsFileName);

                if (File.Exists(pathToGlobalControlsFile))
                {
                    globalControlsDoc.Load(MapPathSecure(pathToGlobalControlsFile));
                }
                else
                {
                    pathToGlobalControlsFile = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_MASTERPAGE + globalControlsFileName);

                    if (File.Exists(pathToGlobalControlsFile))
                    {
                        globalControlsDoc.Load(MapPathSecure(pathToGlobalControlsFile));
                    }
                    else
                    {
                        throw new AsentiaException(_GlobalResources.CouldNotLoadDefaultGlobalControlsTemplate);
                    }
                }
            }
            // if the site is the default site, attempt to load the default controls file
            // if it doesn't exist, throw an exception
            else
            {
                if (globalControlsFileName != null)
                {
                    string pathToGlobalControlsFile = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_MASTERPAGE + globalControlsFileName);

                    if (File.Exists(pathToGlobalControlsFile))
                    {
                        globalControlsDoc.Load(MapPathSecure(pathToGlobalControlsFile));
                    }
                    else
                    {
                        throw new AsentiaException(_GlobalResources.CouldNotLoadDefaultGlobalControlsTemplate);
                    }
                }
            }

            // get the container elements from the file
            XmlNodeList containerNodes = globalControlsDoc.GetElementsByTagName("container");

            // loop through the containers and build each one
            foreach (XmlNode containerNode in containerNodes)
            { _BuildContainer(containerNode.Attributes["id"].Value, containerNode); }
        }
        #endregion

        #region _BuildContainer
        // method to build the containers with control elements
        private void _BuildContainer(string containerId, XmlNode containerNode)
        {
            // initialize panel elements
            Panel containerPanel = new Panel();
            Panel containerColumnLeftPanel = new Panel();
            Panel containerColumnRightPanel = new Panel();

            // set the panel elements to the panel elements of the 
            // current container being processed
            switch (containerId)
            {
                case "MastheadContainer":
                    containerPanel = MastheadContainer;
                    containerColumnLeftPanel = MastheadColumnLeft;
                    containerColumnRightPanel = MastheadColumnRight;
                    break;
                case "BreadcrumbContainer":
                    containerPanel = BreadcrumbContainer;
                    containerColumnLeftPanel = BreadcrumbColumnLeft;
                    containerColumnRightPanel = BreadcrumbColumnRight;
                    break;
                case "ContentContainer":
                    containerPanel = ContentContainer;
                    break;
                case "FooterContainer":
                    containerPanel = FooterContainer;
                    containerColumnLeftPanel = FooterColumnLeft;
                    containerColumnRightPanel = FooterColumnRight;
                    break;
                default:
                    break;
            }

            // get the container's elements
            XmlNodeList containerNodeElements = containerNode.ChildNodes;

            // loop through each element node, and call the element specific
            // method to build and attach the element
            foreach (XmlNode containerNodeElement in containerNodeElements)
            {
                if (containerNodeElement.Name == "element")
                {
                    // get the element's type and column placement
                    string elementType = containerNodeElement.Attributes["type"].Value;
                    string elementColumnPlacement = containerNodeElement.Attributes["columnPlacement"].Value;

                    if (!String.IsNullOrWhiteSpace(elementType))
                    {
                        // background color and background image are applied to the "outer" container
                        // so do the processing for those right here
                        if (elementType == "BackgroundColor")
                        {
                            containerPanel.Style.Add(HtmlTextWriterStyle.BackgroundColor, containerNodeElement.InnerText);
                        }
                        else if (elementType == "BackgroundImage")
                        {
                            // get the file name of the background image
                            string bgImageFileName = containerNodeElement.InnerText;

                            // if the site is not the default site, look for the image in the site's
                            // config directory; if it's not found there, look for it in the default
                            // site's config directory; if neither are found, dont apply the image
                            if (AsentiaSessionState.SiteHostname != "default")
                            {
                                if (!String.IsNullOrWhiteSpace(bgImageFileName))
                                {
                                    string pathToBgImage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + bgImageFileName);

                                    if (File.Exists(pathToBgImage))
                                    {
                                        containerPanel.Style.Add(
                                            HtmlTextWriterStyle.BackgroundImage,
                                            ResolveUrl(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + bgImageFileName)
                                        );
                                    }
                                    else
                                    {
                                        pathToBgImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + bgImageFileName);

                                        if (File.Exists(pathToBgImage))
                                        {
                                            containerPanel.Style.Add(
                                                HtmlTextWriterStyle.BackgroundImage,
                                                ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + bgImageFileName)
                                            );
                                        }
                                    }
                                }
                            }
                            // if the site is the default site, look for the image in the default site's
                            // config directory; if it's not found there, dont apply it
                            else
                            {
                                if (!String.IsNullOrWhiteSpace(bgImageFileName))
                                {
                                    string pathToBgImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + bgImageFileName);

                                    if (File.Exists(pathToBgImage))
                                    {
                                        containerPanel.Style.Add(
                                            HtmlTextWriterStyle.BackgroundImage,
                                            ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + bgImageFileName)
                                        );
                                    }
                                }
                            }
                        }
                        // process all other elements, and call their specific functions
                        // to build and attach
                        else
                        {
                            foreach (string key in _ControlMethods.Keys)
                            {
                                if (elementType == key)
                                {
                                    if (!String.IsNullOrWhiteSpace(elementColumnPlacement))
                                    {
                                        // call the build function of the element with the appropriate
                                        // column container passed
                                        switch (elementColumnPlacement)
                                        {
                                            case "left":
                                                this._ControlMethods[key].Invoke(containerNodeElement, containerColumnLeftPanel);
                                                break;
                                            case "right":
                                                this._ControlMethods[key].Invoke(containerNodeElement, containerColumnRightPanel);
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Control Builder Methods
        // Add builder methods for new controls here. 
        // Use the _GlobalControlTemplate method as a template.

        #region _GlobalControlTemplate
        /// <summary>
        /// This is used as a template method for building and attaching a control.
        /// When a new control type is added to the dictionary, its builder function
        /// should be based on this.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _GlobalControlTemplate(XmlNode elementNode, Panel container)
        {
            // id information
            string id = String.Empty;
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrWhiteSpace(specifiedId))
            { id = specifiedId; }

            // declare control and set its id
            Control control = new Control();
            control.ID = id;

            // build control


            // attach control to container
            container.Controls.Add(control);
        }
        #endregion

        #region _LanguageSelectorControl
        /// <summary>
        /// Control that displays a language selector.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _LanguageSelectorControl(XmlNode elementNode, Panel container)
        {
            if (!HttpContext.Current.Request.Url.ToString().Contains("/unknown/") && !HttpContext.Current.Request.Url.ToString().Contains("/offline/"))
            {
                // id information
                string id = "LanguageSelectorControl";
                string specifiedId = elementNode.Attributes["id"].Value;
                if (!String.IsNullOrWhiteSpace(specifiedId))
                { id = specifiedId; }

                Panel languageSelectorContainer = new Panel();
                languageSelectorContainer.ID = id;

                Image localizationImage = new Image();
                localizationImage.ID = id + "Image";
                localizationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM_LOCALIZATION_WHITE,
                                                                    ImageFiles.EXT_PNG);
                localizationImage.CssClass = "SmallIcon";
                localizationImage.AlternateText = _GlobalResources.LanguageSelector;
                languageSelectorContainer.Controls.Add(localizationImage);

                Panel languageSelectorMenuItemContainer = new Panel();
                languageSelectorMenuItemContainer.ID = "LanguageSelectorMenuItemContainer";

                ArrayList installedLanguages = new ArrayList();

                // en-US is the default language, so it's already installed, add it
                installedLanguages.Add("en-US");

                // loop through each installed language based on the language folders
                // contained in bin
                foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
                {
                    // loop through each language available to the site and add it
                    // to the array list for drop-down items
                    foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
                    {
                        if (language == Path.GetFileName(directory))
                        {
                            installedLanguages.Add(Path.GetFileName(directory));
                            break;
                        }
                    }
                }

                // loop through array list and add each language to the drop-down
                foreach (string installedLanguage in installedLanguages)
                {
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(installedLanguage);

                    Panel languageSelectorMenuItem = new Panel();
                    languageSelectorMenuItem.ID = "LanguageSelectorMenuItem_" + cultureInfo.Name;

                    LinkButton language = new LinkButton();
                    language.ID = "LanguageSelectorMenuItemLink_" + cultureInfo.Name;

                    if (AsentiaSessionState.UserCulture == cultureInfo.Name)
                    { language.CssClass = "bold"; }

                    if (cultureInfo.Name == "es-MX") // Mexican Spanish is "Latin America Spanish"
                    { language.Text = "español (América Latina)"; }
                    else
                    { language.Text = cultureInfo.NativeName; }
                    
                    language.Command += _LanguageSelector_Change_Session;
                    language.CommandArgument = cultureInfo.Name;

                    languageSelectorMenuItem.Controls.Add(language);
                    languageSelectorMenuItemContainer.Controls.Add(languageSelectorMenuItem);
                }

                languageSelectorContainer.Controls.Add(languageSelectorMenuItemContainer);

                container.Controls.Add(languageSelectorContainer);
            }
        }
        #endregion

        #region _LanguageSelector_Change_Session
        /// <summary>
        /// This is the handler method for the "Session" language menu.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">arguments</param>
        private void _LanguageSelector_Change_Session(object sender, CommandEventArgs e)
        {
            string language = (string)e.CommandArgument;

            // change the user culture
            AsentiaSessionState.UserCulture = language;

            // if this session is a logged-in user, not the admin, change the default language for the user
            if (AsentiaSessionState.IdSiteUser > 1)
            {
                // update the user's default language
                AsentiaDatabase databaseObject = new AsentiaDatabase();

                try
                {
                    databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                    databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                    databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                    databaseObject.AddParameter("@callerLangString", language, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                    databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                    databaseObject.ExecuteNonQuery("[User.SetDefaultLanguage]", true);

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    databaseObject.Dispose();
                }
            }

            // redirect back to the page with full url intact
            Context.Response.Redirect(Context.Request.RawUrl);
        }
        #endregion

        #region _MastheadBackground
        /// <summary>
        /// Applies masthead background-image and background-color styling to MastheadContainer if those site params exist.
        /// </summary>
        private void _MastheadBackground()
        {
            if (this._IsHomePage)
            {
                // masthead background image
                string backgroundImageFileName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_BGIMAGE);
                string backgroundImagePath = String.Empty;
                bool backgroundImageFileExists = false;

                if (!String.IsNullOrWhiteSpace(backgroundImageFileName))
                {
                    if (AsentiaSessionState.SiteHostname != "default")
                    {
                        if (backgroundImageFileName != null)
                        {
                            string pathToLogoImage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + backgroundImageFileName);

                            if (File.Exists(pathToLogoImage))
                            {
                                backgroundImageFileExists = true;
                                backgroundImagePath = ResolveUrl(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + backgroundImageFileName);
                            }
                            else
                            {
                                pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + backgroundImageFileName);

                                if (File.Exists(pathToLogoImage))
                                {
                                    backgroundImageFileExists = true;
                                    backgroundImagePath = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + backgroundImageFileName);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (backgroundImageFileName != null)
                        {
                            string pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + backgroundImageFileName);

                            if (File.Exists(pathToLogoImage))
                            {
                                backgroundImageFileExists = true;
                                backgroundImagePath = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + backgroundImageFileName);
                            }
                        }
                    }

                    // attach background-image style to masthead container
                    if (backgroundImageFileExists)
                    {
                        this.MastheadContainer.Style.Add("background-image", "url('" + backgroundImagePath + "')");
                        this.MastheadContainer.Style.Add("background-repeat", "repeat-x");
                    }
                }

                // masthead background color
                string backgroundColor = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_BGCOLOR);

                if (!String.IsNullOrWhiteSpace(backgroundColor))
                {
                    // attach background-color style to masthead container
                    this.MastheadContainer.Style.Add("background-color", backgroundColor);
                }
            }
            else
            {
                // masthead background color
                string backgroundColor = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.APPLICATION_MASTHEAD_BGCOLOR);

                if (!String.IsNullOrWhiteSpace(backgroundColor))
                {
                    // attach background-color style to masthead container
                    this.MastheadContainer.Style.Add("background", backgroundColor);
                }
            }
        }
        #endregion

        #region _HomePageLogoImage
        /// <summary>
        /// Home Page logo image control.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _HomePageLogoImage(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "HomePageLogoImage";
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrWhiteSpace(specifiedId))
            { id = specifiedId; }

            // declare control and set its id
            HyperLink logoImageHyperLink = new HyperLink();
            logoImageHyperLink.ID = id;
            logoImageHyperLink.NavigateUrl = "/";

            // build control
            string logoImageFileName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_LOGO);
            bool logoImageFileExists = false;

            if (!String.IsNullOrWhiteSpace(logoImageFileName))
            {
                if (AsentiaSessionState.SiteHostname != "default")
                {
                    if (logoImageFileName != null)
                    {
                        string pathToLogoImage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                        if (File.Exists(pathToLogoImage))
                        {
                            logoImageFileExists = true;
                            logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                        }
                        else
                        {
                            pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                            if (File.Exists(pathToLogoImage))
                            {
                                logoImageFileExists = true;
                                logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                            }
                        }
                    }
                }
                else
                {
                    if (logoImageFileName != null)
                    {
                        string pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                        if (File.Exists(pathToLogoImage))
                        {
                            logoImageFileExists = true;
                            logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                        }
                    }
                }
                logoImageHyperLink.Text = AsentiaSessionState.GlobalSiteObject.Title;

                // attach control to container
                if (logoImageFileExists)
                { container.Controls.Add(logoImageHyperLink); }
            }
        }
        #endregion

        #region _HomePageSecondaryImage
        /// <summary>
        /// Home Page secondary image control.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _HomePageSecondaryImage(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "HomePageSecondaryImage";
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrWhiteSpace(specifiedId))
            { id = specifiedId; }

            // declare control and set its id
            Image secondaryImage = new Image();
            secondaryImage.ID = id;

            // build control
            string secondaryImageFileName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.HOMEPAGE_MASTHEAD_SECONDARYIMAGE);
            bool secondaryImageFileExists = false;

            if (!String.IsNullOrWhiteSpace(secondaryImageFileName))
            {
                if (AsentiaSessionState.SiteHostname != "default")
                {
                    if (secondaryImageFileName != null)
                    {
                        string pathToSecondaryImage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);

                        if (File.Exists(pathToSecondaryImage))
                        {
                            secondaryImageFileExists = true;
                            secondaryImage.ImageUrl = ResolveUrl(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);
                        }
                        else
                        {
                            pathToSecondaryImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);

                            if (File.Exists(pathToSecondaryImage))
                            {
                                secondaryImageFileExists = true;
                                secondaryImage.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);
                            }
                        }
                    }
                }
                else
                {
                    if (secondaryImageFileName != null)
                    {
                        string pathToSecondaryImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);

                        if (File.Exists(pathToSecondaryImage))
                        {
                            secondaryImageFileExists = true;
                            secondaryImage.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + secondaryImageFileName);
                        }
                    }
                }

                // attach control to container
                if (secondaryImageFileExists)
                { container.Controls.Add(secondaryImage); }
            }
        }
        #endregion

        #region _ApplicationIconImage
        /// <summary>
        /// Application icon image control.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _ApplicationIconImage(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "ApplicationIconImage";
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrWhiteSpace(specifiedId))
            { id = specifiedId; }

            // declare control and set its id
            HyperLink iconImageHyperLink = new HyperLink();
            iconImageHyperLink.ID = id;
            iconImageHyperLink.NavigateUrl = "/";

            // build control
            string iconImageFileName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.APPLICATION_MASTHEAD_ICON);
            bool iconImageFileExists = false;

            if (!String.IsNullOrWhiteSpace(iconImageFileName))
            {
                if (AsentiaSessionState.SiteHostname != "default")
                {
                    if (iconImageFileName != null)
                    {
                        string pathToIconImage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);

                        if (File.Exists(pathToIconImage))
                        {
                            iconImageFileExists = true;
                            iconImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);
                        }
                        else
                        {
                            pathToIconImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);

                            if (File.Exists(pathToIconImage))
                            {
                                iconImageFileExists = true;
                                iconImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);
                            }
                        }
                    }
                }
                else
                {
                    if (iconImageFileName != null)
                    {
                        string pathToIconImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);

                        if (File.Exists(pathToIconImage))
                        {
                            iconImageFileExists = true;
                            iconImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + iconImageFileName);
                        }
                    }
                }

                // attach control to container
                if (iconImageFileExists)
                { container.Controls.Add(iconImageHyperLink); }
            }
        }
        #endregion

        #region _ApplicationLogoImage
        /// <summary>
        /// Application logo image control.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _ApplicationLogoImage(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "ApplicationLogoImage";
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrWhiteSpace(specifiedId))
            { id = specifiedId; }

            // declare control and set its id
            HyperLink logoImageHyperLink = new HyperLink();
            logoImageHyperLink.ID = id;
            logoImageHyperLink.CssClass = "MobileHidden";
            logoImageHyperLink.NavigateUrl = "/";

            // build control
            string logoImageFileName = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.APPLICATION_MASTHEAD_LOGO);
            bool logoImageFileExists = false;

            if (!String.IsNullOrWhiteSpace(logoImageFileName))
            {
                if (AsentiaSessionState.SiteHostname != "default")
                {
                    if (logoImageFileName != null)
                    {
                        string pathToLogoImage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                        if (File.Exists(pathToLogoImage))
                        {
                            logoImageFileExists = true;
                            logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                        }
                        else
                        {
                            pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                            if (File.Exists(pathToLogoImage))
                            {
                                logoImageFileExists = true;
                                logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                            }
                        }
                    }
                }
                else
                {
                    if (logoImageFileName != null)
                    {
                        string pathToLogoImage = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);

                        if (File.Exists(pathToLogoImage))
                        {
                            logoImageFileExists = true;
                            logoImageHyperLink.ImageUrl = ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_MASTERPAGE + logoImageFileName);
                        }
                    }
                }

                // attach control to container
                if (logoImageFileExists)
                { container.Controls.Add(logoImageHyperLink); }
            }
        }
        #endregion

        #region _MainNavigationControl
        /// <summary>
        /// Main navigation menu control.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _MainNavigationControl(XmlNode elementNode, Panel container)
        {
            if (!HttpContext.Current.Request.Url.ToString().Contains("/unknown/") && !HttpContext.Current.Request.Url.ToString().Contains("/offline/"))
            {
                // id information
                string id = "MainNavigationControl";
                string specifiedId = elementNode.Attributes["id"].Value;
                if (!String.IsNullOrWhiteSpace(specifiedId))
                { id = specifiedId; }

                // declare control and set its id
                NavigationMenu mainNavigationControl = new NavigationMenu(MenuType.MainNavigationMenu);
                mainNavigationControl.ID = id;

                // attach control to container
                container.Controls.Add(mainNavigationControl);
            }
        }
        #endregion

        #region _SettingsControl
        /// <summary>
        /// Control that displays the settings control (log-in/out link, my profile, etc).
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _SettingsControl(XmlNode elementNode, Panel container)
        {
            if (!HttpContext.Current.Request.Url.ToString().Contains("/unknown/") && !HttpContext.Current.Request.Url.ToString().Contains("/offline/"))
            {                
                Panel settingsControl = new Panel();

                if (AsentiaSessionState.IdAccountUser > 0 || AsentiaSessionState.IdSiteUser > 0)
                {
                    // id information
                    string id = "SettingsControl";
                    settingsControl.ID = id;

                    // settings image
                    Image settingsImage = new Image();
                    settingsImage.ID = id + "Image";
                    settingsImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM_WHITE,
                                                                    ImageFiles.EXT_PNG);
                    settingsImage.AlternateText = _GlobalResources.Log_In;
                    settingsImage.CssClass = "SmallIcon";
                    settingsControl.Controls.Add(settingsImage);

                    // settings control menu item container
                    Panel settingsControlMenuItemContainer = new Panel();
                    settingsControlMenuItemContainer.ID = "SettingsControlMenuItemContainer";

                    // logged in tag
                    LoggedInTag loggedInTag = new LoggedInTag();
                    loggedInTag.ID = id + "LoggedInTag";
                    settingsControlMenuItemContainer.Controls.Add(loggedInTag);

                    if (AsentiaSessionState.IdSiteUser > 1)
                    {
                        // my profile link
                        string myProfileId = id + "MyProfileLink";

                        // declare container for control
                        Panel myProfileLinkContainer = new Panel();
                        myProfileLinkContainer.ID = myProfileId + "Container";

                        // build control
                        HyperLink myProfileLink = new HyperLink();
                        myProfileLink.Text = _GlobalResources.MyProfile;
                        myProfileLink.NavigateUrl = "~/MyProfile/";
                        myProfileLinkContainer.Controls.Add(myProfileLink);

                        // attach control to container
                        settingsControlMenuItemContainer.Controls.Add(myProfileLinkContainer);

                        // my cart link
                        EcommerceSettings ecommerceSettings = new EcommerceSettings();

                        // only do cart link if ecommerce is set and verified and the processor is not PayPal
                        if (ecommerceSettings.IsEcommerceSetAndVerified && ecommerceSettings.ProcessingMethod != EcommerceSettings.PROCESSING_METHOD_PAYPAL)
                        {
                            string myCartId = id + "MyCartLink";

                            // declare container for control
                            Panel myCartLinkContainer = new Panel();
                            myCartLinkContainer.ID = myCartId + "Container";

                            // build control
                            HyperLink myCartLink = new HyperLink();
                            myCartLink.Text = _GlobalResources.MyCart;
                            myCartLink.NavigateUrl = "/myprofile/MyCart.aspx";
                            myCartLinkContainer.Controls.Add(myCartLink);

                            // attach control to container
                            settingsControlMenuItemContainer.Controls.Add(myCartLinkContainer);
                        }
                    }

                    // logout link
                    string logoutLinkId = id + "LogoutLink";

                    Panel logoutLinkContainer = new Panel();
                    logoutLinkContainer.ID = logoutLinkId + "Container";

                    LinkButton logoutLink = new LinkButton();
                    logoutLink.ID = logoutLinkId + "Link";
                    logoutLink.Text = _GlobalResources.LogOut;
                    logoutLink.Command += new CommandEventHandler(_LogoutLink_Command);

                    logoutLinkContainer.Controls.Add(logoutLink);
                    settingsControlMenuItemContainer.Controls.Add(logoutLinkContainer);

                    settingsControl.Controls.Add(settingsControlMenuItemContainer);
                }
                else // if user is NOT logged in
                {
                    if (AsentiaSessionState.IdSiteUser == 0)
                    {
                        // id information
                        string id = "SettingsControlForLogin";
                        settingsControl.ID = id;

                        // login image
                        Image loginImage = new Image();
                        loginImage.ID = id + "Image";
                        loginImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN_WHITE,
                                                                     ImageFiles.EXT_PNG);
                        loginImage.ToolTip = _GlobalResources.Log_In;
                        loginImage.AlternateText = _GlobalResources.Log_In;
                        loginImage.CssClass = "SmallIcon";
                        settingsControl.Controls.Add(loginImage);

                        // login text
                        Label loginText = new Label();
                        loginText.ID = id + "Label";
                        loginText.CssClass = "LoginLabel";
                        loginText.Text = _GlobalResources.Log_In;
                        settingsControl.Controls.Add(loginText);

                        // set the global login "button" id property to the id of this login link
                        LogInButtonId = settingsControl.ID;

                        // set the LogInButtonControlExists property to true so that the pages know the login button exists
                        LogInButtonControlExists = true;
                    }
                }

                container.Controls.Add(settingsControl);
            }
        }
        #endregion

        #region _LogoutLink_Command
        /// <summary>
        /// Logs the user out.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _LogoutLink_Command(object sender, CommandEventArgs e)
        {
            AsentiaAuthenticatedPage.UpdateUserSessionExpiration(null);
            AsentiaSessionState.EndSession(true);
        }
        #endregion

        #region _PoweredByTag
        /// <summary>
        /// Control that displays the "Asentia" build and copyright information.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _PoweredByTag(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "PoweredByTag";
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrWhiteSpace(specifiedId))
            { id = specifiedId; }

            // declare container for control
            Panel poweredByTagContainer = new Panel();
            poweredByTagContainer.ID = id + "Container";

            // build control
            Literal poweredByTag = new Literal();
            poweredByTag.Text = String.Format("<span id=\"{0}\">{1} <a href=\"http://www.asentialms.com\" target=\"_blank\">{2}</a> {3} {4} {5}{6} <a href=\"http://www.icslearninggroup.com\" target=\"_blank\">{7}</a></span>",
                                              id,
                                              _GlobalResources.Poweredby,
                                              _GlobalResources.Asentia,
                                              _GlobalResources.Build,
                                              ApplicationConstants.VERSION,
                                              _GlobalResources.Copyright,
                                              AsentiaSessionState.UserTimezoneCurrentLocalTime.Year.ToString(),
                                              _GlobalResources.ICSLearningGroup);

            // attach control to container
            poweredByTagContainer.Controls.Add(poweredByTag);
            container.Controls.Add(poweredByTagContainer);
        }
        #endregion

        #region _FooterTag
        /// <summary>
        /// Control that displays the site's branding.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _FooterTag(XmlNode elementNode, Panel container)
        {
            // id information
            string id = "FooterTag";
            string specifiedId = elementNode.Attributes["id"].Value;
            if (!String.IsNullOrWhiteSpace(specifiedId))
            { id = specifiedId; }

            // declare container for control
            Panel siteBrandingTagContainer = new Panel();
            siteBrandingTagContainer.ID = id + "Container";

            // company name 
            HtmlGenericControl companyName = new HtmlGenericControl("p");
            companyName.ID = id + "CompanyName";
            companyName.InnerHtml = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_COMPANYNAME);

            // email
            HtmlGenericControl email = new HtmlGenericControl("p");
            email.ID = id + "Email";
            email.InnerHtml = "<a href=\"mailto: " + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_EMAIL) + "\">" + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_EMAIL) + "</a>";

            // website
            HtmlGenericControl website = new HtmlGenericControl("p");
            website.ID = id + "Website";
            website.InnerHtml = "<a href=\"" + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_WEBSITE) + "\" target=\"_blank\">" + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.FOOTER_WEBSITE) + "</a>";

            // attach control to container
            siteBrandingTagContainer.Controls.Add(companyName);
            siteBrandingTagContainer.Controls.Add(email);
            siteBrandingTagContainer.Controls.Add(website);
            container.Controls.Add(siteBrandingTagContainer);
        }
        #endregion

        #region _CustomControl
        /// <summary>
        /// Control that renders custom HTML.
        /// </summary>
        /// <param name="elementNode">control element node from control file</param>
        /// <param name="container">container to attach the control to</param>
        private void _CustomControl(XmlNode elementNode, Panel container)
        {
            if (!HttpContext.Current.Request.Url.ToString().Contains("/unknown/") && !HttpContext.Current.Request.Url.ToString().Contains("/offline/"))
            {
                // id information
                string id = null;
                string specifiedId = elementNode.Attributes["id"].Value;
                if (!String.IsNullOrWhiteSpace(specifiedId))
                { id = specifiedId; }

                if (id != null && !String.IsNullOrWhiteSpace(elementNode.InnerText))
                {
                    // declare control and set its id
                    Panel customControl = new Panel();
                    customControl.ID = id;

                    // build control
                    Literal controlHTML = new Literal();
                    controlHTML.Text = elementNode.InnerText;
                    customControl.Controls.Add(controlHTML);

                    // attach control to container
                    container.Controls.Add(customControl);
                }
            }
        }
        #endregion
        #endregion
        #endregion

        #region Overridden Methods
        #region OnInit
        /// <summary>
        /// Page iniitalization.
        /// </summary>
        /// <param name="e">Event arguments</param>
        protected override void OnInit(EventArgs e)
        {
            // determine if the home page master page is instansiating this, and set that param so that we can
            // load the home page masthead, otherwise, we load the application masthead
            if (this.AppRelativeVirtualPath.Contains("HomePage.Master"))
            { this._IsHomePage = true; }

            // INITIALIZE GLOBAL CONTROLS DICTIONARY
            //
            // these will be searched for in the GlobalControls.xml file, and 
            this._ControlMethods.Add("SettingsControl", new Action<XmlNode, Panel>(this._SettingsControl));
            this._ControlMethods.Add("LanguageSelectorControl", new Action<XmlNode, Panel>(this._LanguageSelectorControl));
            this._ControlMethods.Add("HomePageLogoImage", new Action<XmlNode, Panel>(this._HomePageLogoImage));
            this._ControlMethods.Add("HomePageSecondaryImage", new Action<XmlNode, Panel>(this._HomePageSecondaryImage));
            this._ControlMethods.Add("ApplicationIconImage", new Action<XmlNode, Panel>(this._ApplicationIconImage));
            this._ControlMethods.Add("ApplicationLogoImage", new Action<XmlNode, Panel>(this._ApplicationLogoImage));
            this._ControlMethods.Add("MainNavigationControl", new Action<XmlNode, Panel>(this._MainNavigationControl));
            this._ControlMethods.Add("PoweredByTag", new Action<XmlNode, Panel>(this._PoweredByTag));
            this._ControlMethods.Add("FooterTag", new Action<XmlNode, Panel>(this._FooterTag));
            this._ControlMethods.Add("CustomControl", new Action<XmlNode, Panel>(this._CustomControl));

            // DECLARE GLOBAL JAVASCRIPT VARIABLES
            //
            // these are variables declared globally for use across the application,
            // whose values need to be assigned from server side functions
            //
            // example: setting a "selected language" variable so that client side
            // objects like TinyMCE can be rendered in the appropriate language
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type=\"text/javascript\">");

            sb.AppendLine(" CurrentPageLanguage = \"" + AsentiaSessionState.UserCulture + "\";");

            sb.AppendLine("</script>");

            this.GlobalJavascriptVariables.Text = sb.ToString();

            // add control bundle to tooklitscriptmanager
            ControlBundle masterBundle = new ControlBundle();
            masterBundle.Name = "MasterBundle";
            asm.ControlBundles.Add(masterBundle);

            // call the base
            base.OnInit(e);
        }
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.AsentiaMasterPage.js");

            base.OnPreRender(e);
        }
        #endregion
        #endregion
    }
}
