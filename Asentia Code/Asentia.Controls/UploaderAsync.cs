﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using AjaxControlToolkit;

namespace Asentia.Controls
{
    /// <summary>
    /// Asynchronous uploader control. 
    /// </summary>
    public class UploaderAsync : WebControl
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">the id of this control</param>
        /// <param name="uploadType">the upload type</param>
        /// <param name="errorPanelId">the id of the panel to display errors in</param>
        public UploaderAsync(string id, UploadType uploadType, string errorPanelId)
        {
            // set initialization variables
            this.ID = id;
            this._UploadType = uploadType;
            this._ErrorPanelId = errorPanelId;

            // set the file upload path
            this._SetFileUploadPath();

            // build the throbber panel
            this._BuildThrobberPanel();

            // build the upload control
            this._BuildUploadControl();

            // build the hidden fields
            this._BuildUploadHiddenFields();

            // build the completed panel
            this._BuildCompletedPanel();

            // add the controls 
            this.Controls.Add(this._UploadControl);
            this.Controls.Add(this._ThrobberPanel);
            this.Controls.Add(this._UploadHiddenField);
            this.Controls.Add(this._UploadHiddenFieldOriginalFileName);
            this.Controls.Add(this._UploadHiddenFieldFileSize);
            this.Controls.Add(this._CompletedPanel);
            this.ExtensionToValidateHiddenField.ID = this.ID + "_ExtensionToValidateHiddenField";
            this.Controls.Add(this.ExtensionToValidateHiddenField);
        }
        #endregion

        #region Properties
        /// <summary>
        /// In the case that this file upload is for an image, should we show a preview of it?
        /// </summary>
        public bool ShowPreviewOnImageUpload = true;

        /// <summary>
        /// In the case that this file upload is for an image, should we resize it?
        /// </summary>
        public bool ResizeOnUpload = false;

        /// <summary>
        /// The maximum width to resize an image file to.
        /// </summary>
        public int ResizeMaxWidth = 100;

        /// <summary>
        /// The maximum height to resize an image file to.
        /// </summary>
        public int ResizeMaxHeight = 100;

        /// <summary>
        /// The path to upload the file to, read only.
        /// </summary>
        public string FileUploadPath
        { get { return this._FileUploadPath; } }

        /// <summary>
        /// The name of the saved (uploaded) file, read only.
        /// </summary>
        public string FileSavedName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(this._UploadHiddenField.Value))
                { return null; }
                else
                { return this._UploadHiddenField.Value; }
            }
        }

        /// <summary>
        /// The full path to the saved (uploaded) file, read only.
        /// </summary>
        public string SavedFilePath
        {
            get
            {
                if (String.IsNullOrWhiteSpace(this._UploadHiddenField.Value))
                { return null; }
                else
                { return this.FileUploadPath + this._UploadHiddenField.Value; }
            }
        }

        /// <summary>
        /// The original file name of the uploaded file.
        /// </summary>
        public string FileOriginalName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(this._UploadHiddenFieldOriginalFileName.Value))
                { return null; }
                else
                { return this._UploadHiddenFieldOriginalFileName.Value; }
            }
        }

        /// <summary>
        /// The file size of the uploaded file.
        /// </summary>
        public string FileSize
        {
            get
            {
                if (String.IsNullOrWhiteSpace(this._UploadHiddenFieldFileSize.Value))
                { return null; }
                else
                { return this._UploadHiddenFieldFileSize.Value; }
            }
        }

        /// <summary>
        /// Method to fire after server-side upload is complete. If specified, this will fire
        /// after this object's completed method, and have a string property containing the path 
        /// where this object's completed method saved the file. This is used if we need to access
        /// the saved file during this uploader's postback.
        /// </summary>
        public Action<object, AsyncFileUploadEventArgs, string> ServerSideCompleteMethod;

        /// <summary>
        /// Client-side (JS) method to fire when the upload begins. If specified, this will fire
        /// inside of this object's OnClientUploadBegin method, after the standard operations
        /// for that method are completed. This would be used if we need to modify page elements
        /// with uploaded file content. Example: Disabling fields and postback buttons.
        /// 
        /// Any JS methods specified for this must contain no arguments and this property should 
        /// be set to only the method name.
        /// </summary>
        public string ClientSideBeginJSMethod;

        /// <summary>
        /// Client-side (JS) method to fire if the upload errors. If specified, this will fire
        /// inside of this object's OnClientUploadError method, after the standard operations
        /// for that method are completed. This would be used if we need to modify page elements
        /// with uploaded file content. Example: Re-enabling fields and postback buttons.
        /// 
        /// Any JS methods specified for this must contain no arguments and this property should 
        /// be set to only the method name.
        /// </summary>
        public string ClientSideErrorJSMethod;

        /// <summary>
        /// Client-side (JS) method to fire after upload is complete. If specified, this will fire
        /// inside of this object's OnClientUploadComplete method, after the standard operations
        /// for that method are completed. This would be used if we need to modify page elements
        /// with uploaded file content. Example: Displaying a preview of an uploaded masthead.
        /// 
        /// Any JS methods specified for this must contain one and only one argument; "uploadedFilePath,"
        /// and this property should be set to only the method name.
        /// </summary>
        public string ClientSideCompleteJSMethod;

        /// <summary>
        /// boolean variable set to true if to check the specific extension.
        /// </summary>
        public bool ValidateSpecificExtension = false;

        /// <summary>
        /// hidden field variable set to the specific extension type to validate. 
        /// </summary>
        public HiddenField ExtensionToValidateHiddenField = new HiddenField();

        /// <summary>
        /// public string used for task uploads that we can use to define valid extensions
        /// </summary>
        public string TaskValidExtensions = null;
        #endregion

        #region Private Properties
        /// <summary>
        /// The panel that contains the "uploading" message when a file is uploading.
        /// </summary>
        private Panel _ThrobberPanel;

        /// <summary>
        /// The file uploader control.
        /// </summary>
        private AsyncFileUpload _UploadControl;

        /// <summary>
        /// Hidden field to store the filename of the saved file.
        /// </summary>
        private HiddenField _UploadHiddenField;

        /// <summary>
        /// Hidden field to store the file name of the original file.
        /// </summary>
        private HiddenField _UploadHiddenFieldOriginalFileName;

        /// <summary>
        /// Hidden field to store the file size of the file.
        /// </summary>
        private HiddenField _UploadHiddenFieldFileSize;

        /// <summary>
        /// The panel that contains the "completed" message when an upload is complete.
        /// </summary>
        private Panel _CompletedPanel;

        /// <summary>
        /// The type of upload this is.
        /// </summary>
        private UploadType _UploadType;

        /// <summary>
        /// The id of the panel used to display errors, if any.
        /// </summary>
        private string _ErrorPanelId = null;

        /// <summary>
        /// The path where the file is to be uploaded.
        /// </summary>
        private string _FileUploadPath = null;
        #endregion

        #region Private Methods
        #region _SetFileUploadPath
        /// <summary>
        /// Sets the path of where the file will be uploaded based on the upload type.
        /// </summary>
        private void _SetFileUploadPath()
        {
            switch (this._UploadType)
            {
                case UploadType.Favicon:
                    this._FileUploadPath = SitePathConstants.UPLOAD_FAVICON;
                    break;
                case UploadType.Avatar:
                    this._FileUploadPath = SitePathConstants.UPLOAD_AVATAR;
                    break;
                case UploadType.ContentPackage: case UploadType.VideoFile: case UploadType.PowerPointFile: case UploadType.PDF:
                    this._FileUploadPath = SitePathConstants.UPLOAD_CONTENTPACKAGE;
                    break;
                case UploadType.CourseMaterial:
                    this._FileUploadPath = SitePathConstants.UPLOAD_COURSEMATERIAL;
                    break;
                case UploadType.GroupDocument:
                    this._FileUploadPath = SitePathConstants.UPLOAD_GROUPDOCUMENT;
                    break;
                case UploadType.Task:
                    this._FileUploadPath = SitePathConstants.UPLOAD_TASK;
                    break;
                case UploadType.Image:
                    this._FileUploadPath = SitePathConstants.UPLOAD_IMAGE;
                    break;
                case UploadType.LearningPathMaterial:
                    this._FileUploadPath = SitePathConstants.UPLOAD_LEARNINGPATHMATERIAL;
                    break;
                case UploadType.LessonMaterial:
                    this._FileUploadPath = SitePathConstants.UPLOAD_LESSONMATERIAL;
                    break;
                case UploadType.MastheadImage:
                    this._FileUploadPath = SitePathConstants.UPLOAD_MASTHEAD;
                    break;
                case UploadType.UserActivityData:
                    this._FileUploadPath = SitePathConstants.UPLOAD_ACTIVITYDATAIMPORT;
                    break;
                case UploadType.UserBatch:
                    this._FileUploadPath = SitePathConstants.UPLOAD_USERBATCH;
                    break;
                case UploadType.UserCertificateData:
                    this._FileUploadPath = SitePathConstants.UPLOAD_CERTIFICATEDATAIMPORT;
                    break;
                case UploadType.ProfileFile:
                    this._FileUploadPath = SitePathConstants.UPLOAD_PROFILEFILE;
                    break;
                case UploadType.RosterBatch:
                    this._FileUploadPath = SitePathConstants.UPLOAD_ROSTERBATCH;
                    break;
            }
        }
        #endregion

        #region _BuildThrobberPanel
        /// <summary>
        /// Builds the panel that contains the upload throbber.
        /// </summary>
        private void _BuildThrobberPanel()
        {
            this._ThrobberPanel = new Panel();
            this._ThrobberPanel.ID = this.ID + "_ThrobberPanel";
            this._ThrobberPanel.CssClass = "AsyncUploaderThrobber";
            this._ThrobberPanel.Style.Add("display", "none");

            Image throbberImage = new Image();
            throbberImage.ID = this.ID + "_ThrobberImage";
            throbberImage.ImageUrl = SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS_ICONSET + "uploading.gif";
            this._ThrobberPanel.Controls.Add(throbberImage);

            Label throbberText = new Label();
            throbberText.ID = this.ID + "_ThrobberText";
            this._ThrobberPanel.Controls.Add(throbberText);
        }
        #endregion

        #region _BuildUploadControl
        /// <summary>
        /// Builds the upload control and attaches associated events.
        /// </summary>
        private void _BuildUploadControl()
        {
            this._UploadControl = new AsyncFileUpload();
            this._UploadControl.ID = this.ID + "_UploadControl";
            this._UploadControl.CssClass = "AsyncUploader";

            // set the colors of the control for error and success states
            this._UploadControl.ErrorBackColor = System.Drawing.Color.FromArgb(0xC75A5A);
            this._UploadControl.CompleteBackColor = System.Drawing.Color.FromArgb(0x68A968);

            // set the id of the throbber
            this._UploadControl.ThrobberID = this._ThrobberPanel.ID;

            // attach client-side event handlers
            this._UploadControl.OnClientUploadStarted = "FileUpload_Begin" + this.ID;
            this._UploadControl.OnClientUploadError = "FileUpload_Error" + this.ID;
            this._UploadControl.OnClientUploadComplete = "FileUpload_Complete" + this.ID;

            // attach server-side upload completed handler
            this._UploadControl.UploadedComplete += new EventHandler<AsyncFileUploadEventArgs>(_UploadedComplete);
        }
        #endregion

        #region _BuildUploadHiddenFields
        /// <summary>
        /// Builds the hidden field controls for storing the filename we use to save the file,
        /// the original file name, and the file size.
        /// </summary>
        private void _BuildUploadHiddenFields()
        {
            this._UploadHiddenField = new HiddenField();
            this._UploadHiddenField.ID = this.ID + "_UploadHiddenField";

            this._UploadHiddenFieldOriginalFileName = new HiddenField();
            this._UploadHiddenFieldOriginalFileName.ID = this.ID + "_UploadHiddenFieldOriginalFileName";

            this._UploadHiddenFieldFileSize = new HiddenField();
            this._UploadHiddenFieldFileSize.ID = this.ID + "_UploadHiddenFieldFileSize";
        }
        #endregion

        #region _BuildCompletedPanel
        /// <summary>
        /// Builds the panel to be displayed when the upload has been completed.
        /// </summary>
        private void _BuildCompletedPanel()
        {
            this._CompletedPanel = new Panel();
            this._CompletedPanel.ID = this.ID + "_CompletedPanel";
            this._CompletedPanel.CssClass = "AsyncUploaderCompleted";

            Label completedPanelText = new Label();
            completedPanelText.ID = this.ID + "_CompletedPanelText";
            this._CompletedPanel.Controls.Add(completedPanelText);
        }
        #endregion

        #region _BuildUploadBeginJS
        /// <summary>
        /// Builds the Javascript method that serves as the client-side event handler
        /// for the asynchronous "begin upload" command.
        /// </summary>
        /// <returns>string containing javascript</returns>
        private string _BuildUploadBeginJS()
        {
            // instansiate a string builder
            StringBuilder sb = new StringBuilder();       
            // beginning of function            
            sb.AppendLine("function FileUpload_Begin" + this.ID + "(sender, args) {");
            sb.AppendLine("    $(\"#" + this._ErrorPanelId + "\").text(\"\")");
            sb.AppendLine("    $(\"#" + this._CompletedPanel.ID + "\").hide()");
            sb.AppendLine("    $(\"#" + this.ID + "_ThrobberText\").text(\"" + _GlobalResources.Uploading + " \" + sender._inputFile.files[0].size.toString().replace(/\\B(?=(\\d{3})+(?!\\d))/g, \",\") + \" " + _GlobalResources.bytes_lower + "\")");

            // file extension validation
            string extensions = null;
            bool extensionsAreExcluded = false;

            switch (this._UploadType)
            {
                case UploadType.ContentPackage:
                    // only zip files
                    extensions = "zip";
                    break;
                case UploadType.Favicon:
                    // only icon and image file types
                    extensions = "gif, jpg, jpeg, png, ico";
                    break;
                case UploadType.Avatar:
                case UploadType.MastheadImage:
                case UploadType.Image:
                    // only image file types
                    extensions = "gif, jpg, jpeg, png";
                    break;
                case UploadType.CourseMaterial:
                case UploadType.GroupDocument:
                case UploadType.Task:
                    if (!String.IsNullOrWhiteSpace(this.TaskValidExtensions))
                    { extensions = this.TaskValidExtensions; }
                    else // if this gets executed, it is because of programmer error
                    { 
                        //throw new AsentiaException(_GlobalResources.TaskUploadsMustHaveTaskValidExtensionsVariableDefined); 
                        extensionsAreExcluded = true;
                        extensions = "bat, cmd, com, crt, dll, exe, hta, msi, pif, scr, sys, vb, vbe, vbs";
                    }
                    break;
                case UploadType.LearningPathMaterial:
                case UploadType.LessonMaterial:
                    // anything but potentially harmful types
                    extensionsAreExcluded = true;
                    extensions = "bat, cmd, com, crt, dll, exe, hta, msi, pif, scr, sys, vb, vbe, vbs";
                    break;
                case UploadType.UserActivityData:
                case UploadType.UserBatch:                    
                case UploadType.RosterBatch:
                    extensions = "txt";
                    break;
                case UploadType.ProfileFile:
                    extensions = "gif, jpg, jpeg, png, txt, pdf, xlsx, doc, docx";
                    break;
                case UploadType.UserCertificateData:
                    // only txt files
                    extensions = "txt";
                    break;
                case UploadType.VideoFile:
                    // video file types - only ones supported by HTML5 <video>
                    extensions = "mp4, ogg, webem";
                    break;
                case UploadType.PowerPointFile:
                    extensions = "ppt, pptx";
                    break;
                case UploadType.PDF:
                    extensions = "pdf, doc, docx";
                    break;
            }

            // build a javascript "if" string containing the extensions from above
            string fileExtensionJsIfString = null;

            string[] extensionsArray = extensions.Split(',');
            int i = 0;

            fileExtensionJsIfString = "if ( ";

            if (this.ValidateSpecificExtension)
            {
                // build a javascript "if" string containing the extensions from above
                fileExtensionJsIfString += "fileExt.toLowerCase() !=";
                fileExtensionJsIfString += " hiddenFieldExtension ";
            }
            else
            {
                foreach (string extension in extensionsArray)
                {
                    fileExtensionJsIfString += "fileExt.toLowerCase() ";

                    if (extensionsAreExcluded)
                    { fileExtensionJsIfString += "== "; }
                    else
                    { fileExtensionJsIfString += "!= "; }

                    fileExtensionJsIfString += "'" + extension.Trim() + "' ";

                    if (i < extensionsArray.Length - 1)
                    {
                        if (extensionsAreExcluded)
                        { fileExtensionJsIfString += "|| "; }
                        else
                        { fileExtensionJsIfString += "&& "; }
                    }

                    i++;
                }
            }

            fileExtensionJsIfString += ") {";

            // build the error message for extension validation
            string extensionErrorMessage = null;

            if (this.ValidateSpecificExtension)
            {
                extensionErrorMessage = _GlobalResources.OnlyFilesWithTheExtension_s + " \" + hiddenFieldExtension + \" " + _GlobalResources.AreAllowed;
            }
            else if (extensionsAreExcluded)
            { extensionErrorMessage = _GlobalResources.FilesWithTheExtension_s + " " + extensions + " " + _GlobalResources.AreNotAllowed; }
            else
            { extensionErrorMessage = _GlobalResources.OnlyFilesWithTheExtension_s + " " + extensions + " " + _GlobalResources.AreAllowed; }

            // build extension validation JS
            sb.AppendLine("    var fileName = args.get_fileName();");
            sb.AppendLine("    var fileExt = fileName.substring(fileName.lastIndexOf(\".\") + 1);");
            sb.AppendLine("    var hiddenFieldExtension = $('#" + this.ID + "_ExtensionToValidateHiddenField').val();");        
            sb.AppendLine("    " + fileExtensionJsIfString);
            sb.AppendLine("        throw {");
            sb.AppendLine("         name:        \"InvalidFileType\",");
            sb.AppendLine("         level:       \"Error\",");
            sb.AppendLine("         message:     \"" + extensionErrorMessage + "\",");
            sb.AppendLine("         htmlMessage: \"" + extensionErrorMessage + "\"");
            sb.AppendLine("        }");
            sb.AppendLine("        return false;");
            sb.AppendLine("    }");

            // file size validation

            // file size error message
            string fileSizeErrorMessage = _GlobalResources.FileExceedsMaximumFileUploadSize;

            // build file size validation JS
            sb.AppendLine("    var fileSizeInBytes = sender._inputFile.files[0].size;");
            sb.AppendLine("    if (fileSizeInBytes > " + Config.ApplicationSettings.MaximumFileUploadSizeInBytes.ToString() + ") {");
            sb.AppendLine("        throw {");
            sb.AppendLine("         name:        \"FileExceedsMaximumSize\",");
            sb.AppendLine("         level:       \"Error\",");
            sb.AppendLine("         message:     \"" + fileSizeErrorMessage + "\",");
            sb.AppendLine("         htmlMessage: \"" + fileSizeErrorMessage + "\"");
            sb.AppendLine("        }");
            sb.AppendLine("        return false;");
            sb.AppendLine("    }");

            // if there is a client-side "on begin" method to fire, fire it
            if (!String.IsNullOrWhiteSpace(this.ClientSideBeginJSMethod))
            {
                sb.AppendLine("    " + this.ClientSideBeginJSMethod + "();");
            }

            // end of function
            sb.AppendLine("    return true;");
            sb.AppendLine("}");

            // return the JS string
            return sb.ToString();
        }
        #endregion

        #region _BuildUploadErrorJS
        /// <summary>
        /// Builds the Javascript method that serves as the client-side event handler
        /// for upload error command.
        /// </summary>
        /// <returns>string containing javascript</returns>
        private string _BuildUploadErrorJS()
        {
            // instansiate a string builder
            StringBuilder sb = new StringBuilder();

            // beginning of function
            sb.AppendLine("function FileUpload_Error" + this.ID + "(sender, args) {");
            sb.AppendLine("    $(\"#" + this._CompletedPanel.ID + "\").hide()");
            sb.AppendLine("    $(\"#" + this._ErrorPanelId + "\").text(args.get_errorMessage())");
            sb.AppendLine("    var span = $get(\"" + this._UploadControl.ID + "\");");
            sb.AppendLine("    var txts = span.getElementsByTagName(\"input\");");
            sb.AppendLine("    for (var i = 0; i < txts.length; i++) {");
            sb.AppendLine("        if (txts[i].type == \"text\") {");
            sb.AppendLine("            txts[i].value = \"\";");
            sb.AppendLine("        }");
            sb.AppendLine("        if (txts[i].type == \"file\") {");
            sb.AppendLine("            txts[i].value = \"\";");
            sb.AppendLine("        }");
            sb.AppendLine("    }");

            // if there is a client-side "error" method to fire, fire it
            if (!String.IsNullOrWhiteSpace(this.ClientSideErrorJSMethod))
            {
                sb.AppendLine("    " + this.ClientSideErrorJSMethod + "();");
            }

            sb.AppendLine("}");

            // return the JS string
            return sb.ToString();
        }
        #endregion

        #region _BuildUploadCompleteJS
        /// <summary>
        /// Builds the Javascript method that serves as the client-side event handler
        /// for upload complete command.
        /// </summary>
        /// <returns>string containing javascript</returns>
        private string _BuildUploadCompleteJS()
        {
            // instansiate a string builder
            StringBuilder sb = new StringBuilder();

            // beginning of function
            sb.AppendLine("function FileUpload_Complete" + this.ID + "(sender, args) {");
            sb.AppendLine("    var span = $get(\"" + this._UploadControl.ID + "\");");
            sb.AppendLine("    var txts = span.getElementsByTagName(\"input\");");
            sb.AppendLine("    for (var i = 0; i < txts.length; i++) {");
            sb.AppendLine("        if (txts[i].type == \"text\") {");
            sb.AppendLine("            txts[i].value = \"\";");
            sb.AppendLine("        }");
            sb.AppendLine("        if (txts[i].type == \"file\") {");
            sb.AppendLine("            txts[i].value = \"\";");
            sb.AppendLine("        }");
            sb.AppendLine("    }");
            sb.AppendLine("    var uploadHiddenField = $get(\"" + this._UploadHiddenField.ID + "\");");
            sb.AppendLine("    uploadHiddenField.value = sender.uploadedFileName;");
            sb.AppendLine("    var uploadHiddenFieldOriginalFileName = $get(\"" + this._UploadHiddenFieldOriginalFileName.ID + "\");");
            sb.AppendLine("    uploadHiddenFieldOriginalFileName.value = args.get_fileName();");
            sb.AppendLine("    var uploadHiddenFieldFileSize = $get(\"" + this._UploadHiddenFieldFileSize.ID + "\");");
            sb.AppendLine("    uploadHiddenFieldFileSize.value = args.get_length();");
            sb.AppendLine("    $(\"#" + this._CompletedPanel.ID + "\").show();");
            sb.AppendLine("    $(\"#" + this._CompletedPanel.ID + "\").text(args.get_fileName() + \" " + _GlobalResources.SuccessfullyUploaded + "\");");

            // if this is an avatar upload and show preview is on, add the image to the completed panel for preview
            if ((this._UploadType == UploadType.Avatar || this._UploadType == UploadType.Image || this._UploadType == UploadType.Favicon) && this.ShowPreviewOnImageUpload)
            {
                sb.AppendLine("    $(\"#" + this._CompletedPanel.ID + "\").prepend('<img id=\"" + this._CompletedPanel.ID + "_ImagePreview" + "\" src=\"" + this.FileUploadPath + "' + uploadHiddenField.value + '\" />');");
            }

            // if there is a client-side completed method to fire, fire it
            if (!String.IsNullOrWhiteSpace(this.ClientSideCompleteJSMethod))
            {
                sb.AppendLine("    " + this.ClientSideCompleteJSMethod + "('" + this.FileUploadPath + "' + sender.uploadedFileName);");
            }

            sb.AppendLine("}");

            // return the JS string
            return sb.ToString();
        }
        #endregion

        #region _UploadedComplete
        /// <summary>
        /// Method to handle the server-side event of the file being uploaded to the server.
        /// Perfroms the renaming and saving of the uploaded file.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _UploadedComplete(object sender, AsyncFileUploadEventArgs e)
        {
            try
            {
                // get the name of the file we uploaded, this is the original name from the user's computer
                string uploadedFileName = e.FileName;

                // get the file extension from the original filename
                string fileExtension = Path.GetExtension(uploadedFileName);

                // initialize variables for the new filename and the findName flag
                string fileName = String.Empty;
                bool findName = true;

                // while instructed to do so (findName = true) attempt to find a unique name for the file
                // by generating names and seeing if the file already exists
                while (findName)
                {
                    // generate a file name using a hash of the original filename plus the current timestamp with milliseconds
                    fileName = Cryptography.GetHash(uploadedFileName + DateTime.UtcNow.ToString("o"), Cryptography.HashType.MD5);

                    // if the file already exists, sleep for 1ms and try again; else proceed, we found our name
                    if (File.Exists(MapPathSecure(this.FileUploadPath + fileName + fileExtension)))
                    { Thread.Sleep(1); }
                    else
                    {
                        findName = false;
                        this._UploadHiddenField.Value = fileName + fileExtension;
                    }
                }

                // save the file
                AsyncFileUpload fileUploadControl = (AsyncFileUpload)sender;
                fileUploadControl.SaveAs(MapPathSecure(this.FileUploadPath + this._UploadHiddenField.Value));

                // if the file is an avatar upload, and to be resized upon upload, resize it
                if ((this._UploadType == UploadType.Avatar || this._UploadType == UploadType.Image || this._UploadType == UploadType.Favicon) && this.ResizeOnUpload)
                {
                    // first resize the image to standard avatar size
                    ImageResizer resizer = new ImageResizer();
                    resizer.MaxX = this.ResizeMaxWidth;
                    resizer.MaxY = this.ResizeMaxHeight;
                    resizer.TrimImage = true;
                    resizer.Resize(MapPathSecure(this.FileUploadPath + this._UploadHiddenField.Value), MapPathSecure(this.FileUploadPath + this._UploadHiddenField.Value + ".resized"));

                    // replace uploaded with "resized"
                    File.Copy(MapPathSecure(this.FileUploadPath + this._UploadHiddenField.Value + ".resized"), MapPathSecure(this.FileUploadPath + this._UploadHiddenField.Value), true);

                    // delete "resized"
                    File.Delete(MapPathSecure(this.FileUploadPath + this._UploadHiddenField.Value + ".resized"));
                }

                // register a script to assign the uploaded file's new name to its JS variable for assignment into our hidden field
                ScriptManager.RegisterStartupScript(this, this.GetType(), "UploadedFileName", "window.parent.$find('" + this._UploadControl.ID + "').uploadedFileName = \"" + fileName + fileExtension + "\";", true);

                // if there is a server-side completed method to be fired, fire it
                if (this.ServerSideCompleteMethod != null)
                { this.ServerSideCompleteMethod.Invoke(sender, e, this.FileUploadPath + this._UploadHiddenField.Value); }
            }
            catch
            {
                // right now, we're just throwing the exception, which the ScriptManager will catch and display as an alert
                // but, we might want to just handle the exception here and pass an error message up to the JS for display on the page
                throw;
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s) -- this has been changed to use ScriptManager rather than the Page's ClientScript because
            // CSM doesn't register client script blocks when a control is created dynamically from an AJAX postback, but ScriptManager does, ensure this
            // has no adverse effects on pages where this control is created "normally" as part of the page's standard lifecycle
            ClientScriptManager csm = this.Page.ClientScript; // remove later

            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UploaderAsync), "FileUpload_Begin" + this.ID, this._BuildUploadBeginJS(), true);
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UploaderAsync), "FileUpload_Error" + this.ID, this._BuildUploadErrorJS(), true);
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UploaderAsync), "FileUpload_Complete" + this.ID, this._BuildUploadCompleteJS(), true);

            // build start up call to strip inline width style from AjajControlToolkit built uploader control           
            StringBuilder startUpCallsScript = new StringBuilder();
            startUpCallsScript.AppendLine("Sys.Application.add_load(");
            startUpCallsScript.AppendLine("function() { ");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    $(\"#" + this._UploadControl.ClientID + " div input \").attr(\"style\", function(i, style) {");
            startUpCallsScript.AppendLine("         if (style != null) {");
            startUpCallsScript.AppendLine("             return style.replace(/width[^;]+;?/g, \"\");");
            startUpCallsScript.AppendLine("         }");
            startUpCallsScript.AppendLine("    });");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("});");

            ScriptManager.RegisterStartupScript(this.Page, typeof(UploaderAsync), this._UploadControl.ClientID + "UploaderInitialize", startUpCallsScript.ToString(), true);

            base.OnPreRender(e);

            // if this control is to be disabled, disable it
            if (!this.Enabled)
            { this._UploadControl.Enabled = false; }
        }
        #endregion
        #endregion
    }
}
