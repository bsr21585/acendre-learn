﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    #region ChartType ENUM
    public enum ChartType
    {
        Bar = 0,
        Doughnut = 1,
        Line = 2,
        Pie = 3,
        List = 4,
    }
    #endregion

    public class Chart : WebControl
    {
        #region Constructor
        public Chart(string id, ChartType type, DataTable data, List<string> colors, string title, string subTitle)
        {
            // set base properties
            this.ID = id;
            this._Type = type;
            this._Data = data;
            this._Colors = colors;
            this._Title = title;
            this._SubTitle = subTitle;

            // override the cutout if this is a pie chart
            if (this._Type == ChartType.Pie)
            { this.CutoutPercentage = 2; }
        }
        #endregion

        #region Properties
        // GLOBAL OPTIONS FOR OBJECT RENDERING

        /// <summary>
        /// The width of the chart canvas. Leave it at 0 to not specify a width.
        /// </summary>
        public int CanvasWidth = 0;

        /// <summary>
        /// The height of the chart canvas. Leave it at 0 to not specify a height.
        /// </summary>
        public int CanvasHeight = 0;

        /// <summary>
        /// Display the chart title (rendered by this code, not Chart.js)?
        /// </summary>
        public bool DisplayTitle = true;

        /// <summary>
        /// Display the chart sub-title (rendered by this code, not Chart.js)?
        /// </summary>
        public bool DisplaySubTitle = true;

        /// <summary>
        /// Show the total of all datapoints in the title of the chart? 
        /// 
        /// This is for doughnut and pie charts only, it will be overridden to
        /// false (no matter the value) for any other chart type.
        /// </summary>
        public bool ShowTotalInTitle = false;

        /// <summary>
        /// Display legend (rendered by this code, not Chart.js)?
        /// </summary>
        public bool DisplayLegend = true;

        /// <summary>
        /// Show the total number for each datapoint and percentage relative to 
        /// total of all datapoints in the legend?
        /// 
        /// This is for doughnut and pie charts only, it will be overridden to
        /// false (no matter the value) for any other chart type.
        /// </summary>
        public bool ShowTotalAndPercentageInLegend = false;

        // GLOBAL OPTIONS FOR CHART.JS RENDERING

        /// <summary>
        /// Is the rendered chart responsive?
        /// </summary>
        public bool IsResponsive = true;

        /// <summary>
        /// Should the rendered chart maintain aspect ratio?
        /// </summary>
        public bool MaintainAspectRatio = false;

        /// <summary>
        /// Enable hover tooltips on chart?
        /// </summary>
        public bool EnableTooltips = false;

        /// <summary>
        /// Cutout percentage for doughnut or pie. If pie, it gets defaulted to 2.
        /// </summary>
        public int CutoutPercentage = 50;

        /// <summary>
        /// Animate the scaling?
        /// </summary>
        public bool AnimateScale = true;

        /// <summary>
        /// Animate the rotation (pie/doughnut)?
        /// </summary>
        public bool AnimateRotate = true;

        /// <summary>
        /// Default font family for text rendered inside the chart canvas.
        /// </summary>
        public string DefaultFontFamily = "Raleway";

        /// <summary>
        /// Default font color for text rendered inside the chart canvas.
        /// </summary>
        public string DefaultFontColor = "#777777";

        /// <summary>
        /// Default font size for text rendered inside the chart canvas.
        /// </summary>
        public int DefaultFontSize = 16;

        // DATA OPTIONS SPECIFIC TO LINE CHART
        
        /// <summary>
        /// Fill the area under the line?
        /// </summary>
        public bool FillUnderLine = false;
        #endregion

        #region Private Properties
        /// <summary>
        /// The type of chart.
        /// </summary>
        private ChartType _Type;

        /// <summary>
        /// The data to be charted.
        /// </summary>
        private DataTable _Data;

        /// <summary>
        /// The list control for ChartType.List.
        /// The list structure gets rendered into this object.
        /// </summary>
        private Control _ListControl;

        /// <summary>
        /// The colors of datapoints. They need to be hex values and there needs
        /// to be at least as many colors as there are datapoints/datasets.
        /// </summary>
        private List<string> _Colors;

        /// <summary>
        /// The title of the chart.
        /// </summary>
        private string _Title;

        /// <summary>
        /// The sub-title of the chart.
        /// </summary>
        private string _SubTitle;

        /// <summary>
        /// Used to internally track if all data in the datasets is zero.
        /// This will determine if we draw a legend or not, despite the setting for that.
        /// </summary>
        private bool _IsAllDataZero = true;
        #endregion

        #region Private Methods
        #region _BuildListControl
        private Control _BuildListControl()
        {
            bool dataTableContainsRequiredColumns = false;
            bool getItemNumberFromDataTable = false;
            int itemIndex = 1;

            // ensure the data table contains the required columns of _Title_ and _Total_
            if (this._Data.Columns.Contains("_Title_") && this._Data.Columns.Contains("_Total_"))
            { dataTableContainsRequiredColumns = true; }

            // check if the data table is managing it's own list numbers (_Position_)
            if (this._Data.Columns.Contains("_Position_"))
            { getItemNumberFromDataTable = true; }

            // if the data table has all required columns and has rows, proceed with the list
            // otherwise, write a div indicitaing "no data."
            if (dataTableContainsRequiredColumns && this._Data.Rows.Count > 0)
            {
                HtmlGenericControl orderedList = new HtmlGenericControl("ol");

                foreach (DataRow row in this._Data.Rows)
                {
                    // declare list item, list number, item title, and item total controls
                    HtmlGenericControl listItem = new HtmlGenericControl("li");
                    Label listNumberWrapper = new Label();
                    Panel listNumberContainer = new Panel();
                    Literal listNumber = new Literal();
                    Label itemTitle = new Label();
                    Label itemTotal = new Label();

                    // list number
                    if (getItemNumberFromDataTable)
                    { listNumber.Text = row["_Position_"].ToString(); }
                    else
                    { listNumber.Text = itemIndex.ToString("N0"); }

                    listNumberContainer.Controls.Add(listNumber);
                    listNumberWrapper.Controls.Add(listNumberContainer);
                    listItem.Controls.Add(listNumberWrapper);

                    // title
                    itemTitle.Text = row["_Title_"].ToString();
                    listItem.Controls.Add(itemTitle);

                    // total
                    itemTotal.Text = Convert.ToInt32(row["_Total_"]).ToString("N0");
                    listItem.Controls.Add(itemTotal);

                    // attach the list item
                    orderedList.Controls.Add(listItem);

                    // increment itemIndex
                    itemIndex++;
                }

                // return
                return orderedList;
            }
            else
            {
                // set is data all 0 flag
                this._IsAllDataZero = true;

                // "no data" div
                Panel noDataContainer = new Panel();
                noDataContainer.CssClass = "ChartListNoData";

                Literal noData = new Literal();
                noData.Text = _GlobalResources.NoRecordsFound;
                noDataContainer.Controls.Add(noData);

                // return
                return noDataContainer;
            }
        }
        #endregion

        #region _BuildOptionsJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's options based on the option properties.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildOptionsJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON            
            sb.AppendLine("var " + this.ID + "Options = {");
            sb.AppendLine("    responsive: " + this.IsResponsive.ToString().ToLower() + ",");            
            sb.AppendLine("    maintainAspectRatio: " + this.MaintainAspectRatio.ToString().ToLower() + ",");
            sb.AppendLine("    defaultFontFamily: " + this._FormatJSStringVarValue(this.DefaultFontFamily) + ",");
            sb.AppendLine("    defaultFontColor: " + this._FormatJSStringVarValue(this.DefaultFontColor) + ",");
            sb.AppendLine("    defaultFontSize: " + this.DefaultFontSize.ToString() + ",");
            sb.AppendLine("    tooltips: {");
            sb.AppendLine("        enabled: " + this.EnableTooltips.ToString().ToLower() + "");
            sb.AppendLine("    },");
            sb.AppendLine("    legend: {");
            sb.AppendLine("        display: false");
            sb.AppendLine("    },");
            sb.AppendLine("    animation: {");
            sb.AppendLine("        animateScale: " + this.AnimateScale.ToString().ToLower() + ",");
            sb.AppendLine("        animateRotate: " + this.AnimateRotate.ToString().ToLower() + "");
            sb.AppendLine("    },");

            if (this._Type == ChartType.Doughnut || this._Type == ChartType.Pie)
            { sb.AppendLine("    cutoutPercentage: " + this.CutoutPercentage.ToString() + ","); }

            if (this._Type == ChartType.Bar || this._Type == ChartType.Line)
            {
                sb.AppendLine("    scales:{");
			    sb.AppendLine("     xAxes: [{");
				sb.AppendLine("         ticks: {");
                sb.AppendLine("    	        fontFamily:\"" + this.DefaultFontFamily + "\"");
				sb.AppendLine("         }");
			    sb.AppendLine("     }],");
			    sb.AppendLine("     yAxes: [{");
				sb.AppendLine("         ticks: {");
				sb.AppendLine("    	        fontFamily:\"" + this.DefaultFontFamily + "\"");
				sb.AppendLine("         }");
			    sb.AppendLine("     }]");
		        sb.AppendLine("     },");
            }
            
            sb.AppendLine("}");

            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildDataJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's data based on the datasets popuated for this chart.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildDataJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // begin variable declaration
            sb.AppendLine("var " + this.ID + "Data = {");
            sb.AppendLine(" labels: [");

            // LABELS

            // build labels by looping through dataset columns
            foreach (DataColumn labelCol in this._Data.Columns)
            {
                // _Label_ and _Total_ columns are reserved for other purposes so do not include them in labels
                if (labelCol.ColumnName != "_Label_" && labelCol.ColumnName != "_Total_")
                {
                    // get the column name for the label
                    string columnNameFormatted = labelCol.ColumnName;

                    // if the column name contains replacer markup ##, it means it is meant to be keyed to a 
                    // language resource, so grab that
                    if (labelCol.ColumnName.Contains("##"))
                    {
                        string resourceKey = labelCol.ColumnName.Replace("##", "");
                        columnNameFormatted = _GlobalResources.ResourceManager.GetString(resourceKey, new CultureInfo(AsentiaSessionState.UserCulture));
                    }

                    // if show total and percentage in legend is flagged, and the data table only contains one row (dataset),
                    // grab the data for that column, and calculate percentage relative to _Total_ column
                    if (this.ShowTotalAndPercentageInLegend && this._Data.Rows.Count == 1)
                    {
                        // get the value of the datapoint
                        int datapointTotal = (int)this._Data.Rows[0][labelCol];

                        // add the value to the formatted string
                        columnNameFormatted += ": " + datapointTotal.ToString("N0");

                        // if there is a total column, grab the total so we can get percentage
                        if (this._Data.Columns.Contains("_Total_"))
                        {
                            // calculate percentage
                            int datasetTotal = (int)this._Data.Rows[0]["_Total_"];

                            if (datasetTotal > 0)
                            {
                                double datapointPercentage = ((double)datapointTotal / (double)datasetTotal);

                                // add percentage to the formatted string                                
                                columnNameFormatted += String.Format(" ({0:0%})", datapointPercentage);
                            }
                        }
                    }

                    // attach the label
                    sb.AppendLine("     \"" + columnNameFormatted + "\",");
                }
            }

            sb.AppendLine(" ],");

            // END LABELS

            // DATASET(S)

            sb.AppendLine(" datasets: [");

            // each row is a "dataset", loop through them to build datasets
            // note that most charts will only involve one dataset
            int rowIndex = 0;

            foreach (DataRow dataRow in this._Data.Rows)
            {
                // begin dataset object
                sb.AppendLine("     {");

                // "label" string, if a _Label_ column exists
                if (this._Data.Columns.Contains("_Label_"))
                { sb.AppendLine("     label: \"" + dataRow["_Label_"].ToString() + "\","); }

                // "data" string, loop through columns to compile the data string
                string dataString = "     data: [";
                string datapointValuesString = String.Empty;
                bool isFirstColumn = true;

                foreach (DataColumn dataCol in this._Data.Columns)
                {
                    // _Label_ and _Total_ columns are reserved for other purposes so do not include them data
                    if (dataCol.ColumnName != "_Label_" && dataCol.ColumnName != "_Total_")
                    {
                        // check to see if data is greater than 0, so we can set the flag for isAllDataZero
                        if (Convert.ToInt32(dataRow[dataCol]) > 0)
                        { this._IsAllDataZero = false; }

                        if (isFirstColumn)
                        {
                            datapointValuesString += dataRow[dataCol].ToString();
                            isFirstColumn = false;
                        }
                        else
                        { datapointValuesString += "," + dataRow[dataCol].ToString(); }
                    }
                }

                // if all values are 0 and this is a pie or doughnut chart, use a single value of 100 so the chart is drawn
                // otherwise, draw the chart as normal
                if (this._IsAllDataZero && (this._Type == ChartType.Doughnut || this._Type == ChartType.Pie))
                { dataString += "100],"; }
                else
                { dataString += datapointValuesString + "],"; }
                
                sb.AppendLine(dataString);               

                // data attributes for line charts only - fill, lineTension, borderCapStyle, pointBackgroundColor, pointBorderWidth, pointRadius
                if (this._Type == ChartType.Line)
                {
                    sb.AppendLine("     fill: " + this.FillUnderLine.ToString().ToLower() + ",");
                    sb.AppendLine("     lineTension: 0.1,");                    // hardcoded
                    sb.AppendLine("     borderCapStyle: \"butt\",");            // hardcoded
                    sb.AppendLine("     pointBackgroundColor: \"#FFFFFF\",");   // hardcoded
                    sb.AppendLine("     pointBorderWidth: 2,");                 // hardcoded
                    sb.AppendLine("     pointRadius: 4,");                      // hardcoded
                    sb.AppendLine("     borderWidth: 3,");                      // hardcoded
                    
                    sb.AppendLine("     borderColor: \"" + this._Colors[rowIndex] + "\",");             // dynamic, index of _Colors for THIS row
                    sb.AppendLine("     hoverBorderColor: \"" + this._Colors[rowIndex] + "\",");        // dynamic, same as border
                    sb.AppendLine("     backgroundColor: \"" + this._Colors[rowIndex] + "\",");         // dynamic, same as border
                    sb.AppendLine("     hoverBackgroundColor: \"" + this._Colors[rowIndex] + "\"");     // dynamic, same as border
                }
                else // data attributes for all other charts
                {
                    sb.AppendLine("     borderWidth: 1,");                      // hardcoded
                    sb.AppendLine("     borderColor: \"#D1D1D1\",");            // hardcoded
                    sb.AppendLine("     hoverBorderColor: \"#D1D1D1\",");       // hardcoded

                    // backgroundColor and hoverBackgroundColor are an array of _Colors, and there should be as 
                    // many items in _Colors as there are datapoints in the dataset
                    // if all values are 0 and this is a pie or doughnut chart, use a single color of gray 
                    // otherwise, add colors as normal

                    if (this._IsAllDataZero && (this._Type == ChartType.Doughnut || this._Type == ChartType.Pie))
                    {
                        sb.AppendLine("     backgroundColor: [\"#D1D1D1\"],");
                        sb.AppendLine("     hoverBackgroundColor: [\"#D1D1D1\"],");
                    }
                    else
                    {
                        // backgroundColor
                        string backgroundColorString = "     backgroundColor: [";
                        isFirstColumn = true;

                        foreach (string backgroundColor in this._Colors)
                        {
                            if (isFirstColumn)
                            {
                                backgroundColorString += "\"" + backgroundColor + "\"";
                                isFirstColumn = false;
                            }
                            else
                            { backgroundColorString += ", \"" + backgroundColor + "\""; }
                        }

                        backgroundColorString += "],";
                        sb.AppendLine(backgroundColorString);

                        // hoverBackgroundColor
                        string hoverBackgroundColorString = "     hoverBackgroundColor: [";
                        isFirstColumn = true;

                        foreach (string hoverBackgroundColor in this._Colors)
                        {
                            if (isFirstColumn)
                            {
                                hoverBackgroundColorString += "\"" + hoverBackgroundColor + "\"";
                                isFirstColumn = false;
                            }
                            else
                            { hoverBackgroundColorString += ", \"" + hoverBackgroundColor + "\""; }
                        }

                        hoverBackgroundColorString += "],";
                        sb.AppendLine(hoverBackgroundColorString);
                    }
                }

                // end dataset object
                sb.AppendLine("     },");

                // increment the rowIndex
                rowIndex++;
            }

            sb.AppendLine(" ]");

            // END DATASET(S)

            sb.AppendLine("};");

            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildAndRegisterJSBlock
        /// <summary>
        /// Builds the JS block that renders the chart, and registers the
        /// JS to this control.
        /// </summary>
        private void _BuildAndRegisterJSBlock()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON
            sb.AppendLine(this._BuildOptionsJSONString());

            // build data JSON
            sb.AppendLine(this._BuildDataJSONString());

            // build call to chart function
            sb.AppendLine(" var " + this.ID + "Bound = new Chart(document.getElementById(\"" + this.ID + "Canvas\").getContext(\"2d\"), { type: \"" + Enum.GetName(typeof(ChartType), this._Type).ToLower() + "\", data: " + this.ID + "Data, options: " + this.ID + "Options });");

            // if we are displaying a legend for the chart, add call to generate it
            if (this.DisplayLegend && !this._IsAllDataZero)
            { sb.AppendLine(" document.getElementById(\"" + this.ID + "LegendContainer\").innerHTML = " + this.ID + "Bound.generateLegend();"); }

            // register script
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), this.ID + "StartUpScript", sb.ToString(), true);
        }
        #endregion

        #region _FormatJSStringVarValue
        /// <summary>
        /// Formats a string for attachment to a JS variable.
        /// </summary>
        /// <param name="value">string value</param>
        /// <returns>formatted string</returns>
        private string _FormatJSStringVarValue(string value)
        {
            if (value == null)
            { return "null"; }
            else
            { return "\"" + value + "\""; }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Chart.2.4.min.js");
        }
        #endregion

        #region Render
        /// <summary>
        /// This is overridden so that the control doesn't render inside of a <span> tag.
        /// Only the contents of the control are rendered.
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        { 
            this.RenderContents(writer);
        }
        #endregion

        #region RenderContents
        protected override void RenderContents(HtmlTextWriter writer)
        {
            // if this is a list, build the list control
            // otherwise, it's a chart, so build the chart js block
            if (this._Type == ChartType.List)
            { this._ListControl = this._BuildListControl(); }
            else
            { this._BuildAndRegisterJSBlock(); }

            // wrapper container
            Panel wrapperContainer = new Panel();
            wrapperContainer.ID = this.ID + "WrapperContainer";

            if (this.DisplayLegend && !this._IsAllDataZero && this._Type != ChartType.List)
            { wrapperContainer.CssClass = "ChartWithLegendWrapper"; }
            else
            { wrapperContainer.CssClass = "ChartWrapper"; }

            // title & sub-title
            if (this.DisplayTitle && !String.IsNullOrWhiteSpace(this._Title))
            {
                // title wrapper container
                Panel titleWrapperContainer = new Panel();
                titleWrapperContainer.ID = this.ID + "TitleWrapperContainer";
                titleWrapperContainer.CssClass = "ChartTitleWrapper";
                wrapperContainer.Controls.Add(titleWrapperContainer);

                // title container
                Panel titleContainer = new Panel();
                titleContainer.ID = this.ID + "TitleContainer";
                titleContainer.CssClass = "ChartTitle";
                titleWrapperContainer.Controls.Add(titleContainer);

                // title text
                Literal title = new Literal();
                title.Text = this._Title;

                // if show total in title is flagged, the data table only contains one row (dataset),
                // there is a _Total_ column, and it is not a list, grab its value
                if (this.ShowTotalInTitle && this._Data.Rows.Count == 1 && this._Data.Columns.Contains("_Total_") && this._Type != ChartType.List)
                { title.Text += " (" + Convert.ToInt32(this._Data.Rows[0]["_Total_"]).ToString("N0") + ")"; }

                titleContainer.Controls.Add(title);

                // sub-title
                if (this.DisplaySubTitle && !String.IsNullOrWhiteSpace(this._SubTitle))
                {
                    // sub-title container
                    Panel subTitleContainer = new Panel();
                    subTitleContainer.ID = this.ID + "SubTitleContainer";
                    subTitleContainer.CssClass = "ChartSubTitle";
                    titleWrapperContainer.Controls.Add(subTitleContainer);

                    // sub-title text
                    Literal subTitle = new Literal();
                    subTitle.Text = this._SubTitle;
                    subTitleContainer.Controls.Add(subTitle);
                }
            }

            // LIST OR CANVAS

            if (this._Type == ChartType.List)
            {
                // list container
                Panel listContainer = new Panel();
                listContainer.ID = this.ID + "ListContainer";
                listContainer.CssClass = "ChartListContainer";
                wrapperContainer.Controls.Add(listContainer);

                listContainer.Controls.Add(this._ListControl);
            }
            else  // if it is not a list, then we draw a canvas tag for rendering
            {
                // canvas
                Panel canvasContainer = new Panel();
                canvasContainer.ID = this.ID + "CanvasContainer";
                canvasContainer.CssClass = "ChartCanvasContainer";
                wrapperContainer.Controls.Add(canvasContainer);

                // canvas tag
                HtmlGenericControl canvasTag = new HtmlGenericControl("canvas");
                canvasTag.ID = this.ID + "Canvas";

                // if a width and height are specified for the canvas, set them
                if (this.CanvasWidth > 0 && this.CanvasHeight > 0)
                {
                    canvasTag.Style.Add("max-width", this.CanvasWidth.ToString() + "px");
                    canvasTag.Style.Add("max-height", this.CanvasHeight.ToString() + "px");
                }

                canvasContainer.Controls.Add(canvasTag);
            }

            // legend
            if (this.DisplayLegend && !this._IsAllDataZero && this._Type != ChartType.List)
            {
                Panel legendContainer = new Panel();
                legendContainer.ID = this.ID + "LegendContainer";
                legendContainer.CssClass = "ChartLegendContainer";
                wrapperContainer.Controls.Add(legendContainer);
            }

            // attach wrapper container to control
            this.Controls.Add(wrapperContainer);

            // execute base
            base.RenderContents(writer);
        }
        #endregion
        #endregion
    }
}
