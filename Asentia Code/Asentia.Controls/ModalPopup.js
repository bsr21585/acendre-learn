﻿/* GLOBAL VARIABLES */
var _ModalPostbackUpdatePanelId = null;

/* ON LOAD */
/* 
Sets the methods to be called on the beginning of an async postback
and the end of an async postback.
*/
$(document).ready(function () {
    // Get the instance of PageRequestManager
    var RequestManager = Sys.WebForms.PageRequestManager.getInstance();

    // Add initializeRequest and endRequest
    RequestManager.add_initializeRequest(ModalPostbackInitializeRequest);
    RequestManager.add_endRequest(ModalPostbackEndRequest);
});


/*
METHOD: ModalPostbackInitializeRequest
Method to handle the beginning of an async postback from a modal.
This method gets the target update panel of the async postback
so that we can show a loader in the modal while its loading.
*/
function ModalPostbackInitializeRequest(sender, args) {

    try {
        var UpdatePanelIds = args.get_updatePanelsToUpdate();

        // first, see if the panel to update has been identified
        // if so, use it to obtain the update panel id
        if (UpdatePanelIds.length == 1) {
            var UpdatePanelRawId = UpdatePanelIds[0];
            _ModalPostbackUpdatePanelId = Helper.GetElementStaticId(UpdatePanelRawId);
        }

        // only do this if the update panel is from a modal
        if (_ModalPostbackUpdatePanelId.indexOf("Modal") > -1) {
            // loop through all child elements of the update panel, showing the
            // loading placeholder and hiding the rest
            $("#" + _ModalPostbackUpdatePanelId).children().each(function () {
                if (this.id.indexOf("Header") > -1 || this.id.indexOf("PostbackLoadingPlaceholder") > -1) {
                    $("#" + this.id).show();
                }
                else {
                    $("#" + this.id).hide();
                }
            });
        }
    }
    catch (e) {
        // do nothing on error, die quietly
    }
}

/*
METHOD: ModalPostbackEndRequest
Method to handle the end of an async postback from a modal.
This method gets the target update panel of the async postback
so that we can show show the content and hide the loader.
*/
function ModalPostbackEndRequest(sender, args) {

    try {
        // only do this if the update panel is from a modal
        if (_ModalPostbackUpdatePanelId.indexOf("Modal") > -1) {
            // loop through all child elements of the panel, hiding the
            // loading placeholder and showing the rest
            $("#" + _ModalPostbackUpdatePanelId).children().each(function () {
                if (this.id.indexOf("PostbackLoadingPlaceholder") > -1) {
                    $("#" + this.id).hide();
                }
                else {
                    $("#" + this.id).show();
                }
            });
        }
    }
    catch (e) {
        // do nothing on error, die quietly
    }
}