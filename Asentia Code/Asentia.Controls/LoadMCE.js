﻿function LoadMCE() {
    if (CurrentPageLanguage != "en-US" && CurrentPageLanguage != "") {
        tinyMCE.init({
            selector: "textarea.MCE",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "media table contextmenu paste textcolor"
            ],
            toolbar: "undo redo | styleselect fontselect fontsizeselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            encoding: "xml",
            language: CurrentPageLanguage
        });
    }
    else {
        tinyMCE.init({
            selector: "textarea.MCE",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "media table contextmenu paste textcolor"
            ],
            toolbar: "undo redo | styleselect fontselect fontsizeselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            encoding: "xml"
        });
    }
}