﻿using System;
using System.ComponentModel;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using AjaxControlToolkit;

namespace Asentia.Controls
{
    #region ModalPopupType
    public enum ModalPopupType
    {
        /// <summary>
        /// Information.
        /// Used to display information.
        /// </summary>
        [Description("Information")]
        Information = 0,

        /// <summary>
        /// Confirm.
        /// Used to confirm an action on a page, i.e. delete objects.
        /// </summary>
        [Description("Confirm")]
        Confirm = 1,

        /// <summary>
        /// Form.
        /// Used as a complete form with inputs, and feedback upon submission.
        /// </summary>
        [Description("Form")]
        Form = 2,

        /// <summary>
        /// Embedded Form.
        /// Used as a form that is already embedded in the modal with its own buttons and feedback panel.
        /// </summary>
        [Description("EmbeddedForm")]
        EmbeddedForm = 3,
    }
    #endregion

    #region ModalPopupButtonText
    public enum ModalPopupButtonText
    {
        /// <summary>
        /// Submit.
        /// </summary>
        [Description("Submit")]
        Submit = 0,

        /// <summary>
        /// Close.
        /// </summary>
        [Description("Close")]
        Close = 1,

        /// <summary>
        /// Yes.
        /// </summary>
        [Description("Yes")]
        Yes = 2,

        /// <summary>
        /// No.
        /// </summary>
        [Description("No")]
        No = 3,

        /// <summary>
        /// Cancel.
        /// </summary>
        [Description("Cancel")]
        Cancel = 4,

        /// <summary>
        /// Ok.
        /// </summary>
        [Description("Ok")]
        Ok = 5,

        /// <summary>
        /// Custom.
        /// </summary>
        [Description("Custom")]
        Custom = 6,
    }
    #endregion

    public class ModalPopup : WebControl, INamingContainer
    {
        #region Constructors
        public ModalPopup()
            : base(HtmlTextWriterTag.Div)
        {
            // set the css class for the modal's container
            this.CssClass = "ModalPopupContainer";
            this.Style.Value = "display: none;"; // this needs to be set so that the modal doesn't "flicker" on page load in IE
            
            // define IDs of main controls to be rendered here to ensure scope
            this._MPE.ID = "ModalPopupExtender";
            this._UpdatePanel.ID = "ModalPopupUpdatePanel";
            this._Header.ID = "ModalPopupHeader";
            this._Header.CssClass = "ModalPopupHeader";
            this._PostbackLoadingPlaceholder.ID = "ModalPopupPostbackLoadingPlaceholder";
            this._PostbackLoadingPlaceholder.CssClass = "ModalPopupPostbackLoadingPlaceholder";
            this._FeedbackContainer.ID = "ModalPopupFeedbackContainer";
            this._Body.ID = "ModalPopupBody";
            this._Body.CssClass = "ModalPopupBody";
            this._Separator.ID = "ModalPopupSeparator";
            this._Separator.CssClass = "ModalPopupSeparator";
            this._Buttons.ID = "ModalPopupButtons";
            this._Buttons.CssClass = "ModalPopupButtons";
            this.SubmitButton.ID = "ModalPopupSubmitButton";
            this.CloseButton.ID = "ModalPopupCloseButton";
        }

        public ModalPopup(string id)
            : base(HtmlTextWriterTag.Div)
        {
            // set the css class for the modal's container
            this.CssClass = "ModalPopupContainer";
            this.Style.Value = "display: none;"; // this needs to be set so that the modal doesn't "flicker" on page load in IE
            
            // define IDs of main controls to be rendered here to ensure scope
            this.ID = id;
            this._MPE.ID = id + "ModalPopupExtender";
            this._UpdatePanel.ID = id + "ModalPopupUpdatePanel";
            this._Header.ID = id + "ModalPopupHeader";
            this._Header.CssClass = "ModalPopupHeader";
            this._PostbackLoadingPlaceholder.ID = id + "ModalPopupPostbackLoadingPlaceholder";
            this._PostbackLoadingPlaceholder.CssClass = "ModalPopupPostbackLoadingPlaceholder";
            this._FeedbackContainer.ID = id + "ModalPopupFeedbackContainer";
            this._Body.ID = id + "ModalPopupBody";
            this._Body.CssClass = "ModalPopupBody";
            this._Separator.ID = id + "ModalPopupSeparator";
            this._Separator.CssClass = "ModalPopupSeparator";
            this._Buttons.ID = id + "ModalPopupButtons";
            this._Buttons.CssClass = "ModalPopupButtons";
            this.SubmitButton.ID = id + "ModalPopupSubmitButton";
            this.CloseButton.ID = id + "ModalPopupCloseButton";
        }
        #endregion

        #region Properties
        /// <summary>
        /// The type of modal popup this is.
        /// Default is "Information"
        /// </summary>
        public ModalPopupType Type = ModalPopupType.Information;

        /// <summary>
        /// The ID of the control that launches this modal popup.
        /// Must be set for the modal to be able to be launched.
        /// </summary>
        public string TargetControlID = null;

        /// <summary>
        /// The ID of the control that is to gain focus when this modal popup is launched.
        /// Set this if you want a control inside the modal to be in focus when the modal
        /// is launched.
        /// </summary>
        public string FocusControlIdOnLaunch = null;

        /// <summary>
        /// Determines whether or not to show a close icon in the upper right corner.
        /// </summary>
        public bool ShowCloseIcon = true;

        /// <summary>
        /// Determines whether or not to show loader.
        /// </summary>
        public bool ShowLoadingPlaceholder = false;

        /// <summary>
        /// The path to the icon image to be used in the modal header.
        /// This is optional.
        /// </summary>
        public string HeaderIconPath = null;

        /// <summary>
        /// The Alt text for the icon image.
        /// This is optional, but if an icon image is used, this must
        /// be set to comply with 508.
        /// </summary>
        public string HeaderIconAlt = null;

        /// <summary>
        /// The text to display in the modal header.
        /// Though not required, the header will be blank without it.
        /// </summary>
        public string HeaderText = null;

        /// <summary>
        /// The text type of the submit button.
        /// This is optional, because if this isn't set, the default 
        /// text type for the type of modal this is will be used. 
        /// This can be set to "Custom" to display custom text on the
        /// button.
        /// </summary>
        public ModalPopupButtonText? SubmitButtonTextType = null;

        /// <summary>
        /// The custom text for the submit button.
        /// This only needs to be set if the text type is set to custom.
        /// </summary>
        public string SubmitButtonCustomText = null;

        /// <summary>
        /// load images from customar manager website project if True (Added By Chetu)
        /// </summary>
        public bool OverrideLoadImagesFromCustomerManager = false;

        /// <summary>
        /// The text type of the close button.
        /// This is optional, because if this isn't set, the default 
        /// text type for the type of modal this is will be used. 
        /// This can be set to "Custom" to display custom text on the
        /// button.
        /// </summary>
        public ModalPopupButtonText? CloseButtonTextType = null;

        /// <summary>
        /// The custom text for the close button.
        /// This only needs to be set if the text type is set to custom.
        /// </summary>
        public string CloseButtonCustomText = null;

        /// <summary>
        /// Determines whether or not to reload the page on Modal close.
        /// </summary>
        public bool ReloadPageOnClose = false;

        /// <summary>
        /// The submit button.
        /// This is public so that it's Command property can be set from
        /// the instansiating code.
        /// </summary>
        public Button SubmitButton = new Button();

        /// <summary>
        /// The close button.
        /// </summary>
        public Button CloseButton = new Button();

        /// <summary>
        /// Use UpdateMode.Conditional for the modal's UpdatePanel?
        /// </summary>
        public bool UseConditionalUpdateModeForUpdatePanel = false;
        #endregion

        #region Private Properties
        /// <summary>
        /// Modal Popup Extender from the AjaxToolkit.
        /// This is what makes the modal function.
        /// </summary>
        private ModalPopupExtender _MPE = new ModalPopupExtender();
        
        /// <summary>
        /// Update Panel that serves as a wrapper when the modal type
        /// is "Form." This allows the modal to stay launched, and its
        /// contents changed to provide feedback upon submission of the
        /// form by doing an asynchronous postback.
        /// 
        /// Note that the method attached to the command event submit 
        /// button cannot have a Response.Write in it or it will break
        /// the functionality of the Update Panel.
        /// </summary>
        private UpdatePanel _UpdatePanel = new UpdatePanel();

        /// <summary>
        /// Container for the modal's header.
        /// </summary>
        private Panel _Header = new Panel();

        /// <summary>
        /// Container for "loading" message on modal postback.
        /// </summary>
        private Panel _PostbackLoadingPlaceholder = new Panel();

        /// <summary>
        /// Container that holds feedback provided upon submission.
        /// Only used in a "Form" modal.
        /// </summary>
        private Panel _FeedbackContainer = new Panel();

        /// <summary>
        /// Container for the modal's body.
        /// </summary>
        private Panel _Body = new Panel();

        /// <summary>
        /// Container that serves as a separator between the modal's
        /// body and buttons.
        /// </summary>
        private Panel _Separator = new Panel();

        /// <summary>
        /// Container that holds the modal's buttons.
        /// </summary>
        private Panel _Buttons = new Panel();
        #endregion

        #region Methods
        #region AddControlToBody
        /// <summary>
        /// Method to add a control to the _Body container.
        /// </summary>
        /// <param name="control">control to be added</param>
        public void AddControlToBody(Control control)
        {
            this._Body.Controls.Add(control);
        }
        #endregion

        #region AddControlToButtonsContainer
        /// <summary>
        /// Method to add a control to the _Buttons container.
        /// </summary>
        /// <param name="control">control to be added</param>
        public void AddControlToButtonsContainer(Control control)
        {
            this._Buttons.Controls.Add(control);
        }
        #endregion

        #region ClearBodyControls
        /// <summary>
        /// Method to remove all controls from the _Body container.
        /// </summary>
        public void ClearBodyControls()
        {
            this._Body.Controls.Clear();
        }
        #endregion

        #region DisableSubmitButton
        /// <summary>
        /// Disables the submit button of this modal.
        /// </summary>
        public void DisableSubmitButton()
        {
            this.SubmitButton.CssClass = "Button ActionButton DisabledButton";
            this.SubmitButton.Enabled = false;
        }
        #endregion

        #region ClearFeedback
        /// <summary>
        /// Clears out the _Feedback container.
        /// </summary>
        public void ClearFeedback()
        {
            this._FeedbackContainer.CssClass = "";
            this._FeedbackContainer.Controls.Clear();
            this._FeedbackContainer.Visible = false;
        }
        #endregion

        #region DisplayFeedback
        /// <summary>
        /// Method to populate the _Feedback container with a success, or error message.
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="isError">determines if this message is an error message</param>
        public void DisplayFeedback(string message, bool isError)
        {
            // clear the container
            this._FeedbackContainer.Controls.Clear();

            // build and attach info status panel
            InformationStatusPanel infoStatusPanel;

            if (isError)
            {
                infoStatusPanel = new InformationStatusPanel(this.ID + "FeedbackInformationStatusPanel", InformationStatusPanel_Type.OnPage, InformationStatusPanel_MessageType.Error, message.Replace(Environment.NewLine, "<br />"));
            }
            else
            {
                infoStatusPanel = new InformationStatusPanel(this.ID + "FeedbackInformationStatusPanel", InformationStatusPanel_Type.OnPage, InformationStatusPanel_MessageType.Success, message.Replace(Environment.NewLine, "<br />"));
            }

            this._FeedbackContainer.Controls.Add(infoStatusPanel);

            // make container visible
            this._FeedbackContainer.Visible = true;
        }

        /// <summary>
        /// Overloaded method to populate the _Feedback container with a defined message type.
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="messageType">message type</param>
        public void DisplayFeedback(string message, InformationStatusPanel_MessageType messageType)
        {
            // clear the container
            this._FeedbackContainer.Controls.Clear();

            // build and attach info status panel
            InformationStatusPanel infoStatusPanel;

            infoStatusPanel = new InformationStatusPanel(this.ID + "FeedbackInformationStatusPanel", InformationStatusPanel_Type.OnPage, messageType, message.Replace(Environment.NewLine, "<br />"));

            this._FeedbackContainer.Controls.Add(infoStatusPanel);

            // make container visible
            this._FeedbackContainer.Visible = true;
        }
        #endregion

        #region RemoveControlFromBody
        /// <summary>
        /// Method to remove a control from the _Body container.
        /// </summary>
        /// <param name="control">control to be removed</param>
        public void RemoveControlFromBody(Control control)
        {
            this._Body.Controls.Remove(control);
        }
        #endregion

        #region RemoveControlFromBodyById
        /// <summary>
        /// Method to remove a control from the _Body container by its ID.
        /// </summary>
        /// <param name="controlId">id of the control to be removed</param>
        public void RemoveControlFromBodyById(string controlId)
        {
            foreach (Control control in this._Body.Controls)
            {
                if (control.ID == controlId)
                { this._Body.Controls.Remove(control); }
            }
        }
        #endregion

        #region ShowModal
        /// <summary>
        /// Public method to show the modal using the Show method of the private ModalPopupExtender _MPE.
        /// </summary>
        public void ShowModal()
        { this._MPE.Show(); }
        #endregion

        #region HideModal
        /// <summary>
        /// Public method to hide the modal using the Show method of the private ModalPopupExtender _MPE.
        /// </summary>
        public void HideModal()
        { this._MPE.Hide(); }
        #endregion
        #endregion

        #region Private Methods
        #region _ApplyButtonText
        /// <summary>
        /// Method to apply text to a button.
        /// </summary>
        /// <param name="textType">type of text to be applied</param>
        /// <param name="customText">string representing custom text, if applicable</param>
        /// <returns>button text string</returns>
        private string _ApplyButtonText(ModalPopupButtonText? textType, string customText)
        {
            // return text based on the text type of the button
            switch (textType)
            {
                case ModalPopupButtonText.Submit:
                    return _GlobalResources.Submit;
                case ModalPopupButtonText.Close:
                    return _GlobalResources.Close;
                case ModalPopupButtonText.Yes:
                    return _GlobalResources.Yes;
                case ModalPopupButtonText.No:
                    return _GlobalResources.No;
                case ModalPopupButtonText.Cancel:
                    return _GlobalResources.Cancel;
                case ModalPopupButtonText.Ok:
                    return _GlobalResources.OK;
                case ModalPopupButtonText.Custom:
                    return customText;
                default:
                    return null;
            }
        }
        #endregion

        #region _BuildPostbackLoadingPlaceholder
        /// <summary>
        /// Builds a "loading" placeholder to display while a modal UpdatePanel
        /// is waiting for postback action.
        /// </summary>
        private void _BuildPostbackLoadingPlaceholder()
        {
            // set display to none for the loading placeholder
            this._PostbackLoadingPlaceholder.Style.Add("display", "none");
            this._PostbackLoadingPlaceholder.CssClass = "LoadingPlaceholder";

            // loading icon
            HtmlGenericControl contentLoadingImageWrapper = new HtmlGenericControl("p");
            contentLoadingImageWrapper.ID = this._PostbackLoadingPlaceholder.ID + "ContentLoadingImageWrapper";

            Image contentLoadingImage = new Image();
            contentLoadingImage.ID = this._PostbackLoadingPlaceholder.ID + "ContentLoadingImage";
            contentLoadingImage.AlternateText = _GlobalResources.Loading;
            contentLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING,
                                                                  ImageFiles.EXT_GIF, this.OverrideLoadImagesFromCustomerManager);
            contentLoadingImage.CssClass = "MediumIcon";

            contentLoadingImageWrapper.Controls.Add(contentLoadingImage);
            this._PostbackLoadingPlaceholder.Controls.Add(contentLoadingImageWrapper);

            // loading text
            HtmlGenericControl contentLoadingTextWrapper = new HtmlGenericControl("p");
            contentLoadingTextWrapper.ID = this._PostbackLoadingPlaceholder.ID + "ContentLoadingTextWrapper";

            Localize contentLoadingText = new Localize();
            contentLoadingText.Text = _GlobalResources.Loading;

            contentLoadingTextWrapper.Controls.Add(contentLoadingText);
            this._PostbackLoadingPlaceholder.Controls.Add(contentLoadingTextWrapper);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.ModalPopup.js");

            // if there is a control to give focus to when modal is launched, write the function for it
            if (!String.IsNullOrWhiteSpace(this.FocusControlIdOnLaunch))
            {
                // write the function that sets the focus
                StringBuilder setFocusScript = new StringBuilder();
                setFocusScript.AppendLine("function SetFocusForModal_" + this._MPE.ID + "() { ");
                setFocusScript.AppendLine(" $get(\"" + this.FocusControlIdOnLaunch + "\").focus();");
                setFocusScript.AppendLine("}");

                // register it
                csm.RegisterClientScriptBlock(typeof(Asentia.Controls.ClientScript), "SetFocusForModal_" + this._MPE.ID, setFocusScript.ToString(), true);
            }

            // write the function that automatically re-centers the modal "on shown"
            StringBuilder setReCenterScript = new StringBuilder();
            setReCenterScript.AppendLine("function SetReCenterOnShownForModal_" + this._MPE.ID + "() { ");
            setReCenterScript.AppendLine(" $find(\"" + this._MPE.ID + "\")._layout();");
            setReCenterScript.AppendLine("}");

            // register it
            csm.RegisterClientScriptBlock(typeof(Asentia.Controls.ClientScript), "SetReCenterOnShownForModal_" + this._MPE.ID, setReCenterScript.ToString(), true);

            // for "start up" scripts, we need to build start up calls and add them to the Page_Load
            StringBuilder startUpScript = new StringBuilder();
            startUpScript.AppendLine("Sys.Application.add_load(");
            startUpScript.AppendLine("function() { ");
            startUpScript.AppendLine(" var modalPopup_" + this._MPE.ID + " = $find(\"" + this._MPE.ID + "\");");

            // control focus "on shown"
            if (!String.IsNullOrWhiteSpace(this.FocusControlIdOnLaunch))
            { startUpScript.AppendLine(" modalPopup_" + this._MPE.ID + ".add_shown(SetFocusForModal_" + this._MPE.ID + ");"); }

            // re-center "on shown"
            startUpScript.AppendLine(" modalPopup_" + this._MPE.ID + ".add_shown(SetReCenterOnShownForModal_" + this._MPE.ID + ");");

            // re-center "on body change" - call _layout twice due to bug in ASP.NET AJAX
            startUpScript.AppendLine(" $(\"#" + this._Body.ID + "\").bind(\"DOMNodeInserted DOMNodeRemoved\", function() { setTimeout(function() { $find(\"" + this._MPE.ID + "\")._layout(); $find(\"" + this._MPE.ID + "\")._layout(); }, 100); });");

            startUpScript.AppendLine("});");

            // add start up script
            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "Modal_Load_" + this.ID, startUpScript.ToString(), true);

            // execute the base
            base.OnPreRender(e);
        }
        #endregion

        #region CreateChildControls
        protected override void CreateChildControls()
        {
            // CLOSE IMAGE ICON FOR TOP RIGHT CORNER
            if (this.ShowCloseIcon)
            {
                Image exitIcon = new Image();
                exitIcon.ID = this.ID + "ModalCloseIcon";
                exitIcon.CssClass = "ModalPopupCloseIcon MediumIcon";
                exitIcon.AlternateText = _GlobalResources.CloseWindow;
                exitIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE_MODAL, ImageFiles.EXT_PNG, this.OverrideLoadImagesFromCustomerManager);
                
                if (this.ReloadPageOnClose)
                { exitIcon.Attributes.Add("onClick", "javascript:window.location = window.location.href;"); }
                else
                { exitIcon.Attributes.Add("onClick", "javascript:$find('" + this._MPE.ID + "').hide(); return false;"); }

                this.Controls.Add(exitIcon); 
            }

            // MODAL POPUP EXTENDER
            this._MPE.TargetControlID = this.TargetControlID;
            this._MPE.PopupControlID = this.ID;
            this._MPE.BackgroundCssClass = "ModalPopupBackground";

            // HEADER
            HtmlGenericControl headerWrapper = new HtmlGenericControl("p");
            headerWrapper.ID = this.ID + "ModalPopupHeaderWrapper";

            // icon
            if (!String.IsNullOrWhiteSpace(this.HeaderIconPath))
            {
                Image headerIcon = new Image();
                headerIcon.ID = this.ID + "ModalPopupHeaderIcon";
                headerIcon.AlternateText = this.HeaderIconAlt;
                headerIcon.ImageUrl = this.HeaderIconPath;
                headerIcon.CssClass = "ModalHeaderIcon LargeIcon";

                headerWrapper.Controls.Add(headerIcon);
            }

            // text
            Label headerText = new Label();
            headerText.ID = this.ID + "ModalPopupHeaderText";
            headerText.Text = this.HeaderText;
            headerWrapper.Controls.Add(headerText);

            // add control
            this._Header.Controls.Add(headerWrapper);

            // BUTTONS

            // there is always a close button, so prepare it first
            this.CloseButton.CssClass = "Button NonActionButton";
            
            if (this.ReloadPageOnClose)
            { this.CloseButton.OnClientClick = "javascript:window.location = window.location.href;"; }
            else
            { this.CloseButton.OnClientClick = "javascript:$find('" + this._MPE.ID + "').hide(); return false;"; }

            // apply buttons based on what type of modal popup this is and what the text is defined as
            switch (this.Type)
            {
                case ModalPopupType.Confirm:
                    // submit
                    this.SubmitButton.CssClass = "Button ActionButton SaveButton";

                    if (this.SubmitButtonTextType == null)
                    { this.SubmitButton.Text = _GlobalResources.Yes; }
                    else
                    { this.SubmitButton.Text = this._ApplyButtonText(this.SubmitButtonTextType, this.SubmitButtonCustomText); }

                    // close
                    if (this.CloseButtonTextType == null)
                    { this.CloseButton.Text = _GlobalResources.No; }
                    else
                    { this.CloseButton.Text = this._ApplyButtonText(this.CloseButtonTextType, this.CloseButtonCustomText); }

                    // make this submit button the default button for the body panel
                    this._Body.DefaultButton = this.SubmitButton.ID;

                    // add buttons
                    this._Buttons.Controls.Add(this.SubmitButton);
                    this._Buttons.Controls.Add(this.CloseButton);

                    break;
                case ModalPopupType.Form:
                    // submit
                    this.SubmitButton.CssClass = "Button ActionButton SaveButton";

                    if (this.SubmitButtonTextType == null)
                    { this.SubmitButton.Text = _GlobalResources.Submit; }
                    else
                    { this.SubmitButton.Text = this._ApplyButtonText(this.SubmitButtonTextType, this.SubmitButtonCustomText); }

                    // close
                    if (this.CloseButtonTextType == null)
                    { this.CloseButton.Text = _GlobalResources.Close; }
                    else
                    { this.CloseButton.Text = this._ApplyButtonText(this.CloseButtonTextType, this.CloseButtonCustomText); }

                    // make this submit button the default button for the body panel
                    this._Body.DefaultButton = this.SubmitButton.ID;

                    // add buttons
                    this._Buttons.Controls.Add(this.SubmitButton);
                    this._Buttons.Controls.Add(this.CloseButton);

                    break;
                case ModalPopupType.Information:
                    // NO submit

                    // NO close - lets do this for now

                    // close
                    //if (this.CloseButtonTextType == null)
                    //{ this.CloseButton.Text = _GlobalResources.Close; }
                    //else
                    //{ this.CloseButton.Text = this._ApplyButtonText(this.CloseButtonTextType, this.CloseButtonCustomText); }

                    // add buttons
                    //this._Buttons.Controls.Add(this.CloseButton);

                    break;
                case ModalPopupType.EmbeddedForm:
                    // NO BUTTONS

                    break;
            }

            // ADD CONTROLS TO MODAL
            // Note that the _Body control's contents are defined and added in the instansiating code.

            // if this is a "Form" modal, add the content controls to the Update Panel,
            // then add the Update Panel to this modal
            if (this.Type == ModalPopupType.Form)
            {
                // determine the update mode of the update panel, default is Always
                if (this.UseConditionalUpdateModeForUpdatePanel)
                { this._UpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional; }
                else
                { this._UpdatePanel.UpdateMode = UpdatePanelUpdateMode.Always; }

                // make the submit button an AsyncPostBackTrigger for the Update Panel
                AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
                trigger.ControlID = this.SubmitButton.ID;
                trigger.EventName = "Click";
                this._UpdatePanel.Triggers.Add(trigger);

                // add content controls to Update Panel
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._Header);
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._FeedbackContainer);
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._Body);
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._Separator);
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._Buttons);

                // build and attach the postback loading placeholder
                this._BuildPostbackLoadingPlaceholder();
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._PostbackLoadingPlaceholder);

                // add the Update Panel to this modal
                this.Controls.Add(_UpdatePanel);
            }
            // if this is a "Embedded Form" modal, add the content controls to the Update Panel,
            // then add the Update Panel to this modal
            else if (this.Type == ModalPopupType.EmbeddedForm)
            {
                // always update the Update Panel on postback
                this._UpdatePanel.UpdateMode = UpdatePanelUpdateMode.Always;

                // add content controls to Update Panel
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._Header);
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._Body);
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._Separator);
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._Buttons);

                // build and attach the postback loading placeholder
                this._BuildPostbackLoadingPlaceholder();
                this._UpdatePanel.ContentTemplateContainer.Controls.Add(this._PostbackLoadingPlaceholder);

                // add the Update Panel to this modal
                this.Controls.Add(_UpdatePanel);
            }
            else
            {
                // add content controls directly to this modal
                this.Controls.Add(this._Header);
                this.Controls.Add(this._FeedbackContainer);
                this.Controls.Add(this._Body);
                this.Controls.Add(this._Separator);
                this.Controls.Add(this._Buttons);

                //If flag is true then build the loader inside modal
                if (this.ShowLoadingPlaceholder)
                {
                    this._BuildPostbackLoadingPlaceholder();
                    this.Controls.Add(this._PostbackLoadingPlaceholder);
                }
            }

            // add the modal popup extender directly to this modal
            // it must exist outside of the Update Panel, if there is one
            this.Controls.Add(this._MPE);

            // continue executing base method
            base.CreateChildControls();
        }
        #endregion
        #endregion
    }
}
