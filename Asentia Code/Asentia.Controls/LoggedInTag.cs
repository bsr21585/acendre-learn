﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    public class LoggedInTag : Panel
    {
        #region OnInit
        protected override void OnInit(EventArgs e)
        {
            // disable viewstate
            this.ViewStateMode = ViewStateMode.Disabled;

            // build logged in tag
            Label loggedInText = new Label();
            LinkButton impersonatingUserLink = new LinkButton();
            //LinkButton logoutLink = new LinkButton();

            if (AsentiaSessionState.IdAccountUser > 0 || AsentiaSessionState.IdSiteUser > 0)
            {
                if (AsentiaSessionState.IdImpersonatingSiteUser > 0)
                {
                    impersonatingUserLink.Text = HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.ImpersonatingUserFirstName)
                        + " "
                        + HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.ImpersonatingUserLastName)
                        + " ";
                    impersonatingUserLink.Command += new CommandEventHandler(_ImpersonatingSiteUserLink_Command);

                    this.Controls.Add(impersonatingUserLink);

                    loggedInText.Text = _GlobalResources.loggedinas_lower
                        + " "
                        + HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.UserFirstName)
                        + " "
                        + HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.UserLastName);
                }
                else if (AsentiaSessionState.IdImpersonatingAccountUser > 0)
                {
                    impersonatingUserLink.Text = HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.ImpersonatingUserFirstName)
                        + " "
                        + HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.ImpersonatingUserLastName)
                        + " ";
                    impersonatingUserLink.Command += new CommandEventHandler(_ImpersonatingAccountUserLink_Command);

                    this.Controls.Add(impersonatingUserLink);

                    loggedInText.Text = _GlobalResources.loggedinas_lower
                        + " "
                        + HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.UserFirstName)
                        + " "
                        + HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.UserLastName);
                }
                else
                {
                    loggedInText.Text = _GlobalResources.LoggedInAs
                        + " "
                        + HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.UserFirstName)
                        + " "
                        + HttpContext.Current.Server.HtmlEncode(AsentiaSessionState.UserLastName);
                }

                this.Controls.Add(loggedInText);
            }
            else
            {
                loggedInText.Text = _GlobalResources.YouAreNotLoggedIn;

                this.Controls.Add(loggedInText);
            }

            base.OnInit(e);
        }
        #endregion

        #region _ImpersonatingSiteUserLink_Command
        /// <summary>
        /// Reverts the current session back to the impersonating user and redirects.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ImpersonatingSiteUserLink_Command(object sender, CommandEventArgs e)
        {
            // clear out the session expiration timestamp of the impersonated user
            AsentiaAuthenticatedPage.UpdateUserSessionExpiration(AsentiaSessionState.UtcNow);

            // set the impersonating user as the current session user
            AsentiaSessionState.IdSiteUser = AsentiaSessionState.IdImpersonatingSiteUser;
            AsentiaSessionState.UserCulture = AsentiaSessionState.ImpersonatingUserCulture;
            AsentiaSessionState.UserFirstName = AsentiaSessionState.ImpersonatingUserFirstName;
            AsentiaSessionState.UserLastName = AsentiaSessionState.ImpersonatingUserLastName;
            AsentiaSessionState.UserAvatar = AsentiaSessionState.ImpersonatingUserAvatar;
            AsentiaSessionState.UserGender = AsentiaSessionState.ImpersonatingUserGender;
            AsentiaSessionState.IsUserACourseExpert = AsentiaSessionState.ImpersonatingUserIsUserACourseExpert;
            AsentiaSessionState.IsUserACourseApprover = AsentiaSessionState.ImpersonatingUserIsUserACourseApprover;
            AsentiaSessionState.IsUserASupervisor = AsentiaSessionState.ImpersonatingUserIsUserASupervisor;
            AsentiaSessionState.IsUserAWallModerator = AsentiaSessionState.ImpersonatingUserIsUserAWallModerator;
            AsentiaSessionState.IsUserAnILTInstructor = AsentiaSessionState.ImpersonatingUserIsUserAnILTInstructor;

            // if reverting back to user id 1, null the effective permissions
            // else, get the effective permissions for the user we're reverting back to
            if (AsentiaSessionState.IdSiteUser == 1)
            { AsentiaSessionState.UserEffectivePermissions = null; }
            else
            { AsentiaAuthenticatedPage.UpdateUserEffectivePermissions(true); }

            // clear out the impersonating user information
            AsentiaSessionState.IdImpersonatingSiteUser = 0;
            AsentiaSessionState.ImpersonatingUserCulture = null;
            AsentiaSessionState.ImpersonatingUserFirstName = null;
            AsentiaSessionState.ImpersonatingUserLastName = null;
            AsentiaSessionState.ImpersonatingUserAvatar = null;
            AsentiaSessionState.ImpersonatingUserGender = null;
            AsentiaSessionState.ImpersonatingUserIsUserACourseExpert = false;
            AsentiaSessionState.ImpersonatingUserIsUserACourseApprover = false;
            AsentiaSessionState.ImpersonatingUserIsUserASupervisor = false;
            AsentiaSessionState.ImpersonatingUserIsUserAWallModerator = false;
            AsentiaSessionState.ImpersonatingUserIsUserAnILTInstructor = false;

            // redirect
            HttpContext.Current.Response.Redirect("~/dashboard");
        }
        #endregion

        #region _ImpersonatingAccountUserLink_Command
        /// <summary>
        /// Reverts the account session back to the impersonating user and redirects.
        /// This is for Customer Manager only!
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ImpersonatingAccountUserLink_Command(object sender, CommandEventArgs e)
        {
            // set the impersonating user as the current session user
            AsentiaSessionState.IdAccountUser= AsentiaSessionState.IdImpersonatingAccountUser;
            AsentiaSessionState.UserFirstName = AsentiaSessionState.ImpersonatingUserFirstName;
            AsentiaSessionState.UserLastName = AsentiaSessionState.ImpersonatingUserLastName;
            AsentiaSessionState.IdAccount = AsentiaSessionState.IdImpersonatingAccount;

            // clear out the impersonating user information
            AsentiaSessionState.IdImpersonatingAccountUser = 0;
            AsentiaSessionState.ImpersonatingUserFirstName = null;
            AsentiaSessionState.ImpersonatingUserLastName = null;
            AsentiaSessionState.IdImpersonatingAccount = 0;

            // redirect
            HttpContext.Current.Response.Redirect("~/");
        }
        #endregion
    }
}
