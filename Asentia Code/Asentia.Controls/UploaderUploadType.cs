﻿
namespace Asentia.Controls
{
    /// <summary>
    /// The type of file being uploaded.
    /// </summary>
    public enum UploadType
    {
        Avatar = 1,
        ContentPackage = 2,
        CourseMaterial = 3,
        GroupDocument = 4,
        Task = 5,
        Image = 6,
        LearningPathMaterial = 7,
        LessonMaterial = 8,
        MastheadImage = 9,
        UserActivityData = 10,
        UserBatch = 11,
        UserCertificateData = 12,
        VideoFile = 13,
        PowerPointFile = 14,
        PDF = 15,
        ProfileFile = 16,
        RosterBatch = 17,
        Favicon = 18
    }
}