﻿using System.Data;

namespace Asentia.Controls
{
    /// <summary>
    /// Class to define an object specific filter for the grid.
    /// </summary>
    class GridFilter
    {
        #region Constructors
        /// <summary>
        /// Constructs an instance of a <see cref="GridFilter"/> using a name,
        /// type, size, and value.
        /// </summary>
        /// <param name="name">The name of the filter.</param>
        /// <param name="type">The type of filter.</param>
        /// <param name="size">The size of the filter.</param>
        /// <param name="value">The value of the filter.</param>
        public GridFilter(string name, SqlDbType type, int size, object value)
        {
            this.Name = name;
            this.Type = type;
            this.Size = size;
            this.Value = value;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The name of the filter.
        /// </summary>
        public string Name;

        /// <summary>
        /// The type of filter <see cref="SqlDbType"/>
        /// </summary>
        public SqlDbType Type;

        /// <summary>
        /// The size of the filter.
        /// </summary>
        public int Size;

        /// <summary>
        /// The filter's value.
        /// </summary>
        public object Value;
        #endregion
    }
}
