﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    #region LanguageDropDownType
    /// <summary>
    /// Enumerated type for selecting the "type" of language selector you are building.
    /// </summary>
    public enum LanguageDropDownType
    {
        /// <summary>
        /// Object.
        /// Language Drop-Down for changing the language for an object you are editing.
        /// </summary>
        [Description("Object")]
        Object = 0,

        /// <summary>
        /// DataOnly.
        /// Language Drop-Down for selecting language data only, performs no action
        /// on selected index change. This is used when you are just selecting a 
        /// language for something and do not need to reload the page with properties
        /// in the selected language.
        /// </summary>
        [Description("DataOnly")]
        DataOnly = 1,
    }
    #endregion

    /// <summary>
    /// Builds a drop-down list of languages based on which languages are
    /// installed and available to the site.
    /// </summary>
    public class LanguageSelector : DropDownList
    {
        #region Constructor
        public LanguageSelector(LanguageDropDownType type)
        {
            // set the type
            this._Type = type;

            // populate with languages
            this._Populate();

            // attach "change" action
            this._AttachChangeAction();
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// The type of language selector.
        /// </summary>
        LanguageDropDownType _Type;

        /// <summary>
        /// 
        /// </summary>
        Dictionary<string, string> _QueryStringParams = new Dictionary<string, string>();
        #endregion

        #region Methods
        #region AddQueryStringParam
        /// <summary>
        /// Adds a querystring param to the querystring param collection that gets
        /// attached to the URL.
        /// </summary>
        /// <param name="paramKey"></param>
        /// <param name="paramValue"></param>
        public void AddQueryStringParam(string paramKey, string paramValue)
        {
            this._QueryStringParams.Add(paramKey, paramValue);
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Populate
        /// <summary>
        /// Populates the drop-down with languages based on what is available to the system (installed) 
        /// and what is available to the site.
        /// </summary>
        private void _Populate()
        {
            // create an ArrayList to hold our installed languages
            ArrayList installedLanguages = new ArrayList();

            // en-US is the default language, so it's already installed, add it
            installedLanguages.Add("en-US");

            // loop through each installed language based on the language folders contained in bin
            foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
            {
                // loop through each language available to the site and add it to the array list for drop-down items
                foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
                {
                    if (language == Path.GetFileName(directory))
                    {
                        installedLanguages.Add(Path.GetFileName(directory));
                        break;
                    }
                }
            }

            // loop through array list and add each language to the drop-down
            foreach (string installedLanguage in installedLanguages)
            {
                // get the culture of the info for the language
                CultureInfo cultureInfo = CultureInfo.GetCultureInfo(installedLanguage);

                if (cultureInfo.Name == "es-MX") // Mexican Spanish is "Latin America Spanish"
                { this.Items.Add(new ListItem("español (América Latina)", cultureInfo.Name)); }
                else
                { this.Items.Add(new ListItem(cultureInfo.NativeName, cultureInfo.Name)); }
            }
        }
        #endregion

        #region _AttachChangeAction
        /// <summary>
        /// Attaches the "SelectedIndexChange" server-side handler based on the type of
        /// language selector this is.
        /// </summary>
        private void _AttachChangeAction()
        {
            switch (this._Type)
            {
                case LanguageDropDownType.Object:
                    this.SelectedIndexChanged += this._LanguageSelector_Change_Object;
                    this.AutoPostBack = true;
                    break;
                default:
                    this.AutoPostBack = false;
                    break;
            }
        }
        #endregion

        #region _LanguageSelector_Change_Session
        /// <summary>
        /// This is the "on change" handler method for a "Session" language drop-down.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">arguments</param>
        private void _LanguageSelector_Change_Session(object sender, EventArgs e)
        {
            // get reference to the drop-down instance
            DropDownList languageDropDown = (DropDownList)sender;

            // change the user culture
            AsentiaSessionState.UserCulture = languageDropDown.SelectedValue;

            // if this session is a logged-in user, not the admin, change the default language for the user
            if (AsentiaSessionState.IdSiteUser > 1)
            {
                // update the user's default language
                AsentiaDatabase databaseObject = new AsentiaDatabase();

                try
                {
                    databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                    databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                    databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                    databaseObject.AddParameter("@callerLangString", languageDropDown.SelectedValue, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                    databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                    databaseObject.ExecuteNonQuery("[User.SetDefaultLanguage]", true);

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    databaseObject.Dispose();
                }
            }

            // redirect back to the page with full url intact
            Context.Response.Redirect(Context.Request.RawUrl);
        }
        #endregion

        #region _LanguageSelector_Change_Object
        /// <summary>
        /// This is the "on change" handler method for an "Object" language drop-down.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">arguments</param>
        private void _LanguageSelector_Change_Object(object sender, EventArgs e)
        {
            // get reference to the drop-down instance
            DropDownList languageDropDown = (DropDownList)sender;
            
            // grab url and querystring
            UriBuilder uriBuilder = new UriBuilder(Context.Request.Url);
            string queryString = uriBuilder.Query;
            var queryStringArray = HttpUtility.ParseQueryString(queryString);

            // look for "lang" key in querystring, if exists, replace it
            // if not, add it
            bool containsLangParam = false;

            foreach (string key in queryStringArray.Keys)
            {
                if (key == "lang")
                {
                    containsLangParam = true;
                    queryStringArray[key] = languageDropDown.SelectedValue;
                    break;
                }
            }

            if (!containsLangParam)
            { queryStringArray.Add("lang", languageDropDown.SelectedValue); }

            // loop through "added" querystring params and add them to the
            // querystring only if they do not currently exist in the querystring
            foreach (string key in this._QueryStringParams.Keys)
            {
                bool addKey = true;

                foreach (string qsKey in queryStringArray.Keys)
                {
                    if (key == qsKey)
                    {
                        addKey = false;
                        break;
                    }
                }

                if (addKey)
                { queryStringArray.Add(key, this._QueryStringParams[key]); }
            }

            // rebuild the querystring
            StringBuilder sb = new StringBuilder();

            foreach (string key in queryStringArray.Keys)
            { sb.Append(String.Format("{0}={1}&", key, queryStringArray[key])); }

            uriBuilder.Query = sb.ToString().TrimEnd('&');

            // redirect back to page with querystring intact
            Context.Response.Redirect(Context.Request.Path + uriBuilder.Query);
        }
        #endregion
        #endregion
    }
}
