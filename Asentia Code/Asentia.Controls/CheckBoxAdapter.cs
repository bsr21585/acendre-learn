﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;

namespace Asentia.Controls
{
    public class CheckBoxAdapter : WebControlAdapter
    {
        protected override void RenderBeginTag(HtmlTextWriter writer)
        {
            if (!this.Control.ClientID.Equals("GridSelectAllRecords") && !this.Control.ClientID.Contains("GridSelectRecord_")) // do not do this for checkboxes in Grids
            {
                // css class
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "ToggleInput");

                // render opening tag
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
            }
        }

        protected override void RenderEndTag(HtmlTextWriter writer)
        {
            if (!this.Control.ClientID.Equals("GridSelectAllRecords") && !this.Control.ClientID.Contains("GridSelectRecord_")) // do not do this for checkboxes in Grids
            {
                writer.RenderEndTag();
            }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            CheckBox adaptedControl = (CheckBox)this.Control;

            writer.Indent++;

            // input
            writer.AddAttribute(HtmlTextWriterAttribute.Id, adaptedControl.ClientID);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");
            writer.AddAttribute(HtmlTextWriterAttribute.Name, adaptedControl.UniqueID);

            if (adaptedControl.Checked)
            { writer.AddAttribute(HtmlTextWriterAttribute.Checked, "checked"); }

            if (!adaptedControl.Enabled)
            { writer.AddAttribute("disabled", "disabled"); }

            if (!String.IsNullOrWhiteSpace(adaptedControl.CssClass))
            { writer.AddAttribute(HtmlTextWriterAttribute.Class, adaptedControl.CssClass); }

            if (!String.IsNullOrWhiteSpace(adaptedControl.Attributes["value"]))
            { writer.AddAttribute(HtmlTextWriterAttribute.Value, adaptedControl.Attributes["value"]); }

            if (!String.IsNullOrWhiteSpace(adaptedControl.Attributes["onclick"]))
            { writer.AddAttribute(HtmlTextWriterAttribute.Onclick, adaptedControl.Attributes["onclick"]); }

            if (!String.IsNullOrWhiteSpace(adaptedControl.Attributes["onchange"]))
            { writer.AddAttribute(HtmlTextWriterAttribute.Onclick, adaptedControl.Attributes["onchange"]); }

            // add any input attributes
            foreach (string key in adaptedControl.InputAttributes.Keys)
            {
                writer.AddAttribute(key, adaptedControl.InputAttributes[key]);
            }

            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            // label
            if (!String.IsNullOrWhiteSpace(adaptedControl.Text))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.For, adaptedControl.ClientID);
                writer.RenderBeginTag(HtmlTextWriterTag.Label);
                writer.Write(adaptedControl.Text);
                writer.RenderEndTag();
            }

            writer.Indent--;

            if (this.Page != null)
            { Page.ClientScript.RegisterForEventValidation(adaptedControl.UniqueID); }
        }
    }
}
