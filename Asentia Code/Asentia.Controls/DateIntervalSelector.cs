﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    /// <summary>
    /// Builds a "date interval selector" with interval and timeframe.
    /// </summary>
    public class DateIntervalSelector : WebControl
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the Date Interval Selector control.
        /// </summary>
        /// <param name="id">identifier</param>
        /// <param name="useNone">use "none" checkbox</param>
        public DateIntervalSelector(string id, bool useNone)
            : base(HtmlTextWriterTag.Div)
        {
            // set the properties
            this.ID = id;
            this.UseNone = useNone;

            // build the controls
            this._BuildControls();
        }
        #endregion

        #region Properties
        /// <summary>
        /// The text to display before the selector.
        /// </summary>
        public string TextBeforeSelector
        { set { this._BeforeSelectorText.Text = value + " "; } }

        /// <summary>
        /// The text to display after the selector.
        /// </summary>
        public string TextAfterSelector
        { set { this._AfterSelectorText.Text = " " + value; } }

        /// <summary>
        /// Exclude "Day(s)" from the timeframe drop-down.
        /// </summary>
        public bool ExcludeTimeframe_Day = false;

        /// <summary>
        /// Exclude "Week(s)" from the timeframe drop-down.
        /// </summary>
        public bool ExcludeTimeframe_Week = false;

        /// <summary>
        /// Exclude "Month(s)" from the timeframe drop-down.
        /// </summary>
        public bool ExcludeTimeframe_Month = false;

        /// <summary>
        /// Exclude "Year(s)" from the timeframe drop-down.
        /// </summary>
        public bool ExcludeTimeframe_Year = false;

        /// <summary>
        /// Does this date interval selector use the "none" checkbox?
        /// </summary>
        public bool UseNone = false;

        /// <summary>
        /// To Check Uncheck None Checkbox and also check the status of None Checkbox
        /// </summary>
        public bool NoneCheckBoxChecked
        {
            set { this._None.Checked = value; }
            get { return this._None.Checked; }
        }

        /// <summary>
        /// The text to display for the "none" checkbox.
        /// </summary>
        public string NoneCheckboxText
        { set { this._None.Text = " " + value; } }

        /// <summary>
        /// Gets string value of the interval text box.
        /// </summary>
        public string IntervalValue
        {
            get
            {
                if (this.UseNone)
                {
                    if (this._None.Checked)
                    { return null; }
                }

                if (!String.IsNullOrWhiteSpace(this._Interval.Text))
                { return this._Interval.Text; }
                else
                { return null; }
            }
            set
            {
                if (value != null)
                { this._Interval.Text = value; }
                else
                {
                    this._Interval.Text = String.Empty;

                    if (this.UseNone)
                    { this._None.Checked = true; }
                }
            }
        }

        /// <summary>
        /// Gets string value of the timeframe selector drop-down.
        /// </summary>
        public string TimeframeValue
        {
            get
            {
                if (this.UseNone)
                {
                    if (this._None.Checked)
                    { return null; }
                }

                return this._Timeframe.SelectedValue;
            }
            set
            {
                if (value != null)
                { this._Timeframe.SelectedValue = value; }
                else
                {
                    this._Timeframe.SelectedValue = "d";

                    if (this.UseNone)
                    { this._None.Checked = true; }
                }
            }
        }

        public const string INTERVAL_VALUE_DAY = "d";
        public const string INTERVAL_VALUE_WEEK = "ww";
        public const string INTERVAL_VALUE_MONTH = "m";
        public const string INTERVAL_VALUE_YEAR = "yyyy";
        #endregion

        #region Private Properties
        /// <summary>
        /// Text to display before the selector.
        /// </summary>
        private Literal _BeforeSelectorText = new Literal();

        /// <summary>
        /// Text to display after the selector.
        /// </summary>
        private Literal _AfterSelectorText = new Literal();

        /// <summary>
        /// TextBox control to hold the interval value.
        /// </summary>
        private TextBox _Interval = new TextBox();

        /// <summary>
        /// Drop-Down list for timeframe.
        /// </summary>
        private DropDownList _Timeframe = new DropDownList();

        /// <summary>
        /// CheckBox control for "none".
        /// </summary>
        private CheckBox _None = new CheckBox();
        #endregion

        #region Private Methods
        #region _BuildControls
        /// <summary>
        /// Builds the text box input and calendar image controls.
        /// </summary>
        private void _BuildControls()
        {
            // build the "before selector" text
            this.Controls.Add(this._BeforeSelectorText);

            // build the interval text box
            this._Interval.ID = this.ID + "_Interval";
            this._Interval.CssClass = "InputXShort";
            this._Interval.ReadOnly = false;
            this.Controls.Add(this._Interval);

            // build the timeframe drop-down
            this._Timeframe.ID = this.ID + "_Timeframe";

            if (!this.ExcludeTimeframe_Day)
            { this._Timeframe.Items.Add(new ListItem(Asentia.Common._GlobalResources.Day_s, INTERVAL_VALUE_DAY)); }

            if (!this.ExcludeTimeframe_Week)
            { this._Timeframe.Items.Add(new ListItem(Asentia.Common._GlobalResources.Week_s, INTERVAL_VALUE_WEEK)); }

            if (!this.ExcludeTimeframe_Month)
            { this._Timeframe.Items.Add(new ListItem(Asentia.Common._GlobalResources.Month_s, INTERVAL_VALUE_MONTH)); }

            if (!this.ExcludeTimeframe_Year)
            { this._Timeframe.Items.Add(new ListItem(Asentia.Common._GlobalResources.Year_s, INTERVAL_VALUE_YEAR)); }

            this.Controls.Add(this._Timeframe);

            // build the "after selector" text
            this.Controls.Add(this._AfterSelectorText);

            // build the use none checkbox if we need to build it
            if (this.UseNone)
            {
                this._None.ID = this.ID + "NoneCheckbox";
                this._None.Text = " " + Asentia.Common._GlobalResources.None;

                this.Controls.Add(this._None);
            }
        }
        #endregion

        #region _BuildJSStartUpScript
        /// <summary>
        /// Builds the start up javascript to apply the jQuery functions to the controls. This gets fired OnInit.
        /// </summary>
        private void _BuildJSStartUpScript()
        {
            // build the javascript using a StringBuilder
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" $(document).ready(function () {");

            // ACTIONS FOR "NONE" CONTROL
            if (this.UseNone)
            {
                sb.AppendLine("     if ($(\"#" + this._None.ID + "\").is(\":checked\")) {");
                sb.AppendLine("         $(\"#" + this._Interval.ID + "\").prop(\"disabled\", true);");
                sb.AppendLine("         $(\"#" + this._Timeframe.ID + "\").attr(\"disabled\", true);");
                sb.AppendLine("     }");

                sb.AppendLine("     $(\"#" + this._None.ID + "\").click(function () {");
                sb.AppendLine("         if ($(\"#" + this._None.ID + "\").is(\":checked\")) {");
                sb.AppendLine("             $(\"#" + this._Interval.ID + "\").prop(\"disabled\", true);");
                sb.AppendLine("             $(\"#" + this._Interval.ID + "\").val(\"\");");
                sb.AppendLine("             $(\"#" + this._Timeframe.ID + "\").attr(\"disabled\", true);");
                sb.AppendLine("             $(\"#" + this._Timeframe.ID + "\").val(\"d\");");
                sb.AppendLine("         }");
                sb.AppendLine("         else {");
                sb.AppendLine("             $(\"#" + this._Interval.ID + "\").prop(\"disabled\", false);");
                sb.AppendLine("             $(\"#" + this._Timeframe.ID + "\").prop(\"disabled\", false);");
                sb.AppendLine("         }");
                sb.AppendLine("     });");
            }

            sb.AppendLine(" });");

            // attach the script as a startup script
            ScriptManager.RegisterStartupScript(this, typeof(Asentia.Controls.ClientScript), this.ID, sb.ToString(), true);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnInit
        protected override void OnInit(EventArgs e)
        {
            // execute base
            base.OnInit(e);

            // build js start up script
            this._BuildJSStartUpScript();
        }
        #endregion
        #endregion
    }
}
