﻿$(document).ready(function () {
    // BEGIN APPLICATION "SETTINGS" CONTROL FUNCTIONS
    $("#SettingsControl").each(function () {
        $("#" + this.id).click(function (event) {
            event.stopPropagation();
        });
    });

    $("#SettingsControlImage").click(function () {
        $("#LanguageSelectorMenuItemContainer").hide(); // language selector resides next to settings control, so hide it
        $("#SettingsControlMenuItemContainer").toggle();
    });

    $(document).click(function () {
        if ($("#SettingsControlMenuItemContainer").is(":visible")) {
            $("#SettingsControlMenuItemContainer").hide();
        }
    });
    // END APPLICATION "SETTINGS" CONTROL FUNCTIONS

    // BEGIN APPLICATION LANGUAGE SELECTOR FUNCTIONS
    $("#LanguageSelectorControl").each(function () {
        $("#" + this.id).click(function (event) {
            event.stopPropagation();
        });
    });

    $("#LanguageSelectorControlImage").click(function () {
        $("#SettingsControlMenuItemContainer").hide(); // settings control resides next to language selector, so hide it
        $("#LanguageSelectorMenuItemContainer").toggle();
    });

    $(document).click(function () {
        if ($("#LanguageSelectorMenuItemContainer").is(":visible")) {
            $("#LanguageSelectorMenuItemContainer").hide();
        }
    });
    // END APPLICATION LANGUAGE SELECTOR FUNCTIONS
});