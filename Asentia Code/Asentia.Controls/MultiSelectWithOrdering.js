﻿// METHOD: MoveAllToSelected
// Moves all items from available list box to selected list box.
function MoveAllToSelected(identifier) {
    $("#" + identifier + "_AvailableItems option").appendTo("#" + identifier + "_SelectedItems");

    // populate the hidden field
    PopulateHiddenFieldWithSelectedValues(identifier);

    // evaluate the button states
    EvaluateButtonStates(identifier);
}

// METHOD: MoveItemToSelected
// Moves an item from available list box to selected list box.
function MoveItemToSelected(identifier) {
    $("#" + identifier + "_AvailableItems option:selected").appendTo("#" + identifier + "_SelectedItems");

    // populate the hidden field
    PopulateHiddenFieldWithSelectedValues(identifier);

    // evaluate the button states
    EvaluateButtonStates(identifier);
}

// METHOD: MoveItemToAvailable
// Moves an item from selected list box to available list box.
function MoveItemToAvailable(identifier) {
    var selectedItem = $("#" + identifier + "_SelectedItems option:selected")
    var selectedOrdinal = selectedItem.attr("ordinal");
    for (i = parseInt(selectedOrdinal) - 1 ; i > 0; i--)
    {
        if ($("#" + identifier + "_AvailableItems [ordinal='" + i.toString() +"']").length) { break; }
    }
    if ($("#" + identifier + "_AvailableItems option").size() > 0) {
        if (i > 0) {
            selectedItem.insertAfter($("#" + identifier + "_AvailableItems [ordinal='" + i.toString() + "']"));
        }
        else {
            selectedItem.insertBefore("#" + identifier + "_AvailableItems option:eq(0)");
        }
    }
    else
    {
        selectedItem.appendTo("#" + identifier + "_AvailableItems");
    }
    // populate the hidden field
    PopulateHiddenFieldWithSelectedValues(identifier);

    // evaluate the button states
    EvaluateButtonStates(identifier);
}

// METHOD: MoveAllToAvailable
// Moves all items from selected list box to available list box.
function MoveAllToAvailable(identifier) {
    //$("#" + identifier + "_SelectedItems option").appendTo("#" + identifier + "_AvailableItems");
    var selectedItem;
    var selectedOrdinal;
    var optionNum = $("#" + identifier + "_SelectedItems option").size();
    for (j = 0; j < optionNum; j++) {        
        selectedItem = $("#" + identifier + "_SelectedItems option:eq(0)");
        selectedOrdinal = selectedItem.attr("ordinal");
        for (i = parseInt(selectedOrdinal) - 1 ; i > 0; i--) {
            if ($("#" + identifier + "_AvailableItems [ordinal='" + i.toString() + "']").length) { break; }
        }
        if ($("#" + identifier + "_AvailableItems option").size() > 0) {
            if (i > 0) {
                selectedItem.insertAfter($("#" + identifier + "_AvailableItems [ordinal='" + i.toString() + "']"));
            }
            else {
                selectedItem.insertBefore("#" + identifier + "_AvailableItems option:eq(0)");
            }
        }
        else {
            selectedItem.appendTo("#" + identifier + "_AvailableItems");
        }
    }
    // populate the hidden field
    PopulateHiddenFieldWithSelectedValues(identifier);

    // evaluate the button states
    EvaluateButtonStates(identifier);
}

// METHOD: MoveItemUp
// Moves item up in the selected list box.
function MoveItemUp(identifier) {
    var selectedOption = $("#" + identifier + "_SelectedItems option:selected");
    var prevOption = $("#" + identifier + "_SelectedItems option:selected").prev("option");

    if ($(prevOption).text() != "") {
        $(selectedOption).remove();
        $(prevOption).before($(selectedOption));
    }

    // populate the hidden field
    PopulateHiddenFieldWithSelectedValues(identifier);
}

// METHOD: MoveItemDown
// Moves item down in the selected list box.
function MoveItemDown(identifier) {
    var selectedOption = $("#" + identifier + "_SelectedItems option:selected");
    var nextOption = $("#" + identifier + "_SelectedItems option:selected").next("option");

    if ($(nextOption).text() != "") {
        $(selectedOption).remove();
        $(nextOption).after($(selectedOption));
    }

    // populate the hidden field
    PopulateHiddenFieldWithSelectedValues(identifier);
}

// METHOD: PopulateHiddenFieldWithSelectedValues
// Reads the selected items list box and compiles items into a hidden field.
function PopulateHiddenFieldWithSelectedValues(identifier) {
    var selectedItems = "";

    $("#" + identifier + "_SelectedItems option").each(function () {
        selectedItems += $(this).val() + "|";
    });

    if (selectedItems.length == 0) {
        $("#" + identifier + "_SelectedItemsHiddenField").val("");
    }
    else {
        // strip last character from selectedItems string
        selectedItems = selectedItems.substring(0, selectedItems.length - 1);

        $("#" + identifier + "_SelectedItemsHiddenField").val(selectedItems);
    }
}

// METHOD: EvaluateButtonStates
// Enables or disables buttons baded on items in (or not in) list boxes.
function EvaluateButtonStates(identifier) {
    var availableItemsCount = 0;
    var selectedItemsCount = 0;

    // get number of items in available items list box
    $("#" + identifier + "_AvailableItems option").each(function () {
        availableItemsCount++;
    });

    // get number of items in selected items list box
    $("#" + identifier + "_SelectedItems option").each(function () {
        selectedItemsCount++;
    });

    // change "add/remove" button states for available items count
    if (availableItemsCount == 0) {
        $("#" + identifier + "_MoveAllToSelected").addClass("DimIcon");
        $("#" + identifier + "_MoveAllToSelected").prop("disabled", true);
        $("#" + identifier + "_MoveItemToSelected").addClass("DimIcon");
        $("#" + identifier + "_MoveItemToSelected").prop("disabled", true);
    }
    else {
        $("#" + identifier + "_MoveAllToSelected").removeClass("DimIcon");
        $("#" + identifier + "_MoveAllToSelected").prop("disabled", false);
        $("#" + identifier + "_MoveItemToSelected").removeClass("DimIcon");
        $("#" + identifier + "_MoveItemToSelected").prop("disabled", false);
    }

    // change "add/remove" button states for selected items count
    if (selectedItemsCount == 0) {
        $("#" + identifier + "_MoveAllToAvailable").addClass("DimIcon");
        $("#" + identifier + "_MoveAllToAvailable").prop("disabled", true);
        $("#" + identifier + "_MoveItemToAvailable").addClass("DimIcon");
        $("#" + identifier + "_MoveItemToAvailable").prop("disabled", true);
    }
    else {
        $("#" + identifier + "_MoveAllToAvailable").removeClass("DimIcon");
        $("#" + identifier + "_MoveAllToAvailable").prop("disabled", false);
        $("#" + identifier + "_MoveItemToAvailable").removeClass("DimIcon");
        $("#" + identifier + "_MoveItemToAvailable").prop("disabled", false);
    }

    // change "ordering" button states for selected items count
    if (selectedItemsCount <= 1) {
        $("#" + identifier + "_MoveItemUp").addClass("DimIcon");
        $("#" + identifier + "_MoveItemUp").prop("disabled", true);
        $("#" + identifier + "_MoveItemDown").addClass("DimIcon");
        $("#" + identifier + "_MoveItemDown").prop("disabled", true);
    }
    else {
        $("#" + identifier + "_MoveItemUp").removeClass("DimIcon");
        $("#" + identifier + "_MoveItemUp").prop("disabled", false);
        $("#" + identifier + "_MoveItemDown").removeClass("DimIcon");
        $("#" + identifier + "_MoveItemDown").prop("disabled", false);
    }
}