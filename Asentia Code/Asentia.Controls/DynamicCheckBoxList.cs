﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    /// <summary>
    /// CheckedCheckBoxList control with select all and select none controls, and option for search.
    /// </summary>
    public class DynamicCheckBoxList : WebControl, INamingContainer
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">the id of this control</param>
        public DynamicCheckBoxList(string id)
            : base(HtmlTextWriterTag.Div)
        {
            // set initialization variables
            this.ID = id;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Will this CheckBoxList include a search box?
        /// </summary>
        public bool IsSearchable = true;

        /// <summary>
        /// Will this CheckBoxList include select "All" and "None" options?
        /// </summary>
        public bool IncludeSelectAllNone = true;

        /// <summary>
        /// The "Search" button.
        /// </summary>
        public Button SearchButton = new Button();

        /// <summary>
        /// The "Clear" button for clearing the search.
        /// </summary>
        public Button ClearSearchButton = new Button();

        /// <summary>
        /// The "Search" text box.
        /// </summary>
        public TextBox SearchTextBox = new TextBox();

        /// <summary>
        /// The CheckBoxList control.
        /// </summary>
        public CheckBoxList CheckBoxListControl = new CheckBoxList();

        /// <summary>
        /// Message to display in CheckBoxList if there are no records found upon DataBind.
        /// </summary>
        public string NoRecordsFoundMessage = null;
        #endregion

        #region Private Properties
        /// <summary>
        /// Container for "Search."
        /// </summary>
        private Panel _SearchContainer;

        /// <summary>
        /// Container for the CheckBoxList.
        /// </summary>
        private Panel _CheckBoxListContainer;

        /// <summary>
        /// Container for the select "All" and "None" links.
        /// </summary>
        private Panel _SelectAllNoneContainer;
        #endregion

        #region Private Methods
        #region _BuildSearchContainer
        /// <summary>
        /// Builds the "Search" controls into the "Search" container.
        /// </summary>
        /// 
        private void _BuildSearchContainer()
        {
            this._SearchContainer = new Panel();
            this._SearchContainer.ID = this.ID + "SearchContainer";
            this._SearchContainer.CssClass = "CheckBoxListSearchContainer";

            this.SearchTextBox.ID = this.ID + "SearchTextBox";
            this.SearchTextBox.CssClass = "InputText InputMedium";

            this.SearchButton.ID = this.ID + "SearchButton";
            this.SearchButton.CssClass = "Button ActionButton SearchButton";
            this.SearchButton.Text = _GlobalResources.Search;

            this.ClearSearchButton.ID = this.ID + "ClearSearchButton";
            this.ClearSearchButton.CssClass = "Button NonActionButton";
            this.ClearSearchButton.Text = _GlobalResources.Clear;

            this._SearchContainer.Controls.Add(this.SearchTextBox);
            this._SearchContainer.Controls.Add(this.SearchButton);
            this._SearchContainer.Controls.Add(this.ClearSearchButton);

            this._SearchContainer.DefaultButton = this.SearchButton.ID;

            this.Controls.Add(this._SearchContainer);
        }
        #endregion

        #region _BuildCheckBoxListContainer
        /// <summary>
        /// Builds the CheckBoxList control into the "CheckBoxList" container.
        /// </summary>
        private void _BuildCheckBoxListContainer()
        {
            this._CheckBoxListContainer = new Panel();
            this._CheckBoxListContainer.ID = this.ID + "CheckBoxListContainer";

            this.CheckBoxListControl.ID = this.ID + "CheckBoxList";
            this.CheckBoxListControl.CssClass = "CheckBoxList";
            this.CheckBoxListControl.CssClass = "CheckBoxList";

            this._CheckBoxListContainer
                .Controls.Add(this.CheckBoxListControl);

            this.Controls.Add(this._CheckBoxListContainer);
        }
        #endregion

        #region _BuildSelectAllNoneContainer
        /// <summary>
        /// Builds the select "All" and "None" links into the container.
        /// </summary>
        private void _BuildSelectAllNoneContainer()
        {
            this._SelectAllNoneContainer = new Panel();
            this._SelectAllNoneContainer.ID = this.ID + "SelectAllNoneContainer";
            this._SelectAllNoneContainer.CssClass = "CheckBoxListSelectAllNoneContainer";

            HyperLink selectAll = new HyperLink();
            selectAll.ID = this.ID + "SelectAllLink";
            selectAll.NavigateUrl = "javascript:$(\"#" + this.CheckBoxListControl.ID + " input\").prop(\"checked\", true);";
            selectAll.Text = _GlobalResources.All;

            HyperLink selectNone = new HyperLink();
            selectNone.ID = this.ID + "SelectNoneLink";
            selectNone.NavigateUrl = "javascript:$(\"#" + this.CheckBoxListControl.ID + " input\").prop(\"checked\", false);";
            selectNone.Text = _GlobalResources.None;

            Literal selectText = new Literal();
            selectText.Text = _GlobalResources.Select + " ";

            Literal allNoneSeparator = new Literal();
            allNoneSeparator.Text = "&nbsp;|&nbsp;";

            this._SelectAllNoneContainer.Controls.Add(selectText);
            this._SelectAllNoneContainer.Controls.Add(selectAll);
            this._SelectAllNoneContainer.Controls.Add(allNoneSeparator);
            this._SelectAllNoneContainer.Controls.Add(selectNone);

            this.Controls.Add(this._SelectAllNoneContainer);
        }
        #endregion
        #endregion

        #region Methods
        #region GetSelectedValues
        /// <summary>
        /// Gets selected values from the CheckBoxList.
        /// </summary>
        /// <returns>list of selected items</returns>
        public List<string> GetSelectedValues()
        {
            List<string> selectedValues = new List<string>();
            
            for (int i = 0; i < this.CheckBoxListControl.Items.Count; i++)
            {
                if (this.CheckBoxListControl.Items[i].Selected)
                { selectedValues.Add(this.CheckBoxListControl.Items[i].Value); }
            }

            return selectedValues;
        }
        #endregion

        #region GetNotSelectedValues
        /// <summary>
        /// Gets not selected values from the CheckBoxList.
        /// </summary>
        /// <returns>list of not selected items</returns>
        public List<string> GetNotSelectedValues()
        {
            List<string> notSelectedValues = new List<string>();

            for (int i = 0; i < this.CheckBoxListControl.Items.Count; i++)
            {
                if (!this.CheckBoxListControl.Items[i].Selected)
                { notSelectedValues.Add(this.CheckBoxListControl.Items[i].Value); }
            }

            return notSelectedValues;
        }
        #endregion

        #region RemoveSelectedItems
        /// <summary>
        /// Removes selected items from CheckBoxList.
        /// </summary>
        public void RemoveSelectedItems()
        {
            for (int i = this.CheckBoxListControl.Items.Count - 1; i >= 0; i--)
            {
                if (this.CheckBoxListControl.Items[i].Selected)
                { this.CheckBoxListControl.Items.RemoveAt(i); }
            }            
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // check to see if there are any items and if not, display the "no records found" 
            // message, and disable CheckBoxList
            if (this.CheckBoxListControl.Items.Count <= 0)
            {
                this.CheckBoxListControl.Items.Add(new ListItem(this.NoRecordsFoundMessage));
                this.CheckBoxListControl.Enabled = false;
            }
            else
            { this.CheckBoxListControl.Enabled = true; }

        }
        #endregion

        #region CreateChildControls
        protected override void CreateChildControls()
        {
            if (this.IsSearchable)
            { this._BuildSearchContainer(); }

            this._BuildCheckBoxListContainer();

            if (this.IncludeSelectAllNone)
            { this._BuildSelectAllNoneContainer(); }

            base.CreateChildControls();
        }
        #endregion
        #endregion
    }
}
