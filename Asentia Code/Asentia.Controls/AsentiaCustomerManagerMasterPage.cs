﻿using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Asentia.Common;
using AjaxControlToolkit;
using System;
using Asentia.Controls;
using System.Collections;
using System.IO;
using System.Globalization;
using System.Data;

namespace Asentia.Controls
{
    public class AsentiaCustomerManagerMasterPage : MasterPage
    {
        #region Properties
        public HtmlForm MasterForm;
        public static Panel PageContainer;
        public Literal GlobalJavascriptVariables;
        public ContentPlaceHolder HeadPlaceholder;

        // masthead elements
        public Panel MastheadContainer;
        public Panel MastheadColumnLeft;
        public Panel MastheadColumnRight;

        // content elements
        public Panel ContentContainer;
        public Panel MasterContentLeft;
        public Panel MasterContentRight;
        public Panel MasterContentMiddle;

        // breadcrumb elements
        public Panel BreadcrumbContainer;
        public Panel BreadcrumbColumnLeft;
        public Panel BreadcrumbColumnRight;

        // page content placeholders
        public ContentPlaceHolder BreadcrumbContentPlaceholder;
        public ContentPlaceHolder SideContentPlaceholder1;
        public ContentPlaceHolder PageContentPlaceholder;
        public ContentPlaceHolder SideContentPlaceholder2;

        // login button id
        public static string LogInButtonId = null;

        // log in button control exists
        public static bool LogInButtonControlExists = false;
        #endregion

        #region Private Properties

        // parameter that gets set to true if the master page instansiating this class is HomePage.master
        // if true, a different masthead will be loaded
        private bool _IsHomePage = false;

        private string _BackgroundImagePath;
        private string _BackgroundColor;
        private string _LogoImageUrl;
        private string _SecondaryImageUrl;
        private string _ApplicationIconImageUrl;
        private string _ApplicationLogoImageUrl;

        #endregion

        #region Methods

        #region Page_Load
        /// <summary>
        /// Page load.
        /// </summary>
        /// <param name="sender">Page</param>
        /// <param name="e">Event arguments</param>
        public void Page_Load(object sender, EventArgs e)
        {
            // give the master page an ID so one is not auto-generated
            this.ID = "Master";

            // call functions to build page
            this._IncludeCSSFiles();
            this._BuildMasthead();

            if (!Page.IsPostBack)
            { }
        }
        #endregion
        #endregion

        #region Private Methods

        #region _IncludeCSSFiles
        /// <summary>
        /// Method to include all css files.
        /// </summary>
        private void _IncludeCSSFiles()
        {
            // tag format for the stylesheet link
            const string STYLE_SHEET_LINK_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}?ver={1}\" />\r\n";

            // style sheets that aren't site configurable - located in the root css folder
            ArrayList styleSheets = new ArrayList();

            // add stylesheets
            styleSheets.Add("Reset.css");
            styleSheets.Add("Layout.css");
            styleSheets.Add("Inputs.css");
            styleSheets.Add("Modal.css");
            styleSheets.Add("Grid.css");
            styleSheets.Add("LoginForm.css");
            styleSheets.Add("DatePicker.css");
            styleSheets.Add("CMMainMenu.css");
            styleSheets.Add("AdminMenu.css");

            // attach references to stylesheets
            foreach (string stylesheet in styleSheets)
            {
                string pathToStylesheet = MapPathSecure(SitePathConstants.CM_CSS + stylesheet);

                if (File.Exists(pathToStylesheet))
                {
                    Page.Header.Controls.Add(
                        new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.CM_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                    );
                }
            }
        }
        #endregion

        #region Control Builder Methods
        // Add builder methods for new controls here. 

        #region _MastheadBackground
        /// <summary>
        /// Applies masthead background-image and background-color styling to MastheadContainer if those site params exist.
        /// </summary>
        private void _MastheadBackground()
        {
            // masthead background image
            this.MastheadContainer.Style.Add("background-image", "url('" + _BackgroundImagePath + "')");

            if (!String.IsNullOrWhiteSpace(_BackgroundColor))
            {
                // attach background-color style to masthead container
                this.MastheadContainer.Style.Add("background-color", _BackgroundColor);
            }
        }
        #endregion

        #region _HomePageLogoImage
        /// <summary>
        /// Home Page logo image control.
        /// </summary>
        /// <param name="container"></param>
        private void _HomePageLogoImage(Panel container)
        {
            // id information
            string id = "HomePageLogoImage";

            // declare control and set its id
            HyperLink logoImageHyperLink = new HyperLink();
            logoImageHyperLink.ID = id;
            logoImageHyperLink.NavigateUrl = "/";
            logoImageHyperLink.ImageUrl = _LogoImageUrl;

            // attach control to container
            container.Controls.Add(logoImageHyperLink);
        }
        #endregion

        #region _HomePageSecondaryImage
        /// <summary>
        /// Home Page secondary image control.
        /// </summary>
        /// <param name="container">container to attach the control to</param>
        private void _HomePageSecondaryImage(Panel container)
        {
            // id information
            string id = "HomePageSecondaryImage";

            // declare control and set its id
            Image secondaryImage = new Image();
            secondaryImage.ID = id;
            secondaryImage.ImageUrl = _SecondaryImageUrl;

            container.Controls.Add(secondaryImage);
        }
        #endregion

        #region _ApplicationIconImage
        /// <summary>
        /// Application icon image control.
        /// </summary>
        /// <param name="container">container to attach the control to</param>
        private void _ApplicationIconImage(Panel container)
        {
            // id information
            string id = "ApplicationIconImage";

            // declare control and set its id
            HyperLink iconImageHyperLink = new HyperLink();
            iconImageHyperLink.ID = id;
            iconImageHyperLink.NavigateUrl = "/";
            iconImageHyperLink.ImageUrl = _ApplicationIconImageUrl;

            container.Controls.Add(iconImageHyperLink);
        }
        #endregion

        #region _ApplicationLogoImage
        /// <summary>
        /// Application logo image control.
        /// </summary>
        /// <param name="container">container to attach the control to</param>
        private void _ApplicationLogoImage(Panel container)
        {
            // id information
            string id = "ApplicationLogoImage";

            // declare control and set its id
            HyperLink logoImageHyperLink = new HyperLink();
            logoImageHyperLink.ID = id;
            logoImageHyperLink.CssClass = "MobileHidden";
            logoImageHyperLink.NavigateUrl = "/";
            logoImageHyperLink.ImageUrl = _ApplicationLogoImageUrl;

            container.Controls.Add(logoImageHyperLink);

        }
        #endregion

        #region _ApplicationIconImageAndTitle
        /// <summary>
        /// Application icon image and title control.
        /// </summary>
        /// <param name="container">container to attach the control to</param>
        private void _ApplicationIconImageAndTitle(Panel container, string title, string titleImagePath)
        {
            HtmlGenericControl titleWrapper = new HtmlGenericControl("span");
            Localize titleText = new Localize();

            titleText.Text = title;
            titleWrapper.Controls.Add(titleText);


            if (!String.IsNullOrWhiteSpace(titleImagePath))
            {
                Image titleImage = new Image();
                titleImage.ImageUrl = titleImagePath;
                titleImage.AlternateText = title;
                titleImage.CssClass = "PageTitleIcon";

                // id information
                string id = "ApplicationIcon";

                // declare control and set its id
                HyperLink iconImageHyperLink = new HyperLink();
                iconImageHyperLink.ID = id;
                iconImageHyperLink.NavigateUrl = "/";
                iconImageHyperLink.ImageUrl = _ApplicationIconImageUrl;

                iconImageHyperLink.Controls.Add(titleImage);
                container.Controls.Add(iconImageHyperLink);

            }

            container.Controls.Add(titleWrapper);
        }
        #endregion

        #region _BuildMasthead
        /// <summary>
        /// Control that displays a "Mast Head".
        /// </summary>
        private void _BuildMasthead()
        {
            //add application icon and title on head left side.
            this._ApplicationIconImageAndTitle(this.MastheadColumnLeft,
                                                _GlobalResources.AsentiaCustomerManager,
                                                ImageFiles.GetIconPath(ImageFiles.ICON_ADMINISTRATOR,
                                                                        ImageFiles.EXT_PNG,
                                                                        true
                                                                       )
                                               );

            //add aditional controls to mast head(Rightk)
            this._SettingsControl();
            this._LanguageSelectorControl();
        }
        #endregion

        #region _LanguageSelectorControl
        /// <summary>
        /// Control that displays a language selector.
        /// </summary>
        private void _LanguageSelectorControl()
        {
            if (!HttpContext.Current.Request.Url.ToString().Contains("/unknown/") && !HttpContext.Current.Request.Url.ToString().Contains("/offline/"))
            {
                // id information
                string id = "LanguageSelectorControl";

                Panel languageSelectorContainer = new Panel();
                languageSelectorContainer.ID = id;

                Image localizationImage = new Image();
                localizationImage.ID = id + "Image";
                localizationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM_LOCALIZATION,
                                                                    ImageFiles.EXT_PNG, true);
                localizationImage.CssClass = "SmallIcon";
                languageSelectorContainer.Controls.Add(localizationImage);

                Panel languageSelectorMenuItemContainer = new Panel();
                languageSelectorMenuItemContainer.ID = "LanguageSelectorMenuItemContainer";

                ArrayList installedLanguages = new ArrayList();

                // en-US is the default language, so it's already installed, add it
                installedLanguages.Add("en-US");

                // loop through each installed language based on the language folders
                // contained in bin
                foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
                {
                    // loop through each language available to the site and add it
                    // to the array list for drop-down items
                    if (Path.GetFileName(directory).Contains("-"))
                    { installedLanguages.Add(Path.GetFileName(directory)); }
                }

                // loop through array list and add each language to the drop-down
                foreach (string installedLanguage in installedLanguages)
                {
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(installedLanguage);

                    Panel languageSelectorMenuItem = new Panel();
                    languageSelectorMenuItem.ID = "LanguageSelectorMenuItem_" + cultureInfo.Name;

                    LinkButton language = new LinkButton();
                    language.ID = "LanguageSelectorMenuItemLink_" + cultureInfo.Name;

                    if (AsentiaSessionState.UserCulture == cultureInfo.Name)
                    { language.CssClass = "bold"; }

                    language.Text = cultureInfo.NativeName;
                    language.Command += _LanguageSelector_Change_Session;
                    language.CommandArgument = cultureInfo.Name;

                    languageSelectorMenuItem.Controls.Add(language);
                    languageSelectorMenuItemContainer.Controls.Add(languageSelectorMenuItem);
                }

                languageSelectorContainer.Controls.Add(languageSelectorMenuItemContainer);

                this.MastheadColumnRight.Controls.Add(languageSelectorContainer);
            }
        }
        #endregion

        #region _SettingsControl
        /// <summary>
        /// Control that displays the settings control (log-in/out link etc).
        /// </summary>
        private void _SettingsControl()
        {
            if (!HttpContext.Current.Request.Url.ToString().Contains("/unknown/") && !HttpContext.Current.Request.Url.ToString().Contains("/offline/"))
            {
                Panel settingsControl = new Panel();

                if (AsentiaSessionState.IdAccountUser > 0 || AsentiaSessionState.IdSiteUser > 0)
                {
                    // id information
                    string id = "SettingsControl";
                    settingsControl.ID = id;

                    // settings image
                    Image settingsImage = new Image();
                    settingsImage.ID = id + "Image";
                    settingsImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_USERM_WHITE,
                                                                    ImageFiles.EXT_PNG, true);
                    settingsImage.AlternateText = _GlobalResources.Log_In;
                    settingsImage.CssClass = "SmallIcon";
                    settingsControl.Controls.Add(settingsImage);

                    // settings control menu item container
                    Panel settingsControlMenuItemContainer = new Panel();
                    settingsControlMenuItemContainer.ID = "SettingsControlMenuItemContainer";

                    // logged in tag
                    LoggedInTag loggedInTag = new LoggedInTag();
                    loggedInTag.ID = id + "LoggedInTag";
                    settingsControlMenuItemContainer.Controls.Add(loggedInTag);

                    // logout link
                    string logoutLinkId = id + "LogoutLink";

                    Panel logoutLinkContainer = new Panel();
                    logoutLinkContainer.ID = logoutLinkId + "Container";

                    LinkButton logoutLink = new LinkButton();
                    logoutLink.ID = logoutLinkId + "Link";
                    logoutLink.Text = _GlobalResources.LogOut;
                    logoutLink.Command += new CommandEventHandler(_LogoutLink_Command);

                    logoutLinkContainer.Controls.Add(logoutLink);
                    settingsControlMenuItemContainer.Controls.Add(logoutLinkContainer);

                    settingsControl.Controls.Add(settingsControlMenuItemContainer);
                }
                this.MastheadColumnRight.Controls.Add(settingsControl);
            }
        }
        #endregion

        #region _LogoutLink_Command
        /// <summary>
        /// Logs the user out.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _LogoutLink_Command(object sender, CommandEventArgs e)
        {
            AsentiaSessionState.EndSession(true);
        }
        #endregion

        #region _LanguageSelector_Change_Session
        /// <summary>
        /// This is the handler method for the "Session" language menu.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">arguments</param>
        private void _LanguageSelector_Change_Session(object sender, CommandEventArgs e)
        {
            string language = (string)e.CommandArgument;

            // change the user culture
            AsentiaSessionState.UserCulture = language;

            // if this session is a logged-in user, not the admin, change the default language for the user
            if (AsentiaSessionState.IdSiteUser > 1)
            {
                // update the user's default language
                AsentiaDatabase databaseObject = new AsentiaDatabase(DatabaseType.Account);

                try
                {
                    databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                    databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                    databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                    databaseObject.AddParameter("@callerLangString", language, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                    databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                    databaseObject.ExecuteNonQuery("[User.SetDefaultLanguage]", true);

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    databaseObject.Dispose();
                }
            }

            // redirect back to the page with full url intact
            Context.Response.Redirect(Context.Request.RawUrl);
        }
        #endregion
        #endregion
        #endregion

        #region Overridden Methods

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(AsentiaCustomerManagerMasterPage), "Asentia.Controls.AsentiaCustomerManagerMasterPage.js");
        }
        #endregion
        #endregion
    }
}