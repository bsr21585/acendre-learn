﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    #region ENUMS
    public enum InformationStatusPanel_Type
    {
        DraggablePopup = 0,
        DraggablePopupWithBlackout = 1,
        Popup = 2,
        PopupWithBlackout = 3,
        OnPage = 4,
    }

    public enum InformationStatusPanel_MessageType
    {
        Alert = 0,
        Error = 1,
        Help = 2,
        Information = 3,
        Success = 4,
    }
    #endregion

    public class InformationStatusPanel : Panel
    {
        #region Constructors
        /// <summary>
        /// Contstructor that takes a message only.
        /// </summary>
        /// <param name="id">control id</param>
        /// <param name="type">type</param>
        /// <param name="messageType">message type</param>
        /// <param name="message">message</param>
        public InformationStatusPanel(string id, InformationStatusPanel_Type type, InformationStatusPanel_MessageType messageType, string message, bool addPopupCloseButton = true)
        {
            this.ID = id;
            this.Type = type;
            this.MessageType = messageType;
            this.Message = message;
            this.AddPopupCloseButton = addPopupCloseButton;

            this.CssClass = BASE_CSS_CLASS;
        }

        /// <summary>
        /// Contstructor that takes a message and body text.
        /// </summary>
        /// <param name="id">control id</param>
        /// <param name="type">type</param>
        /// <param name="messageType">message type</param>
        /// <param name="message">message</param>
        /// <param name="bodyText">body text</param>
        public InformationStatusPanel(string id, InformationStatusPanel_Type type, InformationStatusPanel_MessageType messageType, string message, string bodyText, bool addPopupCloseButton = true)
        {
            this.ID = id;
            this.Type = type;
            this.MessageType = messageType;
            this.Message = message;
            this.BodyText = bodyText;
            this.AddPopupCloseButton = addPopupCloseButton;

            this.CssClass = BASE_CSS_CLASS;
        }

        /// <summary>
        /// Contstructor that takes a message and body control.
        /// </summary>
        /// <param name="id">control id</param>
        /// <param name="type">type</param>
        /// <param name="messageType">message type</param>
        /// <param name="message">message</param>
        /// <param name="bodyControl">body control</param>
        public InformationStatusPanel(string id, InformationStatusPanel_Type type, InformationStatusPanel_MessageType messageType, string message, Control bodyControl, bool addPopupCloseButton = true)
        {
            this.ID = id;
            this.Type = type;
            this.MessageType = messageType;
            this.Message = message;
            this.BodyControl = bodyControl;
            this.AddPopupCloseButton = addPopupCloseButton;

            this.CssClass = BASE_CSS_CLASS;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The type of panel.
        /// </summary>
        public InformationStatusPanel_Type Type;
        
        /// <summary>
        /// The type of message for this panel.
        /// </summary>
        public InformationStatusPanel_MessageType MessageType;

        /// <summary>
        /// The text to override the message header with, instead of the standard Success or Error message.
        /// </summary>
        public string MessageHeaderOverrideText;
        
        /// <summary>
        /// Does this use a launch trigger? Popuo only.
        /// </summary>
        public bool UseLaunchTrigger = false;
        
        /// <summary>
        /// Should we draw a launch trigger button?
        /// </summary>
        public bool DrawLaunchTriggerIconButton = false;
        
        /// <summary>
        /// The ID of the external Control to be used as a trigger to launch this.
        /// Only applies to popups and when DrawLaunchTriggerIconButton is false.
        /// </summary>
        public string LaunchTriggerId;

        /// <summary>
        /// Should the control use display: inline-block?
        /// This should be set to true when the type is a popup and this control is building the trigger
        /// itself, it is so the control trigger gets displayed inline with text it is positioned next to.
        /// Usually you would set this when building this for a "Help" popup.
        /// </summary>
        public bool DisplayInlineBlock = false;

        /// <summary>
        /// Should the control use position: relative?
        /// This should be set to true when the type is a popup and the launched popup should be positioned
        /// relative to where it is placed on page, not absolutely positioned on the page. Usually you would
        /// set this when building this for a "Help" popup.
        /// </summary>
        public bool PositionRelative = false;

        /// <summary>
        /// The message to be displayed as a sub-heading.
        /// </summary>
        public string Message;

        /// <summary>
        /// Text to be attached to the body content, can be used alone, or along with BodyControl.
        /// </summary>
        public string BodyText;

        /// <summary>
        /// Control to be attached to the body content, can be used alone, or along with BodyText.
        /// </summary>
        public Control BodyControl;

        /// <summary>
        /// Are we loading images for this from Customer Manager?
        /// </summary>
        public bool LoadImagesForCustomerManager = false;

        /// <summary>
        /// Should we add a close button for the popup message? 
        /// Only applies to popups, default is true.
        /// </summary>
        public bool AddPopupCloseButton = true;
        #endregion

        #region Private Properties
        /// <summary>
        /// The base css class to prefix all css classes with for this object.
        /// </summary>
        private const string BASE_CSS_CLASS = "InformationStatusPanel";
        #endregion

        #region Private Methods
        #region _BuildLaunchTriggerIconButton
        /// <summary>
        /// Builds the launch trigger for when this is set to build a launch trigger.
        /// </summary>
        private void _BuildLaunchTriggerIconButton()
        {
            // container
            Panel launchTriggerIconButtonContainer = new Panel();
            launchTriggerIconButtonContainer.ID = this.ID + "LaunchTriggerIconButtonContainer";
            launchTriggerIconButtonContainer.CssClass = BASE_CSS_CLASS + "-LaunchTriggerIconButtonContainer";

            // image
            Image launchTrigger = new Image();
            launchTrigger.ID = this.ID + "LaunchTrigger";

            if (this.Type == InformationStatusPanel_Type.DraggablePopupWithBlackout || this.Type == InformationStatusPanel_Type.PopupWithBlackout)
            { launchTrigger.Attributes.Add("onclick", "LaunchInformationStatusPanel(\"" + launchTrigger.ID + "\", \"" + this.ID + "\", true," + this.PositionRelative.ToString().ToLower() + ")"); }
            else
            { launchTrigger.Attributes.Add("onclick", "LaunchInformationStatusPanel(\"" + launchTrigger.ID + "\", \"" + this.ID + "\", false," + this.PositionRelative.ToString().ToLower() + ")"); }

            launchTriggerIconButtonContainer.Controls.Add(launchTrigger);

            switch (this.MessageType)
            {
                case InformationStatusPanel_MessageType.Alert:
                    launchTrigger.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG, this.LoadImagesForCustomerManager);
                    launchTrigger.AlternateText = _GlobalResources.Alert;
                    break;
                case InformationStatusPanel_MessageType.Error:
                    launchTrigger.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ERROR, ImageFiles.EXT_PNG, this.LoadImagesForCustomerManager);
                    launchTrigger.AlternateText = _GlobalResources.Error;
                    break;
                case InformationStatusPanel_MessageType.Help:
                    launchTrigger.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_QUESTION, ImageFiles.EXT_PNG, this.LoadImagesForCustomerManager);
                    launchTrigger.AlternateText = _GlobalResources.Help;
                    break;
                case InformationStatusPanel_MessageType.Information:
                    launchTrigger.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION, ImageFiles.EXT_PNG, this.LoadImagesForCustomerManager);
                    launchTrigger.AlternateText = _GlobalResources.Information;
                    break;
                case InformationStatusPanel_MessageType.Success:
                    launchTrigger.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG, this.LoadImagesForCustomerManager);
                    launchTrigger.AlternateText = _GlobalResources.Success;
                    break;
                default: // use information as default
                    launchTrigger.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION, ImageFiles.EXT_PNG, this.LoadImagesForCustomerManager);
                    launchTrigger.AlternateText = _GlobalResources.Information;
                    break;
            }

            // attach
            this.Controls.Add(launchTriggerIconButtonContainer);
        }
        #endregion

        #region _BuildBlackoutPanel
        /// <summary>
        /// Builds the blackout panel for when this is a popup with a blackout.
        /// </summary>
        private void _BuildBlackoutPanel()
        {
            // build and attach the blackout
            Panel blackoutPanel = new Panel();
            blackoutPanel.ID = this.ID + "BlackoutPanel";
            blackoutPanel.CssClass = BASE_CSS_CLASS + "-BlackoutPanel";
            this.Controls.Add(blackoutPanel);

            // if we're using a launch trigger, set the blackout to display none
            if (this.UseLaunchTrigger)
            { blackoutPanel.Style.Add("display", "none"); }
        }
        #endregion

        #region _BuildContentPanel
        /// <summary>
        /// Builds the main content panel where all the messaging and styling go.
        /// </summary>
        private void _BuildContentPanel()
        {
            // wrapper
            Panel wrapperContainer = new Panel();
            wrapperContainer.ID = this.ID + "WrapperContainer";
            wrapperContainer.CssClass = this._SetCssClassesBasedOnType();

            // messaging container
            Panel messagingContainer = new Panel();
            messagingContainer.ID = this.ID + "MessagingContainer";
            messagingContainer.CssClass = BASE_CSS_CLASS + "-MessagingContainer";
            wrapperContainer.Controls.Add(messagingContainer);

            // message header - based on the message type
            HtmlGenericControl messageHeader = new HtmlGenericControl("p");
            messageHeader.Attributes.Add("class", BASE_CSS_CLASS + "-MessageHeader");

            switch (this.MessageType)
            {
                case InformationStatusPanel_MessageType.Alert:
                    messageHeader.InnerHtml = _GlobalResources.Alert;
                    break;
                case InformationStatusPanel_MessageType.Error:
                    messageHeader.InnerHtml = _GlobalResources.Error;
                    break;
                case InformationStatusPanel_MessageType.Help:
                    messageHeader.InnerHtml = _GlobalResources.Help;
                    break;
                case InformationStatusPanel_MessageType.Information:
                    messageHeader.InnerHtml = _GlobalResources.Information;
                    break;
                case InformationStatusPanel_MessageType.Success:
                    messageHeader.InnerHtml = _GlobalResources.Success;
                    break;
                default:
                    break;
            }

            // if the message header overide text is populated, that should be used
            if (!String.IsNullOrWhiteSpace(this.MessageHeaderOverrideText))
            { messageHeader.InnerHtml = this.MessageHeaderOverrideText; }

            messagingContainer.Controls.Add(messageHeader);

            // message
            HtmlGenericControl message = new HtmlGenericControl("p");
            message.Attributes.Add("class", BASE_CSS_CLASS + "-Message");
            message.InnerHtml = this.Message;
            messagingContainer.Controls.Add(message);

            // if there is body text or a body control, add that
            if (!String.IsNullOrWhiteSpace(this.BodyText) || this.BodyControl != null)
            {
                // wrapper
                Panel bodyWrapper = new Panel();
                bodyWrapper.ID = this.ID + "BodyWrapperContainer";
                bodyWrapper.CssClass = BASE_CSS_CLASS + "-BodyWrapperContainer";
                wrapperContainer.Controls.Add(bodyWrapper);

                // body text - <p>
                if (!String.IsNullOrWhiteSpace(this.BodyText))
                {
                    HtmlGenericControl bodyText = new HtmlGenericControl("p");
                    bodyText.InnerHtml = this.BodyText;
                    bodyWrapper.Controls.Add(bodyText);
                }

                // body control - just attach the control
                if (this.BodyControl != null)
                { bodyWrapper.Controls.Add(this.BodyControl); }
            }

            // if this is a popup and we should add a close button, then add a close button
            if (
                (this.Type == InformationStatusPanel_Type.DraggablePopup
                || this.Type == InformationStatusPanel_Type.DraggablePopupWithBlackout
                || this.Type == InformationStatusPanel_Type.Popup
                || this.Type == InformationStatusPanel_Type.PopupWithBlackout)
                && this.AddPopupCloseButton
               )
            {
                // close button wrapper
                Panel closeButtonWrapper = new Panel();
                closeButtonWrapper.ID = this.ID + "CloseButtonWrapperContainer";
                closeButtonWrapper.CssClass = BASE_CSS_CLASS + "-CloseButtonWrapperContainer";
                wrapperContainer.Controls.Add(closeButtonWrapper);

                // close button
                Button closeButton = new Button();
                closeButton.ID = this.ID + "CloseButton";
                closeButton.CssClass = "Button";
                closeButton.Text = _GlobalResources.OK;
                
                if (this.Type == InformationStatusPanel_Type.DraggablePopupWithBlackout || this.Type == InformationStatusPanel_Type.PopupWithBlackout)
                { closeButton.Attributes.Add("onclick", "CloseInformationStatusPanel(\"" + this.ID + "\", true); return false;"); }
                else
                { closeButton.Attributes.Add("onclick", "CloseInformationStatusPanel(\"" + this.ID + "\", false); return false;"); }

                closeButtonWrapper.Controls.Add(closeButton);
            }

            // if we're using a launch trigger, set the wrapper to display none
            if (this.UseLaunchTrigger)
            { wrapperContainer.Style.Add("display", "none"); }

            // attach
            this.Controls.Add(wrapperContainer);
        }
        #endregion

        #region _SetCssClassesBasedOnType
        /// <summary>
        /// Returns a string of classes for the panel based on the type of panel and the message type for the panel.
        /// Classes are actually applied to the panel's main content wrapper.
        /// </summary>
        /// <returns>string of css classes</returns>
        private string _SetCssClassesBasedOnType()
        {
            string cssClass = BASE_CSS_CLASS;

            // set classes based on if this is a popup, is draggable, or is onpage
            switch (this.Type)
            {
                case InformationStatusPanel_Type.DraggablePopup:
                case InformationStatusPanel_Type.DraggablePopupWithBlackout:
                case InformationStatusPanel_Type.Popup:
                case InformationStatusPanel_Type.PopupWithBlackout:
                    // set popup class
                    cssClass += " " + BASE_CSS_CLASS + "-Popup";

                    // set draggable class
                    if (this.Type == InformationStatusPanel_Type.DraggablePopup || this.Type == InformationStatusPanel_Type.DraggablePopupWithBlackout)
                    { cssClass += " " + BASE_CSS_CLASS + "-Draggable"; }

                    break;
                case InformationStatusPanel_Type.OnPage:
                    // set onpage class
                    cssClass += " " + BASE_CSS_CLASS + "-OnPage";
                    
                    break;
                default:
                    break;                        
            }

            // set additional classes based on message type (Alert, Error, Help, Information, Success)
            switch (this.MessageType)
            {
                case InformationStatusPanel_MessageType.Alert:
                    cssClass += " " + BASE_CSS_CLASS + "-ALERT";
                    break;
                case InformationStatusPanel_MessageType.Error:
                    cssClass += " " + BASE_CSS_CLASS + "-ERROR";
                    break;
                case InformationStatusPanel_MessageType.Help:
                    cssClass += " " + BASE_CSS_CLASS + "-HELP";
                    break;
                case InformationStatusPanel_MessageType.Information:
                    cssClass += " " + BASE_CSS_CLASS + "-INFORMATION";
                    break;
                case InformationStatusPanel_MessageType.Success:
                    cssClass += " " + BASE_CSS_CLASS + "-SUCCESS";
                    break;
                default:
                    break;
            }

            // return
            return cssClass;
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides OnPreRender
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)            
            ScriptManager.RegisterClientScriptResource(this.Page, typeof(Asentia.Controls.ClientScript), "Asentia.Controls.InformationStatusPanel.js");            
            
            // JS start up script for applying actions to elements
            StringBuilder startupJs = new StringBuilder();

            // set draggable if draggable
            if (this.Type == InformationStatusPanel_Type.DraggablePopup || this.Type == InformationStatusPanel_Type.DraggablePopupWithBlackout)
            { startupJs.AppendLine("$(\"#" + this.ID + " ." + BASE_CSS_CLASS + "-Draggable\").draggable({containment: \"#PageContentContainer\"});"); }
         
            // if we are using a launch trigger and an id has been specified (the trigger is external), attach an onclick event to it
            if (this.UseLaunchTrigger && !this.DrawLaunchTriggerIconButton && !String.IsNullOrWhiteSpace(this.LaunchTriggerId))
            {
                if (this.Type == InformationStatusPanel_Type.DraggablePopupWithBlackout || this.Type == InformationStatusPanel_Type.PopupWithBlackout)
                { startupJs.AppendLine("$(\"#" + this.LaunchTriggerId + "\").attr(\"onclick\", \"LaunchInformationStatusPanel(\"" + this.LaunchTriggerId + "\", \"" + this.ID + "\", true," + this.PositionRelative.ToString().ToLower() + ")\");"); }
                else
                { startupJs.AppendLine("$(\"#" + this.LaunchTriggerId + "\").attr(\"onclick\", \"LaunchInformationStatusPanel(\"" + this.LaunchTriggerId + "\", \"" + this.ID + "\", false," + this.PositionRelative.ToString().ToLower() + ")\");"); }
            }

            // if there is a body, prevent mouse wheel scrolling of page when scrolling inside of the body
            if (!String.IsNullOrWhiteSpace(this.BodyText) || this.BodyControl != null)
            {
                startupJs.AppendLine("$(\"#" + this.ID + "BodyWrapperContainer\").bind('mousewheel DOMMouseScroll', function (e) {");
                startupJs.AppendLine("var e0 = e.originalEvent,");
                startupJs.AppendLine("    delta = e0.wheelDelta || -e0.detail;");
                startupJs.AppendLine("");
                startupJs.AppendLine("this.scrollTop += (delta < 0 ? 1 : -1) * 30;");
                startupJs.AppendLine("e.preventDefault();");
                startupJs.AppendLine("});");
            }

            /*
            TODO: Decide if auto-hiding the success message after a few seconds makes sense.
            // if this a success message, make it disappear after a half second
            if (this.MessageType == InformationStatusPanel_MessageType.Success)
            { startupJs.AppendLine("setTimeout(function() { CloseInformationStatusPanel(\"" + this.ID + "\", true); }, 500);"); }
            */

            // add start up script
            ScriptManager.RegisterStartupScript(this.Page, typeof(InformationStatusPanel), this.ID + "StartupJs", startupJs.ToString(), true);
        }
        #endregion

        #region Render
        /// <summary>
        /// Overrides Render
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            // set inline styles for position and display
            if (this.DisplayInlineBlock)
            { this.Style.Add("display", "inline-block"); }

            // this may no longer be needed since we have jQuery calculating relative position, but leave here for now
            //if (this.PositionRelative)
            //{ this.Style.Add("position", "relative"); }

            // if we're building a trigger control for this, then lets do it, but only for popup types
            if (
                this.UseLaunchTrigger 
                && this.DrawLaunchTriggerIconButton 
                && (
                    this.Type == InformationStatusPanel_Type.DraggablePopup 
                    || this.Type == InformationStatusPanel_Type.DraggablePopupWithBlackout
                    || this.Type == InformationStatusPanel_Type.Popup
                    || this.Type == InformationStatusPanel_Type.PopupWithBlackout
                   )
               )
            {
                this._BuildLaunchTriggerIconButton();
            }

            // if this is a popup with blackout, build the blackout
            if (this.Type == InformationStatusPanel_Type.DraggablePopupWithBlackout || this.Type == InformationStatusPanel_Type.PopupWithBlackout)
            {
                this._BuildBlackoutPanel();
            }

            // build the content panel
            this._BuildContentPanel();

            // execute base
            base.Render(writer);
        }
        #endregion
        #endregion
    }
}
