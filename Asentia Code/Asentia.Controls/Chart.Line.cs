﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    #region Chart_Line_Dataset CLASS
    public class Chart_Line_Dataset
    {
        #region Constructor
        public Chart_Line_Dataset(string fillColor, string strokeColor, string pointColor, string pointStrokeColor, object[] data)
        {
            this._FillColor = fillColor;
            this._StrokeColor = strokeColor;
            this._PointColor = pointColor;
            this._PointStrokeColor = pointStrokeColor;
            this._Data = data;
        }
        #endregion

        #region Properties
        public string FillColor
        {
            get { return this._FillColor; }
        }

        public string StrokeColor
        {
            get { return this._StrokeColor; }
        }

        public string PointColor
        {
            get { return this._PointColor; }
        }

        public string PointStrokeColor
        {
            get { return this._PointStrokeColor; }
        }

        public object[] Data
        {
            get { return this._Data; }
        }
        #endregion

        #region Private Properties
        private string _FillColor;
        private string _StrokeColor;
        private string _PointColor;
        private string _PointStrokeColor;
        private object[] _Data;
        #endregion
    }
    #endregion

    public class Chart_Line : WebControl
    {
        #region Constructor
        public Chart_Line(string id, int width, int height)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.CanvasWidth = width;
            this.CanvasHeight = height;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The width of the canvas tag.
        /// </summary>
        public int CanvasWidth;

        /// <summary>
        /// The height of the canvas tag.
        /// </summary>
        public int CanvasHeight;

        /// <summary>
        /// Show scale over chart data?
        /// </summary>		
	    public bool ScaleOverlay = false;

        /// <summary>
        /// Override with hard-coded scale?
        /// If true, ScaleSteps, ScaleStepWidth and ScaleStartValue must be set.
        /// </summary>
	    public bool ScaleOverride = false;

        /// <summary>
        /// The number of steps in the hard-coded scale.
        /// Must be set if ScaleOverride is true.
        /// </summary>
	    public int? ScaleSteps = null;

        /// <summary>
        /// The value jump in the hard-coded scale.
        /// Must be set if ScaleOverride is true.
        /// </summary>
	    public int? ScaleStepWidth = null;

        /// <summary>
        /// The starting value of the hard-coded scale.
        /// Must be set if ScaleOverride is true.
        /// </summary>
	    public int? ScaleStartValue = null;

        /// <summary>
        /// Color of the scale line.
        /// </summary>
	    public string ScaleLineColor = "rgba(0,0,0,.1)";

        /// <summary>
        /// Width of the scale line.
        /// </summary>
	    public int ScaleLineWidth = 1;

        /// <summary>
        /// Show labels on the scale?
        /// </summary>
	    public bool ScaleShowLabels = true;

        /// <summary>
        /// Interpolated JS string - can access value
        /// </summary>
	    public string ScaleLabel = "<%=value%>";

        /// <summary>
        /// The font of the scale labels.
        /// </summary>
	    public string ScaleFontFamily = "Helvetica";

        /// <summary>
        /// The font size of the scale labels in pixels.
        /// </summary>
	    public int ScaleFontSize = 12;

        /// <summary>
        /// The font style of the scale labels.
        /// </summary>	
	    public string ScaleFontStyle = "normal";

        /// <summary>
        /// The font color of the scale labels.
        /// </summary>
        public string ScaleFontColor = "#3D3A39";

        /// <summary>
        /// Show grid lines across chart?
        /// </summary>
	    public bool ScaleShowGridLines = true;

        /// <summary>
        /// Color of scale grid lines.
        /// </summary>
	    public string ScaleGridLineColor = "rgba(0,0,0,.05)";

        /// <summary>
        /// Width of the scale grid lines.
        /// </summary>
	    public int ScaleGridLineWidth = 1;
	
	    /// <summary>
	    /// Show line curve between points?
	    /// </summary>
	    public bool BezierCurve = true;
	
	    /// <summary>
	    /// Show a dot for each point?
	    /// </summary>
	    public bool PointDot = true;
	
	    /// <summary>
	    /// Radius of point dot in pixels.
	    /// </summary>
	    public int PointDotRadius = 3;
	
	    /// <summary>
	    /// Stroke width of point dot.
	    /// </summary>
	    public int PointDotStrokeWidth = 1;
	
	    /// <summary>
	    /// Show stroke for datasets?
	    /// </summary>
	    public bool DatasetStroke = true;
	
	    /// <summary>
	    /// Width of dataset stroke in pixels.
	    /// </summary>
	    public int DatasetStrokeWidth = 2;
	
	    /// <summary>
	    /// Fill dataset with color?
	    /// </summary>
	    public bool DatasetFill = true;

        /// <summary>
        /// Animate the chart?
        /// </summary>
	    public bool Animation = true;

        /// <summary>
        /// Number of animation steps.
        /// </summary>
	    public int AnimationSteps = 60;

        /// <summary>
        /// Animation easing effect.
        /// </summary>
	    public string AnimationEasing = "easeOutQuart";

        /// <summary>
        /// Method to fire when animation is complete.
        /// </summary>
	    public string OnAnimationComplete = null;
        #endregion

        #region Private Properties
        /// <summary>
        /// An array of labels for the data.
        /// </summary>
        List<string> _DataLabels = null;

        /// <summary>
        /// An ArrayList of datasets for this chart.
        /// </summary>
        ArrayList _Datasets = null;
        #endregion

        #region Methods
        #region AddLabel
        /// <summary>
        /// Adds a label to the labels array.
        /// </summary>
        /// <param name="label">label</param>
        public void AddLabel(string label)
        {
            if (this._DataLabels == null)
            { this._DataLabels = new List<string>(); }

            this._DataLabels.Add(label);
        }
        #endregion

        #region ClearLabels
        /// <summary>
        /// Clears the labels array.
        /// </summary>
        public void ClearLabels()
        {
            if (this._DataLabels == null)
            { return; }

            this._DataLabels.Clear();
        }
        #endregion

        #region AddDataset
        // TODO: VALIDATIONS
        /// <summary>
        /// Adds a dataset of type Chart_Line_Dataset to the dataset ArrayList.
        /// </summary>
        /// <param name="dataset">dataset data</param>
        public void AddDataset(Chart_Line_Dataset dataset)
        {
            if (this._Datasets == null)
            { this._Datasets = new ArrayList(); }

            this._Datasets.Add(dataset);
        }
        #endregion

        #region ClearDatasets
        /// <summary>
        /// Clears the dataset ArrayList.
        /// </summary>
        public void ClearDatasets()
        {
            if (this._Datasets == null)
            { return; }

            this._Datasets.Clear();
        }
        #endregion
        #endregion

        #region Private Methods
        #region _BuildOptionsJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's options
        /// based on the option properties.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildOptionsJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON
            sb.AppendLine("var " + this.ID + "Options = {");
            sb.AppendLine(" scaleOverlay : " + this.ScaleOverlay.ToString().ToLower() + ",");
            sb.AppendLine(" scaleOverride : " + this.ScaleOverride.ToString().ToLower() + ",");
            sb.AppendLine(" scaleSteps : " + (this.ScaleSteps == null ? "null" : this.ScaleSteps.ToString()) + ",");
            sb.AppendLine(" scaleStepWidth : " + (this.ScaleStepWidth == null ? "null" : this.ScaleStepWidth.ToString()) + ",");
            sb.AppendLine(" scaleStartValue : " + (this.ScaleStartValue == null ? "null" : this.ScaleStartValue.ToString()) + ",");
            sb.AppendLine(" scaleLineColor : " + this._FormatJSStringVarValue(this.ScaleLineColor) + ",");
            sb.AppendLine(" scaleLineWidth : " + this.ScaleLineWidth.ToString() + ",");
            sb.AppendLine(" scaleShowLabels : " + this.ScaleShowLabels.ToString().ToLower() + ",");
            sb.AppendLine(" scaleLabel : " + this._FormatJSStringVarValue(this.ScaleLabel) + ",");
            sb.AppendLine(" scaleFontFamily : " + this._FormatJSStringVarValue(this.ScaleFontFamily) + ",");
            sb.AppendLine(" scaleFontSize : " + this.ScaleFontSize.ToString() + ",");
            sb.AppendLine(" scaleFontStyle : " + this._FormatJSStringVarValue(this.ScaleFontStyle) + ",");
            sb.AppendLine(" scaleFontColor : " + this._FormatJSStringVarValue(this.ScaleFontColor) + ",");
            sb.AppendLine(" scaleShowGridLines : " + this.ScaleShowGridLines.ToString().ToLower() + ",");
            sb.AppendLine(" scaleGridLineColor : " + this._FormatJSStringVarValue(this.ScaleGridLineColor) + ",");
            sb.AppendLine(" scaleGridLineWidth : " + this.ScaleGridLineWidth.ToString() + ",");
            sb.AppendLine(" bezierCurve : " + this.BezierCurve.ToString().ToLower() + ",");
            sb.AppendLine(" pointDot : " + this.PointDot.ToString().ToLower() + ",");
            sb.AppendLine(" pointDotRadius : " + this.PointDotRadius.ToString() + ",");
            sb.AppendLine(" pointDotStrokeWidth : " + this.PointDotStrokeWidth.ToString() + ",");
            sb.AppendLine(" datasetStroke : " + this.DatasetStroke.ToString().ToLower() + ",");
            sb.AppendLine(" datasetStrokeWidth : " + this.DatasetStrokeWidth.ToString() + ",");
            sb.AppendLine(" datasetFill : " + this.DatasetFill.ToString().ToLower() + ",");
            sb.AppendLine(" animation : " + this.Animation.ToString().ToLower() + ",");
            sb.AppendLine(" animationSteps : " + this.AnimationSteps.ToString() + ",");
            sb.AppendLine(" animationEasing : " + this._FormatJSStringVarValue(this.AnimationEasing) + ",");
            sb.AppendLine(" onAnimationComplete : " + this._FormatJSStringVarValue(this.OnAnimationComplete));
            sb.AppendLine("};");

            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildDataJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's data
        /// based on the datasets popuated for this chart.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildDataJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // begin variable declaration
            sb.AppendLine("var " + this.ID + "Data = {");

            // build labels JSON
            string dataLabels = String.Empty;

            foreach (string label in this._DataLabels)
            { dataLabels += "\"" + label + "\","; }

            dataLabels = dataLabels.TrimEnd(',');

            // append labels JSON
            sb.AppendLine(" labels : [" + dataLabels + "],");

            // build and append datasets JSON
            sb.AppendLine(" datasets : [");

            int i = 0;

            foreach (Chart_Line_Dataset ds in this._Datasets)
            {
                // increment i
                i++;

                // build the data
                sb.AppendLine("     {");
                sb.AppendLine("     fillColor : " + this._FormatJSStringVarValue(ds.FillColor) + ",");
                sb.AppendLine("     strokeColor : " + this._FormatJSStringVarValue(ds.StrokeColor) + ",");
                sb.AppendLine("     pointColor : " + this._FormatJSStringVarValue(ds.PointColor) + ",");
                sb.AppendLine("     pointStrokeColor : " + this._FormatJSStringVarValue(ds.PointStrokeColor) + ",");
                string dataValues = string.Join(",", ds.Data);
                sb.AppendLine("     data : [" + dataValues + "]");

                if (i < this._Datasets.Count)
                { sb.AppendLine("     },"); }
                else
                { sb.AppendLine("     }"); }
            }

            sb.AppendLine(" ]");
            sb.AppendLine("};");

            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildAndRegisterJSBlock
        /// <summary>
        /// Builds the JS block that renders the chart, and registers the
        /// JS to this control.
        /// </summary>
        private void _BuildAndRegisterJSBlock()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON
            sb.AppendLine(this._BuildOptionsJSONString());

            // build data JSON
            sb.AppendLine(this._BuildDataJSONString());

            // build call to chart function
            sb.AppendLine("var " + this.ID + "Bound = new Chart(document.getElementById(\"" + this.ID + "Canvas\").getContext(\"2d\")).Line(" + this.ID + "Data, " + this.ID + "Options);");

            // register script
            ScriptManager.RegisterStartupScript(this, this.GetType(), this.ID + "StartUpScript", sb.ToString(), true);
        }
        #endregion

        #region _FormatJSStringVarValue
        /// <summary>
        /// Formats a string for attachment to a JS variable.
        /// </summary>
        /// <param name="value">string value</param>
        /// <returns>formatted string</returns>
        private string _FormatJSStringVarValue(string value)
        {
            if (value == null)
            { return "null"; }
            else
            { return "\"" + value + "\""; }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Chart.js");
        }
        #endregion

        #region RenderContents
        protected override void RenderContents(HtmlTextWriter writer)
        {
            // canvas
            HtmlGenericControl canvasTag = new HtmlGenericControl("canvas");
            canvasTag.ID = this.ID + "Canvas";
            canvasTag.Attributes.Add("width", this.CanvasWidth.ToString());
            canvasTag.Attributes.Add("height", this.CanvasHeight.ToString());

            // attach controls
            this.Controls.Add(canvasTag);

            // build and register JS block
            this._BuildAndRegisterJSBlock();

            // execute base
            base.RenderContents(writer);
        }
        #endregion
        #endregion
    }
}
