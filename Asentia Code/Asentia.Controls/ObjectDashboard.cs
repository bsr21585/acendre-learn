﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    public class ObjectDashboard : WebControl
    {
        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">the id of this control</param>        
        public ObjectDashboard(string id)
            : base(HtmlTextWriterTag.Div)
        {
            // set the ID
            this.ID = id;

            // set css style for container div
            this.CssClass = "ObjectDashboardContainer";
        }
        #endregion

        #region Properties
        #endregion

        #region Enums
        /// <summary>
        /// WidgetItemType enum
        /// </summary>
        public enum WidgetItemType
        {
            Text = 0,
            Object = 1, 
            Link = 2,
        }
        #endregion

        #region Classes
        /// <summary>
        /// WidgetItem class
        /// </summary>
        public class WidgetItem
        {
            public string Id;
            public string Label;
            public object Object;
            public WidgetItemType ObjectType;

            public WidgetItem(string id, string label, object obj, WidgetItemType objectType)
            {
                this.Id = id;
                this.Label = label;                
                this.Object = obj;
                this.ObjectType = objectType;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a widget to the object dashboard.
        /// </summary>
        /// <param name="id">id of the widget</param>
        /// <param name="headerText">widget header text</param>
        /// <param name="items">widget items</param>
        public void AddWidget(string id, string headerText, List<WidgetItem> items)
        {
            // widget panel
            Panel objectDashboardWidget = new Panel();
            objectDashboardWidget.ID = this.ID + "_" + id;
            objectDashboardWidget.CssClass = "ObjectDashboardWidget";

            // widget inner wrapper panel
            Panel objectDashboardWidgetInnerWrapper = new Panel();
            objectDashboardWidgetInnerWrapper.ID = this.ID + "_" + id + "_InnerWrapper";
            objectDashboardWidgetInnerWrapper.CssClass = "ObjectDashboardWidgetInnerWrapper";
            objectDashboardWidget.Controls.Add(objectDashboardWidgetInnerWrapper);

            // widget header
            Panel objectDashboardWidgetHeader = new Panel();
            objectDashboardWidgetHeader.ID = this.ID + "_" + id + "_Header";
            objectDashboardWidgetHeader.CssClass = "ObjectDashboardWidgetHeader";
            objectDashboardWidgetInnerWrapper.Controls.Add(objectDashboardWidgetHeader);

            Literal objectDashboardWidgetHeaderText = new Literal();
            objectDashboardWidgetHeaderText.Text = headerText;
            objectDashboardWidgetHeader.Controls.Add(objectDashboardWidgetHeaderText);

            // widget content
            Panel objectDashboardWidgetContent = new Panel();
            objectDashboardWidgetContent.ID = this.ID + "_" + id + "_Content";
            objectDashboardWidgetContent.CssClass = "ObjectDashboardWidgetContent";
            objectDashboardWidgetInnerWrapper.Controls.Add(objectDashboardWidgetContent);

            // widget items
            foreach (WidgetItem item in items)
            {
                // widget item
                Panel objectDashboardWidgetItem = new Panel();
                objectDashboardWidgetItem.ID = this.ID + "_" + id + "_" + item.Id;
                objectDashboardWidgetItem.CssClass = "ObjectDashboardWidgetItem";
                objectDashboardWidgetContent.Controls.Add(objectDashboardWidgetItem);

                // widget item label
                if (!String.IsNullOrEmpty(item.Label))
                {
                    Panel objectDashboardWidgetItemLabel = new Panel();
                    objectDashboardWidgetItemLabel.ID = this.ID + "_" + id + "_" + item.Id + "_Label";
                    objectDashboardWidgetItemLabel.CssClass = "ObjectDashboardWidgetItemLabel";
                    objectDashboardWidgetItem.Controls.Add(objectDashboardWidgetItemLabel);

                    Literal objectDashboardWidgetItemLabelText = new Literal();
                    objectDashboardWidgetItemLabelText.Text = item.Label;
                    objectDashboardWidgetItemLabel.Controls.Add(objectDashboardWidgetItemLabelText);
                }

                // widget item object
                if (item.ObjectType == WidgetItemType.Text)
                {
                    Panel objectDashboardWidgetItemText = new Panel();
                    objectDashboardWidgetItemText.ID = this.ID + "_" + id + "_" + item.Id + "_Text";
                    objectDashboardWidgetItemText.CssClass = "ObjectDashboardWidgetItemText";
                    objectDashboardWidgetItem.Controls.Add(objectDashboardWidgetItemText);

                    Literal objectDashboardWidgetItemTextText = new Literal();
                    objectDashboardWidgetItemTextText.Text = (string)item.Object;
                    objectDashboardWidgetItemText.Controls.Add(objectDashboardWidgetItemTextText);
                }
                else if (item.ObjectType == WidgetItemType.Object)
                {
                    Panel objectDashboardWidgetItemObject= new Panel();
                    objectDashboardWidgetItemObject.ID = this.ID + "_" + id + "_" + item.Id + "_Object";
                    objectDashboardWidgetItemObject.CssClass = "ObjectDashboardWidgetItemObject";
                    objectDashboardWidgetItem.Controls.Add(objectDashboardWidgetItemObject);

                    objectDashboardWidgetItemObject.Controls.Add((Control)item.Object);
                }
                else if (item.ObjectType == WidgetItemType.Link)
                {
                    Panel objectDashboardWidgetItemLink = new Panel();
                    objectDashboardWidgetItemLink.ID = this.ID + "_" + id + "_" + item.Id + "_Link";
                    objectDashboardWidgetItemLink.CssClass = "ObjectDashboardWidgetItemLink ImageLink";
                    objectDashboardWidgetItem.Controls.Add(objectDashboardWidgetItemLink);

                    objectDashboardWidgetItemLink.Controls.Add((Control)item.Object);
                }
                else
                { }
            }

            // add the widget to the container
            this.Controls.Add(objectDashboardWidget);
        }
        #endregion
    }
}
