﻿// Constructor
function Grid()
{ }

// Properties for the row CSS classes.
Grid.ROW_CLASS_SELECTED = "GridRowSelected";
Grid.ROW_CLASS_HOVER = "GridRowHover";

// Function: ToggleSelectAllRecords
// Handles the actions that need to be performed when the select
// all checkbox is checked or unchecked.
// Processes include checking/unchecking all of the records in the
// grid, depending on whether the select all checkbox has been checked
// or unchecked, and hightlighting and removing highlighting on the 
// rows depending on whether or not they are checked.
Grid.ToggleSelectAllRecords = function (idPrefix, selectAllCheckbox) {
    // loop through all of the checkboxes checking or unchecking
    // them based on the checked/unchecked status of the select all
    // checkbox; additionally, highlight the rows when checked, and
    // return to normal state when unchecked
    for (var i = 0; i > -1; i++) {
        var selectRecordCheckbox = document.getElementById(idPrefix + "_GridSelectRecord_" + i);
        var recordRow = document.getElementById(idPrefix + "_GridRow_" + i);

        if (selectRecordCheckbox != null && recordRow != null) {
            if (selectAllCheckbox.checked && !selectRecordCheckbox.disabled) {
                selectRecordCheckbox.checked = true;
                Grid.ApplyRowClass(recordRow, Grid.ROW_CLASS_SELECTED);
            }
            else {
                selectRecordCheckbox.checked = false;
                Grid.RemoveRowClasses(recordRow);
            }
        }
        else {
            break;
        }
    }
};

// Function: ToggleSelectRecord
// Handles the selection of an individual record in the grid.
// Processes include highlighting and removing highlighting on the row
// depending on the checked state of the checkbox, and evaluating the
// checked/unchecked state of the select all checkbox based on whether
// or not all records are selected.
Grid.ToggleSelectRecord = function (idPrefix, selectRecordCheckbox, rowIndex, isRowClick) {
    // if the checkbox that was passed does not exist, die
    if (selectRecordCheckbox == null)
    { return; }

    // if this was a row click, and the checkbox was not disabled, we need to actually toggle the checkbox
    if (isRowClick) {
        // get the target of the click, important to determine if the target was an element that causes an action
        // if it is the checkbox, two events will fire, one for the row, and one for the checkbox; we can exit the call for the row
        // if it is an <a> tag or <img> inside <a> tag, we dont need the row event to fire because the intent was to click a link
        var $clickTarget = $(event.target);

        if ($clickTarget.is(":checkbox") || $clickTarget.is("a") || $clickTarget.is("a img") || $clickTarget.is("img") || $clickTarget.is("input") || $clickTarget.is("select")) {
            return;
        }
        
        // if we get here, continue operations for row click
        if (!selectRecordCheckbox.disabled) {
            if (!selectRecordCheckbox.checked) {
                selectRecordCheckbox.checked = true;
            }
            else {
                selectRecordCheckbox.checked = false;
            }
        }
    }

    // get the row that the checkbox is in
    var recordRow = document.getElementById(idPrefix + "_GridRow_" + rowIndex);
    
    // if the checkbox is checked, highlight the row
    // else, return the row to normal state
    if (recordRow != null) {        
        if (selectRecordCheckbox.checked) {
            Grid.ApplyRowClass(recordRow, Grid.ROW_CLASS_SELECTED);
        }
        else {
            Grid.RemoveRowClasses(recordRow);
        }
    }

    // check to see if all of the select record checkboxes are checked
    // so that we can check/uncheck the select all checkbox

    // grab the select all checkbox, and if it doesnt exist, die
    var selectAllCheckbox = document.getElementById(idPrefix + "_GridSelectAllRecords");
    if (selectAllCheckbox == null)
    { return; }

    // loop through all checkboxes and if any are unchecked,
    // make sure the select all is unchecked and exit
    for (var i = 0; i > -1; i++) {
        selectRecordCheckbox = document.getElementById(idPrefix + "_GridSelectRecord_" + i);

        if (selectRecordCheckbox != null) {
            if (!selectRecordCheckbox.checked) {
                selectAllCheckbox.checked = false;
                return;
            }
        }
        else {
            break;
        }
    }

    // if we reached this point, all of the select record boxes are
    // checked, so make the select all checkbox checked
    selectAllCheckbox.checked = true;
};

// Function: HoverRowOn
// Handles the highlighting state change when we are hovering on a row.
Grid.HoverRowOn = function (recordRow) {
    // highlight the row we hovered over
    Grid.ApplyRowClass(recordRow, Grid.ROW_CLASS_HOVER);
};

// Function: HoverRowOff
// Handles the highlighting state change when we are no longer hovering on a row.
Grid.HoverRowOff = function (idPrefix, recordRow, index) {
    // grab the select record checkbox for the row
    var selectRecordCheckbox = document.getElementById(idPrefix + "_GridSelectRecord_" + index);

    // if a checkbox exists and it is checked, make the row the 
    // selected state and exit
    if (selectRecordCheckbox != null) {
        if (selectRecordCheckbox.checked) {
            Grid.ApplyRowClass(recordRow, Grid.ROW_CLASS_SELECTED);
            return;
        }
    }

    // if we reach this point, return the row back to its normal state
    Grid.RemoveRowClasses(recordRow);
};

// Function: ApplyRowClass
// Handles the application of a CSS class to a row.
Grid.ApplyRowClass = function (element, name) {
    // remove all of the existing grid row classes
    Helper.RemoveClass(element, Grid.ROW_CLASS_SELECTED);
    Helper.RemoveClass(element, Grid.ROW_CLASS_HOVER);

    // apply the grid row class specified
    Helper.AddClass(element, name);
};

// Function: RemoveRowClasses
// Handles the application of a CSS class to a row.
Grid.RemoveRowClasses = function (element) {
    // remove all of the existing grid row classes
    Helper.RemoveClass(element, Grid.ROW_CLASS_SELECTED);
    Helper.RemoveClass(element, Grid.ROW_CLASS_HOVER);
};

// Function: ShowLoadingPanel
// Shows the grid loading panel when a grid function is performed.
Grid.ShowLoadingPanel = function (idPrefix) {
    document.getElementById(idPrefix + "_GridLoadingPanel").style.display = "block";
}