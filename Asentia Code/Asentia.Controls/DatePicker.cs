﻿using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Asentia.Common;

namespace Asentia.Controls
{
    /// <summary>
    /// Displays a date picker that allows user to input date.
    /// </summary>
    public class DatePicker : WebControl
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the Date Picker control.
        /// </summary>
        /// <param name="id">identifier</param>
        /// <param name="useNone">use "none" checkbox?</param>
        /// <param name="useNow">use "now" checkbox?</param>
        /// <param name="useTime">user time selector options?</param>
        /// <param name="overrideLoadImagesFromCustomerManager"></param>
        public DatePicker(string id, bool useNone, bool useNow, bool useTime, bool overrideLoadImagesFromCustomerManager = false)
            : base(HtmlTextWriterTag.Div)
        {
            // set the properties
            this.ID = id;
            this.UseNone = useNone;
            this.UseNow = useNow;
            this.UseTime = useTime;
            this.OverrideLoadImagesFromCustomerManager = overrideLoadImagesFromCustomerManager;

            // build the controls
            this._BuildControls();
        }
        #endregion

        #region Properties
        /// <summary>
        /// The lowest year in the date picker.
        /// </summary>
        public int LowYear = 0;

        /// <summary>
        /// The highest year in the date picker.
        /// </summary>
        public int HighYear = 0;

        /// <summary>
        /// Does this date picker use the "none" checkbox?
        /// </summary>
        public bool UseNone = false;

        /// <summary>
        /// To Check Uncheck None Checkbox and also check the status of None Checkbox
        /// </summary>
        public bool NoneCheckBoxChecked
        {
            set { this._None.Checked = value; }
            get { return this._None.Checked; }
        }

        /// <summary>
        /// The text to display for the "none" checkbox.
        /// </summary>
        public string NoneCheckboxText
        { set { this._None.Text = " " + value; } }

        /// <summary>
        /// Does this date picker use the "now" checkbox?
        /// </summary>
        public bool UseNow = false;

        /// <summary>
        /// The text to display for the "now" checkbox.
        /// </summary>
        public bool NowCheckBoxChecked
        {
            set { this._Now.Checked = value; }
            get { return this._Now.Checked; }
        }

        /// <summary>
        /// The text to display for the "now" checkbox.
        /// </summary>
        public string NowCheckboxText
        { set { this._Now.Text = " " + value; } }

        /// <summary>
        /// Does this date picker use the time textboxes?
        /// </summary>
        public bool UseTime = false;

        /// <summary>
        /// Gets or sets date value.
        /// </summary>
        public DateTime? Value
        {
            get
            {
                if (this.UseNone)
                {
                    if (this._None.Checked)
                    { return null; }
                }

                if (this.UseNow)
                {
                    if (this._Now.Checked)
                    { return DateTime.UtcNow; }
                }

                if (!String.IsNullOrWhiteSpace(this._InputControl.Text))
                {
                    if (this.UseTime)
                    {
                        int hour = Convert.ToInt32(this._Hour.SelectedValue);
                        int minute = Convert.ToInt32(this._Minute.SelectedValue);
                        string ampm = this._AMPM.SelectedValue;

                        if (ampm == "PM" && hour < 12)
                        { hour += 12; }
                        else if (ampm == "AM" && hour == 12)
                        { hour = 0; }
                        else
                        { }

                        string hourString = hour.ToString();
                        string minuteString = minute.ToString();

                        if (hourString.Length == 1)
                        { hourString = "0" + hourString; }

                        if (minuteString.Length == 1)
                        { minuteString = "0" + minuteString; }

                        return DateTime.ParseExact(this._InputControl.Text + " " + hourString + ":" + minuteString, DateFormatDotNet + " HH:mm", CultureInfo.InvariantCulture);
                    }
                    else
                    { return DateTime.ParseExact(this._InputControl.Text, DateFormatDotNet, CultureInfo.InvariantCulture); }
                }
                else
                { return null; }
            }
            set
            {
                if (value != null)
                {
                    DateTime valueDateTime = Convert.ToDateTime(value);

                    if (this.UseTime)
                    {
                        // get the time from the datetime element
                        int hour = Convert.ToInt32(valueDateTime.ToString("HH"));
                        int minute = Convert.ToInt32(valueDateTime.ToString("mm"));

                        // set the hour & AMPM
                        if (hour == 0)
                        {
                            this._Hour.SelectedValue = "12";
                            this._AMPM.SelectedValue = "AM";
                        }
                        else if (hour > 0 && hour < 12)
                        {
                            this._Hour.SelectedValue = hour.ToString();
                            this._AMPM.SelectedValue = "AM";
                        }
                        else
                        {
                            // to handle the case if dropdown list has the selected value = 12
                            if (hour == 12)
                            {
                                this._Hour.SelectedValue = hour.ToString();
                                this._AMPM.SelectedValue = "PM";
                            }
                            else
                            {
                                this._Hour.SelectedValue = (hour - 12).ToString();
                                this._AMPM.SelectedValue = "PM";
                            }
                        }

                        // set the minute
                        string minuteString = minute.ToString();

                        if (minuteString.Length == 1)
                        { minuteString = "0" + minuteString; }

                        this._Minute.SelectedValue = minuteString;

                        // set the date
                        this._InputControl.Text = valueDateTime.ToString(DateFormatDotNet);
                    }
                    else
                    { this._InputControl.Text = valueDateTime.ToString(DateFormatDotNet); }
                }
                else
                {
                    this._InputControl.Text = string.Empty;

                    if (this.UseNone)
                    { this._None.Checked = true; }
                }
            }
        }

        /// <summary>
        /// Gets string value of the input control.
        /// </summary>
        public string ValueString
        {
            get
            {
                if (this.UseNone)
                {
                    if (this._None.Checked)
                    { return null; }
                }

                if (this.UseNow)
                {
                    if (this._Now.Checked)
                    { return DateTime.UtcNow.ToString(DateFormatDotNet); }
                }

                if (!String.IsNullOrWhiteSpace(this._InputControl.Text))
                {
                    if (this.UseTime)
                    {
                        int hour = Convert.ToInt32(this._Hour.SelectedValue);
                        int minute = Convert.ToInt32(this._Minute.SelectedValue);
                        string ampm = this._AMPM.SelectedValue;

                        if (ampm == "PM" && hour < 12)
                        { hour += 12; }
                        else if (ampm == "PM" && hour == 12)
                        { hour = 0; }
                        else
                        { }

                        string hourString = hour.ToString();
                        string minuteString = minute.ToString();

                        if (hourString.Length == 1)
                        { hourString = "0" + hourString; }

                        if (minuteString.Length == 1)
                        { minuteString = "0" + minuteString; }

                        return this._InputControl.Text + " " + hourString + ":" + minuteString;
                    }
                    else
                    { return this._InputControl.Text; }
                }
                else
                { return null; }
            }
        }


        /// <summary>
        /// load images from customar manager website project if True
        /// </summary>
        public bool OverrideLoadImagesFromCustomerManager;

        #endregion

        #region Private Properties
        /// <summary>
        /// Date format for .NET
        /// </summary>
        public const string DateFormatDotNet = "yyyy-MM-dd"; // ISO-8601

        /// <summary>
        /// Date format for JS
        /// </summary>
        public const string DateFormatJS = "yy-mm-dd"; // ISO-8601

        /// <summary>
        /// TextBox control to hold the selected date value.
        /// </summary>
        private TextBox _InputControl = new TextBox();

        /// <summary>
        /// Drop-Down list for hour.
        /// </summary>
        private DropDownList _Hour = new DropDownList();

        /// <summary>
        /// Drop-Down list for minute.
        /// </summary>
        private DropDownList _Minute = new DropDownList();

        /// <summary>
        /// Drop-Down list for AM/PM.
        /// </summary>
        private DropDownList _AMPM = new DropDownList();

        /// <summary>
        /// CheckBox control for "none."
        /// </summary>
        private CheckBox _None = new CheckBox();

        /// <summary>
        /// CheckBox control for "now."
        /// </summary>
        private CheckBox _Now = new CheckBox();

        /// <summary>
        /// Calendar icon for date picker.
        /// </summary>
        private Image _DatePickerImage = new Image();
        #endregion

        #region Private Methods
        #region _BuildControls
        /// <summary>
        /// Builds the text box input and calendar image controls.
        /// </summary>
        private void _BuildControls()
        {
            // build the input control
            this._InputControl.ID = this.ID + "_DateInputControl";
            this._InputControl.CssClass = "DatePickerInput";
            if (this.ID == "SessionDtStart_1")
            {
                this._InputControl.Attributes.Add("onChange", "SessionDtStartChange(this);");
            }
            this._InputControl.ReadOnly = false;
            this._InputControl.Attributes.Add("autocomplete", "off");
            this.Controls.Add(this._InputControl);

            // build the calendar image control
            this._DatePickerImage.ID = this.ID + "_DatePickerImage";
            this._DatePickerImage.CssClass = "DatePickerImage SmallIcon";
            this._DatePickerImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR,
                                                                    ImageFiles.EXT_PNG, this.OverrideLoadImagesFromCustomerManager);

            this.Controls.Add(this._DatePickerImage);

            if (this.UseTime)
            {
                // hour
                this._Hour.ID = this.ID + "_HourInputControl";
                this._Hour.CssClass = "DatePickerTimeHour";

                this._Hour.Items.Add("12");

                int hour;

                for (hour = 1; hour < 12; hour++)
                {
                    string hourString = hour.ToString();
                    this._Hour.Items.Add(hourString);
                }
                this.Controls.Add(this._Hour);

                // colon for between hour and minute
                Literal colon = new Literal();
                colon.Text = ":";
                this.Controls.Add(colon);

                // minute
                this._Minute.ID = this.ID + "_MinuteInputControl";
                this._Minute.CssClass = "DatePickerTimeMinute";

                int minute;

                for (minute = 0; minute < 60; minute++)
                {
                    string minuteString = minute.ToString();

                    if (minuteString.Length == 1)
                    { minuteString = "0" + minuteString; }

                    this._Minute.Items.Add(minuteString);
                }
                this.Controls.Add(this._Minute);

                // AMPM
                this._AMPM.ID = this.ID + "_AMPMInputControl";
                this._AMPM.CssClass = "DatePickerTimeAMPM";
                this._AMPM.Items.Add("AM");
                this._AMPM.Items.Add("PM");

                this.Controls.Add(this._AMPM);
            }

            // build the use none checkbox if we need to build it
            if (this.UseNone && !this.UseNow)
            {
                this._None.ID = this.ID + "NoneCheckbox";
                this._None.CssClass = "DatePickerCheckBox";
                this._None.Text = " " + _GlobalResources.None;

                this.Controls.Add(this._None);
            }

            // build the use now checkbox if we need to build it
            if (this.UseNow && !this.UseNone)
            {
                this._Now.ID = this.ID + "NowCheckbox";
                this._Now.CssClass = "DatePickerCheckBox";
                this._Now.Text = " " + _GlobalResources.Now;

                this.Controls.Add(this._Now);
            }
        }
        #endregion

        #region _BuildJSStartUpScript
        /// <summary>
        /// Builds the start up javascript to apply the jQuery datepicker function to the
        /// controls. This gets fired OnInit.
        /// </summary>
        private void _BuildJSStartUpScript()
        {
            // compile string of month names
            string monthNames = "['"
                                + _GlobalResources.January_abbr + "','"
                                + _GlobalResources.February_abbr + "','"
                                + _GlobalResources.March_abbr + "','"
                                + _GlobalResources.April_abbr + "','"
                                + _GlobalResources.May + "','"
                                + _GlobalResources.June_abbr + "','"
                                + _GlobalResources.July_abbr + "','"
                                + _GlobalResources.August_abbr + "','"
                                + _GlobalResources.September_abbr + "','"
                                + _GlobalResources.October_abbr + "','"
                                + _GlobalResources.November_abbr + "','"
                                + _GlobalResources.December_abbr + "']";

            // compile string of day names
            string dayNames = "['"
                              + _GlobalResources.Sunday_abbr + "','"
                              + _GlobalResources.Monday_abbr + "','"
                              + _GlobalResources.Tuesday_abbr + "','"
                              + _GlobalResources.Wednesday_abbr + "','"
                              + _GlobalResources.Thursday_abbr + "','"
                              + _GlobalResources.Friday_abbr + "','"
                              + _GlobalResources.Saturday_abbr + "']";

            // previous button text
            string prevText = "'" + _GlobalResources.Prev + "'";

            // next button text
            string nextText = "'" + _GlobalResources.Next + "'";

            // build the javascript using a StringBuilder
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" $(document).ready(function () {");
            sb.AppendLine("     $(\"#" + this._InputControl.ID + "\").datepicker({");
            sb.AppendLine("         dateFormat: \"" + DateFormatJS + "\",");

            // LOW - HIGH YEAR FOR DATE PICKER
            if (this.LowYear > 0 && this.HighYear > 0)
            { sb.AppendLine("         yearRange: \"" + this.LowYear + ":" + this.HighYear + "\","); }
            else if (this.LowYear == 0 && this.HighYear > 0)
            { sb.AppendLine("         yearRange: \"-0:" + this.HighYear + "\","); }
            else if (this.LowYear > 0 && this.HighYear == 0)
            { sb.AppendLine("         yearRange: \"" + this.LowYear + ":+0\","); }
            else
            { }

            sb.AppendLine("         changeMonth: true,");
            sb.AppendLine("         changeYear: true,");
            sb.AppendLine("         nextText: " + nextText + ",");
            sb.AppendLine("         prevText: " + prevText + ",");
            sb.AppendLine("         dayNamesMin: " + dayNames + ",");
            sb.AppendLine("         monthNamesShort: " + monthNames);
            sb.AppendLine("     });");

            sb.AppendLine("     $(\"#" + this._DatePickerImage.ID + "\").click(function () {");
            sb.AppendLine("         if ($(\"#" + this._InputControl.ID + "\").prop(\"disabled\") == false) {");
            sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").datepicker(\"show\");");
            sb.AppendLine("         }");
            sb.AppendLine("     });");

            // ACTIONS FOR "NONE" CONTROL
            if (this.UseNone)
            {
                sb.AppendLine("     if ($(\"#" + this._None.ID + "\").is(\":checked\")) {");
                sb.AppendLine("         $(\"#" + this._InputControl.ID + "\").prop(\"disabled\", true);");

                if (this.UseTime)
                {
                    sb.AppendLine("         $(\"#" + this._Hour.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("         $(\"#" + this._Minute.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("         $(\"#" + this._AMPM.ID + "\").attr(\"disabled\", true);");
                }

                sb.AppendLine("     }");

                sb.AppendLine("     $(\"#" + this._None.ID + "\").click(function () {");
                sb.AppendLine("         if ($(\"#" + this._None.ID + "\").is(\":checked\")) {");
                sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").prop(\"disabled\", true);");
                sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").val(\"\");");

                if (this.UseTime)
                {
                    sb.AppendLine("             $(\"#" + this._Hour.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("             $(\"#" + this._Minute.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("             $(\"#" + this._AMPM.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("             $(\"#" + this._Hour.ID + "\").val(\"12\");");
                    sb.AppendLine("             $(\"#" + this._Minute.ID + "\").val(\"00\");");
                    sb.AppendLine("             $(\"#" + this._AMPM.ID + "\").val(\"AM\");");
                }

                sb.AppendLine("         }");
                sb.AppendLine("         else {");
                sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").prop(\"disabled\", false);");

                if (this.UseTime)
                {
                    sb.AppendLine("             $(\"#" + this._Hour.ID + "\").prop(\"disabled\", false);");
                    sb.AppendLine("             $(\"#" + this._Minute.ID + "\").prop(\"disabled\", false);");
                    sb.AppendLine("             $(\"#" + this._AMPM.ID + "\").prop(\"disabled\", false);");
                }

                sb.AppendLine("         }");
                sb.AppendLine("     });");
            }

            // ACTIONS FOR "NOW" CONTROL
            if (this.UseNow)
            {
                sb.AppendLine("     if ($(\"#" + this._Now.ID + "\").is(\":checked\")) {");
                sb.AppendLine("         $(\"#" + this._InputControl.ID + "\").prop(\"disabled\", true);");

                if (this.UseTime)
                {
                    sb.AppendLine("         $(\"#" + this._Hour.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("         $(\"#" + this._Minute.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("         $(\"#" + this._AMPM.ID + "\").attr(\"disabled\", true);");
                }

                sb.AppendLine("     }");

                sb.AppendLine("     $(\"#" + this._Now.ID + "\").click(function () {");
                sb.AppendLine("         if ($(\"#" + this._Now.ID + "\").is(\":checked\")) {");
                sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").prop(\"disabled\", true);");
                sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").val(\"\");");

                if (this.UseTime)
                {
                    sb.AppendLine("             $(\"#" + this._Hour.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("             $(\"#" + this._Minute.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("             $(\"#" + this._AMPM.ID + "\").attr(\"disabled\", true);");
                    sb.AppendLine("             $(\"#" + this._Hour.ID + "\").val(\"12\");");
                    sb.AppendLine("             $(\"#" + this._Minute.ID + "\").val(\"00\");");
                    sb.AppendLine("             $(\"#" + this._AMPM.ID + "\").val(\"AM\");");
                }

                sb.AppendLine("         }");
                sb.AppendLine("         else {");
                sb.AppendLine("             $(\"#" + this._InputControl.ID + "\").prop(\"disabled\", false);");

                if (this.UseTime)
                {
                    sb.AppendLine("             $(\"#" + this._Hour.ID + "\").prop(\"disabled\", false);");
                    sb.AppendLine("             $(\"#" + this._Minute.ID + "\").prop(\"disabled\", false);");
                    sb.AppendLine("             $(\"#" + this._AMPM.ID + "\").prop(\"disabled\", false);");
                }

                sb.AppendLine("         }");
                sb.AppendLine("     });");
            }

            sb.AppendLine(" });");

            // attach the script as a startup script
            ScriptManager.RegisterStartupScript(this, typeof(Asentia.Controls.ClientScript), this.ID, sb.ToString(), true);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnInit
        protected override void OnInit(EventArgs e)
        {
            // execute base
            base.OnInit(e);

            // build js start up script
            this._BuildJSStartUpScript();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.DatePicker.js");
        }
        #endregion
        #endregion
    }
}
