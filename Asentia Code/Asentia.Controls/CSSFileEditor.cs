﻿using System;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using Asentia.Common;

namespace Asentia.Controls
{
    public class CSSFileEditor : WebControl, INamingContainer
    {
        #region Constructors
        public CSSFileEditor()
            : base(HtmlTextWriterTag.Div)
        {
            // define IDs of main controls to be rendered here to ensure scope
            this._CSSFileTree.ID = "CSSFileEditorFileListing";
            this._FileNameLabel.ID = "CSSFileEditorFileNameLabel";
            this._FileNameHiddenField.ID = "CSSFileEditorFileNameHiddenField";
            this._FileContentBox.ID = "CSSFileEditorFileContentBox";
            this._DefaultContentBox.ID = "CSSDefaultFileContentBox";
            this._FileNameLabel.Text = _GlobalResources.Editing;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Button to save the css file
        /// </summary>
        public Button SaveButton;

        /// <summary>
        /// Button to retreive the default css file
        /// </summary>
        public Button RevertToDefaultButton;

        /// <summary>
        /// Button to cancel
        /// </summary>
        public Button CancelButton;
        #endregion

        #region Private Properties
        /// <summary>
        /// TreeView to list available Hierarichical Directory and containing CSS files to edit.
        /// </summary>
        private TreeView _CSSFileTree = new TreeView();

        /// <summary>
        /// Label to display the file name of the file being edited.
        /// </summary>
        private Literal _FileNameLabel = new Literal();

        /// <summary>
        /// HiddenField to store the name of the file being edited.
        /// </summary>
        private HiddenField _FileNameHiddenField = new HiddenField();

        /// <summary>
        /// TextArea to display and edit the file content.
        /// </summary>
        private TextBox _FileContentBox = new TextBox();

        /// <summary>
        /// TextArea to display default content.
        /// </summary>
        private TextBox _DefaultContentBox = new TextBox();
        
        #endregion

        #region Public Methods
        #region SaveFile
        public string SaveFile()
        {
            try
            {
                string relativePathWithFileName = this._FileNameHiddenField.Value;
                string fileName = Path.GetFileName(this._FileNameHiddenField.Value);
                string relativeFolderPath = relativePathWithFileName.Replace(fileName, string.Empty);                
                string fullFilePath = SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + relativePathWithFileName;

                if (!Directory.Exists(MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + relativeFolderPath)))
                {
                    Directory.CreateDirectory(MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + relativeFolderPath));
                }

                // write the new file
                using (StreamWriter sw = new StreamWriter(MapPathSecure(fullFilePath + ".new")))
                { sw.Write(this._FileContentBox.Text); }

                // copy new file into the old
                File.Copy(MapPathSecure(fullFilePath + ".new"),
                          MapPathSecure(fullFilePath), true);

                // delete the placeholder file
                File.Delete(MapPathSecure(fullFilePath + ".new"));

                return fileName;
            }
            catch
            {
                // bubble the exception up
                throw;
            }
        }
        #endregion

        #region DeleteFile
        public string DeleteFile()
        {
            try
            {
                string relativePathWithFileName = this._FileNameHiddenField.Value;
                string fileName = Path.GetFileName(this._FileNameHiddenField.Value);
                string relativeFolderPath = relativePathWithFileName.Replace(fileName, string.Empty);
                string fullFilePath = SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + relativePathWithFileName;

                if (File.Exists(MapPathSecure(MapPathSecure(fullFilePath))))
                {
                    // delete the uploaded file
                    File.Delete(MapPathSecure(fullFilePath));
                }
                
                string defaultFilePath = SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + relativePathWithFileName;

                using (StreamReader sr = new StreamReader(MapPathSecure(defaultFilePath)))
                {
                    String fileContent = sr.ReadToEnd();
                    this._FileContentBox.Text = fileContent;
                }

                return fileName;
            }
            catch
            {
                // bubble the exception up
                throw;
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _SelectedNodeChanged
        /// <summary>
        /// _SelectedNodeChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _SelectedNodeChanged(object sender, EventArgs e)
        {
            if (!File.Exists(Page.Server.MapPath(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + this._CSSFileTree.SelectedNode.Value))
                && !File.Exists(Page.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + this._CSSFileTree.SelectedNode.Value)))
            {
                return;
            }
            else
            {
                string relativePath = this._CSSFileTree.SelectedNode.Value;
                string fullFilePath = string.Empty;
                string defaultFilePath = string.Empty;

                this._FileNameHiddenField.Value = relativePath;
                this._FileNameLabel.Text = _GlobalResources.Editing + ": " + relativePath;
                this._FileContentBox.Text = String.Empty;

                if (File.Exists(Page.Server.MapPath(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + relativePath)))
                {
                    fullFilePath = SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + relativePath;
                    this.RevertToDefaultButton.Enabled = true;
                    this.RevertToDefaultButton.CssClass = "Button NonActionButton";
                }
                else
                {
                    fullFilePath = SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + relativePath;
                    this.RevertToDefaultButton.Enabled = false;
                    this.RevertToDefaultButton.CssClass = "Button NonActionButton DisabledButton";
                }
                defaultFilePath = SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + relativePath;

                try
                {
                    using (StreamReader sr = new StreamReader(Page.Server.MapPath(fullFilePath)))
                    {
                        String fileContent = sr.ReadToEnd();
                        this._FileContentBox.Text = fileContent;
                    }

                    using (StreamReader dr = new StreamReader(Page.Server.MapPath(defaultFilePath)))
                    {
                        String defaultContent = dr.ReadToEnd();
                        this._DefaultContentBox.Text = defaultContent;
                    }
                }
                catch
                {
                    // bubble the exception up
                    throw;
                }
            }
        }
        #endregion

        #region _BuildCSSTree
        /// <summary>
        /// _BuildCSSTree
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="rootPath"></param>
        private void _BuildCSSTree(TreeView treeView, string rootPath)
        {
            // clear nodes from tree view
            treeView.Nodes.Clear();

            // get directory info of root path
            var rootDirectoryInfo = new DirectoryInfo(rootPath);

            // build image tree nodes
            treeView.Nodes.Add(this._BuildCSSTreeNode(rootDirectoryInfo, rootPath));
        }
        #endregion

        #region _BuildCSSTreeNode
        /// <summary>
        /// _BuildCSSTreeNode
        /// </summary>
        /// <param name="directoryInfo"></param>
        /// <param name="rootPath"></param>
        /// <returns></returns>
        private TreeNode _BuildCSSTreeNode(DirectoryInfo directoryInfo, string rootPath)
        {
            // get the directory node
            TreeNode directoryNode = new TreeNode(directoryInfo.Name, string.Empty);

            // set the image for the directory node
            directoryNode.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FOLDER, ImageFiles.EXT_PNG);

            // set the action on the directory node
            directoryNode.SelectAction = TreeNodeSelectAction.SelectExpand;

            // loop through sub directories and build nodes for those
            foreach (DirectoryInfo subDirectory in directoryInfo.GetDirectories())
            {
                directoryNode.ChildNodes.Add(_BuildCSSTreeNode(subDirectory, rootPath));
            }

            // loop through all the files and add them to their respective directory nodes
            // only if they are CSS files
            foreach (FileInfo subFile in directoryInfo.GetFiles())
            {
                if (this._IsCSSFile(subFile.Extension))
                {
                    TreeNode fileNode = new TreeNode(subFile.Name);                    

                    // get the path to the file that is relative to the root CSS folder
                    string filePathRelativeToRoot = subFile.FullName.Substring(rootPath.Length, subFile.FullName.Length - rootPath.Length);
                    //filePathRelativeToRoot = subFile.FullName.Replace(rootPath.Substring(0, rootPath.IndexOf("/") - 1), String.Empty);
                    filePathRelativeToRoot = filePathRelativeToRoot.Replace("\\", "/");
                    filePathRelativeToRoot = filePathRelativeToRoot.Replace(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS, "");
                    filePathRelativeToRoot = filePathRelativeToRoot.Replace(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS, "");

                    // build the url to the file based on whether or not it exist within the site's images folder
                    string fileURL = String.Empty;

                    if (File.Exists(Page.Server.MapPath(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + filePathRelativeToRoot)))
                    {
                        fileNode.Text += " (" + _GlobalResources.CUSTOMIZED + ")";
                        fileURL = SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + filePathRelativeToRoot; 
                    }
                    else
                    { fileURL = SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + filePathRelativeToRoot; }

                    // set the select action, url, and image for the file node
                    fileNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    fileNode.Value = filePathRelativeToRoot;
                    fileNode.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CSS, ImageFiles.EXT_PNG);

                    // attach file node
                    directoryNode.ChildNodes.Add(fileNode);
                }
            }

            // return directory node
            return directoryNode;
        }
        #endregion

        #region _IsCSSFile
        /// <summary>
        /// _IsCSSFile
        /// </summary>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        private bool _IsCSSFile(string fileExtension)
        {
            switch (fileExtension.ToLower())
            {
                case ".css":
                    return true;
                default:
                    return false;
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region CreateChildControls
        /// <summary>
        /// Overrides CreateChildControls to create the proper layout of the control.        
        /// This is where we would add any controls that should appear on the Fluid Controls layout.
        /// CreateChildControls method is typically used for rendering in custom server controls.
        /// </summary>        
        /// <returns></returns>
        protected override void CreateChildControls()
        {
            // current theme
            // file listing files inner panel
            Panel fileListingFilesInnerPanel = new Panel();
            fileListingFilesInnerPanel.ID = "CSSFileEditorFileListingFilesInnerPanel";
            fileListingFilesInnerPanel.CssClass = "xd-3 xm-12";

            fileListingFilesInnerPanel.Controls.Add(this._CSSFileTree);

            // attach controls
            this.Controls.Add(fileListingFilesInnerPanel);

            // FILE CONTENT PANEL
            Panel fileContentWrapperPanel = new Panel();
            fileContentWrapperPanel.ID = "CSSFileEditorFileContentWrapperPanel";
            fileContentWrapperPanel.CssClass = "xd-9 xm-12";

            Panel fileContentPanel = new Panel();
            fileContentPanel.ID = "CSSFileEditorFileContentPanel";
            fileContentPanel.CssClass = "FormContentContainer";
            fileContentPanel.Style.Add("display", "inline-block");

            Panel fileNamePanel = new Panel();
            fileNamePanel.ID = "CSSFileEditorFileNamePanel";
            fileNamePanel.Controls.Add(this._FileNameLabel);            

            // file content box
            this._FileContentBox.TextMode = TextBoxMode.MultiLine;
            this._FileContentBox.CssClass = String.Empty;

            // ACE editor panel
            Panel aceEditorPanel = new Panel();
            aceEditorPanel.ID = "ACEEditorPanel";
            aceEditorPanel.CssClass = "ACEEditorPanel";

            // ACTIONS PANEL
            Panel actionsPanel = new Panel();
            actionsPanel.CssClass = "CSSEditorActionsPanel";
            actionsPanel.Controls.Add(this.SaveButton);
            actionsPanel.Controls.Add(this.RevertToDefaultButton);
            actionsPanel.Controls.Add(this.CancelButton);

            // attach controls
            fileContentPanel.Controls.Add(fileNamePanel);
            fileContentPanel.Controls.Add(aceEditorPanel);
            fileContentPanel.Controls.Add(this._FileContentBox);
            fileContentPanel.Controls.Add(actionsPanel);

            // default value panels
            Panel defaultContentPanel = new Panel();
            defaultContentPanel.ID = "CSSFileDefaultFileContentPanel";
            defaultContentPanel.CssClass = "FormContentContainer";
            defaultContentPanel.Style.Add("display", "inline-block");

            Panel defaultNamePanel = new Panel();
            defaultNamePanel.ID = "CSSFileDefaultFileNamePanel";

            Literal defaultNameLabel = new Literal();
            defaultNameLabel.Text = _GlobalResources.ThemeDefaultCSSContent;
            defaultNamePanel.Controls.Add(defaultNameLabel);

            // default content box
            this._DefaultContentBox.TextMode = TextBoxMode.MultiLine;
            this._DefaultContentBox.CssClass = String.Empty;

            // ACE default panel
            Panel aceDefaultPanel = new Panel();
            aceDefaultPanel.ID = "ACEDefaultPanel";
            aceDefaultPanel.CssClass = "ACEEditorPanel";

            // attach controls
            defaultContentPanel.Controls.Add(defaultNamePanel);
            defaultContentPanel.Controls.Add(aceDefaultPanel);
            defaultContentPanel.Controls.Add(this._DefaultContentBox);

            fileContentWrapperPanel.Controls.Add(fileContentPanel);
            fileContentWrapperPanel.Controls.Add(defaultContentPanel);
            this.Controls.Add(fileContentWrapperPanel);

            // FILE NAME HIDDEN FIELD
            this.Controls.Add(this._FileNameHiddenField);

            // continue executing base method
            base.CreateChildControls();
        }

        #endregion

        #region OnInit
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            // set control state and clear controls
            Page.RegisterRequiresControlState(this);
            this.Controls.Clear();

            // build the image tree
            this._BuildCSSTree(this._CSSFileTree, Page.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS));

            // set styling for the image tree selected nodes
            this._CSSFileTree.SkipLinkText = String.Empty;
            this._CSSFileTree.SelectedNodeStyle.BackColor = Color.LightBlue;
            this._CSSFileTree.SelectedNodeStyle.ForeColor = Color.White;

            // set selected node changed event for tree view
            this._CSSFileTree.SelectedNodeChanged += new EventHandler(this._SelectedNodeChanged);

            base.CreateChildControls();

            if (!this.Page.IsPostBack)
            {
                this._CSSFileTree.CollapseAll();

                if (this._CSSFileTree.Nodes.Count > 0)
                {
                    this._CSSFileTree.Nodes[0].Expand();
                }
            }
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // build start up call to initialize the ACE editor    
            StringBuilder startUpCallsScript = new StringBuilder();
            startUpCallsScript.AppendLine("Sys.Application.add_load(");
            startUpCallsScript.AppendLine("function() { ");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // apply ACE language tools");
            startUpCallsScript.AppendLine("    ace.require(\"ace/ext/language_tools\");");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // instansiate ACE editor");
            startUpCallsScript.AppendLine("    var ACEEditor = ace.edit(\"ACEEditorPanel\");");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // set ACE editor options");
            startUpCallsScript.AppendLine("    ACEEditor.$blockScrolling = Infinity;");
            startUpCallsScript.AppendLine("    ACEEditor.setTheme(\"ace/theme/chrome\");");
            startUpCallsScript.AppendLine("    ACEEditor.session.setMode(\"ace/mode/css\");");
            startUpCallsScript.AppendLine("    ACEEditor.setOptions({");
            startUpCallsScript.AppendLine("     showPrintMargin: false,");
            startUpCallsScript.AppendLine("     enableBasicAutocompletion: true,");
            startUpCallsScript.AppendLine("     enableSnippets: true,");
            startUpCallsScript.AppendLine("     enableLiveAutocompletion: true");
            startUpCallsScript.AppendLine("    });");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // get and hide the textarea input");
            startUpCallsScript.AppendLine("    var cssTextarea = $(\"#CSSFileEditorFileContentBox\").hide();");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // set the ACE editor text to the textarea's value");
            startUpCallsScript.AppendLine("    ACEEditor.getSession().setValue(cssTextarea.val());");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // set onchange for ACE editor to update the textarea when text is changed");
            startUpCallsScript.AppendLine("    ACEEditor.getSession().on(\"change\", function(){");
            startUpCallsScript.AppendLine("     cssTextarea.val(ACEEditor.getSession().getValue());");
            startUpCallsScript.AppendLine("    });");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // instansiate ACE default file");
            startUpCallsScript.AppendLine("    var ACEDefault = ace.edit(\"ACEDefaultPanel\");");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // set ACE editor options");
            startUpCallsScript.AppendLine("    ACEDefault.$blockScrolling = Infinity;");
            startUpCallsScript.AppendLine("    ACEDefault.setTheme(\"ace/theme/chrome\");");
            startUpCallsScript.AppendLine("    ACEDefault.session.setMode(\"ace/mode/css\");");
            startUpCallsScript.AppendLine("    ACEDefault.setOptions({");
            startUpCallsScript.AppendLine("     showPrintMargin: false,");
            startUpCallsScript.AppendLine("     readOnly: true,");
            startUpCallsScript.AppendLine("    });");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // get and hide the textarea input");
            startUpCallsScript.AppendLine("    var cssDefaultTextarea = $(\"#CSSDefaultFileContentBox\").hide();");
            startUpCallsScript.AppendLine("");
            startUpCallsScript.AppendLine("    // set the ACE editor text to the textarea's value");
            startUpCallsScript.AppendLine("    ACEDefault.getSession().setValue(cssDefaultTextarea.val());");
            startUpCallsScript.AppendLine("});");            

            ScriptManager.RegisterStartupScript(this.Page, typeof(UploaderAsync), "ACEEditorInitialize", startUpCallsScript.ToString(), true);

            base.OnPreRender(e);
        }
        #endregion
        #endregion
    }
}
