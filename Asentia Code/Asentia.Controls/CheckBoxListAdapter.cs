﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;

namespace Asentia.Controls
{
    public class CheckBoxListAdapter : WebControlAdapter
    {
        protected override void RenderBeginTag(HtmlTextWriter writer)
        {
            // id
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.Control.ClientID);

            // css class
            if (!String.IsNullOrWhiteSpace(this.Control.CssClass))
            { writer.AddAttribute(HtmlTextWriterAttribute.Class, this.Control.CssClass); }

            // onclick
            if (!String.IsNullOrWhiteSpace(this.Control.Attributes["onclick"]))
            { writer.AddAttribute(HtmlTextWriterAttribute.Onclick, this.Control.Attributes["onclick"]); }

            // render opening tag
            writer.RenderBeginTag(HtmlTextWriterTag.Div); 
        }

        protected override void RenderEndTag(HtmlTextWriter writer)
        { writer.RenderEndTag(); }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            CheckBoxList adaptedControl = (CheckBoxList)Control;

            int itemCounter = 0;

            writer.Indent++;

            foreach (ListItem item in adaptedControl.Items)
            {
                var inputId = adaptedControl.ClientID + "_" + itemCounter++;

                // item div
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "ToggleInput");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                writer.Indent++;

                // input
                writer.AddAttribute(HtmlTextWriterAttribute.Id, inputId);
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "checkbox");
                writer.AddAttribute(HtmlTextWriterAttribute.Name, adaptedControl.UniqueID);
                writer.AddAttribute(HtmlTextWriterAttribute.Value, item.Value);                

                if (item.Selected)
                { writer.AddAttribute(HtmlTextWriterAttribute.Checked, "checked"); }

                writer.RenderBeginTag(HtmlTextWriterTag.Input);
                writer.RenderEndTag();

                // label
                if (!String.IsNullOrWhiteSpace(item.Text))
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.For, inputId);
                    writer.RenderBeginTag(HtmlTextWriterTag.Label);
                    writer.Write(item.Text);
                    writer.RenderEndTag();
                }

                // div
                writer.Indent--;
                writer.RenderEndTag();

                if (this.Page != null)
                { Page.ClientScript.RegisterForEventValidation(adaptedControl.UniqueID, item.Value); }
            }

            writer.Indent--;

            if (this.Page != null)
            { Page.ClientScript.RegisterForEventValidation(adaptedControl.UniqueID); }
        }
    }
}
