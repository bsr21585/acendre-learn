﻿$(document).ready(function () {
    // BEGIN DROP-DOWN BREADCRUMB FUNCTIONS
    $("#PageBreadcrumbContainer").each(function () {
        $("#" + this.id).click(function (event) {
            event.stopPropagation();
        });
    });

    $("#BreadcrumbDownArrowLI").click(function () {
        $("#BreadcrumbDropDownItems").toggle();

        if ($("#BreadcrumbDropDownItems").is(":visible")) {
            $("#BreadcrumbDownArrowLI img").css("transform", "rotate(90deg)");
        }
        else {
            $("#BreadcrumbDownArrowLI img").css("transform", "rotate(0deg)");
        }
    });

    $("#FullDropDownBreadcrumbContainer").each(function () {
        $("#" + this.id).click(function (event) {
            event.stopPropagation();
        });
    });

    $("#FullDropDownBreadcrumbDownArrowLI").click(function () {
        $("#FullDropDownBreadcrumbDropDownItems").toggle();

        if ($("#FullDropDownBreadcrumbDropDownItems").is(":visible")) {
            $("#FullDropDownBreadcrumbDownArrowLI img").css("transform", "rotate(90deg)");
        }
        else {
            $("#FullDropDownBreadcrumbDownArrowLI img").css("transform", "rotate(0deg)");
        }
    });

    $(document).click(function () {
        if ($("#BreadcrumbDropDownItems").is(":visible") || $("#FullDropDownBreadcrumbDropDownItems").is(":visible")) {
            $("#BreadcrumbDropDownItems").hide();
            $("#BreadcrumbDownArrowLI img").css("transform", "rotate(0deg)");
            $("#FullDropDownBreadcrumbDropDownItems").hide();
            $("#FullDropDownBreadcrumbDownArrowLI img").css("transform", "rotate(0deg)");
        }
    });
    // END DROP-DOWN BREADCRUMB FUNCTIONS
});