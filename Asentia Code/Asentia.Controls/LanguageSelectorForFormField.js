﻿// Displays the selected language field for a standard form field.
function ToggleLanguageSpecificStandardFormField(formFieldId, selectedLanguage) {
    var fieldInputContainerId = formFieldId.replace("_Field", "_InputContainer");

    $("#" + fieldInputContainerId).children().each(function () {
        if (this.id.indexOf(formFieldId) != -1 && this.id.indexOf("LanguageSelector") == -1) {
            $(this).hide();
        }
    });

    if (selectedLanguage == "_DEFAULTLANGUAGE_") {
        $("#" + formFieldId).show();
    }
    else {
        $("#" + formFieldId + "_" + selectedLanguage).show();
    }
}

// Displays the selected language field for a form field that contains the CKEditor.
function ToggleLanguageSpecificCKEditorFormField(formFieldId, selectedLanguage) {
    var fieldInputContainerId = formFieldId.replace("_Field", "_InputContainer");

    $("#" + fieldInputContainerId).children().each(function () {
        if (this.id.indexOf(formFieldId.replace("_Field", "_CKEDITORContainer")) != -1 && this.id.indexOf("LanguageSelector") == -1) {
            $(this).hide();
        }
    });

    if (selectedLanguage == "_DEFAULTLANGUAGE_") {
        $("#" + formFieldId.replace("_Field", "_CKEDITORContainer")).show();
    }
    else {
        $("#" + formFieldId.replace("_Field", "_CKEDITORContainer") + "_" + selectedLanguage).show();
    }
}