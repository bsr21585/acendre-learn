﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;

namespace Asentia.Controls
{
    public class RadioButtonAdapter : WebControlAdapter
    {
        protected override void RenderBeginTag(HtmlTextWriter writer)
        {
            // css class
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "ToggleInput");

            // render opening tag
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
        }

        protected override void RenderEndTag(HtmlTextWriter writer)
        { writer.RenderEndTag(); }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            RadioButton adaptedControl = (RadioButton)Control;

            writer.Indent++;

            // input
            writer.AddAttribute(HtmlTextWriterAttribute.Id, adaptedControl.ClientID);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "radio");
            
			//Following 2 lines are added by Chetu team as we are using one custom attribute data-id for gettiing the selected instructor led training
			//instances while enrolling int a  course or Instructor led training on from user catalog page by Java script
            if (!string.IsNullOrWhiteSpace(adaptedControl.Attributes["data-id"]))
            { writer.AddAttribute("data-id", adaptedControl.Attributes["data-id"]); }

            // add any input attributes
            foreach (string key in adaptedControl.InputAttributes.Keys)
            {
                writer.AddAttribute(key, adaptedControl.InputAttributes[key]);
            }

            string uniqueGroupName = adaptedControl.GetType().GetProperty("UniqueGroupName", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(adaptedControl, null).ToString();
            writer.AddAttribute(HtmlTextWriterAttribute.Name, uniqueGroupName);

            if (!String.IsNullOrWhiteSpace(adaptedControl.CssClass))
            { writer.AddAttribute(HtmlTextWriterAttribute.Class, adaptedControl.CssClass); }

            writer.AddAttribute(HtmlTextWriterAttribute.Value, adaptedControl.ClientID);

            if (adaptedControl.Checked)
            { writer.AddAttribute(HtmlTextWriterAttribute.Checked, "checked"); }

            if (!adaptedControl.Enabled)
            { writer.AddAttribute("disabled", "disabled"); }

            if (!String.IsNullOrWhiteSpace(adaptedControl.Attributes["onclick"]))
            { writer.AddAttribute(HtmlTextWriterAttribute.Onclick, adaptedControl.Attributes["onclick"]); }

            writer.RenderBeginTag(HtmlTextWriterTag.Input);
            writer.RenderEndTag();

            // label
            if (!String.IsNullOrWhiteSpace(adaptedControl.Text))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.For, adaptedControl.ClientID);
                writer.RenderBeginTag(HtmlTextWriterTag.Label);
                writer.Write(adaptedControl.Text);
                writer.RenderEndTag();
            }

            writer.Indent--;

            if (this.Page != null)
            { Page.ClientScript.RegisterForEventValidation(uniqueGroupName, adaptedControl.ClientID); }
        }
    }
}
