Tabs = function(
	parent, 
	node
	){
		
	this.TypeName = "Tabs";
	
	this.Items = [];
	this.Parent = parent;
	
	this.Panel = document.createElement("div");
		this.Panel.className = "TabsContainer PageCategory_LearningAssets";
	
	this.UL = document.createElement("ul")
		this.UL.className = "TabbedList";
		
	this.Panel.append(this.UL);
	node.append(this.Panel);
}

Tabs.prototype.SetTab = function(s){
	try{
		this.Parent.SetTab(s);
	} catch(e){}
	
	
	
	for (var i = 0; i < this.Items.length; i++){
		if (this.Items[i].Value == s){
			this.Items[i].On = true;
			
			//preserve other stylings, if present (i.e. Warning/Error stylings)
			this.Parent.AddClassToElement(this.Items[i].LI, "TabbedListLIOn");
			//this.Items[i].LI.className = "TabbedListLI TabbedListLIOn";
		}else{
			this.Items[i].On = false;
			
			this.Parent.RemoveClassFromElement(this.Items[i].LI, "TabbedListLIOn");
			//this.Items[i].LI.className = "TabbedListLI";
		}
	}
}