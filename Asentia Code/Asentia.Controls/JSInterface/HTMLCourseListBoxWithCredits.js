HTMLCourseListBoxWithCredits = function(
	deleteImagePath, 
	data,
	parent
	){
	
	this.DeleteImagePath = deleteImagePath;
	
	if (parent)
		this.Parent = parent;
	
	    this.Container = document.createElement("div");
		this.Container.className = "ItemListingContainer";
		this.Container.id = "AttachedCoursesList_Container";
		
		this.Container.removeItem = function(o)
		{
			//use HTMLCourseItemWithCredit ("o") Identifier to single out the target attached course
			var targetCourse = "AttachedCourse_" + o.Identifier;
			
			//get the Attached Courses container and the target element to be removed
			var attachedCoursesListContainer = document.getElementById(this.id);
			var targetCourseElement = document.getElementById(targetCourse);
			
			//remove the item from the list
			attachedCoursesListContainer.removeChild(targetCourseElement);
		};
		
	this.Items = [];
	
	if (typeof data == "object"){ 
	
		this.Data = data;
		
		for (var i = 0; i < this.Data.length; i++){
			
			this.AddItem(
				this.Data[i].id, 
				this.Data[i].Name, 
				this.Data[i].Credits
			);
		}
		
	}
	
	/*
	
	this.SelectPanel = document.createElement("div");
		
	this.SelectAllLink = document.createElement("a");
		this.SelectAllLink.Parent = this;
		this.SelectAllLink.append(document.createTextNode("All"));
		this.SelectAllLink.onclick = function(){
			this.Parent.SelectAll();
		}
	this.SelectNoneLink = document.createElement("a");
		this.SelectNoneLink.Parent = this;
		this.SelectNoneLink.append(document.createTextNode("None"));
		this.SelectNoneLink.onclick = function(){
				this.Parent.SelectNone();
			}
	this.SelectPanel.append(document.createTextNode("Select: "));
	this.SelectPanel.append(this.SelectAllLink);
	this.SelectPanel.append(document.createTextNode("|"));
	this.SelectPanel.append(this.SelectNoneLink);
	
	this.Panel.append(this.SelectPanel);
	*/
}

HTMLCourseListBoxWithCredits.prototype.AddItem = function(
	id, 
	label, 
	credits
	){
	
	this.Items[this.Items.length] = new HTMLCourseItemWithCredit(
		this, 
		id, 
		label, 
		credits,
		this.DeleteImagePath
		);
		
	this.Container.append(this.Items[this.Items.length - 1].Container);
	
}

HTMLCourseListBoxWithCredits.prototype.RemoveItem = function(o){
	
	this.Container.removeItem(o);
	//o is HTMLCourseItemWithCredit {Parent: HTMLCourseListBoxWithCredits, Identifier: 123, Image: img, className: "SmallIcon", Container: d...
	
	//also remove the item from the Items list
	for (var i = 0; i < this.Items.length; i++)
	{
		var thisId = this.Items[i].Identifier;
		
		if (thisId == o.Identifier)
		{
			this.Items.splice(i, 1);
			
			break;
		}
	}
	
	//Remove the item from the ApplicableCourses object
	try
	{
		//perform all tasks required to delete Course from Attached Courses List
		this.Parent.Parent.DoAttachedCoursesListCourseDelete(o.Identifier);

	}
	catch(e)
	{
		//error removing course
	}
}

HTMLCourseListBoxWithCredits.prototype.ShowRemoveItemAlert = function(o){
	
	if (!this.Parent.Parent.ValidateCourseDeleteByIdOnTabs(o.Identifier))
	{
		//add Yes/No Confirm, add fxn/object to perform delete, add fxnality to delete any requirement where deleted course is the ONLY course in the list
		this.Parent.Parent.ShowAlertModal(
				this.Parent.Parent.GetDictionaryTerm("Delete Course"),
				this.Parent.Parent.GetDictionaryTerm("deleting this course will affect one or more requirements"),//"Deleting this course will affect one or more Requirements. Delete anyway?"
				this.Parent.Parent.GetDictionaryTerm("Yes"),
				this.Parent.Parent.GetDictionaryTerm("No"),
				{targetObject: this, functionName: "RemoveItem", functionParam:o}
			);
	}
	else
	{
		this.Parent.Parent.ShowAlertModal(
				this.Parent.Parent.GetDictionaryTerm("Delete Course"),
				this.Parent.Parent.GetDictionaryTerm("Are you sure you want to delete this course"),//"Are you sure you want to delete this course?"
				this.Parent.Parent.GetDictionaryTerm("Yes"),
				this.Parent.Parent.GetDictionaryTerm("No"),
				{targetObject: this, functionName: "RemoveItem", functionParam:o}
			);
	}
}

HTMLCourseListBoxWithCredits.prototype.AddRemoveItemAtIndex = function(index){
	
	this.Container.removeItem(this.Items[index]);
	this.Items.shift(index, 1);
	
}

HTMLCourseListBoxWithCredits.prototype.GetData = function(){
	
	var s = "";
	var first = true;
	
	s = s + "[";
	
	for (var i = 0; i < this.Items.length; i++){
		if (this.Items[i].Input.checked){
			if (!first){
				s = s + ",";
			}
			s = s + "{\"id\":" + this.Items[i].Input.value + "}";
			first = false;
		}
	}

	s = s + "]";
	
	return s;
}

HTMLCourseItemWithCredit = function(
	parent, 
	id, 
	label, 
	credits,
	deleteImagePath
	){	

	this.Parent = parent;
	this.Identifier = id;
	
	this.Image = document.createElement("img");
		this.Image.Parent 		= this;
		this.className = "SmallIcon";
		this.Image.setAttribute("src", deleteImagePath);
		this.Image.style.cursor = "pointer";
		this.Image.onclick = function(){
			//this.Parent.Parent.RemoveItem(this.Parent);
			this.Parent.Parent.ShowRemoveItemAlert(this.Parent);
		}
		
	this.Container = document.createElement("div");
		this.Container.Parent		= this;
		this.Container.id = "AttachedCourse_" + this.Identifier;
		
	this.Input = document.createElement("input");
		this.Input.Parent 		= this;
		this.Input.className 	= "InputXShort";
		this.Input.type 		= "text";
		this.Input.value 		= credits;
	
	this.LastAcceptableInputValue = credits;
	
	//for manually changing the amount of credits for attached course
	this.Input.addEventListener("keyup", function(event){
		//event.preventDefault();
		if (event.keyCode == 13)
		{
			//only submit when the value has changed
			if (this.Parent.LastAcceptableInputValue != this.value)
			{
				this.Parent.ClearError();
				this.Parent.Parent.Parent.Parent.UpdateCreditsForApplicableCourseWithErrorCheck(this.Parent.Identifier, this.Parent.Input.value, this.Parent);
			}
		}
	});
	
	this.Input.addEventListener("blur", function(event){
		event.preventDefault();
		
		//only submit when the value has changed
		if (this.Parent.LastAcceptableInputValue != this.value)
		{
			this.Parent.ClearError();
			this.Parent.Parent.Parent.Parent.UpdateCreditsForApplicableCourseWithErrorCheck(this.Parent.Identifier, this.Parent.Input.value, this.Parent);
		}

	});
	
	this.Container.append(this.Image);
	this.Container.append(document.createTextNode(label));
	this.Container.append(this.Input);
		
		
}

HTMLCourseItemWithCredit.prototype.UpdateLastAcceptableInputValue = function(lastValue)
{
	this.LastAcceptableInputValue = lastValue;
}

HTMLCourseItemWithCredit.prototype.RevertToLastAcceptableInputValue = function()
{
	this.Input.value = this.LastAcceptableInputValue;
}

HTMLCourseItemWithCredit.prototype.ConfirmChangeCreditValueAndUpdateRequirements = function(creditChangeInfoObject)
{
	//creditChangeInfoObject will have parameters: creditChangeInfoObject.updatedNumberOfCredits, creditChangeInfoObject.courseId
	this.Input.value = creditChangeInfoObject.updatedNumberOfCredits;

	this.Parent.Parent.Interface.UpdateCreditsForApplicableCourse(creditChangeInfoObject.courseId, creditChangeInfoObject.updatedNumberOfCredits, this);
	this.Parent.Parent.Interface.ReduceMinCreditsRequiredOnAllTabSegmentsOnCourseDelete();
	this.Parent.Parent.Interface.UpdateAllRequirementInterfacesOnAllTabSegments();
}

HTMLCourseItemWithCredit.prototype.ClearError = function(){

	// remove the existing error
	try{
		this.Container.removeChild(this.FormFieldErrorContainer);
	} catch(e){}

}
	
HTMLCourseItemWithCredit.prototype.ShowError = function(s){
	
	this.ClearError();
	
	this.FormFieldErrorContainer = document.createElement("div");
		this.FormFieldErrorContainer.className = "FormFieldErrorContainer";
	var verticalSpacerTop = document.createElement("div");
	var p = document.createElement("p");
		p.append(document.createTextNode(s));
	var verticalSpacerBottom = document.createElement("div");
	
	this.FormFieldErrorContainer.append(verticalSpacerTop);
	this.FormFieldErrorContainer.append(p);
	this.FormFieldErrorContainer.append(verticalSpacerBottom);
	this.Container.append(this.FormFieldErrorContainer);
}
