LanguageSelector = function(
		parent, 		// object
		node,
		flagImagePath, 	// folder location of the flag images
		languages, 		// array of languages
		selected		// the pre-selected langauge code, 
	){
	
	this.TypeName = "LanguageSelector";
	
	this.Parent = parent;
	this.Onclick = onclick;
	
	this.FlagImagePath 		= flagImagePath;
	this.SelectedLanguage 	= selected;
	this.Languages 			= languages;
	
	this.Open 				= false;
	
	this.FormFieldLanguageSelector = document.createElement("div");
		this.FormFieldLanguageSelector.Handler = this;
		this.FormFieldLanguageSelector.onclick = function(){
			this.Handler.ToggleOpen();
		}
	this.FormFieldLanguageSelectorDropDown  = document.createElement("dl");
	this.DT = document.createElement("dt");
	this.DD = document.createElement("dd");
	this.UL = document.createElement("ul");
	
	this.FormFieldLanguageSelector.className 			= "FormFieldLanguageSelector FormFieldLanguageSelectorInlineField";
	this.FormFieldLanguageSelectorDropDown.className 	= "FormFieldLanguageSelectorDropDown FormFieldLanguageSelectorDropDownDL";
	
	this.SelectedFlagImage = document.createElement("img");
		this.SetFlagToSelected();
	
	var Arrow = document.createElement("div");
		Arrow.className = "FormFieldLanguageSelectorDropDownArrow";
		
	this.FormFieldLanguageSelector.append(this.FormFieldLanguageSelectorDropDown);
	this.FormFieldLanguageSelectorDropDown.append(this.DT);
	this.DT.append(this.SelectedFlagImage);
	this.FormFieldLanguageSelectorDropDown.append(this.DD);
	
	for (var i = 0; i < this.Languages.length; i++){
		this.UL.append(this.NewOption(this.Languages[i]));
	}
	
	this.DD.append(this.UL);
	
	this.FormFieldLanguageSelector.append(Arrow);
	
	node.append(this.FormFieldLanguageSelector);

}
LanguageSelector.prototype.NewOption = function(code){
	
	var li = document.createElement("li");
		li.Handler = this;
		li.onclick = function(){
			this.Handler.SetLanguage(code);
		}
		
	var img = document.createElement("img");
		img.setAttribute("src", this.FlagImagePath + code + ".png");
		img.Handler = this;
		
	li.append(img);
	
	return li;
	
}
LanguageSelector.prototype.OpenOptions = function(){
	this.UL.style.display = "block";
	this.Open = true;
}
LanguageSelector.prototype.SetFlagToSelected = function(){
	this.SelectedFlagImage .setAttribute("src", this.FlagImagePath + "/" + this.SelectedLanguage + ".png");
}
LanguageSelector.prototype.CloseOptions = function(){
	this.UL.style.display = "none";
	this.Open = false;
}
LanguageSelector.prototype.ToggleOpen = function(){
	if (this.Open == true){
		this.CloseOptions();
	}else{
		this.OpenOptions();
	}
}
LanguageSelector.prototype.SetLanguage = function(code){
	this.SelectedLanguage = code;
	this.SetFlagToSelected();
	try{
		this.Parent.SetLanguage(code);
	}catch(e){}
}