HTMLSelectBox = function(
	id, 
	name, 
	className, 
	disabled, 
	data,
	isMultiple, 
	rows
	){
	
	this.ID 		= id;
	this.Name 		= name;
	this.className 	= className;
	this.IsMultiple = isMultiple;
	this.IsDisabled = disabled;
	this.Rows 		= rows;
	
	this.SelectBox 	= document.createElement("select");
	
	this.SelectBox.id 			= this.ID;
	this.SelectBox.name 		= this.Name;
	this.SelectBox.className 	= this.className;
	this.SelectBox.disabled 	= this.IsDisabled;
	(this.IsMultiple == true) ? this.SelectBox.setAttribute("multiple", "true") : this.SelectBox.removeAttribute("multiple");
	try{this.SelectBox.setAttribute("size", this.Rows);}catch(e){}
	
	
	this.AddItems(data);
		
}

HTMLSelectBox.prototype.Enable = function(
	bln
	){
	
	this.SelectBox.disabled = !bln;
	
}

HTMLSelectBox.prototype.AddItem = function(
	value, 
	caption,
	selected
	){
	
	var o = new Option;
		o.value = value;
		o.text = caption;
		o.selected = selected;
		
	this.SelectBox.options[this.SelectBox.options.length] = o;
	
}

/*

[
	{Value:XXX,Caption:YYY},
	{Value:AAA,Caption:BBB},
]

*/
HTMLSelectBox.prototype.AddItems = function(
	data
	){
	
	try{
		var Data = JSON.parse(data);
	} catch(e){
		return false;
	}
	
	for (var i = 0; i < Data.length; i++){
		this.AddItem(Data[i].Value, Data[i].Caption, Data[i].Selected);
	}
	
}

HTMLSelectBox.prototype.RemoveItem = function(
	value
	){
	
}

HTMLSelectBox.prototype.RemoveAllItems = function(){
	
	this.SelectBox.options.length = 0;
	
}

HTMLSelectBox.prototype.RemoveSelected = function(
	value
	){
	
}

HTMLSelectBox.prototype.GetContentsAsJSONString = function(){
	
	var v = "";
	
	for (var i = 0; i < this.SelectBox.options.length; i++){
		if (v.length != 0){	v = v + ","; }
		v = v + "{\"Value\":\"" + this.SelectBox.options[i].value + "\",\"Caption\":\"" + this.SelectBox.options[i].text + "\",\"Selected\":\"" + this.SelectBox.options[i].selected + "\"}";
	}
	
	return "[" + v + "]";
	
}

HTMLSelectBox.prototype.GetSelectedContentsAsJSONString = function(){
	
	var v = "";
	
	for (var i = 0; i < this.SelectBox.options.length; i++){
		if (this.SelectBox.options[i].selected){
			if (v.length != 0){	v = v + ","; }
			v = v + "{\"Value\":\"" + this.SelectBox.options[i].value + "\",\"Caption\":\"" + this.SelectBox.options[i].text + "\",\"Selected\":\"" + this.SelectBox.options[i].selected + "\"}";
		}
	}
	
	return "[" + v + "]";
	
}

HTMLSelectBox.prototype.GetValue = function(){
	
	var v = "";
	
	for (var i = 0; i < this.SelectBox.options.length; i++){
		if(this.SelectBox.options[i].selected){
			if (v.length == 0){ 
				v = this.SelectBox.options[i].value;
			} else {
				v = v + "," + this.SelectBox.options[i].value;
			}
		}
	}
	
	return v;
	
}

HTMLRadioOptionWithLabel = function(
	name, 
	id, 
	value, 
	label, 
	checked,
	disabled, 
	className
	){	

	this.Label = document.createElement("label");
		this.Label.setAttribute("for", id);
		this.Label.append(document.createTextNode(label));
	this.Input = document.createElement("input");
		this.Input.type 		= "radio";
		this.Input.checked 		= checked;
		this.Input.disabled 	= disabled;
		this.Input.value 		= value;
		this.Input.id			= id;
		this.Input.name			= name;
		
	this.Container = document.createElement("div");
		this.Container.className = className;
		
	this.Container.append(this.Input);
	this.Container.append(this.Label);
	
}



HTMLListOfCheckBoxWithLabel = function(
	name, 
	id,
	data, 
	className
	){
		
	this.Panel = document.createElement("div");
		
		
	this.Container = document.createElement("div");
		this.Container.id = id;
		this.Container.className = className;
		this.Panel.append(this.Container);
		
	this.Items = [];
	
	if (typeof data == "object"){ 
	
		this.Data = data;
		
		for (var i = 0; i < this.Data.length; i++){
			
			this.AddItem(
				this.Data[i].Name, 
				this.Data[i].id, 
				this.Data[i].Value, 
				this.Data[i].Label, 
				this.Data[i].IsChecked,
				this.Data[i].IsDisabled
			);
		}
		
	}
	
	this.SelectPanel = document.createElement("div");
		
	this.SelectAllLink = document.createElement("a");
		this.SelectAllLink.Parent = this;
		this.SelectAllLink.append(document.createTextNode("All"));
		this.SelectAllLink.onclick = function(){
			this.Parent.SelectAll();
		}
	this.SelectNoneLink = document.createElement("a");
		this.SelectNoneLink.Parent = this;
		this.SelectNoneLink.append(document.createTextNode("None"));
		this.SelectNoneLink.onclick = function(){
				this.Parent.SelectNone();
			}
	this.SelectPanel.append(document.createTextNode("Select: "));
	this.SelectPanel.append(this.SelectAllLink);
	this.SelectPanel.append(document.createTextNode("|"));
	this.SelectPanel.append(this.SelectNoneLink);
	
	this.Panel.append(this.SelectPanel);
}

HTMLListOfCheckBoxWithLabel.prototype.AddItem = function(
	name, 
	id, 
	value, 
	label, 
	checked,
	disabled
	){
	
	
	this.Items[this.Items.length] = new HTMLCheckBoxOptionWithLabel(
		name, 
		id, 
		value, 
		label, 
		checked,
		disabled
		);
		
	this.Container.append(this.Items[this.Items.length - 1].Container);
	
}

HTMLListOfCheckBoxWithLabel.prototype.SelectAll = function(){

	for (var i = 0; i < this.Items.length; i++){
		if (!this.Items[i].Input.disabled){
			this.Items[i].Input.checked = true;
		}
	}
	
}

HTMLListOfCheckBoxWithLabel.prototype.SelectNone = function(){

	for (var i = 0; i < this.Items.length; i++){
		if (!this.Items[i].Input.disabled){
			this.Items[i].Input.checked = false;
		}
	}
	
}

HTMLListOfCheckBoxWithLabel.prototype.DisableAll = function(){

	for (var i = 0; i < this.Items.length; i++){
		this.Items[i].Input.disabled = true;
	}
	
}

HTMLListOfCheckBoxWithLabel.prototype.DisableNone = function(){

	for (var i = 0; i < this.Items.length; i++){
		this.Items[i].Input.disabled = false;
	}
	
}

HTMLListOfCheckBoxWithLabel.prototype.GetData = function(){
	
	var s = "";
	var first = true;
	
	s = s + "[";
	
	for (var i = 0; i < this.Items.length; i++){
		if (this.Items[i].Input.checked){
			if (!first){
				s = s + ",";
			}
			s = s + "{\"id\":" + this.Items[i].Input.value + "}";
			first = false;
		}
	}

	s = s + "]";
	
	return s;
}

HTMLCheckBoxOptionWithLabel = function(
	name, 
	id, 
	value, 
	label, 
	checked,
	disabled
	){	

	this.Label = document.createElement("label");
		this.Label.Parent		= this;
		this.Label.setAttribute("for", id);
		this.Label.append(document.createTextNode(label));
	this.Input = document.createElement("input");
		this.Input.Parent 		= this;
		this.Input.type 		= "checkbox";
		this.Input.checked 		= checked;
		this.Input.disabled 	= disabled;
		this.Input.value 		= value;
		this.Input.id			= id;
		this.Input.name			= name;
		
	this.Container = document.createElement("div");
	this.Container.append(this.Input);
	this.Container.append(this.Label);
	
}

HTMLListOfCheckBoxWithLabel.prototype.RemoveItem = function(){
	
}

HTML = function(){
	
}
HTML.text = function(s){
	
	return document.createTextNode(s);
	
}

HTML.c = function(
	parent, 
	type, 
	name, 
	id, 
	className, 
	appendTo,
	textcontent
	){
	
	var E = document.createElement(type);
		
		
	if(typeof name == "string"){
		E.name = name;
	}
	
	if(typeof id == "string"){
		E.id = id;
	}
	
	if(typeof className == "string"){
		E.className = className;
	}
	
	if(typeof textcontent == "string"){
		E.append(document.createTextNode(textcontent));
	}
	
	try{
		E.Parent = parent;
	}catch(e){}
	
	try{
		appendTo.append(E);
	}catch(e){}
	
	return E;
	
}