AsentiaCarousel = function(
	identifier, 
	node
	){
	
	this.Identifier 				= identifier;
	this.ActualItemIndex 			= 0;
	this.EffectiveItemIndex 		= 0;
	this.ItemIndexQueuedForDelete 	= -1;
	this.LastViewableIndex			= 0;
	
	this.VisiblePanes				= 1;
	
	this.ItemCounter				= -1;
	
	this.Panel = document.createElement("div");
		this.Panel.id 			= identifier;
		this.Panel.className 	= "AsentiaCarouselContainer";
		
	this.UL = document.createElement("ul");
		this.UL.className 		= "AsentiaCarousel";
		
	this.Items 		= [];
	this.Sizes 		= [];
	this.Excludes 	= [];
	
	this.NextItemButton = new AsentiaCarouselButton(
		this, 
		identifier + "-Next-Button",
		"NextButton", 
		function(){
			this.Parent.GoToNextItem();
			}
		);
		
	this.PreviousItemButton = new AsentiaCarouselButton(
		this, 
		identifier + "-Previous-Button", 
		"PreviousButton",
		function(){
			this.Parent.GoToPreviousItem();
			}
		);
	
	this.NextItemButton.Enable(false); // start disabled
	this.PreviousItemButton.Enable(false); // start disabled
	
	this.Dots = new AsentiaCarouselDots(
		this, 
		this.Identifier + "-CarouselDots"
		);
	
	this.Panel.append(this.UL);
	node.append(this.Panel);
	
}

AsentiaCarousel.prototype.AttachItem = function(
	node, 
	size, 
	exclude, 
	className
	){
		
	this.ItemCounter++;
	
	this.BlockTransitions(true);
	this.Items.push(document.createElement("li"));
	
	if (typeof size == "number"){
		if (isNaN(size)){
			this.Sizes.push(100);
		}else{
			this.Sizes.push(Number(size));
		}
	}else{
		this.Sizes.push(100);
	}	
	
	if (exclude == true){
		this.Excludes[this.Excludes.length] = true;
	}else{
		this.Excludes[this.Excludes.length] = false;
	}
	
	this.UpdateLastViewableIndex();
	
	this.Items[this.Items.length - 1].className = "AsentiaCarouselItem";
	if (typeof className == "string"){
		this.Items[this.Items.length - 1].className = this.Items[this.Items.length - 1].className + " " + className;
	}
	this.Items[this.Items.length - 1].id = this.Identifier + "-Item" + this.ItemCounter;
	this.Items[this.Items.length - 1].append(node);
	
	this.UL.append(this.Items[this.Items.length - 1]);
	
	this.EffectiveItemCount = this.GetActualItemCount();
	
	this.NextItemButton.Enable(this.GetEffectiveItemIndex() < this.GetLastViewableIndex());
	
	//carousel width 
	this.SetCarouselCSSWidth();
	
	// each module width
	this.SetAllItemCSSWidths();
	
	// correct the positioning for new values
	this.SetCarouselCSSPosition()
	
	this.Dots.NewDot();
	this.Dots.SetActiveDot(this.GetEffectiveItemIndex());
	
	this.BlockTransitions(false);
	
}

AsentiaCarousel.prototype.RemoveItemAtIndex = function(
	index
	){
		
	this.DoMaintenance();
	
	// queue for delete the ACTUAL index
	this.ItemIndexQueuedForDelete = index;
	
	// delay MOVEMENT transitions for delete
	this.DelayTransitions(true);
	
	// fade out
	this.Items[index].className += " Invisible";
	
	// MANUALLY slide to the next slide
	if (index == this.GetActualItemCount() - 1){
		this.ActualItemIndex = index - 1;
		this.EffectiveItemIndex--;
	}else{
		this.ActualItemIndex = index + 1;
		// this.EffectiveItemIndex stays the same
	}
	
	this.UL.style.right = this.GetCarouselCSSPosition();
	
	// update the effective values (reduce count)
	this.EffectiveItemCount--;
		
	// remove nav dot
	this.Dots.RemoveDot();
	
	// reset active dot
	this.Dots.SetActiveDot(this.GetEffectiveItemIndex());
	
	// enable/disable 
	// use the 'effective' values since we have not removed from the DOM yet
	this.PreviousItemButton.Enable(this.GetEffectiveItemIndex() > 0);
	this.NextItemButton.Enable(this.GetEffectiveItemIndex() < this.GetEffectiveItemCount() - 1); 
	
}

AsentiaCarousel.prototype.GoToNextItem = function(){

	if (this.Excludes[this.GetEffectiveItemIndex() + 1] == true){
		this.GoToItemAtIndex(this.GetEffectiveItemIndex() + 2);
	}else{
		this.GoToItemAtIndex(this.GetEffectiveItemIndex() + 1);
	}
	
	
}

AsentiaCarousel.prototype.GoToPreviousItem = function(){
	
	if (this.Excludes[this.GetEffectiveItemIndex() - 1] == true){
		this.GoToItemAtIndex(this.GetEffectiveItemIndex() - 2);
	}else{
		this.GoToItemAtIndex(this.GetEffectiveItemIndex() - 1);
	}
	
}

AsentiaCarousel.prototype.GoToFirstItem = function(){
	
	this.GoToItemAtIndex(0);
	
}

AsentiaCarousel.prototype.GoToLastItem = function(){
	
	this.GoToItemAtIndex(this.GetLastViewableIndex());
	
}

AsentiaCarousel.prototype.GoToItemAtIndex = function(index){
	
	this.DoMaintenance();
	
	if (index < 0){ index = 0;}
	if (index > this.GetLastViewableIndex()){ index = this.GetLastViewableIndex();}
	
	// EDITS HERE
	
	this.DelayTransitions(false);
	
	this.ActualItemIndex = index;
	this.EffectiveItemIndex = index;
	
	this.SetCarouselCSSPosition();
	
	this.PreviousItemButton.Enable(index > 0);
	this.NextItemButton.Enable(this.GetEffectiveItemIndex() < this.GetLastViewableIndex());
	
	this.Dots.SetActiveDot(this.GetEffectiveItemIndex());
	
}

AsentiaCarousel.prototype.SetVisiblePanes = function(c){
	
	this.VisiblePanes = c;
	
	// recalc
	
	this.SetCarouselCSSWidth();
	
}

AsentiaCarousel.prototype.GetVisiblePanes = function(c){
	
	return this.VisiblePanes;
	
}


/* INTERNALS: */

AsentiaCarousel.prototype.BlockTransitions = function(bln){
	
	this.UL.style.transition = (bln) ? "none" : "left 0.5s, right 0.5s";
	
}

AsentiaCarousel.prototype.DelayTransitions = function(bln){
	
	this.UL.style.transitionDelay = (!bln) ? "none" : "0.25s";
	
}

AsentiaCarousel.prototype.GetEffectiveItemCount = function(){
	
	return this.EffectiveItemCount;
	
}

AsentiaCarousel.prototype.GetActualItemCount = function(){
	 
	 return this.Items.length;
	 
}

AsentiaCarousel.prototype.GetActualItemIndex = function(){
	 
	 return this.ActualItemIndex;
	 
}

AsentiaCarousel.prototype.GetEffectiveItemIndex = function(){
	 
	 return this.EffectiveItemIndex;
	 
}

AsentiaCarousel.prototype.GetCarouselCSSWidth = function(){
	
	return this.GetCarouselWidth().toString() + "%";
	
}
AsentiaCarousel.prototype.GetCarouselWidth = function(){
	
	// (100% * item count)
	// 1 item = 100%
	// 2 items = 200%
	// 3 items = 300%
	// etc
	
	var j = 0;
	
	for (var i = 0; i < this.Sizes.length; i++){
		j += this.Sizes[i];
	}
	
	if(j < 100){
		j = 100;
	}
	
	return j;
	
	//return (this.GetActualItemCount() * 100) / this.GetVisiblePanes() + "%";
	
}

AsentiaCarousel.prototype.GetItemCSSWidth = function(){
	
	// width = 100% / number of items
	// 3 items = 33% each
	// 2 items = 50% each
	// etc
	
	return (100 / this.GetActualItemCount()).toString() + "%";
	
}

AsentiaCarousel.prototype.GetCarouselCSSPosition = function(){
	
	var j = 0;
	
	for (var i = 0; i < this.GetActualItemIndex(); i++){
		j = j + this.Sizes[i];
	}
	
	
	return j.toString() + "%";
	//return (this.GetActualItemIndex() * 100/ this.GetVisiblePanes()).toString() + "%";

}

AsentiaCarousel.prototype.DoMaintenance = function(){
	
	this.BlockTransitions(true);
	
	// delete the queued item if it exists
	this.HardDeleteQueuedItem();
	
	this.EffectiveItemCountCount = this.GetActualItemCount();
	
	//carousel width 
	this.SetCarouselCSSWidth();
	
	// each module width
	this.SetAllItemCSSWidths();
	
	// correct the positioning for new values
	this.SetCarouselCSSPosition()
	
	this.BlockTransitions(false);
	
}

AsentiaCarousel.prototype.HardDeleteQueuedItem = function(){
	
	if (this.ItemIndexQueuedForDelete == -1) { return true; }
	
	this.Items[this.ItemIndexQueuedForDelete].parentNode.removeChild(this.Items[this.ItemIndexQueuedForDelete]);
	
	this.Items.splice(this.ItemIndexQueuedForDelete, 1);
	this.Sizes.splice(this.ItemIndexQueuedForDelete, 1);
	
	this.ItemIndexQueuedForDelete = -1;
	
}

AsentiaCarousel.prototype.SetCarouselCSSPosition = function(){
	
	this.UL.style.right = this.GetCarouselCSSPosition();
	
}

AsentiaCarousel.prototype.SetCarouselCSSWidth = function(){
	
	this.UL.style.width = this.GetCarouselCSSWidth();

}
	
AsentiaCarousel.prototype.SetAllItemCSSWidths = function(){
	
	for (var i = 0; i < this.GetActualItemCount(); i++){
		
		this.UL.childNodes[i].style.width = this.GetItemCSSWidthByIndex(i);
		
	}
	
}

AsentiaCarousel.prototype.GetItemCSSWidthByIndex = function(index){
	
	return (this.Sizes[index] * (100 / this.GetCarouselWidth())).toString() + "%";
	
}
	
AsentiaCarousel.prototype.GetLastViewableIndex = function(){
	
	return this.LastViewableIndex;
	
}

AsentiaCarousel.prototype.UpdateLastViewableIndex = function(){
	
	var c = 0;
	
	for (var i = this.Sizes.length - 1; i >= 0; i--){
		
		c += this.Sizes[i];
		
		if (c <= 100){this.LastViewableIndex = i;}
		
	}
	
	if (c < 100){this.LastViewableIndex = 0;}
	
}

AsentiaCarousel.prototype.NumberToPct = function(i){
	
	return i.toString() + "%";
	
}

/* DOTS OBJECT */

AsentiaCarouselDots = function(
	parent, 
	identifier
	){
	
	this.Parent 		= parent;
	this.Identifier 	= identifier;
	
	this.Panel = document.createElement("div");
		this.Panel.className = "AsentiaCarouselDots";
		
}

AsentiaCarouselDots.prototype.NewDot = function(){
	
	var span = document.createElement("span");
		span.className = "AsentiaCarouselDot";
		
	span.Index = this.Panel.childNodes.length;
	span.Parent = this;
	
	span.onclick = function(){
			this.Parent.Parent.GoToItemAtIndex(this.Index);
		}
		
	this.Panel.append(span);
}

AsentiaCarouselDots.prototype.RemoveDot = function(){
	
	this.Panel.removeChild(this.Panel.lastChild);
	
}

AsentiaCarouselDots.prototype.SetActiveDot = function(idx){
	for (var i = 0; i < this.Panel.childNodes.length; i++)
		if (idx == i){
			this.Panel.childNodes[i].className = "AsentiaCarouselDot On";
		}else{
			this.Panel.childNodes[i].className = "AsentiaCarouselDot";
		}
}

/*  NAV BUTTON OBJECT */

AsentiaCarouselButton = function(
	parent, 
	identifier, 
	className, 
	onclick
	){
	
	this.Onclick 		= onclick;
	this.Identifier 	= identifier;
	this.Parent 		= parent;
	
	this.Button = document.createElement("input");
		this.Button.Parent = this;
		this.Button.type = "button";
		this.Button.className = "AsentiaCarouselButton";
		
		if (className) {this.Button.className += " " + className;}
		
		this.Button.onclick = function(){
			this.Parent.Onclick();
		}
	
}

AsentiaCarouselButton.prototype.Enable = function(bln){

	this.Button.disabled = !bln;
		
}
