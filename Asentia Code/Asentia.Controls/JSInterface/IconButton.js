
IconButton = function(
	parent,
	node,
	src,
	label,
	isOn, 
	className
	){

	this.TypeName = "IconButton";
		
	this.Parent = parent;
	this.Disabled = false;
	
	this.Panel 	= document.createElement("div");
		this.Panel.className = "IconButton";
		if (className == "undefined"){
			this.Panel.className = "IconButton";
		}else{
			this.Panel.className = "IconButton " + className;
		}
	var divIcon  		= document.createElement("div");
		divIcon.className = "icon";
	var divLabel 		= document.createElement("div");
		divLabel.className = "label"
	
	this.Image = document.createElement("img");
		this.Image.parent = this;
		this.Image.src = src;
		this.Image.onclick = function(){
			this.parent.onclick();
		}
	this.Label = document.createElement("div");
		this.Label.parent = this;
		this.Label.append(HTML.text(label));
		this.Label.onclick = function(){
			this.parent.onclick();
		}
	divIcon.append(this.Image);
	divLabel.append(this.Label);
	
	this.Panel.append(divIcon);
	this.Panel.append(divLabel);
	
	node.append(this.Panel);

}

IconButton.prototype.Enable = function(bln){
	
	this.Disabled = !bln;
	
	if(bln){
		if (this.Panel.className == "IconButtonDisabled"){ 
			this.Panel.className = ""; 
		}else{
			this.Panel.className = this.Panel.className.replace("IconButtonDisabled ", "");
			this.Panel.className = this.Panel.className.replace(" IconButtonDisabled", "")
		}
	}else{
		if (this.Panel.className.indexOf("IconButtonDisabled") == -1){
			this.Panel.className = this.Panel.className + " IconButtonDisabled";
		} 
	}
	
}