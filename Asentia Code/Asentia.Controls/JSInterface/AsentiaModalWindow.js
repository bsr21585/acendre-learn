Asentia_ModalWindow = function(
	parent,
	node, 
	title, 
	containmentNode, 
	closeButtonIconPath
	){
	
	this.TypeName = "Asentia-ModalWindow";
	this.Parent = parent;
	
	// panel
	this.Panel = document.getElementById(node);
	
	// destroy existing DOM within element
	try{
		while(this.Panel.childNodes.length > 0){
			this.Panel.removeChild(this.Panel.childNodes[0]);
		}
	}catch(e){}
	
	// begin
	this.Panel.className = "ModalPopupContainer JSModalContainer";	
	this.Panel.style.visibility = "visible";
	this.Panel.style.opacity = 1;
		
	// close button
	this.CloseButton = document.createElement("img");
		this.CloseButton.Parent = this;
		this.CloseButton.className = "ModalPopupCloseIcon MediumIcon";
		this.CloseButton.setAttribute("src", closeButtonIconPath);
		this.CloseButton.onclick = function(){
			this.Parent.Close();
		}
		
	// header
	this.Header = document.createElement("div");
		this.Header.className = "ModalPopupHeader";
		
	var P = document.createElement("p");
	
	// icon
	this.Icon = document.createElement("img");
	
	// title	
	this.Title = document.createElement("div");
		this.Title.append(document.createTextNode(title));
		
	this.Panel.append(this.CloseButton);
	this.Panel.append(this.Header);
	
	P.append(this.Icon);
	P.append(this.Title);
	
	this.Header.append(P);
	
	// move to the body end so we can assume the dimensions
	//this.DocBody = document.getElementsByTagName("BODY")[0];
	//this.Panel.parentNode.removeChild(this.Panel);
	//this.DocBody.append(this.Panel); // put at the end
	
	if (containmentNode != "document" && containmentNode != "window"){containmentNode = "#" + containmentNode;}
	
	$("#" + this.Panel.id).draggable({
		containment: containmentNode,
		scroll:false
	});
	
}

Asentia_ModalWindow.prototype.SetTitle = function(s){
	
	this.Title.removeChild(this.Title.childNodes[0]);
	this.Title.append(document.createTextNode(s));
}

Asentia_ModalWindow.prototype.Close = function(){
	
	this.Panel.style.opacity = 0;
	this.Panel.style.visibility = "hidden";
	
}

Asentia_ModalWindow.prototype.Reposition = function(){
	
	this.ActualWidth = this.Panel.offsetWidth;
	this.ActualHeight = this.Panel.offsetHeight;

	this.WindowWidth =  (window.innerWidth) ? window.innerWidth : document.documentElement.clientWidth;
	this.WindowHeight =  (window.innerHeight) ? window.innerHeight : document.documentElement.clientHeight;
	
	/*
	alert(this.ActualWidth);
	alert(this.ActualHeight);
	alert(this.WindowWidth);
	alert(this.WindowHeight);
	*/
	
	this.Panel.style.top = ((this.WindowHeight / 2) - (this.ActualHeight / 2)).toString() + "px";
	this.Panel.style.left = ((this.WindowWidth / 2) - (this.ActualWidth / 2)).toString() + "px";
	
	//var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	//	var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	
}


	