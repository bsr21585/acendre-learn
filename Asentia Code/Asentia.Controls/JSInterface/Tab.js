Tab = function(
	parent, 
	node,		// attach to node
	label,		// label
	isOn,		// bit 
	value
	){
	
	this.TypeName = "Tab";
	
	this.Parent = parent;
	this.Value = value;
	
	if (isOn == true){
		this.On = true;
	} else {
		this.On = false;
	}
	
	var onClass = "";
	
	if (this.On == true){
		onClass = " TabbedListLIOn";
	}
	
	this.LI = document.createElement("li");
		this.LI.Tab = this;
		this.LI.className = "TabbedListLI" + onClass;
		this.LI.onclick = function(){
			if (this.Tab.On != true){
				this.Tab.Click();
			}
		}
		
	var div = document.createElement("div");
		div.appendChild(HTML.text(label));
		
	this.LI.append(div);
	node.append(this.LI);
	
	return this;
}
Tab.prototype.Click = function(s){
	try{
		this.Parent.SetTab(this.Value);
	} catch(e){}

}