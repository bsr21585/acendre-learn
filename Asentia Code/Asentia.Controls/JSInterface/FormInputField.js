
FormInputField = function(
	parent, 			//object
	node,				// DOM node to append to
	type,				// type (input|textarea)
	label,  			// text
	isRequired, 		// bit
	languages, 			// array
	initialLanguage,	// initialLanguage
	flagImagePath,		// path to flag images
	data, 				// JSON
	className,			// className to add to the input
	defaultLabel		// default label if unable to extract
	){
	
	this.TypeName = "FormInputField";
	
	try {
		this.Data = data;
	}catch(e){
		this.Data = ""; // if error, then we are an empty string
	}
	
	switch(type.toLowerCase()){
		case "input":
		case "textarea":
		case "custom": // manually add
			break;
		default:
			return false;
			break;
	}
	
	
	this.parent 			= parent;
	this.Inputs 			= []; // array of the language inputs (container and input)
	this.InitialLanguage 	= initialLanguage;
	this.Languages			= languages;
	this.FlagImagePath 		= flagImagePath;
	
	this.FormFieldContainer 		= document.createElement("div");
	this.FormFieldLabelContainer 	= document.createElement("div");
	this.FormFieldInputContainer 	= document.createElement("div");
	this.Label						= document.createElement("label");
	
	this.FormFieldContainer.className 		= "FormFieldContainer";
	this.FormFieldInputContainer.className 	= "FormFieldInputContainer";
		if (languages.length > 1 && type.toLowerCase() != "custom"){
			this.FormFieldInputContainer.className 	= this.FormFieldInputContainer.className + " InputWithLanguageSelector";
		}
	this.FormFieldLabelContainer.className	= "FormFieldLabelContainer";
	this.Label.className 					= "";
	
	//LABEL
	
	this.FormFieldContainer.append(this.FormFieldLabelContainer);
		this.FormFieldLabelContainer.append(this.Label);
		this.Label.append(HTML.text(label + ":"));
		if (isRequired){
			var Req = document.createElement("span");
				Req.className = "RequiredAsterisk";
				Req.append(HTML.text(" * "));
			this.FormFieldLabelContainer.append(Req);
		}
		
	// if this is a custom input, done. exit.
	if (type.toLowerCase() == "custom"){
		this.FormFieldContainer.append(this.FormFieldInputContainer);
		node.append(this.FormFieldContainer);
		return true;
	}    
	
	// LANGUAGE SELECTOR before TEXTAREA
	if (type.toLowerCase() == "textarea"){
		if (languages.length > 1){
			this.LanguageSelector = new LanguageSelector(
				this,
				this.FormFieldInputContainer,
				this.FlagImagePath,
				this.Languages, 
				this.InitialLanguage
				)
		}
	}
	
	//INPUTS	
	
	for (var i = 0; i < this.Languages.length; i++){
		
		var input = document.createElement(type.toLowerCase());
			input.setAttribute("lang", this.Languages[i]);
				
		switch(type.toLowerCase()){
			case "input":
				break;
			case "textarea":
				input.rows = 5;
				input.cols = 20;
				break;
		}
					
		try{if (className.length > 0) {input.className = className + " ";}} catch(e){}
		
		if (this.Languages[i] != this.InitialLanguage){
			if(input.className.indexOf("certification-editor-hide") == -1){
				input.className = input.className + "certification-editor-hide";
			}
		}
		
		// insert the JSON values
		// if single language, then text, else array
		switch(typeof this.Data){
			case "undefined":
				input.value = defaultLabel;
				break;
			case "string":
			    if (this.Data != "" && this.Data != null) {
			        input.value = unescape(this.Data);
				}
			    else {
			        if (defaultLabel != "" && defaultLabel != null) {
			            input.value = defaultLabel;
			        }
			        else {
			            input.value = "";
			        }
				}
				break;
		    case "object":
		        if (this.Data != "" && this.Data != null) {
		            for (var j = 0; j < this.Data.length; j++) {
		                if (this.Data[j].Language == this.Languages[i]) {
		                    input.value = unescape(this.Data[j].Value);
		                }
		            }
		        }
		        else {
		            input.value = "";
		        }
				break;
		}
		
		// add to input array
		this.Inputs.push(input);
		
		// add to DOM
		this.FormFieldInputContainer.append(input);
		
	}
	
	// LANGUAGE SELECTOR after INPUTS
	if (type.toLowerCase() == "input"){
		if (languages.length > 1){
			this.LanguageSelector = new LanguageSelector(
				this,
				this.FormFieldInputContainer,
				this.FlagImagePath,
				this.Languages, 
				this.InitialLanguage
				)
		}
	}

	this.FormFieldContainer.append(this.FormFieldInputContainer);
	
	node.append(this.FormFieldContainer);
	
}
FormInputField.prototype.SetLanguage = function(code){
	for (var i = 0; i < this.Inputs.length; i++){
		if (this.Inputs[i].getAttribute("lang") == code){
			if (this.Inputs[i].className == "certification-editor-hide"){
				this.Inputs[i].className = "";
			} else {
				this.Inputs[i].className = this.Inputs[i].className.replace(" certification-editor-hide", "");
				this.Inputs[i].className = this.Inputs[i].className.replace("certification-editor-hide ", "");
			}
		}else{
			if(this.Inputs[i].className.indexOf("certification-editor-hide") == -1){
				this.Inputs[i].className = this.Inputs[i].className + " certification-editor-hide";
			}
		}
	}
}

FormInputField.prototype.AreAnyInputFieldsEmpty = function()
{
	//checks all the Inputs[] text fields to see if any are empty
	for (var i = 0; i < this.Inputs.length; i++)
	{
		if (this.Inputs[i].value == "" || this.Inputs[i].value == undefined)
			return true;
	}
	
	return false;
}

FormInputField.prototype.IsInitialLanguageFieldEmpty = function()
{    
	//check Inputs[] and be sure default language field is field
	for (var i = 0; i < this.Inputs.length; i++)
	{
		//find the field that is the default/initial language
		if (this.Inputs[i].lang == this.InitialLanguage)
		{
			//be sure the field is not empty
			if (this.Inputs[i].value == "" || this.Inputs[i].value == undefined)
				return true;
		}
	}
	
	return false;
}

FormInputField.prototype.ClearError = function(){

	// remove the existing error
	try{
		this.FormFieldContainer.removeChild(this.FormFieldErrorContainer);
	} catch(e){}

}
	
FormInputField.prototype.ShowError = function(s){
	
	this.ClearError();
	
	this.FormFieldErrorContainer = document.createElement("div");
		this.FormFieldErrorContainer.className = "FormFieldErrorContainer";
	var p = document.createElement("p");
		p.append(document.createTextNode(s));
	
	this.FormFieldErrorContainer.append(p);
	this.FormFieldContainer.insertBefore(this.FormFieldErrorContainer, this.FormFieldInputContainer);
	
}

FormInputField.prototype.GetData = function(){
	
	var s = "";
	
	if (this.Inputs.length == 1)
	{
		//s = s + "\"" + this.Inputs[0].value + "\"";
	    s = s + "{\"Language\":\"" + this.Inputs[0].getAttribute("lang") + "\",\"Value\":\"" + this.EscapeNewlineFeeds(this.Inputs[0].value) + "\"}";
		
	} else {
		for (var i = 0; i < this.Inputs.length; i++){
			if (s.length != 0) {s = s + ",";}
			s = s + "{\"Language\":\"" + this.Inputs[i].getAttribute("lang") + "\",\"Value\":\"" + this.EscapeNewlineFeeds(this.Inputs[i].value) + "\"}";
		}
	}
	
	return "[" + s + "]";
	
}

FormInputField.prototype.GetDataForSingleInputValue = function()
{	
	//if this input field only has one value, send just that value with no containing braces
	if (this.Inputs.length == 1)
	    return "\"" + this.EscapeNewlineFeeds(this.Inputs[0].value) + "\"";

	return;
}

FormInputField.prototype.EscapeNewlineFeeds = function (targetString)
{
    //strip out the new line 
    var updatedString = targetString.replace(/\r?\n/g, "%0A");

    return updatedString;
}