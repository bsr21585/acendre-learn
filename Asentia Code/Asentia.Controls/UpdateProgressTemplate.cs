﻿using Asentia.Common;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    /// <summary>
    ///MyProgressTemplate class
    /// </summary>
    public class UpdateProgressTemplate : ITemplate
    {
        public UpdateProgressTemplate()
        { }

        public UpdateProgressTemplate(bool loadImagesFromCustomerManager)
        {
            OverrideLoadImagesFromCustomerManager = loadImagesFromCustomerManager;
        }

        /// <summary>
        /// load images from customar manager website project if True
        /// </summary>
        public bool OverrideLoadImagesFromCustomerManager = false;

        /// <summary>
        /// Progress Template
        /// </summary>
        /// <param name="container"></param>
        public void InstantiateIn(Control container)
        {
            Panel loaderPanel = new Panel();
            loaderPanel.CssClass = "divWaiting";
            Image imgLoader = new Image();
            imgLoader.AlternateText = _GlobalResources.Loading;
            imgLoader.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING,
                                                        ImageFiles.EXT_GIF, OverrideLoadImagesFromCustomerManager);
            imgLoader.CssClass = "LargeIcon";
            loaderPanel.Controls.Add(imgLoader);
            container.Controls.Add(loaderPanel);
        }
    }
}
