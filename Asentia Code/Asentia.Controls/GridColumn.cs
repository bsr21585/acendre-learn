﻿using System.Collections;

namespace Asentia.Controls
{
    /// <summary>
    /// Class to define the column of a <see cref="Grid" />.
    /// </summary>
    public class GridColumn
    {
        #region Constructors
        /// <summary>
        /// Constructs an instance of a <see cref="GridColumn"/> using a header,
        /// data field, sort field, and specification on whether to center the data.
        /// </summary>
        /// <param name="headerText">The label for the <see cref="GridColumn" /></param>
        /// <param name="dataField">The data field for the <see cref="GridColumn" /></param>
        /// <param name="sortField">The field used to sort the <see cref="GridColumn" /></param>
        /// <param name="centerDataInColumn">Determines whether or not to center the data in the <see cref="GridColumn" />.</param>
        public GridColumn(string headerText, string dataField, string sortField, bool centerDataInColumn, bool isResponsiveHidden = false)
        {
            this.HeaderText = headerText;
            this.DataField = dataField;
            this.SortField = sortField;
            this.CenterDataInColumn = centerDataInColumn;
            this.IsResponsiveHidden = isResponsiveHidden;
            this.Properties = ArrayList.ReadOnly(_Properties);
        }

        /// <summary>
        /// Constructs an instance of a <see cref="GridColumn"/> using a header,
        /// data field, and sort field.
        /// </summary>
        /// <param name="headerText">The label for the <see cref="GridColumn" /></param>
        /// <param name="dataField">The data field for the <see cref="GridColumn" /></param>
        /// <param name="sortField">The field used to sort the <see cref="GridColumn" /></param>
        public GridColumn(string headerText, string dataField, string sortField)
        {
            this.HeaderText = headerText;
            this.DataField = dataField;
            this.SortField = sortField;
            this.Properties = ArrayList.ReadOnly(_Properties);
        }

        /// <summary>
        /// Constructs an instance of a <see cref="GridColumn"/> using a header,
        /// data field, and specification on whether to center the data.
        /// </summary>
        /// <param name="headerText">The label for the <see cref="GridColumn" /></param>
        /// <param name="dataField">The data field for the <see cref="GridColumn" /></param>
        /// <param name="centerDataInColumn">Determines whether or not to center the data in the <see cref="GridColumn" />.</param>
        public GridColumn(string headerText, string dataField, bool centerDataInColumn, bool isResponsiveHidden = false)
        {
            this.HeaderText = headerText;
            this.DataField = dataField;
            this.CenterDataInColumn = centerDataInColumn;
            this.IsResponsiveHidden = isResponsiveHidden;
            this.Properties = ArrayList.ReadOnly(_Properties);
        }

        /// <summary>
        /// Constructs an instance of a <see cref="GridColumn"/> using a header,
        /// and data field.
        /// </summary>
        /// <param name="headerText">The label for the <see cref="GridColumn" /></param>
        /// <param name="dataField">The data field for the <see cref="GridColumn" /></param>
        public GridColumn(string headerText, string dataField)
        {
            this.HeaderText = headerText;
            this.DataField = dataField;
            this.Properties = ArrayList.ReadOnly(_Properties);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The label for the column.
        /// </summary>
        public string HeaderText = null;

        /// <summary>
        /// The data field name the column maps to.
        /// </summary>
        public string DataField = null;

        /// <summary>
        /// The data field name used to sort the column.
        /// </summary>
        public string SortField = null;

        /// <summary>
        /// Determines whether or not to center the data in the column.
        /// </summary>
        public bool CenterDataInColumn = false;

        /// <summary>
        /// Determines whether or not a column is to be hidden for responsive layout (sub 640px wide).
        /// </summary>
        public bool IsResponsiveHidden = false;

        /// <summary>
        /// Read only list of column properties.
        /// </summary>
        public ArrayList Properties;
        #endregion

        #region Private Properties
        /// <summary>
        /// Private list to store the column's properties.
        /// </summary>
        private ArrayList _Properties = new ArrayList();
        #endregion

        #region Methods
        #region AddProperty
        /// <summary>
        /// Adds an instance of <see cref="GridColumnProperty"/> to the <see cref="GridColumn"/>
        /// </summary>
        /// <param name="gridColumnProperty">The <see cref="GridColumnProperty"/> to add.</param>
        public void AddProperty(GridColumnProperty gridColumnProperty)
        {
            this._Properties.Add(gridColumnProperty);
        }
        #endregion
        #endregion
    }
}
