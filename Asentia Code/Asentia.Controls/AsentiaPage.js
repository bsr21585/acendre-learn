﻿$(document).ready(function () {
    // BEGIN DROP-DOWN BREADCRUMB FUNCTIONS
    $("#PageBreadcrumbContainer").each(function () {
        $("#" + this.id).click(function (event) {
            event.stopPropagation();
        });
    });

    $("#BreadcrumbDownArrowLI").click(function () {
        $("#BreadcrumbDropDownItems").toggle();

        if ($("#BreadcrumbDropDownItems").is(":visible")) {
            $("#BreadcrumbDownArrowLI img").css("transform", "rotate(90deg)");
        }
        else {
            $("#BreadcrumbDownArrowLI img").css("transform", "rotate(0deg)");
        }
    });

    $("#FullDropDownBreadcrumbContainer").each(function () {
        $("#" + this.id).click(function (event) {
            event.stopPropagation();
        });
    });

    $("#FullDropDownBreadcrumbDownArrowLI").click(function () {
        $("#FullDropDownBreadcrumbDropDownItems").toggle();

        if ($("#FullDropDownBreadcrumbDropDownItems").is(":visible")) {
            $("#FullDropDownBreadcrumbDownArrowLI img").css("transform", "rotate(90deg)");
        }
        else {
            $("#FullDropDownBreadcrumbDownArrowLI img").css("transform", "rotate(0deg)");
        }
    });

    $(document).click(function () {
        if ($("#BreadcrumbDropDownItems").is(":visible") || $("#FullDropDownBreadcrumbDropDownItems").is(":visible")) {
            $("#BreadcrumbDropDownItems").hide();
            $("#BreadcrumbDownArrowLI img").css("transform", "rotate(0deg)");
            $("#FullDropDownBreadcrumbDropDownItems").hide();
            $("#FullDropDownBreadcrumbDownArrowLI img").css("transform", "rotate(0deg)");
        }
    });
    // END DROP-DOWN BREADCRUMB FUNCTIONS

    // BEGIN GENERALIZED TOOLTIP FUNCTION
    $(document).tooltip({        
        position: {
            my: "center bottom",
            at: "center bottom+32",
            collision: "flip",            
            using: function (position, feedback) {
                $(this).css(position);
            }
        }
    });

    // this will ensure all "popped-up" tooltips are removed on page load, this is mainly for UpdatePanel content where the postback is not a true page reload
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () { $(".ui-tooltip-content").parents("div").remove(); });
    // END GENERALIZED TOOLTIP FUNCTION

    // initialize explandable paragraph containers
    Helper.ExpandableParagraphContainer();
});

// sets cookies then rolls out the cookie pop-up container
function AcceptCookieConsent()
{
    document.cookie = "CookieNecessaryConsent=true;path=/;";

    // sets statistics cookie if enabled
    if (document.getElementById("CookieConsentStatistics_Checkbox"))
    {
        document.cookie = "CookieStatisticsConsent=true;path=/;";
    }

    try {
        $("#CookieConsentOuter_Container").removeClass("CookieConsentRollIn");
        $("#CookieConsentOuter_Container").addClass("CookieConsentRollOut");
    } catch (e) { }
}