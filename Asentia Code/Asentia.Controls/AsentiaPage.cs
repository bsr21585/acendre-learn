﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using Asentia.Common;
using AjaxControlToolkit;

namespace Asentia.Controls
{
    public class AsentiaPage : Page
    {
        #region Constructor
        public AsentiaPage()
        {
            // initialize the control adapters to provide overrides for rendering of select .NET objects
            this._InitializeControlAdapters();
        }
        #endregion

        #region Properties
        // content containers
        public Content BreadcrumbContent = new Content();
        public Content PageContentPlaceholder = new Content();
        public Content SideContent1Placeholder = new Content();
        public Content SideContent2Placeholder = new Content();

        // panels
        public Panel PageContentContainer = new Panel();
        public Panel PageBreadcrumbContainer = new Panel();
        public Panel PageTitleContainer = new Panel();
        public Panel FullDropDownBreadcrumbContainer = new Panel();
        public Panel PageFeedbackContainer = new Panel();

        public string PageCategory = "General";

        public string TrackingCodeFilePath
        {
            get
            { return SitePathConstants.SITE_CONFIG_ROOT + "TrackingCode.txt"; }
        }

        #endregion

        #region Private Properties
        private static string[] AspNetFormElements = new string[] 
        { 
            "__EVENTTARGET",
            "__EVENTARGUMENT",
            "__VIEWSTATE",
            "__EVENTVALIDATION",
            "__VIEWSTATEENCRYPTED",
        };
        #endregion

        #region GetTrackingCode (Google Analytics)
        public string GetTrackingCode()
        {
            string trackingCode = String.Empty;

            if (File.Exists(Server.MapPath(TrackingCodeFilePath)))
            {
                using (StreamReader reader = new StreamReader(Server.MapPath(TrackingCodeFilePath)))
                { trackingCode = reader.ReadToEnd(); }
            }

            return trackingCode;
        }
        #endregion

        #region IncludePageSpecificCssFile
        /// <summary>
        /// Method to include page specific css files.
        /// </summary>
        public void IncludePageSpecificCssFile(string CSS_FILE_RELATIVE_PATH)
        {
            // tag format for the stylesheet link
            const string STYLE_SHEET_LINK_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}?ver={1}\" />\r\n";

            // attach references to "default site configurable" stylesheets
            // TODO - REMOVE EXCEPTION FOR "styling" and "icslearninggroup"
            if (AsentiaSessionState.SiteHostname != "styling" && AsentiaSessionState.SiteHostname != "#icslearninggroup#")
            {
                if (AsentiaSessionState.IsThemePreviewMode)
                {
                    string defaultPathToStylesheet = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + CSS_FILE_RELATIVE_PATH);

                    if (File.Exists(defaultPathToStylesheet))
                    {
                        Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + CSS_FILE_RELATIVE_PATH), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
                else
                {
                    string defaultPathToStylesheet = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + CSS_FILE_RELATIVE_PATH);

                    if (File.Exists(defaultPathToStylesheet))
                    {
                        Page.Header.Controls.Add(
                            new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + CSS_FILE_RELATIVE_PATH), Common.Utility.APPLICATION_COMMON_VERSION))
                        );
                    }
                }
            }

            if (AsentiaSessionState.IsThemePreviewMode)
            {
                string sitePathToStylesheet = MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + CSS_FILE_RELATIVE_PATH);

                if (File.Exists(sitePathToStylesheet))
                {
                    Page.Header.Controls.Add(
                        new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + CSS_FILE_RELATIVE_PATH), Common.Utility.APPLICATION_COMMON_VERSION))
                    );
                }
            }
            else
            {
                string sitePathToStylesheet = MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + CSS_FILE_RELATIVE_PATH);

                if (File.Exists(sitePathToStylesheet))
                {
                    Page.Header.Controls.Add(
                        new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + CSS_FILE_RELATIVE_PATH), Common.Utility.APPLICATION_COMMON_VERSION))
                    );
                }
            }
        }
        #endregion

        #region Breadcrumb
        /// <summary>
        /// Class that represents a breadcrumb link item.
        /// </summary>
        protected class BreadcrumbLink
        {
            public string LinkTitle;
            public string LinkHref;

            public BreadcrumbLink(string linkTitle, string linkUrl)
            {
                this.LinkTitle = linkTitle;
                this.LinkHref = linkUrl;
            }

            public BreadcrumbLink(string linkTitle)
            {
                this.LinkTitle = linkTitle;
                this.LinkHref = null;
            }
        }

        /// <summary>
        /// Builds the breadcrumb and attaches it to its container.
        /// </summary>
        /// <param name="crumbs">Breadcrumb items</param>
        public void BuildBreadcrumb(ArrayList crumbs)
        {
            this.PageBreadcrumbContainer.Controls.Clear();
            this.FullDropDownBreadcrumbContainer.Controls.Clear();

            string breadcrumbDelimiterString = "//";
            bool isFirstCrumb = true;
            int i;

            if (crumbs.Count > 2)
            {
                // declare ULs for drop-down breadcrumbs
                HtmlGenericControl breadcrumbOuterUL = new HtmlGenericControl("ul");
                breadcrumbOuterUL.Attributes.Add("class", "BreadcrumbOuterUL");

                HtmlGenericControl breadcrumbInnerUL = new HtmlGenericControl("ul");
                breadcrumbInnerUL.ID = "BreadcrumbDropDownItems";
                breadcrumbInnerUL.Attributes.Add("class", "BreadcrumbInnerUL");

                // build drop-down character
                HtmlGenericControl breadcrumbDownArrowLI = new HtmlGenericControl("li");
                breadcrumbDownArrowLI.ID = "BreadcrumbDownArrowLI";
                breadcrumbDownArrowLI.Attributes.Add("class", "BreadcrumbDownArrowLI");

                Image expandArrowImage = new Image();
                expandArrowImage.CssClass = "XSmallIcon";
                expandArrowImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EXPAND_ARROW, ImageFiles.EXT_PNG);
                breadcrumbDownArrowLI.Controls.Add(expandArrowImage);

                Label elipsisString = new Label();
                elipsisString.Text = "...";
                breadcrumbDownArrowLI.Controls.Add(elipsisString);

                // attach drop-down UL/LI controls
                breadcrumbDownArrowLI.Controls.Add(breadcrumbInnerUL);
                breadcrumbOuterUL.Controls.Add(breadcrumbDownArrowLI);

                // build drop-down breadcrumbs
                for (i = 0; i < crumbs.Count - 2; i++)
                {
                    BreadcrumbLink crumb = (BreadcrumbLink)crumbs[i];
                    HtmlGenericControl breadcrumbItemLI = new HtmlGenericControl("li");
                    breadcrumbItemLI.Attributes.Add("class", "BreadcrumbItemLI");

                    if (String.IsNullOrWhiteSpace(crumb.LinkHref))
                    {
                        Localize breadCrumbNoLink = new Localize();
                        breadCrumbNoLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemLI.Controls.Add(breadCrumbNoLink);
                    }
                    else
                    {
                        HyperLink breadCrumbLink = new HyperLink();
                        breadCrumbLink.NavigateUrl = crumb.LinkHref;
                        breadCrumbLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemLI.Controls.Add(breadCrumbLink);
                    }

                    // attach breadcrumb item
                    breadcrumbInnerUL.Controls.Add(breadcrumbItemLI);
                }

                // attach drop-down breadcrumbs to breadcrumb container
                this.PageBreadcrumbContainer.Controls.Add(breadcrumbOuterUL);

                // add delimiter after drop-down
                Panel postDropDownDelimiterContainer = new Panel();
                postDropDownDelimiterContainer.CssClass = "BreadcrumbDelimiter";
                Literal postDropDownDelimiter = new Literal();
                postDropDownDelimiter.Text = breadcrumbDelimiterString;
                postDropDownDelimiterContainer.Controls.Add(postDropDownDelimiter);
                this.PageBreadcrumbContainer.Controls.Add(postDropDownDelimiterContainer);

                // loop through and attach remaining breadcrumb items
                for (i = i; i < crumbs.Count; i++)
                {
                    BreadcrumbLink crumb = (BreadcrumbLink)crumbs[i];

                    // add a delimiter if this is not the first breadcrumb item
                    if (!isFirstCrumb)
                    {
                        Panel breadcrumbDelimiterContainer = new Panel();
                        breadcrumbDelimiterContainer.CssClass = "BreadcrumbDelimiter";
                        Literal breadcrumbDelimiter = new Literal();
                        breadcrumbDelimiter.Text = breadcrumbDelimiterString;
                        breadcrumbDelimiterContainer.Controls.Add(breadcrumbDelimiter);
                        this.PageBreadcrumbContainer.Controls.Add(breadcrumbDelimiterContainer);
                    }

                    // add breadcrumb item
                    Panel breadcrumbItemContainer = new Panel();
                    breadcrumbItemContainer.CssClass = "BreadcrumbItem";

                    if (String.IsNullOrWhiteSpace(crumb.LinkHref))
                    {
                        Localize breadCrumbNoLink = new Localize();
                        breadCrumbNoLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemContainer.Controls.Add(breadCrumbNoLink);
                        isFirstCrumb = false;
                    }
                    else
                    {
                        HyperLink breadCrumbLink = new HyperLink();
                        breadCrumbLink.NavigateUrl = crumb.LinkHref;
                        breadCrumbLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemContainer.Controls.Add(breadCrumbLink);
                        isFirstCrumb = false;
                    }

                    this.PageBreadcrumbContainer.Controls.Add(breadcrumbItemContainer);
                }
            }
            else
            {
                // loop through breadcrumb items, build controls for them,
                // and add them to the breadcrumb container
                foreach (BreadcrumbLink crumb in crumbs)
                {
                    // add a delimiter if this is not the first breadcrumb item
                    if (!isFirstCrumb)
                    {
                        Panel breadcrumbDelimiterContainer = new Panel();
                        breadcrumbDelimiterContainer.CssClass = "BreadcrumbDelimiter";
                        Literal breadcrumbDelimiter = new Literal();
                        breadcrumbDelimiter.Text = breadcrumbDelimiterString;
                        breadcrumbDelimiterContainer.Controls.Add(breadcrumbDelimiter);
                        this.PageBreadcrumbContainer.Controls.Add(breadcrumbDelimiterContainer);
                    }

                    // add breadcrumb item
                    Panel breadcrumbItemContainer = new Panel();
                    breadcrumbItemContainer.CssClass = "BreadcrumbItem";

                    if (String.IsNullOrWhiteSpace(crumb.LinkHref))
                    {
                        Localize breadCrumbNoLink = new Localize();
                        breadCrumbNoLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemContainer.Controls.Add(breadCrumbNoLink);
                        isFirstCrumb = false;
                    }
                    else
                    {
                        HyperLink breadCrumbLink = new HyperLink();
                        breadCrumbLink.NavigateUrl = crumb.LinkHref;
                        breadCrumbLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemContainer.Controls.Add(breadCrumbLink);
                        isFirstCrumb = false;
                    }

                    this.PageBreadcrumbContainer.Controls.Add(breadcrumbItemContainer);
                }
            }

            // make the breadcrumb container visible
            this.PageBreadcrumbContainer.Visible = true;


            // build the breadcrumb "full drop-down" that attaches to the page title container
            // and is made visible only when screen is 480px wide or less

            this.FullDropDownBreadcrumbContainer.CssClass = "FullDropDownBreadcrumbContainer";

            // declare ULs for drop-down breadcrumbs
            HtmlGenericControl fullDropDownBreadcrumbOuterUL = new HtmlGenericControl("ul");
            fullDropDownBreadcrumbOuterUL.Attributes.Add("class", "BreadcrumbOuterUL");

            HtmlGenericControl fullDropDownBreadcrumbInnerUL = new HtmlGenericControl("ul");
            fullDropDownBreadcrumbInnerUL.ID = "FullDropDownBreadcrumbDropDownItems";
            fullDropDownBreadcrumbInnerUL.Attributes.Add("class", "BreadcrumbInnerUL");

            // build drop-down character
            HtmlGenericControl fullDropDownBreadcrumbDownArrowLI = new HtmlGenericControl("li");
            fullDropDownBreadcrumbDownArrowLI.ID = "FullDropDownBreadcrumbDownArrowLI";
            fullDropDownBreadcrumbDownArrowLI.Attributes.Add("class", "BreadcrumbDownArrowLI");

            Image expandArrowImage1 = new Image();
            expandArrowImage1.CssClass = "XSmallIcon";
            expandArrowImage1.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EXPAND_ARROW, ImageFiles.EXT_PNG);
            fullDropDownBreadcrumbDownArrowLI.Controls.Add(expandArrowImage1);

            Label elipsisString1 = new Label();
            elipsisString1.Text = "...";
            fullDropDownBreadcrumbDownArrowLI.Controls.Add(elipsisString1);

            // attach drop-down UL/LI controls
            fullDropDownBreadcrumbDownArrowLI.Controls.Add(fullDropDownBreadcrumbInnerUL);
            fullDropDownBreadcrumbOuterUL.Controls.Add(fullDropDownBreadcrumbDownArrowLI);

            // build drop-down breadcrumbs
            for (i = 0; i < crumbs.Count - 1; i++)
            {
                BreadcrumbLink crumb = (BreadcrumbLink)crumbs[i];
                HtmlGenericControl breadcrumbItemLI = new HtmlGenericControl("li");
                breadcrumbItemLI.Attributes.Add("class", "BreadcrumbItemLI");

                if (String.IsNullOrWhiteSpace(crumb.LinkHref))
                {
                    Localize breadCrumbNoLink = new Localize();
                    breadCrumbNoLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                    breadcrumbItemLI.Controls.Add(breadCrumbNoLink);
                }
                else
                {
                    HyperLink breadCrumbLink = new HyperLink();
                    breadCrumbLink.NavigateUrl = crumb.LinkHref;
                    breadCrumbLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                    breadcrumbItemLI.Controls.Add(breadCrumbLink);
                }

                // attach breadcrumb item
                fullDropDownBreadcrumbInnerUL.Controls.Add(breadcrumbItemLI);
            }

            // attach drop-down breadcrumbs to full drop down breadcrumb container and make container visible
            this.FullDropDownBreadcrumbContainer.Controls.Add(fullDropDownBreadcrumbOuterUL);
            this.FullDropDownBreadcrumbContainer.Visible = true;
        }
        #endregion

        #region PageTitleCategory ENUM
        /// <summary>
        /// Enumeration of "Administrator Menu" categories fo use in applying color-coding 
        /// CSS classes to page title icon containers.
        /// </summary>
        public enum PageCategoryForTitle
        {
            General = 0,
            System = 1,
            UsersAndGroups = 2,
            InterfaceAndLayout = 3,
            LearningAssets = 4,
            Reporting = 5,
        }
        #endregion

        #region BuildPageTitle
        /// <summary>
        /// Builds a page title and adds it to its container.
        /// </summary>
        /// <param name="title">title of the page</param>
        /// <param name="titleImagePath">path to image shown next to title</param>
        public void BuildPageTitle(PageCategoryForTitle category, string title, string titleImagePath, string imageCssClass = null)
        {
            // css class for page title
            this.PageTitleContainer.CssClass = "xd-12 xm-12 PageTitleContainer";

            // clear container
            this.PageTitleContainer.Controls.Clear();

            // DO PAGE TITLE

            HtmlGenericControl titleWrapper = new HtmlGenericControl("p");
            Localize titleText = new Localize();

            if (!String.IsNullOrWhiteSpace(titleImagePath))
            {
                Panel titleIconContainer = new Panel();
                titleIconContainer.ID = "PageTitleIconContainer";
                titleIconContainer.CssClass = "PageTitleIconContainer";

                // apply category sub-class to title icon container
                switch (category)
                {
                    case PageCategoryForTitle.General:
                        titleIconContainer.CssClass += " GeneralCategoryColor";
                        this.PageCategory = "General";
                        break;
                    case PageCategoryForTitle.System:
                        titleIconContainer.CssClass += " SystemCategoryColor";
                        this.PageCategory = "System";
                        break;
                    case PageCategoryForTitle.UsersAndGroups:
                        titleIconContainer.CssClass += " UsersAndGroupsCategoryColor";
                        this.PageCategory = "UsersAndGroups";
                        break;
                    case PageCategoryForTitle.InterfaceAndLayout:
                        titleIconContainer.CssClass += " InterfaceAndLayoutCategoryColor";
                        this.PageCategory = "InterfaceAndLayout";
                        break;
                    case PageCategoryForTitle.LearningAssets:
                        titleIconContainer.CssClass += " LearningAssetsCategoryColor";
                        this.PageCategory = "LearningAssets";
                        break;
                    case PageCategoryForTitle.Reporting:
                        titleIconContainer.CssClass += " ReportingCategoryColor";
                        this.PageCategory = "Reporting";
                        break;
                    default:
                        break;
                }

                Image titleImage = new Image();
                titleImage.ImageUrl = titleImagePath;
                titleImage.AlternateText = title;
                titleImage.CssClass = "PageTitleIcon";

                if (imageCssClass != null)
                {
                    titleImage.CssClass += " " + imageCssClass;

                    // if the image gets class "AvatarImage", give the container "Avatar" as a class
                    if (imageCssClass.Contains("AvatarImage"))
                    { titleIconContainer.CssClass += " Avatar"; }
                }

                titleIconContainer.Controls.Add(titleImage);

                this.PageTitleContainer.Controls.Add(titleIconContainer);
            }

            titleText.Text = Server.HtmlEncode(title);
            titleWrapper.Controls.Add(titleText);

            // add title to container and make container visible
            this.PageTitleContainer.Controls.Add(titleWrapper);
            this.PageTitleContainer.Visible = true;
        }
        #endregion

        #region BuildPageTitle
        /// <summary>
        /// Overridden method to Build a page title with 2 levels and add it to its container.
        /// </summary>
        /// <param name="level1Title">level 1 title of the page</param>
        /// <param name="level1TitleImagePath">path to image shown next to level 1 title</param>
        /// <param name="level2Title">level 2 title of the page</param>
        /// <param name="level2TitleImagePath">path to image shown next to level 2 title</param>
        /// <param name="level1ImageCssClass">the additional css class to apply to the level 1 title image</param>
        /// <param name="level2ImageCssClass">the additional css class to apply to the level 2 title image</param>
        public void BuildPageTitle(PageCategoryForTitle category, string level1Title, string level1TitleImagePath, string level2Title, string level2TitleImagePath, string level1ImageCssClass = null, string level2ImageCssClass = null)
        {
            // css class for page title
            this.PageTitleContainer.CssClass = "xd-12 xm-12";

            // clear container
            this.PageTitleContainer.Controls.Clear();

            // DO LEVEL 1 TITLE

            Panel level1TitleContainer = new Panel();
            level1TitleContainer.ID = "Level1PageTitleContainer";
            level1TitleContainer.CssClass = "PageTitleContainer Level1PageTitle";

            HtmlGenericControl level1TitleWrapper = new HtmlGenericControl("p");
            Localize level1TitleText = new Localize();

            if (!String.IsNullOrWhiteSpace(level1TitleImagePath))
            {
                Panel level1TitleIconContainer = new Panel();
                level1TitleIconContainer.ID = "Level1PageTitleIconContainer";
                level1TitleIconContainer.CssClass = "PageTitleIconContainer";

                // apply category sub-class to title icon container
                switch (category)
                {
                    case PageCategoryForTitle.General:
                        level1TitleIconContainer.CssClass += " GeneralCategoryColor";
                        this.PageCategory = "General";
                        break;
                    case PageCategoryForTitle.System:
                        level1TitleIconContainer.CssClass += " SystemCategoryColor";
                        this.PageCategory = "System";
                        break;
                    case PageCategoryForTitle.UsersAndGroups:
                        level1TitleIconContainer.CssClass += " UsersAndGroupsCategoryColor";
                        this.PageCategory = "UsersAndGroups";
                        break;
                    case PageCategoryForTitle.InterfaceAndLayout:
                        level1TitleIconContainer.CssClass += " InterfaceAndLayoutCategoryColor";
                        this.PageCategory = "InterfaceAndLayout";
                        break;
                    case PageCategoryForTitle.LearningAssets:
                        level1TitleIconContainer.CssClass += " LearningAssetsCategoryColor";
                        this.PageCategory = "LearningAssets";
                        break;
                    case PageCategoryForTitle.Reporting:
                        level1TitleIconContainer.CssClass += " ReportingCategoryColor";
                        this.PageCategory = "Reporting";
                        break;
                    default:
                        break;
                }

                Image level1TitleImage = new Image();
                level1TitleImage.ImageUrl = level1TitleImagePath;
                level1TitleImage.AlternateText = level1Title;
                level1TitleImage.CssClass = "PageTitleIcon";

                if (level1ImageCssClass != null)
                {
                    level1TitleImage.CssClass += " " + level1ImageCssClass;

                    // if the image gets class "AvatarImage", give the container "Avatar" as a class
                    if (level1ImageCssClass.Contains("AvatarImage"))
                    { level1TitleIconContainer.CssClass += " Avatar"; }
                }

                level1TitleIconContainer.Controls.Add(level1TitleImage);

                level1TitleContainer.Controls.Add(level1TitleIconContainer);
            }

            level1TitleText.Text = Server.HtmlEncode(level1Title);
            level1TitleWrapper.Controls.Add(level1TitleText);

            level1TitleContainer.Controls.Add(level1TitleWrapper);

            // DO LEVEL 2 TITLE

            Panel level2TitleContainer = new Panel();
            level2TitleContainer.ID = "Level2PageTitleContainer";
            level2TitleContainer.CssClass = "PageTitleContainer Level2PageTitle";

            HtmlGenericControl level2TitleWrapper = new HtmlGenericControl("p");
            Localize level2TitleText = new Localize();

            if (!String.IsNullOrWhiteSpace(level2TitleImagePath))
            {
                Panel level2TitleIconContainer = new Panel();
                level2TitleIconContainer.ID = "Level2PageTitleIconContainer";
                level2TitleIconContainer.CssClass = "PageTitleIconContainer";

                // apply category sub-class to title icon container
                switch (category)
                {
                    case PageCategoryForTitle.General:
                        level2TitleIconContainer.CssClass += " GeneralCategoryColor";
                        this.PageCategory = "General";
                        break;
                    case PageCategoryForTitle.System:
                        level2TitleIconContainer.CssClass += " SystemCategoryColor";
                        this.PageCategory = "System";
                        break;
                    case PageCategoryForTitle.UsersAndGroups:
                        level2TitleIconContainer.CssClass += " UsersAndGroupsCategoryColor";
                        this.PageCategory = "UsersAndGroups";
                        break;
                    case PageCategoryForTitle.InterfaceAndLayout:
                        level2TitleIconContainer.CssClass += " InterfaceAndLayoutCategoryColor";
                        this.PageCategory = "InterfaceAndLayout";
                        break;
                    case PageCategoryForTitle.LearningAssets:
                        level2TitleIconContainer.CssClass += " LearningAssetsCategoryColor";
                        this.PageCategory = "LearningAssets";
                        break;
                    case PageCategoryForTitle.Reporting:
                        level2TitleIconContainer.CssClass += " ReportingCategoryColor";
                        this.PageCategory = "Reporting";
                        break;
                    default:
                        break;
                }

                Image level2TitleImage = new Image();
                level2TitleImage.ImageUrl = level2TitleImagePath;
                level2TitleImage.AlternateText = level2Title;
                level2TitleImage.CssClass = "PageTitleIcon";

                if (level2ImageCssClass != null)
                {
                    level2TitleImage.CssClass += " " + level2ImageCssClass;

                    // if the image gets class "AvatarImage", give the container "Avatar" as a class
                    if (level2ImageCssClass.Contains("AvatarImage"))
                    { level2TitleIconContainer.CssClass += " Avatar"; }
                }

                level2TitleIconContainer.Controls.Add(level2TitleImage);

                level2TitleContainer.Controls.Add(level2TitleIconContainer);
            }

            level2TitleText.Text = Server.HtmlEncode(level2Title);
            level2TitleWrapper.Controls.Add(level2TitleText);

            level2TitleContainer.Controls.Add(level2TitleWrapper);

            // add titles to container and make container visible
            this.PageTitleContainer.Controls.Add(level1TitleContainer);
            this.PageTitleContainer.Controls.Add(level2TitleContainer);
            this.PageTitleContainer.Visible = true;
        }
        #endregion

        #region HideNavigationAndLoginControls
        /// <summary>
        /// Hides the navigation and login controls from the masthead.
        /// Used for when we want to invoke an Asentia page, but do not want it to have "normal"
        /// application functionality, example: the report subscription unsubscribe page.
        /// Notes: 
        /// - This needs to be called from the calling page's OnPreRender method in order to 
        ///   function properly.
        /// - If the control(s) cannot be found, we will not throw an exception, the function
        ///   will just do nothing.
        /// </summary>
        public void HideNavigationAndLoginControls()
        {
            // get all supporting controls, we need to navigate through the stack because .net does
            // not do FindControl recursively
            Control masterForm = this.Master.FindControl("MasterForm");
            Control pageContainer = masterForm.FindControl("PageContainer");
            Control mastheadContainer = pageContainer.FindControl("MastheadContainer");
            Control mastheadColumnLeft = mastheadContainer.FindControl("MastheadColumnLeft");
            Control mastheadColumnRight = mastheadContainer.FindControl("MastheadColumnRight");

            // verify that we have found all of the supporting controls, if any of them are null, die quietly
            if (masterForm == null || pageContainer == null || mastheadContainer == null || mastheadColumnLeft == null || mastheadColumnRight == null)
            { return; }

            // find and hide the main navigation control - if not left, then look right
            Control mainNavigationControl = null;

            mainNavigationControl = mastheadColumnLeft.FindControl("MainNavigationControl");
            if (mainNavigationControl != null)
            { mainNavigationControl.Visible = false; }

            mainNavigationControl = mastheadColumnRight.FindControl("MainNavigationControl");
            if (mainNavigationControl != null)
            { mainNavigationControl.Visible = false; }

            // find and hide the login control - if not left, then look right
            Control settingsControlForLogin = null;

            settingsControlForLogin = mastheadColumnLeft.FindControl("SettingsControlForLogin");
            if (settingsControlForLogin != null)
            { settingsControlForLogin.Visible = false; }

            settingsControlForLogin = mastheadColumnRight.FindControl("SettingsControlForLogin");
            if (settingsControlForLogin != null)
            { settingsControlForLogin.Visible = false; }
        }
        #endregion

        #region ClearContainer
        /// <summary>
        /// Clears a specified container of classes and controls, and makes the container not visible.
        /// Used mostly for clearing "feedback" containers.
        /// </summary>
        public void ClearContainer(Panel container)
        {
            container.CssClass = "";
            container.Controls.Clear();
            container.Visible = false;
        }
        #endregion

        #region BuildFormField
        /// <summary>
        /// Builds a form field with all appropriate containers and controls.
        /// </summary>
        /// <param name="fieldIdentifier">the field identifier, used as a prefix for IDs of controls created within this method</param>
        /// <param name="fieldLabel">the label for the field</param>
        /// <param name="labelControlId">the control id the label is for, allows the label to be clicked and the input control with specified id to gain focus</param>
        /// <param name="inputControl">the input control</param>
        /// <param name="isRequired">is this field required?</param>
        /// <param name="buildErrorPanel">build an error panel for this field?</param>
        /// <param name="buildLanguageSpecificInputControls">build language specific controls for this field?</param>
        /// <returns>a Panel formatted with all controls that make up a form field</returns>
        public static Panel BuildFormField(string fieldIdentifier, string fieldLabel, string labelControlId, Control inputControl, bool isRequired, bool buildErrorPanel, bool buildLanguageSpecificInputControls)
        {
            // form field container
            Panel formFieldContainer = new Panel();
            formFieldContainer.ID = fieldIdentifier + "_Container";
            formFieldContainer.CssClass = "FormFieldContainer";

            // form field input container
            Panel formFieldInputContainer = new Panel();
            formFieldInputContainer.ID = fieldIdentifier + "_InputContainer";
            formFieldInputContainer.CssClass = "FormFieldInputContainer";

            // attach input control
            if (inputControl != null)
            {
                // if we're building language specific input controls, do it
                if (buildLanguageSpecificInputControls)
                {
                    // create an ArrayList to hold the installed languages
                    ArrayList installedLanguages = new ArrayList();

                    // create a TextBox element to hold the inputControl so that we can use it to copy
                    // properties into our newly created controls
                    //
                    // Note that inputControl, as defined, is a Control, and that we are casting to a TextBox. 
                    // We need to do this because TextBox is the type of control we are creating, and has properties
                    // that Control does not have, and we need values for. So, we are assuming that inputControl 
                    // is in fact a TextBox, which we can safely assume because language-specific inputs can only be
                    // TextBoxes. If the inputControl passed were not a TextBox, we would get an unhandled exception
                    // here, and that would be caused by programmer error :).
                    TextBox defaultLanguageSpecificControl = (TextBox)inputControl;

                    // HANDLE THE DEFAULT LANGUAGE'S CONTROL

                    // if the control has a CKEditor, we render it inside an extra container, so do that
                    // otherwise, just add it to the input container
                    if (defaultLanguageSpecificControl.CssClass.Contains("ckeditor"))
                    {
                        Panel ckeditorControlContainerDefaultLanguage = new Panel();
                        ckeditorControlContainerDefaultLanguage.ID = fieldIdentifier + "_CKEDITORContainer";
                        ckeditorControlContainerDefaultLanguage.Controls.Add(defaultLanguageSpecificControl);

                        formFieldInputContainer.Controls.Add(ckeditorControlContainerDefaultLanguage);
                    }
                    else
                    { formFieldInputContainer.Controls.Add(defaultLanguageSpecificControl); }

                    // GET THE AVAILABLE LANGUAGES

                    // en-US is the default application language, so it's already installed, add it
                    installedLanguages.Add("en-US");

                    // loop through each installed language based on the language folders contained in bin
                    foreach (string directory in Directory.GetDirectories(HostingEnvironment.MapPath(SitePathConstants.BIN)))
                    {
                        // loop through each language available to the site and add it to the array list for drop-down items
                        foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
                        {
                            if (language == Path.GetFileName(directory))
                            {
                                installedLanguages.Add(Path.GetFileName(directory));
                                break;
                            }
                        }
                    }

                    // BUILD LANGUAGE-SPECIFIC CONTROLS FOR ALL OTHER LANGUAGES (EXCLUDING DEFAULT LANGUAGE)

                    foreach (string installedLanguage in installedLanguages)
                    {
                        // get the culture info for the language
                        CultureInfo cultureInfo = CultureInfo.GetCultureInfo(installedLanguage);

                        // if the language is not the site's default, build a control for it based off of
                        // properties that belong to the default language's control
                        if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                        {
                            // declare a new TextBox control and set properties based off of the default, with the
                            // exception of ID, that gets set for the language we're creating this for
                            TextBox languageSpecificInputControl = new TextBox();
                            languageSpecificInputControl.ID = defaultLanguageSpecificControl.ID + "_" + cultureInfo.Name;
                            languageSpecificInputControl.CssClass = defaultLanguageSpecificControl.CssClass;
                            languageSpecificInputControl.TextMode = defaultLanguageSpecificControl.TextMode;
                            languageSpecificInputControl.Rows = defaultLanguageSpecificControl.Rows;

                            // "Style" is a collection, so it needs to be looped and attached
                            foreach (string key in defaultLanguageSpecificControl.Style.Keys)
                            { languageSpecificInputControl.Style.Add(key, defaultLanguageSpecificControl.Style[key]); }

                            // if the control has a CKEditor, we render it inside an extra container, so do that
                            // otherwise, just add it to the input container
                            if (languageSpecificInputControl.CssClass.Contains("ckeditor"))
                            {
                                Panel ckeditorControlContainerLanguage = new Panel();
                                ckeditorControlContainerLanguage.ID = fieldIdentifier + "_CKEDITORContainer" + "_" + cultureInfo.Name;
                                ckeditorControlContainerLanguage.Style.Add("display", "none"); // make it hidden by default
                                ckeditorControlContainerLanguage.Controls.Add(languageSpecificInputControl);

                                formFieldInputContainer.Controls.Add(ckeditorControlContainerLanguage);
                            }
                            else
                            {
                                languageSpecificInputControl.Style.Add("display", "none"); // make it hidden by default
                                formFieldInputContainer.Controls.Add(languageSpecificInputControl);
                            }
                        }
                    }

                    // LANGUAGE SELECTOR CONTROL

                    // declare and attach language selector
                    LanguageSelectorForFormField fieldLanguageSelector = new LanguageSelectorForFormField(inputControl.ID, defaultLanguageSpecificControl.CssClass.Contains("ckeditor"));
                    fieldLanguageSelector.ID = defaultLanguageSpecificControl.ID + "_LanguageSelector";

                    // if the language-specific control is a MultiLine, add "Above" class and attach
                    // it at the beginning of the input container; otherwise, add "Inline" class and
                    // attach it at the end
                    if (defaultLanguageSpecificControl.TextMode == TextBoxMode.MultiLine)
                    {
                        fieldLanguageSelector.CssClass += " FormFieldLanguageSelectorAboveField";
                        formFieldInputContainer.Controls.AddAt(0, fieldLanguageSelector);
                    }
                    else
                    {
                        fieldLanguageSelector.CssClass += " FormFieldLanguageSelectorInlineField";
                        formFieldInputContainer.Controls.Add(fieldLanguageSelector);

                        // apply InputWithLanguageSelector class to the input container
                        formFieldInputContainer.CssClass += " InputWithLanguageSelector";
                    }
                }
                else // just attach non-language-specific control
                { formFieldInputContainer.Controls.Add(inputControl); }
            }

            if (!String.IsNullOrWhiteSpace(fieldLabel))
            {
                // form field label container
                Panel formFieldLabelContainer = new Panel();
                formFieldLabelContainer.ID = fieldIdentifier + "_LabelContainer";
                formFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                // form field label
                Label formFieldLabel = new Label();
                formFieldLabel.Text = fieldLabel + ": ";
                formFieldLabel.AssociatedControlID = labelControlId;

                // attach the form field label
                formFieldLabelContainer.Controls.Add(formFieldLabel);

                // if this field is required, build and attach a required asterisk
                if (isRequired)
                {
                    Label formFieldRequiredAsterisk = new Label();
                    formFieldRequiredAsterisk.Text = " * ";
                    formFieldRequiredAsterisk.CssClass = "RequiredAsterisk";
                    formFieldLabelContainer.Controls.Add(formFieldRequiredAsterisk);
                }

                // add label to form field container
                formFieldContainer.Controls.Add(formFieldLabelContainer);
            }

            // if we are to build the error panel, build and attach it
            if (buildErrorPanel)
            {
                Panel formFieldErrorContainer = new Panel();
                formFieldErrorContainer.ID = fieldIdentifier + "_ErrorContainer";
                formFieldErrorContainer.CssClass = "FormFieldErrorContainer";
                formFieldContainer.Controls.Add(formFieldErrorContainer);
            }

            // attach the input container - this is done last
            formFieldContainer.Controls.Add(formFieldInputContainer);

            // return form field
            return formFieldContainer;
        }
        #endregion

        #region BuildMultipleInputControlFormField
        /// <summary>
        /// Builds a form field with all appropriate containers and controls that uses multiple input controls.
        /// 
        /// Note that the List of "inputControls" are added to the field's "InputContainer" are done so in order
        /// and can be actual input controls (TextBox, CheckBox, etc.) and/or "static" controls (Label, Localize, etc.).
        /// This is so that we can easily build inputs that render like "[   ] hour and [   ] minutes".
        /// 
        /// This is not used for language-specific fields. Only "BuildFormField" does that.
        /// </summary>
        /// <param name="fieldIdentifier">the field identifier, used as a prefix for IDs of controls created within this method</param>
        /// <param name="fieldLabel">the label for the field</param>
        /// <param name="inputControls">a list of "input" control to be placed in the input container</param>
        /// <param name="isRequired">is this field required?</param>
        /// <param name="buildErrorPanel">build an error panel for this field?</param>
        /// <returns>a Panel formatted with all controls that make up a form field with multiple input controls</returns>
        public static Panel BuildMultipleInputControlFormField(string fieldIdentifier, string fieldLabel, List<Control> inputControls, bool isRequired, bool buildErrorPanel, bool separateControlsWithPanels = false)
        {
            // form field container
            Panel formFieldContainer = new Panel();
            formFieldContainer.ID = fieldIdentifier + "_Container";
            formFieldContainer.CssClass = "FormFieldContainer";

            // form field input container
            Panel formFieldInputContainer = new Panel();
            formFieldInputContainer.ID = fieldIdentifier + "_InputContainer";
            formFieldInputContainer.CssClass = "FormFieldInputContainer";

            // attach input controls
            if (inputControls != null && inputControls.Count > 0)
            {
                // attach all controls in the control list in order
                foreach (Control control in inputControls)
                {
                    if (separateControlsWithPanels)
                    {
                        Panel separatorPanel = new Panel();
                        separatorPanel.ID = control.ID + "_Container";
                        separatorPanel.Controls.Add(control);

                        formFieldInputContainer.Controls.Add(separatorPanel);
                    }
                    else
                    { formFieldInputContainer.Controls.Add(control); }
                }
            }

            if (!String.IsNullOrWhiteSpace(fieldLabel))
            {
                // form field label container
                Panel formFieldLabelContainer = new Panel();
                formFieldLabelContainer.ID = fieldIdentifier + "_LabelContainer";
                formFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                // form field label
                Label formFieldLabel = new Label();
                formFieldLabel.Text = fieldLabel + ": ";

                // attach the form field label
                formFieldLabelContainer.Controls.Add(formFieldLabel);

                // if this field is required, build and attach a required asterisk
                if (isRequired)
                {
                    Label formFieldRequiredAsterisk = new Label();
                    formFieldRequiredAsterisk.Text = " * ";
                    formFieldRequiredAsterisk.CssClass = "RequiredAsterisk";
                    formFieldLabelContainer.Controls.Add(formFieldRequiredAsterisk);
                }

                // add label to form field container
                formFieldContainer.Controls.Add(formFieldLabelContainer);
            }

            // if we are to build the error panel, build and attach it
            if (buildErrorPanel)
            {
                Panel formFieldErrorContainer = new Panel();
                formFieldErrorContainer.ID = fieldIdentifier + "_ErrorContainer";
                formFieldErrorContainer.CssClass = "FormFieldErrorContainer";
                formFieldContainer.Controls.Add(formFieldErrorContainer);
            }

            // attach the input container - this is done last
            formFieldContainer.Controls.Add(formFieldInputContainer);

            // return form field
            return formFieldContainer;
        }
        #endregion

        #region BuildMultipleInputControlFormField
        /// <summary>
        /// Builds a form field with all appropriate containers and controls that uses multiple input controls.
        /// 
        /// Note that the List of "inputControls" are added to the field's "InputContainer" are done so in order
        /// and can be actual input controls (TextBox, CheckBox, etc.) and/or "static" controls (Label, Localize, etc.).
        /// This is so that we can easily build inputs that render like "[   ] hour and [   ] minutes".
        /// 
        /// This is not used for language-specific fields. Only "BuildFormField" does that.
        /// </summary>
        /// <param name="fieldIdentifier">the field identifier, used as a prefix for IDs of controls created within this method</param>
        /// <param name="fieldLabelControl">a control to be used as the field label, i.e. placed inside of field label container</param>
        /// <param name="inputControls">a list of "input" control to be placed in the input container</param>
        /// <param name="isRequired">is this field required?</param>
        /// <param name="buildErrorPanel">build an error panel for this field?</param>
        /// <returns>a Panel formatted with all controls that make up a form field with multiple input controls</returns>
        public static Panel BuildMultipleInputControlFormField(string fieldIdentifier, Control fieldLabelControl, List<Control> inputControls, bool isRequired, bool buildErrorPanel, bool separateControlsWithPanels = false)
        {
            // form field container
            Panel formFieldContainer = new Panel();
            formFieldContainer.ID = fieldIdentifier + "_Container";
            formFieldContainer.CssClass = "FormFieldContainer";

            // form field input container
            Panel formFieldInputContainer = new Panel();
            formFieldInputContainer.ID = fieldIdentifier + "_InputContainer";
            formFieldInputContainer.CssClass = "FormFieldInputContainer";

            // attach input controls
            if (inputControls != null && inputControls.Count > 0)
            {
                // attach all controls in the control list in order
                foreach (Control control in inputControls)
                {
                    if (separateControlsWithPanels)
                    {
                        Panel separatorPanel = new Panel();
                        separatorPanel.ID = control.ID + "_Container";
                        separatorPanel.Controls.Add(control);

                        formFieldInputContainer.Controls.Add(separatorPanel);
                    }
                    else
                    { formFieldInputContainer.Controls.Add(control); }
                }
            }

            if (fieldLabelControl != null)
            {
                // form field label container
                Panel formFieldLabelContainer = new Panel();
                formFieldLabelContainer.ID = fieldIdentifier + "_LabelContainer";
                formFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                // attach the form field label control
                formFieldLabelContainer.Controls.Add(fieldLabelControl);

                // if this field is required, build and attach a required asterisk
                if (isRequired)
                {
                    Label formFieldRequiredAsterisk = new Label();
                    formFieldRequiredAsterisk.Text = " * ";
                    formFieldRequiredAsterisk.CssClass = "RequiredAsterisk";
                    formFieldLabelContainer.Controls.Add(formFieldRequiredAsterisk);
                }

                // add label to form field container
                formFieldContainer.Controls.Add(formFieldLabelContainer);
            }

            // if we are to build the error panel, build and attach it
            if (buildErrorPanel)
            {
                Panel formFieldErrorContainer = new Panel();
                formFieldErrorContainer.ID = fieldIdentifier + "_ErrorContainer";
                formFieldErrorContainer.CssClass = "FormFieldErrorContainer";
                formFieldContainer.Controls.Add(formFieldErrorContainer);
            }

            // attach the input container - this is done last
            formFieldContainer.Controls.Add(formFieldInputContainer);

            // return form field
            return formFieldContainer;
        }
        #endregion

        #region GetArrayListOfSiteAvailableInstalledLanguages
        /// <summary>
        /// Gets all of the languages that are installed in the application and available to the site,
        /// puts them in an ArrayList, and returns it. This is used in save methods on pages where there
        /// are language-specific inputs so that we can loop through the returned ArrayList and grab values
        /// from inputs for each corresponding language.
        /// </summary>
        /// <returns>ArrayList</returns>
        public ArrayList GetArrayListOfSiteAvailableInstalledLanguages()
        {
            ArrayList languages = new ArrayList();

            // en-US is the default language, so it's already installed, add it
            languages.Add("en-US");

            // loop through each installed language based on the language folders contained in bin
            foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
            {
                // loop through each language available to the site and add it to the array list for drop-down items
                foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
                {
                    if (language == Path.GetFileName(directory))
                    {
                        languages.Add(Path.GetFileName(directory));
                        break;
                    }
                }
            }

            return languages;
        }
        #endregion

        #region DisplayFeedback
        /// <summary>
        /// Method to populate the PageFeedbackContainer container with a success, or error message.
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="isError">determines if this message is an error message</param>
        public void DisplayFeedback(string message, bool isError, bool addPopupCloseButton = true)
        {
            // clear the container
            this.PageFeedbackContainer.Controls.Clear();

            // build and attach info status panel
            InformationStatusPanel infoStatusPanel;

            if (isError)
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, InformationStatusPanel_MessageType.Error, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);
            }
            else
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, InformationStatusPanel_MessageType.Success, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);
            }

            this.PageFeedbackContainer.Controls.Add(infoStatusPanel);

            // make container visible
            this.PageFeedbackContainer.Visible = true;
        }
        
        /// <summary>
        /// Overloaded method to populate the PageFeedbackContainer container with a defined message type.
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="messageType">message type</param>
        public void DisplayFeedback(string message, InformationStatusPanel_MessageType messageType, bool addPopupCloseButton = true)
        {
            // clear the container
            this.PageFeedbackContainer.Controls.Clear();

            // build and attach info status panel
            InformationStatusPanel infoStatusPanel;

            infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, messageType, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);            

            this.PageFeedbackContainer.Controls.Add(infoStatusPanel);

            // make container visible
            this.PageFeedbackContainer.Visible = true;
        }
        #endregion

        #region DisplayFeedbackInSpecifiedContainer
        /// <summary>
        /// Method to populate a specified container with a success, or error message.
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="isError">determines if this message is an error message</param>
        public void DisplayFeedbackInSpecifiedContainer(Panel container, string message, bool isError, bool addPopupCloseButton = true)
        {
            // clear the container
            container.Controls.Clear();

            // build and attach info status panel
            InformationStatusPanel infoStatusPanel;

            if (isError)
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, InformationStatusPanel_MessageType.Error, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);
            }
            else
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, InformationStatusPanel_MessageType.Success, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);
            }

            container.Controls.Add(infoStatusPanel);

            // make container visible
            container.Visible = true;
        }
        #endregion

        #region FormatPageInformationPanel
        /// <summary>
        /// Takes a panel and a message, and formats the panel as a "PageInformation" panel with an image and the message.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatPageInformationPanel(Panel informationPanel, string message, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "PageInformationPanel PageInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "PageInformationPanel"; }

            // attach information image - only if there is not a border as the styling, because that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }
            
            // attach message to panel
            HtmlGenericControl messageP = new HtmlGenericControl("p");
            messageP.InnerText = message;
            informationPanel.Controls.Add(messageP);
        }
        #endregion

        #region FormatPageInformationPanel
        /// <summary>
        /// Takes a panel and a List of messages, and formats the panel as a "PageInformation"
        /// panel with an image and the messages.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatPageInformationPanel(Panel informationPanel, List<string> messages, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "PageInformationPanel PageInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "PageInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach messages to panel
            foreach (string message in messages)
            {
                HtmlGenericControl messageP = new HtmlGenericControl("p");
                messageP.InnerText = message;
                informationPanel.Controls.Add(messageP);
            }
        }
        #endregion

        #region FormatSectionInformationPanel
        /// <summary>
        /// Takes a panel and a message, and formats the panel as a "SectionInformation"
        /// panel with an image and the message.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatSectionInformationPanel(Panel informationPanel, string message, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "SectionInformationPanel SectionInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "SectionInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach message to panel
            HtmlGenericControl messageP = new HtmlGenericControl("p");
            messageP.InnerText = message;
            informationPanel.Controls.Add(messageP);
        }
        #endregion

        #region FormatSectionInformationPanel
        /// <summary>
        /// Takes a panel and a List of messages, and formats the panel as a "SectionInformation"
        /// panel with an image and the messages.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatSectionInformationPanel(Panel informationPanel, List<string> messages, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "SectionInformationPanel SectionInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "SectionInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach messages to panel
            foreach (string message in messages)
            {
                HtmlGenericControl messageP = new HtmlGenericControl("p");
                messageP.InnerText = message;
                informationPanel.Controls.Add(messageP);
            }
        }
        #endregion

        #region FormatFormInformationPanel
        /// <summary>
        /// Takes a panel and a message, and formats the panel as a "FormInformation"
        /// panel with an image and the message.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatFormInformationPanel(Panel informationPanel, string message, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "FormInformationPanel FormInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "FormInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach message to panel
            HtmlGenericControl messageP = new HtmlGenericControl("p");
            messageP.InnerText = message;
            informationPanel.Controls.Add(messageP);
        }
        #endregion

        #region FormatFormInformationPanel
        /// <summary>
        /// Takes a panel and a List of messages, and formats the panel as a "FormInformation"
        /// panel with an image and the messages.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatFormInformationPanel(Panel informationPanel, List<string> messages, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "FormInformationPanel FormInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "FormInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach messages to panel
            foreach (string message in messages)
            {
                HtmlGenericControl messageP = new HtmlGenericControl("p");
                messageP.InnerText = message;
                informationPanel.Controls.Add(messageP);
            }
        }
        #endregion

        #region BuildTabListPanel
        /// <summary>
        /// Builds a panel consisiting of an unordered list consisting of list items for tabs. The panel can either be 
        /// built and returned, or an existing panel can be passed for the list to be rendered into. 
        /// </summary>
        /// <param name="id">the id prefix of all controls this method builds</param>
        /// <param name="listItems">a queue consisting of list items used for tabs, we use a queue because it's FIFO</param>
        /// <param name="targetPanel">optional, the panel to render the list into</param>
        /// <param name="pageInstance">optional, the instance of the calling page, used to attach controls that retain tab state on postback</param>
        /// <param name="tabChangeCallbackJSFunction">
        ///     optional, a "page embedded" JS function to call when tabs are changed; used when we might need to do page-specific things
        ///     
        ///     notes:
        ///         - the string value passed should just be the name of the function, not the call, i.e. "function" not "function();"
        ///         - when writing a page-specific function for callback, be sure that it accepts the following arguments
        ///             selectedTabId
        ///             tabContainerId
        ///         - this method is called in conjunction with the tab change function, so that functionality does not need to be in the 
        ///           page-specific function
        /// </param>
        /// <returns>Panel or null</returns>
        public static Panel BuildTabListPanel(string id, Queue<KeyValuePair<string, string>> listItems, Panel targetPanel = null, Page pageInstance = null, string tabChangeCallbackJSFunction = null)
        {
            // container panel
            Panel tabsPanel = new Panel();

            if (targetPanel != null)
            { tabsPanel = targetPanel; }
            else
            { tabsPanel.ID = id + "_TabsPanel"; }

            // set class on the tabs container
            tabsPanel.CssClass = "TabsContainer";

            // DIV for responsive label and pull-down
            Panel responsiveLabel = new Panel();
            responsiveLabel.ID = id + "_TabsResponsiveLabel";
            responsiveLabel.CssClass = "TabsResponsiveLabel";
            responsiveLabel.Attributes.Add("onclick", "Helper.ResponsiveLabelClick('" + id + "')");
            tabsPanel.Controls.Add(responsiveLabel);

            Label selectedTabTitle = new Label(); // the value of this will be the first item pulled off the tabs queue
            responsiveLabel.Controls.Add(selectedTabTitle);

            Button responsivePullDownTrigger = new Button();
            responsivePullDownTrigger.ID = id + "_TabsResponsivePullDownTrigger";
            responsivePullDownTrigger.CssClass = "TabsResponsivePullDownTrigger";
            responsivePullDownTrigger.Attributes.Add("onclick", "return false;");
            responsiveLabel.Controls.Add(responsivePullDownTrigger);            

            // UL for tabs
            HtmlGenericControl tabsUL = new HtmlGenericControl("ul");
            tabsUL.ID = id + "_TabsUL";
            tabsUL.Attributes.Add("class", "TabbedList HideTabsUL");
            tabsPanel.Controls.Add(tabsUL);

            bool isFirstElement = true;
            
            // loop through the queue and pick off the items
            while (listItems.Count > 0)
            {
                // grab the item from the queue
                KeyValuePair<string,string> kvp = listItems.Dequeue();

                // if this is the first tab, populate the selectedTabTitle text
                if (isFirstElement)
                { selectedTabTitle.Text = kvp.Value; }

                // LI
                HtmlGenericControl tabLI = new HtmlGenericControl("li");
                tabLI.ID = id + "_" + kvp.Key + "_TabLI";

                if (!String.IsNullOrWhiteSpace(tabChangeCallbackJSFunction))
                { tabLI.Attributes.Add("onclick", "Helper.ToggleTab('" + kvp.Key + "', '" + id + "'); " + tabChangeCallbackJSFunction + "('" + kvp.Key + "', '" + id + "'); "); }
                else
                { tabLI.Attributes.Add("onclick", "Helper.ToggleTab('" + kvp.Key + "', '" + id + "');"); }

                if (isFirstElement)
                { tabLI.Attributes.Add("class", "TabbedListLI TabbedListLIOn"); }
                else
                { tabLI.Attributes.Add("class", "TabbedListLI"); }

                tabsUL.Controls.Add(tabLI);

                // INNER DIV
                Panel tabDiv = new Panel();
                tabLI.Controls.Add(tabDiv);

                Literal tabDivText = new Literal();
                tabDivText.Text = kvp.Value;
                tabDiv.Controls.Add(tabDivText);

                // set first element flag to false
                isFirstElement = false;
            }

            // if a page instance was passed, find its content container, and add hidden input and start-up JS for retaining tab state on postback
            if (pageInstance != null)
            {
                // get the page content container and active tab hidden input which will not exist if this is initial page load
                Control pageContentContainer = AsentiaPage.FindControlRecursive(pageInstance, "PageContentContainer");
                Control existingActiveTabField = AsentiaPage.FindControlRecursive(pageInstance, id + "_ActiveTab");

                // if the page container does exist and the active tab hidden input does NOT exist, attach the hidden input and start-up JS
                if (pageContentContainer != null && existingActiveTabField == null)
                {                   
                    // hidden input
                    HiddenField activeTabField = new HiddenField();
                    activeTabField.ID = id + "_ActiveTab";
                    pageContentContainer.Controls.Add(activeTabField);

                    // start-up JS
                    if (!String.IsNullOrWhiteSpace(tabChangeCallbackJSFunction))
                    { ScriptManager.RegisterStartupScript(pageContentContainer, typeof(ClientScript), id + "_TabStartup", "Helper.SelectActiveTab('" + id + "', '" + tabChangeCallbackJSFunction + "');", true); }
                    else
                    { ScriptManager.RegisterStartupScript(pageContentContainer, typeof(ClientScript), id + "_TabStartup", "Helper.SelectActiveTab('" + id + "', null);", true); }
                }
            }

            // return
            // if there is a target panel, we return null because the panel is already in the control stack
            if (targetPanel != null)
            { return null; }
            else
            { return tabsPanel; }
        }
        #endregion

        #region FormatCollapsableSubHeaderPanel
        /// <summary>
        /// 
        /// </summary>
        public static void FormatCollapsableSubHeaderPanel(Panel informationPanel, string headerText, string panelIdToCollapse)
        {
            informationPanel.Controls.Clear();

            informationPanel.CssClass = "SubItemHeaderContainer";

            // attach header text
            HtmlGenericControl headerTextP = new HtmlGenericControl("p");
            headerTextP.InnerText = headerText;

            // attach expand/collapse images
            string expandImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_EXPAND,
                                                            ImageFiles.EXT_PNG);
            string collapseImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_COLLAPSE,
                                                              ImageFiles.EXT_PNG);

            Image expandCollapse = new Image();
            expandCollapse.ID = panelIdToCollapse + "ExpandCollapseIcon";
            expandCollapse.ImageUrl = collapseImagePath;
            expandCollapse.CssClass = "XSmallIcon";
            expandCollapse.Attributes.Add("onClick", "Helper.ShowHideContainer(this.id, '" + panelIdToCollapse + "', '" + expandImagePath + "', '" + collapseImagePath + "');");


            // attach controls to panel
            informationPanel.Controls.Add(headerTextP);
            informationPanel.Controls.Add(expandCollapse);
        }
        #endregion

        #region FormatSubHeaderPanel
        /// <summary>
        /// 
        /// </summary>
        public static void FormatSubHeaderPanel(Panel informationPanel, string headerText)
        {
            informationPanel.Controls.Clear();

            informationPanel.CssClass = "SubItemHeaderContainer";

            // attach header text
            HtmlGenericControl headerTextP = new HtmlGenericControl("p");
            headerTextP.InnerText = headerText;

            // attach controls to panel
            informationPanel.Controls.Add(headerTextP);
        }
        #endregion

        #region ApplyErrorMessageToFieldErrorPanel
        /// <summary>
        /// Adds an error message to the error panel and sets the ErrorOn class for an input field.
        /// </summary>
        /// <param name="fieldIdentifier">the field identifier</param>
        /// <param name="errorMessage">the error message to be displayed</param>
        public void ApplyErrorMessageToFieldErrorPanel(Panel container, string fieldIdentifier, string errorMessage)
        {
            // get the container and error panels
            Panel containerPanel = (Panel)FindControlRecursive(container, fieldIdentifier + "_Container");
            Panel errorPanel = (Panel)FindControlRecursive(container, fieldIdentifier + "_ErrorContainer");

            // if we found the container and error panels, apply the class 
            // to the container panel and add the message to the error panel
            if (containerPanel != null && errorPanel != null)
            {
                // container class
                if (!containerPanel.CssClass.Contains("ErrorOn"))
                { containerPanel.CssClass += " ErrorOn"; }

                // error
                HtmlGenericControl errorP = new HtmlGenericControl("p");
                errorP.InnerHtml = errorMessage;
                errorPanel.Controls.Add(errorP);
            }
        }
        #endregion

        #region ClearErrorClassFromFieldsRecursive
        /// <summary>
        /// Recursively finds all controls from a parent and clears the ErrorOn class.
        /// The controls that would have ErrorOn class applied are burried within other controls.
        /// Therefore we need to go through them recursively.
        /// </summary>
        /// <param name="root">the root control to start in</param>
        /// <returns></returns>
        public static void ClearErrorClassFromFieldsRecursive(Control root)
        {
            if (root.GetType() == typeof(Panel))
            {
                Panel rootPanel = (Panel)root;

                if (rootPanel == null)
                { return; }

                if (rootPanel.CssClass.Contains("ErrorOn"))
                { rootPanel.CssClass = rootPanel.CssClass.Replace("ErrorOn", ""); }
            }

            foreach (Control c in root.Controls)
            { ClearErrorClassFromFieldsRecursive(c); }
        }
        #endregion

        #region ApplyErrorImageAndClassToTab
        /// <summary>
        /// Adds an error image and error class to a tab.
        /// </summary>
        /// <param name="tabIdentifier">the tab identifier</param>
        public void ApplyErrorImageAndClassToTab(Panel container, string tabIdentifier)
        {
            // find the tab li element and set the error css
            HtmlGenericControl tabLI = (HtmlGenericControl)container.FindControl(tabIdentifier);

            if (tabLI != null)
            {
                // instansiate a string builder
                Image errorImage = new Image();
                errorImage.CssClass = "SmallIcon";

                errorImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG);
                tabLI.Controls.Add(errorImage);

                // instansiate a string builder
                StringBuilder sb = new StringBuilder();

                // beginning of function
                sb.AppendLine("$(\"#" + tabLI.ID + "\").addClass(\"TabbedListLIError\");");
                sb.AppendLine("$(\".TabbedListLIError img\").show();");

                // register a script to add the error class to the tab
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Toggle" + tabLI.ID, sb.ToString(), true);
            }
        }
        #endregion

        #region BuildOptionsPanelImageLink
        /// <summary>
        /// Builds a Panel containing a HyperLink with an Image and overlay Image (if specified).
        /// </summary>
        /// <param name="id">identifier</param>
        /// <param name="linkButton">LinkButton control to use for this link, if specified</param>
        /// <param name="href">link url</param>
        /// <param name="onclick">onclick action</param>
        /// <param name="linkText">link text</param>
        /// <param name="hrefTarget">target</param>
        /// <param name="imagePath">path to link image</param>
        /// <param name="overlayPath">path to link image overlay</param>
        /// <returns>Panel</returns>
        public Panel BuildOptionsPanelImageLink(string id, LinkButton linkButton, string href, string onclick, string linkText, string hrefTarget, string imagePath, string overlayPath = null)
        {
            // container panel
            Panel optionsPanelLinkContainer = new Panel();
            optionsPanelLinkContainer.ID = id;
            optionsPanelLinkContainer.CssClass = "OptionsPanelLinkContainer";

            // image
            Panel optionsPanelLinkIconContainer = new Panel();
            optionsPanelLinkIconContainer.CssClass = "OptionsPanelLinkIconContainer";

            Image mainIcon = new Image();
            mainIcon.CssClass = "OptionsPanelLinkMainIcon"; // MediumIcon
            mainIcon.ImageUrl = imagePath;
            mainIcon.AlternateText = linkText;
            optionsPanelLinkIconContainer.Controls.Add(mainIcon);

            // overlay
            Image overlayIcon = new Image();
            overlayIcon.CssClass = "OptionsPanelLinkOverlayIcon"; // XSmallIcon

            if (!String.IsNullOrWhiteSpace(overlayPath))
            { overlayIcon.ImageUrl = overlayPath; }
            else
            { overlayIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_BLANK, ImageFiles.EXT_PNG); }

            optionsPanelLinkIconContainer.Controls.Add(overlayIcon);

            // label
            Panel optionsPanelLinkLabelContainer = new Panel();
            optionsPanelLinkLabelContainer.CssClass = "OptionsPanelLinkLabelContainer";

            Literal optionsPanelLinkLabel = new Literal();
            optionsPanelLinkLabel.Text = linkText;

            optionsPanelLinkLabelContainer.Controls.Add(optionsPanelLinkLabel);

            // if linkButton is not null, use that, otherwise create and use a HyperLink
            if (linkButton != null)
            {
                linkButton.CssClass = "OptionsPanelLink";

                if (!String.IsNullOrWhiteSpace(onclick))
                { linkButton.OnClientClick = onclick; }

                // attach controls to containers
                linkButton.Controls.Add(optionsPanelLinkIconContainer);
                linkButton.Controls.Add(optionsPanelLinkLabelContainer);
                optionsPanelLinkContainer.Controls.Add(linkButton);
            }
            else
            {
                // hyperlink
                HyperLink optionsPanelLink = new HyperLink();
                optionsPanelLink.ID = id + "_Link";
                optionsPanelLink.CssClass = "OptionsPanelLink";

                if (!String.IsNullOrWhiteSpace(href))
                { optionsPanelLink.NavigateUrl = href; }

                if (!String.IsNullOrWhiteSpace(hrefTarget))
                { optionsPanelLink.Target = hrefTarget; }

                if (!String.IsNullOrWhiteSpace(onclick))
                { optionsPanelLink.Attributes.Add("onclick", onclick); }

                // attach controls to containers
                optionsPanelLink.Controls.Add(optionsPanelLinkIconContainer);
                optionsPanelLink.Controls.Add(optionsPanelLinkLabelContainer);
                optionsPanelLinkContainer.Controls.Add(optionsPanelLink);
            }

            return optionsPanelLinkContainer;
        }
        #endregion

        #region QueryStringBool
        public bool QueryStringBool(string queryString, bool defaultValue)
        {
            bool returnValue = QueryStringBool(queryString);

            if (!returnValue)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public bool QueryStringBool(string queryString)
        {
            if (Request.QueryString[queryString] == null)
                return false;
            else
            {
                try
                {
                    return Convert.ToBoolean(Request.QueryString[queryString]);
                }
                catch
                {
                    return false;
                }
            }
        }
        #endregion

        #region QueryStringInt
        public int QueryStringInt(string queryString, int defaultValue)
        {
            int returnValue = QueryStringInt(queryString);

            if (returnValue == 0)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public int QueryStringInt(string queryString)
        {
            if (Request.QueryString[queryString] == null)
                return 0;
            else
            {
                try
                {
                    return Convert.ToInt32(Request.QueryString[queryString]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region QueryStringString
        public string QueryStringString(string queryString, string defaultValue)
        {
            string returnValue = QueryStringString(queryString);

            if (returnValue == null)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public string QueryStringString(string queryString)
        {
            if (Request.QueryString[queryString] == null)
                return null;
            else
            {
                try
                {
                    return Request.QueryString[queryString].ToString();
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        #region QueryStringDate
        public DateTime QueryStringDate(string queryString, DateTime defaultValue)
        {
            DateTime returnValue = QueryStringDate(queryString);

            if (returnValue == DateTime.MinValue)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public DateTime QueryStringDate(string queryString)
        {
            if (Request.QueryString[queryString] == null)
                return DateTime.MinValue;
            else
            {
                try
                {
                    return DateTime.Parse(Request.QueryString[queryString].ToString());
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
        }
        #endregion

        #region ViewStateBool
        public bool ViewStateBool(StateBag viewState, string key, bool defaultValue)
        {
            bool returnValue = ViewStateBool(viewState, key);

            if (!returnValue)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public bool ViewStateBool(StateBag viewState, string key)
        {
            if (viewState[key] == null)
                return false;
            else
            {
                try
                {
                    return Convert.ToBoolean(viewState[key]);
                }
                catch
                {
                    return false;
                }
            }
        }
        #endregion

        #region ViewStateInt
        public int ViewStateInt(StateBag viewState, string key, int defaultValue)
        {
            int returnValue = ViewStateInt(viewState, key);

            if (returnValue == 0)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public int ViewStateInt(StateBag viewState, string key)
        {
            if (viewState[key] == null)
                return 0;
            else
            {
                try
                {
                    return Convert.ToInt32(viewState[key]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region ViewStateString
        public string ViewStateString(StateBag viewState, string key, string defaultValue)
        {
            string returnValue = ViewStateString(viewState, key);

            if (returnValue == null)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public string ViewStateString(StateBag viewState, string key)
        {
            if (viewState[key] == null)
                return null;
            else
            {
                try
                {
                    return viewState[key].ToString();
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        #region ViewStateDate
        public DateTime ViewStateDate(StateBag viewState, string key, DateTime defaultValue)
        {
            DateTime returnValue = ViewStateDate(viewState, key);

            if (returnValue == DateTime.MinValue)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public DateTime ViewStateDate(StateBag viewState, string key)
        {
            if (viewState[key] == null)
                return DateTime.MinValue;
            else
            {
                try
                {
                    return DateTime.Parse(viewState[key].ToString());
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
        }
        #endregion

        #region FindControlRecursive
        /// <summary>
        /// Recursively finds a control with a specified ID.
        /// This is used when a control we need to find is buried within other controls, and is
        /// necessary because the standard FindControl only looks at direct children (not recursive).
        /// </summary>
        /// <param name="root">the root control to start in</param>
        /// <param name="id">the id to look for</param>
        /// <returns></returns>
        public static Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            { return root; }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);

                if (t != null)
                { return t; }
            }

            return null;
        }
        #endregion
        
        #region BuildCookieConsentPanel
        /// <summary>
        /// Builds a cookie consent panel that appears if the cookie consent has not been accepted.
        /// </summary>
        public void BuildCookieConsentPanel()
        {
            // check for tracking code
            bool statsCookiesUsed = false;

            if (File.Exists(Server.MapPath(TrackingCodeFilePath)))
            {
                statsCookiesUsed = true;
            }

            // always build panel, otherwise there will be a viewstate issue on some postbacks
            
            // outer container
            Panel cookieConsentOuterContainer = new Panel();
            cookieConsentOuterContainer.ID = "CookieConsentOuter_Container";
            cookieConsentOuterContainer.CssClass = "CookieConsentRollIn";
            cookieConsentOuterContainer.Visible = false;
                
            // inner container (inside outer)
            Panel cookieConsentInnerContainer = new Panel();
            cookieConsentInnerContainer.ID = "CookieConsentInner_Container";
            cookieConsentOuterContainer.Controls.Add(cookieConsentInnerContainer);
                
            // main container (inside inner)
            Panel cookieConsentMainContainer = new Panel();
            cookieConsentMainContainer.ID = "CookieConsentMain_Container";
            cookieConsentInnerContainer.Controls.Add(cookieConsentMainContainer);

            // headline label (first child inside main)
            Label cookieConsentHeadline = new Label();
            cookieConsentHeadline.ID = "CookieConsentHeadline_Label";
            cookieConsentHeadline.Text = _GlobalResources.ThisSiteUsesCookies;
            cookieConsentMainContainer.Controls.Add(cookieConsentHeadline);

            // text container (second child inside main)
            Panel cookieConsentTextContainer = new Panel();
            cookieConsentTextContainer.ID = "CookieConsentText_Container";
            cookieConsentMainContainer.Controls.Add(cookieConsentTextContainer);

            // intro label (first child inside text)
            Label cookieConsentIntro = new Label();
            cookieConsentIntro.ID = "CookieConsentIntro_Label";
            cookieConsentIntro.Text = _GlobalResources.WeUseCookiesToPersonalizeContentAndAnalyzeTraffic;
            cookieConsentTextContainer.Controls.Add(cookieConsentIntro);
              
            // statistics label (second child inside text)
            if (statsCookiesUsed == true)
            {
                Label cookieConsentStatisticsOption = new Label();
                cookieConsentStatisticsOption.ID = "CookieConsentStatistics_Label";
                cookieConsentStatisticsOption.Text = _GlobalResources.WeAlsoShareInformationAboutYourUseOfOurSiteWithAnalyticsPartnersWhoMayCombineItWithOtherInformationThatYouHaveProvidedToThemOrThatTheyHaveCollectedFromYourUseOfTheirServices;
                cookieConsentTextContainer.Controls.Add(cookieConsentStatisticsOption);
            }

            // closing label (last child inside text)
            Label cookieConsentClose = new Label();
            cookieConsentClose.ID = "CookieConsentClose_Label";
            cookieConsentClose.Text = _GlobalResources.YouConsentToOurCookiesIfYouContinueToUseThisWebsite;
            cookieConsentTextContainer.Controls.Add(cookieConsentClose);

            // selectors container (last child inside main)
            Panel cookieConsentSelectorsContainer = new Panel();
            cookieConsentSelectorsContainer.ID = "CookieConsentSelectors_Container";
            cookieConsentMainContainer.Controls.Add(cookieConsentSelectorsContainer);

            // "necessary" checkbox (inside selectors)
            CheckBox cookieConsentNecessary = new CheckBox();
            cookieConsentNecessary.ID = "CookieConsentNecessary_Checkbox";
            cookieConsentNecessary.Text = _GlobalResources.Necessary;
            cookieConsentNecessary.Checked = true;
            cookieConsentNecessary.Enabled = false;
            cookieConsentSelectorsContainer.Controls.Add(cookieConsentNecessary);

            // "statistics" checkbox (inside selectors)
            if (statsCookiesUsed == true)
            {
                CheckBox cookieConsentStatistics = new CheckBox();
                cookieConsentStatistics.ID = "CookieConsentStatistics_Checkbox";
                cookieConsentStatistics.Text = _GlobalResources.Statistics;
                cookieConsentStatistics.Checked = true;
                cookieConsentStatistics.Enabled = false;
                cookieConsentSelectorsContainer.Controls.Add(cookieConsentStatistics);
            }

            // button container (inside inner)
            Panel cookieConsentButtonContainer = new Panel();
            cookieConsentButtonContainer.ID = "CookieConsentButton_Container";
            cookieConsentInnerContainer.Controls.Add(cookieConsentButtonContainer);

            // button (inside button container)
            Button cookieConsentButton = new Button();
            cookieConsentButton.ID = "CookieConsent_Button";
            cookieConsentButton.Text = _GlobalResources.OK;
            cookieConsentButton.Attributes.Add("onclick", "AcceptCookieConsent(); return false;");
            cookieConsentButtonContainer.Controls.Add(cookieConsentButton);

            PageContentContainer.Controls.Add(cookieConsentOuterContainer);

            // if no cookie value, show the cookie consent panel
            if (HttpContext.Current.Request.Cookies["CookieNecessaryConsent"] == null)
            {
                cookieConsentOuterContainer.Visible = true;
            }
        }
        #endregion

        #region SetFavicon
        /// <summary>
        /// Sets the selected favorite icon to show up in the browser tab. 
        /// </summary>
        private void SetFavicon()
        {
            string faviconFilePath = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite).Favicon;
            string faviconFileExtension = Path.GetExtension(faviconFilePath);

            string mime = String.Empty;

            switch (faviconFileExtension)
            {
                case ".png":
                    mime = "image/png";
                    break;
                case ".jpg": case ".jpeg":
                    mime = "image/jpeg";
                    break;
                case ".gif":
                    mime = "image/gif";
                    break;
                case ".ico":
                    mime = "image/x-icon";
                    break;
                default:
                    mime = "image.jpeg";    // hopefully this works, but we should never end up here.
                    break;
            }

            // tag format for the favicon link
            string faviconLinkTag = "<link id=\"Favicon_Link\" rel=\"shortcut icon\" type=\"" + mime + "\" href=\"" + SitePathConstants.SITE_CONFIG_ROOT + faviconFilePath + "\"/>";

            Page.Header.Controls.Add(new LiteralControl(String.Format(faviconLinkTag)));
        }
        #endregion

        #region Private Methods
        #region _InitializeControlAdapters
        /// <summary>
        /// Initializes control adapters that are used so that we can control how certain .NET controls are rendered.
        /// </summary>
        private void _InitializeControlAdapters()
        {
            // dictionary object for control adapters
            IDictionary adapters = Context.Request.Browser.Adapters;

            //TODO: Fix CheckboxListAdapter postback issue.
            // CheckBoxList Adapter 
            //string checkBoxListTypeName = typeof(CheckBoxList).AssemblyQualifiedName;
            //string checkBoxListAdapterTypeName = typeof(CheckBoxListAdapter).AssemblyQualifiedName;
            //if (!adapters.Contains(checkBoxListTypeName))
            //{ adapters.Add(checkBoxListTypeName, checkBoxListAdapterTypeName); }

            // RadioButtonList Adapter
            string radioButtonListTypeName = typeof(RadioButtonList).AssemblyQualifiedName;
            string radioButtonListAdapterTypeName = typeof(RadioButtonListAdapter).AssemblyQualifiedName;
            if (!adapters.Contains(radioButtonListTypeName))
            { adapters.Add(radioButtonListTypeName, radioButtonListAdapterTypeName); }

            // CheckBox Adapter
            string checkBoxTypeName = typeof(CheckBox).AssemblyQualifiedName;
            string checkBoxAdapterTypeName = typeof(CheckBoxAdapter).AssemblyQualifiedName;
            if (!adapters.Contains(checkBoxTypeName))
            { adapters.Add(checkBoxTypeName, checkBoxAdapterTypeName); }

            // RadioButton Adapter
            string radioButtonTypeName = typeof(RadioButton).AssemblyQualifiedName;
            string radioButtonAdapterTypeName = typeof(RadioButtonAdapter).AssemblyQualifiedName;
            if (!adapters.Contains(radioButtonTypeName))
            { adapters.Add(radioButtonTypeName, radioButtonAdapterTypeName); }
        }
        #endregion

        #region _GetTimeAndTimezoneInformation
        /// <summary>
        /// Gets current UTC time from database server, and timezone information
        /// for the user from the database and stores them in session variables.
        /// </summary>
        /// <remarks>
        /// We use the database server's UTC as our only official source for time.
        /// </remarks>
        private void _GetTimeAndTimezoneInformation()
        {
            // Connect to Account database to retrieve the time and  timezone information
            AsentiaDatabase accountDatabaseObject = new AsentiaDatabase();

            accountDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            accountDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            accountDatabaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            accountDatabaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            accountDatabaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            accountDatabaseObject.AddParameter("@timezoneDotNetName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            accountDatabaseObject.AddParameter("@timezoneGMTOffset", null, SqlDbType.Float, 4, ParameterDirection.Output);
            accountDatabaseObject.AddParameter("@timezoneDisplayName", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            accountDatabaseObject.AddParameter("@utcNow", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

            try
            {
                accountDatabaseObject.ExecuteNonQuery("[Session.GetTimezoneInformation]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(accountDatabaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = accountDatabaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                // only use database values if they were successfully returned
                // defaults will be used in session variables if we don't populate them
                if (returnCode == 0)
                {
                    string timezoneDotNetName = accountDatabaseObject.Command.Parameters["@timezoneDotNetName"].Value.ToString();
                    float timezoneGMTOffset = float.Parse(accountDatabaseObject.Command.Parameters["@timezoneGMTOffset"].Value.ToString());
                    string timezoneDisplayName = accountDatabaseObject.Command.Parameters["@timezoneDisplayName"].Value.ToString();
                    DateTime utcNow = DateTime.Parse(accountDatabaseObject.Command.Parameters["@utcNow"].Value.ToString());

                    AsentiaSessionState.UserTimezoneDotNetName = timezoneDotNetName;
                    AsentiaSessionState.UserTimezoneBaseUtcOffset = timezoneGMTOffset;
                    AsentiaSessionState.UserTimezoneDisplayName = timezoneDisplayName;
                    AsentiaSessionState.UtcNow = utcNow;
                    AsentiaSessionState.UserTimezoneCurrentLocalTime = TimeZoneInfo.ConvertTimeFromUtc(AsentiaSessionState.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                accountDatabaseObject.Dispose();
            }
        }
        #endregion

        #region _IncludeCSSFiles
        /// <summary>
        /// Method to include all css files.
        /// </summary>
        private void _IncludeCSSFiles()
        {
            // tag format for the stylesheet link
            const string STYLE_SHEET_LINK_TAG = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}?ver={1}\" />\r\n";

            // style sheets that aren't site configurable - located in the root css folder
            ArrayList notSiteConfigurableStyleSheets = new ArrayList();

            // add stylesheets
            notSiteConfigurableStyleSheets.Add("Reset.css");

            // stylesheets that are site configurable - located in css folders under _config
            // defaults will always be loaded first, followed by client configured ones
            ArrayList siteConfigurableStyleSheets = new ArrayList();

            // add stylesheets - TODO, move some of these to their individual pages as they may not be needed globally     
            siteConfigurableStyleSheets.Add("LoginForm.css");
            siteConfigurableStyleSheets.Add("DashboardWidgetContainer.css");
            siteConfigurableStyleSheets.Add("Chart.css");
            siteConfigurableStyleSheets.Add("CSSFileEditor.css");
            siteConfigurableStyleSheets.Add("DatePicker.css");
            siteConfigurableStyleSheets.Add("Modal.css");
            siteConfigurableStyleSheets.Add("Grid.css");
            siteConfigurableStyleSheets.Add("Widgets.css");
            siteConfigurableStyleSheets.Add("Inputs.css");
            siteConfigurableStyleSheets.Add("AdminMenu.css");
            siteConfigurableStyleSheets.Add("AutoJoinRuleSet.css");
            siteConfigurableStyleSheets.Add("PageSubMenu.css");
            siteConfigurableStyleSheets.Add("Layout.css");

            // attach references to "not site configurable" stylesheets
            foreach (string stylesheet in notSiteConfigurableStyleSheets)
            {
                string pathToStylesheet = MapPathSecure(SitePathConstants.CSS + stylesheet);

                if (File.Exists(pathToStylesheet))
                {
                    Page.Header.Controls.Add(
                        new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                    );
                }
            }

            // attach references to "default site configurable" stylesheets
            // TODO - REMOVE EXCEPTION FOR "styling" and "icslearninggroup"
            if (AsentiaSessionState.SiteHostname != "styling" && AsentiaSessionState.SiteHostname != "#icslearninggroup#")
            {
                if (AsentiaSessionState.IsThemePreviewMode)
                {
                    foreach (string stylesheet in siteConfigurableStyleSheets)
                    {
                        string pathToStylesheet = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet);

                        if (File.Exists(pathToStylesheet))
                        {
                            Page.Header.Controls.Add(
                                new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                            );
                        }
                    }
                }
                else
                {
                    foreach (string stylesheet in siteConfigurableStyleSheets)
                    {
                        string pathToStylesheet = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet);

                        if (File.Exists(pathToStylesheet))
                        {
                            Page.Header.Controls.Add(
                                new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                            );
                        }
                    }
                }
            }

            // attach references to "client configured site configurable" stylesheets
            if (AsentiaSessionState.SiteHostname != "default")
            {
                if (AsentiaSessionState.IsThemePreviewMode)
                {
                    foreach (string stylesheet in siteConfigurableStyleSheets)
                    {
                        string pathToStylesheet = MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet);

                        if (File.Exists(pathToStylesheet))
                        {
                            Page.Header.Controls.Add(
                                new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                            );
                        }
                    }
                }
                else
                {
                    foreach (string stylesheet in siteConfigurableStyleSheets)
                    {
                        string pathToStylesheet = MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet);

                        if (File.Exists(pathToStylesheet))
                        {
                            Page.Header.Controls.Add(
                                new LiteralControl(String.Format(STYLE_SHEET_LINK_TAG, ResolveUrl(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + stylesheet), Common.Utility.APPLICATION_COMMON_VERSION))
                            );
                        }
                    }
                }
            }
        }
        #endregion
        #endregion

        #region Theme Controller Methods
        #region _BuildThemePreviewModePanel
        /// <summary>
        /// Builds the "top bar" container displayed across all pages for theme preview mode.
        /// </summary>
        protected void _BuildThemePreviewModePanel()
        {
            // build the container
            Panel themePreviewModePanel = new Panel();
            themePreviewModePanel.ID = "ThemePreviewModePanel";

            // LEFT SIDE - INPUTS
            Panel themePreviewModeLeftSide = new Panel();
            themePreviewModeLeftSide.ID = "ThemePreviewModePanelLeft";
            themePreviewModePanel.Controls.Add(themePreviewModeLeftSide);            

            // styling text
            Label stylingText = new Label();
            stylingText.Text = _GlobalResources.Styling + ": ";
            themePreviewModeLeftSide.Controls.Add(stylingText);

            // styling drop down
            DropDownList selectThemeStyling = new DropDownList();
            selectThemeStyling.ID = "ThemePreviewModeStyling_Field";
            selectThemeStyling.DataTextField = "name";
            selectThemeStyling.DataValueField = "folder";
            selectThemeStyling.DataSource = this._GetAvailableThemesForDropDown();
            selectThemeStyling.DataBind();
            selectThemeStyling.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.THEME_PREVIEW);

            themePreviewModeLeftSide.Controls.Add(selectThemeStyling);

            // iconset text
            Label iconsetText = new Label();
            iconsetText.Text = _GlobalResources.Icons + ": ";
            themePreviewModeLeftSide.Controls.Add(iconsetText);

            // iconset drop down
            DropDownList selectThemeIconset = new DropDownList();
            selectThemeIconset.ID = "ThemePreviewModeIconset_Field";
            selectThemeIconset.DataTextField = "name";
            selectThemeIconset.DataValueField = "folder";
            selectThemeIconset.DataSource = this._GetAvailableIconsetsForDropDown();
            selectThemeIconset.DataBind();
            selectThemeIconset.SelectedValue = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.ICONSET_PREVIEW);

            themePreviewModeLeftSide.Controls.Add(selectThemeIconset);

            // RIGHT SIDE - BUTTONS
            Panel themePreviewModeRightSide = new Panel();
            themePreviewModeRightSide.ID = "ThemePreviewModePanelRight";
            themePreviewModePanel.Controls.Add(themePreviewModeRightSide);

            // apply changes button
            LinkButton applyTheme = new LinkButton();
            applyTheme.ID = "ThemePreviewMode_ApplyChangesButton";
            applyTheme.Text = _GlobalResources.ApplyTheme;
            applyTheme.Command += new CommandEventHandler(this._ApplyThemeChanges);
            themePreviewModeRightSide.Controls.Add(applyTheme);

            // preview theme button
            LinkButton previewTheme = new LinkButton();
            previewTheme.ID = "ThemePreviewMode_PreviewThemeButton";
            previewTheme.Text = _GlobalResources.PreviewTheme;
            previewTheme.Command += new CommandEventHandler(this._SetPreviewTheme);
            themePreviewModeRightSide.Controls.Add(previewTheme);

            // exit preview mode button
            LinkButton exitPreviewMode = new LinkButton();
            exitPreviewMode.ID = "ThemePreviewMode_ExitPreviewModeButton";
            exitPreviewMode.Text = _GlobalResources.ExitPreviewMode;
            exitPreviewMode.Command += new CommandEventHandler(this._ExitThemePreviewMode);
            themePreviewModeRightSide.Controls.Add(exitPreviewMode);

            // add the panel to index 0 of page controls
            Control pageContainerControl = (Control)FindControlRecursive(this.Page, "PageContainer");
            pageContainerControl.Controls.AddAt(0, themePreviewModePanel);
        }
        #endregion

        #region _GetAvailableThemesForDropDown
        /// <summary>
        /// Builds a DataTable of themes available to the portal, for use in theme styling selector drop downs.
        /// </summary>
        /// <returns>DataTable</returns>
        protected DataTable _GetAvailableThemesForDropDown()
        {
            // get the available themes - 1st do the default, then do the ones only for this site
            DataTable availableThemes = new DataTable();

            availableThemes.Columns.Add(new DataColumn("name", typeof(string)));
            availableThemes.Columns.Add(new DataColumn("folder", typeof(string)));

            // default
            DirectoryInfo defaultThemesRoot = new DirectoryInfo(MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_ROOT));

            foreach (DirectoryInfo themeFolder in defaultThemesRoot.GetDirectories())
            {
                DataRow availableTheme = availableThemes.NewRow();
                availableTheme["folder"] = themeFolder.Name;

                if (File.Exists(MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_ROOT + themeFolder.Name + "/Metadata.xml")))
                {
                    XmlDocument themeMetadataDocument = new XmlDocument();
                    themeMetadataDocument.Load(MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_ROOT + themeFolder.Name + "/Metadata.xml"));

                    availableTheme["name"] = themeMetadataDocument.GetElementsByTagName("name")[0].InnerText;
                }
                else
                { availableTheme["name"] = themeFolder.Name; }

                availableThemes.Rows.Add(availableTheme);
            }

            // site-specific
            DirectoryInfo siteSpecificThemesRoot = new DirectoryInfo(MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_ROOT));

            if (siteSpecificThemesRoot.Exists)
            {
                foreach (DirectoryInfo themeFolder in siteSpecificThemesRoot.GetDirectories())
                {
                    if (availableThemes.Select("folder = '" + themeFolder.Name + "'").Length == 0)
                    {
                        DataRow availableTheme = availableThemes.NewRow();
                        availableTheme["folder"] = themeFolder.Name;

                        if (File.Exists(MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_ROOT + themeFolder.Name + "/Metadata.xml")))
                        {
                            XmlDocument themeMetadataDocument = new XmlDocument();
                            themeMetadataDocument.Load(MapPathSecure(SitePathConstants.SITE_TEMPLATE_THEMES_ROOT + themeFolder.Name + "/Metadata.xml"));

                            availableTheme["name"] = themeMetadataDocument.GetElementsByTagName("name")[0].InnerText;
                        }
                        else
                        { availableTheme["name"] = themeFolder.Name; }

                        availableThemes.Rows.Add(availableTheme);
                    }
                }
            }

            // return
            return availableThemes;
        }
        #endregion

        #region _GetAvailableIconsetsForDropDown
        /// <summary>
        /// Builds a DataTable of iconsets available to the portal, for use in theme iconset selector drop downs.
        /// </summary>
        /// <returns>DataTable</returns>
        protected DataTable _GetAvailableIconsetsForDropDown()
        {
            // get the available iconsets - 1st do the default, then do the ones only for this site
            DataTable availableIconsets = new DataTable();

            availableIconsets.Columns.Add(new DataColumn("name", typeof(string)));
            availableIconsets.Columns.Add(new DataColumn("folder", typeof(string)));

            // default
            DirectoryInfo defaultIconsetsRoot = new DirectoryInfo(MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS));

            foreach (DirectoryInfo iconsetFolder in defaultIconsetsRoot.GetDirectories())
            {
                DataRow availableIconset = availableIconsets.NewRow();
                availableIconset["folder"] = iconsetFolder.Name;

                if (File.Exists(MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + iconsetFolder.Name + "/Metadata.xml")))
                {
                    XmlDocument iconsetMetadataDocument = new XmlDocument();
                    iconsetMetadataDocument.Load(MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_ICONSETS + iconsetFolder.Name + "/Metadata.xml"));

                    availableIconset["name"] = iconsetMetadataDocument.GetElementsByTagName("name")[0].InnerText;
                }
                else
                { availableIconset["name"] = iconsetFolder.Name; }

                availableIconsets.Rows.Add(availableIconset);
            }

            // site-specific
            DirectoryInfo siteSpecificIconsetsRoot = new DirectoryInfo(MapPathSecure(SitePathConstants.SITE_TEMPLATE_ICONSETS));

            if (siteSpecificIconsetsRoot.Exists)
            {
                foreach (DirectoryInfo iconsetFolder in siteSpecificIconsetsRoot.GetDirectories())
                {
                    if (availableIconsets.Select("folder = '" + iconsetFolder.Name + "'").Length == 0)
                    {
                        DataRow availableIconset = availableIconsets.NewRow();
                        availableIconset["folder"] = iconsetFolder.Name;

                        if (File.Exists(MapPathSecure(SitePathConstants.SITE_TEMPLATE_ICONSETS + iconsetFolder.Name + "/Metadata.xml")))
                        {
                            XmlDocument iconsetMetadataDocument = new XmlDocument();
                            iconsetMetadataDocument.Load(MapPathSecure(SitePathConstants.SITE_TEMPLATE_ICONSETS + iconsetFolder.Name + "/Metadata.xml"));

                            availableIconset["name"] = iconsetMetadataDocument.GetElementsByTagName("name")[0].InnerText;
                        }
                        else
                        { availableIconset["name"] = iconsetFolder.Name; }

                        availableIconsets.Rows.Add(availableIconset);
                    }
                }
            }

            // return
            return availableIconsets;
        }
        #endregion

        #region _ApplyThemeChanges
        /// <summary>
        /// Applies the theme changes to the portal.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        protected void _ApplyThemeChanges(object sender, EventArgs e)
        {
            // sender information
            Control senderButton = (Control)sender;

            // is the sender the themes modify page
            bool isSenderThemesModifyPage = false;

            if (senderButton.Parent.ID == "ActionsPanel")
            { isSenderThemesModifyPage = true; }

            try
            {
                // get the selected theme from either the themes modify page, or the preview panel
                string selectedTheme = String.Empty;
                string selectedIconset = String.Empty;
                string successMessage = String.Empty;

                // from the themes modify page
                if (isSenderThemesModifyPage)
                {
                    DropDownList stylingDropDown = (DropDownList)FindControlRecursive(this.Page, "ThemeStyling_Field");
                    DropDownList iconsetDropDown = (DropDownList)FindControlRecursive(this.Page, "ThemeIconset_Field");

                    selectedTheme = stylingDropDown.SelectedValue;
                    selectedIconset = iconsetDropDown.SelectedValue;

                    successMessage = _GlobalResources.YourPortalsThemeHasBeenSavedSuccessfully;
                }
                else
                {
                    DropDownList stylingDropDown = (DropDownList)FindControlRecursive(this.Page, "ThemePreviewModeStyling_Field");
                    DropDownList iconsetDropDown = (DropDownList)FindControlRecursive(this.Page, "ThemePreviewModeIconset_Field");

                    selectedTheme = stylingDropDown.SelectedValue;
                    selectedIconset = iconsetDropDown.SelectedValue;

                    successMessage = _GlobalResources.TheSelectedThemeHasBeenAppliedToThePortal;
                }

                // create datatable for site params
                DataTable siteParams = new DataTable(); ;
                siteParams.Columns.Add("param", typeof(string));
                siteParams.Columns.Add("value", typeof(string));

                // theme styling
                DataRow themeStyling = siteParams.NewRow();
                themeStyling["param"] = SiteParamConstants.THEME_ACTIVE;
                themeStyling["value"] = selectedTheme;
                siteParams.Rows.Add(themeStyling);

                // theme iconset
                DataRow themeIconset = siteParams.NewRow();
                themeIconset["param"] = SiteParamConstants.ICONSET_ACTIVE;
                themeIconset["value"] = selectedIconset;
                siteParams.Rows.Add(themeIconset);

                // preview theme styling
                DataRow previewThemeStyling = siteParams.NewRow();
                previewThemeStyling["param"] = SiteParamConstants.THEME_PREVIEW;
                previewThemeStyling["value"] = String.Empty;
                siteParams.Rows.Add(previewThemeStyling);

                // preview theme iconset
                DataRow previewThemeIconset = siteParams.NewRow();
                previewThemeIconset["param"] = SiteParamConstants.ICONSET_PREVIEW;
                previewThemeIconset["value"] = selectedIconset;
                siteParams.Rows.Add(previewThemeIconset);

                // set preview mode false
                AsentiaSessionState.IsThemePreviewMode = false;

                // save the site params
                Common.AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite,
                                                  AsentiaSessionState.UserCulture,
                                                  AsentiaSessionState.IdSiteUser,
                                                  AsentiaSessionState.IdSite,
                                                  siteParams);

                // display success message to user
                this.DisplayFeedback(successMessage, false);

                // register a script to make the OK button reload the page, we need to do this so new styling takes effect
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReloadPageForSavedTheme", "$(\"#FeedbackInformationStatusPanelCloseButton\").attr(\"onclick\", \"window.location.href = '" + HttpContext.Current.Request.Url.AbsolutePath + "'; return false;\");", true);                
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                if (isSenderThemesModifyPage)
                { this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true); }
                else
                { this.DisplayFeedback(_GlobalResources.AnErrorOccurredWhileApplyingTheThemePleaseTryAgain, true); }
            }
        }
        #endregion

        #region _SetPreviewTheme
        /// <summary>
        /// Sets the selected theme for preview mode.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        protected void _SetPreviewTheme(object sender, EventArgs e)
        {
            // sender information
            Control senderButton = (Control)sender;

            // is the sender the themes modify page
            bool isSenderThemesModifyPage = false;

            if (senderButton.Parent.ID == "ActionsPanel")
            { isSenderThemesModifyPage = true; }

            try
            {
                // get the selected theme from either the themes modify page, or the preview mode panel
                string selectedTheme = String.Empty;
                string selectedIconset = String.Empty;
                string successMessage = String.Empty;

                // from the themes modify page
                if (isSenderThemesModifyPage)
                {
                    DropDownList stylingDropDown = (DropDownList)FindControlRecursive(this.Page, "ThemeStyling_Field");
                    DropDownList iconsetDropDown = (DropDownList)FindControlRecursive(this.Page, "ThemeIconset_Field");

                    selectedTheme = stylingDropDown.SelectedValue;
                    selectedIconset = iconsetDropDown.SelectedValue;

                    successMessage = _GlobalResources.YouHaveEnteredPreviewModeForYourPortalsThemeSelection;
                }
                else
                {
                    DropDownList stylingDropDown = (DropDownList)FindControlRecursive(this.Page, "ThemePreviewModeStyling_Field");
                    DropDownList iconsetDropDown = (DropDownList)FindControlRecursive(this.Page, "ThemePreviewModeIconset_Field");

                    selectedTheme = stylingDropDown.SelectedValue;
                    selectedIconset = iconsetDropDown.SelectedValue;

                    successMessage = _GlobalResources.YouAreNowPreviewingTheNewlySelectedThemeOptions;
                }

                // create datatable for site params
                DataTable siteParams = new DataTable(); ;
                siteParams.Columns.Add("param", typeof(string));
                siteParams.Columns.Add("value", typeof(string));

                // theme styling
                DataRow themeStyling = siteParams.NewRow();
                themeStyling["param"] = SiteParamConstants.THEME_PREVIEW;
                themeStyling["value"] = selectedTheme;
                siteParams.Rows.Add(themeStyling);

                // theme iconset
                DataRow themeIconset = siteParams.NewRow();
                themeIconset["param"] = SiteParamConstants.ICONSET_PREVIEW;
                themeIconset["value"] = selectedIconset;
                siteParams.Rows.Add(themeIconset);

                // set to preview mode
                AsentiaSessionState.IsThemePreviewMode = true;

                // save the site params
                Common.AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite,
                                                  AsentiaSessionState.UserCulture,
                                                  AsentiaSessionState.IdSiteUser,
                                                  AsentiaSessionState.IdSite,
                                                  siteParams);

                // display success message to user
                this.DisplayFeedback(successMessage, false);

                // register a script to make the OK button reload the page, we need to do this so new styling takes effect                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReploadPageForPreviewTheme", "$(\"#FeedbackInformationStatusPanelCloseButton\").attr(\"onclick\", \"window.location.href = '" + HttpContext.Current.Request.Url.AbsolutePath + "'; return false;\");", true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                if (isSenderThemesModifyPage)
                { this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true); }
                else
                { this.DisplayFeedback(_GlobalResources.AnErrorOccurredWhileSettingTheThemeForPreviewModePleaseTryAgain, true); }
            }
        }
        #endregion

        #region _ExitThemePreviewMode
        /// <summary>
        /// Discards the current theme preview, and exits preview mode.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        protected void _ExitThemePreviewMode(object sender, EventArgs e)
        {
            try
            {
                // create datatable for site params
                DataTable siteParams = new DataTable(); ;
                siteParams.Columns.Add("param", typeof(string));
                siteParams.Columns.Add("value", typeof(string));

                // theme styling
                DataRow themeStyling = siteParams.NewRow();
                themeStyling["param"] = SiteParamConstants.THEME_PREVIEW;
                themeStyling["value"] = String.Empty;
                siteParams.Rows.Add(themeStyling);

                // theme iconset
                DataRow themeIconset = siteParams.NewRow();
                themeIconset["param"] = SiteParamConstants.ICONSET_PREVIEW;
                themeIconset["value"] = String.Empty;
                siteParams.Rows.Add(themeIconset);

                // set preview mode false
                AsentiaSessionState.IsThemePreviewMode = false;

                // save the site params
                Common.AsentiaSite.SaveSiteParams(AsentiaSessionState.IdSite,
                                                  AsentiaSessionState.UserCulture,
                                                  AsentiaSessionState.IdSiteUser,
                                                  AsentiaSessionState.IdSite,
                                                  siteParams);

                // display success message to user
                this.DisplayFeedback(_GlobalResources.YouHaveExitedThemePreviewModeWithNoThemeChangesApplied, false);

                // register a script to make the OK button reload the page, we need to do this so new styling takes effect
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ReploadPageForExitThemePreview", "$(\"#FeedbackInformationStatusPanelCloseButton\").attr(\"onclick\", \"window.location.href = '" + HttpContext.Current.Request.Url.AbsolutePath + "'; return false;\");", true);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.AnErrorOccurredWhileExitingThemePreviewModePleaseTryAgain, true);
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreInit
        protected override void OnPreInit(EventArgs e)
        {
            // load the master page file
            string masterPageFileName = "GlobalSite.Master";

            // if this is the home page, load that master page file
            if (Request.Url.LocalPath.ToLower() == "/default.aspx")
            { masterPageFileName = "HomePage.Master"; }

            string masterPagePath = SitePathConstants.DEFAULT_SITE_TEMPLATE_MASTERPAGE + masterPageFileName;

            // attempt to find a custom master page, if it exists use it
            if (AsentiaSessionState.SiteHostname != "default")
            {
                string pathToSiteMasterPage = MapPathSecure(SitePathConstants.SITE_TEMPLATE_MASTERPAGE + masterPageFileName);

                if (File.Exists(pathToSiteMasterPage))
                { masterPagePath = SitePathConstants.SITE_TEMPLATE_MASTERPAGE + masterPageFileName; }
            }

            base.MasterPageFile = masterPagePath;

            // get the global site object
            AsentiaSessionState.GlobalSiteObject = new AsentiaSite(0, AsentiaSessionState.IdSite);

            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            //If the user is not logged in and detect the preferred language
            //And the preferred language in the list of site available installed languages
            try
            {
                if ((AsentiaSessionState.IdSiteUser == 0) && (Request.UserLanguages[0] != null))
                {
                    if (this.GetArrayListOfSiteAvailableInstalledLanguages().Contains(Request.UserLanguages[0]))
                    {
                        // See if the preferred language not the portal's default
                        // See if users not pick a specific language that is not the portal's default.
                        if ((Request.UserLanguages[0] != AsentiaSessionState.GlobalSiteObject.LanguageString) && (AsentiaSessionState.UserCulture == AsentiaSessionState.GlobalSiteObject.LanguageString))
                        {
                            // Set the Language
                            culture = Request.UserLanguages[0];
                            AsentiaSessionState.UserCulture = culture;
                        }
                    }
                }
            }
            catch
            { }

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            // get time and timezone information
            this._GetTimeAndTimezoneInformation();

            // get browser information
            string u = Request.ServerVariables["HTTP_USER_AGENT"];
            Regex t = new Regex(@"(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex b = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);

            if (!String.IsNullOrWhiteSpace(u) && t.IsMatch(u))
            {
                AsentiaSessionState.UserDevice = AsentiaSessionState.UserDeviceType.Tablet;
            }
            else if (!String.IsNullOrWhiteSpace(u) && (b.IsMatch(u) || v.IsMatch(u.Substring(0, 4))))
            {
                AsentiaSessionState.UserDevice = AsentiaSessionState.UserDeviceType.Mobile;
            }
            else
            {
                AsentiaSessionState.UserDevice = AsentiaSessionState.UserDeviceType.Desktop;
            }

            // get the requested url
            string requestedUrl = Request.Url.AbsolutePath.ToString().ToLower();

            // redirect to the user agreement page if a user agreement exists and required, and the user is not logged in as admin and not impersonated.   
            if (requestedUrl != "/useragreement.aspx"
                && AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.USERAGREEMENT_REQUIRED) == true
                && AsentiaSessionState.IdImpersonatingSiteUser == 0
                && AsentiaSessionState.IdSiteUser != 1
                && AsentiaSessionState.MustExecuteUserAgreement
                && !AsentiaSessionState.UserMustChangePassword)
            { Response.Redirect("/UserAgreement.aspx"); }
            
            // redirect to the execute license agreement page if a license agreement exists, it has not been executed, and the user is logged in as admin (not impersonated)            
            if (requestedUrl != "/administrator/license/executelicenseagreement.aspx"
                && AsentiaSessionState.IdImpersonatingSiteUser == 0
                && AsentiaSessionState.IdSiteUser == 1
                && AsentiaSessionState.MustExecuteLicenseAgreement
                && File.Exists(Server.MapPath(@"~\_config\" + AsentiaSessionState.SiteHostname + @"\License.html")))
            { Response.Redirect("/administrator/license/ExecuteLicenseAgreement.aspx"); }

            // execute normal PreInit operations
            base.OnPreInit(e);
        }
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            ClientScriptManager csm = this.Page.ClientScript;

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "JsVariables", "var ThresholdHeight =100"
                + ";var ExpandArrowURL ='" + ImageFiles.GetIconPath(ImageFiles.ICON_EXPAND_ARROW, ImageFiles.EXT_PNG)
                + "';var MoreText = '" + _GlobalResources.More
                + "';var LessText = '" + _GlobalResources.Less
                + "';", true);

            // register the embedded javascript resource(s)
            csm.RegisterClientScriptResource(typeof(Asentia.Common.ClientScript), "Asentia.Common.Helper.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery-1.10.2.min.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery-ui.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.AsentiaPage.js");

            // add global application javascript start up events
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" $(window).load(function() {");
            sb.AppendLine("     // add a page load handler to set content container heights on AJAX postbacks");
            sb.AppendLine("     Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(Helper.SetContentContainerHeights);");
            sb.AppendLine("");
            sb.AppendLine("     // fire the set container heights method for initial page load");
            sb.AppendLine("     Helper.SetContentContainerHeights();");            
            sb.AppendLine("");
            sb.AppendLine("     // add page category class and language to body tag");
            sb.AppendLine("     $(\"body\").addClass(\"PageCategory_" + this.PageCategory + "\");");
            sb.AppendLine("     $(\"body\").prop(\"lang\", \"" + AsentiaSessionState.UserCulture + "\");");
            sb.AppendLine("");
            sb.AppendLine("     // add language to html tag");
            sb.AppendLine("     $(\"html\").prop(\"lang\", \"" + AsentiaSessionState.UserCulture + "\");");
            // sb.AppendLine("     $(\"html\").prop(\"dir\", \"ltr\");");
            // sb.AppendLine("     $(\"html\").prop(\"dir\", \"rtl\");");
            sb.AppendLine(" });");

            csm.RegisterStartupScript(typeof(AsentiaPage), "GlobalApplicationStartUpScript", sb.ToString(), true);

            base.OnPreRender(e);
        }
        #endregion

        #region OnLoad
        protected override void OnLoad(EventArgs e)
        {
            // if site id is 1 (default) and the url we are at now is not the unknown page, redirect there
            // else, check to see if the site is active, if it is not and we are not on the offline page, redirect there
            if (AsentiaSessionState.IdSite == 1)
            {
                if (!HttpContext.Current.Request.Url.ToString().Contains("/unknown/"))
                { Response.Redirect("/unknown"); }
            }
            else
            {
                if (!AsentiaSessionState.GlobalSiteObject.IsActive || AsentiaSessionState.GlobalSiteObject.Expires <= AsentiaSessionState.UtcNow)
                {
                    if (!HttpContext.Current.Request.Url.ToString().Contains("/offline/"))
                    { Response.Redirect("/offline"); }
                }
            }

            // get portal title in language for "browser title"
            if (AsentiaSessionState.IdAccount > 1 && AsentiaSessionState.IdSite > 1)
            {
                string portalTitleInInterfaceLanguage = AsentiaSessionState.GlobalSiteObject.Title;
                base.Title = portalTitleInInterfaceLanguage;
            }

            // load the css
            this._IncludeCSSFiles();

            // set the visibility of breadcrumb, page title, and page feedback
            // containers to false, the individual page will show them if needed
            this.PageBreadcrumbContainer.Visible = false;
            this.PageTitleContainer.Visible = false;
            this.PageFeedbackContainer.Visible = false;

            // if this is theme preview mode, build the preview mode container
            if (AsentiaSessionState.IsThemePreviewMode)
            { this._BuildThemePreviewModePanel(); }

            // build cookie consent panel if it needs to be built
            if (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.PRIVACY_REQUIRECOOKIECONSENT) == true)
            { this.BuildCookieConsentPanel(); }

            // check if favicon is set
            string faviconFilePath = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite).Favicon;

            if (!String.IsNullOrEmpty(faviconFilePath))
            {
                this.SetFavicon();
            }
            
            // execute the base
            base.OnLoad(e);            
        }
        #endregion

        #region Render
        protected override void Render(HtmlTextWriter writer)
        {
            // register our custom postback helper to fix issues with how .net ajax handles static ids
            // this script needs to be registered here in the render method so that we know it is being
            // included after all of the javascript resources included by .net ajax
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Common.ClientScript), "Asentia.Common.PostBackHelper.js");

            // declare a string writer and an html writer
            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

            // execute the base, rendering nothing
            base.Render(htmlWriter);

            // go through html to be rendered and relocate the hidden form elements
            // this helps with viewstate issues
            string html = stringWriter.ToString();
            int formStart = html.IndexOf("<form");
            int endForm = -1;

            if (formStart >= 0)
            { endForm = html.IndexOf(">", formStart); }

            if (endForm >= 0)
            {
                StringBuilder viewStateBuilder = new StringBuilder();
                foreach (string element in AspNetFormElements)
                {
                    int startPoint = html.IndexOf("<input type=\"hidden\" name=\"" + element + "\"");

                    if (startPoint >= 0 && startPoint > endForm)
                    {
                        int endPoint = html.IndexOf("/>", startPoint);

                        if (endPoint >= 0)
                        {
                            endPoint += 2;
                            string viewStateInput = html.Substring(startPoint, endPoint - startPoint);
                            html = html.Remove(startPoint, endPoint - startPoint);
                            viewStateBuilder.Append(viewStateInput).Append("\r\n");
                        }
                    }
                }

                if (viewStateBuilder.Length > 0)
                {
                    viewStateBuilder.Insert(0, "\r\n");
                    html = html.Insert(endForm + 1, viewStateBuilder.ToString());
                }
            }

            // render html
            writer.Write(html);
        }
        #endregion
        #endregion
    }
}
