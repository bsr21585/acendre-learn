﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    #region Chart_Radar_Dataset CLASS
    public class Chart_Radar_Dataset
    {
        #region Constructor
        public Chart_Radar_Dataset(string fillColor, string strokeColor, string pointColor, string pointStrokeColor, object[] data)
        {
            this._FillColor = fillColor;
            this._StrokeColor = strokeColor;
            this._PointColor = pointColor;
            this._PointStrokeColor = pointStrokeColor;
            this._Data = data;
        }
        #endregion

        #region Properties
        public string FillColor
        {
            get { return this._FillColor; }
        }

        public string StrokeColor
        {
            get { return this._StrokeColor; }
        }

        public string PointColor
        {
            get { return this._PointColor; }
        }

        public string PointStrokeColor
        {
            get { return this._PointStrokeColor; }
        }

        public object[] Data
        {
            get { return this._Data; }
        }
        #endregion

        #region Private Properties
        private string _FillColor;
        private string _StrokeColor;
        private string _PointColor;
        private string _PointStrokeColor;
        private object[] _Data;
        #endregion
    }
    #endregion

    public class Chart_Radar : WebControl
    {
        #region Constructor
        public Chart_Radar(string id, int width, int height)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.CanvasWidth = width;
            this.CanvasHeight = height;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The width of the canvas tag.
        /// </summary>
        public int CanvasWidth;

        /// <summary>
        /// The height of the canvas tag.
        /// </summary>
        public int CanvasHeight;

        /// <summary>
        /// Show scale over chart data?
        /// </summary>
	    public bool ScaleOverlay = false;

        /// <summary>
        /// Override with hard-coded scale?
        /// If true, ScaleSteps, ScaleStepWidth and ScaleStartValue must be set.
        /// </summary>
	    public bool ScaleOverride = false;

        /// <summary>
        /// The number of steps in the hard-coded scale.
        /// Must be set if ScaleOverride is true.
        /// </summary>
	    public int? ScaleSteps = null;

        /// <summary>
        /// The value jump in the hard-coded scale.
        /// Must be set if ScaleOverride is true.
        /// </summary>
	    public int? ScaleStepWidth = null;

        /// <summary>
        /// The starting value of the hard-coded scale.
        /// Must be set if ScaleOverride is true.
        /// </summary>
	    public int? ScaleStartValue = null;

        /// <summary>
        /// Show line for each value in the scale?
        /// </summary>
	    public bool ScaleShowLine = true;

        /// <summary>
        /// Color of the scale line.
        /// </summary>
	    public string ScaleLineColor = "rgba(0,0,0,.1)";

        /// <summary>
        /// Width of the scale line.
        /// </summary>
	    public int ScaleLineWidth = 1;

        /// <summary>
        /// Show labels on the scale?
        /// </summary>
	    public bool ScaleShowLabels = false;

        /// <summary>
        /// Interpolated JS string - can access value
        /// </summary>
	    public string ScaleLabel = "<%=value%>";

        /// <summary>
        /// The font of the scale labels.
        /// </summary>
        public string ScaleFontFamily = "Helvetica";

        /// <summary>
        /// The font size of the scale labels in pixels.
        /// </summary>
        public int ScaleFontSize = 12;

        /// <summary>
        /// The font style of the scale labels.
        /// </summary>
        public string ScaleFontStyle = "normal";

        /// <summary>
        /// The font color of the scale labels.
        /// </summary>
        public string ScaleFontColor = "#3D3A39";

        /// <summary>
        /// Show a backdrop to the scale label?
        /// </summary>
        public bool ScaleShowLabelBackdrop = true;

        /// <summary>
        /// The color of the label backdrop.
        /// </summary>
        public string ScaleBackdropColor = "rgba(255,255,255,0.75)";

        /// <summary>
        /// The backdrop padding above & below the label in pixels.
        /// </summary>
        public int ScaleBackdropPaddingY = 2;

        /// <summary>
        /// The backdrop padding to the side of the label in pixels.
        /// </summary>
        public int ScaleBackdropPaddingX = 2;
	
	    /// <summary>
	    /// Show angle lines out of radar?
	    /// </summary>
	    public bool AngleShowLineOut = true;
	
	    /// <summary>
	    /// The color of the angle line.
	    /// </summary>
	    public string AngleLineColor = "rgba(0,0,0,.1)";
	
	    /// <summary>
	    /// The width of the angle line.
	    /// </summary>
	    public int AngleLineWidth = 1;
	
	    /// <summary>
	    /// The font of the point label.
	    /// </summary>
	    public string PointLabelFontFamily = "Helvetica";
	
	    /// <summary>
	    /// The font style of the point label.
	    /// </summary>
	    public string PointLabelFontStyle = "normal";
	
	    /// <summary>
	    /// The font size of the point label.
	    /// </summary>
	    public int PointLabelFontSize = 12;
	
	    /// <summary>
	    /// The font color of the point label.
	    /// </summary>
        public string PointLabelFontColor = "#3D3A39";

        /// <summary>
        /// Show a dot for each point?
        /// </summary>
	    public bool PointDot = true;

        /// <summary>
        /// Radius of point dot in pixels.
        /// </summary>
	    public int PointDotRadius = 3;

        /// <summary>
        /// Stroke width of point dot.
        /// </summary>
	    public int PointDotStrokeWidth = 1;

        /// <summary>
        /// Show stroke for datasets?
        /// </summary>
	    public bool DatasetStroke = true;

        /// <summary>
        /// Width of dataset stroke in pixels.
        /// </summary>
	    public int DatasetStrokeWidth = 2;

        /// <summary>
        /// Fill dataset with color?
        /// </summary>
	    public bool DatasetFill = true;

        /// <summary>
        /// Animate the chart?
        /// </summary>
	    public bool Animation = true;

        /// <summary>
        /// Number of animation steps.
        /// </summary>
	    public int AnimationSteps = 60;

        /// <summary>
        /// Animation easing effect.
        /// </summary>
	    public string AnimationEasing = "easeOutQuart";

        /// <summary>
        /// Method to fire when animation is complete.
        /// </summary>
	    public string OnAnimationComplete = null;
        #endregion

        #region Private Properties
        /// <summary>
        /// An array of labels for the data.
        /// </summary>
        List<string> _DataLabels = null;

        /// <summary>
        /// An ArrayList of datasets for this chart.
        /// </summary>
        ArrayList _Datasets = null;
        #endregion

        #region Methods
        #region AddLabel
        /// <summary>
        /// Adds a label to the labels array.
        /// </summary>
        /// <param name="label">label</param>
        public void AddLabel(string label)
        {
            if (this._DataLabels == null)
            { this._DataLabels = new List<string>(); }

            this._DataLabels.Add(label);
        }
        #endregion

        #region ClearLabels
        /// <summary>
        /// Clears the labels array.
        /// </summary>
        public void ClearLabels()
        {
            if (this._DataLabels == null)
            { return; }

            this._DataLabels.Clear();
        }
        #endregion

        #region AddDataset
        // TODO: VALIDATIONS
        /// <summary>
        /// Adds a dataset of type Chart_Radar_Dataset to the dataset ArrayList.
        /// </summary>
        /// <param name="dataset">dataset data</param>
        public void AddDataset(Chart_Radar_Dataset dataset)
        {
            if (this._Datasets == null)
            { this._Datasets = new ArrayList(); }

            this._Datasets.Add(dataset);
        }
        #endregion

        #region ClearDatasets
        /// <summary>
        /// Clears the dataset ArrayList.
        /// </summary>
        public void ClearDatasets()
        {
            if (this._Datasets == null)
            { return; }

            this._Datasets.Clear();
        }
        #endregion
        #endregion

        #region Private Methods
        #region _BuildOptionsJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's options
        /// based on the option properties.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildOptionsJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON
            sb.AppendLine("var " + this.ID + "Options = {");
            sb.AppendLine(" scaleOverlay : " + this.ScaleOverlay.ToString().ToLower() + ",");
            sb.AppendLine(" scaleOverride : " + this.ScaleOverride.ToString().ToLower() + ",");
            sb.AppendLine(" scaleSteps : " + (this.ScaleSteps == null ? "null" : this.ScaleSteps.ToString()) + ",");
            sb.AppendLine(" scaleStepWidth : " + (this.ScaleStepWidth == null ? "null" : this.ScaleStepWidth.ToString()) + ",");
            sb.AppendLine(" scaleStartValue : " + (this.ScaleStartValue == null ? "null" : this.ScaleStartValue.ToString()) + ",");
            sb.AppendLine(" scaleShowLine : " + this.ScaleShowLine.ToString().ToLower() + ",");
            sb.AppendLine(" scaleLineColor : " + this._FormatJSStringVarValue(this.ScaleLineColor) + ",");
            sb.AppendLine(" scaleLineWidth : " + this.ScaleLineWidth.ToString() + ",");
            sb.AppendLine(" scaleShowLabels : " + this.ScaleShowLabels.ToString().ToLower() + ",");
            sb.AppendLine(" scaleLabel : " + this._FormatJSStringVarValue(this.ScaleLabel) + ",");
            sb.AppendLine(" scaleFontFamily : " + this._FormatJSStringVarValue(this.ScaleFontFamily) + ",");
            sb.AppendLine(" scaleFontSize : " + this.ScaleFontSize.ToString() + ",");
            sb.AppendLine(" scaleFontStyle : " + this._FormatJSStringVarValue(this.ScaleFontStyle) + ",");
            sb.AppendLine(" scaleFontColor : " + this._FormatJSStringVarValue(this.ScaleFontColor) + ",");
            sb.AppendLine(" scaleShowLabelBackdrop : " + this.ScaleShowLabelBackdrop.ToString().ToLower() + ",");
            sb.AppendLine(" scaleBackdropColor : " + this._FormatJSStringVarValue(this.ScaleBackdropColor) + ",");
            sb.AppendLine(" scaleBackdropPaddingY : " + this.ScaleBackdropPaddingY.ToString() + ",");
            sb.AppendLine(" scaleBackdropPaddingX : " + this.ScaleBackdropPaddingX.ToString() + ",");
            sb.AppendLine(" angleShowLineOut : " + this.AngleShowLineOut.ToString().ToLower() + ",");
            sb.AppendLine(" angleLineColor : " + this._FormatJSStringVarValue(this.AngleLineColor) + ",");
            sb.AppendLine(" angleLineWidth : " + this.AngleLineWidth.ToString() + ",");
            sb.AppendLine(" pointLabelFontFamily : " + this._FormatJSStringVarValue(this.PointLabelFontFamily) + ",");
            sb.AppendLine(" pointLabelFontStyle : " + this._FormatJSStringVarValue(this.PointLabelFontStyle) + ",");
            sb.AppendLine(" pointLabelFontSize : " + this.PointLabelFontSize.ToString() + ",");
            sb.AppendLine(" pointLabelFontColor : " + this._FormatJSStringVarValue(this.PointLabelFontColor) + ",");
            sb.AppendLine(" pointDot : " + this.PointDot.ToString().ToLower() + ",");
            sb.AppendLine(" pointDotRadius : " + this.PointDotRadius.ToString() + ",");
            sb.AppendLine(" pointDotStrokeWidth : " + this.PointDotStrokeWidth.ToString() + ",");
            sb.AppendLine(" datasetStroke : " + this.DatasetStroke.ToString().ToLower() + ",");
            sb.AppendLine(" datasetStrokeWidth : " + this.DatasetStrokeWidth.ToString() + ",");
            sb.AppendLine(" datasetFill : " + this.DatasetFill.ToString().ToLower() + ",");
	        sb.AppendLine(" animation : " + this.Animation.ToString().ToLower() + ",");
	        sb.AppendLine(" animationSteps : " + this.AnimationSteps.ToString() + ",");
            sb.AppendLine(" animationEasing : " + this._FormatJSStringVarValue(this.AnimationEasing) + ",");
            sb.AppendLine(" onAnimationComplete : " + this._FormatJSStringVarValue(this.OnAnimationComplete));
            sb.AppendLine("};");
            
            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildDataJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's data
        /// based on the datasets popuated for this chart.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildDataJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // begin variable declaration
            sb.AppendLine("var " + this.ID + "Data = {");

            // build labels JSON
            string dataLabels = String.Empty;

            foreach (string label in this._DataLabels)
            { dataLabels += "\"" + label + "\","; }

            dataLabels = dataLabels.TrimEnd(',');

            // append labels JSON
            sb.AppendLine(" labels : [" + dataLabels + "],");

            // build and append datasets JSON
            sb.AppendLine(" datasets : [");

            int i = 0;

            foreach (Chart_Radar_Dataset ds in this._Datasets)
            {
                // increment i
                i++;

                // build the data
                sb.AppendLine("     {");
                sb.AppendLine("     fillColor : " + this._FormatJSStringVarValue(ds.FillColor) + ",");
                sb.AppendLine("     strokeColor : " + this._FormatJSStringVarValue(ds.StrokeColor) + ",");
                sb.AppendLine("     pointColor : " + this._FormatJSStringVarValue(ds.PointColor) + ",");
                sb.AppendLine("     pointStrokeColor : " + this._FormatJSStringVarValue(ds.PointStrokeColor) + ",");
                string dataValues = string.Join(",", ds.Data);
                sb.AppendLine("     data : [" + dataValues + "]");

                if (i < this._Datasets.Count)
                { sb.AppendLine("     },"); }
                else
                { sb.AppendLine("     }"); }
            }

            sb.AppendLine(" ]");
            sb.AppendLine("};");

            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildAndRegisterJSBlock
        /// <summary>
        /// Builds the JS block that renders the chart, and registers the
        /// JS to this control.
        /// </summary>
        private void _BuildAndRegisterJSBlock()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON
            sb.AppendLine(this._BuildOptionsJSONString());

            // build data JSON
            sb.AppendLine(this._BuildDataJSONString());

            // build call to chart function
            sb.AppendLine("var " + this.ID + "Bound = new Chart(document.getElementById(\"" + this.ID + "Canvas\").getContext(\"2d\")).Radar(" + this.ID + "Data, " + this.ID + "Options);");

            // register script
            ScriptManager.RegisterStartupScript(this, this.GetType(), this.ID + "StartUpScript", sb.ToString(), true);
        }
        #endregion

        #region _FormatJSStringVarValue
        /// <summary>
        /// Formats a string for attachment to a JS variable.
        /// </summary>
        /// <param name="value">string value</param>
        /// <returns>formatted string</returns>
        private string _FormatJSStringVarValue(string value)
        {
            if (value == null)
            { return "null"; }
            else
            { return "\"" + value + "\""; }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Chart.js");
        }
        #endregion

        #region RenderContents
        protected override void RenderContents(HtmlTextWriter writer)
        {
            // canvas
            HtmlGenericControl canvasTag = new HtmlGenericControl("canvas");
            canvasTag.ID = this.ID + "Canvas";
            canvasTag.Attributes.Add("width", this.CanvasWidth.ToString());
            canvasTag.Attributes.Add("height", this.CanvasHeight.ToString());

            // attach controls
            this.Controls.Add(canvasTag);

            // build and register JS block
            this._BuildAndRegisterJSBlock();

            // execute base
            base.RenderContents(writer);
        }
        #endregion
        #endregion
    }
}
