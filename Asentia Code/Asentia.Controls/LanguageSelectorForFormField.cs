﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    /// <summary>
    /// Builds a "drop-down" (custom, not <select>) list of languages for a language-specific form field
    /// based on which languages are installed and available to the site.
    /// </summary>
    public class LanguageSelectorForFormField : WebControl
    {
        #region Constructor
        public LanguageSelectorForFormField(string formFieldIdentifier, bool isCKEditorField)
            : base(HtmlTextWriterTag.Div)
        {
            // set the css class
            this.CssClass = "FormFieldLanguageSelector";

            // set the form field identifier
            this._FormFieldIdentifier = formFieldIdentifier;

            // set CK Editor field property
            this._IsCKEditorField = isCKEditorField;

            // build the drop-down list
            this._BuildDropDownList();
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// The identifier of the form field this will control multi-lingual form field show/hide for.
        /// </summary>
        string _FormFieldIdentifier;

        /// <summary>
        /// Is this language selector for a field that uses CK Editor?
        /// </summary>
        bool _IsCKEditorField;
        #endregion

        #region Private Methods
        #region _BuildDropDownList
        /// <summary>
        /// Populates the drop-down with languages based on what is available to the system (installed) 
        /// and what is available to the site.
        /// </summary>
        private void _BuildDropDownList()
        {
            // <dl> element, the root of our "drop-down"
            HtmlGenericControl dlElement = new HtmlGenericControl("dl");

            // <dt> element, the container for our "drop-down"'s currently selected item
            HtmlGenericControl dtElement = new HtmlGenericControl("dt");

            // <dt> element, the container for our "drop-down" items
            HtmlGenericControl ddElement = new HtmlGenericControl("dd");

            // <ul> element, the definition of our "drop-down" items
            HtmlGenericControl ulElement = new HtmlGenericControl("ul");

            // create an ArrayList to hold our installed languages
            ArrayList installedLanguages = new ArrayList();

            // en-US is the application default language, so it's already installed, add it
            installedLanguages.Add("en-US");

            // loop through each installed language based on the language folders contained in bin
            foreach (string directory in Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN)))
            {
                // loop through each language available to the site and add it to the array list for drop-down items
                foreach (string language in AsentiaSessionState.GlobalSiteObject.AvailableLanguages)
                {
                    if (language == Path.GetFileName(directory))
                    {
                        installedLanguages.Add(Path.GetFileName(directory));
                        break;
                    }
                }
            }

            // loop through array list and add each language to the "drop-down"
            foreach (string installedLanguage in installedLanguages)
            {
                // get the culture of the info for the language
                CultureInfo cultureInfo = CultureInfo.GetCultureInfo(installedLanguage);

                // if this language is the site's default, make it the "selected language" by adding it to the <dt> element
                if (cultureInfo.Name == AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    HyperLink selectedLanguageLink = new HyperLink();
                    selectedLanguageLink.ImageUrl = SitePathConstants.IMAGES_FLAGS + cultureInfo.Name + ".png";
                    selectedLanguageLink.ToolTip = cultureInfo.NativeName;
                    dtElement.Controls.Add(selectedLanguageLink);                    
                }

                // create the <li> for the "drop-down" item
                HtmlGenericControl liElement = new HtmlGenericControl("li");

                // assign a custom "language" attribute to the item so that we can do client-side stuff with its value
                if (cultureInfo.Name == AsentiaSessionState.GlobalSiteObject.LanguageString)
                { liElement.Attributes.Add("language", "_DEFAULTLANGUAGE_"); }
                else
                { liElement.Attributes.Add("language", cultureInfo.Name); }

                HyperLink languageItemLink = new HyperLink();
                languageItemLink.ImageUrl = SitePathConstants.IMAGES_FLAGS + cultureInfo.Name + ".png";
                languageItemLink.ToolTip = cultureInfo.NativeName;
                liElement.Controls.Add(languageItemLink);

                // attach the item to the <ul>
                ulElement.Controls.Add(liElement);
            }

            // apply class to <dl>
            dlElement.Attributes.Add("class", "FormFieldLanguageSelectorDropDown FormFieldLanguageSelectorDropDownDL");

            // add <ul> to <dd>
            ddElement.Controls.Add(ulElement);

            // add <dt> to <dl>
            dlElement.Controls.Add(dtElement);

            // add <dd> to <dl>
            dlElement.Controls.Add(ddElement);

            // attach the <dl> control to this control
            this.Controls.Add(dlElement);

            // build the down arrow to mimic the look of an actual drop-down list
            Panel downArrow = new Panel();
            downArrow.CssClass = "FormFieldLanguageSelectorDropDownArrow";
            this.Controls.Add(downArrow);
        }
        #endregion

        #region _BuildJSStartUpScript
        /// <summary>
        /// Builds the start up javascript to apply the jQuery functions to the control. This gets fired OnInit.
        /// </summary>
        private void _BuildJSStartUpScript()
        {
            // build the javascript using a StringBuilder
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" $(document).ready(function () {");

            // attach click event to the selected item and the arrow to expand the list items
            //sb.AppendLine("    $(\"#" + this.ID + " dt a, #" + this.ID + " .FormFieldLanguageSelectorDropDownArrow\").click(function(event) {");
            sb.AppendLine("    $(\"#" + this.ID + "\").click(function(event) {");
            sb.AppendLine("        event.stopPropagation();");
            sb.AppendLine("        $(\"#" + this.ID + " dd ul\").toggle();");
            sb.AppendLine("    });");

            // attach click event to items to change selected item, show/hide text boxes, and collapse list items
            sb.AppendLine("    $(\"#" + this.ID + " dd ul li\").click(function(event) {");
            sb.AppendLine("        event.stopPropagation();");
            sb.AppendLine("        var innerHtml = $(this).html();");
            sb.AppendLine("        $(\"#" + this.ID + " dt a\").html(innerHtml);");
            sb.AppendLine("        $(\"#" + this.ID + " dd ul\").hide();");

            if (this._IsCKEditorField)
            { sb.AppendLine("        ToggleLanguageSpecificCKEditorFormField(\"" + this._FormFieldIdentifier + "\", $(this).attr(\"language\"));"); }
            else
            { sb.AppendLine("        ToggleLanguageSpecificStandardFormField(\"" + this._FormFieldIdentifier + "\", $(this).attr(\"language\"));"); }
            
            sb.AppendLine("    });");

            // attach click event to document so that clicks anywhere but on the "drop-down" elements
            // will collapse the list items
            sb.AppendLine("    $(document).bind('click', function(e) {");
            sb.AppendLine("        var $clicked = $(e.target);");
            sb.AppendLine("        $clicked.parents().each(function () {");
            sb.AppendLine("            if (this.id.indexOf(\"" + this.ID + "\") == -1) {");
            sb.AppendLine("                $(\"#" + this.ID + " dd ul\").hide();");
            sb.AppendLine("            }");
            sb.AppendLine("        });");
            sb.AppendLine("    });");

            sb.AppendLine(" });");

            // attach the script as a startup script
            ScriptManager.RegisterStartupScript(this, typeof(LanguageSelectorForFormField), this.ID, sb.ToString(), true);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnInit
        protected override void OnInit(EventArgs e)
        {
            // execute base
            base.OnInit(e);

            // build js start up script
            this._BuildJSStartUpScript();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            // execute base
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(LanguageSelectorForFormField), "Asentia.Controls.LanguageSelectorForFormField.js");
        }
        #endregion
        #endregion
    }
}
