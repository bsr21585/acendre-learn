﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    /// <summary>
    /// Builds a drop-down list of time zones.
    /// </summary>
    public class TimeZoneSelector : DropDownList
    {
        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TimeZoneSelector()
        {
            // populate with timezones
            this._Populate();
        }
        #endregion

        #region Private Methods
        #region _Populate
        /// <summary>
        /// Populates the drop-down with timezones from the .NET library of
        /// timezones.
        /// </summary>
        private void _Populate()
        {
            // get the recordset of timezones from database
            DataTable timeZones = Timezone.GetTimezones();

            // loop through data table and add each timezone to the drop-down
            foreach (DataRow row in timeZones.Rows)
            {
                this.Items.Add(new ListItem(row["displayName"].ToString(), row["idTimezone"].ToString()));
            }
        }
        #endregion
        #endregion
    }
}

