﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    /// <summary>
    /// Control that draws a static "progress indicator" to indicate
    /// percentage of "completness," "used," or anything else that could
    /// use a graphic indicating percentage of something.
    /// </summary>
    public class ProgressIndicator : WebControl
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <remarks>
        /// Used when just putting a tag in a page, ID
        /// and Percent should be set in tag.
        /// </remarks>
        public ProgressIndicator()
            : base(HtmlTextWriterTag.Div)
        {
            // set css style for "outer" div
            this.CssClass = "ProgressIndicatorWrapper";
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">the id of this control</param>
        /// <param name="percent">percent of progress</param>
        public ProgressIndicator(string id, int? percent, string text)
            : base(HtmlTextWriterTag.Div)
        {
            // set the ID
            this.ID = id;

            // set css style for "outer" div
            this.CssClass = "ProgressIndicatorWrapper";

            // set percent
            if (percent != null && percent > 0)
            { this.Percent = (int)percent; }
            else
            { this.Percent = 0; }

            // set text
            if (!String.IsNullOrWhiteSpace(text))
            { this.Text = text; }
            else
            { this.Text = null; }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Percentage of progress bar to be filled.
        /// </summary>
        public int Percent;

        /// <summary>
        /// The text to be displayed next to progress bar, i.e. x% or x/x
        /// </summary>
        public string Text;
        #endregion

        #region Overridden Methods
        #region RenderContents
        protected override void RenderContents(HtmlTextWriter writer)
        {
            // build the progress indicator inner and outer controls

            // "outer" div
            HtmlGenericControl outerDiv = new HtmlGenericControl("div");
            outerDiv.ID = this.ID + "_ProgresBar";
            outerDiv.Attributes.Add("class", "ProgressBar");

            // "inner" span
            HtmlGenericControl innerSpan = new HtmlGenericControl("span");
            outerDiv.Controls.Add(innerSpan);

            // set the width, which is the percentage
            innerSpan.Style.Add("width", this.Percent.ToString() + "%");

            this.Controls.Add(outerDiv);

            // build the progress indicator text container

            if (this.Text != null)
            {
                HtmlGenericControl textContainer = new HtmlGenericControl("div");
                textContainer.ID = this.ID + "_ProgresIndicatorText";
                textContainer.Attributes.Add("class", "ProgresIndicatorText");

                Literal progressText = new Literal();
                progressText.Text = this.Text;
                textContainer.Controls.Add(progressText);

                this.Controls.Add(textContainer);
            }

            // render 
            base.RenderContents(writer);
        }
        #endregion
        #endregion
    }
}
