﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Asentia.Controls")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ICS Learning Group")]
[assembly: AssemblyProduct("Asentia.Controls")]
[assembly: AssemblyCopyright("Copyright © ICS Learning Group 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7636519e-d918-4aa3-a93b-8cdd3b598cd9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.X.*")]
[assembly: AssemblyVersion("1.16.*")]

// Embeded Resources

// 3rd Party
[assembly: WebResource("Asentia.Controls.jquery-1.10.1.min.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.jquery-1.10.2.min.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.jquery.easing.1.3.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.jquery-ui.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.jquery.mobile-1.4.5.min.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.jquery.kinetic.min.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.jquery.smoothTouchScroll.min.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.Carousel.min.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.Chart.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.Chart.2.4.min.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.ColorPicker.ColorPicker.css", "text/css")]
[assembly: WebResource("Asentia.Controls.ColorPicker.ColorPicker.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.ColorPicker.Eye.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.ColorPicker.Layout.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.ColorPicker.Utils.js", "text/javascript")]

// ICS Built
[assembly: WebResource("Asentia.Controls.AsentiaCustomerManagerMasterPage.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.AsentiaPage.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.AsentiaMasterPage.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.Calendar.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.CertificateViewer.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.CSSFileEditor.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.CustomerManagerPage.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.DatePicker.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.Grid.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.InformationStatusPanel.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.InfoPopup.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.LanguageSelectorForFormField.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.LoadMCE.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.LoadCKEditor.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.LoginForm.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.ModalPopup.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.MultiSelectWithOrdering.js", "text/javascript")]

// ICS Built - JS Interface
[assembly: WebResource("Asentia.Controls.JSInterface.AsentiaCarousel.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.JSInterface.AsentiaModalWindow.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.JSInterface.FormInputField.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.JSInterface.HTMLCourseListBoxWithCredits.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.JSInterface.HTMLLibrary.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.JSInterface.IconButton.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.JSInterface.LanguageSelector.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.JSInterface.Tab.js", "text/javascript")]
[assembly: WebResource("Asentia.Controls.JSInterface.Tabs.js", "text/javascript")]