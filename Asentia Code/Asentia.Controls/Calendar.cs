﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;

// TODO: Figure out a way to drop this!

namespace Asentia.Controls
{
    /// <summary>
    /// Class to create a custom calendar control to display the 
    /// occurences of courses and standup training instances enrollments.
    /// </summary>
    public class Calendar : WebControl
    {
        #region Private Properties
        private DateTime _CurrentDate;
        private DataTable _CalendarItems = new DataTable();

        private int _IdUser = 0;
        private CalendarEventsType _CalendarEventsType;

        private bool _RenderModalOnly = false;
        private string _ModalTriggerControlId = null;
        private ModalPopup _CalendarModal = new ModalPopup("CalendarModalPopup");

        private Localize _Month = new Localize();
        private Localize _TimeZoneMessage = new Localize();
        private Localize _CurrMonth = new Localize();

        private LinkButton _OpenCalendarLink = new LinkButton();
        private LinkButton _ThisMonth = new LinkButton();
        private LinkButton _PrevMonth = new LinkButton();
        private LinkButton _NextMonth = new LinkButton();

        private Image _PreviousMonthLinkIcon = new Image();
        private Image _ThisMonthLinkIcon = new Image();
        private Image _NextMonthLinkIcon = new Image();

        private Button _HiddenPopupTargetButton = new Button();
        private Button _HiddenOpenLinkButton;
        private Button _HiddenEventDetailLoadButton;
        private HiddenField _HiddenEventId = new HiddenField();
        private HiddenField _HiddenEventTitle = new HiddenField();
        private HiddenField _HiddenEventType = new HiddenField();

        private bool _IsMonthChanged = false;

        private UpdatePanel _CalendarUpdatePanel = new UpdatePanel();
        public Panel _DetailModalPopupInnerBody = new Panel();

        private HiddenField _ICalendarContent = new HiddenField();
        #endregion

        #region Public Properties
        /// <summary>
        /// this will be used to show alert icon on learner dashboard for calender widget ,If calender has any item for current date.
        /// This public field has been addd by chetu team after discussion with Joe on 28th March meeting
        /// </summary>
        public bool HasRecordsForShowingAlertIconOnWidget = false;
        public Control CalendarModalControlDestination;
        public Button _HiddenDownloadCalendarFileButton = new Button();
        public ModalPopup _EventEnrollmentModalPopup;
        #endregion

        #region Enums
        /// <summary>
        /// Enumeration to determine the types of events to display on the calendar. 
        /// </summary>
        public enum CalendarEventsType
        {
            // ILT sessions for the catalog
            CatalogILT = 0,

            // User/learner events (enrollment events, ilt, etc.)
            User = 1,
        }
        #endregion

        #region Constructors
        /// <summary>
        /// The constructor of this Calendar, it sets properties that are common to every calendar.
        /// </summary>
        /// <param name="date">the current date</param>
        /// <param name="idUser">the user to build the calendar for, if not specified, we use the current session user</param>
        /// <param name="renderModalOnly">render only the modal? this would be for just launching a calendar from a button</param>
        /// <param name="modalTriggerControlId">the control to trigger the modal launch, if render modal only, this must be specified</param>
        public Calendar(DateTime date, int idUser = 0, bool renderModalOnly = false, string modalTriggerControlId = null, CalendarEventsType calendarEventsType = CalendarEventsType.User)
        {
            // make sure if we're rendering a modal only, that we also have a trigger control
            if (renderModalOnly && String.IsNullOrWhiteSpace(modalTriggerControlId))
            { throw new AsentiaException(_GlobalResources.AModalControlTriggerMustBeSpecified); }

            this._CurrentDate = date;
            this._IdUser = idUser;
            this._RenderModalOnly = renderModalOnly;
            this._ModalTriggerControlId = modalTriggerControlId;
            this._CalendarEventsType = calendarEventsType;
        }
        #endregion

        #region OnInit
        /// <summary>
        /// Overrides the OnInit method so that controls can be initialized.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnInit(EventArgs e)
        {
            if (!Page.IsPostBack)
            { AsentiaSessionState.DateTimeForCalendar = this._CurrentDate; }

            base.OnInit(e);

            EnsureChildControls();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Calendar.js");

            // build JS variables
            StringBuilder javascriptVariables = new StringBuilder();
            javascriptVariables.AppendLine("HiddenPopupTargetButton = \"" + this._HiddenPopupTargetButton.ClientID + "\";");
            javascriptVariables.AppendLine("HiddenEventDetailLoadButton = \"" + this._HiddenEventDetailLoadButton.ClientID + "\";");

            // this only apply to the dashboard calendar due to the IE 11 issue which require the modal to be built in the this.Controls instead of widget update panel
            if (this.CalendarModalControlDestination != null)
            {
                javascriptVariables.AppendLine("HiddenOpenLinkButton = \"" + this._HiddenOpenLinkButton.ClientID + "\";");
            }

            javascriptVariables.AppendLine("HiddenEventId = \"" + this._HiddenEventId.ID + "\";");
            javascriptVariables.AppendLine("HiddenEventTitle = \"" + this._HiddenEventTitle.ID + "\";");
            javascriptVariables.AppendLine("HiddenEventType = \"" + this._HiddenEventType.ID + "\";");

            csm.RegisterClientScriptBlock(typeof(Calendar), "CalendarJS", javascriptVariables.ToString(), true);
        }
        #endregion

        #region CreateChildControls
        /// <summary>
        /// Overrides CreateChildControls to create the proper layout of the control.        
        /// This is where we would add any controls that should appear on the Fluid Controls layout.
        /// </summary>        
        /// <returns></returns>
        protected override void CreateChildControls()
        {
            if (!this._RenderModalOnly)
            {
                this._CalendarUpdatePanel.ID = "CalendarUpdatePanel";

                // open calendar link
                this._OpenCalendarLink.ID = "OpenCalendarLink";
                this._OpenCalendarLink.CssClass = "OpenCalendarLink";

                // open calendar link image
                Image openCalendarLinkIcon = new Image();
                openCalendarLinkIcon.CssClass = "SmallIcon";
                openCalendarLinkIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OPEN,
                                                                       ImageFiles.EXT_PNG);
                openCalendarLinkIcon.AlternateText = _GlobalResources.Open;
                openCalendarLinkIcon.ToolTip = _GlobalResources.Open;

                this._OpenCalendarLink.Controls.Add(openCalendarLinkIcon);

                // this only applies to the dashboard calendar due to the IE 11 issue which requires the modal to be built in the this.Controls instead of widget update panel
                if (this.CalendarModalControlDestination != null)
                {
                    this._OpenCalendarLink.OnClientClick = "$('#HiddenOpenLinkButton').click();";
                }

                this._CalendarUpdatePanel.ContentTemplateContainer.Controls.Add(this._OpenCalendarLink);

                this.Controls.Add(this._CalendarUpdatePanel);
            }

            // hidden target button for event or enrollment details modal popup
            this._HiddenPopupTargetButton.ID = "HiddenPopupTargetButton";
            this._HiddenPopupTargetButton.ClientIDMode = ClientIDMode.Static;
            this._HiddenPopupTargetButton.Style.Add("display", "none");

            if (this._RenderModalOnly)
            { this.Controls.Add(this._HiddenPopupTargetButton); }
            else
            {
                // this only apply to the dashboard calendar due to the IE 11 issue which require the modal to be built in the this.Controls instead of widget update panel
                if (this.CalendarModalControlDestination == null)
                {
                    this._CalendarUpdatePanel.ContentTemplateContainer.Controls.Add(this._HiddenPopupTargetButton);
                }
                else if (this.FindControl(this._HiddenPopupTargetButton.ID) == null)
                {
                    this.CalendarModalControlDestination.Controls.Add(this._HiddenPopupTargetButton);
                }
            }

            // hidden button to download 
            this._HiddenDownloadCalendarFileButton.ID = "HiddenDownloadCalendarFileButton";
            this._HiddenDownloadCalendarFileButton.ClientIDMode = ClientIDMode.Static;
            this._HiddenDownloadCalendarFileButton.Style.Add("display", "none");
            this._HiddenDownloadCalendarFileButton.Command += this._DownloadCalendarFile;

            if (this._RenderModalOnly)
            {
                this.Controls.Add(this._HiddenDownloadCalendarFileButton);
                this.Controls.Add(this._ICalendarContent);
            }
            else
            {
                // this only apply to the dashboard calendar due to the IE 11 issue which require the modal to be built in the this.Controls instead of widget update panel
                if (this.CalendarModalControlDestination == null)
                {
                    this._CalendarUpdatePanel.ContentTemplateContainer.Controls.Add(this._HiddenDownloadCalendarFileButton);
                    this._CalendarUpdatePanel.ContentTemplateContainer.Controls.Add(this._ICalendarContent);
                }
                else if (this.FindControl(this._HiddenDownloadCalendarFileButton.ID) == null)
                {
                    this.CalendarModalControlDestination.Controls.Add(this._HiddenDownloadCalendarFileButton);
                    this._CalendarUpdatePanel.ContentTemplateContainer.Controls.Add(this._ICalendarContent);
                }
            }


            this._BuildCalendarModal();
        }
        #endregion

        #region _ModalCloseButton_Command
        /// <summary>
        /// Handles the event initiated by the Modal Close Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _ModalCloseButton_Command(object sender, CommandEventArgs e)
        {
            // reset the session state variable so that subsequent openings
            // of the modal will reflect the current month as it is displayed
            // in the widget
            AsentiaSessionState.DateTimeForCalendar = AsentiaSessionState.UserTimezoneCurrentLocalTime;
            DateTime dateForCalendar = AsentiaSessionState.DateTimeForCalendar;

            // set "month" properties

            this._Month.Text = this._BuildCalendarForMonth(new DateTime(dateForCalendar.Year,
                                                                        dateForCalendar.Month,
                                                                        1),
                                                           CalendarType.ModalCalendar);
            this._PreviousMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._PreviousMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._CurrMonth.Text = dateForCalendar.ToString("MMMM")
                              + " "
                              + dateForCalendar.Year;
            this._NextMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(1).ToString("MMMM")
                                        + " "
                                        + dateForCalendar.AddMonths(1).Year;
            this._NextMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(1).ToString("MMMM")
                              + " "
                              + dateForCalendar.AddMonths(1).Year;
        }
        #endregion

        #region _ThisMonthButton_Command
        /// <summary>
        /// Handles the event initiated by the Calendar "This Month" Link Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _ThisMonthButton_Command(object sender, CommandEventArgs e)
        {
            // reset to this month
            DateTime dateForCalendar = AsentiaSessionState.UtcNow;
            AsentiaSessionState.DateTimeForCalendar = dateForCalendar;

            // set "month" properties
            this._Month.Text = this._BuildCalendarForMonth(new DateTime(dateForCalendar.Year,
                                                                        dateForCalendar.Month,
                                                                        1),
                                                           CalendarType.ModalCalendar);
            this._PreviousMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._PreviousMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._CurrMonth.Text = dateForCalendar.ToString("MMMM")
                              + " "
                              + dateForCalendar.Year;
            this._NextMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(1).ToString("MMMM")
                                        + " "
                                        + dateForCalendar.AddMonths(1).Year;
            this._NextMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(1).ToString("MMMM")
                              + " "
                              + dateForCalendar.AddMonths(1).Year;

            // register script to handle the end of the calandar page request
            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("CalendarPageRequestPrevMonth"))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                                                 "CalendarPageRequestPrevMonth",
                                                                 "EndCalendarPageRequest();");
            }

        }
        #endregion

        #region _PrevMonthButton_Command
        /// <summary>
        /// Handles the event initiated by the Calendar "Previous Month" Link Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _PrevMonthButton_Command(object sender, CommandEventArgs e)
        {
            // subtract the currently selected date by 1 month and set the session variable to it
            DateTime dateForCalendar = AsentiaSessionState.DateTimeForCalendar.AddMonths(-1);
            AsentiaSessionState.DateTimeForCalendar = dateForCalendar;

            // set "month" properties
            this._Month.Text = this._BuildCalendarForMonth(new DateTime(dateForCalendar.Year,
                                                                        dateForCalendar.Month,
                                                                        1),
                                                           CalendarType.ModalCalendar);
            this._PreviousMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                                + " "
                                                + dateForCalendar.AddMonths(-1).Year;
            this._PreviousMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._CurrMonth.Text = dateForCalendar.ToString("MMMM")
                              + " "
                              + dateForCalendar.Year;
            this._NextMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(1).ToString("MMMM")
                                        + " "
                                        + dateForCalendar.AddMonths(1).Year;
            this._NextMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(1).ToString("MMMM")
                              + " "
                              + dateForCalendar.AddMonths(1).Year;

            // indicate that the month was changed in the modal
            this._IsMonthChanged = true;

            // register script to handle the end of the calandar page request
            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("CalendarPageRequestPrevMonth"))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                                                 "CalendarPageRequestPrevMonth",
                                                                 "EndCalendarPageRequest();");
            }
        }
        #endregion

        #region _NextMonthButton_Command
        /// <summary>
        /// Handles the event initiated by the Calendar "Next Month" Link Button.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        protected void _NextMonthButton_Command(object sender, CommandEventArgs e)
        {
            // add to the currently selected date by 1 month and set the session variable to it
            DateTime dateForCalendar = AsentiaSessionState.DateTimeForCalendar.AddMonths(1);
            AsentiaSessionState.DateTimeForCalendar = dateForCalendar;

            // set "month" properties
            this._Month.Text = this._BuildCalendarForMonth(new DateTime(dateForCalendar.Year,
                                                                        dateForCalendar.Month,
                                                                        1),
                                                           CalendarType.ModalCalendar);
            this._PreviousMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._PreviousMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(-1).ToString("MMMM")
                                    + " "
                                    + dateForCalendar.AddMonths(-1).Year;
            this._CurrMonth.Text = dateForCalendar.ToString("MMMM")
                              + " "
                              + dateForCalendar.Year;
            this._NextMonthLinkIcon.AlternateText = dateForCalendar.AddMonths(1).ToString("MMMM")
                                        + " "
                                        + dateForCalendar.AddMonths(1).Year;
            this._NextMonthLinkIcon.ToolTip = dateForCalendar.AddMonths(1).ToString("MMMM")
                              + " "
                              + dateForCalendar.AddMonths(1).Year;

            // indicate that the month was changed in the modal
            this._IsMonthChanged = true;

            // register script to handle the end of the calandar page request
            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("CalendarPageRequestNextMonth"))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                                                                 "CalendarPageRequestNextMonth",
                                                                 "EndCalendarPageRequest();");
            }
        }
        #endregion

        #region _BuildCalendarModal
        /// <summary>
        /// Creates the Modal Popup Content.
        /// </summary>        
        private void _BuildCalendarModal()
        {
            this._CalendarModal.ID = "CalendarModal";
            this._CalendarModal.Type = ModalPopupType.Information;
            this._CalendarModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR,
                                                                        ImageFiles.EXT_PNG);
            this._CalendarModal.HeaderIconAlt = _GlobalResources.Calendar;
            this._CalendarModal.HeaderText = _GlobalResources.Calendar;

            if (this._RenderModalOnly)
            { this._CalendarModal.TargetControlID = this._ModalTriggerControlId; }
            else
            {
                if (this.CalendarModalControlDestination == null)
                {
                    this._CalendarModal.TargetControlID = this._OpenCalendarLink.ID;
                }
                else
                {
                    this._HiddenOpenLinkButton = new Button();
                    this._HiddenOpenLinkButton.ID = "HiddenOpenLinkButton";
                    this._HiddenOpenLinkButton.Style.Add("display", "none");
                    this._HiddenOpenLinkButton.ClientIDMode = ClientIDMode.Static;
                    // this only apply to the dashboard calendar due to the IE 11 issue which require the modal to be built in the this.Controls instead of widget update panel
                    if (this.FindControl(this._HiddenOpenLinkButton.ID) == null)
                    {
                        this.CalendarModalControlDestination.Controls.Add(this._HiddenOpenLinkButton);
                    }
                    this._CalendarModal.TargetControlID = this._HiddenOpenLinkButton.ID;
                }
            }

            this._CalendarModal.CloseButton.Command += new CommandEventHandler(this._ModalCloseButton_Command);

            // build the modal body
            UpdatePanel calendarModalUpdatePanel = new UpdatePanel();
            calendarModalUpdatePanel.ID = "CalendarModalUpdatePanel";

            Panel calendarBodyContent = new Panel();
            calendarBodyContent.ID = "CalendarBodyContent";

            // TOP DIV
            Panel calendarBodyContentTop = new Panel();
            calendarBodyContentTop.ID = "CalendarBodyContentTop";

            Panel panelTableRow = new Panel();
            panelTableRow.ID = "CalendarBodyContentTopRow";

            // current month label
            this._CurrMonth.Text = this._CurrentDate.ToString("MMMM")
                                   + " "
                                   + this._CurrentDate.Year;

            Panel panelCalendarCurrentMonthLabel = new Panel();
            panelCalendarCurrentMonthLabel.Controls.Add(this._CurrMonth);
            panelCalendarCurrentMonthLabel.Attributes.Add("class", "CalendarCurrentMonthLabelModal");

            Literal spacer = new Literal();
            spacer.Text = "&nbsp;";
            panelCalendarCurrentMonthLabel.Controls.Add(spacer);

            // this month button
            this._ThisMonth.ID = "CalendarThisMonth";

            this._ThisMonthLinkIcon.CssClass = "XSmallIcon ";
            this._ThisMonthLinkIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PROMOTE, ImageFiles.EXT_PNG);
            this._ThisMonthLinkIcon.AlternateText = _GlobalResources.JumpToCurrentMonth;
            this._ThisMonthLinkIcon.ToolTip = _GlobalResources.JumpToCurrentMonth;
            this._ThisMonth.Controls.Add(this._ThisMonthLinkIcon);
            this._ThisMonth.OnClientClick = "BeginCalendarPageRequest();";
            this._ThisMonth.Command += new CommandEventHandler(this._ThisMonthButton_Command);
            panelCalendarCurrentMonthLabel.Controls.Add(this._ThisMonth);

            // previous month button
            Panel panelPrevMonth = new Panel();
            panelPrevMonth.CssClass = "CalendarBodyContentTopLeftCorner";
            this._PrevMonth.ID = "CalendarPrevMonth";

            this._PreviousMonthLinkIcon.CssClass = "SmallIcon ";
            this._PreviousMonthLinkIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PREVIOUS, ImageFiles.EXT_PNG);
            this._PreviousMonthLinkIcon.AlternateText = AsentiaSessionState.DateTimeForCalendar.AddMonths(-1).ToString("MMMM")
                                           + " "
                                           + AsentiaSessionState.DateTimeForCalendar.AddMonths(-1).Year;
            this._PreviousMonthLinkIcon.ToolTip = AsentiaSessionState.DateTimeForCalendar.AddMonths(-1).ToString("MMMM")
                                           + " "
                                           + AsentiaSessionState.DateTimeForCalendar.AddMonths(-1).Year;
            this._PrevMonth.Controls.Add(this._PreviousMonthLinkIcon);
            this._PrevMonth.OnClientClick = "BeginCalendarPageRequest();";
            this._PrevMonth.Command += new CommandEventHandler(this._PrevMonthButton_Command);
            panelPrevMonth.Controls.Add(this._PrevMonth);

            // next month button
            Panel panelNextMonth = new Panel();
            panelNextMonth.CssClass = "CalendarBodyContentTopRightCorner";
            this._NextMonth.ID = "CalendarNextMonth";

            this._NextMonthLinkIcon.CssClass = "SmallIcon ";
            this._NextMonthLinkIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_NEXT, ImageFiles.EXT_PNG);
            this._NextMonthLinkIcon.AlternateText = AsentiaSessionState.DateTimeForCalendar.AddMonths(1).ToString("MMMM")
                                           + " "
                                           + AsentiaSessionState.DateTimeForCalendar.AddMonths(1).Year;
            this._NextMonthLinkIcon.ToolTip = AsentiaSessionState.DateTimeForCalendar.AddMonths(1).ToString("MMMM")
                                           + " "
                                           + AsentiaSessionState.DateTimeForCalendar.AddMonths(1).Year;
            this._NextMonth.Controls.Add(this._NextMonthLinkIcon);
            this._NextMonth.OnClientClick = "BeginCalendarPageRequest();";
            this._NextMonth.Command += new CommandEventHandler(this._NextMonthButton_Command);
            panelNextMonth.Controls.Add(this._NextMonth);

            // add controls to div
            panelTableRow.Controls.Add(panelPrevMonth);
            panelTableRow.Controls.Add(panelCalendarCurrentMonthLabel);
            panelTableRow.Controls.Add(panelNextMonth);

            calendarBodyContentTop.Controls.Add(panelTableRow);

            // MIDDLE DIV
            Panel calendarBodyContentMiddle = new Panel();
            calendarBodyContentMiddle.ID = "CalendarBodyContentMiddle";

            //Build Modal up
            this._BuildEventAndEnrollmentDetailModalPopup(this._HiddenPopupTargetButton.ID);

            // the calendar
            this._Month.Text = this._BuildCalendarForMonth(new DateTime(AsentiaSessionState.DateTimeForCalendar.Year, AsentiaSessionState.DateTimeForCalendar.Month, 1), CalendarType.ModalCalendar);

            // add controls to div
            calendarBodyContentMiddle.Controls.Add(this._Month);

            // BOTTOM DIV
            Panel calendarBodyContentBottom = new Panel();
            calendarBodyContentBottom.ID = "CalendarBodyContentBottom";

            // note indicating time zone
            this._TimeZoneMessage.Text = _GlobalResources.DatesAndTimesForOnlineActivitiesAreInTimeZone.Replace("##TIMEZONE_NAME##", AsentiaSessionState.UserTimezoneDisplayName);

            // add controls to div
            calendarBodyContentBottom.Controls.Add(this._TimeZoneMessage);

            // LOADING DIV - shown when paging between months
            Panel calendarBodyLoading = new Panel();
            calendarBodyLoading.ID = "CalendarPostbackLoadingPlaceholder";
            calendarBodyLoading.Style.Add("display", "none");

            // loading icon
            HtmlGenericControl calendarBodyLoadingImageWrapper = new HtmlGenericControl("p");
            calendarBodyLoadingImageWrapper.ID = "CalendarBodyLoadingImageWrapper";
            calendarBodyLoadingImageWrapper.Attributes.Add("class", "CalendarBodyLoadingModal");

            Image calendarBodyLoadingImage = new Image();
            calendarBodyLoadingImage.ID = "CalendarBodyLoadingImage";
            calendarBodyLoadingImage.AlternateText = _GlobalResources.Loading;
            calendarBodyLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING,
                                                                       ImageFiles.EXT_GIF);
            calendarBodyLoadingImage.CssClass = "LargeIcon";

            calendarBodyLoadingImageWrapper.Controls.Add(calendarBodyLoadingImage);
            calendarBodyLoading.Controls.Add(calendarBodyLoadingImageWrapper);

            // loading text
            HtmlGenericControl calendarBodyLoadingTextWrapper = new HtmlGenericControl("p");
            calendarBodyLoadingTextWrapper.ID = "CalendarBodyLoadingTextWrapper";
            calendarBodyLoadingTextWrapper.Attributes.Add("class", "CalendarBodyLoadingModal");

            Localize calendarBodyLoadingText = new Localize();
            calendarBodyLoadingText.Text = _GlobalResources.Loading;

            calendarBodyLoadingTextWrapper.Controls.Add(calendarBodyLoadingText);
            calendarBodyLoading.Controls.Add(calendarBodyLoadingTextWrapper);

            // add controls to body
            calendarBodyContent.Controls.Add(calendarBodyContentTop);
            calendarBodyContent.Controls.Add(calendarBodyContentMiddle);
            calendarBodyContent.Controls.Add(calendarBodyContentBottom);

            calendarModalUpdatePanel.ContentTemplateContainer.Controls.Add(calendarBodyLoading);
            calendarModalUpdatePanel.ContentTemplateContainer.Controls.Add(calendarBodyContent);

            this._CalendarModal.AddControlToBody(calendarModalUpdatePanel);

            // if this is to be rendered as a modal only, add the modal directly to this control
            // otherwise, add it to the update panel for this control
            if (this._RenderModalOnly)
            { this.Controls.Add(this._CalendarModal); }
            else
            {
                // this only apply to the dashboard calendar due to the IE 11 issue which require the modal to be built in the this.Controls instead of widget update panel
                if (this.CalendarModalControlDestination == null)
                {
                    this._CalendarUpdatePanel.ContentTemplateContainer.Controls.Add(this._CalendarModal);
                }
                else if (this.FindControl(this._CalendarModal.ID) == null)
                {
                    this.CalendarModalControlDestination.Controls.Add(this._CalendarModal);
                }
            }

        }
        #endregion

        #region _BuildEventAndEnrollmentDetailModalPopup
        /// <summary>
        /// Creates the event's detail Modal Popup Content.
        /// </summary>        
        private void _BuildEventAndEnrollmentDetailModalPopup(string targetControlId)
        {
            // set modal properties
            this._EventEnrollmentModalPopup = new ModalPopup("EventEnrollmentModalPopup");
            this._EventEnrollmentModalPopup.Type = ModalPopupType.Form;
            //this._EventEnrollmentModalPopup.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);            
            this._EventEnrollmentModalPopup.TargetControlID = targetControlId;
            this._EventEnrollmentModalPopup.ReloadPageOnClose = false;

            this._EventEnrollmentModalPopup.SubmitButton.Visible = false;
            //this._EventEnrollmentModalPopup.SubmitButton.Command += this._DropILTSession_Command;

            this._EventEnrollmentModalPopup.CloseButton.Visible = false;

            // build the modal body            
            // add controls to body
            this._HiddenEventDetailLoadButton = new Button();
            this._HiddenEventDetailLoadButton.ID = "HiddenEventDetailLoadButton";
            this._HiddenEventDetailLoadButton.Style.Add("display", "none");
            this._HiddenEventDetailLoadButton.ClientIDMode = ClientIDMode.Static;
            this._HiddenEventDetailLoadButton.Command += new CommandEventHandler(this._HiddenEventDetailLoadButton_Click);

            this._EventEnrollmentModalPopup.AddControlToBody(this._HiddenEventDetailLoadButton);
            this._EventEnrollmentModalPopup.AddControlToBody(this._DetailModalPopupInnerBody);

            this._HiddenEventId.ID = "HiddenEventId";
            this._EventEnrollmentModalPopup.AddControlToBody(this._HiddenEventId);

            this._HiddenEventTitle.ID = "HiddenEventTitle";
            this._EventEnrollmentModalPopup.AddControlToBody(this._HiddenEventTitle);

            this._HiddenEventType.ID = "HiddenIsEnrollment";
            this._EventEnrollmentModalPopup.AddControlToBody(this._HiddenEventType);

            if (this._RenderModalOnly)
            { this.Controls.Add(this._EventEnrollmentModalPopup); }
            else
            {
                // this only apply to the dashboard calendar due to the IE 11 issue which require the modal to be built in the this.Controls instead of widget update panel
                if (this.CalendarModalControlDestination == null)
                {
                    this._CalendarUpdatePanel.ContentTemplateContainer.Controls.Add(this._EventEnrollmentModalPopup);
                }
                else
                {
                    if (this.FindControl(this._EventEnrollmentModalPopup.ID) == null)
                    {
                        this.CalendarModalControlDestination.Controls.Add(this._EventEnrollmentModalPopup);
                    }
                }
            }
        }
        #endregion

        #region _HiddenEventDetailLoadButton_Click
        /// <summary>
        /// Method to load the event or enrollment detail in the modal popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _HiddenEventDetailLoadButton_Click(object sender, EventArgs e)
        {
            // clear controls from detail modal popup
            this._DetailModalPopupInnerBody.Controls.Clear();
            this._EventEnrollmentModalPopup.ClearFeedback();

            // show calendar modal
            this._CalendarModal.ShowModal();

            // show detail modal popup
            this._EventEnrollmentModalPopup.ShowModal();

            if (this._HiddenEventType.Value.Contains("session"))
            {
                this._EventEnrollmentModalPopup.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG);

                DataTable dtStandupTrainingInstanceDetail = new DataTable();
                dtStandupTrainingInstanceDetail = this._GetEventDetails(Convert.ToInt32(this._HiddenEventId.Value));

                if (dtStandupTrainingInstanceDetail.Rows.Count > 0)
                {
                    DataRow standupTrainingInstanceMainRow = dtStandupTrainingInstanceDetail.Rows[0];

                    int idUser = Convert.ToInt32(standupTrainingInstanceMainRow["idUser"]);
                    int idStandupTraining = Convert.ToInt32(standupTrainingInstanceMainRow["idStandupTraining"]);
                    string title = standupTrainingInstanceMainRow["eventTitle"].ToString();
                    string description = standupTrainingInstanceMainRow["description"].ToString();
                    int type = Convert.ToInt32(standupTrainingInstanceMainRow["type"]);
                    string urlRegistration = standupTrainingInstanceMainRow["urlRegistration"].ToString();
                    string urlAttend = standupTrainingInstanceMainRow["urlAttend"].ToString();
                    string city = standupTrainingInstanceMainRow["city"].ToString();
                    string province = standupTrainingInstanceMainRow["province"].ToString();
                    string locationDescription = standupTrainingInstanceMainRow["locationDescription"].ToString();
                    string genericJoinUrl = standupTrainingInstanceMainRow["genericJoinUrl"].ToString();
                    string meetingPassword = standupTrainingInstanceMainRow["meetingPassword"].ToString();
                    string instructor = standupTrainingInstanceMainRow["instructor"].ToString();
                    string specificJoinUrl = standupTrainingInstanceMainRow["specificJoinUrl"].ToString();
                    string registrantKey = standupTrainingInstanceMainRow["registrantKey"].ToString();
                    bool isCallerEnrolled = Convert.ToBoolean(standupTrainingInstanceMainRow["isCallerEnrolled"]);
                    bool isCallerWaitlisted = Convert.ToBoolean(standupTrainingInstanceMainRow["isCallerWaitlisted"]);
                    bool isRestrictedDrop = Convert.ToBoolean(standupTrainingInstanceMainRow["isRestrictedDrop"]);
                    bool isStandaloneEnroll = Convert.ToBoolean(standupTrainingInstanceMainRow["isStandaloneEnroll"]);
                    string containedWithinCourses = standupTrainingInstanceMainRow["containedWithinCourses"].ToString();

                    // determine if session has started
                    bool standupTrainingInstanceHasStarted = false;

                    foreach (DataRow row in dtStandupTrainingInstanceDetail.Rows)
                    {
                        if (Convert.ToDateTime(row["dtStart"]).AddMinutes(-15) <= AsentiaSessionState.UtcNow)
                        { standupTrainingInstanceHasStarted = true; }
                    }

                    #region Description
                    if (!String.IsNullOrWhiteSpace(description))
                    {
                        Literal descriptionText = new Literal();
                        descriptionText.Text = description;

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildFormField("Description",
                                                                                         _GlobalResources.Description,
                                                                                         descriptionText.ID,
                                                                                         descriptionText,
                                                                                         false,
                                                                                         false,
                                                                                         false));
                    }
                    #endregion

                    #region Location
                    if (type == 0) // online
                    {
                        List<Control> onlineSessionControls = new List<Control>();

                        // location label
                        Panel locationContainer = new Panel();
                        locationContainer.Style.Add("margin-bottom", "5px");

                        Literal locationText = new Literal();
                        locationText.Text = _GlobalResources.Online;
                        locationContainer.Controls.Add(locationText);

                        onlineSessionControls.Add(locationContainer);

                        // show registration/attendance URLs only if the caller is enrolled
                        if (isCallerEnrolled)
                        {
                            // url register -- show until 15 minutes prior to session starting
                            if (!String.IsNullOrWhiteSpace(urlRegistration) && !standupTrainingInstanceHasStarted)
                            {
                                Panel urlRegisterContainer = new Panel();

                                Label urlRegisterLabel = new Label();
                                urlRegisterLabel.Text = _GlobalResources.Register + ": ";
                                urlRegisterContainer.Controls.Add(urlRegisterLabel);

                                HyperLink urlRegisterLink = new HyperLink();
                                urlRegisterLink.NavigateUrl = urlRegistration;
                                urlRegisterLink.Target = "_blank";
                                urlRegisterLink.Text = urlRegistration;
                                urlRegisterContainer.Controls.Add(urlRegisterLink);

                                onlineSessionControls.Add(urlRegisterContainer);
                            }

                            // url attend - only show 15 minutes prior to session starting                        
                            if (!String.IsNullOrWhiteSpace(urlAttend) && standupTrainingInstanceHasStarted)
                            {
                                Panel urlAttendContainer = new Panel();

                                Label urlAttendLabel = new Label();
                                urlAttendLabel.Text = _GlobalResources.Attend + ": ";
                                urlAttendContainer.Controls.Add(urlAttendLabel);

                                HyperLink urlAttendLink = new HyperLink();
                                urlAttendLink.NavigateUrl = urlAttend;
                                urlAttendLink.Target = "_blank";
                                urlAttendLink.Text = urlAttend;
                                urlAttendContainer.Controls.Add(urlAttendLink);

                                onlineSessionControls.Add(urlAttendContainer);
                            }
                        }

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                             _GlobalResources.Location,
                                                                                                             onlineSessionControls,
                                                                                                             false,
                                                                                                             false));
                    }
                    else if (type == 2) // GTM
                    {
                        List<Control> gtmSessionControls = new List<Control>();

                        // location label
                        Panel locationContainer = new Panel();
                        locationContainer.Style.Add("margin-bottom", "5px");

                        Image gtmImage = new Image();
                        gtmImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOMEETING, ImageFiles.EXT_PNG);
                        gtmImage.Style.Add("margin-right", "6px");
                        gtmImage.CssClass = "SmallIcon";
                        locationContainer.Controls.Add(gtmImage);

                        Literal locationText = new Literal();
                        locationText.Text = _GlobalResources.GoToMeeting;
                        locationContainer.Controls.Add(locationText);

                        gtmSessionControls.Add(locationContainer);

                        // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                        if (isCallerEnrolled)
                        {
                            // join link - only show 15 minutes prior to session starting                        
                            if (!String.IsNullOrWhiteSpace(genericJoinUrl) && standupTrainingInstanceHasStarted)
                            {
                                Panel joinLinkContainer = new Panel();

                                HyperLink joinLink = new HyperLink();
                                joinLink.NavigateUrl = genericJoinUrl;
                                joinLink.Target = "_blank";
                                joinLink.Text = _GlobalResources.JoinMeeting;
                                joinLinkContainer.Controls.Add(joinLink);

                                gtmSessionControls.Add(joinLinkContainer);
                            }
                        }

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                             _GlobalResources.Location,
                                                                                                             gtmSessionControls,
                                                                                                             false,
                                                                                                             false));
                    }
                    else if (type == 3) // GTW
                    {
                        List<Control> gtwSessionControls = new List<Control>();

                        // location label
                        Panel locationContainer = new Panel();
                        locationContainer.Style.Add("margin-bottom", "5px");

                        Image gtwImage = new Image();
                        gtwImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOWEBINAR, ImageFiles.EXT_PNG);
                        gtwImage.Style.Add("margin-right", "6px");
                        gtwImage.CssClass = "SmallIcon";
                        locationContainer.Controls.Add(gtwImage);

                        Literal locationText = new Literal();
                        locationText.Text = _GlobalResources.GoToWebinar;
                        locationContainer.Controls.Add(locationText);

                        gtwSessionControls.Add(locationContainer);

                        // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                        if (isCallerEnrolled)
                        {
                            // join link - only show 15 minutes prior to session starting                        
                            if (!String.IsNullOrWhiteSpace(specificJoinUrl) && standupTrainingInstanceHasStarted)
                            {
                                Panel joinLinkContainer = new Panel();

                                HyperLink joinLink = new HyperLink();
                                joinLink.NavigateUrl = specificJoinUrl;
                                joinLink.Target = "_blank";
                                joinLink.Text = _GlobalResources.JoinWebinar;
                                joinLinkContainer.Controls.Add(joinLink);

                                gtwSessionControls.Add(joinLinkContainer);
                            }
                        }

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                             _GlobalResources.Location,
                                                                                                             gtwSessionControls,
                                                                                                             false,
                                                                                                             false));
                    }
                    else if (type == 4) // GTT
                    {
                        List<Control> gttSessionControls = new List<Control>();

                        // location label
                        Panel locationContainer = new Panel();
                        locationContainer.Style.Add("margin-bottom", "5px");

                        Image gttImage = new Image();
                        gttImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GOTOTRAINING, ImageFiles.EXT_PNG);
                        gttImage.Style.Add("margin-right", "6px");
                        gttImage.CssClass = "SmallIcon";
                        locationContainer.Controls.Add(gttImage);

                        Literal locationText = new Literal();
                        locationText.Text = _GlobalResources.GoToTraining;
                        locationContainer.Controls.Add(locationText);

                        gttSessionControls.Add(locationContainer);

                        // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                        if (isCallerEnrolled)
                        {
                            // join link - only show 15 minutes prior to session starting                        
                            if (!String.IsNullOrWhiteSpace(specificJoinUrl) && standupTrainingInstanceHasStarted)
                            {
                                Panel joinLinkContainer = new Panel();

                                HyperLink joinLink = new HyperLink();
                                joinLink.NavigateUrl = specificJoinUrl;
                                joinLink.Target = "_blank";
                                joinLink.Text = _GlobalResources.JoinTraining;
                                joinLinkContainer.Controls.Add(joinLink);

                                gttSessionControls.Add(joinLinkContainer);
                            }
                        }

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                             _GlobalResources.Location,
                                                                                                             gttSessionControls,
                                                                                                             false,
                                                                                                             false));
                    }
                    else if (type == 5) // WebEx
                    {
                        List<Control> wbxSessionControls = new List<Control>();

                        // location label
                        Panel locationContainer = new Panel();
                        locationContainer.Style.Add("margin-bottom", "5px");

                        Image wbxImage = new Image();
                        wbxImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_WEBEX, ImageFiles.EXT_PNG);
                        wbxImage.Style.Add("margin-right", "6px");
                        wbxImage.CssClass = "SmallIcon";
                        locationContainer.Controls.Add(wbxImage);

                        Literal locationText = new Literal();
                        locationText.Text = _GlobalResources.WebEx;
                        locationContainer.Controls.Add(locationText);

                        wbxSessionControls.Add(locationContainer);

                        // show attendance link only if the caller is enrolled, and session is starting in less than 15 minutes
                        if (isCallerEnrolled)
                        {
                            // join link - only show 15 minutes prior to session starting                        
                            if ((!String.IsNullOrWhiteSpace(specificJoinUrl) || !String.IsNullOrWhiteSpace(genericJoinUrl)) && standupTrainingInstanceHasStarted)
                            {
                                Panel joinLinkContainer = new Panel();

                                HyperLink joinLink = new HyperLink();

                                if (!String.IsNullOrWhiteSpace(specificJoinUrl))
                                { joinLink.NavigateUrl = specificJoinUrl; }
                                else
                                { joinLink.NavigateUrl = genericJoinUrl; }

                                joinLink.Target = "_blank";
                                joinLink.Text = _GlobalResources.JoinMeeting;
                                joinLinkContainer.Controls.Add(joinLink);

                                if (!String.IsNullOrWhiteSpace(meetingPassword))
                                {
                                    Literal meetingPasswordLit = new Literal();
                                    meetingPasswordLit.Text = " (" + _GlobalResources.Password + ": " + meetingPassword + ")";
                                    joinLinkContainer.Controls.Add(meetingPasswordLit);
                                }

                                wbxSessionControls.Add(joinLinkContainer);
                            }
                        }

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Location",
                                                                                                             _GlobalResources.Location,
                                                                                                             wbxSessionControls,
                                                                                                             false,
                                                                                                             false));
                    }
                    else // classroom
                    {
                        Literal locationText = new Literal();
                        locationText.Text = city + ", " + province;

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildFormField("Location",
                                                                                         _GlobalResources.Location,
                                                                                         locationText.ID,
                                                                                         locationText,
                                                                                         false,
                                                                                         false,
                                                                                         false));
                    }
                    #endregion

                    #region Location Description
                    if (!String.IsNullOrWhiteSpace(locationDescription) && isCallerEnrolled) // show if caller is enrolled
                    {
                        Literal locationDescriptionText = new Literal();
                        locationDescriptionText.Text = locationDescription;

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildFormField("LocationDescription",
                                                                                         _GlobalResources.LocationDescription,
                                                                                         locationDescriptionText.ID,
                                                                                         locationDescriptionText,
                                                                                         false,
                                                                                         false,
                                                                                         false));
                    }
                    #endregion

                    #region Instructor
                    if (!String.IsNullOrWhiteSpace(instructor))
                    {
                        Literal instructorText = new Literal();
                        instructorText.Text = instructor;

                        //Add controls to instructor panel
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildFormField("Instructor",
                                                                                         _GlobalResources.Instructor,
                                                                                         instructorText.ID,
                                                                                         instructorText,
                                                                                         false,
                                                                                         false,
                                                                                         false));
                    }
                    #endregion

                    #region Meeting Time(s)
                    List<Control> meetingTimesControls = new List<Control>();

                    foreach (DataRow row in dtStandupTrainingInstanceDetail.Rows)
                    {
                        Panel meetingTimePanel = new Panel();

                        DateTime dtStart = Convert.ToDateTime(row["dtStart"]);
                        DateTime dtEnd = Convert.ToDateTime(row["dtEnd"]);

                        // if this is a classroom-based session, convert meeting times to session's timezone, if it's online, convert meeting times to learner's timezone
                        if (Convert.ToInt32(row["type"]) == 1)
                        {
                            int idTimezone = Convert.ToInt32(row["timezone"]);
                            string tzDotNetName = new Timezone(idTimezone).dotNetName;

                            dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));
                            dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(tzDotNetName));

                        }
                        else
                        {
                            dtStart = TimeZoneInfo.ConvertTimeFromUtc(dtStart, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                            dtEnd = TimeZoneInfo.ConvertTimeFromUtc(dtEnd, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                        }

                        // meeting time values
                        Literal meetingTimesText = new Literal();
                        meetingTimesText.Text = Convert.ToString(dtStart) + " - " + Convert.ToString(dtEnd);
                        meetingTimePanel.Controls.Add(meetingTimesText);

                        // Combine location description with the city/state
                        string fullLocationWithCity;
                        // if there is no location description, only use city/province, and vice versa
                        if (locationDescription == null || locationDescription == "")
                        {
                            fullLocationWithCity = city + ", " + province;
                        }
                        else if (city == null || city == "" || province == null || province == "")
                        {
                            fullLocationWithCity = locationDescription;
                        }
                        else
                        {
                            fullLocationWithCity = locationDescription + " (" + city + ", " + province + ")";
                        }

                        // icalendar link
                        this._ICalendarContent.Value = Utility.GetICalendarFormatedString(dtStart.ToString("o"), dtEnd.ToString("o"), title, fullLocationWithCity, description);

                        LinkButton standupTrainingInstanceMeetingTimeICalendarLink = new LinkButton();
                        standupTrainingInstanceMeetingTimeICalendarLink.CssClass = "ICalendarLink";
                        standupTrainingInstanceMeetingTimeICalendarLink.OnClientClick = "Helper.CreateICalendarObject(); return false;";

                        Image icalendarImage = new Image();
                        icalendarImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CALENDAR, ImageFiles.EXT_PNG);
                        icalendarImage.CssClass = "XSmallIcon";

                        standupTrainingInstanceMeetingTimeICalendarLink.Controls.Add(icalendarImage);
                        meetingTimePanel.Controls.Add(standupTrainingInstanceMeetingTimeICalendarLink);

                        meetingTimesControls.Add(meetingTimePanel);
                    }

                    // attach the controls
                    this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("MeetingTimes",
                                                                                         _GlobalResources.MeetingTime_s,
                                                                                         meetingTimesControls,
                                                                                         false,
                                                                                         false));
                    #endregion

                    #region Enrollment Availability
                    // only show this if we are looking at this from the Catalog
                    if (this._CalendarEventsType == CalendarEventsType.CatalogILT)
                    {
                        List<Control> enrollmentAvailabilityControls = new List<Control>();

                        // standalone enrollment
                        Panel standaloneEnrollmentContainer = new Panel();
                        standaloneEnrollmentContainer.ID = "StandaloneEnrollmentOptionContainer";
                        enrollmentAvailabilityControls.Add(standaloneEnrollmentContainer);

                        Image standaloneEnrollmentYesOrNoImage = new Image();
                        standaloneEnrollmentYesOrNoImage.CssClass = "XSmallIcon";
                        standaloneEnrollmentContainer.Controls.Add(standaloneEnrollmentYesOrNoImage);

                        if (isStandaloneEnroll)
                        {
                            standaloneEnrollmentYesOrNoImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CHECK_GREEN, ImageFiles.EXT_PNG);

                            HyperLink standaloneEnrollmentCatalogLink = new HyperLink();
                            standaloneEnrollmentCatalogLink.NavigateUrl = "/catalog/Default.aspx?id=" + idStandupTraining.ToString() + "&type=ilt";
                            standaloneEnrollmentCatalogLink.Text = _GlobalResources.StandaloneEnrollment;
                            standaloneEnrollmentContainer.Controls.Add(standaloneEnrollmentCatalogLink);
                        }
                        else
                        {
                            standaloneEnrollmentYesOrNoImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE, ImageFiles.EXT_PNG);

                            Literal standaloneEnrollmentText = new Literal();
                            standaloneEnrollmentText.Text = _GlobalResources.StandaloneEnrollment;
                            standaloneEnrollmentContainer.Controls.Add(standaloneEnrollmentText);
                        }

                        // contained within course(s)
                        if (!String.IsNullOrWhiteSpace(containedWithinCourses))
                        {
                            // "Within course(s)"
                            Panel containedWithinCoursesContainer = new Panel();
                            containedWithinCoursesContainer.ID = "ContainedWithinCoursesContainer";
                            enrollmentAvailabilityControls.Add(containedWithinCoursesContainer);

                            Panel withinCoursesWrapper = new Panel();
                            containedWithinCoursesContainer.Controls.Add(withinCoursesWrapper);

                            Literal withinCoursesText = new Literal();
                            withinCoursesText.Text = _GlobalResources.WithinCourse_s + ":";
                            withinCoursesWrapper.Controls.Add(withinCoursesText);

                            // list the courses
                            HtmlGenericControl coursesUL = new HtmlGenericControl("ul");
                            string[] courseList = containedWithinCourses.Split(new string[] { "||" }, StringSplitOptions.None);

                            foreach (string courseString in courseList)
                            {
                                string[] courseDetailsList = courseString.Split(new string[] { "##" }, StringSplitOptions.None);
                                HtmlGenericControl courseLI = new HtmlGenericControl("li");

                                HyperLink courseLink = new HyperLink();
                                courseLink.NavigateUrl = "/catalog/Default.aspx?id=" + courseDetailsList[0] + "&type=course";
                                courseLink.Text = courseDetailsList[1];
                                courseLI.Controls.Add(courseLink);

                                coursesUL.Controls.Add(courseLI);
                            }

                            withinCoursesWrapper.Controls.Add(coursesUL);
                        }

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("EnrollmentAvailability",
                                                                                             _GlobalResources.EnrollmentAvailability,
                                                                                             enrollmentAvailabilityControls,
                                                                                             false,
                                                                                             false));
                    }
                    #endregion


                    #region Session Additional Information
                    // if the caller is waitlisted, let them know
                    if (isCallerWaitlisted)
                    {
                        List<Control> sessionAdditionalInfoControls = new List<Control>();

                        // on waitlist text
                        Panel waitlistTextContainer = new Panel();
                        waitlistTextContainer.Style.Add("margin-bottom", "5px");

                        Literal waitlistText = new Literal();
                        waitlistText.Text = _GlobalResources.YouAreCurrentlyOnTheWaitlistForThisSession;
                        waitlistTextContainer.Controls.Add(waitlistText);

                        sessionAdditionalInfoControls.Add(waitlistTextContainer);

                        // attach the controls
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("SessionAdditionalInformation",
                                                                                                                String.Empty,
                                                                                                                sessionAdditionalInfoControls,
                                                                                                                false,
                                                                                                                false));
                    }

                    // if the caller can drop the session, show the "drop session" button
                    /*
                    if (!isRestrictedDrop && (isCallerEnrolled || isCallerWaitlisted) && !standupTrainingInstanceHasStarted)
                    {
                        this._EventEnrollmentModalPopup.SubmitButton.Visible = true;
                        this._EventEnrollmentModalPopup.SubmitButton.CommandArgument = standupTrainingInstanceHasStarted.ToString() + "|" + idUser.ToString();
                        this._EventEnrollmentModalPopup.SubmitButton.Text = _GlobalResources.DropSession;
                    }
                    */
                    #endregion
                }
            }
            else
            {
                this._EventEnrollmentModalPopup.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG);

                DataTable dtEnrollmentDetail = new DataTable();
                dtEnrollmentDetail = this._GetEnrollmentDetails(Convert.ToInt32(this._HiddenEventId.Value));

                if (dtEnrollmentDetail.Rows.Count > 0)
                {
                    DataRow enrollmentDetailRow = dtEnrollmentDetail.Rows[0];

                    #region Status
                    if (!String.IsNullOrWhiteSpace(Convert.ToString(enrollmentDetailRow["status"])))
                    {
                        string enrollmentStatus = enrollmentDetailRow["status"].ToString();
                        Literal statusText = new Literal();

                        if (enrollmentStatus == "completed")
                        { statusText.Text = _GlobalResources.Completed; }
                        if (enrollmentStatus == "expired")
                        { statusText.Text = _GlobalResources.Expired; }
                        if (enrollmentStatus == "overdue")
                        { statusText.Text = _GlobalResources.Overdue; }
                        if (enrollmentStatus == "future")
                        { statusText.Text = _GlobalResources.Future; }
                        else
                        { statusText.Text = _GlobalResources.Enrolled; }

                        //Add controls to status panel
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildFormField("Status",
                                                                 _GlobalResources.Status,
                                                                 statusText.ID,
                                                                 statusText,
                                                                 false,
                                                                 false,
                                                                 false));
                    }
                    #endregion

                    #region Start Date
                    if (!String.IsNullOrWhiteSpace(Convert.ToString(enrollmentDetailRow["dtStart"])))
                    {
                        Literal startDate = new Literal();
                        DateTime dtStart = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(enrollmentDetailRow["dtStart"]), TimeZoneInfo.FindSystemTimeZoneById(new Timezone(Convert.ToInt32(enrollmentDetailRow["idTimezone"])).dotNetName));
                        startDate.Text = Convert.ToString(dtStart);

                        //Add controls to start date panel
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildFormField("StartDate",
                                                                                              _GlobalResources.StartDate,
                                                                                              startDate.ID,
                                                                                              startDate,
                                                                                              false,
                                                                                              false,
                                                                                              false));
                    }
                    #endregion

                    #region Due Date
                    if (!String.IsNullOrWhiteSpace(Convert.ToString(enrollmentDetailRow["dtDue"])))
                    {

                        Literal dueDate = new Literal();
                        DateTime dtDue = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(enrollmentDetailRow["dtDue"]), TimeZoneInfo.FindSystemTimeZoneById(new Timezone(Convert.ToInt32(enrollmentDetailRow["idTimezone"])).dotNetName));
                        dueDate.Text = Convert.ToString(dtDue);

                        //Add controls to Due date panel
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildFormField("DueDate",
                                                                                              _GlobalResources.DueDate,
                                                                                              dueDate.ID,
                                                                                              dueDate,
                                                                                              false,
                                                                                              false,
                                                                                              false));
                    }
                    #endregion

                    #region Expiration Date
                    if (!String.IsNullOrWhiteSpace(Convert.ToString(enrollmentDetailRow["expirationDate"])))
                    {
                        Literal expirationDate = new Literal();
                        DateTime dtExpiration = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(enrollmentDetailRow["expirationDate"]), TimeZoneInfo.FindSystemTimeZoneById(new Timezone(Convert.ToInt32(enrollmentDetailRow["idTimezone"])).dotNetName));
                        expirationDate.Text = Convert.ToString(dtExpiration);

                        //Add controls to expiration date panel
                        this._DetailModalPopupInnerBody.Controls.Add(AsentiaPage.BuildFormField("ExpirationDate",
                                                                                              _GlobalResources.ExpirationDate,
                                                                                              expirationDate.ID,
                                                                                              expirationDate,
                                                                                              false,
                                                                                              false,
                                                                                              false));
                    }
                    #endregion
                }
            }
        }
        #endregion

        #region _DownloadCalendarFile
        /// <summary>
        /// Download the calendar file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DownloadCalendarFile(object sender, CommandEventArgs e)
        {
            // split the ICalendarContent to remove the new line characters
            string[] splitString = this._ICalendarContent.Value.Split(new string[] { @"\n" }, StringSplitOptions.RemoveEmptyEntries);

            // append each line separately to the calendar file
            StringBuilder sb = new StringBuilder();
            foreach (string s in splitString)
            {
                sb.AppendLine(s);
            }

            // download the calendar file
            HttpContext.Current.Response.ContentType = "text/calendar;charset-utf8";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=AsentiaMeeting.ics");
            HttpContext.Current.Response.Write(sb.ToString());
            HttpContext.Current.Response.End();
        }
        #endregion

        #region _DropILTSession_Command
        /// <summary>
        /// Handles the event initiated by the button click for removing learner from standup training instance.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">Arguments</param>
        private void _DropILTSession_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // get the values needed to drop the session
                Button dropButton = (Button)sender;
                string[] commandArguments = dropButton.CommandArgument.Split('|');

                bool hasSessionAlreadyOccurred = Convert.ToBoolean(commandArguments[0]);

                int idUser = 0;
                if (!String.IsNullOrWhiteSpace(commandArguments[1]))
                { idUser = Convert.ToInt32(commandArguments[1]); }

                int idStandupTrainingInstance = Convert.ToInt32(this._HiddenEventId.Value);

                // remove learner from standup training instance
                if (!hasSessionAlreadyOccurred)
                {
                    //StandupTrainingInstance standupTrainingInstance = new StandupTrainingInstance(idStandupTrainingInstance);
                    //standupTrainingInstance.RemoveUser(idUser);

                    // hide the deleted calendar session
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideDeletedCalendarSession", "$(\".CalendarEventSession_" + idStandupTrainingInstance.ToString() + "\").hide();", true);

                    // display feedback
                    this._EventEnrollmentModalPopup.DisplayFeedback(_GlobalResources.YouHaveBeenRemovedFromTheInstructorLedTrainingSessionSuccessfully, false);
                }

                this._EventEnrollmentModalPopup.SubmitButton.Visible = false;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this._EventEnrollmentModalPopup.DisplayFeedback(dnfEx.Message, true);
                this._EventEnrollmentModalPopup.SubmitButton.Visible = false;
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this._EventEnrollmentModalPopup.DisplayFeedback(fnuEx.Message, true);
                this._EventEnrollmentModalPopup.SubmitButton.Visible = false;
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this._EventEnrollmentModalPopup.DisplayFeedback(cpeEx.Message, true);
                this._EventEnrollmentModalPopup.SubmitButton.Visible = false;
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this._EventEnrollmentModalPopup.DisplayFeedback(dEx.Message, true);
                this._EventEnrollmentModalPopup.SubmitButton.Visible = false;
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this._EventEnrollmentModalPopup.DisplayFeedback(ex.Message, true);
                this._EventEnrollmentModalPopup.SubmitButton.Visible = false;
            }
        }
        #endregion

        #region Render
        /// <summary>
        /// Overrides the Render method so that layout can be rendered.
        /// </summary>
        /// <param name="output">HtmlTextWriter</param>
        protected override void Render(HtmlTextWriter output)
        {
            DateTime currentDate = new DateTime(this._CurrentDate.Year, this._CurrentDate.Month, 1);

            // if we're not just rendering a modal, take over the render method and render all elements of this control to the page
            if (!this._RenderModalOnly)
            {
                output.RenderBeginTag("div");
                output.AddAttribute(HtmlTextWriterAttribute.Class, "WidgetCalendarMonthLabel");
                output.RenderBeginTag("p");
                output.Write(this._CurrentDate.ToString("MMMM") + " " + this._CurrentDate.Year);
                output.RenderEndTag();
                _CalendarUpdatePanel.RenderControl(output);
                output.RenderEndTag();

                string renderedCalendarMonth = this._BuildCalendarForMonth(currentDate, CalendarType.WidgetCalendar);
                output.Write(renderedCalendarMonth);
            }

            // javascript to show modal during pagination of modal calendar
            if (this._IsMonthChanged)
            {
                ScriptManager.RegisterStartupScript(this.Page,
                                                    this.GetType(),
                                                    "CalendarModalPopup",
                                                    "$('#CalendarModalPopupModalPopupExtender_backgroundElement').css('display', 'block');$('#CalendarModal').css('display', 'block');", true);
            }

            // if we're just rendering a modal, the control was added directly to this control, so let the base render method fire
            if (this._RenderModalOnly)
            { base.Render(output); }
        }
        #endregion

        #region _BuildCalendarForMonth
        /// <summary>
        /// Builds a calendar table for the month of the given "currentDate."
        /// 
        /// Note: The method we use to build this calendar, "build then return a 
        /// string of HTML that represents the calendar," goes against what we 
        /// normally do with building controls and adding them to the "stack."
        /// We are doing this because we do not want the calendar added to the
        /// "control stack," this makes it easier for us to page between calendar
        /// months.
        /// </summary>
        /// <param name="currentDate">date used to build calendar from</param>
        /// <param name="calendarType">the type of calendar table to render</param>
        private string _BuildCalendarForMonth(DateTime currentDate, CalendarType calendarType)
        {
            if (this._CalendarEventsType == CalendarEventsType.CatalogILT)
            {
                // get the calendar items for all ILT sessions
                this._CalendarItems = this._GetILTSessions(currentDate);
            }
            else
            {
                // get the calendar items for the user
                this._CalendarItems = this._GetItemsForUser(currentDate);
            }

            // create a string builder
            StringBuilder sb = new StringBuilder();

            sb.Append("<div id=\"WidgetCalendarTableDiv\">");

            // if this is the calendar for the widget, use abbreviated day names,
            // else (for the clanedar modal) use full day names
            if (calendarType == CalendarType.WidgetCalendar)
            {
                sb.Append("<table id=\"WidgetCalendarTable\" class=\"WidgetCalendarTable\">");
                sb.Append("<thead>");

                for (int i = 0; i < DateTimeFormatInfo.CurrentInfo.AbbreviatedDayNames.Length; i++)
                {
                    sb.Append("<th>");
                    sb.Append(DateTimeFormatInfo.CurrentInfo.AbbreviatedDayNames[i]);
                    sb.Append("</th>");
                }

                sb.Append("</thead>");
            }
            else
            {
                sb.Append("<table id=\"ModalCalendarTable\" class=\"ModalCalendarTable\">");
                sb.Append("<thead>");

                for (int i = 0; i < DateTimeFormatInfo.CurrentInfo.DayNames.Length; i++)
                {
                    sb.Append("<th>");
                    sb.Append(DateTimeFormatInfo.CurrentInfo.DayNames[i]);
                    sb.Append("</th>");
                }

                sb.Append("</thead>");
            }

            // build the body
            sb.Append("<tbody>");

            // set variables to calculate out the calendar
            int currentMonth = currentDate.Month;
            int dayOfWeek = (_GetNumberOfDayInWeek(currentDate.DayOfWeek.ToString())) % 7;
            currentDate = currentDate.AddDays(-(dayOfWeek + (dayOfWeek < 0 ? 7 : 0)));

            for (int i = 0; i < 6; i++) // 6 rows in calendar
            {
                sb.Append("<tr>");

                for (int j = 0; j < 7; j++) // 7 columns in calendar
                {
                    TableCell dayColumn = new TableCell();

                    if (currentMonth == currentDate.Month)
                    {
                        // if the date being drawn is today, apply styling
                        if (currentDate.ToString("MM/dd/yyyy") == AsentiaSessionState.UserTimezoneCurrentLocalTime.ToString("MM/dd/yyyy"))
                        {
                            sb.Append("<td class=\"CurrentDayCell\">");
                            sb.Append("<div class=\"DayNumberDiv\">");
                            sb.Append(currentDate.Day.ToString());
                            sb.Append("</div>");
                        }
                        else
                        {
                            sb.Append("<td class=\"DayInMonthCell\">");
                            sb.Append("<div class=\"DayNumberDiv\">");
                            sb.Append(currentDate.Day.ToString());
                            sb.Append("</div>");
                        }

                        // if there are items for the calendar, go through them
                        if (this._CalendarItems.Rows.Count > 0)
                        {
                            this.HasRecordsForShowingAlertIconOnWidget = false;

                            foreach (DataRow dataRow in this._CalendarItems.Rows)
                            {
                                // convert the event datetime to local datetime
                                DateTime eventDate = Convert.ToDateTime(dataRow["dtEvent"]);
                                eventDate = TimeZoneInfo.ConvertTimeFromUtc(eventDate, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                                string itemIdPrefix = "CalendarEvent";

                                if (dataRow["event"].ToString() == "session")
                                { itemIdPrefix += "Session_"; }
                                else
                                { itemIdPrefix += "Enrollment_"; }

                                // if event falls on the current day being rendered, add it
                                if (eventDate.ToString("MM/dd/yyyy") == currentDate.ToString("MM/dd/yyyy"))
                                {
                                    sb.Append("<div class=\"EventInfoDiv\">");
                                    sb.Append("<span>");
                                    sb.Append("<a id=\"" + itemIdPrefix + dataRow["id"].ToString() + "\" class=\"" + itemIdPrefix + dataRow["id"].ToString() + "\" href=\"javascript:void(0);\" onclick=\"OpenDetailModalPopup('" + dataRow["eventTitle"].ToString().Replace("'", @"\'") + "','" + dataRow["id"].ToString() + "','" + dataRow["event"].ToString() + "');\" title=\""
                                              + dataRow["eventTitle"].ToString()
                                              + "\">");
                                    sb.Append("<img class=\"SmallIcon\" src=\"" + this._GetItemIcon(dataRow["event"].ToString()) + "\" /></a></span>");
                                    sb.Append("</div>");
                                }

                                //today's date 
                                //checking if there are any item for current date
                                DateTime todayDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                                if (eventDate.ToString("MM/dd/yyyy") == todayDate.ToString("MM/dd/yyyy"))
                                {
                                    this.HasRecordsForShowingAlertIconOnWidget = true;
                                }
                            }
                        }

                        // close column
                        sb.Append("</td>");
                    }
                    else // for days NOT in the month being rendered
                    {
                        sb.Append("<td class=\"DayNotInMonthCell\">");
                        sb.Append("<div class=\"DayNumberDiv\">");
                        sb.Append(currentDate.Day.ToString());
                        sb.Append("</td>");
                    }

                    currentDate = currentDate.AddDays(1);
                }

                // close row
                sb.Append("</tr>");
            }

            // close tags
            sb.Append("</tbody>");
            sb.Append("</table>");
            sb.Append("</div>");

            // return HTML
            return sb.ToString();
        }

        #endregion

        #region _GetItemsForUser
        /// <summary>
        /// Returns the list of calendar events for a particular user.
        /// </summary>
        private DataTable _GetItemsForUser(DateTime calendarDate)
        {
            DataTable calendarItems = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

                // if a user was specified, use it; otherwise, use the current session user
                if (this._IdUser > 0)
                { databaseObject.AddParameter("@idCaller", this._IdUser, SqlDbType.Int, 4, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input); }

                databaseObject.AddParameter("@calendarDate", calendarDate, SqlDbType.DateTime, 8, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Calendar.GetItemsForUser]", true);
                calendarItems.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return calendarItems;
        }
        #endregion

        #region _GetILTSessions
        /// <summary>
        /// Returns the list of all ILT events occurring in the same month of the date passed.
        /// </summary>
        private DataTable _GetILTSessions(DateTime calendarDate)
        {
            DataTable calendarItems = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

                // if a user was specified, use it; otherwise, use the current session user
                if (this._IdUser > 0)
                { databaseObject.AddParameter("@idCaller", this._IdUser, SqlDbType.Int, 4, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input); }

                databaseObject.AddParameter("@calendarDate", calendarDate, SqlDbType.DateTime, 8, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Calendar.GetSessions]", true);
                calendarItems.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return calendarItems;
        }
        #endregion

        #region _GetEventDetails
        /// <summary>
        /// Returns the details of session instance.
        /// </summary>
        private DataTable _GetEventDetails(int idStandupTrainingInstance)
        {
            DataTable calendarItems = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

                // if a user was specified, use it; otherwise, use the current session user
                if (this._IdUser > 0)
                { databaseObject.AddParameter("@idCaller", this._IdUser, SqlDbType.Int, 4, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input); }

                databaseObject.AddParameter("@idStandupTrainingInstance", idStandupTrainingInstance, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Calendar.GetEventDetails]", true);
                calendarItems.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return calendarItems;
        }
        #endregion

        #region _GetEnrollmentDetails
        /// <summary>
        /// Returns the details of enrollment event.
        /// </summary>
        private DataTable _GetEnrollmentDetails(int idEnrollment)
        {
            DataTable calendarItems = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);

                // if a user was specified, use it; otherwise, use the current session user
                if (this._IdUser > 0)
                { databaseObject.AddParameter("@idCaller", this._IdUser, SqlDbType.Int, 4, ParameterDirection.Input); }
                else
                { databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input); }

                databaseObject.AddParameter("@idEnrollment", idEnrollment, SqlDbType.Int, 4, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Calendar.GetEnrollmentDetails]", true);
                calendarItems.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return calendarItems;
        }
        #endregion

        #region _GetItemIcon
        /// <summary>
        /// Gets the icon for the event item on the calendar.
        /// </summary>
        /// <param name="eventType">the event type</param>
        /// <returns>string with path to icon</returns>
        private string _GetItemIcon(string eventType)
        {
            switch (eventType)
            {
                case "enrollmentDue":
                    return ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT,
                                                  ImageFiles.EXT_PNG);
                case "enrollmentExpires":
                    return ImageFiles.GetIconPath(ImageFiles.ICON_ENROLLMENT,
                                                  ImageFiles.EXT_PNG);
                case "session":
                    return ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING_SESSION,
                                                  ImageFiles.EXT_PNG);
                default:
                    return String.Empty;
            }
        }
        #endregion

        #region _GetNumberOfDayInWeek
        /// <summary>
        /// Returns the array index number of a particular day in a week.
        /// </summary>
        /// <param name="day">the name of the weekday</param>
        private int _GetNumberOfDayInWeek(string day)
        {
            switch (day)
            {
                case "Sunday":
                    return 0;
                case "Monday":
                    return 1;
                case "Tuesday":
                    return 2;
                case "Wednesday":
                    return 3;
                case "Thursday":
                    return 4;
                case "Friday":
                    return 5;
                case "Saturday":
                    return 6;
                default:
                    return 0;
            }
        }
        #endregion

        #region CalendarType [ENUM]
        /// <summary>
        /// Enumeration for different types of Calendar.
        /// </summary>
        public enum CalendarType
        {
            /// <summary>
            /// WidgetCalendar.
            /// Used for calendar table in widget on the dashboard screen.
            /// </summary>
            [Description("WidgetCalendar")]
            WidgetCalendar = 0,

            /// <summary>
            /// MainCalendar.
            /// Used for calendar table in Modal Popup.
            /// </summary>
            [Description("ModalCalendar")]
            ModalCalendar = 1
        }
        #endregion
    }
}