﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;


namespace Asentia.Controls
{
    public class AdministratorMenu : WebControl
    {
        #region Private Properties
        #region Icon Paths
        // System
        private string _IconPathSystem;
        private string _IconPathSystemOn;

        // Users and Groups
        private string _IconPathUsersAndGroups;
        private string _IconPathUsersAndGroupsOn;

        // Interface and Layout
        private string _IconPathInterfaceAndLayout;
        private string _IconPathInterfaceAndLayoutOn;

        // Learning Assets
        private string _IconPathLearningAssets;
        private string _IconPathLearningAssetsOn;

        // Reporting
        private string _IconPathReporting;
        private string _IconPathReportingOn;

        // Modify (+) Button
        private string _IconPathModifyButton;

        #endregion

        #region Href Destinations
        // System
        private string _HrefDestinationSystemItemAccountSettings;
        private string _HrefDestinationSystemItemConfiguration;
        private string _HrefDestinationSystemItemUserFieldConfiguration;
        private string _HrefDestinationSystemItemMessagingSettings;
        private string _HrefDestinationSystemItemEmailNotifications;
        private string _HrefDestinationSystemItemWebMeetingIntegration;
        private string _HrefDestinationSystemItemTrackingCodes;
        private string _HrefDestinationSystemItemAPI;
        private string _HrefDestinationSystemItemEcommerce;
        private string _HrefDestinationSystemItemLogs;
        private string _HrefDestinationSystemItemxAPIEndpoints;
        private string _HrefDestinationSystemItemCouponCodes;
        private string _HrefDestinationSystemItemRulesEngine;

        // Users and Groups
        private string _HrefDestinationUsersAndGroupsItemUsers;
        private string _HrefDestinationUsersAndGroupsItemGroups;
        private string _HrefDestinationUsersAndGroupsItemRoles;
        private string _HrefDestinationUsersAndGroupsItemActivityImport;
        private string _HrefDestinationUsersAndGroupsItemCertificateImport;
        private string _HrefDestinationUsersAndGroupsItemLeaderboards;

        // Interface and Layout
        private string _HrefDestinationInterfaceAndLayoutItemHomePage;
        private string _HrefDestinationInterfaceAndLayoutItemMasthead;
        private string _HrefDestinationInterfaceAndLayoutItemFooter;
        private string _HrefDestinationInterfaceAndLayoutItemThemes;
        private string _HrefDestinationInterfaceAndLayoutItemCSSEditor;
        private string _HrefDestinationInterfaceAndLayoutItemImageEditor;

        // Learning Assets
        private string _HrefDestinationLearningAssetsItemCourseCatalog;
        private string _HrefDestinationLearningAssetsItemLearningPaths;
        private string _HrefDestinationLearningAssetsItemCertificateTemplates;
        private string _HrefDestinationLearningAssetsItemCourses;
        private string _HrefDestinationLearningAssetsItemResourceManagement;
        private string _HrefDestinationLearningAssetsItemContentPackages;
        private string _HrefDestinationLearningAssetsItemQuizzesAndSurveys;
        private string _HrefDestinationLearningAssetsItemStandupTraining;
        private string _HrefDestinationLearningAssetsItemCertifications;
        private string _HrefDestinationLearningAssetsItemOpenSesameMarketplace;

        // Reporting
        private string _HrefDestinationReportingItemReports;
        private string _HrefDestinationReportingItemAnalytics;
        #endregion

        #region Menu Items Display
        // System
        private bool _DisplaySystemCategory = false;
        private bool _DisplaySystemItemAccountSettings = false;
        private bool _DisplaySystemItemConfiguration = false;
        private bool _DisplaySystemItemEcommerce = false;
        private bool _DisplaySystemItemCouponCodes = false;
        private bool _DisplaySystemItemTrackingCodes = false;
        private bool _DisplaySystemItemEmailNotifications = false;       
        private bool _DisplaySystemItemWebMeetingIntegration = false;
        private bool _DisplaySystemItemAPI = false;
        private bool _DisplaySystemItemxAPIEndpoints = false;
        private bool _DisplaySystemItemLogs = false;
        private bool _DisplaySystemItemRulesEngine = false;

        // Users and Groups
        private bool _DisplayUsersAndGroupsCategory = false;
        private bool _DisplayUsersAndGroupsItemUserFieldConfiguration = false;
        private bool _DisplayUsersAndGroupsItemUsers = false;
        private bool _DisplayUsersAndGroupsItemGroups = false;
        private bool _DisplayUsersAndGroupsItemRoles = false;
        private bool _DisplayUsersAndGroupsItemActivityImport = false;
        private bool _DisplayUsersAndGroupsItemCertificateImport = false;
        private bool _DisplayUsersAndGroupsItemLeaderboards = false;

        // Interface and Layout
        private bool _DisplayInterfaceAndLayoutCategory = false;
        private bool _DisplayInterfaceAndLayoutItemHomePage = false;
        private bool _DisplayInterfaceAndLayoutItemMasthead = false;
        private bool _DisplayInterfaceAndLayoutItemFooter = false;
        private bool _DisplayInterfaceAndLayoutItemThemes = false;
        private bool _DisplayInterfaceAndLayoutItemCSSEditor = false;
        private bool _DisplayInterfaceAndLayoutItemImageEditor = false;

        // Learning Assets
        private bool _DisplayLearningAssetsCategory = false;
        private bool _DisplayLearningAssetsItemCourseCatalog = false;
        private bool _DisplayLearningAssetsItemCourses = false;
        private bool _DisplayLearningAssetsItemLearningPaths = false;
        private bool _DisplayLearningAssetsItemContentPackages = false;
        private bool _DisplayLearningAssetsItemQuizzesAndSurveys = false;
        private bool _DisplayLearningAssetsItemCertificateTemplates = false;
        private bool _DisplayLearningAssetsItemStandupTraining = false;
        private bool _DisplayLearningAssetsItemResourceManagement = false;
        private bool _DisplayLearningAssetsItemCertifications = false;
        private bool _DisplayLearningAssetsItemOpenSesameMarketplace = false;

        // Reporting
        private bool _DisplayReportingCategory = false;
        private bool _DisplayReportingItemReports = false;
        private bool _DisplayReportingItemAnalytics = false;
        #endregion

        private EcommerceSettings _EcommerceSettings;
        private HtmlGenericControl _JavascriptTag;
        private Panel _AdministratorMenuCategoryIconsContainer = new Panel();        
        #endregion

        #region ReportTypesAccess
        //private ReportDataSet ReportDataSetObject = new ReportDataSet();
        #endregion

        #region Constructor
        public AdministratorMenu() : base(HtmlTextWriterTag.Div)
        { ;}
        #endregion

        #region Private Methods
        #region _SetIconPaths
        /// <summary>
        /// Sets the paths to the category icons for the menu.
        /// </summary>
        private void _SetIconPaths()
        {
            // System
            this._IconPathSystem = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM, ImageFiles.EXT_PNG);
            this._IconPathSystemOn = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM, ImageFiles.EXT_PNG);

            // Users and Groups
            this._IconPathUsersAndGroups = ImageFiles.GetIconPath(ImageFiles.ICON_USERSANDGROUPS, ImageFiles.EXT_PNG);
            this._IconPathUsersAndGroupsOn = ImageFiles.GetIconPath(ImageFiles.ICON_USERSANDGROUPS, ImageFiles.EXT_PNG);

            // Interface and Layout
            this._IconPathInterfaceAndLayout = ImageFiles.GetIconPath(ImageFiles.ICON_APPLICATION, ImageFiles.EXT_PNG);
            this._IconPathInterfaceAndLayoutOn = ImageFiles.GetIconPath(ImageFiles.ICON_APPLICATION, ImageFiles.EXT_PNG);

            // Learning Assets
            this._IconPathLearningAssets = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGASSETS, ImageFiles.EXT_PNG);
            this._IconPathLearningAssetsOn = ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGASSETS, ImageFiles.EXT_PNG);

            // Reporting
            this._IconPathReporting = ImageFiles.GetIconPath(ImageFiles.ICON_GRAPH, ImageFiles.EXT_PNG);
            this._IconPathReportingOn = ImageFiles.GetIconPath(ImageFiles.ICON_GRAPH, ImageFiles.EXT_PNG);

            //Modify (+) Button
            this._IconPathModifyButton = ImageFiles.GetIconPath(ImageFiles.ICON_ADD, ImageFiles.EXT_PNG);
        }
        #endregion

        #region _SetHrefDestinations
        /// <summary>
        /// Sets the link destinations for the menu items.
        /// </summary>
        private void _SetHrefDestinations()
        {
            // System
            this._HrefDestinationSystemItemAccountSettings = SitePathConstants.ADMINISTRATOR + "system/account/Settings.aspx";
            this._HrefDestinationSystemItemConfiguration = SitePathConstants.ADMINISTRATOR + "system/configuration/";
            this._HrefDestinationSystemItemUserFieldConfiguration = SitePathConstants.ADMINISTRATOR + "system/users/UserFieldConfiguration.aspx";
            this._HrefDestinationSystemItemMessagingSettings = SitePathConstants.ADMINISTRATOR + "system/messaging/Settings.aspx";
            this._HrefDestinationSystemItemEmailNotifications = SitePathConstants.ADMINISTRATOR + "system/emailnotifications/";
            this._HrefDestinationSystemItemWebMeetingIntegration = SitePathConstants.ADMINISTRATOR + "system/webmeetingintegration/";
            this._HrefDestinationSystemItemTrackingCodes = SitePathConstants.ADMINISTRATOR + "system/trackingcodes/";
            this._HrefDestinationSystemItemAPI = SitePathConstants.ADMINISTRATOR + "system/api/";
            this._HrefDestinationSystemItemEcommerce = SitePathConstants.ADMINISTRATOR + "system/ecommerce/";
            this._HrefDestinationSystemItemLogs = SitePathConstants.ADMINISTRATOR + "system/logs/";
            this._HrefDestinationSystemItemxAPIEndpoints = SitePathConstants.ADMINISTRATOR + "system/xAPIEndpoints/";
            this._HrefDestinationSystemItemCouponCodes = SitePathConstants.ADMINISTRATOR + "couponcodes/";
            this._HrefDestinationSystemItemRulesEngine = SitePathConstants.ADMINISTRATOR + "rulesengine/";

            // Users and Groups
            this._HrefDestinationUsersAndGroupsItemUsers = SitePathConstants.ADMINISTRATOR + "users/";
            this._HrefDestinationUsersAndGroupsItemGroups = SitePathConstants.ADMINISTRATOR + "groups/";
            this._HrefDestinationUsersAndGroupsItemRoles = SitePathConstants.ADMINISTRATOR + "roles/";
            this._HrefDestinationUsersAndGroupsItemActivityImport = SitePathConstants.ADMINISTRATOR + "activityimport/";
            this._HrefDestinationUsersAndGroupsItemCertificateImport = SitePathConstants.ADMINISTRATOR + "certificateimport/";
            this._HrefDestinationUsersAndGroupsItemLeaderboards = SitePathConstants.ADMINISTRATOR + "leaderboards/";

            // Interface and Layout
            this._HrefDestinationInterfaceAndLayoutItemHomePage = SitePathConstants.ADMINISTRATOR + "system/interface/HomePage.aspx";
            this._HrefDestinationInterfaceAndLayoutItemMasthead = SitePathConstants.ADMINISTRATOR + "system/interface/Masthead.aspx";
            this._HrefDestinationInterfaceAndLayoutItemFooter = SitePathConstants.ADMINISTRATOR + "system/interface/Footer.aspx";
            this._HrefDestinationInterfaceAndLayoutItemThemes = SitePathConstants.ADMINISTRATOR + "system/interface/Themes.aspx";
            this._HrefDestinationInterfaceAndLayoutItemCSSEditor = SitePathConstants.ADMINISTRATOR + "system/interface/CSSEditor.aspx";
            this._HrefDestinationInterfaceAndLayoutItemImageEditor = SitePathConstants.ADMINISTRATOR + "system/interface/ImageEditor.aspx";

            // Learning Assets
            this._HrefDestinationLearningAssetsItemCourseCatalog = SitePathConstants.ADMINISTRATOR + "catalog/";
            this._HrefDestinationLearningAssetsItemLearningPaths = SitePathConstants.ADMINISTRATOR + "learningpaths/";
            this._HrefDestinationLearningAssetsItemCertificateTemplates = SitePathConstants.ADMINISTRATOR + "certificates/";
            this._HrefDestinationLearningAssetsItemCourses = SitePathConstants.ADMINISTRATOR + "courses/";
            this._HrefDestinationLearningAssetsItemResourceManagement = SitePathConstants.ADMINISTRATOR + "resourcemanagement/";
            this._HrefDestinationLearningAssetsItemContentPackages = SitePathConstants.ADMINISTRATOR + "packages/";
            this._HrefDestinationLearningAssetsItemQuizzesAndSurveys = SitePathConstants.ADMINISTRATOR + "quizzesandsurveys/";
            this._HrefDestinationLearningAssetsItemStandupTraining = SitePathConstants.ADMINISTRATOR + "standuptraining/";
            this._HrefDestinationLearningAssetsItemCertifications = SitePathConstants.ADMINISTRATOR + "certifications/";
            this._HrefDestinationLearningAssetsItemOpenSesameMarketplace = SitePathConstants.ADMINISTRATOR + "opensesame/Marketplace.aspx";

            // Reporting
            this._HrefDestinationReportingItemReports = SitePathConstants.REPORTING + "reports/";
            this._HrefDestinationReportingItemAnalytics = SitePathConstants.REPORTING + "analytics/";
        }
        #endregion

        #region _SetMenuItemDisplayProperties
        /// <summary>
        /// Sets whether or not menu items will be displayed based on permissions.
        /// </summary>
        private void _SetMenuItemDisplayProperties()
        {
            // CHECK PERMISSIONS FOR DISPLAY OF MENU OPTIONS

            if (AsentiaSessionState.IdSiteUser == 1) // id user 1 gets access to everything
            {
                // SYSTEM

                this._DisplaySystemCategory = true;
                this._DisplaySystemItemAccountSettings = true;
                this._DisplaySystemItemConfiguration = true;

                // e-commerce 
                if (this._EcommerceSettings.IsEcommerceEnabled)
                { this._DisplaySystemItemEcommerce = true; }

                // coupon codes - e-commerce must be set-up, but doesn't have to be validated
                if (this._EcommerceSettings.IsEcommerceSet)
                { this._DisplaySystemItemCouponCodes = true; }

                this._DisplaySystemItemTrackingCodes = true;
                this._DisplaySystemItemEmailNotifications = true;       

                // web meeting integration
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE))
                { this._DisplaySystemItemWebMeetingIntegration = true; }

                this._DisplaySystemItemAPI = true;

                // xAPI endpoints
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.XAPIENDPOINTS_ENABLE))
                { this._DisplaySystemItemxAPIEndpoints = true; }

                this._DisplaySystemItemLogs = true;

                this._DisplaySystemItemRulesEngine = true;

                // USERS AND GROUPS

                this._DisplayUsersAndGroupsCategory = true;
                this._DisplayUsersAndGroupsItemUserFieldConfiguration = true;
                this._DisplayUsersAndGroupsItemUsers = true;
                this._DisplayUsersAndGroupsItemGroups = true;
                this._DisplayUsersAndGroupsItemRoles = true;
                this._DisplayUsersAndGroupsItemActivityImport = true;
                this._DisplayUsersAndGroupsItemCertificateImport = true;

                // leaderboards
                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSETOTAL_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSECREDITS_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATETOTAL_ENABLE)
                    || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATECREDITS_ENABLE))
                { this._DisplayUsersAndGroupsItemLeaderboards = true; }

                // INTERFACE AND LAYOUT

                this._DisplayInterfaceAndLayoutCategory = true;
                this._DisplayInterfaceAndLayoutItemHomePage = true;
                this._DisplayInterfaceAndLayoutItemMasthead = true;
                this._DisplayInterfaceAndLayoutItemFooter = true;
                this._DisplayInterfaceAndLayoutItemThemes = true;

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.IMAGE_EDITOR_ENABLE))
                { this._DisplayInterfaceAndLayoutItemImageEditor = true; }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CSSEDITOR_ENABLE))
                { this._DisplayInterfaceAndLayoutItemCSSEditor = true; }

                // LEARNING ASSETS

                this._DisplayLearningAssetsCategory = true;
                this._DisplayLearningAssetsItemCourseCatalog = true;
                this._DisplayLearningAssetsItemCourses = true;

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                { this._DisplayLearningAssetsItemLearningPaths = true; }

                this._DisplayLearningAssetsItemContentPackages = true;
                this._DisplayLearningAssetsItemCertificateTemplates = true;
                this._DisplayLearningAssetsItemStandupTraining = true;
                this._DisplayLearningAssetsItemResourceManagement = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE);

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                { this._DisplayLearningAssetsItemCertifications = true; }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE))
                { this._DisplayLearningAssetsItemQuizzesAndSurveys = true; }

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OPENSESAME_ENABLE))
                { this._DisplayLearningAssetsItemOpenSesameMarketplace = true; }

                // REPORTING

                this._DisplayReportingCategory = true;
                this._DisplayReportingItemReports = true;
                this._DisplayReportingItemAnalytics = true;
            }
            else // normal user, check permissions
            {
                // SYSTEM

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_AccountSettings))
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemAccountSettings = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Configuration))
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemConfiguration = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_UserFieldConfiguration))
                {
                    this._DisplayUsersAndGroupsCategory = true;
                    this._DisplayUsersAndGroupsItemUserFieldConfiguration = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Ecommerce) && this._EcommerceSettings.IsEcommerceEnabled)
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemEcommerce = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_CouponCodes) && this._EcommerceSettings.IsEcommerceSet)
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemCouponCodes = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_TrackingCodes))
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemTrackingCodes = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_EmailNotifications))
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemEmailNotifications = true;       
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_WebMeetingIntegration)
                    && ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTM_ENABLE)
                        || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTW_ENABLE)
                        || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_GTT_ENABLE)
                        || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.WEBMEETING_WEBEX_ENABLE)))
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemWebMeetingIntegration = true; 
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_API))
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemAPI = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_xAPIEndpoints))
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemxAPIEndpoints = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_Logs))
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemLogs = true;  
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.System_RulesEngine))
                {
                    this._DisplaySystemCategory = true;
                    this._DisplaySystemItemRulesEngine = true;
                }

                // USERS AND GROUPS

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserCreator)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserDeleter)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserEditor)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserManager)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_UserImpersonator)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
                {
                    this._DisplayUsersAndGroupsCategory = true;
                    this._DisplayUsersAndGroupsItemUsers = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupCreator)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupDeleter)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupEditor)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_GroupManager)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
                {
                    this._DisplayUsersAndGroupsCategory = true;
                    this._DisplayUsersAndGroupsItemGroups = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_RoleManager))
                {
                    this._DisplayUsersAndGroupsCategory = true;
                    this._DisplayUsersAndGroupsItemRoles = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_ActivityImport))
                {
                    this._DisplayUsersAndGroupsCategory = true;
                    this._DisplayUsersAndGroupsItemActivityImport = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_CertificateImport))
                {
                    this._DisplayUsersAndGroupsCategory = true;
                    this._DisplayUsersAndGroupsItemCertificateImport = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.UsersAndGroups_Leaderboards)
                    && ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSETOTAL_ENABLE)
                        || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_COURSECREDITS_ENABLE)
                        || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATETOTAL_ENABLE)
                        || (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEADERBOARDS_CERTIFICATECREDITS_ENABLE)))
                {
                    this._DisplayUsersAndGroupsCategory = true;
                    this._DisplayUsersAndGroupsItemLeaderboards = true;
                }

                // INTERFACE AND LAYOUT

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_HomePage))
                {
                    this._DisplayInterfaceAndLayoutCategory = true;
                    this._DisplayInterfaceAndLayoutItemHomePage = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_Masthead))
                {
                    this._DisplayInterfaceAndLayoutCategory = true;
                    this._DisplayInterfaceAndLayoutItemMasthead = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_Footer))
                {
                    this._DisplayInterfaceAndLayoutCategory = true;
                    this._DisplayInterfaceAndLayoutItemFooter = true;
                }

                if ((AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_CSSEditor)) &&
                    ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CSSEDITOR_ENABLE)))
                {
                    this._DisplayInterfaceAndLayoutCategory = true;
                    this._DisplayInterfaceAndLayoutItemThemes = true;
                    this._DisplayInterfaceAndLayoutItemCSSEditor = true;
                }

                if ((AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.InterfaceAndLayout_ImageEditor)) &&
                    ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.IMAGE_EDITOR_ENABLE)))
                {
                    this._DisplayInterfaceAndLayoutCategory = true;
                    this._DisplayInterfaceAndLayoutItemThemes = true;
                    this._DisplayInterfaceAndLayoutItemImageEditor = true;
                }

                // LEARNING ASSETS

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseCatalog))
                {
                    this._DisplayLearningAssetsCategory = true;
                    this._DisplayLearningAssetsItemCourseCatalog = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseContentManager) || 
                    AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CourseEnrollmentManager))
                {
                    this._DisplayLearningAssetsCategory = true;
                    this._DisplayLearningAssetsItemCourses = true;
                }

                if ((AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathContentManager) ||
                    AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_LearningPathEnrollmentManager)) &&
                    ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE)))
                {
                    this._DisplayLearningAssetsCategory = true;
                    this._DisplayLearningAssetsItemLearningPaths = true;
                }

                if (
                    AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CertificationsManager)
                    && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE)
                   )
                {
                    this._DisplayLearningAssetsCategory = true;
                    this._DisplayLearningAssetsItemCertifications = true;
                }

                if (
                    AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_QuizAndSurveyManager)
                    && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.QUIZZESANDSURVEYS_ENABLE)
                   )
                {
                    this._DisplayLearningAssetsCategory = true;
                    this._DisplayLearningAssetsItemQuizzesAndSurveys = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_ContentPackageManager))
                {
                    this._DisplayLearningAssetsCategory = true;
                    this._DisplayLearningAssetsItemContentPackages = true;

                    if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.OPENSESAME_ENABLE))
                    { this._DisplayLearningAssetsItemOpenSesameMarketplace = true; }
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_CertificateTemplateManager))
                {
                    this._DisplayLearningAssetsCategory = true;
                    this._DisplayLearningAssetsItemCertificateTemplates = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_InstructorLedTrainingManager))
                {
                    this._DisplayLearningAssetsCategory = true;
                    this._DisplayLearningAssetsItemStandupTraining = true;
                }

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.LearningAssets_ResourceManager))
                {
                    this._DisplayLearningAssetsCategory = true;
                    this._DisplayLearningAssetsItemResourceManagement = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE);
                }

                // REPORTING

                if (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserDemographics)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCourseTranscripts)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Certificates)
                    || AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_XAPI)
                    || (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Purchases) && this._EcommerceSettings.IsEcommerceSetAndVerified))
                {
                    this._DisplayReportingCategory = true;
                    this._DisplayReportingItemReports = true;
                }
            }

            // remove analytics for now, it is not done: TODO
            this._DisplayReportingItemAnalytics = false;
        }
        #endregion

        #region _BuildMenu
        /// <summary>
        /// Builds the administrator menu
        /// </summary>
        private void _BuildMenu()
        {
            this._AdministratorMenuCategoryIconsContainer.ID = "AdministratorMenuCategoryIconsContainer";

            if (this._DisplaySystemCategory)
            { 
                //this._AddCategoryPanelToControl(this._BuildSystemCategoryMenu());
                this._AddCategoryPanelToControl(this._BuildSystemCategoryMenuWithBlockDesign());
            }

            if (this._DisplayUsersAndGroupsCategory)
            {
                //this._AddCategoryPanelToControl(this._BuildUsersAndGroupsCategoryMenu());
                this._AddCategoryPanelToControl(this._BuildUsersAndGroupsCategoryMenuWithBlockDesign());
            }

            if (this._DisplayInterfaceAndLayoutCategory)
            {
                //this._AddCategoryPanelToControl(this._BuildInterfaceAndLayoutCategoryMenu());
                this._AddCategoryPanelToControl(this._BuildInterfaceAndLayoutCategoryMenuWithBlockDesign());
            }

            if (this._DisplayLearningAssetsCategory)
            {
                //this._AddCategoryPanelToControl(this._BuildLearningAssetsCategoryMenu());
                this._AddCategoryPanelToControl(this._BuildLearningAssetsCategoryMenuWithBlockDesign());
            }

            if (this._DisplayReportingCategory)
            {
                //this._AddCategoryPanelToControl(this._BuildReportingCategoryMenu());
                this._AddCategoryPanelToControl(this._BuildReportingCategoryMenuWithBlockDesign());
            }
        }
        #endregion

        #region _AddCategoryMenuItem
        /// <summary>
        /// Adds a menu item to a admin menu category
        /// </summary>
        /// <param name="identifier">menu item identifier</param>
        /// <param name="imagePath">path to the menu item image</param>
        /// <param name="label">menu item label</param>
        /// <param name="href">menu item link</param>
        /// <returns>menu item link</returns>
        private HyperLink _AddCategoryMenuItem(string identifier, string imagePath, string label, string href)
        {
            // create the link
            HyperLink itemLink = new HyperLink();
            itemLink.ID = identifier + "_CategoryMenuItemLink";
            itemLink.NavigateUrl = href;

            // create the container
            Panel itemContainer = new Panel();
            itemContainer.ID = identifier + "_CategoryMenuItemLinkContainer";
            itemContainer.CssClass = "AdministratorMenuCategoryMenuItem";

            // attach image to item container
            if (!String.IsNullOrWhiteSpace(imagePath))
            {
                Image itemLinkImage = new Image();
                itemLinkImage.CssClass = "XSmallIcon";
                itemLinkImage.ImageUrl = imagePath;
                itemContainer.Controls.Add(itemLinkImage);
            }

            // attach text to item container
            Literal itemLinkLabel = new Literal();
            itemLinkLabel.Text = label;
            itemContainer.Controls.Add(itemLinkLabel);

            // attach container to link
            itemLink.Controls.Add(itemContainer);

            // return link
            return itemLink;
        }
        #endregion

        #region _AddCategoryMenuItemMobile
        /// <summary>
        /// Adds a menu item to a category for mobile devices
        /// </summary>
        /// <param name="identifier">menu item identifier</param>
        /// <param name="imagePath">path to menu item image</param>
        /// <param name="label">menu item label</param>
        /// <param name="href">menu item link</param>
        /// <param name="blockLevel">block level</param>
        /// <param name="hasModifyButton">does the item have a modify button?</param>
        /// <param name="isEnabled">is the item enabled?</param>
        /// <returns>menu item panel</returns>
        private Panel _AddCategoryMenuItemMobile(string identifier, string imagePath, string label, string href, int blockLevel = 1, bool hasModifyButton = false, bool isEnabled = false)
        {
            // create the container
            Panel itemContainer = new Panel();
            itemContainer.ID = identifier + "_CategoryMenuItemLinkContainerMobile";
            itemContainer.CssClass = "AdministratorMenuCategoryMenuItemBlockMobile";
            itemContainer.Attributes.Add("onclick", "document.location.href = '" + href + "';");

            //add disabled class if feature isn't available to current user
            if (!isEnabled)
            {
                itemContainer.CssClass += " AdministratorMenuCategoryMenuItemBlockDisabled";
            }

            //create container that holds icon and label
            Panel iconLabelContainerMobile = new Panel();
            string blockLevelAddition = "";
            if (blockLevel == 2)
                blockLevelAddition = "LevelTwo";
            else if (blockLevel == 3)
                blockLevelAddition = "LevelThree";
            else if (blockLevel == 4)
                blockLevelAddition = "LevelFour";

            iconLabelContainerMobile.CssClass = "AdministratorMenuCategoryMenuItemBlock" + blockLevelAddition + "_IconLabelContainerMobile";

            //create and add the icon/image
            Panel iconContainerMobile = new Panel();
            iconContainerMobile.CssClass = "AdministratorMenuCategoryMenuItemBlock_IconContainerMobile";
            iconLabelContainerMobile.Controls.Add(iconContainerMobile);

            //add the (+) button, if applicable
            if (hasModifyButton)
            {
                Panel modifyButtonContainerMobile = new Panel();
                modifyButtonContainerMobile.CssClass = "AdministratorMenuCategoryMenuItemBlockMobile_AddButtonContainer";

                //Not using (+) for mobile thus far
            }

            // attach image to item container
            if (!String.IsNullOrWhiteSpace(imagePath))
            {
                Image itemLinkImage = new Image();
                itemLinkImage.CssClass = "AdministratorMenuCategoryMenuItemBlock_IconMobile XSmallIcon";
                itemLinkImage.ImageUrl = imagePath;
                iconContainerMobile.Controls.Add(itemLinkImage);
            }

            //create the label container
            Panel labelContainerMobile = new Panel();
            labelContainerMobile.CssClass = "AdministratorMenuCategoryMenuItemBlock_LabelMobile";
            iconLabelContainerMobile.Controls.Add(labelContainerMobile);

            // attach text to label container
            Literal itemLinkLabel = new Literal();
            itemLinkLabel.Text = label;
            labelContainerMobile.Controls.Add(itemLinkLabel);

            //add the constructed item to the main container
            itemContainer.Controls.Add(iconLabelContainerMobile);

            return itemContainer;
        }
        #endregion

        #region _AddCategoryMenuItemBlockMedium
        /// <summary>
        /// Adds a menu item block to a category (medium size)
        /// </summary>
        /// <param name="identifier">menu item identifier</param>
        /// <param name="imagePath">path to menu item image</param>
        /// <param name="label">menu item label</param>
        /// <param name="href">menu item link</param>
        /// <param name="leftOrRightFloat">floats left or right?</param>
        /// <param name="compoundType">compound type</param>
        /// <param name="hasModifyButton">does item have a modify button?</param>
        /// <param name="isEnabled">is the item enabled?</param>
        /// <param name="condenseHeight">should we condense the height of the menu item?</param>
        /// <returns>menu item panel</returns>
        private Panel _AddCategoryMenuItemBlockMedium(string identifier, string imagePath, string label, string href, string leftOrRightFloat, string compoundType = "none", bool hasModifyButton = false, bool isEnabled = false, bool condenseHeight = false)
        {
            // create hyperlink control to wrap everything in so that it can be opened in a new window
            HyperLink menuItemBlockHyperLink = new HyperLink();
            menuItemBlockHyperLink.ID = identifier + "_CategoryMenuItemBlockHyperLink";
            menuItemBlockHyperLink.NavigateUrl = href;

            //create the outer container
            Panel menuItemBlockContainer = new Panel();
            String menuItemBlockContainerClass = "AdministratorMenuCategoryMenuItemBlock AdministratorMenuCategoryMenuItemBlockMedium";
            
            //if specifically condensing the height for this block, add the class here
            if (condenseHeight)
            { menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemBlockMediumCondensedHeight"; }
            
            menuItemBlockContainer.ID = identifier + "_CategoryMenuItemBlock";

            //add disabled class if feature isn't available to current user
            if (!isEnabled)
            {
                menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemBlockDisabled";
            }

            //add whether it is left or right floated block
            if (leftOrRightFloat == "left")
            {
                menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemBlockMediumLeft";
            }
            else if (leftOrRightFloat == "right")
            {
                menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemBlockMediumRight";
            }

            //create the Modify (+) button, if applicable
            if (hasModifyButton)
            {
                Panel modifyButtonContainer = new Panel();
                modifyButtonContainer = this._AddModifyButton(identifier);

                //add the button to the block
                menuItemBlockContainer.Controls.Add(modifyButtonContainer);

                //based on the menu item, add the relevant context menu
                if (identifier == "UsersAndGroups_Roles" || identifier == "LearningAssets_LearningPaths" || identifier == "LearningAssets_Certifications" || identifier == "LearningAssets_CertificateTemplates" || identifier == "Reporting_Reports")
                {
                    modifyButtonContainer.Controls.Add(this._AddItemSpecificContextMenu(identifier));
                }

            }

            //create icon container (contains the icon and the label)
            Panel menuItemBlockContainerIconContainer = new Panel();
            menuItemBlockContainerIconContainer.CssClass = "AdministratorMenuCategoryMenuItemBlock_IconContainer";
            
            //add the image
            if (!String.IsNullOrWhiteSpace(imagePath))
            {
                Image menuItemBlockIcon = new Image();
                menuItemBlockIcon.CssClass = "AdministratorMenuCategoryMenuItemBlock_Icon";
                menuItemBlockIcon.ImageUrl = imagePath;
                menuItemBlockContainerIconContainer.Controls.Add(menuItemBlockIcon);
            }

            //create the label container & attach the label text
            Panel menuItemBlockContainerLabelContainer = new Panel();
            menuItemBlockContainerLabelContainer.CssClass = "AdministratorMenuCategoryMenuItemBlock_Label";
            Literal menuItemBlockLabel = new Literal();
            menuItemBlockLabel.Text = label;
            menuItemBlockContainerLabelContainer.Controls.Add(menuItemBlockLabel);

            //add the indicated box compound type
            String compoundClassDef = "";

            if (compoundType == "top")
            {
                compoundClassDef = " AdministratorMenuCategoryMenuItemBlockMediumCompoundTop";
            }
            else if (compoundType == "bottom")
            {
                compoundClassDef = " AdministratorMenuCategoryMenuItemBlockMediumCompoundBottom";
            }
            else if (compoundType == "both")
            {
                compoundClassDef = " AdministratorMenuCategoryMenuItemBlockMediumCompoundBoth";
            }

            menuItemBlockContainerClass += compoundClassDef;

            //add the final constructed class to the main container
            menuItemBlockContainer.CssClass = menuItemBlockContainerClass;

            // add the icon and label containers to the hyperlink
            menuItemBlockHyperLink.Controls.Add(menuItemBlockContainerIconContainer);
            menuItemBlockHyperLink.Controls.Add(menuItemBlockContainerLabelContainer);

            // add the hyperlink to the block
            menuItemBlockContainer.Controls.Add(menuItemBlockHyperLink);

            //return the block
            return menuItemBlockContainer;
        }
        #endregion

        #region _AddCategoryMenuItemBlockLarge
        /// <summary>
        /// Adds a menu item block to a category (large size)
        /// </summary>
        /// <param name="identifier">menu item identifier</param>
        /// <param name="imagePath">path to menu item image</param>
        /// <param name="label">menu item label</param>
        /// <param name="href">menu item link</param>
        /// <param name="hasModifyButton">does the menu item have a modify button?</param>
        /// <param name="isEnabled">is the menu item enabled?</param>
        /// <returns>menu item panel</returns>
        private Panel _AddCategoryMenuItemBlockLarge(string identifier, string imagePath, string label, string href, bool hasModifyButton = false, bool isEnabled = false)
        {
            // create hyperlink control to wrap everything in so that it can be opened in a new window
            HyperLink menuItemBlockHyperLink = new HyperLink();
            menuItemBlockHyperLink.ID = identifier + "_CategoryMenuItemBlockHyperLink";
            menuItemBlockHyperLink.NavigateUrl = href;

            //create the outer container
            Panel menuItemBlockContainer = new Panel();
            String menuItemBlockContainerClass = "AdministratorMenuCategoryMenuItemBlock AdministratorMenuCategoryMenuItemBlockLarge";
            menuItemBlockContainer.ID = identifier + "_CategoryMenuItemBlock";            

            //add disabled class if feature isn't available to current user
            if (!isEnabled)
            {
                menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemBlockDisabled";
            }

            //create the Modify (+) button, if applicable
            if (hasModifyButton)
            {
                Panel modifyButtonContainer = new Panel();
                modifyButtonContainer = this._AddModifyButton(identifier);

                //add the button to the block
                menuItemBlockContainer.Controls.Add(modifyButtonContainer);

                //based on the menu item, add the relevant context menu
                if (identifier == "UsersAndGroups_Groups")
                {
                    modifyButtonContainer.Controls.Add(this._AddItemSpecificContextMenu(identifier));
                }
            }

            //create icon container (contains the icon and the label)
            Panel menuItemBlockContainerIconContainer = new Panel();
            menuItemBlockContainerIconContainer.CssClass = "AdministratorMenuCategoryMenuItemBlock_IconContainer";

            //add the image
            if (!String.IsNullOrWhiteSpace(imagePath))
            {
                Image menuItemBlockIcon = new Image();
                menuItemBlockIcon.CssClass = "AdministratorMenuCategoryMenuItemBlock_Icon";
                menuItemBlockIcon.ImageUrl = imagePath;
                menuItemBlockContainerIconContainer.Controls.Add(menuItemBlockIcon);
            }

            //create the label container & attach the label text
            Panel menuItemBlockContainerLabelContainer = new Panel();
            menuItemBlockContainerLabelContainer.CssClass = "AdministratorMenuCategoryMenuItemBlock_Label";
            Literal menuItemBlockLabel = new Literal();
            menuItemBlockLabel.Text = label;
            menuItemBlockContainerLabelContainer.Controls.Add(menuItemBlockLabel);

            //add the final constructed class to the main container
            menuItemBlockContainer.CssClass = menuItemBlockContainerClass;

            // add the icon and label containers to the hyperlink
            menuItemBlockHyperLink.Controls.Add(menuItemBlockContainerIconContainer);
            menuItemBlockHyperLink.Controls.Add(menuItemBlockContainerLabelContainer);

            // add the hyperlink to the block
            menuItemBlockContainer.Controls.Add(menuItemBlockHyperLink);

            //return the block
            return menuItemBlockContainer;
        }
        #endregion

        #region _AddCategoryMenuItemBlockMediumWithChildren
        /// <summary>
        /// The Users & Groups Menu "Users" block/button must have its own custom function since it has an atypical format
        /// </summary>
        /// <param name="identifier">menu item identifier</param>
        /// <param name="imagePath">path to menu item image</param>
        /// <param name="label">menu item label</param>
        /// <param name="href">link to menu item</param>
        /// <param name="hasModifyButton">does the menu item have a modify button?</param>
        /// <param name="isEnabled">is the menu item enabled?</param>
        /// <param name="condenseHeight">should we condense the height of the menu button?</param>
        /// <returns>menu item panel</returns>
        private Panel _AddCategoryMenuItemBlockMediumWithChildren(string identifier, string imagePath, string label, string href, bool hasModifyButton = false, bool isEnabled = false, bool condenseHeight = false)
        {
            // create hyperlink control to wrap everything in so that it can be opened in a new window
            HyperLink menuItemBlockHyperLink = new HyperLink();
            menuItemBlockHyperLink.ID = identifier + "_CategoryMenuItemBlockHyperLink";
            menuItemBlockHyperLink.NavigateUrl = href;

            //create the outer container
            Panel menuItemBlockContainer = new Panel();
            String menuItemBlockContainerClass = "AdministratorMenuCategoryMenuItemBlock AdministratorMenuCategoryMenuItemBlockMedium AdministratorMenuCategoryMenuItemBlockMediumLeft";
            if (condenseHeight)
                menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemBlockMediumWithChildrenCondensedHeight";
            menuItemBlockContainer.ID = identifier + "_CategoryMenuItemBlock";            

            //add disabled class if feature isn't available to current user
            if (!isEnabled)
            {
                menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemBlockDisabled";
            }

            //create the Modify (+) button, if applicable
            if (hasModifyButton)
            {
                Panel modifyButtonContainer = new Panel();
                modifyButtonContainer = this._AddModifyButton(identifier);

                //add the button to the block
                menuItemBlockContainer.Controls.Add(modifyButtonContainer);

                //based on the menu item, add the relevant context menu
                if (identifier == "UsersAndGroups_Users" || identifier == "LearningAssets_Courses" || identifier == "LearningAssets_StandupTraining")
                {
                    modifyButtonContainer.Controls.Add(this._AddItemSpecificContextMenu(identifier));
                }

            }

            //create icon container (contains the icon and the label)
            Panel menuItemBlockContainerIconContainer = new Panel();
            menuItemBlockContainerIconContainer.CssClass = "AdministratorMenuCategoryMenuItemBlock_IconContainer";

            //lower the top margin if condense height enabled
            if (condenseHeight)
            {
                if (identifier == "LearningAssets_Courses")
                    menuItemBlockContainerIconContainer.CssClass += " AdministratorMenuCategoryMenuItemBlock_IconContainerCondensedHeightWithArrow";
                else
                    menuItemBlockContainerIconContainer.CssClass += " AdministratorMenuCategoryMenuItemBlock_IconContainerCondensedHeight";
            }

            //add the image
            if (!String.IsNullOrWhiteSpace(imagePath))
            {
                Image menuItemBlockIcon = new Image();
                menuItemBlockIcon.CssClass = "AdministratorMenuCategoryMenuItemBlock_Icon";
                //adjust margin if condense height enabled
                if (condenseHeight && identifier == "LearningAssets_StandupTraining")
                    menuItemBlockIcon.CssClass += " AdministratorMenuCategoryMenuItemBlock_IconCondensedHeight";
                menuItemBlockIcon.ImageUrl = imagePath;
                menuItemBlockContainerIconContainer.Controls.Add(menuItemBlockIcon);
            }

            //create the label container & attach the label text
            Panel menuItemBlockContainerLabelContainer = new Panel();
            menuItemBlockContainerLabelContainer.CssClass = "AdministratorMenuCategoryMenuItemBlock_Label";
            Literal menuItemBlockLabel = new Literal();
            menuItemBlockLabel.Text = label;
            menuItemBlockContainerLabelContainer.Controls.Add(menuItemBlockLabel);

            //add the final constructed class to the main container
            menuItemBlockContainer.CssClass = menuItemBlockContainerClass;

            //add the Icon Container to the main container
            //menuItemBlockContainer.Controls.Add(menuItemBlockContainerIconContainer);
            menuItemBlockHyperLink.Controls.Add(menuItemBlockContainerIconContainer);

            //add the Label container to the main container
            //menuItemBlockContainer.Controls.Add(menuItemBlockContainerLabelContainer);
            menuItemBlockHyperLink.Controls.Add(menuItemBlockContainerLabelContainer);

            //add components for the down-pointing arrow (only if not condensing the height)
            // Also, don't display down arrow if the current user doesn't have the permission to
            // access activity import, certificate import, or leaderboards
            if (!condenseHeight && 
                (this._DisplayUsersAndGroupsItemActivityImport || 
                this._DisplayUsersAndGroupsItemCertificateImport || 
                this._DisplayUsersAndGroupsItemLeaderboards)
                )
            {
                //container to hold the arrowstem
                Panel menuItemBlockForkedVerticalStemContainer = new Panel();
                menuItemBlockForkedVerticalStemContainer.CssClass = "AdministratorMenuCategoryMenuItem_ForkedVerticalStemsContainer AdministratorMenuCategoryMenuItem_SecondaryForkedVerticalStemsContainer";
                //menuItemBlockContainer.Controls.Add(menuItemBlockForkedVerticalStemContainer);

                //the arrow stem itself
                Panel menuItemBlockForkedVerticalStemForkedSmallLeft = new Panel();
                menuItemBlockForkedVerticalStemForkedSmallLeft.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemForkedSmallLeft";
                menuItemBlockForkedVerticalStemContainer.Controls.Add(menuItemBlockForkedVerticalStemForkedSmallLeft);

                //container to hold the arrow head
                Panel menuItemBlockForkedVerticalArrowheadContainer = new Panel();
                menuItemBlockForkedVerticalArrowheadContainer.CssClass = "AdministratorMenuCategoryMenuItem_ForkedVerticalArrowheadsContainer";
                //menuItemBlockContainer.Controls.Add(menuItemBlockForkedVerticalArrowheadContainer);

                //the arrow head itself
                Panel menuItemBlockForkedVerticalArrowheadForkedLeftContainer = new Panel();
                menuItemBlockForkedVerticalArrowheadForkedLeftContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowheadForkedLeft";
                menuItemBlockForkedVerticalArrowheadContainer.Controls.Add(menuItemBlockForkedVerticalArrowheadForkedLeftContainer);

                //add the arrow components                
                menuItemBlockHyperLink.Controls.Add(menuItemBlockForkedVerticalStemContainer);
                menuItemBlockHyperLink.Controls.Add(menuItemBlockForkedVerticalArrowheadContainer);
            }

            // add the hyperlink to the menu item block
            menuItemBlockContainer.Controls.Add(menuItemBlockHyperLink);

            //return the block
            return menuItemBlockContainer;
        }
        #endregion

        #region _AddCategoryMenuItemBlockSecondary
        /// <summary>
        /// Creates a first Child block that sits inside of another block
        /// </summary>
        /// <param name="identifier">menu item identifier</param>
        /// <param name="imagePath">path to menu item image</param>
        /// <param name="label">menu item label</param>
        /// <param name="href">menu item link</param>
        /// <param name="isFirstBlock">is this the first block in the sequence?</param>
        /// <param name="hasChild">does this block have a child?</param>
        /// <param name="hasModifyButton">does this item have a modify button?</param>
        /// <param name="isEnabled">is this item enabled?</param>
        /// <returns>menu item panel</returns>
        private Panel _AddCategoryMenuItemBlockSecondary(string identifier, string imagePath, string label, string href, bool isFirstBlock = false, bool hasChild = false, bool hasModifyButton = false, bool isEnabled = false)
        {
            // create hyperlink control to wrap everything in so that it can be opened in a new window
            HyperLink menuItemBlockHyperLink = new HyperLink();
            menuItemBlockHyperLink.ID = identifier + "_CategoryMenuItemBlockHyperLink";
            menuItemBlockHyperLink.NavigateUrl = href;

            //create the outer container
            Panel menuItemBlockContainer = new Panel();
            String menuItemBlockContainerClass = "AdministratorMenuCategoryMenuItemBlock AdministratorMenuCategoryMenuItemSecondaryBlock";
            menuItemBlockContainer.ID = identifier + "_CategoryMenuItemBlock";

            //the first secondary block will have a little extra top spacing
            if (isFirstBlock)
            {
                menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemSecondaryBlockFirst";
            }

            //add disabled class if feature isn't available to current user
            if (!isEnabled)
            {
                menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemBlockDisabled";
            }

            //create the Modify (+) button, if applicable
            if (hasModifyButton)
            {
                Panel modifyButtonContainer = new Panel();
                modifyButtonContainer = this._AddModifyButton(identifier);

                //add the button to the block
                menuItemBlockContainer.Controls.Add(modifyButtonContainer);

                //based on the menu item, add the relevant context menu
                if (identifier == "LearningAssets_QuizzesAndSurveys" || identifier == "LearningAssets_StandupTraining")
                {
                    modifyButtonContainer.Controls.Add(this._AddItemSpecificContextMenu(identifier));
                }
            }

            //create icon/label container (contains the icon and the label)
            Panel menuItemBlockContainerIconLabelContainer = new Panel();
            if (!hasChild)
                menuItemBlockContainerIconLabelContainer.CssClass = "AdministratorMenuCategoryMenuItemSecondaryBlock_IconLabelContainer";
            else
                menuItemBlockContainerIconLabelContainer.CssClass = "AdministratorMenuCategoryMenuItemSecondaryBlock_IconLabelContainerWithChild";

            //create the container that holds just the icon
            Panel menuItemBlockContainerIconContainer = new Panel();
            menuItemBlockContainerIconContainer.CssClass = "AdministratorMenuCategoryMenuItemSecondaryBlock_IconContainer";
            menuItemBlockContainerIconLabelContainer.Controls.Add(menuItemBlockContainerIconContainer);

            //add the image
            if (!String.IsNullOrWhiteSpace(imagePath))
            {
                Image menuItemBlockIcon = new Image();
                menuItemBlockIcon.CssClass = "AdministratorMenuCategoryMenuItemSecondaryBlock_Icon";
                menuItemBlockIcon.ImageUrl = imagePath;
                menuItemBlockContainerIconContainer.Controls.Add(menuItemBlockIcon);
            }

            //create the label container & attach the label text
            Panel menuItemBlockContainerLabelContainer = new Panel();
            menuItemBlockContainerLabelContainer.CssClass = "AdministratorMenuCategoryMenuItemSecondaryBlock_Label";
            Literal menuItemBlockLabel = new Literal();
            menuItemBlockLabel.Text = label;
            menuItemBlockContainerLabelContainer.Controls.Add(menuItemBlockLabel);
            menuItemBlockContainerIconLabelContainer.Controls.Add(menuItemBlockContainerLabelContainer);

            //add the final constructed class to the main container
            menuItemBlockContainer.CssClass = menuItemBlockContainerClass;

            // add the icon and label containers to the hyperlink
            menuItemBlockHyperLink.Controls.Add(menuItemBlockContainerIconLabelContainer);

            // add the menu item hyperlink to the block
            menuItemBlockContainer.Controls.Add(menuItemBlockHyperLink);

            //return the block
            return menuItemBlockContainer;
        }
        #endregion

        #region _AddCategoryMenuItemBlockTertiary
        /// <summary>
        /// Creates a third Child block that sits inside of another block (block that is 3 levels deep)
        /// </summary>
        /// <param name="identifier">menu item identifier</param>
        /// <param name="imagePath">path to menu item image</param>
        /// <param name="label">menu item label</param>
        /// <param name="href">link to menu item</param>
        /// <param name="isEnabled">is the menu item enabled?</param>
        /// <returns>menu item panel</returns>
        private Panel _AddCategoryMenuItemBlockTertiary(string identifier, string imagePath, string label, string href, bool isEnabled = false)
        {
            // create hyperlink control to wrap everything in so that it can be opened in a new window
            HyperLink menuItemBlockHyperLink = new HyperLink();
            menuItemBlockHyperLink.ID = identifier + "_CategoryMenuItemBlockHyperLink";
            menuItemBlockHyperLink.NavigateUrl = href;

            //create the outer container
            Panel menuItemBlockContainer = new Panel();
            String menuItemBlockContainerClass = "AdministratorMenuCategoryMenuItemTertiaryBlock_IconLabelContainerChild";
            menuItemBlockContainer.ID = identifier + "_CategoryMenuItemBlock";

            //add disabled class if feature isn't available to current user
            if (!isEnabled)
            {
                menuItemBlockContainerClass += " AdministratorMenuCategoryMenuItemBlockDisabled";
            }

            //create the arrow components
            Panel menuItemBlockArrowContainer = new Panel();
            menuItemBlockArrowContainer.CssClass = "AdministratorMenuCategoryMenuItemTertiaryBlock_ArrowContainer";
            menuItemBlockContainer.Controls.Add(menuItemBlockArrowContainer);

            //vertical arrow stem
            Panel menuItemBlockArrowStemVertical = new Panel();
            menuItemBlockArrowStemVertical.CssClass = "AdministratorMenuCategoryMenuItemTertiaryBlock_ArrowStemVertical";
            menuItemBlockArrowContainer.Controls.Add(menuItemBlockArrowStemVertical);

            //horizontal arrow stem
            Panel menuItemBlockArrowStemHorizontal = new Panel();
            menuItemBlockArrowStemHorizontal.CssClass = "AdministratorMenuCategoryMenuItemTertiaryBlock_ArrowStemHorizontal";
            menuItemBlockArrowContainer.Controls.Add(menuItemBlockArrowStemHorizontal);

            //add the arrow head
            Panel menuItemBlockArrowheadRight = new Panel();
            menuItemBlockArrowheadRight.CssClass = "AdministratorMenuCategoryMenuItemTertiaryBlock_ArrowheadRight";
            menuItemBlockArrowContainer.Controls.Add(menuItemBlockArrowheadRight);

            //create and add the image/icon
            if (!String.IsNullOrWhiteSpace(imagePath))
            {
                Image menuItemBlockIcon = new Image();
                menuItemBlockIcon.CssClass = "AdministratorMenuCategoryMenuItemTertiaryBlock_Icon";
                menuItemBlockIcon.ImageUrl = imagePath;
                menuItemBlockContainer.Controls.Add(menuItemBlockIcon);
                menuItemBlockHyperLink.Controls.Add(menuItemBlockIcon);
            }

            //create the label container & attach the label text
            Panel menuItemBlockContainerLabelContainer = new Panel();
            menuItemBlockContainerLabelContainer.CssClass = "AdministratorMenuCategoryMenuItemTertiaryBlock_Label";
            Literal menuItemBlockLabel = new Literal();
            menuItemBlockLabel.Text = label;
            menuItemBlockContainerLabelContainer.Controls.Add(menuItemBlockLabel);
            //menuItemBlockContainer.Controls.Add(menuItemBlockContainerLabelContainer);
            menuItemBlockHyperLink.Controls.Add(menuItemBlockContainerLabelContainer);

            //add the final constructed class to the main container
            menuItemBlockContainer.CssClass = menuItemBlockContainerClass;

            // add the hyperlink to the block
            menuItemBlockContainer.Controls.Add(menuItemBlockHyperLink);

            //return the block
            return menuItemBlockContainer;
        }
        #endregion

        #region _AddModifyButton
        /// <summary>
        /// Creates the "+" button that's attached to some of the block menu items
        /// </summary>
        /// <param name="identifier">button identifier</param>
        /// <returns>modify button panel</returns>
        private Panel _AddModifyButton(string identifier)
        {
            Panel modifyButtonContainer = new Panel();
            modifyButtonContainer.CssClass = "AdministratorMenuCategoryMenuItemBlock_AddButtonContainer";
            modifyButtonContainer.ID = identifier + "_ContextMenuTriggerAdminMenu";
            
            //add the onclick action for the button to show the context menu
            modifyButtonContainer.Attributes.Add("onclick", "ShowContextMenuForAdminMenuItem(event, this.id);");

            //create the image for the button
            if (!String.IsNullOrWhiteSpace(_IconPathModifyButton))
            {
                Image modifyButtonImage = new Image();
                modifyButtonImage.CssClass = "AdministratorMenuCategoryMenuItemBlock_AddButton";
                modifyButtonImage.ImageUrl = _IconPathModifyButton;
                modifyButtonContainer.Controls.Add(modifyButtonImage);
            }

            return modifyButtonContainer;
        }
        #endregion

        #region _AddItemSpecificContextMenu
        /// <summary>
        /// returns the respective Context Menu for when the user clicks the "+" button on an Admin menu item
        /// </summary>
        /// <param name="identifier">context menu identifier</param>
        /// <returns>context menu panel</returns>
        private Panel _AddItemSpecificContextMenu(string identifier)
        {
            //create the menu container
            Panel mainContextMenuItemContainer = new Panel();
            mainContextMenuItemContainer.CssClass = "AccordionMenuContextMenuContainer AdminMenuAccordionMenuContextMenuContainer";
            //give the menu container a unique id
            mainContextMenuItemContainer.ID = identifier + "_ContextMenuItemContainerAdminMenu";

            //based on the menu item, add the relevant context menu
            if (identifier == "UsersAndGroups_Groups")
            {
                //create the menu item link container
                Panel groupsContextMenuNewGroupItem = new Panel();
                groupsContextMenuNewGroupItem.ID = "NewGroupLink";

                //add the menu Item text
                Literal groupsContextMenuNewGroupItemText = new Literal();
                groupsContextMenuNewGroupItemText.Text = _GlobalResources.NewGroup;

                //create the HyperLink that wraps the menu item
                HyperLink groupsContextMenuNewGroupItemLink = new HyperLink();
                groupsContextMenuNewGroupItemLink.ID = "NewGroupLink_Hyperlink";
                groupsContextMenuNewGroupItemLink.NavigateUrl = this._HrefDestinationUsersAndGroupsItemGroups + "Modify.aspx";
                groupsContextMenuNewGroupItemLink.Controls.Add(groupsContextMenuNewGroupItemText);

                groupsContextMenuNewGroupItem.Controls.Add(groupsContextMenuNewGroupItemLink);

                mainContextMenuItemContainer.Controls.Add(groupsContextMenuNewGroupItem);
            }
            else if (identifier == "LearningAssets_LearningPaths")
            {
                //create the menu item link container
                Panel learningPathsContextMenuNewLearningPathItem = new Panel();
                learningPathsContextMenuNewLearningPathItem.ID = "NewLearningPathLink";

                //add the menu Item text
                Literal learningPathsContextMenuNewLearningPathItemText = new Literal();
                learningPathsContextMenuNewLearningPathItemText.Text = _GlobalResources.NewLearningPath;

                //create the HyperLink that wraps the menu item
                HyperLink learningPathsContextMenuNewLearningPathItemLink = new HyperLink();
                learningPathsContextMenuNewLearningPathItemLink.ID = "NewGroupLink_Hyperlink";
                learningPathsContextMenuNewLearningPathItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemLearningPaths + "Modify.aspx";
                learningPathsContextMenuNewLearningPathItemLink.Controls.Add(learningPathsContextMenuNewLearningPathItemText);

                learningPathsContextMenuNewLearningPathItem.Controls.Add(learningPathsContextMenuNewLearningPathItemLink);
                
                mainContextMenuItemContainer.Controls.Add(learningPathsContextMenuNewLearningPathItem);
            }
            else if (identifier == "UsersAndGroups_Roles")
            {
                //create the menu item link container
                Panel usersAndGroupsContextMenuNewRoleItem = new Panel();
                usersAndGroupsContextMenuNewRoleItem.ID = "NewRoleLink";

                //add the menu Item text
                Literal usersAndGroupsContextMenuNewRoleItemText = new Literal();
                usersAndGroupsContextMenuNewRoleItemText.Text = _GlobalResources.NewRole;

                //create the HyperLink that wraps the menu item
                HyperLink usersAndGroupsContextMenuNewRoleItemLink = new HyperLink();
                usersAndGroupsContextMenuNewRoleItemLink.ID = "NewRoleLink_Hyperlink";
                usersAndGroupsContextMenuNewRoleItemLink.NavigateUrl = this._HrefDestinationUsersAndGroupsItemRoles + "Modify.aspx";
                usersAndGroupsContextMenuNewRoleItemLink.Controls.Add(usersAndGroupsContextMenuNewRoleItemText);

                usersAndGroupsContextMenuNewRoleItem.Controls.Add(usersAndGroupsContextMenuNewRoleItemLink);

                mainContextMenuItemContainer.Controls.Add(usersAndGroupsContextMenuNewRoleItem);
            }
            else if (identifier == "LearningAssets_Certifications")
            {
                //create the menu item link container
                Panel certificationsContextMenuNewCertificationItem = new Panel();
                certificationsContextMenuNewCertificationItem.ID = "NewCertificationLink";

                //add the menu Item text
                Literal certificationsContextMenuNewCertificationItemText = new Literal();
                certificationsContextMenuNewCertificationItemText.Text = _GlobalResources.NewCertification;

                //create the HyperLink that wraps the menu item
                HyperLink certificationsContextMenuNewCertificationItemLink = new HyperLink();
                certificationsContextMenuNewCertificationItemLink.ID = "NewCertificationLink_Hyperlink";
                certificationsContextMenuNewCertificationItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemCertifications + "Modify.aspx";
                certificationsContextMenuNewCertificationItemLink.Controls.Add(certificationsContextMenuNewCertificationItemText);

                certificationsContextMenuNewCertificationItem.Controls.Add(certificationsContextMenuNewCertificationItemLink);

                mainContextMenuItemContainer.Controls.Add(certificationsContextMenuNewCertificationItem);
            }
            else if (identifier == "LearningAssets_CertificateTemplates")
            {
                //create the menu item link container
                Panel certificateTemplatesContextMenuNewCertificateTemplateItem = new Panel();
                certificateTemplatesContextMenuNewCertificateTemplateItem.ID = "NewCertificateTemplateLink";

                //add the menu Item text
                Literal certificateTemplatesContextMenuNewCertificateTemplateItemText = new Literal();
                certificateTemplatesContextMenuNewCertificateTemplateItemText.Text = _GlobalResources.NewCertificateTemplate;

                //create the HyperLink that wraps the menu item
                HyperLink certificateTemplatesContextMenuNewCertificateTemplateItemLink = new HyperLink();
                certificateTemplatesContextMenuNewCertificateTemplateItemLink.ID = "NewCertificateTemplateLink_Hyperlink";
                certificateTemplatesContextMenuNewCertificateTemplateItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemCertificateTemplates + "Modify.aspx";
                certificateTemplatesContextMenuNewCertificateTemplateItemLink.Controls.Add(certificateTemplatesContextMenuNewCertificateTemplateItemText);

                certificateTemplatesContextMenuNewCertificateTemplateItem.Controls.Add(certificateTemplatesContextMenuNewCertificateTemplateItemLink);

                mainContextMenuItemContainer.Controls.Add(certificateTemplatesContextMenuNewCertificateTemplateItem);
            }
            else if (identifier == "UsersAndGroups_Users")
            {
                //create the menu item link containers
                Panel usersAndGroupsContextMenuUserBatchItem = new Panel();
                usersAndGroupsContextMenuUserBatchItem.ID = "UserBatchLink";

                Panel usersAndGroupsContextMenuNewUserItem = new Panel();
                usersAndGroupsContextMenuNewUserItem.ID = "NewUserLink";

                //add the menu Item texts
                Literal usersAndGroupsContextMenuUserBatchItemText = new Literal();
                usersAndGroupsContextMenuUserBatchItemText.Text = _GlobalResources.UserBatch;

                Literal usersAndGroupsContextMenuNewUserItemText = new Literal();
                usersAndGroupsContextMenuNewUserItemText.Text = _GlobalResources.NewUser;

                //create the HyperLinks that wrap the menu item
                HyperLink usersAndGroupsContextMenuUserBatchItemLink = new HyperLink();
                usersAndGroupsContextMenuUserBatchItemLink.ID = "UserBatchLink_Hyperlink";
                usersAndGroupsContextMenuUserBatchItemLink.NavigateUrl = this._HrefDestinationUsersAndGroupsItemUsers + "BatchUpload.aspx";
                usersAndGroupsContextMenuUserBatchItemLink.Controls.Add(usersAndGroupsContextMenuUserBatchItemText);

                usersAndGroupsContextMenuUserBatchItem.Controls.Add(usersAndGroupsContextMenuUserBatchItemLink);

                HyperLink usersAndGroupsContextMenuNewUserItemLink = new HyperLink();
                usersAndGroupsContextMenuNewUserItemLink.ID = "NewUserLink_Hyperlink";
                usersAndGroupsContextMenuNewUserItemLink.NavigateUrl = this._HrefDestinationUsersAndGroupsItemUsers + "Modify.aspx";
                usersAndGroupsContextMenuNewUserItemLink.Controls.Add(usersAndGroupsContextMenuNewUserItemText);

                usersAndGroupsContextMenuNewUserItem.Controls.Add(usersAndGroupsContextMenuNewUserItemLink);

                mainContextMenuItemContainer.Controls.Add(usersAndGroupsContextMenuUserBatchItem);
                mainContextMenuItemContainer.Controls.Add(usersAndGroupsContextMenuNewUserItem);
            }
            else if (identifier == "LearningAssets_ContentPackages")
            {
                //create the menu item link containers
                Panel learningAssetsContextMenuUploadPackageItem = new Panel();
                learningAssetsContextMenuUploadPackageItem.ID = "UploadPackageLink";

                Panel learningAssetsContextMenuImportVideoItem = new Panel();
                learningAssetsContextMenuImportVideoItem.ID = "ImportVideoLink";

                Panel learningAssetsContextMenuImportPowerPointItem = new Panel();
                learningAssetsContextMenuImportPowerPointItem.ID = "ImportPowerPointLink";

                Panel learningAssetsContextMenuImportPDFItem = new Panel();
                learningAssetsContextMenuImportPDFItem.ID = "ImportPDFLink";

                //add the menu Item texts
                Literal learningAssetsContextMenuUploadPackageItemText = new Literal();
                learningAssetsContextMenuUploadPackageItemText.Text = _GlobalResources.UploadPackage;

                Literal learningAssetsContextMenuImportVideoItemText = new Literal();
                learningAssetsContextMenuImportVideoItemText.Text = _GlobalResources.ImportVideo;

                Literal learningAssetsContextMenuImportPowerPointItemText = new Literal();
                learningAssetsContextMenuImportPowerPointItemText.Text = _GlobalResources.ImportPowerPoint;

                Literal learningAssetsContextMenuImportPDFItemText = new Literal();
                learningAssetsContextMenuImportPDFItemText.Text = _GlobalResources.ImportPDF;

                //create the HyperLinks that wrap the menu item
                HyperLink learningAssetsContextMenuUploadPackageItemLink = new HyperLink();
                learningAssetsContextMenuUploadPackageItemLink.ID = "UploadPackageLink_Hyperlink";
                learningAssetsContextMenuUploadPackageItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemContentPackages;
                learningAssetsContextMenuUploadPackageItemLink.Controls.Add(learningAssetsContextMenuUploadPackageItemText);

                learningAssetsContextMenuUploadPackageItem.Controls.Add(learningAssetsContextMenuUploadPackageItemLink);

                HyperLink learningAssetsContextMenuImportVideoItemLink = new HyperLink();
                learningAssetsContextMenuImportVideoItemLink.ID = "ImportVideoLink_Hyperlink";
                learningAssetsContextMenuImportVideoItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemContentPackages;
                learningAssetsContextMenuImportVideoItemLink.Controls.Add(learningAssetsContextMenuImportVideoItemText);

                learningAssetsContextMenuImportVideoItem.Controls.Add(learningAssetsContextMenuImportVideoItemLink);

                HyperLink learningAssetsContextMenuImportPowerPointItemLink = new HyperLink();
                learningAssetsContextMenuImportPowerPointItemLink.ID = "ImportPowerPointLink_Hyperlink";
                learningAssetsContextMenuImportPowerPointItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemContentPackages;
                learningAssetsContextMenuImportPowerPointItemLink.Controls.Add(learningAssetsContextMenuImportPowerPointItemText);

                learningAssetsContextMenuImportPowerPointItem.Controls.Add(learningAssetsContextMenuImportPowerPointItemLink);

                HyperLink learningAssetsContextMenuImportPDFItemLink = new HyperLink();
                learningAssetsContextMenuImportPDFItemLink.ID = "ImportPDFLink_Hyperlink";
                learningAssetsContextMenuImportPDFItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemContentPackages;
                learningAssetsContextMenuImportPDFItemLink.Controls.Add(learningAssetsContextMenuImportPDFItemText);

                learningAssetsContextMenuImportPDFItem.Controls.Add(learningAssetsContextMenuImportPDFItemLink);

                //add the items to the menu
                mainContextMenuItemContainer.Controls.Add(learningAssetsContextMenuUploadPackageItem);
                mainContextMenuItemContainer.Controls.Add(learningAssetsContextMenuImportVideoItem);
                mainContextMenuItemContainer.Controls.Add(learningAssetsContextMenuImportPowerPointItem);
                mainContextMenuItemContainer.Controls.Add(learningAssetsContextMenuImportPDFItem);

            }
            else if (identifier == "LearningAssets_QuizzesAndSurveys")
            {
                // NEW QUIZ

                //create the menu item link container
                Panel learningAssetsContextMenuNewQuizItem = new Panel();
                learningAssetsContextMenuNewQuizItem.ID = "NewQuizLink";

                //add the menu Item text
                Literal learningAssetsContextMenuNewQuizItemText = new Literal();
                learningAssetsContextMenuNewQuizItemText.Text = _GlobalResources.NewQuiz;

                //create the HyperLink that wraps the menu item
                HyperLink learningAssetsContextMenuNewQuizItemLink = new HyperLink();
                learningAssetsContextMenuNewQuizItemLink.ID = "NewQuizLink_Hyperlink";
                learningAssetsContextMenuNewQuizItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemQuizzesAndSurveys + "Modify.aspx?type=quiz";
                learningAssetsContextMenuNewQuizItemLink.Controls.Add(learningAssetsContextMenuNewQuizItemText);

                learningAssetsContextMenuNewQuizItem.Controls.Add(learningAssetsContextMenuNewQuizItemLink);

                mainContextMenuItemContainer.Controls.Add(learningAssetsContextMenuNewQuizItem);

                // NEW SURVEY

                //create the menu item link container
                Panel learningAssetsContextMenuNewSurveyItem = new Panel();
                learningAssetsContextMenuNewSurveyItem.ID = "NewSurveyLink";

                //add the menu Item text
                Literal learningAssetsContextMenuNewSurveyItemText = new Literal();
                learningAssetsContextMenuNewSurveyItemText.Text = _GlobalResources.NewSurvey;

                //create the HyperLink that wraps the menu item
                HyperLink learningAssetsContextMenuNewSurveyItemLink = new HyperLink();
                learningAssetsContextMenuNewSurveyItemLink.ID = "NewSurveyLink_Hyperlink";
                learningAssetsContextMenuNewSurveyItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemQuizzesAndSurveys + "Modify.aspx?type=survey";
                learningAssetsContextMenuNewSurveyItemLink.Controls.Add(learningAssetsContextMenuNewSurveyItemText);

                learningAssetsContextMenuNewSurveyItem.Controls.Add(learningAssetsContextMenuNewSurveyItemLink);

                mainContextMenuItemContainer.Controls.Add(learningAssetsContextMenuNewSurveyItem);
            }
            else if (identifier == "LearningAssets_StandupTraining")
            {
                //create the menu item link container
                Panel learningAssetsContextMenuNewInstructorLedTrainingItem = new Panel();
                learningAssetsContextMenuNewInstructorLedTrainingItem.ID = "NewInstructorLedTrainingLink";

                //add the menu Item text
                Literal learningAssetsContextMenuNewInstructorLedTrainingItemText = new Literal();
                learningAssetsContextMenuNewInstructorLedTrainingItemText.Text = _GlobalResources.NewInstructorLedTrainingModule;

                //create the HyperLink that wraps the menu item
                HyperLink learningAssetsContextMenuNewInstructorLedTrainingItemLink = new HyperLink();
                learningAssetsContextMenuNewInstructorLedTrainingItemLink.ID = "NewInstructorLedTrainingLink_Hyperlink";
                learningAssetsContextMenuNewInstructorLedTrainingItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemStandupTraining + "Modify.aspx";
                learningAssetsContextMenuNewInstructorLedTrainingItemLink.Controls.Add(learningAssetsContextMenuNewInstructorLedTrainingItemText);

                learningAssetsContextMenuNewInstructorLedTrainingItem.Controls.Add(learningAssetsContextMenuNewInstructorLedTrainingItemLink);

                mainContextMenuItemContainer.Controls.Add(learningAssetsContextMenuNewInstructorLedTrainingItem);
            }
            else if (identifier == "LearningAssets_Courses")
            {
                //create the menu item link container
                Panel learningAssetsContextMenuNewCourseItem = new Panel();
                learningAssetsContextMenuNewCourseItem.ID = "NewCourseLink";

                //add the menu Item text
                Literal learningAssetsContextMenuNewCourseItemText = new Literal();
                learningAssetsContextMenuNewCourseItemText.Text = _GlobalResources.NewCourse;

                //create the HyperLink that wraps the menu item
                HyperLink learningAssetsContextMenuNewCourseItemLink = new HyperLink();
                learningAssetsContextMenuNewCourseItemLink.ID = "NewCourseLink_Hyperlink";
                learningAssetsContextMenuNewCourseItemLink.NavigateUrl = this._HrefDestinationLearningAssetsItemCourses + "Modify.aspx";
                learningAssetsContextMenuNewCourseItemLink.Controls.Add(learningAssetsContextMenuNewCourseItemText);

                learningAssetsContextMenuNewCourseItem.Controls.Add(learningAssetsContextMenuNewCourseItemLink);

                mainContextMenuItemContainer.Controls.Add(learningAssetsContextMenuNewCourseItem);
            }
            else if (identifier == "Reporting_Reports")
            {
                //Reports context items must be created dynamically based on the available datasets
                DataTable dtDataSet = new DataTable();
                dtDataSet = GetReportDataSetListTableForAdminMenu();

                //build the context menu item based on the dataset items
                foreach (DataRow row in dtDataSet.Rows)
                {
                    if (
                        (Convert.ToInt32(row["idDataset"]) == 1 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserDemographics))
                        || (Convert.ToInt32(row["idDataset"]) == 2 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCourseTranscripts))
                        || (Convert.ToInt32(row["idDataset"]) == 3 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation))
                        || (Convert.ToInt32(row["idDataset"]) == 4 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Certificates))
                        //|| (Convert.ToInt32(row["idDataset"]) == 5 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_XAPI))                    
                        || (Convert.ToInt32(row["idDataset"]) == 6 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts))
                        || (Convert.ToInt32(row["idDataset"]) == 7 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts))
                        || (Convert.ToInt32(row["idDataset"]) == 8 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Purchases) && this._EcommerceSettings.IsEcommerceSetAndVerified)
                        || (Convert.ToInt32(row["idDataset"]) == 9 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                        || (Convert.ToInt32(row["idDataset"]) >= 1000) // custom datasets
                       )
                    {
                        //create the container for the link
                        Panel reportsContextMenuReportItem = new Panel();
                        reportsContextMenuReportItem.ID = "NewReportLink" + row["idDataset"].ToString();

                        //create the text for the menu item
                        Literal reportsContextMenuReportItemText = new Literal();
                        reportsContextMenuReportItemText.Text = String.Format(_GlobalResources.NewXReport, row["name"].ToString()); 

                        //create the HyperLink that wraps the menu item
                        HyperLink reportsContextMenuReportItemLink = new HyperLink();
                        reportsContextMenuReportItemLink.ID = "NewReportLink" + row["idDataset"].ToString() + "_Hyperlink";
                        reportsContextMenuReportItemLink.NavigateUrl = this._HrefDestinationReportingItemReports + "Modify.aspx?idDataset=" + row["idDataset"].ToString();
                        reportsContextMenuReportItemLink.Controls.Add(reportsContextMenuReportItemText);

                        //add the link to the main item
                        reportsContextMenuReportItem.Controls.Add(reportsContextMenuReportItemLink);

                        //finally, add the main item to the context menu
                        mainContextMenuItemContainer.Controls.Add(reportsContextMenuReportItem);
                    }

                }
            }

            return mainContextMenuItemContainer;
        }

        #endregion

        #region _AddCategoryPanelToControl
        /// <summary>
        /// Adds a category panel to a control
        /// </summary>
        /// <param name="categoryPanel">category panel</param>
        private void _AddCategoryPanelToControl(Panel categoryPanel)
        {
            if (categoryPanel != null)
            { this.Controls.Add(categoryPanel); }
        }
        #endregion

        #region _BuildIconForCategoryMenu
        /// <summary>
        /// Builds an icon for a category menu
        /// </summary>
        /// <param name="categoryIdentifier">category identifier</param>
        /// <param name="iconPath">path to icon</param>
        /// <param name="iconOnPath">icon on path</param>
        /// <param name="iconAltText">icon alternate text</param>
        private void _BuildIconForCategoryMenu(string categoryIdentifier, string iconPath, string iconOnPath, string iconAltText)
        {
            // build category icon container
            Panel categoryIconContainer = new Panel();
            categoryIconContainer.ID = categoryIdentifier + "_CategoryIconContainer";
            categoryIconContainer.CssClass = "AdministratorMenuCategoryIconContainer";

            // attach category image to container
            Image categoryIcon = new Image();
            categoryIcon.ID = categoryIdentifier + "_CategoryIcon";
            categoryIcon.ImageUrl = iconPath;
            categoryIcon.CssClass = "MediumIcon";
            categoryIcon.AlternateText = iconAltText;
            categoryIconContainer.Controls.Add(categoryIcon);

            this._AdministratorMenuCategoryIconsContainer.Controls.Add(categoryIconContainer);
        }
        #endregion

        #region _BuildJSActions
        /// <summary>
        /// Builds the javascript actions to animate opening and closing the admin menu
        /// </summary>
        private void _BuildJSActions()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("$(document).ready(function () {");
            sb.AppendLine("");
            sb.AppendLine("$(\"#ContentContainer\").append(\"<div id=\\\"AdministratorMenuPageContentBlackout\\\"></div>\");");
            sb.AppendLine("");
            sb.AppendLine("");
            sb.AppendLine("    $(\".AdministratorMenuCategoryMenuItemsContainer\").each(function(){");
            sb.AppendLine("");            
            sb.AppendLine("        $(\"#\" + this.id).mouseleave(function(event){");
            //hide any open context menus
            sb.AppendLine("            $(\".AdminMenuAccordionMenuContextMenuContainer\").fadeOut(200);");
            sb.AppendLine("");
            sb.AppendLine("            var categoryId = this.id.replace(\"_CategoryMenuItemsContainer\", \"\");");
            sb.AppendLine("            try {");
            sb.AppendLine("                 if(!event.relatedTarget.id.startsWith(categoryId + \"_\")){");
            sb.AppendLine("                     var categoryIconContainerId = this.id.replace(\"_CategoryMenuItemsContainer\", \"_CategoryIconContainer\");");
            sb.AppendLine("");
            sb.AppendLine("                     $(\"#\" + categoryIconContainerId).removeClass(\"AdministratorMenuSelectedCategory\");");
            sb.AppendLine("                     var categoryMenuItemContainerWidth = $(\"#\" + this.id).width();");
            sb.AppendLine("                     if(!$(\"#\" + event.relatedTarget.id).hasClass(\"AdministratorMenuCategoryIconContainer\")){");
            sb.AppendLine("                         $(\"#AdministratorMenuPageContentBlackout\").fadeOut();");
            sb.AppendLine("                     }");
            sb.AppendLine("                     $(\"#\" + this.id).removeClass(\"adminMenuGroupExpanded\");");
            sb.AppendLine("                     $(\"#\" + this.id).addClass(\"adminMenuGroupCollapsed\");");
            sb.AppendLine("                 }");
            sb.AppendLine("            }catch(e){/*id is null*/};");
            sb.AppendLine("        });");
            sb.AppendLine("");
            sb.AppendLine("     });");           
            sb.AppendLine("");
            sb.AppendLine("");
            sb.AppendLine("    $(\".AdministratorMenuCategoryIconContainer\").each(function () {");
            sb.AppendLine("");            
            sb.AppendLine("        $(\"#\" + this.id).click(function (event) {");            
            sb.AppendLine("");
            sb.AppendLine("            var categoryIcon = this;");
            sb.AppendLine("");
            sb.AppendLine("            $(\".AdministratorMenuCategoryMenuItemsContainer\").each(function () {");
            sb.AppendLine("");
            sb.AppendLine("                var categoryIdFromIcon = categoryIcon.id.replace(\"_CategoryIconContainer\", \"\");");
            sb.AppendLine("                var categoryIdFromMenuItemsContainer = this.id.replace(\"_CategoryMenuItemsContainer\", \"\");");
            sb.AppendLine("                var categoryIconContainerId = this.id.replace(\"_CategoryMenuItemsContainer\", \"_CategoryIconContainer\");");
            sb.AppendLine("");
            sb.AppendLine("                $(\"#\" + categoryIconContainerId).removeClass(\"AdministratorMenuSelectedCategory\");");
            sb.AppendLine("");
            sb.AppendLine("                var categoryIconsContainerWidth = $(\"#AdministratorMenuCategoryIconsContainer\").outerWidth(true);");
            sb.AppendLine("                var categoryMenuItemContainerWidth = $(\"#\" + this.id).width();");
            sb.AppendLine("");
            //collapse all other menu items that are NOT this this icon's menu
            sb.AppendLine("                if ($(\"#\" + this.id).hasClass(\"adminMenuGroupExpanded\") && categoryIdFromIcon != categoryIdFromMenuItemsContainer) {");
            sb.AppendLine("                    $(\"#\" + this.id).removeClass(\"adminMenuGroupExpanded\");");
            sb.AppendLine("                    $(\"#\" + this.id).addClass(\"adminMenuGroupCollapsed\");");
            sb.AppendLine("                }");
            sb.AppendLine("");
            sb.AppendLine("                else {");
            sb.AppendLine("");
            //expand the menu that IS this icon's menu
            sb.AppendLine("                    if (categoryIdFromIcon == categoryIdFromMenuItemsContainer) {");
            sb.AppendLine("");
            sb.AppendLine("                        if ($(\"#\" + this.id).hasClass(\"adminMenuGroupCollapsed\")) {");
            sb.AppendLine("");
            sb.AppendLine("                            $(\"#AdministratorMenuPageContentBlackout\").fadeIn();");
            sb.AppendLine("                            $(\"#\" + this.id).removeClass(\"adminMenuGroupCollapsed\");");
            sb.AppendLine("                            $(\"#\" + this.id).addClass(\"adminMenuGroupExpanded\");");
            sb.AppendLine("                            $(\"#\" + categoryIcon.id).addClass(\"AdministratorMenuSelectedCategory\");");
            sb.AppendLine("");
            sb.AppendLine("                        }");
            sb.AppendLine("");
            sb.AppendLine("                    }");
            sb.AppendLine("");
            sb.AppendLine("                }");
            sb.AppendLine("");
            sb.AppendLine("            });");
            sb.AppendLine("");
            sb.AppendLine("        });");
            sb.AppendLine("");
            sb.AppendLine("    });");
            sb.AppendLine("");
            sb.AppendLine("    $(\"#AdministratorMenuPageContentBlackout\").click(function () {");
            sb.AppendLine("");
            sb.AppendLine("        $(\".AdministratorMenuCategoryMenuItemsContainer\").each(function () {");
            sb.AppendLine("");
            sb.AppendLine("            var categoryIconContainerId = this.id.replace(\"_CategoryMenuItemsContainer\", \"_CategoryIconContainer\");");
            sb.AppendLine("            $(\"#\" + categoryIconContainerId).removeClass(\"AdministratorMenuSelectedCategory\");");
            sb.AppendLine("");
            sb.AppendLine("            var categoryMenuItemContainerWidth = $(\"#\" + this.id).width();");
            sb.AppendLine("");
            sb.AppendLine("            if ($(\"#\" + this.id).hasClass(\"adminMenuGroupExpanded\")) {");
            sb.AppendLine("                 $(\"#AdministratorMenuPageContentBlackout\").fadeOut();");
            sb.AppendLine("                 $(\"#\" + this.id).removeClass(\"adminMenuGroupExpanded\");");
            sb.AppendLine("                 $(\"#\" + this.id).addClass(\"adminMenuGroupCollapsed\");");
            sb.AppendLine("            }");
            sb.AppendLine("");
            sb.AppendLine("        });");
            sb.AppendLine("");
            sb.AppendLine("    });");            
            sb.AppendLine("");
            sb.AppendLine("});");

            sb.AppendLine("  function ShowContextMenuForAdminMenuItem(event, contextMenuTriggerId) { ");
            sb.AppendLine("     event.stopPropagation();");
            sb.AppendLine("     var contextMenuId = contextMenuTriggerId.replace('Trigger', 'ItemContainer'); ");
            sb.AppendLine("     $('#' + contextMenuId).show(); ");
            sb.AppendLine("     $('#' + contextMenuId).hover(function () { }, function () { ");
            sb.AppendLine("     $('#' + contextMenuId).fadeOut(200); ");
            sb.AppendLine("  }); ");
            sb.AppendLine("} ");

            this._JavascriptTag.InnerHtml += sb.ToString();
        }
        #endregion

        #region _BuildSystemCategoryMenu
        /// <summary>
        /// Builds the system category menu
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildSystemCategoryMenu()
        {
            int menuItemsBuilt = 0;

            Panel systemCategoryMenuContainer = new Panel();
            systemCategoryMenuContainer.ID = "System_CategoryMenuItemsContainer";
            systemCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer";

            Panel systemCategoryMenuHeaderContainer = new Panel();
            systemCategoryMenuHeaderContainer.ID = "System_CategoryMenuItemsHeaderContainer";
            systemCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize systemCategoryMenuHeader = new Localize();
            systemCategoryMenuHeader.Text = _GlobalResources.System;
            systemCategoryMenuHeaderContainer.Controls.Add(systemCategoryMenuHeader);

            systemCategoryMenuContainer.Controls.Add(systemCategoryMenuHeaderContainer);

            // add individual category menu items
            if (this._DisplaySystemItemAccountSettings)
            { 
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_AccountSettings", ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY, ImageFiles.EXT_PNG), _GlobalResources.AccountSettings, this._HrefDestinationSystemItemAccountSettings)); 
            }

            if (this._DisplaySystemItemConfiguration)
            { 
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_Configuration", ImageFiles.GetIconPath(ImageFiles.ICON_CONFIGURATION, ImageFiles.EXT_PNG), _GlobalResources.Configuration, this._HrefDestinationSystemItemConfiguration));
            }

            if (this._DisplaySystemItemConfiguration)
            {
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_UserFieldConfiguration", ImageFiles.GetIconPath(ImageFiles.ICON_USERACCOUNTDATA, ImageFiles.EXT_PNG), _GlobalResources.UserFieldConfiguration, this._HrefDestinationSystemItemUserFieldConfiguration));
            }

            if (this._DisplaySystemItemEcommerce)
            {
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_Ecommerce", ImageFiles.GetIconPath(ImageFiles.ICON_ECOMMERCE, ImageFiles.EXT_PNG), _GlobalResources.ECommerce, this._HrefDestinationSystemItemEcommerce));
            }

            if (this._DisplaySystemItemCouponCodes)
            {
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_CouponCodes", ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG), _GlobalResources.CouponCodes, this._HrefDestinationSystemItemCouponCodes));
            }

            if (this._DisplaySystemItemRulesEngine)
            {
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_RulesEngine", ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG), _GlobalResources.RulesEngine, this._HrefDestinationSystemItemCouponCodes));
            }

            if (this._DisplaySystemItemTrackingCodes)
            {
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_TrackingCodes", ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS, ImageFiles.EXT_PNG), _GlobalResources.TrackingCodes, this._HrefDestinationSystemItemTrackingCodes));
            }

            if (this._DisplaySystemItemEmailNotifications)
            {
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_EmailNotifications", ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG), _GlobalResources.EmailNotifications, this._HrefDestinationSystemItemEmailNotifications));
            }

            if (this._DisplaySystemItemWebMeetingIntegration)
            {
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_WebMeetingIntegration", ImageFiles.GetIconPath(ImageFiles.ICON_WEBMEETING, ImageFiles.EXT_PNG), _GlobalResources.WebMeetingIntegration, this._HrefDestinationSystemItemWebMeetingIntegration));
            }

            if (this._DisplaySystemItemAPI)
            { 
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_API", ImageFiles.GetIconPath(ImageFiles.ICON_API, ImageFiles.EXT_PNG), _GlobalResources.API, this._HrefDestinationSystemItemAPI));
            }

            if (this._DisplaySystemItemxAPIEndpoints)
            {
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_xAPIEndpoints", ImageFiles.GetIconPath(ImageFiles.ICON_API, ImageFiles.EXT_PNG), _GlobalResources.xAPIEndpoints, this._HrefDestinationSystemItemxAPIEndpoints));
            }

            if (this._DisplaySystemItemLogs)
            { 
                menuItemsBuilt++;
                systemCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("System_Logs", ImageFiles.GetIconPath(ImageFiles.ICON_LOG, ImageFiles.EXT_PNG), _GlobalResources.Logs, this._HrefDestinationSystemItemLogs));
            }

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("System", this._IconPathSystem, this._IconPathSystemOn, _GlobalResources.System);

            // return the built menu
            return systemCategoryMenuContainer;
        }
        #endregion

        #region _BuildSystemCategoryMenuWithBlockDesign
        /// <summary>
        /// Builds the system category menu (new version with flow lines)
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildSystemCategoryMenuWithBlockDesign()
        {
            int menuItemsBuilt = 0;
            int menuItemsBuiltLeft = 0;
            int menuItemsBuiltRight = 0;

            Panel systemCategoryMenuContainer = new Panel();
            systemCategoryMenuContainer.ID = "System_CategoryMenuItemsContainer";
            systemCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer adminMenuGroupCollapsed";

            Panel systemCategoryMenuHeaderContainer = new Panel();
            systemCategoryMenuHeaderContainer.ID = "System_CategoryMenuItemsHeaderContainer";
            systemCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize systemCategoryMenuHeader = new Localize();
            systemCategoryMenuHeader.Text = _GlobalResources.System;
            systemCategoryMenuHeaderContainer.Controls.Add(systemCategoryMenuHeader);

            systemCategoryMenuContainer.Controls.Add(systemCategoryMenuHeaderContainer);

            //add the overall blocks container
            Panel systemCategoryMenuItemBlocksContainer = new Panel();
            systemCategoryMenuItemBlocksContainer.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer";

            //create the left-side blocks container
            Panel systemCategoryMenuItemBlocksContainerLeft = new Panel();
            systemCategoryMenuItemBlocksContainerLeft.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderCondensedHeight";

            //create the right-side blocks container
            Panel systemCategoryMenuItemBlocksContainerRight = new Panel();
            systemCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderRight AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderCondensedHeight";

            //add the left/right-side blocks to the overall container
            systemCategoryMenuItemBlocksContainer.Controls.Add(systemCategoryMenuItemBlocksContainerLeft);
            systemCategoryMenuItemBlocksContainer.Controls.Add(systemCategoryMenuItemBlocksContainerRight);

            //add the mobile version container
            Panel systemCategoryMenuItemBlocksContainerMobile = new Panel();
            systemCategoryMenuItemBlocksContainerMobile.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainerMobile";

            // add individual category menu items
            if (this._DisplaySystemItemAccountSettings)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                systemCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_AccountSettings", ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY, ImageFiles.EXT_PNG), _GlobalResources.AccountSettings, this._HrefDestinationSystemItemAccountSettings, "left", "bottom", false, this._DisplaySystemItemAccountSettings));
            }

            if (this._DisplaySystemItemConfiguration)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                systemCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_Configuration", ImageFiles.GetIconPath(ImageFiles.ICON_CONFIGURATION, ImageFiles.EXT_PNG), _GlobalResources.Configuration, this._HrefDestinationSystemItemConfiguration, "left", "both", false, this._DisplaySystemItemConfiguration));
            }

            if (this._DisplayUsersAndGroupsItemUserFieldConfiguration)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                systemCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_UserFieldConfiguration", ImageFiles.GetIconPath(ImageFiles.ICON_USERACCOUNTDATA, ImageFiles.EXT_PNG), _GlobalResources.UserFieldConfiguration, this._HrefDestinationSystemItemUserFieldConfiguration, "left", "both", false, this._DisplayUsersAndGroupsItemUserFieldConfiguration));
            }

            if (this._DisplaySystemItemEmailNotifications)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                //if no bottom item, this must not be compound to keep unrelated blocks from sticking to it's bottom edge
                string compoundType = "both";

                if (!this._DisplaySystemItemWebMeetingIntegration)
                    compoundType = "top";

                systemCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_EmailNotifications", ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG), _GlobalResources.EmailNotifications, this._HrefDestinationSystemItemEmailNotifications, "left", compoundType, false, this._DisplaySystemItemEmailNotifications));
            }

            if (this._DisplaySystemItemWebMeetingIntegration)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                systemCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_WebMeetingIntegration", ImageFiles.GetIconPath(ImageFiles.ICON_WEBMEETING, ImageFiles.EXT_PNG), _GlobalResources.WebMeetingIntegration, this._HrefDestinationSystemItemWebMeetingIntegration, "left", "top", false, this._DisplaySystemItemWebMeetingIntegration));
            }

            if (this._DisplaySystemItemLogs)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                systemCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_Logs", ImageFiles.GetIconPath(ImageFiles.ICON_LOG, ImageFiles.EXT_PNG), _GlobalResources.Logs, this._HrefDestinationSystemItemLogs, "left", "none", false, this._DisplaySystemItemLogs));
            }

            if (this._DisplaySystemItemEcommerce)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                //if no bottom item, this must not be compound to keep unrelated blocks from sticking to it's bottom edge
                string compoundType = "bottom";

                if (!this._DisplaySystemItemCouponCodes)
                    compoundType = "none";

                systemCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_Ecommerce", ImageFiles.GetIconPath(ImageFiles.ICON_ECOMMERCE, ImageFiles.EXT_PNG), _GlobalResources.ECommerce, this._HrefDestinationSystemItemEcommerce, "right", compoundType, false, this._DisplaySystemItemEcommerce));
            }

            if (this._DisplaySystemItemCouponCodes)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                systemCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_CouponCodes", ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG), _GlobalResources.CouponCodes, this._HrefDestinationSystemItemCouponCodes, "right", "top", false, this._DisplaySystemItemCouponCodes));
            }

            if (this._DisplaySystemItemRulesEngine)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                systemCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_RulesEngine", ImageFiles.GetIconPath(ImageFiles.ICON_RULESET, ImageFiles.EXT_PNG), _GlobalResources.RulesEngine, this._HrefDestinationSystemItemRulesEngine, "right", "none", false, this._DisplaySystemItemRulesEngine));
            }

            if (this._DisplaySystemItemTrackingCodes)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                systemCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_TrackingCodes", ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS, ImageFiles.EXT_PNG), _GlobalResources.TrackingCodes, this._HrefDestinationSystemItemTrackingCodes, "right", "none", false, this._DisplaySystemItemTrackingCodes));
            }

            if (this._DisplaySystemItemAPI)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                string compoundType = "top";

                if (!this._DisplaySystemItemxAPIEndpoints)
                    compoundType = "none";

                systemCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_API", ImageFiles.GetIconPath(ImageFiles.ICON_API, ImageFiles.EXT_PNG), _GlobalResources.API, this._HrefDestinationSystemItemAPI, "right", compoundType, false, this._DisplaySystemItemAPI));
            }

            if (this._DisplaySystemItemxAPIEndpoints)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                systemCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("System_xAPIEndpoints", ImageFiles.GetIconPath(ImageFiles.ICON_API, ImageFiles.EXT_PNG), _GlobalResources.xAPIEndpoints, this._HrefDestinationSystemItemxAPIEndpoints, "right", "top", false, this._DisplaySystemItemxAPIEndpoints));
            }

            //mobile items (grouped here for ordering purposes)
            if (this._DisplaySystemItemAccountSettings)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_AccountSettings", ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY, ImageFiles.EXT_PNG), _GlobalResources.AccountSettings, this._HrefDestinationSystemItemAccountSettings, 1, false, this._DisplaySystemItemAccountSettings));
            if (this._DisplayUsersAndGroupsItemUserFieldConfiguration)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_UserFieldConfiguration", ImageFiles.GetIconPath(ImageFiles.ICON_USERACCOUNTDATA, ImageFiles.EXT_PNG), _GlobalResources.UserFieldConfiguration, this._HrefDestinationSystemItemUserFieldConfiguration, 1, false, this._DisplayUsersAndGroupsItemUserFieldConfiguration));
            if (this._DisplaySystemItemConfiguration)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_Configuration", ImageFiles.GetIconPath(ImageFiles.ICON_CONFIGURATION, ImageFiles.EXT_PNG), _GlobalResources.Configuration, this._HrefDestinationSystemItemConfiguration, 1, false, this._DisplaySystemItemConfiguration));
            if (this._DisplaySystemItemEmailNotifications)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_EmailNotifications", ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL, ImageFiles.EXT_PNG), _GlobalResources.EmailNotifications, this._HrefDestinationSystemItemEmailNotifications, 1, false, this._DisplaySystemItemEmailNotifications));
            if (this._DisplaySystemItemWebMeetingIntegration)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_WebMeetingIntegration", ImageFiles.GetIconPath(ImageFiles.ICON_WEBMEETING, ImageFiles.EXT_PNG), _GlobalResources.WebMeetingIntegration, this._HrefDestinationSystemItemWebMeetingIntegration, 1, false, this._DisplaySystemItemWebMeetingIntegration));
            if (this._DisplaySystemItemEcommerce)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_Ecommerce", ImageFiles.GetIconPath(ImageFiles.ICON_ECOMMERCE, ImageFiles.EXT_PNG), _GlobalResources.ECommerce, this._HrefDestinationSystemItemEcommerce, 1, false, this._DisplaySystemItemEcommerce));
            if (this._DisplaySystemItemCouponCodes)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_CouponCodes", ImageFiles.GetIconPath(ImageFiles.ICON_COUPONCODE, ImageFiles.EXT_PNG), _GlobalResources.CouponCodes, this._HrefDestinationSystemItemCouponCodes, 1, false, this._DisplaySystemItemCouponCodes));
            if (this._DisplaySystemItemTrackingCodes)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_TrackingCodes", ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS, ImageFiles.EXT_PNG), _GlobalResources.TrackingCodes, this._HrefDestinationSystemItemTrackingCodes, 1, false, this._DisplaySystemItemTrackingCodes));
            if (this._DisplaySystemItemAPI)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_API", ImageFiles.GetIconPath(ImageFiles.ICON_API, ImageFiles.EXT_PNG), _GlobalResources.API, this._HrefDestinationSystemItemAPI, 1, false, this._DisplaySystemItemAPI));
            if (this._DisplaySystemItemxAPIEndpoints)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_xAPIEndpoints", ImageFiles.GetIconPath(ImageFiles.ICON_API, ImageFiles.EXT_PNG), _GlobalResources.xAPIEndpoints, this._HrefDestinationSystemItemxAPIEndpoints, 1, false, this._DisplaySystemItemxAPIEndpoints));
            if (this._DisplaySystemItemLogs)
                systemCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("System_Logs", ImageFiles.GetIconPath(ImageFiles.ICON_LOG, ImageFiles.EXT_PNG), _GlobalResources.Logs, this._HrefDestinationSystemItemLogs, 1, false, this._DisplaySystemItemLogs));

            //if no items have been built on the left, move the rightside items to the left
            if (menuItemsBuiltLeft == 0)
                systemCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderCondensedHeight";

            //add the blocks container to the menu
            systemCategoryMenuContainer.Controls.Add(systemCategoryMenuItemBlocksContainer);
            systemCategoryMenuContainer.Controls.Add(systemCategoryMenuItemBlocksContainerMobile);

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("System", this._IconPathSystem, this._IconPathSystemOn, _GlobalResources.System);

            // return the built menu
            return systemCategoryMenuContainer;
        }
        #endregion

        #region _BuildUsersAndGroupsCategoryMenu
        /// <summary>
        /// Builds users and groups category menu
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildUsersAndGroupsCategoryMenu()
        {
            int menuItemsBuilt = 0;

            Panel usersAndGroupsCategoryMenuContainer = new Panel();
            usersAndGroupsCategoryMenuContainer.ID = "UsersAndGroups_CategoryMenuItemsContainer";
            usersAndGroupsCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer";

            Panel usersAndGroupsCategoryMenuHeaderContainer = new Panel();
            usersAndGroupsCategoryMenuHeaderContainer.ID = "UsersAndGroups_CategoryMenuItemsHeaderContainer";
            usersAndGroupsCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize usersAndGroupsCategoryMenuHeader = new Localize();
            usersAndGroupsCategoryMenuHeader.Text = _GlobalResources.UsersAndGroups;
            usersAndGroupsCategoryMenuHeaderContainer.Controls.Add(usersAndGroupsCategoryMenuHeader);

            usersAndGroupsCategoryMenuContainer.Controls.Add(usersAndGroupsCategoryMenuHeaderContainer);

            // add individual category menu items
            if (this._DisplayUsersAndGroupsItemUsers)
            { 
                menuItemsBuilt++;
                usersAndGroupsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("UsersAndGroups_Users", ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG), _GlobalResources.Users, this._HrefDestinationUsersAndGroupsItemUsers));
            }

            if (this._DisplayUsersAndGroupsItemGroups)
            { 
                menuItemsBuilt++;
                usersAndGroupsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("UsersAndGroups_Groups", ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG), _GlobalResources.Groups, this._HrefDestinationUsersAndGroupsItemGroups));
            }

            if (this._DisplayUsersAndGroupsItemRoles)
            { 
                menuItemsBuilt++;
                usersAndGroupsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("UsersAndGroups_Roles", ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG), _GlobalResources.Roles, this._HrefDestinationUsersAndGroupsItemRoles));
            }

            if (this._DisplayUsersAndGroupsItemActivityImport)
            { 
                menuItemsBuilt++;
                usersAndGroupsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("UsersAndGroups_ActivityImport", ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTACTIVITYDATA, ImageFiles.EXT_PNG), _GlobalResources.ImportActivityData, this._HrefDestinationUsersAndGroupsItemActivityImport));
            }

            if (this._DisplayUsersAndGroupsItemCertificateImport)
            {
                menuItemsBuilt++;
                usersAndGroupsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("UsersAndGroups_CertificateImport", ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTCERTIFICATEDATA, ImageFiles.EXT_PNG), _GlobalResources.ImportCertificateData, this._HrefDestinationUsersAndGroupsItemCertificateImport));
            }

            if (this._DisplayUsersAndGroupsItemLeaderboards)
            {
                menuItemsBuilt++;
                usersAndGroupsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("UsersAndGroups_Leaderboards", ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD, ImageFiles.EXT_PNG), _GlobalResources.TeamLeaderboards, this._HrefDestinationUsersAndGroupsItemLeaderboards));
            }

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("UsersAndGroups", this._IconPathUsersAndGroups, this._IconPathUsersAndGroupsOn, _GlobalResources.UsersAndGroups);

            // return the built menu
            return usersAndGroupsCategoryMenuContainer;
        }
        #endregion

        #region _BuildUsersAndGroupsCategoryMenuWithBlockDesign
        /// <summary>
        /// Builds users and groups category menu (new version with flow lines)
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildUsersAndGroupsCategoryMenuWithBlockDesign()
        {
            int menuItemsBuilt = 0;
            int menuItemsBuiltLeft = 0;
            int menuItemsBuiltRight = 0;

            Panel usersAndGroupsCategoryMenuContainer = new Panel();
            usersAndGroupsCategoryMenuContainer.ID = "UsersAndGroups_CategoryMenuItemsContainer";
            usersAndGroupsCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer adminMenuGroupCollapsed";

            Panel usersAndGroupsCategoryMenuHeaderContainer = new Panel();
            usersAndGroupsCategoryMenuHeaderContainer.ID = "UsersAndGroups_CategoryMenuItemsHeaderContainer";
            usersAndGroupsCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize usersAndGroupsCategoryMenuHeader = new Localize();
            usersAndGroupsCategoryMenuHeader.Text = _GlobalResources.UsersAndGroups;
            usersAndGroupsCategoryMenuHeaderContainer.Controls.Add(usersAndGroupsCategoryMenuHeader);

            usersAndGroupsCategoryMenuContainer.Controls.Add(usersAndGroupsCategoryMenuHeaderContainer);

            //add the overall blocks container
            Panel usersAndGroupsCategoryMenuItemBlocksContainer = new Panel();
            usersAndGroupsCategoryMenuItemBlocksContainer.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer";

            //create the left-side blocks container
            Panel usersAndGroupsCategoryMenuItemBlocksContainerLeft = new Panel();
            usersAndGroupsCategoryMenuItemBlocksContainerLeft.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft";

            //create the right-side blocks container
            Panel usersAndGroupsCategoryMenuItemBlocksContainerRight = new Panel();
            usersAndGroupsCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderRight";

            //create the arrows container
            Panel usersAndGroupsCategoryMenuItemBlocksArrowContainer =  new Panel();
            usersAndGroupsCategoryMenuItemBlocksArrowContainer.CssClass = "AdministratorMenuCategoryMenuItemBlocksArrowContainer";

            //build the various different parts of the arrow system
            Panel usersAndGroupsCategoryMenuItemBlocksArrowStemVerticalSmallCenterContainer = new Panel();
            usersAndGroupsCategoryMenuItemBlocksArrowStemVerticalSmallCenterContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemVerticalSmallCenter AdministratorMenuCategoryMenuItem_ArrowStemFirst";
            usersAndGroupsCategoryMenuItemBlocksArrowContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksArrowStemVerticalSmallCenterContainer);

            //large horizontal arrow stem that goes across
            Panel usersAndGroupsCategoryMenuItemBlocksArrowStemHorizontalLargeContainer = new Panel();
            usersAndGroupsCategoryMenuItemBlocksArrowStemHorizontalLargeContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemHorizontalLarge";
            if (!this._DisplayUsersAndGroupsItemUsers || !this._DisplayUsersAndGroupsItemRoles)
                usersAndGroupsCategoryMenuItemBlocksArrowStemHorizontalLargeContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemHorizontalMediumLeft";
            usersAndGroupsCategoryMenuItemBlocksArrowContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksArrowStemHorizontalLargeContainer);

            //left and right vertical arrow stems container
            Panel usersAndGroupsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer = new Panel();
            usersAndGroupsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer.CssClass = "AdministratorMenuCategoryMenuItem_ForkedVerticalStemsContainer";

            //left stem to put into vertical stems container
            if(this._DisplayUsersAndGroupsItemUsers){
                Panel usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalSmallLeftContainer = new Panel();
                usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalSmallLeftContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemForkedSmallLeft";
                usersAndGroupsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalSmallLeftContainer);
            }

            //right stem to put into vertical stems container
            if (this._DisplayUsersAndGroupsItemRoles)
            {
                Panel usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalSmallRightContainer = new Panel();
                usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalSmallRightContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemForkedSmallRight";
                usersAndGroupsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalSmallRightContainer);
            }

            //add the forked vertical stems to the main arrow container
            usersAndGroupsCategoryMenuItemBlocksArrowContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer);

            //create the container for the left/right arrow heads
            Panel usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer = new Panel();
            usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer.CssClass = "AdministratorMenuCategoryMenuItem_ForkedVerticalArrowheadsContainer";

            //left arrow head (only add if leftside items will be present)
            if(this._DisplayUsersAndGroupsItemUsers){
                Panel usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadLeftContainer = new Panel();
                usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadLeftContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowheadForkedLeft";
                usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadLeftContainer);
            }

            //right arrow head (only add if rightside items will be present)
            if (this._DisplayUsersAndGroupsItemRoles)
            {
                Panel usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadRightContainer = new Panel();
                usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadRightContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowheadForkedRight";
                usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadRightContainer);
            }

            //add the left/right arrow heads container to the main arrow container
            usersAndGroupsCategoryMenuItemBlocksArrowContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer);

            //add the main arrow container to the main item blocks container
            if (this._DisplayUsersAndGroupsItemUsers || this._DisplayUsersAndGroupsItemRoles)
            {
                usersAndGroupsCategoryMenuItemBlocksContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksArrowContainer);
            }

            //add the mobile version container
            Panel usersAndGroupsCategoryMenuItemBlocksContainerMobile = new Panel();
            usersAndGroupsCategoryMenuItemBlocksContainerMobile.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainerMobile";    


            // add individual category menu items
            if (this._DisplayUsersAndGroupsItemGroups)
            {
                menuItemsBuilt++;
                usersAndGroupsCategoryMenuItemBlocksContainer.Controls.Add(this._AddCategoryMenuItemBlockLarge("UsersAndGroups_Groups", ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG), _GlobalResources.Groups, this._HrefDestinationUsersAndGroupsItemGroups, true, this._DisplayUsersAndGroupsItemGroups));
            }

            //add the left/right-side blocks to the overall container (add AFTER adding the full-length GROUPS block so it will sit underneath)
            usersAndGroupsCategoryMenuItemBlocksContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksContainerLeft);
            usersAndGroupsCategoryMenuItemBlocksContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksContainerRight);

            if (this._DisplayUsersAndGroupsItemUsers)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                //the Users blocks has child elements, so it must be built in stages
                Panel CategoryMenuItemBlockUsers = new Panel();
                CategoryMenuItemBlockUsers = this._AddCategoryMenuItemBlockMediumWithChildren("UsersAndGroups_Users", ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG), _GlobalResources.Users, this._HrefDestinationUsersAndGroupsItemUsers, true, this._DisplayUsersAndGroupsItemUsers);

                //add child elements to the Users block (add to the Panel control, not the Hyperlink)
                bool isFirstBlock = true;

                if (this._DisplayUsersAndGroupsItemActivityImport)
                {
                    CategoryMenuItemBlockUsers.Controls.Add(this._AddCategoryMenuItemBlockSecondary("UsersAndGroups_ActivityImport", ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTACTIVITYDATA, ImageFiles.EXT_PNG), _GlobalResources.ImportActivityData, this._HrefDestinationUsersAndGroupsItemActivityImport, isFirstBlock, false, false, this._DisplayUsersAndGroupsItemActivityImport));
                    isFirstBlock = false;
                }

                if (this._DisplayUsersAndGroupsItemCertificateImport)
                {
                    CategoryMenuItemBlockUsers.Controls.Add(this._AddCategoryMenuItemBlockSecondary("UsersAndGroups_CertificateImport", ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTCERTIFICATEDATA, ImageFiles.EXT_PNG), _GlobalResources.ImportCertificateData, this._HrefDestinationUsersAndGroupsItemCertificateImport, isFirstBlock, false, false, this._DisplayUsersAndGroupsItemCertificateImport));
                    isFirstBlock = false;
                }

                if (this._DisplayUsersAndGroupsItemLeaderboards)
                {
                    CategoryMenuItemBlockUsers.Controls.Add(this._AddCategoryMenuItemBlockSecondary("UsersAndGroups_Leaderboards", ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD, ImageFiles.EXT_PNG), _GlobalResources.TeamLeaderboards, this._HrefDestinationUsersAndGroupsItemLeaderboards, isFirstBlock, false, false, this._DisplayUsersAndGroupsItemLeaderboards));
                }

                //finally, add the Users button itself
                usersAndGroupsCategoryMenuItemBlocksContainerLeft.Controls.Add(CategoryMenuItemBlockUsers);
            }

            if (this._DisplayUsersAndGroupsItemRoles)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                usersAndGroupsCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("UsersAndGroups_Roles", ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG), _GlobalResources.Roles, this._HrefDestinationUsersAndGroupsItemRoles, "right", "none", true, this._DisplayUsersAndGroupsItemRoles));
            }

            //if there are no left-side menu items, move the right-side items to the left
            if (menuItemsBuiltLeft == 0)
                usersAndGroupsCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft";

            //add the mobile menu items
            if (this._DisplayUsersAndGroupsItemGroups)
                usersAndGroupsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("UsersAndGroups_Groups", ImageFiles.GetIconPath(ImageFiles.ICON_GROUP, ImageFiles.EXT_PNG), _GlobalResources.Groups, this._HrefDestinationUsersAndGroupsItemGroups, 1, true, this._DisplayUsersAndGroupsItemGroups));
            if (this._DisplayUsersAndGroupsItemUsers)
                usersAndGroupsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("UsersAndGroups_Users", ImageFiles.GetIconPath(ImageFiles.ICON_USERM, ImageFiles.EXT_PNG), _GlobalResources.Users, this._HrefDestinationUsersAndGroupsItemUsers, 2, true, this._DisplayUsersAndGroupsItemUsers));
            if (this._DisplayUsersAndGroupsItemActivityImport)
                usersAndGroupsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("UsersAndGroups_ActivityImport", ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTACTIVITYDATA, ImageFiles.EXT_PNG), _GlobalResources.ImportActivityData, this._HrefDestinationUsersAndGroupsItemActivityImport, 3, false, this._DisplayUsersAndGroupsItemActivityImport));
            if (this._DisplayUsersAndGroupsItemCertificateImport)
                usersAndGroupsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("UsersAndGroups_CertificateImport", ImageFiles.GetIconPath(ImageFiles.ICON_IMPORTCERTIFICATEDATA, ImageFiles.EXT_PNG), _GlobalResources.ImportCertificateData, this._HrefDestinationUsersAndGroupsItemCertificateImport, 3, false, this._DisplayUsersAndGroupsItemCertificateImport));
            if (this._DisplayUsersAndGroupsItemLeaderboards)
                usersAndGroupsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("UsersAndGroups_Leaderboards", ImageFiles.GetIconPath(ImageFiles.ICON_LEADERBOARD, ImageFiles.EXT_PNG), _GlobalResources.TeamLeaderboards, this._HrefDestinationUsersAndGroupsItemLeaderboards, 3, false, this._DisplayUsersAndGroupsItemLeaderboards));
            if (this._DisplayUsersAndGroupsItemRoles)
                usersAndGroupsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("UsersAndGroups_Roles", ImageFiles.GetIconPath(ImageFiles.ICON_PERMISSION, ImageFiles.EXT_PNG), _GlobalResources.Roles, this._HrefDestinationUsersAndGroupsItemRoles, 2, true, this._DisplayUsersAndGroupsItemRoles));


            //add the menu items block to the menu container
            usersAndGroupsCategoryMenuContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksContainer);
            usersAndGroupsCategoryMenuContainer.Controls.Add(usersAndGroupsCategoryMenuItemBlocksContainerMobile);

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("UsersAndGroups", this._IconPathUsersAndGroups, this._IconPathUsersAndGroupsOn, _GlobalResources.UsersAndGroups);

            // return the built menu
            return usersAndGroupsCategoryMenuContainer;
        }
        #endregion

        #region _BuildInterfaceAndLayoutCategoryMenu
        /// <summary>
        /// Builds interface and layout category menu
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildInterfaceAndLayoutCategoryMenu()
        {
            int menuItemsBuilt = 0;

            Panel interfaceAndLayoutCategoryMenuContainer = new Panel();
            interfaceAndLayoutCategoryMenuContainer.ID = "InterfaceAndLayout_CategoryMenuItemsContainer";
            interfaceAndLayoutCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer";

            Panel interfaceAndLayoutCategoryMenuHeaderContainer = new Panel();
            interfaceAndLayoutCategoryMenuHeaderContainer.ID = "InterfaceAndLayout_CategoryMenuItemsHeaderContainer";
            interfaceAndLayoutCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize interfaceAndLayoutCategoryMenuHeader = new Localize();
            interfaceAndLayoutCategoryMenuHeader.Text = _GlobalResources.InterfaceAndLayout;
            interfaceAndLayoutCategoryMenuHeaderContainer.Controls.Add(interfaceAndLayoutCategoryMenuHeader);

            interfaceAndLayoutCategoryMenuContainer.Controls.Add(interfaceAndLayoutCategoryMenuHeaderContainer);

            // add individual category menu items
            if (this._DisplayInterfaceAndLayoutItemHomePage)
            { 
                menuItemsBuilt++;
                interfaceAndLayoutCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("InterfaceAndLayout_HomePage", ImageFiles.GetIconPath(ImageFiles.ICON_HOME, ImageFiles.EXT_PNG), _GlobalResources.HomePage, this._HrefDestinationInterfaceAndLayoutItemHomePage));
            }

            if (this._DisplayInterfaceAndLayoutItemMasthead)
            { 
                menuItemsBuilt++;
                interfaceAndLayoutCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("InterfaceAndLayout_Header", ImageFiles.GetIconPath(ImageFiles.ICON_MASTHEAD, ImageFiles.EXT_PNG), _GlobalResources.Masthead, this._HrefDestinationInterfaceAndLayoutItemMasthead));
            }

            if (this._DisplayInterfaceAndLayoutItemFooter)
            { 
                menuItemsBuilt++;
                interfaceAndLayoutCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("InterfaceAndLayout_Footer", ImageFiles.GetIconPath(ImageFiles.ICON_FOOTER, ImageFiles.EXT_PNG), _GlobalResources.Footer, this._HrefDestinationInterfaceAndLayoutItemFooter));
            }

            if (this._DisplayInterfaceAndLayoutItemThemes)
            {
                menuItemsBuilt++;
                interfaceAndLayoutCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("InterfaceAndLayout_Themes", ImageFiles.GetIconPath(ImageFiles.ICON_THEMES, ImageFiles.EXT_PNG), _GlobalResources.Themes, this._HrefDestinationInterfaceAndLayoutItemThemes));
            }

            if (this._DisplayInterfaceAndLayoutItemCSSEditor)
            { 
                menuItemsBuilt++;


                interfaceAndLayoutCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("InterfaceAndLayout_CSSEditor", ImageFiles.GetIconPath(ImageFiles.ICON_CSS, ImageFiles.EXT_PNG), _GlobalResources.CSSEditor, this._HrefDestinationInterfaceAndLayoutItemCSSEditor));
            }

            if (this._DisplayInterfaceAndLayoutItemImageEditor)
            { 
                menuItemsBuilt++;
                interfaceAndLayoutCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("InterfaceAndLayout_ImageEditor", ImageFiles.GetIconPath(ImageFiles.ICON_IMAGE, ImageFiles.EXT_PNG), _GlobalResources.ImageEditor, this._HrefDestinationInterfaceAndLayoutItemImageEditor));
            }

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("InterfaceAndLayout", this._IconPathInterfaceAndLayout, this._IconPathInterfaceAndLayoutOn, _GlobalResources.InterfaceAndLayout);

            // return the built menu
            return interfaceAndLayoutCategoryMenuContainer;
        }
        #endregion

        #region _BuildInterfaceAndLayoutCategoryMenuWithBlockDesign
        /// <summary>
        /// Builds interface and layout category menu (new version with flow lines)
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildInterfaceAndLayoutCategoryMenuWithBlockDesign()
        {
            int menuItemsBuilt = 0;
            int menuItemsBuiltLeft = 0;
            int menuItemsBuiltRight = 0;

            Panel interfaceAndLayoutCategoryMenuContainer = new Panel();
            interfaceAndLayoutCategoryMenuContainer.ID = "InterfaceAndLayout_CategoryMenuItemsContainer";
            interfaceAndLayoutCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer adminMenuGroupCollapsed";

            Panel interfaceAndLayoutCategoryMenuHeaderContainer = new Panel();
            interfaceAndLayoutCategoryMenuHeaderContainer.ID = "InterfaceAndLayout_CategoryMenuItemsHeaderContainer";
            interfaceAndLayoutCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize interfaceAndLayoutCategoryMenuHeader = new Localize();
            interfaceAndLayoutCategoryMenuHeader.Text = _GlobalResources.InterfaceAndLayout;
            interfaceAndLayoutCategoryMenuHeaderContainer.Controls.Add(interfaceAndLayoutCategoryMenuHeader);

            interfaceAndLayoutCategoryMenuContainer.Controls.Add(interfaceAndLayoutCategoryMenuHeaderContainer);

            //add the overall blocks container
            Panel interfaceAndLayoutCategoryMenuItemBlocksContainer = new Panel();
            interfaceAndLayoutCategoryMenuItemBlocksContainer.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer";

            //create the left-side blocks container
            Panel interfaceAndLayoutCategoryMenuItemBlocksContainerLeft = new Panel();
            interfaceAndLayoutCategoryMenuItemBlocksContainerLeft.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft";

            //create the right-side blocks container
            Panel interfaceAndLayoutCategoryMenuItemBlocksContainerRight = new Panel();
            interfaceAndLayoutCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderRight";

            //add the left/right-side blocks to the overall container
            interfaceAndLayoutCategoryMenuItemBlocksContainer.Controls.Add(interfaceAndLayoutCategoryMenuItemBlocksContainerLeft);
            interfaceAndLayoutCategoryMenuItemBlocksContainer.Controls.Add(interfaceAndLayoutCategoryMenuItemBlocksContainerRight);

            //add the mobile version container
            Panel interfaceAndLayoutCategoryMenuItemBlocksContainerMobile = new Panel();
            interfaceAndLayoutCategoryMenuItemBlocksContainerMobile.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainerMobile";

            // add individual category menu items
            if (this._DisplayInterfaceAndLayoutItemHomePage)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                //whether it's compound on bottom or not will depend on at least one other item below it being added/enabled
                string compoundType = "bottom";

                if (!this._DisplayInterfaceAndLayoutItemMasthead && !this._DisplayInterfaceAndLayoutItemFooter)
                    compoundType = "none";

                interfaceAndLayoutCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("InterfaceAndLayout_HomePage", ImageFiles.GetIconPath(ImageFiles.ICON_HOME, ImageFiles.EXT_PNG), _GlobalResources.HomePage, this._HrefDestinationInterfaceAndLayoutItemHomePage, "left", compoundType, false, this._DisplayInterfaceAndLayoutItemHomePage));

                //add the mobile item
                interfaceAndLayoutCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("InterfaceAndLayout_HomePage", ImageFiles.GetIconPath(ImageFiles.ICON_HOME, ImageFiles.EXT_PNG), _GlobalResources.HomePage, this._HrefDestinationInterfaceAndLayoutItemHomePage, 1, false, this._DisplayInterfaceAndLayoutItemHomePage));
            }

            if (this._DisplayInterfaceAndLayoutItemMasthead)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                //determine if compound
                string compoundType = "both";

                if (!this._DisplayInterfaceAndLayoutItemHomePage && this._DisplayInterfaceAndLayoutItemFooter)
                    compoundType = "bottom";
                else if (this._DisplayInterfaceAndLayoutItemHomePage && !this._DisplayInterfaceAndLayoutItemFooter)
                    compoundType = "top";

                interfaceAndLayoutCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("InterfaceAndLayout_Header", ImageFiles.GetIconPath(ImageFiles.ICON_MASTHEAD, ImageFiles.EXT_PNG), _GlobalResources.Masthead, this._HrefDestinationInterfaceAndLayoutItemMasthead, "left", compoundType, false, this._DisplayInterfaceAndLayoutItemMasthead));

                //add mobile item
                interfaceAndLayoutCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("InterfaceAndLayout_Header", ImageFiles.GetIconPath(ImageFiles.ICON_MASTHEAD, ImageFiles.EXT_PNG), _GlobalResources.Masthead, this._HrefDestinationInterfaceAndLayoutItemMasthead, 1, false, this._DisplayInterfaceAndLayoutItemMasthead));
            }

            if (this._DisplayInterfaceAndLayoutItemFooter)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                //determine if compound
                string compoundType = "top";

                if (!this._DisplayInterfaceAndLayoutItemHomePage && !this._DisplayInterfaceAndLayoutItemMasthead)
                    compoundType = "none";

                interfaceAndLayoutCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("InterfaceAndLayout_Footer", ImageFiles.GetIconPath(ImageFiles.ICON_FOOTER, ImageFiles.EXT_PNG), _GlobalResources.Footer, this._HrefDestinationInterfaceAndLayoutItemFooter, "left", compoundType, false, this._DisplayInterfaceAndLayoutItemFooter));

                //mobile item
                interfaceAndLayoutCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("InterfaceAndLayout_Footer", ImageFiles.GetIconPath(ImageFiles.ICON_FOOTER, ImageFiles.EXT_PNG), _GlobalResources.Footer, this._HrefDestinationInterfaceAndLayoutItemFooter, 1, false, this._DisplayInterfaceAndLayoutItemFooter));
            }

            if (this._DisplayInterfaceAndLayoutItemThemes)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                //determine if compound
                string compoundType = "bottom";

                if ((!this._DisplayInterfaceAndLayoutItemCSSEditor) && (!this._DisplayInterfaceAndLayoutItemImageEditor))
                    compoundType = "none";
                
                interfaceAndLayoutCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("InterfaceAndLayout_Themes", ImageFiles.GetIconPath(ImageFiles.ICON_THEMES, ImageFiles.EXT_PNG), _GlobalResources.Themes, this._HrefDestinationInterfaceAndLayoutItemThemes, "right", compoundType, false, this._DisplayInterfaceAndLayoutItemThemes));

                //mobile item
                interfaceAndLayoutCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("InterfaceAndLayout_Themes", ImageFiles.GetIconPath(ImageFiles.ICON_THEMES, ImageFiles.EXT_PNG), _GlobalResources.Themes, this._HrefDestinationInterfaceAndLayoutItemThemes, 1, false, this._DisplayInterfaceAndLayoutItemThemes));
            }

            if (this._DisplayInterfaceAndLayoutItemCSSEditor)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                //determine if compound
                string compoundType = "both";

                if (!this._DisplayInterfaceAndLayoutItemImageEditor)
                    compoundType = "top";

                interfaceAndLayoutCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("InterfaceAndLayout_CSSEditor", ImageFiles.GetIconPath(ImageFiles.ICON_CSS, ImageFiles.EXT_PNG), _GlobalResources.CSSEditor, this._HrefDestinationInterfaceAndLayoutItemCSSEditor, "right", compoundType, false, this._DisplayInterfaceAndLayoutItemCSSEditor));

                //mobile item
                interfaceAndLayoutCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("InterfaceAndLayout_CSSEditor", ImageFiles.GetIconPath(ImageFiles.ICON_CSS, ImageFiles.EXT_PNG), _GlobalResources.CSSEditor, this._HrefDestinationInterfaceAndLayoutItemCSSEditor, 1, false, this._DisplayInterfaceAndLayoutItemCSSEditor));
            }

            if (this._DisplayInterfaceAndLayoutItemImageEditor)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                //determine if compound
                string compoundType = "top";

                if ((!this._DisplayInterfaceAndLayoutItemCSSEditor) && (!this._DisplayInterfaceAndLayoutItemImageEditor))
                    compoundType = "none";

                interfaceAndLayoutCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("InterfaceAndLayout_ImageEditor", ImageFiles.GetIconPath(ImageFiles.ICON_IMAGE, ImageFiles.EXT_PNG), _GlobalResources.ImageEditor, this._HrefDestinationInterfaceAndLayoutItemImageEditor, "right", compoundType, false, this._DisplayInterfaceAndLayoutItemImageEditor));

                //mobile item
                interfaceAndLayoutCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("InterfaceAndLayout_ImageEditor", ImageFiles.GetIconPath(ImageFiles.ICON_IMAGE, ImageFiles.EXT_PNG), _GlobalResources.ImageEditor, this._HrefDestinationInterfaceAndLayoutItemImageEditor, 1, false, this._DisplayInterfaceAndLayoutItemImageEditor));
            }

            //if NO left menu items have been built, shift the rightside items to the left
            if (menuItemsBuiltLeft == 0)
                interfaceAndLayoutCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft";

            //add the blocks container to the menu
            interfaceAndLayoutCategoryMenuContainer.Controls.Add(interfaceAndLayoutCategoryMenuItemBlocksContainer);
            interfaceAndLayoutCategoryMenuContainer.Controls.Add(interfaceAndLayoutCategoryMenuItemBlocksContainerMobile);

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("InterfaceAndLayout", this._IconPathInterfaceAndLayout, this._IconPathInterfaceAndLayoutOn, _GlobalResources.InterfaceAndLayout);

            // return the built menu
            return interfaceAndLayoutCategoryMenuContainer;
        }
        #endregion

        #region _BuildLearningAssetsCategoryMenu
        /// <summary>
        /// Builds learning assets category menu
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildLearningAssetsCategoryMenu()
        {
            int menuItemsBuilt = 0;

            Panel learningAssetsCategoryMenuContainer = new Panel();
            learningAssetsCategoryMenuContainer.ID = "LearningAssets_CategoryMenuItemsContainer";
            learningAssetsCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer";

            Panel learningAssetsCategoryMenuHeaderContainer = new Panel();
            learningAssetsCategoryMenuHeaderContainer.ID = "LearningAssets_CategoryMenuItemsHeaderContainer";
            learningAssetsCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize learningAssetsCategoryMenuHeader = new Localize();
            learningAssetsCategoryMenuHeader.Text = _GlobalResources.LearningAssets;
            learningAssetsCategoryMenuHeaderContainer.Controls.Add(learningAssetsCategoryMenuHeader);

            learningAssetsCategoryMenuContainer.Controls.Add(learningAssetsCategoryMenuHeaderContainer);

            // add individual category menu items
            if (this._DisplayLearningAssetsItemCourseCatalog)
            { 
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_CourseCatalog", ImageFiles.GetIconPath(ImageFiles.ICON_COURSECATALOG, ImageFiles.EXT_PNG), _GlobalResources.CourseCatalog, this._HrefDestinationLearningAssetsItemCourseCatalog));
            }

            if (this._DisplayLearningAssetsItemCourses)
            { 
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_Courses", ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG), _GlobalResources.Courses, this._HrefDestinationLearningAssetsItemCourses));
            }

            if (this._DisplayLearningAssetsItemLearningPaths)
            {
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_LearningPaths", ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG), _GlobalResources.LearningPaths, this._HrefDestinationLearningAssetsItemLearningPaths));
            }

            if (this._DisplayLearningAssetsItemCertifications)
            {
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_Certifications", ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG), _GlobalResources.Certifications, this._HrefDestinationLearningAssetsItemCertifications));
            }

            if (this._DisplayLearningAssetsItemContentPackages)
            {
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_ContentPackages", ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG), _GlobalResources.ContentPackages, this._HrefDestinationLearningAssetsItemContentPackages));
            }

            if (this._DisplayLearningAssetsItemQuizzesAndSurveys)
            {
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_QuizzesAndSurveys", ImageFiles.GetIconPath(ImageFiles.ICON_QUIZ, ImageFiles.EXT_PNG), _GlobalResources.QuizzesAndSurveys, this._HrefDestinationLearningAssetsItemQuizzesAndSurveys));
            }

            if (this._DisplayLearningAssetsItemCertificateTemplates)
            {
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_CertificateTemplates", ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE_TEMPLATE, ImageFiles.EXT_PNG), _GlobalResources.CertificateTemplates, this._HrefDestinationLearningAssetsItemCertificateTemplates));
            }

            if (this._DisplayLearningAssetsItemStandupTraining)
            { 
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_StandupTraining", ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG), _GlobalResources.InstructorLedTraining, this._HrefDestinationLearningAssetsItemStandupTraining));
            }

            if (this._DisplayLearningAssetsItemResourceManagement)
            { 
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_ResourceManagement", ImageFiles.GetIconPath(ImageFiles.ICON_RESOURCE_MANAGEMENT, ImageFiles.EXT_PNG), _GlobalResources.ResourceManagement, this._HrefDestinationLearningAssetsItemResourceManagement));
            }

            if (this._DisplayLearningAssetsItemOpenSesameMarketplace)
            {
                menuItemsBuilt++;
                learningAssetsCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("LearningAssets_OpenSesameMarketplace", ImageFiles.GetIconPath(ImageFiles.ICON_OPENSESAME, ImageFiles.EXT_PNG), _GlobalResources.OpenSesame, this._HrefDestinationLearningAssetsItemOpenSesameMarketplace));
            }

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("LearningAssets", this._IconPathLearningAssets, this._IconPathLearningAssetsOn, _GlobalResources.LearningAssets);

            // return the built menu
            return learningAssetsCategoryMenuContainer;
        }
        #endregion

        #region _BuildLearningAssetsCategoryMenuWithBlockDesign
        /// <summary>
        /// Builds learning assets category menu (new version with flow lines)
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildLearningAssetsCategoryMenuWithBlockDesign()
        {
            int menuItemsBuilt = 0;
            int menuItemsBuiltLeft = 0;
            int menuItemsBuiltRight = 0;

            Panel learningAssetsCategoryMenuContainer = new Panel();
            learningAssetsCategoryMenuContainer.ID = "LearningAssets_CategoryMenuItemsContainer";
            learningAssetsCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer adminMenuGroupCollapsed";

            Panel learningAssetsCategoryMenuHeaderContainer = new Panel();
            learningAssetsCategoryMenuHeaderContainer.ID = "LearningAssets_CategoryMenuItemsHeaderContainer";
            learningAssetsCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize learningAssetsCategoryMenuHeader = new Localize();
            learningAssetsCategoryMenuHeader.Text = _GlobalResources.LearningAssets;
            learningAssetsCategoryMenuHeaderContainer.Controls.Add(learningAssetsCategoryMenuHeader);

            learningAssetsCategoryMenuContainer.Controls.Add(learningAssetsCategoryMenuHeaderContainer);

            //create the overall blocks container
            Panel learningAssetsCategoryMenuItemBlocksContainer = new Panel();
            learningAssetsCategoryMenuItemBlocksContainer.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer";

            //create the left-side blocks container
            Panel learningAssetsCategoryMenuItemBlocksContainerLeft = new Panel();
            learningAssetsCategoryMenuItemBlocksContainerLeft.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft";

            //create the right-side blocks container
            Panel learningAssetsCategoryMenuItemBlocksContainerRight = new Panel();
            learningAssetsCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderRight";

            //add the overall blocks container to the menu
            learningAssetsCategoryMenuContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksContainer);

            //create the arrows container
            Panel learningAssetsCategoryMenuItemBlocksArrowContainer = new Panel();
            learningAssetsCategoryMenuItemBlocksArrowContainer.CssClass = "AdministratorMenuCategoryMenuItemBlocksArrowContainer";

            //build the various different parts of the arrow system
            Panel learningAssetsCategoryMenuItemBlocksArrowStemVerticalSmallCenterContainer = new Panel();
            learningAssetsCategoryMenuItemBlocksArrowStemVerticalSmallCenterContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemVerticalSmallCenter AdministratorMenuCategoryMenuItem_ArrowStemFirst";
            learningAssetsCategoryMenuItemBlocksArrowContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemVerticalSmallCenterContainer);

            //large horizontal arrow stem that goes across
            Panel learningAssetsCategoryMenuItemBlocksArrowStemHorizontalLargeContainer = new Panel();
            learningAssetsCategoryMenuItemBlocksArrowStemHorizontalLargeContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemHorizontalLarge";
            //if only one forked item, update the class name
            if ((this._DisplayLearningAssetsItemCourses && !this._DisplayLearningAssetsItemLearningPaths) ||
                (!this._DisplayLearningAssetsItemCourses && this._DisplayLearningAssetsItemLearningPaths))
                learningAssetsCategoryMenuItemBlocksArrowStemHorizontalLargeContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemHorizontalMediumLeft";
            

            learningAssetsCategoryMenuItemBlocksArrowContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemHorizontalLargeContainer);
            

            //left and right vertical arrow stems container
            Panel learningAssetsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer = new Panel();
            learningAssetsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer.CssClass = "AdministratorMenuCategoryMenuItem_ForkedVerticalStemsContainer";

            //only add the left-side arrow components if something will be over there
            if ((this._DisplayLearningAssetsItemCourseCatalog && this._DisplayLearningAssetsItemCourses) ||
                (this._DisplayLearningAssetsItemCourseCatalog && !this._DisplayLearningAssetsItemCourses && this._DisplayLearningAssetsItemLearningPaths))
            {
                Panel learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalSmallLeftContainer = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalSmallLeftContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemForkedSmallLeft";
                learningAssetsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalSmallLeftContainer);
            }

            //only add the right-side arrow components if something will be over there
            if (this._DisplayLearningAssetsItemCourseCatalog && this._DisplayLearningAssetsItemLearningPaths && this._DisplayLearningAssetsItemCourses)
            {
                Panel learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalSmallRightContainer = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalSmallRightContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowStemForkedSmallRight";
                learningAssetsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalSmallRightContainer);
            }

            //add the forked vertical stems to the main arrow container
            learningAssetsCategoryMenuItemBlocksArrowContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemsForkedVerticalContainer);

            //create the container for the left/right arrow heads
            Panel learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer = new Panel();
            learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer.CssClass = "AdministratorMenuCategoryMenuItem_ForkedVerticalArrowheadsContainer";

            //left arrow head
            if (this._DisplayLearningAssetsItemCourseCatalog && this._DisplayLearningAssetsItemCourses ||
                (this._DisplayLearningAssetsItemCourseCatalog && !this._DisplayLearningAssetsItemCourses && this._DisplayLearningAssetsItemLearningPaths))
            {
                Panel learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadLeftContainer = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadLeftContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowheadForkedLeft";
                learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadLeftContainer);
            }

            //right arrow head
            if (this._DisplayLearningAssetsItemCourseCatalog && this._DisplayLearningAssetsItemLearningPaths && this._DisplayLearningAssetsItemCourses)
            {
                Panel learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadRightContainer = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadRightContainer.CssClass = "AdministratorMenuCategoryMenuItem_ArrowheadForkedRight";
                learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadRightContainer);
            }

            //add the left/right arrow heads container to the main arrow container
            learningAssetsCategoryMenuItemBlocksArrowContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemForkedVerticalArrowHeadsContainer);

            //create horz arrow that points from Courses to Learning Paths
            if (this._DisplayLearningAssetsItemCourses && this._DisplayLearningAssetsItemLearningPaths)
            {
                Panel learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_Courses = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_Courses.CssClass = "AdministratorMenuCategoryMenuItem_HorizontalCenterSmallArrowContainer_Courses";
                learningAssetsCategoryMenuItemBlocksArrowContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_Courses);

                //create the arrow stem
                Panel learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterStem_Courses = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterStem_Courses.CssClass = "AdministratorMenuCategoryMenuItem_HorizontalCenterSmallArrowStemRight";
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_Courses.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterStem_Courses);

                //create the arrow head
                Panel learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterHead_Courses = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterHead_Courses.CssClass = "AdministratorMenuCategoryMenuItem_HorizontalCenterSmallArrowHeadRight";
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_Courses.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterHead_Courses);
            }

            //create horz arrow that points from Instructor Led Training to Courses
            if (this._DisplayLearningAssetsItemCourses && this._DisplayLearningAssetsItemStandupTraining)
            {
                Panel learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_ILT = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_ILT.CssClass = "AdministratorMenuCategoryMenuItem_HorizontalCenterSmallArrowContainer_ILT";
                learningAssetsCategoryMenuItemBlocksArrowContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_ILT);

                //create the arrow head
                Panel learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterHead_ILT = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterHead_ILT.CssClass = "AdministratorMenuCategoryMenuItem_HorizontalCenterSmallArrowHeadLeft";
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_ILT.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterHead_ILT);

                //create the arrow stem
                Panel learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterStem_ILT = new Panel();
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterStem_ILT.CssClass = "AdministratorMenuCategoryMenuItem_HorizontalCenterSmallArrowStemLeft";
                learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterContainer_ILT.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowStemSmallHorizontalCenterStem_ILT);
            }

            //add the main arrow container to the main item blocks container
            if (this._DisplayLearningAssetsItemCourseCatalog && (this._DisplayLearningAssetsItemCourses || this._DisplayLearningAssetsItemLearningPaths))
            {
                learningAssetsCategoryMenuItemBlocksContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksArrowContainer);
            }

            //add the mobile version container
            Panel learningAssetsCategoryMenuItemBlocksContainerMobile = new Panel();
            learningAssetsCategoryMenuItemBlocksContainerMobile.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainerMobile";


            // add individual category menu items
            if (this._DisplayLearningAssetsItemCourseCatalog)
            {
                menuItemsBuilt++;
                learningAssetsCategoryMenuItemBlocksContainer.Controls.Add(this._AddCategoryMenuItemBlockLarge("LearningAssets_CourseCatalog", ImageFiles.GetIconPath(ImageFiles.ICON_COURSECATALOG, ImageFiles.EXT_PNG), _GlobalResources.CourseCatalog, this._HrefDestinationLearningAssetsItemCourseCatalog, false, this._DisplayLearningAssetsItemCourseCatalog));
            }

            //add the left/right-side blocks to the overall container (AFTER the the Course Catalog button since it spans the entire length of the menu)
            learningAssetsCategoryMenuItemBlocksContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksContainerLeft);
            learningAssetsCategoryMenuItemBlocksContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksContainerRight);

            if (this._DisplayLearningAssetsItemCourses)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                //Courses button must be built in stages since it has Child buttons
                Panel learningAssetsCoursesBlock = new Panel();
                learningAssetsCoursesBlock = this._AddCategoryMenuItemBlockMediumWithChildren("LearningAssets_Courses", ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG), _GlobalResources.Courses, this._HrefDestinationLearningAssetsItemCourses, true, this._DisplayLearningAssetsItemCourses, true);
                learningAssetsCategoryMenuItemBlocksContainerLeft.Controls.Add(learningAssetsCoursesBlock);

                //add children
                if (this._DisplayLearningAssetsItemContentPackages)
                    learningAssetsCoursesBlock.Controls.Add(this._AddCategoryMenuItemBlockSecondary("LearningAssets_ContentPackages", ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG), _GlobalResources.ContentPackages, this._HrefDestinationLearningAssetsItemContentPackages, false, false, false, this._DisplayLearningAssetsItemContentPackages));                

                if (this._DisplayLearningAssetsItemQuizzesAndSurveys)
                    learningAssetsCoursesBlock.Controls.Add(this._AddCategoryMenuItemBlockSecondary("LearningAssets_QuizzesAndSurveys", ImageFiles.GetIconPath(ImageFiles.ICON_QUIZ, ImageFiles.EXT_PNG), _GlobalResources.QuizzesAndSurveys, this._HrefDestinationLearningAssetsItemQuizzesAndSurveys, false, false, true, this._DisplayLearningAssetsItemQuizzesAndSurveys));

                if (this._DisplayLearningAssetsItemOpenSesameMarketplace)
                    learningAssetsCoursesBlock.Controls.Add(this._AddCategoryMenuItemBlockSecondary("LearningAssets_OpenSesameMarketplace", ImageFiles.GetIconPath(ImageFiles.ICON_OPENSESAME, ImageFiles.EXT_PNG), _GlobalResources.OpenSesame, this._HrefDestinationLearningAssetsItemOpenSesameMarketplace, false, false, false, this._DisplayLearningAssetsItemOpenSesameMarketplace));
            }

            if (this._DisplayLearningAssetsItemCertificateTemplates)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                learningAssetsCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("LearningAssets_CertificateTemplates", ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE_TEMPLATE, ImageFiles.EXT_PNG), _GlobalResources.CertificateTemplates, this._HrefDestinationLearningAssetsItemCertificateTemplates, "right", "none", true, this._DisplayLearningAssetsItemCertificateTemplates, true));
            }            

            if (this._DisplayLearningAssetsItemLearningPaths)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                learningAssetsCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("LearningAssets_LearningPaths", ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG), _GlobalResources.LearningPaths, this._HrefDestinationLearningAssetsItemLearningPaths, "right", "none", true, this._DisplayLearningAssetsItemLearningPaths, true));
            }

            //Instructor Led Training block has a child, so we must also build it in stages
            if (this._DisplayLearningAssetsItemStandupTraining)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                Panel instructorLedTrainingBlock = new Panel();
                instructorLedTrainingBlock = this._AddCategoryMenuItemBlockMediumWithChildren("LearningAssets_StandupTraining", ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG), _GlobalResources.InstructorLedTraining, this._HrefDestinationLearningAssetsItemStandupTraining, true, this._DisplayLearningAssetsItemStandupTraining, true);
                learningAssetsCategoryMenuItemBlocksContainerRight.Controls.Add(instructorLedTrainingBlock);

                if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.ILTRESOURCES_ENABLE))
                {
                    //add the Intructor Led Training's child, Resource Management
                    instructorLedTrainingBlock.Controls.Add(this._AddCategoryMenuItemBlockSecondary("LearningAssets_ResourceManagement", ImageFiles.GetIconPath(ImageFiles.ICON_RESOURCE_MANAGEMENT, ImageFiles.EXT_PNG), _GlobalResources.ResourceManagement, this._HrefDestinationLearningAssetsItemResourceManagement, false, false, false, this._DisplayLearningAssetsItemResourceManagement));
                }
            }

            if (this._DisplayLearningAssetsItemCertifications)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                learningAssetsCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("LearningAssets_Certifications", ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG), _GlobalResources.Certifications, this._HrefDestinationLearningAssetsItemCertifications, "right", "none", true, this._DisplayLearningAssetsItemCertifications, true));
            }

            //if there are no left-side menu items, move the right-side items to the left
            if (menuItemsBuiltLeft == 0)
                learningAssetsCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft";

            //add the mobile menu items
            if (this._DisplayLearningAssetsItemCourseCatalog)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_CourseCatalog", ImageFiles.GetIconPath(ImageFiles.ICON_COURSECATALOG, ImageFiles.EXT_PNG), _GlobalResources.CourseCatalog, this._HrefDestinationLearningAssetsItemCourseCatalog, 1, false, this._DisplayLearningAssetsItemCourseCatalog));
            if (this._DisplayLearningAssetsItemCourses)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_Courses", ImageFiles.GetIconPath(ImageFiles.ICON_COURSE, ImageFiles.EXT_PNG), _GlobalResources.Courses, this._HrefDestinationLearningAssetsItemCourses, 2, true, this._DisplayLearningAssetsItemCourses));
            if (this._DisplayLearningAssetsItemContentPackages)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_ContentPackages", ImageFiles.GetIconPath(ImageFiles.ICON_PACKAGE, ImageFiles.EXT_PNG), _GlobalResources.ContentPackages, this._HrefDestinationLearningAssetsItemContentPackages, 3, true, this._DisplayLearningAssetsItemContentPackages));            
            if (this._DisplayLearningAssetsItemQuizzesAndSurveys)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_QuizzesAndSurveys", ImageFiles.GetIconPath(ImageFiles.ICON_QUIZ, ImageFiles.EXT_PNG), _GlobalResources.QuizzesAndSurveys, this._HrefDestinationLearningAssetsItemQuizzesAndSurveys, 3, true, this._DisplayLearningAssetsItemQuizzesAndSurveys));
            if (this._DisplayLearningAssetsItemOpenSesameMarketplace)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_OpenSesameMarketplace", ImageFiles.GetIconPath(ImageFiles.ICON_OPENSESAME, ImageFiles.EXT_PNG), _GlobalResources.OpenSesame, this._HrefDestinationLearningAssetsItemOpenSesameMarketplace, 3, true, this._DisplayLearningAssetsItemOpenSesameMarketplace));
            if (this._DisplayLearningAssetsItemStandupTraining)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_StandupTraining", ImageFiles.GetIconPath(ImageFiles.ICON_STANDUPTRAINING, ImageFiles.EXT_PNG), _GlobalResources.InstructorLedTraining, this._HrefDestinationLearningAssetsItemStandupTraining, 3, true, this._DisplayLearningAssetsItemStandupTraining));
            if (this._DisplayLearningAssetsItemResourceManagement)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_ResourceManagement", ImageFiles.GetIconPath(ImageFiles.ICON_RESOURCE_MANAGEMENT, ImageFiles.EXT_PNG), _GlobalResources.ResourceManagement, this._HrefDestinationLearningAssetsItemResourceManagement, 4, false, this._DisplayLearningAssetsItemResourceManagement));
            if (this._DisplayLearningAssetsItemLearningPaths)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_LearningPaths", ImageFiles.GetIconPath(ImageFiles.ICON_LEARNINGPATH, ImageFiles.EXT_PNG), _GlobalResources.LearningPaths, this._HrefDestinationLearningAssetsItemLearningPaths, 2, true, this._DisplayLearningAssetsItemLearningPaths));
            if (this._DisplayLearningAssetsItemCertifications)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_Certifications", ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATION, ImageFiles.EXT_PNG), _GlobalResources.Certifications, this._HrefDestinationLearningAssetsItemCertifications, 1, true, this._DisplayLearningAssetsItemCertifications));
            if (this._DisplayLearningAssetsItemCertificateTemplates)
                learningAssetsCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("LearningAssets_CertificateTemplates", ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE_TEMPLATE, ImageFiles.EXT_PNG), _GlobalResources.CertificateTemplates, this._HrefDestinationLearningAssetsItemCertificateTemplates, 1, true, this._DisplayLearningAssetsItemCertificateTemplates));            

            learningAssetsCategoryMenuContainer.Controls.Add(learningAssetsCategoryMenuItemBlocksContainerMobile);

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("LearningAssets", this._IconPathLearningAssets, this._IconPathLearningAssetsOn, _GlobalResources.LearningAssets);

            // return the built menu
            return learningAssetsCategoryMenuContainer;
        }
        #endregion

        #region _BuildReportingCategoryMenu
        /// <summary>
        /// Builds reporting category  menu
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildReportingCategoryMenu()
        {
            int menuItemsBuilt = 0;

            Panel reportingCategoryMenuContainer = new Panel();
            reportingCategoryMenuContainer.ID = "Reporting_CategoryMenuItemsContainer";
            reportingCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer";

            Panel reportingCategoryMenuHeaderContainer = new Panel();
            reportingCategoryMenuHeaderContainer.ID = "Reporting_CategoryMenuItemsHeaderContainer";
            reportingCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize reportingCategoryMenuHeader = new Localize();
            reportingCategoryMenuHeader.Text = _GlobalResources.Reporting;
            reportingCategoryMenuHeaderContainer.Controls.Add(reportingCategoryMenuHeader);

            reportingCategoryMenuContainer.Controls.Add(reportingCategoryMenuHeaderContainer);

            // add individual category menu items
            if (this._DisplayReportingItemReports)
            { 
                menuItemsBuilt++;
                reportingCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("Reporting_Reports", ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG), _GlobalResources.Reports, this._HrefDestinationReportingItemReports));
            }

            if (this._DisplayReportingItemAnalytics)
            { 
                menuItemsBuilt++;
                reportingCategoryMenuContainer.Controls.Add(this._AddCategoryMenuItem("Reporting_Analytics", ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS, ImageFiles.EXT_PNG), _GlobalResources.Analytics, this._HrefDestinationReportingItemAnalytics));
            }

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("Reporting", this._IconPathReporting, this._IconPathReportingOn, _GlobalResources.Reporting);

            // return the built menu
            return reportingCategoryMenuContainer;
        }
        #endregion

        #region _BuildReportingCategoryMenuWithBlockDesign
        /// <summary>
        /// Builds reporting category menu (new version with flow lines)
        /// </summary>
        /// <returns>menu panel</returns>
        private Panel _BuildReportingCategoryMenuWithBlockDesign()
        {
            int menuItemsBuilt = 0;
            int menuItemsBuiltLeft = 0;
            int menuItemsBuiltRight = 0;

            Panel reportingCategoryMenuContainer = new Panel();
            reportingCategoryMenuContainer.ID = "Reporting_CategoryMenuItemsContainer";
            reportingCategoryMenuContainer.CssClass = "AdministratorMenuCategoryMenuItemsContainer adminMenuGroupCollapsed";

            Panel reportingCategoryMenuHeaderContainer = new Panel();
            reportingCategoryMenuHeaderContainer.ID = "Reporting_CategoryMenuItemsHeaderContainer";
            reportingCategoryMenuHeaderContainer.CssClass = "AdministratorMenuCategoryMenuItemsHeaderContainer";

            Localize reportingCategoryMenuHeader = new Localize();
            reportingCategoryMenuHeader.Text = _GlobalResources.Reporting;
            reportingCategoryMenuHeaderContainer.Controls.Add(reportingCategoryMenuHeader);

            reportingCategoryMenuContainer.Controls.Add(reportingCategoryMenuHeaderContainer);

            //add the overall blocks container
            Panel reportingCategoryMenuItemBlocksContainer = new Panel();
            reportingCategoryMenuItemBlocksContainer.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer";

            //create the left-side blocks container
            Panel reportingCategoryMenuItemBlocksContainerLeft = new Panel();
            reportingCategoryMenuItemBlocksContainerLeft.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft";

            //create the right-side blocks container
            Panel reportingCategoryMenuItemBlocksContainerRight = new Panel();
            reportingCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderRight";

            //add the left/right-side blocks to the overall container
            reportingCategoryMenuItemBlocksContainer.Controls.Add(reportingCategoryMenuItemBlocksContainerLeft);
            reportingCategoryMenuItemBlocksContainer.Controls.Add(reportingCategoryMenuItemBlocksContainerRight);

            //add the mobile version container
            Panel reportingCategoryMenuItemBlocksContainerMobile = new Panel();
            reportingCategoryMenuItemBlocksContainerMobile.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainerMobile";

            // add individual category menu items
            if (this._DisplayReportingItemReports)
            {
                menuItemsBuilt++;
                menuItemsBuiltLeft++;

                reportingCategoryMenuItemBlocksContainerLeft.Controls.Add(this._AddCategoryMenuItemBlockMedium("Reporting_Reports", ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG), _GlobalResources.Reports, this._HrefDestinationReportingItemReports, "left", "none", true, this._DisplayReportingItemReports));
            }

            if (this._DisplayReportingItemAnalytics)
            {
                menuItemsBuilt++;
                menuItemsBuiltRight++;

                reportingCategoryMenuItemBlocksContainerRight.Controls.Add(this._AddCategoryMenuItemBlockMedium("Reporting_Analytics", ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS, ImageFiles.EXT_PNG), _GlobalResources.Analytics, this._HrefDestinationReportingItemAnalytics, "right", "none", false, this._DisplayReportingItemAnalytics));
            }

            //if no left items exist, move the right items to the left
            if (menuItemsBuiltLeft == 0)
                reportingCategoryMenuItemBlocksContainerRight.CssClass = "AdministratorMenuCategoryMenuItemBlocksContainer_BlockHolderLeft";

            //add the mobile menu items
            if (this._DisplayReportingItemReports)
                reportingCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("Reporting_Reports", ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG), _GlobalResources.Reports, this._HrefDestinationReportingItemReports, 1, true, this._DisplayReportingItemReports));
            if (this._DisplayReportingItemAnalytics)
                reportingCategoryMenuItemBlocksContainerMobile.Controls.Add(_AddCategoryMenuItemMobile("Reporting_Analytics", ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS, ImageFiles.EXT_PNG), _GlobalResources.Analytics, this._HrefDestinationReportingItemAnalytics, 1, false, this._DisplayReportingItemAnalytics));

            //add the blocks container to the menu
            reportingCategoryMenuContainer.Controls.Add(reportingCategoryMenuItemBlocksContainer);
            reportingCategoryMenuContainer.Controls.Add(reportingCategoryMenuItemBlocksContainerMobile);

            // if no menu items were built, return null
            if (menuItemsBuilt == 0)
            { return null; }

            // build the category icon
            this._BuildIconForCategoryMenu("Reporting", this._IconPathReporting, this._IconPathReportingOn, _GlobalResources.Reporting);

            // return the built menu
            return reportingCategoryMenuContainer;
        }
        #endregion

        #region GetReportDataSetListTableForAdminMenu
        /// <summary>
        /// this functionality is pulled from GetReportDataSetListTable() in ReportDataSet.cs since that class is out of this scope
        /// </summary>
        /// <returns>report data table</returns>
        private DataTable GetReportDataSetListTableForAdminMenu()
        {
            DataTable dataSetList = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader dataSetReader = databaseObject.ExecuteDataReader("[Dataset.ListDataSets]", true);
                dataSetList.Load(dataSetReader);
                dataSetReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();

                if (returnCode == DBReturnValue.DetailsNotFound)
                { throw new DatabaseDetailsNotFoundException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode)); }

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return dataSetList;
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery.easing.1.3.js"); // for animation
        }
        #endregion

        #region RenderContents
        protected override void RenderContents(HtmlTextWriter writer)
        {
            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // set icon paths
            this._SetIconPaths();

            // set href destinations for menu items
            this._SetHrefDestinations();

            // set menu item display properties
            this._SetMenuItemDisplayProperties();

            // if nothing is true, die - get count of items

            // javascript container
            this._JavascriptTag = new HtmlGenericControl("script");
            this._JavascriptTag.Attributes.Add("type", "text/javascript");

            // menu
            this._BuildMenu();

            this.Controls.AddAt(0, this._AdministratorMenuCategoryIconsContainer);

            // build JS menu actions
            this._BuildJSActions();

            // attach javascript tag - needs to be done last
            this.Controls.Add(this._JavascriptTag);

            // execute base
            base.RenderContents(writer);
        }
        #endregion
        #endregion
    }
}