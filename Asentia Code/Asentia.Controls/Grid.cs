﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    /// <summary>
    /// Creates a grid from a call to a stored procedure.
    /// </summary>
    public class Grid : GridView
    {
        #region Constructors
        /// <summary>
        /// The constructor of this Grid, it sets properties that are common to every grid.
        /// </summary>
        /// <param name="dbType">
        /// The database that contains this Grid's "get" procedure.
        /// Optional Parameter, defaults to Account Database.
        /// </param>
        public Grid(string dbType = "Account", bool forWidget = false)
        {
            // override default values for properties inherited by GridView
            if (dbType == "CustomerManager")
            { 
                this.DBType = DatabaseType.CustomerManager;
                this.OverrideLoadImagesFromCustomerManager = true;
                this._LoadImagesFromCustomerManager = true;
            }
            else if (dbType == "AccountWithCMImages")
            {
                this.DBType = DatabaseType.Account;
                this.OverrideLoadImagesFromCustomerManager = true;
                this._LoadImagesFromCustomerManager = true;
            }
            else
            { this.DBType = DatabaseType.Account; }

            this.AutoGenerateColumns = false;
            this.AllowPaging = true;
            this.AllowCustomPaging = true;
            this.PagerSettings.Position = PagerPosition.TopAndBottom;
            this.AllowSorting = true;
            this.ShowHeaderWhenEmpty = true;
            this.EmptyDataText = _GlobalResources.NoRecordsFound;

            if (forWidget)
            {
                this.AddCheckboxColumn = false;
                this.ShowSearchBox = false;
                this.ShowRecordsPerPageSelectbox = false;
                this.PagerSettings.Position = PagerPosition.Bottom;
            }

            // event handlers
            this.RowCreated += new GridViewRowEventHandler(_Grid_RowCreated);
            this.RowDataBound += new GridViewRowEventHandler(_Grid_RowDataBound);
            this.DataBound += new EventHandler(_Grid_DataBound);
            this.Sorting += new GridViewSortEventHandler(_Grid_Sorting);
            this.PageIndexChanging += new GridViewPageEventHandler(_Grid_PageIndexChanging);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The database to use, default is Account.
        /// </summary>
        public DatabaseType DBType = DatabaseType.Account;

        /// <summary>
        /// Determines whether or not to add a checkbox column to the grid.
        /// </summary>
        public bool AddCheckboxColumn = true;

        /// <summary>
        /// Determines whether or not to add hover actions to rows.
        /// </summary>
        public bool AddRowHoverActions = true;

        /// <summary>
        /// The stored procedure used to populate the grid with data.
        /// </summary>
        public string StoredProcedure = null;

        /// <summary>
        /// The user id of the user calling the grid procedure.
        /// Will always be the current session user id.
        /// </summary>
        public int StoredProcedureCallerId;

        /// <summary>
        /// The identifier column of the table we are pulling the data from.
        /// </summary>
        public string IdentifierField = null;

        /// <summary>
        /// The default column to sort by.
        /// </summary>
        public string DefaultSortColumn = String.Empty;

        /// <summary>
        /// The default number of records per page.
        /// </summary>
        public int DefaultRecordsPerPage = 25;

        /// <summary>
        /// Determines whether or not the default sort order is decending.
        /// </summary>
        public bool IsDefaultSortDescending = false;

        /// <summary>
        /// Determines whether or not to show the search filter box.
        /// </summary>
        public bool ShowSearchBox
        {
            get
            {
                if (ViewState["ShowSearchBox"] == null)
                {
                    return true;
                }
                else
                {
                    return Convert.ToBoolean(ViewState["ShowSearchBox"]);
                }
            }
            set { ViewState["ShowSearchBox"] = value; }
        }

        /// <summary>
        /// Placeholder text to show in the search textbox.
        /// </summary>
        public string SearchBoxPlaceholderText;

        /// <summary>
        /// Determines whether or not to format the search string to FTQ (full-text query) syntax.
        /// </summary>
        public bool SearchStringFormatFTQ = true;

        /// <summary>
        /// Determines whether or not to show the "Records per Page" selectbox.
        /// </summary>
        public bool ShowRecordsPerPageSelectbox
        {
            get
            {
                if (ViewState["ShowRecordsPerPageSelectbox"] == null)
                {
                    return true;
                }
                else
                {
                    return Convert.ToBoolean(ViewState["ShowRecordsPerPageSelectbox"]);
                }
            }
            set { ViewState["ShowRecordsPerPageSelectbox"] = value; }
        }

        /// <summary>
        /// Show the time with the dates when the returned value is a date.
        /// </summary>
        public bool ShowTimeInDateStrings = false;

        /// <summary>
        /// The number of data rows returned to the grid (read only).
        /// </summary>
        public int RowCount
        {
            get
            {
                return this._RowCount;
            }
        }

        /// <summary>
        /// This holds a public reference to the DataTable generated in BindData, in case
        /// we need direct access to any of the data returned for this grid for other functions
        /// inside a page. This prevents us from having to make a second call to a procedure to
        /// return the data.
        /// 
        /// Note: If we try to use this at any time prior to the data bind taking place, it will
        /// throw a NullReferenceException!
        /// </summary>
        public DataTable DataSourceDataTable;

        /// <summary>
        /// If true, load images from customer manager.
        /// </summary>
        public bool OverrideLoadImagesFromCustomerManager = false;

        #endregion

        #region Private Properties
        /// <summary>
        /// The data column to sort the grid by.
        /// </summary>
        private string _SortColumn
        {
            get
            {
                if (this.ViewState["SortColumn"] == null || (string)this.ViewState["SortColumn"] == String.Empty)
                { return DefaultSortColumn; }
                else
                { return this.ViewState["SortColumn"].ToString(); }
            }
            set
            {
                this.ViewState["SortColumn"] = value.ToString();
            }
        }

        /// <summary>
        /// Determines whether or not the sort column order is ascending or decending.
        /// </summary>
        private bool _IsSortAscending
        {
            get
            {
                if (this.ViewState["IsSortAscending"] == null && !this.IsDefaultSortDescending)
                { return true; }
                else if (this.ViewState["IsSortAscending"] == null && this.IsDefaultSortDescending)
                { return false; }
                else
                { return Convert.ToBoolean(this.ViewState["IsSortAscending"]); }
            }
            set
            {
                this.ViewState["IsSortAscending"] = Convert.ToBoolean(value);
            }
        }

        /// <summary>
        /// The number of data rows returned to the grid.
        /// </summary>
        private int _RowCount
        {
            get
            {
                if (this.ViewState["RowCount"] == null)
                { return -1; }
                else
                { return Convert.ToInt32(this.ViewState["RowCount"]); }
            }
            set
            {
                this.ViewState["RowCount"] = Convert.ToInt32(value);
            }
        }

        /// <summary>
        /// The search filter.
        /// </summary>
        private string _SearchParameter
        {
            get
            {
                if (this.ViewState["SearchParameter"] == null)
                { return String.Empty; }
                else
                { return this.ViewState["SearchParameter"].ToString(); }
            }
            set
            {
                this.ViewState["SearchParameter"] = value.ToString();
            }
        }

        /// <summary>
        /// Determines whether or not to load the pager and sort images from
        /// Customer Manager paths instead of the default Asentia paths. This
        /// will be set to true by the constructor if the database type is
        /// CustomerManager.
        /// </summary>
        private bool _LoadImagesFromCustomerManager = false;

        /// <summary>
        /// The text box object for the search filter.
        /// </summary>
        private TextBox _SearchBox = new TextBox();

        /// <summary>
        /// The selectbox object for controlling the number of records to show per page.
        /// </summary>
        private DropDownList _RecordsPerPage = new DropDownList();

        /// <summary>
        /// ArrayList to hold the columns to add to the grid.
        /// </summary>
        private ArrayList _Columns = new ArrayList();

        /// <summary>
        /// ArrayList to hold the columns that need data evaluation.
        /// </summary>
        private ArrayList _ColumnsToDataProcess = new ArrayList();

        /// <summary>
        /// An array of parameter names that are common to every grid, and hence built into this Grid object.
        /// </summary>
        private readonly string[] _ReservedParameterNames = { "@Return_Code", "@Error_Description_Code", "@searchParam", "@pageNum", "@pageSize", "@orderColumn", "@orderAsc" };

        /// <summary>
        /// ArrayList to hold object specific filters.
        /// The filters are used as SQL parameters in the call to the stored procedure used to build the grid.
        /// </summary>
        private ArrayList _Filters = new ArrayList();

        /// <summary>
        /// This will maintain the actual page count of the paged data source since the built-in
        /// PageCount of GridView does not accuarately do it because we are are not keeping an open
        /// recordset that is paged, we're doing paging in a very specific way. 
        /// </summary>
        private int _PageCount = 0;
        #endregion

        #region Overridden Properties
        /// <summary>
        /// The current page of the grid.
        /// </summary>
        public override int PageIndex
        {
            get
            {
                if (this.ViewState["PageIndex"] == null)
                { return 0; }
                else
                { return Convert.ToInt32(this.ViewState["PageIndex"]); }
            }
            set
            {
                this.ViewState["PageIndex"] = Convert.ToInt32(value);
            }
        }

        /// <summary>
        /// The number of records per page.
        /// </summary>
        public override int PageSize
        {
            get
            {
                if (this.ViewState["PageSize"] == null)
                { return this.DefaultRecordsPerPage; }
                else
                { return Convert.ToInt32(this.ViewState["PageSize"]); }
            }
            set
            {
                this.ViewState["PageSize"] = Convert.ToInt32(value);
            }
        }
        #endregion

        #region Methods
        #region AddFilter
        /// <summary>
        /// Adds an object specific filter (SQL parameter) for the grid object.
        /// </summary>
        /// <param name="filterName">The name of the filter.</param>
        /// <param name="filterType">The type of filter <see cref="SqlDbType"/>.</param>
        /// <param name="filterSize">The size of the filter.</param>
        /// <param name="filterValue">The value of the filter.</param>
        public void AddFilter(string filterName, SqlDbType filterType, int filterSize, object filterValue)
        {
            // prefix the filter name with @ if it's not done already
            if (filterName.IndexOf("@") != 0)
            { filterName = "@" + filterName; }

            // loop through the reserved parameter names to make sure this filter does not have a matching name
            for (int i = 0; i < _ReservedParameterNames.Length; i++)
            {
                if (filterName == _ReservedParameterNames[i])
                {
                    throw new AsentiaException("The specified parameter name is a reserved parameter for the Grid object.");
                }
            }

            // add the filter to the _Filters array list
            GridFilter filter = new GridFilter(filterName, filterType, filterSize, filterValue);
            this._Filters.Add(filter);
        }
        #endregion

        #region AddColumn
        /// <summary>
        /// Adds a column to the grid.
        /// </summary>
        /// <param name="gridColumn">The <see cref="GridColumn"/> to add.</param>
        public void AddColumn(GridColumn gridColumn)
        {
            // create a BoundField object
            BoundField boundField = new BoundField();
            boundField.HeaderText = gridColumn.HeaderText;
            boundField.DataField = gridColumn.DataField;
            boundField.ReadOnly = true;
            boundField.SortExpression = gridColumn.SortField;

            // if the data is to be centered in the column, set the css class for that
            if (gridColumn.CenterDataInColumn)
            { boundField.ItemStyle.CssClass += " centered "; }

            // if the column is supposed to be hidden for responsive (sub 640px wide), set the css class for that
            if (gridColumn.IsResponsiveHidden)
            { boundField.ItemStyle.CssClass += " ResponsiveColumnHidden "; }

            // add the BoundField to the internal _Columns array list.
            this._Columns.Add(boundField);

            // if this column has properties to evaluate, add it to the internal _ColumnsToDataProcess ArrayList
            if (gridColumn.Properties.Count > 0)
            { this._ColumnsToDataProcess.Add(gridColumn); }
        }
        #endregion

        #region BindData
        /// <summary>
        /// Binds data to the grid.
        /// </summary>
        public void BindData()
        {
            try
            {
                //It is a work around implemented as discussed with Joe on April 8, 2016.
                //If old page size is not null it means the page size was changed in previous 
                //action of next or last buttons because of less records at the last grid page 
                //so we need to retake the old page size value so that the grid display the 
                //required (selected or default page size) number of records.
                if (ViewState["OldPageSize"] != null)
                {
                    this.PageSize = Convert.ToInt32(ViewState["OldPageSize"]);
                    ViewState["OldPageSize"] = null;
                }

                // if this is a customer manager database call, make sure
                // the flag is set for any images to be loaded from the
                // customer manager file system
                if (this.DBType == DatabaseType.CustomerManager || this.OverrideLoadImagesFromCustomerManager)
                { this._LoadImagesFromCustomerManager = true; }

                // if the columns have not already been added, add them
                if (this.Columns.Count == 0)
                { _AddColumns(); }

                // retrieve the data for the grid
                SqlDataReader sqlDataReader = _RetrieveGridData();
                
                // read the first recordset from the data, it contains the row count
                sqlDataReader.Read();
                this._RowCount = Convert.ToInt32(sqlDataReader.GetValue(0));

                // move to the next recordset in the data, it contains the data
                // used to populate the grid
                sqlDataReader.NextResult();

                // create a DataTable and load the grid data into it
                DataTable dataSource = new DataTable();
                dataSource.Load(sqlDataReader);
                this.DataSourceDataTable = dataSource;

                //It is a work around implemented as discussed with Joe on April 8, 2016.
                //Logic bellow is basically for the grid's last page, if the difference is non zero ie. 
                //the records coming are less than the page size(that can be only in case of last page) 
                //then to change page size equal to the number of records coming at last page so that 
                //we could skip the issue coming at bottom previous or first button click immediately 
                //after clicking the last button.
                int recordsPagesizeDiff = this.PageSize - dataSource.Rows.Count;
                
                if (recordsPagesizeDiff > 0)
                {
                    //Store the page size
                    ViewState["OldPageSize"] = this.PageSize;
                    
                    //Change current page size to the row count value
                    this.PageSize = dataSource.Rows.Count;
                }
                
                // close the SqlDataReader
                sqlDataReader.Close();

                // set the DataTable as the grid's DataSource
                this.DataSource = dataSource;

                // bind the data to the grid
                this.DataBind();

                // dispose of the local dataSource DataTable
                dataSource.Dispose();
            }
            catch // bubble the exception up if one is caught
            { throw; }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _AddColumns
        /// <summary>
        /// Adds the columns to the grid.
        /// </summary>
        private void _AddColumns()
        {
            // if there are no columns to add, throw an exception
            if (this._Columns.Count == 0)
            { throw new AsentiaException("Grid does not contain any columns, cannot continue."); }

            // if a checkbox column is to be added, add it
            if (this.AddCheckboxColumn)
            {
                BoundField checkboxColumn = new BoundField();
                checkboxColumn.HeaderText = null;
                checkboxColumn.DataField = null;
                checkboxColumn.ReadOnly = true;
                checkboxColumn.SortExpression = null;

                this.Columns.Add(checkboxColumn);
            }

            // loop through the internal ArrayList of columns and add them to the grid
            foreach (BoundField boundField in this._Columns)
            { this.Columns.Add(boundField); }
        }
        #endregion

        #region _RetrieveGridData
        /// <summary>
        /// Retrives the grid data from the database.
        /// </summary>
        /// <returns>Grid</returns>
        private SqlDataReader _RetrieveGridData()
        {
            // create a database object
            AsentiaDatabase DatabaseObject = new AsentiaDatabase(DBType);

            try
            {
                DatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                DatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                // add object specific parameters
                foreach (GridFilter filter in this._Filters)
                {
                    DatabaseObject.AddParameter(filter.Name, filter.Value, filter.Type, filter.Size, ParameterDirection.Input);
                }

                // COMMON GRID PARAMETERS

                // SEARCH PARAMETER
                string fullTextSearchQuery = this._SearchParameter;

                if (this.SearchStringFormatFTQ)
                {
                    // build the full text query
                    FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                    fullTextSearchQuery = ftsQuery.ToFtsQuery(this._SearchParameter);
                }

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                DatabaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                // THE REST OF THE COMMON PARAMETERS
                DatabaseObject.AddParameter("@pageNum", this.PageIndex + 1, SqlDbType.Int, 4, ParameterDirection.Input);
                DatabaseObject.AddParameter("@pageSize", this.PageSize, SqlDbType.Int, 4, ParameterDirection.Input);
                DatabaseObject.AddParameter("@orderColumn", this._SortColumn, SqlDbType.NVarChar, 255, ParameterDirection.Input);
                DatabaseObject.AddParameter("@orderAsc", this._IsSortAscending, SqlDbType.Bit, 1, ParameterDirection.Input);

                // execute
                return DatabaseObject.ExecuteDataReader(this.StoredProcedure, true);
            }
            catch // bubble the exception up if one is caught
            {
                throw;
            }
            // DO NOT put a "finally" that disposes of the database object, it 
            // will break the Grid's data read operations, the Grid object itself
            // will do the disposal.
        }
        #endregion

        #region _Grid_Sorting
        /// <summary>
        /// Event handler for sorting the grid.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Arguments</param>
        private void _Grid_Sorting(object sender, GridViewSortEventArgs e)
        {
            // default sort ascending to true
            bool isSortAscending = true;

            // if the sort value on the sort column is already ascending, make it descending
            if (this._SortColumn == e.SortExpression
                && this._IsSortAscending)
            { isSortAscending = false; }

            // set the sort column and is sort ascending properties
            this._SortColumn = e.SortExpression;
            this._IsSortAscending = isSortAscending;

            // make the page index 0
            this.PageIndex = 0;

            // bind the data to the grid
            this.BindData();
        }
        #endregion

        #region _Grid_RowCreated
        private void _Grid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (this.AddCheckboxColumn)
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].CssClass = "GridCheckboxColumn";

                    // add the "Select All" checkbox to the first column of the HeaderRow 
                    CheckBox selectAll = new CheckBox();
                    selectAll.ID = this.ID + "_GridSelectAllRecords";
                    selectAll.Attributes.Add("onclick", "Grid.ToggleSelectAllRecords(\"" + this.ID + "\", this);");

                    e.Row.Cells[0].Controls.Add(selectAll);
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].CssClass = "GridCheckboxColumn";

                    // add the "Select Record" checkbox
                    CheckBox selectRecord = new CheckBox();
                    selectRecord.ID = this.ID + "_GridSelectRecord_" + e.Row.RowIndex.ToString();
                    selectRecord.Attributes.Add("onclick", "Grid.ToggleSelectRecord(\"" + this.ID + "\", this, " + e.Row.RowIndex.ToString() + ", false);");                    

                    e.Row.Cells[0].Controls.Add(selectRecord);
                }
            }
        }
        #endregion

        #region _Grid_RowDataBound
        /// <summary>
        /// Event handler for when data is bound to a grid row.
        /// This handles processing and formatting of data versus the properties
        /// and conditions set for the individual columns.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Arguments</param>
        private void _Grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            // if the bound row is a data row, perform data processing
            // based on grid column properties to data process
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int columnOrdinal = 0;

                // loop through each column of the row and process the column's data
                foreach (BoundField boundField in this.Columns)
                {
                    // if the column data is to be centered, set the css class
                    if (boundField.ItemStyle.CssClass.Contains("centered"))
                    { e.Row.Cells[columnOrdinal].CssClass += " centered "; }

                    // if the column is supposed to be hidden for responsive (sub 640px wide), set the css class for that
                    if (boundField.ItemStyle.CssClass.Contains("ResponsiveColumnHidden"))
                    { e.Row.Cells[columnOrdinal].CssClass += " ResponsiveColumnHidden "; }

                    // if there are no properties to process, just display the data
                    if (this._ColumnsToDataProcess.Count == 0)
                    {
                        // if the data is a date, it should be displayed as a formatted date
                        if (boundField.DataField.StartsWith("dt") || boundField.DataField.Contains("date") || boundField.DataField.Contains("timestamp"))
                        {
                            DateTime dateValue;
                            if (DateTime.TryParse(e.Row.Cells[columnOrdinal].Text, out dateValue))
                            {
                                // convert to local datetime
                                dateValue = TimeZoneInfo.ConvertTimeFromUtc(dateValue, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                                e.Row.Cells[columnOrdinal].Text = dateValue.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern) + " ";

                                if (this.ShowTimeInDateStrings)
                                { e.Row.Cells[columnOrdinal].Text += " @ " + dateValue.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern); }
                            }
                        }
                    }
                    // if there are properties to process, evaluate them against the data and apply the proper one
                    else
                    {
                        // loop through the columns that are to be processed
                        foreach (GridColumn gridColumn in this._ColumnsToDataProcess)
                        {
                            // if this column is to be processed, process it
                            if (boundField.DataField == gridColumn.DataField)
                            {
                                // if there is more than one property to process, loop through them and apply the correct one
                                if (gridColumn.Properties.Count > 1)
                                {
                                    // default to the "else" property condition
                                    bool useElseCondition = true;

                                    // loop through the properties and evaluate against the data
                                    foreach (GridColumnProperty gridColumnPropertyOuter in gridColumn.Properties)
                                    {
                                        // if the data matches the property condition, use it
                                        if (e.Row.Cells[columnOrdinal].Text == gridColumnPropertyOuter.Condition && gridColumnPropertyOuter.Condition != null)
                                        {
                                            useElseCondition = false;

                                            // set the data value to the HTML of the property
                                            e.Row.Cells[columnOrdinal].Text = gridColumnPropertyOuter.Html;

                                            // loop through the data keys and replace with data where we find replacers
                                            foreach (string dataKey in this.DataKeyNames)
                                            {
                                                e.Row.Cells[columnOrdinal].Text = e.Row.Cells[columnOrdinal].Text.Replace("##" + dataKey + "##", this.DataKeys[e.Row.RowIndex].Values[dataKey].ToString());
                                            }
                                        }

                                        // if this was the chosen property, break this loop
                                        if (!useElseCondition)
                                        { break; }
                                    }

                                    // if the loop was not broken above, that means we will use the else condition property
                                    if (useElseCondition)
                                    {
                                        // loop through the properties looking for the one with a null condition
                                        // this is our else property
                                        foreach (GridColumnProperty gridColumnPropertyInner in gridColumn.Properties)
                                        {
                                            if (gridColumnPropertyInner.Condition == null)
                                            {
                                                // set the data value to the HTML of the property
                                                e.Row.Cells[columnOrdinal].Text = gridColumnPropertyInner.Html;

                                                // loop through the data keys and replace with data where we find replacers
                                                foreach (string dataKey in this.DataKeyNames)
                                                {
                                                    e.Row.Cells[columnOrdinal].Text = e.Row.Cells[columnOrdinal].Text.Replace("##" + dataKey + "##", this.DataKeys[e.Row.RowIndex].Values[dataKey].ToString());
                                                }

                                                // break this loop
                                                break;
                                            }
                                        }
                                    }
                                }
                                // if there is only one property to process, use it
                                else if (gridColumn.Properties.Count == 1)
                                {
                                    // this should only go through one iteration of the loop
                                    foreach (GridColumnProperty gridColumnProperty in gridColumn.Properties)
                                    {
                                        // set the data value to the HTML of the property
                                        e.Row.Cells[columnOrdinal].Text = gridColumnProperty.Html;

                                        // loop through the data keys and replace with data where we find replacers
                                        foreach (string dataKey in this.DataKeyNames)
                                        {
                                            e.Row.Cells[columnOrdinal].Text = e.Row.Cells[columnOrdinal].Text.Replace("##" + dataKey + "##", this.DataKeys[e.Row.RowIndex].Values[dataKey].ToString());
                                        }

                                        // break this loop
                                        break;
                                    }
                                }
                            }
                            else // just display the data
                            {
                                // if the data is a date, it should be displayed as a formatted date
                                if (boundField.DataField.StartsWith("dt") || boundField.DataField.Contains("date") || boundField.DataField.Contains("timestamp"))
                                {
                                    DateTime dateValue;
                                    if (DateTime.TryParse(e.Row.Cells[columnOrdinal].Text, out dateValue))
                                    {
                                        // convert to local datetime
                                        dateValue = TimeZoneInfo.ConvertTimeFromUtc(dateValue, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));

                                        e.Row.Cells[columnOrdinal].Text = dateValue.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern) + " ";

                                        if (this.ShowTimeInDateStrings)
                                        { e.Row.Cells[columnOrdinal].Text += " @ " + dateValue.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern); }
                                    }
                                }

                                // determine if we need to break the loop because the current boundField is not
                                // in the columns to data process, this will prevent those columns from hitting
                                // the above else statement for each data processed column we need to check against
                                // if we dont do this, columns that don't get data processed will hit the else clause
                                // multiple times, which will result in issues when the column is a date column
                                bool doBreak = true;

                                foreach (GridColumn gc in this._ColumnsToDataProcess)
                                {
                                    if (boundField.DataField == gc.DataField)
                                    { doBreak = false; }
                                }

                                if (doBreak)
                                { break; }
                            }
                        }
                    }

                    // increment the column ordinal
                    columnOrdinal++;
                }
            }

            if (e.Row.RowType == DataControlRowType.EmptyDataRow)
            { e.Row.CssClass = "GridDataRowEmpty"; }
        }
        #endregion

        #region _Grid_DataBound
        /// <summary>
        /// Event handler for when all data has been bound to the grid.
        /// This handles the application of all asthetic and client side scripting 
        /// elements for the grid.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Arguments</param>
        private void _Grid_DataBound(object sender, EventArgs e)
        {
            // make sure the pager is displayed even if only one page
            if (this.TopPagerRow != null && this._RowCount > 0)
            { this.TopPagerRow.Visible = true; }

            if (this.BottomPagerRow != null && this._RowCount > 0)
            { this.BottomPagerRow.Visible = true; }

            // add styling to the grid table
            if (this.ShowSearchBox || this.ShowRecordsPerPageSelectbox)
            {
                Table gridTable = (Table)this.Controls[1];
                gridTable.CssClass = "GridTable";
            }
            else
            {
                Table gridTable = (Table)this.Controls[0];
                gridTable.CssClass = "GridTable";
            }

            this.HeaderRow.CssClass = "GridHeaderRow";

            // if the row is a header row, add the sort direction arrow to the sorted column, and
            // if there is a checkbox column, add a select all checkbox
            if (this.HeaderRow != null)
            {
                int columnOrdinal = 0;

                // loop through the columns, find the column that we are sorting on, and apply the proper sort arrow
                foreach (BoundField boundField in this.Columns)
                {                    
                    // if the column data is to be centered, set the css class
                    if (boundField.ItemStyle.CssClass.Contains("centered"))
                    { this.HeaderRow.Cells[columnOrdinal].CssClass += " centered "; }

                    // if the column is supposed to be hidden for responsive (sub 640px wide), set the css class for that
                    if (boundField.ItemStyle.CssClass.Contains("ResponsiveColumnHidden"))
                    { this.HeaderRow.Cells[columnOrdinal].CssClass += " ResponsiveColumnHidden "; }

                    // get the cell for this column ordinal
                    TableCell cell = this.HeaderRow.Cells[columnOrdinal];

                    // create a new image
                    Image sortImage = new Image();
                    sortImage.CssClass = "XSmallIcon";

                    // if this is the column we sorted on, add the proper arrow
                    if (boundField.SortExpression == this._SortColumn)
                    {
                        // if decending, use the descending arrow
                        // else, use the ascending arrow
                        if (!this._IsSortAscending)
                        {
                            sortImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DESC,
                                                                        ImageFiles.EXT_PNG,
                                                                        this._LoadImagesFromCustomerManager);
                            sortImage.AlternateText = _GlobalResources.DownArrow;
                            cell.Controls.Add(sortImage);
                        }
                        else
                        {
                            sortImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ASC,
                                                                        ImageFiles.EXT_PNG,
                                                                        this._LoadImagesFromCustomerManager);
                            sortImage.AlternateText = _GlobalResources.UpArrow;
                            cell.Controls.Add(sortImage);
                        }
                    }

                    columnOrdinal++;
                }
            }

            // loop through all rows in the grid, add row id and hover actions,
            // and if there is a checkbox to be added, add it
            int rowIndex = 0;

            foreach (GridViewRow gvr in this.Rows)
            {
                // if this is a data row, give it an id, add hover actions, and add checkbox
                if (gvr.RowType == DataControlRowType.DataRow)
                {
                    // add an id to the row
                    gvr.Attributes.Add("id", this.ID + "_GridRow_" + rowIndex.ToString());

                    // add css for alternating row
                    if (rowIndex % 2 == 1)
                    { gvr.CssClass = "GridDataRow GridDataRowAlternate"; }
                    else
                    { gvr.CssClass = "GridDataRow"; }

                    // if the grid uses hover actions, add them
                    if (this.AddRowHoverActions)
                    {
                        // add hover actions
                        gvr.Attributes.Add("onmouseover", "Grid.HoverRowOn(this);");
                        gvr.Attributes.Add("onmouseout", "Grid.HoverRowOff(\"" + this.ID + "\", this, " + rowIndex.ToString() + ");");
                    }

                    // if the grid has a checkbox column, data bind its checkbox
                    if (this.AddCheckboxColumn)
                    {
                        CheckBox checkBox = (CheckBox)gvr.FindControl(this.ID + "_GridSelectRecord_" + rowIndex.ToString());
                        checkBox.InputAttributes.Add("value", this.DataKeys[gvr.RowIndex].Values[IdentifierField].ToString());

                        // if we are adding row hover actions, make clicking the row select the checkbox as long as the checkbox is enabled
                        if (this.AddRowHoverActions && checkBox.Enabled)
                        { 
                            gvr.Attributes.Add("onclick", "Grid.ToggleSelectRecord(\"" + this.ID + "\", document.getElementById(\"" + checkBox.ID + "\"), " + rowIndex.ToString() + ", true);");
                            gvr.Style.Add("cursor", "pointer");
                        }
                    }
                }

                // increment the row index
                rowIndex++;
            }
        }
        #endregion

        #region _Grid_PageIndexChanging
        /// <summary>
        /// Event handler for when the PageIndex is changing.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Arguments</param>
        private void _Grid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // change the current page index to the new page index
            this.PageIndex = e.NewPageIndex;

            // bind the data to the grid
            this.BindData();
        }
        #endregion

        #region _ClearButton_Command
        /// <summary>
        /// Command event handler used to clear the search filter.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Arguments</param>
        private void _ClearButton_Command(object sender, EventArgs e)
        {
            // set properties that will clear the search filter
            this.PageIndex = 0;
            this._IsSortAscending = true;
            this._SortColumn = String.Empty;
            this._SearchParameter = String.Empty;

            // bind the data to the grid
            this.BindData();
        }
        #endregion

        #region _SearchButton_Command
        /// <summary>
        /// Command event handler used to apply a search filter.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Arguments</param>
        private void _SearchButton_Command(object sender, EventArgs e)
        {
            // set the properties to apply the search filter
            this.PageIndex = 0;
            this._IsSortAscending = true;
            this._SortColumn = String.Empty;
            this._SearchParameter = _SearchBox.Text;

            // bind the data to the grid
            this.BindData();
        }
        #endregion

        #region _PagerButton_Command
        private void _PagerButton_Command(object sender, CommandEventArgs e)
        {            
            int currentPageIndex = this.PageIndex;
            int newPageIndex = 0;

            switch (e.CommandName)
            {
                case "First":
                    newPageIndex = 0;
                    break;
                case "Previous":
                    if (currentPageIndex > 0)
                    { newPageIndex = currentPageIndex - 1; }
                    break;
                case "Next":
                    if (currentPageIndex != this._PageCount - 1)
                    { newPageIndex = currentPageIndex + 1; }
                    break;
                case "Last":
                    newPageIndex = this._PageCount - 1;
                    break;
            }

            OnPageIndexChanging(new GridViewPageEventArgs(newPageIndex));
        }
        #endregion

        #region _RecordsPerPageChanged
        /// <summary>
        /// Event handler used when the records per page drop-down has changed.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Arguments</param>
        private void _RecordsPerPageChanged(object sender, EventArgs e)
        {
            this.PageIndex = 0;
            this.PageSize = Convert.ToInt32(this._RecordsPerPage.SelectedValue);

            //It is a work around implemented as discussed with Joe on April 8, 2016.
            //Reset OldPageSize viewstate value
            ViewState["OldPageSize"] = null;
            
            // bind the data to the grid
            this.BindData();
        }
        #endregion

        #region _BuildSearchAndRecordsPerPagePanel
        /// <summary>
        /// Builds the grid search form and the records per page drop-down.
        /// </summary>
        private void _BuildSearchAndRecordsPerPagePanel()
        {
            if (this.ShowSearchBox || this.ShowRecordsPerPageSelectbox)
            {
                // spacer
                Literal spacer = new Literal();
                spacer.Text = "&nbsp;";

                // create the search table
                Table gridSearchTable = new Table();
                gridSearchTable.ID = this.ID + "_GridSearchTable";
                gridSearchTable.CssClass = "GridSearchTable";

                // create a row for the search table
                GridViewRow gridRow = new GridViewRow(0, -1, DataControlRowType.Header, DataControlRowState.Normal);

                // create a cell for the search table row, make it span all columns of the grid
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();

                if (this.ShowSearchBox)
                {
                    Panel gridSearchControlsContainer = new Panel();
                    gridSearchControlsContainer.ID = this.ID + "_GridSearchControlsContainer";

                    // set properties for the search text box
                    this._SearchBox.ID = this.ID + "_GridSearchTextBox";
                    this._SearchBox.CssClass = "GridSearchTextBox";
                    this._SearchBox.Attributes.Add("autocomplete", "off");

                    if (!String.IsNullOrWhiteSpace(this.SearchBoxPlaceholderText))
                    { this._SearchBox.Attributes.Add("placeholder", this.SearchBoxPlaceholderText); }

                    this._SearchBox.Text = this._SearchParameter;

                    // create the search button that will apply the search filter
                    Button searchButton = new Button();
                    searchButton.ID = this.ID + "_GridSearchButton";
                    searchButton.CssClass = "Button ActionButton SearchButton";
                    searchButton.Text = _GlobalResources.Search;
                    searchButton.Attributes.Add("onclick", "Grid.ShowLoadingPanel(\"" + this.ID + "\");");
                    searchButton.Command += new CommandEventHandler(this._SearchButton_Command);

                    // make the search button the default button
                    gridSearchControlsContainer.DefaultButton = searchButton.ID;

                    // create a clear button that will clear the search filter
                    Button clearButton = new Button();
                    clearButton.ID = this.ID + "_GridClearSearchButton";
                    clearButton.CssClass = "Button NonActionButton";
                    clearButton.Text = _GlobalResources.Clear;
                    clearButton.Attributes.Add("onclick", "Grid.ShowLoadingPanel(\"" + this.ID + "\");");
                    clearButton.Command += new CommandEventHandler(this._ClearButton_Command);

                    // add the search box, search button, and clear button controls to the controls container
                    gridSearchControlsContainer.Controls.Add(_SearchBox);
                    gridSearchControlsContainer.Controls.Add(searchButton);
                    gridSearchControlsContainer.Controls.Add(spacer);
                    gridSearchControlsContainer.Controls.Add(clearButton);

                    // add the controls container to the cell
                    cell1.Controls.Add(gridSearchControlsContainer);
                }

                if (this.ShowRecordsPerPageSelectbox)
                {
                    Localize recordsPerPageLabel = new Localize();
                    recordsPerPageLabel.Text = _GlobalResources.RecordsPerPage + " ";

                    this._RecordsPerPage.ID = this.ID + "_GridRecordsPerPage";

                    if (_RecordsPerPage.Items.Count == 0)
                    {
                        this._RecordsPerPage.Items.Add("25");
                        this._RecordsPerPage.Items.Add("50");
                        this._RecordsPerPage.Items.Add("75");
                        this._RecordsPerPage.Items.Add("100");
                    }

                    this._RecordsPerPage.AutoPostBack = true;
                    this._RecordsPerPage.Attributes.Add("onchange", "Grid.ShowLoadingPanel(\"" + this.ID + "\");");
                    this._RecordsPerPage.SelectedIndexChanged += new EventHandler(this._RecordsPerPageChanged);

                    cell2.Controls.Add(recordsPerPageLabel);
                    cell2.Controls.Add(this._RecordsPerPage);
                }

                // add the cell to the row
                gridRow.Cells.Add(cell1);
                gridRow.Cells.Add(cell2);

                // add the row to the search table
                gridSearchTable.Rows.AddAt(0, gridRow);

                // add the search table to this grid's controls at index 0
                this.Controls.AddAt(0, gridSearchTable);
            }
        }
        #endregion

        #region _BuildGridLoadingPanel
        /// <summary>
        /// Builds the grid loading panel that shows when a grid action (previous, next, etc) is performed.
        /// </summary>
        private void _BuildGridLoadingPanel()
        {
            Panel gridLoadingPanel = new Panel();
            gridLoadingPanel.ID = this.ID + "_GridLoadingPanel";
            gridLoadingPanel.CssClass = "GridLoadingPanel";

            Image gridLoadingImage = new Image();
            gridLoadingImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LOADING, ImageFiles.EXT_GIF, this._LoadImagesFromCustomerManager);            
            gridLoadingPanel.Controls.Add(gridLoadingImage);

            this.Controls.Add(gridLoadingPanel);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {            
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Grid.js");
        }
        #endregion

        #region Render
        /// <summary>
        /// Overrides the Render method so that we can manipulate the HTML that this control renders.
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            string html = String.Empty;

            using (StringWriter stringWriter = new StringWriter())
            using (HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter))
            {
                // render the current page content to our temp writer
                base.Render(htmlWriter);
                htmlWriter.Close();

                // get the rendered html
                html = stringWriter.ToString();
            }

            // manipulate the rendered HTML so that we can add a class to the containing <div> it renders,
            // we do this through a RegEx so that we can replace only the first occurrence 
            // note, this is the only way to do this, the <div> is not in the control stack, because .NET
            Regex divRegex = new Regex(Regex.Escape("<div>"));
            string newHtml = divRegex.Replace(html, "<div class=\"GridContainer\">", 1);

            // write the new html to the page
            writer.Write(newHtml);
        }
        #endregion

        #region CreateChildControls
        /// <summary>
        /// Overrides CreateChildControls so that we can add our own custom controls around the grid.
        /// (Example: The search control)
        /// This is where we would add any controls that should appear above or below the grid.
        /// </summary>
        /// <param name="dataSource">Data Source</param>
        /// <param name="dataBinding">Data Binding</param>
        /// <returns></returns>
        protected override int CreateChildControls(IEnumerable dataSource, bool dataBinding)
        {            
            // execute the base fuctionality of this method so that the grid control is built
            int count = base.CreateChildControls(dataSource, dataBinding);

            // build the search box and records per page selector
            this._BuildSearchAndRecordsPerPagePanel();

            // build the grid loading panel
            this._BuildGridLoadingPanel();           

            // return
            return count;
        }
        #endregion

        #region PrepareControlHierarchy
        /// <summary>
        /// Overrides PrepareControlHierarchy to keep that method from performing its normal actions.
        /// This grid has additional custom controls added to it, so we control its hierarchy from this class.
        /// </summary>
        protected override void PrepareControlHierarchy()
        { ;}
        #endregion

        #region InitializePager
        /// <summary>
        /// Overrides InitializePager so that we can use custom paging.
        /// </summary>
        /// <param name="row">Row</param>
        /// <param name="columnSpan">Column Span</param>
        /// <param name="pagedDataSource">Paged Data Source</param>
        protected override void InitializePager(GridViewRow row, int columnSpan, PagedDataSource pagedDataSource)
        {                        
            // if this grid has rows, set the custom paging properties
            if (this._RowCount > -1)
            {                
                pagedDataSource.AllowCustomPaging = true;
                pagedDataSource.VirtualCount = this._RowCount;
                pagedDataSource.CurrentPageIndex = this.PageIndex;
            }

            // set the page count
            this._PageCount = pagedDataSource.PageCount;

            Panel pager = new Panel();
            pager.ID = this.ID + "_GridPager";

            Table pagerTable = new Table();
            pagerTable.ID = this.ID + "_GridPagerTable";
            pagerTable.CssClass = "GridPagerTable";
            pagerTable.CellPadding = 0;
            pagerTable.CellSpacing = 0;

            TableRow pagerRow = new TableRow();
            pagerRow.ID = this.ID + "_GridPagerTableRow";

            Literal pageIndex = new Literal();
            pageIndex.ID = this.ID + "_GridPageIndexLit";
            pageIndex.Text = (this.PageIndex + 1).ToString();

            Literal pageCount = new Literal();
            pageCount.ID = this.ID + "_GridPageCountLit";            

            //It is a work around implemented as discussed with Joe on April 8, 2016.
            //if it is last page then set the page count text same as page index text.
            if (!String.IsNullOrWhiteSpace(Convert.ToString(ViewState["OldPageSize"])))
            {
                pageCount.Text = (this.PageIndex + 1).ToString();
            }
            else
            {
                pageCount.Text = (this._PageCount).ToString();
            }

            TableCell pageXofY = new TableCell();
            pageXofY.ID = this.ID + "_GridPagerCellPageXofY";
            pageXofY.CssClass = "GridPagerCellPageXofY";
            pageXofY.Controls.Add(new LiteralControl(_GlobalResources.Page + " "));
            pageXofY.Controls.Add(pageIndex);
            pageXofY.Controls.Add(new LiteralControl(" " + _GlobalResources.of_lower + " "));
            pageXofY.Controls.Add(pageCount);

            ImageButton first = new ImageButton();
            first.ID = this.ID + "_GridPagerButtonFirst";
            first.CommandName = "First";
            first.ToolTip = _GlobalResources.FirstPage;
            first.ImageAlign = ImageAlign.AbsMiddle;
            first.Style.Add("cursor", "pointer");
            first.CausesValidation = false;
            first.Attributes.Add("onclick", "Grid.ShowLoadingPanel(\"" + this.ID + "\");");
            first.Command += new CommandEventHandler(_PagerButton_Command);

            ImageButton previous = new ImageButton();
            previous.ID = this.ID + "_GridPagerButtonPrevious";
            previous.CommandName = "Previous";
            previous.ToolTip = _GlobalResources.PreviousPage;
            previous.ImageAlign = ImageAlign.AbsMiddle;
            previous.Style.Add("cursor", "pointer");
            previous.CausesValidation = false;
            previous.Attributes.Add("onclick", "Grid.ShowLoadingPanel(\"" + this.ID + "\");");
            previous.Command += new CommandEventHandler(_PagerButton_Command);

            ImageButton next = new ImageButton();
            next.ID = this.ID + "_GridPagerButtonNext";
            next.CommandName = "Next";
            next.ToolTip = _GlobalResources.NextPage;
            next.ImageAlign = ImageAlign.AbsMiddle;
            next.Style.Add("cursor", "pointer");
            next.CausesValidation = false;
            next.Attributes.Add("onclick", "Grid.ShowLoadingPanel(\"" + this.ID + "\");");
            next.Command += new CommandEventHandler(_PagerButton_Command);

            ImageButton last = new ImageButton();
            last.ID = this.ID + "_GridPagerButtonLast";
            last.CommandName = "Last";
            last.ToolTip = _GlobalResources.LastPage;
            last.ImageAlign = ImageAlign.AbsMiddle;
            last.Style.Add("cursor", "pointer");
            last.CausesValidation = false;
            last.Attributes.Add("onclick", "Grid.ShowLoadingPanel(\"" + this.ID + "\");");
            last.Command += new CommandEventHandler(_PagerButton_Command);

            if (this.PageIndex > 0)
            {
                first.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FIRST,
                                                        ImageFiles.EXT_PNG,
                                                        this._LoadImagesFromCustomerManager);
                first.CssClass = "MediumIcon";

                previous.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PREVIOUS,
                                                           ImageFiles.EXT_PNG,
                                                           this._LoadImagesFromCustomerManager);
                previous.CssClass = "MediumIcon";

                first.Enabled = true;
                previous.Enabled = true;
            }
            else
            {
                first.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FIRST,
                                                        ImageFiles.EXT_PNG,
                                                        this._LoadImagesFromCustomerManager);
                first.CssClass = "MediumIcon DimIcon";

                previous.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PREVIOUS,
                                                           ImageFiles.EXT_PNG,
                                                           this._LoadImagesFromCustomerManager);
                previous.CssClass = "MediumIcon DimIcon";

                first.Enabled = false;
                previous.Enabled = false;
                first.Style.Add("cursor", "default");
                previous.Style.Add("cursor", "default");
            }

            //It is a work around implemented as discussed with Joe on April 8, 2016.
            //If it is last page or old page size value is not null then disable buttons.
            if ((this.PageIndex == pagedDataSource.PageCount - 1) || !String.IsNullOrWhiteSpace(Convert.ToString(ViewState["OldPageSize"])))
            {
                next.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_NEXT,
                                                       ImageFiles.EXT_PNG,
                                                       this._LoadImagesFromCustomerManager);
                next.CssClass = "MediumIcon DimIcon";

                last.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LAST,
                                                       ImageFiles.EXT_PNG,
                                                       this._LoadImagesFromCustomerManager);
                last.CssClass = "MediumIcon DimIcon";

                next.Enabled = false;
                last.Enabled = false;
                next.Style.Add("cursor", "default");
                last.Style.Add("cursor", "default");
            }
            else
            {
                next.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_NEXT,
                                                       ImageFiles.EXT_PNG,
                                                       this._LoadImagesFromCustomerManager);
                next.CssClass = "MediumIcon";

                last.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LAST,
                                                       ImageFiles.EXT_PNG,
                                                       this._LoadImagesFromCustomerManager);
                last.CssClass = "MediumIcon";

                next.Enabled = true;
                last.Enabled = true;
            }


            TableCell buttons = new TableCell();
            buttons.ID = this.ID + "_GridPagerCellButtons";
            buttons.CssClass = "GridPagerCellButtons";
            buttons.Controls.Add(first);
            buttons.Controls.Add(previous);
            buttons.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
            buttons.Controls.Add(next);
            buttons.Controls.Add(last);

            TableCell recordCount = new TableCell();
            recordCount.ID = this.ID + "_GridPagerCellRecordCount";
            recordCount.CssClass = "GridPagerCellRecordCount";
            recordCount.Controls.Add(new LiteralControl(this._RowCount.ToString() + " " + _GlobalResources.RecordsFound));

            pagerRow.Cells.Add(pageXofY);
            pagerRow.Cells.Add(buttons);
            pagerRow.Cells.Add(recordCount);

            pagerTable.Rows.Add(pagerRow);

            pager.Controls.Add(pagerTable);

            row.Controls.AddAt(0, new TableCell());
            row.Cells[0].ColumnSpan = columnSpan;
            row.Cells[0].Controls.Add(pager);            
        }
        #endregion
        #endregion
    }
}
