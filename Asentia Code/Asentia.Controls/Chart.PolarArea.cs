﻿using System;
using System.Collections;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    #region Chart_PolarArea_Dataset CLASS
    public class Chart_PolarArea_Dataset
    {
        #region Constructor
        public Chart_PolarArea_Dataset(string fillColor, object value)
        {
            this._FillColor = fillColor;
            this._Value = value;
        }
        #endregion

        #region Properties
        public string FillColor
        {
            get { return this._FillColor; }
        }

        public object Value
        {
            get { return this._Value; }
        }
        #endregion

        #region Private Properties
        private string _FillColor;
        private object _Value;
        #endregion
    }
    #endregion

    public class Chart_PolarArea: WebControl
    {
        #region Constructor
        public Chart_PolarArea(string id, int width, int height)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.CanvasWidth = width;
            this.CanvasHeight = height;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The width of the canvas tag.
        /// </summary>
        public int CanvasWidth;

        /// <summary>
        /// The height of the canvas tag.
        /// </summary>
        public int CanvasHeight;

        /// <summary>
        /// Show scale over chart data?
        /// </summary>
	    public bool ScaleOverlay = true;

        /// <summary>
        /// Override with hard-coded scale?
        /// If true, ScaleSteps, ScaleStepWidth and ScaleStartValue must be set.
        /// </summary>
	    public bool ScaleOverride = false;

        /// <summary>
        /// The number of steps in the hard-coded scale.
        /// Must be set if ScaleOverride is true.
        /// </summary>
	    public int? ScaleSteps = null;

        /// <summary>
        /// The value jump in the hard-coded scale.
        /// Must be set if ScaleOverride is true.
        /// </summary>
	    public int? ScaleStepWidth = null;

        /// <summary>
        /// The starting value of the hard-coded scale.
        /// Must be set if ScaleOverride is true.
        /// </summary>
	    public int? ScaleStartValue = null;
	
	    /// <summary>
	    /// Show line for each value in the scale?
	    /// </summary>
	    public bool ScaleShowLine = true;

        /// <summary>
        /// Color of the scale line.
        /// </summary>
	    public string ScaleLineColor = "rgba(0,0,0,.1)";

        /// <summary>
        /// Width of the scale line.
        /// </summary>
	    public int ScaleLineWidth = 1;

        /// <summary>
        /// Show labels on the scale?
        /// </summary>
	    public bool ScaleShowLabels = true;

        /// <summary>
        /// Interpolated JS string - can access value
        /// </summary>
	    public string ScaleLabel = "<%=value%>";

        /// <summary>
        /// The font of the scale labels.
        /// </summary>
        public string ScaleFontFamily = "Helvetica";

        /// <summary>
        /// The font size of the scale labels in pixels.
        /// </summary>
	    public int ScaleFontSize = 12;

        /// <summary>
        /// The font style of the scale labels.
        /// </summary>
	    public string ScaleFontStyle = "normal";

        /// <summary>
        /// The font color of the scale labels.
        /// </summary>
        public string ScaleFontColor = "#3D3A39";
	
	    /// <summary>
	    /// Show a backdrop to the scale label?
	    /// </summary>
	    public bool ScaleShowLabelBackdrop = true;
	
	    /// <summary>
	    /// The color of the label backdrop.
	    /// </summary>
	    public string ScaleBackdropColor = "rgba(255,255,255,0.75)";
	
        /// <summary>
        /// The backdrop padding above & below the label in pixels.
        /// </summary>
	    public int ScaleBackdropPaddingY = 2;
	
	    /// <summary>
        /// The backdrop padding to the side of the label in pixels.
	    /// </summary>
	    public int ScaleBackdropPaddingX = 2;

	    /// <summary>
        /// Stroke a line around each segment in the chart?
	    /// </summary>
	    public bool SegmentShowStroke = true;
	
	    /// <summary>
        /// The color of the stroke on each segement.
	    /// </summary>
	    public string SegmentStrokeColor = "#FFFFFF";
	
	    /// <summary>
        /// The width of the stroke value in pixels.
	    /// </summary>
	    public int SegmentStrokeWidth = 2;

        /// <summary>
        /// Animate the chart?
        /// </summary>
	    public bool Animation = true;

        /// <summary>
        /// Number of animation steps.
        /// </summary>
	    public int AnimationSteps = 100;

        /// <summary>
        /// Animation easing effect.
        /// </summary>
	    public string AnimationEasing = "easeOutBounce";

        /// <summary>
        /// Animate the rotation of the chart?
        /// </summary>
	    public bool AnimateRotate = true;

        /// <summary>
        /// Animate scaling of the chart from center?
        /// </summary>
	    public bool AnimateScale = false;

        /// <summary>
        /// Method to fire when animation is complete.
        /// </summary>
	    public string OnAnimationComplete = null;
        #endregion

        #region Private Properties
        /// <summary>
        /// An ArrayList of datasets for this chart.
        /// </summary>
        ArrayList _Datasets = null;
        #endregion

        #region Methods
        #region AddDataset
        // TODO: VALIDATIONS
        /// <summary>
        /// Adds a dataset of type Chart_PolarArea_Dataset to the dataset ArrayList.
        /// </summary>
        /// <param name="dataset">dataset data</param>
        public void AddDataset(Chart_PolarArea_Dataset dataset)
        {
            if (this._Datasets == null)
            { this._Datasets = new ArrayList(); }

            this._Datasets.Add(dataset);
        }
        #endregion

        #region ClearDatasets
        /// <summary>
        /// Clears the dataset ArrayList.
        /// </summary>
        public void ClearDatasets()
        {
            if (this._Datasets == null)
            { return; }

            this._Datasets.Clear();
        }
        #endregion
        #endregion

        #region Private Methods
        #region _BuildOptionsJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's options
        /// based on the option properties.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildOptionsJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON
            sb.AppendLine("var " + this.ID + "Options = {");
            sb.AppendLine(" scaleOverlay : " + this.ScaleOverlay.ToString().ToLower() + ",");
            sb.AppendLine(" scaleOverride : " + this.ScaleOverride.ToString().ToLower() + ",");
            sb.AppendLine(" scaleSteps : " + (this.ScaleSteps == null ? "null" : this.ScaleSteps.ToString()) + ",");
            sb.AppendLine(" scaleStepWidth : " + (this.ScaleStepWidth == null ? "null" : this.ScaleStepWidth.ToString()) + ",");
            sb.AppendLine(" scaleStartValue : " + (this.ScaleStartValue == null ? "null" : this.ScaleStartValue.ToString()) + ",");
            sb.AppendLine(" scaleShowLine : " + this.ScaleShowLine.ToString().ToLower() + ",");
            sb.AppendLine(" scaleLineColor : " + this._FormatJSStringVarValue(this.ScaleLineColor) + ",");
            sb.AppendLine(" scaleLineWidth : " + this.ScaleLineWidth.ToString() + ",");
            sb.AppendLine(" scaleShowLabels : " + this.ScaleShowLabels.ToString().ToLower() + ",");
            sb.AppendLine(" scaleLabel : " + this._FormatJSStringVarValue(this.ScaleLabel) + ",");
            sb.AppendLine(" scaleFontFamily : " + this._FormatJSStringVarValue(this.ScaleFontFamily) + ",");
            sb.AppendLine(" scaleFontSize : " + this.ScaleFontSize.ToString() + ",");
            sb.AppendLine(" scaleFontStyle : " + this._FormatJSStringVarValue(this.ScaleFontStyle) + ",");
            sb.AppendLine(" scaleFontColor : " + this._FormatJSStringVarValue(this.ScaleFontColor) + ",");
            sb.AppendLine(" scaleShowLabelBackdrop : " + this.ScaleShowLabelBackdrop.ToString().ToLower() + ",");
            sb.AppendLine(" scaleBackdropColor : " + this._FormatJSStringVarValue(this.ScaleBackdropColor) + ",");
            sb.AppendLine(" scaleBackdropPaddingY : " + this.ScaleBackdropPaddingY.ToString() + ",");
            sb.AppendLine(" scaleBackdropPaddingX : " + this.ScaleBackdropPaddingX.ToString() + ",");
            sb.AppendLine(" segmentShowStroke : " + this.SegmentShowStroke.ToString().ToLower() + ",");
            sb.AppendLine(" segmentStrokeColor  : " + this._FormatJSStringVarValue(this.SegmentStrokeColor) + ",");
            sb.AppendLine(" segmentStrokeWidth  : " + this.SegmentStrokeWidth.ToString() + ",");
            sb.AppendLine(" animation : " + this.Animation.ToString().ToLower() + ",");
            sb.AppendLine(" animationSteps : " + this.AnimationSteps.ToString() + ",");
            sb.AppendLine(" animationEasing : " + this._FormatJSStringVarValue(this.AnimationEasing) + ",");
            sb.AppendLine(" animateRotate : " + this.AnimateRotate.ToString().ToLower() + ",");
            sb.AppendLine(" animateScale : " + this.AnimateScale.ToString().ToLower() + ",");
            sb.AppendLine(" onAnimationComplete : " + this._FormatJSStringVarValue(this.OnAnimationComplete));
            sb.AppendLine("};");

            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildDataJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's data
        /// based on the datasets popuated for this chart.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildDataJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // begin variable declaration
            sb.AppendLine("var " + this.ID + "Data = [");

            int i = 0;

            foreach (Chart_PolarArea_Dataset ds in this._Datasets)
            {
                // increment i
                i++;

                // build the data
                sb.AppendLine(" {");
                sb.AppendLine(" value : " + ds.Value + ",");
                sb.AppendLine(" color : " + this._FormatJSStringVarValue(ds.FillColor) + ",");

                if (i < this._Datasets.Count)
                { sb.AppendLine("   },"); }
                else
                { sb.AppendLine("   }"); }
            }

            sb.AppendLine("];");

            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildAndRegisterJSBlock
        /// <summary>
        /// Builds the JS block that renders the chart, and registers the
        /// JS to this control.
        /// </summary>
        private void _BuildAndRegisterJSBlock()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON
            sb.AppendLine(this._BuildOptionsJSONString());

            // build data JSON
            sb.AppendLine(this._BuildDataJSONString());

            // build call to chart function
            sb.AppendLine("var " + this.ID + "Bound = new Chart(document.getElementById(\"" + this.ID + "Canvas\").getContext(\"2d\")).PolarArea(" + this.ID + "Data, " + this.ID + "Options);");

            // register script
            ScriptManager.RegisterStartupScript(this, this.GetType(), this.ID + "StartUpScript", sb.ToString(), true);
        }
        #endregion

        #region _FormatJSStringVarValue
        /// <summary>
        /// Formats a string for attachment to a JS variable.
        /// </summary>
        /// <param name="value">string value</param>
        /// <returns>formatted string</returns>
        private string _FormatJSStringVarValue(string value)
        {
            if (value == null)
            { return "null"; }
            else
            { return "\"" + value + "\""; }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Chart.js");
        }
        #endregion

        #region RenderContents
        protected override void RenderContents(HtmlTextWriter writer)
        {
            // canvas
            HtmlGenericControl canvasTag = new HtmlGenericControl("canvas");
            canvasTag.ID = this.ID + "Canvas";
            canvasTag.Attributes.Add("width", this.CanvasWidth.ToString());
            canvasTag.Attributes.Add("height", this.CanvasHeight.ToString());

            // attach controls
            this.Controls.Add(canvasTag);

            // build and register JS block
            this._BuildAndRegisterJSBlock();

            // execute base
            base.RenderContents(writer);
        }
        #endregion
        #endregion
    }
}
