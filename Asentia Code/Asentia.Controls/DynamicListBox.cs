﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    /// <summary>
    /// ListBox control with select all and select none controls, and option for search.
    /// </summary>
    public class DynamicListBox : WebControl, INamingContainer
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">the id of this control</param>
        public DynamicListBox(string id)
            : base(HtmlTextWriterTag.Div)
        {
            // set initialization variables
            this.ID = id;
        }
        #endregion

        #region Properties
        /// <summary>
        /// With this ListBox include a search box?
        /// </summary>
        public bool IsSearchable = true;

        /// <summary>
        /// Will this ListBox include select "All" and "None" options?
        /// </summary>
        public bool IncludeSelectAllNone = true;

        /// <summary>
        /// Is the ListBox multiple select?
        /// </summary>
        public bool IsMultipleSelect = true;

        /// <summary>
        /// The "Search" button.
        /// </summary>
        public Button SearchButton = new Button();

        /// <summary>
        /// The "Clear" button for clearing the search.
        /// </summary>
        public Button ClearSearchButton = new Button();

        /// <summary>
        /// The "Search" text box.
        /// </summary>
        public TextBox SearchTextBox = new TextBox();

        /// <summary>
        /// The ListBox control.
        /// </summary>
        public ListBox ListBoxControl = new ListBox();

        /// <summary>
        /// Message to display in ListBox if there are no records found upon DataBind.
        /// </summary>
        public string NoRecordsFoundMessage = null;
        #endregion

        #region Private Properties
        /// <summary>
        /// Container for "Search."
        /// </summary>
        private Panel _SearchContainer;

        /// <summary>
        /// Container for the ListBox.
        /// </summary>
        private Panel _ListBoxContainer;

        /// <summary>
        /// Container for the select "All" and "None" links.
        /// </summary>
        private Panel _SelectAllNoneContainer;
        #endregion

        #region Private Methods
        #region _BuildSearchContainer
        /// <summary>
        /// Builds the "Search" controls into the "Search" container.
        /// </summary>
        private void _BuildSearchContainer()
        {
            this._SearchContainer = new Panel();
            this._SearchContainer.ID = this.ID + "SearchContainer";
            this._SearchContainer.CssClass = "ListBoxSearchContainer";

            this.SearchTextBox.ID = this.ID + "SearchTextBox";
            this.SearchTextBox.CssClass = "InputText InputMedium";

            this.SearchButton.ID = this.ID + "SearchButton";
            this.SearchButton.CssClass = "Button ActionButton SearchButton";
            this.SearchButton.Text = _GlobalResources.Search;

            this.ClearSearchButton.ID = this.ID + "ClearSearchButton";
            this.ClearSearchButton.CssClass = "Button NonActionButton";
            this.ClearSearchButton.Text = _GlobalResources.Clear;

            this._SearchContainer.Controls.Add(this.SearchTextBox);
            this._SearchContainer.Controls.Add(this.SearchButton);
            this._SearchContainer.Controls.Add(this.ClearSearchButton);

            this._SearchContainer.DefaultButton = this.SearchButton.ID;

            this.Controls.Add(this._SearchContainer);
        }
        #endregion

        #region _BuildListBoxContainer
        /// <summary>
        /// Builds the ListBox control into the "ListBox" container.
        /// </summary>
        private void _BuildListBoxContainer()
        {
            this._ListBoxContainer = new Panel();
            this._ListBoxContainer.ID = this.ID + "ListBoxContainer";

            this.ListBoxControl.ID = this.ID + "ListBox";
            this.ListBoxControl.CssClass = "ListBox";

            if (this.IsMultipleSelect)
            { this.ListBoxControl.SelectionMode = ListSelectionMode.Multiple; }
            else
            { this.ListBoxControl.SelectionMode = ListSelectionMode.Single; }

            this._ListBoxContainer.Controls.Add(this.ListBoxControl);

            this.Controls.Add(this._ListBoxContainer);
        }
        #endregion

        #region _BuildSelectAllNoneContainer
        /// <summary>
        /// Builds the select "All" and "None" links into the container.
        /// </summary>
        private void _BuildSelectAllNoneContainer()
        {
            this._SelectAllNoneContainer = new Panel();
            this._SelectAllNoneContainer.ID = this.ID + "SelectAllNoneContainer";
            this._SelectAllNoneContainer.CssClass = "ListBoxSelectAllNoneContainer";

            HyperLink selectAll = new HyperLink();
            selectAll.ID = this.ID + "SelectAllLink";
            selectAll.NavigateUrl = "javascript:$(\"#" + this.ListBoxControl.ID + " option\").prop(\"selected\", true);";
            selectAll.Text = _GlobalResources.All;

            HyperLink selectNone = new HyperLink();
            selectNone.ID = this.ID + "SelectNoneLink";
            selectNone.NavigateUrl = "javascript:$(\"#" + this.ListBoxControl.ID + " option\").prop(\"selected\", false);";
            selectNone.Text = _GlobalResources.None;

            Literal selectText = new Literal();
            selectText.Text = _GlobalResources.Select + " ";

            Literal allNoneSeparator = new Literal();
            allNoneSeparator.Text = "&nbsp;|&nbsp;";

            this._SelectAllNoneContainer.Controls.Add(selectText);
            this._SelectAllNoneContainer.Controls.Add(selectAll);
            this._SelectAllNoneContainer.Controls.Add(allNoneSeparator);
            this._SelectAllNoneContainer.Controls.Add(selectNone);

            this.Controls.Add(this._SelectAllNoneContainer);
        }
        #endregion
        #endregion

        #region Methods
        #region GetSelectedValues
        /// <summary>
        /// Gets selected values from the ListBox.
        /// </summary>
        /// <returns>list of selected items</returns>
        public List<string> GetSelectedValues()
        {
            List<string> selectedValues = new List<string>();

            if (this.IsMultipleSelect)
            {
                for (int i = 0; i < this.ListBoxControl.Items.Count; i++)
                {
                    if (this.ListBoxControl.Items[i].Selected)
                    { selectedValues.Add(this.ListBoxControl.Items[i].Value); }
                }
            }
            else
            { selectedValues.Add(this.ListBoxControl.SelectedValue); }

            return selectedValues;
        }
        #endregion

        #region RemoveSelectedItems
        /// <summary>
        /// Removes selected items from ListBox.
        /// </summary>
        public void RemoveSelectedItems()
        {
            for (int i = this.ListBoxControl.Items.Count - 1; i >= 0; i--)
            {
                if (this.ListBoxControl.Items[i].Selected)
                { this.ListBoxControl.Items.RemoveAt(i); }
            }            
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // check to see if there are any items and if not, display the "no records found" 
            // message, and disable ListBox
            if (this.ListBoxControl.Items.Count <= 0)
            {
                this.ListBoxControl.Items.Add(new ListItem(this.NoRecordsFoundMessage));
                this.ListBoxControl.Enabled = false;
            }
            else
            { this.ListBoxControl.Enabled = true; }

        }
        #endregion

        #region CreateChildControls
        protected override void CreateChildControls()
        {
            if (this.IsSearchable)
            { this._BuildSearchContainer(); }

            this._BuildListBoxContainer();

            if (this.IncludeSelectAllNone)
            { this._BuildSelectAllNoneContainer(); }

            base.CreateChildControls();
        }
        #endregion
        #endregion
    }
}
