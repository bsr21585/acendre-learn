﻿using System;
using Asentia.Common;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    public class CustomerManagerAuthenticatedPage : CustomerManagerPage
    {
		#region Properties
        public CustomerManagerAdministratorMenu AdminMenu = new CustomerManagerAdministratorMenu();
        #endregion

        #region InitializeAdminMenu
        /// <summary>
        /// Initializes admnistrator menu for the customer manager website
        /// </summary>
        public void InitializeAdminMenu()
        {
            AdminMenu.Visible = true;

            Panel masterContentMiddleContainer = (Panel)this.Master.FindControl("MasterContentMiddle");

            if (masterContentMiddleContainer != null)
            { masterContentMiddleContainer.CssClass += " MarginOffsetForAdministratorMenu"; }

        }
        #endregion

        #region OnLoad
        /// <summary>
        /// OnLoad event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            // hide the administrator menu by default
            this.AdminMenu.Visible = false;

            // execute the base
            base.OnLoad(e);
        }
        #endregion

        #region OnPreInit
        protected override void OnPreInit(EventArgs e)
        {
            if (AsentiaSessionState.IdAccountUser == 0)
            { Response.Redirect("/Login.aspx"); }

            // execute normal PreInit operations
            base.OnPreInit(e);
        }
        #endregion
    }
}
