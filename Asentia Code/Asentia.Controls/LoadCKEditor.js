﻿function LoadCKEditor() {
    if (CurrentPageLanguage != "en-US" && CurrentPageLanguage != "") {
        CKEDITOR.config.language = CurrentPageLanguage;
    }

    //CKEDITOR.config.uiColor = "#333333";
    //CKEDITOR.config.htmlEncodeOutput = true;
    CKEDITOR.config.skin = "moono";

    CKEDITOR.config.toolbar_Full =
    [
        { name: 'document', items: ['Source', '-', 'NewPage', 'DocProps', 'Preview', 'Print'] },
        { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker'] },
        '/',
        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
        { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
        { name: 'insert', items: ['Image', 'Table', 'HorizontalRule'] },
        '/',
        { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
        { name: 'colors', items: ['TextColor', 'BGColor'] },
        { name: 'tools', items: ['Maximize', 'ShowBlocks'] }
    ];

    CKEDITOR.config.toolbar = "Full";
    CKEDITOR.config.allowedContent = true;
    CKEDITOR.config.extraAllowedContent = "*{*}";    

    // include site layout stylesheets
    if (DefaultLayoutCSSFilePath.length != "")
    { CKEDITOR.config.contentsCss = [ DefaultLayoutCSSFilePath, "/_css/Reset.css" ]; }

    if (SiteSpecificLayoutCSSFilePath != "")
    { CKEDITOR.config.contentsCss = [DefaultLayoutCSSFilePath, SiteSpecificLayoutCSSFilePath, "/_css/Reset.css"]; }
}

function ReloadCKEditorForField(fieldNamePrefix) {    
    for (var instanceName in CKEDITOR.instances) {
        if (instanceName.includes(fieldNamePrefix)) {
            CKEDITOR.replace(instanceName);
        }
    }
}