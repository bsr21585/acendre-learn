﻿using Asentia.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    public class CustomerManagerPage : Page
    {
        #region Constructor
        public CustomerManagerPage()
        {
            // initialize the control adapters to provide overrides for rendering of select .NET objects
            this._InitializeControlAdapters();
        }
        #endregion

        #region Properties
        // content containers
        public Content BreadcrumbContent = new Content();
        public Content PageContent = new Content();

        // panels
        public Panel PageContentContainer = new Panel();
        public Panel PageBreadcrumbContainer = new Panel();
        public Panel PageTitleContainer = new Panel();
        public Panel FullDropDownBreadcrumbContainer = new Panel();
        public Panel PageFeedbackContainer = new Panel();                
        #endregion

        #region Private Properties
        private static string[] AspNetFormElements = new string[] 
        { 
            "__EVENTTARGET",
            "__EVENTARGUMENT",
            "__VIEWSTATE",
            "__EVENTVALIDATION",
            "__VIEWSTATEENCRYPTED",
        };
        #endregion
        
        #region Breadcrumb
        /// <summary>
        /// Class that represents a breadcrumb link item.
        /// </summary>
        protected class BreadcrumbLink
        {
            public string LinkTitle;
            public string LinkHref;

            public BreadcrumbLink(string linkTitle, string linkUrl)
            {
                this.LinkTitle = linkTitle;
                this.LinkHref = linkUrl;
            }

            public BreadcrumbLink(string linkTitle)
            {
                this.LinkTitle = linkTitle;
                this.LinkHref = null;
            }
        }

        /// <summary>
        /// Builds the breadcrumb and attaches it to its container.
        /// </summary>
        /// <param name="crumbs">Breadcrumb items</param>
        public void BuildBreadcrumb(ArrayList crumbs)
        {
            this.PageBreadcrumbContainer.Controls.Clear();
            this.FullDropDownBreadcrumbContainer.Controls.Clear();

            string breadcrumbDelimiterString = "//";
            bool isFirstCrumb = true;
            int i;

            if (crumbs.Count > 2)
            {
                // declare ULs for drop-down breadcrumbs
                HtmlGenericControl breadcrumbOuterUL = new HtmlGenericControl("ul");
                breadcrumbOuterUL.Attributes.Add("class", "BreadcrumbOuterUL");

                HtmlGenericControl breadcrumbInnerUL = new HtmlGenericControl("ul");
                breadcrumbInnerUL.ID = "BreadcrumbDropDownItems";
                breadcrumbInnerUL.Attributes.Add("class", "BreadcrumbInnerUL");

                // build drop-down character
                HtmlGenericControl breadcrumbDownArrowLI = new HtmlGenericControl("li");
                breadcrumbDownArrowLI.ID = "BreadcrumbDownArrowLI";
                breadcrumbDownArrowLI.Attributes.Add("class", "BreadcrumbDownArrowLI");

                Image expandArrowImage = new Image();
                expandArrowImage.CssClass = "XSmallIcon";
                expandArrowImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EXPAND_ARROW, ImageFiles.EXT_PNG, true);
                breadcrumbDownArrowLI.Controls.Add(expandArrowImage);

                Label elipsisString = new Label();
                elipsisString.Text = "...";
                breadcrumbDownArrowLI.Controls.Add(elipsisString);

                // attach drop-down UL/LI controls
                breadcrumbDownArrowLI.Controls.Add(breadcrumbInnerUL);
                breadcrumbOuterUL.Controls.Add(breadcrumbDownArrowLI);

                // build drop-down breadcrumbs
                for (i = 0; i < crumbs.Count - 2; i++)
                {
                    BreadcrumbLink crumb = (BreadcrumbLink)crumbs[i];
                    HtmlGenericControl breadcrumbItemLI = new HtmlGenericControl("li");
                    breadcrumbItemLI.Attributes.Add("class", "BreadcrumbItemLI");

                    if (String.IsNullOrWhiteSpace(crumb.LinkHref))
                    {
                        Localize breadCrumbNoLink = new Localize();
                        breadCrumbNoLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemLI.Controls.Add(breadCrumbNoLink);
                    }
                    else
                    {
                        HyperLink breadCrumbLink = new HyperLink();
                        breadCrumbLink.NavigateUrl = crumb.LinkHref;
                        breadCrumbLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemLI.Controls.Add(breadCrumbLink);
                    }

                    // attach breadcrumb item
                    breadcrumbInnerUL.Controls.Add(breadcrumbItemLI);
                }

                // attach drop-down breadcrumbs to breadcrumb container
                this.PageBreadcrumbContainer.Controls.Add(breadcrumbOuterUL);

                // add delimiter after drop-down
                Panel postDropDownDelimiterContainer = new Panel();
                postDropDownDelimiterContainer.CssClass = "BreadcrumbDelimiter";
                Literal postDropDownDelimiter = new Literal();
                postDropDownDelimiter.Text = breadcrumbDelimiterString;
                postDropDownDelimiterContainer.Controls.Add(postDropDownDelimiter);
                this.PageBreadcrumbContainer.Controls.Add(postDropDownDelimiterContainer);

                // loop through and attach remaining breadcrumb items
                for (i = i; i < crumbs.Count; i++)
                {
                    BreadcrumbLink crumb = (BreadcrumbLink)crumbs[i];

                    // add a delimiter if this is not the first breadcrumb item
                    if (!isFirstCrumb)
                    {
                        Panel breadcrumbDelimiterContainer = new Panel();
                        breadcrumbDelimiterContainer.CssClass = "BreadcrumbDelimiter";
                        Literal breadcrumbDelimiter = new Literal();
                        breadcrumbDelimiter.Text = breadcrumbDelimiterString;
                        breadcrumbDelimiterContainer.Controls.Add(breadcrumbDelimiter);
                        this.PageBreadcrumbContainer.Controls.Add(breadcrumbDelimiterContainer);
                    }

                    // add breadcrumb item
                    Panel breadcrumbItemContainer = new Panel();
                    breadcrumbItemContainer.CssClass = "BreadcrumbItem";

                    if (String.IsNullOrWhiteSpace(crumb.LinkHref))
                    {
                        Localize breadCrumbNoLink = new Localize();
                        breadCrumbNoLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemContainer.Controls.Add(breadCrumbNoLink);
                        isFirstCrumb = false;
                    }
                    else
                    {
                        HyperLink breadCrumbLink = new HyperLink();
                        breadCrumbLink.NavigateUrl = crumb.LinkHref;
                        breadCrumbLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemContainer.Controls.Add(breadCrumbLink);
                        isFirstCrumb = false;
                    }

                    this.PageBreadcrumbContainer.Controls.Add(breadcrumbItemContainer);
                }
            }
            else
            {
                // loop through breadcrumb items, build controls for them,
                // and add them to the breadcrumb container
                foreach (BreadcrumbLink crumb in crumbs)
                {
                    // add a delimiter if this is not the first breadcrumb item
                    if (!isFirstCrumb)
                    {
                        Panel breadcrumbDelimiterContainer = new Panel();
                        breadcrumbDelimiterContainer.CssClass = "BreadcrumbDelimiter";
                        Literal breadcrumbDelimiter = new Literal();
                        breadcrumbDelimiter.Text = breadcrumbDelimiterString;
                        breadcrumbDelimiterContainer.Controls.Add(breadcrumbDelimiter);
                        this.PageBreadcrumbContainer.Controls.Add(breadcrumbDelimiterContainer);
                    }

                    // add breadcrumb item
                    Panel breadcrumbItemContainer = new Panel();
                    breadcrumbItemContainer.CssClass = "BreadcrumbItem";

                    if (String.IsNullOrWhiteSpace(crumb.LinkHref))
                    {
                        Localize breadCrumbNoLink = new Localize();
                        breadCrumbNoLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemContainer.Controls.Add(breadCrumbNoLink);
                        isFirstCrumb = false;
                    }
                    else
                    {
                        HyperLink breadCrumbLink = new HyperLink();
                        breadCrumbLink.NavigateUrl = crumb.LinkHref;
                        breadCrumbLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                        breadcrumbItemContainer.Controls.Add(breadCrumbLink);
                        isFirstCrumb = false;
                    }

                    this.PageBreadcrumbContainer.Controls.Add(breadcrumbItemContainer);
                }
            }

            // make the breadcrumb container visible
            this.PageBreadcrumbContainer.Visible = true;


            // build the breadcrumb "full drop-down" that attaches to the page title container
            // and is made visible only when screen is 480px wide or less

            this.FullDropDownBreadcrumbContainer.CssClass = "FullDropDownBreadcrumbContainer";

            // declare ULs for drop-down breadcrumbs
            HtmlGenericControl fullDropDownBreadcrumbOuterUL = new HtmlGenericControl("ul");
            fullDropDownBreadcrumbOuterUL.Attributes.Add("class", "BreadcrumbOuterUL");

            HtmlGenericControl fullDropDownBreadcrumbInnerUL = new HtmlGenericControl("ul");
            fullDropDownBreadcrumbInnerUL.ID = "FullDropDownBreadcrumbDropDownItems";
            fullDropDownBreadcrumbInnerUL.Attributes.Add("class", "BreadcrumbInnerUL");

            // build drop-down character
            HtmlGenericControl fullDropDownBreadcrumbDownArrowLI = new HtmlGenericControl("li");
            fullDropDownBreadcrumbDownArrowLI.ID = "FullDropDownBreadcrumbDownArrowLI";
            fullDropDownBreadcrumbDownArrowLI.Attributes.Add("class", "BreadcrumbDownArrowLI");

            Image expandArrowImage1 = new Image();
            expandArrowImage1.CssClass = "XSmallIcon";
            expandArrowImage1.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EXPAND_ARROW, ImageFiles.EXT_PNG, true);
            fullDropDownBreadcrumbDownArrowLI.Controls.Add(expandArrowImage1);

            Label elipsisString1 = new Label();
            elipsisString1.Text = "...";
            fullDropDownBreadcrumbDownArrowLI.Controls.Add(elipsisString1);

            // attach drop-down UL/LI controls
            fullDropDownBreadcrumbDownArrowLI.Controls.Add(fullDropDownBreadcrumbInnerUL);
            fullDropDownBreadcrumbOuterUL.Controls.Add(fullDropDownBreadcrumbDownArrowLI);

            // build drop-down breadcrumbs
            for (i = 0; i < crumbs.Count - 1; i++)
            {
                BreadcrumbLink crumb = (BreadcrumbLink)crumbs[i];
                HtmlGenericControl breadcrumbItemLI = new HtmlGenericControl("li");
                breadcrumbItemLI.Attributes.Add("class", "BreadcrumbItemLI");

                if (String.IsNullOrWhiteSpace(crumb.LinkHref))
                {
                    Localize breadCrumbNoLink = new Localize();
                    breadCrumbNoLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                    breadcrumbItemLI.Controls.Add(breadCrumbNoLink);
                }
                else
                {
                    HyperLink breadCrumbLink = new HyperLink();
                    breadCrumbLink.NavigateUrl = crumb.LinkHref;
                    breadCrumbLink.Text = Server.HtmlEncode(crumb.LinkTitle);
                    breadcrumbItemLI.Controls.Add(breadCrumbLink);
                }

                // attach breadcrumb item
                fullDropDownBreadcrumbInnerUL.Controls.Add(breadcrumbItemLI);
            }

            // attach drop-down breadcrumbs to full drop down breadcrumb container and make container visible
            this.FullDropDownBreadcrumbContainer.Controls.Add(fullDropDownBreadcrumbOuterUL);
            this.FullDropDownBreadcrumbContainer.Visible = true;
        }
        #endregion

        #region BuildPageTitle
        /// <summary>
        /// Builds a page title and adds it to its container.
        /// </summary>
        /// <param name="title">title of the page</param>
        /// <param name="titleImagePath">path to image shown next to title</param>
        public void BuildPageTitle(string title, string titleImagePath)
        {
            // css class for page title
            this.PageTitleContainer.CssClass = "xd-12 xm-12";

            // do page title
            this.PageTitleContainer.Controls.Clear();

            HtmlGenericControl titleWrapper = new HtmlGenericControl("p");
            Localize titleText = new Localize();

            if (!String.IsNullOrWhiteSpace(titleImagePath))
            {
                Panel titleIconContainer = new Panel();
                titleIconContainer.ID = "PageTitleIconContainer";
                titleIconContainer.CssClass = "PageTitleIconContainer GeneralCategoryColor";

                Image titleImage = new Image();
                titleImage.ImageUrl = titleImagePath;
                titleImage.AlternateText = title;
                titleImage.CssClass = "PageTitleIcon";

                titleIconContainer.Controls.Add(titleImage);

                this.PageTitleContainer.Controls.Add(titleIconContainer);
            }

            titleText.Text = title;
            titleWrapper.Controls.Add(titleText);

            // add title to container and make container visible
            this.PageTitleContainer.Controls.Add(titleWrapper);
            this.PageTitleContainer.Visible = true;
        }
        #endregion

        #region ClearContainer
        /// <summary>
        /// Clears a specified container of classes and controls, and makes the container not visible.
        /// Used mostly for clearing "feedback" containers.
        /// </summary>
        public void ClearContainer(Panel container)
        {
            container.CssClass = "";
            container.Controls.Clear();
            container.Visible = false;
        }
        #endregion

        #region BuildFormField
        /// <summary>
        /// Builds a form field with all appropriate containers and controls.
        /// </summary>
        /// <param name="fieldIdentifier">the field identifier, used as a prefix for IDs of controls created within this method</param>
        /// <param name="fieldLabel">the label for the field</param>
        /// <param name="labelControlId">the control id the label is for, allows the label to be clicked and the input control with specified id to gain focus</param>
        /// <param name="inputControl">the input control</param>
        /// <param name="isRequired">is this field required?</param>
        /// <param name="buildErrorPanel">build an error panel for this field?</param>
        /// <param name="buildLanguageSpecificInputControls">build language specific controls for this field?</param>
        /// <returns>a Panel formatted with all controls that make up a form field</returns>
        public static Panel BuildFormField(string fieldIdentifier, string fieldLabel, string labelControlId, Control inputControl, bool isRequired, bool buildErrorPanel)
        {            
            // form field container
            Panel formFieldContainer = new Panel();
            formFieldContainer.ID = fieldIdentifier + "_Container";
            formFieldContainer.CssClass = "FormFieldContainer";

            // form field input container
            Panel formFieldInputContainer = new Panel();
            formFieldInputContainer.ID = fieldIdentifier + "_InputContainer";
            formFieldInputContainer.CssClass = "FormFieldInputContainer";

            // attach input control
            if (inputControl != null)
            { formFieldInputContainer.Controls.Add(inputControl); }

            if (!String.IsNullOrWhiteSpace(fieldLabel))
            {
                // form field label container
                Panel formFieldLabelContainer = new Panel();
                formFieldLabelContainer.ID = fieldIdentifier + "_LabelContainer";
                formFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                // form field label
                Label formFieldLabel = new Label();
                formFieldLabel.Text = fieldLabel + ": ";
                formFieldLabel.AssociatedControlID = labelControlId;

                // attach the form field label
                formFieldLabelContainer.Controls.Add(formFieldLabel);

                // if this field is required, build and attach a required asterisk
                if (isRequired)
                {
                    Label formFieldRequiredAsterisk = new Label();
                    formFieldRequiredAsterisk.Text = " * ";
                    formFieldRequiredAsterisk.CssClass = "RequiredAsterisk";
                    formFieldLabelContainer.Controls.Add(formFieldRequiredAsterisk);
                }

                // add label to form field container
                formFieldContainer.Controls.Add(formFieldLabelContainer);
            }

            // if we are to build the error panel, build and attach it
            if (buildErrorPanel)
            {
                Panel formFieldErrorContainer = new Panel();
                formFieldErrorContainer.ID = fieldIdentifier + "_ErrorContainer";
                formFieldErrorContainer.CssClass = "FormFieldErrorContainer";
                formFieldContainer.Controls.Add(formFieldErrorContainer);
            }

            // attach the input container - this is done last
            formFieldContainer.Controls.Add(formFieldInputContainer);

            // return form field
            return formFieldContainer;
        }
        #endregion

        #region BuildMultipleInputControlFormField
        /// <summary>
        /// Builds a form field with all appropriate containers and controls that uses multiple input controls.
        /// 
        /// Note that the List of "inputControls" are added to the field's "InputContainer" are done so in order
        /// and can be actual input controls (TextBox, CheckBox, etc.) and/or "static" controls (Label, Localize, etc.).
        /// This is so that we can easily build inputs that render like "[   ] hour and [   ] minutes".
        /// 
        /// This is not used for language-specific fields. Only "BuildFormField" does that.
        /// </summary>
        /// <param name="fieldIdentifier">the field identifier, used as a prefix for IDs of controls created within this method</param>
        /// <param name="fieldLabel">the label for the field</param>
        /// <param name="inputControls">a list of "input" control to be placed in the input container</param>
        /// <param name="isRequired">is this field required?</param>
        /// <param name="buildErrorPanel">build an error panel for this field?</param>
        /// <returns>a Panel formatted with all controls that make up a form field with multiple input controls</returns>
        public static Panel BuildMultipleInputControlFormField(string fieldIdentifier, string fieldLabel, List<Control> inputControls, bool isRequired, bool buildErrorPanel, bool separateControlsWithPanels = false)
        {
            // form field container
            Panel formFieldContainer = new Panel();
            formFieldContainer.ID = fieldIdentifier + "_Container";
            formFieldContainer.CssClass = "FormFieldContainer";

            // form field input container
            Panel formFieldInputContainer = new Panel();
            formFieldInputContainer.ID = fieldIdentifier + "_InputContainer";
            formFieldInputContainer.CssClass = "FormFieldInputContainer";

            // attach input controls
            if (inputControls != null && inputControls.Count > 0)
            {
                // attach all controls in the control list in order
                foreach (Control control in inputControls)
                {
                    if (separateControlsWithPanels)
                    {
                        Panel separatorPanel = new Panel();
                        separatorPanel.ID = control.ID + "_Container";
                        separatorPanel.Controls.Add(control);

                        formFieldInputContainer.Controls.Add(separatorPanel);
                    }
                    else
                    { formFieldInputContainer.Controls.Add(control); }
                }
            }

            if (!String.IsNullOrWhiteSpace(fieldLabel))
            {
                // form field label container
                Panel formFieldLabelContainer = new Panel();
                formFieldLabelContainer.ID = fieldIdentifier + "_LabelContainer";
                formFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                // form field label
                Label formFieldLabel = new Label();
                formFieldLabel.Text = fieldLabel + ": ";

                // attach the form field label
                formFieldLabelContainer.Controls.Add(formFieldLabel);

                // if this field is required, build and attach a required asterisk
                if (isRequired)
                {
                    Label formFieldRequiredAsterisk = new Label();
                    formFieldRequiredAsterisk.Text = " * ";
                    formFieldRequiredAsterisk.CssClass = "RequiredAsterisk";
                    formFieldLabelContainer.Controls.Add(formFieldRequiredAsterisk);
                }

                // add label to form field container
                formFieldContainer.Controls.Add(formFieldLabelContainer);
            }

            // if we are to build the error panel, build and attach it
            if (buildErrorPanel)
            {
                Panel formFieldErrorContainer = new Panel();
                formFieldErrorContainer.ID = fieldIdentifier + "_ErrorContainer";
                formFieldErrorContainer.CssClass = "FormFieldErrorContainer";
                formFieldContainer.Controls.Add(formFieldErrorContainer);
            }

            // attach the input container - this is done last
            formFieldContainer.Controls.Add(formFieldInputContainer);

            // return form field
            return formFieldContainer;
        }
        #endregion

        #region BuildMultipleInputControlFormField
        /// <summary>
        /// Builds a form field with all appropriate containers and controls that uses multiple input controls.
        /// 
        /// Note that the List of "inputControls" are added to the field's "InputContainer" are done so in order
        /// and can be actual input controls (TextBox, CheckBox, etc.) and/or "static" controls (Label, Localize, etc.).
        /// This is so that we can easily build inputs that render like "[   ] hour and [   ] minutes".
        /// 
        /// This is not used for language-specific fields. Only "BuildFormField" does that.
        /// </summary>
        /// <param name="fieldIdentifier">the field identifier, used as a prefix for IDs of controls created within this method</param>
        /// <param name="fieldLabelControl">a control to be used as the field label, i.e. placed inside of field label container</param>
        /// <param name="inputControls">a list of "input" control to be placed in the input container</param>
        /// <param name="isRequired">is this field required?</param>
        /// <param name="buildErrorPanel">build an error panel for this field?</param>
        /// <returns>a Panel formatted with all controls that make up a form field with multiple input controls</returns>
        public static Panel BuildMultipleInputControlFormField(string fieldIdentifier, Control fieldLabelControl, List<Control> inputControls, bool isRequired, bool buildErrorPanel, bool separateControlsWithPanels = false)
        {
            // form field container
            Panel formFieldContainer = new Panel();
            formFieldContainer.ID = fieldIdentifier + "_Container";
            formFieldContainer.CssClass = "FormFieldContainer";

            // form field input container
            Panel formFieldInputContainer = new Panel();
            formFieldInputContainer.ID = fieldIdentifier + "_InputContainer";
            formFieldInputContainer.CssClass = "FormFieldInputContainer";

            // attach input controls
            if (inputControls != null && inputControls.Count > 0)
            {
                // attach all controls in the control list in order
                foreach (Control control in inputControls)
                {
                    if (separateControlsWithPanels)
                    {
                        Panel separatorPanel = new Panel();
                        separatorPanel.ID = control.ID + "_Container";
                        separatorPanel.Controls.Add(control);

                        formFieldInputContainer.Controls.Add(separatorPanel);
                    }
                    else
                    { formFieldInputContainer.Controls.Add(control); }
                }
            }

            if (fieldLabelControl != null)
            {
                // form field label container
                Panel formFieldLabelContainer = new Panel();
                formFieldLabelContainer.ID = fieldIdentifier + "_LabelContainer";
                formFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                // attach the form field label control
                formFieldLabelContainer.Controls.Add(fieldLabelControl);

                // if this field is required, build and attach a required asterisk
                if (isRequired)
                {
                    Label formFieldRequiredAsterisk = new Label();
                    formFieldRequiredAsterisk.Text = " * ";
                    formFieldRequiredAsterisk.CssClass = "RequiredAsterisk";
                    formFieldLabelContainer.Controls.Add(formFieldRequiredAsterisk);
                }

                // add label to form field container
                formFieldContainer.Controls.Add(formFieldLabelContainer);
            }

            // if we are to build the error panel, build and attach it
            if (buildErrorPanel)
            {
                Panel formFieldErrorContainer = new Panel();
                formFieldErrorContainer.ID = fieldIdentifier + "_ErrorContainer";
                formFieldErrorContainer.CssClass = "FormFieldErrorContainer";
                formFieldContainer.Controls.Add(formFieldErrorContainer);
            }

            // attach the input container - this is done last
            formFieldContainer.Controls.Add(formFieldInputContainer);

            // return form field
            return formFieldContainer;
        }
        #endregion

        #region GetAvailableInstalledLanguages
        /// <summary>
        /// Gets all of the languages that are installed in the application and available to site(s), puts them in an  Dictionary<string, string>, and returns it. 
        /// </summary>
        /// <returns> Dictionary<string, string></returns>
        public Dictionary<string, string> GetAvailableInstalledLanguages()
        {
            Dictionary<string, string> _availableLanguageList = new Dictionary<string, string>();

            //// en-US is the default language, so it's already installed, add it
            _availableLanguageList.Add("en-US", GetLanguageNameByCode("en-US"));

            // loop through each installed language based on the language folders contained in bin
            foreach (string directory in this._GetInstalledLanguages())
            {
                // loop through each language available to the site and add it to the array list for drop-down items
                foreach (KeyValuePair<string, string> language in this._GetAvailableLanguages())
                {
                    if (language.Key == Path.GetFileName(directory))
                    {
                        _availableLanguageList.Add(language.Key, language.Value);
                        break;
                    }
                }
            }

            return _availableLanguageList;
        }
        #endregion

        #region GetLanguageNameByCode
        /// <summary>
        /// Gets the language name according to language code from available languages in database
        /// </summary>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        public string GetLanguageNameByCode(string languageCode)
        {
            string languageName = string.Empty;

            if (this._GetAvailableLanguages().Count > 0)
            {
                Dictionary<string, string> availableLanguages = this._GetAvailableLanguages();

                if (availableLanguages.ContainsKey(languageCode))
                {
                    languageName = availableLanguages[languageCode];
                }
            }

            return languageName;
        }
        #endregion

        #region DisplayFeedback
        /// <summary>
        /// Method to populate the PageFeedbackContainer container with a success, or error message.
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="isError">determines if this message is an error message</param>
        public void DisplayFeedback(string message, bool isError, bool addPopupCloseButton = true)
        {
            // clear the container
            this.PageFeedbackContainer.Controls.Clear();

            // build and attach info status panel
            InformationStatusPanel infoStatusPanel;

            if (isError)
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, InformationStatusPanel_MessageType.Error, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);
                infoStatusPanel.LoadImagesForCustomerManager = true;
            }
            else
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, InformationStatusPanel_MessageType.Success, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);
                infoStatusPanel.LoadImagesForCustomerManager = true;
            }

            this.PageFeedbackContainer.Controls.Add(infoStatusPanel);

            // make container visible
            this.PageFeedbackContainer.Visible = true;
        }

        /// <summary>
        /// Overloaded method to populate the PageFeedbackContainer container with a defined message type.
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="messageType">message type</param>
        public void DisplayFeedback(string message, InformationStatusPanel_MessageType messageType, bool addPopupCloseButton = true)
        {
            // clear the container
            this.PageFeedbackContainer.Controls.Clear();

            // build and attach info status panel
            InformationStatusPanel infoStatusPanel;

            infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, messageType, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);
            infoStatusPanel.LoadImagesForCustomerManager = true;

            this.PageFeedbackContainer.Controls.Add(infoStatusPanel);

            // make container visible
            this.PageFeedbackContainer.Visible = true;
        }
        #endregion

        #region DisplayFeedbackInSpecifiedContainer
        /// <summary>
        /// Method to populate a specified container with a success, or error message.
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="isError">determines if this message is an error message</param>
        public void DisplayFeedbackInSpecifiedContainer(Panel container, string message, bool isError, bool addPopupCloseButton = true)
        {
            // clear the container
            container.Controls.Clear();

            // build and attach info status panel
            InformationStatusPanel infoStatusPanel;

            if (isError)
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, InformationStatusPanel_MessageType.Error, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);
                infoStatusPanel.LoadImagesForCustomerManager = true;
            }
            else
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.PopupWithBlackout, InformationStatusPanel_MessageType.Success, message.Replace(Environment.NewLine, "<br />"), addPopupCloseButton);
                infoStatusPanel.LoadImagesForCustomerManager = true;
            }

            container.Controls.Add(infoStatusPanel);

            // make container visible
            container.Visible = true;
        }
        #endregion

        #region FormatPageInformationPanel
        /// <summary>
        /// Takes a panel and a message, and formats the panel as a "PageInformation" panel with an image and the message.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatPageInformationPanel(Panel informationPanel, string message, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "PageInformationPanel PageInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "PageInformationPanel"; }

            // attach information image - only if there is not a border as the styling, because that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach message to panel
            HtmlGenericControl messageP = new HtmlGenericControl("p");
            messageP.InnerText = message;
            informationPanel.Controls.Add(messageP);
        }
        #endregion

        #region FormatPageInformationPanel
        /// <summary>
        /// Takes a panel and a List of messages, and formats the panel as a "PageInformation"
        /// panel with an image and the messages.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatPageInformationPanel(Panel informationPanel, List<string> messages, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "PageInformationPanel PageInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "PageInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach messages to panel
            foreach (string message in messages)
            {
                HtmlGenericControl messageP = new HtmlGenericControl("p");
                messageP.InnerText = message;
                informationPanel.Controls.Add(messageP);
            }
        }
        #endregion

        #region FormatSectionInformationPanel
        /// <summary>
        /// Takes a panel and a message, and formats the panel as a "SectionInformation"
        /// panel with an image and the message.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatSectionInformationPanel(Panel informationPanel, string message, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "SectionInformationPanel SectionInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "SectionInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach message to panel
            HtmlGenericControl messageP = new HtmlGenericControl("p");
            messageP.InnerText = message;
            informationPanel.Controls.Add(messageP);
        }
        #endregion

        #region FormatSectionInformationPanel
        /// <summary>
        /// Takes a panel and a List of messages, and formats the panel as a "SectionInformation"
        /// panel with an image and the messages.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatSectionInformationPanel(Panel informationPanel, List<string> messages, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "SectionInformationPanel SectionInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "SectionInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach messages to panel
            foreach (string message in messages)
            {
                HtmlGenericControl messageP = new HtmlGenericControl("p");
                messageP.InnerText = message;
                informationPanel.Controls.Add(messageP);
            }
        }
        #endregion

        #region FormatFormInformationPanel
        /// <summary>
        /// Takes a panel and a message, and formats the panel as a "FormInformation"
        /// panel with an image and the message.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatFormInformationPanel(Panel informationPanel, string message, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "FormInformationPanel FormInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "FormInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach message to panel
            HtmlGenericControl messageP = new HtmlGenericControl("p");
            messageP.InnerText = message;
            informationPanel.Controls.Add(messageP);
        }
        #endregion

        #region FormatFormInformationPanel
        /// <summary>
        /// Takes a panel and a List of messages, and formats the panel as a "FormInformation"
        /// panel with an image and the messages.
        /// </summary>
        /// <param name="informationPanel">panel to be formatted</param>
        /// <param name="message">message to be attached to the panel</param>
        /// <param name="withBorder">format panel with border?, optional</param>
        /// <param name="informationImagePath">path to an image that overrides the default, optional</param>
        public void FormatFormInformationPanel(Panel informationPanel, List<string> messages, bool withBorder = false, string informationImagePath = null)
        {
            informationPanel.Controls.Clear();

            // apply class to panel
            if (withBorder)
            { informationPanel.CssClass = "FormInformationPanel FormInformationPanelBorder"; }
            else
            { informationPanel.CssClass = "FormInformationPanel"; }

            // attach information image - only if there is not a border as the styling for that has an image embedded
            if (!withBorder)
            {
                Image informationImage = new Image();
                informationImage.CssClass = "SmallIcon";

                if (informationImagePath != null)
                { informationImage.ImageUrl = informationImagePath; }
                else
                {
                    informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE, ImageFiles.EXT_PNG);
                }

                // attach controls to panel
                informationPanel.Controls.Add(informationImage);
            }

            // attach messages to panel
            foreach (string message in messages)
            {
                HtmlGenericControl messageP = new HtmlGenericControl("p");
                messageP.InnerText = message;
                informationPanel.Controls.Add(messageP);
            }
        }
        #endregion

        #region BuildTabListPanel
        /// <summary>
        /// Builds a panel consisiting of an unordered list consisting of list items for tabs. The panel can either be 
        /// built and returned, or an existing panel can be passed for the list to be rendered into. 
        /// </summary>
        /// <param name="id">the id prefix of all controls this method builds</param>
        /// <param name="listItems">a queue consisting of list items used for tabs, we use a queue because it's FIFO</param>
        /// <param name="targetPanel">optional, the panel to render the list into</param>
        /// <returns>Panel or null</returns>
        public static Panel BuildTabListPanel(string id, Queue<KeyValuePair<string, string>> listItems, Panel targetPanel = null)
        {
            // container panel
            Panel tabsPanel = new Panel();

            if (targetPanel != null)
            { tabsPanel = targetPanel; }
            else
            { tabsPanel.ID = id + "_TabsPanel"; }

            // UL for tabs
            HtmlGenericControl tabsUL = new HtmlGenericControl("ul");
            tabsUL.ID = id + "_TabsUL";
            tabsUL.Attributes.Add("class", "TabbedList");
            tabsPanel.Controls.Add(tabsUL);

            bool isFirstElement = true;

            // loop through the queue and pick off the items
            while (listItems.Count > 0)
            {
                // grab the item from the queue
                KeyValuePair<string, string> kvp = listItems.Dequeue();

                // LI
                HtmlGenericControl tabLI = new HtmlGenericControl("li");
                tabLI.ID = id + "_" + kvp.Key + "_TabLI";
                tabLI.Attributes.Add("onclick", "Helper.ToggleTab('" + kvp.Key + "', '" + id + "');");

                if (isFirstElement)
                { tabLI.Attributes.Add("class", "TabbedListLI TabbedListLIOn"); }
                else
                { tabLI.Attributes.Add("class", "TabbedListLI"); }

                tabsUL.Controls.Add(tabLI);

                // INNER DIV
                Panel tabDiv = new Panel();
                tabLI.Controls.Add(tabDiv);

                Literal tabDivText = new Literal();
                tabDivText.Text = kvp.Value;
                tabDiv.Controls.Add(tabDivText);

                // set first element flag to false
                isFirstElement = false;
            }

            // return
            // if there is a target panel, we return null because the panel is already in the control stack
            if (targetPanel != null)
            { return null; }
            else
            { return tabsPanel; }
        }
        #endregion

        #region ApplyErrorMessageToFieldErrorPanel
        /// <summary>
        /// Adds an error message to the error panel for an input field.
        /// </summary>
        /// <param name="fieldIdentifier">the field identifier</param>
        /// <param name="errorMessage">the error message to be displayed</param>
        public void ApplyErrorMessageToFieldErrorPanel(Panel container, string fieldIdentifier, string errorMessage)
        {
            // get the container and error panels
            Panel containerPanel = (Panel)FindControlRecursive(container, fieldIdentifier + "_Container");
            Panel errorPanel = (Panel)FindControlRecursive(container, fieldIdentifier + "_ErrorContainer");

            // if we found the container and error panels, apply the class 
            // to the container panel and add the message to the error panel
            if (containerPanel != null && errorPanel != null)
            {
                // container class
                if (!containerPanel.CssClass.Contains("ErrorOn"))
                { containerPanel.CssClass += " ErrorOn"; }

                // error
                HtmlGenericControl errorP = new HtmlGenericControl("p");
                errorP.InnerHtml = errorMessage;
                errorPanel.Controls.Add(errorP);
            }
        }
        #endregion

        #region ClearErrorClassFromFieldsRecursive
        /// <summary>
        /// Recursively finds all controls from a parent and clears the ErrorOn class.
        /// The controls that would have ErrorOn class applied are burried within other controls.
        /// Therefore we need to go through them recursively.
        /// </summary>
        /// <param name="root">the root control to start in</param>
        /// <returns></returns>
        public static void ClearErrorClassFromFieldsRecursive(Control root)
        {
            if (root.GetType() == typeof(Panel))
            {
                Panel rootPanel = (Panel)root;

                if (rootPanel == null)
                { return; }

                if (rootPanel.CssClass.Contains("ErrorOn"))
                { rootPanel.CssClass = rootPanel.CssClass.Replace("ErrorOn", ""); }
            }

            foreach (Control c in root.Controls)
            { ClearErrorClassFromFieldsRecursive(c); }
        }
        #endregion

        #region ApplyErrorImageAndClassToTab
        /// <summary>
        /// Adds an error image and error class to a tab.
        /// </summary>
        /// <param name="tabIdentifier">the tab identifier</param>
        public void ApplyErrorImageAndClassToTab(Panel container, string tabIdentifier)
        {
            // find the tab li element and set the error css
            HtmlGenericControl tabLI = (HtmlGenericControl)container.FindControl(tabIdentifier);

            if (tabLI != null)
            {
                // instansiate a string builder
                Image errorImage = new Image();
                errorImage.CssClass = "SmallIcon";

                errorImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_WARNING_YELLOW, ImageFiles.EXT_PNG, true);
                tabLI.Controls.Add(errorImage);

                // instansiate a string builder
                StringBuilder sb = new StringBuilder();

                // beginning of function
                sb.AppendLine("$(\"#" + tabLI.ID + "\").addClass(\"TabbedListLIError\");");
                sb.AppendLine("$(\".TabbedListLIError img\").show();");

                // register a script to add the error class to the tab
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Toggle" + tabLI.ID, sb.ToString(), true);
            }
        }
        #endregion

        #region BuildOptionsPanelImageLink
        /// <summary>
        /// Builds a Panel containing a HyperLink with an Image and overlay Image (if specified).
        /// </summary>
        /// <param name="id">identifier</param>
        /// <param name="linkButton">LinkButton control to use for this link, if specified</param>
        /// <param name="href">link url</param>
        /// <param name="onclick">onclick action</param>
        /// <param name="linkText">link text</param>
        /// <param name="hrefTarget">target</param>
        /// <param name="imagePath">path to link image</param>
        /// <param name="overlayPath">path to link image overlay</param>
        /// <returns>Panel</returns>
        public Panel BuildOptionsPanelImageLink(string id, LinkButton linkButton, string href, string onclick, string linkText, string hrefTarget, string imagePath, string overlayPath = null)
        {
            // container panel
            Panel optionsPanelLinkContainer = new Panel();
            optionsPanelLinkContainer.ID = id;
            optionsPanelLinkContainer.CssClass = "OptionsPanelLinkContainer";

            // image
            Panel optionsPanelLinkIconContainer = new Panel();
            optionsPanelLinkIconContainer.CssClass = "OptionsPanelLinkIconContainer";

            Image mainIcon = new Image();
            mainIcon.CssClass = "OptionsPanelLinkMainIcon"; // MediumIcon
            mainIcon.ImageUrl = imagePath;
            mainIcon.AlternateText = linkText;
            optionsPanelLinkIconContainer.Controls.Add(mainIcon);

            // overlay
            Image overlayIcon = new Image();
            overlayIcon.CssClass = "OptionsPanelLinkOverlayIcon"; // XSmallIcon

            if (!String.IsNullOrWhiteSpace(overlayPath))
            { overlayIcon.ImageUrl = overlayPath; }
            else
            { overlayIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_BLANK, ImageFiles.EXT_PNG, true); }

            optionsPanelLinkIconContainer.Controls.Add(overlayIcon);

            // label
            Panel optionsPanelLinkLabelContainer = new Panel();
            optionsPanelLinkLabelContainer.CssClass = "OptionsPanelLinkLabelContainer";

            Literal optionsPanelLinkLabel = new Literal();
            optionsPanelLinkLabel.Text = linkText;

            optionsPanelLinkLabelContainer.Controls.Add(optionsPanelLinkLabel);

            // if linkButton is not null, use that, otherwise create and use a HyperLink
            if (linkButton != null)
            {
                linkButton.CssClass = "OptionsPanelLink";

                if (!String.IsNullOrWhiteSpace(onclick))
                { linkButton.OnClientClick = onclick; }

                // attach controls to containers
                linkButton.Controls.Add(optionsPanelLinkIconContainer);
                linkButton.Controls.Add(optionsPanelLinkLabelContainer);
                optionsPanelLinkContainer.Controls.Add(linkButton);
            }
            else
            {
                // hyperlink
                HyperLink optionsPanelLink = new HyperLink();
                optionsPanelLink.ID = id + "_Link";
                optionsPanelLink.CssClass = "OptionsPanelLink";

                if (!String.IsNullOrWhiteSpace(href))
                { optionsPanelLink.NavigateUrl = href; }

                if (!String.IsNullOrWhiteSpace(hrefTarget))
                { optionsPanelLink.Target = hrefTarget; }

                if (!String.IsNullOrWhiteSpace(onclick))
                { optionsPanelLink.Attributes.Add("onclick", onclick); }

                // attach controls to containers
                optionsPanelLink.Controls.Add(optionsPanelLinkIconContainer);
                optionsPanelLink.Controls.Add(optionsPanelLinkLabelContainer);
                optionsPanelLinkContainer.Controls.Add(optionsPanelLink);
            }

            return optionsPanelLinkContainer;
        }
        #endregion

        #region QueryStringBool
        public bool QueryStringBool(string queryString, bool defaultValue)
        {
            bool returnValue = QueryStringBool(queryString);

            if (!returnValue)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public bool QueryStringBool(string queryString)
        {
            if (Request.QueryString[queryString] == null)
                return false;
            else
            {
                try
                {
                    return Convert.ToBoolean(Request.QueryString[queryString]);
                }
                catch
                {
                    return false;
                }
            }
        }
        #endregion

        #region QueryStringInt
        public int QueryStringInt(string queryString, int defaultValue)
        {
            int returnValue = QueryStringInt(queryString);

            if (returnValue == 0)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public int QueryStringInt(string queryString)
        {
            if (Request.QueryString[queryString] == null)
                return 0;
            else
            {
                try
                {
                    return Convert.ToInt32(Request.QueryString[queryString]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region QueryStringString
        public string QueryStringString(string queryString, string defaultValue)
        {
            string returnValue = QueryStringString(queryString);

            if (returnValue == null)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public string QueryStringString(string queryString)
        {
            if (Request.QueryString[queryString] == null)
                return null;
            else
            {
                try
                {
                    return Request.QueryString[queryString].ToString();
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        #region QueryStringDate
        public DateTime QueryStringDate(string queryString, DateTime defaultValue)
        {
            DateTime returnValue = QueryStringDate(queryString);

            if (returnValue == DateTime.MinValue)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public DateTime QueryStringDate(string queryString)
        {
            if (Request.QueryString[queryString] == null)
                return DateTime.MinValue;
            else
            {
                try
                {
                    return DateTime.Parse(Request.QueryString[queryString].ToString());
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
        }
        #endregion

        #region ViewStateBool
        public bool ViewStateBool(StateBag viewState, string key, bool defaultValue)
        {
            bool returnValue = ViewStateBool(viewState, key);

            if (!returnValue)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public bool ViewStateBool(StateBag viewState, string key)
        {
            if (viewState[key] == null)
                return false;
            else
            {
                try
                {
                    return Convert.ToBoolean(viewState[key]);
                }
                catch
                {
                    return false;
                }
            }
        }
        #endregion

        #region ViewStateInt
        public int ViewStateInt(StateBag viewState, string key, int defaultValue)
        {
            int returnValue = ViewStateInt(viewState, key);

            if (returnValue == 0)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public int ViewStateInt(StateBag viewState, string key)
        {
            if (viewState[key] == null)
                return 0;
            else
            {
                try
                {
                    return Convert.ToInt32(viewState[key]);
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region ViewStateString
        public string ViewStateString(StateBag viewState, string key, string defaultValue)
        {
            string returnValue = ViewStateString(viewState, key);

            if (returnValue == null)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public string ViewStateString(StateBag viewState, string key)
        {
            if (viewState[key] == null)
                return null;
            else
            {
                try
                {
                    return viewState[key].ToString();
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        #region ViewStateDate
        public DateTime ViewStateDate(StateBag viewState, string key, DateTime defaultValue)
        {
            DateTime returnValue = ViewStateDate(viewState, key);

            if (returnValue == DateTime.MinValue)
            { return defaultValue; }
            else
            { return returnValue; }
        }

        public DateTime ViewStateDate(StateBag viewState, string key)
        {
            if (viewState[key] == null)
                return DateTime.MinValue;
            else
            {
                try
                {
                    return DateTime.Parse(viewState[key].ToString());
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
        }
        #endregion

        #region FindControlRecursive
        /// <summary>
        /// Recursively finds a control with a specified ID.
        /// This is used when a control we need to find is buried within other controls, and is
        /// necessary because the standard FindControl only looks at direct children (not recursive).
        /// </summary>
        /// <param name="root">the root control to start in</param>
        /// <param name="id">the id to look for</param>
        /// <returns></returns>
        public Control FindControlRecursive(Control root, string id)
        {
            if (root.ID == id)
            { return root; }

            foreach (Control c in root.Controls)
            {
                Control t = FindControlRecursive(c, id);

                if (t != null)
                { return t; }
            }

            return null;
        }
        #endregion

        #region Private Methods
        #region _InitializeControlAdapters
        /// <summary>
        /// Initializes control adapters that are used so that we can control how certain .NET controls are rendered.
        /// </summary>
        private void _InitializeControlAdapters()
        {
            //// dictionary object for control adapters
            IDictionary adapters = Context.Request.Browser.Adapters;

            //TODO: Fix CheckboxListAdapter postback issue.
            //// CheckBoxList Adapter
            //string checkBoxListTypeName = typeof(CheckBoxList).AssemblyQualifiedName;
            //string checkBoxListAdapterTypeName = typeof(CheckBoxListAdapter).AssemblyQualifiedName;
            //if (!adapters.Contains(checkBoxListTypeName))
            //{ adapters.Add(checkBoxListTypeName, checkBoxListAdapterTypeName); }

            // RadioButtonList Adapter
            string radioButtonListTypeName = typeof(RadioButtonList).AssemblyQualifiedName;
            string radioButtonListAdapterTypeName = typeof(RadioButtonListAdapter).AssemblyQualifiedName;
            if (!adapters.Contains(radioButtonListTypeName))
            { adapters.Add(radioButtonListTypeName, radioButtonListAdapterTypeName); }

            // CheckBox Adapter
            string checkBoxTypeName = typeof(CheckBox).AssemblyQualifiedName;
            string checkBoxAdapterTypeName = typeof(CheckBoxAdapter).AssemblyQualifiedName;
            if (!adapters.Contains(checkBoxTypeName))
            { adapters.Add(checkBoxTypeName, checkBoxAdapterTypeName); }

            // RadioButton Adapter
            string radioButtonTypeName = typeof(RadioButton).AssemblyQualifiedName;
            string radioButtonAdapterTypeName = typeof(RadioButtonAdapter).AssemblyQualifiedName;
            if (!adapters.Contains(radioButtonTypeName))
            { adapters.Add(radioButtonTypeName, radioButtonAdapterTypeName); }
        }
        #endregion

        #region _GetInstalledLanguages
        /// <summary>
        /// Get List of all installed languages
        /// </summary>
        /// <returns>string[]</returns>
        private string[] _GetInstalledLanguages()
        {
            // return all installed languages
            return Directory.GetDirectories(MapPathSecure(SitePathConstants.BIN));
        }
        #endregion

        #region _GetAvailableLanguages
        /// <summary>
        /// Gets the languages that are available.
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> _GetAvailableLanguages()
        {
            Dictionary<string, string> availableLanguages = new Dictionary<string, string>();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", null, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", String.Empty, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", null, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sqlDataReader = databaseObject.ExecuteDataReader("[Language.GetLanguages]", true);

                // loop through the returned recordset, add each language string to the AvailableLanguages ArrayList
                while (sqlDataReader.Read())
                {
                    availableLanguages.Add(Convert.ToString(sqlDataReader["langString"]), Convert.ToString(sqlDataReader["name"]));
                }

                sqlDataReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return availableLanguages;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreInit
        protected override void OnPreInit(EventArgs e)
        {
            // load the master page
            string masterPageFileName = "CustomerManager.Master";
            string masterPagePath = SitePathConstants.CM_MASTERPAGE + masterPageFileName;
            base.MasterPageFile = masterPagePath;

            // get the current culture from the session
            string culture = AsentiaSessionState.UserCulture;

            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);

            // execute normal PreInit operations
            base.OnPreInit(e);
        }
        #endregion

        #region OnPreRender
        protected override void OnPreRender(EventArgs e)
        {
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Common.ClientScript), "Asentia.Common.Helper.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery-1.10.2.min.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery-ui.js");
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.CustomerManagerPage.js");

            // add global application javascript start up events
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" $(window).load(function() {");            
            sb.AppendLine("     // add page category class and language to body tag");
            sb.AppendLine("     $(\"body\").addClass(\"PageCategory_General\");");
            sb.AppendLine("     $(\"body\").prop(\"lang\", \"" + AsentiaSessionState.UserCulture + "\");");
            sb.AppendLine("");
            sb.AppendLine("     // add language to html tag");
            sb.AppendLine("     $(\"html\").prop(\"lang\", \"" + AsentiaSessionState.UserCulture + "\");");
            sb.AppendLine(" });");

            csm.RegisterStartupScript(typeof(AsentiaPage), "GlobalApplicationStartUpScript", sb.ToString(), true);

            base.OnPreRender(e);
        }
        #endregion

        #region OnLoad
        protected override void OnLoad(EventArgs e)
        {
            // set the visibility of breadcrumb, page title, and page feedback
            // containers to false, the individual page will show them if needed
            this.PageBreadcrumbContainer.Visible = false;
            this.PageTitleContainer.Visible = false;
            this.PageFeedbackContainer.Visible = false;

            // execute the base
            base.OnLoad(e);
        }
        #endregion

        #region Render
        protected override void Render(HtmlTextWriter writer)
        {
            // register our custom postback helper to fix issues with how .net ajax handles static ids
            // this script needs to be registered here in the render method so that we know it is being
            // included after all of the javascript resources included by .net ajax
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Common.ClientScript), "Asentia.Common.PostBackHelper.js");

            // declare a string writer and an html writer
            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

            // execute the base, rendering nothing
            base.Render(htmlWriter);

            // go through html to be rendered and relocate the hidden form elements
            // this helps with viewstate issues
            string html = stringWriter.ToString();
            int formStart = html.IndexOf("<form");
            int endForm = -1;

            if (formStart >= 0)
            { endForm = html.IndexOf(">", formStart); }

            if (endForm >= 0)
            {
                StringBuilder viewStateBuilder = new StringBuilder();
                foreach (string element in AspNetFormElements)
                {
                    int startPoint = html.IndexOf("<input type=\"hidden\" name=\"" + element + "\"");

                    if (startPoint >= 0 && startPoint > endForm)
                    {
                        int endPoint = html.IndexOf("/>", startPoint);

                        if (endPoint >= 0)
                        {
                            endPoint += 2;
                            string viewStateInput = html.Substring(startPoint, endPoint - startPoint);
                            html = html.Remove(startPoint, endPoint - startPoint);
                            viewStateBuilder.Append(viewStateInput).Append("\r\n");
                        }
                    }
                }

                if (viewStateBuilder.Length > 0)
                {
                    viewStateBuilder.Insert(0, "\r\n");
                    html = html.Insert(endForm + 1, viewStateBuilder.ToString());
                }
            }

            // render html
            writer.Write(html);
        }
        #endregion
        #endregion
    }
}
