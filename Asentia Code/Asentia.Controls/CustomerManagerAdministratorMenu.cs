﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    public class CustomerManagerAdministratorMenu : WebControl
    {
        #region Private Properties
        #region Icon Paths

        // System Configurations
        private string _IconPathSystemConfiguration;
        private string _IconPathSystemConfigurationOn;

        // System Users
        private string _IconPathSystemUsers;
        private string _IconPathSystemUsersOn;

        // Accounts
        private string _IconPathAccounts;
        private string _IconPathAccountsOn;

        // Database Servers
        private string _IconPathDatabaseServers;
        private string _IconPathDatabaseServersOn;

        // Clone Portal
        private string _IconPathClonePortal;
        private string _IconPathClonePortalOn;

        // Inquisiq Migration
        private string _IconPathInquisiqMigration;
        private string _IconPathInquisiqMigrationOn;

        // Exception Log
        private string _IconPathExceptionLog;
        private string _IconPathExceptionLogOn;

        // Account Configuration
        private string _IconPathAccountConfiguration;
        private string _IconPathAccountConfigurationOn;

        // Account Users
        private string _IconPathAccountUsers;
        private string _IconPathAccountUsersOn;

        // Sites
        private string _IconPathSites;
        private string _IconPathSitesOn;

        // Administrator License Agreement
        private string _IconPathAdminLicenseAgreement;
        private string _IconPathAdminLicenseAgreementOn;

        // Account License Agreement
        private string _IconPathAccountLicenseAgreement;
        private string _IconPathAccountLicenseAgreementOn;

        #endregion

        #region Href Destinations

        // System Configurations
        private string _HrefDestinationSystemConfiguration;

        // System Users
        private string _HrefDestinationSystemUsers;

        // Accounts
        private string _HrefDestinationAccounts;

        // Database Servers
        private string _HrefDestinationDatabaseServers;
        
        // Clone Portal 
        private string _HrefDestinationClonePortal;

        // Inquisiq Migration
        private string _HrefDestinationInquisiqMigration;

        // Exception Log
        private string _HrefDestinationExceptionLog;

        // Account Configuration
        private string _HrefDestinationAccountConfiguration;

        // Account Users
        private string _HrefDestinationAccountUsers;

        // Sites
        private string _HrefDestinationSites;

        // Admin License Agreement
        private string _HrefDestinationAdminLicenseAgreement;

        // Account License Agreement
        private string _HrefDestinationAccountLicenseAgreement;

        #endregion

        #region Menu Items Display
        // System Configuration
        private bool _DisplaySystemConfiguration = false;

        // System Users
        private bool _DisplaySystemUsers = false;

        // Accounts
        private bool _DisplayAccounts = false;

        // Database Servers
        private bool _DisplayDatabaseServers = false;

        // Clone Portal
        private bool _DisplayClonePortal = false;

        // Inquisiq to Asentia Migration
        private bool _DisplayInquisiqMigration = false;

        // Exception Log
        private bool _DisplayExceptionLog = false;

        // Account Configuration 
        private bool _DisplayAccountConfiguration = false;

        // Account Users
        private bool _DisplayAccountUsers = false;

        // Sites
        private bool _DisplaySites = false;

        // Admin License Agreement
        private bool _DisplayAdminLicenseAgreement = false;

        // Account License Agreement
        private bool _DisplayAccountLicenseAgreement = false;

        #endregion

        private HtmlGenericControl _JavascriptTag;
        private Panel _AdministratorMenuCategoryIconsContainer = new Panel();

        #endregion

        #region Constructor
        public CustomerManagerAdministratorMenu() : base(HtmlTextWriterTag.Div)
        {; }
        #endregion

        #region Private Methods
        #region _SetIconPaths
        /// <summary>
        /// Sets the paths to the category icons for the menu.
        /// </summary>
        private void _SetIconPaths()
        {
            // Check whether system user or not
            if (AsentiaSessionState.IdAccount == 1)
            {
                // System Configuration
                this._IconPathSystemConfiguration = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM,
                                                            ImageFiles.EXT_PNG, true);

                this._IconPathSystemConfigurationOn = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM,
                                                                   ImageFiles.EXT_PNG, true);

                // System Users
                this._IconPathSystemUsers = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                ImageFiles.EXT_PNG, true);
                this._IconPathSystemUsersOn = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                   ImageFiles.EXT_PNG, true);

                // Accounts
                this._IconPathAccounts = ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY,
                                                                ImageFiles.EXT_PNG, true);
                this._IconPathAccountsOn = ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY,
                                                                   ImageFiles.EXT_PNG, true);

                // DatabaseServers
                this._IconPathDatabaseServers = ImageFiles.GetIconPath(ImageFiles.ICON_SERVER,
                                                                ImageFiles.EXT_PNG, true);
                this._IconPathDatabaseServersOn = ImageFiles.GetIconPath(ImageFiles.ICON_SERVER,
                                                                   ImageFiles.EXT_PNG, true);

                // ClonePortal
                this._IconPathClonePortal = ImageFiles.GetIconPath(ImageFiles.ICON_CLONEPORTAL,
                                                                ImageFiles.EXT_PNG, true);
                this._IconPathClonePortalOn = ImageFiles.GetIconPath(ImageFiles.ICON_CLONEPORTAL,
                                                                   ImageFiles.EXT_PNG, true);

                // InquisiqMigration
                this._IconPathInquisiqMigration = ImageFiles.GetIconPath(ImageFiles.ICON_CLONEPORTAL,
                                                                ImageFiles.EXT_PNG, true);
                this._IconPathInquisiqMigrationOn = ImageFiles.GetIconPath(ImageFiles.ICON_CLONEPORTAL,
                                                                   ImageFiles.EXT_PNG, true);

                // ExceptionLog
                this._IconPathExceptionLog = ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EXCEPTION,
                                                                ImageFiles.EXT_PNG, true);
                this._IconPathExceptionLogOn = ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EXCEPTION,
                                                                   ImageFiles.EXT_PNG, true);

                // Administrator License Agreement
                this._IconPathAdminLicenseAgreement = ImageFiles.GetIconPath(ImageFiles.ICON_LICENSE,
                                                                ImageFiles.EXT_PNG, true);
                this._IconPathAdminLicenseAgreement = ImageFiles.GetIconPath(ImageFiles.ICON_LICENSE,
                                                                ImageFiles.EXT_PNG, true);
            }
            // Account user
            else
            {
                // Account Configuration
                this._IconPathAccountConfiguration = ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY,
                                                            ImageFiles.EXT_PNG, true);

                this._IconPathAccountConfigurationOn = ImageFiles.GetIconPath(ImageFiles.ICON_COMPANY,
                                                                   ImageFiles.EXT_PNG, true);

                // Account Users
                this._IconPathAccountUsers = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                            ImageFiles.EXT_PNG, true);

                this._IconPathAccountUsersOn = ImageFiles.GetIconPath(ImageFiles.ICON_USERM,
                                                                   ImageFiles.EXT_PNG, true);

                // Sites
                this._IconPathSites = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                            ImageFiles.EXT_PNG, true);

                this._IconPathSitesOn = ImageFiles.GetIconPath(ImageFiles.ICON_SITE,
                                                                   ImageFiles.EXT_PNG, true);

                // Exception Log
                this._IconPathExceptionLog = ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EXCEPTION,
                                                            ImageFiles.EXT_PNG, true);

                this._IconPathExceptionLogOn = ImageFiles.GetIconPath(ImageFiles.ICON_LOG_EXCEPTION,
                                                                   ImageFiles.EXT_PNG, true);

                // Account License Agreement
                this._IconPathAccountLicenseAgreement = ImageFiles.GetIconPath(ImageFiles.ICON_LICENSE,
                                                                ImageFiles.EXT_PNG, true);
                this._IconPathAccountLicenseAgreement = ImageFiles.GetIconPath(ImageFiles.ICON_LICENSE,
                                                                ImageFiles.EXT_PNG, true);
            }
        }
        #endregion

        #region _SetHrefDestinations
        /// <summary>
        /// Sets the link destinations for the menu items.
        /// </summary>
        private void _SetHrefDestinations()
        {
            // Check whether system user or not
            if (AsentiaSessionState.IdAccount == 1)
            {
                // System Configuration
                this._HrefDestinationSystemConfiguration = "~/system/";

                // System Users
                this._HrefDestinationSystemUsers = "~/users/";

                // Accounts
                this._HrefDestinationAccounts = "~/accounts/";

                // Database Servers
                this._HrefDestinationDatabaseServers = "~/system/database/";

                // Clone Portal
                this._HrefDestinationClonePortal = "~/ClonePortal/";

                // Inquisiq Migration
                this._HrefDestinationInquisiqMigration = "~/InquisiqMigration/";

                // System Exception Log
                this._HrefDestinationExceptionLog = "~/system/exceptions/";

                // Administrator License Agreement
                this._HrefDestinationAdminLicenseAgreement = "~/system/license/";
            }
            // Account user
            else
            {
                // Account Configuration
                this._HrefDestinationAccountConfiguration = "~/accounts/system/";

                // Account Users
                this._HrefDestinationAccountUsers = "~/accounts/users/";

                // Sites
                this._HrefDestinationSites = "~/accounts/sites/";

                // Account Exception Log
                this._HrefDestinationExceptionLog = "~/accounts/system/exceptions/";

                // Account License Agreement
                this._HrefDestinationAccountLicenseAgreement = "~/accounts/license/";
            }
        }
        #endregion

        #region _SetMenuItemDisplayProperties
        /// <summary>
        /// Sets whether or not menu items will be displayed based on permissions.
        /// </summary>
        private void _SetMenuItemDisplayProperties()
        {
            // Check whether system user or not
            if (AsentiaSessionState.IdAccount == 1)
            {
                // System Configuration
                this._DisplaySystemConfiguration = true;

                // System Users
                this._DisplaySystemUsers = true;

                // Accounts
                this._DisplayAccounts = true;

                // Database Servers
                this._DisplayDatabaseServers = true;

                // Clone Portal
                this._DisplayClonePortal = true;

                // Inquisiq Migration
                this._DisplayInquisiqMigration = true;

                // Exception Log
                this._DisplayExceptionLog = true;

                // Administrator License Agreement
                this._DisplayAdminLicenseAgreement = true;
            }
            // Account user
            else
            {
                // Account Configuration
                this._DisplayAccountConfiguration = true;

                // Account Users
                this._DisplayAccountUsers = true;

                // Sites
                this._DisplaySites = true;

                // Exception Log
                this._DisplayExceptionLog = true;

                // Account License Agreement
                this._DisplayAccountLicenseAgreement = true;
            }
        }
        #endregion

        #region _BuildMenu
        private void _BuildMenu()
        {
            this._AdministratorMenuCategoryIconsContainer.ID = "AdministratorMenuCategoryIconsContainer";

            // Check whether system user or not
            if (AsentiaSessionState.IdAccount == 1)
            {
                if (this._DisplaySystemConfiguration)
                {
                    // build system configuration category icon
                    this._BuildIconForCategoryMenu("SystemConfiguration", this._IconPathSystemConfiguration, this._IconPathSystemConfigurationOn, _GlobalResources.SystemConfiguration, this._HrefDestinationSystemConfiguration);
                }
                if (AsentiaSessionState.IdAccountUser == 1)
                {
                    if (this._DisplaySystemUsers)
                    { // build system user category icon
                        this._BuildIconForCategoryMenu("SystemUsers", this._IconPathSystemUsers, this._IconPathSystemUsersOn, _GlobalResources.SystemUsers, this._HrefDestinationSystemUsers);
                    }
                }
                if (this._DisplayAccounts)
                {
                    // build account category icon
                    this._BuildIconForCategoryMenu("Accounts", this._IconPathAccounts, this._IconPathAccountsOn, _GlobalResources.Accounts, this._HrefDestinationAccounts);
                }

                if (this._DisplayDatabaseServers)
                {
                    // build database server category icon
                    this._BuildIconForCategoryMenu("DatabaseServers", this._IconPathDatabaseServers, this._IconPathDatabaseServersOn, _GlobalResources.DatabaseServers, this._HrefDestinationDatabaseServers);
                }

                if (this._DisplayClonePortal)
                {
                    // build clone portal category icon
                    this._BuildIconForCategoryMenu("ClonePortal", this._IconPathClonePortal, this._IconPathClonePortalOn, _GlobalResourcesCHETU1.ClonePortal, this._HrefDestinationClonePortal);
                }

                if (this._DisplayInquisiqMigration)
                {
                    // build clone portal category icon
                    this._BuildIconForCategoryMenu("InquisiqMigration", this._IconPathInquisiqMigration, this._IconPathInquisiqMigrationOn, _GlobalResourcesCHETU1.InquisiqMigration, this._HrefDestinationInquisiqMigration);
                }

                if (this._DisplayExceptionLog)
                {
                    // build system exception log category icon
                    this._BuildIconForCategoryMenu("ExceptionLog", this._IconPathExceptionLog, this._IconPathExceptionLogOn, _GlobalResources.ExceptionLog, this._HrefDestinationExceptionLog);
                }

                if (this._DisplayAdminLicenseAgreement)
                {
                    // build admin license agreement category icon
                    this._BuildIconForCategoryMenu("LicenseAgreement", this._IconPathAdminLicenseAgreement, this._IconPathAdminLicenseAgreementOn, _GlobalResources.LicenseAgreement, this._HrefDestinationAdminLicenseAgreement);
                }
            }
            // Account user
            else
            {
                if (this._DisplayAccountConfiguration)
                {
                    // build account configuration category icon
                    this._BuildIconForCategoryMenu("AccountConfiguration", this._IconPathAccountConfiguration, this._IconPathAccountConfigurationOn, _GlobalResources.AccountConfiguration, this._HrefDestinationAccountConfiguration);
                }

                if (AsentiaSessionState.IdAccountUser == 1)
                {
                    if (this._DisplayAccountUsers)
                    { // build account user category icon
                        this._BuildIconForCategoryMenu("SystemUsers", this._IconPathAccountUsers, this._IconPathAccountUsersOn, _GlobalResources.AccountUsers, this._HrefDestinationAccountUsers);
                    }
                }
                if (this._DisplaySites)
                {
                    // build site category icon
                    this._BuildIconForCategoryMenu("Sites", this._IconPathSites, this._IconPathSitesOn, _GlobalResources.Sites, this._HrefDestinationSites);
                }

                if (this._DisplayExceptionLog)
                {
                    // build account exception log category icon
                    this._BuildIconForCategoryMenu("ExceptionLog", this._IconPathExceptionLog, this._IconPathExceptionLogOn, _GlobalResources.ExceptionLog, this._HrefDestinationExceptionLog);
                }

                if (this._DisplayAccountLicenseAgreement)
                {
                    // build admin license agreement category icon
                    this._BuildIconForCategoryMenu("LicenseAgreement", this._IconPathAccountLicenseAgreement, this._IconPathAccountLicenseAgreementOn, _GlobalResources.LicenseAgreement, this._HrefDestinationAccountLicenseAgreement);
                }

            }
        }
        #endregion

        #region _BuildIconForCategoryMenu
        private void _BuildIconForCategoryMenu(string categoryIdentifier, string iconPath, string iconOnPath, string iconAltText, string href)
        {
            // build category icon container
            Panel categoryIconContainer = new Panel();
            categoryIconContainer.ID = categoryIdentifier + "_CategoryIconContainer";
            categoryIconContainer.CssClass = "AdministratorMenuCategoryIconContainer";

            // attach category image to container
            Image categoryIcon = new Image();
            categoryIcon.ID = categoryIdentifier + "_CategoryIcon";
            categoryIcon.ImageUrl = iconPath;
            categoryIcon.CssClass = "MediumIcon";
            categoryIcon.AlternateText = iconAltText;
            categoryIconContainer.Controls.Add(categoryIcon);

            // attach icon tooltip to category icon container
            Panel categoryIconTooltipContainer = new Panel();
            categoryIconTooltipContainer.ID = categoryIdentifier + "_CategoryIconTooltipContainer";
            categoryIconTooltipContainer.CssClass = "AdministratorMenuCategoryIconTooltipContainer";

            Label categoryIconTooltip = new Label();
            categoryIconTooltip.Text = iconAltText;

            categoryIconTooltipContainer.Controls.Add(categoryIconTooltip);
            categoryIconContainer.Controls.Add(categoryIconTooltipContainer);

            HyperLink iconLink = new HyperLink();
            iconLink.NavigateUrl = href;
            iconLink.Controls.Add(categoryIconContainer);

            this._AdministratorMenuCategoryIconsContainer.Controls.Add(iconLink);
        }
        #endregion

        #region _BuildJSActions
        private void _BuildJSActions()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("$(document).ready(function () {");
            sb.AppendLine("");
            sb.AppendLine("");

            sb.AppendLine("    $(\".AdministratorMenuCategoryMenuItemsContainer\").each(function () {");
            sb.AppendLine("");
            sb.AppendLine("        $(\"#\" + this.id).click(function (event) {");
            sb.AppendLine("");
            sb.AppendLine("            event.stopPropagation();");
            sb.AppendLine("");
            sb.AppendLine("        });");
            sb.AppendLine("");
            sb.AppendLine("    });");
            sb.AppendLine("");

            sb.AppendLine("    $(\".AdministratorMenuCategoryIconContainer\").each(function () {");
            sb.AppendLine("");
            sb.AppendLine("        $(\"#\" + this.id).click(function (event) {");
            sb.AppendLine("");
            sb.AppendLine("            event.stopPropagation();");
            sb.AppendLine("");
            sb.AppendLine("");
            sb.AppendLine("            var categoryIcon = this;");
            sb.AppendLine("");
            sb.AppendLine("            $(\".AdministratorMenuCategoryMenuItemsContainer\").each(function () {");
            sb.AppendLine("");
            sb.AppendLine("                var categoryIdFromIcon = categoryIcon.id.replace(\"_CategoryIconContainer\", \"\");");
            sb.AppendLine("                var categoryIdFromMenuItemsContainer = this.id.replace(\"_CategoryMenuItemsContainer\", \"\");");
            sb.AppendLine("                var categoryIconContainerId = this.id.replace(\"_CategoryMenuItemsContainer\", \"_CategoryIconContainer\");");
            sb.AppendLine("");
            sb.AppendLine("                $(\"#\" + categoryIconContainerId).removeClass(\"AdministratorMenuSelectedCategory\");");
            sb.AppendLine("");
            sb.AppendLine("                var categoryIconsContainerWidth = $(\"#AdministratorMenuCategoryIconsContainer\").outerWidth(true);");
            sb.AppendLine("                var categoryMenuItemContainerWidth = $(\"#\" + this.id).width();");
            sb.AppendLine("");
            sb.AppendLine("                if ($(\"#\" + this.id).css(\"left\") != (categoryMenuItemContainerWidth * -1) + \"px\" && categoryIdFromIcon != categoryIdFromMenuItemsContainer) {");
            sb.AppendLine("                    $(\"#\" + this.id).css(\"left\", (categoryMenuItemContainerWidth * -1) + \"px\");");
            sb.AppendLine("                }");
            sb.AppendLine("                else {");
            sb.AppendLine("                    if (categoryIdFromIcon == categoryIdFromMenuItemsContainer) {");
            sb.AppendLine("                        if ($(\"#\" + this.id).css(\"left\") != (categoryMenuItemContainerWidth * -1) + \"px\") {");
            sb.AppendLine("                            $(\"#\" + this.id).css(\"left\", (categoryMenuItemContainerWidth * -1) + \"px\");");
            sb.AppendLine("                        }");
            sb.AppendLine("                        else {");
            sb.AppendLine("                            $(\"#\" + this.id).css(\"left\", categoryIconsContainerWidth + \"px\");");
            sb.AppendLine("                            $(\"#\" + categoryIcon.id).addClass(\"AdministratorMenuSelectedCategory\");");
            sb.AppendLine("                        }");
            sb.AppendLine("                    }");
            sb.AppendLine("                }");
            sb.AppendLine("");
            sb.AppendLine("            });");
            sb.AppendLine("");
            sb.AppendLine("        });");
            sb.AppendLine("");
            sb.AppendLine("    });");
            sb.AppendLine("");

            sb.AppendLine("    $(document).click(function () {");
            sb.AppendLine("");
            sb.AppendLine("        $(\".AdministratorMenuCategoryMenuItemsContainer\").each(function () {");
            sb.AppendLine("");
            sb.AppendLine("            var categoryIconContainerId = this.id.replace(\"_CategoryMenuItemsContainer\", \"_CategoryIconContainer\");");
            sb.AppendLine("            $(\"#\" + categoryIconContainerId).removeClass(\"AdministratorMenuSelectedCategory\");");
            sb.AppendLine("");
            sb.AppendLine("            var categoryMenuItemContainerWidth = $(\"#\" + this.id).width();");
            sb.AppendLine("");
            sb.AppendLine("            if ($(\"#\" + this.id).css(\"left\") != (categoryMenuItemContainerWidth * -1) + \"px\") {");
            sb.AppendLine("                 $(\"#\" + this.id).css(\"left\", (categoryMenuItemContainerWidth * -1) + \"px\");");
            sb.AppendLine("            }");
            sb.AppendLine("");
            sb.AppendLine("        });");
            sb.AppendLine("");
            sb.AppendLine("    });");
            sb.AppendLine("");
            sb.AppendLine("});");

            this._JavascriptTag.InnerHtml += sb.ToString();
        }
        #endregion

        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.jquery.easing.1.3.js"); // for animation
        }
        #endregion

        #region RenderContents
        protected override void RenderContents(HtmlTextWriter writer)
        {
            // set icon paths
            this._SetIconPaths();

            // set href destinations for menu items
            this._SetHrefDestinations();

            // set menu item display properties
            this._SetMenuItemDisplayProperties();

            // if nothing is true, die - get count of items

            // javascript container
            this._JavascriptTag = new HtmlGenericControl("script");
            this._JavascriptTag.Attributes.Add("type", "text/javascript");

            // menu
            this._BuildMenu();

            this.Controls.AddAt(0, this._AdministratorMenuCategoryIconsContainer);

            // build JS menu actions
            this._BuildJSActions();

            // attach javascript tag - needs to be done last
            this.Controls.Add(this._JavascriptTag);

            // execute base
            base.RenderContents(writer);
        }
        #endregion
        #endregion
    }
}