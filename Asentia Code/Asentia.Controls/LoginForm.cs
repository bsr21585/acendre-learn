﻿using System;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    #region LoginFormLoginType
    public enum LoginFormLoginType
    {
        /// <summary>
        /// Standard.
        /// Login form for logging a user into their site.
        /// </summary>
        [Description("Standard")]
        Standard = 0,

        /// <summary>
        /// Customer Manager.
        /// Login form for logging a user into Customer Manager.
        /// </summary>
        [Description("Customer Manager")]
        CustomerManager = 1,
    }
    #endregion

    #region LoginFormRenderType
    public enum LoginFormRenderType
    {
        /// <summary>
        /// Standard.
        /// Login form renders on page with a modal for forgot password.
        /// </summary>
        [Description("Standard")]
        Standard = 0,

        /// <summary>
        /// In Modal.
        /// Login form renders inside of a modal, with forgot password 
        /// showing/hiding in same modal.
        /// </summary>
        [Description("In Modal")]
        InModal = 1,
    }
    #endregion

    public class LoginForm : WebControl, INamingContainer
    {
        #region Constructors
        public LoginForm()
            : base(HtmlTextWriterTag.Div)
        {
            // define IDs of main controls to be rendered here to ensure scope
            this.UsernameBox.ID = "LoginFormUsernameBox";
            this.PasswordBox.ID = "LoginFormPasswordBox";
            this.LoginButton.ID = "LoginFormLoginButton";
            this.ForgotPasswordLinkButton.ID = "LoginFormForgotPasswordLinkButton";
            this.ForgotPasswordModal.ID = "ForgotPasswordModal";
            this.ForgotPasswordSubmitButton.ID = "LoginFormForgotPasswordSubmitButton";
            this._ForgotPasswordCancelButton.ID = "LoginFormForgotPasswordCancelButton";
            this._FormContent.ID = "LoginFormFormContent";
            this._ForgotPasswordContent.ID = "LoginFormForgotPasswordContent";
            this._FeedbackPanel.ID = "LoginFormFeedbackPanel";
        }
        #endregion

        #region Properties
        /// <summary>
        /// The type of login that this login form is for. Default is "Standard" (Asentia)
        /// </summary>
        public LoginFormLoginType FormLoginType = LoginFormLoginType.Standard;

        /// <summary>
        /// The type of rendering for this login form. Default is "Standard" (On Page)
        /// </summary>
        public LoginFormRenderType FormRenderType = LoginFormRenderType.Standard;

        /// <summary>
        /// Show "Forgot Password" link?
        /// </summary>
        public bool ShowForgotPasswordLink = true;

        /// <summary>
        /// Show "Register" link?
        /// </summary>
        public bool ShowRegisterLink = true;

        /// <summary>
        /// TextBox for "Username."
        /// </summary>
        public TextBox UsernameBox = new TextBox();

        /// <summary>
        /// TextBox for "Password."
        /// </summary>
        public TextBox PasswordBox = new TextBox();

        /// <summary>
        /// The login button. 
        /// This is public so that it's Command property can be set from
        /// the instansiating code.
        /// </summary>
        public Button LoginButton = new Button();

        /// <summary>
        /// The forgot password link button. 
        /// This is public so that it's Command property can be set from
        /// the instansiating code.
        /// </summary>
        public LinkButton ForgotPasswordLinkButton = new LinkButton();

        /// <summary>
        /// Modal Popup for forgot password form. Only rendered if this login form is not InModal.
        /// </summary>
        public ModalPopup ForgotPasswordModal = new ModalPopup();

        /// <summary>
        /// The forgot password submit button.
        /// This is public so that it's Command property can be set from
        /// the instansiating code.
        /// </summary>
        public Button ForgotPasswordSubmitButton = new Button();

        /// <summary>
        /// Username TextBox for Forgot Password Modal.
        /// </summary>
        public TextBox ForgotPasswordModalUsernameBox = new TextBox();

        #endregion

        #region Private Properties
        /// <summary>
        /// Container that holds errors returned upon submission.
        /// </summary>
        private Panel _FeedbackPanel = new Panel();

        /// <summary>
        /// Container that holds the form content.
        /// </summary>
        private Panel _FormContent = new Panel();

        /// <summary>
        /// Container that holds the forgot password content. This is used for "InModal" login forms only.
        /// </summary>
        private Panel _ForgotPasswordContent = new Panel();

        /// <summary>
        /// The forgot password cancel button.
        /// This is only used/rendered in an InModal form for cancelling "forgot password," essentially
        /// it switches you back to the in modal login form.
        /// </summary>
        private Button _ForgotPasswordCancelButton = new Button();

        /// <summary>
        /// Path to the "forgot password" header image.
        /// </summary>
        private string _ForgotPasswordImagePath = String.Empty;

        /// <summary>
        /// Path to the "login" heafer image.
        /// </summary>
        private string _LoginImagePath = String.Empty;
        #endregion

        #region Methods
        #region DisplayFeedback
        /// <summary>
        /// Method to populate the _FeedbackPanel container with a success, or error message.
        /// </summary>
        /// <param name="message">message to be displayed</param>
        /// <param name="isError">determines if this message is an error message</param>
        public void DisplayFeedback(string message, bool isError, bool failedLogin)
        {
            // clear the container
            this._FeedbackPanel.Controls.Clear();

            // build and attach info status panel
            InformationStatusPanel infoStatusPanel;

            if (isError)
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.OnPage, InformationStatusPanel_MessageType.Error, message.Replace(Environment.NewLine, "<br />"));

                if (failedLogin)
                { infoStatusPanel.MessageHeaderOverrideText = _GlobalResources.LoginFailed; }
            }
            else
            {
                infoStatusPanel = new InformationStatusPanel("FeedbackInformationStatusPanel", InformationStatusPanel_Type.OnPage, InformationStatusPanel_MessageType.Success, message.Replace(Environment.NewLine, "<br />"));
            }

            this._FeedbackPanel.Controls.Add(infoStatusPanel);

            // make container visible
            this._FeedbackPanel.Visible = true;
        }
        #endregion

        #region ShowForgotPasswordPanel
        /// <summary>
        /// Shows the forgot password content panel. This is called from postbacks when we need to show
        /// the forgot password panel and hide the form content panel for InModal login form.
        /// </summary>
        public void ShowForgotPasswordPanel(bool isPostbackOperationSuccessful)
        {
            // set display to block on forgot password content panel
            this._ForgotPasswordContent.Style.Remove("display");
            this._ForgotPasswordContent.Style.Add("display", "block");

            // set display to none on form content panel
            this._FormContent.Style.Remove("display");
            this._FormContent.Style.Add("display", "none");

            // add startup JS to change the header icon and text to the forgot password information
            StringBuilder sb = new StringBuilder();
            sb.Append("Sys.Application.add_load(ShowForgotPasswordModalHeader);");
            sb.Append("function ShowForgotPasswordModalHeader() {");
            sb.Append(" Sys.Application.remove_load(ShowForgotPasswordModalHeader);");
            sb.Append(" $(\"#LoginFormModalModalPopupHeaderIcon\").prop(\"src\", \"" + this._ForgotPasswordImagePath + "\");");
            sb.Append(" $(\"#LoginFormModalModalPopupHeaderText\").text(\"" + _GlobalResources.ResetPassword + "\");");
            sb.Append("}");
            ScriptManager.RegisterStartupScript(this, GetType(), "ShowForgotPasswordModalHeader", sb.ToString(), true);

            // if the postback operation was successful, disable the forgot password submit button
            // and change the forgot password cancel button text to "Return to Login Form"
            if (isPostbackOperationSuccessful)
            {
                this.ForgotPasswordSubmitButton.Enabled = false;
                this.ForgotPasswordSubmitButton.CssClass = "Button ActionButton DisabledButton";

                this._ForgotPasswordCancelButton.Text = _GlobalResources.ReturnToLoginForm;
            }
        }
        #endregion

        #region ShowFormContentPanel
        /// <summary>
        /// Shows the form content panel. This is called from postbacks when we need to show
        /// the form content panel and hide the forgot password panel for InModal login form.
        /// </summary>
        public void ShowFormContentPanel()
        {
            // set display to block on form content panel
            this._FormContent.Style.Remove("display");
            this._FormContent.Style.Add("display", "block");

            // set display to none on forgot password content panel
            this._ForgotPasswordContent.Style.Remove("display");
            this._ForgotPasswordContent.Style.Add("display", "none");
        }
        #endregion
        #endregion

        #region Private Methods
        #region _BuildFormContent
        /// <summary>
        /// Method to build the login form content.
        /// </summary>
        private void _BuildFormContent()
        {
            // USERNAME LABEL
            HtmlGenericControl usernameLabelWrapper = new HtmlGenericControl("p");
            Label usernameLabel = new Label();
            usernameLabel.AssociatedControlID = this.UsernameBox.ID;
            usernameLabelWrapper.ID = "LoginFormUsernameLabelWrapper";

            usernameLabel.Text = _GlobalResources.Username;
            usernameLabelWrapper.Controls.Add(usernameLabel);
            this._FormContent.Controls.Add(usernameLabelWrapper);

            // USERNAME BOX
            HtmlGenericControl usernameBoxWrapper = new HtmlGenericControl("p");
            usernameBoxWrapper.ID = "LoginFormUsernameBoxWrapper";

            if (this.FormRenderType == LoginFormRenderType.InModal)
            { usernameBoxWrapper.Attributes.Add("class", "LoginFormBoxWrapperLong"); }
            else
            { usernameBoxWrapper.Attributes.Add("class", "LoginFormBoxWrapper"); }

            usernameBoxWrapper.Controls.Add(this.UsernameBox);
            this._FormContent.Controls.Add(usernameBoxWrapper);

            // PASSWORD LABEL
            HtmlGenericControl passwordLabelWrapper = new HtmlGenericControl("p");
            Label passwordLabel = new Label();
            passwordLabel.AssociatedControlID = this.PasswordBox.ID;
            passwordLabelWrapper.ID = "LoginFormPasswordLabelWrapper";

            passwordLabel.Text = _GlobalResources.Password_casesensitive;
            passwordLabelWrapper.Controls.Add(passwordLabel);
            this._FormContent.Controls.Add(passwordLabelWrapper);

            // PASSWORD BOX
            HtmlGenericControl passwordBoxWrapper = new HtmlGenericControl("p");
            passwordBoxWrapper.ID = "LoginFormPasswordBoxWrapper";

            if (this.FormRenderType == LoginFormRenderType.InModal)
            { passwordBoxWrapper.Attributes.Add("class", "LoginFormBoxWrapperLong"); }
            else
            { passwordBoxWrapper.Attributes.Add("class", "LoginFormBoxWrapper"); }

            this.PasswordBox.TextMode = TextBoxMode.Password;
            passwordBoxWrapper.Controls.Add(this.PasswordBox);
            this._FormContent.Controls.Add(passwordBoxWrapper);

            // BUTTON
            HtmlGenericControl buttonsWrapper = new HtmlGenericControl("p");
            buttonsWrapper.ID = "LoginFormButtonsWrapper";

            this.LoginButton.Text = _GlobalResources.Log_In;
            this.LoginButton.CssClass = "Button ActionButton";
            buttonsWrapper.Controls.Add(this.LoginButton);

            this._FormContent.Controls.Add(buttonsWrapper);

            // SET THE DEFAULT BUTTON FOR THE FORM CONTENT TO THE LOGIN BUTTON
            this._FormContent.DefaultButton = this.LoginButton.ID;

            // FORGOT PASSWORD & REGISTER LINKS PANEL
            if (this.ShowForgotPasswordLink || this.ShowRegisterLink)
            {
                // PANEL
                Panel forgotPasswordAndRegisterLinksPanel = new Panel();
                forgotPasswordAndRegisterLinksPanel.ID = this.ID + "ForgotPasswordAndRegisterLinkPanel";

                //check is resetting password allowed
                if (this.FormLoginType != LoginFormLoginType.CustomerManager)
                {
                    this.ShowForgotPasswordLink = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_PASSWORDRESET_ENABLED);
                    this.ShowRegisterLink = (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.SYSTEM_SELFREGISTRATION_ENABLED);
                }


                // FORGOT PASSWORD LINK
                if (this.ShowForgotPasswordLink)
                {
                    HtmlGenericControl forgotPasswordWrapper = new HtmlGenericControl("p");
                    forgotPasswordWrapper.ID = "LoginFormForgotPasswordLinkWrapper";
                    this.ForgotPasswordLinkButton.Text = _GlobalResources.IForgotMyPassword;

                    if (this.FormRenderType == LoginFormRenderType.InModal)
                    { this.ForgotPasswordLinkButton.OnClientClick = "javascript:ForgotPassword_Click(\"" + this._ForgotPasswordImagePath + "\", \"" + _GlobalResources.ResetPassword + "\", \"" + _GlobalResources.Cancel + "\"); return false;"; }

                    forgotPasswordWrapper.Controls.Add(this.ForgotPasswordLinkButton);

                    forgotPasswordAndRegisterLinksPanel.Controls.Add(forgotPasswordWrapper);
                }

                // REGISTER LINK
                if (this.ShowRegisterLink)
                {
                    HtmlGenericControl registerWrapper = new HtmlGenericControl("p");
                    registerWrapper.ID = "LoginFormRegisterLinkWrapper";

                    Localize registerText = new Localize();
                    registerText.Text = _GlobalResources.ToCreateAnAccount + " ";
                    registerWrapper.Controls.Add(registerText);

                    HyperLink registerLink = new HyperLink();

                    string registrationUrl;

                    if (this.FormLoginType == LoginFormLoginType.CustomerManager)
                    {
                        registrationUrl = string.Empty;
                    }
                    else
                    {
                        if (AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_SELFREGISTRATION_TYPE) == "custom")
                        {
                            registrationUrl = AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_SELFREGISTRATION_URLPREFIX) + AsentiaSessionState.GlobalSiteObject.ParamString(SiteParamConstants.SYSTEM_SELFREGISTRATION_URL);
                            registerLink.NavigateUrl = registrationUrl;
                        }
                        else if (HttpContext.Current.Request.Url.ToString().Contains("/catalog/") && HttpContext.Current.Request.Url.ToString().Contains("sc=")) //for shortcode redirection
                        {
                            string QStr = HttpContext.Current.Request.Url.PathAndQuery.ToString();
                            string RStr = QStr.Substring(QStr.IndexOf("default.aspx?")+13);
                            registerLink.NavigateUrl = "~/Register.aspx?referrer=shortcode&" + RStr;
                        }
                        else
                        {
                            registerLink.NavigateUrl = "~/Register.aspx";
                        }
                    }

                    registerLink.Text = _GlobalResources.RegisterHere;
                    registerWrapper.Controls.Add(registerLink);

                    forgotPasswordAndRegisterLinksPanel.Controls.Add(registerWrapper);
                }

                // ATTACH PANEL
                this._FormContent.Controls.Add(forgotPasswordAndRegisterLinksPanel);
            }

            // attach the form content to this control
            this.Controls.Add(this._FormContent);
        }
        #endregion

        #region _BuildForgotPasswordModal
        /// <summary>
        /// Method to build the forgot password modal.
        /// Note that all buttons are handeled by th model itself.
        /// </summary>
        private void _BuildForgotPasswordModal()
        {
            // set modal properties
            this.ForgotPasswordModal.Type = ModalPopupType.Form;

            // header image
            if (this.FormLoginType == LoginFormLoginType.CustomerManager)
            {
                this.ForgotPasswordModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PASSWORD, ImageFiles.EXT_PNG, true);                
                this.ForgotPasswordModal.OverrideLoadImagesFromCustomerManager = true;
            }
            else
            {
                this.ForgotPasswordModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_PASSWORD, ImageFiles.EXT_PNG);
            }

            this.ForgotPasswordModal.HeaderIconAlt = _GlobalResources.ResetPassword;
            this.ForgotPasswordModal.HeaderText = _GlobalResources.ResetPassword;
            this.ForgotPasswordModal.TargetControlID = this.ForgotPasswordLinkButton.ID;
            this.ForgotPasswordModal.FocusControlIdOnLaunch = this.ForgotPasswordModalUsernameBox.ID;
            this.ForgotPasswordModal.ReloadPageOnClose = true;


            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Localize body1 = new Localize();
            Localize body2 = new Localize();
            body1Wrapper.ID = "ForgotPasswordModalBody1";
            body2Wrapper.ID = "ForgotPasswordModalBody2";

            body1.Text = "<label>" + _GlobalResources.EnterYourUsernameInTheBoxBelow + "</label>";
            body2.Text = "<label>" + _GlobalResources.YourPasswordWillBeResetAndEmailed + "</label>";

            body1Wrapper.Controls.Add(body1);
            body2Wrapper.Controls.Add(body2);

            HtmlGenericControl usernameBoxWrapper = new HtmlGenericControl("p");
            usernameBoxWrapper.ID = "ForgotPasswordUsernameBoxWrapper";

            this.ForgotPasswordModalUsernameBox.ID = "ForgotPasswordUsernameBox";
            usernameBoxWrapper.Controls.Add(this.ForgotPasswordModalUsernameBox);

            // add controls to body
            this.ForgotPasswordModal.AddControlToBody(body1Wrapper);
            this.ForgotPasswordModal.AddControlToBody(body2Wrapper);
            this.ForgotPasswordModal.AddControlToBody(usernameBoxWrapper);

            // add modal to this control
            this.Controls.Add(this.ForgotPasswordModal);
        }
        #endregion

        #region _BuildForgotPasswordPanel
        /// <summary>
        /// Method to build the forgot password panel.
        /// This is for "InModal" login forms.
        /// </summary>
        private void _BuildForgotPasswordPanel()
        {
            this._ForgotPasswordContent.Style.Add("display", "none");

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            HtmlGenericControl body2Wrapper = new HtmlGenericControl("p");
            Localize body1 = new Localize();
            Localize body2 = new Localize();
            body1Wrapper.ID = "ForgotPasswordModalBody1";
            body2Wrapper.ID = "ForgotPasswordModalBody2";

            body1.Text = _GlobalResources.EnterYourUsernameInTheBoxBelow;
            body2.Text = _GlobalResources.YourPasswordWillBeResetAndEmailed;

            body1Wrapper.Controls.Add(body1);
            body2Wrapper.Controls.Add(body2);

            HtmlGenericControl usernameBoxWrapper = new HtmlGenericControl("p");
            usernameBoxWrapper.ID = "ForgotPasswordUsernameBoxWrapper";

            this.ForgotPasswordModalUsernameBox.ID = "ForgotPasswordUsernameBox";
            usernameBoxWrapper.Controls.Add(this.ForgotPasswordModalUsernameBox);

            // build the actions panel
            Panel actionsPanel = new Panel();
            actionsPanel.ID = "ForgotPasswordActionsPanel";
            actionsPanel.CssClass = "ActionsPanel";

            // submit button
            this.ForgotPasswordSubmitButton.Text = _GlobalResources.Submit;
            this.ForgotPasswordSubmitButton.CssClass = "Button ActionButton";
            actionsPanel.Controls.Add(this.ForgotPasswordSubmitButton);

            // cancel button
            this._ForgotPasswordCancelButton.Text = _GlobalResources.Cancel;
            this._ForgotPasswordCancelButton.CssClass = "Button NonActionButton";
            this._ForgotPasswordCancelButton.OnClientClick = "javascript:ForgotPasswordCancel_Click(\"" + this._LoginImagePath + "\", \"" + _GlobalResources.Log_In + "\"); return false;";
            actionsPanel.Controls.Add(this._ForgotPasswordCancelButton);

            // SET THE DEFAULT BUTTON FOR THE FORGOT PASSWORD CONTENT TO THE SUBMIT BUTTON
            this._ForgotPasswordContent.DefaultButton = this.ForgotPasswordSubmitButton.ID;

            // add controls to body
            this._ForgotPasswordContent.Controls.Add(body1Wrapper);
            this._ForgotPasswordContent.Controls.Add(body2Wrapper);
            this._ForgotPasswordContent.Controls.Add(usernameBoxWrapper);
            this._ForgotPasswordContent.Controls.Add(actionsPanel);

            // add modal to this control
            this.Controls.Add(this._ForgotPasswordContent);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            if (this.FormRenderType == LoginFormRenderType.InModal)
            {
                // register the embedded jQuery and javascript resource(s)
                ClientScriptManager csm = this.Page.ClientScript;
                csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.LoginForm.js");
            }
        }
        #endregion

        #region CreateChildControls
        /// <summary>
        /// Creates the controls for rendering to page.
        /// </summary>
        protected override void CreateChildControls()
        {
            // set the css class for the login form's container
            if (this.FormRenderType == LoginFormRenderType.Standard)
            { this.CssClass = "LoginFormOnPageContainer"; }

            if (this.FormLoginType == LoginFormLoginType.CustomerManager)
            {
                this._ForgotPasswordImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_PASSWORD,
                                                                       ImageFiles.EXT_PNG,
                                                                       true);
                this._LoginImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                                              ImageFiles.EXT_PNG,
                                                              true);
            }
            else
            {
                this._ForgotPasswordImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_PASSWORD,
                                                                       ImageFiles.EXT_PNG);
                this._LoginImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_LOGIN,
                                                              ImageFiles.EXT_PNG);
            }

            // FEEDBACK PANEL PLACEHOLDER
            this.Controls.Add(this._FeedbackPanel);

            // BUILD FORM CONTENT
            this._BuildFormContent();

            // BUILD FORGOT PASSWORD MODAL OR PANEL
            if (this.ShowForgotPasswordLink)
            {
                if (this.FormRenderType == LoginFormRenderType.InModal)
                { this._BuildForgotPasswordPanel(); }
                else
                { this._BuildForgotPasswordModal(); }
            }

            // continue executing base method
            base.CreateChildControls();
        }
        #endregion
        #endregion
    }
}