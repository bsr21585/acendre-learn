﻿function LaunchInformationStatusPanel(launchTriggerId, idPrefix, hasBlackout, isRelative) {
    // launch the blackout
    if (hasBlackout) {
        $("#" + idPrefix + "BlackoutPanel").show();
    }

    // launch the panel
    if (isRelative) {        
        var offset = $("#" + launchTriggerId).offset();

        $("#" + idPrefix + "WrapperContainer").fadeIn().css({
                left: Math.min(offset.left, $(window).innerWidth() - $("#" + idPrefix + "WrapperContainer").outerWidth() - 10),
                top: offset.top - 40
            });
    }
    else {
        $("#" + idPrefix + "WrapperContainer").fadeIn()
    }
}

function CloseInformationStatusPanel(idPrefix, hasBlackout) {    
    // close the panel
    $("#" + idPrefix + "WrapperContainer").fadeOut();

    // close the blackout
    if (hasBlackout) {
        $("#" + idPrefix + "BlackoutPanel").hide();
    }
}