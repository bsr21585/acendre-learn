﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    /// <summary>
    /// Displays a control that allows a user to select items and order them.
    /// </summary>
    public class MultiSelectWithOrdering : WebControl
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the MultiSelectWithOrdering control.
        /// </summary>
        /// <param name="id">identifier</param>
        public MultiSelectWithOrdering(string id)
            : base(HtmlTextWriterTag.Div)
        {
            // set the id
            this.ID = id;

            // build the controls
            this._BuildControls();
        }
        #endregion

        #region Properties
        /// <summary>
        /// The label to put above the available items list box.
        /// </summary>
        public string AvailableItemsLabel;

        /// <summary>
        /// The label to put above the selected items list box.
        /// </summary>
        public string SelectedItemsLabel;

        /// <summary>
        /// The height of the list boxes.
        /// </summary>
        public int ListBoxesRows = 0;

        /// <summary>
        /// A list of values to be pre-selected, should be populated with ordering.
        /// </summary>
        public List<string> PreSelectedValues;

        /// <summary>
        /// Read-only property that returns a list of selected values.
        /// </summary>
        public List<string> SelectedValuesList
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(this._SelectedItemsHF.Value))
                {
                    List<string> selectedItemsList = new List<string>();
                    string[] selectedItemsArr = this._SelectedItemsHF.Value.Split('|');

                    foreach (string selectedItem in selectedItemsArr)
                    { selectedItemsList.Add(selectedItem); }

                    return selectedItemsList;
                }
                else
                { return null; }
            }
        }

        /// <summary>
        /// Read-only property that returns a | delimited string of selected values.
        /// </summary>
        public string SelectedValuesString
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(this._SelectedItemsHF.Value))
                { return this._SelectedItemsHF.Value; }
                else
                { return null; }
            }
        }
        #endregion

        #region Private Properties
        private Localize _AvailableItemsLabel;
        private Localize _SelectedItemsLabel;
        private int _NumberAvailableItems = 0;
        private ListBox _AvailableItems;
        private ListBox _SelectedItems;
        private ImageButton _MoveAllToSelected;
        private ImageButton _MoveItemToSelected;
        private ImageButton _MoveItemToAvailable;
        private ImageButton _MoveAllToAvailable;
        private ImageButton _MoveItemUp;
        private ImageButton _MoveItemDown;
        private HiddenField _SelectedItemsHF;
        #endregion

        #region Methods
        #region AddAvailableItem
        /// <summary>
        /// Adds an item to the available items list box.
        /// </summary>
        /// <param name="label">item label</param>
        /// <param name="value">item value</param>
        public void AddAvailableItem(string label, string value)
        {
            this._NumberAvailableItems++;

            ListItem availableItem = new ListItem();
            availableItem.Text = label;
            availableItem.Value = value;
            availableItem.Attributes.Add("ordinal", this._NumberAvailableItems.ToString());

            this._AvailableItems.Items.Add(availableItem);
        }

        public void AddAvailableItem(string label, string value, string optionGroup)
        {
            this._NumberAvailableItems++;

            ListItem availableItem = new ListItem();
            availableItem.Text = label;
            availableItem.Value = value;
            availableItem.Attributes.Add("ordinal", this._NumberAvailableItems.ToString());
            availableItem.Attributes.Add("classification", optionGroup);

            this._AvailableItems.Items.Add(availableItem);
        }
        #endregion
        #endregion

        #region Private Methods
        #region _BuildControls
        /// <summary>
        /// Method that builds the controls for this control.
        /// </summary>
        private void _BuildControls()
        {
            // INITIALIZE CONTROLS

            // available items label
            this._AvailableItemsLabel = new Localize();
            this._AvailableItemsLabel.Text = _GlobalResources.AvailableItems;

            // available items list box
            this._AvailableItems = new ListBox();
            this._AvailableItems.ID = this.ID + "_AvailableItems";
            this._AvailableItems.CssClass = "MultipleSelectWithOrderingListBox";
            this._AvailableItems.SelectionMode = ListSelectionMode.Multiple;
            this._AvailableItems.Rows = 10;

            // available items label
            this._SelectedItemsLabel = new Localize();
            this._SelectedItemsLabel.Text = _GlobalResources.SelectedItems;

            // selected items list box
            this._SelectedItems = new ListBox();
            this._SelectedItems.ID = this.ID + "_SelectedItems";
            this._SelectedItems.CssClass = "MultipleSelectWithOrderingListBox";
            this._SelectedItems.Rows = 10;

            // selected items hidden field
            this._SelectedItemsHF = new HiddenField();
            this._SelectedItemsHF.ID = this.ID + "_SelectedItemsHiddenField";

            // move all to selected image button
            this._MoveAllToSelected = new ImageButton();
            this._MoveAllToSelected.ID = this.ID + "_MoveAllToSelected";
            this._MoveAllToSelected.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_LAST,
                                                                      ImageFiles.EXT_PNG);
            this._MoveAllToSelected.CssClass = "MediumIcon";
            this._MoveAllToSelected.OnClientClick = "javascript:MoveAllToSelected('" + this.ID + "'); return false;";

            // move item to selected image button
            this._MoveItemToSelected = new ImageButton();
            this._MoveItemToSelected.ID = this.ID + "_MoveItemToSelected";
            this._MoveItemToSelected.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_NEXT,
                                                                       ImageFiles.EXT_PNG);
            this._MoveItemToSelected.CssClass = "MediumIcon";
            this._MoveItemToSelected.OnClientClick = "javascript:MoveItemToSelected('" + this.ID + "'); return false;";

            // move item to available image button
            this._MoveItemToAvailable = new ImageButton();
            this._MoveItemToAvailable.ID = this.ID + "_MoveItemToAvailable";
            this._MoveItemToAvailable.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PREVIOUS,
                                                                        ImageFiles.EXT_PNG);
            this._MoveItemToAvailable.CssClass = "MediumIcon";
            this._MoveItemToAvailable.OnClientClick = "javascript:MoveItemToAvailable('" + this.ID + "'); return false;";

            // move all to available image button
            this._MoveAllToAvailable = new ImageButton();
            this._MoveAllToAvailable.ID = this.ID + "_MoveAllToAvailable";
            this._MoveAllToAvailable.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_FIRST,
                                                                       ImageFiles.EXT_PNG);
            this._MoveAllToAvailable.CssClass = "MediumIcon";
            this._MoveAllToAvailable.OnClientClick = "javascript:MoveAllToAvailable('" + this.ID + "'); return false;";

            // move item up image button
            this._MoveItemUp = new ImageButton();
            this._MoveItemUp.ID = this.ID + "_MoveItemUp";
            this._MoveItemUp.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PROMOTE,
                                                               ImageFiles.EXT_PNG);
            this._MoveItemUp.CssClass = "MediumIcon";
            this._MoveItemUp.OnClientClick = "javascript:MoveItemUp('" + this.ID + "'); return false;";

            // move item down image button
            this._MoveItemDown = new ImageButton();
            this._MoveItemDown.ID = this.ID + "_MoveItemDown";
            this._MoveItemDown.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DEMOTE,
                                                                 ImageFiles.EXT_PNG);
            this._MoveItemDown.CssClass = "MediumIcon";
            this._MoveItemDown.OnClientClick = "javascript:MoveItemDown('" + this.ID + "'); return false;";

            // BUILD DISPLAY STRUCTURE

            // table
            Table containerTable = new Table();
            containerTable.ID = this.ID + "_ContainerTable";

            // rows
            TableRow row1 = new TableRow();
            TableRow row2 = new TableRow();

            // columns
            TableCell row1col1 = new TableCell();
            TableCell row1col2 = new TableCell();
            TableCell row1col3 = new TableCell();
            TableCell row1col4 = new TableCell();
            TableCell row2col1 = new TableCell();
            TableCell row2col2 = new TableCell();
            TableCell row2col3 = new TableCell();
            TableCell row2col4 = new TableCell();

            // add label controls to columns
            Panel availableItemsLabelContainer = new Panel();
            availableItemsLabelContainer.ID = this.ID + "_AvailableItemsLabelContainer";
            availableItemsLabelContainer.CssClass = "FormFieldLabelContainer";
            availableItemsLabelContainer.Controls.Add(this._AvailableItemsLabel);
            row1col1.Controls.Add(availableItemsLabelContainer);

            Panel selectedItemsLabelContainer = new Panel();
            selectedItemsLabelContainer.ID = this.ID + "_SelectedItemsLabelContainer";
            selectedItemsLabelContainer.CssClass = "FormFieldLabelContainer";
            selectedItemsLabelContainer.Controls.Add(this._SelectedItemsLabel);
            row1col3.Controls.Add(selectedItemsLabelContainer);

            // add list box controls to columns
            row2col1.Controls.Add(this._AvailableItems);
            row2col3.Controls.Add(this._SelectedItems);
            row2col3.Controls.Add(this._SelectedItemsHF);

            // build panels for selector controls
            Panel moveAllToSelectedContainer = new Panel();
            moveAllToSelectedContainer.ID = this.ID + "_MoveAllToSelectedContainer";
            moveAllToSelectedContainer.CssClass = "MultiSelectWithOrderingButtonContainer";
            moveAllToSelectedContainer.Controls.Add(this._MoveAllToSelected);

            Panel moveItemToSelectedContainer = new Panel();
            moveItemToSelectedContainer.ID = this.ID + "_MoveItemToSelectedContainer";
            moveItemToSelectedContainer.CssClass = "MultiSelectWithOrderingButtonContainer";
            moveItemToSelectedContainer.Controls.Add(this._MoveItemToSelected);

            Panel moveItemToAvailableContainer = new Panel();
            moveItemToAvailableContainer.ID = this.ID + "_MoveItemToAvailableContainer";
            moveItemToAvailableContainer.CssClass = "MultiSelectWithOrderingButtonContainer";
            moveItemToAvailableContainer.Controls.Add(this._MoveItemToAvailable);

            Panel moveAllToAvailableContainer = new Panel();
            moveAllToAvailableContainer.ID = this.ID + "_MoveAllToAvailableContainer";
            moveAllToAvailableContainer.CssClass = "MultiSelectWithOrderingButtonContainer";
            moveAllToAvailableContainer.Controls.Add(this._MoveAllToAvailable);

            // add selector control panels to column
            row2col2.Controls.Add(moveAllToSelectedContainer);
            row2col2.Controls.Add(moveItemToSelectedContainer);
            row2col2.Controls.Add(moveItemToAvailableContainer);
            row2col2.Controls.Add(moveAllToAvailableContainer);

            // build panels for ordering controls
            Panel moveItemUpContainer = new Panel();
            moveItemUpContainer.ID = this.ID + "_MoveItemUpContainer";
            moveItemUpContainer.CssClass = "MultiSelectWithOrderingButtonContainer";
            moveItemUpContainer.Controls.Add(this._MoveItemUp);

            Panel moveItemDownContainer = new Panel();
            moveItemDownContainer.ID = this.ID + "_MoveItemDownContainer";
            moveItemDownContainer.CssClass = "MultiSelectWithOrderingButtonContainer";
            moveItemDownContainer.Controls.Add(this._MoveItemDown);

            // add ordering control panels to column
            row2col4.Controls.Add(moveItemUpContainer);
            row2col4.Controls.Add(moveItemDownContainer);

            // add columns to rows
            row1.Cells.Add(row1col1);
            row1.Cells.Add(row1col2);
            row1.Cells.Add(row1col3);
            row1.Cells.Add(row1col4);
            row2.Cells.Add(row2col1);
            row2.Cells.Add(row2col2);
            row2.Cells.Add(row2col3);
            row2.Cells.Add(row2col4);

            // add rows to table
            containerTable.Rows.Add(row1);
            containerTable.Rows.Add(row2);

            // add table to this control
            this.Controls.Add(containerTable);
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.MultiSelectWithOrdering.js");

            // build start up script
            StringBuilder startUpScript = new StringBuilder();
            startUpScript.AppendLine("EvaluateButtonStates('" + this.ID + "');");

            csm.RegisterStartupScript(typeof(Asentia.Controls.ClientScript), "MultiSelectWithOrderingStartUp", startUpScript.ToString(), true);
        }
        #endregion

        #region CreateChildControls
        protected override void CreateChildControls()
        {
            // set the available and selected field labels if there are label overrides
            if (!String.IsNullOrWhiteSpace(this.AvailableItemsLabel))
            { this._AvailableItemsLabel.Text = this.AvailableItemsLabel; }

            if (!String.IsNullOrWhiteSpace(this.SelectedItemsLabel))
            { this._SelectedItemsLabel.Text = this.SelectedItemsLabel; }

            // set the row height of the available and selected fields if there is an override
            if (this.ListBoxesRows > 0)
            {
                this._SelectedItems.Rows = this.ListBoxesRows;
                this._AvailableItems.Rows = this.ListBoxesRows;
            }

            // pre-select fields if there are fields to be pre-selected
            if (this.PreSelectedValues != null)
            {
                // string for hidden field value
                string selectedValuesForHiddenField = String.Empty;

                // loop through pre-selected values, and if it exists in available items, add it to
                // selected items, remove it from available items, and add to hidden field string
                List<ListItem> removeFromAvailableListBox = new List<ListItem>();

                foreach (string value in this.PreSelectedValues)
                {
                    foreach (ListItem item in this._AvailableItems.Items)
                    {
                        if (item.Value == value)
                        {
                            ListItem selectedItem = new ListItem();
                            selectedItem.Text = item.Text;
                            selectedItem.Value = item.Value;
                            selectedItem.Attributes.Add("ordinal", item.Attributes["ordinal"]);
                            selectedItem.Attributes.Add("classification", item.Attributes["classification"]);

                            this._SelectedItems.Items.Add(selectedItem);
                            removeFromAvailableListBox.Add(item);
                            selectedValuesForHiddenField += item.Value + "|";
                        }
                    }
                }

                // remove the selected items from available list
                foreach (ListItem item in removeFromAvailableListBox)
                {
                    this._AvailableItems.Items.Remove(item);
                }

                // clean up hidden field string
                if (selectedValuesForHiddenField.Length > 0)
                {
                    if (selectedValuesForHiddenField.Substring(selectedValuesForHiddenField.Length - 1, 1) == "|")
                    { selectedValuesForHiddenField = selectedValuesForHiddenField.Substring(0, selectedValuesForHiddenField.Length - 1); }
                }

                // set the hidden field value
                this._SelectedItemsHF.Value = selectedValuesForHiddenField;
            }

            // execute the base
            base.CreateChildControls();
        }
        #endregion
        #endregion

    }
}