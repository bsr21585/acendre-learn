﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Web.UI.WebControls;
using Asentia.Common;

namespace Asentia.Controls
{
    public class AsentiaAuthenticatedPage : AsentiaPage
    {
        #region Properties
        public AdministratorMenu AdminMenu = new AdministratorMenu();
        #endregion

        #region InitializeAdminMenu
        public void InitializeAdminMenu()
        {
            // get the requested url
            string requestedUrl = Request.Url.AbsolutePath.ToString().ToLower();
          
            // check the permission for catalog browse
            if (AsentiaSessionState.IdSiteUser > 0) // this check needs to be done because if we go straight to the check below and we do not have a logged in user, we get an exception
            {
                if (AsentiaSessionState.IdSiteUser == 1 || AsentiaSessionState.UserEffectivePermissions.Count > 0)
                {
                    this.AdminMenu.Visible = true;

                    Panel masterContentMiddleContainer = (Panel)this.Master.FindControl("MasterContentMiddle");

                    if (masterContentMiddleContainer != null)
                    { masterContentMiddleContainer.CssClass += " MarginOffsetForAdministratorMenu"; }
                }
            }
        }
        #endregion

        #region OnPreInit
        protected override void OnPreInit(EventArgs e)
        {
            // get the requested url
            string requestedUrl = Request.Url.AbsolutePath.ToString().ToLower();
          
            // check the permission for catalog browse
            if (requestedUrl == "/catalog/default.aspx" || requestedUrl == "/catalog/search.aspx")
            {
                AsentiaSite siteObject = new AsentiaSite(AsentiaSessionState.IdSiteUser, AsentiaSessionState.IdSite);

                bool catalogBrowseOption = siteObject.ParamBool(SiteParamConstants.CATALOG_ENABLE) ?? false;
                bool isLoginRequired = siteObject.ParamBool(SiteParamConstants.CATALOG_REQUIRE_LOGIN) ?? false;

                // if the user isnt authenticated, then either throw a 404 or redirect back to home
                if (AsentiaSessionState.IdSiteUser == 0 && Config.ApplicationSettings.Throw404OnNonAuthenticatedPages)
                {
                    Response.Clear();
                    Response.StatusCode = 404;
                    Response.End();
                }
                else if (!catalogBrowseOption || (isLoginRequired && AsentiaSessionState.IdSiteUser == 0))
                {
                    Response.Redirect("/");
                }
            }
            else if (AsentiaSessionState.IdSiteUser == 0)
            {
                // if the user isnt authenticated, then either throw a 404 or redirect back to home
                if (Config.ApplicationSettings.Throw404OnNonAuthenticatedPages)
                {
                    Response.Clear();
                    Response.StatusCode = 404;
                    Response.End();
                }
                else
                {
                    Response.Redirect("/Default.aspx");
                }
            }

            // update user session expiration
            UpdateUserSessionExpiration(AsentiaSessionState.DtExpires);

            // update user effective permissions - unless user id is 1
            if (AsentiaSessionState.IdSiteUser > 1)
            { UpdateUserEffectivePermissions(); }

            // redirect the user to the change password page if the user must change their password
            if (requestedUrl != "/myprofile/changepassword.aspx" && AsentiaSessionState.UserMustChangePassword)
            { Response.Redirect("/myprofile/ChangePassword.aspx"); }

            // execute normal PreInit operations
            base.OnPreInit(e);
        }
        #endregion

        #region OnLoad
        protected override void OnLoad(EventArgs e)
        {
            // hide the administrator menu by default, individual
            // pages will determine whether to turn it on or not
            this.AdminMenu.Visible = false;

            // execute the base
            base.OnLoad(e);
        }
        #endregion

        #region UpdateUserSessionExpiration
        public static void UpdateUserSessionExpiration(DateTime? dtExpires)
        {
            AsentiaDatabase accountDatabaseObject = new AsentiaDatabase();

            accountDatabaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            accountDatabaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            accountDatabaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            accountDatabaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            accountDatabaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            accountDatabaseObject.AddParameter("@dtSessionExpires", dtExpires, SqlDbType.DateTime, 8, ParameterDirection.Input);

            try
            {
                accountDatabaseObject.ExecuteNonQuery("[System.UpdateUserSessionExpire]", true);
            }
            catch
            {
                throw;
            }
            finally
            {
                accountDatabaseObject.Dispose();
            }
        }
        #endregion

        #region UpdateUserEffectivePermissions
        public static void UpdateUserEffectivePermissions(bool forcePermissionUpdate = false)
        {
            // this should only be done if the last check was more than 5 minutes ago
            if (AsentiaSessionState.UserLastPermissionCheckUtc.AddMinutes(5) < AsentiaSessionState.UtcNow || forcePermissionUpdate)
            {
                DataTable dt = new DataTable();

                AsentiaDatabase databaseObject = new AsentiaDatabase();

                try
                {
                    databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                    databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                    databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                    databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                    databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                    databaseObject.AddParameter("@dtLastPermissionCheck", null, SqlDbType.DateTime, 8, ParameterDirection.Output);

                    SqlDataReader sdr = databaseObject.ExecuteDataReader("[User.GetEffectivePermissions]", true);
                    dt.Load(sdr);
                    sdr.Close();

                    DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                    string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                    AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                    // store the last permission check timestamp in session
                    DateTime dtLastPermissionCheck = DateTime.Parse(databaseObject.Command.Parameters["@dtLastPermissionCheck"].Value.ToString());
                    AsentiaSessionState.UserLastPermissionCheckUtc = dtLastPermissionCheck;

                    // store the effective permissions in session state
                    List<AsentiaPermissionWithScope> effectivePermissionsList = new List<AsentiaPermissionWithScope>();

                    foreach (DataRow row in dt.Rows)
                    {
                        AsentiaPermissionWithScope permissionWithScope = new AsentiaPermissionWithScope((AsentiaPermission)Convert.ToInt32(row["idPermission"]), row["scope"].ToString());
                        effectivePermissionsList.Add(permissionWithScope);
                    }

                    AsentiaSessionState.UserEffectivePermissions = effectivePermissionsList;
                }
                catch
                {
                    throw;
                }
                finally
                {
                    databaseObject.Dispose();
                }
            }
        }
        #endregion

        #region CheckPermission
        /// <summary>
        /// Checks whether or not the currently logged in user has a specified permission and scope.
        /// Overridden to take in a list of object ids.
        /// </summary>
        /// <param name="permissionCode">Asentia permission code</param>
        /// <param name="objectList">this will always be a list of group ids as the scoped permissions are scoped to group</param>
        /// <returns>true/false</returns>
        public static bool CheckPermission(AsentiaPermission permissionCode, List<int> objectList)
        {
            // if the user is id 1 (admin) just return true
            if (AsentiaSessionState.IdSiteUser == 1)
            { return true; }

            // if no list of objects, check if the user has global scope for the permission, otherwise check permissions against scope
            if (objectList == null)
            {
                foreach (AsentiaPermissionWithScope permissionWithScope in AsentiaSessionState.UserEffectivePermissions)
                {
                    if (permissionWithScope.PermissionCode == permissionCode && permissionWithScope.Scope == null)
                    { return true; }
                }
            }
            else
            {
                foreach (int idObject in objectList)
                {
                    if (CheckPermission(permissionCode, idObject))
                    { return true; }
                }
            }

            return false; // if we get this far without returning true, the user does not have permission, return false
        }

        /// <summary>
        /// Checks whether or not the currently logged in user has a specified permission and scope.
        /// </summary>
        /// <param name="permissionCode">Asentia permission code</param>
        /// <param name="idObject">this will always be a group id as the scoped permissions are scoped to group</param>
        /// <returns>true/false</returns>
        public static bool CheckPermission(AsentiaPermission permissionCode, int idObject = 0)
        {
            // if the user is id 1 (admin) just return true
            if (AsentiaSessionState.IdSiteUser == 1)
            { return true; }

            // check if user has the permission by looping through effective permissions and checking scope (if necessary)
            foreach (AsentiaPermissionWithScope permissionWithScope in AsentiaSessionState.UserEffectivePermissions)
            {
                if (permissionWithScope.PermissionCode == permissionCode) // if user has permission, check for scope
                {
                    if (idObject == 0) // if there is no specific object to check scope for, just return true
                    { return true; }

                    if (permissionWithScope.Scope == null) // if there is a null scope, user has access to all scoped objects, just return true
                    { return true; }

                    // go through each scope looking for a match to the object, if we match, return true
                    foreach (int idScopedObject in permissionWithScope.Scope)
                    {
                        if (idScopedObject == idObject)
                        { return true; }
                    }
                }
            }

            return false; // if we get this far without returning true, the user does not have permission, return false
        }
        #endregion
    }
}
