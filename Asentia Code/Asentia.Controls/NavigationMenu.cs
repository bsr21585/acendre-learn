﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Asentia.Common;

namespace Asentia.Controls
{
    public enum MenuType
    {
        [Description("Custom Menu")]
        CustomMenu = 0,

        [Description("Main Navigation Menu")]
        MainNavigationMenu = 1,
    }

    public class NavigationMenu : WebControl
    {
        public MenuType Type = MenuType.CustomMenu;
        private Control _MenuControl = new Control();
        private ArrayList _MenuItems = new ArrayList();

        private class _MenuItem
        {
            public string Identifier;
            public string Label;
            public string Href;
            public string HrefTarget;
            public string ParentIdentifier;
            public string ImagePath;

            public _MenuItem(string identifier, string label, string href, string hrefTarget, string parentIdentifier, string imagePath)
            {
                this.Identifier = identifier;
                this.Label = label;
                this.Href = href;
                this.HrefTarget = hrefTarget;
                this.ParentIdentifier = parentIdentifier;
                this.ImagePath = imagePath;
            }
        }

        public NavigationMenu(MenuType type)
            : base(HtmlTextWriterTag.Div)
        {
            this.Type = type;
        }

        public void AddMenuItem(string identifier, string label, string href, string hrefTarget, string parentIdentifier, string imagePath)
        {
            bool catalogBrowseOption = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_ENABLE) ?? false;
            bool isLoginRequired = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CATALOG_REQUIRE_LOGIN) ?? false;

            bool messageCenterOption = AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.MESSAGECENTER_ENABLE) ?? false;

            if (identifier != "Catalog" || (catalogBrowseOption && (!isLoginRequired || AsentiaSessionState.IdSiteUser != 0)))
            {
                if (identifier != "MessageCenter" || messageCenterOption)
                {
                    _MenuItem menuItem = new _MenuItem(identifier, label, href, hrefTarget, parentIdentifier, imagePath);
                    this._MenuItems.Add(menuItem);
                }
            }
        }

        public void AddMenuItem(string identifier, string label, string href, string parentIdentifier, string imagePath)
        {
            AddMenuItem(identifier, label, href, String.Empty, parentIdentifier, imagePath);
        }

        #region _BuildMenuControl
        /// <summary>
        /// Builds the menu control of items. This function takes no arguments
        /// and serves as the entry point to build the menu starting at the root
        /// elements (parentIdentiter is null).
        /// </summary>
        /// <returns>Generic control with a ul > li structure representing the menu.</returns>
        private HtmlGenericControl _BuildMenuControl()
        {
            // create the outer ul control
            HtmlGenericControl control = new HtmlGenericControl("ul");

            // loop through menu and build only root elements
            foreach (_MenuItem item in this._MenuItems)
            {
                if (item.ParentIdentifier == null)
                { control.Controls.Add(this._BuildMenuControl(item.Identifier)); }
            }

            return control;
        }

        /// <summary>
        /// Builds the items of the menu control. This function takes an
        /// argument of a menu item identifier, builds its li, and processes its children.
        /// This function should not be called directly. To build a menu, call the one that
        /// takes no arguments.
        /// </summary>
        /// <returns>Generic control with a ul > li structure representing the menu.</returns>
        private HtmlGenericControl _BuildMenuControl(string identifier)
        {
            // create the li control
            HtmlGenericControl listItem = new HtmlGenericControl("li");

            // loop through the menu items to get the item we're going to process
            foreach (_MenuItem item in this._MenuItems)
            {
                if (item.Identifier == identifier)
                {
                    // build the li, and hyperlink (if necessary) for the menu item
                    if (item.Href != null)
                    {
                        HtmlAnchor itemLink = new HtmlAnchor();
                        itemLink.HRef = item.Href;
                        itemLink.Target = item.HrefTarget;

                        // attach image to link if "built-in" menu item or menu item has an image
                        switch (item.Identifier)
                        {
                            case "MyDashboard":
                                Image imageMyDashboard = new Image();
                                imageMyDashboard.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DASHBOARD_WHITE, ImageFiles.EXT_PNG);
                                imageMyDashboard.CssClass = "SmallIcon";
                                itemLink.Controls.Add(imageMyDashboard);
                                break;
                            case "MessageCenter":
                                Image imageMessageCenter = new Image();
                                imageMessageCenter.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_EMAIL_WHITE, ImageFiles.EXT_PNG);
                                imageMessageCenter.CssClass = "SmallIcon";
                                itemLink.Controls.Add(imageMessageCenter);
                                break;
                            case "Catalog":
                                Image imageCatalog = new Image();
                                imageCatalog.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CATALOG_WHITE, ImageFiles.EXT_PNG);
                                imageCatalog.CssClass = "SmallIcon";
                                itemLink.Controls.Add(imageCatalog);
                                break;
                            default:
                                Image imageGeneric = new Image();

                                if (!String.IsNullOrEmpty(item.ImagePath))
                                { imageGeneric.ImageUrl = item.ImagePath; }
                                else
                                { imageGeneric.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_GO, ImageFiles.EXT_PNG); }

                                imageGeneric.CssClass = "SmallIcon";
                                itemLink.Controls.Add(imageGeneric);
                                break;
                        }

                        // attach label to link
                        Label itemLabel = new Label();
                        itemLabel.CssClass = "MobileHidden";
                        itemLabel.Text = item.Label;
                        itemLink.Controls.Add(itemLabel);

                        // attach link to item
                        listItem.Controls.Add(itemLink);
                    }
                    else
                    {
                        // attach label
                        Label itemLabel = new Label();
                        itemLabel.CssClass = "MobileHidden";
                        itemLabel.Text = item.Label;

                        listItem.Controls.Add(itemLabel);
                    }

                    // add "extras" if "built-in" menu item
                    switch (item.Identifier)
                    {
                        case "MessageCenter":
                            // MesageCenter gets "new messages" count

                            AsentiaDatabase databaseObject = new AsentiaDatabase();
                            int newMessageCount = 0;

                            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                            databaseObject.AddParameter("@newMessageCount", null, SqlDbType.Int, 4, ParameterDirection.Output);

                            try
                            {
                                databaseObject.ExecuteNonQuery("[System.GetLoggedInUsersNewMessageCount]", true);

                                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                                newMessageCount = Convert.ToInt32(databaseObject.Command.Parameters["@newMessageCount"].Value);
                            }
                            catch
                            { throw; }
                            finally
                            { databaseObject.Dispose(); }

                            if (newMessageCount > 0)
                            {
                                Label newMessageCountLabel = new Label();
                                newMessageCountLabel.CssClass = "MainNavigationControlItemAlert";
                                newMessageCountLabel.Text = newMessageCount.ToString();
                                listItem.Controls.Add(newMessageCountLabel);
                            }

                            break;
                        default:
                            break;
                    }

                    // detect if this item has children
                    bool itemHasChildren = false;
                    foreach (_MenuItem potentalChildItem in this._MenuItems)
                    {
                        if (potentalChildItem.ParentIdentifier == item.Identifier)
                        {
                            itemHasChildren = true;
                            break;
                        }
                    }

                    // if this item has children, create a ul control
                    // and recursively process the children
                    if (itemHasChildren)
                    {
                        HtmlGenericControl unorderedList = new HtmlGenericControl("ul");

                        foreach (_MenuItem childItem in this._MenuItems)
                        {
                            if (childItem.ParentIdentifier == item.Identifier)
                            { unorderedList.Controls.Add(this._BuildMenuControl(childItem.Identifier)); }
                        }

                        // add the child ul control to this item
                        listItem.Controls.Add(unorderedList);
                    }
                }
            }

            return listItem;
        }
        #endregion

        private HtmlGenericControl _BuildMainMenu()
        {
            string xmlMainMenuPath = MapPathSecure(SitePathConstants.DEFAULT_SITE_TEMPLATE_MENU + "MainMenu.xml");
            string xmlSiteSpecificMainMenuPath = MapPathSecure(SitePathConstants.SITE_TEMPLATE_MENU + "MainMenu.xml");

            if (File.Exists(xmlSiteSpecificMainMenuPath))
            { _BuildMenuFromXML(xmlSiteSpecificMainMenuPath); }
            else if (File.Exists(xmlMainMenuPath))
            { _BuildMenuFromXML(xmlMainMenuPath); }
            else
            {
                // user must be logged in to view the following links
                if (AsentiaSessionState.IdSiteUser > 0)
                {
                    AddMenuItem("MyDashboard", _GlobalResources.MyDashboard, "/dashboard", null, null);
                    AddMenuItem("MessageCenter", _GlobalResources.MessageCenter, "/messagecenter", null, null);
                }

                AddMenuItem("Catalog", _GlobalResources.Catalog, "/catalog", null, null);
            }

            return this._BuildMenuControl();
        }

        /// <summary>
        /// Builds a menu using an XML file representation of the menu.
        /// </summary>
        /// <param name="xmlMenuPath">path to the XML file</param>
        private void _BuildMenuFromXML(string xmlMenuPath)
        {
            // load the XML document from the path specified
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(xmlMenuPath);

            // get the document element node (which is a <Menu> node) and pass 
            // to the _AddMenuItemFromNode method for recursive processing
            XmlNode documentElement = xmlDocument.DocumentElement;
            this._AddItemFromMenuNode(documentElement, null);
        }

        /// <summary>
        /// Recursive function to process "Menu" XML nodes and add them to the menu.
        /// </summary>
        /// <param name="menuNode">"Menu" node</param>
        /// <param name="parentItemIdentifier">the identifier of this menu item's parent</param>
        private void _AddItemFromMenuNode(XmlNode menuNode, string parentItemIdentifier)
        {
            // grab the children of this <Menu> node
            XmlNodeList menuNodeChildren = menuNode.ChildNodes;

            foreach (XmlNode childNode in menuNodeChildren)
            {
                // if this is an <Item> process it - note that the only direct children
                // of <Menu> will always be <Item>s
                if (childNode.Name == "Item")
                {
                    // set the loginRequired property, it will determine if this item gets added to the menu or not
                    bool loginRequired = Convert.ToBoolean(childNode.Attributes["loginRequired"].Value);

                    // if login is not required, or it is required and the user is logged in, proceed
                    // to add this item to the menu
                    if (!loginRequired || (loginRequired && AsentiaSessionState.IdSiteUser > 0))
                    {
                        // grab the children of this <Item> node
                        XmlNodeList itemChildren = childNode.ChildNodes;

                        // declare properties needed for menu building
                        string identifier = null;
                        string label = null;
                        string href = null;
                        string hrefTarget = null;
                        string imagePath = null;

                        // loop through the children of this <Item> to build the properties
                        foreach (XmlNode itemChild in itemChildren)
                        {
                            // build the label based on the language
                            if (itemChild.Name == "Label")
                            {
                                string language = itemChild.Attributes["language"].Value;

                                if (language == "default" || language == AsentiaSessionState.UserCulture)
                                {
                                    identifier = itemChild.Attributes["identifier"].Value;
                                    label = itemChild.InnerText;
                                }
                            }

                            // build the href
                            if (itemChild.Name == "Href")
                            {
                                href = itemChild.InnerText;
                                hrefTarget = itemChild.Attributes["target"].Value;
                            }

                            // build the image
                            if (itemChild.Name == "ImagePath")
                            {
                                imagePath = itemChild.InnerText;
                            }

                            // if this <Item> has a "sub" <Menu>, process through recursion
                            if (itemChild.Name == "Menu")
                            {
                                this._AddItemFromMenuNode(itemChild, identifier);
                            }
                        }

                        // add this <Item> to the menu
                        AddMenuItem(identifier, label, href, hrefTarget, parentItemIdentifier, imagePath);
                    }
                }
            }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            switch (Type)
            {
                case MenuType.CustomMenu:
                    _MenuControl.Controls.Add(_BuildMenuControl());
                    break;
                case MenuType.MainNavigationMenu:
                    _MenuControl.Controls.Add(_BuildMainMenu());
                    break;
                default:
                    _MenuControl.Controls.Add(_BuildMenuControl());
                    break;
            }

            if (!this._MenuControl.HasControls())
            { return; }

            this.Controls.Add(_MenuControl);
            base.RenderContents(writer);
        }
    }
}
