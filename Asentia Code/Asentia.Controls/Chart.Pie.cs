﻿using System;
using System.Collections;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.Controls
{
    #region Chart_Pie_Dataset CLASS
    public class Chart_Pie_Dataset
    {
        #region Constructor
        public Chart_Pie_Dataset(string fillColor, object value)
        {
            this._FillColor = fillColor;
            this._Value = value;
        }
        #endregion

        #region Properties
        public string FillColor
        {
            get { return this._FillColor; }
        }

        public object Value
        {
            get { return this._Value; }
        }
        #endregion

        #region Private Properties
        private string _FillColor;
        private object _Value;
        #endregion
    }
    #endregion

    public class Chart_Pie : WebControl
    {
        #region Constructor
        public Chart_Pie(string id, int width, int height)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.CanvasWidth = width;
            this.CanvasHeight = height;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The width of the canvas tag.
        /// </summary>
        public int CanvasWidth;

        /// <summary>
        /// The height of the canvas tag.
        /// </summary>
        public int CanvasHeight;

        /// <summary>
        /// Show stroke on each segment?
        /// </summary>
	    public bool SegmentShowStroke = true;

        /// <summary>
        /// The color of each segment stroke.
        /// </summary>
	    public string SegmentStrokeColor = "#FFFFFF";

        /// <summary>
        /// The width of each segment stroke.
        /// </summary>
	    public int SegmentStrokeWidth = 2;

        /// <summary>
        /// Animate the chart?
        /// </summary>
	    public bool Animation = true;

        /// <summary>
        /// Number of animation steps.
        /// </summary>
	    public int AnimationSteps = 100;

        /// <summary>
        /// Animation easing effect.
        /// </summary>
	    public string AnimationEasing = "easeOutBounce";

        /// <summary>
        /// Animate the rotation of the pie?
        /// </summary>
	    public bool AnimateRotate = true;

        /// <summary>
        /// Animate scaling of the pie from center?
        /// </summary>
	    public bool AnimateScale = false;

        /// <summary>
        /// Method to fire when animation is complete.
        /// </summary>
	    public string OnAnimationComplete = null;
        #endregion

        #region Private Properties
        /// <summary>
        /// An ArrayList of datasets for this chart.
        /// </summary>
        ArrayList _Datasets = null;
        #endregion

        #region Methods
        #region AddDataset
        // TODO: VALIDATIONS
        /// <summary>
        /// Adds a dataset of type Chart_Pie_Dataset to the dataset ArrayList.
        /// </summary>
        /// <param name="dataset">dataset data</param>
        public void AddDataset(Chart_Pie_Dataset dataset)
        {
            if (this._Datasets == null)
            { this._Datasets = new ArrayList(); }

            this._Datasets.Add(dataset);
        }
        #endregion

        #region ClearDatasets
        /// <summary>
        /// Clears the dataset ArrayList.
        /// </summary>
        public void ClearDatasets()
        {
            if (this._Datasets == null)
            { return; }

            this._Datasets.Clear();
        }
        #endregion
        #endregion

        #region Private Methods
        #region _BuildOptionsJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's options
        /// based on the option properties.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildOptionsJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON
            sb.AppendLine("var " + this.ID + "Options = {");
            sb.AppendLine(" segmentShowStroke : " + this.SegmentShowStroke.ToString().ToLower() + ",");
            sb.AppendLine(" segmentStrokeColor  : " + this._FormatJSStringVarValue(this.SegmentStrokeColor) + ",");
            sb.AppendLine(" segmentStrokeWidth  : " + this.SegmentStrokeWidth.ToString() + ",");
            sb.AppendLine(" animation : " + this.Animation.ToString().ToLower() + ",");
            sb.AppendLine(" animationSteps : " + this.AnimationSteps.ToString() + ",");
            sb.AppendLine(" animationEasing : " + this._FormatJSStringVarValue(this.AnimationEasing) + ",");
            sb.AppendLine(" animateRotate : " + this.AnimateRotate.ToString().ToLower() + ",");
            sb.AppendLine(" animateScale : " + this.AnimateScale.ToString().ToLower() + ",");
            sb.AppendLine(" onAnimationComplete : " + this._FormatJSStringVarValue(this.OnAnimationComplete));
            sb.AppendLine("};");

            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildDataJSONString
        /// <summary>
        /// Builds JSON variable representation of the chart's data
        /// based on the datasets popuated for this chart.
        /// </summary>
        /// <returns>JSON string variable for inclusion in script tag</returns>
        private string _BuildDataJSONString()
        {
            StringBuilder sb = new StringBuilder();

            // begin variable declaration
            sb.AppendLine("var " + this.ID + "Data = [");

            int i = 0;

            foreach (Chart_Pie_Dataset ds in this._Datasets)
            {
                // increment i
                i++;

                // build the data
                sb.AppendLine(" {");
                sb.AppendLine(" value : " + ds.Value + ",");
                sb.AppendLine(" color : " + this._FormatJSStringVarValue(ds.FillColor) + ",");

                if (i < this._Datasets.Count)
                { sb.AppendLine("   },"); }
                else
                { sb.AppendLine("   }"); }
            }

            sb.AppendLine("];");

            // return
            return sb.ToString();
        }
        #endregion

        #region _BuildAndRegisterJSBlock
        /// <summary>
        /// Builds the JS block that renders the chart, and registers the
        /// JS to this control.
        /// </summary>
        private void _BuildAndRegisterJSBlock()
        {
            StringBuilder sb = new StringBuilder();

            // build options JSON
            sb.AppendLine(this._BuildOptionsJSONString());

            // build data JSON
            sb.AppendLine(this._BuildDataJSONString());

            // build call to chart function
            sb.AppendLine("var " + this.ID + "Bound = new Chart(document.getElementById(\"" + this.ID + "Canvas\").getContext(\"2d\")).Pie(" + this.ID + "Data, " + this.ID + "Options);");

            // register script
            ScriptManager.RegisterStartupScript(this, this.GetType(), this.ID + "StartUpScript", sb.ToString(), true);
        }
        #endregion

        #region _FormatJSStringVarValue
        /// <summary>
        /// Formats a string for attachment to a JS variable.
        /// </summary>
        /// <param name="value">string value</param>
        /// <returns>formatted string</returns>
        private string _FormatJSStringVarValue(string value)
        {
            if (value == null)
            { return "null"; }
            else
            { return "\"" + value + "\""; }
        }
        #endregion
        #endregion

        #region Overridden Methods
        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded javascript resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Asentia.Controls.ClientScript), "Asentia.Controls.Chart.js");
        }
        #endregion

        #region RenderContents
        protected override void RenderContents(HtmlTextWriter writer)
        {
            // canvas
            HtmlGenericControl canvasTag = new HtmlGenericControl("canvas");
            canvasTag.ID = this.ID + "Canvas";
            canvasTag.Attributes.Add("width", this.CanvasWidth.ToString());
            canvasTag.Attributes.Add("height", this.CanvasHeight.ToString());

            // attach controls
            this.Controls.Add(canvasTag);

            // build and register JS block
            this._BuildAndRegisterJSBlock();

            // execute base
            base.RenderContents(writer);
        }
        #endregion
        #endregion
    }
}
