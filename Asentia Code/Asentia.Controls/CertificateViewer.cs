﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Asentia.Common;
using System.IO;
using System.Xml;
using System.Web.UI.HtmlControls;
using System.Reflection;

namespace Asentia.Controls
{
    /// <summary>
    /// Displays a certificate viewer.
    /// </summary>
    [ToolboxData("<{0}:CertificateViewer runat=server></{0}:CertificateViewer>")]
    public class CertificateViewer : WebControl, INamingContainer
    {
        #region Properties
        /// <summary>
        /// ID of the Certificate.
        /// </summary>
        public int CertificateId
        {
            get
            {
                return Convert.ToInt32(ViewState["CertificateViewerCertificateId"]);
            }

            set
            {
                ViewState["CertificateViewerCertificateId"] = value;
            }
        }

        /// <summary>
        /// Background image file name.
        /// </summary>
        public string BackgroundImageFileName
        {
            get
            {
                return ViewState["CertificateViewerBackgroundImageFileName"] as String;
            }

            set
            {
                ViewState["CertificateViewerBackgroundImageFileName"] = value;
            }
        }

        /// <summary>
        /// User object.
        /// </summary>
        public Object UserObject
        {
            get
            {
                return ViewState["CertificateViewerUserObject"];
            }

            set
            {
                ViewState["CertificateViewerUserObject"] = value;
            }
        }

        /// <summary>
        /// User Fields List.
        /// </summary>
        public List<String> UserFieldsList
        {
            get
            {
                return ViewState["CertificateViewerUserFieldsList"] as List<String>;
            }

            set
            {
                ViewState["CertificateViewerUserFieldsList"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the download link will be visible.
        /// </summary>
        public bool DisplayDownloadLink
        {
            get
            {
                if (ViewState["CertificateViewerDisplayDownloadLink"] == null)
                {
                    return true; //Download link will be visible by default.
                }
                else
                {
                    return Convert.ToBoolean(ViewState["CertificateViewerDisplayDownloadLink"]);
                }
            }

            set
            {
                ViewState["CertificateViewerDisplayDownloadLink"] = value;
            }
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// Used to hold certificate viewer html.
        /// </summary>
        private HiddenField _HiddenCertificateViewerHtml;

        /// <summary>
        /// Certificate wrapper control.
        /// </summary>
        private HtmlGenericControl _CertificateWrapper;

        /// <summary>
        /// Certificate viewer html.
        /// </summary>
        private string _CertificateViewerHtml;

        /// <summary>
        /// Certificate data for PDF document.
        /// </summary>
        private string _PdfString;

        /// <summary>
        /// Download link panel.
        /// </summary>
        private Panel _DownloadLinkPanel;

        /// <summary>
        /// Used to download the certificate.
        /// </summary>
        private LinkButton _DownloadLink;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the Certificate Viewer control.
        /// </summary>
        public CertificateViewer()
        {
            _HiddenCertificateViewerHtml = new HiddenField();
            _CertificateWrapper = new HtmlGenericControl("div");
            _DownloadLink = new LinkButton();
            _DownloadLinkPanel = new Panel();
        }
        #endregion

        #region Methods
        #region Bind Data
        /// <summary>
        /// Binds certificate viewer data.
        /// </summary>
        public void BindData()
        {
            _LoadData(); //Load viewer html

            if (UserObject == null || String.IsNullOrWhiteSpace(_CertificateViewerHtml))
            {
                return;
            }

            FieldInfo[] fieldInfoArray = UserObject.GetType().GetFields();

            foreach (String identifier in UserFieldsList)
            {
                foreach (FieldInfo fieldInfo in fieldInfoArray)
                {
                    if (fieldInfo.Name.ToLower() == identifier.ToLower())
                    {
                        object fieldValue = fieldInfo.GetValue(UserObject);
                        string value = fieldValue != null ? fieldValue.ToString() : String.Empty;
                        _CertificateViewerHtml = _CertificateViewerHtml.Replace("[" + identifier + "]", value);
                        break;
                    }
                }
            }

            _HiddenCertificateViewerHtml.Value = _CertificateViewerHtml;
        }
        #endregion
        #endregion

        #region Private Methods
        #region Load Data
        /// <summary>
        /// Loads certificate data.
        /// </summary>
        private void _LoadData()
        {
            _DisplayBackgroundImage();

            //Build file path
            string filePath = SitePathConstants.SITE_CERTIFICATES_ROOT + CertificateId + "/Certificate.xml";

            //Load html of the selected language
            if (File.Exists(HttpContext.Current.Server.MapPath(filePath)))
            {
                XmlDocument document = new XmlDocument();
                document.Load(HttpContext.Current.Server.MapPath(filePath));
                XmlNodeList nodeList = document.SelectNodes("certificateHtml/language");
                XmlNode selectedLanguageNode = null;
                XmlNode defaultLanguageNode = null;
                string defaultLanguage = "en-US";
                foreach (XmlNode languageNode in nodeList)
                {
                    string language = languageNode.Attributes["code"].Value;
                    if (language == AsentiaSessionState.UserCulture)
                    {
                        selectedLanguageNode = languageNode;
                        break;
                    }
                    else if (language == defaultLanguage)
                    {
                        defaultLanguageNode = languageNode;
                        //Don't break here. Continue searching for the selected language node.
                    }
                }

                if (selectedLanguageNode != null)
                {
                    _CertificateViewerHtml = selectedLanguageNode.SelectSingleNode("viewerHtml").InnerText;
                    _PdfString = selectedLanguageNode.SelectSingleNode("pdfString").InnerText;
                }
                else if (defaultLanguageNode != null)
                {
                    _CertificateViewerHtml = defaultLanguageNode.SelectSingleNode("viewerHtml").InnerText;
                    _PdfString = defaultLanguageNode.SelectSingleNode("pdfString").InnerText;
                }
            }
        }
        #endregion

        #region Display Background Image
        /// <summary>
        /// Displays the certificate background image.
        /// </summary>
        private void _DisplayBackgroundImage()
        {
            if (!String.IsNullOrWhiteSpace(BackgroundImageFileName))
            {
                string backgroundImageFilePath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + CertificateId + "/" + BackgroundImageFileName);
                if (File.Exists(backgroundImageFilePath))
                {
                    string siteRootRelativeFilePath = SitePathConstants.SITE_CERTIFICATES_ROOT + CertificateId + "/" + BackgroundImageFileName;
                    _CertificateWrapper.Style.Add("background-image", siteRootRelativeFilePath);
                }
            }
        }
        #endregion

        #region _BuildDownloadPanel
        /// <summary>
        /// Builds a panel for download link.
        /// </summary>
        private void _BuildDownloadPanel()
        {
            _DownloadLinkPanel.ID = this.ID + "_DownloadLinkPanel";
            _DownloadLinkPanel.CssClass = "DownloadPanel";
            
            _DownloadLink.ID = "DownloadLink";
            _DownloadLink.Command += new CommandEventHandler(_DownloadLink_Command);

            // download button image
            Image downloadImage = new Image();
            downloadImage.ID = "DownloadImage";
            downloadImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DOWNLOAD, 
                                                            ImageFiles.EXT_PNG);
            downloadImage.CssClass = "MediumIcon";

            downloadImage.AlternateText = _GlobalResources.DownloadCertificate;
            this._DownloadLink.Controls.Add(downloadImage);

            // upload text
            Literal downloadText = new Literal();
            downloadText.Text = _GlobalResources.DownloadCertificate;
            this._DownloadLink.Controls.Add(downloadText);

            // add download link to panel
            this._DownloadLinkPanel.Controls.Add(this._DownloadLink);

            //Add download link panel
            this.Controls.Add(_DownloadLinkPanel);
        }
        #endregion
        #endregion

        #region Overridden Methods
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.RegisterRequiresControlState(this);
            Controls.Clear();
            base.CreateChildControls();

            //Add hidden field
            _HiddenCertificateViewerHtml.ID = "HiddenCertificateViewerHtml";
            Controls.Add(_HiddenCertificateViewerHtml);

            //Add certificate wrapper
            _CertificateWrapper.ID = "CertificateWrapper";
            Controls.Add(_CertificateWrapper);

            //Build download panel
            _BuildDownloadPanel();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            EnsureChildControls();

            //Register script
            Page.ClientScript.RegisterClientScriptResource(this.GetType(), "Asentia.Controls.jquery-1.10.1.min.js");
            string url = Page.ClientScript.GetWebResourceUrl(this.GetType(), "Asentia.Controls.CertificateViewer.js");
            Page.ClientScript.RegisterClientScriptInclude("certificateViewer", url);
        }

        protected override void RenderContents(HtmlTextWriter output)
        {
            //Render HTML
            output.Write("<div class=\"CertificateViewer\">");
            _HiddenCertificateViewerHtml.RenderControl(output); //Render hidden field
            _CertificateWrapper.InnerHtml = "<div id=\"Certificate\"></div>";
            _CertificateWrapper.RenderControl(output); //Render certificate wrapper

            if (DisplayDownloadLink)
            {
                _DownloadLinkPanel.RenderControl(output); //Render download link panel
            }
            
            output.Write("</div>");
        }
        #endregion

        #region Event Handlers
        private void _DownloadLink_Command(object sender, CommandEventArgs e)
        {
            _LoadData();

            if (!(String.IsNullOrWhiteSpace(_PdfString) || UserObject == null))
            {
                string backgroundImageFilePath = null;
                if (!String.IsNullOrWhiteSpace(BackgroundImageFileName))
                {
                    backgroundImageFilePath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_CERTIFICATES_ROOT + CertificateId + "/" + BackgroundImageFileName);
                }

                string url;
                if (backgroundImageFilePath != null && File.Exists(backgroundImageFilePath))
                {
                    url = backgroundImageFilePath;
                }
                else
                {
                    url = HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_IMAGES_CERT + "default.jpg");
                }

                double pointsPerInch = 72.0;
                double pixelsPerInch = 96.0;

                //Same width as the DIV tag on the Certificate Builder has (converted to points from pixels).
                double imageWidth = 639.75;

                //Calculate proportional height.
                System.Drawing.Image image = System.Drawing.Image.FromFile(url);
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                double originalWidthInPoints = (double)originalWidth * pointsPerInch / pixelsPerInch;
                double originalHeightInPoints = (double)originalHeight * pointsPerInch / pixelsPerInch;
                double ratio = originalHeightInPoints / originalWidthInPoints;
                double imageHeight = ratio * imageWidth;

                using (var pdfGenerator = new PdfGenerator(url, imageWidth, imageHeight, PdfGenerator.PageSize.Letter, PdfGenerator.Orientation.Landscape))
                {
                    FieldInfo[] fieldInfoArray = UserObject.GetType().GetFields();
                    string[] labelsArray = _PdfString.Split(new string[] { " ^ " }, StringSplitOptions.None);

                    foreach (var str in labelsArray)
                    {
                        var arr = str.Split(new string[] { " | " }, StringSplitOptions.None);
                        bool isStaticLabel = Convert.ToBoolean(arr[0]);
                        string label = arr[1];
                        double fontSize = Convert.ToDouble(arr[2]);
                        double x = Convert.ToDouble(arr[3]);
                        double y = Convert.ToDouble(arr[4]);
                        string textAlignment = arr[5].ToLower();
                        double width = Convert.ToDouble(arr[6]);
                        double height = Convert.ToDouble(arr[7]);

                        if (isStaticLabel)
                        {
                            label = label.Replace("[^]", "^").Replace("[|]", "|"); //Replace the marked separators
                        }
                        else
                        {
                            foreach (FieldInfo fieldInfo in fieldInfoArray)
                            {
                                if (fieldInfo.Name.ToLower() == label.ToLower())
                                {
                                    object fieldValue = fieldInfo.GetValue(UserObject);
                                    string value = fieldValue != null ? fieldValue.ToString() : String.Empty;
                                    label = value;
                                    break;
                                }
                            }
                        }

                        PdfGenerator.Alignment alignment;
                        switch (textAlignment)
                        {
                            case "center":
                                alignment = PdfGenerator.Alignment.Center;
                                break;
                            case "right":
                                alignment = PdfGenerator.Alignment.Right;
                                break;
                            default:
                                alignment = PdfGenerator.Alignment.Left;
                                break;
                        }

                        //Conversion from pixels to points
                        fontSize = fontSize * pointsPerInch / pixelsPerInch;
                        x = x * pointsPerInch / pixelsPerInch;
                        y = y * pointsPerInch / pixelsPerInch;
                        width = width * pointsPerInch / pixelsPerInch;
                        height = height * pointsPerInch / pixelsPerInch;

                        //Write string to the document
                        pdfGenerator.WriteString(label, x, y, fontSize, alignment, width, fontSize + 12);
                    }

                    //Send certificate as an attachment.
                    MemoryStream stream = pdfGenerator.GetStream();
                    Page.Response.Clear();
                    Page.Response.ContentType = "application/pdf";
                    Page.Response.AddHeader("Content-Disposition", "attachment; filename=Certificate.pdf");
                    Page.Response.AddHeader("content-length", stream.Length.ToString());
                    Page.Response.BinaryWrite(stream.ToArray());
                    Page.Response.Flush();
                    stream.Close();
                    Page.Response.End();
                }
            }
        }
        #endregion
    }
}