﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Asentia.LRS")]
[assembly: AssemblyDescription("Libraries for the Asentia LRS.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ICS Learning Group")]
[assembly: AssemblyProduct("Asentia.LRS")]
[assembly: AssemblyCopyright("Copyright © ICS Learning Group 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a31e693c-a42a-470e-8e94-1108c395a911")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.X.*")]
[assembly: AssemblyVersion("1.16.*")]

//Date Picker
[assembly: WebResource("Asentia.Controls.DatePicker.js", "text/javascript")]

//Report UI
[assembly: WebResource("Asentia.LRS.Controls.ReportForm.js", "text/javascript")]
[assembly: WebResource("Asentia.LRS.Pages.Reporting.Analytics.Modify.js", "text/javascript")]
[assembly: WebResource("Asentia.LRS.Pages.Reporting.Reports.Modify.js", "text/javascript")]
