﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LRS.Library;
using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;


namespace Asentia.LRS.Pages.Reporting.Analytics
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel NewAnalyticInstructionsPanel;
        public Panel NewAnalyticContainer;
        public Panel MySavedAnalyticsInstructionsPanel;
        public Panel MySavedAnalyticsContainer;
        public AnalyticDataSet AnalyticDataSetObject = new AnalyticDataSet();
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup GridConfirmAction = new ModalPopup();
        public Panel PublicAnalyticsInstructionsPanel;
        public Panel PublicAnalyticsContainer;

        #endregion

        #region Private Properties
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Analytics));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.Reporting, _GlobalResources.Analytics, ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS,
                                                                                                                   ImageFiles.EXT_PNG));

            this.InitializeAdminMenu();

            // format a page information panel with new Analytic instructions
            this.FormatPageInformationPanel(this.NewAnalyticInstructionsPanel, _GlobalResources.AvailableDatasetsAreListedBelowAnalytics, true);

            // build new Analytic container
            this._BuildNewAnalyticContainer();

            // format a page information panel with new Analytic instructions
            this.FormatPageInformationPanel(this.MySavedAnalyticsInstructionsPanel, _GlobalResources.ReportsThatYouHaveConfiguredAreListedBelow, true);

            // build my saved Analytic container
            this._BuildMySavedAnalyticsContainer();

            // format a page information panel with new Analytic instructions
            this.FormatPageInformationPanel(this.PublicAnalyticsInstructionsPanel, _GlobalResources.AnalyticsThatOthersHaveConfiguredAndMarkedAsPublicAreListedBelow, true);

            // build my saved Analytic container
            this._BuildPublicAnalyticsContainer();

            //display save message if any
            if (Session["CreateAnalyticSuccess"] != null)
            {
                this.DisplayFeedback(_GlobalResources.AnalyticHasBeenSavedSuccessfully, false);
                Session.Remove("CreateAnalyticSuccess");
            }

        }
        #endregion

        #region _BuildNewAnalyticContainer
        /// <summary>
        /// Builds the container that contains the list of the datasets.
        /// </summary>
        private void _BuildNewAnalyticContainer()
        {
            DataTable dtDataSet = new DataTable();

            dtDataSet = AnalyticDataSetObject.GetAnalyticDataSetListTable();

            foreach (DataRow row in dtDataSet.Rows)
            {
                Panel dataSetPanel = new Panel();
                dataSetPanel.CssClass = "DataSetPanelSubItem";

                Image dataSetIcon = new Image();
                dataSetIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM_DATASETS, ImageFiles.EXT_PNG);
                dataSetIcon.CssClass = "SmallIcon";

                // Add dataset name with an spacer prefixed.
                Literal dataSetPanelHTML = new Literal();
                dataSetPanelHTML.Text = "&#160;" + row["name"].ToString().ToUpper();

                // attach the elements to the dataSet Panel
                dataSetPanel.Controls.Add(dataSetIcon);
                dataSetPanel.Controls.Add(dataSetPanelHTML);

                // Create dataSetContainer to hold informatino, existing Analytic, and new Analytic link
                Panel dataSetContainer = new Panel();
                dataSetContainer.CssClass = "DataSetPanel";

                // format an information panel with dataSet
                Panel dataSetPanelInstruction = new Panel();
                this.FormatPageInformationPanel(dataSetContainer, row["description"].ToString(), false);

                // list available user saved Analytics under this dataset
                Analytic datasetAnalytic = new Analytic();
                DataTable dtSavedAnalytic = new DataTable();
                Panel dataSetAnalyticPanel = new Panel();
                dataSetAnalyticPanel.CssClass = "DataSetPanelSubItem";
                dtSavedAnalytic = datasetAnalytic.GetTitles(Convert.ToInt32(row["idDataset"]), AsentiaSessionState.IdSiteUser, 0);

                foreach (DataRow dataSetAnalyticRow in dtSavedAnalytic.Rows)
                {
                    // format existing Analytic panel with dataSet
                    Panel AddNewAnalyticPanel = new Panel();
                    AddNewAnalyticPanel.CssClass = "DataSetPanelSubItem";
                    HyperLink AnalyticImageLink = new HyperLink();
                    AnalyticImageLink.CssClass = "ImageLink";
                    AnalyticImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS,
                                                                        ImageFiles.EXT_PNG);
                    AnalyticImageLink.NavigateUrl = "Modify.aspx?idAnalytic=" + dataSetAnalyticRow["idAnalytic"].ToString();

                    HyperLink AnalyticTitleLink = new HyperLink();
                    AnalyticTitleLink.Text = dataSetAnalyticRow["title"].ToString();
                    AnalyticTitleLink.NavigateUrl = "Modify.aspx?idAnalytic=" + dataSetAnalyticRow["idAnalytic"].ToString();
                    AddNewAnalyticPanel.Controls.Add(AnalyticImageLink);
                    AddNewAnalyticPanel.Controls.Add(AnalyticTitleLink);
                    dataSetAnalyticPanel.Controls.Add(AddNewAnalyticPanel);
                }

                Panel dataSetAddNewAnalyticPanel = new Panel();
                dataSetAddNewAnalyticPanel.CssClass = "DataSetPanelSubItem";
                // format adding Analytic panel with dataSet
                HyperLink AnalyticAddImageLink = new HyperLink();
                AnalyticAddImageLink.CssClass = "ImageLink";
                AnalyticAddImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD,
                                                                       ImageFiles.EXT_PNG);
                AnalyticAddImageLink.NavigateUrl = "Modify.aspx?idDataset=" + row["idDataset"].ToString();

                HyperLink AnalyticAddLink = new HyperLink();
                AnalyticAddLink.Text = _GlobalResources.NewAnalytic;
                AnalyticAddLink.NavigateUrl = "Modify.aspx?idDataset=" + row["idDataset"].ToString();

                dataSetAddNewAnalyticPanel.Controls.Add(AnalyticAddImageLink);
                dataSetAddNewAnalyticPanel.Controls.Add(AnalyticAddLink);

                // attach the elements to the addAnalytic Panel
                if (dtSavedAnalytic.Rows.Count > 0)
                    dataSetContainer.Controls.Add(dataSetAnalyticPanel);
                dataSetContainer.Controls.Add(dataSetAddNewAnalyticPanel);

                // attach panels to container    
                dataSetPanel.Controls.Add(dataSetContainer);
                this.NewAnalyticContainer.Controls.Add(dataSetPanel);
            }
        }
        #endregion

        #region _BuildMySavedAnalyticsContainer
        /// <summary>
        /// Builds the container that contains the list of the personal saved Analytics.
        /// </summary>
        private void _BuildMySavedAnalyticsContainer()
        {
            Analytic mySavedAnalytic = new Analytic();
            DataTable dtSavedAnalytic = new DataTable();
            dtSavedAnalytic = mySavedAnalytic.GetTitles(0, AsentiaSessionState.IdSiteUser, 0);

            foreach (DataRow row in dtSavedAnalytic.Rows)
            {
                Panel plAnalytic = new Panel();
                string deleteMessage = _GlobalResources.AreYouSureYouWantToDeleteAnalyticX.Replace("##AnalyticName##", row["title"].ToString()).Replace("'", "");
                plAnalytic.CssClass = "DataSetPanelSubItem";
                LinkButton AnalyticDeleteButton = new LinkButton();
                AnalyticDeleteButton.ID = "DeleteIcon" + row["idAnalytic"].ToString();

                // delete button image
                Image deleteImage = new Image();
                deleteImage.CssClass = "ImageLink";
                AnalyticDeleteButton.Text = row["title"].ToString();
                deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                              ImageFiles.EXT_PNG);
                deleteImage.CssClass = "MediumIcon";

                AnalyticDeleteButton.Controls.Add(deleteImage);
                AnalyticDeleteButton.OnClientClick = "return confirm('" + deleteMessage + "');";
                AnalyticDeleteButton.Command += new CommandEventHandler(this._DeleteButton_Command);
                plAnalytic.Controls.Add(AnalyticDeleteButton);

                HyperLink AnalyticTitle = new HyperLink();
                AnalyticTitle.NavigateUrl = "Modify.aspx?idAnalytic=" + row["idAnalytic"].ToString();
                AnalyticTitle.Text = row["title"].ToString();
                AnalyticTitle.Attributes.Add("style", "margin: 2px 2px 2px 10px;");
                plAnalytic.Controls.Add(AnalyticTitle);

                Literal dataSetName = new Literal();
                dataSetName.Text = " (" + _GlobalResources.Dataset + " : " + row["dataSet"].ToString() + ")";
                plAnalytic.Controls.Add(dataSetName);

                this.MySavedAnalyticsContainer.Controls.Add(plAnalytic);
            }
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Handles the "Delete" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            LinkButton btn = (LinkButton)(sender);
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(int));
                dt.Rows.Add(Convert.ToInt32(btn.ID.Replace("DeleteIcon", "")));
                Analytic.Delete(dt);
                this.MySavedAnalyticsContainer.Controls.Clear();
                _BuildMySavedAnalyticsContainer();
                this.NewAnalyticContainer.Controls.Clear();
                _BuildNewAnalyticContainer();
                this.DisplayFeedback(_GlobalResources.ReportXHasBeenDeletedSuccessfullyAnalytics.Replace("##AnalyticName##", btn.Text), false);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.ReportXWasNotDeletedAnalytics.Replace("##AnalyticName##", btn.Text), true);
            }
        }
        #endregion

        #region _BuildPublicAnalyticsContainer
        /// <summary>
        /// Builds the container that contains the list of the public Analytics.
        /// </summary>
        private void _BuildPublicAnalyticsContainer()
        {
            Analytic publicAnalytic = new Analytic();
            DataTable dtPublicAnalytic = new DataTable();
            dtPublicAnalytic = publicAnalytic.GetTitles(0, 0, 1);

            foreach (DataRow row in dtPublicAnalytic.Rows)
            {
                Panel plAnalytic = new Panel();
                string deleteMessage = _GlobalResources.AreYouSureYouWantToDeleteAnalyticX.Replace("##AnalyticName##", row["title"].ToString()).Replace("'", "");
                plAnalytic.CssClass = "DataSetPanelSubItem";

                HyperLink AnalyticImageLink = new HyperLink();
                AnalyticImageLink.CssClass = "ImageLink";
                AnalyticImageLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS,
                                                                    ImageFiles.EXT_PNG);
                AnalyticImageLink.NavigateUrl = "Modify.aspx?idAnalytic=" + row["idAnalytic"].ToString();
                plAnalytic.Controls.Add(AnalyticImageLink);

                HyperLink AnalyticTitle = new HyperLink();
                AnalyticTitle.NavigateUrl = "Modify.aspx?idAnalytic=" + row["idAnalytic"].ToString();
                AnalyticTitle.Text = row["title"].ToString();
                AnalyticTitle.Attributes.Add("style", "margin: 2px 2px 2px 10px;");
                plAnalytic.Controls.Add(AnalyticTitle);

                Literal dataSetName = new Literal();
                dataSetName.Text = " (" + _GlobalResources.Dataset + " : " + row["dataSet"].ToString() + ", ";
                dataSetName.Text += _GlobalResources.Owner + " : " + row["ownerName"].ToString() + ")";
                plAnalytic.Controls.Add(dataSetName);

                this.PublicAnalyticsContainer.Controls.Add(plAnalytic);
            }
        }
        #endregion

    }
}
