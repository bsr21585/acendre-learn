﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LRS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Asentia.LRS.Pages.Reporting.Analytics
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel PageInstructionsPanel;
        public Panel AnalyticTitlePanel;
        public Panel AnalyticFormContainer;
        public Panel ActionsPanel;
        #endregion

        #region Private Properties
        private Analytic _AnalyticObject;
        private AnalyticDataSet _DataSet;
        private RadioButtonList _RadioButtonListIsPublic = new RadioButtonList();
        private Button _SaveButton = new Button();
        private Button _CancelButton = new Button();
        private Button _RunButton = new Button();
        private Button _UpdateButton = new Button();
        private DatePicker _StartDateTextBox;
        private DatePicker _EndDateTextBox;
        private DropDownList _FrequencySelectList;
        private CheckBoxList _ExcludeDaySelectList;
        private RadioButtonList _ChartTypeSelectList;
        private RadioButtonList _ActivityList;
        private TextBox _AnalyticTitleTextBox;
        private bool _CreateAnalytic;
        private Panel _TabPropertyContentPanel;
        private Panel _TabFiltersContentPanel;
        private List<AnalyticDataSet.DatasetFieldProperties> _LabelDataset;
        private Panel _FrequencyFieldContainer;
        private Panel _ExcludeOptionFieldContainer;
        Dictionary<string, string> _FilterConditions = new Dictionary<string, string>();

        private DynamicListBox _SelectCertificateTitle;
        private ModalPopup _SelectCertificateTitleForAnalytic;
        private Panel _AnalyticCertificatesListContainer;
        private HiddenField _SelectedAnalyticCertificates;
        private Panel _CertificatesPanel;
        private DataTable _AnalyticCertificatesForSelectList;

        private RadioButtonList _UserOrGroupRadioButton;
        private RadioButtonList _OrderRadioButton;
        private RadioButtonList _OrderColumnRadioButton;
        private Panel _UserOrGroupOptionFieldContainer;
        private Panel _OrderOptionFieldContainer;
        private Panel _OrderColumnOptionFieldContainer;

        private DataTable _AnalyticCourses;
        private DataTable _EligibleAnalyticCoursesForSelectList;
        private DynamicListBox _SelectEligibleAnalyticCourses;
        private HiddenField _SelectedAnalyticCourses;
        private Panel _AnalyticCoursesListContainer;
        private ModalPopup _SelectCoursesForAnalytic;
        private Panel _CoursesPanel;

        private bool _IsDateFilterRequired = false;
        private bool _IsExcludeDayFilterRequired = false;
        private bool _IsFrequencyFilterRequired = false;
        private bool _IsCourseListRequired = false;
        private bool _IsActivityRequired = false;

        private bool _IsCertificateTitleRequired = false;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            // register the embedded javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LRS.Pages.Reporting.Analytics.Modify.js");

            // build global JS variables
            StringBuilder globalJS = new StringBuilder();
            globalJS.AppendLine("DeleteImagePath = \"" + ImageFiles.GetIconPath(ImageFiles.ICON_DELETE, ImageFiles.EXT_PNG) + "\";");
            globalJS.AppendLine("MaxCoursesSelection =  " + Convert.ToInt32(this._FilterConditions["MaxCoursesSelection"]));
            globalJS.AppendLine("MaxCertificatesSelection =  " + Convert.ToInt32(this._FilterConditions["MaxCertificatesSelection"]));


            csm.RegisterClientScriptBlock(typeof(Modify), "GlobalJS", globalJS.ToString(), true);
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/reporting/analytics/Modify.css");

            // get the Analytic object
            this._GetAnalyticObject();

            //Builds the breadcrumb and page title.
            this._BuildBreadcrumbAndPageTitle();

            this.InitializeAdminMenu();

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.UseTheInterfaceBelowToSelectFiltersToApplyToThisAnalytic, true);

            this._GetAllRequirdFildsStatusFromXml();

            // build the Analytic form
            this._BuildAnalyticForm();

            // build the form actions panel
            this._BuildActionsPanel();
            this._GetAnalyticDatasetObjectAndPopulateSelectListInputControls();
            if (!IsPostBack)
            {

                this._PopulateAnalyticformInputControls();
            }

        }
        #endregion

        #region _GetAnalyticObject
        /// <summary>
        /// Gets a Analytic object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetAnalyticObject()
        {
            int idAnalytic = _GetIDFromLoadingPage("idAnalytic");
            int idDataset = _GetIDFromLoadingPage("idDataset");

            try
            {
                if (idAnalytic > 0)
                {
                    this._AnalyticObject = new Analytic(idAnalytic);
                    _CreateAnalytic = false;
                }
                else if (idDataset > 0)
                {
                    this._AnalyticObject = new Analytic();
                    this._AnalyticObject.IdDataset = idDataset;
                    _CreateAnalytic = true;
                }

            }
            catch
            {
                // bubble the exception up
                throw;
            }

        }
        #endregion

        #region _GetAnalyticDatasetObjectAndPopulateSelectListInputControls
        /// <summary>
        /// Gets a Analytic Dataset object based on Analytic Object
        /// Then populate select list input controls accordingly
        /// </summary>
        private void _GetAnalyticDatasetObjectAndPopulateSelectListInputControls()
        {
            if (this._AnalyticObject != null)
            {
                ListItem item;

                this._DataSet = new AnalyticDataSet();
                this._FrequencySelectList.Items.Clear();
                this._ExcludeDaySelectList.Items.Clear();
                this._ChartTypeSelectList.Items.Clear();
                this._ActivityList.Items.Clear();
                if (this._LabelDataset != null)
                {
                    foreach (AnalyticDataSet.DatasetFieldProperties li in _LabelDataset)
                    {
                        if (li.IdentiFier.ToLower() == "frequency")
                        {
                            item = new ListItem(_GlobalResources.ResourceManager.GetString(li.ResourceString.Trim()), li.Value);
                            this._FrequencySelectList.Items.Add(item);
                        }
                        if (li.IdentiFier.ToLower() == "excludeday")
                        {
                            item = new ListItem(_GlobalResources.ResourceManager.GetString(li.ResourceString.Trim()), li.Value);
                            this._ExcludeDaySelectList.Items.Add(item);

                        }
                        if (li.IdentiFier.ToLower() == "chart")
                        {
                            item = new ListItem(_GlobalResources.ResourceManager.GetString(li.ResourceString.Trim()), li.Value);
                            this._ChartTypeSelectList.Items.Add(item);
                        }
                        if (li.IdentiFier.ToLower() == "coursesselectionlist")
                        {
                            this._IsCourseListRequired = true;
                        }
                        if (li.IdentiFier.ToLower() == "certificatetitle")
                        {
                            this._IsCertificateTitleRequired = true;
                        }
                        if (li.IdentiFier.ToLower() == "activity")
                        {
                            item = new ListItem(_GlobalResources.ResourceManager.GetString(li.ResourceString.Trim()), li.Value);
                            this._ActivityList.Items.Add(item);
                        }
                        if (li.IdentiFier.ToLower() == "datefilterrequired")
                        {
                            this._IsDateFilterRequired = true;
                        }
                    }
                }

                if (this._ChartTypeSelectList.Items.Count > 0)
                { this._ChartTypeSelectList.SelectedIndex = 0; }

                if (this._ActivityList.Items.Count > 0)
                { this._ActivityList.SelectedIndex = 0; }

                //hide and show input controls
                this._EnableAndDisableControls();

            }
        }
        #endregion

        #region _GetIDFromLoadingPage
        /// <summary>
        /// Gets an ID from loading a page
        /// </summary>
        private int _GetIDFromLoadingPage(string ID)
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt(ID, 0);
            int vsId = this.ViewStateInt(this.ViewState, ID, 0);
            int id = 0;

            if (qsId > 0 || vsId > 0)
            {
                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }
            }
            return id;
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Analytics, "/reporting/analytics"));
            breadCrumbLinks.Add(new BreadcrumbLink(this._CreateAnalytic ? _GlobalResources.Add : _GlobalResources.Modify));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            AnalyticDataSet AnalyticDataSetObject = new AnalyticDataSet();
            string dataSetName = AnalyticDataSetObject.GetAnalyticDataSetListTable().Select("idDataset = " + this._AnalyticObject.IdDataset.ToString())[0]["name"].ToString();

            this.BuildPageTitle(PageCategoryForTitle.Reporting,
                                this._CreateAnalytic ? _GlobalResources.Analytic + ": " + dataSetName : _GlobalResources.Analytic + ": " + this._AnalyticObject.Title,
                                ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS,
                                                       ImageFiles.EXT_PNG));

            Enum.GetName(typeof(EnumDataSet), this._AnalyticObject.IdDataset);
        }
        #endregion

        #region _BuildAnalyticForm
        /// <summary>
        /// Builds the Analytic form.
        /// </summary>
        private void _BuildAnalyticForm()
        {
            // clear controls from container
            this.AnalyticFormContainer.Controls.Clear();

            // build the analytics  form tabs
            this._BuildAnalyticPropertiesFormTabs();

            //build input controls
            this._AnalyticPropertiesFormInputControls();

        }
        #endregion

        #region _BuildAnalyticPropertiesFormTabs
        /// <summary>
        /// build analytics form tabs
        /// </summary>
        private void _BuildAnalyticPropertiesFormTabs()
        {
            Panel analyticPropertiesTabsPanel = new Panel();
            analyticPropertiesTabsPanel.ID = "Analytic_TabsPanel";

            // UL for tabs
            HtmlGenericControl analyticPropertiesTabsUL = new HtmlGenericControl("ul");
            HtmlGenericControl AnalyticFilterTabLI = new HtmlGenericControl("li");
            analyticPropertiesTabsUL.ID = "Analytic_TabsUL";
            analyticPropertiesTabsUL.Attributes.Add("class", "TabbedList Reporting");

            // ACTIVITY TAB I
            //it will not be visible for login activity analytics
            HtmlGenericControl analyticActivityTabLI = new HtmlGenericControl("li");
            if (this._IsActivityRequired)
            {
                analyticActivityTabLI.ID = "Analytic_" + "Activity" + "_TabLI";
                analyticActivityTabLI.Attributes.Add("onclick", "Helper.ToggleTab('Activity', 'Analytic');");
                analyticActivityTabLI.Attributes.Add("class", "TabbedListLI TabbedListLIOn");

                HyperLink analyticActivityTabLink = new HyperLink();
                analyticActivityTabLink.ID = "Analytic_" + "Activity" + "_TabLink";
                analyticActivityTabLink.CssClass = "TabLink";
                analyticActivityTabLink.NavigateUrl = "javascript:void(0);";
                analyticActivityTabLink.Text = _GlobalResources.Activities;

                // attach controls
                analyticActivityTabLI.Controls.Add(analyticActivityTabLink);
                analyticPropertiesTabsUL.Controls.Add(analyticActivityTabLI);

                AnalyticFilterTabLI.Attributes.Add("class", "TabbedListLI");
            }
            else
            {
                analyticActivityTabLI.Attributes.Add("class", "TabbedListLI ");
                AnalyticFilterTabLI.Attributes.Add("class", "TabbedListLI TabbedListLIOn");
            }


            // OPTIONS TAB II
            // "Filter" is the default tab, so this is "on" on page load.

            AnalyticFilterTabLI.ID = "Analytic_" + "Filter" + "_TabLI";
            AnalyticFilterTabLI.Attributes.Add("onclick", "Helper.ToggleTab('Filter', 'Analytic');");


            HyperLink analyticFilterTabLink = new HyperLink();
            analyticFilterTabLink.ID = "Analytic_" + "Filter" + "_TabLink";
            analyticFilterTabLink.CssClass = "TabLink";
            analyticFilterTabLink.NavigateUrl = "javascript:void(0);";
            analyticFilterTabLink.Text = _GlobalResources.Filters;

            // attach controls
            AnalyticFilterTabLI.Controls.Add(analyticFilterTabLink);
            analyticPropertiesTabsUL.Controls.Add(AnalyticFilterTabLI);

            // OPTIONS TAB III
            HtmlGenericControl analyticPropertiesTabLI = new HtmlGenericControl("li");
            analyticPropertiesTabLI.ID = "Analytic_" + "Property" + "_TabLI";
            analyticPropertiesTabLI.Attributes.Add("onclick", "Helper.ToggleTab('Property', 'Analytic');");
            analyticPropertiesTabLI.Attributes.Add("class", "TabbedListLI");

            HyperLink propertiesTabLink = new HyperLink();
            propertiesTabLink.ID = "Analytic_" + "Properties" + "_TabLink";
            propertiesTabLink.CssClass = "TabLink";
            propertiesTabLink.NavigateUrl = "javascript:void(0);";
            propertiesTabLink.Text = _GlobalResources.Properties;

            // attach controls
            analyticPropertiesTabLI.Controls.Add(propertiesTabLink);
            analyticPropertiesTabsUL.Controls.Add(analyticPropertiesTabLI);

            // OPTIONS TAB IV
            HtmlGenericControl analyticChartTypesTabLI = new HtmlGenericControl("li");
            analyticChartTypesTabLI.ID = "Analytic_" + "Charts" + "_TabLI";
            analyticChartTypesTabLI.Attributes.Add("onclick", "Helper.ToggleTab('Charts', 'Analytic');");
            analyticChartTypesTabLI.Attributes.Add("class", "TabbedListLI");

            HyperLink chartTypesTabLink = new HyperLink();
            chartTypesTabLink.ID = "Analytic_" + "Charts" + "_TabLink";
            chartTypesTabLink.CssClass = "TabLink";
            chartTypesTabLink.NavigateUrl = "javascript:void(0);";
            chartTypesTabLink.Text = _GlobalResources.Chart;

            // attach controls
            analyticChartTypesTabLI.Controls.Add(chartTypesTabLink);
            analyticPropertiesTabsUL.Controls.Add(analyticChartTypesTabLI);



            // END OPTIONS TAB

            // attach tab ul control
            analyticPropertiesTabsPanel.Controls.Add(analyticPropertiesTabsUL);
            this.AnalyticFormContainer.Controls.Add(analyticPropertiesTabsPanel);
        }
        #endregion

        #region _AnalyticPropertiesFormInputControls
        private void _AnalyticPropertiesFormInputControls()
        {
            //filters tab contents
            this._BuildFiltersTabContent();

            //property tab contents
            this._BuildPropertyTabContent();

            #region Tab Chart Contents
            Panel analyticFormChartSelectionPanel = new Panel();
            analyticFormChartSelectionPanel.ID = "Analytic_" + "Charts_TabPanel";
            analyticFormChartSelectionPanel.CssClass = "TabContentPanel";
            analyticFormChartSelectionPanel.Attributes.Add("style", "display: none;");

            #region ChartType

            // chart type select  container
            Panel chartFieldContainer = new Panel();
            chartFieldContainer.ID = "Chart_Container";
            chartFieldContainer.CssClass = "UserFieldContainer UserPanel";

            //chart type select  input
            Panel chartFieldInputContainer = new Panel();
            chartFieldInputContainer.ID = "Chart_InputContainer";
            chartFieldInputContainer.CssClass = "FormFieldInputContainer";

            this._ChartTypeSelectList = new RadioButtonList();
            this._ChartTypeSelectList.ID = "Chart_Field";
            //EnumToListBox(typeof(ChartTypeEnum), this._ChartTypeSelectList);
            this._ChartTypeSelectList.SelectedIndex = 0;
            this._ChartTypeSelectList.RepeatDirection = RepeatDirection.Horizontal;
            chartFieldInputContainer.Controls.Add(this._ChartTypeSelectList);

            // chart type select  label
            Panel chartFieldLabelContainer = new Panel();
            chartFieldLabelContainer.ID = "Chart_LabelContainer";
            chartFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label chartFieldLabel = new Label();
            chartFieldLabel.Text = _GlobalResources.ChartType + " : ";
            chartFieldLabel.AssociatedControlID = this._ChartTypeSelectList.ID;

            chartFieldLabelContainer.Controls.Add(chartFieldLabel);

            //chart type select error panel
            Panel chartFieldErrorContainer = new Panel();
            chartFieldErrorContainer.ID = "Chart_ErrorContainer";
            chartFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to container
            chartFieldContainer.Controls.Add(chartFieldLabelContainer);
            chartFieldContainer.Controls.Add(chartFieldErrorContainer);
            chartFieldContainer.Controls.Add(chartFieldInputContainer);

            analyticFormChartSelectionPanel.Controls.Add(chartFieldContainer);
            #endregion
            #endregion

            #region Tab Activity Controls
            Panel analyticFormActivitySelectionPanel = new Panel();
            analyticFormActivitySelectionPanel.ID = "Analytic_" + "Activity_TabPanel";
            analyticFormActivitySelectionPanel.CssClass = "TabContentPanel";
            if (this._IsActivityRequired)
            {
                //it will be first tab
                analyticFormActivitySelectionPanel.Attributes.Add("style", "display: block;");
            }
            else
            {
                //it will be second tab
                analyticFormActivitySelectionPanel.Attributes.Add("style", "display: none;");

            }

            // activity select  container
            Panel activityFieldContainer = new Panel();
            activityFieldContainer.ID = "Activity_Container";
            activityFieldContainer.CssClass = "UserFieldContainer UserPanel";

            //activity select  input
            Panel activityFieldInputContainer = new Panel();
            activityFieldInputContainer.ID = "Activity_InputContainer";
            activityFieldInputContainer.CssClass = "FormFieldInputContainer";

            this._ActivityList = new RadioButtonList();
            this._ActivityList.ID = "Activity_Field";
            this._ActivityList.RepeatDirection = RepeatDirection.Vertical;
            this._ActivityList.Attributes.Add("onClick", "showAndHideFrequencyListAccordingToActivity();");
            activityFieldInputContainer.Controls.Add(this._ActivityList);

            // activity select  label
            Panel activityFieldLabelContainer = new Panel();
            activityFieldLabelContainer.ID = "Activity_LabelContainer";
            activityFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label activityFieldLabel = new Label();
            activityFieldLabel.Text = _GlobalResources.Activity + " : ";
            activityFieldLabel.AssociatedControlID = this._ActivityList.ID;

            activityFieldLabelContainer.Controls.Add(activityFieldLabel);

            //activity select error panel
            Panel activityFieldErrorContainer = new Panel();
            activityFieldErrorContainer.ID = "Activity_ErrorContainer";
            activityFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to container
            activityFieldContainer.Controls.Add(activityFieldLabelContainer);
            activityFieldContainer.Controls.Add(activityFieldErrorContainer);
            activityFieldContainer.Controls.Add(activityFieldInputContainer);

            analyticFormActivitySelectionPanel.Controls.Add(activityFieldContainer);
            #endregion

            //adds controls to tabs
            this.AnalyticFormContainer.Controls.Add(this._TabFiltersContentPanel);
            this.AnalyticFormContainer.Controls.Add(this._TabPropertyContentPanel);
            this.AnalyticFormContainer.Controls.Add(analyticFormChartSelectionPanel);
            this.AnalyticFormContainer.Controls.Add(analyticFormActivitySelectionPanel);

        }
        #endregion

        #region _BuildFiltersTabContent
        /// <summary>
        /// _BuildFiltersTabContent
        /// </summary>
        private void _BuildFiltersTabContent()
        {
            this._TabFiltersContentPanel = new Panel();
            this._TabFiltersContentPanel.ID = "Analytic_" + "Filter_TabPanel";
            this._TabFiltersContentPanel.CssClass = "TabContentPanel";
            if (this._IsActivityRequired)
            {
                //it will be second tab
                this._TabFiltersContentPanel.Attributes.Add("style", "display: none;");
            }
            else
            {
                //it will be first tab
                this._TabFiltersContentPanel.Attributes.Add("style", "display: block;");
            }

            if (this._IsDateFilterRequired)
            {
                #region Start Date
                // start date container
                Panel startDateFieldContainer = new Panel();
                startDateFieldContainer.ID = "StartDate_Container";
                startDateFieldContainer.CssClass = "UserFieldContainer UserPanel";

                //start date input
                Panel startDateFieldInputContainer = new Panel();
                startDateFieldInputContainer.ID = "StartDate_InputContainer";
                startDateFieldInputContainer.CssClass = "FormFieldInputContainer";

                this._StartDateTextBox = new DatePicker("DTStartID", false, false, false);
                this._StartDateTextBox.ID = "StartDate_Field";

                startDateFieldInputContainer.Controls.Add(this._StartDateTextBox);

                // start date label
                Panel startDateFieldLabelContainer = new Panel();
                startDateFieldLabelContainer.ID = "StartDate_LabelContainer";
                startDateFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                Label startDateFieldLabel = new Label();
                startDateFieldLabel.Text = _GlobalResources.StartDate + ": ";
                startDateFieldLabel.AssociatedControlID = this._StartDateTextBox.ID;

                Label requiredAsteriskStartDate = new Label();
                requiredAsteriskStartDate.Text = "* ";
                requiredAsteriskStartDate.CssClass = "RequiredAsterisk";
                startDateFieldLabelContainer.Controls.Add(requiredAsteriskStartDate);
                startDateFieldLabelContainer.Controls.Add(startDateFieldLabel);

                // start date error panel
                Panel startDateFieldErrorContainer = new Panel();
                startDateFieldErrorContainer.ID = "StartDate_ErrorContainer";
                startDateFieldErrorContainer.CssClass = "FormFieldErrorContainer";

                //start date and end date optinal error panel
                Panel startEndDateFieldErrorContainer = new Panel();
                startEndDateFieldErrorContainer.ID = "StartEndDate_ErrorContainer";
                startEndDateFieldErrorContainer.CssClass = "FormFieldErrorContainer";

                // add controls to container
                startDateFieldContainer.Controls.Add(startEndDateFieldErrorContainer);
                startDateFieldContainer.Controls.Add(startDateFieldLabelContainer);
                startDateFieldContainer.Controls.Add(startDateFieldErrorContainer);
                startDateFieldContainer.Controls.Add(startDateFieldInputContainer);

                this._TabFiltersContentPanel.Controls.Add(startDateFieldContainer);
                #endregion

                #region End Date
                // end date container
                Panel endDateFieldContainer = new Panel();
                endDateFieldContainer.ID = "EndDate_Container";
                endDateFieldContainer.CssClass = "FormFieldContainer";

                //end date input
                Panel endDateFieldInputContainer = new Panel();
                endDateFieldInputContainer.ID = "EndDate_InputContainer";
                endDateFieldInputContainer.CssClass = "FormFieldInputContainer";

                this._EndDateTextBox = new DatePicker("DTEndID", false, false, false);
                this._EndDateTextBox.ID = "EndDate_Field";

                endDateFieldInputContainer.Controls.Add(this._EndDateTextBox);

                // end date label
                Panel endDateFieldLabelContainer = new Panel();
                endDateFieldLabelContainer.ID = "EndDate_LabelContainer";
                endDateFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                Label endDateFieldLabel = new Label();
                endDateFieldLabel.Text = _GlobalResources.EndDate + ": ";
                endDateFieldLabel.AssociatedControlID = this._EndDateTextBox.ID;

                Label requiredAsteriskEndDate = new Label();
                requiredAsteriskEndDate.Text = "* ";
                requiredAsteriskEndDate.CssClass = "RequiredAsterisk";
                endDateFieldLabelContainer.Controls.Add(requiredAsteriskEndDate);
                endDateFieldLabelContainer.Controls.Add(endDateFieldLabel);

                // end date error panel
                Panel endtDateFieldErrorContainer = new Panel();
                endtDateFieldErrorContainer.ID = "EndDate_ErrorContainer";
                endtDateFieldErrorContainer.CssClass = "FormFieldErrorContainer";

                // add controls to container
                endDateFieldContainer.Controls.Add(endDateFieldLabelContainer);
                endDateFieldContainer.Controls.Add(endtDateFieldErrorContainer);
                endDateFieldContainer.Controls.Add(endDateFieldInputContainer);

                this._TabFiltersContentPanel.Controls.Add(endDateFieldContainer);
                #endregion
            }
            #region Frequency
            // frequency container
            this._FrequencyFieldContainer = new Panel();
            this._FrequencyFieldContainer.ID = "Frequency_Container";
            this._FrequencyFieldContainer.CssClass = "FormFieldContainer";

            //frequency input
            Panel frequencyFieldInputContainer = new Panel();
            frequencyFieldInputContainer.ID = "Frequency_InputContainer";
            frequencyFieldInputContainer.CssClass = "FormFieldInputContainer";

            this._FrequencySelectList = new DropDownList();
            this._FrequencySelectList.ID = "Frequency_Field";
            this._FrequencySelectList.CssClass = "DropdownSelectList";
            frequencyFieldInputContainer.Controls.Add(this._FrequencySelectList);

            // frequency label
            Panel frequencyFieldLabelContainer = new Panel();
            frequencyFieldLabelContainer.ID = "Frequency_LabelContainer";
            frequencyFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label frequencyFieldLabel = new Label();
            frequencyFieldLabel.Text = _GlobalResources.Frequency + " : ";
            frequencyFieldLabel.AssociatedControlID = this._FrequencySelectList.ID;

            frequencyFieldLabelContainer.Controls.Add(frequencyFieldLabel);

            //frequency error panel
            Panel frequencyFieldErrorContainer = new Panel();
            frequencyFieldErrorContainer.ID = "Frequency_ErrorContainer";
            frequencyFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to container
            this._FrequencyFieldContainer.Controls.Add(frequencyFieldLabelContainer);
            this._FrequencyFieldContainer.Controls.Add(frequencyFieldErrorContainer);
            this._FrequencyFieldContainer.Controls.Add(frequencyFieldInputContainer);

            this._TabFiltersContentPanel.Controls.Add(this._FrequencyFieldContainer);
            #endregion

            #region Exclude day of week option
            // day select list container
            this._ExcludeOptionFieldContainer = new Panel();
            this._ExcludeOptionFieldContainer.ID = "DaySelectList_Container";
            this._ExcludeOptionFieldContainer.CssClass = "FormFieldContainer";

            //day select list input
            Panel excludeOptionFieldInputContainer = new Panel();
            excludeOptionFieldInputContainer.ID = "DaySelectList_InputContainer";
            excludeOptionFieldInputContainer.CssClass = "FormFieldInputContainer";

            this._ExcludeDaySelectList = new CheckBoxList();
            this._ExcludeDaySelectList.ID = "DaySelectList_Field";
            this._ExcludeDaySelectList.RepeatDirection = RepeatDirection.Horizontal;
            excludeOptionFieldInputContainer.Controls.Add(this._ExcludeDaySelectList);

            Label lblFilter = new Label();
            lblFilter.Controls.Add(this._ExcludeDaySelectList);

            excludeOptionFieldInputContainer.Controls.Add(lblFilter);

            // day select list label
            Panel excludeOptionFieldLabelContainer = new Panel();
            excludeOptionFieldLabelContainer.ID = "DaySelectList_LabelContainer";
            excludeOptionFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label excludeOptionFieldLabel = new Label();
            excludeOptionFieldLabel.Text = _GlobalResources.ExcludeDayOfWeek + " : ";
            excludeOptionFieldLabel.AssociatedControlID = this._ExcludeDaySelectList.ID;

            excludeOptionFieldLabelContainer.Controls.Add(excludeOptionFieldLabel);

            //day select list error panel
            Panel excludeOptionFieldErrorContainer = new Panel();
            excludeOptionFieldErrorContainer.ID = "DaySelectList_ErrorContainer";
            excludeOptionFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            // add controls to container
            this._ExcludeOptionFieldContainer.Controls.Add(excludeOptionFieldLabelContainer);
            this._ExcludeOptionFieldContainer.Controls.Add(excludeOptionFieldErrorContainer);
            this._ExcludeOptionFieldContainer.Controls.Add(excludeOptionFieldInputContainer);

            this._TabFiltersContentPanel.Controls.Add(this._ExcludeOptionFieldContainer);
            #endregion

            #region Cousre Selection Section
            //only used for course completion datatset
            this._BuildSelectCoursesPanel();
            #endregion

            #region Certificate Selection Section
            this._BuildSelectCertificatePanel();
            #endregion


            if ((EnumDataSetAnalytic)this._AnalyticObject.IdDataset == EnumDataSetAnalytic.UserGroupSnapshot)
            {
                #region User Or Group option
                // day select list container
                this._UserOrGroupOptionFieldContainer = new Panel();
                this._UserOrGroupOptionFieldContainer.ID = "UserOrGroupOptionFieldContainer";
                this._UserOrGroupOptionFieldContainer.CssClass = "UserFieldContainer UserPanel";

                //day select list input
                Panel userOrGroupFieldInputContainer = new Panel();
                userOrGroupFieldInputContainer.ID = "UserOrGroupField_InputContainer";
                userOrGroupFieldInputContainer.CssClass = "FormFieldInputContainer";

                this._UserOrGroupRadioButton = new RadioButtonList();
                this._UserOrGroupRadioButton.ID = "UserOrGroupFeild";
                this._UserOrGroupRadioButton.RepeatDirection = RepeatDirection.Horizontal;
                this._UserOrGroupRadioButton.Attributes.Add("onClick", "displayOrderFeildsForGroup(this);");

                this._UserOrGroupRadioButton.Items.Add(new ListItem(_GlobalResources.User, "1"));
                this._UserOrGroupRadioButton.Items.Add(new ListItem(_GlobalResources.Group, "2"));

                userOrGroupFieldInputContainer.Controls.Add(this._UserOrGroupRadioButton);

                Label userOrGrouplblFilter = new Label();
                userOrGrouplblFilter.Controls.Add(this._UserOrGroupRadioButton);

                userOrGroupFieldInputContainer.Controls.Add(userOrGrouplblFilter);

                // day select list label
                Panel userOrGroupFieldLabelContainer = new Panel();
                userOrGroupFieldLabelContainer.ID = "UserOrGroupFeild_LabelContainer";
                userOrGroupFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                Label userOrGroupFieldLabel = new Label();
                userOrGroupFieldLabel.Text = _GlobalResources.SelectChartForUserOrGroup + ": ";
                userOrGroupFieldLabel.AssociatedControlID = this._UserOrGroupRadioButton.ID;

                userOrGroupFieldLabelContainer.Controls.Add(userOrGroupFieldLabel);

                //day select list error panel
                Panel userOrGroupFieldErrorContainer = new Panel();
                userOrGroupFieldErrorContainer.ID = "UserOrGroupFeild_ErrorContainer";
                userOrGroupFieldErrorContainer.CssClass = "FormFieldErrorContainer";

                // add controls to container
                this._UserOrGroupOptionFieldContainer.Controls.Add(userOrGroupFieldLabelContainer);
                this._UserOrGroupOptionFieldContainer.Controls.Add(userOrGroupFieldErrorContainer);
                this._UserOrGroupOptionFieldContainer.Controls.Add(userOrGroupFieldInputContainer);

                this._TabFiltersContentPanel.Controls.Add(this._UserOrGroupOptionFieldContainer);
                #endregion

                #region OrderColumn option
                // day select list container
                this._OrderColumnOptionFieldContainer = new Panel();
                this._OrderColumnOptionFieldContainer.ID = "OrderColumnOptionFieldContainer";
                this._OrderColumnOptionFieldContainer.CssClass = "FormFieldContainer";
                this._OrderColumnOptionFieldContainer.Attributes.Add("style", "display: none;");

                //day select list input
                Panel orderColumnFieldInputContainer = new Panel();
                orderColumnFieldInputContainer.ID = "OrderColumnField_InputContainer";
                orderColumnFieldInputContainer.CssClass = "FormFieldInputContainer";

                this._OrderColumnRadioButton = new RadioButtonList();
                this._OrderColumnRadioButton.ID = "OrderColumnFeild";
                this._OrderColumnRadioButton.RepeatDirection = RepeatDirection.Horizontal;

                _OrderColumnRadioButton.Items.Add(new ListItem(_GlobalResources.GroupName, "1"));
                _OrderColumnRadioButton.Items.Add(new ListItem(_GlobalResources.NumberOfGroupUsers, "2"));

                orderColumnFieldInputContainer.Controls.Add(this._OrderColumnRadioButton);

                Label orderColumnlblFilter = new Label();
                orderColumnlblFilter.Controls.Add(this._OrderColumnRadioButton);

                orderColumnFieldInputContainer.Controls.Add(orderColumnlblFilter);

                // day select list label
                Panel orderColumnFieldLabelContainer = new Panel();
                orderColumnFieldLabelContainer.ID = "OrderColumnFeild_LabelContainer";
                orderColumnFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                Label orderColumnFieldLabel = new Label();
                orderColumnFieldLabel.Text = _GlobalResources.OrderBy + ": ";
                orderColumnFieldLabel.AssociatedControlID = this._OrderColumnRadioButton.ID;

                orderColumnFieldLabelContainer.Controls.Add(orderColumnFieldLabel);

                //day select list error panel
                Panel orderColumnFieldErrorContainer = new Panel();
                orderColumnFieldErrorContainer.ID = "OrderColumnFeild_ErrorContainer";
                orderColumnFieldErrorContainer.CssClass = "FormFieldErrorContainer";

                // add controls to container
                this._OrderColumnOptionFieldContainer.Controls.Add(orderColumnFieldLabelContainer);
                this._OrderColumnOptionFieldContainer.Controls.Add(orderColumnFieldErrorContainer);
                this._OrderColumnOptionFieldContainer.Controls.Add(orderColumnFieldInputContainer);

                this._TabFiltersContentPanel.Controls.Add(this._OrderColumnOptionFieldContainer);
                #endregion

                #region Order option
                // day select list container
                this._OrderOptionFieldContainer = new Panel();
                this._OrderOptionFieldContainer.ID = "OrderOptionFieldContainer";
                this._OrderOptionFieldContainer.CssClass = "FormFieldContainer";
                this._OrderOptionFieldContainer.Attributes.Add("style", "display: none;");

                //day select list input
                Panel orderFieldInputContainer = new Panel();
                orderFieldInputContainer.ID = "OrderField_InputContainer";
                orderFieldInputContainer.CssClass = "FormFieldInputContainer";

                this._OrderRadioButton = new RadioButtonList();
                this._OrderRadioButton.ID = "OrderFeild";
                this._OrderRadioButton.RepeatDirection = RepeatDirection.Horizontal;

                _OrderRadioButton.Items.Add(new ListItem(_GlobalResources.Ascending, "1"));
                _OrderRadioButton.Items.Add(new ListItem(_GlobalResources.Descending, "2"));

                orderFieldInputContainer.Controls.Add(this._OrderRadioButton);

                Label orderlblFilter = new Label();
                orderlblFilter.Controls.Add(this._OrderRadioButton);

                orderFieldInputContainer.Controls.Add(orderlblFilter);

                // day select list label
                Panel orderFieldLabelContainer = new Panel();
                orderFieldLabelContainer.ID = "OrderFeild_LabelContainer";
                orderFieldLabelContainer.CssClass = "FormFieldLabelContainer";

                Label orderFieldLabel = new Label();
                orderFieldLabel.Text = _GlobalResources.ChooseOrder + ": ";
                orderFieldLabel.AssociatedControlID = this._OrderRadioButton.ID;

                orderFieldLabelContainer.Controls.Add(orderFieldLabel);

                //day select list error panel
                Panel orderFieldErrorContainer = new Panel();
                orderFieldErrorContainer.ID = "OrderFeild_ErrorContainer";
                orderFieldErrorContainer.CssClass = "FormFieldErrorContainer";

                // add controls to container
                this._OrderOptionFieldContainer.Controls.Add(orderFieldLabelContainer);
                this._OrderOptionFieldContainer.Controls.Add(orderFieldErrorContainer);
                this._OrderOptionFieldContainer.Controls.Add(orderFieldInputContainer);

                this._TabFiltersContentPanel.Controls.Add(this._OrderOptionFieldContainer);
                #endregion
            }

        }
        #endregion

        #region _BuildPropertyTabContent
        /// <summary>
        /// Builds the tab content container for property
        /// </summary>
        private void _BuildPropertyTabContent()
        {
            this._TabPropertyContentPanel = new Panel();
            this._TabPropertyContentPanel.ID = "Analytic_" + "Property_TabPanel";
            this._TabPropertyContentPanel.CssClass = "TabContentPanel";
            this._TabPropertyContentPanel.Attributes.Add("style", "display: none;");

            Panel tabPropertyPanel = new Panel();
            tabPropertyPanel.ID = "Analytic_Property_Panel";
            tabPropertyPanel.CssClass = "UserPanel UserFieldContainer";

            Label titleLabel = new Label();
            titleLabel.Text = _GlobalResources.Name + " : ";
            titleLabel.CssClass = "Bold";

            ///text box area for report title
            this._AnalyticTitleTextBox = new TextBox();
            this._AnalyticTitleTextBox.ID = "AnalyticTitleText";
            this._AnalyticTitleTextBox.CssClass = "_Field";
            this._AnalyticTitleTextBox.Columns = 50;
            this._AnalyticTitleTextBox.Text = this._AnalyticObject.Title;


            // title error panel
            Panel titleFieldErrorContainer = new Panel();
            titleFieldErrorContainer.ID = "Title_ErrorContainer";
            titleFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            tabPropertyPanel.Controls.Add(titleLabel);
            tabPropertyPanel.Controls.Add(titleFieldErrorContainer);
            tabPropertyPanel.Controls.Add(this._AnalyticTitleTextBox);

            ///privacy section           
            ///instruction to the privacy
            ///
            Panel privacyPanel = new Panel();
            privacyPanel.CssClass = "Privacy";
            Image informationImage = new Image();
            informationImage.CssClass = "InfoImage";
            informationImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_INFORMATION_BLUE,
                                                   ImageFiles.EXT_PNG);
            informationImage.CssClass = "SmallIcon";
            privacyPanel.Controls.Add(informationImage);

            Label privacyInstructionLabel = new Label();
            privacyInstructionLabel.Text = _GlobalResources.SelectPublicToAllowOthersToViewAndRunYourAnalyticConfiguration;
            privacyPanel.Controls.Add(privacyInstructionLabel);

            ///Radio buttons for the privacy
            Panel privacyRadioButtonPanel = new Panel();

            Label privacyTitleLabel = new Label();
            privacyTitleLabel.Text = _GlobalResources.Privacy + ": ";
            privacyTitleLabel.CssClass = "TitleLabel";

            privacyRadioButtonPanel.Controls.Add(privacyTitleLabel);

            this._RadioButtonListIsPublic.ID = this.ID + "_IsPublic_Field";
            this._RadioButtonListIsPublic.RepeatDirection = RepeatDirection.Horizontal;
            this._RadioButtonListIsPublic.RepeatLayout = RepeatLayout.Flow;

            ListItem privacyPublic = new ListItem(_GlobalResources.Public, "true");
            this._RadioButtonListIsPublic.Items.Add(privacyPublic);

            ListItem privacyPrivate = new ListItem(_GlobalResources.Private, "false");
            this._RadioButtonListIsPublic.Items.Add(privacyPrivate);

            this._RadioButtonListIsPublic.SelectedIndex = (this._AnalyticObject.IsPublic) ? 0 : 1;
            privacyRadioButtonPanel.Controls.Add(this._RadioButtonListIsPublic);

            tabPropertyPanel.Controls.Add(privacyPanel);
            tabPropertyPanel.Controls.Add(privacyRadioButtonPanel);

            this._TabPropertyContentPanel.Controls.Add(tabPropertyPanel);

        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // run button
            this._RunButton.ID = "RunAnalyticButton";
            this._RunButton.CssClass = "Button AnalyticButton";
            this._RunButton.Text = _GlobalResources.RunAnalytic;
            this._RunButton.Command += new CommandEventHandler(_RunButton_Command);
            this._RunButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements();");
            this.ActionsPanel.Controls.Add(this._RunButton);

            // save button
            this._SaveButton.ID = "SaveAnalyticButton";
            this._SaveButton.CssClass = "Button AnalyticButton";

            this._SaveButton.Text = this._CreateAnalytic ? _GlobalResources.CreateAnalytic : this._SaveButton.Text = _GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this._SaveButton.Attributes.Add("onclick", "PopulateHiddenFieldsForDynamicElements();");
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // cancel button
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button AnalyticButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            { // validate the form
                if (!this._ValidateForm(true))
                { throw new AsentiaException(); }

                this._AnalyticObject.IsPublic = this._RadioButtonListIsPublic.SelectedIndex == 0 ? true : false;
                this._AnalyticObject.IsDeleted = false;
                this._AnalyticObject.Title = this._AnalyticTitleTextBox.Text;
                if (this._FrequencySelectList.SelectedIndex > 0)
                {
                    this._AnalyticObject.Frequency = Convert.ToInt32(this._FrequencySelectList.SelectedValue);
                }
                if (!String.IsNullOrWhiteSpace(this._ExcludeDaySelectList.SelectedValue))
                {
                    this._AnalyticObject.ExcludeDays = this._GetAllSelectedDays();
                }
                if (!String.IsNullOrWhiteSpace(this._SelectedAnalyticCertificates.Value))
                {
                    this._AnalyticObject.Certificates = this._SelectedAnalyticCertificates.Value;
                }
                if (_IsDateFilterRequired)
                {
                    this._AnalyticObject.DtStart = this._StartDateTextBox.Value;
                    this._AnalyticObject.DtEnd = this._EndDateTextBox.Value;
                }
                this._AnalyticObject.IdChartType = Convert.ToInt32(this._ChartTypeSelectList.SelectedValue);
                if (this._IsActivityRequired)
                {
                    this._AnalyticObject.Activity = Convert.ToInt32(this._ActivityList.SelectedValue);
                }
                // save the Analytic, save its returned id to viewstate, and 
                // instansiate a new Analytic object with the id
                int id = this._AnalyticObject.Save();
                this._AnalyticObject = new Analytic(id);

                // delcare data table
                DataTable selectedCourses = new DataTable();
                selectedCourses.Columns.Add("id", typeof(int));
                selectedCourses.Columns.Add("order", typeof(int));

                int order = 0;
                string selectedFieldString = string.Empty;

                // do course experts
                if (!String.IsNullOrWhiteSpace(this._SelectedAnalyticCourses.Value))
                {
                    // split the "value" of the hidden field to get an array of course ids
                    string[] selectedLearningPathCourses = this._SelectedAnalyticCourses.Value.Split(',');

                    // put ids into datatable 
                    foreach (string courseId in selectedLearningPathCourses)
                    {
                        order++;
                        selectedCourses.Rows.Add(Convert.ToInt32(courseId), order);
                    }

                    // save the learning path courses
                    this._AnalyticObject.SaveCourses(selectedCourses);
                }

                Session["CreateAnalyticSuccess"] = this._CreateAnalytic;
                Response.Redirect("~/reporting/analytics");
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/Analyticing/analytics");
        }
        #endregion

        #region _RunButton_Command
        /// <summary>
        /// Handles the "Run" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RunButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateForm(false))
                { throw new AsentiaException(); }

                if (_IsDateFilterRequired)
                {
                    this._AnalyticObject.DtStart = this._StartDateTextBox.Value;
                    this._AnalyticObject.DtEnd = this._EndDateTextBox.Value;
                }

                //find all selected days value
                if (!String.IsNullOrWhiteSpace(this._ExcludeDaySelectList.SelectedValue))
                {
                    this._AnalyticObject.ExcludeDays = this._GetAllSelectedDays();
                }

                //if(this._GetAnalyticObject.idda)
                this._AnalyticObject.Courses = this._SelectedAnalyticCourses.Value;

                this._AnalyticObject.Certificates = this._SelectedAnalyticCertificates.Value;

                //assign values to analytic object according to datset
                this._AssignValuesAccordingToDataset();

                //generate analytic
                DataSet analyticDataset = this._AnalyticObject.Run();

                Session["AnalyticData"] = analyticDataset;

                //redirect ot anatylict output page
                Response.Redirect("Run.aspx?id=" + Convert.ToString(this._ChartTypeSelectList.SelectedValue) + "&dsid=" + Convert.ToString(this._AnalyticObject.IdDataset)); ;
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                //display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _PopulateAnalyticformInputControls
        /// <summary>
        /// populate all input controls
        /// </summary>
        private void _PopulateAnalyticformInputControls()
        {
            if (this._AnalyticObject != null)
            {//title
                this._AnalyticTitleTextBox.Text = this._AnalyticObject.Title;

                //selected analytic chart type 
                if (this._AnalyticObject.IdChartType > 0)
                {
                    this._ChartTypeSelectList.SelectedValue = Convert.ToString(this._AnalyticObject.IdChartType);
                }
                //frequency
                if (this._AnalyticObject.Frequency > 0)
                {
                    this._FrequencySelectList.SelectedValue = Convert.ToString(this._AnalyticObject.Frequency);
                }

                if (this._AnalyticObject.ExcludeDays != null && this._AnalyticObject.ExcludeDays.Contains(','))
                {
                    string[] days;
                    days = this._AnalyticObject.ExcludeDays.Split(',');
                    for (int i = 0; i < days.Length; i++)
                    {
                        if (this._ExcludeDaySelectList.Items.FindByValue(days[i]) != null)
                        {
                            this._ExcludeDaySelectList.Items.FindByValue(days[i]).Selected = true;
                        }

                    }

                }
                if (this._IsDateFilterRequired)
                {
                    this._StartDateTextBox.Value = this._AnalyticObject.DtStart;
                    this._EndDateTextBox.Value = this._AnalyticObject.DtEnd;
                }
                this._RadioButtonListIsPublic.SelectedIndex = this._AnalyticObject.IsPublic == true ? 0 : 1;

                //bind all courses of analytic
                // loop through the datatable and add each member to the listing container
                if ((EnumDataSetAnalytic)this._AnalyticObject.IdDataset == EnumDataSetAnalytic.CourseCompletions && this._AnalyticObject.IdAnalytic > 0)
                {
                    foreach (DataRow row in this._AnalyticCourses.Rows)
                    {
                        // container
                        Panel courseNameContainer = new Panel();
                        courseNameContainer.ID = "Course_" + row["idCourse"].ToString();

                        // remove course button
                        System.Web.UI.WebControls.Image removeCourseImage = new System.Web.UI.WebControls.Image();
                        removeCourseImage.ID = "Course_" + row["idCourse"].ToString() + "_RemoveImage";
                        removeCourseImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                            ImageFiles.EXT_PNG);
                        removeCourseImage.CssClass = "SmallIcon";
                        removeCourseImage.Attributes.Add("onClick", "javascript:RemoveCoursesFromAnalytic('" + row["idCourse"].ToString() + "');");
                        removeCourseImage.Style.Add("cursor", "pointer");

                        // course name
                        Literal courseName = new Literal();
                        courseName.Text = row["title"].ToString();

                        // add controls to container
                        courseNameContainer.Controls.Add(removeCourseImage);
                        courseNameContainer.Controls.Add(courseName);
                        this._AnalyticCoursesListContainer.Controls.Add(courseNameContainer);
                    }
                }

                //bind activity list to value
                if (this._IsActivityRequired && (EnumDataSetAnalytic)this._AnalyticObject.IdDataset == EnumDataSetAnalytic.CourseCompletions)
                {
                    this._ActivityList.SelectedValue = Convert.ToString(this._AnalyticObject.Activity);
                }

            }
        }
        #endregion

        #region FormValidation
        /// <summary>
        /// Form Validation
        /// </summary>
        /// <param name="isGoingToSave"></param>
        /// <returns></returns>
        private bool _ValidateForm(bool isGoingToSave)
        {
            bool isValid = true;

            //titel field validation while saving
            if (isGoingToSave && String.IsNullOrWhiteSpace(this._AnalyticTitleTextBox.Text))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "Title", _GlobalResources.EndDate + " " + _GlobalResources.IsRequired);
            }

            //dates year validation (Optional)

            if (this._IsDateFilterRequired && this._StartDateTextBox.Value != null && this._EndDateTextBox.Value != null && this._FilterConditions != null)
            {
                if (this._FilterConditions.ContainsKey("FilterOfSameYear") && Convert.ToString(this._FilterConditions["FilterOfSameYear"]).ToLower() == "true")
                {
                    int startYear = Convert.ToDateTime(this._StartDateTextBox.Value).Year;
                    int endYear = Convert.ToDateTime(this._EndDateTextBox.Value).Year;
                    if (startYear != endYear)
                    {
                        isValid = false;
                        this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "StartEndDate", _GlobalResources.StartAndEndDateShouldBothBeInTheSameYear + " .");

                    }
                }
            }
            //apply dataset wise form validations
            switch ((EnumDataSetAnalytic)this._AnalyticObject.IdDataset)
            {
                case EnumDataSetAnalytic.LoginActivity:
                    isValid = this._LoginActivityFormValidations();
                    break;
                case EnumDataSetAnalytic.CourseCompletions:
                    isValid = this._CourseCompletionsFormValidations();
                    break;
                case EnumDataSetAnalytic.CertificatesAnalytics:
                    isValid = this._CertificateAnalyticsFormValidations();
                    break;
                case EnumDataSetAnalytic.UserGroupSnapshot:
                case EnumDataSetAnalytic.UseStatistics:
                    break;
                default:
                    break;
            }
            return isValid;

        }
        #endregion

        #region _GetAllSelectedDays
        private string _GetAllSelectedDays()
        {
            //find all selected days value

            string days = string.Empty;
            for (int i = 0; i < this._ExcludeDaySelectList.Items.Count; i++)
            {
                if (this._ExcludeDaySelectList.Items[i].Selected)
                {
                    days += this._ExcludeDaySelectList.Items[i].Value + ",";
                }

            }

            days = days.TrimEnd(',');
            return days;

        }
        #endregion

        #region Course Completion Section

        #region _BuildSelectCoursesModal
        /// <summary>
        /// Builds the modal for selecting courses to add to the learning path.
        /// </summary>
        private void _BuildSelectCoursesModal(string targetControlId)
        {
            // set modal properties
            this._SelectCoursesForAnalytic = new ModalPopup("SelectCoursesForAnalyticModal");
            this._SelectCoursesForAnalytic.Type = ModalPopupType.Form;
            this._SelectCoursesForAnalytic.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE,
                                                                                   ImageFiles.EXT_PNG);
            this._SelectCoursesForAnalytic.HeaderIconAlt = _GlobalResources.SelectCourse_s;
            this._SelectCoursesForAnalytic.HeaderText = _GlobalResources.SelectCourse_s;
            this._SelectCoursesForAnalytic.TargetControlID = targetControlId;
            this._SelectCoursesForAnalytic.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCoursesForAnalytic.SubmitButtonCustomText = _GlobalResources.New;
            this._SelectCoursesForAnalytic.SubmitButton.OnClientClick = "javascript:AddCoursesToAnalytic(); return false;";
            this._SelectCoursesForAnalytic.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCoursesForAnalytic.CloseButtonCustomText = _GlobalResources.Close;
            this._SelectCoursesForAnalytic.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectEligibleAnalyticCourses = new DynamicListBox("SelectEligibleCourses");
            this._SelectEligibleAnalyticCourses.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectEligibleAnalyticCourses.SearchButton.Command += new CommandEventHandler(this._SearchSelectCoursesButton_Command);
            this._SelectEligibleAnalyticCourses.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectCoursesButton_Command);
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataSource = this._EligibleAnalyticCoursesForSelectList;
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataBind();
            this._SelectEligibleAnalyticCourses.IncludeSelectAllNone = false;
            // add controls to body
            this._SelectCoursesForAnalytic.AddControlToBody(this._SelectEligibleAnalyticCourses);

            // add modal to container
            this.AnalyticFormContainer.Controls.Add(this._SelectCoursesForAnalytic);
        }
        #endregion

        #region _SearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Course(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCoursesForAnalytic.ClearFeedback();

            // clear the listbox control
            this._SelectEligibleAnalyticCourses.ListBoxControl.Items.Clear();

            // do the search
            if (this._AnalyticObject != null && this._AnalyticObject.IdAnalytic > 0)
            {
                this._AnalyticCourses = this._AnalyticObject.GetCourses(null);
                this._EligibleAnalyticCoursesForSelectList = Analytic.IdsAndNamesForAnalyticSelectList(this._AnalyticObject.IdAnalytic, this._SelectEligibleAnalyticCourses.SearchTextBox.Text);
            }
            else
            {
                this._EligibleAnalyticCoursesForSelectList = Analytic.IdsAndNamesForAnalyticSelectList(0, this._SelectEligibleAnalyticCourses.SearchTextBox.Text);
            }

            this._SelectEligibleAnalyticCourses.ListBoxControl.DataSource = this._EligibleAnalyticCoursesForSelectList;
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataBind();
        }
        #endregion

        #region _ClearSearchSelectCoursesButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Courses" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectCoursesButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCoursesForAnalytic.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectEligibleAnalyticCourses.ListBoxControl.Items.Clear();
            this._SelectEligibleAnalyticCourses.SearchTextBox.Text = "";

            // clear the search
            if (this._AnalyticObject != null && this._AnalyticObject.IdAnalytic > 0)
            { this._EligibleAnalyticCoursesForSelectList = Analytic.IdsAndNamesForAnalyticSelectList(this._AnalyticObject.IdAnalytic, null); }
            else
            { this._EligibleAnalyticCoursesForSelectList = Analytic.IdsAndNamesForAnalyticSelectList(0, null); }

            this._SelectEligibleAnalyticCourses.ListBoxControl.DataSource = this._EligibleAnalyticCoursesForSelectList;
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataTextField = "title";
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataValueField = "idCourse";
            this._SelectEligibleAnalyticCourses.ListBoxControl.DataBind();
        }
        #endregion

        #region _BuildSelectCoursesPanel
        /// <summary>
        /// _BuildSelectCoursesPanel
        /// </summary>
        private void _BuildSelectCoursesPanel()
        {
            // populate datatables with lists of courses
            if (this._AnalyticObject != null && this._AnalyticObject.IdAnalytic > 0)
            {
                this._AnalyticCourses = this._AnalyticObject.GetCourses(null);
                this._EligibleAnalyticCoursesForSelectList = Analytic.IdsAndNamesForAnalyticSelectList(this._AnalyticObject.IdAnalytic, null);
            }
            else
            {
                this._EligibleAnalyticCoursesForSelectList = Analytic.IdsAndNamesForAnalyticSelectList(0, null);
            }

            // start date label
            Panel coursesFieldLabelContainer = new Panel();
            coursesFieldLabelContainer.ID = "Courses_LabelContainer";
            coursesFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label coursesFieldLabel = new Label();
            coursesFieldLabel.Text = _GlobalResources.CoursesMaximum5Courses + ": ";

            coursesFieldLabelContainer.Controls.Add(coursesFieldLabel);

            //Error Panel
            Panel coursesFieldErrorContainer = new Panel();
            coursesFieldErrorContainer.ID = "Courses" + "_ErrorContainer";
            coursesFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            this._CoursesPanel = new Panel();
            this._CoursesPanel.ID = "AnalyticProperties_" + "Courses" + "_TabPanel";
            this._CoursesPanel.Attributes.Add("style", "display: none;");

            // course container
            Panel AnalyticCoursesFieldContainer = new Panel();
            AnalyticCoursesFieldContainer.ID = "Analytic_Courses_Container";
            AnalyticCoursesFieldContainer.CssClass = "FormFieldContainer";

            // course selected hidden field
            this._SelectedAnalyticCourses = new HiddenField();
            this._SelectedAnalyticCourses.ID = "SelectedAnalyticCourses_Field";
            AnalyticCoursesFieldContainer.Controls.Add(this._SelectedAnalyticCourses);

            // build a container for the user membership listing
            this._AnalyticCoursesListContainer = new Panel();
            this._AnalyticCoursesListContainer.ID = "AnalyticCoursesList_Container";
            this._AnalyticCoursesListContainer.CssClass = "ItemListingContainer";

            AnalyticCoursesFieldContainer.Controls.Add(this._AnalyticCoursesListContainer);

            Panel AnalyticCoursesButtonsPanel = new Panel();
            AnalyticCoursesButtonsPanel.ID = "AnalyticCourses_ButtonsPanel";

            // select courses button

            // link
            Image selectCoursessImageForLink = new Image();
            selectCoursessImageForLink.ID = "LaunchSelectCoursesModalImage";
            selectCoursessImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_COURSE,
                                                                         ImageFiles.EXT_PNG);
            selectCoursessImageForLink.CssClass = "MediumIcon";

            Localize selectCoursesTextForLink = new Localize();
            selectCoursesTextForLink.Text = _GlobalResources.SelectCourse_s;

            LinkButton selectCoursesLink = new LinkButton();
            selectCoursesLink.ID = "LaunchSelectCoursesModal";
            selectCoursesLink.CssClass = "ImageLink";
            selectCoursesLink.Controls.Add(selectCoursessImageForLink);
            selectCoursesLink.Controls.Add(selectCoursesTextForLink);

            AnalyticCoursesButtonsPanel.Controls.Add(selectCoursesLink); ;

            // attach the buttons panel to the container
            AnalyticCoursesFieldContainer.Controls.Add(AnalyticCoursesButtonsPanel);

            // build modals for adding and removing course
            this._BuildSelectCoursesModal(selectCoursesLink.ID);

            // add controls to container

            //Error Container To Field Container
            this._CoursesPanel.Controls.Add(coursesFieldLabelContainer);
            this._CoursesPanel.Controls.Add(coursesFieldErrorContainer);
            this._CoursesPanel.Controls.Add(AnalyticCoursesFieldContainer);

            // attach panel to container
            this._TabFiltersContentPanel.Controls.Add(this._CoursesPanel);
        }
        #endregion

        #endregion

        #region Certificate Section
        #region _BuildSelectCerificateModal
        /// <summary>
        /// Builds the modal for selecting certificate title.
        /// </summary>
        private void _BuildSelectCerificateModal(string targetControlId)
        {
            // set modal properties
            this._SelectCertificateTitleForAnalytic = new ModalPopup("SelectCertificateTitleForAnalytic");
            this._SelectCertificateTitleForAnalytic.Type = ModalPopupType.Form;
            this._SelectCertificateTitleForAnalytic.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE,
                                                                                            ImageFiles.EXT_PNG);
            this._SelectCertificateTitleForAnalytic.HeaderIconAlt = _GlobalResources.SelectCertificate_s;
            this._SelectCertificateTitleForAnalytic.HeaderText = _GlobalResources.SelectCertificate_s;
            this._SelectCertificateTitleForAnalytic.TargetControlID = targetControlId;
            this._SelectCertificateTitleForAnalytic.SubmitButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCertificateTitleForAnalytic.SubmitButtonCustomText = _GlobalResources.New;
            this._SelectCertificateTitleForAnalytic.SubmitButton.OnClientClick = "javascript:AddCertificatesToAnalytic(); return false;";
            this._SelectCertificateTitleForAnalytic.CloseButtonTextType = ModalPopupButtonText.Custom;
            this._SelectCertificateTitleForAnalytic.CloseButtonCustomText = _GlobalResources.Done;
            this._SelectCertificateTitleForAnalytic.ReloadPageOnClose = false;

            // build the modal body

            // build a container for the user listing
            this._SelectCertificateTitle = new DynamicListBox("SelectCertificateTitle");
            this._SelectCertificateTitle.NoRecordsFoundMessage = _GlobalResources.NoRecordsFound;
            this._SelectCertificateTitle.SearchButton.Command += new CommandEventHandler(this._SearchSelectCertificateButton_Command);
            this._SelectCertificateTitle.ClearSearchButton.Command += new CommandEventHandler(this._ClearSearchSelectCertificateButton_Command);
            this._SelectCertificateTitle.ListBoxControl.DataSource = this._AnalyticCertificatesForSelectList;
            this._SelectCertificateTitle.ListBoxControl.DataTextField = "name";
            this._SelectCertificateTitle.ListBoxControl.DataValueField = "idCertificate";
            this._SelectCertificateTitle.ListBoxControl.DataBind();
            this._SelectCertificateTitle.IncludeSelectAllNone = false;

            // add controls to body
            this._SelectCertificateTitleForAnalytic.AddControlToBody(this._SelectCertificateTitle);

            // add modal to container
            this.AnalyticFormContainer.Controls.Add(this._SelectCertificateTitleForAnalytic);
        }
        #endregion

        #region _SearchSelectCertificateButton_Command
        /// <summary>
        /// Handles the "Search" button click event for the "Select Certificate(s)" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SearchSelectCertificateButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCertificateTitleForAnalytic.ClearFeedback();

            // clear the listbox control
            this._SelectCertificateTitle.ListBoxControl.Items.Clear();

            // do the search
            this._AnalyticCertificatesForSelectList = Analytic.IdsAndNamesForAnalyticSelectList(0, this._SelectCertificateTitle.SearchTextBox.Text);


            this._SelectCertificateTitle.ListBoxControl.DataSource = this._AnalyticCertificatesForSelectList;
            this._SelectCertificateTitle.ListBoxControl.DataTextField = "name";
            this._SelectCertificateTitle.ListBoxControl.DataValueField = "idCertificate";
            this._SelectCertificateTitle.ListBoxControl.DataBind();
        }
        #endregion

        #region _ClearSearchSelectCertificateButton_Command
        /// <summary>
        /// Handles the "Clear" button click event for the "Select Certificate" searchable list box control.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _ClearSearchSelectCertificateButton_Command(object sender, CommandEventArgs e)
        {
            // clear the modal's feedback container
            this._SelectCertificateTitleForAnalytic.ClearFeedback();

            // clear the listbox control and search text box
            this._SelectCertificateTitle.ListBoxControl.Items.Clear();
            this._SelectCertificateTitle.SearchTextBox.Text = "";

            // clear the search
            this._AnalyticCertificatesForSelectList = Analytic.CertificateIdsAndNamesForAnalyticSelectList(null);

            this._SelectCertificateTitle.ListBoxControl.DataSource = this._AnalyticCertificatesForSelectList;
            this._SelectCertificateTitle.ListBoxControl.DataTextField = "name";
            this._SelectCertificateTitle.ListBoxControl.DataValueField = "idCertificate";
            this._SelectCertificateTitle.ListBoxControl.DataBind();
        }
        #endregion

        #region _BuildSelectCertificatePanel
        /// <summary>
        /// _BuildSelectCertificatePanel
        /// </summary>
        private void _BuildSelectCertificatePanel()
        {
            this._AnalyticCertificatesForSelectList = Analytic.CertificateIdsAndNamesForAnalyticSelectList(null);

            // start date label
            Panel certificateFieldLabelContainer = new Panel();
            certificateFieldLabelContainer.ID = "Certificates_LabelContainer";
            certificateFieldLabelContainer.CssClass = "FormFieldLabelContainer";

            Label certificateFieldLabel = new Label();
            certificateFieldLabel.Text = _GlobalResources.CertificatesMaximum5Certificates + ": ";

            certificateFieldLabelContainer.Controls.Add(certificateFieldLabel);

            //Error Panel
            Panel certificateFieldErrorContainer = new Panel();
            certificateFieldErrorContainer.ID = "Certificates" + "_ErrorContainer";
            certificateFieldErrorContainer.CssClass = "FormFieldErrorContainer";

            this._CertificatesPanel = new Panel();
            this._CertificatesPanel.ID = "AnalyticProperties_" + "Certificates" + "_TabPanel";
            this._CertificatesPanel.Attributes.Add("style", "display: none;");

            // certificate container
            Panel AnalyticCertificatesFieldContainer = new Panel();
            AnalyticCertificatesFieldContainer.ID = "Analytic_Certificates_Container";
            AnalyticCertificatesFieldContainer.CssClass = "FormFieldContainer";

            // certificates selected hidden field
            this._SelectedAnalyticCertificates = new HiddenField();
            this._SelectedAnalyticCertificates.ID = "SelectedAnalyticCertificates_Field";
            AnalyticCertificatesFieldContainer.Controls.Add(this._SelectedAnalyticCertificates);

            // build a container for the user membership listing
            this._AnalyticCertificatesListContainer = new Panel();
            this._AnalyticCertificatesListContainer.ID = "AnalyticCertificatesList_Container";
            this._AnalyticCertificatesListContainer.CssClass = "ItemListingContainer";

            AnalyticCertificatesFieldContainer.Controls.Add(this._AnalyticCertificatesListContainer);

            Panel AnalyticCertificatesButtonsPanel = new Panel();
            AnalyticCertificatesButtonsPanel.ID = "AnalyticCertificates_ButtonsPanel";

            // select certificate button

            // link
            System.Web.UI.WebControls.Image selectCertificateImageForLink = new System.Web.UI.WebControls.Image();
            selectCertificateImageForLink.ID = "LaunchSelectCertificatesModalImage";
            selectCertificateImageForLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_CERTIFICATE,
                                                                            ImageFiles.EXT_PNG);
            selectCertificateImageForLink.CssClass = "MediumIcon";

            Localize selectCertificatesTextForLink = new Localize();
            selectCertificatesTextForLink.Text = _GlobalResources.SelectCertificate_s;

            LinkButton selectCertificatesLink = new LinkButton();
            selectCertificatesLink.ID = "LaunchSelectCertificateModal";
            selectCertificatesLink.CssClass = "ImageLink";
            selectCertificatesLink.Controls.Add(selectCertificateImageForLink);
            selectCertificatesLink.Controls.Add(selectCertificatesTextForLink);

            AnalyticCertificatesButtonsPanel.Controls.Add(selectCertificatesLink); ;

            // attach the buttons panel to the container
            AnalyticCertificatesFieldContainer.Controls.Add(AnalyticCertificatesButtonsPanel);

            // build modals for adding and removing course
            this._BuildSelectCerificateModal(selectCertificatesLink.ID);

            // add controls to container

            //Error Container To Field Container
            this._CertificatesPanel.Controls.Add(certificateFieldLabelContainer);
            this._CertificatesPanel.Controls.Add(certificateFieldErrorContainer);
            this._CertificatesPanel.Controls.Add(AnalyticCertificatesFieldContainer);

            // attach panel to container
            this._TabFiltersContentPanel.Controls.Add(this._CertificatesPanel);
        }
        #endregion
        #endregion

        #region _GetOrderColumnText
        /// <summary>
        /// gives the column value used in order by clause
        /// </summary>
        /// <param name="frequency"></param>
        /// <returns></returns>
        private string _GetOrderColumnText(EnumOrderColumnAnalytic text)
        {
            switch (text)
            {
                case EnumOrderColumnAnalytic.frequency:
                    return _GlobalResources.Frequency;
                case EnumOrderColumnAnalytic.count:
                    return _GlobalResources.Count;
                default:
                    return _GlobalResources.Frequency;
            }

        }
        #endregion

        #region _GetOrderText
        /// <summary>
        /// gives the ordering value used in order by clause
        /// </summary>
        /// <param name="frequency"></param>
        /// <returns></returns>
        private string _GetOrderText(EnumOrderAnalytic text)
        {
            switch (text)
            {
                case EnumOrderAnalytic.Ascending:
                    return _GlobalResources.Ascending;
                case EnumOrderAnalytic.Descending:
                    return _GlobalResources.Descending;
                default:
                    return _GlobalResources.Ascending;
            }

        }
        #endregion

        #region _EnableAndDisableControls
        private void _EnableAndDisableControls()
        {

            //hide containers if input controls not required

            //Frequency list
            if (this._IsFrequencyFilterRequired)
            {
                this._FrequencySelectList.Items.Insert(0, _GlobalResources.Select);
                this._FrequencySelectList.SelectedIndex = 0;
                this._FrequencyFieldContainer.Attributes.Add("style", "display: block;");
            }
            else
            {
                this._FrequencyFieldContainer.Attributes.Add("style", "display: none;");
            }

            //exclude day list
            if (this._IsExcludeDayFilterRequired)
            {
                this._ExcludeOptionFieldContainer.Attributes.Add("style", "display: block;");
            }
            else
            {
                this._ExcludeOptionFieldContainer.Attributes.Add("style", "display: none;");
            }

            //course filed
            if (this._IsCourseListRequired)
            {
                this._CoursesPanel.Attributes.Add("style", "display: block;");
            }
            else
            {
                this._CoursesPanel.Attributes.Add("style", "display: none;");
            }
            //certificate title filed
            if (this._IsCertificateTitleRequired)
            {
                this._CertificatesPanel.Attributes.Add("style", "display: block;");
            }
            else
            {
                this._CertificatesPanel.Attributes.Add("style", "display: none;");
            }
        }
        #endregion

        #region _AssignValuesAccordingToDataset
        /// <summary>
        /// assign values to analytic object according to datset
        /// </summary>
        private void _AssignValuesAccordingToDataset()
        {
            #region Activity
            //for user and group snap shot datset
            if ((EnumDataSetAnalytic)this._AnalyticObject.IdDataset == EnumDataSetAnalytic.UserGroupSnapshot)
            {
                if (this._UserOrGroupRadioButton.SelectedIndex != -1)
                {
                    this._AnalyticObject.Activity = Convert.ToInt32(this._UserOrGroupRadioButton.SelectedItem.Value);
                }

                if (this._OrderColumnRadioButton.SelectedIndex != -1)
                {
                    this._AnalyticObject.OrderColumn = this._GetOrderColumnText((EnumOrderColumnAnalytic)Convert.ToInt32(this._OrderColumnRadioButton.SelectedItem.Value)).ToUpper();
                }
                if (this._OrderRadioButton.SelectedIndex != -1)
                {
                    this._AnalyticObject.OrderOfRecords = this._GetOrderText((EnumOrderAnalytic)Convert.ToInt32(this._OrderRadioButton.SelectedItem.Value)).ToUpper();
                }
            }

            //for course completions datset
            if ((EnumDataSetAnalytic)this._AnalyticObject.IdDataset == EnumDataSetAnalytic.CourseCompletions && !String.IsNullOrWhiteSpace(this._ActivityList.SelectedValue))
            {
                this._AnalyticObject.Activity = Convert.ToInt32(this._ActivityList.SelectedValue);
            }

            if ((EnumDataSetAnalytic)this._AnalyticObject.IdDataset == EnumDataSetAnalytic.CertificatesAnalytics && !String.IsNullOrWhiteSpace(this._ActivityList.SelectedValue))
            {
                this._AnalyticObject.Activity = Convert.ToInt32(this._ActivityList.SelectedValue);
            }

            #endregion

            #region Frequency

            //assign _IsFrequencyFilterRequired = false when activity = 2 and datset is course completions
            if ((EnumDataSetAnalytic)this._AnalyticObject.IdDataset == EnumDataSetAnalytic.CourseCompletions && this._AnalyticObject.Activity == 2)
            {
                this._IsFrequencyFilterRequired = false;
            }


            if (this._IsFrequencyFilterRequired && this._FrequencySelectList.SelectedIndex > 0)
            {
                this._AnalyticObject.Frequency = Convert.ToInt32(this._FrequencySelectList.SelectedValue);
                Session["Frequency"] = this._AnalyticObject.Frequency;
            }
            else
            {
                this._AnalyticObject.Frequency = 0;
                Session["Frequency"] = this._AnalyticObject.Frequency;
            }
            #endregion

        }
        #endregion

        #region Page validations according to dataset
        private bool _LoginActivityFormValidations()
        {
            bool isValid = true;
            //date filter
            if (this._IsDateFilterRequired)
            {
                // start date  field
                if (this._StartDateTextBox.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "StartDate", _GlobalResources.StartDate + " " + _GlobalResources.IsRequired);
                }

                // end date  field
                if (this._EndDateTextBox.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "EndDate", _GlobalResources.EndDate + " " + _GlobalResources.IsRequired);
                }
            }

            //frequency filter
            if (this._IsFrequencyFilterRequired)
            {
                if (this._FrequencySelectList.SelectedIndex == 0)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "Frequency", _GlobalResources.Frequency + " " + _GlobalResources.IsRequired);
                }
            }
            return isValid;

        }

        private bool _CourseCompletionsFormValidations()
        {
            bool isValid = true;

            //date filter
            if (this._IsDateFilterRequired)
            {
                // start date  field
                if (this._StartDateTextBox.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "StartDate", _GlobalResources.StartDate + " " + _GlobalResources.IsRequired);
                }

                // end date  field
                if (this._EndDateTextBox.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "EndDate", _GlobalResources.EndDate + " " + _GlobalResources.IsRequired);
                }
            }

            //frequency
            if (this._IsFrequencyFilterRequired)
            {
                if (this._ActivityList.SelectedIndex == 0 && this._FrequencySelectList.SelectedIndex == 0)//check if activity is Total Course Completions as per frequency
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "Frequency", _GlobalResources.Frequency + " " + _GlobalResources.IsRequired);
                }
            }

            //for courses selection
            if (this._ActivityList.SelectedIndex == 1 && String.IsNullOrWhiteSpace(this._SelectedAnalyticCourses.Value))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "Courses", _GlobalResources.Courses + " " + _GlobalResources.IsRequired);
            }

            return isValid;

        }

        private bool _CertificateAnalyticsFormValidations()
        {
            bool isValid = true;

            //date filter
            if (this._IsDateFilterRequired)
            {
                // start date  field
                if (this._StartDateTextBox.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "StartDate", _GlobalResources.StartDate + " " + _GlobalResources.IsRequired);
                }

                // end date  field
                if (this._EndDateTextBox.Value == null)
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "EndDate", _GlobalResources.EndDate + " " + _GlobalResources.IsRequired);
                }
            }

            //frequency
            if (this._IsFrequencyFilterRequired)
            {
                if (this._ActivityList.SelectedIndex == 0 && this._FrequencySelectList.SelectedIndex == 0)//check if activity is Total Course Completions as per frequency
                {
                    isValid = false;
                    this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "Frequency", _GlobalResources.Frequency + " " + _GlobalResources.IsRequired);
                }
            }

            //for courses selection
            if (this._ActivityList.SelectedIndex == 1 && String.IsNullOrWhiteSpace(this._SelectedAnalyticCertificates.Value))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this.AnalyticFormContainer, "Certificates", _GlobalResources.Certificate + " " + _GlobalResources.IsRequired);
            }

            return isValid;

        }
        #endregion

        #region _GetAllRequirdFildsStatusFromXml
        private void _GetAllRequirdFildsStatusFromXml()
        {

            if (this._AnalyticObject != null)
            {
                this._DataSet = new AnalyticDataSet();
                this._DataSet.DatasetFieldsDataFile = "~/_bin/analyticDataset/Dataset." + Enum.GetName(typeof(EnumDataSetAnalytic), this._AnalyticObject.IdDataset) + ".Fields.xml";
                this._LabelDataset = this._DataSet.GetDataSetFields();
                if (this._LabelDataset != null)
                {
                    foreach (AnalyticDataSet.DatasetFieldProperties li in _LabelDataset)
                    {
                        if (li.IdentiFier.ToLower() == "frequency")
                        {
                            this._IsFrequencyFilterRequired = true;
                        }
                        if (li.IdentiFier.ToLower() == "excludeday")
                        {

                            this._IsExcludeDayFilterRequired = true;
                        }
                        if (li.IdentiFier.ToLower() == "coursesselectionlist")
                        {
                            this._IsCourseListRequired = true;
                        }
                        if (li.IdentiFier.ToLower() == "activity")
                        {
                            this._IsActivityRequired = true;
                        }
                        if (li.IdentiFier.ToLower() == "datefilterrequired")
                        {
                            this._IsDateFilterRequired = true;
                        }

                    }

                    //gets all filter conditions value into a dictionaries from xml
                    this._FilterConditions = this._DataSet._GetAllAnalyticSettings();


                }
            }


        }
        #endregion
    }

}
