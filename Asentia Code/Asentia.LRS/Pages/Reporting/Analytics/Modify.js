﻿function AddCoursesToAnalytic() {
    var AnalyticCoursesListContainer = $("#AnalyticCoursesList_Container");
    var selectedCourses = $('select#SelectEligibleCoursesListBox').val();
    var selectedAnalyticCourses = $("#AnalyticCoursesList_Container").children().length;
    if (selectedCourses != null && selectedAnalyticCourses <= MaxCoursesSelection) {
        //decide max no of loops to be run
        var count;
        if (selectedAnalyticCourses + selectedCourses.length <= MaxCoursesSelection) {
            count = selectedCourses.length;
        }
        else {
            count = MaxCoursesSelection - selectedAnalyticCourses
        }

        for (var i = 0; i < count; i++) {
            if (!$("#Course_" + selectedCourses[i]).length) {
                // add the selected user to the course expert list container
                var itemContainerDiv = $("<div id=\"Course_" + selectedCourses[i] + "\">" + "<img onclick=\"javascript:RemoveCoursesFromAnalytic('" + selectedCourses[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + $("#SelectEligibleCoursesListBox option[value='" + selectedCourses[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(AnalyticCoursesListContainer);
            }

            // remove the user from the select list
            $("#SelectEligibleCoursesListBox option[value='" + selectedCourses[i] + "']").remove();
        }
    }
}

function RemoveCoursesFromAnalytic(courseId) {
    $("#Course_" + courseId).remove();
}

function GetSelectedCoursesForHiddenField() {

    var AnalyticCoursesListContainer = $("#AnalyticCoursesList_Container");
    var selectedAnalyticCoursesField = $("#SelectedAnalyticCourses_Field");
    var selectedAnalyticCourses = "";

    AnalyticCoursesListContainer.children().each(function () {
        selectedAnalyticCourses = selectedAnalyticCourses + $(this).prop("id").replace("Course_", "") + ",";
    });

    if (selectedAnalyticCourses.length > 0)
    { selectedAnalyticCourses = selectedAnalyticCourses.substring(0, selectedAnalyticCourses.length - 1); }

    selectedAnalyticCoursesField.val(selectedAnalyticCourses);
}

function PopulateHiddenFieldsForDynamicElements() {
    GetSelectedCoursesForHiddenField();
    GetSelectedCertificatesForHiddenField();
}

function AddCertificatesToAnalytic() {
    var AnalyticCertificatesListContainer = $("#AnalyticCertificatesList_Container");
    var selectedCertificates = $('select#SelectCertificateTitleListBox').val();
    var selectedAnalyticCertificates = $("#AnalyticCertificatesList_Container").children().length;
    if (selectedCertificates != null && selectedAnalyticCertificates <= MaxCertificatesSelection) {

        //decide max no of loops to be run
        var count;
        if (selectedAnalyticCertificates + selectedCertificates.length <= MaxCertificatesSelection) {
            count = selectedCertificates.length;
        }
        else {
            count = MaxCertificatesSelection - selectedAnalyticCertificates
        }
        for (var i = 0; i < count; i++) {
            if (!$("#Certificate_" + selectedCertificates[i]).length) {
                // add the selected certificate to the certificate list container
                var itemContainerDiv = $("<div id=\"Certificate_" + selectedCertificates[i] + "\">" + "<img onclick=\"javascript:RemoveCertificatesFromAnalytic('" + selectedCertificates[i] + "');\" src=\"" + DeleteImagePath + "\" style=\"cursor:pointer;\" />" + $("#SelectCertificateTitleListBox option[value='" + selectedCertificates[i] + "']").text() + "</div>");
                itemContainerDiv.appendTo(AnalyticCertificatesListContainer);
            }

            // remove the certificate from the select list
            $("#SelectCertificateTitle option[value='" + selectedCertificates[i] + "']").remove();
        }
    }
}

function RemoveCertificatesFromAnalytic(certificateId) {
    $("#Certificate_" + certificateId).remove();
}

function GetSelectedCertificatesForHiddenField() {

    var AnalyticCertificatesListContainer = $("#AnalyticCertificatesList_Container");
    var selectedAnalyticCertificatesField = $("#SelectedAnalyticCertificates_Field");
    var selectedAnalyticCertificates = "";

    AnalyticCertificatesListContainer.children().each(function () {
        selectedAnalyticCertificates = selectedAnalyticCertificates + $(this).prop("id").replace("Certificate_", "") + ",";
    });

    if (selectedAnalyticCertificates.length > 0)
    { selectedAnalyticCertificates = selectedAnalyticCertificates.substring(0, selectedAnalyticCertificates.length - 1); }

    selectedAnalyticCertificatesField.val(selectedAnalyticCertificates);
}

//used for user and group snap shot datset
function displayOrderFeildsForGroup(radioButtonList) {
    for (var i = 0; i < radioButtonList.rows[0].cells.length; ++i) {

        if (radioButtonList.rows[0].cells[i].firstChild.checked) {
            radioButtonList.rows[0].cells[i].firstChild.checked = true;
            if (radioButtonList.rows[0].cells[i].firstChild.value == 2) {
                $("#OrderOptionFieldContainer").css("display", "block");
                $("#OrderColumnOptionFieldContainer").css("display", "block");
            }
            else {
                $("#OrderOptionFieldContainer").css("display", "none");
                $("#OrderColumnOptionFieldContainer").css("display", "none");
            }
        }
    }
}

//used for course completions datset
function showAndHideFrequencyListAccordingToActivity(ele) {
    var registrationType = parseInt($("#Activity_Field").find(":checked").val());
    $("#Frequency_Field").val('Select');
    if (registrationType == 1) {
        $("#Frequency_Container").css("display", "block");
    }
    else if (registrationType == 2) {
        $("#Frequency_Container").css("display", "none");
    }
}

function GetSelectedCourses() {
    var selectedAnalyticCourses = $("#AnalyticCoursesList_Container").children().length;

    AnalyticCoursesListContainer.children().each(function () {
        selectedAnalyticCourses = selectedAnalyticCourses + $(this).prop("id").replace("Course_", "") + ",";
    });
    alert(selectedAnalyticCourses.length);

    if (selectedAnalyticCourses.length > 0)
    { selectedAnalyticCourses = selectedAnalyticCourses.substring(0, selectedAnalyticCourses.length - 1); }

    selectedAnalyticCoursesField.val(selectedAnalyticCourses);
    alert(selectedAnalyticCourses.length);
}


$(document).ready(function () {
    $("#AnalyticCoursesList_Container").sortable()
    $("#AnalyticCertificatesList_Container").sortable();
    $("#AnalyticCoursesList_Container").sortable()
    showAndHideFrequencyListAccordingToActivity();
});



