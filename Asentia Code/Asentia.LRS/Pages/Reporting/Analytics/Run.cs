﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LRS.Library;
using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;

namespace Asentia.LRS.Pages.Reporting.Analytics
{
    public class Run : AsentiaAuthenticatedPage
    {

        #region Properties
        public Panel AnalyticResultContainer;
        public Panel ActionsPanel;
        public Panel PageInstructionsPanel;
        #endregion

        #region Private Properties
        private int _ChartTypeId;
        private int _Frequency;
        private Button _AnlyticCancelButton;
        private Button _CreateNewAnalyticButton;
        private int _IdDataSet;
        #endregion

        #region Page_Load
        /// <summary>
        /// page load method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Page_Load(object sender, EventArgs e)
        {
            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/reporting/analytics/Run.css");

            // Gets an ID from loading a page
            this._GetIDFromLoadingPage();

            //build the breadcrumb
            this._BuildBreadcrumbAndPageTitle();

            //build action panel
            this._BuildAnalyticResultActionsPanel();

            this.InitializeAdminMenu();

            //create analytic chart
            this._OutputAnalytic();

        }
        #endregion

        #region _BuildInstructionPanel
        /// <summary>
        /// creates instruction panle according to idDataset
        /// </summary>
        private void _BuildInstructionPanel()
        {
            string infoText;
            if (this._IdDataSet > 0)
            {
                //getting the information text of chart description
                switch ((EnumDataSetAnalytic)this._IdDataSet)
                {
                    case EnumDataSetAnalytic.LoginActivity:
                        infoText = _GlobalResources.LoginActivity;
                        break;
                    case EnumDataSetAnalytic.CourseCompletions:
                        infoText = _GlobalResources.CourseCompletions;
                        break;
                    case EnumDataSetAnalytic.UseStatistics:
                        infoText = _GlobalResources.LastUseDistribution;
                        break;
                    case EnumDataSetAnalytic.CertificatesAnalytics:
                        infoText = _GlobalResources.Certificates;
                        break;
                    case EnumDataSetAnalytic.UserGroupSnapshot:
                        infoText = _GlobalResources.DisplaysTheUserActiveInactiveDisabledStatisticsGroupStatistics;
                        break;
                    default:
                        infoText = "  ";
                        break;
                }

                //gets the text for chart title
                if (this._Frequency > 0)
                {
                    infoText = infoText + " " + _GlobalResources.Per + " " + (EnumFrequencyAnalytic)Convert.ToInt32(this._Frequency);
                }

                this.FormatPageInformationPanel(this.PageInstructionsPanel, infoText, true);
            }

        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        ///  build the breadcrumb
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Analytic));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.Reporting, _GlobalResources.AnalyticChart, ImageFiles.GetIconPath(ImageFiles.ICON_ANALYTICS,
                                                                                                                       ImageFiles.EXT_PNG));

        }
        #endregion

        #region _GetIDFromLoadingPage
        /// <summary>
        /// Gets an ID from loading a page
        /// </summary>
        private void _GetIDFromLoadingPage()
        {
            // get the id querystring parameter
            int ctId = this.QueryStringInt("id", 0);
            this._IdDataSet = this.QueryStringInt("dsid", 0);
            if (ctId > 0)
            {
                if (ctId > 0)
                { this._ChartTypeId = ctId; }
            }
            else
            {

                Response.Redirect("~/reporting/analytics");
            }

        }
        #endregion

        #region _OutputAnalytic
        /// <summary>
        /// Generate Analytic and put the links to the files
        /// </summary>
        private void _OutputAnalytic()
        {
            DataSet dsAnalyticData = new DataSet();
            if (Session["AnalyticData"] != null)
            {
                dsAnalyticData = (DataSet)Session["AnalyticData"];
                Session.Remove("AnalyticData");
            }
            else
            {
                this.DisplayFeedback(_GlobalResources.NoRecordFoundPleasechangeCriteriaAndTryAgain, true);
            }
            if (Session["Frequency"] != null)
            {
                this._Frequency = Convert.ToInt32(Session["Frequency"]);
                Session.Remove("Frequency");
            }
            else
            {
                this.DisplayFeedback(_GlobalResources.NoRecordFoundPleasechangeCriteriaAndTryAgain, true);
            }
            if (dsAnalyticData.Tables.Count > 0)
            {
                // format a page information panel with page instructions
                this._BuildInstructionPanel();

                Panel ChartContainer = new Panel();
                ChartContainer.ID = "ChartContainer";
                ChartContainer.CssClass = "FormFieldContainer";


                Table chartingTable = new Table();
                chartingTable.ID = "ChartingTable";

                TableRow dataRow = new TableRow();

                // DATA
                TableCell dataCell1 = new TableCell();
                TableCell dataCell2 = new TableCell();

                Panel legendContainer = new Panel();
                legendContainer.CssClass = "legendContainer";

                Panel legendSaperator = new Panel();
                legendSaperator.CssClass = "legendSaperator";

                //finding the largest value in datatable
                int maxTableValue = int.MinValue;
                foreach (DataTable dt in dsAnalyticData.Tables)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        int accountLevel = dt.Rows[i].Field<int>("Count");
                        maxTableValue = Math.Max(maxTableValue, accountLevel);
                    }
                }
                if ((EnumChartTypeAnalytic)this._ChartTypeId == EnumChartTypeAnalytic.ChartBar)
                {
                    Chart_Bar barChart = new Chart_Bar("barChart", 700, 400);
                    barChart.ScaleOverride = false;
                    //ScaleOverride is false means it will decide automaticaly
                    if (barChart.ScaleOverride)
                    {
                        barChart.ScaleSteps = 11;
                        barChart.ScaleStepWidth = this._FindChartScaleWidth(maxTableValue);
                        barChart.ScaleStartValue = 0;
                    }

                    //add label text displayed on x-axis
                    for (int i = 0; i < dsAnalyticData.Tables[0].Rows.Count; i++)
                    {
                        barChart.AddLabel(this._GetLabelText(this._Frequency, dsAnalyticData.Tables[0].Rows[i]["Frequency"]));
                    }
                    var random = new Random();
                    foreach (DataTable dtanalytic in dsAnalyticData.Tables)
                    {
                        //assign values of y-axis
                        object[] count = new object[dtanalytic.Rows.Count];
                        for (int i = 0; i < dtanalytic.Rows.Count; i++)
                        {
                            count[i] = Convert.ToInt32(dtanalytic.Rows[i]["Count"]);
                        }

                        //create and assign dynamic color
                        var color = String.Format("#{0:X6}", random.Next(0x1000000));
                        Chart_Bar_Dataset dataset = new Chart_Bar_Dataset(color, color, count);
                        barChart.AddDataset(dataset);

                        Panel legendBox = new Panel();
                        legendBox.CssClass = "box";
                        legendBox.Attributes.Add("style", "background-color:" + color);

                        Panel legendText = new Panel();
                        legendText.CssClass = "legend";

                        Literal legendValue = new Literal();
                        legendValue.Text = Convert.ToString(dtanalytic.Rows[0]["LegendName"]);

                        legendText.Controls.Add(legendValue);

                        legendSaperator.Controls.Add(legendBox);
                        legendSaperator.Controls.Add(legendText);

                        legendContainer.Controls.Add(legendSaperator);
                    }
                    dataCell2.Controls.Add(legendContainer);
                    dataCell1.Controls.Add(barChart);
                }
                else if ((EnumChartTypeAnalytic)this._ChartTypeId == EnumChartTypeAnalytic.ChatLine)
                {
                    Chart_Line lineChart = new Chart_Line("lineChart", 700, 400);
                    lineChart.ScaleOverride = false;
                    //ScaleOverride is false means it will decide automaticaly
                    if (lineChart.ScaleOverride)
                    {
                        lineChart.ScaleSteps = 11;
                        lineChart.ScaleStepWidth = this._FindChartScaleWidth(maxTableValue);
                        lineChart.ScaleStartValue = 0;
                    }

                    //add label text displayed on x-axis
                    for (int i = 0; i < dsAnalyticData.Tables[0].Rows.Count; i++)
                    {
                        lineChart.AddLabel(this._GetLabelText(this._Frequency, dsAnalyticData.Tables[0].Rows[i]["Frequency"]));
                    }

                    var random = new Random();
                    foreach (DataTable dtanalytic in dsAnalyticData.Tables)
                    {
                        //assign values of y-axis
                        object[] count = new object[dtanalytic.Rows.Count];
                        for (int i = 0; i < dtanalytic.Rows.Count; i++)
                        {
                            count[i] = Convert.ToInt32(dtanalytic.Rows[i]["Count"]);
                        }

                        //create and assign dynamic color
                        var randomColor = random.Next(300);
                        var color = randomColor.ToString("000");

                        var strokeColor = string.Format("rgba({0},{0},{0},1)", color);
                        var fillColor = string.Format("rgba({0},{0},{0},0.2)", color);
                        Chart_Line_Dataset dataset = new Chart_Line_Dataset(fillColor, strokeColor, "#fff", "#fff", count);
                        lineChart.AddDataset(dataset);

                        Panel legendBox = new Panel();
                        legendBox.CssClass = "box";
                        legendBox.Attributes.Add("style", "background-color:" + fillColor);

                        Panel legendText = new Panel();
                        legendText.CssClass = "legend";

                        Literal legendValue = new Literal();
                        legendValue.Text = Convert.ToString(dtanalytic.Rows[0]["LegendName"]);

                        legendText.Controls.Add(legendValue);

                        legendSaperator.Controls.Add(legendBox);
                        legendSaperator.Controls.Add(legendText);

                        legendContainer.Controls.Add(legendSaperator);
                    }
                    dataCell1.Controls.Add(lineChart);
                    dataCell2.Controls.Add(legendContainer);
                }
                else if ((EnumChartTypeAnalytic)this._ChartTypeId == EnumChartTypeAnalytic.ChartPie)
                {
                    foreach (DataTable dtanalytic in dsAnalyticData.Tables)
                    {
                        Chart_Pie PieChart = new Chart_Pie("pieChart", 700, 225);

                        DataSet ds = new DataSet();
                        DataTable table = new DataTable();
                        var random = new Random();
                        for (int i = 0; i < dtanalytic.Rows.Count; i++)
                        {
                            var color = String.Format("#{0:X6}", random.Next(0x1000000));
                            Chart_Pie_Dataset dataset = new Chart_Pie_Dataset(color, dtanalytic.Rows[i]["Count"]);
                            PieChart.AddDataset(dataset);
                        }
                        dataCell1.Controls.Add(PieChart);
                    }
                }

                //addinf table cell to table row
                dataRow.Cells.Add(dataCell1);
                dataRow.Cells.Add(dataCell2);

                //adding row to table
                chartingTable.Rows.Add(dataRow);
                //adding chart table to chart container
                ChartContainer.Controls.Add(chartingTable);

                //adding controls to page
                this.AnalyticResultContainer.Controls.Add(ChartContainer);
            }
            else
            {
                this.DisplayFeedback(_GlobalResources.NoRecordFoundPleasechangeCriteriaAndTryAgain, true);
            }
            dsAnalyticData.Dispose();
        }
        #endregion

        #region _FindChartScaleWidth
        /// <summary>
        /// return chart scale width according to maximum value returned
        /// </summary>
        /// <param name="maxTableValue"></param>
        /// <returns></returns>
        private int _FindChartScaleWidth(int maxTableValue)
        {
            if (maxTableValue > 10)
            {
                if (maxTableValue % 10 > 0)
                {
                    return (maxTableValue / 10) + 1;
                }
                else
                {
                    return maxTableValue / 10;
                }
            }
            else
            {
                return 1;
            }
        }
        #endregion

        #region _BuildAnalyticResultActionsPanel
        /// <summary>
        /// Builds the container and buttons for analytic result actions.
        /// </summary>
        private void _BuildAnalyticResultActionsPanel()
        {
            // clear controls from container
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            //  button for creating new analytic
            this._CreateNewAnalyticButton = new Button();
            this._CreateNewAnalyticButton.ID = "CreateNewAnalyticButton";
            this._CreateNewAnalyticButton.CssClass = "Button ActionButton";
            this._CreateNewAnalyticButton.Text = _GlobalResources.CreateAnalytic;
            this._CreateNewAnalyticButton.Command += new CommandEventHandler(this._CreateNewAnalyticButton_Command);
            this.ActionsPanel.Controls.Add(this._CreateNewAnalyticButton);

            // cancel button
            this._AnlyticCancelButton = new Button();
            this._AnlyticCancelButton.ID = "CancelButton";
            this._AnlyticCancelButton.CssClass = "Button NonActionButton";
            this._AnlyticCancelButton.Text = _GlobalResources.Cancel;
            this._AnlyticCancelButton.Command += new CommandEventHandler(this._AnlyticCancelButton_Command);
            this.ActionsPanel.Controls.Add(this._AnlyticCancelButton);
        }
        #endregion

        #region _CreateNewAnalyticActionsPanel
        /// <summary>
        /// Handles the "Create New Analytic" button click .
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CreateNewAnalyticButton_Command(object sender, CommandEventArgs e)
        {
            if (this._IdDataSet > 0)
            { Response.Redirect("~/reporting/analytics/Modify.aspx?idDataSet=" + this._IdDataSet.ToString()); }
            else
            { Response.Redirect("~/reporting/analytics"); }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click .
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _AnlyticCancelButton_Command(object sender, CommandEventArgs e)
        {

            Response.Redirect("~/reporting/analytics");
        }
        #endregion

        #region _GetLabelText
        /// <summary>
        /// gives the text of labels at X in chart.
        /// </summary>
        /// <param name="frequency"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private string _GetLabelText(int frequency, object value)
        {
            string text;
            if ((EnumFrequencyAnalytic)frequency == EnumFrequencyAnalytic.Month)
            {
                text = Convert.ToString((EnumMonthAnalytic)value);
            }
            else if ((EnumFrequencyAnalytic)frequency == EnumFrequencyAnalytic.Day)
            {
                text = Convert.ToString((EnumDayAnalytic)value);
            }
            else if ((EnumFrequencyAnalytic)frequency == EnumFrequencyAnalytic.Week)
            {
                text = _GlobalResources.Week + " " + Convert.ToString(value);
            }
            else if ((EnumFrequencyAnalytic)frequency == EnumFrequencyAnalytic.Year)
            {
                text = Convert.ToString(value);
            }
            else if ((EnumFrequencyAnalytic)frequency == EnumFrequencyAnalytic.Hour)
            {
                text = String.Format("{0:0.00}", value);
            }
            else
            {
                text = Convert.ToString(value); ;
            }
            return text;

        }
        #endregion
    }
}
