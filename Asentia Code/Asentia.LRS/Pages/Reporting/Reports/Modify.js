﻿
function OnRunClick()
{
    //click the run hidden link button to show modal
    UpdateSelectedFieldsString();
    UpdateQueryString();
    var orderByString = '';
    for (i = 0; i < $('.orderDDL').length; i++) {
        orderByString += " [" + $("option:selected", $('.orderDDL').eq(i)).val() + "] ";
        orderByString += $('.orderRadioList').eq(i).find(":checked").val() + ",";
    }
    $("#hdReportOrderBy").val(orderByString);
    document.getElementById(RunHiddenButton).click();
    document.getElementById(ReportTypeModalButton).click();
}

// This function will handle the end request event
function requestEndHandler(sender, args) {
    if (args.get_error()) {
        document.getElementById("ReportTypesModalModalPopupBody").innerHTML =
           "<div id=\"ReportTypesModalModalPopupFeedbackContainer\" class=\"ModalFeedbackError\"> " +
	       "<div id=\"ReportTypesModalModalFeedbackErrorIconContainer\" class=\"ModalFeedbackErrorIconContainer\"> " +
		   "<img class=\"SmallIcon\" src=\"" + ReportTimeoutErrorImagePath + "\"> " +
	       "</div><div id=\"ReportTypesModalModalFeedbackTextContainer\" class=\"ModalFeedbackTextContainer\"> " +
		   "<p class=\"ModalFeedbackErrorHeader\">Error</p><p class=\"ModalFeedbackMessage\">" + ReportTimeoutErrorMessage + "</p> " +
	       "</div> " +
           "</div>";
        args.set_errorHandled(true);
    }
}

function SaveFileOnClick(fileName)
{
    $('#HiddenFileNameString').val(fileName);
}

