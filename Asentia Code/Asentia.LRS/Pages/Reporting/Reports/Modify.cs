﻿using Asentia.Common;
using Asentia.Controls;
using Asentia.LRS.Controls;
using Asentia.LRS.Library;
using Asentia.UMS.Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows;


namespace Asentia.LRS.Pages.Reporting.Reports
{
    public class Modify : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ObjectOptionsPanel;
        public Panel ReportFormContentWrapperContainer;
        public Panel ReportWrapperContainer;
        public Panel PageInstructionsPanel;
        public Panel ReportFormContainer;
        public Panel ReportTypeModal;
        public Panel ActionsPanel;
        public ModalPopup GridConfirmAction = new ModalPopup();

        #endregion

        #region Private Properties
        private EcommerceSettings _EcommerceSettings;
        private Report _ReportObject;
        private ReportForm _ReportForm;
        private ReportDataSet _DataSet = new ReportDataSet();
        private Panel _FormatPanel = new Panel();
        private Panel _PanelHTMLOuter = new Panel();
        private Panel _PanelCSVOuter = new Panel();
        private Panel _PanelPDFOuter = new Panel();
        private Panel _PanelSaveOuter = new Panel();
        private RadioButtonList _RadioButtonListIsPublic = new RadioButtonList();
        private LinkButton _AddShortcut, _RemoveShortcut;
        private Button _SaveButton = new Button();
        private LinkButton _SaveIconLink = new LinkButton();
        private LinkButton _SaveTextLink = new LinkButton();
        private Button _CancelButton = new Button();
        private Button _RunButton = new Button();
        private Button _UpdateButton = new Button();
        private bool _CreateReport;
        private ModalPopup _ReportTypeModal;
        private Button _ModalHiddenButton;
        private HiddenField _HiddenFileNameString;
        private LinkButton _RunHiddenButton;
        private HyperLink _HtmlIconLink, _CsvIconLink, _PdfIconLink, _HtmlTextLink, _CsvTextLink, _PdfTextLink;
        private const int _REPORT_PDF_COLUMNS_MAX = 8;
        private const int _REPORT_PDF_ROWS_MAX = 1000;
        private const int _REPORT_HTML_COLUMNS_MAX = 1000;
        private const int _REPORT_HTML_ROWS_MAX = 1000;
        private double _HtmlSize = 0, _CsvSize = 0, _PdfSize = 0;
        #endregion

        #region Page Init Event
        public void Page_Init(object sender, EventArgs e)
        {
            this._RunHiddenButton = new LinkButton();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;
            csm.RegisterClientScriptResource(typeof(Modify), "Asentia.LRS.Pages.Reporting.Reports.Modify.js");
            
            csm.RegisterStartupScript(typeof(Modify), "JsVariables", "var RunHiddenButton ='" + this._RunHiddenButton.ID + "'; "
                + "var ReportTimeoutErrorMessage = '" + _GlobalResources.TheReportYouAreRunningHasReachedItsExecutionTimeoutLimit + "'; "
                + "var ReportTimeoutErrorImagePath = '" + ImageFiles.GetIconPath(ImageFiles.ICON_ALERT_ERROR_RED, ImageFiles.EXT_PNG)  + "'; "
                + "var ReportTypeModalButton = '" + this._ModalHiddenButton.ID + "'; "
                + "Sys.WebForms.PageRequestManager.getInstance().add_endRequest(requestEndHandler);", true);
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // set timeout for the report
            Server.ScriptTimeout = 600;

            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // get the report object
            this._GetReportObject();

            // check permissions
            if (                
                (this._ReportObject.IdDataset == 1 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserDemographics))
                || (this._ReportObject.IdDataset == 2 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCourseTranscripts))
                || (this._ReportObject.IdDataset == 3 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation))
                || (this._ReportObject.IdDataset == 4 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Certificates))
                //|| (this._ReportObject.IdDataset == 5 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_XAPI))
                || (this._ReportObject.IdDataset == 6 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts))
                || (this._ReportObject.IdDataset == 7 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts))
                || (this._ReportObject.IdDataset == 8 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Purchases))
                || (this._ReportObject.IdDataset == 8 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Purchases) && !this._EcommerceSettings.IsEcommerceSetAndVerified)
                || (this._ReportObject.IdDataset == 9 && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts))
                || (this._ReportObject.IdDataset == 9 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts) && !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
               )
            { Response.Redirect("/"); }

            // include page-specific css files
            this.IncludePageSpecificCssFile("page-specific/reporting/reports/Modify.css");

            // get the report object
            this._GetReportObject();

            this._BuildBreadcrumbAndPageTitle();

            Enum.GetName(typeof(EnumDataSet), this._ReportObject.IdDataset);

            this.InitializeAdminMenu();
            if (!this._CreateReport)
            {
                this._BuildObjectOptionsPanel();
            }
            this.ReportFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ReportWrapperContainer.CssClass = "FormContentContainer";

            // format a page information panel with page instructions
            this.FormatPageInformationPanel(this.PageInstructionsPanel, _GlobalResources.UseTheInterfaceBelowToSelectTheColumnsAndFiltersToApplyToThisReport, true);

            // build the report form
            this._BuildReportForm();

            // build the grid action modal
            this._BuildGridActionsModal();

            // build the form actions panel
            this._BuildActionsPanel();

            // build the report type modal
            this._BuildReportTypesModal(this._RunHiddenButton.ID, this.ReportTypeModal);


            if (this.QueryStringString("mode", string.Empty).Length > 0)
            {
                this.DisplayFeedback(_GlobalResources.TheReportFileHasBeenSavedSuccessfully, false);
            }
        }
        #endregion

        #region _BuildObjectOptionsPanel
        /// <summary>
        /// Builds the options panel for the object, i.e. "add" links, etc.
        /// </summary>
        private void _BuildObjectOptionsPanel()
        {
            this.ObjectOptionsPanel.CssClass = "ObjectOptionsPanel";

            Panel optionsPanelLinksContainer = new Panel();
            optionsPanelLinksContainer.ID = "OptionsPanelLinksContainer";
            optionsPanelLinksContainer.CssClass = "OptionsPanelLinksContainer";

            if (this._ReportObject.HasShortcut)
            {
                this._RemoveShortcut = new LinkButton();
                this._RemoveShortcut.ID = "RemoveShortcutButton";
                this._RemoveShortcut.Command += new CommandEventHandler(_RemoveShortcutButton_Command);

                // Remove Shortcut
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("RemoveShortcutLink",
                                                    _RemoveShortcut,
                                                    null,
                                                    null,
                                                    _GlobalResources.RemoveReportShortcut,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_X, ImageFiles.EXT_PNG))
                    );
            }
            else
            {
                this._AddShortcut = new LinkButton();
                this._AddShortcut.ID = "AddShortcutButton";
                this._AddShortcut.Command += new CommandEventHandler(_AddShortcutButton_Command);

                // Add Shortcut
                optionsPanelLinksContainer.Controls.Add(
                    this.BuildOptionsPanelImageLink("AddShortcutLink",
                                                    _AddShortcut,
                                                    null,
                                                    null,
                                                    _GlobalResources.AddReportShortcut,
                                                    null,
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG),
                                                    ImageFiles.GetIconPath(ImageFiles.ICON_OVERLAY_PLUS, ImageFiles.EXT_PNG))
                    );
            }

            this.ObjectOptionsPanel.Controls.Add(optionsPanelLinksContainer);
        }
        #endregion

        #region _BuildBreadcrumbAndPageTitle
        /// <summary>
        /// Builds the breadcrumb and page title.
        /// </summary>
        private void _BuildBreadcrumbAndPageTitle()
        {
            // build the page title
            ReportDataSet reportDataSetObject = new ReportDataSet();
            string dataSetName = reportDataSetObject.GetReportDataSetListTable().Select("idDataset = " + this._ReportObject.IdDataset.ToString())[0]["name"].ToString();

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Reports, "/reporting/reports"));
            breadCrumbLinks.Add(new BreadcrumbLink(this._CreateReport ? _GlobalResources.NewReport + " " + _GlobalResources.DatasetX.Replace("##Dataset##", dataSetName) : this._ReportObject.Title + " " + _GlobalResources.DatasetX.Replace("##Dataset##", dataSetName)));
            this.BuildBreadcrumb(breadCrumbLinks);

            this.BuildPageTitle(PageCategoryForTitle.Reporting,
                                this._CreateReport ? _GlobalResources.NewReport + " " + _GlobalResources.DatasetX.Replace("##Dataset##", dataSetName) : this._ReportObject.Title + " " + _GlobalResources.DatasetX.Replace("##Dataset##", dataSetName),
                                ImageFiles.GetIconPath(ImageFiles.ICON_REPORT,
                                                       ImageFiles.EXT_PNG));
        }
        #endregion

        #region _GetReportObject
        /// <summary>
        /// Gets a report object based on either viewstate or querystring if exists.
        /// </summary>
        private void _GetReportObject()
        {
            int idReport = _GetIDFromLoadingPage("idReport");
            int idDataset = _GetIDFromLoadingPage("idDataset");

            try
            {
                if (idReport > 0)
                {
                    this._ReportObject = new Report(idReport);
                    this._CreateReport = false;
                }
                else if (idDataset > 0)
                {
                    this._ReportObject = new Report();
                    this._ReportObject.IdDataset = idDataset;
                    this._CreateReport = true;
                }

            }
            catch
            {
                // bubble the exception up
                throw;
            }

        }
        #endregion

        #region _GetIDFromLoadingPage
        /// <summary>
        /// Gets an ID from loading a page
        /// </summary>
        private int _GetIDFromLoadingPage(string ID)
        {
            // get the id querystring parameter
            int qsId = this.QueryStringInt(ID, 0);
            int vsId = this.ViewStateInt(this.ViewState, ID, 0);
            int id = 0;

            if (qsId > 0 || vsId > 0)
            {
                if (qsId > 0)
                { id = qsId; }

                if (vsId > 0)
                { id = vsId; }
            }
            return id;
        }
        #endregion

        #region _BuildReportForm
        /// <summary>
        /// Builds the report form.
        /// </summary>
        private void _BuildReportForm()
        {
            this._ReportForm = new ReportForm("ReportModify", UserAccountDataFileType.Site, AsentiaSessionState.UserCulture, this._ReportObject);

            // LANGUAGE SPECIFIC PROPERTIES

            // title
            bool isDefaultPopulated = false;

            foreach (Report.LanguageSpecificProperty reportLanguageSpecificProperty in this._ReportObject.LanguageSpecificProperties)
            {

                // if the language is the default language, populate the control directly attached to this page,
                // and set the isDefaultPopulated flag; otherwise, find the language-specific control and populate 
                // it; note that if we cannot populate the controls directly attached to this page (default) from
                // language-specific properties, we will use the values in the properties that come from the base table
                if (reportLanguageSpecificProperty.LangString == AsentiaSessionState.GlobalSiteObject.LanguageString)
                {
                    this._ReportForm.ReportTitleTextBox.Text = reportLanguageSpecificProperty.Title;

                    isDefaultPopulated = true;
                }
                else
                {
                    // get text box
                    TextBox languageSpecificReportTitleTextBox = (TextBox)this._ReportForm.TabPropertyContentPanel.FindControl(this._ReportForm.ReportTitleTextBox.ID + "_" + reportLanguageSpecificProperty.LangString);

                    //if the text boxes were found, set the text box values to the language-specific value
                    if (languageSpecificReportTitleTextBox != null)
                    { languageSpecificReportTitleTextBox.Text = reportLanguageSpecificProperty.Title; }
                }
            }

            if (!isDefaultPopulated)
            {
                this._ReportForm.ReportTitleTextBox.Text = this._ReportObject.Title;
            }

            this.ReportFormContainer.Controls.Add(this._ReportForm);

            // certificate html hidden field
            this._HiddenFileNameString = new HiddenField();
            this._HiddenFileNameString.ClientIDMode = ClientIDMode.Static;
            this._HiddenFileNameString.ID = "HiddenFileNameString";
            this.ReportFormContainer.Controls.Add(this._HiddenFileNameString);
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for form actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            this.ActionsPanel.Controls.Clear();

            // style actions panel
            this.ActionsPanel.CssClass = "ActionsPanel";

            // run button
            this._RunButton.ID = "RunReportButton";
            this._RunButton.CssClass = "Button ActionButton SaveButton";
            this._RunButton.Text = _GlobalResources.RunReport;
            this._RunButton.OnClientClick = "javascript:OnRunClick();return false;";
            this.ActionsPanel.Controls.Add(this._RunButton);

            // save button
            this._SaveButton.ID = "SaveReportButton";
            this._SaveButton.CssClass = "Button ActionButton SaveButton";

            this._SaveButton.Text = this._CreateReport ? _GlobalResources.CreateReport :_GlobalResources.SaveChanges;
            this._SaveButton.Command += new CommandEventHandler(_SaveButton_Command);
            this._SaveButton.Style.Add("display", this._CreateReport? "none" : "show");
            this.ActionsPanel.Controls.Add(this._SaveButton);

            // update subscription button
            this._UpdateButton.ID = "UpdateSubscriptionButton";
            this._UpdateButton.CssClass = "Button ActionButton SaveButton";
            this._UpdateButton.Text = _GlobalResources.UpdateSubscription;
            this._UpdateButton.Command += new CommandEventHandler(_UpdateButton_Command);
            this._UpdateButton.Style.Add("display", "none");
            this.ActionsPanel.Controls.Add(this._UpdateButton);

            // cancel button
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(_CancelButton_Command);
            this.ActionsPanel.Controls.Add(this._CancelButton);

            this._RunHiddenButton.ID = "RunHiddenButton";
            this._RunHiddenButton.Text = _GlobalResources.RunReport;
            this._RunHiddenButton.Style.Add("display", "none");
            this.ActionsPanel.Controls.Add(_RunHiddenButton);

        }
        #endregion

        #region _SaveButton_Command
        /// <summary>
        /// Handles the "Save Changes" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // validate the form
                if (!this._ValidateForm())
                { throw new AsentiaException(); }

                // populate the object.SelectedItem.Text
                this._ReportObject.IdUser = this._ReportForm.IdUser;
                this._ReportObject.IdDataset = this._ReportForm.IdDataset;
                this._ReportObject.IdSite = AsentiaSessionState.IdSite;
                this._ReportObject.IsPublic = this._ReportForm.IsPublic;
                this._ReportObject.IsDeleted = false;
                this._ReportObject.Title = this._ReportForm.Title;
                this._ReportObject.Fields = this._ReportForm.Fields;
                this._ReportObject.Filter = this._ReportForm.Filter;
                this._ReportObject.Order = this._ReportForm.Order;

                // check whether the report owner is the login user or not, if not set user id to 0
                if (this._ReportObject != null && this._ReportObject.IdReport > 0)
                {
                    if (this._ReportObject.IdUser != AsentiaSessionState.IdSiteUser)
                    {
                        this._ReportObject.IdReport = 0;
                        this._ReportObject.IdUser = AsentiaSessionState.IdSiteUser;
                    }
                }

                // save the report, save its returned id to viewstate, and 
                // instansiate a new report object with the id
                int id = this._ReportObject.Save();
                this.ViewState["idReport"] = id;
                this._ReportObject.IdReport = id;

                // do group language-specific properties

                // get the available languages
                ArrayList availableLanguages = this.GetArrayListOfSiteAvailableInstalledLanguages();

                // loop through languages, grab values from inputs, and populate language specific properties
                foreach (string availableLanguage in availableLanguages)
                {
                    // get the culture of the info for the language
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo(availableLanguage);

                    // if this is the not the default language, get values from the language-specific text boxes
                    // and save the language-specific properties; default language is already taken care of in the
                    // object's Save procedure
                    if (cultureInfo.Name != AsentiaSessionState.GlobalSiteObject.LanguageString)
                    {
                        string reportTitle = null;

                        // get text boxes
                        TextBox languageSpecificReportTitleTextBox = (TextBox)this._ReportForm.TabPropertyContentPanel.FindControl(this._ReportForm.ReportTitleTextBox.ID + "_" + cultureInfo.Name);
                      
                        // if the text boxes were found and they have values, set the properties
                        if (languageSpecificReportTitleTextBox != null)
                        {
                            if (!String.IsNullOrWhiteSpace(languageSpecificReportTitleTextBox.Text))
                            { reportTitle = languageSpecificReportTitleTextBox.Text; }
                        }

                        // save the properties if at least one property is populated
                        if (!String.IsNullOrWhiteSpace(reportTitle))
                        {
                            this._ReportObject.SaveLang(cultureInfo.Name, reportTitle);
                        }
                    }
                }
                string feedbackText = this._CreateReport ? _GlobalResources.TheReportHasBeenCreatedSuccessfully : _GlobalResources.TheReportHasBeenSavedSuccessfully;

                this._CreateReport = false;
                this.ActionsPanel.Controls.Clear();
                this._BuildActionsPanel();

                this.ReportFormContainer.Controls.Clear();
                this._BuildReportForm();
                this._ReportForm.ReportTitleTextBox.Text = this._ReportObject.Title;

                this.ReportFormContainer.Controls.Add(this.GridConfirmAction);

                this.GridConfirmAction.ID = "DeleteConfirmModal";
                LinkButton saveFileDeleteButton = (LinkButton)this._ReportForm.SaveFileDeleteButton;
                saveFileDeleteButton.Style.Add("display", "none");
                this.ReportFormContainer.Controls.Add(saveFileDeleteButton);

                this._BuildBreadcrumbAndPageTitle();
                this.DisplayFeedback(feedbackText, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(_GlobalResources.PleaseCorrectTheErrorsBelowAndTryAgain, true);
            }
        }
        #endregion

        #region _ValidateForm
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateForm()
        {
            bool isValid = true;
            bool fieldsTabHasErrors = false;
            bool filterTabHasErrors = false;
            bool orderTabHasErrors = false;
            bool propertyTabHasErrors = false;
            bool subscriptionTabHasErrors = false;
            bool reportFilesTabHasErrors = false;

            // Report Fields field
            if (String.IsNullOrWhiteSpace(this._ReportForm.Fields))
            {
                isValid = false;
                fieldsTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this._ReportForm.TabFieldsContentPanel, "ReportFieldsControls", _GlobalResources.Columns + " " + _GlobalResources.IsRequired);
            }

            // Report TextBox field
            if ((this._ReportForm.Filter.Replace(" ", "").IndexOf("''")) > 0 || (this._ReportForm.Filter.Replace(" ", "").IndexOf("=)")) > 0)
            {
                isValid = false;
                filterTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this._ReportForm.TabFilterContentPanel, "ReportFilterControls", _GlobalResources.ReportFilterTextBoxCanNotBeEmpty);
            }

            // Report Title field
            if (String.IsNullOrWhiteSpace(this._ReportForm.Title))
            {
                isValid = false;
                propertyTabHasErrors = true;
                this.ApplyErrorMessageToFieldErrorPanel(this._ReportForm.TabPropertyContentPanel, "ReportTitleText", _GlobalResources.Title + " " + _GlobalResources.IsRequired);
            }

            // apply error image and class to tabs if they have errors
            if (fieldsTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this._ReportForm.TabFieldsContentPanel, "ReportModify_Fields_TabLI"); }

            if (filterTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this._ReportForm.TabFilterContentPanel, "ReportModify_Filter_TabLI"); }

            if (orderTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this._ReportForm.TabOrderContentPanel, "ReportModify_Order_TabLI"); }

            if (propertyTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this._ReportForm.TabPropertyContentPanel, "ReportModify_Property_TabLI"); }

            if (subscriptionTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this._ReportForm.TabSubscriptionContentPanel, "ReportModify_Subscription_TabLI"); }

            if (reportFilesTabHasErrors)
            { this.ApplyErrorImageAndClassToTab(this._ReportForm.TabReportFilesContentPanel, "ReportModify_ReportFiles_TabLI"); }

            return isValid;
        }
        #endregion

        #region _ValidateRunForm
        /// <summary>
        /// Validates the form when run button clicked.
        /// </summary>
        /// <returns>true/false</returns>
        private string _ValidateRunForm()
        {
            string errorString = String.Empty;

            // Report Fields field
            if (String.IsNullOrWhiteSpace(this._ReportForm.Fields))
            {
                errorString = _GlobalResources.Columns + " " + _GlobalResources.IsRequired;
            }

            // Report Filter field
            if ((this._ReportForm.Filter.Replace(" ", "").IndexOf("''")) > 0 || (this._ReportForm.Filter.Replace(" ", "").IndexOf("=)")) > 0)
            {
                errorString = (String.IsNullOrWhiteSpace(errorString) ? String.Empty : errorString.Remove(errorString.Length - 1) + " " +
                                _GlobalResources.and_lower + " ") + _GlobalResources.ReportFilterTextBoxCanNotBeEmpty;
            }

            return errorString;
        }
        #endregion

        #region _ValidateSubscription
        /// <summary>
        /// Validates the entry for subscription.
        /// </summary>
        /// <returns>true/false</returns>
        private bool _ValidateSubscription()
        {
            bool isValid = true;
            //validate the starting time field
            if (this._ReportForm.DTStart < DateTime.Today)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._ReportForm.TabSubscriptionContentPanel, "ReportSubscriptionStartDate",
                    _GlobalResources.StartDate + " " + _GlobalResources.CannotBeEarlierThan + " " + _GlobalResources.R_D0);
            }

            //validate the frequency field 
            int shortestDays;
            int allowedMin = 1;

            if (!String.IsNullOrWhiteSpace(this._ReportForm._GetShortestDaysValue()))
            {
               shortestDays = Convert.ToInt32(this._ReportForm._GetShortestDaysValue());


            string timeFrame = this._ReportForm.RecurTimeFrame;
                        
                switch (timeFrame) {
                    case "d":
                        allowedMin = shortestDays;
                        break;
                    case "ww":
                        allowedMin = (int)Math.Ceiling(Convert.ToDecimal(shortestDays / 7));
                        break;
                    case "m":
                        allowedMin = (int)Math.Ceiling(Convert.ToDecimal(shortestDays / 31));
                        break;
                    case "yyyy":
                        allowedMin = (int)Math.Ceiling(Convert.ToDecimal(shortestDays / 365));
                        break;
                    default:
                        allowedMin = shortestDays;
                        break;
                }
            }
            if (String.IsNullOrWhiteSpace(this._ReportForm.RecurInterval) || this._ReportForm.RecurInterval == "0")
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._ReportForm.TabSubscriptionContentPanel, "ReportSubscriptionFrequency",
                    _GlobalResources.Frequency + " " + _GlobalResources.IsRequired);
            }
            else if(!int.TryParse(this._ReportForm.RecurInterval, out shortestDays))
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._ReportForm.TabSubscriptionContentPanel, "ReportSubscriptionFrequency",
                    "Only " + _GlobalResources.Integer + " " + _GlobalResources.AreAllowed);
            }
            else if(Convert.ToInt32(this._ReportForm.RecurInterval) < allowedMin)
            {
                isValid = false;
                this.ApplyErrorMessageToFieldErrorPanel(this._ReportForm.TabSubscriptionContentPanel, "ReportSubscriptionFrequency",
                    _GlobalResources.TheNumberOfTheRecurrenceIntervalCannotBeSmaller.Replace("#s#", allowedMin.ToString()));
            }

            if (!isValid)
            { this.ApplyErrorImageAndClassToTab(this._ReportForm.TabSubscriptionContentPanel, "ReportModify_Subscription_TabLI"); }

            return isValid;
        }
        #endregion

        #region _SaveFileButton_Command
        /// <summary>
        /// Handles the "Save Report File" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _SaveFileButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                // create ReportFile object and populate the data
                string filePath = SitePathConstants.SITE_WAREHOUSE_REPORTDATA + this._HiddenFileNameString.Value;
                ReportFile rfObject = new ReportFile();

                if (File.Exists(MapPathSecure(filePath + ".html"))) 
                {
                    this._HtmlSize = new System.IO.FileInfo(MapPathSecure(filePath + ".html")).Length;
                    rfObject.HtmlKb = unchecked((int)Math.Ceiling(this._HtmlSize/1024));
                } 
                else
                {
                    rfObject.HtmlKb = null;
                }
                if (File.Exists(MapPathSecure(filePath + ".csv"))) 
                {
                    this._CsvSize = new System.IO.FileInfo(MapPathSecure(filePath + ".csv")).Length;
                    rfObject.CsvKb = unchecked((int)Math.Ceiling(this._CsvSize / 1024));
                }
                else
                {
                    rfObject.CsvKb = null;
                }
                if (File.Exists(MapPathSecure(filePath + ".pdf"))) 
                {
                    this._PdfSize = new System.IO.FileInfo(MapPathSecure(filePath + ".pdf")).Length;
                    rfObject.PdfKb = unchecked((int)Math.Ceiling(this._PdfSize / 1024));
                }
                else
                {
                    rfObject.PdfKb = null;
                }
                
                rfObject.IdReport = this._ReportObject.IdReport;
                rfObject.Filename = this._HiddenFileNameString.Value;             
                rfObject.Save();

                var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
                nameValues.Set("mode", "savefile");
                string url = Request.Url.AbsolutePath;
                string updatedQueryString = "?" + nameValues.ToString();
                Response.Redirect(url + updatedQueryString);

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("~/reporting/reports");
        }
        #endregion

        #region _RemoveShortcutButton_Command
        /// <summary>
        /// Handles the "Remove Shortcut" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RemoveShortcutButton_Command(object sender, CommandEventArgs e)
        {
            // remove report shortcut
            DataTable reports = new DataTable(); ;
            reports.Columns.Add("id", typeof(int));
            reports.Rows.Add(this._ReportObject.IdReport);
            Library.Report._RemoveShortcut(reports);
            Response.Redirect(Request.RawUrl);
        }
        #endregion

        #region _AddShortcutButton_Command
        /// <summary>
        /// Handles the "Add Shortcut" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _AddShortcutButton_Command(object sender, CommandEventArgs e)
        {
            // add report shortcut
            DataTable reports = new DataTable(); ;
            reports.Columns.Add("id", typeof(int));
            reports.Rows.Add(this._ReportObject.IdReport);
            Library.Report._AddShortcut(reports);
            Response.Redirect(Request.RawUrl);
        }
        #endregion

        #region _RunButton_Command
        /// <summary>
        /// Handles the "Run" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _RunButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                //populate the object.SelectedItem.Text
                this._ReportObject.IdUser = AsentiaSessionState.IdSiteUser;
                this._ReportObject.IdDataset = this._ReportForm.IdDataset;
                this._ReportObject.IdSite = AsentiaSessionState.IdSite;
                this._ReportObject.Fields = this._ReportForm.Fields;
                this._ReportObject.Filter = this._ReportForm.Filter;
                this._ReportObject.Order = this._ReportForm.Order;

                // validate the form
                if (String.IsNullOrWhiteSpace(this._ValidateRunForm()))
                {
                    _PanelSaveOuter.Visible = true;
                    _OutputReport();
                }
                else
                {
                    _PanelHTMLOuter.Visible = false;
                    _PanelCSVOuter.Visible = false;
                    _PanelPDFOuter.Visible = false;
                    _PanelSaveOuter.Visible = false;
                    this._ReportTypeModal.DisplayFeedback(this._ValidateRunForm(), true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            catch (Exception ex) // JC EDIT
            {
                // display the failure message
                this._ReportTypeModal.DisplayFeedback(ex.Message + " | " + ex.StackTrace, true);
            }
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction.ID = "DeleteConfirmModal";

            LinkButton saveFileDeleteButton = (LinkButton)this._ReportForm.SaveFileDeleteButton;
            saveFileDeleteButton.Style.Add("display", "none");
            this.ReportFormContainer.Controls.Add(saveFileDeleteButton);

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedReportFile_s;
            this.GridConfirmAction.TargetControlID = saveFileDeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseReportFile_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.ReportFormContainer.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_Command
        /// <summary>
        /// Handles the "Delete" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _DeleteButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this._ReportForm.ReportFileGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this._ReportForm.ReportFileGrid.Rows[i].FindControl(this._ReportForm.ReportFileGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        {
                            string filePath = SitePathConstants.SITE_WAREHOUSE_REPORTDATA + this._ReportForm.ReportFileGrid.Rows[i].Cells[1].Text;
                            if (File.Exists(MapPathSecure(filePath + ".html"))) { File.Delete(MapPathSecure(filePath + ".html")); }
                            if (File.Exists(MapPathSecure(filePath + ".csv"))) { File.Delete(MapPathSecure(filePath + ".csv")); }
                            if (File.Exists(MapPathSecure(filePath + ".pdf"))) { File.Delete(MapPathSecure(filePath + ".pdf")); }  
                            recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); 
                        }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    Library.ReportFile.Delete(recordsToDelete);
                }

                if (this._ReportForm.ReportFileGrid.Rows.Count == 0)
                {
                    this._ReportForm.FindControl("ReportModify_ReportFiles_TabLI").Visible = false;
                    this._ReportForm.FindControl("ReportModify_ReportFiles_TabLink").Visible = false;
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.TheSelectedFile_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedbackInSpecifiedContainer(this.PageFeedbackContainer, _GlobalResources.NoFile_sSelected, true);
                }

                // rebind the grid
                this._ReportForm.ReportFileGrid.BindData();

                if (this._ReportForm.ReportFileGrid.Rows.Count == 0)
                {
                    this._ReportForm.FindControl("ReportModify_ReportFiles_TabLI").Visible = false;
                }

            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedback(dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
            catch (Exception ex)
            {
                // display failure message to user
                this.DisplayFeedback(ex.Message, true);
         
            }
        }
        #endregion

        #region _UpdateButton_Command
        /// <summary>
        /// Handles the "Update Subscription" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _UpdateButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                if (this._ReportForm.IsSubscribed)
                {
                    // validate the form
                    if (!this._ValidateSubscription())
                    {
                        throw new AsentiaException(_GlobalResources.PleaseCorrectTheErrorsShownInMarkedTab_s);
                    }

                    this._ReportObject.IdSite = AsentiaSessionState.IdSite;
                    this._ReportObject.DtSubscriptionStart = this._ReportForm.DTStart;
                    this._ReportObject.RecurInterval = Convert.ToInt32(this._ReportForm.RecurInterval);
                    this._ReportObject.RecurTimeframe = this._ReportForm.RecurTimeFrame;
                    int id = this._ReportObject.UpdateSubscription();
                    this.DisplayFeedback(_GlobalResources.TheReportSubscriptionHasBeenSavedSuccessfully, false);
                }
                else
                {
                    DataTable recordsToDelete = new DataTable();
                    recordsToDelete.Columns.Add("id", typeof(int));
                    recordsToDelete.Rows.Add(this._ReportObject.IdReportSubscription);
                    Report.DeleteReportSubscription(recordsToDelete);
                    this.DisplayFeedback(_GlobalResources.TheSelectedReportSubscription_sHaveBeenDeletedSuccessfully, false);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedback(dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedback(fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedback(cpeEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedback(ex.Message, true);
            }
        }
        #endregion

        #region _BuildReportTypesModal
        /// <summary>
        /// Generate Report and put the links to the files
        /// </summary>
        private void _BuildReportTypesModal(string targetControlId, Panel modalContainer)
        {
            this.ReportTypeModal.Controls.Clear();

            this._ReportTypeModal = new ModalPopup("ReportTypesModal");
            this._ReportTypeModal.ID = "ReportTypesModal";
            this._ReportTypeModal.Type = ModalPopupType.Form;
            this._ReportTypeModal.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_REPORT,
                                                                                ImageFiles.EXT_PNG);
            this._ReportTypeModal.HeaderIconAlt = _GlobalResources.ReportTypes;
            this._ReportTypeModal.HeaderText = _GlobalResources.OpenTheReportInTheFormatsBelow + ": ";
            this._ReportTypeModal.TargetControlID = targetControlId;
            this._ReportTypeModal.CssClass += " AddModifyRuleSetModal";
            this._ReportTypeModal.ReloadPageOnClose = false;
            this._ReportTypeModal.SubmitButton.Visible = false;
            this._ReportTypeModal.CloseButton.Visible = false;

            _FormatPanel.CssClass = "VerticalImageLinkContainer";

            _BuildFormatIcons();

            this._ReportTypeModal.AddControlToBody(_FormatPanel);
            modalContainer.Controls.Add(this._ReportTypeModal);

            // hiddenButtonforReportModal
            this._ModalHiddenButton = new Button();
            this._ModalHiddenButton.ClientIDMode = ClientIDMode.Static;
            this._ModalHiddenButton.ID = "ModalHiddenButton";
            this._ModalHiddenButton.Style.Add("display", "none");
            this._ModalHiddenButton.CssClass = "Button ActionButton";
            this._ModalHiddenButton.Command += new CommandEventHandler(_RunButton_Command);
            this._ReportTypeModal.AddControlToBody(this._ModalHiddenButton);
        }
        #endregion

        #region _OutputReport
        /// <summary>
        /// Generate Report and put the links to the files
        /// </summary>
        private void _OutputReport()
        {
            string fileName = string.Empty;

            fileName = this._ReportObject.Run(true, true, true, false);
            
            if (String.IsNullOrWhiteSpace(fileName))
            {
                _PanelHTMLOuter.Visible = false;
                _PanelCSVOuter.Visible = false;
                _PanelPDFOuter.Visible = false;
                _PanelSaveOuter.Visible = false;
                this._ReportTypeModal.DisplayFeedback(_GlobalResources.NoRecord_sFound, true);
            }
            else
            {

                this._ReportTypeModal.ClearFeedback();

                if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_WAREHOUSE_REPORTDATA + fileName + ".html")))
                {
                    _PanelHTMLOuter.Visible = true;
                    _HtmlIconLink.NavigateUrl = Server.UrlDecode(SitePathConstants.SITE_WAREHOUSE_REPORTDATA + fileName + ".html");
                    _HtmlTextLink.NavigateUrl = _HtmlIconLink.NavigateUrl;
                }
                else
                {
                    _PanelHTMLOuter.Visible = false;
                }

                if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_WAREHOUSE_REPORTDATA + fileName + ".csv")))
                {
                    _PanelCSVOuter.Visible = true;
                    _CsvIconLink.NavigateUrl = Server.UrlDecode(SitePathConstants.SITE_WAREHOUSE_REPORTDATA + fileName + ".csv");
                    _CsvTextLink.NavigateUrl = _CsvIconLink.NavigateUrl;
                }
                else
                {
                    _PanelCSVOuter.Visible = false;
                }

                if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_WAREHOUSE_REPORTDATA + fileName + ".pdf")))
                {
                    _PanelPDFOuter.Visible = true;
                    _PdfIconLink.NavigateUrl = Server.UrlDecode(SitePathConstants.SITE_WAREHOUSE_REPORTDATA + fileName + ".pdf");
                    _PdfTextLink.NavigateUrl = _PdfIconLink.NavigateUrl;
                }
                else
                {
                    _PanelPDFOuter.Visible = false;
                }

                _SaveIconLink.OnClientClick = "SaveFileOnClick(\"" + fileName + "\");";

            }
        }
        #endregion

        #region _BuildFormatIcons
        /// <summary>
        /// Generate Report and put the links to the files
        /// </summary>
        private void _BuildFormatIcons()
        {
            #region HTML Type
            _PanelHTMLOuter.ID = "HTMLOuterPanel";
            _PanelHTMLOuter.CssClass = "VerticalImageLinkOuter";

            Panel panelHtmlInner = new Panel();
            panelHtmlInner.CssClass = "VerticalImageLinkInner";

            // display html icon and its link
            Panel panelHtmlIcon = new Panel();
            panelHtmlIcon.CssClass = "VerticalImageLinkImageContainer";

            _HtmlIconLink = new HyperLink();
            System.Web.UI.WebControls.Image htmlImage = new System.Web.UI.WebControls.Image();
            htmlImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_IE,
                                                        ImageFiles.EXT_PNG);
            htmlImage.CssClass = "XLargeIcon";
            _HtmlIconLink.Controls.Add(htmlImage);
            _HtmlIconLink.Target = "_blank";
            panelHtmlIcon.Controls.Add(_HtmlIconLink);

            // display html text and its link
            Panel panelHtmlLabel = new Panel();
            panelHtmlLabel.CssClass = "VerticalImageLinkLabelContainer";
            _HtmlTextLink = new HyperLink();
            _HtmlTextLink.Text = "HTML";
            _HtmlTextLink.Target = "_blank";
            panelHtmlLabel.Controls.Add(_HtmlTextLink);

            // put image and label to the container
            panelHtmlInner.Controls.Add(panelHtmlIcon);
            panelHtmlInner.Controls.Add(panelHtmlLabel);
            _PanelHTMLOuter.Controls.Add(panelHtmlInner);
            #endregion

            #region CSV Type
            _PanelCSVOuter.ID = "CSVOuterPanel";
            _PanelCSVOuter.CssClass = "VerticalImageLinkOuter";

            Panel panelCsvInner = new Panel();
            panelCsvInner.CssClass = "VerticalImageLinkInner";

            // display office icon and its link
            Panel panelCsvIcon = new Panel();
            panelCsvIcon.CssClass = "VerticalImageLinkImageContainer";
            _CsvIconLink = new HyperLink();
            System.Web.UI.WebControls.Image csvImage = new System.Web.UI.WebControls.Image();
            csvImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_MSOFFICE_2013,
                                                       ImageFiles.EXT_PNG);
            csvImage.CssClass = "XLargeIcon";
            _CsvIconLink.Controls.Add(csvImage);
            panelCsvIcon.Controls.Add(_CsvIconLink);

            // display office text and its link
            Panel panelCsvLabel = new Panel();
            panelCsvLabel.CssClass = "VerticalImageLinkLabelContainer";
            _CsvTextLink = new HyperLink();
            _CsvTextLink.Text = "Excel (.csv)";
            panelCsvLabel.Controls.Add(_CsvTextLink);

            // put image and label to the container
            panelCsvInner.Controls.Add(panelCsvIcon);
            panelCsvInner.Controls.Add(panelCsvLabel);
            _PanelCSVOuter.Controls.Add(panelCsvInner);
            #endregion

            #region PDF Type
            _PanelPDFOuter.ID = "PDFOuterPanel";
            _PanelPDFOuter.CssClass = "VerticalImageLinkOuter";

            Panel panelPdfInner = new Panel();
            panelPdfInner.CssClass = "VerticalImageLinkInner";

            // display pdf icon and its link
            Panel panelPdfIcon = new Panel();
            panelPdfIcon.CssClass = "VerticalImageLinkImageContainer";
            Image pdfImage = new Image();
            pdfImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PDF,
                                                        ImageFiles.EXT_PNG);
            _PdfIconLink = new HyperLink();
            pdfImage.CssClass = "XLargeIcon";
            _PdfIconLink.Controls.Add(pdfImage);
            _PdfIconLink.Target = "_blank";
            panelPdfIcon.Controls.Add(_PdfIconLink);

            Panel panelPdfLabel = new Panel();
            panelPdfLabel.CssClass = "VerticalImageLinkLabelContainer";
            _PdfTextLink = new HyperLink();
            _PdfTextLink.Text = "PDF";
            _PdfTextLink.Target = "_blank";
            panelPdfLabel.Controls.Add(_PdfTextLink);

            // put image and label to the container
            panelPdfInner.Controls.Add(panelPdfIcon);
            panelPdfInner.Controls.Add(panelPdfLabel);
            _PanelPDFOuter.Controls.Add(panelPdfInner);

            #endregion

            #region Save
            _PanelSaveOuter.ID = "SaveOuterPanel";
            _PanelSaveOuter.CssClass = "VerticalImageLinkOuter";

            Panel panelSaveInner = new Panel();
            panelSaveInner.CssClass = "VerticalImageLinkInner";


            // display save icon and its link
            Panel panelSaveIcon = new Panel();
            panelSaveIcon.CssClass = "VerticalImageLinkImageContainer";
            System.Web.UI.WebControls.Image saveImage = new System.Web.UI.WebControls.Image();
            saveImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SAVE,
                                                        ImageFiles.EXT_PNG);
            saveImage.CssClass = "XLargeIcon";
            _SaveIconLink.Controls.Add(saveImage);
            _SaveIconLink.Command += new CommandEventHandler(_SaveFileButton_Command);
            panelSaveIcon.Controls.Add(_SaveIconLink);

            // display save text and its link
            Panel panelSaveLabel = new Panel();
            panelSaveLabel.CssClass = "VerticalImageLinkLabelContainer";
            _SaveTextLink.Text = "Save";
            _SaveTextLink.Command += new CommandEventHandler(_SaveFileButton_Command);
            panelSaveLabel.Controls.Add(_SaveTextLink);

            // put image and label to the container
            panelSaveInner.Controls.Add(panelSaveIcon);
            panelSaveInner.Controls.Add(panelSaveLabel);
            _PanelSaveOuter.Controls.Add(panelSaveInner);
            #endregion

            _FormatPanel.Controls.Add(_PanelHTMLOuter);
            _FormatPanel.Controls.Add(_PanelCSVOuter);
            _FormatPanel.Controls.Add(_PanelPDFOuter);   

            if (!this._CreateReport)
            {
                _FormatPanel.Controls.Add(_PanelSaveOuter);
            }          
        }

        #endregion
    }
}
