﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LRS.Library;


namespace Asentia.LRS.Pages.Reporting.Reports
{
    public class Default : AsentiaAuthenticatedPage
    {
        #region Properties
        public Panel ReportFormContentWrapperContainer;
        public Panel ReportAccordionMenuContainer;
        public Panel ReportPropertiesWrapperContainer;
        public Panel ReportFeedbackContainer;
        public Panel SavedReportsFeedbackContainer;
        public Panel TabContainer, TabContentWrapperPanel;
        public UpdatePanel MySavedReportsContainer;
        public Panel MySavedReportsInstructionsPanel;
        public Grid MySavedReportsGrid;
        public LinkButton MySavedReportsDeleteButton;
        public Panel MySavedReportsActionsPanel;
        public UpdatePanel PublicReportsContainer;
        public Panel PublicReportsFeedbackContainer;
        public Panel PublicReportsInstructionsPanel;
        public Grid PublicReportsGrid;
        public LinkButton PublicReportsDeleteButton;
        public Panel PublicReportsActionsPanel;
        public Panel MySubscriptionsFeedbackContainer;
        public Panel MySubscriptionsInstructionsPanel;
        public UpdatePanel MySubscriptionsContainer = new UpdatePanel();
        public Grid ReportSubscriptionGrid;
        public ReportDataSet ReportDataSetObject = new ReportDataSet();
        public Panel MySubscriptionsReportActionsPanel;
        public LinkButton DeleteButton = new LinkButton();
        public ModalPopup SavedReportsGridConfirmAction;
        public ModalPopup PublicReportsGridConfirmAction;
        public ModalPopup GridConfirmAction = new ModalPopup();

        #endregion

        #region Private Properties
        private EcommerceSettings _EcommerceSettings;
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            // get the ecommerce settings
            this._EcommerceSettings = new EcommerceSettings();

            // check permissions
            if (
                !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Certificates)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserDemographics)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCourseTranscripts)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts)
                && !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts)
                //&& !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_XAPI)
                && (
                    !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Purchases)
                    ||
                    (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Purchases) && !this._EcommerceSettings.IsEcommerceSetAndVerified)
                   )
                && (
                    !AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts)
                    ||
                    (AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts) && !(bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                   )
               )
            { Response.Redirect("/"); }

            // include page-specific css files            
            this.IncludePageSpecificCssFile("page-specific/reporting/reports/Default.css");

            // build the breadcrumb
            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.MyDashboard, "/dashboard"));
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Reports));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.Reporting, _GlobalResources.Reports, ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG));
            this.InitializeAdminMenu();

            this.ReportFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";

            this.ReportAccordionMenuContainer.CssClass = "xd-3 xm-12 AccMenuContainer";
            this.ReportPropertiesWrapperContainer.CssClass = "xd-9 xm-12 FormContentContainer RightWrapper";

            this._BuildReportAccordionMenu();

            this._RemoveGeneratedReportFiles();

            this._BuildReport();

            this._JSFunction();
        }
        #endregion

        #region _BuildReportAccordionMenu
        /// <summary>
        /// 
        /// </summary>
        private void _BuildReportAccordionMenu()
        {
            Panel reportAccordionMenu = new Panel();
            reportAccordionMenu.CssClass = "AccordionMenu";

            // DataSets
            DataTable dtDataSet = new DataTable();
            dtDataSet = ReportDataSetObject.GetReportDataSetListTable();

            foreach (DataRow row in dtDataSet.Rows)
            {
                if (
                    (Convert.ToInt32(row["idDataset"]) == 1 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserDemographics))
                    || (Convert.ToInt32(row["idDataset"]) == 2 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCourseTranscripts))
                    || (Convert.ToInt32(row["idDataset"]) == 3 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_CatalogAndCourseInformation))
                    || (Convert.ToInt32(row["idDataset"]) == 4 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Certificates))
                    //|| (Convert.ToInt32(row["idDataset"]) == 5 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_XAPI))                    
                    || (Convert.ToInt32(row["idDataset"]) == 6 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserLearningPathTranscripts) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.LEARNINGPATHS_ENABLE))
                    || (Convert.ToInt32(row["idDataset"]) == 7 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserInstructorLedTrainingTranscripts))
                    || (Convert.ToInt32(row["idDataset"]) == 8 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_Purchases) && this._EcommerceSettings.IsEcommerceSetAndVerified)
                    || (Convert.ToInt32(row["idDataset"]) == 9 && AsentiaAuthenticatedPage.CheckPermission(AsentiaPermission.Reporting_Reporter_UserCertificationTranscripts) && (bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.CERTIFICATIONS_ENABLE))
                    || (Convert.ToInt32(row["idDataset"]) >= 1000) // custom datasets
                   )
                {
                    // Dataset item container
                    Panel dataSetItemContainer = new Panel();
                    dataSetItemContainer.ID = "DataSetItemContainer" + row["idDataset"].ToString();
                    dataSetItemContainer.CssClass = "AccordionMenuItemContainer";

                    // Dataset link container
                    Panel dataSetLinkContainer = new Panel();
                    dataSetLinkContainer.ID = "DataSetLinkContainer" + row["idDataset"].ToString();
                    dataSetLinkContainer.CssClass = "AccordionMenuItem";

                    Image dataSetImage = new Image();
                    dataSetImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_SYSTEM_DATASETS,
                                                                   ImageFiles.EXT_PNG);
                    dataSetImage.AlternateText = row["name"].ToString();
                    dataSetItemContainer.Controls.Add(dataSetImage);

                    Literal dataSetName = new Literal();
                    dataSetName.Text = row["name"].ToString();
                    dataSetItemContainer.Controls.Add(dataSetName);

                    dataSetItemContainer.Controls.Add(dataSetLinkContainer);

                    // report "new" context menu
                    Panel reportNewContextMenu = new Panel();
                    reportNewContextMenu.ID = "ReportNewContextMenu" + row["idDataset"].ToString();
                    reportNewContextMenu.CssClass = "AccordionMenuItemContextMenu MobileHidden";

                    // open menu link
                    HyperLink reportNewContextMenuOpenLink = new HyperLink();
                    reportNewContextMenuOpenLink.ID = "ReportNewContextMenuTrigger" + row["idDataset"].ToString();
                    reportNewContextMenuOpenLink.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD,
                                                                                   ImageFiles.EXT_PNG);
                    reportNewContextMenuOpenLink.Attributes.Add("onclick", "ShowContextMenu(this.id);");
                    reportNewContextMenu.Controls.Add(reportNewContextMenuOpenLink);

                    // report "new" context menu items container
                    Panel reportNewContextMenuItemContainer = new Panel();
                    reportNewContextMenuItemContainer.ID = "ReportNewContextMenuItemContainer" + row["idDataset"].ToString();
                    reportNewContextMenuItemContainer.CssClass = "AccordionMenuContextMenuContainer";

                    // new report link
                    Panel reportLinkContainer = new Panel();
                    reportLinkContainer.ID = "NewReportLink" + row["idDataset"].ToString();

                    HyperLink reportLink = new HyperLink();
                    reportLink.Text = String.Format(_GlobalResources.NewXReport, row["name"].ToString());                    
                    reportLink.NavigateUrl = "/reporting/reports/Modify.aspx?idDataset=" + row["idDataset"].ToString();
                    reportLinkContainer.Controls.Add(reportLink);

                    reportNewContextMenuItemContainer.Controls.Add(reportLinkContainer);

                    // attach context menu controls
                    reportNewContextMenu.Controls.Add(reportNewContextMenuItemContainer);
                    dataSetItemContainer.Controls.Add(reportNewContextMenu);

                    Panel dataSetInformation = new Panel();
                    dataSetInformation.ID = "InformationContainer" + row["idDataset"].ToString();
                    dataSetInformation.CssClass = "AccordionMenuItemContentsContainer MobileHidden AccordionShow";
                    Literal dataSetInformationText = new Literal();
                    dataSetInformationText.Text = row["description"].ToString();
                    dataSetInformation.Controls.Add(dataSetInformationText);

                    dataSetItemContainer.Controls.Add(dataSetInformation);

                    reportAccordionMenu.Controls.Add(dataSetItemContainer);
                }
            }

            //attach reportAccordionMenu to the container
            this.ReportAccordionMenuContainer.Controls.Add(reportAccordionMenu);
        }
        #endregion

        #region _RemoveGeneratedReportFilesMoreThan24Hours
        /// <summary>
        /// 
        /// </summary>
        private void _RemoveGeneratedReportFiles()
        {           
            if (Directory.Exists(Server.MapPath(SitePathConstants.SITE_WAREHOUSE_REPORTDATA)))
            {
                string[] filePaths = Directory.GetFiles(MapPathSecure(SitePathConstants.SITE_WAREHOUSE_REPORTDATA), "*.*");                
                List<string> fileNames = ReportFile.GetSavedFileNamesForSite();

                foreach (string filePath in filePaths)
                {
                    bool toBeDeleted = true;

                    foreach (string fileName in fileNames)
                    {
                        if (!String.IsNullOrWhiteSpace(fileName))
                        {
                            if (filePath.Contains(fileName))
                            { toBeDeleted = false; }
                        }
                    }

                    TimeSpan diff = AsentiaSessionState.UtcNow - File.GetCreationTime(filePath);
                    
                    if (File.Exists(filePath) && (diff.TotalHours > 24) && toBeDeleted)
                    { File.Delete(filePath); }
                }
            }
        }
        #endregion

        #region _BuildReport
        /// <summary>
        /// 
        /// </summary>
        private void _BuildReport()
        {
            this._BuildReportTabs();

            // format a page information panel with saved report instructions
            this.FormatPageInformationPanel(this.MySavedReportsInstructionsPanel, _GlobalResources.ReportsThatYouHaveConfiguredAreListedBelow, true);

            // build my saved reports container
            this._BuildMySavedReportsContainer();
            this._BuildMySavedReportsActionsPanel();

            if (!IsPostBack)
            {
                this.MySavedReportsGrid.BindData();
            }
            this._BuildSavedReportsGridActionsModal();

            if (AsentiaSessionState.IdSiteUser > 1)
            {
                // format a page information panel with report subscription instruction
                this.FormatPageInformationPanel(this.MySubscriptionsInstructionsPanel, _GlobalResources.ReportsThatYouHaveSubscribedTo + ": ", true);

                // build my subscribed reports container
                this._BuildMySubscriptionsContainer();

                // if not postback
                if (!IsPostBack)
                {
                    // bind data grid
                    this.ReportSubscriptionGrid.BindData();
                }

                this._BuildSubscriptionActionsPanel();
                this._BuildGridActionsModal();
            }

            // format a page information panel with saved report instructions
            this.FormatPageInformationPanel(this.PublicReportsInstructionsPanel, _GlobalResources.ReportsThatOthersHaveConfiguredAndMarkedAsPublicAreListedBelow, true);

            // build my saved reports container
            this._BuildPublicReportsContainer();
            if (AsentiaSessionState.IdSiteUser == 1)
            {
                this._BuildPublicReportsActionsPanel();
                this._BuildPublicReportsGridActionsModal();
            }

            if (!IsPostBack)
            {
                this.PublicReportsGrid.BindData();
            }

        }
        #endregion

        #region _BuildReportTabs
        /// <summary>
        /// 
        /// </summary>
        private void _BuildReportTabs()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("MySavedReports", _GlobalResources.MySavedReports));

            // subscriptions tab - only show when user is not "administrator"
            if (AsentiaSessionState.IdSiteUser > 1)
            { tabs.Enqueue(new KeyValuePair<string, string>("MySubscriptions", _GlobalResources.MyReportSubscriptions)); }

            tabs.Enqueue(new KeyValuePair<string, string>("Public", _GlobalResources.PublicSavedReports));

            // build and attach the tabs
            this.TabContainer.Controls.Add(AsentiaPage.BuildTabListPanel("Report", tabs));
        }
        #endregion

        #region _BuildMySavedReportsContainer
        /// <summary>
        /// Builds the container that contains the list of the personal saved reports.
        /// </summary>
        private void _BuildMySavedReportsContainer()
        {
            this.MySavedReportsContainer.ID = "Report_MySavedReports_TabPanelContainer";
            this.MySavedReportsContainer.UpdateMode = UpdatePanelUpdateMode.Conditional;

            this.MySavedReportsGrid.StoredProcedure = Library.Report.GridProcedure;
            this.MySavedReportsGrid.ShowSearchBox = false;
            this.MySavedReportsGrid.ShowRecordsPerPageSelectbox = false;
            this.MySavedReportsGrid.ShowFooter = false;
            this.MySavedReportsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.MySavedReportsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.MySavedReportsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.MySavedReportsGrid.AddFilter("@idUser", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.MySavedReportsGrid.PagerSettings.Position = PagerPosition.Bottom;
            this.MySavedReportsGrid.IdentifierField = "idReport";
            this.MySavedReportsGrid.DefaultSortColumn = "title";

            // data key names
            this.MySavedReportsGrid.DataKeyNames = new string[] { "idReport", "title" };

            // columns
            GridColumn nameDataSetOwner = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.Dataset + ", " + _GlobalResources.Owner + "", null, "title"); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.MySavedReportsGrid.AddColumn(nameDataSetOwner);

            // row data bound event
            this.MySavedReportsGrid.RowDataBound += new GridViewRowEventHandler(_MySavedReportsGrid_RowDataBound);

        }
        #endregion

        #region _MySavedReportsGrid_RowDataBound
        protected void _MySavedReportsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                string title = Server.HtmlEncode(rowView["title"].ToString());
                int idDataSet = Convert.ToInt16(rowView["idDataSet"]);
                string dataSetText = rowView["dataSet"].ToString();
                string owner = Server.HtmlEncode(rowView["ownerName"].ToString());
                int numSubscription = Convert.ToInt16(rowView["subscriptionCount"]);

                // REPORT ICON
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = _GlobalResources.Report;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // NAME
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink titleLink = new HyperLink();
                titleLink.NavigateUrl = "Modify.aspx?idReport=" + rowView["idReport"].ToString();
                titleLink.Text = title;
                nameLabel.Controls.Add(titleLink);

                // ADD DATASET AT THE SECOND LINE
                Label dataSetLabel = new Label();
                dataSetLabel.CssClass = "GridSecondaryLine";
                dataSetLabel.Text = dataSetText;
                e.Row.Cells[1].Controls.Add(dataSetLabel);

                // owner & Subscription - they're on a single line, so they go into a wrapper
                Panel ownerSubscriptionInlineWrapper = new Panel();
                ownerSubscriptionInlineWrapper.CssClass = "GridSecondaryLineInlineWrapper";
                e.Row.Cells[1].Controls.Add(ownerSubscriptionInlineWrapper);

                // owner
                Label ownerLabel = new Label();
                ownerLabel.CssClass = "GridSecondaryLineInline";
                ownerSubscriptionInlineWrapper.Controls.Add(ownerLabel);

                Literal ownerLit = new Literal();

                if (owner == "##Administrator##")
                { ownerLit.Text = _GlobalResources.Administrator; }
                else
                { ownerLit.Text = owner; }
                ownerLabel.Controls.Add(ownerLit);

                // subsription                
                Label subscriptionLabel = new Label();
                subscriptionLabel.CssClass = "GridSecondaryLineInline";
                ownerSubscriptionInlineWrapper.Controls.Add(subscriptionLabel);

                Literal subscriptionLit = new Literal();
                subscriptionLit.Text = numSubscription.ToString() + " " + ((numSubscription != 1) ? _GlobalResources.Subscriptions : _GlobalResources.Subscription);
                subscriptionLabel.Controls.Add(subscriptionLit);
            }
        }
        #endregion

        #region _BuildMySavedReportsActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on My Saved Reports Grid data.
        /// </summary>
        private void _BuildMySavedReportsActionsPanel()
        {
            this.MySavedReportsActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.MySavedReportsDeleteButton = new LinkButton();
            this.MySavedReportsDeleteButton.CssClass = "GridDeleteButton";
            this.MySavedReportsDeleteButton.ID = "MySavedReportsGridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "MySavedReportsGridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.MySavedReportsDeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedReport_s;
            this.MySavedReportsDeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.MySavedReportsActionsPanel.Controls.Add(this.MySavedReportsDeleteButton);
        }
        #endregion

        #region _BuildPublicReportsActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Public Reports Grid data.
        /// </summary>
        private void _BuildPublicReportsActionsPanel()
        {
            this.PublicReportsActionsPanel.CssClass = "ActionsPanel";

            // delete button
            this.PublicReportsDeleteButton = new LinkButton();
            this.PublicReportsDeleteButton.CssClass = "GridDeleteButton";
            this.PublicReportsDeleteButton.ID = "MyPublicReportsGridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "MyPublicReportsGridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.PublicReportsDeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedReport_s;
            this.PublicReportsDeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.PublicReportsActionsPanel.Controls.Add(this.PublicReportsDeleteButton);
        }
        #endregion

        #region _BuildMySubscriptionsContainer
        /// <summary>
        /// Builds the container that contains the list of the personal subscribed reports.
        /// </summary>
        private void _BuildMySubscriptionsContainer()
        {
            this.MySubscriptionsContainer.ID = "Report_MySubscriptions_TabPanelContainer";
            this.MySubscriptionsContainer.UpdateMode = UpdatePanelUpdateMode.Conditional;

            this.ReportSubscriptionGrid.StoredProcedure = "[Report.GetSubscriptionForUser]";
            this.ReportSubscriptionGrid.DefaultRecordsPerPage = 10;
            this.ReportSubscriptionGrid.ShowSearchBox = false;
            this.ReportSubscriptionGrid.ShowRecordsPerPageSelectbox = false;
            this.ReportSubscriptionGrid.ShowFooter = false;
            this.ReportSubscriptionGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.ReportSubscriptionGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.ReportSubscriptionGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.ReportSubscriptionGrid.AddFilter("@idUser", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.ReportSubscriptionGrid.IdentifierField = "idReportSubscription";
            this.ReportSubscriptionGrid.PagerSettings.Position = PagerPosition.Bottom;
            this.ReportSubscriptionGrid.DefaultSortColumn = "reportName";
            this.ReportSubscriptionGrid.ShowTimeInDateStrings = false;

            // data key names
            this.ReportSubscriptionGrid.DataKeyNames = new string[] { "idReportSubscription", "idReport" };

            // columns
            GridColumn nameDataSetOwner = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.Dataset + ", " + _GlobalResources.Owner + "", null, "title"); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.ReportSubscriptionGrid.AddColumn(nameDataSetOwner);

            // row data bound event
            this.ReportSubscriptionGrid.RowDataBound += new GridViewRowEventHandler(_MySubscriptions_RowDataBound);

        }
        #endregion

        #region _MySubscriptions_RowDataBound
        protected void _MySubscriptions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                string title = Server.HtmlEncode(rowView["title"].ToString());
                int idDataSet = Convert.ToInt16(rowView["idDataSet"]);
                string dataSetText = rowView["dataSet"].ToString();
                string owner = Server.HtmlEncode(rowView["ownerName"].ToString());

                // REPORT ICON
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = _GlobalResources.Report;
                e.Row.Cells[1].Controls.Add(avatarImage);

                // NAME
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[1].Controls.Add(nameLabel);

                HyperLink titleLink = new HyperLink();
                titleLink.NavigateUrl = "Modify.aspx?idReport=" + rowView["idReport"].ToString();
                titleLink.Text = title;
                nameLabel.Controls.Add(titleLink);

                // ADD DATASET AT THE SECOND LINE
                Label dataSetLabel = new Label();
                dataSetLabel.CssClass = "GridSecondaryLine";
                dataSetLabel.Text = dataSetText;
                e.Row.Cells[1].Controls.Add(dataSetLabel);

                // ADD OWNER AT THE THIRD LINE
                Label ownerLabel = new Label();
                ownerLabel.CssClass = "GridSecondaryLine";
                ownerLabel.Text = owner;
                e.Row.Cells[1].Controls.Add(ownerLabel);
            }
        }
        #endregion

        #region _BuildSubscriptionActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Subscription Grid data.
        /// </summary>
        private void _BuildSubscriptionActionsPanel()
        {
            // delete button
            this.DeleteButton.ID = "GridDeleteButton";
            this.DeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.DeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedReportSubscription_s;
            this.DeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.MySubscriptionsReportActionsPanel.Controls.Add(this.DeleteButton);
        }
        #endregion

        #region _BuildSavedReportsGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Saved Reports Grid data.
        /// </summary>
        private void _BuildSavedReportsGridActionsModal()
        {
            this.SavedReportsGridConfirmAction = new ModalPopup("SavedReportsDeleteConfirmModal");

            // set modal properties
            this.SavedReportsGridConfirmAction.Type = ModalPopupType.Confirm;
            this.SavedReportsGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.SavedReportsGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.SavedReportsGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedReport_s;
            this.SavedReportsGridConfirmAction.TargetControlID = this.MySavedReportsDeleteButton.ClientID;
            this.SavedReportsGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_SavedReports_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "SavedReportsGridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseReport_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.SavedReportsGridConfirmAction.AddControlToBody(body1Wrapper);

            this.MySavedReportsActionsPanel.Controls.Add(this.SavedReportsGridConfirmAction);
        }
        #endregion

        #region _BuildPublicReportsGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Saved Reports Grid data.
        /// </summary>
        private void _BuildPublicReportsGridActionsModal()
        {
            this.PublicReportsGridConfirmAction = new ModalPopup("PublicReportsDeleteConfirmModal");

            // set modal properties
            this.PublicReportsGridConfirmAction.Type = ModalPopupType.Confirm;
            this.PublicReportsGridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.PublicReportsGridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.PublicReportsGridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedReport_s;
            this.PublicReportsGridConfirmAction.TargetControlID = this.PublicReportsDeleteButton.ClientID;
            this.PublicReportsGridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_PublicReports_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "PublicReportsGridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseReport_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.PublicReportsGridConfirmAction.AddControlToBody(body1Wrapper);

            this.PublicReportsActionsPanel.Controls.Add(this.PublicReportsGridConfirmAction);
        }
        #endregion

        #region _BuildGridActionsModal
        /// <summary>
        /// Builds the confirmation modal for actions performed on Grid data.
        /// </summary>
        private void _BuildGridActionsModal()
        {
            this.GridConfirmAction.ID = "DeleteConfirmModal";

            // set modal properties
            this.GridConfirmAction.Type = ModalPopupType.Confirm;
            this.GridConfirmAction.HeaderIconPath = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                                           ImageFiles.EXT_PNG);
            this.GridConfirmAction.HeaderIconAlt = _GlobalResources.Delete;
            this.GridConfirmAction.HeaderText = _GlobalResources.DeleteSelectedReportSubscription_s;
            this.GridConfirmAction.TargetControlID = this.DeleteButton.ClientID;
            this.GridConfirmAction.SubmitButton.Command += new CommandEventHandler(this._DeleteButton_ReportSubscription_Command);

            // build the modal body
            HtmlGenericControl body1Wrapper = new HtmlGenericControl("p");
            Literal body1 = new Literal();

            body1Wrapper.ID = "GridConfirmActionModalBody1";
            body1.Text = _GlobalResources.AreYouSureYouWantToDeleteTheseReportSubscription_s;

            body1Wrapper.Controls.Add(body1);

            // add controls to body
            this.GridConfirmAction.AddControlToBody(body1Wrapper);

            this.MySubscriptionsReportActionsPanel.Controls.Add(this.GridConfirmAction);
        }
        #endregion

        #region _DeleteButton_SavedReports_Command
        /// <summary>
        /// Performs the delete action on Saved Report Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_SavedReports_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));
                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.MySavedReportsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.MySavedReportsGrid.Rows[i].FindControl(this.MySavedReportsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    // delete the records
                    Library.Report.Delete(recordsToDelete);

                    // rebind the grid
                    this.MySavedReportsGrid.BindData();

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this.SavedReportsFeedbackContainer, _GlobalResources.TheSelectedSavedReports_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedbackInSpecifiedContainer(this.SavedReportsFeedbackContainer, _GlobalResources.NoReport_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SavedReportsFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SavedReportsFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SavedReportsFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SavedReportsFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.SavedReportsFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.MySavedReportsGrid.BindData();
            }
        }
        #endregion

        #region _DeleteButton_PublicReports_Command
        /// <summary>
        /// Performs the delete action on Saved Report Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_PublicReports_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.PublicReportsGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.PublicReportsGrid.Rows[i].FindControl(this.PublicReportsGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                if (recordsToDelete.Rows.Count > 0)
                {
                    // delete the records
                    Library.Report.Delete(recordsToDelete);
                    this.PublicReportsGrid.BindData();

                    // display the success message
                    this.DisplayFeedbackInSpecifiedContainer(this.PublicReportsFeedbackContainer, _GlobalResources.TheSelectedSavedReports_sHaveBeenDeletedSuccessfully, false);
                }
                else
                {
                    // display the error message
                    this.DisplayFeedbackInSpecifiedContainer(this.PublicReportsFeedbackContainer, _GlobalResources.NoReport_sSelectedForDeletion, true);
                }
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PublicReportsFeedbackContainer, dnfEx.Message, true);
            }
            catch (DatabaseFieldNotUniqueException fnuEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PublicReportsFeedbackContainer, fnuEx.Message, true);
            }
            catch (DatabaseCallerPermissionException cpeEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PublicReportsFeedbackContainer, cpeEx.Message, true);
            }
            catch (DatabaseException dEx)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PublicReportsFeedbackContainer, dEx.Message, true);
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.PublicReportsFeedbackContainer, ex.Message, true);
            }
            finally
            {
                // rebind the grid
                this.PublicReportsGrid.BindData();
            }
        }
        #endregion

        #region _DeleteButton_ReportSubscription_Command
        /// <summary>
        /// Performs the delete action on Grid data.
        /// </summary>
        /// <param name="sender">page</param>
        /// <param name="e">page arguments</param>
        private void _DeleteButton_ReportSubscription_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DataTable recordsToDelete = new DataTable(); ;
                recordsToDelete.Columns.Add("id", typeof(int));

                // loop through rows in Grid to find checked checkboxes
                for (int i = 0; i < this.ReportSubscriptionGrid.Rows.Count; i++)
                {
                    CheckBox checkBox = (CheckBox)this.ReportSubscriptionGrid.Rows[i].FindControl(this.ReportSubscriptionGrid.ID + "_GridSelectRecord_" + i);

                    if (checkBox != null)
                    {
                        if (checkBox.Checked)
                        { recordsToDelete.Rows.Add(Convert.ToInt32(checkBox.InputAttributes["value"])); }
                    }
                }

                // delete the records
                Library.Report.DeleteReportSubscription(recordsToDelete);

                // display the success message
                this.DisplayFeedbackInSpecifiedContainer(this.MySubscriptionsFeedbackContainer, _GlobalResources.TheSelectedReportSubscription_sHaveBeenDeletedSuccessfully, false);

                // rebind the grid
                this.ReportSubscriptionGrid.BindData();
            }
            catch (AsentiaException ex)
            {
                // display the failure message
                this.DisplayFeedbackInSpecifiedContainer(this.MySubscriptionsFeedbackContainer, ex.Message, true);

                // rebind the grid
                this.ReportSubscriptionGrid.BindData();
            }
        }
        #endregion

        #region _BuildPublicReportsContainer
        /// <summary>
        /// Builds the container that contains the list of the public reports.
        /// </summary>
        private void _BuildPublicReportsContainer()
        {
            this.PublicReportsContainer.ID = "Report_Public_TabPanelContainer";
            this.PublicReportsContainer.UpdateMode = UpdatePanelUpdateMode.Conditional;

            this.PublicReportsGrid.StoredProcedure = Library.Report.GridProcedure;
            this.PublicReportsGrid.ShowSearchBox = false;
            this.PublicReportsGrid.ShowRecordsPerPageSelectbox = false;
            this.PublicReportsGrid.ShowFooter = false;
            this.PublicReportsGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            this.PublicReportsGrid.AddFilter("@callerLangString", SqlDbType.NVarChar, 10, AsentiaSessionState.UserCulture);
            this.PublicReportsGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            this.PublicReportsGrid.AddFilter("@isPublicRequired", SqlDbType.Bit, 2, Convert.ToBoolean(true));
            this.PublicReportsGrid.PagerSettings.Position = PagerPosition.Bottom;
            this.PublicReportsGrid.IdentifierField = "idReport";
            this.PublicReportsGrid.DefaultSortColumn = "title";
            this.PublicReportsGrid.AddCheckboxColumn = (AsentiaSessionState.IdSiteUser == 1) ? true : false;

            // data key names
            this.PublicReportsGrid.DataKeyNames = new string[] { "idReport", "title" };

            // columns
            GridColumn nameDataSetOwner = new GridColumn(_GlobalResources.Name + ", " + _GlobalResources.Dataset + ", " + _GlobalResources.Owner + ", " + _GlobalResources.Subscriptions + "", null, "title"); // this is calculated dynamically in the RowDataBound method

            // add columns to data grid
            this.PublicReportsGrid.AddColumn(nameDataSetOwner);

            // row data bound event
            this.PublicReportsGrid.RowDataBound += new GridViewRowEventHandler(_PublicReportsGrid_RowDataBound);
        }
        #endregion

        #region _PublicReportsGrid_RowDataBound
        protected void _PublicReportsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                string title = Server.HtmlEncode(rowView["title"].ToString());
                int idDataSet = Convert.ToInt16(rowView["idDataSet"]);
                string dataSetText = rowView["dataSet"].ToString();
                string owner = Server.HtmlEncode(rowView["ownerName"].ToString());
                int numSubscription = Convert.ToInt16(rowView["subscriptionCount"]);

                // REPORT ICON
                string avatarImagePath = ImageFiles.GetIconPath(ImageFiles.ICON_REPORT, ImageFiles.EXT_PNG);
                string avatarImageClass = "GridAvatarImage";

                // if administrator, then delete check box is granted
                int columnIndex = (AsentiaSessionState.IdSiteUser > 1)? 0:1;

                Image avatarImage = new Image();
                avatarImage.ImageUrl = avatarImagePath;
                avatarImage.CssClass = avatarImageClass;
                avatarImage.AlternateText = _GlobalResources.Report;
                e.Row.Cells[columnIndex].Controls.Add(avatarImage);

                // NAME
                Label nameLabel = new Label();
                nameLabel.CssClass = "GridBaseTitle";
                e.Row.Cells[columnIndex].Controls.Add(nameLabel);

                HyperLink titleLink = new HyperLink();
                titleLink.NavigateUrl = "Modify.aspx?idReport=" + rowView["idReport"].ToString();
                titleLink.Text = title;
                nameLabel.Controls.Add(titleLink);

                // ADD DATASET AT THE SECOND LINE
                Label dataSetLabel = new Label();
                dataSetLabel.CssClass = "GridSecondaryLine";
                dataSetLabel.Text = dataSetText;
                e.Row.Cells[columnIndex].Controls.Add(dataSetLabel);

                // owner & Subscription - they're on a single line, so they go into a wrapper
                Panel ownerSubscriptionInlineWrapper = new Panel();
                ownerSubscriptionInlineWrapper.CssClass = "GridSecondaryLineInlineWrapper";
                e.Row.Cells[columnIndex].Controls.Add(ownerSubscriptionInlineWrapper);

                // owner
                Label ownerLabel = new Label();
                ownerLabel.CssClass = "GridSecondaryLineInline";
                ownerSubscriptionInlineWrapper.Controls.Add(ownerLabel);

                Literal ownerLit = new Literal();

                if (owner == "##Administrator##")
                { ownerLit.Text = _GlobalResources.Administrator; }
                else
                { ownerLit.Text = owner; }
                ownerLabel.Controls.Add(ownerLit);

                // subsription                
                Label subscriptionLabel = new Label();
                subscriptionLabel.CssClass = "GridSecondaryLineInline";
                ownerSubscriptionInlineWrapper.Controls.Add(subscriptionLabel);

                Literal subscriptionLit = new Literal();
                subscriptionLit.Text = numSubscription.ToString() + " " + ((numSubscription != 1) ? _GlobalResources.Subscriptions : _GlobalResources.Subscription);
                subscriptionLabel.Controls.Add(subscriptionLit);
            }
        }
        #endregion

        #region _JSFunction
        /// <summary>
        /// Using Javascript to track the errors in the _BatchUserDataTable
        /// </summary>
        private void _JSFunction()
        {
            Literal javascriptCode = new Literal();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type=\"text/javascript\">");
            sb.AppendLine("  Sys.Application.add_load(jScript); ");
            sb.AppendLine(" $(document).ready(function () { ");
            sb.AppendLine("      $('#Report_MySavedReports_TabPanelContainer').addClass('TabPanelContainer');");
            sb.AppendLine("      $('#Report_MySubscriptions_TabPanelContainer').addClass('TabPanelContainer');");
            sb.AppendLine("      $('#Report_Public_TabPanelContainer').addClass('TabPanelContainer');");
            sb.AppendLine("      $('#Report_MySavedReports_TabPanelContainer').show();  ");
            sb.AppendLine("      $('#Report_MySubscriptions_TabPanelContainer').hide();  ");
            sb.AppendLine("      $('#Report_Public_TabPanelContainer').hide();  ");
            sb.AppendLine("  }); ");
            sb.AppendLine(" function jScript() {");
            sb.AppendLine(" $('#Report_Datasets_TabLI').click(function () { ");
            sb.AppendLine("  $('#Report_MySubscriptions_TabPanelContainer').hide();  ");
            sb.AppendLine("  $('#Report_MySavedReports_TabPanelContainer').hide();  ");
            sb.AppendLine("  $('#Report_Public_TabPanelContainer').hide();  ");
            sb.AppendLine("  });   ");
            sb.AppendLine(" $('#Report_MySavedReports_TabLink').click(function () { ");
            sb.AppendLine("  $('#Report_MySavedReports_TabPanelContainer').show();  ");
            sb.AppendLine("  $('#Report_MySubscriptions_TabPanelContainer').hide();  ");
            sb.AppendLine("  $('#Report_Public_TabPanelContainer').hide();  ");
            sb.AppendLine("  });   ");
            sb.AppendLine(" $('#Report_MySavedReports_TabLI').click(function () { ");
            sb.AppendLine("  $('#Report_MySavedReports_TabPanelContainer').show();  ");
            sb.AppendLine("  $('#Report_MySubscriptions_TabPanelContainer').hide();  ");
            sb.AppendLine("  $('#Report_Public_TabPanelContainer').hide();  ");
            sb.AppendLine("  });   ");
            sb.AppendLine(" $('#Report_Public_TabLink').click(function () { ");
            sb.AppendLine("  $('#Report_Public_TabPanelContainer').show();  ");
            sb.AppendLine("  $('#Report_MySubscriptions_TabPanelContainer').hide();  ");
            sb.AppendLine("  $('#Report_MySavedReports_TabPanelContainer').hide();  ");
            sb.AppendLine("  });   ");
            sb.AppendLine(" $('#Report_Public_TabLI').click(function () { ");
            sb.AppendLine("  $('#Report_Public_TabPanelContainer').show();  ");
            sb.AppendLine("  $('#Report_MySubscriptions_TabPanelContainer').hide();  ");
            sb.AppendLine("  $('#Report_MySavedReports_TabPanelContainer').hide();  ");
            sb.AppendLine("  });   ");
            sb.AppendLine(" $('#Report_MySubscriptions_TabLink').click(function () { ");
            sb.AppendLine("  $('#Report_MySubscriptions_TabPanelContainer').show();  ");
            sb.AppendLine("  $('#Report_MySavedReports_TabPanelContainer').hide();  ");
            sb.AppendLine("  $('#Report_Public_TabPanelContainer').hide();  ");
            sb.AppendLine("  });   ");
            sb.AppendLine(" $('#Report_MySubscriptions_TabLI').click(function () { ");
            sb.AppendLine("  $('#Report_MySubscriptions_TabPanelContainer').show();  ");
            sb.AppendLine("  $('#Report_MySavedReports_TabPanelContainer').hide();  ");
            sb.AppendLine("  $('#Report_Public_TabPanelContainer').hide();  ");
            sb.AppendLine("  });   ");
            sb.AppendLine("  }   ");

            sb.AppendLine("  function ShowContextMenu(contextMenuTriggerId) { ");
            sb.AppendLine("  var contextMenuId = contextMenuTriggerId.replace('Trigger', 'ItemContainer'); ");
            sb.AppendLine("  $('#' + contextMenuId).show(); ");
            sb.AppendLine("  $('#' + contextMenuId).hover(function () { }, function () { ");
            sb.AppendLine("  $('#' + contextMenuId).fadeOut(200); ");
            sb.AppendLine("    }); ");
            sb.AppendLine("} ");
            sb.AppendLine("</script>");
            javascriptCode.Text = sb.ToString();

            this.Controls.Add(javascriptCode);
        }

        #endregion
    }
}
