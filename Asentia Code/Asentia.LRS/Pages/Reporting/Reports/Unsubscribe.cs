﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.UMS.Library;
using Asentia.LRS.Library;

namespace Asentia.LRS.Pages.Reporting.Reports
{
    public class Unsubscribe : AsentiaPage
    {
        #region Properties
        public Panel ReportUnsubscribeFormContentWrapperContainer;
        public Panel ReportUnsubscribeWrapperContainer;
        public Panel ReportUnsubscribeInstructionsPanel;
        public Panel ReportUnsubscribeActionsPanel;
        #endregion

        #region Private Properties
        private string _ReportSubscriptionToken;
        private int _ReportSubscriptionUser;
        private string _ReportTitleInLanguage;
        private Button _UnsubscribeButton;
        private Button _CancelButton;
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // hide the navigation and login controls
            this.HideNavigationAndLoginControls();
        }
        #endregion

        #region Page_Load
        public void Page_Load(object sender, EventArgs e)
        {
            this.ReportUnsubscribeFormContentWrapperContainer.CssClass = "FormContentWrapperContainer";
            this.ReportUnsubscribeWrapperContainer.CssClass = "FormContentContainer";

            // get the report subscription information
            bool gotReportSubscriptionInformation = this._GetReportSubscriptionInformation();

            // build the breadcrumb and page title
            string pageTitle;

            if (!String.IsNullOrWhiteSpace(this._ReportTitleInLanguage))
            { pageTitle = this._ReportTitleInLanguage + ": " + _GlobalResources.Unsubscribe; }
            else
            { pageTitle = _GlobalResources.Unsubscribe; }

            ArrayList breadCrumbLinks = new ArrayList();
            breadCrumbLinks.Add(new BreadcrumbLink(_GlobalResources.Home, "/"));
            breadCrumbLinks.Add(new BreadcrumbLink(pageTitle));
            this.BuildBreadcrumb(breadCrumbLinks);

            // build the page title
            this.BuildPageTitle(PageCategoryForTitle.Reporting, pageTitle, ImageFiles.GetIconPath(ImageFiles.ICON_REPORT,
                                                                                                  ImageFiles.EXT_PNG));

            if (gotReportSubscriptionInformation)
            {
                // format a page information panel with page instructions
                this.FormatPageInformationPanel(this.ReportUnsubscribeInstructionsPanel, _GlobalResources.ClickTheUnsubscribeButtonBelowToConfirm, true);

                // build the actions panel
                this._BuildActionsPanel();
            }
        }
        #endregion

        #region _GetReportSubscriptionInformation
        /// <summary>
        /// 
        /// </summary>
        private bool _GetReportSubscriptionInformation()
        {
            // initialize the return value
            bool returnValue = false;

            // get the querystring parameters
            this._ReportSubscriptionUser = QueryStringInt("e");
            this._ReportSubscriptionToken = QueryStringString("token");

            if (this._ReportSubscriptionUser > 0 && !String.IsNullOrWhiteSpace(this._ReportSubscriptionToken))
            {
                try
                {
                    DataTable dt = ReportSubscription.GetByToken(this._ReportSubscriptionToken, this._ReportSubscriptionUser);
                    DataRow dr = dt.Rows[0];
                    this._ReportTitleInLanguage = dr["title"].ToString();
                    returnValue = true;
                }
                catch
                { this.DisplayFeedback(_GlobalResources.UnableToFindSubscriptionYouMayHaveAlreadyUnsubscribedFromThisReportOrTheReportMayHaveBeenDeleted, true); }
            }
            else
            { this.DisplayFeedback(_GlobalResources.UnableToFindSubscription, true); }

            return returnValue;
        }
        #endregion

        #region _BuildActionsPanel
        /// <summary>
        /// Builds the container and buttons for actions.
        /// </summary>
        private void _BuildActionsPanel()
        {
            // clear controls from container
            this.ReportUnsubscribeActionsPanel.Controls.Clear();

            // style actions panel
            this.ReportUnsubscribeActionsPanel.CssClass = "ActionsPanel";

            // unsubscribe button
            this._UnsubscribeButton = new Button();
            this._UnsubscribeButton.ID = "UnsubscribeButton";
            this._UnsubscribeButton.CssClass = "Button ActionButton";
            this._UnsubscribeButton.Text = _GlobalResources.Unsubscribe;
            this._UnsubscribeButton.Command += new CommandEventHandler(this._UnsubscribeButton_Command);
            this.ReportUnsubscribeActionsPanel.Controls.Add(this._UnsubscribeButton);

            // cancel button
            this._CancelButton = new Button();
            this._CancelButton.ID = "CancelButton";
            this._CancelButton.CssClass = "Button NonActionButton";
            this._CancelButton.Text = _GlobalResources.Cancel;
            this._CancelButton.Command += new CommandEventHandler(this._CancelButton_Command);
            this.ReportUnsubscribeActionsPanel.Controls.Add(this._CancelButton);
        }
        #endregion

        #region _UnsubscribeButton_Command
        /// <summary>
        /// Handles the "Unsubscribe" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _UnsubscribeButton_Command(object sender, CommandEventArgs e)
        {
            try
            {
                ReportSubscription.DeleteByToken(this._ReportSubscriptionToken, this._ReportSubscriptionUser);
                this.DisplayFeedback(_GlobalResources.YouHaveBeenUnsubscribedFromThisReport, false);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            { this.DisplayFeedback(_GlobalResources.UnableToFindSubscriptionYouMayHaveAlreadyUnsubscribedFromThisReportOrTheReportMayHaveBeenDeleted, true); }
            catch (AsentiaException ex)
            { this.DisplayFeedback(_GlobalResources.UnknownErrorOccurred, true); }

            this.ReportUnsubscribeInstructionsPanel.Visible = false;
            this.ReportUnsubscribeActionsPanel.Visible = false;
        }
        #endregion

        #region _CancelButton_Command
        /// <summary>
        /// Handles the "Cancel" button click.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void _CancelButton_Command(object sender, CommandEventArgs e)
        {
            Response.Redirect("/");
        }
        #endregion
    }
}
