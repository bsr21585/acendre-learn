﻿using System;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml;
using System.Threading;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LRS.Library;
using Asentia.UMS.Library;

namespace Asentia.LRS.Controls
{
    public class ReportForm : WebControl, INamingContainer
     {
        #region Constructors
        /// <summary>
        /// Constructor for building a user form pre-populated with report data.
        /// </summary>
        /// <param name="ReportObject">report object</param>
        public ReportForm(string id, UserAccountDataFileType fileType, string languageString, Report reportObject)
            : base(HtmlTextWriterTag.Div)
        {
            this.ID = id;
            this.UserAccountDataObj = new UserAccountData(fileType, true, false);
            this.ReportDataSetObj = new ReportDataSet();
            this.LanguageString = languageString;
            this.ReportObject = reportObject;
            this.IdDataset = ReportObject.IdDataset;
            this.IdReport = ReportObject.IdReport;
            this.IdReportSubscription = ReportObject.IdReportSubscription;
            this.IdSite = ReportObject.IdSite;
            this.IdUser = ReportObject.IdUser;
                            

            // build the form
            this._Initialize();
        }
        #endregion

        #region OnPreRender
        /// <summary>
        /// Overrides the OnPreRender method so that embeded jQuery, javascript and CSS resource(s) can be registered.
        /// </summary>
        /// <param name="e">Arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // register the embedded jQuery and javascript resource(s)
            ClientScriptManager csm = this.Page.ClientScript;

            csm.RegisterStartupScript(this.GetType(), "JsVariables", "var ThenByText = '" + _GlobalResources.ThenBy + "';" +
            "var FirstByText = '" + _GlobalResources.FirstBy + "';", true);
            csm.RegisterClientScriptResource(this.GetType(), "Asentia.LRS.Controls.ReportForm.js");

        }
        #endregion

        #region Properties
        /// <summary>
        /// User Account Data object.
        /// </summary>
        public UserAccountData UserAccountDataObj;

        /// <summary>
        /// Dataset object.
        /// </summary>
        public ReportDataSet ReportDataSetObj;

        /// <summary>
        /// Report object.
        /// </summary>
        public Report ReportObject;

        /// <summary>
        /// ReportFileGrid object.
        /// </summary>
        public Grid ReportFileGrid;

        /// <summary>
        /// The language of this form.
        /// </summary>
        public string LanguageString;

        /// <summary>
        /// Site Id.
        /// </summary>
        public int IdSite { get; set; }

        /// <summary>
        /// User Id.
        /// </summary>
        public int IdUser { get; set; }
       
        /// <summary>
        /// Dataset Id.
        /// </summary>
        public int IdDataset { get; set; }

        /// <summary>
        /// Report Id.
        /// </summary>
        public int IdReport { get; set; }

        /// <summary>
        /// Report Subscription Id.
        /// </summary>
        public int IdReportSubscription { get; set; }

        /// <summary>
        /// Report Title.
        /// </summary>
        public string Title { get { return ReportTitleTextBox.Text; } }

        /// <summary>
        /// Privacy IsPublic Value
        /// </summary>
        public bool IsPublic { get { return Convert.ToBoolean(_RadioButtonListIsPublic.SelectedValue); } }


        // VARIABLES FOR REPORT FORM TABS

        public string Fields
        { get { return this._GetInputValue("fields"); } }

        public string Filter
        { get { return this._GetInputValue("filter"); } }

        public string Order
        { get { return this._GetInputValue("order"); } }

        /// <summary>
        /// Subscription Value
        /// </summary>
        public bool IsSubscribed { get { return Convert.ToBoolean(_SubscriptionList.SelectedValue); } }

        public DateTime DTStart { get { return Convert.ToDateTime(_StartDatepicker.Value); } }

        public string RecurInterval { get { return _FrequencyTextBox.Text; } }

        public string RecurTimeFrame { get { return _FrequencyDropDownList.SelectedValue; } }

        public Panel TabFieldsContentPanel, TabFilterContentPanel, TabOrderContentPanel, TabPropertyContentPanel, TabSubscriptionContentPanel, TabReportFilesContentPanel;

        public TextBox ReportTitleTextBox;

        public LinkButton SaveFileDeleteButton = new LinkButton();

        // This is a copy from LMS.Library.Lesson, will be removed after moving all Enum into Asentia.Common  
        #region LessonContentType ENUM
        public enum LessonContentType
        {
            AdministratorOverride = 0,
            ContentPackage = 1,
            StandupTrainingModule = 2,
            Task = 3,
            OJT = 4,
        }
        #endregion

        #endregion

        #region Private Properties
        /// <summary>
        /// Private Property: LabelDataset Columns from the dataset
        /// </summary>        
        private List<ReportDataSet.DatasetColumnProperties> _LabelDataset;
        private List<_FilterConditionProperties> _LabelFilterCondition;
        private TextBox _FrequencyTextBox = new TextBox();
        private RadioButtonList _RadioButtonListIsPublic = new RadioButtonList();
        private RadioButtonList _SubscriptionList = new RadioButtonList();
        private DatePicker _StartDatepicker = new DatePicker("dpStartDateSubscription", false, false, true);
        private DropDownList _FrequencyDropDownList = new DropDownList();       
        private AsentiaPage _AsentiaPageInstance = new AsentiaPage();
        private Panel _TabPropertyPanel, _TabSubscriptionPanel;
        private HiddenField _ReportShortestDay = new HiddenField();

        // ADDED BY JC 9/25
        private MultiSelectWithOrdering _FieldsToInclude;
        private int _FilterNumber = 0;
        private int _OrderColumnNumber = 0;
        private const string _FilterConditionXMLFileLocation = "~/_bin/report/Report.FilterCondition.xml";     


        #region FilterConditionProperties
        /// <summary>
        /// Language specific column properties of a dataset.
        /// </summary>

        private class _FilterConditionProperties
        {
            public string Identifier;
            public string Label;
            public string DataType;
            public string Input;
            public string Operator;
        }

        #endregion
    
        #endregion

        #region Methods
        #region Private Methods
        #region _Initialize
        /// <summary>
        /// Builds the user form containers and fields.
        /// </summary>
        private void _Initialize()
        {
            /// Link to the dataset xml file to retreive the correct columns List
            ReportDataSetObj.LanguageString = this.LanguageString;

            if (this.IdDataset < 1000) // built-in dataset
            { ReportDataSetObj.DatasetColumnsDataFile = "~/_bin/dataset/Dataset." + Enum.GetName(typeof(EnumDataSet), this.IdDataset) + ".Columns.xml"; }
            else // custom dataset
            {
                ReportDataSet datasetObject = new ReportDataSet(this.IdDataset);
                ReportDataSetObj.DatasetColumnsDataFile = "~/_bin/dataset/" + datasetObject.StoredProcedureName.Replace("[", "").Replace("]", "") + ".Columns.xml";
            }

            /// Get the List of the columns from the dataset
            _LabelDataset = ReportDataSetObj.GetDataSetColumns();
            _LabelFilterCondition = _GetAllFilterConditionData();
            string fcsize = Convert.ToString(_LabelFilterCondition.Count);

            // compile string of month names
            string monthNames = "['"
                                + _GlobalResources.January_abbr + "','"
                                + _GlobalResources.February_abbr + "','"
                                + _GlobalResources.March_abbr + "','"
                                + _GlobalResources.April_abbr + "','"
                                + _GlobalResources.May + "','"
                                + _GlobalResources.June_abbr + "','"
                                + _GlobalResources.July_abbr + "','"
                                + _GlobalResources.August_abbr + "','"
                                + _GlobalResources.September_abbr + "','"
                                + _GlobalResources.October_abbr + "','"
                                + _GlobalResources.November_abbr + "','"
                                + _GlobalResources.December_abbr + "']";

            // compile string of day names
            string dayNames = "['"
                              + _GlobalResources.Sunday_abbr + "','"
                              + _GlobalResources.Monday_abbr + "','"
                              + _GlobalResources.Tuesday_abbr + "','"
                              + _GlobalResources.Wednesday_abbr + "','"
                              + _GlobalResources.Thursday_abbr + "','"
                              + _GlobalResources.Friday_abbr + "','"
                              + _GlobalResources.Saturday_abbr + "']";

            // previous button text
            string prevText = "'" + _GlobalResources.Prev + "'";

            // next button text
            string nextText = "'" + _GlobalResources.Next + "'";

            /// Create a multiple dimentional array in global js object to carry the filter information 
            Literal javascriptCode = new Literal();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type=\"text/javascript\">");
            sb.AppendLine(" var stringObj = {};");
            sb.AppendLine(" var integerObj = {};");
            sb.AppendLine(" var dateTimeObj = {};");
            sb.AppendLine(" var bitObj = {}; ");
            sb.AppendLine(" var statusObj = {}; ");
            sb.AppendLine(" var enumObj = {}; ");
            sb.AppendLine(" var containObj = {}; ");
            sb.AppendLine(" var showInputObj = {}; ");
            sb.AppendLine(" var conditionOperator = {}; ");
            sb.AppendLine(" var operatorId = {}; ");
            sb.AppendLine(" var addRowHdn, addOrderRowHdn; ");
            sb.AppendLine(" var seed = 1, orderColumnSeed = 1; ");
            sb.AppendLine(" var datePickerObj = {}; ");
            sb.AppendLine(" var filterObject = new Array(" + fcsize + ");");
            sb.AppendLine(" var customPattern = '1 '; ");
            sb.AppendLine(" var filterUsed = false; ");
            sb.AppendLine(" var shortestDays = 0, allowedMin = 0;");
            sb.AppendLine(" var MsgCharNotAllowed = '" + _GlobalResources.OnlyAndOrSpaceAndNumbersAllowed + "'; ");
            sb.AppendLine(" var MsgSaveBeforeDelete = '" + _GlobalResources.SavePatternBeforeDeletingAFilter + "'; ");
            sb.AppendLine(" var MsgSaveBeforeAdd = '" + _GlobalResources.SavePatternBeforeAddingAFilter + "'; ");
            sb.AppendLine(" var MsgNotInOrder = '" + _GlobalResources.TheNumbersInThisPatternAreNotInOrder + "'; ");
            sb.AppendLine(" var MsgAdjustAgain = '" + _GlobalResources.PleaseAdjustThePatternAgainAndSave + "'; ");
            sb.AppendLine(" var MsgNumNotMached = '" + _GlobalResources.TheNumberOfConditionsInThisPatternDoNotMatch + "'; ");
            sb.AppendLine(" var MsgNotBalancedparenthesis = '" + _GlobalResources.ParenthesisAreNotBalanced + "'; ");
            sb.AppendLine(" var ReportFilterStatusActive = '" + _GlobalResources.Active + "'; ");
            sb.AppendLine(" var ReportFilterStatusDisabled = '" + _GlobalResources.Disabled + "'; ");
            sb.AppendLine(" var ReportFilterStatusYes = '" + _GlobalResources.YES_REPORTFILTER + "'; ");
            sb.AppendLine(" var ReportFilterStatusNo = '" + _GlobalResources.NO_REPORTFILTER + "'; ");
            sb.AppendLine(" var idReport = '" + this.IdReport+ "'; ");
            for (int i = 0; i < _LabelFilterCondition.Count; i++)
            {
             sb.AppendLine("     filterObject[" + i + "] = new Array(5); ");
             sb.AppendLine(" 	 filterObject[" + i + "][0] = '" + _LabelFilterCondition[i].Identifier + "';");
             sb.AppendLine(" 	 filterObject[" + i + "][1] = '" + _LabelFilterCondition[i].Label + "';");
             sb.AppendLine(" 	 filterObject[" + i + "][2] = '" + _LabelFilterCondition[i].DataType + "';");
             sb.AppendLine(" 	 filterObject[" + i + "][3] = " + _LabelFilterCondition[i].Input + ";");
             sb.AppendLine(" 	 filterObject[" + i + "][4] = \"" + _LabelFilterCondition[i].Operator + "\";");
            }
            sb.AppendLine(" var optionGroupObject = new Array();");
            int totalNum = 0;
            string currentGroup = string.Empty;
            for (int i = 0; i < _LabelDataset.Count; i++)
            {
                if ((_LabelDataset[i].OptionGroup.Length > 1) && (_LabelDataset[i].OptionGroup != currentGroup))
                {
                    sb.AppendLine("  optionGroupObject[" + totalNum + "] = '" + _GlobalResources.ResourceManager.GetString(_LabelDataset[i].OptionGroup.Replace(" ", "")) + "';");
                    currentGroup = _LabelDataset[i].OptionGroup;
                    totalNum++;
                }
            }
            sb.AppendLine(" datePickerObj['dateFormat'] = '" + DatePicker.DateFormatJS + "' ; ");
            sb.AppendLine(" datePickerObj['nextText'] = " + nextText + " ; ");
            sb.AppendLine(" datePickerObj['prevText'] = " + prevText + " ; ");
            sb.AppendLine(" datePickerObj['dayNamesMin'] = " + dayNames + " ; ");
            sb.AppendLine(" datePickerObj['monthNamesShort'] = " + monthNames + " ; ");
            sb.AppendLine("</script>");
            javascriptCode.Text = sb.ToString();

            // attach controls
            this.Controls.Add(javascriptCode);

            // build tabs and user fields
            this._BuildTabsList();
            
            // build tab contents
            this._BuildReportForm();
        }
        #endregion

        #region _BuildTabsList
        /// <summary>
        /// Builds the container and tabs for the form.
        /// </summary>
        private void _BuildTabsList()
        {
            // queue up the tabs
            Queue<KeyValuePair<string, string>> tabs = new Queue<KeyValuePair<string, string>>();

            tabs.Enqueue(new KeyValuePair<string, string>("Fields", _GlobalResources.Columns));
            tabs.Enqueue(new KeyValuePair<string, string>("Filter", _GlobalResources.Filters));
            tabs.Enqueue(new KeyValuePair<string, string>("Order", _GlobalResources.Order));
            tabs.Enqueue(new KeyValuePair<string, string>("Property", _GlobalResources.Properties));

            // Administrator can not see this tab + (Report must be public or user owned report)
            if (AsentiaSessionState.IdSiteUser > 1 && (this.ReportObject.IsPublic || (this.ReportObject.IdUser == AsentiaSessionState.IdSiteUser)))
            { tabs.Enqueue(new KeyValuePair<string, string>("Subscription", _GlobalResources.Subscription)); }

            // Report Files TAB - only show when there are saved files
            if (this.ReportObject.NumberOfSavedFiles > 0)
            { tabs.Enqueue(new KeyValuePair<string, string>("ReportFiles", _GlobalResources.SavedFiles)); }

            // build and attach the tabs
            this.Controls.Add(AsentiaPage.BuildTabListPanel(this.ID, tabs));
        }
        #endregion

        #region _BuildReportForm
        /// <summary>
        /// Builds the tab content container for specific purpose of each tab
        /// </summary>
        private void _BuildReportForm()
        {
                this.TabFieldsContentPanel = new Panel();
                this.TabFieldsContentPanel.ID = this.ID + "_Fields_TabPanel";
                this.TabFieldsContentPanel.CssClass = "TabPanelsContentContainer";
                Panel tabFieldPanel = new Panel();
                tabFieldPanel.CssClass = "TabPanelContainer";
                this._BuildFieldsTabContent(tabFieldPanel);
                this.TabFieldsContentPanel.Controls.Add(tabFieldPanel);
                this.Controls.Add(this.TabFieldsContentPanel);

                this.TabFilterContentPanel = new Panel();
                this.TabFilterContentPanel.ID = this.ID + "_Filter_TabPanel";
                this.TabFilterContentPanel.CssClass = "TabPanelsContentContainer";
                this.TabFilterContentPanel.Style.Add("display", "none");
                this._BuildFilterTabContent();
                this.Controls.Add(this.TabFilterContentPanel);

                this.TabOrderContentPanel = new Panel();
                this.TabOrderContentPanel.ID = this.ID + "_Order_TabPanel";
                this.TabOrderContentPanel.CssClass = "TabPanelsContentContainer";
                this.TabOrderContentPanel.Style.Add("display", "none");
                this._BuildOrderTabContent();
                this.Controls.Add(this.TabOrderContentPanel);

                this.TabPropertyContentPanel = new Panel();
                this.TabPropertyContentPanel.ID = this.ID + "_Property_TabPanel";
                this.TabPropertyContentPanel.CssClass = "TabPanelsContentContainer";
                this.TabPropertyContentPanel.Style.Add("display", "none");
                this._BuildPropertyTabContent();
                this.Controls.Add(this.TabPropertyContentPanel);

                //Administrator can not see this tab + (Report must be public or user owned report)
                if (AsentiaSessionState.IdSiteUser > 1 && (this.ReportObject.IsPublic || (this.ReportObject.IdUser == AsentiaSessionState.IdSiteUser)))  
                {
                    this.TabSubscriptionContentPanel = new Panel();
                    this.TabSubscriptionContentPanel.ID = this.ID + "_Subscription_TabPanel";
                    this.TabSubscriptionContentPanel.CssClass = "TabPanelsContentContainer";
                    this.TabSubscriptionContentPanel.Style.Add("display", "none");
                    this._BuildSubscriptionTabContent();
                    this.Controls.Add(this.TabSubscriptionContentPanel);  
                }

                // Report Files TAB (Only show when there is exisitng files listed)
                if (this.ReportObject.NumberOfSavedFiles > 0)
                {
                    this.TabReportFilesContentPanel = new Panel();
                    this.TabReportFilesContentPanel.ID = this.ID + "_ReportFiles_TabPanel";
                    this.TabReportFilesContentPanel.CssClass = "TabPanelsContentContainer";
                    this.TabReportFilesContentPanel.Style.Add("display", "none");
                    this._BuildReportFilesTabContent();
                    this.Controls.Add(this.TabReportFilesContentPanel);
                }

                this.SaveFileDeleteButton.ID = "DeleteReportFileButton";
        }
        #endregion

        #region _BuildFieldsTabContent
        /// <summary>
        /// Builds the tab content container for Field tab
        /// </summary>
        private void _BuildFieldsTabContent(Control FieldsControls)
        {
            HiddenField reportField = new HiddenField();
            reportField.ID = "hdReportField";

            // create control for selecting fileds
            this._FieldsToInclude = new MultiSelectWithOrdering(this.ID + "_FieldsToInclude");

            if (this.ReportObject.Fields != null)
            {
                string reportObjectFields = this.ReportObject.Fields;
                List<string> preSelectedFields = new List<string>(reportObjectFields.Replace("[", "").Replace("]", "").Split(','));
                this._FieldsToInclude.PreSelectedValues = preSelectedFields;
            }
            // add available fields to control
            if (this._LabelDataset != null)
            {
                foreach (ReportDataSet.DatasetColumnProperties li in _LabelDataset)
                {
                    if (li.IsVisible)
                    { 
                        this._FieldsToInclude.AddAvailableItem(li.Label, li.Identifier, _GlobalResources.ResourceManager.GetString(li.OptionGroup, new CultureInfo(AsentiaSessionState.UserCulture)));
                    }
                }
            }

            List<Control> reportFieldControls = new List<Control>();

            Panel interactionInfoPanel = new Panel();
            interactionInfoPanel.ID = "InteractionInfoPanel";
            this._AsentiaPageInstance.FormatFormInformationPanel(interactionInfoPanel, _GlobalResources.WhenIncludingColumnsForInteractionDetailsInteractionDescriptionCorrectResponsesLearnerResponsesTheInteractionColumnWillAutomaticallyBeIncluded);

            if (this.IdDataset == (int)EnumDataSet.UserCourseTranscripts) {
                reportFieldControls.Add(interactionInfoPanel);
            }

            reportFieldControls.Add(reportField);
            reportFieldControls.Add(this._FieldsToInclude);

            FieldsControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ReportFieldsControls",
                                                                                        String.Empty,
                                                                                        reportFieldControls,
                                                                                        false,
                                                                                        true));
        }
        #endregion

        #region _BuildFilterTabContent
        /// <summary>
        /// Builds the tab content container for Order tab
        /// </summary>
        private void _BuildFilterTabContent()
        {
             _ReportShortestDay.ID = "hdShortestDay";
             this.TabFilterContentPanel.Controls.Add(_ReportShortestDay);

             Panel reportFieldsControls_Container = new Panel();
             reportFieldsControls_Container.ID = "ReportFilterControls_Container";
             reportFieldsControls_Container.CssClass = "FormFieldContainer";

             Panel formFilterErrorContainer = new Panel();
             formFilterErrorContainer.ID = "ReportFilterControls_ErrorContainer";
             formFilterErrorContainer.CssClass = "FormFieldErrorContainer";
             reportFieldsControls_Container.Controls.Add(formFilterErrorContainer);

             this.TabFilterContentPanel.Controls.Add(reportFieldsControls_Container);

             Panel tabFilterZeroPanel = new Panel();
             tabFilterZeroPanel.CssClass = "TabPanelContainer";
             tabFilterZeroPanel.ID = "Filter_Zero_Panel";

             ///add add icon to the filterRow 
             HyperLink filterAddIconZero = new HyperLink();
             filterAddIconZero.ID = "filterAddFilterZero";
             filterAddIconZero.CssClass = "ImageLinkZero ImageLink";
             filterAddIconZero.NavigateUrl = "javascript:void(0);";
             filterAddIconZero.Attributes.Add("onclick", "AddFirstFilterRow();");
             filterAddIconZero.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD,
                                                                 ImageFiles.EXT_PNG);

             Label filterZeroInstruction = new Label();
             filterZeroInstruction.ID = "filterAddFilterZeroText";
             filterZeroInstruction.CssClass = "labelFilterZero";
             filterZeroInstruction.Text = _GlobalResources.ClickAddIconToSpecifyTheReportFilters;

             tabFilterZeroPanel.Controls.Add(filterAddIconZero);
             tabFilterZeroPanel.Controls.Add(filterZeroInstruction);
             this.TabFilterContentPanel.Controls.Add(tabFilterZeroPanel);

             Panel tabFilterRowPanel = new Panel();
             tabFilterRowPanel.ID = "Main_FilterRow_Panel";
             _BuildFilterRow(tabFilterRowPanel);
             this.TabFilterContentPanel.Controls.Add(tabFilterRowPanel);

             Panel tabFilterCriteriaPanel = new Panel();
             tabFilterCriteriaPanel.ID = "Filter_Criteria_Panel";
             _BuildFilterCriteria(tabFilterCriteriaPanel);

             this.TabFilterContentPanel.Controls.Add(tabFilterCriteriaPanel);
        }
        #endregion

        #region _BuildOrderTabContent
        /// <summary>
        /// Builds the tab content container for Order tab
        /// </summary>
        private void _BuildOrderTabContent()
        {
            Panel tabOrderPanel = new Panel();
            tabOrderPanel.CssClass = "TabPanelContainer";
            tabOrderPanel.ID = "Report_Order_Panel";
            this._BuildOrderSelector(tabOrderPanel);
            this.TabOrderContentPanel.Controls.Add(tabOrderPanel);
        }
        #endregion

        #region _BuildOrderRow
        ///<summary>
        /// Builds the order selector
        /// </summary>
        private void _BuildOrderSelector(Control orderControls)
        {
            HiddenField reportOrderBy = new HiddenField();
            reportOrderBy.ID = "hdReportOrderBy";
            reportOrderBy.Value = this.ReportObject.Order;
            _OrderColumnNumber++;

            /// Panel for the filter row
            Panel orderColumnPanel = new Panel();
            orderColumnPanel.ID = "panelOrderColumnNum0";
            orderColumnPanel.CssClass = "labelOrderColumnNumber";

            List<Control> orderColumnPanelControls = new List<Control>();

            HiddenField numOrderColumn = new HiddenField();
            numOrderColumn.ID = "labelOrderColumnNum0";

            /// Build dropdownlist for order columns
            DropDownList ddlDatasetColumns = new DropDownList();
            ddlDatasetColumns.ID = "ddlDatasetColumnsOrderNum0";
            ddlDatasetColumns.CssClass = "orderDDL";


            ///add the Dataset Columns List to the DropDownList
            if (_LabelDataset != null)
                foreach (ReportDataSet.DatasetColumnProperties li in _LabelDataset)
                    if (li.IsVisible)
                    {
                        ListItem field = new ListItem(li.Label, li.Identifier);
                        ddlDatasetColumns.Items.Add(field);
                    }

            ///add radio buttons for ascending or descending

            RadioButtonList radioListOrderColumn = new RadioButtonList();
            radioListOrderColumn.ID = "OrderColumnAscendingNum0";
            radioListOrderColumn.CssClass = "orderRadioList";

            ListItem ascending = new ListItem(_GlobalResources.Ascending, "asc");
            radioListOrderColumn.Items.Add(ascending);

            ListItem descending = new ListItem(_GlobalResources.Descending, "desc");
            radioListOrderColumn.Items.Add(descending);

            radioListOrderColumn.SelectedIndex = 0;

            ///add delete icon to the orderRow 
            HyperLink orderDeleteIcon = new HyperLink();
            orderDeleteIcon.CssClass = "OrderImageLink ImageLink";
            orderDeleteIcon.ID = "orderDeleteNum0";            
            orderDeleteIcon.NavigateUrl = "javascript:void(0);";
            orderDeleteIcon.Attributes.Add("onclick", "OrderDeleteRow('Num0');");
            orderDeleteIcon.Attributes.Add("style", "display:none;");
            orderDeleteIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                              ImageFiles.EXT_PNG);

            ///add add icon to the orderRow 
            HyperLink orderAddIcon = new HyperLink();
            orderAddIcon.CssClass = "OrderImageLink ImageLink";
            orderAddIcon.ID = "orderAddNum0";
            orderAddIcon.NavigateUrl = "javascript:void(0);";
            orderAddIcon.Attributes.Add("onclick", "OrderAddRow();");
            orderAddIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD,
                                                           ImageFiles.EXT_PNG);

            orderColumnPanelControls.Add(numOrderColumn);
            orderColumnPanelControls.Add(ddlDatasetColumns);
            orderColumnPanelControls.Add(radioListOrderColumn);
            orderColumnPanelControls.Add(orderDeleteIcon);
            orderColumnPanelControls.Add(orderAddIcon);

            orderColumnPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("orderColumnPanelControls",
                                                                                        _GlobalResources.FirstBy,
                                                                                        orderColumnPanelControls,
                                                                                        false,
                                                                                        false));

            orderControls.Controls.Add(reportOrderBy);
            orderControls.Controls.Add(orderColumnPanel);
        }
        #endregion

        #region _BuildFilterRow
        /// <summary>
        /// Builds the filter row the tab content container 
        /// </summary>
        private void _BuildFilterRow(Control filterRowControls)
        {
            _FilterNumber++;

            /// Panel for the filter row
            Panel filterRowPanel = new Panel();
            filterRowPanel.ID = "RowPanelFilterNum0";

            Label numFilter = new Label();
            numFilter.ID = "labelNum0";
            numFilter.CssClass = "ReportFormFilterLabelNumber";
            numFilter.Text = _FilterNumber.ToString();

            /// build and/or drowp down list
            DropDownList ddlAndOr = new DropDownList();
            ddlAndOr.ID = "ddlAndOrNum0";
            ddlAndOr.CssClass = "ReportFormFilterAndOr";
            ListItem listAnd = new ListItem(_GlobalResources.AND_UPPER, "and");
            ddlAndOr.Items.Add(listAnd);
            ListItem listOr = new ListItem(_GlobalResources.OR_UPPER, "or");
            ddlAndOr.Items.Add(listOr);


            /// build dropdownlist for dataset columns
            DropDownList ddlDatasetColumns = new DropDownList();
            ddlDatasetColumns.ID = "ddlDatasetColumnsFilterNum0";
            ddlDatasetColumns.CssClass = "ReportFormFilterColumns";
            ddlDatasetColumns.Attributes.Add("onChange", "DatasetColumnsOnChange('Num0');");

            ///add the Dataset Columns List to the DropDownList
            if (_LabelDataset != null)
                foreach (ReportDataSet.DatasetColumnProperties li in _LabelDataset)
                    if (li.IsVisible && li.Identifier != "Interaction Description" && li.Identifier != "Interaction Type" && li.Identifier != "Learner Response" && li.Identifier != "Correct Responses")
                    {
                        ListItem field = new ListItem(li.Label, li.Identifier);
                        field.Attributes.Add("dataType", li.DataType);
                        field.Attributes.Add("classification", _GlobalResources.ResourceManager.GetString(li.OptionGroup, new CultureInfo(AsentiaSessionState.UserCulture)));
                        ddlDatasetColumns.Items.Add(field);
                    }

            /// build dropdownlist for filter conditions, data is filled from js function
            DropDownList ddlFilterCondition = new DropDownList();
            ddlFilterCondition.ID = "ddlFilterConditionFilterNum0";
            ddlFilterCondition.CssClass = "ReportFormFilterCondition";
            ddlFilterCondition.Attributes.Add("onChange", "FilterConditionOnChange('Num0');");

            ///text box area for string input
            TextBox tbFilter = new TextBox();
            tbFilter.ID = "tbFilterNum0";
            tbFilter.CssClass = "ReportFormFilterTextBox";
            
            tbFilter.Attributes.Add("onChange", "UpdateQueryString();");

            ///date picker for DateTime input
            DatePicker datepicker = new DatePicker("dpFilterNum0", false, false, false);
            datepicker.LowYear = 1900;
            datepicker.CssClass = "DatePickerFilter";
            datepicker.Attributes.Add("style", "display:none;");
            datepicker.Attributes.Add("onChange", "UpdateQueryString();");

            Label labelBw = new Label();
            labelBw.ID = "labelBwFilterNum0";
            labelBw.Text = _GlobalResources.and_lower + " ";
            labelBw.Attributes.Add("style", "display:none;");

            ///date picker for DateTime input
            DatePicker datepickerbw = new DatePicker("dpFilterBwNum0", false, false, false);
            datepickerbw.LowYear = 1900;
            datepickerbw.CssClass = "DatePickerFilter";
            datepickerbw.Attributes.Add("style", "display:none;");
            datepickerbw.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Committed Content Type
            DropDownList ddlEnum = new DropDownList();
            ddlEnum.ID = "ddlEnumNum0";
            ddlEnum.CssClass = "ReportFormFilterValueColumn";
            ddlEnum.Attributes.Add("style", "display:none;");

            ddlEnum.Items.Add(new ListItem(_GlobalResources.AdministratorOverride, Convert.ToInt32(LessonContentType.AdministratorOverride).ToString()));
            ddlEnum.Items.Add(new ListItem(_GlobalResources.ContentPackage, Convert.ToInt32(LessonContentType.ContentPackage).ToString()));
            ddlEnum.Items.Add(new ListItem(_GlobalResources.InstructorLedTrainingModule, Convert.ToInt32(LessonContentType.StandupTrainingModule).ToString()));
            ddlEnum.Items.Add(new ListItem(_GlobalResources.Task, Convert.ToInt32(LessonContentType.Task).ToString()));
            ddlEnum.Items.Add(new ListItem(_GlobalResources.OJT, Convert.ToInt32(LessonContentType.OJT).ToString()));
            ddlEnum.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filters
            DropDownList ddlCourseStatus = new DropDownList();
            ddlCourseStatus.ID = "ddlCourseStatusNum0";
            ddlCourseStatus.CssClass = "ReportFormFilterValueColumn";
            ddlCourseStatus.Attributes.Add("style", "display:none;");

            ddlCourseStatus.Items.Add(new ListItem(_GlobalResources.Completed, "completed"));
            ddlCourseStatus.Items.Add(new ListItem(_GlobalResources.Expired, "expired"));
            ddlCourseStatus.Items.Add(new ListItem(_GlobalResources.Overdue, "overdue"));
            ddlCourseStatus.Items.Add(new ListItem(_GlobalResources.Future, "future"));
            ddlCourseStatus.Items.Add(new ListItem(_GlobalResources.Enrolled, "enrolled"));
            ddlCourseStatus.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Learning Path Status
            DropDownList ddlLearningPathStatus = new DropDownList();
            ddlLearningPathStatus.ID = "ddlLearningPathStatusNum0";
            ddlLearningPathStatus.CssClass = "ReportFormFilterValueColumn";
            ddlLearningPathStatus.Attributes.Add("style", "display:none;");

            ddlLearningPathStatus.Items.Add(new ListItem(_GlobalResources.Completed, "completed"));
            ddlLearningPathStatus.Items.Add(new ListItem(_GlobalResources.Expired, "expired"));
            ddlLearningPathStatus.Items.Add(new ListItem(_GlobalResources.Overdue, "overdue"));
            ddlLearningPathStatus.Items.Add(new ListItem(_GlobalResources.Future, "future"));
            ddlLearningPathStatus.Items.Add(new ListItem(_GlobalResources.Enrolled, "enrolled"));
            ddlLearningPathStatus.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Lesson Completion
            DropDownList ddlLessonCompletion = new DropDownList();
            ddlLessonCompletion.ID = "ddlLessonCompletionNum0";
            ddlLessonCompletion.CssClass = "ReportFormFilterValueColumn";
            ddlLessonCompletion.Attributes.Add("style", "display:none;");

            ddlLessonCompletion.Items.Add(new ListItem(_GlobalResources.Completed, "completed"));
            ddlLessonCompletion.Items.Add(new ListItem(_GlobalResources.Incomplete, "incomplete"));
            ddlLessonCompletion.Items.Add(new ListItem(_GlobalResources.Unknown, "unknown"));
            ddlLessonCompletion.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Lesson Success
            DropDownList ddlLessonSuccess = new DropDownList();
            ddlLessonSuccess.ID = "ddlLessonSuccessNum0";
            ddlLessonSuccess.CssClass = "ReportFormFilterValueColumn";
            ddlLessonSuccess.Attributes.Add("style", "display:none;");

            ddlLessonSuccess.Items.Add(new ListItem(_GlobalResources.Passed, "passed"));
            ddlLessonSuccess.Items.Add(new ListItem(_GlobalResources.Failed, "failed"));
            ddlLessonSuccess.Items.Add(new ListItem(_GlobalResources.Unknown, "unknown"));
            ddlLessonSuccess.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Completion Status
            DropDownList ddlCompletionStatus = new DropDownList();
            ddlCompletionStatus.ID = "ddlCompletionStatusNum0";
            ddlCompletionStatus.CssClass = "ReportFormFilterValueColumn";
            ddlCompletionStatus.Attributes.Add("style", "display:none;");

            ddlCompletionStatus.Items.Add(new ListItem(_GlobalResources.Completed, "completed"));
            ddlCompletionStatus.Items.Add(new ListItem(_GlobalResources.Incomplete, "incomplete"));
            ddlCompletionStatus.Items.Add(new ListItem(_GlobalResources.Unknown, "unknown"));
            ddlCompletionStatus.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Success Status
            DropDownList ddlSuccessStatus = new DropDownList();
            ddlSuccessStatus.ID = "ddlSuccessStatusNum0";
            ddlSuccessStatus.CssClass = "ReportFormFilterValueColumn";
            ddlSuccessStatus.Attributes.Add("style", "display:none;");

            ddlSuccessStatus.Items.Add(new ListItem(_GlobalResources.Passed, "passed"));
            ddlSuccessStatus.Items.Add(new ListItem(_GlobalResources.Failed, "failed"));
            ddlSuccessStatus.Items.Add(new ListItem(_GlobalResources.Unknown, "unknown"));
            ddlSuccessStatus.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Content Types
            DropDownList ddlContentTypes = new DropDownList();
            ddlContentTypes.ID = "ddlContentTypesNum0";
            ddlContentTypes.CssClass = "ReportFormFilterValueColumn";
            ddlContentTypes.Attributes.Add("style", "display:none;");

            ddlContentTypes.Items.Add(new ListItem(_GlobalResources.ContentPackage, Convert.ToInt32(LessonContentType.ContentPackage).ToString()));
            ddlContentTypes.Items.Add(new ListItem(_GlobalResources.InstructorLedTrainingModule, Convert.ToInt32(LessonContentType.StandupTrainingModule).ToString()));
            ddlContentTypes.Items.Add(new ListItem(_GlobalResources.Task, Convert.ToInt32(LessonContentType.Task).ToString()));
            ddlContentTypes.Items.Add(new ListItem(_GlobalResources.OJT, Convert.ToInt32(LessonContentType.OJT).ToString()));
            ddlContentTypes.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Certification Track
            DropDownList ddlCertificationTrack = new DropDownList();
            ddlCertificationTrack.ID = "ddlCertificationTrackNum0";
            ddlCertificationTrack.CssClass = "ReportFormFilterValueColumn";
            ddlCertificationTrack.Attributes.Add("style", "display:none;");

            ddlCertificationTrack.Items.Add(new ListItem(_GlobalResources.Initial, 0.ToString()));
            ddlCertificationTrack.Items.Add(new ListItem(_GlobalResources.Renewal, 1.ToString()));
            ddlCertificationTrack.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filters
            DropDownList ddlCertificationStatus = new DropDownList();
            ddlCertificationStatus.ID = "ddlCertificationStatusNum0";
            ddlCertificationStatus.CssClass = "ReportFormFilterValueColumn";
            ddlCertificationStatus.Attributes.Add("style", "display:none;");

            ddlCertificationStatus.Items.Add(new ListItem(_GlobalResources.Current, "current"));
            ddlCertificationStatus.Items.Add(new ListItem(_GlobalResources.Expired, "expired"));
            ddlCertificationStatus.Items.Add(new ListItem(_GlobalResources.Incomplete, "incomplete"));
            ddlCertificationStatus.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Roster Status
            DropDownList ddlRosterStatus = new DropDownList();
            ddlRosterStatus.ID = "ddlRosterStatusNum0";
            ddlRosterStatus.CssClass = "ReportFormFilterValueColumn";
            ddlRosterStatus.Attributes.Add("style", "display:none;");

            ddlRosterStatus.Items.Add(new ListItem(_GlobalResources.Enrolled, "enrolled"));
            ddlRosterStatus.Items.Add(new ListItem(_GlobalResources.WaitingList, "waitlist"));
            ddlRosterStatus.Attributes.Add("onChange", "UpdateQueryString();");

            ///build dropdownlist for enum filter for Purchase Item Type
            DropDownList ddlPurchaseItemType = new DropDownList();
            ddlPurchaseItemType.ID = "ddlPurchaseItemTypeNum0";
            ddlPurchaseItemType.CssClass = "ReportFormFilterValueColumn";
            ddlPurchaseItemType.Attributes.Add("style", "display:none;");

            ddlPurchaseItemType.Items.Add(new ListItem(_GlobalResources.Course, "Course"));
            ddlPurchaseItemType.Items.Add(new ListItem(_GlobalResources.Catalog, "Catalog"));
            ddlPurchaseItemType.Items.Add(new ListItem(_GlobalResources.LearningPath, "Learning Path"));
            ddlPurchaseItemType.Items.Add(new ListItem(_GlobalResources.InstructorLedTraining, "Instructor Led Training"));
            ddlPurchaseItemType.Attributes.Add("onChange", "UpdateQueryString();");

            /// build dropdownlist for language
            LanguageSelector ddlLanguage = new LanguageSelector(LanguageDropDownType.Object);
            ddlLanguage.ID = "ddlLanguageNum0";
            ddlLanguage.CssClass = "ReportFormFilterValueColumn";
            ddlLanguage.SelectedValue = AsentiaSessionState.UserCulture;
            ddlLanguage.Attributes.Add("style", "display:none;");
            ddlLanguage.Attributes.Add("onChange", "UpdateQueryString();");

            /// build dropdownlist for timezone
            TimeZoneSelector ddlTimezone = new TimeZoneSelector();
            ddlTimezone.ID = "ddlTimezoneNum0";
            ddlTimezone.CssClass = "ReportFormFilterValueColumn";
            ddlTimezone.SelectedValue = AsentiaSessionState.GlobalSiteObject.IdTimezone.ToString();
            ddlTimezone.Attributes.Add("style", "display:none;");
            ddlTimezone.Attributes.Add("onChange", "UpdateQueryString();");
            
            ///add delete icon to the filterRow 
            HyperLink filterDeleteIcon = new HyperLink();
            filterDeleteIcon.ID = "filterDeleteFilterNum0";
            filterDeleteIcon.CssClass = "FilterImageLink ImageLink";
            filterDeleteIcon.NavigateUrl = "javascript:void(0);";
            filterDeleteIcon.Attributes.Add("onclick", "FilterDeleteRow('Num0');");
            filterDeleteIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                               ImageFiles.EXT_PNG);

            ///add add icon to the filterRow 
            HyperLink filterAddIcon = new HyperLink();
            filterAddIcon.ID = "filterAddFilterNum0";
            filterAddIcon.CssClass = "FilterImageLink ImageLink";
            filterAddIcon.NavigateUrl = "javascript:void(0);";
            filterAddIcon.Attributes.Add("onclick", "FilterAddRow();");
            filterAddIcon.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_ADD,
                                                            ImageFiles.EXT_PNG);

            filterRowPanel.Controls.Add(numFilter);
            filterRowPanel.Controls.Add(ddlAndOr);
            filterRowPanel.Controls.Add(ddlDatasetColumns);
            filterRowPanel.Controls.Add(ddlFilterCondition);
            filterRowPanel.Controls.Add(tbFilter);
            filterRowPanel.Controls.Add(datepicker);
            filterRowPanel.Controls.Add(labelBw);
            filterRowPanel.Controls.Add(datepickerbw);
            filterRowPanel.Controls.Add(ddlEnum);
            filterRowPanel.Controls.Add(ddlContentTypes);
            filterRowPanel.Controls.Add(ddlCertificationTrack);
            filterRowPanel.Controls.Add(ddlCertificationStatus);
            filterRowPanel.Controls.Add(ddlCourseStatus);
            filterRowPanel.Controls.Add(ddlLearningPathStatus);
            filterRowPanel.Controls.Add(ddlLessonCompletion);
            filterRowPanel.Controls.Add(ddlLessonSuccess);
            filterRowPanel.Controls.Add(ddlCompletionStatus);
            filterRowPanel.Controls.Add(ddlSuccessStatus);
            filterRowPanel.Controls.Add(ddlRosterStatus);
            filterRowPanel.Controls.Add(ddlPurchaseItemType);
            filterRowPanel.Controls.Add(ddlLanguage);
            filterRowPanel.Controls.Add(ddlTimezone);
            filterRowPanel.Controls.Add(filterDeleteIcon);
            filterRowPanel.Controls.Add(filterAddIcon);

            List<Control> criteriaControls = new List<Control>();
            criteriaControls.Add(filterRowPanel);

            filterRowControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("Criteria",
                                                                                           _GlobalResources.Criteria,
                                                                                           criteriaControls,
                                                                                           false,
                                                                                           false));
        }
        #endregion

        #region _BuildFilterCriteria
        /// <summary>
        /// Builds the filter criteria at the bottom of the tab content container 
        /// </summary>
        private void _BuildFilterCriteria(Control filterCriteriaControls)
        {
            HiddenField reportFilter = new HiddenField();
            reportFilter.ID = "hdReportFilter";
            reportFilter.Value = this.ReportObject.Filter;
            
            Label criteriaContent = new Label();
            criteriaContent.ID = "criteriaContentLabel";
            criteriaContent.Text = "1";
 
            ///text box area for changing criteria pattern
            TextBox criteriaContentTextBox = new TextBox();
            criteriaContentTextBox.ID = "criteriaContentTextBox";
            criteriaContentTextBox.CssClass = "criteriaContentTextBox";
            criteriaContentTextBox.Attributes.Add("style", "display:none;");

            HyperLink changePatternButton = new HyperLink();
            changePatternButton.ID = "changePatternButton";
            changePatternButton.Text = _GlobalResources.ChangePattern;
            changePatternButton.NavigateUrl = "javascript:void(0);";
            changePatternButton.Attributes.Add("onclick", "ChangePattern();");
            changePatternButton.Attributes.Add("style", "display:none;");

            HyperLink saveChangePatternButton = new HyperLink();
            saveChangePatternButton.ID = "saveChangePatternButton";
            saveChangePatternButton.Text = _GlobalResources.SaveChanges;
            saveChangePatternButton.NavigateUrl = "javascript:void(0);";
            saveChangePatternButton.Attributes.Add("onclick", "SaveChangePattern();");
            saveChangePatternButton.Attributes.Add("style", "display:none;");

            Label divider = new Label();
            divider.ID = "divider";
            divider.Text = "|";
            divider.Attributes.Add("style", "display:none;");

            HyperLink cancelChangePatternButton = new HyperLink();
            cancelChangePatternButton.ID = "cancelChangePatternButton";
            cancelChangePatternButton.Text = _GlobalResources.Cancel;
            cancelChangePatternButton.NavigateUrl = "javascript:void(0);";
            cancelChangePatternButton.Attributes.Add("onclick", "CancelChangePattern();");
            cancelChangePatternButton.Attributes.Add("style", "display:none;");

            List<Control> criteriaPatternControls = new List<Control>();
            criteriaPatternControls.Add(reportFilter);
            criteriaPatternControls.Add(criteriaContent);
            criteriaPatternControls.Add(criteriaContentTextBox);
            criteriaPatternControls.Add(changePatternButton);
            criteriaPatternControls.Add(saveChangePatternButton);
            criteriaPatternControls.Add(divider);
            criteriaPatternControls.Add(cancelChangePatternButton);

            filterCriteriaControls.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("CriteriaPattern",
                                                                                               _GlobalResources.CriteriaPattern,
                                                                                               criteriaPatternControls,
                                                                                               false,
                                                                                               false));
        }
        #endregion

        #region _GetAllFilterConditionData
        /// <summary>
        /// fill _AllFilterConditionDictionary with all kind of filter condition.
        /// </summary>
        private List<_FilterConditionProperties> _GetAllFilterConditionData()
        {
            List<_FilterConditionProperties> filterConditionList = new List<_FilterConditionProperties>();
            _FilterConditionProperties filterCondition;

            //Read XML document
            XmlDocument document = new XmlDocument();

            if (File.Exists(HttpContext.Current.Server.MapPath(_FilterConditionXMLFileLocation)))
            {
                document.Load(HttpContext.Current.Server.MapPath(_FilterConditionXMLFileLocation));
            }

            XmlNodeList nodeListFilterCondition = document.SelectNodes("conditions/condition");

            //Read all the condition information and add to the List

            foreach (XmlNode node in nodeListFilterCondition)
            {
                filterCondition = new _FilterConditionProperties();
                filterCondition.Identifier = node.Attributes["identifier"].Value;
                filterCondition.DataType = node.Attributes["dataType"].Value;

                if (filterCondition.Identifier.Replace(" ", "").ToUpper() == "YES")
                { filterCondition.Label = _GlobalResources.ResourceManager.GetString(filterCondition.Identifier.Replace(" ", "").ToUpper() + "_REPORTFILTER"); }
                else if (filterCondition.Identifier.Replace(" ", "").ToUpper() == "NO")
                { filterCondition.Label = _GlobalResources.ResourceManager.GetString(filterCondition.Identifier.Replace(" ", "").ToUpper() + "_REPORTFILTER"); }
                else if ((filterCondition.Identifier.Replace(" ", "").ToUpper() == "ACTIVE") || (filterCondition.Identifier.Replace(" ", "").ToUpper() == "DISABLED"))
                { filterCondition.Label = _GlobalResources.ResourceManager.GetString(filterCondition.Identifier.Replace(" ", "")); }
                else
                { filterCondition.Label = _GlobalResources.ResourceManager.GetString(filterCondition.Identifier.Replace(" ", "").ToUpper()); }
     
                filterCondition.Input = node.Attributes["input"].Value;
                filterCondition.Operator = node.Attributes["operator"].Value;
                filterConditionList.Add(filterCondition);
            }

            return filterConditionList;
        }

        #endregion

        #region _BuildPropertyTabContent
        /// <summary>
        /// Builds the tab content container for Subscription tab
        /// </summary>
        private void _BuildPropertyTabContent()
        {
            _TabPropertyPanel = new Panel();
            _TabPropertyPanel.CssClass = "TabPanelContainer";
            _TabPropertyPanel.ID = "Report_Property_Panel";


            // report name field
            this.ReportTitleTextBox = new TextBox();
            this.ReportTitleTextBox.ID = "ReportTitleText_Field";
            this.ReportTitleTextBox.CssClass = "InputLong";

            _TabPropertyPanel.Controls.Add(AsentiaPage.BuildFormField("ReportTitleText",
                                                                      _GlobalResources.Name,
                                                                      this.ReportTitleTextBox.ID,
                                                                      this.ReportTitleTextBox,
                                                                      true,
                                                                      true,
                                                                      true));

            //Privacy Instruction
            List<Control> privacyControls = new List<Control>();
            
            Panel privacyCustomInfoPanel = new Panel();
            privacyCustomInfoPanel.ID = "PrivacyCustomInfoPanel";
            this._AsentiaPageInstance.FormatFormInformationPanel(privacyCustomInfoPanel, _GlobalResources.SelectPublicToAllowOthersToViewAndRunYourReport);
            privacyControls.Add(privacyCustomInfoPanel);
            
            //Radio Button List for Privacy Options
            _RadioButtonListIsPublic.ID = this.ID + "_IsPublic_Field";
            _RadioButtonListIsPublic.RepeatDirection = RepeatDirection.Horizontal;
            _RadioButtonListIsPublic.RepeatLayout = RepeatLayout.Flow;

            ListItem privacyPublic = new ListItem(_GlobalResources.Public, "true");
            _RadioButtonListIsPublic.Items.Add(privacyPublic);

            ListItem privacyPrivate = new ListItem(_GlobalResources.Private, "false");
            _RadioButtonListIsPublic.Items.Add(privacyPrivate);

            _RadioButtonListIsPublic.SelectedIndex = (this.ReportObject.IsPublic) ? 0 : 1;
            privacyControls.Add(_RadioButtonListIsPublic);

            this._TabPropertyPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("_IsPublic",
                                                                                          _GlobalResources.Privacy,
                                                                                          privacyControls,
                                                                                          false,
                                                                                          false));

            this.TabPropertyContentPanel.Controls.Add(this._TabPropertyPanel);

        }
        #endregion

        #region _BuildSubscriptionTabContent
        /// <summary>
        /// Builds the tab content container for Subscription tab
        /// </summary>
        private void _BuildSubscriptionTabContent()
        {
            _TabSubscriptionPanel = new Panel();
            _TabSubscriptionPanel.CssClass = "TabPanelContainer";
            _TabSubscriptionPanel.ID = "Report_Subscription_Panel";

            ///Report Subscription Instruction
            Panel subscriptionInfoPanel = new Panel();
            subscriptionInfoPanel.ID = "SubscriptionInfoPanel";
            this._AsentiaPageInstance.FormatFormInformationPanel(subscriptionInfoPanel, _GlobalResources.UseTheFormBelowToConfigureYourSubscriptionToThisReport);
            _TabSubscriptionPanel.Controls.Add(subscriptionInfoPanel);

            //Subscription
            List<Control> subscriptionControl = new List<Control>();

            //Radio Button List for Privacy Options
            _SubscriptionList.ID = this.ID + "_Subscription_Field";
            _SubscriptionList.RepeatDirection = RepeatDirection.Horizontal;
            _SubscriptionList.RepeatLayout = RepeatLayout.Flow;

            ListItem subscriptionOn = new ListItem(_GlobalResources.On, "true");
            _SubscriptionList.Items.Add(subscriptionOn);

            ListItem subscriptionOff = new ListItem(_GlobalResources.Off, "false");
            _SubscriptionList.Items.Add(subscriptionOff);

            _SubscriptionList.SelectedIndex = (!String.IsNullOrWhiteSpace(this.ReportObject.RecurInterval.ToString()) && (this.ReportObject.RecurInterval > 0)) ? 0 : 1;
            subscriptionControl.Add(_SubscriptionList);

            this._TabSubscriptionPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("_Subscription",
                                                                                                    _GlobalResources.Subscription,
                                                                                                    subscriptionControl,
                                                                                                    false,
                                                                                                    false));

            #region Start Time Section

            Label lblStartDateInput = new Label();
            lblStartDateInput.Text = _GlobalResources.WhenShouldThisSubscriptionBegin;
            
            _StartDatepicker.LowYear = DateTime.UtcNow.Year;
            _StartDatepicker.HighYear = DateTime.UtcNow.Year + 3;
            _StartDatepicker.Value = this.ReportObject.DtSubscriptionStart;
            
            Label lblLocalTimeZone = new Label();
            lblLocalTimeZone.Text = AsentiaSessionState.UserTimezoneDisplayName;

            List<Control> subscriptionStartDateControls = new List<Control>();
            subscriptionStartDateControls.Add(lblStartDateInput);
            _StartDatepicker.Controls.Add(lblLocalTimeZone);
            subscriptionStartDateControls.Add(_StartDatepicker);

            this._TabSubscriptionPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ReportSubscriptionStartDate",
                                                                                                _GlobalResources.StartDate,
                                                                                                subscriptionStartDateControls,
                                                                                                true,
                                                                                                true));
            #endregion

            #region Frequency Section
            Panel pnlLabelFrequency = new Panel();

            Label lblFrequencyInput = new Label();
            lblFrequencyInput.Text = _GlobalResources.HowOftenShouldTheReportBeDelivered;

            ///Panel for input information and input textbox           
            Panel pnlFrequencyInput = new Panel();
            pnlFrequencyInput.ID = "SubscriptionFrequencySection";

            Label lblEvery = new Label();
            lblEvery.Text = _GlobalResources.Every;
            pnlFrequencyInput.Controls.Add(lblEvery);

            _FrequencyTextBox.ID = "tbFrequency";
            _FrequencyTextBox.CssClass = "InputXShort";
            _FrequencyTextBox.Text = this.ReportObject.RecurInterval.ToString();
            pnlFrequencyInput.Controls.Add(_FrequencyTextBox);

            _FrequencyDropDownList.ID = "ddlFrequency";
            _FrequencyDropDownList.Items.Add(new ListItem(_GlobalResources.Year_s, "yyyy"));
            _FrequencyDropDownList.Items.Add(new ListItem(_GlobalResources.Month_s, "m"));
            _FrequencyDropDownList.Items.Add(new ListItem(_GlobalResources.Week_s, "ww"));
            _FrequencyDropDownList.Items.Add(new ListItem(_GlobalResources.Day_s, "d"));
            _FrequencyDropDownList.SelectedValue = this.ReportObject.RecurTimeframe;
            pnlFrequencyInput.Controls.Add(_FrequencyDropDownList);

            List<Control> subscriptionFrequencyControls = new List<Control>();
            subscriptionFrequencyControls.Add(lblFrequencyInput);
            subscriptionFrequencyControls.Add(pnlLabelFrequency);
            subscriptionFrequencyControls.Add(pnlFrequencyInput);

            this._TabSubscriptionPanel.Controls.Add(AsentiaPage.BuildMultipleInputControlFormField("ReportSubscriptionFrequency",
                                                                                                    _GlobalResources.Frequency,
                                                                                                    subscriptionFrequencyControls,
                                                                                                    true,
                                                                                                    true));

            #endregion

            TabSubscriptionContentPanel.Controls.Add(this._TabSubscriptionPanel);
        }
        #endregion

        #region _BuildReportFilesTabContent
        /// <summary>
        /// Builds the tab content container for report files tab
        /// </summary>
        private void _BuildReportFilesTabContent()
        {
            Panel tabReportFileUpadatePanel = new Panel();
            tabReportFileUpadatePanel.CssClass = "TabPanelContainer";
            ReportFileGrid = new Grid();
            ReportFileGrid.ID = "ReportFileGrid";
            ReportFileGrid.StoredProcedure = Library.ReportFile.GridProcedure;
            ReportFileGrid.AddFilter("@idCallerSite", SqlDbType.Int, 4, AsentiaSessionState.IdSite);
            ReportFileGrid.AddFilter("@idReport", SqlDbType.Int, 4, this.IdReport);
            ReportFileGrid.AddFilter("@idCaller", SqlDbType.Int, 4, AsentiaSessionState.IdSiteUser);
            ReportFileGrid.IdentifierField = "idReportFile";
            ReportFileGrid.DefaultSortColumn = "dtCreated";
            ReportFileGrid.AllowPaging = false;
            ReportFileGrid.ShowRecordsPerPageSelectbox = false;
            ReportFileGrid.ShowSearchBox = false;
            ReportFileGrid.ShowTimeInDateStrings = true;

            // data key names
            ReportFileGrid.DataKeyNames = new string[] { "idReportFile", "filename" };

            // columns
            GridColumn dtCreated = new GridColumn(_GlobalResources.Generated, "dtCreated");

            GridColumn size = new GridColumn(_GlobalResources.Size, "size", true);

            GridColumn html = new GridColumn(_GlobalResources.ViewHTML, "htmlOn", true);
            html.AddProperty(new GridColumnProperty("True", "<a href=\"" + SitePathConstants.SITE_WAREHOUSE_REPORTDATA + "##filename##.html\" target=\"_blank\">"
                                                                + "<img class=\"MediumIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_IE,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"HTML\" />"
                                                                + "</a>"));
            html.AddProperty(new GridColumnProperty("False", "<img class=\"MediumIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_IE,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"HTML\" />"));

            GridColumn csv = new GridColumn(_GlobalResources.ViewExcel, "csvOn", true);
            csv.AddProperty(new GridColumnProperty("True", "<a href=\"" + SitePathConstants.SITE_WAREHOUSE_REPORTDATA + "##filename##.csv\" >"
                                                                + "<img class=\"MediumIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MSOFFICE_2013,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"CSV\" />"
                                                                + "</a>"));
            csv.AddProperty(new GridColumnProperty("False", "<img class=\"MediumIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_MSOFFICE_2013,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"CSV\" />"));
            GridColumn pdf = new GridColumn(_GlobalResources.ViewPDF, "pdfOn", true);
            pdf.AddProperty(new GridColumnProperty("True", "<a href=\"" + SitePathConstants.SITE_WAREHOUSE_REPORTDATA + "##filename##.pdf\" target=\"_blank\">"
                                                                + "<img class=\"MediumIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_PDF,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"PDF\" />"
                                                                + "</a>"));
            pdf.AddProperty(new GridColumnProperty("False", "<img class=\"MediumIcon DimIcon\" src=\""
                                                                + ImageFiles.GetIconPath(ImageFiles.ICON_PDF,
                                                                                         ImageFiles.EXT_PNG)
                                                                + "\" alt=\"PDF\" />"));           

            // add columns to data grid
            ReportFileGrid.AddColumn(dtCreated);
            ReportFileGrid.AddColumn(size);
            ReportFileGrid.AddColumn(html);
            ReportFileGrid.AddColumn(csv);
            if ((bool)AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.REPORTINGPDFEXPORT_ENABLE))
            {
                ReportFileGrid.AddColumn(pdf);
            }
            ReportFileGrid.BindData();

            tabReportFileUpadatePanel.Controls.Add(ReportFileGrid);

            this.TabReportFilesContentPanel.Controls.Add(tabReportFileUpadatePanel);

            this._BuildReportFileActionsPanel();

        }
        #endregion

        #region _BuildReportFileActionsPanel
        /// <summary>
        /// Builds an actions panel for actions performed on Report File Grid data.
        /// </summary>
        private void _BuildReportFileActionsPanel()
        {
            // delete button            
            this.SaveFileDeleteButton.CssClass = "GridDeleteButton";

            // delete button image
            Image deleteImage = new Image();
            deleteImage.ID = "GridDeleteButtonImage";
            deleteImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_DELETE,
                                                          ImageFiles.EXT_PNG);
            deleteImage.CssClass = "MediumIcon";
            deleteImage.AlternateText = _GlobalResources.Delete;
            this.SaveFileDeleteButton.Controls.Add(deleteImage);

            // delete button text
            Literal deleteText = new Literal();
            deleteText.Text = _GlobalResources.DeleteSelectedReportFile_s;
            this.SaveFileDeleteButton.Controls.Add(deleteText);

            // add delete button to panel
            this.TabReportFilesContentPanel.Controls.Add(this.SaveFileDeleteButton);
        }
        #endregion

        #region _SetInputValueString
        /// <summary>
        /// Used to set the value of an input that takes a string (most inputs).
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>string value</returns>
        private string _SetInputValueString(string fieldIdentifier)
        {
            object inputValue = this._SetInputValue(fieldIdentifier);

            if (inputValue == null)
            { return String.Empty; }

            return inputValue.ToString();
        }
        #endregion

        #region _SetInputValueDateTime
        /// <summary>
        /// Used to set the value of an input that takes a DateTime.
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>DateTime value</returns>
        private DateTime? _SetInputValueDateTime(string fieldIdentifier)
        {
            object inputValue = this._SetInputValue(fieldIdentifier);

            if (inputValue == null)
            { return null; }

            DateTime inputValueFormatted;

            if (DateTime.TryParse(inputValue.ToString(), out inputValueFormatted))
            { return inputValueFormatted; }

            return null;
        }
        #endregion

        #region _SetInputValue
        /// <summary>
        /// Used to set the value of an input field from a user object property.
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>object containing the value of the user object property</returns>
        private object _SetInputValue(string fieldIdentifier)
        {
            switch (fieldIdentifier)
            {
                case "title":
                    return this.ReportObject.Title;
                case "idUser":
                    return this.ReportObject.IdUser;
                case "idSite":
                    return this.ReportObject.IdSite;
                case "idDataset":
                    return this.ReportObject.IdDataset;
                case "fields":
                    return this.ReportObject.Fields;
                case "filter":
                    return this.ReportObject.Filter;
                case "order":
                    return this.ReportObject.Order;
                case "dtCreated":
                    return this.ReportObject.DtCreated;
                case "dtModified":
                    return this.ReportObject.DtModified;
                case "dtDeleted":
                    return this.ReportObject.DtDeleted;
                case "isPublic":
                    if (this.ReportObject.IsPublic)
                    { return "true"; }
                    else
                    { return "false"; }
                case "isDeleted":
                    if (this.ReportObject.IsDeleted)
                    { return "true"; }
                    else
                    { return "false"; }
                case "dtStart":
                    return this.ReportObject.DtSubscriptionStart;
            }

            return null;
        }
        #endregion 

        #region _GetInputValue
        /// <summary>
        /// Gets the properly formatted string value of an input control,
        /// i.e. blanks get turned into nulls.
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>string value</returns>
        public string _GetInputValue(string tabIdentifier)
        {
            Control fieldControl;

            switch (tabIdentifier)
            {
                case "fields":
                    fieldControl = this.FindControl("hdReportField");
                    break;
                case "filter":
                    fieldControl = this.FindControl("hdReportFilter");
                    break;
                case "order":
                    fieldControl = this.FindControl("hdReportOrderBy");
                    break;
                default:
                    fieldControl = this.FindControl("hdReportField");
                    break;
            }

                HiddenField hdField = (HiddenField)fieldControl;
                return hdField.Value;
        }
        #endregion

        #region _GetShortestDaysValue
        /// <summary>
        /// Gets the properly formatted string value of an input control,
        /// i.e. blanks get turned into nulls.
        /// </summary>
        /// <param name="fieldIdentifier">the id of the user field</param>
        /// <returns>string value</returns>
        public string _GetShortestDaysValue()
        {
            return this._ReportShortestDay.Value;
        }
        #endregion
        #endregion
        #endregion
     }
}
