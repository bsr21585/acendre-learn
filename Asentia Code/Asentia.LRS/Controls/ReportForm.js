﻿/*
functions when page loading
*/

$(document).ready(function () {
    var DisableSortableColumns = 0;
    //load data into object stringObj for the filter condition list by different data type
    //data source from the xml file which was read in the file ReportForm.cs
    for (i = 0; i < filterObject.length; i++) {
        if (filterObject[i][2].indexOf('String') >= 0)
            stringObj[filterObject[i][0]] = filterObject[i][1];
        if (filterObject[i][2].indexOf('Integer') >= 0)
            integerObj[filterObject[i][0]] = filterObject[i][1];
        if (filterObject[i][2].indexOf('DateTime') >= 0)
            dateTimeObj[filterObject[i][0]] = filterObject[i][1];
        if (filterObject[i][2].indexOf('Bit') >= 0) 
            bitObj[filterObject[i][0]] = filterObject[i][1];
        if (filterObject[i][2].indexOf('Status') >= 0)
            statusObj[filterObject[i][0]] = filterObject[i][1];
        if (filterObject[i][2].indexOf('Enum') >= 0)
            enumObj[filterObject[i][0]] = filterObject[i][1];
        if (filterObject[i][2].indexOf('Contain') >= 0)
            containObj[filterObject[i][0]] = filterObject[i][1];

        //load data to object showInputObj which determin if the input is needed for a specific filter condition
        showInputObj[filterObject[i][0]] = filterObject[i][3];

        //load data to object conditionOperator which translate the dropdownlist value into a sql operation language
        conditionOperator[filterObject[i][0]] = filterObject[i][4];

        //load data to object operatorID which translate the sql operation language back to the dropdownlist value for the conditon filter.
        operatorId[filterObject[i][4].replace(/data|bDate|eDate|\s/g, "")] = filterObject[i][0];
     }

    //load optionGroup to available columns select box
    for (j = 0; j < optionGroupObject.length; ++j) {
        $("#ReportModify_FieldsToInclude_AvailableItems option[classification='" + optionGroupObject[j] + "']").wrapAll("<optgroup label='" + optionGroupObject[j] + "'>");
    }

    //load preexisitng selected columns string
    var fieldString = $("#hdReportField").val().replace(/,$/, '');

    if (fieldString.length > 1) {
        fieldArray = fieldString.split(',');
        $("#ReportModify_FieldsToInclude_MoveAllToAvailable").click();
        for (i = 0; i < fieldArray.length; i++) {
            var selectedValue = fieldArray[i].substring(1, fieldArray[i].length - 1);
            $("#ReportModify_FieldsToInclude_AvailableItems option[value='" + selectedValue + "']").prop("selected", true);
            $("#ReportModify_FieldsToInclude_MoveItemToSelected").click();
        }
        $("option:selected", $("#ReportModify_FieldsToInclude_SelectedItems")).removeAttr("selected");
    }

    // load items into filter condition dropdownlist
    $.each(stringObj, function (val, text) {
        $("select[id^='ddlFilterConditionFilterNum']").append(
        $('<option></option>').val(val).html(text)
    );
    });

    addRowHdn = $("#RowPanelFilterNum0").html();
    addOrderRowHdn = $("#panelOrderColumnNum0").html();

    // load existing criteria pattern into the filter area.
    var totalCriteria = $("#hdReportFilter").val().replace('[_sb##languageCode##]', '[##idLanguage##]');
    var shortestPeriodCode = '', shortestPeriodValue, shortestInt,  criteria; //for report subscription
    if (totalCriteria.length > 1) {
        filterUsed = true;
        var str = totalCriteria, dataTypeSelected;
        var criteriaNum, criteriaArray, columnName, condition, conditionId, dateArray, firstValue, secondValue, compShortPeriod;
        var patt = /([\[][^\[]*)[)]((\s*(and|or)\s*[(])|\s*[)])/g;
        criteriaArray = str.match(patt);
        criteriaNum = totalCriteria.replace(/[^\[]/g, "").length;

        for (i = 1; i <= criteriaNum; i++) {
            criteria = (criteriaNum == 1) ? totalCriteria.replace(/^ *\(|\) *$/g, "") : criteriaArray[i - 1].substr(0, criteriaEndPosition(criteriaArray[i - 1]));
            columnName = str.match(/[\[][^\]]*[\]]/).toString().slice(1, -1);
            str = str.replace("(" + criteria + ")", i);
            if (i > 1) {
                FilterAddRow();
                $(".ReportFormFilterAndOr").eq(i - 1).val(str.substring(str.indexOf(i - 1) + i.toString().length, str.indexOf(i)).replace(/[( )]/g, ''));
            }            
            $("#ddlDatasetColumnsFilterNum" +(i - 1).toString()).val(columnName)
            DatasetColumnsOnChange("Num" + (i - 1).toString());
            dataTypeSelected = $("option:selected", $("#ddlDatasetColumnsFilterNum" + (i - 1).toString())).attr("dataType");
            switch (dataTypeSelected) {
                case 'String':
                    firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "").replace(/&#39;/g, '\'').replace(/&#40;/g, '(').replace(/&#41;/g, ')');
                    conditionId = criteria.replace(/[\[].*[\]]([^']*['%]+)[^'%]*([%]?['])|[\[].*[\]]([^']*)/, "$1$2$3").replace(/\s/g, "");
                    if (conditionId.substr(0, 4) == "LIKE") {
                        if (criteria.replace(/[^%]/g, "").length == 2)
                            condition = "ct";
                        else if (criteria.indexOf("%") == criteria.length - 2)
                            condition = "sw";
                        else
                            condition = "ew";
                    }
                    else if (typeof operatorId[conditionId]=== "undefined") 
                    {
                        condition = "is";
                        firstValue = "";
                    }
                    else {
                        condition = operatorId[conditionId];
                    }
                    $("#ddlFilterConditionFilterNum" + (i - 1).toString()).val(condition);
                    FilterConditionOnChange("Num" + (i - 1).toString());
                    $("#tbFilterNum" + (i - 1).toString()).val(showInputObj[condition] ? firstValue : "");
                    break;
                case 'Integer':
                case 'Double':
                    firstValue = criteria.replace(/[\[][^\]]*[\]]|[^\d]/g, "");
                    condition = criteria.replace(/[\[][^\]]*[\]]|\d|\s/g, "").trim();
                    $("#ddlFilterConditionFilterNum" +(i -1).toString()).val((typeof operatorId[condition]=== "undefined") ? "eq": operatorId[condition]);
                    FilterConditionOnChange("Num" + (i - 1).toString());
                    $("#tbFilterNum" + (i - 1).toString()).val(firstValue);
                    break;
                case 'Bit':
                    $("#ddlFilterConditionFilterNum" + (i - 1).toString()).val((criteria.slice(-1) == 1) ? ReportFilterStatusYes : ReportFilterStatusNo);
                    FilterConditionOnChange("Num" + (i - 1).toString());
                    break;
                case 'Status':
                    $("#ddlFilterConditionFilterNum" + (i - 1).toString()).val((criteria.slice(-1) == 1) ? ReportFilterStatusActive : ReportFilterStatusDisabled);
                    FilterConditionOnChange("Num" + (i - 1).toString());
                    break;
                case 'Contain':
                    firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "").replace(/&#39;/g, '\'').replace(/&#40;/g, '(').replace(/&#41;/g, ')');
                    condition = criteria.replace(/[\[].*[\]]([^']*['%]+)[^'%]*([%]?['])|[\[].*[\]]([^']*)/, "$1$2$3").replace(/\s/g, "");
                    $("#ddlFilterConditionFilterNum" + (i - 1).toString()).val((typeof operatorId[condition]=== "undefined")? "ct" : operatorId[condition]);
                    FilterConditionOnChange("Num" + (i - 1).toString());
                    $("#ddlContentTypesNum" + (i - 1).toString()).val(firstValue);
                    break;
                case 'Enum':
                    firstValue = criteria.replace(/[\[][^\]]*[\]]|[^\d]/g, "");
                    condition = criteria.replace(/[\[][^\]]*[\]]|\d|\s/g, "").trim();
                    if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Language') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlLanguageNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Timezone') >= 0) {
                        $("#ddlTimezoneNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Course Status') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlCourseStatusNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Learning Path Status') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlLearningPathStatusNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Lesson Completion') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlLessonCompletionNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Completion Status') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlCompletionStatusNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Lesson Success') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlLessonSuccessNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Success Status') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlSuccessStatusNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Certification Status') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlCertificationStatusNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Roster Status') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlRosterStatusNum" + (i - 1).toString()).val(firstValue);
                    }
                    else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i - 1).toString())).val().indexOf('Item Type') >= 0) {
                        firstValue = criteria.replace(/[\[][^'%]*['%]+|['%]/g, "");
                        condition = criteria.replace(/[\[][^\]]*[\]]|[\'][^\]]*[\']|\s/g, "");
                        $("#ddlPurchaseItemTypeNum" + (i - 1).toString()).val(firstValue);
                    }
                    else {
                        $("#ddlEnumNum" + (i - 1).toString()).val(firstValue);
                    }
                    $("#ddlFilterConditionFilterNum" +(i -1).toString()).val((typeof operatorId[condition]=== "undefined") ? "eq": operatorId[condition]);
                    FilterConditionOnChange("Num" + (i - 1).toString());
                    break;
                case 'Status':
                    $("#ddlFilterConditionFilterNum" + (i - 1).toString()).val((criteria.slice(-1) == 1) ? ReportFilterStatusActive : ReportFilterStatusDisabled);
                    FilterConditionOnChange("Num" + (i - 1).toString());
                    break;
                case 'DateTime':
                    dateArray = criteria.match(/[\d]{4}-[\d]{2}-[\d]{2}/g);
                    condition = criteria.replace(/[\[][^\]]*[\]]|[\d]{4}-[\d]{2}-[\d]{2}|\s/g, "");
                       if (typeof operatorId[condition]=== "undefined") {
                             $("#ddlFilterConditionFilterNum" +(i -1).toString()).val("is_date");
                        }
                       else if (operatorId[condition]== "bw" && dateArray[0]== dateArray[1]) {
                             $("#ddlFilterConditionFilterNum" + (i -1).toString()).val("is_date");
                        }
                       else if (operatorId[condition]== "nbw" && dateArray[0]== dateArray[1]) {
                             $("#ddlFilterConditionFilterNum" + (i - 1).toString()).val("nis_date");
                        }
                       else {
                             $("#ddlFilterConditionFilterNum" +(i -1).toString()).val(operatorId[condition]);
                        }
                    if (!typeof operatorId[condition]=== "undefined")
                     {compShortPeriod = operatorId[condition].substring(operatorId[condition].indexOf('_') +1).replace('w', 'k');
                    if ((condition.indexOf('GETDATE')) > 0 &&(shortestPeriodCode == '' || shortestPeriodCode > compShortPeriod))
                        {
                        shortestPeriodCode = compShortPeriod;
                        shortestInt = shortestPeriodCode.charAt(1).replace('0', '1');
                        switch (shortestPeriodCode.charAt(0)) {
                            case 'd':
                                shortestPeriodValue = shortestInt + " " + ((shortestPeriodCode.charAt(1) > 1) ? "days" : "day");
                                shortestDays = parseInt(shortestInt);
                                break;
                            case 'k':
                                shortestPeriodValue = shortestInt + " " + ((shortestPeriodCode.charAt(1) > 1) ? "weeks" : "week");
                                shortestDays = parseInt(shortestInt) * 7;
                                break;
                            case 'm':
                                shortestPeriodValue = shortestInt + " " + ((shortestPeriodCode.charAt(1) > 1) ? "months" : "month");
                                shortestDays = parseInt(shortestInt) * 31;
                                break;
                            case 'q':
                                shortestPeriodValue = shortestInt + " " + ((shortestPeriodCode.charAt(1) > 1) ? "quarters" : "quarter");
                                shortestDays = parseInt(shortestInt) * 93;
                                break;
                            case 'y':
                                shortestPeriodValue = shortestInt + " " + ((shortestPeriodCode.charAt(1) > 1) ? "years" : "year");
                                shortestDays = parseInt(shortestInt) * 365;
                                break;
                            }
                        }
                    }
                    FilterConditionOnChange("Num" + (i - 1).toString());
                    if (dateArray != undefined) {
                        if (i == 1) {
                            $('#dpFilterNum0_DateInputControl').val(dateArray[0]);
                            if (dateArray[1] != undefined) $('#dpFilterBwNum0_DateInputControl').val(dateArray[1]);
                        } else {
                            $('#dpFilterNum' + (i - 1).toString() + '_DateInputControl').attr("id", "dateInput" + (i - 1).toString());
                            $('#dateInput' + (i - 1).toString()).val(dateArray[0]);
                            if (dateArray[1] != undefined) {
                                $('#dpFilterBwNum' + (i - 1).toString() + '_DateInputControl').attr("id", "dateInputBw" + (i - 1).toString());
                                $('#dateInputBw' + (i - 1).toString()).val(dateArray[1]);
                            }
                        }
                    }
                    break;
            }
        }
        var value0 = $("option:selected", "#ddlDatasetColumnsFilterNum0").val();
        for (j = 0; j < optionGroupObject.length; ++j) {
            $("#ddlDatasetColumnsFilterNum0 option[classification='" +optionGroupObject[j]+ "']").wrapAll("<optgroup label='" +optionGroupObject[j]+ "'>");
        }
        $("#ddlDatasetColumnsFilterNum0").val(value0);

        $('#dateInput0').attr("id", "dpFilterNum0_DateInputControl");
        $('#dateInputBw0').attr("id", "dpFilterBwNum0_DateInputControl");
        $("#criteriaContentLabel").text(str);
        $("#criteriaContentTextBox").val(str);
        customPattern = str;
        $('#Filter_Zero_Panel').hide();
        $('#Main_FilterRow_Panel').show();
        $('#Filter_Criteria_Panel').show();
    } else {
        $('#Filter_Zero_Panel').show();
        $('#Main_FilterRow_Panel').hide();
        $('#Filter_Criteria_Panel').hide();
    }

    //load preexisitng order string into the order by area.
    var orderByString = $("#hdReportOrderBy").val().replace(/,$/, '');
    if (orderByString.length > 1) {
        var orderByArray = orderByString.split(",");
        for (i = 0; i < orderByArray.length; i++) {
            if (i > 0) {
                var newNumber = String(orderColumnSeed++);
                var orderColumnRow = addOrderRowHdn.replace(/Num0/g, 'Num' + newNumber).replace(FirstByText, ThenByText);
                $("div[id^='panelOrderColumnNum']:last").after('<div id=\'panelOrderColumnNum' + newNumber + '\'>' + orderColumnRow + '</div>');
                $("option:selected", $("#ddlDatasetColumnsOrderNum" + newNumber)).removeAttr("selected");
            }
            var count = $('.orderDDL').length;
            var columnName = orderByArray[i].replace(/.*\[|(].*)/g, '');
            var ascOption = orderByArray[i].replace(/.*]\s/g, '');
            $("input[id^='OrderColumnAscendingNum" + i + "'][value='" + ascOption + "']").prop('checked', true);
            if (i < orderByArray.length - 1)
            {
                $("select[id^='ddlDatasetColumnsOrderNum']").prop("disabled", true);
            } 

            OrderByDisabledWhenNotSelectedField();
            
            $("#ddlDatasetColumnsOrderNum" + i).val(columnName);
        }
        ($('.orderDDL').length > 1) ? $('.OrderImageLink').first().show() : $('.OrderImageLink').first().hide();
        (($("select[id$='FieldsToInclude_SelectedItems'] option").length - DisableSortableColumns) == count) ? $('.OrderImageLink').last().hide : $('.OrderImageLink').last().show();

        for (i = 0; i < orderByArray.length; i++) {
                var removedValue = $("option:selected", $('.orderDDL').eq(i)).val();
                $("#ddlDatasetColumnsOrderNum" + newNumber + " option[value='" + removedValue + "']").prop('disabled', true);
        }
    }

    ($("select[id$='FieldsToInclude_SelectedItems'] option").length > 0) ? $("#Report_Order_Panel").show() : $("#Report_Order_Panel").hide();

    //Bound Interaction columns for correct report of data
    $("input[id^='ReportModify_FieldsToInclude_MoveItemTo']").click(function () {
        var interactionSubElements = false;
        var interactionElement = false;
        $('#ReportModify_FieldsToInclude_SelectedItems option').each(function() {
            if ($(this).val() == 'Interaction Description' || $(this).val() == 'Interaction Type' || $(this).val() == 'Learner Response' || $(this).val() == 'Correct Responses') {
                interactionSubElements = true;
            } else if ($(this).val() == 'Interaction')
            {
                interactionElement = true;
            }
        });
        if (interactionSubElements && !interactionElement) {
            $("#ReportModify_FieldsToInclude_AvailableItems option[value='Interaction']").appendTo("#ReportModify_FieldsToInclude_SelectedItems");
            OrderByDisabledWhenNotSelectedField();
        }
    });

    //Update Order By Tab disabled columns
    $("input[id^='ReportModify_FieldsToInclude_Move']").click(function () {
        var selectedNumber = $("select[id$='FieldsToInclude_SelectedItems'] option").length;
        $("#hdReportOrderBy").val('');
        if (selectedNumber > 0) {
            $("#Report_Order_Panel").show();
            OrderByDisabledWhenNotSelectedField();
            for (i = 0; i < $('.orderDDL').length; i++) {
                var selectedOrderByValue = $("option:selected", $('.orderDDL').eq(i)).val();
                var selectedOrderByID = $('.orderDDL').eq(i).attr('id');
                var needDeleted = true;
                for (j = 0; j < selectedNumber; j++) {
                    if ($("select[id$='FieldsToInclude_SelectedItems'] option:eq(" + j + ")").val() == selectedOrderByValue)
                        needDeleted = false;
                }
                if (needDeleted) { 
                    // in case all OrderBy rows are all deleted, leave the last one with setting removed
                    if ($('.orderDDL').length == 1) {
                        $("option:selected", $('.orderDDL')).removeAttr("selected").prop("disabled", true);
                        OrderByDisabledWhenNotSelectedField();
                    }
                    else {
                        OrderDeleteRow(selectedOrderByID.replace("ddlDatasetColumnsOrder", ""));
                    }
                }
            }
            ($('.orderDDL:last > option').length > 1) ? $('.OrderImageLink').last().show() : $('.OrderImageLink').last().hide();
        } else {            
            for (i = $('.orderDDL').length - 1; i >= 1; i--) {
                OrderDeleteRow($('.orderDDL').eq(i).attr('id').replace("ddlDatasetColumnsOrder", ""));
            }
            $("option:selected", $('.orderDDL')).removeAttr("selected").prop("disabled", true);
            $("#Report_Order_Panel").hide();
        }

        $("#ReportModify_FieldsToInclude_AvailableItems").html($("#ReportModify_FieldsToInclude_AvailableItems").html().replace(/(<optgroup label=")[^"]*">|(<\/option>)/g, ''));

        for (j = 0; j < optionGroupObject.length; ++j) {
            $("#ReportModify_FieldsToInclude_AvailableItems option[classification='" + optionGroupObject[j] + "']").wrapAll("<optgroup label='" + optionGroupObject[j] + "'>");
        }
    });

    //Validation for inputs when click Save or Run Reports
    $("input[id$='ReportButton']").click(function () {
        // Assign the Order By String $('.orderRadioList').eq(i).attr('id')
        var orderByString = '';
        var selectID;
        for (i = 0; i < $('.orderDDL').length; i++) {
            orderByString += " [" + $("option:selected", $('.orderDDL').eq(i)).val() + "] ";
            orderByString += $('.orderRadioList').eq(i).find(":checked").val() + ",";
        }
        $("#hdReportOrderBy").val(orderByString);
        UpdateQueryString();
        UpdateSelectedFieldsString();
    });

    var isExistedReport = idReport > 0;
   
    $("[id*='_TabL']").click(function () {
        if (this.id.toString().indexOf("Property_TabL") >= 0)
        {
            $('#RunReportButton').show();
            $('#SaveReportButton').show();
            $('#UpdateSubscriptionButton').hide();
            $('#DeleteReportFileButton').hide();
            $('#CancelButton').show();
        }
        else if (this.id.toString().indexOf("Subscription_TabL") >= 0)
        {
            $('#RunReportButton').hide();
            $('#SaveReportButton').hide();
            $('#DeleteReportFileButton').hide();
            $('#UpdateSubscriptionButton').show();
            $('#CancelButton').show();
            if (criteria.indexOf("GETDATE") >= 0)
            {
                $("#hdShortestDay").val(shortestDays);    
            }
        }
        else if (this.id.toString().indexOf("ReportFiles_T") >= 0)
        {
            $('#RunReportButton').hide();
            $('#SaveReportButton').hide();
            $('#DeleteReportFileButton').show();
            $('#UpdateSubscriptionButton').hide();
            $('#CancelButton').hide();
        }
        else
        {
            $('#RunReportButton').show();
            (isExistedReport)? $('#SaveReportButton').show() : $('#SaveReportButton').hide();
            $('#DeleteReportFileButton').hide();
            $('#UpdateSubscriptionButton').hide();
            $('#CancelButton').show();
        }        
    });

    $('.ReportFormFilterAndOr').first().css('visibility', 'hidden');
    $('.MultipleSelectWithOrderingListBox').css('height', '360px');

    $("#tbFrequency").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#GridSearchTable").css('visibility', 'hidden');

    if (document.location.href.indexOf('mode') > 0) {
        $("#ReportModify_ReportFiles_TabLink").click();
    }

    if ($("#ReportModify_Subscription_Field input:checked").val() === "false")
    {
        $("#dpStartDateSubscription_DateInputControl").val("");
        $("option:selected", $("#dpStartDateSubscription_HourInputControl")).removeAttr("selected");
        $("option:selected", $("#dpStartDateSubscription_MinuteInputControl")).removeAttr("selected");
        $("option:selected", $("#dpStartDateSubscription_AMPMInputControl")).removeAttr("selected");
        $("option:selected", $("#ddlFrequency")).removeAttr("selected");
        $("#tbFrequency").val("0");
        $("#dpStartDateSubscription").children().prop("disabled", true);
        $("#SubscriptionFrequencySection").children().prop("disabled", true);
    }

    //Enable/Disable Subscription Panel
    $("#ReportModify_Subscription_Field").change(function () {
        if ($("#ReportModify_Subscription_Field input:checked").val() === "true") {
            var d = new Date();
            var month = d.getMonth() + 1;
            var day = d.getDate();
            var output = d.getFullYear() + '-' +
                (month < 10 ? '0' : '') + month + '-' +
                (day < 10 ? '0' : '') + day;
            if (d.getHours() > 12) {
                $("#dpStartDateSubscription_AMPMInputControl").val("PM");
                $("#dpStartDateSubscription_HourInputControl").val(d.getHours() - 12);
            }
            else {
                $("#dpStartDateSubscription_AMPMInputControl").val("AM");
                $("#dpStartDateSubscription_HourInputControl").val(d.getHours());
            }
            $("#dpStartDateSubscription_DateInputControl").val(output);
            $("#dpStartDateSubscription_MinuteInputControl").val(d.getMinutes());
            $("#dpStartDateSubscription").children().removeProp("disabled");
            $("#SubscriptionFrequencySection").children().removeProp("disabled");
        }
        else {
            $("#dpStartDateSubscription_DateInputControl").val("");
            $("option:selected", $("#dpStartDateSubscription_HourInputControl")).removeAttr("selected");
            $("option:selected", $("#dpStartDateSubscription_MinuteInputControl")).removeAttr("selected");
            $("option:selected", $("#dpStartDateSubscription_AMPMInputControl")).removeAttr("selected");
            $("option:selected", $("#ddlFrequency")).removeAttr("selected");
            $("#tbFrequency").val("0");
            $("#dpStartDateSubscription").children().prop("disabled", true);
            $("#SubscriptionFrequencySection").children().prop("disabled", true); 
        }
    });

});


//onChange method for field column -- to display the dynamic filter condition sets
function DatasetColumnsOnChange(NumString) {
    var columnNameSelected = $("option:selected", $("#ddlDatasetColumnsFilter" + NumString)).val();
    var dataTypeSelected = $("option:selected", $("#ddlDatasetColumnsFilter" + NumString)).attr("dataType");
    var relatedFilter = $("#ddlFilterConditionFilter" + NumString);
    var txFilter = $("#tbFilter" + NumString);
    var dpFilter = $("#dpFilter" + NumString);
    var dpFilterBw = $("#dpFilterBw" + NumString);
    var labelBw = $("#labelBwFilter" + NumString);
    var ddlEnum = $("#ddlEnum" + NumString);
    var ddlContentTypes = $("#ddlContentTypes" + NumString);
    var ddlCourseStatus = $("#ddlCourseStatus" + NumString);
    var ddlCertificationStatus = $("#ddlCertificationStatus" + NumString);
    var ddlLearningPathStatus = $("#ddlLearningPathStatus" + NumString);
    var ddlLessonCompletion = $("#ddlLessonCompletion" + NumString);
    var ddlLessonSuccess = $("#ddlLessonSuccess" + NumString);
    var ddlCompletionStatus = $("#ddlCompletionStatus" + NumString);
    var ddlSuccessStatus = $("#ddlSuccessStatus" + NumString);
    var ddlRosterStatus = $("#ddlRosterStatus" + NumString);
    var ddlPurchaseItemType = $("#ddlPurchaseItemType" + NumString);
    var ddlLanguage = $("#ddlLanguage" + NumString);
    var ddlTimezone = $("#ddlTimezone" + NumString);
    var ddlCertificationTrack = $("#ddlCertificationTrack" + NumString);
    var filterObj;
    var validationString;

    if (NumString == 'Num0') {
        $('#dpFilterNum0_DateInputControl').val('');
        $('#dpFilterBwNum0_DateInputControl').val('');
    } 
        txFilter.val('');
        dpFilter.val('');

        txFilter.hide();
        dpFilter.hide();
        dpFilterBw.hide();
        labelBw.hide();
        ddlEnum.hide();
        ddlContentTypes.hide();
        ddlCourseStatus.hide();
        ddlCertificationStatus.hide();
        ddlLearningPathStatus.hide();
        ddlLessonCompletion.hide();
        ddlLessonSuccess.hide();
        ddlCompletionStatus.hide();
        ddlSuccessStatus.hide();
        ddlRosterStatus.hide();
        ddlPurchaseItemType.hide();
        ddlLanguage.hide();
        ddlTimezone.hide();
        ddlCertificationTrack.hide();

    switch (dataTypeSelected) {
        case 'String':
            filterObj = stringObj;
            txFilter.show();
            validationString = "^.*?";
            break;
        case 'Integer':
            filterObj = integerObj;
            txFilter.show();
            validationString = "^[0-9]+$";
            break;
        case 'Double':
            filterObj = integerObj;
            txFilter.show();
            validationString = "^[0-9.]+$";
            break;
        case 'DateTime':
            filterObj = dateTimeObj;
            dpFilter.show();
            break;
        case 'Bit':
            filterObj = bitObj;
            break;
        case 'Status':
            filterObj = statusObj;
            break;
        case 'Enum':
            filterObj = enumObj;

            if (columnNameSelected.indexOf("Language") >= 0)
            {
                ddlLanguage.show();
            }
            else if (columnNameSelected.indexOf("Course Status") >= 0) {
                ddlCourseStatus.show();
            }
            else if (columnNameSelected.indexOf("Learning Path Status") >= 0) {
                ddlLearningPathStatus.show();
            }
            else if (columnNameSelected.indexOf("Certification Status") >= 0) {
                ddlCertificationStatus.show();
            }
            else if (columnNameSelected.indexOf("Roster Status") >= 0) {
                ddlRosterStatus.show();
            }
            else if (columnNameSelected.indexOf("Item Type") >= 0) {
                ddlPurchaseItemType.show();
            }
            else if (columnNameSelected.indexOf("Lesson Completion") >= 0) {
                ddlLessonCompletion.show();
            }
            else if (columnNameSelected.indexOf("Completion Status") >= 0) {
                ddlCompletionStatus.show();
            }
            else if (columnNameSelected.indexOf("Lesson Success") >= 0) {
                ddlLessonSuccess.show();
            }
            else if (columnNameSelected.indexOf("Success Status") >= 0) {
                ddlSuccessStatus.show();
            }
            else if (columnNameSelected.indexOf("Timezone") >= 0)
            {
                ddlTimezone.show();
            }
            else if (columnNameSelected.indexOf("Committed Content Type") >= 0) {
                ddlEnum.show();
            }
            else if (columnNameSelected.indexOf("Current Track") >= 0) {
                ddlCertificationTrack.show();
            }
            break;
        case 'Contain':
            filterObj = containObj;
            ddlContentTypes.show();
            break;
    }


    txFilter.unbind();
    txFilter.bind('keypress', function (event) {
        var regex = new RegExp(validationString);
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
        
    relatedFilter.html('');

    $.each(filterObj, function (val, text) {
        relatedFilter.append($('<option></option>')
                .val(val)
                .html(text));
    });

}


//onChange method for filter condition -- to display the dynamic input boxes
function FilterConditionOnChange(NumString) {
    var columnNameSelected = $("option:selected", $("#ddlDatasetColumnsFilter" + NumString)).val();
    var dataTypeSelected = $("option:selected", $("#ddlDatasetColumnsFilter" + NumString)).attr("dataType");
    var txFilter = $("#tbFilter" + NumString);
    var dpFilter = $("#dpFilter" + NumString);
    var dpFilterBw = $("#dpFilterBw" + NumString);
    var labelBw = $("#labelBwFilter" + NumString);
    var ddlEnum = $("#ddlEnum" + NumString);
    var ddlContentTypes = $("#ddlContentTypes" + NumString);
    var ddlCertificationTrack = $("#ddlCertificationTrack" + NumString);
    var ddlCourseStatus = $("#ddlCourseStatus" + NumString);
    var ddlCertificationStatus = $("#ddlCertificationStatus" + NumString);
    var ddlLearningPathStatus = $("#ddlLearningPathStatus" + NumString);
    var ddlLessonCompletion = $("#ddlLessonCompletion" + NumString);
    var ddlLessonSuccess = $("#ddlLessonSuccess" + NumString);
    var ddlCompletionStatus = $("#ddlCompletionStatus" + NumString);
    var ddlSuccessStatus = $("#ddlSuccessStatus" + NumString);
    var ddlRosterStatus = $("#ddlRosterStatus" + NumString);
    var ddlPurchaseItemType = $("#ddlPurchaseItemType" + NumString);
    var ddlLanguage = $("#ddlLanguage" + NumString);
    var ddlTimezone = $("#ddlTimezone" + NumString);
    var boolShowInput = showInputObj[$("option:selected", $("#ddlFilterConditionFilter" + NumString)).val()];
    if (NumString == 'Num0') {
        $('#dpFilterNum0_DateInputControl').val('');
        $('#dpFilterBwNum0_DateInputControl').val('');
    }
    txFilter.val('');
    dpFilter.val('');

    ddlLanguage.hide();
    ddlTimezone.hide();
    ddlEnum.hide();
    ddlCourseStatus.hide();
    ddlCertificationStatus.hide();
    ddlLessonCompletion.hide();
    ddlLessonSuccess.hide();
    ddlCompletionStatus.hide();
    ddlSuccessStatus.hide();
    ddlCertificationTrack.hide();
    ddlRosterStatus.hide();
    ddlPurchaseItemType.hide();

    switch (dataTypeSelected) {
        case 'String':
            ddlContentTypes.hide();
            boolShowInput ? txFilter.show() : txFilter.hide();
            if (!boolShowInput) txFilter.value = "";
            dpFilter.hide();
            ddlContentTypes.hide();
            break;
        case 'Integer':
        case 'Double':
            boolShowInput ? txFilter.show() : txFilter.hide();
            if (!boolShowInput) txFilter.value = "";
            dpFilter.hide();
            break;
        case 'DateTime':
            txFilter.hide();
            boolShowInput ? dpFilter.show() : dpFilter.hide();
            if ($("option:selected", $("#ddlFilterConditionFilter" + NumString)).val().indexOf('bw') >= 0) {
                dpFilterBw.show();
                labelBw.show();
            }
            else {
                dpFilterBw.hide();
                labelBw.hide();
            }
            break;
        case 'Bit':
        case 'Status':
            txFilter.hide();
            dpFilter.hide();
            break;
        case 'Enum':
            if (boolShowInput) {
                if (columnNameSelected.indexOf("Language") >= 0)
                    ddlLanguage.show();
                else if (columnNameSelected.indexOf("Timezone") >= 0)
                    ddlTimezone.show();
                else if (columnNameSelected.indexOf("Roster Status") >= 0)
                    ddlRosterStatus.show();
                else if (columnNameSelected.indexOf("Item Type") >= 0)
                    ddlPurchaseItemType.show();
                else if (columnNameSelected.indexOf("Course Status") >= 0)
                    ddlCourseStatus.show();
                else if (columnNameSelected.indexOf("Certification Status") >= 0)
                    ddlCertificationStatus.show();
                else if (columnNameSelected.indexOf("Learning Path Status") >= 0)
                    ddlLearningPathStatus.show();
                else if (columnNameSelected.indexOf("Lesson Completion") >= 0)
                    ddlLessonCompletion.show();
                else if (columnNameSelected.indexOf("Completion Status") >= 0)
                    ddlCompletionStatus.show();
                else if (columnNameSelected.indexOf("Lesson Success") >= 0)
                    ddlLessonSuccess.show();
                else if (columnNameSelected.indexOf("Success Status") >= 0)
                    ddlSuccessStatus.show();
                else if (columnNameSelected.indexOf("Content Type_s") >= 0) {
                    ddlContentTypes.show();
                }
                else if (columnNameSelected.indexOf("Current Track") >= 0) {
                    ddlCertificationTrack.show();
                }
                else {
                    ddlEnum.show();
                }
            }

            dpFilter.hide();
            break;
        case 'Contain':
            txFilter.hide();
            dpFilter.hide();
            dpFilterBw.hide();
            labelBw.hide();
            ddlContentTypes.show();
            ddlEnum.hide();
            ddlCourseStatus.hide();
            ddlLearningPathStatus.hide();
            ddlLessonCompletion.hide();
            ddlLessonSuccess.hide();
            ddlCompletionStatus.hide();
            ddlSuccessStatus.hide();
            ddlCertificationStatus.hide();
            ddlRosterStatus.hide();
            ddlPurchaseItemType.hide();
            ddlLanguage.hide();
            ddlTimezone.hide();
            ddlCertificationTrack.hide();
            break;
    }

}

function FilterAddRow()
{
    //check if the criteria pattern text box is close
    if ($("#criteriaContentTextBox").is(":visible")) {
        alert(MsgSaveBeforeAdd);
        return false;
    }
    var count = $('.ReportFormFilterLabelNumber').length;
    var newNumber = String(seed++);
    var filterRow = addRowHdn.replace(/Num0/g, 'Num'+ newNumber);

    if (count == 0) {
        $("#Criteria_InputContainer").after('<div id=\'RowPanelFilterNum' + newNumber + '\'>' + filterRow + '</div>');
    }
    else {
        $("div[id^='RowPanelFilterNum']:last").after('<div id=\'RowPanelFilterNum' + newNumber + '\'>' + filterRow + '</div>');
    }

    $("#ddlFilterConditionFilterNum" + newNumber).html('');
    $.each(stringObj, function (val, text) {
        $("#ddlFilterConditionFilterNum" + newNumber).append(
        $('<option></option>').val(val).html(text));
    });
    for (j = 0; j < optionGroupObject.length; ++j) {
        $("#ddlDatasetColumnsFilterNum" + newNumber + " option[classification='" + optionGroupObject[j] + "']").wrapAll("<optgroup label='" + optionGroupObject[j] + "'>");
    }
    $("#ddlDatasetColumnsFilterNum" + newNumber).prop('selectedIndex', 0);
    $("option:selected", $("#ddlFilterConditionFilterNum" + newNumber)).removeAttr("selected");
    $('.ReportFormFilterLabelNumber').eq(count).text(count + 1);
    $("#dpFilterNum" + newNumber).hide();
    $("#tbFilterNum" + newNumber).show();
    $('.FilterImageLink:even').show();
    $('.FilterImageLink:odd').hide();
    $('.FilterImageLink').last().show();
    $('.ReportFormFilterAndOr').css('visibility', 'visible');
    $('.ReportFormFilterAndOr').first().css('visibility', 'hidden');

    //recreate datepicker method for the new row
    $("#dpFilterNum" + newNumber + "_DatePickerImage").click(function () {
        $("#dpFilterNum" + newNumber + "_DateInputControl").attr("id", "dateInput" + newNumber);
        $("#dateInput" + newNumber).attr("class", "DatePickerInput");
        $("#dateInput" + newNumber).datepicker({
            dateFormat: datePickerObj['dateFormat'],
            changeMonth: true,
            changeYear: true,
            yearRange: "1900:+10",
            nextText: datePickerObj['nextText'],
            prevText: datePickerObj['prevText'],
            dayNamesMin: datePickerObj['dayNamesMin'],
            monthNamesShort: datePickerObj['monthNamesShort']
        });
        $("#dateInput" + newNumber).datepicker("show");

    });

    //recreate 2nd datepicker method for the new row for "between" and "not between" conditions
    $("#dpFilterBwNum" + newNumber + "_DatePickerImage").click(function () {
        $("#dpFilterBwNum" + newNumber + "_DateInputControl").attr("id", "dateInputBw" + newNumber);
        $("#dateInputBw" + newNumber).attr("class", "DatePickerInput");
        $("#dateInputBw" + newNumber).datepicker({
            dateFormat: datePickerObj['dateFormat'],
            changeMonth: true,
            changeYear: true,
            yearRange: '1900:+10',
            nextText: datePickerObj['nextText'],
            prevText: datePickerObj['prevText'],
            dayNamesMin: datePickerObj['dayNamesMin'],
            monthNamesShort: datePickerObj['monthNamesShort']
        });
        $("#dateInputBw" + newNumber).datepicker("show");

    });


    //Update the criteria pattern text when and/or dropdown list changed
    $("#ddlAndOrNum" + newNumber).change(function () {
        var count = $('.ReportFormFilterLabelNumber').length;

        for (i = 1; i < Math.max.apply(null, customPattern.match(/\d+/g)); i++) {
            var aoreg = new RegExp("(and|or)(?=[() ]*?" + (i+1) + ")", "ig");
            customPattern = customPattern.replace(aoreg, $("option:selected", $('.ReportFormFilterAndOr').eq(i)).val());
        }
        CriteriaAutoUpdate(count);
    });
   
    //update criteria pattern
    CriteriaAutoUpdate(count + 1);
}

function AddFirstFilterRow() {
    $('#Filter_Zero_Panel').hide();
    $('#Main_FilterRow_Panel').show();
    $('#Filter_Criteria_Panel').show();
    for (j = 0; j < optionGroupObject.length; ++j) {
        $("#ddlDatasetColumnsFilterNum0 option[classification='" + optionGroupObject[j] + "']").wrapAll("<optgroup label='" + optionGroupObject[j] + "'>");
    }
    $("#ddlDatasetColumnsFilterNum0").prop('selectedIndex', 0);
    filterUsed = true;
    if ($('.ReportFormFilterLabelNumber').length < 1)
    FilterAddRow();
}

function FilterDeleteRow(deleteRowNum) {
    //check if the criteria pattern text box is close
    if ($("#criteriaContentTextBox").is(":visible")) {
        alert(MsgSaveBeforeDelete);
        return false;
    }

    //check if it is the last row to be deleted
    if ($('.ReportFormFilterLabelNumber').length == 1) {
        $('#Filter_Zero_Panel').show();
        $('#Main_FilterRow_Panel').hide();
        $('#Filter_Criteria_Panel').hide();
        $("#criteriaContentLabel").text('');
        filterUsed = false;
    }

    //remove the row number in the saved pattern
    var rowNumber = $("#label" + deleteRowNum).html();
    var lastAoIndex = Math.max(customPattern.lastIndexOf('d'), customPattern.lastIndexOf('r'));
    var lastNum = Math.max.apply(null, customPattern.match(/\d+/g));

    if (lastAoIndex > 0 && rowNumber <= lastNum){
       if (rowNumber == 1) 
          customPattern = customPattern.replace(/(and|or)/i, '').replace('1', '');
        for (i = 2; i <= lastNum; i++) {
            if (i == rowNumber) {
                var myregexp = new RegExp("(and|or)(?=[() ]*?" + i + ")", "ig");
                customPattern = customPattern.replace(myregexp, '').replace(i, '');
            }
            else if (i > rowNumber) {
                customPattern = customPattern.replace(i, i - 1);
            }
        }
    }

    //remove the selected row
    $("#RowPanelFilter" + deleteRowNum).remove();
    var count = $('.ReportFormFilterLabelNumber').length;

    //rearrange the labels of number order
    for (i = 0; i < count; i++) {
        $('.ReportFormFilterLabelNumber').eq(i).text(i + 1);
    }

    //show the last add row icon and hide the first and/or ddl
    $('.ReportFormFilterAndOr').first().css('visibility', 'hidden');
    $('.FilterImageLink').last().show();

    //update criteria pattern
    CriteriaAutoUpdate(count);

    //Adjust criteria textbox size
    $("#criteriaContentTextBox").css("width", $('.ReportFormFilterLabelNumber').length * 60 + 20 + "px");
}

//update criteria pattern when user change conditions
function CriteriaAutoUpdate(count) {
    (count > 2 && !$("#saveChangePatternButton").is(":visible")) ? $("#changePatternButton").show() : $("#changePatternButton").hide();

    // update the existing saved pattern
    customPattern = customPattern.toLowerCase().replace(/([\d\s\(\)]|^)[and]([\d\s\(\)]|$)/g, '$1and$2').replace(/([\d\s\(\)]|^)[or]([\d\s\(\)]|$)/g, '$1or$2');  //remove all capitalized style, fix typo
    customPattern = customPattern.replace(/[or]{2}/g, 'or').replace(/[and]{2,3}/g, 'and');  //continue fix typo
    customPattern = customPattern.replace(/\(\s*\)/g, ''); //remove all empty parenthesis 
    customPattern = customPattern.replace(/(and|or)[\s\)\(]*(and|or)/g, '$1'); //remove 2nd continuous and/or but no digit inside
    customPattern = customPattern.replace(/^([\s\(]*)(and|or)/, '$1'); //remove the and/or before the number 1 
    customPattern = customPattern.replace(/(and|or)([\s\)]*)$/, '$2'); //remove the and/or after the last number
    customPattern = customPattern.replace(/([\(\s]*)(and|or)(\s*[1-9]?[0-9])/g, '$2$1$3') //adjust left parentesis position
    customPattern = customPattern.replace(/(and|or)([\s\)]*)(\s*[1-9]?[0-9])/g, '$2$1$3') //adjust right parentesis position  
    customPattern = customPattern.replace(/\(\s*((and|or)\s*[1-9]?[0-9])\s*\)/g, '$1'); //remove parentesis if only and/or and a number in it
    customPattern = customPattern.replace(/\(\s*(\([0-9a-zA-z ]*\))\s*\)/g, '$1');  //removed redundant parenthesis      
    customPattern = customPattern.replace(/\s*(\(|and|or|[1-9]?[0-9]|\))\s*/g, '$1' + " ");  //adjust spaces around the number, and, or, (, )
    customPattern = customPattern.replace(/\(\s*([1-9]?[0-9])\s*\)/g, '$1');  //remove parenthesis around the number
    
    $("#criteriaContentLabel").text(customPattern);
    $("#criteriaContentTextBox").val(customPattern);

    // update the unsaved pattern
    for (i = Math.max(1, Math.max.apply(null, customPattern.match(/\d+/g))); i < count; i++) {
        $("#criteriaContentLabel").prepend("( ").append(" " + $("option:selected", $('.ReportFormFilterAndOr').eq(i)).val() + " " + String(i + 1) + " )");
        $("#criteriaContentTextBox").val("( " + $("#criteriaContentTextBox").val() + " " + $("option:selected", $('.ReportFormFilterAndOr').eq(i)).val() + " " + String(i + 1) + " )").css("width", $('.ReportFormFilterLabelNumber').length * 60 + 20 + "px");
    }
}

// generate the query string and assign to the hidden field
function UpdateQueryString() {
    var filterQueryString = $("#criteriaContentLabel").text().replace(/([\d]?[\d])/g, ':^$1^:');
    var filterRow, operatorStr, idNum;

    for (i = 0; i < $('.ReportFormFilterLabelNumber').length; i++) {
        filterRow = '';
        idNum = $('.ReportFormFilterAndOr').eq(i).attr('id').replace(/[A-Za-z]/g, '');
        if (idNum != '0') {
            $('#dpFilterNum' + idNum + '_DateInputControl').attr("id", "dateInput" + idNum);
            $('#dpFilterBwNum' + idNum + '_DateInputControl').attr("id", "dateInputBw" + idNum);
        }
        operatorStr = conditionOperator[$('option:selected', $('#ddlFilterConditionFilterNum' + idNum)).val()];
        operatorStr = operatorStr.replace('data', $('#tbFilterNum' + idNum).val().replace(/\'/g, '&#39;').replace(/\(/g, '&#40;').replace(/\)/g, '&#41;'));
        operatorStr = operatorStr.replace(/bDate/g, (idNum == '0') ? $('#dpFilterNum0_DateInputControl').val() : $('#dateInput' + idNum).val());
        operatorStr = operatorStr.replace('eDate', (idNum == '0') ? $('#dpFilterBwNum0_DateInputControl').val() : $('#dateInputBw' + idNum).val());

        filterRow += '[' + $('option:selected', $('#ddlDatasetColumnsFilterNum' + idNum)).val() + '] ';
        filterRow += operatorStr;

        if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).attr("dataType") == "Enum" ||
            $("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).attr("dataType") == "Contain")
        {
            if (filterRow.slice(-4) == "NULL") 
            {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + ")");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Course Status') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlCourseStatusNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Learning Path Status') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlLearningPathStatusNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Lesson Completion') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlLessonCompletionNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Completion Status') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlCompletionStatusNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Lesson Success') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlLessonSuccessNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Success Status') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlSuccessStatusNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Certification Status') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlCertificationStatusNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Roster Status') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlRosterStatusNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Item Type') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlPurchaseItemTypeNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Language') >= 0 ) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace('[##idLanguage##]', '[_sb##languageCode##]').replace(/undefined/g, '') + "'" + $('option:selected', $('#ddlLanguageNum' + idNum)).val() + "')");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Timezone') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + $('option:selected', $('#ddlTimezoneNum' + idNum)).val() + ")");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Content Type_s') >= 0) {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '').replace('%\'', $('option:selected', $('#ddlContentTypesNum' + idNum)).val() + '%\'') + ")");
            }
            else if ($("option:selected", $("select[id^='ddlDatasetColumnsFilterNum']").eq((i).toString())).val().indexOf('Current Track') >= 0) {                
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + $('option:selected', $('#ddlCertificationTrackNum' + idNum)).val() + ")");
            }
            else {
                filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:', "(" + filterRow.replace(/undefined/g, '') + $('option:selected', $('#ddlEnumNum' + idNum)).val() + ")");
            }
        }
        else
        {
            filterQueryString = filterQueryString.replace(':^' + (i + 1) + '^:',"(" + filterRow.replace(/undefined/g, '') + ")");
        }
    }
    
    $("#hdReportFilter").val((filterUsed) ? filterQueryString : '');
    }

//put the pattern into a text box for modification
function ChangePattern() {
    $("#criteriaContentTextBox").val($("#criteriaContentLabel").text()).show();
    $("#criteriaContentLabel").hide();
    $("#saveChangePatternButton").show();
    $("#divider").show();
    $("#cancelChangePatternButton").show();
    $("#changePatternButton").hide();
}

//cancel change pattern action
function CancelChangePattern() {
    $("#criteriaContentTextBox").hide();
    $("#criteriaContentLabel").show();
    $("#changePatternButton").show();
    $("#saveChangePatternButton").hide();
    $("#divider").hide();
    $("#cancelChangePatternButton").hide();
}

//save the modification to the change pattern
function SaveChangePattern() {
    customPattern = $("#criteriaContentTextBox").val();
    origPattern = $("#criteriaContentLabel").text();
    CriteriaAutoUpdate($('.ReportFormFilterLabelNumber').length);
    var str = customPattern;
    var strArray = str.match(/\d+/g);

    if (str.length == 0) {
        str = customPattern;
    }

    if (/[^\d\s\(\)andor]/ig.test(str)) {
        alert(MsgCharNotAllowed + '\n' + MsgAdjustAgain);
        $("#criteriaContentTextBox").val(origPattern);
        return false;
    }

    if ((strArray.length != $('.ReportFormFilterLabelNumber').length) || (parseInt($('.ReportFormFilterLabelNumber').eq($('.ReportFormFilterLabelNumber').length - 1).text(), 10) != Math.max.apply(null, strArray))) {
        alert(MsgNumNotMached + '\n' + MsgAdjustAgain);
        $("#criteriaContentTextBox").val(origPattern);
        return false;
    }


    for (i = 0; i < strArray.length - 1; i++) {
        if (parseInt(strArray[i],10) >= parseInt(strArray[i + 1],10)) {
            alert(MsgNotInOrder + strArray[i] + '\n' + MsgAdjustAgain);
            $("#criteriaContentTextBox").val(origPattern);
            return false;
        }
    }

    if (!bracketsAreBalanced(str)) {
        alert(MsgNotBalancedparenthesis + '\n' + MsgAdjustAgain);
        $("#criteriaContentTextBox").val(origPattern);
        return false;
    } else {
        $("#criteriaContentLabel").text(customPattern);
        $("#criteriaContentTextBox").val(customPattern);
    }

    for (i = 1; i < $('.ReportFormFilterLabelNumber').length; i++)
        $(".ReportFormFilterAndOr").eq(i).val(str.substring(str.indexOf(i) + i.toString().length, str.indexOf(i + 1)).replace(/[( )]/g, ''));

    UpdateQueryString();
    CancelChangePattern();
}

function bracketsAreBalanced(s) {
    var open = (arguments.length > 1) ? arguments[1] : '(';
    var close = (arguments.length > 2) ? arguments[2] : ')';
    var c = 0;
    for (var i = 0; i < s.length; i++) {
        var ch = s.charAt(i);
        if (ch == open)
            c++;        
        else if (ch == close)
            c--;
         if (c < 0) return false;
    }

    for (i = 0; i < c; i++) 
        s = s.replace("(", "");

    customPattern = s;
    return true;
}

function criteriaEndPosition(s) {
    var open = (arguments.length > 1) ? arguments[1] : '(';
    var close = (arguments.length > 2) ? arguments[2] : ')';
    var c = 0;
    var endPosition;
    for (var i = 0; i < s.length; i++) {
        var ch = s.charAt(i);
        if (ch == open)
            c++;
        else if (ch == close)
            c--;
        if (c < 0) return i;
    }
    return s.length - 1;
}

function OrderAddRow() {

    var count = $('.orderDDL').length;
    var newNumber = String(orderColumnSeed++);
    var orderColumnRow = addOrderRowHdn.replace(/Num0/g, 'Num' + newNumber).replace(FirstByText, ThenByText);
    $("select[id^='ddlDatasetColumnsOrderNum']").prop("disabled", true);

    $("div[id^='panelOrderColumnNum']:last").after('<div id=\'panelOrderColumnNum' + newNumber + '\'>' + orderColumnRow + '</div>');

    OrderByDisabledWhenNotSelectedField();
 
    $("option:selected", $("#ddlDatasetColumnsOrderNum" + newNumber)).removeAttr("selected");

    $('.OrderImageLink').first().show();
    (($("select[id$='FieldsToInclude_SelectedItems'] option").length - count - DisableSortableColumns) == 1) ? $('.OrderImageLink').last().hide() : $('.OrderImageLink').last().show();
}

function OrderDeleteRow(deleteRowNum) {
    var deletedValue = $("option:selected", $("#ddlDatasetColumnsOrder" + deleteRowNum)).val();
    var deletedText = $("option:selected", $("#ddlDatasetColumnsOrder" + deleteRowNum)).text();

    //remove the selected row
    $("#panelOrderColumn" + deleteRowNum).remove();

    var lastSelected = $("option:selected", $(".orderDDL:last")).val();
    $("#orderColumnPanelControls_LabelContainer").text(FirstByText + ":");

    $(".orderDDL:last option[value='" + deletedValue + "']").hide();
    $(".orderDDL:last").append(
    $('<option></option>').val(deletedValue).html(deletedText)
    );
    OrderByDisabledWhenNotSelectedField();
    $(".orderDDL:last").val(lastSelected);
    ($('.orderDDL').length > 1) ? $('.OrderImageLink').first().show() : $('.OrderImageLink').first().hide();
    ($('.orderDDL:last > option').length > 1) ? $('.OrderImageLink').last().show() : $('.OrderImageLink').last().hide();
}


function OrderByDisabledWhenNotSelectedField() {

    $(".orderDDL:last").find('option').remove().end();

    var FiledToIncludeSize = $('#ReportModify_FieldsToInclude_SelectedItems option').size();
    DisableSortableColumns = 0;
    for (i = 0; i < FiledToIncludeSize; i++) {
        var availableValue = $("#ReportModify_FieldsToInclude_SelectedItems option:eq(" + i.toString() + ")").val()
        var availableText = $("#ReportModify_FieldsToInclude_SelectedItems option:eq(" + i.toString() + ")").text()
        if (availableValue != "Interaction Description" && availableValue != "Interaction Type" && availableValue != "Learner Response" && availableValue != "Correct Responses") {
            $(".orderDDL:last").append(
            $('<option></option>').val(availableValue).html(availableText)
            );
        } else {
            DisableSortableColumns++;
        }
    }
    var count = $('.orderDDL').length;
    for (i = 0; i < count -1; i++) {
        var removedValue = $("option:selected", $('.orderDDL').eq(i)).val();
        $(".orderDDL:last option[value='" + removedValue + "']").remove();
    }

    var selectedNumber = $("select[id$='FieldsToInclude_SelectedItems'] option").length;
    var count = $('.orderDDL').length;

    //always make the last row enabled since IE 11 can not show the single item after selector disabled
    $('.orderDDL').last().prop("disabled", false);

    $('.OrderImageLink:odd').hide();
    $('.OrderImageLink:even').show();
}

function UpdateSelectedFieldsString() {
    var selectedFieldsString = '';
    var selectedNumber = $("select[id$='FieldsToInclude_SelectedItems'] option").length;
    for (i = 0; i < selectedNumber; i++) {
        selectedFieldsString = selectedFieldsString + "[" + $("select[id$='FieldsToInclude_SelectedItems'] option:eq("+ i.toString() + ")").val() +"],";
    }
    selectedFieldsString = selectedFieldsString.substr(0, selectedFieldsString.length - 1);
    $("#hdReportField").val(selectedFieldsString);
}
