﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Asentia.Common;
using Asentia.Controls;
using Asentia.LRS.Controls;
using Asentia.UMS.Library;
using Newtonsoft.Json;

namespace Asentia.LRS.Library
{
    [XmlInclude(typeof(LanguageSpecificProperty))]
    public class Report
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Report()
        {
           this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idCourse">Course Id</param>
        public Report(int idReport)
        {
            this.LanguageSpecificProperties = new ArrayList();
            this._Details(idReport);
            this._GetPropertiesInLanguages(idReport);
            this._SubscriptionDetails(idReport);
            this.NumberOfSavedFiles = ReportFile.GetNumberOfSavedFiles(idReport);
        }
        #endregion

        #region Properties
        /// <summary>
        /// The stored procedure used to populate a grid for this object.
        /// </summary>
        public static readonly string GridProcedure = "[Report.GetGrid]";

        /// <summary>
        /// Report Id.
        /// </summary>
        public int IdReport { get; set; }
        

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite { get; set; }
        
        /// <summary>
        /// Dataset Id.
        /// </summary>
        public int IdDataset { get; set; }
        
        /// <summary>
        /// User Id.
        /// </summary>
        public int IdUser { get; set; }

        /// <summary>
        /// Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;

        /// <summary>
        /// Fields.
        /// </summary>
        public string Fields { get; set; }

        /// <summary>
        /// Short Description.
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Filter.
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Max Records.
        /// </summary>
        public int MaxRecords { get; set; }

        /// <summary>
        /// Is Public.
        /// </summary>
        public bool IsPublic { get; set; }

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime? DtCreated { get; set; }

        /// <summary>
        /// Date Modified.
        /// </summary>
        public DateTime? DtModified { get; set; }

        /// <summary>
        /// Is Deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Report Subscription Id.
        /// </summary>
        public int IdReportSubscription { get; set; }

        /// <summary>
        /// Date Deleted.
        /// </summary>
        public DateTime? DtDeleted { get; set; }

        /// <summary>
        /// Date Subscription Started.
        /// </summary>
        public DateTime DtSubscriptionStart { get; set; }

        /// <summary>
        /// Recur Interval
        /// </summary>
        public int RecurInterval { get; set; }

        /// <summary>
        /// Recur Timeframe
        /// </summary>
        public string RecurTimeframe { get; set; }

        /// <summary>
        /// File Numbers
        /// </summary>
        public int NumberOfSavedFiles { get; set; }

        /// <summary>
        /// Has Shortcut
        /// </summary>
        public bool HasShortcut { get; set; }

        #endregion

        #region Private Properties
        private const int _REPORT_HTML_COLUMNS_MAX = 1000;
        private const int _REPORT_HTML_ROWS_MAX = 1000;
        private const int _REPORT_PDF_COLUMNS_MAX = 8;
        private const int _REPORT_PDF_ROWS_MAX = 1000;
        private const string _TABLE_SORTING_JAVASCRIPT_FILE = "Asentia.Common.TableSorting.js";

        #endregion

        #region Classes
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>      
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Title;

            public LanguageSpecificProperty()
            { ;}
            public LanguageSpecificProperty(string langString, string title)
            {
                this.LangString = langString;
                this.Title = title;
            }
        }
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves report data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }
        #endregion

        #region UpdateSubscription
        /// <summary>
        /// Saves report subscription data to the database as the current session user.
        /// </summary>
        public int UpdateSubscription()
        {
            return this._UpdateSubscription(AsentiaSessionState.IdSiteUser);
        }
        #endregion

        #region Run
        /// <summary>
        /// Run Report and have them been generated and saved in the warehouse/reportdata folder
        /// overwritePDFRestriction: Normally max number for columnns is 8 and max number of rows is 1000
        /// Set overwritePDFRestriction to ture if want to pass the restriction to PDF format reports
        /// Return null if no record found
        /// </summary>
        public string Run(bool htmlFormat, bool csvFormat, bool pdfFormat, bool overwritePDFRestriction)
        {
            return Run(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, htmlFormat, csvFormat, pdfFormat, overwritePDFRestriction);
        }
        #endregion

        #region Run
        /// <summary>
        /// Run Report and have them been generated and saved in the warehouse/reportdata folder
        /// overwritePDFRestriction: Normally max number for columnns is 8 and max number of rows is 1000
        /// Set overwritePDFRestriction to ture if want to pass the restriction to PDF format reports
        /// Return null if no record found
        /// </summary>
        public string Run(int idSite, string userCulture, int idSiteUser, bool htmlFormat, bool csvFormat, bool pdfFormat, bool overwritePDFRestriction)
        {
            //var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            //var random = new Random();
            //string randomString = new string(Enumerable.Repeat(chars, 8).Select(s => s[random.Next(s.Length)]).ToArray());

            string reportTitle;

            if (this.IdDataset < 1000) // built-in dataset
            { reportTitle = String.IsNullOrWhiteSpace(this.Title) ? Enum.GetName(typeof(EnumDataSet), this.IdDataset) : Utility.RemoveSpecialCharactersForFileName(this.Title); }
            else // custom dataset
            {
                ReportDataSet datasetObject = new ReportDataSet(this.IdDataset);
                reportTitle = String.IsNullOrWhiteSpace(this.Title) ? Utility.RemoveSpecialCharactersForFileName(datasetObject.Name) : Utility.RemoveSpecialCharactersForFileName(this.Title);
            }

            string fileName = reportTitle + DateTime.UtcNow.ToString("_yyyy-MM-dd-HH-mm-ss");
            DataTable reformedReportData = _DataTableTransform(this._Run(idSite, userCulture, idSiteUser, true));

            if (reformedReportData.Rows.Count > 0)
            {
                if (htmlFormat)
                { _SaveReportHTMLFile(reformedReportData, reportTitle, fileName); }

                if (csvFormat)
                {                    
                    if (reformedReportData.Columns.Contains("Data Lesson ID"))
                    { reformedReportData.Columns.Remove("Data Lesson ID"); }

                    if (reformedReportData.Columns.Contains("SCO Identifier"))
                    { reformedReportData.Columns.Remove("SCO Identifier"); }
                       
                    _SaveReportCSVFile(reformedReportData, reportTitle, fileName);
                }

                if (pdfFormat && (AsentiaSessionState.GlobalSiteObject.ParamBool(SiteParamConstants.REPORTINGPDFEXPORT_ENABLE) ?? true) && 
                    (overwritePDFRestriction || (reformedReportData.Columns.Count <= _REPORT_PDF_COLUMNS_MAX && reformedReportData.Rows.Count <= _REPORT_PDF_ROWS_MAX)))
                {                    
                    if (reformedReportData.Columns.Contains("Data Lesson ID"))
                    { reformedReportData.Columns.Remove("Data Lesson ID"); }

                    if (reformedReportData.Columns.Contains("SCO Identifier"))
                    { reformedReportData.Columns.Remove("SCO Identifier"); }

                    _SaveReportPDFFile(reformedReportData, reportTitle, fileName);
                }

                return fileName;
            }
            else
            {
                return null;
            }
        }
        #endregion
        #endregion

        #region Static Methods
        /// <summary>
        /// Run Report and have them been generated and saved in the warehouse/reportdata folder
        /// overwritePDFRestriction: Normally max number for columnns is 8 and max number of rows is 1000
        /// Set overwritePDFRestriction to ture if want to pass the restriction to PDF format reports
        /// Return null if no record found
        /// </summary>
        public static DataTable Run(int idSite, string userCulture, int idSiteUser, string fields, string filter, string order, int maxRecords, int idDataset, string accountWebConfigPath, string timezoneDotNetName, string defaultUserAccountDataPath, string siteUserAccountDataPath, string siteDefaultLanguageString)
        {
            DataTable reformedReportData = _DataTableTransform(_Run(idSite, userCulture, idSiteUser, idDataset, fields, filter, order, maxRecords, true, accountWebConfigPath, defaultUserAccountDataPath, siteUserAccountDataPath, siteDefaultLanguageString), timezoneDotNetName, idSite, idSiteUser, userCulture, defaultUserAccountDataPath, siteUserAccountDataPath, siteDefaultLanguageString, accountWebConfigPath);
            return reformedReportData;
        }

        #region _Run
        /// <summary>
        /// Run the report and store reports into specific folder
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the report</returns>
        private static DataTable _Run(int idSite, string userCulture, int idCaller, int idDataset, string fields, string filter, string order, int maxRecords, bool logReportExecution, string accountWebConfigPath = null, string defaultUserAccountDataPath = null, string siteUserAccountDataPath = null, string siteDefaultLanguageString = null)
        {
            AsentiaDatabase databaseObject;

            if (!String.IsNullOrWhiteSpace(accountWebConfigPath))
            { databaseObject = new AsentiaDatabase(accountWebConfigPath, DatabaseType.AccountDatabaseUsingWebConfigPath, 1140); }
            else
            { databaseObject = new AsentiaDatabase(1140); }

            // arrange the filter to accommodate the input type of date for isTyepable fields
            List<UserFieldGlobalRules> globalUserFieldRules = UserFieldGlobalRules.GetAllRules();

            foreach (UserFieldGlobalRules ruleSet in globalUserFieldRules)
            {
                if (ruleSet.IsTypeable && filter.Contains("[##" + ruleSet.Identifier + "##]"))
                {
                    UserAccountData uad;

                    if (!String.IsNullOrWhiteSpace(defaultUserAccountDataPath) && !String.IsNullOrWhiteSpace(siteUserAccountDataPath))
                    { uad = new UserAccountData(UserAccountDataFileType.Site, true, false, defaultUserAccountDataPath, siteUserAccountDataPath, siteDefaultLanguageString); }
                    else
                    { uad = new UserAccountData(UserAccountDataFileType.Site, true, false); }
                    

                    foreach (UserAccountData.Tab tab in uad.Tabs)
                    {
                        foreach (UserAccountData.UserField userField in tab.UserFields)
                        {
                            if (userField.Identifier == ruleSet.Identifier && userField.InputType == UserAccountData.InputType.Date)
                            {
                                filter = filter.Replace("[##" + userField.Identifier + "##]", "DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), TRY_CAST([##" + userField.Identifier + "##] AS DATETIME))");
                            }
                        }
                    }
                }
            }

            // replace filter single quote value to runnable sql treatment
            filter = filter.Replace("&#39;", "''");

            DataTable dtReportData = new DataTable();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", idSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", userCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@fields", fields, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@whereClause", filter, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            // if the order string does not already end in a comma, make it end with a comma
            // the comma gets stripped by the SQL procedure, but is put here to help make the calling 
            // of dataset procedures more secure by breaking the SQL if there is another query attached
            if (!String.IsNullOrWhiteSpace(order) && order.Substring(order.Length - 1) != ",")
            { databaseObject.AddParameter("@orderByClause", order + ",", SqlDbType.NVarChar, 255, ParameterDirection.Input); }
            else
            { databaseObject.AddParameter("@orderByClause", order, SqlDbType.NVarChar, 255, ParameterDirection.Input); }

            databaseObject.AddParameter("@maxRecords", maxRecords, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@logReportExecution", logReportExecution, SqlDbType.Bit, 1, ParameterDirection.Input);

            try
            {
                SqlDataReader reportDataReader;

                if (idDataset < 1000) // built-in dataset
                { reportDataReader = databaseObject.ExecuteDataReader("[Dataset." + Enum.GetName(typeof(EnumDataSet), idDataset) + "]", true); }
                else // custom dataset
                {
                    ReportDataSet datasetObject = new ReportDataSet(idDataset);
                    reportDataReader = databaseObject.ExecuteDataReader(datasetObject.StoredProcedureName, true);
                }

                dtReportData.Load(reportDataReader);
                reportDataReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            // JC 10-27-2017: I'm not sure why we'd remove this column for any reason. So I commented it out, but am leaving it here just in case.
            // if (dtReportData.Columns.Contains("_idUser") && (idDataset != (int)EnumDataSet.UserCourseTranscripts) && (idDataset != (int)EnumDataSet.UserLearningPathTranscripts) && (idDataset != (int)EnumDataSet.UserInstructorLedTranscripts))
            // {
                // dtReportData.Columns.Remove("_idUser");
            // }

            return dtReportData;
        }
        #endregion
        #endregion

        #region Public Methods
        #region Save Language
        /// <summary>
        /// Add or updates the optional language fields for a report
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseFieldNotUniqueException">
        /// Thrown when a field must be unique..
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name=""></param>
        public void SaveLang(string languageString, string title)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReport", this.IdReport, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@languageString", languageString, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@title", title, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Report.SaveLang]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetPersonalTranscriptQuickView
        public DataTable GetPersonalTranscriptQuickView(EnumDataSet transcriptType)
        {
            DataTable dtTranscript = new DataTable();
            this.IdUser = AsentiaSessionState.IdSiteUser;
            this.IdDataset = (int)transcriptType;
            this.IdSite = AsentiaSessionState.IdSite;
            switch (transcriptType){
                case EnumDataSet.UserCourseTranscripts:
                    this.Fields = "[Course],[Credits],[Date Completed]";
                    break;
                case EnumDataSet.UserLearningPathTranscripts:
                    this.Fields = "[Learning Path],[Date Completed]";
                    break;
                case EnumDataSet.UserInstructorLedTranscripts:
                    this.Fields = "[Module],[Session],[Date Completed]";
                    break;
            }
            this.Filter = "( (_idUser = " + AsentiaSessionState.IdSiteUser + ") and ([Date Completed] <> '') and ([Date Completed] IS NOT NULL) )";
            this.MaxRecords = 10;
            this.Order = "[Date Completed] DESC,";
            dtTranscript = this._Run(false);

            foreach (DataRow sourcerow in dtTranscript.Rows)
            {
                if (!String.IsNullOrWhiteSpace(sourcerow["Date Completed"].ToString()))
                {
                    // if the data is a date, it should be displayed as a formatted date
                    DateTime dateValue;
                    if (DateTime.TryParse(sourcerow["Date Completed"].ToString(), out dateValue))
                    {
                        // convert to local datetime
                        dateValue = TimeZoneInfo.ConvertTimeFromUtc(dateValue, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                        sourcerow["Date Completed"] = Convert.ToDateTime(dateValue).ToString("yyyy-MM-dd HH:mm:ss");
                    }
                }
            }

            return dtTranscript;
        }
        #endregion

        #region GetPersonalTranscriptFullView
        public DataTable GetPersonalTranscriptFullView(string userID, EnumDataSet transcriptType)
        {
            DataTable dtTranscript = new DataTable();
            this.IdUser = AsentiaSessionState.IdSiteUser;
            this.IdDataset = (int)transcriptType;
            this.IdSite = AsentiaSessionState.IdSite;
            switch (transcriptType)
            {
                case EnumDataSet.UserCourseTranscripts:
                    this.Fields = "[Course],[Credits],[Course Status],[Date Completed],[Lesson],[Lesson Completion],[Lesson Success],[Score]";
                    break;
                case EnumDataSet.UserLearningPathTranscripts:
                    this.Fields = "[Learning Path],[Learning Path Status],[Date Completed]";
                    break;
                case EnumDataSet.UserInstructorLedTranscripts:
                    this.Fields = "[Module],[Session],[Completion Status],[Success Status],[Date Completed],[Session Score]";
                    break;
            }
            this.Filter = "( (_idUser = " + userID + ") and ([Date Completed] <> '') and ([Date Completed] IS NOT NULL) )";
            this.Order = "[Date Completed] DESC,";
            dtTranscript = this._Run(false);

            foreach (DataRow sourcerow in dtTranscript.Rows)
            {
                if (!String.IsNullOrWhiteSpace(sourcerow["Date Completed"].ToString()))
                {
                    // if the data is a date, it should be displayed as a formatted date
                    DateTime dateValue;
                    if (DateTime.TryParse(sourcerow["Date Completed"].ToString(), out dateValue))
                    {
                        // convert to local datetime
                        dateValue = TimeZoneInfo.ConvertTimeFromUtc(dateValue, TimeZoneInfo.FindSystemTimeZoneById(AsentiaSessionState.UserTimezoneDotNetName));
                        sourcerow["Date Completed"] = Convert.ToDateTime(dateValue).ToString("yyyy-MM-dd HH:mm:ss");
                    }
                }
            }

            return dtTranscript;
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a report.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Reports", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Report.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete Report Subscription
        /// <summary>
        /// Deletes report subscriptions.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void DeleteReportSubscription(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@ReportSubscriptions", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[ReportSubscription.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _AddShortcut
        /// <summary>
        /// Add Report Shortcut
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the report</returns>
        public static void _AddShortcut(DataTable idReports)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReports", idReports, SqlDbType.Structured, null, ParameterDirection.Input);
            try
            {
                databaseObject.ExecuteNonQuery("[Report.AddShortcuts]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _RemoveShortcut
        /// <summary>
        /// Remove Report Shortcut
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the report</returns>
        public static void _RemoveShortcut(DataTable idReports)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReports", idReports, SqlDbType.Structured, null, ParameterDirection.Input);
            try
            {
                databaseObject.ExecuteNonQuery("[Report.DeleteShortcuts]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idCourse">Report Id</param>
        private void _Details(int idReport)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReport", idReport, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idDataset", this.IdDataset, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@fields", this.Fields, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@filter", this.Filter, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@order", this.Order, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@isPublic", this.IsPublic, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", this.DtCreated, SqlDbType.DateTime, null, ParameterDirection.Output); // confirm null part
            databaseObject.AddParameter("@dtModified", this.DtModified, SqlDbType.DateTime, null, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", this.IsDeleted, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@hasShortcut", this.HasShortcut, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Report.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdReport = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idReport"].Value);
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.IdDataset = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idDataset"].Value);
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.Fields = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@fields"].Value);
                this.Filter = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@filter"].Value);
                this.Order = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@order"].Value);
                this.IsPublic = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isPublic"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.HasShortcut = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@hasShortcut"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _SubscriptionDetails
        /// <summary>
        /// Retrieves subscription details if any
        /// </summary>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idCourse">Report Id</param>
        private void _SubscriptionDetails(int idReport)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReport", idReport, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idReportSubscription", idReport, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@dtStart", this.DtSubscriptionStart, SqlDbType.DateTime, null, ParameterDirection.Output);
            databaseObject.AddParameter("@recurInterval", this.RecurInterval, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@recurTimeframe", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[ReportSubscription.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdReport = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idReport"].Value);
                this.IdReportSubscription = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idReportSubscription"].Value);
                this.DtSubscriptionStart = AsentiaDatabase.ParseDbParamDateTime(databaseObject.Command.Parameters["@dtStart"].Value).ToLocalTime();
                this.RecurInterval = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@recurInterval"].Value);
                this.RecurTimeframe = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@recurTimeframe"].Value);
            }
            catch (DatabaseDetailsNotFoundException dnfEx)
            {
                this.DtSubscriptionStart = AsentiaSessionState.UserTimezoneCurrentLocalTime;
                this.RecurInterval = 0;
                this.RecurTimeframe = "";
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Save
        /// <summary>
        /// Saves group data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the report</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idReport", this.IdReport, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataset", this.IdDataset, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@fields", this.Fields, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@filter", this.Filter, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@order", Regex.Replace(this.Order, @",*$", ""), SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@isPublic", this.IsPublic, SqlDbType.Bit, 1, ParameterDirection.Input);
            try
            {
                databaseObject.ExecuteNonQuery("[Report.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdReport = Convert.ToInt32(databaseObject.Command.Parameters["@idReport"].Value);
                return this.IdReport;

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _UpdateSubscription
        /// <summary>
        /// Update Report Subscription
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the report</returns>
        private int _UpdateSubscription(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReportSubscription", this.IdReportSubscription, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idReport", this.IdReport, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtSubscriptionStart.ToUniversalTime(), SqlDbType.DateTime, null, ParameterDirection.Input);
            databaseObject.AddParameter("@recurInterval", this.RecurInterval, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@recurTimeframe", this.RecurTimeframe, SqlDbType.NVarChar, 4, ParameterDirection.Input);
            try
            {
                databaseObject.ExecuteNonQuery("[ReportSubscription.Update]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdReportSubscription = Convert.ToInt32(databaseObject.Command.Parameters["@idReportSubscription"].Value);
                return this.IdReportSubscription;

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Run
        /// <summary>
        /// Run the report and store reports into specific folder
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the report</returns>
        private DataTable _Run(bool logReportExecution = true)
        {
            return _Run(AsentiaSessionState.IdSite, AsentiaSessionState.UserCulture, AsentiaSessionState.IdSiteUser, logReportExecution);
        }
        #endregion

        #region _Run
        /// <summary>
        /// Run the report and store reports into specific folder
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the report</returns>
        private DataTable _Run(int idSite, string userCulture, int idCaller, bool logReportExecution)
        {
            return _Run(idSite, userCulture, idCaller, this.IdDataset, this.Fields, this.Filter, this.Order, this.MaxRecords, logReportExecution);           
        }
        #endregion

        #region _DataTableTransform
        /// <summary>
        /// _Transform DataTable into Presentable Format
        /// </summary>
        /// <param name="source">DataTable to transform data for</param>
        /// <param name="timezoneDotNetName">the timezone dot net name for date time conversions, optional - if not specified the value will come from session state</param>
        /// <param name="idSite">the site id, optional - if not specified the value will some from session state, used only for calling method to get timezone names</param>
        /// <param name="idCaller">the caller id, optional - if not specified the value will some from session state, used only for calling method to get timezone names</param>
        /// <param name="userCulture">the language to convert to, optional - if not specified the value will come from session state</param>
        private static DataTable _DataTableTransform(DataTable source, string timezoneDotNetName = null, int? idSite = null, int? idCaller = null, string userCulture = null, string defaultUserAccountDataPath = null, string siteUserAccountDataPath = null, string siteDefaultLanguageString = null, string accountWebConfigPath = null)
        {
            DataTable dest = source.Clone();
            ReportDataSet ds = new ReportDataSet();

            // if timezoneDotNetName, idSite, idCaller, userCulture, or siteDefaultLanguageString are null, use their respective session state values
            if (String.IsNullOrWhiteSpace(timezoneDotNetName))
            { timezoneDotNetName = AsentiaSessionState.UserTimezoneDotNetName; }

            if (idSite == null)
            { idSite = AsentiaSessionState.IdSite; }

            if (idCaller == null)
            { idCaller = AsentiaSessionState.IdSiteUser; }

            if (String.IsNullOrWhiteSpace(userCulture))
            { userCulture = AsentiaSessionState.UserCulture; }

            if (String.IsNullOrWhiteSpace(siteDefaultLanguageString))
            { siteDefaultLanguageString = AsentiaSessionState.GlobalSiteObject.LanguageString; }

            // get a DataTable of timezones so we can use their names
            DataTable dtTimezones = Timezone.GetTimezones((int)idSite, userCulture, (int)idCaller, accountWebConfigPath);

            // get a DataTable of languages so we can use their codes
            DataTable dtLanguages = Language.GetLanguages((int)idSite, userCulture, (int)idCaller, accountWebConfigPath);

            // list of destination table columns to remove
            List<string> columnNamesToRemoveFromDestinationTable = new List<string>();

            foreach (DataColumn col in dest.Columns)
            {
                if (col.DataType.ToString().Contains("Boolean") || col.ColumnName.Contains("Language") || 
                    col.ColumnName.Contains("Timezone") || col.ColumnName.Contains("Course Status") || 
                    col.ColumnName.Contains("Learning Path Status") || col.ColumnName.Contains("Lesson Success") || 
                    col.ColumnName.Contains("Lesson Completion") || col.ColumnName.Contains("Completion Status") || 
                    col.ColumnName.Contains("Success Status") || col.ColumnName.Contains("Result") ||
                    col.ColumnName.Contains("Interaction Description") || col.ColumnName.Contains("Interaction Type") ||
                    col.ColumnName.Contains("Learner Response") || col.ColumnName.Contains("Correct Responses") ||
                    col.ColumnName.Contains("Content Type_s") || col.ColumnName.Contains("Certification Status") || 
                    col.ColumnName.Contains("Roster Status"))
                {
                    col.DataType = typeof(String);
                    if (col.ColumnName.Contains("Interaction Description") || col.ColumnName.Contains("Learner Response") || col.ColumnName.Contains("Correct Responses"))
                    {
                        col.MaxLength = -1;
                    }
                    else
                    {
                        col.MaxLength = 255;
                    }
                }

                col.ExtendedProperties.Add("DateFormat", (col.ColumnName.Contains("##dtDOB##") || col.ColumnName.Contains("##dtHire##") || col.ColumnName.Contains("##dtTerm##"))?"NoTime":"FullDateTime");
                 
                // if the column begins with a _, it should never be seen by the user, catalog it for removal
                if (col.ColumnName.IndexOf("_") == 0)
                { columnNamesToRemoveFromDestinationTable.Add(col.ColumnName); }
            }

            foreach (DataRow sourcerow in source.Rows)
            {
                DataRow destRow = dest.NewRow();
                string[] returnInteractionData = new string[4];
                string correctResponses = string.Empty;

                if (sourcerow.Table.Columns.Contains("Interaction") && sourcerow["Interaction"] != null && sourcerow["Interaction"].ToString().Length > 1)
                {
                    returnInteractionData = _RetrieveInteractionDataForReports(Convert.ToInt32(sourcerow["_idUser"]), Convert.ToInt32(sourcerow["_idDataLesson"]), sourcerow["_scoIdentifier"].ToString(), sourcerow["Interaction"].ToString());
                
                    if (returnInteractionData[3] != null)
                    {
                        returnInteractionData[3] = returnInteractionData[3].Replace("\"pattern\":", string.Empty).Replace("]", string.Empty).Replace("{", string.Empty).Replace("}", string.Empty);
                        string[] correctObject = returnInteractionData[3].Substring(1, returnInteractionData[3].Length - 1).Split(new string[] { "," }, StringSplitOptions.None);

                        if (correctObject.Length > 1)
                        {
                            correctResponses = "( ";
                            for (int i = 0; i < correctObject.Length; i++)
                            {
                                correctResponses += correctObject[i].Replace("\"", string.Empty) + ((i != correctObject.Length - 1) ? " , " : "");
                            }
                            correctResponses += " )";
                        }
                        else if (correctObject.Length == 1)
                        {
                            correctResponses = correctObject[0].Replace("\"", string.Empty);
                        }
                        correctResponses = correctResponses.Trim();
                    }
                }

                foreach (DataColumn col in source.Columns)
                {
                    if (col.DataType.ToString().Contains("Date") && !String.IsNullOrWhiteSpace(sourcerow[col].ToString()))
                    {
                        // if the data is a date, it should be displayed as a formatted date
                        DateTime dateValue;

                        if (DateTime.TryParse(sourcerow[col].ToString(), out dateValue))
                        {
                            // dtDOB, dtHire and dtTerm not convert to local time.
                            if (col.ColumnName.Contains("##dtDOB##") || col.ColumnName.Contains("##dtHire##") || col.ColumnName.Contains("##dtTerm##"))
                            {
                                destRow[col.ToString()] = Convert.ToDateTime(dateValue).ToShortDateString();
                            }
                            else
                            {
                                dateValue = TimeZoneInfo.ConvertTimeFromUtc(dateValue, TimeZoneInfo.FindSystemTimeZoneById(timezoneDotNetName));
                                destRow[col.ToString()] = Convert.ToDateTime(dateValue).ToString();   
                            }
                        }
                    }
                    else if (col.ColumnName.Contains("Certificate Status"))
                    {
                        if (String.IsNullOrWhiteSpace(sourcerow[col].ToString()))
                            destRow[col.ToString()] = String.Empty;
                        else
                            destRow[col.ToString()] = (Convert.ToBoolean(sourcerow[col])) ? _GlobalResources.Active : _GlobalResources.Inactive;
                    }
                    else if (col.ColumnName.Contains("isActive"))
                    {
                        destRow[col.ToString()] = (Convert.ToBoolean(sourcerow[col])) ? _GlobalResources.Active : _GlobalResources.Disabled;
                    }
                    else if (col.DataType.ToString().Contains("Boolean"))
                    {
                        if (String.IsNullOrWhiteSpace(sourcerow[col].ToString()))
                            destRow[col.ToString()] = _GlobalResources.NULL;
                        else
                            destRow[col.ToString()] = (Convert.ToBoolean(sourcerow[col])) ? _GlobalResources.Yes : _GlobalResources.No;
                    }
                    else if (col.ColumnName.Contains("Language"))
                    {
                        destRow[col.ToString()] = "";

                        foreach (DataRow lgRow in dtLanguages.Rows)
                        {
                            if (lgRow["idLanguage"].ToString() == sourcerow[col.ToString()].ToString())
                            {
                                CultureInfo cultureInfo = CultureInfo.GetCultureInfo(lgRow["code"].ToString());
                                destRow[col.ToString()] = cultureInfo.NativeName;
                                break;
                            }
                        }
                    }
                    else if (col.ColumnName.Contains("Timezone"))
                    {
                        destRow[col.ToString()] = "";

                        foreach (DataRow tzRow in dtTimezones.Rows)
                        {
                            if (tzRow["idTimezone"].ToString() == sourcerow[col.ToString()].ToString())
                            { 
                                destRow[col.ToString()] = tzRow["displayName"].ToString();
                                break;
                            }
                        }
                    }
                    else if (col.ColumnName.Contains("Committed Content Type"))
                    {
                        if (!String.IsNullOrWhiteSpace(sourcerow["Committed Content Type"].ToString()))
                        {
                            ReportForm.LessonContentType committedContentType = (ReportForm.LessonContentType)(Convert.ToInt32(sourcerow["Committed Content Type"]));

                            switch (committedContentType)
                            {
                                case ReportForm.LessonContentType.AdministratorOverride:
                                    destRow["Committed Content Type"] = _GlobalResources.AdministratorOverride;
                                    break;
                                case ReportForm.LessonContentType.ContentPackage:
                                    destRow["Committed Content Type"] = _GlobalResources.ContentPackage;
                                    break;
                                case ReportForm.LessonContentType.StandupTrainingModule:
                                    destRow["Committed Content Type"] = _GlobalResources.InstructorLedTrainingModule;
                                    break;
                                case ReportForm.LessonContentType.Task:
                                    destRow["Committed Content Type"] = _GlobalResources.Task;
                                    break;
                                case ReportForm.LessonContentType.OJT:
                                    destRow["Committed Content Type"] = _GlobalResources.OJT;
                                    break;
                            }
                        }
                    }
                    else if (col.ColumnName.Contains("Content Type_s"))
                    {
                        if (!String.IsNullOrWhiteSpace(sourcerow["Content Type_s"].ToString()))
                        {
                            destRow["Content Type_s"] = string.Empty;
                            for (int i = 0; i < sourcerow["Content Type_s"].ToString().Length; i++)
                            {
                                ReportForm.LessonContentType contentType = (ReportForm.LessonContentType)(Convert.ToInt32(sourcerow["Content Type_s"].ToString().Substring(i, 1)));

                                switch (contentType)
                                {
                                    case ReportForm.LessonContentType.ContentPackage:
                                        destRow["Content Type_s"] += _GlobalResources.Online;
                                        break;
                                    case ReportForm.LessonContentType.StandupTrainingModule:
                                        destRow["Content Type_s"] += ((destRow["Content Type_s"].ToString().Length > 0) ? ", " : string.Empty) + _GlobalResources.InstructorLedTraining;
                                        break;
                                    case ReportForm.LessonContentType.Task:
                                        destRow["Content Type_s"] += ((destRow["Content Type_s"].ToString().Length > 0) ? ", " : string.Empty) + _GlobalResources.Task;
                                        break;
                                    case ReportForm.LessonContentType.OJT:
                                        destRow["Content Type_s"] += ((destRow["Content Type_s"].ToString().Length > 0) ? ", " : string.Empty) + _GlobalResources.OJT;
                                        break;
                                }
                            }
                        }
                    }
                    else if (col.ColumnName.Contains("Course Status") || col.ColumnName.Contains("Learning Path Status") || col.ColumnName.Contains("Lesson Success") ||
                        col.ColumnName.Contains("Lesson Completion") || col.ColumnName.Contains("Completion Status") || col.ColumnName.Contains("Success Status") ||
                        col.ColumnName.Contains("Certification Status") || col.ColumnName.Contains("Roster Status"))
                    {
                        switch (sourcerow[col.ToString()].ToString())
                        {
                            case "unknown":
                                destRow[col.ToString()] = _GlobalResources.Unknown;
                                break;
                            case "completed":
                                destRow[col.ToString()] = _GlobalResources.Completed;
                                break;
                            case "expired":
                                destRow[col.ToString()] = _GlobalResources.Expired;
                                break;
                            case "overdue":
                                destRow[col.ToString()] = _GlobalResources.Overdue;
                                break;
                            case "future":
                                destRow[col.ToString()] = _GlobalResources.Future;
                                break;
                            case "enrolled":
                                destRow[col.ToString()] = _GlobalResources.Enrolled;
                                break;
                            case "current":
                                destRow[col.ToString()] = _GlobalResources.Current;
                                break;
                            case "incomplete":
                                destRow[col.ToString()] = _GlobalResources.Incomplete;
                                break;
                            case "passed":
                                destRow[col.ToString()] = _GlobalResources.Passed;
                                break;
                            case "failed":
                                destRow[col.ToString()] = _GlobalResources.Failed;
                                break;
                            case "waitlist":
                                destRow[col.ToString()] = _GlobalResources.WaitingList;
                                break;
                        }
                    }
                    else if (col.ColumnName.Contains("Item Type"))
                    {
                        switch (sourcerow[col.ToString()].ToString())
                        {
                            case "Course":
                                destRow[col.ToString()] = _GlobalResources.Course;
                                break;
                            case "Catalog":
                                destRow[col.ToString()] = _GlobalResources.Catalog;
                                break;
                            case "Learning Path":
                                destRow[col.ToString()] = _GlobalResources.LearningPath;
                                break;
                            case "Instructor Led Training":
                                destRow[col.ToString()] = _GlobalResources.InstructorLedTraining;
                                break;                           
                        }
                    }
                    else if (col.ColumnName.Contains("Interaction Description"))
                    {
                        destRow[col.ToString()] = returnInteractionData[0];
                    }
                    else if (col.ColumnName.Contains("Interaction Type"))
                    {
                        destRow[col.ToString()] = returnInteractionData[1];
                    }
                    else if (col.ColumnName.Contains("Learner Response"))
                    {
                        destRow[col.ToString()] = returnInteractionData[2];
                    }
                    else if (col.ColumnName.Contains("Correct Responses"))
                    {
                        destRow[col.ToString()] = correctResponses;
                    }
                    else if (col.ColumnName.Contains("Current Track"))
                    {
                        switch (sourcerow[col.ToString()].ToString())
                        {
                            case "0":
                                destRow[col.ToString()] = _GlobalResources.Initial;
                                break;
                            case "1":
                                destRow[col.ToString()] = _GlobalResources.Renewal;
                                break;                            
                        }
                    }
                    else if (col.ColumnName.Contains("Certification Status"))
                    {
                        switch (sourcerow[col.ToString()].ToString())
                        {
                            case "current":
                                destRow[col.ToString()] = _GlobalResources.Current;
                                break;
                            case "expired":
                                destRow[col.ToString()] = _GlobalResources.Expired;
                                break;
                            case "incomplete":
                                destRow[col.ToString()] = _GlobalResources.IncompleteInProgress;
                                break;
                        }
                    }
                    else if (col.ColumnName.Contains("Uploaded Task File"))
                    {
                        string filename = sourcerow[col.ToString()].ToString();

                        if (!String.IsNullOrWhiteSpace(filename))
                        {
                            int idUser = Convert.ToInt32(sourcerow["_idUser"]);
                            int idDataCertificationModuleRequirement = Convert.ToInt32(sourcerow["_idDataCertificationModuleRequirement"]);
                            
                            destRow[col.ToString()] = "<a href=\"" + SitePathConstants.SITE_USERS_ROOT + idUser.ToString() + "/CertificationTasks/" + idDataCertificationModuleRequirement.ToString() + "/" + filename + "\" target=\"_blank\">" + filename + "</a>";                            
                        }
                        else
                        { destRow[col.ToString()] = filename; }
                    }
                    else
                    {
                        destRow[col.ToString()] = sourcerow[col.ToString()];
                    }
                }

                dest.Rows.Add(destRow);
            }

            // if we have a direct path to the user account data files (because this was called from job processor), 
            // then get labels using that method, otherwise use standard method
            if (!String.IsNullOrWhiteSpace(defaultUserAccountDataPath) && !String.IsNullOrWhiteSpace(siteUserAccountDataPath))
            {
                foreach (DataColumn col in dest.Columns)
                {
                    if (col.ColumnName.Contains("##"))
                    { col.ColumnName = ds.GetLabelInLanguage(col.ColumnName, userCulture, defaultUserAccountDataPath, siteUserAccountDataPath, siteDefaultLanguageString); }
                    else if (col.ColumnName.IndexOf("_") != 0)
                    { col.ColumnName = _GlobalResources.ResourceManager.GetString(col.ColumnName.Replace(" ", "").Replace("/", "").Replace(".", "")); }
                    else
                    { }
                }
            }
            else
            {
                foreach (DataColumn col in dest.Columns)
                {
                    if (col.ColumnName.Contains("##"))
                    { col.ColumnName = ds.GetLabelInLanguage(col.ColumnName, userCulture); }
                    else if (col.ColumnName.IndexOf("_") != 0)
                    {
                        if (!String.IsNullOrWhiteSpace(_GlobalResources.ResourceManager.GetString(col.ColumnName.Replace(" ", "").Replace("/", "").Replace(".", ""))))
                        { col.ColumnName = _GlobalResources.ResourceManager.GetString(col.ColumnName.Replace(" ", "").Replace("/", "").Replace(".", "")); }
                    }
                    else
                    { }
                }
            }

            // remove columns from the destination table that need to be removed
            foreach (string columnName in columnNamesToRemoveFromDestinationTable)
            { dest.Columns.Remove(columnName); }

            // return
            return dest;
        }
        #endregion

        #region _RetrieveInteractionDataForReports
        /// <summary>
        /// Get Interaction Data from XML files
        /// </summary>
        private static string[] _RetrieveInteractionDataForReports(int userId, int dataLessonId, string scoIdentifier, string interactionIdentifier)
        {
            string folderPath = HttpContext.Current.Server.MapPath(SitePathConstants.SITE_LOG_LESSONDATA + userId + "/" + dataLessonId.ToString());
            string filePath = Path.Combine(folderPath, scoIdentifier + ".json");
            string[] returnInteractionData = new string[4];

            if (File.Exists(filePath))
            {
                string content = File.ReadAllText(filePath);
                returnInteractionData[3] = content;
                try
                {
                    //Parse the content json into json object
                    Newtonsoft.Json.Linq.JObject cmiDataModelObject = Newtonsoft.Json.Linq.JObject.Parse(content);

                    //Fetch interactions object
                    string interactions = Convert.ToString(cmiDataModelObject["interactions"]);

                    //Fetch the interaction array from interactions object
                    Newtonsoft.Json.Linq.JArray interactionArray = (Newtonsoft.Json.Linq.JArray)cmiDataModelObject["interactions"];

                    var interaction = interactionArray.Where(item => (Convert.ToString(item["id"]) == interactionIdentifier)).FirstOrDefault();
                    if (interaction != null)
                    {
                        returnInteractionData[0] = interaction["description"] != null ? interaction["description"].ToString() : string.Empty;
                        returnInteractionData[1] = interaction["type"].ToString();
                        returnInteractionData[2] = interaction["learner_response"] != null ? interaction["learner_response"].ToString() : interaction["student_response"].ToString();
                        returnInteractionData[3] = Convert.ToString(interaction["correct_responses"]);
                    }

                }
                catch (Exception ex)
                { }
            }
            return returnInteractionData;
        }
        #endregion

        #region _SaveReportHTMLFile
        /// <summary>
        /// Generate Report in html version to the warehouse 
        /// </summary>
        private void _SaveReportHTMLFile(DataTable dtReportData, string reportTitle, string fileName)
        {
            string htmlTemplate, reportCss, tableSortingJavaScript;
            StringWriter stringWriter = new StringWriter();

            //Read template file into a string
            htmlTemplate = File.ReadAllText(HttpContext.Current.Server.MapPath("ReportHTMLTemplate.html"));

            //Replace with the title
            htmlTemplate = htmlTemplate.Replace("##reportTitle##", reportTitle);

            // compile the css
            reportCss = String.Empty;

            if (AsentiaSessionState.IsThemePreviewMode)
            {
                if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + "Report.css")))
                { reportCss += File.ReadAllText(HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + "Report.css")); }

                if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + "Report.css")))
                { reportCss += File.ReadAllText(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_PREVIEW_CSS + "Report.css")); }
            }
            else
            {
                if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + "Report.css")))
                { reportCss += File.ReadAllText(HttpContext.Current.Server.MapPath(SitePathConstants.DEFAULT_SITE_TEMPLATE_THEMES_THEME_CSS + "Report.css")); }

                if (File.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + "Report.css")))
                { reportCss += File.ReadAllText(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_TEMPLATE_THEMES_THEME_CSS + "Report.css")); }
            }

            htmlTemplate = htmlTemplate.Replace("##cssStyleContent##", reportCss);

            //Replace with table sorting javascript code
            tableSortingJavaScript = File.ReadAllText(HttpContext.Current.Server.MapPath("/_scripts/TableSorting.js"));
            htmlTemplate = htmlTemplate.Replace("##tableSortingJavaScript##", tableSortingJavaScript);
            
            //Replace with the icon paths
            htmlTemplate = htmlTemplate.Replace("##icon_close_modal##", ImageFiles.GetIconPath(ImageFiles.ICON_CLOSE_MODAL, ImageFiles.EXT_PNG));
            htmlTemplate = htmlTemplate.Replace("##icon_lesson##", ImageFiles.GetIconPath(ImageFiles.ICON_LESSON, ImageFiles.EXT_PNG));
            htmlTemplate = htmlTemplate.Replace("##icon_loading##", ImageFiles.GetIconPath(ImageFiles.ICON_LOADING, ImageFiles.EXT_GIF));

            stringWriter.Write(htmlTemplate);
            HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Body);

            //Create file name and specify file location.
            try
            {
                string fullFilePath = SitePathConstants.SITE_WAREHOUSE_REPORTDATA + fileName + ".html";

                if (!Directory.Exists(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_WAREHOUSE_REPORTDATA)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(SitePathConstants.SITE_WAREHOUSE_REPORTDATA));
                }

                Panel htmlReportPanel = new Panel();

                HyperLink printLink = new HyperLink();
                printLink.Text = "Print";
                printLink.CssClass = "print";
                printLink.NavigateUrl = "javascript:self.print();";

                LinkButton printImageButton = new LinkButton();
                System.Web.UI.WebControls.Image printImage = new System.Web.UI.WebControls.Image();
                printImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_PRINT,
                                                             ImageFiles.EXT_PNG);
                printImage.CssClass = "MediumIcon print";
                printImageButton.Controls.Add(printImage);
                printImageButton.OnClientClick = "javascript:self.print();";

                //Report Title
                HtmlGenericControl titleWrapper = new HtmlGenericControl("p");
                titleWrapper.Attributes.Add("class", "PageTitleContainer");
                Label titleText = new Label();
                titleText.CssClass = "PageTitleText";
                System.Web.UI.WebControls.Image titleImage = new System.Web.UI.WebControls.Image();
                titleImage.ImageUrl = ImageFiles.GetIconPath(ImageFiles.ICON_REPORT,
                                                             ImageFiles.EXT_PNG);
                titleImage.CssClass = "PageTitleIconContainer";
                titleImage.AlternateText = reportTitle;
                titleWrapper.Controls.Add(titleImage);
                titleText.Text = "&nbsp;" + reportTitle;

                titleWrapper.Controls.Add(titleText);
                titleWrapper.Controls.Add(printImageButton);
                titleWrapper.Controls.Add(printLink);
                htmlReportPanel.Controls.Add(titleWrapper);

                Table reporTable = new Table();
                TableHeaderRow rowColumnHeader = new TableHeaderRow();
                rowColumnHeader.CssClass = "GridHeaderRow";
                reporTable.ID = "ReportHTMLVersion";

                int colNum = 0;
                //Report Column Header
                foreach (DataColumn colName in dtReportData.Columns)
                {
                    string dataType = "T";

                    switch (colName.DataType.ToString())
                    {
                        case "System.String":
                            dataType = "T";
                            break;
                        case "System.Double":
                        case "System.Integer":
                        case "System.Int32":
                            dataType = "N";
                            break;
                        case "System.DateTime":
                            dataType = "D";
                            break;
                    }

                    if (colName.ColumnName != "Data Lesson ID" && colName.ColumnName != "SCO Identifier")
                    {
                        TableHeaderCell cellColumnHeader = new TableHeaderCell();
                        cellColumnHeader.CssClass = "th";
                        HyperLink localizeColumnName = new HyperLink();
                        localizeColumnName.Text = colName.ColumnName;
                        localizeColumnName.NavigateUrl = "javascript: TableIDvalue = 'ReportHTMLVersion';  SortTable(" + colNum + ",'" + dataType + "');";
                        cellColumnHeader.Controls.Add(localizeColumnName);
                        rowColumnHeader.Controls.Add(cellColumnHeader);
                        colNum = colNum + 1;
                    }
                }

                reporTable.Controls.Add(rowColumnHeader);
                reporTable.Rows[0].TableSection = TableRowSection.TableHeader;

                int i = 0;
                //Report Data Grid
                foreach (DataRow row in dtReportData.Rows)
                {                    
                    // begin building table row html
                    i = i + 1;
                    TableRow rowReportData = new TableRow();
                    rowReportData.CssClass = (i % 2 == 0) ? "GridDataRow GridDataRowAlternate" : "GridDataRow";
                    rowReportData.TableSection = TableRowSection.TableBody;

                    foreach (DataColumn col in row.Table.Columns)
                    {                        
                        // include data if the column doesn't begin with _
                        if (col.ColumnName.IndexOf("_") != 0)
                        {
                            TableCell cellReportData = new TableCell();
                            cellReportData.ID = col.ColumnName;

                            Label labelData = new Label();

                            if (col.ColumnName.Contains("Email"))
                            {
                                labelData.Text = "<a href=\"mailto:" + row[col].ToString() + "\">" + row[col].ToString() + "</a>";
                            }
                            else if (col.ColumnName.Contains("Uploaded Task File"))
                            {
                                labelData.Text = row[col].ToString();
                            }
                            else if (col.ExtendedProperties["DateFormat"].ToString() == "NoTime" && !String.IsNullOrEmpty(row[col].ToString()))
                            {
                                labelData.Text = Convert.ToDateTime(row[col]).ToShortDateString();
                            }
                            else
                            {
                                labelData.Text = HttpUtility.HtmlEncode(row[col].ToString());
                            }

                            labelData.Text = labelData.Text;
                            cellReportData.Controls.Add(labelData);
                            rowReportData.Controls.Add(cellReportData);
                        }
                    }

                    reporTable.Controls.Add(rowReportData);
                }

                reporTable.CssClass = ((i > _REPORT_HTML_ROWS_MAX) || (colNum > _REPORT_HTML_COLUMNS_MAX)) ? "GridTable" : "GridTable sortable";

                Panel pnlReportDataTable = new Panel();
                pnlReportDataTable.Controls.Add(reporTable);
                htmlReportPanel.Controls.Add(pnlReportDataTable);
                
                htmlReportPanel.RenderControl(htmlWriter);
                htmlWriter.RenderEndTag();

                //stringWriter.Write("");

                // write the new file to HTML
                using (StreamWriter sw = new StreamWriter(HttpContext.Current.Server.MapPath(fullFilePath)))
                {
                    sw.Write(stringWriter.ToString());
                    sw.Close();
                }

            }
            catch
            {
                // bubble the exception up
                throw;
            }
            finally
            {
                htmlWriter.Close();
                stringWriter.Close();
            }
        }
        #endregion

        #region _SaveReportCSVFile
        /// <summary>
        /// Generate Report in Excel version to the warehouse 
        /// </summary>
        private void _SaveReportCSVFile(DataTable dtReportData, string reportTitle, string fileName)
        {
            //Create file name and specify file location.
            try
            {
                string fullFilePath = SitePathConstants.SITE_WAREHOUSE_REPORTDATA + fileName + ".csv";

                using (StreamWriter writer = new StreamWriter(HttpContext.Current.Server.MapPath(fullFilePath), false, Encoding.UTF8))
                {
                    List<string> headerValues = new List<string>();
                    foreach (DataColumn column in dtReportData.Columns)
                    {
                        headerValues.Add(column.ColumnName);
                    }
                    writer.WriteLine(String.Join(",", headerValues.ToArray()));

                    string[] items = null;
                    foreach (DataRow row in dtReportData.Rows)
                    {
                        items = row.ItemArray.Select(o => "\"" + o.ToString().Replace("\"", "\"\"") + "\"").ToArray();
                        writer.WriteLine(String.Join(",", items));
                    }
                    writer.Flush();
                }
            }
            catch
            {
                // bubble the exception up
                throw;
            }
            finally
            {

            }
        }
        #endregion

        #region _SaveReportPDFFile
        /// <summary>
        /// Generate Report in pdf version to the warehouse 
        /// </summary>
        private void _SaveReportPDFFile(DataTable dtReportData, string reportTitle, string fileName)
        {
            //Create file name and specify file location.
            try
            {
                string fullFilePath = SitePathConstants.SITE_WAREHOUSE_REPORTDATA + fileName + ".pdf";

                PdfGenerator pdfGenerator = new PdfGenerator();
                ReportDataSet ds = new ReportDataSet();

                pdfGenerator.PDFform(dtReportData, reportTitle, HttpContext.Current.Server.MapPath(fullFilePath));
            }
            catch
            {
                // bubble the exception up
                throw;
            }
            finally
            {

            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a report into a collection.
        /// </summary>
        /// <param name="idGroup">report id</param>
        private void _GetPropertiesInLanguages(int idReport)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReport", idReport, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Report.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["title"].ToString()
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
