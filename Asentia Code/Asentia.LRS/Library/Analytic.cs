﻿using Asentia.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Asentia.LRS.Library
{
    public class Analytic
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public Analytic()
        {
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idCourse">Course Id</param>
        public Analytic(int idAnalytic)
        {
            this._Details(idAnalytic);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Analytic Id.
        /// </summary>
        public int IdAnalytic;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Dataset Id.
        /// </summary>
        public int IdDataset;

        /// <summary>
        /// User Id.
        /// </summary>
        public int IdUser;

        /// <summary>
        /// Title.
        /// </summary>
        public string Title;

        /// <summary>
        /// id Chart Type .
        /// </summary>
        public int IdChartType;

        /// <summary>
        /// Frequency Text
        /// </summary>
        public string FrequencyText;

        /// <summary>
        /// Frequency
        /// </summary>
        public int Frequency;

        /// <summary>
        /// Is Public.
        /// </summary>
        public bool IsPublic;

        /// <summary>
        /// User or Group
        /// </summary>
        public string OrderColumn;

        /// <summary>
        /// order value
        /// </summary>
        public string OrderOfRecords;


        /// <summary>
        /// courses in string (seperated by ,)
        /// </summary>
        public string Courses;

        /// <summary>
        /// Date Created.
        /// </summary>
        public DateTime? DtCreated;

        /// <summary>
        /// Date Modified.
        /// </summary>
        public DateTime? DtModified;

        /// <summary>
        /// Is Deleted.
        /// </summary>
        public bool IsDeleted;

        /// <summary>
        /// Date Deleted.
        /// </summary>
        public DateTime? DtDeleted;

        /// <summary>
        /// day to exclude
        /// </summary>
        public string ExcludeDays;

        /// <summary>
        /// certificate title
        /// </summary>
        public string Certificates;

        /// <summary>
        /// Start Date
        /// </summary>
        public DateTime? DtStart;

        /// <summary>
        /// End Date
        /// </summary>
        public DateTime? DtEnd;

        /// <summary>
        /// Activity id (used for course and lesson snapshot analytic)
        /// it is activity select list id in int form
        /// </summary>
        public int Activity;
        #endregion

        #region Methods
        #region Save
        /// <summary>
        /// Saves Analytic data to the database as the current session user.
        /// </summary>
        public int Save()
        {
            return this._Save(AsentiaSessionState.IdSiteUser);
        }
        #endregion

        #region Run
        /// <summary>
        /// Run Analytic and have them been generated and saved in the warehouse/Analyticdata folder
        /// </summary>
        public DataSet Run()
        {
            return this._Run(AsentiaSessionState.IdSiteUser);
        }
        #endregion
        #endregion

        #region Public Methods

        #region GetTitles
        /// <summary>
        /// Gets a list of Analytic ids and titles, dataset name with or without specific dataSet or user
        /// </summary>
        /// <returns>DataTable of Analytic titles</returns>
        public DataTable GetTitles(int idDataSet, int idUser, int isPublicRequired)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idDataset", idDataSet, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@idUser", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@isPublicRequired", isPublicRequired, SqlDbType.Bit, 1, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Analytic.GetTitles]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes a Analytic.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="deletees"></param>
        public static void Delete(DataTable deletees)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@Analytics", deletees, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Analytic.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetCourses
        /// <summary>
        /// Gets a listing of courses ids and names that belong to this analytic.
        /// </summary>
        /// <returns>DataTable of courses ids and names that belong to this analytic</returns>
        public DataTable GetCourses(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idAnalytic", this.IdAnalytic, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Analytic.GetCourses]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region SaveCourses
        /// <summary>
        /// Saves Courses for this Learning Path.
        /// </summary>
        /// <param name="courses">DataTable of courses to save</param>
        public void SaveCourses(DataTable courses)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idAnalytic", this.IdAnalytic, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@Courses", courses, SqlDbType.Structured, null, ParameterDirection.Input);

            // execute the procedure and populate the properties
            try
            {
                databaseObject.ExecuteNonQuery("[Analytic.SaveCourses]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion

        #region Private Methods

        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idCourse">Analytic Id</param>
        private void _Details(int idAnalytic)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idAnalytic", idAnalytic, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idUser", this.IdUser, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idDataset", this.IdDataset, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@idChartType", this.IdChartType, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@frequency", this.Frequency, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@excludeDays", this.ExcludeDays, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtEnd", this.DtEnd, SqlDbType.DateTime, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@isPublic", this.IsPublic, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtCreated", this.DtCreated, SqlDbType.DateTime, null, ParameterDirection.Output); // confirm null part
            databaseObject.AddParameter("@dtModified", this.DtModified, SqlDbType.DateTime, null, ParameterDirection.Output);
            databaseObject.AddParameter("@isDeleted", this.IsDeleted, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtDeleted", null, SqlDbType.DateTime, 8, ParameterDirection.Output);
            databaseObject.AddParameter("@activity", this.Activity, SqlDbType.Int, 4, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Analytic.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdAnalytic = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idAnalytic"].Value);
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdUser = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idUser"].Value);
                this.IdDataset = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idDataset"].Value);
                this.Title = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@title"].Value);
                this.IdChartType = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idChartType"].Value);
                this.Frequency = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@frequency"].Value);
                this.ExcludeDays = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@excludeDays"].Value);
                this.IsPublic = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isPublic"].Value);
                this.DtStart = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtStart"].Value);
                this.DtEnd = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtEnd"].Value);
                this.DtCreated = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtCreated"].Value);
                this.DtModified = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtModified"].Value);
                this.IsDeleted = AsentiaDatabase.ParseDbParamBool(databaseObject.Command.Parameters["@isDeleted"].Value);
                this.DtDeleted = AsentiaDatabase.ParseDbParamNullableDateTime(databaseObject.Command.Parameters["@dtDeleted"].Value);
                this.Activity = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@activity"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Save
        /// <summary>
        /// Saves group data to the database.
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the Analytic</returns>
        private int _Save(int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idAnalytic", this.IdAnalytic, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idUser", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataset", this.IdDataset, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@title", this.Title, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@idChartType", this.IdChartType, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@frequency", this.Frequency, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@excludeDays", this.ExcludeDays, SqlDbType.NVarChar, 255, ParameterDirection.Input);
            databaseObject.AddParameter("@dtStart", this.DtStart, SqlDbType.DateTime, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtEnd", this.DtEnd, SqlDbType.DateTime, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@isPublic", this.IsPublic, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@activity", this.Activity, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Analytic.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdAnalytic = Convert.ToInt32(databaseObject.Command.Parameters["@idAnalytic"].Value);
                return this.IdAnalytic;

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _Run
        /// <summary>
        /// Run the Analytic and store Analytics into specific folder
        /// </summary>
        /// <param name="idCaller">the calling user's id</param>
        /// <returns>the id of the Analytic</returns>
        private DataSet _Run(int idCaller)
        {
            DataSet dsAnalyticData = new DataSet();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@activity", this.Activity, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@frequency", this.Frequency, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@groupByClause", this._GetGroupbyClause(this.Frequency), SqlDbType.NVarChar, 255, ParameterDirection.Input);
            String dateFilterQuery = null;
            if (this.DtStart != null && this.DtEnd != null)
            {
                dateFilterQuery = "XXX" + " BETWEEN '" + Convert.ToDateTime(this.DtStart).ToString("yyyy-MM-dd") + "' AND '" + Convert.ToDateTime(this.DtEnd).ToString("yyyy-MM-dd") + "' ";
            }
            databaseObject.AddParameter("@dateFilter", dateFilterQuery, SqlDbType.NVarChar, 255, ParameterDirection.Input);

            //where clause
            String WhereClause = null;
            String orderClause = null;
            if (this.ExcludeDays != null)
            {
                WhereClause = " NOT IN (" + this.ExcludeDays + ")";
            }
            if ((EnumDataSetAnalytic)this.IdDataset == EnumDataSetAnalytic.CourseCompletions)
            {//if courses are selected
                if (!String.IsNullOrWhiteSpace(this.Courses))
                {

                    WhereClause = WhereClause + " AND CCC IN (" + this.Courses + ")";
                    orderClause = WhereClause;
                }
                else
                {
                    orderClause = null;
                }
            }

            if ((EnumDataSetAnalytic)this.IdDataset == EnumDataSetAnalytic.CertificatesAnalytics )
            {//if certificates are selected
                if (!String.IsNullOrWhiteSpace(this.Certificates))
                {

                    WhereClause = WhereClause + " AND CCC IN (" + this.Certificates + ")";
                    orderClause = WhereClause;
                }
                else
                {
                    orderClause = null;
                }
            }

            if ((EnumDataSetAnalytic)this.IdDataset == (EnumDataSetAnalytic.UserGroupSnapshot))
            {
                if (this.Activity > 0 && this.Activity == 2)
                {
                    orderClause = "ORDER BY " + this.OrderColumn + " " + this.OrderOfRecords;
                }
            }

            databaseObject.AddParameter("@orderClause", orderClause, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@whereClause", WhereClause, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            try
            {
                dsAnalyticData = databaseObject.ExecuteDataSet("[Dataset." + Enum.GetName(typeof(EnumDataSetAnalytic), this.IdDataset) + "]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
            return dsAnalyticData;
        }
        #endregion

        #region _GetGroupbyClause
        /// <summary>
        /// this will return the group by clause teaxt on the basis of int frequency
        /// </summary>
        /// <param name="frequency"></param>
        /// <returns></returns>
        private string _GetGroupbyClause(int frequency)
        {
            string text = " ";
            switch ((EnumFrequencyAnalytic)frequency)
            {
                case EnumFrequencyAnalytic.Day:
                    text = " DATEPART(DW , " + "XXX" + ") ";
                    break;
                case EnumFrequencyAnalytic.Week:
                    text = " DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, " + "XXX" + ") , 0), " + "XXX" + ") +1 ";
                    break;
                case EnumFrequencyAnalytic.Month:
                    text = " DATEPART(MONTH , " + "XXX" + ") ";
                    break;
                case EnumFrequencyAnalytic.Hour:
                    text = " DATEPART(HOUR , " + "XXX" + ") ";
                    break;
                case EnumFrequencyAnalytic.Year:
                    text = " DATEPART(YEAR , " + "XXX" + ") ";
                    break;
                default:
                    text = null;
                    break;

            }
            return text;

        }
        #endregion
        #endregion

        #region Static Methods
        #region IdsAndNamesForAnalyticSelectList
        /// <summary>
        /// Gets a listing of course ids and titles to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of course ids and titles.</returns>
        public static DataTable IdsAndNamesForAnalyticSelectList(int idAnalytic, string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idAnalytic", idAnalytic, SqlDbType.Int, 4, ParameterDirection.Input);

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Course.IdsAndNamesForAnalyticSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region IdsAndNamesForAnalyticSelectList
        /// <summary>
        /// Gets a listing of certificate ids and names to populate into a select list.
        /// </summary>
        /// <param name="idCourse">course id</param>
        /// <param name="searchParam">serach parameter</param>
        /// <returns>DataTable of certificate ids and names.</returns>
        public static DataTable CertificateIdsAndNamesForAnalyticSelectList(string searchParam)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);                

                // SEARCH PARAMETER
                // build the full text query
                FullTextSearchQuery ftsQuery = new FullTextSearchQuery();
                string fullTextSearchQuery = ftsQuery.ToFtsQuery(searchParam);

                // if it's not empty, use it; else make it a wildcard
                SqlString formattedSearchQuery = (fullTextSearchQuery != String.Empty) ? new SqlString(fullTextSearchQuery) : new SqlString("*");

                // apply the query parameter
                databaseObject.AddParameter("@searchParam", formattedSearchQuery, SqlDbType.NVarChar, 4000, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[Certificate.IdsAndNamesForAnalyticSelectList]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return dt;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion

        #region Day
        // Summary:
        //     Specifies the day of the week.
        public enum DayEnum
        {
            // Summary:
            //     Indicates Sunday.
            Sunday = 1,
            //
            // Summary:
            //     Indicates Monday.
            Monday = 2,
            //
            // Summary:
            //     Indicates Tuesday.
            Tuesday = 3,
            //
            // Summary:
            //     Indicates Wednesday.
            Wednesday = 4,
            //
            // Summary:
            //     Indicates Thursday.
            Thursday = 5,
            //
            // Summary:
            //     Indicates Friday.
            Friday = 6,
            //
            // Summary:
            //     Indicates Saturday.
            Saturday = 7,
        }
        #endregion

        #region Month
        public enum MonthEnum
        {
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12

        }

        #endregion

        #region Frequency
        public enum FrequencyEnum
        {
            Hour = 1,
            Day = 2,
            Week = 3,
            Month = 4,
            Year = 5
        }
        #endregion

    }
}

