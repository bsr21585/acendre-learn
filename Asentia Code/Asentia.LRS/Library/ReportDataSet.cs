﻿using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.ComponentModel;
using Asentia.Common;
using Asentia.UMS.Library;

namespace Asentia.LRS.Library
{
    #region Enumerations
    #region DataSet Enum
    /// <summary>
    /// DataSet for existing dataset stored procedure
    /// When create a new dataset, manually add it to the bottom of the list
    /// the name of the enum should be consistent with stored procedure name
    /// in order to dynamically coded in the application
    /// </summary>
    public enum EnumDataSet
    {
        /// <summary>
        /// Dataset User Demographics
        /// </summary>
        [Description("User Demographics")]
        UserDemographics = 1,

        /// <summary>
        /// Dataset User Course Transcript
        /// </summary>
        [Description("User Course Transcripts")]
        UserCourseTranscripts = 2,

        /// <summary>
        /// Dataset Catalog and Course Information
        /// </summary>
        [Description("Catalog and Course Information")]
        CatalogCourseInformation = 3,

        /// <summary>
        /// Dataset Certificates
        /// </summary>
        [Description("Certificates")]
        Certificates = 4,

        /// <summary>
        /// Dataset xAPI
        /// </summary>
        [Description("xAPI")]
        TinCan = 5,

        /// <summary>
        /// Dataset User Learning Path Transcript
        /// </summary>
        [Description("User Learning Path Transcripts")]
        UserLearningPathTranscripts = 6,

        /// <summary>
        /// Dataset User Instructor Led Transcript
        /// </summary>
        [Description("User Instructor Led Transcripts")]
        UserInstructorLedTranscripts = 7,

        /// <summary>
        /// Dataset Purchases
        /// </summary>
        [Description("Purchases")]
        Purchases = 8,

        /// <summary>
        /// Dataset User Certification Transcripts
        /// </summary>
        [Description("User Certification Transcripts")]
        UserCertificationTranscripts = 9,
    }
    #endregion
    #endregion

    public class ReportDataSet
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ReportDataSet()
        { 
            this.LanguageSpecificProperties = new ArrayList();
        }

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <param name="idCourse">Course Id</param>
        public ReportDataSet(int idReportDataSet)
        {
            this.LanguageSpecificProperties = new ArrayList();

            this._Details(idReportDataSet);
            this._GetPropertiesInLanguages(idReportDataSet);
        }
        #endregion

        #region Properties
        // DATASET PROPERTIES

        /// <summary>
        /// ReportDataSet Id.
        /// </summary>
        public int Id = 0;

        /// <summary>
        /// Site Id.
        /// </summary>
        /// <seealso cref="Site" />
        public int IdSite;

        /// <summary>
        /// Name
        /// </summary>
        public string Name;

        /// <summary>
        /// Stored Procedure Name.
        /// </summary>
        public string StoredProcedureName;

        /// <summary>
        /// HtmlTemplatePath
        /// </summary>        
        public string HtmlTemplatePath;

        /// <summary>
        /// Description
        /// </summary>
        public string Description;

        /// <summary>
        /// Order.
        /// </summary>
        public int? Order;

        /// <summary>
        /// ArrayList of language specific properties.
        /// </summary>
        public ArrayList LanguageSpecificProperties;


        // PROPERTIES RELATED TO DATASET COLUMNS

        /// <summary>
        /// User Account Data object.
        /// </summary>
        public UserAccountData UserAccountDataObj;

        /// <summary>
        /// Language String.
        /// </summary>
        public string LanguageString;

        /// <summary>
        /// DataSet Column XML file
        /// </summary>
        public string DatasetColumnsDataFile;        
        #endregion

        #region Classes
        #region LanguageSpecificProperty
        /// <summary>
        /// Class that represents language specific properties.
        /// </summary>
        public class LanguageSpecificProperty
        {
            public string LangString;
            public string Name;
            public string Description;            

            /// <summary>
            /// parameterless constructor for serialization purpose
            /// </summary>
            public LanguageSpecificProperty()
            { ;}

            public LanguageSpecificProperty(string langString, string name, string description)
            {
                this.LangString = langString;
                this.Name = name;
                this.Description = description;                
            }
        }
        #endregion

        #region DatasetColumnProperties
        /// <summary>
        /// Language specific column properties of a dataset.
        /// </summary>
        public class DatasetColumnProperties
        {
            public string Identifier;
            public string Label;
            public bool IsVisible;
            public string DataType;
            public string OptionGroup;
        }
        #endregion
        #endregion

        #region Methods
        #region GetReportDataSetListTable
        /// <summary>
        /// Retrieves name and stored procedure name of available datasets and put it into a datatable.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <returns>Returns a DataTable.</returns>
        /// 
        public DataTable GetReportDataSetListTable()
        {
            DataTable dataSetList = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader dataSetReader = databaseObject.ExecuteDataReader("[Dataset.ListDataSets]", true);
                dataSetList.Load(dataSetReader);
                dataSetReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();

                if (returnCode == DBReturnValue.DetailsNotFound)
                { throw new DatabaseDetailsNotFoundException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode)); }

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return dataSetList;
        }
        #endregion

        #region GetDataSetColumns
        /// <summary>
        /// Columns label from dataset
        /// </summary>       

        public List<DatasetColumnProperties> GetDataSetColumns()
        {
            List<DatasetColumnProperties> datasetColumns = new List<DatasetColumnProperties>();
            DatasetColumnProperties dsColumn;
            UserAccountData.InputType? inputType = null;

            //Read XML document
            XmlDocument document = new XmlDocument();

            if (File.Exists(HttpContext.Current.Server.MapPath(this.DatasetColumnsDataFile)))
            {
                document.Load(HttpContext.Current.Server.MapPath(this.DatasetColumnsDataFile));
            }

            XmlNodeList nodeListDatasetColumn = document.SelectNodes("columns/column");

            List<UserFieldGlobalRules> globalUserFieldRules = UserFieldGlobalRules.GetAllRules();


            //Read all the column information and add to the List

            foreach (XmlNode node in nodeListDatasetColumn)
            {
                dsColumn = new DatasetColumnProperties();
                dsColumn.Identifier = node.Attributes["identifier"].Value;
                dsColumn.DataType = null;

                foreach (UserFieldGlobalRules ruleSet in globalUserFieldRules)
                {
                    if (ruleSet.IsTypeable && ruleSet.Identifier == dsColumn.Identifier.Replace("#", ""))
                    {
                        UserAccountData uad = new UserAccountData(UserAccountDataFileType.Site, true, false);

                        foreach (UserAccountData.Tab tab in uad.Tabs)
                        {
                            foreach (UserAccountData.UserField userField in tab.UserFields)
                            {
                                if (userField.Identifier == dsColumn.Identifier.Replace("#", ""))
                                {
                                    inputType = userField.InputType;
                                }
                            }
                        }

                        switch (inputType)
                        {
                            case (UserAccountData.InputType.TextField):
                                dsColumn.DataType = "String";
                                break;
                            case (UserAccountData.InputType.Date):
                                dsColumn.DataType = "DateTime";
                                break;
                            default:
                                dsColumn.DataType = "String";
                                break;
                        }
                    }
                }
                if (dsColumn.DataType == null)
                {
                    dsColumn.DataType = node.Attributes["dataType"].Value;
                }

                dsColumn.OptionGroup = node.Attributes["optionGroup"].Value;

                if (dsColumn.Identifier.Contains("##"))
                    _GetLabelInLanguage(dsColumn);
                else
                {
                    // the Label of static columns
                    dsColumn.Label = _GlobalResources.ResourceManager.GetString(dsColumn.Identifier.Replace(" ", "").Replace("/", ""));
                    dsColumn.IsVisible = !dsColumn.Identifier.Trim().StartsWith("_");
                }
                datasetColumns.Add(dsColumn);
            }

            return datasetColumns;
        }

        #endregion

        #region GetLabelInLanguage
        /// <summary>
        /// public method GetLabelInLanguage
        /// Get Label in Language for ## customized field
        /// </summary>
        /// 
        public string GetLabelInLanguage(string originalLabel, string langstr)
        {
            bool matched = false;
            string newID = originalLabel.Replace("#", "");

            this.UserAccountDataObj = new UserAccountData(UserAccountDataFileType.Site, true, false);
            List<UserAccountData.UserFieldForLabel> labelPool = this.UserAccountDataObj.GetAdminViewableFieldLabelsInLanguage(langstr);

            /// hardcoding for exceptions: column name different from user field
            switch (newID)
            {
                case "timezone":
                    newID = "idTimezone";
                    break;
                case "language":
                    newID = "idLanguage";
                    break;
            }

            foreach (UserAccountData.UserFieldForLabel li in labelPool)
            {
                if (li.Identifier == newID)
                {
                    newID = li.Label;
                    matched = true;
                }
            }
            
            return matched ? newID : originalLabel;
        }

        /// <summary>
        /// public method GetLabelInLanguage
        /// Get Label in Language for ## customized field
        /// Overloaded for direct path to user account data files needed when calling from JobProcessor. 
        /// </summary>
        /// 
        public string GetLabelInLanguage(string originalLabel, string langstr, string defaultDataFilePath, string siteDataFilePath, string siteDefaultLanguageString)
        {
            bool matched = false;
            string newID = originalLabel.Replace("#", "");

            this.UserAccountDataObj = new UserAccountData(UserAccountDataFileType.Site, true, false, defaultDataFilePath, siteDataFilePath, siteDefaultLanguageString);
            List<UserAccountData.UserFieldForLabel> labelPool = this.UserAccountDataObj.GetAdminViewableFieldLabelsInLanguage(langstr);

            /// hardcoding for exceptions: column name different from user field
            switch (newID)
            {
                case "timezone":
                    newID = "idTimezone";
                    break;
                case "language":
                    newID = "idLanguage";
                    break;
            }

            foreach (UserAccountData.UserFieldForLabel li in labelPool)
            {
                if (li.Identifier == newID)
                {
                    newID = li.Label;
                    matched = true;
                }
            }

            return matched ? newID : originalLabel;
        }
        #endregion
        #endregion

        #region Private Methods
        #region _Details
        /// <summary>
        /// Populates this object's properties with the specified report dataset's properties.
        /// </summary>
        /// <param name="idReportDataSet">ReportDataSet Id</param>
        private void _Details(int idReportDataSet)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReportDataSet", idReportDataSet, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@idSite", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@name", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@storedProcedureName", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@htmlTemplatePath", null, SqlDbType.NVarChar, 255, ParameterDirection.Output);
            databaseObject.AddParameter("@description", null, SqlDbType.NVarChar, 512, ParameterDirection.Output);            
            databaseObject.AddParameter("@order", null, SqlDbType.Int, 4, ParameterDirection.Output);            

            try
            {
                databaseObject.ExecuteNonQuery("[ReportDataSet.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.Id = idReportDataSet;
                this.IdSite = AsentiaDatabase.ParseDbParamInt(databaseObject.Command.Parameters["@idSite"].Value);
                this.Name = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@name"].Value);
                this.StoredProcedureName = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@storedProcedureName"].Value);
                this.HtmlTemplatePath = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@htmlTemplatePath"].Value);
                this.Description = AsentiaDatabase.ParseDbParamString(databaseObject.Command.Parameters["@description"].Value);                
                this.Order = AsentiaDatabase.ParseDbParamNullableInt(databaseObject.Command.Parameters["@order"].Value);                
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetPropertiesInLanguages
        /// <summary>
        /// Loads the language specific properties for a report dataset into a collection.
        /// </summary>
        /// <param name="idReportDataSet">ReportDataSet Id</param>
        private void _GetPropertiesInLanguages(int idReportDataSet)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idReportDataSet", idReportDataSet, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader sdr = databaseObject.ExecuteDataReader("[ReportDataSet.GetPropertiesInLanguages]", true);

                // loop through the returned recordset, instansiate a LanguageSpecificProperty object
                // and add it to the LanguageSpecificProperties ArrayList
                while (sdr.Read())
                {
                    this.LanguageSpecificProperties.Add(
                        new LanguageSpecificProperty(
                            sdr["langString"].ToString(),
                            sdr["name"].ToString(),
                            sdr["description"].ToString()                            
                            )
                    );
                }

                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region _GetLabelInLanguage
        /// <summary>
        /// private method _GetLabelInLanguage
        /// Get Label in Language for ## customized field
        /// </summary>
        /// 
        private void _GetLabelInLanguage(DatasetColumnProperties dsColumn)
        {
            bool matched = false;
            string newID = dsColumn.Identifier.Replace("#", "");

            this.UserAccountDataObj = new UserAccountData(UserAccountDataFileType.Site, true, false);
            List<UserAccountData.UserFieldForLabel> labelPool = this.UserAccountDataObj.GetAdminViewableFieldLabelsInLanguage(this.LanguageString);

            /// hardcoding for exceptions: column name different from user field
            switch (newID)
            {
                case "timezone":
                    newID = "idTimezone";
                    break;
                case "language":
                    newID = "idLanguage";
                    break;
            }

            foreach (UserAccountData.UserFieldForLabel li in labelPool)
            {
                
                if (li.Identifier == dsColumn.Identifier.Replace("#",""))
                {
                    dsColumn.Label = li.Label;
                    matched = true;
                }
            }

            //hide those fields not matched to the admin view of user fields
            dsColumn.IsVisible = matched;
        }
        #endregion
        #endregion
    }
}
