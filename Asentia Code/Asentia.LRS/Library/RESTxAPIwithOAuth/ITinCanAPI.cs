﻿using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Asentia.LRS.Library.xAPI;
using System;

namespace Asentia.LRS.Library.RESTxAPIwithOAuth
{
    [ServiceContract]
    [ServiceKnownType(typeof(Statement))]
    [ServiceKnownType(typeof(StatementResult))]
    [ServiceKnownType(typeof(List<String>))]
    public partial interface ITinCanAPI
    {
        #region Statement API
        [OperationContract]
        [WebInvoke(UriTemplate = "/statements", Method = "OPTIONS", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void GetStatementAPIOptions();

        [OperationContract]
        [WebInvoke(UriTemplate = "/statements", Method = "PUT", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String SaveStatement(Stream jsonStream);

        [OperationContract]
        [WebInvoke(UriTemplate = "/statements", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        dynamic SaveStatements(Stream jsonStream);

        [OperationContract]
        [WebInvoke(UriTemplate = "/statements", Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        Stream RetrieveStatements();
        #endregion

        #region State API
        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/state", Method = "OPTIONS", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void GetStateAPIOptions();

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/state", Method = "PUT", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String SaveStateDocument(Stream stream);

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/state", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String PostStateDocument(Stream stream);

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/state", Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        Stream RetrieveStateDocument();

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/state?method=GET", Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        Stream RetrieveStateDocumentForStoryLinePckages(Stream stream);

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/state", Method = "DELETE", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String DeleteStateDocument();
        #endregion

        #region Activity Profile API
        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/profile", Method = "OPTIONS", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void GetActivityProfileAPIOptions();

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities", Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        Stream GetActivity();

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/profile", Method = "PUT", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String SaveActivityProfileDocument(Stream stream);

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/profile", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String PostActivityProfileDocument(Stream stream);

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/profile", Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        Stream RetrieveActivityProfileDocument();

        [OperationContract]
        [WebInvoke(UriTemplate = "/activities/profile", Method = "DELETE", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String DeleteActivityProfileDocument();
        #endregion

        #region Agent Profile API
        [OperationContract]
        [WebInvoke(UriTemplate = "/agents/profile", Method = "OPTIONS", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        void GetAgentProfileAPIOptions();

        [OperationContract]
        [WebInvoke(UriTemplate = "/agents/profile", Method = "PUT", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String SaveAgentProfileDocument(Stream stream);

        [OperationContract]
        [WebInvoke(UriTemplate = "/agents/profile", Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String PostAgentProfileDocument(Stream stream);

        [OperationContract]
        [WebInvoke(UriTemplate = "/agents/profile", Method = "GET", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        Stream RetrieveAgentProfileDocument();

        [OperationContract]
        [WebInvoke(UriTemplate = "/agents/profile", Method = "DELETE", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        String DeleteAgentProfileDocument();
        #endregion

        #region About
        [OperationContract]
        [WebGet(UriTemplate = "/about", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json)]
        About GetLRSInfo();
        #endregion
    }
}
