﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Asentia.LRS.Library.OAuth.Provider;
using System.ServiceModel.Channels;
using Asentia.LRS.Library.OAuth.Framework;
using System.Net;
using System.IO;
using Microsoft.ServiceModel.Web;
using System.Xml.Linq;
using System.Web;

namespace Asentia.LRS.Library.RESTxAPIwithOAuth
{
    public class OAuthInterceptor : RequestInterceptor
    {
        IOAuthProvider _provider;

        public OAuthInterceptor(IOAuthProvider provider)
            : base(false)
        {
            _provider = provider;
        }

        public override void ProcessRequest(ref RequestContext requestContext)
        {
            if (requestContext == null || requestContext.RequestMessage == null)
            {
                return;
            }

            Message request = requestContext.RequestMessage;
            HttpRequestMessageProperty requestProperty = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];

            //Addition
            var newRequest = (HttpRequestMessageProperty)requestProperty;
            string authorization = newRequest.Headers[HttpRequestHeader.Authorization];

            OAuthContext context;
            if (authorization != null)
                context = (OAuthContext)new OAuthContextBuilder().CustomFromUri(requestProperty.Method, request.Headers.To, authorization); //FromHttpRequest(newRequest);
            else
                context = (OAuthContext)new OAuthContextBuilder().FromUri(requestProperty.Method, request.Headers.To);


            try
            {
                _provider.AccessProtectedResourceRequest(context);
            }
            catch (OAuthException authEx)
            {
                XElement response = XElement.Load(new StringReader("<?xml version=\"1.0\" encoding=\"utf-8\"?><html xmlns=\"http://www.w3.org/1999/xhtml\" version=\"-//W3C//DTD XHTML 2.0//EN\" xml:lang=\"en\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml2.xsd\"><HEAD><TITLE>Request Error</TITLE></HEAD><BODY><DIV id=\"content\"><P class=\"heading1\"><B>" + HttpUtility.HtmlEncode(authEx.Report.ToString()) + "</B></P></DIV></BODY></html>"));
                Message reply = Message.CreateMessage(MessageVersion.None, null, response);
                HttpResponseMessageProperty responseProperty = new HttpResponseMessageProperty() { StatusCode = HttpStatusCode.Forbidden, StatusDescription = authEx.Report.ToString() };
                responseProperty.Headers[HttpResponseHeader.ContentType] = "text/html";
                reply.Properties[HttpResponseMessageProperty.Name] = responseProperty;
                requestContext.Reply(reply);

                requestContext = null;
            }
        }
    }
}
*/