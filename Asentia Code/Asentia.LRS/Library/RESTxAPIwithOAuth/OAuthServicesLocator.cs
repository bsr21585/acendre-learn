﻿using System;
using Asentia.LRS.Library.OAuth.Provider;
using Asentia.LRS.Library.OAuth.Provider.Inspectors;
using Asentia.LRS.Library.OAuth.Storage;

namespace Asentia.LRS.Library.RESTxAPIwithOAuth
{
    public class OAuthServicesLocator
    {
        private static IOAuthProvider _provider;

        static OAuthServicesLocator()
        {
            var consumerStore = new ConsumerStore();
            var nonceStore = new NonceStore();

            _provider = new OAuthProvider(
                new SignatureValidationInspector(consumerStore),
                new NonceStoreInspector(nonceStore),
                new TimestampRangeInspector(new TimeSpan(1, 0, 0)),
                new ConsumerValidationInspector(consumerStore));
        }

        public static IOAuthProvider Provider
        {
            get { return _provider; }
        }
    }
}
