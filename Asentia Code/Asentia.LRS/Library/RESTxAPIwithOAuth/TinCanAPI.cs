﻿using System;
using System.Collections.Generic;
using System.IO;
using Asentia.LRS.Library.xAPI;
using Asentia.LRS.Library.xAPI.Storage;
using Asentia.Common;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Net;
using System.Web;
using Asentia.LRS.Library.OAuth.Framework;
using System.Data;
using System.Globalization;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Text;
using Asentia.LRS.Library.xAPI.Helper;
using Asentia.LRS.Library.OAuth.Storage;

namespace Asentia.LRS.Library.RESTxAPIwithOAuth
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class TinCanAPI : ITinCanAPI
    {
        #region Feilds
        private OAuthContext _Context;
        private readonly int _StatementsMaxLimit = 25;
        #endregion

        // using idUser(mapped to @idCaller in procedure) as 1 hardcoded bacause it is not being used in any of the procedure related to tincan API(As per Joe suggestion).

        #region Constructors
        public TinCanAPI()
        {; }
        #endregion

        #region Methods

        #region Statement API

        #region GetStatementAPIOptions
        /// <summary>
        /// Gets the statement API options.
        /// </summary>
        public void GetStatementAPIOptions()
        {
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type,Content-Length,Authorization,If-Match,If-None-Match,X-Experience-API-Version,X-Experience-API-Consistent-Through");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Expose-Headers", "ETag,Last-Modified,Cache-Control,Content-Type,Content-Length,WWW-Authenticate,X-Experience-API-Version,X-Experience-API-Consistent-Through");
            //WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Max-Age", "1728000");
        }
        #endregion

        #region SaveStatement
        /// <summary>
        /// Saves the statement.
        /// </summary>
        /// <param name="jsonStream">The json stream.</param>
        /// <returns></returns>
        public String SaveStatement(Stream jsonStream)
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";
            
            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                // check whether statement id has been provided.
                String statementId = HttpContext.Current.Request.QueryString["statementId"];
                if (String.IsNullOrWhiteSpace(statementId))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return _GlobalResources.StatementIDIsMissing;
                }

                StreamReader reader = new StreamReader(jsonStream);
                string jsonString = reader.ReadToEnd();
                reader.Close();
                reader.Dispose();

                Statement statement;
                var results = new List<Messages>();

                try
                {
                    statement = JsonConvert.DeserializeObject<Statement>(jsonString);

                    // if statement id has not been provided as part of the JSon then use the query string statement id.
                    if (String.IsNullOrWhiteSpace(statement.Id))
                    {
                        statement.Id = statementId;
                    }
                }
                catch
                {

                    try
                    {
                        jsonString = HttpUtility.UrlDecode(jsonString);
                        jsonString = jsonString.Substring(jsonString.IndexOf("content=") + 8);
                        if (jsonString.Contains("&statementId="))
                        {
                            jsonString = jsonString.Substring(0, jsonString.IndexOf("&statementId="));
                        }
                        if (jsonString.Contains("&Authorization="))
                        {
                            jsonString = jsonString.Substring(0, jsonString.IndexOf("&Authorization="));
                        }
                        statement = JsonConvert.DeserializeObject<Statement>(jsonString);
                    }
                    catch
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                        return _GlobalResources.MalformedJSONCouldNotDeserializeStringToStatement;
                    }
                }

                try
                {
                    results.AddRange(statement.Validate(earlyReturnOnFailure: false));
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return _GlobalResources.MalformedStatementCannotValidate;
                }

                if (results.Count == 0)
                {
                    try
                    {
                        DataTinCan dataTinCan = null;

                        // check whether this statement already exists.
                        // if it already exists then compare it with the one already stored.
                        // if both are same then ignore it (don't modify the statement or any other object).
                        bool statementExists;
                        try
                        {
                            dataTinCan = new DataTinCan(1, _Context.IdEndpoint, statement.Id, false);
                            statementExists = true;
                        }
                        catch (DatabaseDetailsNotFoundException)
                        {
                            statementExists = false;
                        }

                        if (statementExists)
                        {
                            Statement storedStatement = JsonConvert.DeserializeObject<Statement>(dataTinCan.Statement);
                            if (!this._CompareStatements(statement, storedStatement))
                            {
                                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Conflict;
                                return _GlobalResources.CannotModifyStatement.Replace("##statementId##", statement.Id);
                            }
                        }
                        else
                        {
                            List<Statement> list = new List<Statement>();
                            list.Add(statement);

                            DataTable contextActivities;
                            DataTable recordsToInsert = _GetTinCanDataTable(list, out contextActivities);

                            if (recordsToInsert != null && recordsToInsert.Rows.Count > 0)
                            {
                                if (dataTinCan == null)
                                {
                                    dataTinCan = new DataTinCan();
                                }
                                bool isInternalApi = false;
                                int? idEndPoint = _Context.IdEndpoint;

                                bool statementsTobeRecorded = false;

                                if (AsentiaSessionState.CurrentlyLaunchedDataLessonId > 0)
                                {
                                    dataTinCan.IdDataLesson = AsentiaSessionState.CurrentlyLaunchedDataLessonId;
                                    isInternalApi = true;
                                    idEndPoint = null;
                                }

                                if (isInternalApi && !DataTinCan.LessonCompleted((int)dataTinCan.IdDataLesson))
                                {
                                    statementsTobeRecorded = true;
                                }
                                else if (!isInternalApi)
                                {
                                    statementsTobeRecorded = true;
                                }

                                if (statementsTobeRecorded)
                                {
                                    // add statements to database
                                    dataTinCan.Save(1, idEndPoint, isInternalApi, recordsToInsert);

                                    // add context activities to database
                                    if (contextActivities != null && contextActivities.Rows.Count > 0)
                                    {
                                        DataTinCanContextActivities dataTinCanContextActivities = new DataTinCanContextActivities();
                                        dataTinCanContextActivities.Save(1, contextActivities);
                                    }
                                }
                            }
                        }

                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                        return null;
                    }
                    catch (DatabaseFieldNotUniqueException fnuEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Conflict;
                        return fnuEx.Message;
                    }
                    catch (DatabaseDetailsNotFoundException dnfEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                        return dnfEx.Message;
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return cpEx.Message;
                    }
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return results[0].Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #region SaveStatements
        /// <summary>
        /// Saves the statements.
        /// </summary>
        /// <param name="jsonStream">The json stream.</param>
        /// <returns></returns>
        public dynamic SaveStatements(Stream jsonStream)
        {
            List<string> statementsIds = null;
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";
            
            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                StreamReader reader = new StreamReader(jsonStream);
                string jsonString = reader.ReadToEnd();
                reader.Close();
                reader.Dispose();

                List<Statement> statements;
                var results = new List<Messages>();

                try
                {
                    try
                    {
                        // deserialize a list of statements
                        statements = JsonConvert.DeserializeObject<List<Statement>>(jsonString);
                    }
                    catch
                    {
                        try
                        {
                            jsonString = HttpUtility.UrlDecode(jsonString);
                            jsonString = jsonString.Substring(jsonString.IndexOf("content=") + 8);
                            if (jsonString.Contains("&statementId="))
                            {
                                jsonString = jsonString.Substring(0, jsonString.IndexOf("&statementId="));
                            }
                            if (jsonString.Contains("&Authorization="))
                            {
                                jsonString = jsonString.Substring(0, jsonString.IndexOf("&Authorization="));
                            }

                            statements = JsonConvert.DeserializeObject<List<Statement>>(jsonString);
                        }
                        catch
                        {
                            statements = null;
                        }
                    }

                    if (statements == null || statements.Count == 0)
                    {
                        // deserialize a single statement
                        Statement statement = JsonConvert.DeserializeObject<Statement>(jsonString);
                        if (statements == null)
                        {
                            statements = new List<Statement>();
                        }
                        statements.Add(statement);
                    }
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return _GlobalResources.MalformedJSONCouldNotDeserializeStringToStatement;
                }

                try
                {
                    // validate each statement.
                    foreach (Statement statement in statements)
                    {
                        results.AddRange(statement.Validate(earlyReturnOnFailure: false));
                    }
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return _GlobalResources.MalformedStatementCannotValidate;
                }

                if (results.Count == 0)
                {
                    try
                    {
                        DataTable contextActivities;
                        DataTable recordsToInsert = _GetTinCanDataTable(statements, out contextActivities);

                        if (recordsToInsert == null || recordsToInsert.Rows.Count <= 0)
                        {
                            return null;
                        }

                        // add statements to database
                        DataTinCan dataTinCan = new DataTinCan();
                        bool isInternalApi = false;
                        int? idEndPoint = _Context.IdEndpoint;
                        if (AsentiaSessionState.CurrentlyLaunchedDataLessonId > 0)
                        {
                            dataTinCan.IdDataLesson = AsentiaSessionState.CurrentlyLaunchedDataLessonId;
                            isInternalApi = true;
                            idEndPoint = null;
                        }

                        bool statementsTobeRecorded = true;

                        if (isInternalApi && DataTinCan.LessonCompleted((int)dataTinCan.IdDataLesson))
                        {
                            statementsTobeRecorded = false;
                        }

                        if (statementsTobeRecorded)
                        {
                            dataTinCan.Save(1, idEndPoint, isInternalApi, recordsToInsert);

                            // add context activities to database
                            if (contextActivities != null && contextActivities.Rows.Count > 0)
                            {
                                DataTinCanContextActivities dataTinCanContextActivities = new DataTinCanContextActivities();
                                dataTinCanContextActivities.Save(1, contextActivities);
                            }
                        }

                        // create list of statement ids.
                        statementsIds = new List<string>();

                        // add statement ids to the list.
                        foreach (Statement statement in statements)
                        {
                            statementsIds.Add(statement.Id);
                        }
                    }
                    catch (DatabaseFieldNotUniqueException fnuEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Conflict;
                        return fnuEx.Message;
                    }
                    catch (DatabaseDetailsNotFoundException dnfEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                        return dnfEx.Message;
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return cpEx.Message;
                    }
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return results[0].Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }

            return statementsIds;
        }
        #endregion

        #region RetrieveStatements
        /// <summary>
        /// Retrieves the statements.
        /// </summary>
        /// <returns></returns>
        public Stream RetrieveStatements()
        {
            string json = null;
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";
            
            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String errorMessage;
                if (!_ValidateGetRequest(out errorMessage))
                {
                    return new MemoryStream(Encoding.UTF8.GetBytes(errorMessage));
                }

                try
                {
                    JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                    jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;

                    String statementId = HttpContext.Current.Request.QueryString["statementId"];
                    String voidedStatementId = HttpContext.Current.Request.QueryString["voidedStatementId"];

                    string format = null;
                    if (HttpContext.Current.Request.QueryString["format"] != null)
                    {
                        format = HttpContext.Current.Request.QueryString["format"].ToLower();
                    }

                    if (statementId != null)
                    {
                        DataTinCan dataTinCan = new DataTinCan(1, _Context.IdEndpoint, statementId, false);
                        Statement statement = JsonConvert.DeserializeObject<Statement>(dataTinCan.Statement);

                        if (format == "ids")
                        {
                            // we need to return identification info only for Agent/Group/Activity.
                            this._SetObjectPropertiesAsNullExceptId(statement.Object);
                            this._SetActorPropertiesAsNullExceptId(statement.Actor);
                        }

                        // note: We are not handling "exact" and "canonical" because we are always
                        // saving original information for Agent/Group/Activity objects.

                        json = JsonConvert.SerializeObject(statement, Formatting.None, jsonSerializerSettings);
                    }
                    else if (voidedStatementId != null)
                    {
                        DataTinCan dataTinCan = new DataTinCan(1, _Context.IdEndpoint, voidedStatementId, true);
                        Statement statement = JsonConvert.DeserializeObject<Statement>(dataTinCan.Statement);

                        if (format == "ids")
                        {
                            // we need to return identification info only for Agent/Group/Activity.
                            this._SetObjectPropertiesAsNullExceptId(statement.Object);
                            this._SetActorPropertiesAsNullExceptId(statement.Actor);
                        }

                        // note: We are not handling "exact" and "canonical" because we are always
                        // saving original information for Agent/Group/Activity objects.

                        json = JsonConvert.SerializeObject(statement, Formatting.None, jsonSerializerSettings);
                    }
                    else
                    {
                        DataTinCan dataTinCan = new DataTinCan();
                        List<Statement> statements = new List<Statement>();

                        bool ascendingOrder = false; //Descending order is the default.
                        if (HttpContext.Current.Request.QueryString["ascending"] != null)
                        {
                            ascendingOrder = Convert.ToBoolean(HttpContext.Current.Request.QueryString["ascending"]);
                        }

                        dataTinCan.VerbId = HttpContext.Current.Request.QueryString["verb"];

                        string mbox = null;
                        string mboxSha1Sum = null;
                        string openId = null;
                        string account = null;
                        if (!String.IsNullOrWhiteSpace(HttpContext.Current.Request.QueryString["agent"]))
                        {
                            Actor actor = JsonConvert.DeserializeObject<Actor>(HttpContext.Current.Request.QueryString["agent"]);
                            if (actor != null)
                            {
                                mbox = actor.Mbox;
                                mboxSha1Sum = actor.Mbox_sha1sum;
                                openId = actor.Openid;
                                if (actor.Account != null)
                                {
                                    account = JsonConvert.SerializeObject(actor.Account, Formatting.None, jsonSerializerSettings);
                                }
                            }
                        }

                        dataTinCan.IdEndpoint = _Context.IdEndpoint;
                        dataTinCan.ActivityId = HttpContext.Current.Request.QueryString["activity"];
                        dataTinCan.Registration = HttpContext.Current.Request.QueryString["registration"];

                        bool relatedActivities = false;
                        if (HttpContext.Current.Request.QueryString["related_activities"] != null)
                        {
                            relatedActivities = Convert.ToBoolean(HttpContext.Current.Request.QueryString["related_activities"]);
                        }

                        bool relatedAgents = false;
                        if (HttpContext.Current.Request.QueryString["related_agents"] != null)
                        {
                            relatedAgents = Convert.ToBoolean(HttpContext.Current.Request.QueryString["related_agents"]);
                        }

                        int start = 1;
                        if (HttpContext.Current.Request.QueryString["start"] != null)
                        {
                            start = Convert.ToInt32(HttpContext.Current.Request.QueryString["start"]);
                        }

                        int limit = 0;
                        if (HttpContext.Current.Request.QueryString["limit"] != null)
                        {
                            limit = Convert.ToInt32(HttpContext.Current.Request.QueryString["limit"]);
                        }

                        if (limit == 0 || limit > _StatementsMaxLimit)
                        {
                            limit = _StatementsMaxLimit;
                        }

                        DateTime? since = null;
                        if (HttpContext.Current.Request.QueryString["since"] != null)
                        {
                            since = DateTime.ParseExact(HttpContext.Current.Request.QueryString["since"], "o", CultureInfo.InvariantCulture);
                        }

                        DateTime? until = null;
                        if (HttpContext.Current.Request.QueryString["until"] != null)
                        {
                            until = DateTime.ParseExact(HttpContext.Current.Request.QueryString["until"], "o", CultureInfo.InvariantCulture);
                        }

                        bool moreResult;

                        // get multiple statements from database.
                        DataTable dataTable = dataTinCan.GetStatements(1, start, limit, since, until, ascendingOrder, relatedActivities, mbox, mboxSha1Sum, openId, account, relatedAgents, out moreResult);

                        foreach (DataRow row in dataTable.Rows)
                        {
                            Statement statement = JsonConvert.DeserializeObject<Statement>(row["statement"].ToString());

                            if (format == "ids")
                            {
                                // we need to return identification info only for Agent/Group/Activity.
                                this._SetObjectPropertiesAsNullExceptId(statement.Object);
                                this._SetActorPropertiesAsNullExceptId(statement.Actor);
                            }

                            // note: We are not handling "exact" and "canonical" because we are always
                            // saving original information for Agent/Group/Activity objects.

                            statements.Add(statement);
                        }

                        StatementResult statementResult;
                        if (moreResult)
                        {
                            int nextStartRecord = start + limit;
                            string more = this._GetMoreResultsIRL(nextStartRecord);
                            statementResult = new StatementResult(statements, more);
                        }
                        else
                        {
                            statementResult = new StatementResult(statements);
                        }

                        json = JsonConvert.SerializeObject(statementResult, Formatting.None, jsonSerializerSettings);
                    }
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return new MemoryStream(Encoding.UTF8.GetBytes(dnfEx.Message));
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return new MemoryStream(Encoding.UTF8.GetBytes(cpEx.Message));
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters));
            }

            if (!String.IsNullOrWhiteSpace(json))
            {
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";

                // add the X-Experience-Api-Consistent-Through in the response header.
                WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-Api-Consistent-Through", DateTime.UtcNow.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffZ"));
                return new MemoryStream(Encoding.UTF8.GetBytes(json));
            }
            else
            {
                return null;
            }
        }
        #endregion

        #endregion


        #region State API

        #region GetStateAPIOptions
        /// <summary>
        /// Gets the state API options.
        /// </summary>
        public void GetStateAPIOptions()
        {
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type,Content-Length,Authorization,If-Match,If-None-Match,X-Experience-API-Version,X-Experience-API-Consistent-Through");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Expose-Headers", "ETag,Last-Modified,Cache-Control,Content-Type,Content-Length,WWW-Authenticate,X-Experience-API-Version,X-Experience-API-Consistent-Through");
        }
        #endregion

        #region SaveStateDocument
        /// <summary>
        /// Saves the state document.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public String SaveStateDocument(Stream stream)
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String json;
                String errorMessage;
                if (!_ValidateStateAPIPutPostRequest(out errorMessage, stream, out json))
                {
                    return errorMessage;
                }
                try
                {
                    // deserialize and serialize the agent so that to generate json string
                    // in proper order (order of the properties). It is useful for comparison.
                    JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                    jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    Actor actor = JsonConvert.DeserializeObject<Actor>(HttpContext.Current.Request.QueryString["agent"]);
                    String agent = JsonConvert.SerializeObject(actor, Formatting.None, jsonSerializerSettings);

                    DateTime dtUpdated;
                    String updated = HttpContext.Current.Request.Headers.Get("updated");
                    if (!String.IsNullOrWhiteSpace(updated))
                    {
                        dtUpdated = Convert.ToDateTime(updated);
                    }
                    else
                    {
                        dtUpdated = AsentiaSessionState.UtcNow;
                    }

                    String contentType = String.Empty;
                    if (HttpContext.Current.Request.ContentType != null)
                    {
                        contentType = HttpContext.Current.Request.ContentType;
                    }

                    DataTinCanState dataTinCanState = new DataTinCanState();
                    dataTinCanState.StateId = HttpContext.Current.Request.QueryString["stateId"];
                    dataTinCanState.ActivityId = HttpContext.Current.Request.QueryString["activityId"];
                    dataTinCanState.IdEndpoint = _Context.IdEndpoint;
                    dataTinCanState.Agent = agent;
                    dataTinCanState.Registration = HttpContext.Current.Request.QueryString["registration"];
                    dataTinCanState.DtUpdated = dtUpdated;
                    dataTinCanState.ContentType = contentType;
                    if (json != null)
                    {
                        dataTinCanState.DocContents = Encoding.UTF8.GetBytes(json);
                    }
                    else
                    {
                        using (BinaryReader reader = new BinaryReader(stream))
                        {
                            dataTinCanState.DocContents = reader.ReadBytes(HttpContext.Current.Request.ContentLength);
                        }
                    }

                    dataTinCanState.Save(1);

                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                    return null;
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #region PostStateDocument
        /// <summary>
        /// Posts the state document.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public String PostStateDocument(Stream stream)
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String json;
                String errorMessage;

                // copy the input stream to another memory stream so that we can read it multiple times if required by just setting the position to 0
                MemoryStream localStream = _StreamCopyAndClose(stream);

                StreamReader streamReader = new StreamReader(localStream);
                string jsonString = streamReader.ReadToEnd();
                jsonString = HttpUtility.UrlDecode(jsonString);

                // resetting the stram position so we can read it again
                localStream.Position = 0;

                // itemDic dictionary collection will be used to find required attributed if they are not available in Request QueryString(in storyline packages)
                Dictionary<string, string> itemDic = new Dictionary<string, string>();

                try
                {
                    var items = jsonString.Split('&');
                    foreach (string item in items)
                    {
                        var splitItem = item.Split('=');
                        if (splitItem.Length > 1)
                        {
                            switch (splitItem[0])
                            {
                                case "stateId":
                                case "activityId":
                                case "agent":
                                case "registration":
                                case "Content-Type":
                                case "content":
                                    itemDic.Add(splitItem[0], splitItem[1]);
                                    break;
                            }
                        }
                    }
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return _GlobalResources.StateIDIsMissing;

                }

                if (!_ValidateStateAPIPutPostRequest(out errorMessage, stream, out json, itemDic, localStream))
                {
                    return errorMessage;
                }

                try
                {
                    /* if document already exists and both documents have content type application/json then they will be merged.
                     * if both documents don't have content type application/json then the request will be rejected.
                     * if document does not already exist then treat the request the same as it would a PUT request
                     * and store the document being posted.
                     */

                    String stateId = HttpContext.Current.Request.QueryString["stateId"];
                    String activityId = HttpContext.Current.Request.QueryString["activityId"];
                    String agent = HttpContext.Current.Request.QueryString["agent"];
                    String registration = HttpContext.Current.Request.QueryString["registration"];

                    string contentType = string.Empty;
                    try
                    {
                        contentType = HttpContext.Current.Request.ContentType;
                    }
                    catch
                    {
                        contentType = string.Empty;
                    }
                    if (String.IsNullOrWhiteSpace(stateId))
                    {
                        stateId = itemDic["stateId"];
                    }

                    if (String.IsNullOrWhiteSpace(activityId))
                    {
                        activityId = itemDic["activityId"];
                    }

                    if (String.IsNullOrWhiteSpace(agent))
                    {
                        agent = itemDic["agent"];
                    }

                    // as regeistration is not required field so we are not validating it in validation funtion.
                    // that is why handling it here.
                    if (String.IsNullOrWhiteSpace(registration))
                    {
                        try
                        {
                            registration = itemDic["registration"];
                        }
                        catch (Exception e)
                        {
                            registration = string.Empty;
                        }
                    }

                    if (String.IsNullOrWhiteSpace(contentType))
                    {
                        contentType = itemDic["Content-Type"];
                    }

                    // deserialize and serialize the agent so that to generate json string
                    // in proper order (order of the properties). It is useful for comparison.
                    JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                    jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    Actor actor = JsonConvert.DeserializeObject<Actor>(agent);
                    agent = JsonConvert.SerializeObject(actor, Formatting.None, jsonSerializerSettings);

                    DataTinCanState dataTinCanState;
                    try
                    {
                        dataTinCanState = new DataTinCanState(1, this._Context.IdEndpoint, stateId, activityId, agent, registration);
                    }
                    catch (DatabaseDetailsNotFoundException)
                    {
                        dataTinCanState = null;
                    }
                    catch (DatabaseCallerPermissionException)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return _GlobalResources.YouAreNotAuthorizedToUpdateThisRecord;
                    }

                    // document exists
                    if (dataTinCanState != null) 
                    {
                        String postedDocContentType = null;
                        if (!String.IsNullOrWhiteSpace(contentType))
                        {
                            postedDocContentType = contentType.ToLower();
                        }

                        // check whether both documents have content type application/json
                        if (dataTinCanState.ContentType.ToLower().Contains("application/json") || postedDocContentType.Contains("application/json") || dataTinCanState.ContentType.ToLower().Contains("text/plain") || postedDocContentType.Contains("text/plain"))
                        {

                            String originalDocJson = Encoding.UTF8.GetString(dataTinCanState.DocContents);

                            Dictionary<string, object> postedDocument = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                            Dictionary<string, object> originalDocument = JsonConvert.DeserializeObject<Dictionary<string, object>>(originalDocJson);

                            // create another dictionary for case insensitive comparison based on the original document
                            Dictionary<string, object> mergedDictionary = new Dictionary<string, object>(originalDocument, StringComparer.OrdinalIgnoreCase);

                            // merge JSON (only top level properties will be merged)
                            foreach (KeyValuePair<string, object> keyValue in postedDocument)
                            {
                                if (mergedDictionary.ContainsKey(keyValue.Key))
                                {
                                    mergedDictionary[keyValue.Key] = keyValue.Value;
                                }
                                else
                                {
                                    mergedDictionary.Add(keyValue.Key, keyValue.Value);
                                }
                            }

                            json = JsonConvert.SerializeObject(mergedDictionary, Formatting.None, jsonSerializerSettings);
                        }
                        else
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                            return _GlobalResources.ContentTypeShouldBeJSONToMergeDocuments;
                        }

                    }
                    else
                    {
                        dataTinCanState = new DataTinCanState();
                        dataTinCanState.StateId = stateId;
                        dataTinCanState.ActivityId = activityId;
                        dataTinCanState.Agent = agent;
                        dataTinCanState.Registration = registration;
                    }

                    DateTime dtUpdated;
                    String updated = HttpContext.Current.Request.Headers.Get("updated");
                    if (!String.IsNullOrWhiteSpace(updated))
                    {
                        dtUpdated = Convert.ToDateTime(updated);
                    }
                    else
                    {
                        dtUpdated = AsentiaSessionState.UtcNow;
                    }


                    dataTinCanState.DtUpdated = dtUpdated;
                    dataTinCanState.ContentType = contentType;
                    if (!String.IsNullOrWhiteSpace(json))
                    {
                        dataTinCanState.DocContents = Encoding.UTF8.GetBytes(json);
                    }
                    else
                    {
                        using (BinaryReader reader = new BinaryReader(localStream))
                        {
                            dataTinCanState.DocContents = reader.ReadBytes(HttpContext.Current.Request.ContentLength);
                        }
                    }

                    dataTinCanState.IdEndpoint = _Context.IdEndpoint;
                    dataTinCanState.Save(1);

                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                    return null;
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #region _StreamCopyAndClose
        /// <summary>
        /// Makes stream copy and then close it.
        /// </summary>
        /// <param name="inputStream">The input stream.</param>
        /// <returns></returns>
        private static MemoryStream _StreamCopyAndClose(Stream inputStream)
        {
            byte[] buffer = new byte[8 * 1024];
            MemoryStream ms = new MemoryStream();

            int count = inputStream.Read(buffer, 0, buffer.Length);
            while (count > 0)
            {
                ms.Write(buffer, 0, count);
                count = inputStream.Read(buffer, 0, buffer.Length);
            }
            ms.Position = 0;
            inputStream.Close();
            return ms;
        }

        #endregion

        #region RetrieveStateDocument
        /// <summary>
        /// Retrieves the state document.
        /// </summary>
        /// <returns></returns>
        public Stream RetrieveStateDocument()
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String errorMessage;
                if (!_ValidateStateAPIDeleteGetRequest(out errorMessage))
                {
                    return new MemoryStream(Encoding.UTF8.GetBytes(errorMessage));
                }

                String stateId = HttpContext.Current.Request.QueryString["stateId"];
                String activityId = HttpContext.Current.Request.QueryString["activityId"];
                String agent = HttpContext.Current.Request.QueryString["agent"];
                String registration = HttpContext.Current.Request.QueryString["registration"];

                // deserialize and serialize the object (don't use the original JSON of agent) so that to make
                // the properties order same (useful for comparison).
                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                Actor actor = JsonConvert.DeserializeObject<Actor>(agent);
                agent = JsonConvert.SerializeObject(actor, Formatting.None, jsonSerializerSettings);

                // if "stateId" parameter is passed then return single document otherwise return the available ids.
                if (stateId == null)
                {
                    try
                    {
                        List<String> ids = new List<string>();
                        DataTinCanState dataTinCanState = new DataTinCanState();
                        dataTinCanState.IdEndpoint = _Context.IdEndpoint;
                        dataTinCanState.ActivityId = activityId;
                        dataTinCanState.Agent = agent;
                        dataTinCanState.Registration = registration;
                        DataTable dataTable = dataTinCanState.GetActivityStates(1);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            ids.Add(row["stateId"].ToString());
                        }

                        if (ids.Count > 0)
                        {
                            String json = JsonConvert.SerializeObject(ids);
                            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
                            return new MemoryStream(Encoding.UTF8.GetBytes(json));
                        }

                        return null;
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return new MemoryStream(Encoding.UTF8.GetBytes(cpEx.Message));
                    }
                }
                else
                {
                    try
                    {
                        DataTinCanState dataTinCanState = new DataTinCanState(1, this._Context.IdEndpoint, stateId, activityId, agent, registration);
                        WebOperationContext.Current.OutgoingResponse.Headers.Add("updated", dataTinCanState.DtUpdated.ToString("o"));
                        if (!String.IsNullOrWhiteSpace(dataTinCanState.ContentType))
                        {
                            WebOperationContext.Current.OutgoingResponse.ContentType = dataTinCanState.ContentType;
                        }

                        return new MemoryStream(dataTinCanState.DocContents);
                    }
                    catch (DatabaseDetailsNotFoundException dnfEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                        return new MemoryStream(Encoding.UTF8.GetBytes(dnfEx.Message));
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return new MemoryStream(Encoding.UTF8.GetBytes(cpEx.Message));
                    }
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters));
            }
        }
        #endregion

        #region RetrieveStateDocumentForStoryLinePckages
        /// <summary>
        /// Retrieves the state document for story line pckages.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public Stream RetrieveStateDocumentForStoryLinePckages(Stream stream)
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {

                // copy the input stream to another memory stream so that we can read it multiple times if required by just setting the position to 0
                MemoryStream localStream = _StreamCopyAndClose(stream);

                StreamReader streamReader = new StreamReader(localStream);
                string jsonString = streamReader.ReadToEnd();
                jsonString = HttpUtility.UrlDecode(jsonString);

                // resetting the stram position so we can read it again
                localStream.Position = 0;

                // itemDic dictionary collection will be used to find required attributed if they are not available in Request QueryString(in storyline packages)
                Dictionary<string, string> itemDic = new Dictionary<string, string>();

                try
                {
                    var items = jsonString.Split('&');
                    foreach (string item in items)
                    {
                        var splitItem = item.Split('=');
                        if (splitItem.Length > 1)
                        {
                            switch (splitItem[0])
                            {
                                case "stateId":
                                case "activityId":
                                case "agent":
                                case "registration":
                                case "Content-Type":
                                    itemDic.Add(splitItem[0], splitItem[1]);
                                    break;
                            }
                        }
                    }
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                }

                String errorMessage;
                if (!_ValidateStateAPIDeleteGetRequest(out errorMessage, itemDic))
                {
                    return new MemoryStream(Encoding.UTF8.GetBytes(errorMessage));
                }

                String stateId = HttpContext.Current.Request.QueryString["stateId"];
                String activityId = HttpContext.Current.Request.QueryString["activityId"];
                String agent = HttpContext.Current.Request.QueryString["agent"];
                String registration = HttpContext.Current.Request.QueryString["registration"];

                if (String.IsNullOrWhiteSpace(stateId))
                {
                    stateId = itemDic["stateId"];
                }

                if (String.IsNullOrWhiteSpace(activityId))
                {
                    activityId = itemDic["activityId"];
                }

                if (String.IsNullOrWhiteSpace(agent))
                {
                    agent = itemDic["agent"];
                }

                if (String.IsNullOrWhiteSpace(registration))
                {
                    registration = itemDic["registration"];
                }


                // deserialize and serialize the object (don't use the original JSON of agent) so that to make
                // the properties order same (useful for comparison).
                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                Actor actor = JsonConvert.DeserializeObject<Actor>(agent);
                agent = JsonConvert.SerializeObject(actor, Formatting.None, jsonSerializerSettings);

                // if "stateId" parameter is passed then return single document otherwise return the available ids.
                if (stateId == null)
                {
                    try
                    {
                        List<String> ids = new List<string>();
                        DataTinCanState dataTinCanState = new DataTinCanState();
                        dataTinCanState.IdEndpoint = _Context.IdEndpoint;
                        dataTinCanState.ActivityId = activityId;
                        dataTinCanState.Agent = agent;
                        dataTinCanState.Registration = registration;
                        DataTable dataTable = dataTinCanState.GetActivityStates(1);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            ids.Add(row["stateId"].ToString());
                        }

                        if (ids.Count > 0)
                        {
                            String json = JsonConvert.SerializeObject(ids);
                            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
                            return new MemoryStream(Encoding.UTF8.GetBytes(json));
                        }

                        return null;
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return new MemoryStream(Encoding.UTF8.GetBytes(cpEx.Message));
                    }
                }
                else
                {
                    try
                    {
                        DataTinCanState dataTinCanState = new DataTinCanState(1, this._Context.IdEndpoint, stateId, activityId, agent, registration);
                        WebOperationContext.Current.OutgoingResponse.Headers.Add("updated", dataTinCanState.DtUpdated.ToString("o"));
                        if (!String.IsNullOrWhiteSpace(dataTinCanState.ContentType))
                        {
                            WebOperationContext.Current.OutgoingResponse.ContentType = dataTinCanState.ContentType;
                        }

                        return new MemoryStream(dataTinCanState.DocContents);
                    }
                    catch (DatabaseDetailsNotFoundException dnfEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                        return new MemoryStream(Encoding.UTF8.GetBytes(dnfEx.Message));
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return new MemoryStream(Encoding.UTF8.GetBytes(cpEx.Message));
                    }
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters));
            }
        }
        #endregion

        #region DeleteStateDocument
        /// <summary>
        /// Deletes the state document.
        /// </summary>
        /// <returns></returns>
        public String DeleteStateDocument()
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String errorMessage;
                if (!_ValidateStateAPIDeleteGetRequest(out errorMessage))
                {
                    return errorMessage;
                }

                // if state id is passed a single record will be deleted.
                // if state id is not passed all records will be deleted (based on other params).

                String stateId = HttpContext.Current.Request.QueryString["stateId"];
                String activityId = HttpContext.Current.Request.QueryString["activityId"];
                String agent = HttpContext.Current.Request.QueryString["agent"];
                String registration = HttpContext.Current.Request.QueryString["registration"];

                // deserialize and serialize the object (don't use the original JSON of agent) so that to make
                // the properties order same (useful for comparison).
                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                Actor actor = JsonConvert.DeserializeObject<Actor>(agent);
                agent = JsonConvert.SerializeObject(actor, Formatting.None, jsonSerializerSettings);

                try
                {
                    DataTinCanState.Delete(1, this._Context.IdEndpoint, stateId, activityId, agent, registration);
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;

                    return null;
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #endregion

        #region Activity Profile API

        #region GetActivityProfileAPIOptions
        /// <summary>
        /// Gets the activity profile API options.
        /// </summary>
        public void GetActivityProfileAPIOptions()
        {
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type,Content-Length,Authorization,If-Match,If-None-Match,X-Experience-API-Version,X-Experience-API-Consistent-Through");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Expose-Headers", "ETag,Last-Modified,Cache-Control,Content-Type,Content-Length,WWW-Authenticate,X-Experience-API-Version,X-Experience-API-Consistent-Through");
        }
        #endregion

        #region GetActivity
        /// <summary>
        /// Gets the activity.
        /// </summary>
        /// <returns></returns>
        public Stream GetActivity()
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            string returnValue = null;

            if (_AuthenticateRequest())
            {
                try
                {
                    String activityId = HttpContext.Current.Request.QueryString["activityId"];
                    if (String.IsNullOrWhiteSpace(activityId))
                    {
                        // bad request.
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                        return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.ActivityIDIsMissing));
                    }
                    else
                    {
                        DataTinCan dataTinCan = new DataTinCan();
                        returnValue = dataTinCan.GetActivity(1, this._Context.IdEndpoint, activityId);
                    }
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    returnValue = dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    returnValue = cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                returnValue = _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }

            if (!String.IsNullOrWhiteSpace(returnValue))
            {
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
                return new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region SaveActivityProfileDocument
        /// <summary>
        /// Saves the activity profile document.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public String SaveActivityProfileDocument(Stream stream)
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String json;
                String errorMessage;
                if (!_ValidateActivityProfileAPIRequest(out errorMessage, stream, out json))
                {
                    return errorMessage;
                }

                try
                {
                    DateTime dtUpdated;
                    String updated = HttpContext.Current.Request.Headers.Get("updated");
                    if (!String.IsNullOrWhiteSpace(updated))
                    {
                        dtUpdated = Convert.ToDateTime(updated);
                    }
                    else
                    {
                        dtUpdated = AsentiaSessionState.UtcNow;
                    }

                    String contentType = String.Empty;
                    if (HttpContext.Current.Request.ContentType != null)
                    {
                        contentType = HttpContext.Current.Request.ContentType;
                    }

                    DataTinCanProfile dataTinCanProfile = new DataTinCanProfile();
                    dataTinCanProfile.ProfileId = HttpContext.Current.Request.QueryString["profileId"];
                    dataTinCanProfile.ActivityId = HttpContext.Current.Request.QueryString["activityId"];
                    dataTinCanProfile.IdEndpoint = this._Context.IdEndpoint;
                    dataTinCanProfile.DtUpdated = dtUpdated;
                    dataTinCanProfile.ContentType = contentType;
                    if (json != null)
                    {
                        dataTinCanProfile.DocContents = Encoding.UTF8.GetBytes(json);
                    }
                    else
                    {
                        using (BinaryReader reader = new BinaryReader(stream))
                        {
                            dataTinCanProfile.DocContents = reader.ReadBytes(HttpContext.Current.Request.ContentLength);
                        }
                    }

                    dataTinCanProfile.Save(1);

                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                    return null;
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #region PostActivityProfileDocument
        /// <summary>
        /// Posts the activity profile document.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public String PostActivityProfileDocument(Stream stream)
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String json;
                String errorMessage;
                if (!_ValidateActivityProfileAPIRequest(out errorMessage, stream, out json))
                {
                    return errorMessage;
                }

                try
                {
                    /* if document already exists and both documents have content type application/json then they will be merged.
                     * if both documents don't have content type application/json then the request will be rejected.
                     * if document does not already exist then treat the request the same as it would a PUT request
                     * and store the document being posted.
                     */

                    String profileId = HttpContext.Current.Request.QueryString["profileId"];
                    String activityId = HttpContext.Current.Request.QueryString["activityId"];

                    // if activityId is missing then return the bad request response.
                    if (String.IsNullOrWhiteSpace(activityId))
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                        return _GlobalResources.ActivityIDIsMissing;
                    }
                    // if profileId is missing then return the bad request response.
                    if (String.IsNullOrWhiteSpace(profileId))
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                        return _GlobalResources.ProfileIDIsMissing;
                    }

                    DataTinCanProfile dataTinCanProfile;
                    try
                    {
                        dataTinCanProfile = new DataTinCanProfile(1, this._Context.IdEndpoint, profileId, activityId, null);
                    }
                    catch (DatabaseDetailsNotFoundException)
                    {
                        dataTinCanProfile = null;
                    }
                    catch (DatabaseCallerPermissionException)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return _GlobalResources.YouAreNotAuthorizedToUpdateThisRecord;
                    }

                    // document exists
                    if (dataTinCanProfile != null) 
                    {
                        String postedDocContentType = null;
                        if (HttpContext.Current.Request.ContentType != null)
                        {
                            postedDocContentType = HttpContext.Current.Request.ContentType.ToLower();
                        }

                        // check whether both documents have content type application/json
                        if (dataTinCanProfile.ContentType.ToLower() != "application/json" || postedDocContentType != "application/json")
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                            return _GlobalResources.ContentTypeShouldBeJSONToMergeDocuments;
                        }

                        String originalDocJson = Encoding.UTF8.GetString(dataTinCanProfile.DocContents);

                        Dictionary<string, object> postedDocument = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                        Dictionary<string, object> originalDocument = JsonConvert.DeserializeObject<Dictionary<string, object>>(originalDocJson);

                        // create another dictionary for case insensitive comparison based on the original document
                        Dictionary<string, object> mergedDictionary = new Dictionary<string, object>(originalDocument, StringComparer.OrdinalIgnoreCase);

                        // merge JSON (only top level properties will be merged)
                        foreach (KeyValuePair<string, object> keyValue in postedDocument)
                        {
                            if (mergedDictionary.ContainsKey(keyValue.Key))
                            {
                                mergedDictionary[keyValue.Key] = keyValue.Value;
                            }
                            else
                            {
                                mergedDictionary.Add(keyValue.Key, keyValue.Value);
                            }
                        }

                        JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                        jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                        json = JsonConvert.SerializeObject(mergedDictionary, Formatting.None, jsonSerializerSettings);
                    }
                    else
                    {
                        dataTinCanProfile = new DataTinCanProfile();
                        dataTinCanProfile.ProfileId = profileId;
                        dataTinCanProfile.ActivityId = activityId;
                    }

                    DateTime dtUpdated;
                    String updated = HttpContext.Current.Request.Headers.Get("updated");
                    if (!String.IsNullOrWhiteSpace(updated))
                    {
                        dtUpdated = Convert.ToDateTime(updated);
                    }
                    else
                    {
                        dtUpdated = AsentiaSessionState.UtcNow;
                    }

                    String contentType = String.Empty;
                    if (HttpContext.Current.Request.ContentType != null)
                    {
                        contentType = HttpContext.Current.Request.ContentType;
                    }

                    dataTinCanProfile.IdEndpoint = this._Context.IdEndpoint;
                    dataTinCanProfile.DtUpdated = dtUpdated;
                    dataTinCanProfile.ContentType = contentType;
                    if (json != null)
                    {
                        dataTinCanProfile.DocContents = Encoding.UTF8.GetBytes(json);
                    }
                    else
                    {
                        using (BinaryReader reader = new BinaryReader(stream))
                        {
                            dataTinCanProfile.DocContents = reader.ReadBytes(HttpContext.Current.Request.ContentLength);
                        }
                    }

                    dataTinCanProfile.Save(1);

                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                    return null;
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #region RetrieveActivityProfileDocument
        /// <summary>
        /// Retrieves the activity profile document.
        /// </summary>
        /// <returns></returns>
        public Stream RetrieveActivityProfileDocument()
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String profileId = HttpContext.Current.Request.QueryString["profileId"];
                String activityId = HttpContext.Current.Request.QueryString["activityId"];

                if (String.IsNullOrWhiteSpace(activityId))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.ActivityIDIsMissing));
                }
                if (String.IsNullOrWhiteSpace(profileId))
                {
                    // bad request.
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.ProfileIDIsMissing));
                }

                DateTime? since = null;
                if (HttpContext.Current.Request.QueryString["since"] != null)
                {
                    try
                    {
                        since = Convert.ToDateTime(HttpContext.Current.Request.QueryString["since"]);
                    }
                    catch
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                        return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.TheValueOfParameterSinceIsMalformed));
                    }
                }

                // if "profileId" parameter is passed then return single document otherwise return the available ids.
                if (profileId == null)
                {
                    try
                    {
                        List<String> ids = new List<string>();
                        DataTinCanProfile dataTinCanProfile = new DataTinCanProfile();
                        dataTinCanProfile.ActivityId = activityId;
                        dataTinCanProfile.IdEndpoint = this._Context.IdEndpoint;
                        DataTable dataTable = dataTinCanProfile.GetProfiles(1, since);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            ids.Add(row["profileId"].ToString());
                        }

                        if (ids.Count > 0)
                        {
                            String json = JsonConvert.SerializeObject(ids);
                            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
                            return new MemoryStream(Encoding.UTF8.GetBytes(json));
                        }

                        return null;
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return new MemoryStream(Encoding.UTF8.GetBytes(cpEx.Message));
                    }
                }
                else
                {
                    try
                    {
                        DataTinCanProfile dataTinCanProfile = new DataTinCanProfile(1, this._Context.IdEndpoint, profileId, activityId, null);
                        WebOperationContext.Current.OutgoingResponse.Headers.Add("updated", dataTinCanProfile.DtUpdated.ToString("o"));
                        if (!String.IsNullOrWhiteSpace(dataTinCanProfile.ContentType))
                        {
                            WebOperationContext.Current.OutgoingResponse.ContentType = dataTinCanProfile.ContentType;
                        }

                        return new MemoryStream(dataTinCanProfile.DocContents);
                    }
                    catch (DatabaseDetailsNotFoundException dnfEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                        return new MemoryStream(Encoding.UTF8.GetBytes(dnfEx.Message));
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return new MemoryStream(Encoding.UTF8.GetBytes(cpEx.Message));
                    }
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters));
            }
        }
        #endregion

        #region DeleteActivityProfileDocument
        /// <summary>
        /// Deletes the activity profile document.
        /// </summary>
        /// <returns></returns>
        public String DeleteActivityProfileDocument()
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String profileId = HttpContext.Current.Request.QueryString["profileId"];
                String activityId = HttpContext.Current.Request.QueryString["activityId"];

                if (String.IsNullOrWhiteSpace(profileId))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return _GlobalResources.ProfileIDIsMissing;
                }

                if (String.IsNullOrWhiteSpace(activityId))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return _GlobalResources.ActivityIDIsMissing;
                }

                try
                {
                    DataTinCanProfile.Delete(1, this._Context.IdEndpoint, profileId, activityId, null);
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;

                    return null;
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #endregion

        #region Agent Profile API

        #region GetAgentProfileAPIOptions
        public void GetAgentProfileAPIOptions()
        {
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Headers", "Content-Type,Content-Length,Authorization,If-Match,If-None-Match,X-Experience-API-Version,X-Experience-API-Consistent-Through");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Access-Control-Expose-Headers", "ETag,Last-Modified,Cache-Control,Content-Type,Content-Length,WWW-Authenticate,X-Experience-API-Version,X-Experience-API-Consistent-Through");
        }
        #endregion

        #region SaveAgentProfileDocument
        public String SaveAgentProfileDocument(Stream stream)
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String json;
                String errorMessage;
                if (!_ValidateAgentProfileAPIRequest(out errorMessage, stream, out json))
                {
                    return errorMessage;
                }

                try
                {
                    DateTime dtUpdated;
                    String updated = HttpContext.Current.Request.Headers.Get("updated");
                    if (!String.IsNullOrWhiteSpace(updated))
                    {
                        dtUpdated = Convert.ToDateTime(updated);
                    }
                    else
                    {
                        dtUpdated = AsentiaSessionState.UtcNow;
                    }

                    String contentType = String.Empty;
                    if (HttpContext.Current.Request.ContentType != null)
                    {
                        contentType = HttpContext.Current.Request.ContentType;
                    }

                    // deserialize and serialize the agent so that to generate json string
                    // in proper order (order of the properties). It is useful for comparison.
                    JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                    jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    Actor actor = JsonConvert.DeserializeObject<Actor>(HttpContext.Current.Request.QueryString["agent"]);
                    String agent = JsonConvert.SerializeObject(actor, Formatting.None, jsonSerializerSettings);

                    DataTinCanProfile dataTinCanProfile = new DataTinCanProfile();
                    dataTinCanProfile.IdEndpoint = this._Context.IdEndpoint;
                    dataTinCanProfile.ProfileId = HttpContext.Current.Request.QueryString["profileId"];
                    dataTinCanProfile.Agent = agent;
                    dataTinCanProfile.DtUpdated = dtUpdated;
                    dataTinCanProfile.ContentType = contentType;
                    if (json != null)
                    {
                        dataTinCanProfile.DocContents = Encoding.UTF8.GetBytes(json);
                    }
                    else
                    {
                        using (BinaryReader reader = new BinaryReader(stream))
                        {
                            dataTinCanProfile.DocContents = reader.ReadBytes(HttpContext.Current.Request.ContentLength);
                        }
                    }

                    dataTinCanProfile.Save(1);

                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                    return null;
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #region PostAgentProfileDocument
        public String PostAgentProfileDocument(Stream stream)
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String json;
                String errorMessage;
                if (!_ValidateAgentProfileAPIRequest(out errorMessage, stream, out json))
                {
                    return errorMessage;
                }

                try
                {
                    /* if document already exists and both documents have content type application/json then they will be merged.
                     * if both documents don't have content type application/json then the request will be rejected.
                     * if document does not already exist then treat the request the same as it would a PUT request
                     * and store the document being posted.
                     */

                    String profileId = HttpContext.Current.Request.QueryString["profileId"];

                    // deserialize and serialize the agent so that to generate json string
                    // in proper order (order of the properties). It is useful for comparison.
                    JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                    jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    Actor actor = JsonConvert.DeserializeObject<Actor>(HttpContext.Current.Request.QueryString["agent"]);
                    String agent = JsonConvert.SerializeObject(actor, Formatting.None, jsonSerializerSettings);

                    DataTinCanProfile dataTinCanProfile;
                    try
                    {
                        dataTinCanProfile = new DataTinCanProfile(1, this._Context.IdEndpoint, profileId, null, agent);
                    }
                    catch (DatabaseDetailsNotFoundException)
                    {
                        dataTinCanProfile = null;
                    }
                    catch (DatabaseCallerPermissionException)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return _GlobalResources.YouAreNotAuthorizedToUpdateThisRecord;
                    }

                    // document exists
                    if (dataTinCanProfile != null)
                    {
                        String postedDocContentType = null;
                        if (HttpContext.Current.Request.ContentType != null)
                        {
                            postedDocContentType = HttpContext.Current.Request.ContentType.ToLower();
                        }

                        // check whether both documents have content type application/json
                        if (dataTinCanProfile.ContentType.ToLower() != "application/json" || postedDocContentType != "application/json")
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                            return _GlobalResources.ContentTypeShouldBeJSONToMergeDocuments;
                        }

                        String originalDocJson = Encoding.UTF8.GetString(dataTinCanProfile.DocContents);

                        Dictionary<string, object> postedDocument = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                        Dictionary<string, object> originalDocument = JsonConvert.DeserializeObject<Dictionary<string, object>>(originalDocJson);

                        // create another dictionary for case insensitive comparison based on the original document
                        Dictionary<string, object> mergedDictionary = new Dictionary<string, object>(originalDocument, StringComparer.OrdinalIgnoreCase);

                        // merge JSON (only top level properties will be merged)
                        foreach (KeyValuePair<string, object> keyValue in postedDocument)
                        {
                            if (mergedDictionary.ContainsKey(keyValue.Key))
                            {
                                mergedDictionary[keyValue.Key] = keyValue.Value;
                            }
                            else
                            {
                                mergedDictionary.Add(keyValue.Key, keyValue.Value);
                            }
                        }

                        json = JsonConvert.SerializeObject(mergedDictionary, Formatting.None, jsonSerializerSettings);
                    }
                    else
                    {
                        dataTinCanProfile = new DataTinCanProfile();
                        dataTinCanProfile.ProfileId = profileId;
                        dataTinCanProfile.Agent = agent;
                    }

                    DateTime dtUpdated;
                    String updated = HttpContext.Current.Request.Headers.Get("updated");
                    if (!String.IsNullOrWhiteSpace(updated))
                    {
                        dtUpdated = Convert.ToDateTime(updated);
                    }
                    else
                    {
                        dtUpdated = AsentiaSessionState.UtcNow;
                    }

                    String contentType = String.Empty;
                    if (HttpContext.Current.Request.ContentType != null)
                    {
                        contentType = HttpContext.Current.Request.ContentType;
                    }

                    dataTinCanProfile.DtUpdated = dtUpdated;
                    dataTinCanProfile.ContentType = contentType;
                    dataTinCanProfile.IdEndpoint = _Context.IdEndpoint;
                    if (json != null)
                    {
                        dataTinCanProfile.DocContents = Encoding.UTF8.GetBytes(json);
                    }
                    else
                    {
                        using (BinaryReader reader = new BinaryReader(stream))
                        {
                            dataTinCanProfile.DocContents = reader.ReadBytes(HttpContext.Current.Request.ContentLength);
                        }
                    }

                    dataTinCanProfile.Save(1);

                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;
                    return null;
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #region RetrieveAgentProfileDocument
        public Stream RetrieveAgentProfileDocument()
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String profileId = HttpContext.Current.Request.QueryString["profileId"];

                if (String.IsNullOrWhiteSpace(HttpContext.Current.Request.QueryString["agent"]))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.AgentIsMissing));
                }

                // deserialize and serialize the agent so that to generate json string
                // in proper order (order of the properties). It is useful for comparison.
                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                Actor actor = JsonConvert.DeserializeObject<Actor>(HttpContext.Current.Request.QueryString["agent"]);
                String agent = JsonConvert.SerializeObject(actor, Formatting.None, jsonSerializerSettings);

                DateTime? since = null;
                if (HttpContext.Current.Request.QueryString["since"] != null)
                {
                    try
                    {
                        since = Convert.ToDateTime(HttpContext.Current.Request.QueryString["since"]);
                    }
                    catch
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                        return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.TheValueOfParameterSinceIsMalformed));
                    }
                }

                // if "profileId" parameter is passed then return single document otherwise return the available ids.
                if (profileId == null)
                {
                    try
                    {
                        List<String> ids = new List<string>();
                        DataTinCanProfile dataTinCanProfile = new DataTinCanProfile();
                        dataTinCanProfile.Agent = agent;
                        dataTinCanProfile.IdEndpoint = _Context.IdEndpoint;
                        DataTable dataTable = dataTinCanProfile.GetProfiles(1, since);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            ids.Add(row["profileId"].ToString());
                        }

                        if (ids.Count > 0)
                        {
                            String json = JsonConvert.SerializeObject(ids);
                            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
                            return new MemoryStream(Encoding.UTF8.GetBytes(json));
                        }

                        return null;
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return new MemoryStream(Encoding.UTF8.GetBytes(cpEx.Message));
                    }
                }
                else
                {
                    try
                    {
                        DataTinCanProfile dataTinCanProfile = new DataTinCanProfile(1, this._Context.IdEndpoint, profileId, null, agent);
                        WebOperationContext.Current.OutgoingResponse.Headers.Add("updated", dataTinCanProfile.DtUpdated.ToString("o"));
                        if (!String.IsNullOrWhiteSpace(dataTinCanProfile.ContentType))
                        {
                            WebOperationContext.Current.OutgoingResponse.ContentType = dataTinCanProfile.ContentType;
                        }

                        return new MemoryStream(dataTinCanProfile.DocContents);
                    }
                    catch (DatabaseDetailsNotFoundException dnfEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                        return new MemoryStream(Encoding.UTF8.GetBytes(dnfEx.Message));
                    }
                    catch (DatabaseCallerPermissionException cpEx)
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                        return new MemoryStream(Encoding.UTF8.GetBytes(cpEx.Message));
                    }
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return new MemoryStream(Encoding.UTF8.GetBytes(_GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters));
            }
        }
        #endregion

        #region DeleteAgentProfileDocument
        public String DeleteAgentProfileDocument()
        {
            // if X-Experience-API-Version is not in request header then set it to 1.0.0 otherwise set the incoming header value of it.
            string version = !string.IsNullOrWhiteSpace(WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"]) ? WebOperationContext.Current.IncomingRequest.Headers["X-Experience-API-Version"] : "1.0.0";

            // add the X-Experience-API-Version in the response header.
            WebOperationContext.Current.OutgoingResponse.Headers.Add("X-Experience-API-Version", version);

            if (_AuthenticateRequest())
            {
                String profileId = HttpContext.Current.Request.QueryString["profileId"];
                String agent = HttpContext.Current.Request.QueryString["agent"];

                if (String.IsNullOrWhiteSpace(profileId))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return _GlobalResources.ProfileIDIsMissing;
                }

                if (String.IsNullOrWhiteSpace(agent))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    return _GlobalResources.ActivityIDIsMissing;
                }

                // deserialize and serialize the agent so that to generate json string
                // in proper order (order of the properties). It is useful for comparison.
                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                Actor actor = JsonConvert.DeserializeObject<Actor>(agent);
                agent = JsonConvert.SerializeObject(actor, Formatting.None, jsonSerializerSettings);

                try
                {
                    DataTinCanProfile.Delete(1, this._Context.IdEndpoint, profileId, null, agent);
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;

                    return null;
                }
                catch (DatabaseDetailsNotFoundException dnfEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    return dnfEx.Message;
                }
                catch (DatabaseCallerPermissionException cpEx)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Forbidden;
                    return cpEx.Message;
                }
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;
                return _GlobalResources.ForbiddenUnableToAuthenticateUsingOAuthParameters;
            }
        }
        #endregion

        #endregion

        #region About
        public About GetLRSInfo()
        {
            // authentication is not required to access this resource.
            return new About();
        }
        #endregion

        #endregion

        #region Private Methods

        #region _AuthenticateRequest
        private bool _AuthenticateRequest()
        {
            bool isAuthenticated = false;
            try
            {
                // as discussed with joe,For internal launching there will be no Authorization Checking(According to Id user)
                // we can also check the same using AsentiaSessionState.CurrentlyLaunchedDataLessonId property
                if (AsentiaSessionState.IdSiteUser > 1)
                {
                    isAuthenticated = true;
                    this._Context = new OAuthContext();
                }
                else
                {
                    string authorization = string.Empty;
                    if (WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Authorization] != null)
                    {
                        authorization = WebOperationContext.Current.IncomingRequest.Headers[HttpRequestHeader.Authorization];
                    }
                    else
                    {
                        var queryStringParameters = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters;

                        string[] keys = { "oauth_consumer_key", "oauth_nonce", "oauth_signature_method", "oauth_timestamp", "oauth_version", "oauth_signature" };

                        authorization = "OAuth realm=\"\",";

                        foreach (string key in keys)
                        {
                            if (queryStringParameters[key] != null && queryStringParameters[key] != "")
                            {
                                authorization = authorization + key + "=\"" + queryStringParameters[key] + "\"" + ",";
                            }
                            else if (key == "oauth_version")
                            {
                                authorization = authorization + key + "\"=1.0,\"";
                            }
                        }
                        authorization = authorization.TrimEnd(',');
                    }
                    if (authorization.Substring(0, 5).Contains("Basic"))
                    {
                        byte[] data = Convert.FromBase64String(authorization.Substring(6, authorization.Length - 6));
                        string decodedString = Encoding.UTF8.GetString(data);
                        string[] keySecretValues = decodedString.Split(':');
                        string key = keySecretValues[0];
                        string secret = keySecretValues[1];
                        this._Context = new OAuthContext();
                        ConsumerStore consumer = new ConsumerStore();
                        this._Context.ConsumerKey = key;
                        consumer.IsConsumer(this._Context);
                        isAuthenticated = true;
                    }
                    else
                    {
                        if (authorization != null)
                        {
                            // create the URI
                            string scheme = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.BaseUri.Scheme;
                            string host = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.BaseUri.Host;
                            string pathAndQuery = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.RequestUri.PathAndQuery;
                            Uri uri = new Uri(scheme + "://" + host + pathAndQuery);
                            _Context = (OAuthContext)new OAuthContextBuilder().CustomFromUri(WebOperationContext.Current.IncomingRequest.Method, uri, authorization);
                            OAuthServicesLocator.Provider.AccessProtectedResourceRequest(_Context);

                            isAuthenticated = true;
                        }
                    }
                }
            }
            catch (OAuthException)
            {; }
            catch (Exception)
            {; }

            return isAuthenticated;
        }
        #endregion

        #region Statement API

        #region _ValidateGetRequest
        private bool _ValidateGetRequest(out String errorMessage)
        {
            String statementId = HttpContext.Current.Request.QueryString["statementId"];
            String voidedStatementId = HttpContext.Current.Request.QueryString["voidedStatementId"];

            // check whether both 'statementId' and 'voidedStatementId' parameters have been provided.
            if (statementId != null && voidedStatementId != null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                errorMessage = _GlobalResources.RequestShouldNotContainBothStatementIDAndVoidedStatementID;
                return false;
            }

            // check whether 'statementId' or 'voidedStatementId' parameter has been provided
            // and other parameters (besides 'format' and 'attachments') have also been provided.
            if (statementId != null || voidedStatementId != null)
            {
                if (HttpContext.Current.Request.QueryString["agent"] != null ||
                    HttpContext.Current.Request.QueryString["verb"] != null ||
                    HttpContext.Current.Request.QueryString["activity"] != null ||
                    HttpContext.Current.Request.QueryString["registration"] != null ||
                    HttpContext.Current.Request.QueryString["related_activities"] != null ||
                    HttpContext.Current.Request.QueryString["related_agents"] != null ||
                    HttpContext.Current.Request.QueryString["since"] != null ||
                    HttpContext.Current.Request.QueryString["until"] != null ||
                    HttpContext.Current.Request.QueryString["limit"] != null ||
                    HttpContext.Current.Request.QueryString["ascending"] != null)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.OtherParametersBesidesAttachmentsOrFormatShouldNot;
                    return false;
                }
            }

            // parameter 'agent'
            if (HttpContext.Current.Request.QueryString["agent"] != null)
            {
                bool isAgentParamValid = true;
                try
                {
                    Actor actor = JsonConvert.DeserializeObject<Actor>(HttpContext.Current.Request.QueryString["agent"]);
                    if (actor == null || (actor.Mbox == null && actor.Mbox_sha1sum == null && actor.Openid == null && actor.Account == null))
                    {
                        isAgentParamValid = false;
                    }
                }
                catch
                {
                    isAgentParamValid = false;
                }

                if (!isAgentParamValid)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheValueOfParameterAgentIsMalformed;
                    return false;
                }
            }

            // parameter 'related_activities'
            if (HttpContext.Current.Request.QueryString["related_activities"] != null)
            {
                try
                {
                    Convert.ToBoolean(HttpContext.Current.Request.QueryString["related_activities"]);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheBooleanValueOfParameterRelatedActivitiesIsMalformed;
                    return false;
                }
            }

            // parameter 'related_agents'
            if (HttpContext.Current.Request.QueryString["related_agents"] != null)
            {
                try
                {
                    Convert.ToBoolean(HttpContext.Current.Request.QueryString["related_agents"]);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheBooleanValueOfParameterRelatedAgentsIsMalformed;
                    return false;
                }
            }

            // parameter 'since'
            if (HttpContext.Current.Request.QueryString["since"] != null)
            {
                try
                {
                    DateTime.ParseExact(HttpContext.Current.Request.QueryString["since"], "o", CultureInfo.InvariantCulture);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheValueOfParameterSinceIsMalformed;
                    return false;
                }
            }

            // parameter 'until'
            if (HttpContext.Current.Request.QueryString["until"] != null)
            {
                try
                {
                    DateTime.ParseExact(HttpContext.Current.Request.QueryString["until"], "o", CultureInfo.InvariantCulture);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheDateValueOfParameterUntilIsMalformed;
                    return false;
                }
            }

            // parameter 'limit'
            if (HttpContext.Current.Request.QueryString["limit"] != null)
            {
                bool isValid = true;
                try
                {
                    int limit = Convert.ToInt32(HttpContext.Current.Request.QueryString["limit"]);
                    if (limit < 0)
                    {
                        isValid = false;
                    }
                }
                catch
                {
                    isValid = false;
                }

                if (!isValid)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheIntegerValueOfParameterLimitIsMalformed;
                    return false;
                }
            }

            // parameter 'start' -- This is custom parameter. We add this parameter to "more" property of StatementResult object.
            if (HttpContext.Current.Request.QueryString["start"] != null)
            {
                int start = Convert.ToInt32(HttpContext.Current.Request.QueryString["start"]);
                if (start <= 0)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheIntegerValueOfParameterStartIsMalformed;
                    return false;
                }
            }

            // parameter 'format'
            if (HttpContext.Current.Request.QueryString["format"] != null)
            {
                string format = HttpContext.Current.Request.QueryString["format"].ToLower();
                if (!(format == "ids" || format == "exact" || format == "canonical"))
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheValueOfParameterFormatIsMalformed;
                    return false;
                }
            }

            // parameter 'attachments'
            if (HttpContext.Current.Request.QueryString["attachments"] != null)
            {
                try
                {
                    Convert.ToBoolean(HttpContext.Current.Request.QueryString["attachments"]);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheBooleanValueOfParameterAttachmentsIsMalformed;
                    return false;
                }
            }

            // parameter 'ascending'
            if (HttpContext.Current.Request.QueryString["ascending"] != null)
            {
                try
                {
                    Convert.ToBoolean(HttpContext.Current.Request.QueryString["ascending"]);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    errorMessage = _GlobalResources.TheBooleanValueOfParameterAscendingIsMalformed;
                    return false;
                }
            }

            errorMessage = null;
            return true;
        }
        #endregion

        #region _CompareStatements
        private bool _CompareStatements(Statement firstStatement, Statement secondStatement)
        {
            // compare them without Timestamp property because we don't know whether Timestamp
            // property was previously provided by client or it was generated on the server side.
            // also don't compare Stored property because it is always genetated by the server.

            firstStatement.Timestamp = null;
            firstStatement.Stored = null;
            secondStatement.Timestamp = null;
            secondStatement.Stored = null;

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;

            String strFirstStatement = JsonConvert.SerializeObject(firstStatement, Formatting.None, jsonSerializerSettings);
            String strSecondStatement = JsonConvert.SerializeObject(secondStatement, Formatting.None, jsonSerializerSettings);

            if (strFirstStatement == strSecondStatement)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region _GetMoreResultsIRL
        private String _GetMoreResultsIRL(int start)
        {
            String more = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.RequestUri.AbsolutePath;
            NameValueCollection collection = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());

            // check whether parameter 'start' already exists.
            if (HttpContext.Current.Request.QueryString["start"] != null)
            {
                collection.Remove("start");
            }

            if (collection != null && collection.Count > 0)
            {
                more += "?" + collection + "&start=" + start;
            }
            else
            {
                more += "?start=" + start;
            }

            return more;
        }
        #endregion

        #region _GetTinCanDataTable
        private DataTable _GetTinCanDataTable(List<Statement> statements, out DataTable contextActivities)
        {
            contextActivities = new DataTable();
            contextActivities.Columns.Add("statementId", typeof(string));
            contextActivities.Columns.Add("activityId", typeof(string));
            contextActivities.Columns.Add("activityType", typeof(string));
            contextActivities.Columns.Add("objectType", typeof(string));

            DataTable statementsTable = new DataTable();
            statementsTable.Columns.Add("statementId", typeof(string));
            statementsTable.Columns.Add("parentStatementId", typeof(string));
            statementsTable.Columns.Add("actor", typeof(string));
            statementsTable.Columns.Add("verbId", typeof(string));
            statementsTable.Columns.Add("verb", typeof(string));
            statementsTable.Columns.Add("activityId", typeof(string));

            statementsTable.Columns.Add("mboxObject", typeof(string));
            statementsTable.Columns.Add("mboxSha1SumObject", typeof(string));
            statementsTable.Columns.Add("openIdObject", typeof(string));
            statementsTable.Columns.Add("accountObject", typeof(string));

            statementsTable.Columns.Add("mboxActor", typeof(string));
            statementsTable.Columns.Add("mboxSha1SumActor", typeof(string));
            statementsTable.Columns.Add("openIdActor", typeof(string));
            statementsTable.Columns.Add("accountActor", typeof(string));

            statementsTable.Columns.Add("mboxAuthority", typeof(string));
            statementsTable.Columns.Add("mboxSha1SumAuthority", typeof(string));
            statementsTable.Columns.Add("openIdAuthority", typeof(string));
            statementsTable.Columns.Add("accountAuthority", typeof(string));

            statementsTable.Columns.Add("mboxTeam", typeof(string));
            statementsTable.Columns.Add("mboxSha1SumTeam", typeof(string));
            statementsTable.Columns.Add("openIdTeam", typeof(string));
            statementsTable.Columns.Add("accountTeam", typeof(string));

            statementsTable.Columns.Add("mboxInstructor", typeof(string));
            statementsTable.Columns.Add("mboxSha1SumInstructor", typeof(string));
            statementsTable.Columns.Add("openIdInstructor", typeof(string));
            statementsTable.Columns.Add("accountInstructor", typeof(string));

            statementsTable.Columns.Add("object", typeof(string));
            statementsTable.Columns.Add("registration", typeof(string));
            statementsTable.Columns.Add("statement", typeof(string));
            statementsTable.Columns.Add("statementIdToBeVoided", typeof(string));
            statementsTable.Columns.Add("scoreScaled", typeof(float));
            foreach (Statement statement in statements)
            {
                bool statementExists = false;

                // check whether this statement already exists.
                // if it already exists then compare it with the one already stored.
                // if both are same then ignore it (don't modify the statement or any other object).
                if (!String.IsNullOrWhiteSpace(statement.Id))
                {
                    DataTinCan dataTinCan = null;
                    try
                    {
                        dataTinCan = new DataTinCan(1, _Context.IdEndpoint, statement.Id, false);
                        statementExists = true;
                    }
                    catch (DatabaseDetailsNotFoundException)
                    {
                        statementExists = false;
                    }

                    if (statementExists)
                    {
                        Statement storedStatement = JsonConvert.DeserializeObject<Statement>(dataTinCan.Statement);
                        if (!this._CompareStatements(statement, storedStatement))
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Conflict;
                            WebOperationContext.Current.OutgoingResponse.StatusDescription = _GlobalResources.CannotModifyStatement.Replace("##statementId##", statement.Id);
                            return null;
                        }
                    }
                }
                else
                {
                    // generate statement id
                    statement.Id = Guid.NewGuid().ToString();
                }

                // if a statement does not exist then add it otherwise don't add/update.
                if (!statementExists)
                {
                    // check whether this is a voiding statement.
                    if (statement.Verb.IsVoided())
                    {
                        // check whether the statement to be voided is a voiding statement.
                        DataTinCan dataTinCan = new DataTinCan(1, _Context.IdEndpoint, ((StatementRef)statement.Object).Id, false);
                        if (dataTinCan.IsVoidingStatement)
                        {
                            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                            WebOperationContext.Current.OutgoingResponse.StatusDescription = _GlobalResources.VoidingStatementCannotBeVoided;
                            return null;
                        }
                    }

                    // set timestamp if not provided.
                    if (statement.Timestamp == null)
                    {
                        statement.Timestamp = AsentiaSessionState.UtcNow;
                    }

                    // set stored time
                    statement.Stored = AsentiaSessionState.UtcNow;

                    this._AddStatementToTable(statement, null, statementsTable, contextActivities);

                    // add sub statement as a separate row.
                    if (((StatementTarget)statement.Object).ObjectType.ToLower() == "substatement")
                    {
                        Statement subStatement = (Statement)statement.Object;
                        string parentStatementId = statement.Id;
                        this._AddStatementToTable(subStatement, parentStatementId, statementsTable, contextActivities);
                    }
                }
            }

            return statementsTable;
        }
        #endregion

        #region _AddStatementToTable
        private void _AddStatementToTable(Statement statement, string parentStatementId, DataTable statementsTable, DataTable contextActivities)
        {
            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;

            string statementId = statement.Id;

            // a sub-statement does not have id.
            // however, we need to store id of the sub-statement in a db column so that to be able to get the PK value
            // of the sub-statement on the basis of statement id when saving context activities of the sub-statement.
            // however, statement id will not be saved with the sub-statement JSON.
            if (String.IsNullOrWhiteSpace(statementId))
            {
                statementId = Guid.NewGuid().ToString();
            }

            string actor = JsonConvert.SerializeObject(statement.Actor, Formatting.None, jsonSerializerSettings);
            string verb = JsonConvert.SerializeObject(statement.Verb, Formatting.None, jsonSerializerSettings);
            string verbId = statement.Verb.Id;
            string activityId = null;
            double? scoreScaled = null;
            if (statement.Result != null && statement.Result.Score != null)
            {
                scoreScaled = statement.Result.Score.Scaled;
            }


            string mboxObject = null;
            string mboxSha1SumObject = null;
            string openIdObject = null;
            string accountObject = null;

            string mboxActor = statement.Actor.Mbox;
            string mboxSha1SumActor = statement.Actor.Mbox_sha1sum;
            string openIdActor = statement.Actor.Openid;
            string accountActor = null;
            if (statement.Actor.Account != null)
            {
                accountActor = JsonConvert.SerializeObject(statement.Actor.Account, Formatting.None, jsonSerializerSettings);
            }

            string mboxAuthority = null;
            string mboxSha1SumAuthority = null;
            string openIdAuthority = null;
            string accountAuthority = null;

            string mboxTeam = null;
            string mboxSha1SumTeam = null;
            string openIdTeam = null;
            string accountTeam = null;

            string mboxInstructor = null;
            string mboxSha1SumInstructor = null;
            string openIdInstructor = null;
            string accountInstructor = null;

            string obj = JsonConvert.SerializeObject(statement.Object, Formatting.None, jsonSerializerSettings);
            string registration = null;
            string statementJson = JsonConvert.SerializeObject(statement, Formatting.None, jsonSerializerSettings);
            string statementIdToBeVoided = null;

            if (((StatementTarget)statement.Object).ObjectType.ToLower() == "activity")
            {
                activityId = ((Activity)statement.Object).Id;
            }

            string statementObjectType = ((StatementTarget)statement.Object).ObjectType.ToLower();
            if (statementObjectType == "agent" || statementObjectType == "group")
            {
                Actor objActor = ((Actor)statement.Object);
                if (objActor != null)
                {
                    mboxObject = objActor.Mbox;
                    mboxSha1SumObject = objActor.Mbox_sha1sum;
                    openIdObject = objActor.Openid;
                    if (objActor.Account != null)
                    {
                        accountObject = JsonConvert.SerializeObject(objActor.Account, Formatting.None, jsonSerializerSettings);
                    }
                }
            }

            if (statement.Authority != null)
            {
                mboxAuthority = statement.Authority.Mbox;
                mboxSha1SumAuthority = statement.Authority.Mbox_sha1sum;
                openIdAuthority = statement.Authority.Openid;
                if (statement.Authority.Account != null)
                {
                    accountAuthority = JsonConvert.SerializeObject(statement.Authority.Account, Formatting.None, jsonSerializerSettings);
                }
            }

            if (statement.Context != null)
            {
                registration = statement.Context.Registration;

                if (statement.Context.Team != null)
                {
                    mboxTeam = statement.Context.Team.Mbox;
                    mboxSha1SumTeam = statement.Context.Team.Mbox_sha1sum;
                    openIdTeam = statement.Context.Team.Openid;
                    if (statement.Context.Team.Account != null)
                    {
                        accountTeam = JsonConvert.SerializeObject(statement.Context.Team.Account, Formatting.None, jsonSerializerSettings);
                    }
                }

                if (statement.Context.Instructor != null)
                {
                    mboxInstructor = statement.Context.Instructor.Mbox;
                    mboxSha1SumInstructor = statement.Context.Instructor.Mbox_sha1sum;
                    openIdInstructor = statement.Context.Instructor.Openid;
                    if (statement.Context.Instructor.Account != null)
                    {
                        accountInstructor = JsonConvert.SerializeObject(statement.Context.Instructor.Account, Formatting.None, jsonSerializerSettings);
                    }
                }

                // add context activities
                if (statement.Context.ContextActivities != null)
                {
                    // add category activities
                    if (statement.Context.ContextActivities.Category != null)
                    {
                        string activityType = "Category";
                        foreach (Activity activity in statement.Context.ContextActivities.Category)
                        {
                            if (activity != null)
                            {
                                contextActivities.Rows.Add(statementId, activity.Id, activityType, activity.ObjectType);
                            }
                        }
                    }

                    // add grouping activities
                    if (statement.Context.ContextActivities.Grouping != null)
                    {
                        string activityType = "Grouping";
                        foreach (Activity activity in statement.Context.ContextActivities.Grouping)
                        {
                            if (activity != null)
                            {
                                contextActivities.Rows.Add(statementId, activity.Id, activityType, activity.ObjectType);
                            }
                        }
                    }

                    // add parent activities
                    if (statement.Context.ContextActivities.Parent != null)
                    {
                        string activityType = "Parent";
                        foreach (Activity activity in statement.Context.ContextActivities.Parent)
                        {
                            if (activity != null)
                            {
                                contextActivities.Rows.Add(statementId, activity.Id, activityType, activity.ObjectType);
                            }
                        }
                    }

                    // add other activities
                    if (statement.Context.ContextActivities.Other != null)
                    {
                        string activityType = "Other";
                        foreach (Activity activity in statement.Context.ContextActivities.Other)
                        {
                            if (activity != null)
                            {
                                contextActivities.Rows.Add(statementId, activity.Id, activityType, activity.ObjectType);
                            }
                        }
                    }
                }
            }

            if (statement.Verb.IsVoided())
            {
                statementIdToBeVoided = ((StatementRef)statement.Object).Id;
            }

            statementsTable.Rows.Add(statementId, parentStatementId, actor, verbId, verb, activityId,
                mboxObject, mboxSha1SumObject, openIdObject, accountObject,
                mboxActor, mboxSha1SumActor, openIdActor, accountActor,
                mboxAuthority, mboxSha1SumAuthority, openIdAuthority, accountAuthority,
                mboxTeam, mboxSha1SumTeam, openIdTeam, accountTeam,
                mboxInstructor, mboxSha1SumInstructor, openIdInstructor, accountInstructor,
                obj, registration, statementJson, statementIdToBeVoided, scoreScaled);
        }
        #endregion

        #region _SetActorPropertiesAsNullExceptId
        private void _SetActorPropertiesAsNullExceptId(Actor actor)
        {
            // set all properties as null except identification information (mbox & objectType).
            actor.Name = null;
            actor.Mbox_sha1sum = null;
            actor.Openid = null;
            actor.Account = null;
            actor.Member = null;
        }
        #endregion

        #region _SetActivityPropertiesAsNullExceptId
        private void _SetActivityPropertiesAsNullExceptId(Activity activity)
        {
            // set all properties as null except identification information (id & objectType).
            activity.Definition = null;
        }
        #endregion

        #region _SetObjectPropertiesAsNullExceptId
        private void _SetObjectPropertiesAsNullExceptId(Object obj)
        {
            String objectType = ((StatementTarget)obj).ObjectType.ToLower();
            switch (objectType)
            {
                case "agent":
                case "group":
                    this._SetActorPropertiesAsNullExceptId((Actor)obj);
                    break;
                case "activity":
                    this._SetActivityPropertiesAsNullExceptId((Activity)obj);
                    break;
            }
        }
        #endregion

        #endregion

        #region State API

        #region _ValidateStateAPIPutPostRequest
        /// <summary>
        /// Validate state API put and post request.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="json">The json.</param>
        /// <param name="itemDic">The item dic.</param>
        /// <param name="MultipleTimesReadableStream">The multiple times readable stream.</param>
        /// <returns></returns>
        private bool _ValidateStateAPIPutPostRequest(out String message, Stream stream, out String json, Dictionary<string, string> itemDic = null, MemoryStream MultipleTimesReadableStream = null)
        {
            json = null;

            String stateId = HttpContext.Current.Request.QueryString["stateId"];
            String activityId = HttpContext.Current.Request.QueryString["activityId"];
            String agent = HttpContext.Current.Request.QueryString["agent"];
            String registration = HttpContext.Current.Request.QueryString["registration"];

            // following code is used to find required parameters from request payout because they are not present in querystring (in storyline packages)
            // code start
            string contentType = string.Empty;
            try
            {
                contentType = WebOperationContext.Current.IncomingRequest.ContentType;
            }
            catch
            {
                try
                {
                    contentType = HttpContext.Current.Request.ContentType;
                }
                catch
                {
                    contentType = string.Empty;
                }
            }

            if (String.IsNullOrWhiteSpace(stateId))
            {
                try
                {
                    stateId = itemDic["stateId"];
                }
                catch (Exception e)
                {
                    stateId = string.Empty;
                }
            }

            if (String.IsNullOrWhiteSpace(activityId))
            {
                try
                {
                    activityId = itemDic["activityId"];
                }
                catch (Exception e)
                {
                    activityId = string.Empty;
                }
            }

            if (String.IsNullOrWhiteSpace(agent))
            {
                try
                {
                    agent = itemDic["agent"];
                }
                catch (Exception e)
                {
                    agent = string.Empty;
                }
            }

            if (String.IsNullOrWhiteSpace(contentType))
            {
                try
                {
                    contentType = itemDic["Content-Type"];
                }
                catch (Exception e)
                {
                    contentType = string.Empty;
                }
            }
            // code end

            // validate state id
            if (String.IsNullOrWhiteSpace(stateId))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                message = _GlobalResources.StateIDIsMissing;

                return false;
            }

            // validate activity id
            if (String.IsNullOrWhiteSpace(activityId))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                message = _GlobalResources.ActivityIDIsMissing;

                return false;
            }

            // validate agent
            if (String.IsNullOrWhiteSpace(agent))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                message = _GlobalResources.AgentIsMissing;

                return false;
            }
            else
            {
                bool isAgentParamValid = true;
                Actor actor = null;
                try
                {
                    actor = JsonConvert.DeserializeObject<Actor>(agent);
                    if (actor == null || (actor.Mbox == null && actor.Mbox_sha1sum == null && actor.Openid == null && actor.Account == null))
                    {
                        isAgentParamValid = false;
                    }
                }
                catch
                {
                    isAgentParamValid = false;
                }

                if (!isAgentParamValid)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    message = _GlobalResources.TheValueOfParameterAgentIsMalformed;

                    return false;
                }
            }

            // validate udpated property
            String updated = HttpContext.Current.Request.Headers.Get("updated");
            if (!String.IsNullOrWhiteSpace(updated))
            {
                try
                {
                    Convert.ToDateTime(updated);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    message = _GlobalResources.UpdatedPropertyIsMalformed;

                    return false;
                }
            }

            // validate json
            ////String contentType = WebOperationContext.Current.IncomingRequest.ContentType;
            if (!String.IsNullOrWhiteSpace(contentType) && (contentType.ToLower().Contains("application/json")) || (contentType.ToLower().Contains("text/plain")))
            {
                try
                {
                    StreamReader streamReader = new StreamReader(MultipleTimesReadableStream);
                    json = streamReader.ReadToEnd();
                    MultipleTimesReadableStream.Position = 0;
                    json = HttpUtility.UrlDecode(json);
                    JsonConvert.DeserializeObject(json);
                }
                catch
                {
                    try
                    {
                        if (!String.IsNullOrWhiteSpace(itemDic["content"]))
                        {
                            json = itemDic["content"];
                        }
                    }
                    catch
                    {
                        WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                        message = _GlobalResources.MalformedJSONCouldNotDeserializeStringToObject;

                        return false;
                    }
                }
            }

            message = null;
            return true;
        }
        #endregion

        #region _ValidateStateAPIDeleteGetRequest
        /// <summary>
        /// Validate state API delete AND get request.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="itemDic">The item dic.</param>
        /// <returns></returns>
        private bool _ValidateStateAPIDeleteGetRequest(out String message, Dictionary<string, string> itemDic = null)
        {
            String activityId = HttpContext.Current.Request.QueryString["activityId"];
            String agent = HttpContext.Current.Request.QueryString["agent"];

            // following code is used to find required parameters from request payout because they are not present in querystring (in storyline packages)
            // code start
            if (String.IsNullOrWhiteSpace(activityId))
            {
                try
                {
                    activityId = itemDic["activityId"];
                }
                catch (Exception e)
                {
                    activityId = string.Empty;
                }
            }

            if (String.IsNullOrWhiteSpace(agent))
            {
                try
                {
                    agent = itemDic["agent"];
                }
                catch (Exception e)
                {
                    agent = string.Empty;
                }
            }
            // code end

            // validate activity id
            if (String.IsNullOrWhiteSpace(activityId))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                message = _GlobalResources.ActivityIDIsMissing;
                return false;
            }

            // validate agent
            if (String.IsNullOrWhiteSpace(agent))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                message = _GlobalResources.AgentIsMissing;
                return false;
            }
            else
            {
                bool isAgentParamValid = true;
                Actor actor = null;
                try
                {
                    actor = JsonConvert.DeserializeObject<Actor>(agent);
                    if (actor == null || (actor.Mbox == null && actor.Mbox_sha1sum == null && actor.Openid == null && actor.Account == null))
                    {
                        isAgentParamValid = false;
                    }
                }
                catch
                {
                    isAgentParamValid = false;
                }

                if (!isAgentParamValid)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    message = _GlobalResources.TheValueOfParameterAgentIsMalformed;
                    return false;
                }
            }

            message = null;
            return true;
        }
        #endregion

        #endregion

        #region Activity Profile API

        #region _ValidateActivityProfileAPIRequest
        private bool _ValidateActivityProfileAPIRequest(out String message, Stream stream, out String json)
        {
            json = null;

            String profileId = HttpContext.Current.Request.QueryString["profileId"];
            String activityId = HttpContext.Current.Request.QueryString["activityId"];

            // validate profile id
            if (String.IsNullOrWhiteSpace(profileId))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                message = _GlobalResources.ProfileIDIsMissing;

                return false;
            }

            // validate activity id
            if (String.IsNullOrWhiteSpace(activityId))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                message = _GlobalResources.ActivityIDIsMissing;

                return false;
            }

            // validate udpated property
            String updated = HttpContext.Current.Request.Headers.Get("updated");
            if (!String.IsNullOrWhiteSpace(updated))
            {
                try
                {
                    Convert.ToDateTime(updated);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    message = _GlobalResources.UpdatedPropertyIsMalformed;

                    return false;
                }
            }

            // validate json
            String contentType = WebOperationContext.Current.IncomingRequest.ContentType;
            if (contentType != null && contentType.ToLower() == "application/json")
            {
                try
                {
                    StreamReader reader = new StreamReader(stream);
                    json = reader.ReadToEnd();
                    reader.Close();
                    JsonConvert.DeserializeObject(json);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    message = _GlobalResources.MalformedJSONCouldNotDeserializeStringToObject;

                    return false;
                }
            }

            message = null;
            return true;
        }
        #endregion

        #endregion

        #region Agent Profile API

        #region _ValidateAgentProfileAPIRequest
        private bool _ValidateAgentProfileAPIRequest(out String message, Stream stream, out String json)
        {
            json = null;

            String profileId = HttpContext.Current.Request.QueryString["profileId"];
            String agent = HttpContext.Current.Request.QueryString["agent"];

            // validate profile id
            if (String.IsNullOrWhiteSpace(profileId))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                message = _GlobalResources.ProfileIDIsMissing;

                return false;
            }

            // validate agent
            if (String.IsNullOrWhiteSpace(agent))
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                message = _GlobalResources.AgentIsMissing;

                return false;
            }
            else
            {
                bool isAgentParamValid = true;
                Actor actor = null;
                try
                {
                    actor = JsonConvert.DeserializeObject<Actor>(agent);
                    if (actor == null || (actor.Mbox == null && actor.Mbox_sha1sum == null && actor.Openid == null && actor.Account == null))
                    {
                        isAgentParamValid = false;
                    }
                }
                catch
                {
                    isAgentParamValid = false;
                }

                if (!isAgentParamValid)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    message = _GlobalResources.TheValueOfParameterAgentIsMalformed;

                    return false;
                }
            }

            // validate udpated property
            String updated = HttpContext.Current.Request.Headers.Get("updated");
            if (!String.IsNullOrWhiteSpace(updated))
            {
                try
                {
                    Convert.ToDateTime(updated);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    message = _GlobalResources.UpdatedPropertyIsMalformed;

                    return false;
                }
            }

            // validate json
            String contentType = WebOperationContext.Current.IncomingRequest.ContentType;
            if (contentType != null && contentType.ToLower() == "application/json")
            {
                try
                {
                    StreamReader reader = new StreamReader(stream);
                    json = reader.ReadToEnd();
                    reader.Close();
                    JsonConvert.DeserializeObject(json);
                }
                catch
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                    message = _GlobalResources.MalformedJSONCouldNotDeserializeStringToObject;

                    return false;
                }
            }

            message = null;
            return true;
        }
        #endregion

        #endregion

        #endregion
    }
}
