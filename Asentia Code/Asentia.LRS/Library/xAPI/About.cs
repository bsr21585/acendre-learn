﻿using System;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class About
    {
        #region Fields
        private string _Version = "1.0.0";
        #endregion

        #region Properties
        /// <summary>
        /// xAPI version this LRS supports.
        /// </summary>
        [DataMember(Name = "version")]
        public string Version
        {
            get { return _Version; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    this._Version = "1.0.0";
                }
                else
                {
                    this._Version = value;
                }
            }
        }

        #endregion

        #region Constructor
        public About() { }
        #endregion
    }
}