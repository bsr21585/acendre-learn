﻿using System.Collections.Generic;
using Asentia.LRS.Library.xAPI.Helper;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class Actor : StatementTarget, IValidatable
    {
        #region Constants
        protected string _ObjectType = "agent";
        #endregion

        #region Fields
        private string _Name;
        private string _Mbox;
        private string _MboxSha1sum;
        private string _OpenId;
        private AgentAccount _Account;
        private Actor[] _Member;
        #endregion

        #region Properties
        /// <summary>
        /// Object Type accessor
        /// </summary>
        [DataMember(Name = "objectType")]
        public override string ObjectType
        {
            get { return _ObjectType; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _ObjectType = "agent";
                }
                else
                {
                    _ObjectType = value.ToLower();
                }
            }
        }

        /// <summary>
        /// Array of Agent Objects
        /// </summary>
        [DataMember(Name = "member")]
        public Actor[] Member
        {
            get { return _Member; }
            set { _Member = value; }
        }

        /// <summary>
        /// Name of the actor
        /// </summary>
        [DataMember(Name = "name")]
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
            }
        }

        /// <summary>
        /// Mailbox of the actor
        /// </summary>
        [DataMember(Name = "mbox")]
        public string Mbox
        {
            get { return _Mbox; }
            set
            {
                if (value != null)
                {
                    _Mbox = value.ToLower();
                }
            }
        }

        /// <summary>
        /// Email sha1sum of the actor
        /// </summary>
        [DataMember(Name = "mbox_sha1sum")]
        public string Mbox_sha1sum
        {
            get { return _MboxSha1sum; }
            set
            {
                if (value != null)
                {
                    _MboxSha1sum = value.ToLower();
                }
            }
        }

        /// <summary>
        /// Open Id of the actor
        /// </summary>
        [DataMember(Name = "openid")]
        public string Openid
        {
            get { return _OpenId; }
            set
            {
                if (value != null)
                {
                    _OpenId = value.ToLower();
                }
            }
        }

        /// <summary>
        /// Account of the actor
        /// </summary>
        [DataMember(Name = "account")]
        public AgentAccount Account
        {
            get { return _Account; }
            set
            {
                if (value != null)
                {
                   _Account = value;
                }
            }
        }

        #endregion

        #region Constructor
        public Actor() { }
        #endregion

        #region Public Methods
        /// <summary>
        /// Validates that the object abides by its rules
        /// </summary>
        public virtual IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            if (_ObjectType == null || _ObjectType.ToLower() == "agent")
                return _ValidateAgent(earlyReturnOnFailure);
            else
                return _ValidateGroup(earlyReturnOnFailure);
        }
        #endregion

        #region Private Methods

        private IEnumerable<Messages> _ValidateAgent(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            failures.AddRange(_ValidateInverseFunctionalIdentifier(earlyReturnOnFailure));
            return failures;
        }

        private IEnumerable<Messages> _ValidateGroup(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            int properties;
            failures.AddRange(_ValidateInverseFunctionalIdentifier(earlyReturnOnFailure, out properties));
            if (failures.Capacity > 0 && earlyReturnOnFailure)
                return failures;
            if (properties == 0) //memebers are must
            {
                 if (_Member == null || _Member.Length == 0)
                 {
                     failures.Add(new Messages(Asentia.Common._GlobalResources.GroupMustBePopulated));
                     if (earlyReturnOnFailure)
                         return failures;
                 }
                 else
                     failures.AddRange(_ValidateMembers(earlyReturnOnFailure));
            }
            else if (properties == 1) //members are not must
            {
                if (_Member != null && _Member.Length > 0)
                {
                    failures.AddRange(_ValidateMembers(earlyReturnOnFailure));
                }
            }

            return failures;
        }

        private int _CountInverseFunctionalIdentifier(out bool mboxIdentifier, out bool accountIdentifier)
        {
            mboxIdentifier = accountIdentifier = false;
            int properties = 0;

            if (!string.IsNullOrWhiteSpace(_Mbox))
            {
                properties++;
                mboxIdentifier = true;
            }
            if (!string.IsNullOrWhiteSpace(_MboxSha1sum))
            {
                properties++;
            }
            if (!string.IsNullOrWhiteSpace(_OpenId))
            {
                properties++;
            }
            if (_Account != null)
            {
                properties++;
                accountIdentifier = true;
            }
            
            return properties;
        }

        private IEnumerable<Messages> _ValidateInverseFunctionalIdentifier(bool earlyReturnOnFailure)
        {
            bool mboxIdentifier;
            bool accountIdentifier;
            var failures = new List<Messages>();
            int properties = _CountInverseFunctionalIdentifier(out mboxIdentifier, out accountIdentifier);
            if (properties != 1)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.Exactly1InverseFunctionalPropertiesMustBeDefined + properties));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            if(mboxIdentifier == true)
            {
                failures.AddRange(_ValidateMBox(earlyReturnOnFailure));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            if (accountIdentifier == true)
            {
                failures.AddRange(Account.Validate(earlyReturnOnFailure));               
            }
            return failures;
        }

        private IEnumerable<Messages> _ValidateInverseFunctionalIdentifier(bool earlyReturnOnFailure, out int properties)
        {
            bool mboxIdentifier;
            bool accountIdentifier;
            var failures = new List<Messages>();
            properties = _CountInverseFunctionalIdentifier(out mboxIdentifier, out accountIdentifier);
            if (properties != 1)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.Exactly1InverseFunctionalPropertiesMustBeDefined + properties));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            if (mboxIdentifier == true)
            {
                failures.AddRange(_ValidateMBox(earlyReturnOnFailure));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            if (accountIdentifier == true)
            {
                failures.AddRange(Account.Validate(earlyReturnOnFailure));
            }
            return failures;
        }

        private IEnumerable<Messages> _ValidateMBox(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            string mboxPrefix = "mailto:";
            if (_Mbox != null)
            {
                if (!_Mbox.StartsWith(mboxPrefix))
                {
                    failures.Add(new Messages(Asentia.Common._GlobalResources.MboxValueMustBeginWithMailToPrefix));
                    if (earlyReturnOnFailure)
                    {
                        return failures;
                    }
                }
                if (!ValidationHelper.IsValidEmailAddress(_Mbox.Substring(mboxPrefix.Length)))
                {
                    failures.Add(new Messages(Asentia.Common._GlobalResources.MboxValueIsNotAValidEmail));
                }
            }

            return failures;
        }

        private IEnumerable<Messages> _ValidateMembers(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            
                  foreach (Actor a in _Member)
                {
                    if(a._ObjectType.ToLower() != "agent")
                        failures.Add(new Messages(Asentia.Common._GlobalResources.GroupCannotContainGroupObjectsInTheMemberProperty));
                    else
                        failures.AddRange(a.Validate(earlyReturnOnFailure));
                    if (earlyReturnOnFailure && failures.Count != 0)
                    {
                        return failures;
                    }
                }
           
            return failures;
        }

        #endregion       
    }
}