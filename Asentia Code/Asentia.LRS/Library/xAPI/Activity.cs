﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class Activity : StatementTarget, IValidatable
    {
        #region Fields
        private string _ObjectType = "Activity";
        private string _Id;
        private ActivityDefinition _Definition;
        #endregion

        #region Properties
        [DataMember(Name = "objectType")]
        public override string ObjectType
        {
            get { return _ObjectType; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    _ObjectType = "Activity";
                }
                else
                {
                    _ObjectType = value;
                }
            }
        }

        [DataMember(Name = "id")]
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        [DataMember(Name = "definition")]
        public ActivityDefinition Definition
        {
            get { return _Definition; }
            set { _Definition = value; }
        }
        #endregion

        #region Constructor
        public Activity() { }
        #endregion

        #region Public Methods
        public IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            if (_Id == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.ActivityDoesNotHaveAnIdentifier));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            if (_Definition != null && _Definition is IValidatable)
            {
                failures.AddRange(((IValidatable)_Definition).Validate(earlyReturnOnFailure));
                if (earlyReturnOnFailure && failures.Count > 0)
                {
                    return failures;
                }
            }
            return failures;
        }
        #endregion
    }
}
