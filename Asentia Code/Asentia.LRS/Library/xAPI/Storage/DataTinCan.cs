#region License

// The MIT License
//
// Copyright (c) 2006-2008 DevDefined Limited.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#endregion

using System;
using Asentia.Common;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.LRS.Library.xAPI.Storage
{
    public class DataTinCan
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DataTinCan()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idUser">User Id</param>
        /// <param name="idEndpoint">Endpoint Id</param>
        /// <param name="statementId">Statement Id</param>
        /// <param name="isVoided">Is Voided Statement</param>
        public DataTinCan(int idUser, int idEndpoint, string statementId, bool isVoided)
        {
            _Details(idUser, idEndpoint, statementId, isVoided);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Id of Tin Can Data.
        /// </summary>
        public int IdDataTinCan { get; set; }

        /// <summary>
        /// Parent Id  of Tin Can Data.
        /// </summary>
        public int idDataTinCanParent { get; set; }

        /// <summary>
        /// Site Id.
        /// </summary>
        public int IdSite { get; set; }

        /// <summary>
        /// Endpoint Id of the statement owner.
        /// </summary>
        public int? IdEndpoint { get; set; }

        /// <summary>
        /// Determines whether this statement was recorded using Internal API.
        /// </summary>
        public bool IsInternalAPI { get; set; }

        /// <summary>
        /// Statement Id.
        /// </summary>
        public string StatementId { get; set; }

        /// <summary>
        /// Actor.
        /// </summary>
        public string Actor { get; set; }

        /// <summary>
        /// Verb Id.
        /// </summary>
        public string VerbId { get; set; }

        /// <summary>
        /// Verb.
        /// </summary>
        public string Verb { get; set; }

        /// <summary>
        /// Activity Id.
        /// </summary>
        public string ActivityId { get; set; }

        /// <summary>
        /// Mbox of Object.
        /// </summary>
        public string MboxObject { get; set; }

        /// <summary>
        /// Mbox Sha1 Sum of Object.
        /// </summary>
        public string MboxSha1SumObject { get; set; }

        /// <summary>
        /// Open Id of Object.
        /// </summary>
        public string OpenIdObject { get; set; }

        /// <summary>
        /// Account of Object.
        /// </summary>
        public string AccountObject { get; set; }

        /// <summary>
        /// Mbox of Actor.
        /// </summary>
        public string MboxActor { get; set; }

        /// <summary>
        /// Mbox Sha1 Sum of Actor.
        /// </summary>
        public string MboxSha1SumActor { get; set; }

        /// <summary>
        /// Open Id of Actor.
        /// </summary>
        public string OpenIdActor { get; set; }

        /// <summary>
        /// Account of Actor.
        /// </summary>
        public string AccountActor { get; set; }

        /// <summary>
        /// Mbox of Authority.
        /// </summary>
        public string MboxAuthority { get; set; }

        /// <summary>
        /// Mbox Sha1 Sum of Authority.
        /// </summary>
        public string MboxSha1SumAuthority { get; set; }

        /// <summary>
        /// Open Id of Authority.
        /// </summary>
        public string OpenIdAuthority { get; set; }

        /// <summary>
        /// Account of Authority.
        /// </summary>
        public string AccountAuthority { get; set; }

        /// <summary>
        /// Mbox of Team.
        /// </summary>
        public string MboxTeam { get; set; }

        /// <summary>
        /// Mbox Sha1 Sum of Team.
        /// </summary>
        public string MboxSha1SumTeam { get; set; }

        /// <summary>
        /// Open Id of Team.
        /// </summary>
        public string OpenIdTeam { get; set; }

        /// <summary>
        /// Account of Team.
        /// </summary>
        public string AccountTeam { get; set; }

        /// <summary>
        /// Mbox of Instructor.
        /// </summary>
        public string MboxInstructor { get; set; }

        /// <summary>
        /// Mbox Sha1 Sum of Instructor.
        /// </summary>
        public string MboxSha1SumInstructor { get; set; }

        /// <summary>
        /// Open Id of Team.
        /// </summary>
        public string OpenIdInstructor { get; set; }

        /// <summary>
        /// Account of Team.
        /// </summary>
        public string AccountInstructor { get; set; }

        /// <summary>
        /// Object.
        /// </summary>
        public string Object { get; set; }

        /// <summary>
        /// Registration.
        /// </summary>
        public string Registration { get; set; }

        /// <summary>
        /// Statement.
        /// </summary>
        public string Statement { get; set; }

        /// <summary>
        /// Determines whether this is a voiding statement.
        /// </summary>
        public bool IsVoidingStatement { get; set; }

        /// <summary>
        /// Determines whether this statement has been voided.
        /// </summary>
        public bool IsStatementVoided { get; set; }

        /// <summary>
        /// Date and Time of when this statement was recorded.
        /// </summary>
        public DateTime DtStored { get; set; }

        public int? IdDataLesson { get; set; }
        #endregion

        #region Methods

        #region Save
        /// <summary>
        /// Saves Tin Can statement(s) to database.
        /// </summary>
        public void Save(int idUser, int? idEndpoint, bool isInternalAPI, DataTable statements)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idDataLesson", IdDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEndpoint", idEndpoint, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@isInternalAPI", isInternalAPI, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@statements", statements, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Data-TinCan.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes an existing Tin Can statement from database.
        /// </summary>
        public static void Delete(int idUser, int idEndpoint, string statementId)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEndpoint", idEndpoint, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@statementId", statementId, SqlDbType.NVarChar, 50, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Data-TinCan.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetStatements
        /// <summary>
        /// Returns Tin Can Statements.
        /// </summary>
        public DataTable GetStatements(int idUser, int start, int limit, DateTime? since, DateTime? until, bool ascendingOrder, bool relatedActivities, string mbox, string mboxSha1Sum, string openId, string account, bool relatedAgents, out bool moreResult)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEndpoint", this.IdEndpoint, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@verbId", this.VerbId, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@activityId", this.ActivityId, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@registration", this.Registration, SqlDbType.NVarChar, 50, ParameterDirection.Input);
            databaseObject.AddParameter("@start", start, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@limit", limit, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@since", since, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@until", until, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@orderAsc", ascendingOrder, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@relatedActivities", relatedActivities, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@mbox", mbox, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@mboxSha1Sum", mboxSha1Sum, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@openId", openId, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@account", account, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@relatedAgents", relatedAgents, SqlDbType.Bit, 1, ParameterDirection.Input);
            databaseObject.AddParameter("@moreResult", null, SqlDbType.Bit, 1, ParameterDirection.Output);

            try
            {
                SqlDataReader dataReader = databaseObject.ExecuteDataReader("[Data-TinCan.GetStatements]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = (String)databaseObject.Command.Parameters["@Error_Description_Code"].Value;
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                DataTable dataTable = new DataTable();
                dataTable.Load(dataReader);
                dataReader.Close();

                moreResult = Convert.ToBoolean(databaseObject.Command.Parameters["@moreResult"].Value);

                return dataTable;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetActivity
        /// <summary>
        /// Returns activity for the specified id.
        /// </summary>
        public String GetActivity(int idUser, int idEndpoint, string activityId)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEndpoint", idEndpoint, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@activityId", activityId, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@object", this.Object, SqlDbType.NVarChar, -1, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Data-TinCan.GetActivity]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                String activity = databaseObject.Command.Parameters["@object"].Value.ToString();

                return activity;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion

        #region Private Methods

        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idUser">User Id</param>
        /// <param name="idEndpoint">Endpoint Id</param>
        /// <param name="statementId">Statement Id</param>
        /// <param name="isVoided">Is Voided Statement</param>
        private void _Details(int idUser, int idEndpoint, string statementId, bool isVoided)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idDataTinCan", this.IdDataTinCan, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idEndpoint", idEndpoint, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@isInternalAPI", this.IsInternalAPI, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@statementId", statementId, SqlDbType.NVarChar, 50, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@actor", this.Actor, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@verbId", this.VerbId, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@verb", this.Verb, SqlDbType.NVarChar, 200, ParameterDirection.Output);
            databaseObject.AddParameter("@activityId", this.ActivityId, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@object", this.Object, SqlDbType.NVarChar, -1, ParameterDirection.Output);

            databaseObject.AddParameter("@mboxObject", this.MboxObject, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@mboxSha1SumObject", this.MboxSha1SumObject, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@openIdObject", this.OpenIdObject, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@accountObject", this.AccountObject, SqlDbType.NVarChar, 100, ParameterDirection.Output);

            databaseObject.AddParameter("@mboxActor", this.MboxActor, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@mboxSha1SumActor", this.MboxSha1SumActor, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@openIdActor", this.OpenIdActor, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@accountActor", this.AccountActor, SqlDbType.NVarChar, 100, ParameterDirection.Output);

            databaseObject.AddParameter("@mboxAuthority", this.MboxAuthority, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@mboxSha1SumAuthority", this.MboxSha1SumAuthority, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@openIdAuthority", this.OpenIdAuthority, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@accountAuthority", this.AccountAuthority, SqlDbType.NVarChar, 100, ParameterDirection.Output);

            databaseObject.AddParameter("@mboxTeam", this.MboxTeam, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@mboxSha1SumTeam", this.MboxSha1SumTeam, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@openIdTeam", this.OpenIdTeam, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@accountTeam", this.AccountTeam, SqlDbType.NVarChar, 100, ParameterDirection.Output);

            databaseObject.AddParameter("@mboxInstructor", this.MboxInstructor, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@mboxSha1SumInstructor", this.MboxSha1SumInstructor, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@openIdInstructor", this.OpenIdInstructor, SqlDbType.NVarChar, 100, ParameterDirection.Output);
            databaseObject.AddParameter("@accountInstructor", this.AccountInstructor, SqlDbType.NVarChar, 100, ParameterDirection.Output);

            databaseObject.AddParameter("@registration", this.Registration, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@statement", this.Statement, SqlDbType.NVarChar, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@isVoidingStatement", this.IsVoidingStatement, SqlDbType.Bit, 1, ParameterDirection.Output);
            databaseObject.AddParameter("@isStatementVoided", isVoided, SqlDbType.Bit, 1, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@dtStored", this.DtStored, SqlDbType.DateTime, 8, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Data-TinCan.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.IdDataTinCan = Convert.ToInt32(databaseObject.Command.Parameters["@idDataTinCan"].Value);
                this.IdSite = Convert.ToInt32(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdEndpoint = databaseObject.Command.Parameters["@idEndpoint"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@idEndpoint"].Value);
                this.IsInternalAPI = Convert.ToBoolean(databaseObject.Command.Parameters["@isInternalAPI"].Value);
                this.StatementId = databaseObject.Command.Parameters["@statementId"].Value.ToString();
                this.Actor = databaseObject.Command.Parameters["@actor"].Value.ToString();
                this.VerbId = databaseObject.Command.Parameters["@verbId"].Value.ToString();
                this.Verb = databaseObject.Command.Parameters["@verb"].Value.ToString();
                this.ActivityId = databaseObject.Command.Parameters["@activityId"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@activityId"].Value.ToString();
                this.Object = databaseObject.Command.Parameters["@object"].Value.ToString();

                this.MboxObject = databaseObject.Command.Parameters["@mboxObject"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxObject"].Value.ToString();
                this.MboxSha1SumObject = databaseObject.Command.Parameters["@mboxSha1SumObject"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxSha1SumObject"].Value.ToString();
                this.OpenIdObject = databaseObject.Command.Parameters["@openIdObject"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@openIdObject"].Value.ToString();
                this.AccountObject = databaseObject.Command.Parameters["@accountObject"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@accountObject"].Value.ToString();

                this.MboxActor = databaseObject.Command.Parameters["@mboxActor"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxActor"].Value.ToString();
                this.MboxSha1SumActor = databaseObject.Command.Parameters["@mboxSha1SumActor"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxSha1SumActor"].Value.ToString();
                this.OpenIdActor = databaseObject.Command.Parameters["@openIdActor"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@openIdActor"].Value.ToString();
                this.AccountActor = databaseObject.Command.Parameters["@accountActor"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@accountActor"].Value.ToString();

                this.MboxAuthority = databaseObject.Command.Parameters["@mboxAuthority"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxAuthority"].Value.ToString();
                this.MboxSha1SumAuthority = databaseObject.Command.Parameters["@mboxSha1SumAuthority"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxSha1SumAuthority"].Value.ToString();
                this.OpenIdAuthority = databaseObject.Command.Parameters["@openIdAuthority"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@openIdAuthority"].Value.ToString();
                this.AccountAuthority = databaseObject.Command.Parameters["@accountAuthority"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@accountAuthority"].Value.ToString();

                this.MboxTeam = databaseObject.Command.Parameters["@mboxTeam"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxTeam"].Value.ToString();
                this.MboxSha1SumTeam = databaseObject.Command.Parameters["@mboxSha1SumTeam"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxSha1SumTeam"].Value.ToString();
                this.OpenIdTeam = databaseObject.Command.Parameters["@openIdTeam"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@openIdTeam"].Value.ToString();
                this.AccountTeam = databaseObject.Command.Parameters["@accountTeam"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@accountTeam"].Value.ToString();

                this.MboxInstructor = databaseObject.Command.Parameters["@mboxInstructor"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxInstructor"].Value.ToString();
                this.MboxSha1SumInstructor = databaseObject.Command.Parameters["@mboxSha1SumInstructor"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@mboxSha1SumInstructor"].Value.ToString();
                this.OpenIdInstructor = databaseObject.Command.Parameters["@openIdInstructor"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@openIdInstructor"].Value.ToString();
                this.AccountInstructor = databaseObject.Command.Parameters["@accountInstructor"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@accountInstructor"].Value.ToString();

                this.Registration = databaseObject.Command.Parameters["@registration"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@registration"].Value.ToString();
                this.Statement = databaseObject.Command.Parameters["@statement"].Value.ToString();
                this.IsVoidingStatement = Convert.ToBoolean(databaseObject.Command.Parameters["@isVoidingStatement"].Value);
                this.IsStatementVoided = Convert.ToBoolean(databaseObject.Command.Parameters["@isStatementVoided"].Value);
                this.DtStored = Convert.ToDateTime(databaseObject.Command.Parameters["@dtStored"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion

        #region StaticMethods
        #region LessonCompleted
        /// <summary>
        /// Gets the status of lesson ,it is already completed or not whch will decide that we need to store statements further or not
        /// </summary>
        /// <param name="searchParam">search Parameter</param>
        /// <returns>
        /// DataTable of Lessons.
        /// </returns>
        public static bool LessonCompleted(int idDataLesson)
        {
            DataTable dt = new DataTable();

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

                databaseObject.AddParameter("@idDataLesson", idDataLesson, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@completed", false, SqlDbType.Bit, 1, ParameterDirection.Output);

                databaseObject.ExecuteNonQuery("[Lesson.IsAlreadyCompletedForTincan]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                return Convert.ToBoolean(databaseObject.Command.Parameters["@completed"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion
    }
}