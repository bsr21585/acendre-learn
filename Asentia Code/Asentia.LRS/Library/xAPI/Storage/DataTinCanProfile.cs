using Asentia.Common;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.LRS.Library.xAPI.Storage
{
    public class DataTinCanProfile
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DataTinCanProfile()
        { ;}

        /// <summary>
        /// Constructor. Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <param name="idEndpoint">Endpoint Id</param>
        /// <param name="idUser">User Id</param>
        /// <param name="profileId">Profile Id</param>
        /// <param name="activityId">Activity Id</param>
        /// <param name="agent">Agent</param>
        public DataTinCanProfile(int idUser, int idEndpoint, string profileId, string activityId, string agent)
        {
            _Details(idUser, idEndpoint, profileId, activityId, agent);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Id of Tin Can Profile.
        /// </summary>
        public int idDataTinCanProfile { get; set; }

        /// <summary>
        /// Endpoint Id of the owner.
        /// </summary>
        public int? IdEndpoint { get; set; }

        /// <summary>
        /// Site Id.
        /// </summary>
        public int IdSite { get; set; }

        /// <summary>
        /// Profile Id.
        /// </summary>
        public String ProfileId { get; set; }

        /// <summary>
        /// Activity Id.
        /// </summary>
        public String ActivityId { get; set; }

        /// <summary>
        /// Agent.
        /// </summary>
        public String Agent { get; set; }

        /// <summary>
        /// Document Contents.
        /// </summary>
        public byte[] DocContents { get; set; }

        /// <summary>
        /// Content Type.
        /// </summary>
        public String ContentType { get; set; }

        /// <summary>
        /// When the document was most recently modified.
        /// </summary>
        public DateTime DtUpdated { get; set; }
        #endregion

        #region Methods

        #region Save
        /// <summary>
        /// Saves Tin Can profile to database.
        /// </summary>
        public void Save(int idUser)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idData_TinCanProfile", this.idDataTinCanProfile, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@profileId", this.ProfileId, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@activityId", this.ActivityId, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@agent", this.Agent, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@contentType", this.ContentType, SqlDbType.NVarChar, 50, ParameterDirection.Input);
            databaseObject.AddParameter("@docContents", this.DocContents, SqlDbType.VarBinary, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@dtUpdated", this.DtUpdated, SqlDbType.DateTime, 8, ParameterDirection.Input);
            databaseObject.AddParameter("@idEndpoint", this.IdEndpoint, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Data-TinCanProfile.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.idDataTinCanProfile = Convert.ToInt32(databaseObject.Command.Parameters["@idData_TinCanProfile"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Deletes profile from database.
        /// </summary>
        public static void Delete(int idUser, int idEndpoint, string profileId, string activityId, string agent)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEndpoint", idEndpoint, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@profileId", profileId, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@activityId", activityId, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@agent", agent, SqlDbType.NVarChar, -1, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Data-TinCanProfile.Delete]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #region GetProfiles
        /// <summary>
        /// Returns profiles.
        /// </summary>
        public DataTable GetProfiles(int idUser, DateTime? since)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idEndpoint", this.IdEndpoint, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@activityId", this.ActivityId, SqlDbType.NVarChar, 100, ParameterDirection.Input);
            databaseObject.AddParameter("@agent", this.Agent, SqlDbType.NVarChar, -1, ParameterDirection.Input);
            databaseObject.AddParameter("@since", since, SqlDbType.DateTime, 8, ParameterDirection.Input);

            try
            {
                SqlDataReader dataReader = databaseObject.ExecuteDataReader("[Data-TinCanProfile.GetProfiles]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = (String)databaseObject.Command.Parameters["@Error_Description_Code"].Value;
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                DataTable dataTable = new DataTable();
                dataTable.Load(dataReader);
                dataReader.Close();

                return dataTable;
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion

        #region Private Methods

        #region _Details
        /// <summary>
        /// Retrieves database details.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <exception cref="DatabaseCallerPermissionException">
        /// Thrown when caller does not have permission to access this <see cref="Object" />.
        /// </exception>
        /// <param name="idUser">User Id</param>
        /// <param name="idEndpoint">Endpoint Id</param>
        /// <param name="profileId">Profile Id</param>
        /// <param name="activityId">Activity Id</param>
        /// <param name="agent">Agent</param>
        private void _Details(int idUser, int idEndpoint, string profileId, string activityId, string agent)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@idData_TinCanProfile", this.idDataTinCanProfile, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idSite", this.IdSite, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@idEndpoint", idEndpoint, SqlDbType.Int, 4, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@profileId", profileId, SqlDbType.NVarChar, 100, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@activityId", activityId, SqlDbType.NVarChar, 100, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@agent", agent, SqlDbType.NVarChar, -1, ParameterDirection.InputOutput);
            databaseObject.AddParameter("@contentType", this.ContentType, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@docContents", this.DocContents, SqlDbType.VarBinary, -1, ParameterDirection.Output);
            databaseObject.AddParameter("@dtUpdated", this.DtUpdated, SqlDbType.DateTime, 8, ParameterDirection.Output);

            try
            {
                databaseObject.ExecuteNonQuery("[Data-TinCanProfile.Details]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                this.idDataTinCanProfile = Convert.ToInt32(databaseObject.Command.Parameters["@idData_TinCanProfile"].Value);
                this.IdSite = Convert.ToInt32(databaseObject.Command.Parameters["@idSite"].Value);
                this.IdEndpoint = databaseObject.Command.Parameters["@idEndpoint"].Value == DBNull.Value ? (int?)null : Convert.ToInt32(databaseObject.Command.Parameters["@idEndpoint"].Value);
                this.ProfileId = databaseObject.Command.Parameters["@profileId"].Value.ToString();
                this.ActivityId = databaseObject.Command.Parameters["@activityId"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@activityId"].Value.ToString();
                this.Agent = databaseObject.Command.Parameters["@agent"].Value == DBNull.Value ? null : databaseObject.Command.Parameters["@agent"].Value.ToString();
                this.ContentType = databaseObject.Command.Parameters["@contentType"].Value.ToString();
                this.DocContents = (byte[])databaseObject.Command.Parameters["@docContents"].Value;
                this.DtUpdated = Convert.ToDateTime(databaseObject.Command.Parameters["@dtUpdated"].Value);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion

        #endregion
    }
}