using Asentia.Common;
using System;
using System.Data;

namespace Asentia.LRS.Library.xAPI.Storage
{
    public class DataTinCanContextActivities
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DataTinCanContextActivities()
        { ;}
        #endregion

        #region Properties
        /// <summary>
        /// Id of Tin Can Data Context Activities.
        /// </summary>
        public int IdDataTinCanContextActivities { get; set; }

        /// <summary>
        /// Id of Tin Can Data.
        /// </summary>
        public int IdDataTinCan { get; set; }

        /// <summary>
        /// Site Id.
        /// </summary>
        public int IdSite { get; set; }

        /// <summary>
        /// Activity Id.
        /// </summary>
        public String ActivityId { get; set; }

        /// <summary>
        /// Activity Type.
        /// </summary>
        public String ActivityType { get; set; }

        /// <summary>
        /// Object Type.
        /// </summary>
        public String ObjectType { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Saves Tin Can statement(s) context activities to database.
        /// </summary>
        public void Save(int idUser, DataTable activities)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idUser, SqlDbType.Int, 4, ParameterDirection.Input);

            databaseObject.AddParameter("@activities", activities, SqlDbType.Structured, null, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[Data-TinCanContextActivities.Save]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
    }
}