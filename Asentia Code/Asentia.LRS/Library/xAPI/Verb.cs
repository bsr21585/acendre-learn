﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class Verb : IValidatable
    {
        #region Fields
        private string _id;
        private LanguageMap _display;
        #endregion

        #region Properties
        [DataMember(Name = "id")]
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember(Name = "display")]
        public LanguageMap Display
        {
            get { return _display; }
            set { _display = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new StatementVerb that is empty.  Used by the JSON Serializer.
        /// </summary>
        public Verb()
        {
        }
        #endregion

        #region Private Methods

        private bool IsUri(string source)
        {
            if (!String.IsNullOrWhiteSpace(source) && Uri.IsWellFormedUriString(source, UriKind.RelativeOrAbsolute))
            {
                Uri tempValue;
                return Uri.TryCreate(source, UriKind.RelativeOrAbsolute, out tempValue);
            }
            return false;
        }
        #endregion

        #region Public Methods

        public bool IsVoided()
        {
            if (Display != null)
            {
                foreach (string s in _display.Values)
                {
                    if (s.ToLower().Equals("voided"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        /// <summary>
        /// Validates that the object abides by its rules
        /// </summary>
        public virtual IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            if (Id == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.VerbIDCannotBeNull));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            //leave it commented as it is not mendatory for story line package
            //if (Display == null)
            //{
            //    failures.Add(new Messages(Asentia.Common._GlobalResources.DisplayPropertyOfVerbCannotBeNull));
            //    if (earlyReturnOnFailure)
            //    {
            //        return failures;
            //    }
            //}

            return failures;
        }

        #endregion
    }
}