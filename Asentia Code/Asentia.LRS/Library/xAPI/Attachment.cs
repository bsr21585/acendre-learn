﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class Attachment : IValidatable
    {
        #region Fields
        private string _UsageType;
        private LanguageMap _Display;
        private LanguageMap _Description;
        private string _ContentType;
        private int? _Length;
        private string _Sha2;
        private string _FileUrl;
        #endregion

        #region Properties
        [DataMember(Name = "usageType")]
        public string UsageType
        {
            get { return _UsageType; }
            set { _UsageType = value; }
        }

        [DataMember(Name = "display")]
        public LanguageMap Display
        {
            get { return _Display; }
            set { _Display = value; }
        }

        [DataMember(Name = "description")]
        public LanguageMap Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [DataMember(Name = "contentType")]
        public string ContentType
        {
            get { return _ContentType; }
            set { _ContentType = value; }
        }

        [DataMember(Name = "length")]
        public int? Length
        {
            get { return _Length; }
            set { _Length = value; }
        }

        [DataMember(Name = "sha2")]
        public string Sha2
        {
            get { return _Sha2; }
            set { _Sha2 = value; }
        }

        [DataMember(Name = "fileUrl")]
        public string FileUrl
        {
            get { return _FileUrl; }
            set { _FileUrl = value; }
        }

        #endregion

        #region Constructor
        public Attachment() { }
        #endregion

        #region Public Methods
        public IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();

            if (_UsageType == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.AttachmentDoesNotHaveAUsageProperty));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }

            if (_Display == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.AttachmentDoesNotHaveADisplayProperty));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }

            if (_ContentType == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.AttachmentDoesNotHaveAContentProperty));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }

            if (_Length == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.AttachmentDoesNotHaveALengthProperty));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }

            if (_Sha2 == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.AttachmentDoesNotHaveASHA2Property));
            }

            return failures;
        }
        #endregion
    }
}
