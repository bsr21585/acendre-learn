﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class StatementResult
    {
        #region Properties

        /// <summary>
        /// List of statements.
        /// </summary>
        [DataMember(Name = "statements")]
        public List<Statement> Statements { get; set; }

        /// <summary>
        /// Additional data.
        /// </summary>
        [DataMember(Name = "more")]
        public String More { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the StatementResult class.
        /// </summary>
        public StatementResult()
        { ;}

        /// <summary>
        /// Initializes a new instance of the StatementResult class that contains the specified statements.
        /// </summary>
        /// <param name="statements">List of statements.</param>
        public StatementResult(List<Statement> statements)
        {
            this.Statements = statements;
        }

        /// <summary>
        /// Initializes a new instance of the StatementResult class that contains the specified statements and additional data.
        /// </summary>
        /// <param name="statements">List of statements.</param>
        /// <param name="more">Additional data.</param>
        public StatementResult(List<Statement> statements, String more)
        {
            this.Statements = statements;
            this.More = more;
        }

        #endregion
    }
}
