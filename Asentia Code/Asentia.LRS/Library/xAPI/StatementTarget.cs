﻿using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class StatementTarget
    {
        #region Fields
        private string _ObjectType;
        #endregion

        #region Properties
        [DataMember(Name = "objectType")]
        public virtual string ObjectType
        {
            get { return _ObjectType; }
            set { _ObjectType = value; }
        }
        #endregion
    }
}
