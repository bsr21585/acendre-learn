using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class StatementRef : StatementTarget
    {
        #region Constants
        protected static readonly string _OBJECT_TYPE = "StatementRef";
        #endregion

        #region Fields
        private string _Id;
        #endregion

        #region Properties
        [DataMember(Name = "id")]
        public string Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        [DataMember(Name = "objectType")]
        public override string ObjectType
        {
            get { return _OBJECT_TYPE; }
        }
        #endregion

        #region Constructor
        public StatementRef()
        { ;}
        #endregion
    }
}
