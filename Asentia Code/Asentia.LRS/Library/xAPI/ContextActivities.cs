﻿using Asentia.LRS.Library.xAPI.Helper;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class ContextActivities : IValidatable
    {
        #region Fields
        private List<Activity> _Parent;
        private List<Activity> _Grouping;
        private List<Activity> _Category;
        private List<Activity> _Other;
        #endregion

        #region Properties
        [DataMember(Name = "parent")]
        [JsonConverter(typeof(SingleOrArrayConverter<Activity>))]
        public List<Activity> Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }


        [DataMember(Name = "grouping")]
        [JsonConverter(typeof(SingleOrArrayConverter<Activity>))]
        public List<Activity> Grouping
        {
            get { return _Grouping; }
            set { _Grouping = value; }
        }

      
        [DataMember(Name = "category")]
        [JsonConverter(typeof(SingleOrArrayConverter<Activity>))]
        public List<Activity> Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        [DataMember(Name = "other")]
        [JsonConverter(typeof(SingleOrArrayConverter<Activity>))]
        public List<Activity> Other
        {
            get { return _Other; }
            set { _Other = value; }
        }
        #endregion

        #region Constructor
        public ContextActivities() { }
        #endregion

        #region Public Methods

        public IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            //Validate children
            object[] children = new object[] { _Parent, _Grouping, _Other };
            var failures = new List<Messages>();
            foreach (object child in children)
            {
                if (child != null && child is IValidatable)
                {
                    failures.AddRange(((IValidatable)child).Validate(earlyReturnOnFailure));
                    if (earlyReturnOnFailure && failures.Count > 0)
                    {
                        return failures;
                    }
                }
            }
            return failures;
        }

        #endregion
    }
    

}
