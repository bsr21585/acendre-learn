﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    /// <summary>
    /// An agent account to verify the uniqueness of the agent
    /// </summary>
    [DataContract]
    public class AgentAccount : IValidatable
    {
        #region Fields
        private string _HomePage;
        private string _Name;
        #endregion

        #region Properties
        /// <summary>
        /// The home page of the agent account
        /// </summary>
        [DataMember(Name = "homepage")]
        public string Homepage
        {
            get { return _HomePage; }
            set
            {
                _HomePage = value;
            }
        }

        /// <summary>
        /// The username used by the agent to log into this account page
        /// </summary>
        [DataMember(Name = "name")]
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new agent account with no set values
        /// </summary>
        public AgentAccount() { }

        #endregion

        #region Public Methods
        /// <summary>
        /// Validates the object.
        /// </summary>
        public IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            if (string.IsNullOrWhiteSpace(_HomePage))
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.AccountServiceHomepageCannotBeNull));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            if (string.IsNullOrWhiteSpace(_Name))
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.AccountNameCannotBeNull));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            return failures;
        }
        #endregion
    }
}
