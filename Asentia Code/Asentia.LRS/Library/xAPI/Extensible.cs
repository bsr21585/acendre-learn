﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    /// <summary>
    /// Definition of any extensible TinCanAPI Statement.
    /// Allows for arbitrary key/value pairs to be attached to
    /// an object.
    /// </summary>
    [DataContract]
    public class Extensible
    {
        #region Fields
        private Dictionary<string, object> _Extensions;
        #endregion

        #region Properties
        [DataMember(Name="extensions")]
        public Dictionary<string, object> Extensions
        {
            get { return _Extensions; }
            set { _Extensions = value; }
        }
        #endregion

        #region Constructor
        public Extensible() { }
        #endregion

        #region Public Methods

        ///// <summary>
        ///// Validates the extension
        ///// </summary>
        //public IEnumerable<ValidationFailure> Validate(bool earlyReturnOnFailure)
        //{
        //    var failures = new List<ValidationFailure>();
        //    foreach(string key in extensions.Keys)
        //        if (Uri.IsWellFormedUriString(key, UriKind.RelativeOrAbsolute))
        //        {
        //            failures.Add(new ValidationFailure("Key in extensions is not a well formed URI."));
        //            if (earlyReturnOnFailure)
        //                return failures;
        //        }

        //    return failures;
        //}
        #endregion

    }
}
