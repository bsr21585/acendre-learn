﻿using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract(Namespace = "Asentia.LRS.Library.RESTxAPIwithOAuth")]
    public class Messages
    {
        #region Properties
        [DataMember]
        public string Message { get; private set; }
        #endregion

        #region Constructor
        public Messages(string msg)
        {
            this.Message = msg;
        }
        #endregion
    }
}
