﻿using System;
using System.Collections.Generic;

using Asentia.LRS.Library.xAPI.Helper;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    /// <summary>
    /// Class represents the context of a TinCan Statement
    /// </summary>
    [DataContract]
    public class Context : Extensible, IValidatable
    {
        #region Fields
        private string _Registration;
        private Actor _Instructor;
        private Actor _Team;
        private ContextActivities _ContextActivities;
        private string _Revision;
        private string _Platform;
        private string _Language;
        private StatementRef _Statement;
        #endregion

        #region Properties
        /// <summary>
        /// The Registration UUID
        /// </summary>
        [DataMember(Name = "registration")]
        public string Registration
        {
            get { return _Registration; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    _Registration = null;
                }
                else
                {
                    _Registration = value.ToLower();
                }
            }
        }

        /// <summary>
        /// The instructor in this context
        /// </summary>
        [DataMember(Name = "instructor")]
        public Actor Instructor
        {
            get { return _Instructor; }
            set { _Instructor = value; }
        }

        /// <summary>
        /// The team in this context
        /// </summary>
        [DataMember(Name = "team")]
        public Actor Team
        {
            get { return _Team; }
            set { _Team = value; }
        }

        /// <summary>
        /// The Activities in this Context
        /// </summary>
        [DataMember(Name = "contextActivities")]
        public ContextActivities ContextActivities
        {
            get { return _ContextActivities; }
            set { _ContextActivities = value; }
        }

        /// <summary>
        /// The revision
        /// </summary>
        [DataMember(Name = "revision")]
        public string Revision
        {
            get { return _Revision; }
            set { _Revision = value; }
        }

        /// <summary>
        /// The platform
        /// </summary>
        [DataMember(Name = "platform")]
        public string Platform
        {
            get { return _Platform; }
            set { _Platform = value; }
        }

        /// <summary>
        /// The language
        /// </summary>
        [DataMember(Name = "language")]
        public String Language
        {
            get { return _Language; }
            set { _Language = value; }
        }

        /// <summary>
        /// The statement
        /// </summary>
        [DataMember(Name = "statement")]
        public StatementRef Statement
        {
            get { return _Statement; }
            set { _Statement = value; }
        }
        #endregion

        #region Constructor
        public Context() { }
        #endregion

        #region Public Methods
        /// <summary>
        /// Validates the context
        /// </summary>
        public IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            if (_Registration != null)
            {
                if (!ValidationHelper.IsValidUUID(_Registration))
                {
                    failures.Add(new Messages(Asentia.Common._GlobalResources.RegistrationMustBeUUID));
                    if (earlyReturnOnFailure)
                    {
                        return failures;
                    }
                }
            }

            object[] children = new object[] 
            { 
                _Instructor, _Team, _ContextActivities, _Statement
            };
            
            foreach (object o in children)
            {
                if (o != null && o is IValidatable)
                {
                    failures.AddRange(((IValidatable)o).Validate(earlyReturnOnFailure));
                    if (failures.Count > 0 && earlyReturnOnFailure)
                    {
                        return failures;
                    }
                }
            } 
            return failures;
        }
        #endregion
    }
}
