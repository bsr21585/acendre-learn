﻿using System;
using System.Collections.Generic;
using Asentia.LRS.Library.xAPI.Model;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class Result : Extensible, IValidatable
    {
        #region Fields
        private Score _Score;
        private bool? _Success;
        private bool? _Completion;
        private string _Response;
        private String _Duration;
        #endregion

        #region Properties
        [DataMember(Name = "score")]
        public Score Score
        {
            get { return _Score; }
            set { _Score = value; }
        }

        [DataMember(Name = "success")]
        public bool? Success
        {
            get { return _Success; }
            set
            {
                _Success = value;
            }
        }

        [DataMember(Name = "completion")]
        public bool? Completion
        {
            get { return _Completion; }
            set
            {
                _Completion = value;
            }
        }

        [DataMember(Name = "response")]
        public string Response
        {
            get { return _Response; }
            set { _Response = value; }
        }

        [DataMember(Name = "duration")]
        public String Duration
        {
            get { return _Duration; }
            set 
            {
                //Validate value
                new ScormTimeSpan(value);

                //Set value
                _Duration = value;
            }
        }
        #endregion

        #region Constructor
        public Result()
        { ;}
        #endregion

        #region Public Methods
        public IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            if (_Score != null)
                failures.AddRange(_Score.Validate(earlyReturnOnFailure));
            return failures;
        }
        #endregion
    }
}
