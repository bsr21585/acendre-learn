﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    public class Score : IValidatable
    {
        #region Fields
        private double? _Scaled;
        private double? _Raw;
        private double? _Min;
        private double? _Max;
        #endregion

        #region Properties
        [DataMember(Name = "scaled")]
        public double? Scaled
        {
            get { return _Scaled; }
            set { _Scaled = value; }
        }

        [DataMember(Name = "raw")]
        public double? Raw
        {
            get { return _Raw; }
            set { _Raw = value; }
        }

        [DataMember(Name = "min")]
        public double? Min
        {
            get { return _Min; }
            set { _Min = value; }
        }

        [DataMember(Name = "max")]
        public double? Max
        {
            get { return _Max; }
            set { _Max = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Empty constructor.
        /// </summary>
        public Score() { }
        #endregion

        #region Public Methods
        public IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();
            if (_Scaled != null && (_Scaled.Value < 0.0 || _Scaled.Value > 1.0))
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.ScaledScoreMustBeBetween0and1));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            if ((_Min != null && _Max != null) && (_Max.Value < _Min.Value))
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.MaxScoreCannotBeLowerThanMinScore));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            if (_Raw != null)
            {
                if (_Max != null && _Raw.Value > _Max.Value)
                {
                    failures.Add(new Messages(Asentia.Common._GlobalResources.RawScoreCannotBeGreaterThanMaxScore));
                    if (earlyReturnOnFailure)
                    {
                        return failures;
                    }
                }
                if (_Min != null && _Raw.Value < _Min.Value)
                {
                    failures.Add(new Messages(Asentia.Common._GlobalResources.RawScoreCannotBeLessThanMinScore));
                    if (earlyReturnOnFailure)
                    {
                        return failures;
                    }
                }
            }
            return failures;
        }
        #endregion
    }
}
