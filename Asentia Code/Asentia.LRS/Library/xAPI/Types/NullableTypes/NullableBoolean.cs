using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI.Model
{
    [DataContract]
    public class NullableBoolean
    {
        public bool Value;
        public NullableBoolean(bool value)
        {
            this.Value = value;
        }

        public override bool Equals(object obj)
        {
            NullableBoolean other = obj as NullableBoolean;
            if (other == null)
            {
                return false;
            }
            else
            {
                return Equals(other.Value, this.Value);
            }
        }
        public override int GetHashCode()
        {
            return Value ? 1 : 0;
        }

        public static implicit operator NullableBoolean(bool b)
        {
            return new NullableBoolean(b);
        }
    }
}
