using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI.Model
{
    [DataContract]
    public class NullableDouble
    {
        public double Value;
        public NullableDouble(double value)
        {
            this.Value = value;
        }

        public override bool Equals(object obj)
        {
            NullableDouble dbl = obj as NullableDouble;
            if (dbl == null)
            {
                return false;
            }
            else
            {
                return Equals(dbl.Value, this.Value);
            }
        }
        public override int GetHashCode()
        {
            return (int)(Value % int.MaxValue);
        }

        public static implicit operator NullableDouble(double d)
        {
            return new NullableDouble(d);
        }
    }
}
