using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI.Model
{
    [DataContract]
    public class NullableInteger
    {
        public int Value;
        
        public NullableInteger(int value)
        {
            this.Value = value;
        }

        public static implicit operator NullableInteger(int i)
        {
            return new NullableInteger(i);
        }
    }
}
