using System;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI.Model
{
    [DataContract]
    public class NullableDateTime
    {
        public DateTime Value;
        public NullableDateTime(DateTime value)
        {
            this.Value = value;
        }

        public override bool Equals(object obj)
        {
            NullableDateTime other = obj as NullableDateTime;
            if (other == null)
            {
                return false;
            }
            else
            {
                return Equals(other.Value, this.Value);
            }
        }
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static implicit operator NullableDateTime(DateTime d)
        {
            return new NullableDateTime(d);
        }
    }
}
