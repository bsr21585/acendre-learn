﻿using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI.Model
{
    [DataContract]
    public class NullableScormTimeSpan
    {
        [DataMember]
        public ScormTimeSpan Value;
        
        public NullableScormTimeSpan(ScormTimeSpan value)
        {
            this.Value = value;
        }
        
        public NullableScormTimeSpan(string value)
        {
            this.Value = new ScormTimeSpan(value);
        }

        public override bool Equals(object obj)
        {
            NullableScormTimeSpan other = obj as NullableScormTimeSpan;
            if (other == null)
            {
                return false;
            }
            else
            {
                return Equals(other.Value.Value, this.Value.Value);
            }
        }
        
        public override int GetHashCode()
        {
            return (int)(Value.Value & int.MaxValue);
        }
    }
}
