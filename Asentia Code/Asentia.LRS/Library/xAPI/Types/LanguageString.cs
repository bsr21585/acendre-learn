﻿using System;

namespace Asentia.LRS.Library.xAPI.Model
{
    /// <summary>
    /// Internal representation of SCORM Metadata elements that derived from vocabulary elements. The elements of
    /// the vocabulary are defined by the particular "source" for that vocabulary.
    /// </summary>
    [Serializable]
    public struct LanguageString
    {
        private string _Language;
        private string _Text;

        public string Language
        {
            get { return _Language; }
            set { _Language = value; }
        }

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public LanguageString(string text)
            : this(text, string.Empty) { }

        public LanguageString(string text, string language)
        {
            this._Text = text;
            this._Language = language;
        }

        public LanguageString(LanguageString source)
        {
            this._Text = source._Text;
            this._Language = source._Language;
        }

        /// <summary>
        /// Allows for an explicit cast to a string
        /// </summary>
        /// <param name="langString">The LangSTring value to be converted to a string</param>
        /// <returns>The string representation of this object</returns>
        public static explicit operator string(LanguageString langString)
        {
            return langString.Text;
        }

        /// <summary>
        /// Returns the Text property as the string representation of this object. 
        /// </summary>
        /// <returns>Text property -- When null, returns empty string</returns>
        public override string ToString()
        {
            if (this._Text == null)
            {
                return string.Empty;
            }
            else
            {
                return this._Text;
            }
        }
    }
}
