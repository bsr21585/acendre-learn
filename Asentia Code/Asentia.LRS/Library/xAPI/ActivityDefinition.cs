﻿using System;
using System.Runtime.Serialization;

namespace Asentia.LRS.Library.xAPI
{
    /// <summary>
    /// Definition of an activity, a core statement piece.
    /// </summary>
    [DataContract]
    public class ActivityDefinition : Extensible//, IValidatable
    {
        #region Fields
        private LanguageMap _Name;
        private LanguageMap _Description;
        private Uri _Type;
        private string _InteractionType;
        private string _MoreInfo;
        #endregion

        #region Properties
        /// <summary>
        /// A collection of language codes and their activity name.
        /// </summary>
        [DataMember(Name="name")]
        public LanguageMap Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        
        /// <summary>
        /// A collection of language codes and their activity description.
        /// </summary>
        [DataMember(Name="description")]
        public LanguageMap Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        
        /// <summary>
        /// The type of activity
        /// </summary>
        [DataMember(Name = "type")]
        public Uri Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        [DataMember(Name = "interactionType")]
        public virtual string InteractionType
        {
            get { return _InteractionType; }
            set { _InteractionType = value; }
        }

        [DataMember(Name = "moreInfo")]
        public string MoreInfo
        {
            get { return _MoreInfo; }
            set { _MoreInfo = value; }
        }
        #endregion

        #region Constructor
        public ActivityDefinition()
        { ;}
        #endregion
    }
}
