﻿using System;
using System.Collections.Generic;
using Asentia.LRS.Library.xAPI.Helper;
using Asentia.LRS.Library.xAPI.Model;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Asentia.LRS.Library.xAPI
{
    [DataContract]
    [KnownType("GetKnownTypes")]
    public class Statement : StatementTarget, IValidatable
    {
        #region Fields
        private string _Id;
        private Actor _Actor;
        private Verb _Verb;
        private StatementTarget _Object;
        private Result _Result;
        private Context _Context;
        private DateTime? _TimeStamp;
        private DateTime? _Stored;
        private Actor _Authority;
        private bool? _Voided;
        private string _Version;
        private Attachment[] _Attachments;
        #endregion

        #region Properties
        /// <summary>
        /// string representation of the statement verb
        /// </summary>
        [DataMember(Name = "verb")]
        public virtual Verb Verb
        {
            get
            {
                return _Verb;
            }
            set
            {
                _Verb = value;
            }
        }

        /// <summary>
        /// The statements ID
        /// </summary>
        [DataMember(Name = "id")]
        public string Id
        {
            get { return _Id; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    _Id = null;
                }
                else
                {
                    _Id = value.ToLower();
                }
            }
        }

        /// <summary>
        /// The statements actor
        /// </summary>
        [DataMember(Name = "actor")]
        public Actor Actor
        {
            get { return _Actor; }
            set
            {
                _Actor = value;
            }
        }

        /// <summary>
        /// Returns the statement verb in its internal enum format
        /// </summary>
        /// <returns>Statement verb as an enum</returns>
        public Verb GetVerbAsEnum()
        {
            return _Verb;
        }

        /// <summary>
        /// The target object of this statement
        /// </summary>
        [DataMember(Name = "object")]
        public dynamic Object
        {
            get { return _Object; }
            set
            {
                StatementTarget statementTarget = JsonConvert.DeserializeObject<StatementTarget>(JsonConvert.SerializeObject(value));

                if (statementTarget == null || String.IsNullOrWhiteSpace(statementTarget.ObjectType))
                    _Object = JsonConvert.DeserializeObject<Activity>(JsonConvert.SerializeObject(value));
                else
                {
                    switch (statementTarget.ObjectType.ToLower())
                    {
                        case "agent":
                        case "group":
                            _Object = JsonConvert.DeserializeObject<Actor>(JsonConvert.SerializeObject(value));
                            break;
                        case "activity":
                            _Object = JsonConvert.DeserializeObject<Activity>(JsonConvert.SerializeObject(value));
                            break;
                        case "statementref":
                            _Object = JsonConvert.DeserializeObject<StatementRef>(JsonConvert.SerializeObject(value));
                            break;
                        case "substatement":
                            _Object = JsonConvert.DeserializeObject<Statement>(JsonConvert.SerializeObject(value));
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// The result of this statement
        /// </summary>
        [DataMember(Name = "result")]
        public Result Result
        {
            get { return _Result; }
            set { _Result = value; }
        }

        /// <summary>
        /// Context information for this statement
        /// </summary>
        [DataMember(Name = "context")]
        public Context Context
        {
            get { return _Context; }
            set { _Context = value; }
        }

        /// <summary>
        /// The timestamp of this statement
        /// </summary>
        [DataMember(Name = "timestamp")]
        public DateTime? Timestamp
        {
            get { return _TimeStamp; }
            set { _TimeStamp = value; }
        }

        /// <summary>
        /// The time at which a Statement is stored by the LRS.
        /// </summary>
        [DataMember(Name = "stored")]
        public DateTime? Stored
        {
            get { return _Stored; }
            set { _Stored = value; }
        }

        /// <summary>
        /// The authority for this statement
        /// </summary>
        [DataMember(Name = "authority")]
        public Actor Authority
        {
            get { return _Authority; }
            set { _Authority = value; }
        }

        /// <summary>
        /// The version of this statement
        /// </summary>
        [DataMember(Name = "version")]
        public string Version
        {
            get { return _Version; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    _Version = null;
                }
                else
                {
                    _Version = value;
                }
            }
        }

        /// <summary>
        /// Attachments
        /// </summary>
        [DataMember(Name = "attachments")]
        public Attachment[] Attachments
        {
            get { return _Attachments; }
            set { _Attachments = value; }
        }

        /// <summary>
        /// Indicates whether this statement have been voided.
        /// </summary>
        [DataMember(Name = "voided")]
        public bool? Voided
        {
            get { return _Voided; }
            set { _Voided = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Empty constructor
        /// </summary>
        public Statement() { }
        #endregion

        #region Public Methods
        /// <summary>
        /// Validates the statement, ensuring required fields are used
        /// and any necessary information (such as a result for specific verbs)
        /// is provided and valid.
        /// </summary>
        public virtual IEnumerable<Messages> Validate(bool earlyReturnOnFailure)
        {
            var failures = new List<Messages>();

            //Validate statement id if provided.
            if (_Id != null)
            {
                if (!ValidationHelper.IsValidUUID(_Id))
                {
                    failures.Add(new Messages(Asentia.Common._GlobalResources.StatementIDMustMatchUUID));
                    if (earlyReturnOnFailure)
                    {
                        return failures;
                    }
                }
            }

            if (_Actor == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.StatementDoesNotHaveAnActor));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }

            if (Verb == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.StatementDoesNotHaveAVerb));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }
            else if (_Verb.IsVoided())
            {
                // This will test for StatementRef.
                bool objectStatementIdentified = ((_Object is StatementRef) && !String.IsNullOrWhiteSpace(((StatementRef)_Object).Id));
                if (!objectStatementIdentified)
                {
                    failures.Add(new Messages(Asentia.Common._GlobalResources.StatementHasVerbVoidedButDoesNotProperlyIdentifyAStatementAsItsObject));
                    if (earlyReturnOnFailure)
                    {
                        return failures;
                    }
                }
            }

            if (_Object == null)
            {
                failures.Add(new Messages(Asentia.Common._GlobalResources.StatementDoesNotHaveAnObject));
                if (earlyReturnOnFailure)
                {
                    return failures;
                }
            }

            object[] children = new object[] { _Actor, _Verb, _Object, _Result, _Context, _TimeStamp, _Stored, _Authority };
            foreach (object o in children)
            {
                if (o != null && o is IValidatable)
                {
                    failures.AddRange(((IValidatable)o).Validate(earlyReturnOnFailure));
                    if (earlyReturnOnFailure && failures.Count > 0)
                    {
                        return failures;
                    }
                }
            }

            if (_Attachments != null)
            {
                foreach (Attachment attachment in _Attachments)
                {
                    failures.AddRange(attachment.Validate(earlyReturnOnFailure));
                    if (earlyReturnOnFailure && failures.Count > 0)
                    {
                        return failures;
                    }
                }

            }
            return failures;
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Returns an array of known types.
        /// </summary>
        static Type[] GetKnownTypes()
        {
            return new Type[] {
                typeof(Activity),
                typeof(StatementRef),
                typeof(NullableScormTimeSpan),
                typeof(ScormTimeSpan)
            };
        }
        #endregion
    }
}
