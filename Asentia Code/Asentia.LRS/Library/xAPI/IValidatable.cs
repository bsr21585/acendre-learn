﻿using System.Collections.Generic;


namespace Asentia.LRS.Library.xAPI
{
    public interface IValidatable
    {
        IEnumerable<Messages> Validate(bool earlyReturnOnFailure);
    }
}
