﻿using Asentia.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace Asentia.LRS.Library
{
    #region Enumerations

    #region DataSet Enum
    /// <summary>
    /// DataSet for existing dataset stored procedure
    /// When create a new dataset, manually add it to the bottom of the list
    /// the name of the enum should be consistent with stored procedure name
    /// in order to dynamically coded in the application
    /// </summary>
    public enum EnumDataSetAnalytic
    {
        /// <summary>
        /// Dataset Login Activity
        /// </summary>
        [Description("Login Activity")]
        LoginActivity = 1,

        /// <summary>
        /// Dataset Course Completions
        /// </summary>
        [Description("Course Completions")]
        CourseCompletions = 2,

        /// <summary>
        /// Dataset User and Group Snapshot
        /// </summary>
        [Description("User and Group Snapshot")]
        UserGroupSnapshot = 3,

        /// <summary>
        /// Dataset User Information
        /// </summary>
        [Description("Use Statistics")]
        UseStatistics = 6,

        /// <summary>
        /// Dataset Certificates
        /// </summary>
        [Description("CertificatesAnalytics")]
        CertificatesAnalytics = 7
    }
    #endregion

    #region ChartTypeEnum
    /// <summary>
    /// Chart type enum
    /// used to select chart to display analytic
    /// index is same as in xml files
    /// if want to add extra charts in xml please add here also
    /// </summary>
    public enum EnumChartTypeAnalytic
    {
        [Description("Bar Chart")]
        ChartBar = 1,

        [Description("Line Chart")]
        ChatLine = 2,

        [Description("Pie Chart")]
        ChartPie = 3,

        [Description("Doughnut Chart")]
        ChartDoughnut = 4,

        [Description("Polar Area Chart")]
        ChartPolarArea = 5,

        [Description("Radar Chart")]
        ChartRadar = 6

    }
    #endregion

    #region Day Enum
    /// <summary>
    /// Specifies the day of the week.
    /// index is same as in xml files
    /// </summary>
    public enum EnumDayAnalytic
    {
        // Summary:
        //     Indicates Sunday.
        Sunday = 1,
        //
        // Summary:
        //     Indicates Monday.
        Monday = 2,
        //
        // Summary:
        //     Indicates Tuesday.
        Tuesday = 3,
        //
        // Summary:
        //     Indicates Wednesday.
        Wednesday = 4,
        //
        // Summary:
        //     Indicates Thursday.
        Thursday = 5,
        //
        // Summary:
        //     Indicates Friday.
        Friday = 6,
        //
        // Summary:
        //     Indicates Saturday.
        Saturday = 7,
    }
    #endregion

    #region Month Enum
    /// <summary>
    /// Specifies the months
    /// index is same as in xml files
    /// </summary>
    public enum EnumMonthAnalytic
    {
        //
        // Summary:
        //     Indicates January.
        January = 1,
        //
        // Summary:
        //     Indicates February.
        February = 2,
        //
        // Summary:
        //     Indicates March.
        March = 3,
        //
        // Summary:
        //     Indicates April.
        April = 4,
        //
        // Summary:
        //     Indicates May.
        May = 5,
        //
        // Summary:
        //     Indicates June.
        June = 6,
        //
        // Summary:
        //     Indicates July.
        July = 7,
        //
        // Summary:
        //     Indicates August.
        August = 8,
        //
        // Summary:
        //     Indicates September.
        September = 9,
        //
        // Summary:
        //     Indicates October.
        October = 10,
        //
        // Summary:
        //     Indicates November.
        November = 11,
        //
        // Summary:
        //     Indicates December.
        December = 12

    }

    #endregion

    #region Frequency Enum
    /// <summary>
    /// Specifies the frequency
    /// used in group by clause
    /// index is same as in xml files
    /// </summary>
    public enum EnumFrequencyAnalytic
    {
        //
        // Summary:
        //     Indicates Hour.
        Hour = 1,
        //
        // Summary:
        //     Indicates Day.
        Day = 2,
        //
        // Summary:
        //     Indicates Week.
        Week = 3,
        //
        // Summary:
        //     Indicates Month.
        Month = 4,
        //
        // Summary:
        //     Indicates Year.
        Year = 5
    }
    #endregion

    #region OrderColumnEnum
    public enum EnumOrderColumnAnalytic
    {
        frequency = 1,
        count = 2
    }
    #endregion

    #region OrderEnum
    public enum EnumOrderAnalytic
    {
        Ascending = 1,
        Descending = 2
    }
    #endregion

    #endregion
    public class AnalyticDataSet
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public AnalyticDataSet()
        { ;}
        #endregion

        #region Properties
        /// <summary>
        /// Language String.
        /// </summary>
        public string LanguageString;

        /// <summary>
        /// DataSet Column XML file
        /// </summary>
        public string DatasetFieldsDataFile;

        public string FilterConditionDataFile = "~/_bin/analytic/Analytic.FilterCondition.xml";

        #region DatasetFieldProperties
        /// <summary>
        /// Language specific column properties of a dataset.
        /// </summary>

        public class DatasetFieldProperties
        {
            public bool IsVisible;
            public string Value;
            public string IdentiFier;
            public string ResourceString;

        }

        #endregion
        #endregion

        #region Methods
        #region Get Analytic DataSet List Into DataTable
        /// <summary>
        /// Retrieves name and stored procedure name of available datasets and put it into a datatable.
        /// </summary>
        /// <exception cref="DatabaseDetailsNotFoundException">
        /// Thrown when database details for this <see cref="Object" /> are not found.
        /// </exception>
        /// <returns>Returns a DataTable.</returns>
        /// 
        public DataTable GetAnalyticDataSetListTable()
        {
            DataTable dataSetList = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", AsentiaSessionState.IdSiteUser, SqlDbType.Int, 4, ParameterDirection.Input);

            try
            {
                SqlDataReader dataSetReader = databaseObject.ExecuteDataReader("[DataSet.ListAnalyticDataSet]", true);
                dataSetList.Load(dataSetReader);
                dataSetReader.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();

                if (returnCode == DBReturnValue.DetailsNotFound)
                { throw new DatabaseDetailsNotFoundException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode)); }

            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return dataSetList;
        }

        #endregion

        #region Get Dataset Fields
        /// <summary>
        /// Fields label from dataset
        /// </summary>       

        public List<DatasetFieldProperties> GetDataSetFields()
        {
            List<DatasetFieldProperties> datasetColumns = new List<DatasetFieldProperties>();
            DatasetFieldProperties dsField;

            //Read XML document
            XmlDocument document = new XmlDocument();

            if (File.Exists(HttpContext.Current.Server.MapPath(this.DatasetFieldsDataFile)))
            {
                document.Load(HttpContext.Current.Server.MapPath(this.DatasetFieldsDataFile));
            }

            XmlNodeList nodeListDatasetColumn = document.SelectNodes("fields/field");

            //Read all the column information and add to the List

            foreach (XmlNode node in nodeListDatasetColumn)
            {
                dsField = new DatasetFieldProperties();
                dsField.IdentiFier = node.Attributes["identifier"].Value;
                dsField.IsVisible = node.Attributes["visible"].Value.ToLower() == "true" ? true : false;

                if (dsField.IsVisible)
                {
                    DatasetFieldProperties dsSubField;
                    XmlNodeList fieldsList = document.SelectNodes("fields/field/" + dsField.IdentiFier);

                    if (fieldsList != null && fieldsList.Count > 0)
                    {
                        foreach (XmlNode field in fieldsList)
                        {
                            dsSubField = new DatasetFieldProperties();
                            dsSubField.IsVisible = field.Attributes["visible"].Value.ToLower() == "true" ? true : false;
                            dsSubField.Value = field.Attributes["value"].Value;
                            dsSubField.IdentiFier = field.Attributes["identifier"].Value;
                            dsSubField.ResourceString = field.Attributes["resourceString"].Value;
                            if (dsSubField.IsVisible)
                            {
                                datasetColumns.Add(dsSubField);
                            }
                        }
                    }
                    else
                    {
                        datasetColumns.Add(dsField);
                    }

                }
            }
            return datasetColumns;
        }

        #endregion

        #region _GetAllAnalyticSettings
        public Dictionary<string, string> _GetAllAnalyticSettings()
        {
            try
            {
                XDocument xdoc = new XDocument();
                if (File.Exists(HttpContext.Current.Server.MapPath(this.FilterConditionDataFile)))
                {
                    xdoc = XDocument.Load(HttpContext.Current.Server.MapPath(this.FilterConditionDataFile));
                }
                Dictionary<string, string> keyValues = new Dictionary<string, string>();

                var map = xdoc.Root.Elements()
                                   .ToDictionary(a => (string)a.Attribute("key"),
                                                 a => (string)a.Attribute("value"));


                keyValues = map;
                return keyValues;
            }
            catch
            {
                //bubble the exception
                throw;
            }

        }
        #endregion
        #endregion
    }
}
