﻿using System;
using System.Data;
using System.Data.SqlClient;
using Asentia.Common;

namespace Asentia.LRS.Library
{
    public class ReportSubscription
    {
        #region Constructors
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ReportSubscription()
        { ;}
        #endregion

        #region Properties
        #endregion

        #region Static Methods
        #region GetByToken
        /// <summary>
        /// Gets a report subscription's information by its token.
        /// </summary>
        /// <returns>DataTable of report subscription information</returns>
        public static DataTable GetByToken(string token, int idCaller)
        {
            DataTable dt = new DataTable();
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            try
            {
                databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
                databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

                databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
                databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);
                databaseObject.AddParameter("@token", Guid.Parse(token), SqlDbType.UniqueIdentifier, 36, ParameterDirection.Input);

                SqlDataReader sdr = databaseObject.ExecuteDataReader("[ReportSubscription.GetByToken]", true);
                dt.Load(sdr);
                sdr.Close();

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return dt;
        }
        #endregion

        #region DeleteByToken
        /// <summary>
        /// Deletes a report subscription by its token.
        /// Used when a user unsubscribes from a report via the link in the email.
        /// </summary>
        public static void DeleteByToken(string token, int idCaller)
        {
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@callerLangString", AsentiaSessionState.UserCulture, SqlDbType.NVarChar, 10, ParameterDirection.Input);
            databaseObject.AddParameter("@idCaller", idCaller, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@token", Guid.Parse(token), SqlDbType.UniqueIdentifier, 36, ParameterDirection.Input);

            try
            {
                databaseObject.ExecuteNonQuery("[ReportSubscription.DeleteByToken]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }
        }
        #endregion
        #endregion
    }
}
