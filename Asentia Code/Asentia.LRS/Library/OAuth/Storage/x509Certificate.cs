#region License

// The MIT License
//
// Copyright (c) 2006-2008 DevDefined Limited.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#endregion

using System.Security.Cryptography.X509Certificates;
using Asentia.LRS.Library.OAuth.Framework;
using Asentia.Common;
using System.Data;
using System.Data.SqlClient;
using System;
using System.Text;

namespace Asentia.LRS.Library.OAuth.Storage
{
	public static class x509Certificate
	{
        public static X509Certificate2 OAuthTestCertificate(IConsumer consumer)
		{
            return new X509Certificate2(Encoding.ASCII.GetBytes(_GetPublicKey(consumer.ConsumerKey)));
		}

        public static string GetPublicKey(string oAuthKey)
        {
           return _GetPublicKey(oAuthKey);
        }

        private static string _GetPublicKey(string key)
        {
            string publicKey = string.Empty;
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@PublicKey", key, SqlDbType.NVarChar, -1, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@oAuthKey", key, SqlDbType.NVarChar, 200, ParameterDirection.Input);

            try
            {
                SqlDataReader dataReader = databaseObject.ExecuteDataReader("[xAPIOAuthConsumer.GetPublicKey]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                                
                if (returnCode == DBReturnValue.OK)
                {
                    publicKey = Convert.ToString(databaseObject.Command.Parameters["@PublicKey"].Value);
                }

                dataReader.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return publicKey;
        }
	}
}