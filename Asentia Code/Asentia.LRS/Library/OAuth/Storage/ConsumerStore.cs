#region License

// The MIT License
//
// Copyright (c) 2006-2008 DevDefined Limited.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#endregion

using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Asentia.LRS.Library.OAuth.Framework;
using Asentia.Common;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.LRS.Library.OAuth.Storage
{
	public class ConsumerStore : IConsumerStore
	{
        private bool _IsConsumer(IConsumer consumer)
        {
            bool result = false;

            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            
            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@idEndpoint", consumer.IdEndpoint, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@oAuthKey", consumer.ConsumerKey, SqlDbType.NVarChar, 200, ParameterDirection.Input);

            try
            {
                SqlDataReader dataReader = databaseObject.ExecuteDataReader("[xAPIoAuthConsumer.IsConsumer]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);

                if (returnCode == DBReturnValue.OK)
                {
                    consumer.IdEndpoint = Convert.ToInt32(databaseObject.Command.Parameters["@idEndpoint"].Value);
                    result = true;
                }

                dataReader.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return result;
        }

        private string _GetConsumerSecret(string key)
        {
            string secret = string.Empty;
            AsentiaDatabase databaseObject = new AsentiaDatabase();

            databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
            databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);
            databaseObject.AddParameter("@oAuthSecret", key, SqlDbType.NVarChar, 200, ParameterDirection.Output);

            databaseObject.AddParameter("@idCallerSite", AsentiaSessionState.IdSite, SqlDbType.Int, 4, ParameterDirection.Input);
            databaseObject.AddParameter("@oAuthKey", key, SqlDbType.NVarChar, 200, ParameterDirection.Input);

            try
            {
                SqlDataReader dataReader = databaseObject.ExecuteDataReader("[xAPIoAuthConsumer.GetConsumerSecret]", true);

                DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(databaseObject.Command.Parameters["@Return_Code"].Value);
                string errorDescriptionCode = databaseObject.Command.Parameters["@Error_Description_Code"].Value.ToString();
                AsentiaDatabase.ThrowDBExceptionIfReturnCodeValueIsError(returnCode, errorDescriptionCode);
                
                if (returnCode == DBReturnValue.OK)
                {
                    secret = Convert.ToString(databaseObject.Command.Parameters["@oAuthSecret"].Value);
                }

                dataReader.Close();
            }
            catch
            {
                throw;
            }
            finally
            {
                databaseObject.Dispose();
            }

            return secret;
        }

		public bool IsConsumer(IConsumer consumer)
		{
           return (_IsConsumer(consumer) && String.IsNullOrWhiteSpace(consumer.Realm));
		}

		public void SetConsumerSecret(IConsumer consumer, string consumerSecret)
		{
			throw new NotImplementedException();
		}

		public string GetConsumerSecret(IOAuthContext consumer)
		{
            return _GetConsumerSecret(consumer.ConsumerKey);
		}

		public void SetConsumerCertificate(IConsumer consumer, X509Certificate2 certificate)
		{
			throw new NotImplementedException();
		}

		public AsymmetricAlgorithm GetConsumerPublicKey(IConsumer consumer)
		{
			return x509Certificate.OAuthTestCertificate(consumer).PublicKey.Key;
		}
	}
}