#region License

// The MIT License
//
// Copyright (c) 2006-2008 DevDefined Limited.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#endregion

using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Asentia.LRS.Library.OAuth.Framework;
using Asentia.LRS.Library.OAuth.Storage;
using Asentia.LRS.Library.OAuth.Tests;
using Asentia.Common;
using System.Data;
using System.Data.SqlClient;

namespace Asentia.LRS.Library.OAuth.Testing
{
	public class TestConsumerStore : IConsumerStore
	{
        ////private bool _IsConsumer(string key)
        ////{
        ////    bool result = false;

        ////    AsentiaDatabase databaseObject = new AsentiaDatabase();

        ////    databaseObject.AddParameter("@Return_Code", null, SqlDbType.Int, 4, ParameterDirection.Output);
        ////    databaseObject.AddParameter("@Error_Description_Code", null, SqlDbType.NVarChar, 50, ParameterDirection.Output);

        ////    databaseObject.AddParameter("@key", key, SqlDbType.VarChar, -1, ParameterDirection.Input);

        ////    try
        ////    {
        ////        SqlDataReader dataReader = databaseObject.ExecuteDataReader("[Asentia.LRS.Library.xAPIOAuthConsumer.IsConsumer]", true);
        ////        while (dataReader.Read())
        ////        {
        ////            DBReturnValue returnCode = (DBReturnValue)Convert.ToInt32(dataReader["@Return_Code"]);
        ////            string errorDescriptionCode = dataReader["@Error_Description_Code"].ToString();

        ////            if (returnCode == DBReturnValue.DetailsNotFound)
        ////            { throw new DatabaseDetailsNotFoundException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode)); }

        ////            if (returnCode == DBReturnValue.FieldNotUnique)
        ////            { throw new DatabaseObjectNotUniqueException(DBErrorDescriptions.ResourceManager.GetString(errorDescriptionCode)); }

        ////            if (returnCode == DBReturnValue.OK)
        ////                result = true;
        ////        }
        ////        dataReader.Close();
        ////    }
        ////    catch
        ////    {
        ////        throw;
        ////    }
        ////    finally
        ////    {
        ////        databaseObject.Dispose();
        ////    }

        ////    return result;
        ////}
        
		public bool IsConsumer(IConsumer consumer)
		{
            return (consumer.ConsumerKey == "key" && string.IsNullOrEmpty(consumer.Realm));
           // return (_IsConsumer(consumer.ConsumerKey) && string.IsNullOrEmpty(consumer.Realm));
		}

		public void SetConsumerSecret(IConsumer consumer, string consumerSecret)
		{
			throw new NotImplementedException();
		}

		public string GetConsumerSecret(IOAuthContext consumer)
		{
			return "secret";
		}

		public void SetConsumerCertificate(IConsumer consumer, X509Certificate2 certificate)
		{
			throw new NotImplementedException();
		}

		public AsymmetricAlgorithm GetConsumerPublicKey(IConsumer consumer)
		{
			return TestCertificates.OAuthTestCertificate().PublicKey.Key;
		}
	}
}