IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUser] (
	[idUser]						INT					IDENTITY(2, 1)							NOT NULL,
	[firstName]						NVARCHAR(255)												NOT NULL,
	[middleName]					NVARCHAR(255)												NULL,
	[lastName]						NVARCHAR(255)												NOT NULL,
	[displayName]					NVARCHAR(768)												NOT NULL,
	[avatar]						NVARCHAR(255)												NULL,
	[email]							NVARCHAR(255)												NULL,
	[username]						NVARCHAR(512)												NOT NULL,
	[password]						NVARCHAR(512)		COLLATE SQL_Latin1_General_CP1_CS_AS	NOT NULL,
	[idSite]						INT															NOT NULL,
	[idTimezone]					INT															NOT NULL,
	[objectGUID]					UNIQUEIDENTIFIER											NULL,
	[activationGUID]				UNIQUEIDENTIFIER											NULL,
	[distinguishedName]				NVARCHAR(512)												NULL,
	[isActive]						BIT															NOT NULL,
	[mustchangePassword]			BIT															NOT NULL,
	[dtCreated]						DATETIME													NOT NULL,
	[dtExpires]						DATETIME													NULL,
	[isDeleted]						BIT															NOT NULL,
	[dtDeleted]						DATETIME													NULL,
	[idLanguage]					INT															NOT NULL,
	[dtModified]					DATETIME													NULL,
	[dtLastLogin]					DATETIME													NULL,
	[employeeID]					NVARCHAR(255)												NULL,
	[company]						NVARCHAR(255)												NULL,
	[address]						NVARCHAR(512)												NULL,
	[city]							NVARCHAR(255)												NULL,
	[province]						NVARCHAR(255)												NULL,
	[postalcode]					NVARCHAR(25)												NULL,
	[country]						NVARCHAR(50)												NULL,
	[phonePrimary]					NVARCHAR(25)												NULL,
	[phoneWork]						NVARCHAR(25)												NULL,
	[phoneFax]						NVARCHAR(25)												NULL,
	[phoneHome]						NVARCHAR(25)												NULL,
	[phoneMobile]					NVARCHAR(25)												NULL,
	[phonePager]					NVARCHAR(25)												NULL,
	[phoneOther]					NVARCHAR(25)												NULL,
	[department]					NVARCHAR(255)												NULL,
	[division]						NVARCHAR(255)												NULL,
	[region]						NVARCHAR(255)												NULL,
	[jobTitle]						NVARCHAR(255)												NULL,
	[jobClass]						NVARCHAR(255)												NULL,
	[gender]						NVARCHAR(1)													NULL,
	[race]							NVARCHAR(64)												NULL,
	[dtDOB]							DATETIME													NULL,
	[dtHire]						DATETIME													NULL,
	[dtTerm]						DATETIME													NULL,
	[dtSessionExpires]				DATETIME													NULL,
	[activeSessionId]				NVARCHAR(80)												NULL,
	[field00]						NVARCHAR(255)												NULL,
	[field01]						NVARCHAR(255)												NULL,
	[field02]						NVARCHAR(255)												NULL,
	[field03]						NVARCHAR(255)												NULL,
	[field04]						NVARCHAR(255)												NULL,
	[field05]						NVARCHAR(255)												NULL,
	[field06]						NVARCHAR(255)												NULL,
	[field07]						NVARCHAR(255)												NULL,
	[field08]						NVARCHAR(255)												NULL,
	[field09]						NVARCHAR(255)												NULL,
	[field10]						NVARCHAR(255)												NULL,
	[field11]						NVARCHAR(255)												NULL,
	[field12]						NVARCHAR(255)												NULL,
	[field13]						NVARCHAR(255)												NULL,
	[field14]						NVARCHAR(255)												NULL,
	[field15]						NVARCHAR(255)												NULL,
	[field16]						NVARCHAR(255)												NULL,
	[field17]						NVARCHAR(255)												NULL,
	[field18]						NVARCHAR(255)												NULL,
	[field19]						NVARCHAR(255)												NULL,
	[dtLastPermissionCheck]			DATETIME													NULL,
	[isRegistrationApproved]		BIT															NULL,
	[dtApproved]					DATETIME													NULL,
	[dtDenied]						DATETIME													NULL,
	[idApprover]					INT															NULL,
	[rejectionComments]				NVARCHAR(MAX)												NULL,
	[dtMarkedDisabled]				DATETIME													NULL,
	[disableLoginFromLoginForm]		BIT															NULL,
	[exemptFromIPLoginRestriction]	BIT															NULL,
	[optOutOfEmailNotifications]	BIT															NULL,
	[dtUserAgreementAgreed]			DATETIME													NULL,
	[excludeFromLeaderboards]		BIT															NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUser ADD CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED (idUser ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_User_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUser ADD CONSTRAINT [FK_User_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_User_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUser ADD CONSTRAINT [FK_User_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblUser]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblUser]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblUser (
	displayName,
	username, 
	email,
	employeeID 
)
KEY INDEX PK_User
ON Asentia -- insert index to this catalog

/**
TEMPLATE USER
**/

SET IDENTITY_INSERT [tblUser] ON

INSERT INTO tblUser (
	idUser, 
	firstName,
	lastName,
	displayName,
	username,
	[password],
	idSite,
	idTimezone,
	idLanguage,
	isActive,
	mustchangePassword,
	dtCreated,
	isDeleted
	)
SELECT
	1, 
	'[FIRSTNAME]',
	'[LASTNAME]',
	'[DISPLAY_NAME]',
	'[USERNAME]',
	'[PASSWORD]',
	1,
	17,
	57,
	1,
	0,
	GETUTCDATE(),
	0
WHERE NOT EXISTS (
	SELECT 1
	FROM tblUser
	WHERE idUser = 1
)

SET IDENTITY_INSERT [tblUser] OFF