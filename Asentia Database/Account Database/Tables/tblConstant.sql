IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblConstant]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblConstant] (
	[idConstant]			INT				IDENTITY(1,1)	NOT NULL,
	[key]					NVARCHAR(32)					NOT NULL, 
	[order]					INT								NOT NULL,
	[value]					NVARCHAR(64)					NOT NULL,
	[caption]				NVARCHAR(128)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Constant]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblConstant ADD CONSTRAINT [PK_Constant] PRIMARY KEY CLUSTERED (idConstant ASC)