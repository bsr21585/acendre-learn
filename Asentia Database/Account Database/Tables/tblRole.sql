IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRole]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRole] (
	[idRole]					INT				IDENTITY(100,1)		NOT NULL,
	[idSite]					INT									NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRole ADD CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED (idRole ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Role_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRole ADD CONSTRAINT [FK_Role_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRole]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRole]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRole (
	name
)
KEY INDEX PK_Role
ON Asentia -- insert index to this catalog

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblRole] ON

INSERT INTO [tblRole] (
	idRole,
	idSite,
	name
)
SELECT
	idRole,
	idSite,
	name
FROM (
	SELECT			1 AS idRole, 1 AS idSite, 'System Administrator' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblRole R WHERE R.idRole = MAIN.idRole)

SET IDENTITY_INSERT [tblRole] OFF