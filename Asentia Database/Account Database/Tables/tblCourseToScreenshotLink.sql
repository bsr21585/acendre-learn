IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseToScreenshotLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseToScreenshotLink] (
	[idCourseToScreenshotLink]	INT		 IDENTITY(1,1)	NOT NULL,
	[idSite]					INT						NOT NULL,
	[idCourse]					INT						NOT NULL, 
	[filename]					NVARCHAR(255)			NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseToScreenshotLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToScreenshotLink ADD CONSTRAINT [PK_CourseToScreenshotLink] PRIMARY KEY CLUSTERED (idCourseToScreenshotLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToScreenshotLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToScreenshotLink ADD CONSTRAINT [FK_CourseToScreenshotLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToScreenshotLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToScreenshotLink ADD CONSTRAINT [FK_CourseToScreenshotLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)
