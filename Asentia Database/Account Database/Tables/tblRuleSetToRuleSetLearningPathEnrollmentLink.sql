IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToRuleSetLearningPathEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetToRuleSetLearningPathEnrollmentLink] (
	[idRuleSetToRuleSetLearningPathEnrollmentLink]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]											INT						NOT NULL,
	[idRuleSet]											INT						NOT NULL,
	[idRuleSetLearningPathEnrollment]					INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToRuleSetLearningPathEnrollmentLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetLearningPathEnrollmentLink ADD CONSTRAINT [PK_RuleSetToRuleSetLearningPathEnrollmentLink] PRIMARY KEY CLUSTERED (idRuleSetToRuleSetLearningPathEnrollmentLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetLearningPathEnrollmentLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetLearningPathEnrollmentLink ADD CONSTRAINT [FK_RuleSetToRuleSetLearningPathEnrollmentLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetLearningPathEnrollmentLink ADD CONSTRAINT FK_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetLearningPathEnrollmentLink ADD CONSTRAINT FK_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSetLearningPathEnrollment FOREIGN KEY ([idRuleSetLearningPathEnrollment]) REFERENCES [tblRuleSetLearningPathEnrollment] ([idRuleSetLearningPathEnrollment])