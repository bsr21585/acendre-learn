IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventTypeToEventTypeRecipientLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventTypeToEventTypeRecipientLink] (
	[idEventTypeToEventTypeRecipientLink]		INT		IDENTITY (1, 1)		NOT NULL,
	[idEventType]								INT							NOT NULL,
	[idEventTypeRecipient]						INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventTypeToEventTypeRecipientLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventTypeToEventTypeRecipientLink ADD CONSTRAINT [PK_EventTypeToEventTypeRecipientLink] PRIMARY KEY CLUSTERED (idEventTypeToEventTypeRecipientLink ASC)

/**
INSERT VALUES

DO NOT EVER USE THE IDENTITY VALUES FROM THIS TABLE!! THEY ONLY EXIST TO ENABLE REPLICATION.
INSTEAD, ONLY REFERENCE THE eventType AND eventTypeRecipient ID VALUES.

**/

SET IDENTITY_INSERT [tblEventTypeToEventTypeRecipientLink] ON

-- ID 190 IS THE NEXT ID

INSERT INTO [tblEventTypeToEventTypeRecipientLink] (
	idEventTypeToEventTypeRecipientLink,
	idEventType,
	idEventTypeRecipient
)
SELECT
	idEventTypeToEventTypeRecipientLink,
	idEventType,
	idEventTypeRecipient
FROM (
	-- User Created
	SELECT			1 AS idEventTypeToEventTypeRecipientLink, 101 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	2 AS idEventTypeToEventTypeRecipientLink, 101 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	3 AS idEventTypeToEventTypeRecipientLink, 101 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	141 AS idEventTypeToEventTypeRecipientLink, 101 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Deleted
	UNION SELECT	4 AS idEventTypeToEventTypeRecipientLink, 102 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	5 AS idEventTypeToEventTypeRecipientLink, 102 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	6 AS idEventTypeToEventTypeRecipientLink, 102 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	142 AS idEventTypeToEventTypeRecipientLink, 102 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Expires
	UNION SELECT	7 AS idEventTypeToEventTypeRecipientLink, 103 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	8 AS idEventTypeToEventTypeRecipientLink, 103 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	9 AS idEventTypeToEventTypeRecipientLink, 103 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	143 AS idEventTypeToEventTypeRecipientLink, 103 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Registration Approved
	UNION SELECT	108 AS idEventTypeToEventTypeRecipientLink, 104 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	109 AS idEventTypeToEventTypeRecipientLink, 104 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	144 AS idEventTypeToEventTypeRecipientLink, 104 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)	

	-- User Registration Rejected
	UNION SELECT	110 AS idEventTypeToEventTypeRecipientLink, 105 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	111 AS idEventTypeToEventTypeRecipientLink, 105 AS idEventType, 3 AS idEventTypeRecipient -- User	
	UNION SELECT	145 AS idEventTypeToEventTypeRecipientLink, 105 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Message Center Inbox 
	UNION SELECT	120 AS idEventTypeToEventTypeRecipientLink, 106 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	146 AS idEventTypeToEventTypeRecipientLink, 106 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Registration Request Submitted
	UNION SELECT	135 AS idEventTypeToEventTypeRecipientLink, 107 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	136 AS idEventTypeToEventTypeRecipientLink, 107 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	137 AS idEventTypeToEventTypeRecipientLink, 107 AS idEventType, 11 AS idEventTypeRecipient -- Approver
	UNION SELECT	147 AS idEventTypeToEventTypeRecipientLink, 107 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Enrolled
	UNION SELECT	10 AS idEventTypeToEventTypeRecipientLink, 201 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	11 AS idEventTypeToEventTypeRecipientLink, 201 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	12 AS idEventTypeToEventTypeRecipientLink, 201 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	148 AS idEventTypeToEventTypeRecipientLink, 201 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Expires
	UNION SELECT	13 AS idEventTypeToEventTypeRecipientLink, 202 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	14 AS idEventTypeToEventTypeRecipientLink, 202 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	15 AS idEventTypeToEventTypeRecipientLink, 202 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	149 AS idEventTypeToEventTypeRecipientLink, 202 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Revoked
	UNION SELECT	16 AS idEventTypeToEventTypeRecipientLink, 203 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	17 AS idEventTypeToEventTypeRecipientLink, 203 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	18 AS idEventTypeToEventTypeRecipientLink, 203 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	150 AS idEventTypeToEventTypeRecipientLink, 203 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Due
	UNION SELECT	19 AS idEventTypeToEventTypeRecipientLink, 204 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	20 AS idEventTypeToEventTypeRecipientLink, 204 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	21 AS idEventTypeToEventTypeRecipientLink, 204 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	151 AS idEventTypeToEventTypeRecipientLink, 204 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Completed
	UNION SELECT	22 AS idEventTypeToEventTypeRecipientLink, 205 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	23 AS idEventTypeToEventTypeRecipientLink, 205 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	24 AS idEventTypeToEventTypeRecipientLink, 205 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	152 AS idEventTypeToEventTypeRecipientLink, 205 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Start
	UNION SELECT	102 AS idEventTypeToEventTypeRecipientLink, 206 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	103 AS idEventTypeToEventTypeRecipientLink, 206 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	104 AS idEventTypeToEventTypeRecipientLink, 206 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	153 AS idEventTypeToEventTypeRecipientLink, 206 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Enrollment Request Approved
	UNION SELECT	112 AS idEventTypeToEventTypeRecipientLink, 207 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	113 AS idEventTypeToEventTypeRecipientLink, 207 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	114 AS idEventTypeToEventTypeRecipientLink, 207 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	154 AS idEventTypeToEventTypeRecipientLink, 207 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Enrollment Request Rejected
	UNION SELECT	115 AS idEventTypeToEventTypeRecipientLink, 208 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	116 AS idEventTypeToEventTypeRecipientLink, 208 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	117 AS idEventTypeToEventTypeRecipientLink, 208 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	155 AS idEventTypeToEventTypeRecipientLink, 208 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Enrollment Request Submitted
	UNION SELECT	138 AS idEventTypeToEventTypeRecipientLink, 209 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	139 AS idEventTypeToEventTypeRecipientLink, 209 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	140 AS idEventTypeToEventTypeRecipientLink, 209 AS idEventType, 11 AS idEventTypeRecipient -- Approver
	UNION SELECT	156 AS idEventTypeToEventTypeRecipientLink, 209 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Lesson/Session Passed
	UNION SELECT	25 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	26 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	27 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	28 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	157 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Lesson/Session Failed
	UNION SELECT	29 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	30 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	31 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	32 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	158 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Lesson/Session Completed
	UNION SELECT	33 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	34 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	35 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	36 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	159 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)
	
	-- OJT Request Submitted
	UNION SELECT	98 AS idEventTypeToEventTypeRecipientLink, 304 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	99 AS idEventTypeToEventTypeRecipientLink, 304 AS idEventType, 9 AS idEventTypeRecipient -- Proctor(s)
	UNION SELECT	160 AS idEventTypeToEventTypeRecipientLink, 304 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Task Submitted
	UNION SELECT	100 AS idEventTypeToEventTypeRecipientLink, 305 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	101 AS idEventTypeToEventTypeRecipientLink, 305 AS idEventType, 9 AS idEventTypeRecipient -- Proctor(s)
	UNION SELECT	161 AS idEventTypeToEventTypeRecipientLink, 305 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Meets
	UNION SELECT	37 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	38 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	39 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 6 AS idEventTypeRecipient -- Learner(s)
	UNION SELECT	40 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 8 AS idEventTypeRecipient -- Supervisor(s)
	UNION SELECT	162 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Instructor Assigned/Changed
	UNION SELECT	41 AS idEventTypeToEventTypeRecipientLink, 402 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	42 AS idEventTypeToEventTypeRecipientLink, 402 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	43 AS idEventTypeToEventTypeRecipientLink, 402 AS idEventType, 6 AS idEventTypeRecipient -- Learner(s)
	UNION SELECT	163 AS idEventTypeToEventTypeRecipientLink, 402 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Instructor Removed
	UNION SELECT	44 AS idEventTypeToEventTypeRecipientLink, 403 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	45 AS idEventTypeToEventTypeRecipientLink, 403 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	46 AS idEventTypeToEventTypeRecipientLink, 403 AS idEventType, 6 AS idEventTypeRecipient -- Learner(s)
	UNION SELECT	164 AS idEventTypeToEventTypeRecipientLink, 403 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Joined
	UNION SELECT	47 AS idEventTypeToEventTypeRecipientLink, 404 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	48 AS idEventTypeToEventTypeRecipientLink, 404 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	49 AS idEventTypeToEventTypeRecipientLink, 404 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	165 AS idEventTypeToEventTypeRecipientLink, 404 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Removed
	UNION SELECT	50 AS idEventTypeToEventTypeRecipientLink, 405 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	51 AS idEventTypeToEventTypeRecipientLink, 405 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	52 AS idEventTypeToEventTypeRecipientLink, 405 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	166 AS idEventTypeToEventTypeRecipientLink, 405 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Time/Location Changed
	UNION SELECT	53 AS idEventTypeToEventTypeRecipientLink, 406 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	54 AS idEventTypeToEventTypeRecipientLink, 406 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	55 AS idEventTypeToEventTypeRecipientLink, 406 AS idEventType, 6 AS idEventTypeRecipient -- Learner(s)
	UNION SELECT	167 AS idEventTypeToEventTypeRecipientLink, 406 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Joined
	UNION SELECT	123 AS idEventTypeToEventTypeRecipientLink, 407 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	124 AS idEventTypeToEventTypeRecipientLink, 407 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	125 AS idEventTypeToEventTypeRecipientLink, 407 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	168 AS idEventTypeToEventTypeRecipientLink, 407 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Removed
	UNION SELECT	126 AS idEventTypeToEventTypeRecipientLink, 408 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	127 AS idEventTypeToEventTypeRecipientLink, 408 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	128 AS idEventTypeToEventTypeRecipientLink, 408 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	169 AS idEventTypeToEventTypeRecipientLink, 408 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Promoted
	UNION SELECT	129 AS idEventTypeToEventTypeRecipientLink, 409 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	130 AS idEventTypeToEventTypeRecipientLink, 409 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	131 AS idEventTypeToEventTypeRecipientLink, 409 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	170 AS idEventTypeToEventTypeRecipientLink, 409 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Demoted
	UNION SELECT	132 AS idEventTypeToEventTypeRecipientLink, 410 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	133 AS idEventTypeToEventTypeRecipientLink, 410 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	134 AS idEventTypeToEventTypeRecipientLink, 410 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	171 AS idEventTypeToEventTypeRecipientLink, 410 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Enrolled/Starts
	UNION SELECT	56 AS idEventTypeToEventTypeRecipientLink, 501 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	57 AS idEventTypeToEventTypeRecipientLink, 501 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	58 AS idEventTypeToEventTypeRecipientLink, 501 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	172 AS idEventTypeToEventTypeRecipientLink, 501 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Expires
	UNION SELECT	59 AS idEventTypeToEventTypeRecipientLink, 502 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	60 AS idEventTypeToEventTypeRecipientLink, 502 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	61 AS idEventTypeToEventTypeRecipientLink, 502 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	173 AS idEventTypeToEventTypeRecipientLink, 502 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Revoked
	UNION SELECT	62 AS idEventTypeToEventTypeRecipientLink, 503 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	63 AS idEventTypeToEventTypeRecipientLink, 503 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	64 AS idEventTypeToEventTypeRecipientLink, 503 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	174 AS idEventTypeToEventTypeRecipientLink, 503 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Due
	UNION SELECT	65 AS idEventTypeToEventTypeRecipientLink, 504 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	66 AS idEventTypeToEventTypeRecipientLink, 504 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	67 AS idEventTypeToEventTypeRecipientLink, 504 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	175 AS idEventTypeToEventTypeRecipientLink, 504 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Completed
	UNION SELECT	68 AS idEventTypeToEventTypeRecipientLink, 505 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	69 AS idEventTypeToEventTypeRecipientLink, 505 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	70 AS idEventTypeToEventTypeRecipientLink, 505 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	176 AS idEventTypeToEventTypeRecipientLink, 505 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Start
	UNION SELECT	105 AS idEventTypeToEventTypeRecipientLink, 506 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	106 AS idEventTypeToEventTypeRecipientLink, 506 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	107 AS idEventTypeToEventTypeRecipientLink, 506 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	177 AS idEventTypeToEventTypeRecipientLink, 506 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Enrolled
	UNION SELECT	71 AS idEventTypeToEventTypeRecipientLink, 601 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	72 AS idEventTypeToEventTypeRecipientLink, 601 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	73 AS idEventTypeToEventTypeRecipientLink, 601 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	178 AS idEventTypeToEventTypeRecipientLink, 601 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Expires
	UNION SELECT	74 AS idEventTypeToEventTypeRecipientLink, 602 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	75 AS idEventTypeToEventTypeRecipientLink, 602 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	76 AS idEventTypeToEventTypeRecipientLink, 602 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	179 AS idEventTypeToEventTypeRecipientLink, 602 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Revoked
	UNION SELECT	77 AS idEventTypeToEventTypeRecipientLink, 603 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	78 AS idEventTypeToEventTypeRecipientLink, 603 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	79 AS idEventTypeToEventTypeRecipientLink, 603 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	180 AS idEventTypeToEventTypeRecipientLink, 603 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Awarded
	UNION SELECT	80 AS idEventTypeToEventTypeRecipientLink, 604 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	81 AS idEventTypeToEventTypeRecipientLink, 604 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	82 AS idEventTypeToEventTypeRecipientLink, 604 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	181 AS idEventTypeToEventTypeRecipientLink, 604 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Renewed
	UNION SELECT	83 AS idEventTypeToEventTypeRecipientLink, 605 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	84 AS idEventTypeToEventTypeRecipientLink, 605 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	85 AS idEventTypeToEventTypeRecipientLink, 605 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	182 AS idEventTypeToEventTypeRecipientLink, 605 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Task Submitted
	UNION SELECT	118 AS idEventTypeToEventTypeRecipientLink, 606 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	119 AS idEventTypeToEventTypeRecipientLink, 606 AS idEventType, 9 AS idEventTypeRecipient -- Proctor(s)
	UNION SELECT	183 AS idEventTypeToEventTypeRecipientLink, 606 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certificate Earned
	UNION SELECT	86 AS idEventTypeToEventTypeRecipientLink, 701 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	87 AS idEventTypeToEventTypeRecipientLink, 701 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	88 AS idEventTypeToEventTypeRecipientLink, 701 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	184 AS idEventTypeToEventTypeRecipientLink, 701 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certificate Awarded
	UNION SELECT	89 AS idEventTypeToEventTypeRecipientLink, 702 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	90 AS idEventTypeToEventTypeRecipientLink, 702 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	91 AS idEventTypeToEventTypeRecipientLink, 702 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	185 AS idEventTypeToEventTypeRecipientLink, 702 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certificate Revoked
	UNION SELECT	92 AS idEventTypeToEventTypeRecipientLink, 703 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	93 AS idEventTypeToEventTypeRecipientLink, 703 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	94 AS idEventTypeToEventTypeRecipientLink, 703 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	186 AS idEventTypeToEventTypeRecipientLink, 703 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certificate Expired
	UNION SELECT	95 AS idEventTypeToEventTypeRecipientLink, 704 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	96 AS idEventTypeToEventTypeRecipientLink, 704 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	97 AS idEventTypeToEventTypeRecipientLink, 704 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	187 AS idEventTypeToEventTypeRecipientLink, 704 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course Moderated Message
	UNION SELECT	121 AS idEventTypeToEventTypeRecipientLink, 901 AS idEventType, 10 AS idEventTypeRecipient -- Moderator
	UNION SELECT	188 AS idEventTypeToEventTypeRecipientLink, 901 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Group Moderated Message
	UNION SELECT	122 AS idEventTypeToEventTypeRecipientLink, 902 AS idEventType, 10 AS idEventTypeRecipient -- Moderator
	UNION SELECT	189 AS idEventTypeToEventTypeRecipientLink, 902 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblEventTypeToEventTypeRecipientLink ETETR WHERE ETETR.idEventTypeToEventTypeRecipientLink = MAIN.idEventTypeToEventTypeRecipientLink)

SET IDENTITY_INSERT [tblEventTypeToEventTypeRecipientLink] OFF