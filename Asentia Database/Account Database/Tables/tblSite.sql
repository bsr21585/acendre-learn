IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSite]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSite] (
	[idSite]						INT					IDENTITY(2,1)	NOT NULL,
	[hostname]						NVARCHAR(255)						NOT NULL,
	[favicon]						NVARCHAR(255)						NULL,
	[password]						NVARCHAR(512)						NOT NULL,
	[isActive]						BIT									NOT NULL,
	[dtExpires]						DATETIME							NULL,
	[title]							NVARCHAR(255)						NOT NULL,
	[company]						NVARCHAR(255)						NOT NULL,
	[contactName]					NVARCHAR(255)						NULL,
	[contactEmail]					NVARCHAR(255)						NULL,
	[userLimit]						INT									NULL,
	[kbLimit]						INT									NULL,
	[idLanguage]					INT									NOT NULL,
	[idTimezone]					INT									NOT NULL,
	[dtLicenseAgreementExecuted]	DATETIME							NULL,
	[dtUserAgreementModified]		DATETIME							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSite ADD CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED (idSite ASC)

/**
TEMPLATE SITE
**/

SET IDENTITY_INSERT [tblSite] ON

INSERT INTO tblSite (
	idSite,
	hostname,
	favicon,
	[password],
	isActive,
	dtExpires,
	title,
	company,
	contactName,
	contactEmail,
	userLimit,
	kbLimit,
	idLanguage,
	idTimezone,
	dtLicenseAgreementExecuted
)
SELECT
	1,
	'default',
	'NULL', 
	'[PASSWORD]',
	1,
	NULL,
	'[TITLE]',
	'[COMPANY]',
	'[CONTACTNAME]',
	'[CONTACTEMAIL]',
	NULL,
	NULL,
	57, -- ENGLISH
	15, -- EST
	NULL
WHERE NOT EXISTS (
	SELECT 1
	FROM tblSite
	WHERE idSite = 1
)

SET IDENTITY_INSERT [tblSite] OFF

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblSite]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblSite]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblSite (
	title,
	hostname
)
	KEY INDEX PK_Site
ON Asentia -- insert index to this catalog