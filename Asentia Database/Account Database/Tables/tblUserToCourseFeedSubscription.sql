IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToCourseFeedSubscription]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToCourseFeedSubscription] (
	[idUserToCourseFeedSubscription]		INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idUser]								INT							NOT NULL,
	[idCourse]								INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToCourseFeedSubscription]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToCourseFeedSubscription ADD CONSTRAINT [PK_UserToCourseFeedSubscription] PRIMARY KEY CLUSTERED (idUserToCourseFeedSubscription ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToCourseFeedSubscription_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToCourseFeedSubscription ADD CONSTRAINT [FK_UserToCourseFeedSubscription_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToCourseFeedSubscription_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToCourseFeedSubscription ADD CONSTRAINT [FK_UserToCourseFeedSubscription_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToCourseFeedSubscription_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToCourseFeedSubscription ADD CONSTRAINT [FK_UserToCourseFeedSubscription_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)