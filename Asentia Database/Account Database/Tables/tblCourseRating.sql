IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseRating]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseRating] (
	[idCourseRating]	INT		IDENTITY(1,1)	NOT NULL,
	[idSite]			INT						NOT NULL,
	[idCourse]			INT						NOT NULL, 
	[idUser]			INT						NOT NULL,
	[rating]			INT						NOT NULL,
	[timestamp]			DATETIME				NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseRating]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseRating ADD CONSTRAINT [PK_CourseRating] PRIMARY KEY CLUSTERED ([idCourseRating] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseRating_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseRating ADD CONSTRAINT [FK_CourseRating_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] ([idCourse])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseRating_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseRating ADD CONSTRAINT [FK_CourseRating_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseRating_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseRating ADD CONSTRAINT [FK_CourseRating_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)