IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollmentLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetEnrollmentLanguage] (
	[idRuleSetEnrollmentLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idRuleSetEnrollment]				INT								NOT NULL, 
	[idLanguage]						INT								NOT NULL,
	[label]								NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetEnrollmentLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetEnrollmentLanguage ADD CONSTRAINT [PK_RuleSetEnrollmentLanguage] PRIMARY KEY CLUSTERED (idRuleSetEnrollmentLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollmentLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetEnrollmentLanguage ADD CONSTRAINT [FK_RuleSetEnrollmentLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollmentLanguage_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetEnrollmentLanguage ADD CONSTRAINT [FK_RuleSetEnrollmentLanguage_RuleSetEnrollment] FOREIGN KEY (idRuleSetEnrollment) REFERENCES [tblRuleSetEnrollment] (idRuleSetEnrollment)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollmentLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollmentLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetEnrollmentLanguage (
	label
)
KEY INDEX PK_RuleSetEnrollmentLanguage
ON Asentia -- insert index to this catalog