IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCode] (
	[idCouponCode]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[code]							NVARCHAR(10)					NOT NULL,
	[comments]						NVARCHAR(MAX)					NULL,
	[usesAllowed]					INT								NOT NULL,
	[discount]						FLOAT							NOT NULL,
	[discountType]					INT						        NOT NULL,
	[forCourse]						INT								NULL,
	[forCatalog]					INT								NULL,
	[forLearningPath]				INT								NULL,
	[dtStart]						DATETIME						NULL,
	[dtEnd]							DATETIME						NULL,
	[isSingleUsePerUser]			BIT								NULL,
	[isDeleted]						BIT								NULL,
	[dtDeleted]						DATETIME						NULL,
	[forStandupTraining]			INT								NULL
) ON [PRIMARY] 

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCode ADD CONSTRAINT [PK_CouponCode] PRIMARY KEY CLUSTERED (idCouponCode ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCode_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCode ADD CONSTRAINT [FK_CouponCode_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCouponCode]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCouponCode]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCouponCode (
	code
)
KEY INDEX PK_CouponCode
ON Asentia -- insert index to this catalog