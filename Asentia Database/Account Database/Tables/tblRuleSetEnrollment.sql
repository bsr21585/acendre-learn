IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetEnrollment] (
	[idRuleSetEnrollment]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL, 
	[idCourse]								INT								NOT NULL, 
	[idTimezone]							INT								NOT NULL,
	[priority]								INT								NOT NULL,	-- per course. indicates which enrollment 'takes' first based on the rules
	[label]									NVARCHAR(255)					NOT NULL,
	[isLockedByPrerequisites]				BIT								NOT NULL,
	[isFixedDate]							BIT								NOT NULL,
	[dtStart]								DATETIME						NOT NULL,
	[dtEnd]									DATETIME						NULL,
	[dtCreated]								DATETIME						NOT NULL,
	[delayInterval]							INT								NULL,
	[delayTimeframe]						NVARCHAR(4)						NULL,
	[dueInterval]							INT								NULL,
	[dueTimeframe]							NVARCHAR(4)						NULL,
	[recurInterval]							INT								NULL,
	[recurTimeframe]						NVARCHAR(4)						NULL,
	[expiresFromStartInterval]				INT								NULL,
	[expiresFromStartTimeframe]				NVARCHAR(4)						NULL,
	[expiresFromFirstLaunchInterval]		INT								NULL,
	[expiresFromFirstLaunchTimeframe]		NVARCHAR(4)						NULL,
	--[expiresFromCompletionInterval]		INT								NULL,
	--[expiresFromCompletionTimeframe]		NVARCHAR(4)						NULL,
	[forceReassignment]						BIT								NULL,
	[idParentRuleSetEnrollment]				INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetEnrollment] ADD CONSTRAINT [PK_RuleSetEnrollment] PRIMARY KEY CLUSTERED ([idRuleSetEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollment_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetEnrollment] ADD CONSTRAINT [FK_RuleSetEnrollment_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetEnrollment] ADD CONSTRAINT [FK_RuleSetEnrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetEnrollment] ADD CONSTRAINT [FK_RuleSetEnrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollment]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollment]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetEnrollment (
	label
)
KEY INDEX PK_RuleSetEnrollment
ON Asentia -- insert index to this catalog