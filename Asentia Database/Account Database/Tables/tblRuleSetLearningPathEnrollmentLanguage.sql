IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollmentLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetLearningPathEnrollmentLanguage] (
	[idRuleSetLearningPathEnrollmentLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]										INT								NOT NULL,
	[idRuleSetLearningPathEnrollment]				INT								NOT NULL,
	[idLanguage]									INT								NOT NULL,
	[label]											NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetLearningPathEnrollmentLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLearningPathEnrollmentLanguage ADD CONSTRAINT [PK_RuleSetLearningPathEnrollmentLanguage] PRIMARY KEY CLUSTERED (idRuleSetLearningPathEnrollmentLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollmentLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLearningPathEnrollmentLanguage ADD CONSTRAINT [FK_RuleSetLearningPathEnrollmentLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollmentLanguage_RuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLearningPathEnrollmentLanguage ADD CONSTRAINT [FK_RuleSetLearningPathEnrollmentLanguage_RuleSetLearningPathEnrollment] FOREIGN KEY (idRuleSetLearningPathEnrollment) REFERENCES [tblRuleSetLearningPathEnrollment] (idRuleSetLearningPathEnrollment)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollmentLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollmentLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetLearningPathEnrollmentLanguage (
	label
)
KEY INDEX PK_RuleSetLearningPathEnrollmentLanguage
ON Asentia -- insert index to this catalog