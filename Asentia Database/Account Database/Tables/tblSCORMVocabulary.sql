IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSCORMVocabulary]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSCORMVocabulary] (
	[idSCORMVocabulary]			INT				 IDENTITY(1,1)	NOT NULL,
	[value]						NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SCORMVocabulary]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblSCORMVocabulary] ADD CONSTRAINT [PK_SCORMVocabulary] PRIMARY KEY CLUSTERED ([idSCORMVocabulary] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblSCORMVocabulary] ON

INSERT INTO [tblSCORMVocabulary] (
	idSCORMVocabulary,
	value
)
SELECT
	idSCORMVocabulary,
	value
FROM (
	SELECT 0 AS idSCORMVocabulary, 'unknown' AS value
	UNION SELECT 1 AS idSCORMVocabulary, 'not attempted' AS value
	UNION SELECT 2 AS idSCORMVocabulary, 'completed' AS value
	UNION SELECT 3 AS idSCORMVocabulary, 'incomplete' AS value
	UNION SELECT 4 AS idSCORMVocabulary, 'passed' AS value
	UNION SELECT 5 AS idSCORMVocabulary, 'failed' AS value
	UNION SELECT 6 AS idSCORMVocabulary, 'correct' AS value
	UNION SELECT 7 AS idSCORMVocabulary, 'wrong' AS value
	UNION SELECT 8 AS idSCORMVocabulary, 'neutral' AS value
	UNION SELECT 9 AS idSCORMVocabulary, 'unanticipated' AS value
	UNION SELECT 10 AS idSCORMVocabulary, 'incorrect' AS value

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblSCORMVocabulary SV WHERE SV.idSCORMVocabulary = MAIN.idSCORMVocabulary)

SET IDENTITY_INSERT [tblSCORMVocabulary] OFF