IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetLearningPathEnrollment] (
	[idRuleSetLearningPathEnrollment]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]									INT								NOT NULL, 
	[idLearningPath]							INT								NOT NULL, 
	[idTimezone]								INT								NOT NULL,
	[priority]									INT								NOT NULL,	-- per learning path. indicates which enrollment 'takes' first based on the rules
	[label]										NVARCHAR(255)					NOT NULL,
	[dtStart]									DATETIME						NOT NULL,
	[dtEnd]										DATETIME						NULL,					
	[delayInterval]								INT								NULL,
	[delayTimeframe]							NVARCHAR(4)						NULL,
	[dueInterval]								INT								NULL,
	[dueTimeframe]								NVARCHAR(4)						NULL,
	[expiresFromStartInterval]					INT								NULL,
	[expiresFromStartTimeframe]					NVARCHAR(4)						NULL,
	[expiresFromFirstLaunchInterval]			INT								NULL,
	[expiresFromFirstLaunchTimeframe]			NVARCHAR(4)						NULL,
	[idParentRuleSetLearningPathEnrollment]		NVARCHAR(4)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetLearningPathEnrollment] ADD CONSTRAINT [PK_RuleSetLearningPathEnrollment] PRIMARY KEY CLUSTERED ([idRuleSetLearningPathEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollment_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetLearningPathEnrollment] ADD CONSTRAINT [FK_RuleSetLearningPathEnrollment_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetLearningPathEnrollment] ADD CONSTRAINT [FK_RuleSetLearningPathEnrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetLearningPathEnrollment] ADD CONSTRAINT [FK_RuleSetLearningPathEnrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollment]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollment]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetLearningPathEnrollment (
	label
)
KEY INDEX PK_RuleSetLearningPathEnrollment
ON Asentia -- insert index to this catalog