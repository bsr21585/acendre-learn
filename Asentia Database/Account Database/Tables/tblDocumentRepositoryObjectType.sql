IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryObjectType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDocumentRepositoryObjectType] (
	[idDocumentRepositoryObjectType]	INT				IDENTITY(1,1)	NOT NULL,
	[name]								NVARCHAR(50)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DocumentRepositoryObjectType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryObjectType] ADD CONSTRAINT [PK_DocumentRepositoryObjectType] PRIMARY KEY CLUSTERED ([idDocumentRepositoryObjectType] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblDocumentRepositoryObjectType] ON

INSERT INTO [tblDocumentRepositoryObjectType] (
	idDocumentRepositoryObjectType, 
	name
)
SELECT
	idDocumentRepositoryObjectType,
	name
FROM (
	SELECT		 1 AS idDocumentRepositoryObjectType, 'Group' AS name
	UNION SELECT 2 AS idDocumentRepositoryObjectType, 'Course' AS name
	UNION SELECT 3 AS idDocumentRepositoryObjectType, 'LearningPath' AS name
	UNION SELECT 4 AS idDocumentRepositoryObjectType, 'Profile' AS name

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblDocumentRepositoryObjectType DROT WHERE DROT.idDocumentRepositoryObjectType = MAIN.idDocumentRepositoryObjectType)

SET IDENTITY_INSERT [tblDocumentRepositoryObjectType] OFF