IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResourceTypeLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblResourceTypeLanguage] (
	[idResourceTypeLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idResourceType]				INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[resourceType]							NVARCHAR(255)					NULL
	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ResourceTypeLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceTypeLanguage ADD CONSTRAINT [PK_ResourceTypeLanguage] PRIMARY KEY CLUSTERED (idResourceTypeLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceTypeLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceTypeLanguage ADD CONSTRAINT [FK_ResourceTypeLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceTypeLanguage_ResourceType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceTypeLanguage ADD CONSTRAINT [FK_ResourceTypeLanguage_ResourceType] FOREIGN KEY (idResourceType) REFERENCES [tblResourceType] (idResourceType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceTypeLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceTypeLanguage ADD CONSTRAINT [FK_ResourceTypeLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceTypeLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceTypeLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblResourceTypeLanguage (
	resourceType
)
KEY INDEX PK_ResourceTypeLanguage
ON Asentia -- insert index to this resource type