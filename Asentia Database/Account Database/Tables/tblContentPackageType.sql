IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblContentPackageType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblContentPackageType] (
	[idContentPackageType]		INT				IDENTITY (1, 1)		NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]
GO

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ContentPackageType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackageType] ADD CONSTRAINT [PK_ContentPackageType] PRIMARY KEY CLUSTERED ([idContentPackageType] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblContentPackageType] ON

INSERT INTO [tblContentPackageType] (
	idContentPackageType,
	name
)
SELECT
	idContentPackageType,
	name
FROM (
	SELECT		 1 AS idContentPackageType, 'SCORM' AS name
	UNION SELECT 2 AS idContentPackageType, 'xAPI' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblContentPackageType CPT WHERE CPT.idContentPackageType = MAIN.idContentPackageType)

SET IDENTITY_INSERT [tblContentPackageType] OFF