IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupFeedMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupFeedMessage] (
	[idGroupFeedMessage]			INT				IDENTITY (1, 1)		NOT NULL, 
	[idSite]						INT									NOT NULL,
	[idGroup]						INT									NOT NULL,
	[idLanguage]					INT									NULL,
	[idAuthor]						INT									NOT NULL,
	[idParentGroupFeedMessage]		INT									NULL,
	[message]						NVARCHAR(MAX)						NOT NULL,
	[timestamp]						DATETIME							NOT NULL,
	[dtApproved]					DATETIME							NULL,
	[idApprover]					INT									NULL,
	[isApproved]					BIT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [PK_GroupFeedMessage] PRIMARY KEY CLUSTERED (idGroupFeedMessage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_ParentGroupFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_ParentGroupFeedMessage] FOREIGN KEY (idParentGroupFeedMessage) REFERENCES [tblGroupFeedMessage] (idGroupFeedMessage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Author]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Author] FOREIGN KEY (idAuthor) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Approver]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Approver] FOREIGN KEY (idApprover) REFERENCES [tblUser] (idUser)