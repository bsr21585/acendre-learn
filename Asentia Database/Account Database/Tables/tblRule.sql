IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRule]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRule] (
	[idRule]						INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idRuleSet]						INT								NOT NULL,
	[userField]						NVARCHAR(25)					NULL,
	[operator]						NVARCHAR(25)					NOT NULL,
	[textValue]						NVARCHAR(255)					NULL,
	[dateValue]						DATETIME						NULL,
	[numValue]						INT								NULL,
	[bitValue]						BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Rule]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRule ADD CONSTRAINT [PK_Rule] PRIMARY KEY CLUSTERED (idRule ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Rule_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRule ADD CONSTRAINT [FK_Rule_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Rule_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRule ADD CONSTRAINT [FK_Rule_RuleSet] FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)