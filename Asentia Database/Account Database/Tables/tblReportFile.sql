IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportFile]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportFile] (
	[idReportFile]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idReport]						INT								NOT NULL, 
	[idUser]						INT								NOT NULL, 
	[filename]						NVARCHAR(255)					NOT NULL,
	[dtCreated]						DATETIME						NULL,
	[htmlKb]						INT								NULL,
	[csvKb]							INT								NULL,
	[pdfKb]							INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportFile]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportFile ADD CONSTRAINT [PK_ReportFile] PRIMARY KEY CLUSTERED (idReportFile ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportFile_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportFile ADD CONSTRAINT [FK_ReportFile_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportFile_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportFile ADD CONSTRAINT [FK_ReportFile_Report] FOREIGN KEY (idReport) REFERENCES [tblReport] (idReport)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportFile_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportFile ADD CONSTRAINT [FK_ReportFile_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
