/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathFeed]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathFeed] (
	[idLearningPathFeed]				INT		IDENTITY (1, 1)		NOT NULL,
	[idSite]							INT							NOT NULL,
	[idLearningPath]					INT							NOT NULL,
	[isActive]							BIT							NULL,
	[isModerated]						BIT							NULL,
	[isOpenSubscription]				BIT							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathFeed]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeed] ADD CONSTRAINT [PK_LearningPathFeed] PRIMARY KEY CLUSTERED (idLearningPathFeed ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeed_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeed] ADD CONSTRAINT [FK_LearningPathFeed_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeed_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeed] ADD CONSTRAINT [FK_LearningPathFeed_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)