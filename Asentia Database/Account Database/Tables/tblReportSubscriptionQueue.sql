IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportSubscriptionQueue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportSubscriptionQueue] (
	[idReportSubscriptionQueue]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL,
	[idReportSubscription]					INT								NOT NULL,
	[idReport]								INT								NOT NULL,
	[idDataset]								INT								NOT NULL,
	[reportName]							NVARCHAR(255)					NOT NULL, 
	[idRecipient]							INT								NOT NULL,
	[recipientLangString]					NVARCHAR(10)					NOT NULL,
	[recipientFullName]						NVARCHAR(512)					NOT NULL,
	[recipientFirstName]					NVARCHAR(255)					NOT NULL,
	[recipientLogin]						NVARCHAR(512)					NOT NULL,
	[recipientEmail]						NVARCHAR(255)					NOT NULL,
	[recipientTimezone]						INT								NOT NULL,
	[recipientTimezoneDotNetName]			NVARCHAR(255)					NOT NULL,
	[priority]								INT								NULL,
	[dtAction]								DATETIME						NOT NULL,	-- the date the email should be sent
	[dtSent]								DATETIME						NULL,		-- the record of the date the email ACTUALLY sent
	[statusDescription]						NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportSubscriptionQueue]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscriptionQueue ADD CONSTRAINT [PK_ReportSubscriptionQueue] PRIMARY KEY CLUSTERED (idReportSubscriptionQueue ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportSubscriptionQueue_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscriptionQueue ADD CONSTRAINT [FK_ReportSubscriptionQueue_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReportSubscriptionQueue]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReportSubscriptionQueue]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblReportSubscriptionQueue (
	reportName,
	recipientFullName
)
KEY INDEX PK_ReportSubscriptionQueue
ON Asentia -- insert index to this catalog