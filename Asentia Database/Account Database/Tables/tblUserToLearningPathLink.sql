/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToLearningPathLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToLearningPathLink] (
	[idUserToLearningPathLink]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idUser]								INT							NOT NULL,
	[idLearningPath]						INT							NOT NULL,
	[idRuleSet]								INT							NULL,
	[created]								DATETIME					NOT NULL
)

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToLearningPathLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToLearningPathLink ADD CONSTRAINT [PK_UserToLearningPathLink] PRIMARY KEY CLUSTERED (idUserToLearningPathLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToLearningPathLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToLearningPathLink ADD CONSTRAINT [FK_UserToLearningPathLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToLearningPathLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToLearningPathLink ADD CONSTRAINT FK_UserToLearningPathLink_User FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToLearningPathLink_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToLearningPathLink ADD CONSTRAINT FK_UserToLearningPathLink_LearningPath FOREIGN KEY ([idLearningPath]) REFERENCES [tblLearningPath] ([idLearningPath])