IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-TinCanState]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-TinCanState] (
	[idData-TinCanState]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idEndpoint]						INT							NULL,
	[stateId]							NVARCHAR(100)				NOT NULL,
	[agent]								NVARCHAR(MAX)				NOT NULL,
	[activityId]						NVARCHAR(100)				NOT NULL,
	[registration]						NVARCHAR(50)				NULL,
	[contentType]						NVARCHAR(50)				NOT NULL,
	[docContents]						VARBINARY(MAX)				NOT NULL,
	[dtUpdated]							DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-TinCanState]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanState] ADD CONSTRAINT [PK_Data-TinCanState] PRIMARY KEY CLUSTERED ([idData-TinCanState] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanState_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanState] ADD CONSTRAINT [FK_Data-TinCanState_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanState_IdEndpoint]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanState] ADD CONSTRAINT [FK_Data-TinCanState_IdEndpoint] FOREIGN KEY (idEndpoint) REFERENCES [tblxAPIoAuthConsumer] (idxAPIoAuthConsumer)