IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteToDomainAliasLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteToDomainAliasLink] (
	[idSiteToDomainAliasLink]			INT				IDENTITY(1, 1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[domain]							NVARCHAR(255)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteToDomainAliasLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteToDomainAliasLink ADD CONSTRAINT [PK_SiteToDomainAliasLink] PRIMARY KEY CLUSTERED (idSiteToDomainAliasLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteToDomainAliasLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteToDomainAliasLink ADD CONSTRAINT [FK_SiteToDomainAliasLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)	