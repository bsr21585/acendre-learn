IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryFolder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDocumentRepositoryFolder] (
	[idDocumentRepositoryFolder]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[idDocumentRepositoryObjectType]	INT								NOT NULL,
	[idObject]							INT								NOT NULL,
	[name]								NVARCHAR(255)					NOT NULL,
	[isDeleted]							BIT								NULL,
	[dtDeleted]							DATETIME						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DocumentRepositoryFolder]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryFolder] ADD CONSTRAINT [PK_DocumentRepositoryFolder] PRIMARY KEY CLUSTERED ([idDocumentRepositoryFolder] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryFolder_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryFolder] ADD CONSTRAINT [FK_DocumentRepositoryFolder_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryFolder_ObjectType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryFolder] ADD CONSTRAINT [FK_DocumentRepositoryFolder_ObjectType] FOREIGN KEY (idDocumentRepositoryObjectType) REFERENCES [tblDocumentRepositoryObjectType] (idDocumentRepositoryObjectType)

--/**
--FULL TEXT INDEX
--**/
--IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryFolder]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
--AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryFolder]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
--CREATE FULLTEXT INDEX ON tblDocumentRepositoryFolder (
--	name
--	)
--KEY INDEX PK_DocumentRepositoryFolder
--ON Asentia -- insert index to this catalog