IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificateLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificateLanguage] (
	[idCertificateLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[idCertificate]				INT								NOT NULL, 
	[idLanguage]				INT								NOT NULL,
	[name]						NVARCHAR(255)					NOT NULL,
	[description]				NVARCHAR(MAX)					NULL,
	[filename]					NVARCHAR(255)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificateLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateLanguage ADD CONSTRAINT [PK_CertificateLanguage] PRIMARY KEY CLUSTERED (idCertificateLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateLanguage ADD CONSTRAINT [FK_CertificateLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateLanguage_Certificate]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateLanguage ADD CONSTRAINT [FK_CertificateLanguage_Certificate] FOREIGN KEY (idCertificate) REFERENCES [tblCertificate] (idCertificate)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateLanguage ADD CONSTRAINT [FK_CertificateLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificateLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificateLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificateLanguage (
	name,
	[description]
)
KEY INDEX PK_CertificateLanguage
ON Asentia -- insert index to this catalog