IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalytic]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalytic] (
	[idAnalytic]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idUser]						INT								NOT NULL, 
	[idDataset]						INT								NOT NULL, 
	[idChartType]                   INT                             NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL,
	[excludeDays]					NVARCHAR(255)				    NULL,
	[isPublic]						BIT								NOT NULL,
	[frequency]                     INT                             NULL,
	[dtStart]						DATETIME						NULL,
	[activity]                      INT                             NULL,
	[dtEnd]				       	    DATETIME						NULL,
	[dtCreated]						DATETIME						NULL,
	[dtModified]					DATETIME						NULL,
	[isDeleted]						BIT								NULL,
	[dtDeleted]						DATETIME						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Analytic]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalytic ADD CONSTRAINT [PK_Analytic] PRIMARY KEY CLUSTERED (idAnalytic ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Analytic_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalytic ADD CONSTRAINT [FK_Analytic_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Analytic_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalytic ADD CONSTRAINT [FK_Analytic_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Report_Dataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalytic ADD CONSTRAINT [FK_Analytic_Dataset] FOREIGN KEY (idDataset) REFERENCES [tblDataset] (idDataset)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAnalytic]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAnalytic]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblAnalytic (
	title
)
KEY INDEX PK_Analytic
ON Asentia -- insert index to this catalog