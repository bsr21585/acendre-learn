IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblTinCanVerb]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTinCanVerb](
	idVerb              INT					IDENTITY(1,1)	    NOT NULL,
	verb				NVARCHAR(25)						    NOT NULL,
	URI					NVARCHAR(100)							NOT NULL,
	dtCreated			DATETIME								NULL,
	dtModified			DATETIME								NULL,
	isDeleted			BIT										NOT NULL,
	dtDeleted			DATETIME								NULL
) ON [PRIMARY]
GO

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_TinCanVerb]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblTinCanVerb] ADD CONSTRAINT [PK_TinCanVerb] PRIMARY KEY CLUSTERED ([idVerb] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblTinCanVerb] ON

INSERT INTO [tblTinCanVerb] (
	idVerb ,
	verb,		
	URI,			
   dtCreated,
	dtModified,	
	isDeleted,	
    dtDeleted	
)
SELECT 
       idVerb ,
	   verb,		
	   URI,		
	   dtCreated,
	   dtModified,
	   isDeleted,	
	   dtDeleted	
FROM	 ( 

	SELECT		 1 AS idVerb, 'answered' AS verb, 'http://adlnet.gov/expapi/verbs/answered' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 2 AS idVerb, 'asked' AS verb, 'http://adlnet.gov/expapi/verbs/asked' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 3 AS idVerb, 'attempted' AS verb, 'http://adlnet.gov/expapi/verbs/attempted' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 4 AS idVerb, 'attended' AS verb, 'http://adlnet.gov/expapi/verbs/attended' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 5 AS idVerb, 'commented' AS verb, 'http://adlnet.gov/expapi/verbs/commented' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 6 AS idVerb, 'completed' AS verb, 'http://adlnet.gov/expapi/verbs/completed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 7 AS idVerb, 'exited' AS verb, 'http://adlnet.gov/expapi/verbs/exited' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 8 AS idVerb, 'experienced' AS verb, 'http://adlnet.gov/expapi/verbs/experienced' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 9 AS idVerb, 'failed' AS verb, 'http://adlnet.gov/expapi/verbs/failed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 10 AS idVerb, 'imported' AS verb, 'http://adlnet.gov/expapi/verbs/imported' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 11 AS idVerb, 'initialized' AS verb, 'http://adlnet.gov/expapi/verbs/initialized' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 12 AS idVerb, 'interacted' AS verb, 'http://adlnet.gov/expapi/verbs/interacted' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 13 AS idVerb, 'launched' AS verb, 'http://adlnet.gov/expapi/verbs/launched' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 14 AS idVerb, 'mastered' AS verb, 'http://adlnet.gov/expapi/verbs/mastered' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 15 AS idVerb, 'passed' AS verb, 'http://adlnet.gov/expapi/verbs/passed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 16 AS idVerb, 'preferred' AS verb, 'http://adlnet.gov/expapi/verbs/preferred' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 17 AS idVerb, 'progressed' AS verb, 'http://adlnet.gov/expapi/verbs/progressed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 18 AS idVerb, 'registered' AS verb, 'http://adlnet.gov/expapi/verbs/registered' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 19 AS idVerb, 'responded' AS verb, 'http://adlnet.gov/expapi/verbs/responded' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 20 AS idVerb, 'resumed' AS verb, 'http://adlnet.gov/expapi/verbs/resumed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 21 AS idVerb, 'scored' AS verb, 'http://adlnet.gov/expapi/verbs/scored' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 22 AS idVerb, 'shared' AS verb, 'http://adlnet.gov/expapi/verbs/shared' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 23 AS idVerb, 'suspended' AS verb, 'http://adlnet.gov/expapi/verbs/suspended' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 24 AS idVerb, 'terminated' AS verb, 'http://adlnet.gov/expapi/verbs/terminated' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 25 AS idVerb, 'voided' AS verb, 'http://adlnet.gov/expapi/verbs/voided' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted

) MAIN
	WHERE NOT EXISTS (SELECT 1 FROM tblTinCanVerb TCV WHERE TCV.idVerb = MAIN.idVerb)

SET IDENTITY_INSERT [tblTinCanVerb] OFF