IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblMediaType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblMediaType] (
	[idMediaType]		INT				IDENTITY (1, 1)		NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]
GO

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_MediaType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblMediaType] ADD CONSTRAINT [PK_MediaType] PRIMARY KEY CLUSTERED ([idMediaType] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblMediaType] ON

INSERT INTO [tblMediaType] (
	idMediaType,
	name
)
SELECT
	idMediaType,
	name
FROM (
	SELECT		 1 AS idMediaType, 'Video' AS name
	UNION SELECT 2 AS idMediaType, 'PowerPoint' AS name
	UNION SELECT 3 AS idMediaType, 'PDF' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblMediaType MT WHERE MT.idMediaType = MAIN.idMediaType)

SET IDENTITY_INSERT [tblMediaType] OFF