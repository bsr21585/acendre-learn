IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticDataset]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticDataset] (
	[idDataset]							INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[name]								NVARCHAR(255)						NOT NULL,
	[storedProcedureName]				NVARCHAR(50)						NOT NULL,
	[htmlTemplatePath]					NVARCHAR(255)						NULL,
	[description]						NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticDataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataset ADD CONSTRAINT [PK_AnalyticDataset] PRIMARY KEY CLUSTERED (idDataset ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticDataset_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataset ADD CONSTRAINT [FK_AnalyticDataset_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)


/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblAnalyticDataset] ON

INSERT INTO tblAnalyticDataset (
	idDataset,
	idSite,
	name,
	storedProcedureName,
	[description]
)
SELECT
	idDataset,
	idSite,
	name,
	storedProcedureName,
	[description]
FROM (
	SELECT 1 AS idDataset, 1 AS idSite, 'Login Activity' AS name, '[Dataset.LoginActivity]' AS storedProcedureName, 'Complete information of login activities of all registered users.' AS [description]
	UNION SELECT 2 AS idDataset, 1 AS idSite, 'Course Completions' AS name, '[Dataset.CourseCompletions]' AS storedProcedureName, 'Complete information of course enrolment and completion.' AS [description]
	UNION SELECT 3 AS idDataset, 1 AS idSite, 'User And Group Snapshot' AS name, '[Dataset.UserGroupSnapshot]' AS storedProcedureName, 'Complete information of users aNdd groups informations.' AS [description]
	UNION SELECT 6 AS idDataset, 1 AS idSite, 'Use Statistics' AS name, '[Dataset.UseStatistics]' AS storedProcedureName, 'Complete information of users' AS [description]
	UNION SELECT 7 AS idDataset, 1 AS idSite, 'Certificates' AS name, '[Dataset.Certificates]' AS storedProcedureName, 'Certificate award information. Includes certificates that have been earned as well as those that have been manually awarded.' AS [description]

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblAnalyticDataset D WHERE D.idDataset = MAIN.idDataset)

SET IDENTITY_INSERT [tblAnalyticDataset] OFF