IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLesson]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLesson] (
	[idLesson]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[idCourse]					INT								NOT NULL, 
	[title]						NVARCHAR(255)					NOT NULL,
	[revcode]					NVARCHAR(32)					NULL,
	[shortDescription]			NVARCHAR(512)					NULL, 
	[longDescription]			NVARCHAR(MAX)					NULL, 
	[dtCreated]					DATETIME						NULL,
	[dtModified]				DATETIME						NULL,
	[isDeleted]					BIT								NOT NULL,
	[dtDeleted]					DATETIME						NULL,
	[order]						INT								NULL,
	[searchTags]				NVARCHAR(512)					NULL,
	[isOptional]				BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLesson ADD CONSTRAINT [PK_Lesson] PRIMARY KEY CLUSTERED ([idLesson] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Lesson_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLesson ADD CONSTRAINT [FK_Lesson_Course] FOREIGN KEY ([idCourse]) REFERENCES [tblCourse] ([idCourse])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Lesson_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLesson ADD CONSTRAINT [FK_Lesson_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLesson]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLesson]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLesson (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_Lesson
ON Asentia -- insert index to this catalog