IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToSupervisorLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblUserToSupervisorLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventEmailQueue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblEventEmailQueue]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblEventLog]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventTypeToEventTypeRecipientLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblEventTypeToEventTypeRecipientLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotificationLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblEventEmailNotificationLanguage]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotification]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblEventEmailNotification]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventTypeRecipient]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblEventTypeRecipient]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblEventType]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationSegmentToCourseCreditLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblCertificationSegmentToCourseCreditLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblCertificationSegment]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertification]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblCertification]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblInboxMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblInboxMessage]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSCORMPackage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblSCORMPackage]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-SCOObj]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblData-SCOObj]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-SCOInt]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblData-SCOInt]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-SCO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblData-SCO]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-Lesson]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblData-Lesson]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLesson-SCORM]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLesson-SCORM]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLesson-LiveSession]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLesson-LiveSession]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLesson-Live]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLesson-Live]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLessonLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLessonLanguage]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLesson]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLesson]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToPermissionLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblUserToPermissionLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToCourseFeedSubscription]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblUserToCourseFeedSubscription]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToLearningPathLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblUserToLearningPathLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathToCourseLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLearningPathToCourseLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathFeedMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLearningPathFeedMessage]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathFeedModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLearningPathFeedModerator]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathFeed]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLearningPathFeed]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLearningPathModerator]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLearningPathApprover]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLearningPathLanguage]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLearningPath]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblRuleSetGroupLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblUserGroupLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblRuleSetGroupLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblRuleSetToRoleLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRuleSetToGroupLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRuleSetEnrollmentLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRuleSetRoleLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRuleSetToRuleSetEnrollmentLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRoleToPermissionLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRoleToPermissionLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblUserRuleSetEnrollmentLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblUserToGroupLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblUserToRoleLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblUserToRuleSetEnrollmentLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblUserRoleLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseToPrerequisiteLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCourseToPrerequisiteLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCoursePrerequisiteLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCoursePrerequisiteLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseCatalogLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCourseCatalogLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRule]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRule
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSet]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRuleSet
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRoleLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRoleLanguage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRole]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRole
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCatalogLanguage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCatalog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCatalog
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCatalogLanguage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEnrollmentRequest]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblEnrollmentRequest]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblEnrollment
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblGroupEnrollment
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblGroupLanguage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupFeedModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblGroupFeedModerator
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupFeedMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblGroupFeedMessage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupFeed]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblGroupFeed
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblGroupEnrollmentApprover
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupToPermissionLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblGroupToPermissionLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblGroup
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblRuleSetEnrollment
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseToCatalogLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCourseToCatalogLink
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCourseLanguage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseFeedModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCourseFeedModerator
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseFeedMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCourseFeedMessage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseFeed]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCourseFeed
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCourseEnrollmentApprover
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseExpert]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblCourseExpert]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourse]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblCourse
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblUser
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblExceptionLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblExceptionLog
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblSiteLanguage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteAvailableLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblSiteAvailableLanguage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteParam]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblSiteParam
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSite]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblSite
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTimezoneLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblTimezoneLanguage
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTimezone]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblTimezone
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblConstant]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblConstant
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccount]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblAccount
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSCORMVocabulary]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblSCORMVocabulary
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSCORMPackageType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblSCORMPackageType]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblLanguage]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblPermission]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE tblPermission
GO