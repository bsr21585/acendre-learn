IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResource]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	
CREATE TABLE [tblResource] (
	[idResource]					INT				IDENTITY (1, 1)		NOT NULL, 
	[idSite]						INT									NOT NULL, 
	[idParentResource]				INT									NULL,
	[idResourceType]                INT                                 NOT NULL,
	[name]						    NVARCHAR(255)						NOT NULL,
	[description]                   NVARCHAR(512)						NULL,
	[locationRoom]					NVARCHAR(255)						NULL,
	[locationBuilding]				NVARCHAR(255)						NULL,
	[locationCity]					NVARCHAR(255)						NULL,
	[locationProvince]				NVARCHAR(255)						NULL,
	[locationCountry]				NVARCHAR(255)						NULL,
	[identifier]					NVARCHAR(50)						NULL,
	[capacity]						INT									NULL,
	[ownerWithinSystem]             BIT									NOT NULL,
	[idOwner]						INT									NULL,
	[ownerName]						NVARCHAR(255)						NULL,
	[ownerEmail]					NVARCHAR(255)						NULL,
	[ownerContactInformation]		NVARCHAR(512)						NULL,
	[isMoveable]					BIT									NOT NULL,
	[isAvailable]					BIT									NOT NULL,
	[isDeleted]						BIT									NOT NULL,
	[dtDeleted]						DATETIME							NULL
	) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Resource]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [PK_Resource] PRIMARY KEY CLUSTERED (idResource ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Resource_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [FK_Resource_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Recource_ResourceType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [FK_Recource_ResourceType] FOREIGN KEY (idResourceType) REFERENCES [tblResourceType] (idResourceType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Recource_Resource]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [FK_Recource_Resource] FOREIGN KEY (idParentResource) REFERENCES [tblResource] (idResource)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Recource_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [FK_Recource_User] FOREIGN KEY (idOwner) REFERENCES [tblUser] (idUser)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResource]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResource]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblResource (
	name,
	[description]
)
KEY INDEX PK_Resource
ON Asentia -- insert index to this resource