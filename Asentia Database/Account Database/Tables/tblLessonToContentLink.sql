IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLessonToContentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLessonToContentLink] (
	[idLessonToContentLink]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idLesson]						INT								NOT NULL, 
	[idObject]						INT								NULL,
	[idContentType]					INT								NOT NULL,
	[idAssignmentDocumentType]		INT								NULL,
	[assignmentResourcePath]		NVARCHAR(255)					NULL,
	[allowSupervisorsAsProctor]		BIT								NULL,
	[allowCourseExpertsAsProctor]	BIT								NULL,
	[externalxAPIIdentifier]		NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LessonToContentLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonToContentLink ADD CONSTRAINT [PK_LessonToContentLink] PRIMARY KEY CLUSTERED ([idLessonToContentLink] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonToContentLink_Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonToContentLink ADD CONSTRAINT [FK_LessonToContentLink_Lesson] FOREIGN KEY ([idLesson]) REFERENCES [tblLesson] ([idLesson])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonToContentLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonToContentLink ADD CONSTRAINT [FK_LessonToContentLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)