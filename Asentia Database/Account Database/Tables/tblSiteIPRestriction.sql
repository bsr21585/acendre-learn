IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteIPRestriction]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteIPRestriction] (
	[idSiteIPRestriction]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[ip]							NVARCHAR(15)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteIPRestriction]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteIPRestriction ADD CONSTRAINT [PK_SiteIPRestriction] PRIMARY KEY CLUSTERED (idSiteIPRestriction ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteIPRestriction_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteIPRestriction ADD CONSTRAINT [FK_SiteIPRestriction_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)