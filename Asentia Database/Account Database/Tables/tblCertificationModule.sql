IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModule]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModule] (
	[idCertificationModule]				INT					IDENTITY(1,1)	NOT NULL,
	[idCertification]					INT									NOT NULL,
	[idSite]							INT									NOT NULL, 
	[title]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL, 
	[isAnyRequirementSet]				BIT									NOT NULL,
	[dtCreated]							DATETIME							NULL,
	[dtModified]						DATETIME							NULL,
	[isDeleted]							BIT									NOT NULL,
	[dtDeleted]							DATETIME							NULL,
	[isInitialRequirement]				BIT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModule]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModule ADD CONSTRAINT [PK_CertificationModule] PRIMARY KEY CLUSTERED (idCertificationModule ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModule_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModule ADD CONSTRAINT [FK_CertificationModule_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModule_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModule ADD CONSTRAINT [FK_CertificationModule_Certification] FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModule]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModule]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModule (
	title,
	shortDescription
)
KEY INDEX PK_CertificationModule
ON Asentia -- insert index to this catalog