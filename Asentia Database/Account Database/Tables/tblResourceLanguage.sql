IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResourceLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblResourceLanguage] (
	[idResourceLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idResource]					INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[name]							NVARCHAR(255)					NOT NULL,
	[description]			       	NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ResourceLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceLanguage ADD CONSTRAINT [PK_ResourceLanguage] PRIMARY KEY CLUSTERED (idResourceLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceLanguage ADD CONSTRAINT [FK_ResourceLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceLanguage_Resource]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceLanguage ADD CONSTRAINT [FK_ResourceLanguage_Resource] FOREIGN KEY (idResource) REFERENCES [tblResource] (idResource)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceLanguage ADD CONSTRAINT [FK_ResourceLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblResourceLanguage (
	name,
	[description]
)
KEY INDEX PK_ResourceLanguage
ON Asentia -- insert index to this catalog