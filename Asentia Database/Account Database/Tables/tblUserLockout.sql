IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserLockout]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserLockout] (
	[idUserLockout]						INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idUser]							INT							NOT NULL,
	[failCount]							INT							NULL,		-- this value indicates the number of failed login attempt
	[dtLockoutUntil]					DATETIME					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserLockout]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLockout ADD CONSTRAINT [PK_UserLockout] PRIMARY KEY CLUSTERED (idUserLockout ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserLockout_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLockout ADD CONSTRAINT [FK_UserLockout_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserLockout_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLockout ADD CONSTRAINT [FK_UserLockout_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	