IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticLanguage] (
	[idAnalyticLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idAnalytic]					INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL,
	[dtCreated]						DATETIME						NULL,
	[dtModified]                    DATETIME                        NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticLanguage ADD CONSTRAINT [PK_AnalyticLanguage] PRIMARY KEY CLUSTERED (idAnalyticLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticLanguage ADD CONSTRAINT [FK_AnalyticLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticLanguage_Analytic]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticLanguage ADD CONSTRAINT [FK_AnalyticLanguage_Analytic] FOREIGN KEY (idAnalytic) REFERENCES [tblAnalytic] (idAnalytic)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticLanguage ADD CONSTRAINT [FK_AnalyticLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAnalyticLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAnalyticLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblAnalyticLanguage (
	title
)
KEY INDEX PK_AnalyticLanguage
ON Asentia -- insert index to this catalog