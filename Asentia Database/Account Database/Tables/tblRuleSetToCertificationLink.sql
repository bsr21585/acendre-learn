IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToCertificationLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetToCertificationLink] (
	[idRuleSetToCertificationLink]	INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idRuleSet]						INT							NOT NULL,
	[idCertification]				INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToCertificationLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToCertificationLink ADD CONSTRAINT PK_RuleSetToCertificationLink PRIMARY KEY CLUSTERED (idRuleSetToCertificationLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToCertificationLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToCertificationLink ADD CONSTRAINT [FK_RuleSetToCertificationLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToCertificationLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToCertificationLink ADD CONSTRAINT FK_RuleSetToCertificationLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToCertificationLink_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToCertificationLink ADD CONSTRAINT FK_RuleSetToCertificationLink_Certification FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)