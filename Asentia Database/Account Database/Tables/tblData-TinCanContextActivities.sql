IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-TinCanContextActivities]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-TinCanContextActivities] (
	[idData-TinCanContextActivities]		INT				IDENTITY(1,1)	NOT NULL,
	[idData-TinCan]							INT								NOT NULL,
	[idSite]								INT								NOT NULL,
	[activityId]							NVARCHAR(100)					NOT NULL,
	[activityType]							NVARCHAR(10)					NOT NULL,
	[objectType]							NVARCHAR(30)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-TinCanContextActivities]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanContextActivities] ADD CONSTRAINT [PK_Data-TinCanContextActivities] PRIMARY KEY CLUSTERED ([idData-TinCanContextActivities] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanContextActivities_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanContextActivities] ADD CONSTRAINT [FK_Data-TinCanContextActivities_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanContextActivities_Data-TinCan]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanContextActivities] ADD CONSTRAINT [FK_Data-TinCanContextActivities_Data-TinCan] FOREIGN KEY ([idData-TinCan]) REFERENCES [tblData-TinCan] ([idData-TinCan])