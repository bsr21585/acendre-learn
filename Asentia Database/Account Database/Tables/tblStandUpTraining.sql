IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTraining]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTraining] (
	[idStandUpTraining]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[title]						NVARCHAR(255)					NOT NULL,
	[description]				NVARCHAR(MAX)					NULL,
	[objectives]				NVARCHAR(MAX)					NULL, 
	[isStandaloneEnroll]		BIT								NOT NULL,
	[isRestrictedEnroll]		BIT								NOT NULL,
	[isRestrictedDrop]			BIT								NOT NULL,
	[searchTags]				NVARCHAR(512)					NULL,
	[avatar]					NVARCHAR(255)					NULL,
	[cost]						FLOAT							NULL,
	[dtCreated]					DATETIME						NULL,
	[dtModified]				DATETIME						NULL,
	[isDeleted]					BIT								NULL,
	[dtDeleted]					DATETIME						NULL,
	[shortcode]					NVARCHAR(10)					NULL	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTraining]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTraining] ADD CONSTRAINT [PK_StandUpTraining] PRIMARY KEY CLUSTERED ([idStandUpTraining] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTraining_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTraining] ADD CONSTRAINT [FK_StandUpTraining_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTraining]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTraining]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblStandUpTraining (
	title,
	[description],
	searchTags
)
KEY INDEX PK_StandUpTraining
ON Asentia -- insert index to this catalog