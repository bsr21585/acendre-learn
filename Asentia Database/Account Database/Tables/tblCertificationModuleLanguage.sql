IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleLanguage] (
	[idCertificationModuleLanguage]		INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idCertificationModule]				INT									NOT NULL,
	[idLanguage]						INT									NOT NULL,
	[title]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleLanguage ADD CONSTRAINT [PK_CertificationModuleLanguage] PRIMARY KEY CLUSTERED (idCertificationModuleLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleLanguage ADD CONSTRAINT [FK_CertificationModuleLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleLanguage_CertificationModule]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleLanguage ADD CONSTRAINT [FK_CertificationModuleLanguage_CertificationModule] FOREIGN KEY (idCertificationModule) REFERENCES [tblCertificationModule] (idCertificationModule)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleLanguage ADD CONSTRAINT [FK_CertificationModuleLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleLanguage (
	title,
	shortDescription
)
KEY INDEX PK_CertificationModuleLanguage
ON Asentia -- insert index to this catalog