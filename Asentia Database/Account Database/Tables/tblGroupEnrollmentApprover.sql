IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupEnrollmentApprover] (
	[idGroupEnrollmentApprover]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]						INT						NOT NULL,
	[idGroup]						INT						NOT NULL, 
	[idUser]						INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupEnrollmentApprover ADD CONSTRAINT [PK_GroupEnrollmentApprover] PRIMARY KEY CLUSTERED (idGroupEnrollmentApprover ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollmentApprover_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupEnrollmentApprover ADD CONSTRAINT [FK_GroupEnrollmentApprover_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollmentApprover_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupEnrollmentApprover ADD CONSTRAINT [FK_GroupEnrollmentApprover_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollmentApprover_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupEnrollmentApprover ADD CONSTRAINT [FK_GroupEnrollmentApprover_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)