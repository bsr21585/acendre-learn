IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticToCourseLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticToCourseLink] (
	[idAnalyticToCourseLink]			INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL,
	[idAnalytic]						INT						NOT NULL,
	[idCourse]							INT						NOT NULL,
	[order]								INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticToCourseLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticToCourseLink ADD CONSTRAINT [PK_AnalyticToCourseLink] PRIMARY KEY CLUSTERED (idAnalyticToCourseLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticToCourseLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticToCourseLink ADD CONSTRAINT [FK_AnalyticToCourseLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticToCourseLink_Analytic]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticToCourseLink ADD CONSTRAINT FK_AnalyticToCourseLink_Analytic FOREIGN KEY (idAnalytic) REFERENCES [tblAnalytic] (idAnalytic)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticToCourseLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticToCourseLink ADD CONSTRAINT FK_AnalyticToCourseLink_Course FOREIGN KEY ([idCourse]) REFERENCES [tblCourse] ([idCourse])