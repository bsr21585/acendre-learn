IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEnrollmentRequest]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEnrollmentRequest] (
	[idEnrollmentRequest]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idCourse]							INT							NOT NULL,
	[idUser]							INT							NOT NULL,
	[timestamp]							DATETIME					NOT NULL,
	[dtApproved]						DATETIME					NULL,
	[dtDenied]							DATETIME					NULL,
	[idApprover]						INT							NULL,
	[rejectionComments]					NVARCHAR(MAX)				NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EnrollmentRequest]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [PK_EnrollmentRequest] PRIMARY KEY CLUSTERED (idEnrollmentRequest ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EnrollmentRequest_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [FK_EnrollmentRequest_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EnrollmentRequest_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [FK_EnrollmentRequest_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EnrollmentRequest_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [FK_EnrollmentRequest_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EnrollmentRequest_Approver]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [FK_EnrollmentRequest_Approver] FOREIGN KEY (idApprover) REFERENCES [tblUser] (idUser)