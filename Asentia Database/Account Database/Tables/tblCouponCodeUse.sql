/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeUse]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeUse](
	[idTransactionItem]					INT		NOT NULL,
	[idPurchaseItem]					INT		NOT NULL,
	[idCouponCode]						INT		NOT NULL,
	[confirmed]							BIT		NULL,
	[idEnrollment]						INT		NULL
) ON [PRIMARY]