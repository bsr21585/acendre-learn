IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblxAPIoAuthConsumer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [dbo].[tblxAPIoAuthConsumer] (
	[idxAPIoAuthConsumer]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[label]							NVARCHAR(200)					NULL,
	[oAuthKey]						NVARCHAR(500)					NULL,
	[oAuthSecret]					NVARCHAR(200)					NULL,
	[isActive]						BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_xAPIoAuthConsumer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblxAPIoAuthConsumer] ADD CONSTRAINT [PK_xAPIoAuthConsumer] PRIMARY KEY CLUSTERED ([idxAPIoAuthConsumer] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_xAPIoAuthConsumer_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblxAPIoAuthConsumer] ADD CONSTRAINT [FK_xAPIoAuthConsumer_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblxAPIoAuthConsumer]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblxAPIoAuthConsumer]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblxAPIoAuthConsumer (
	label
)
KEY INDEX PK_xAPIoAuthConsumer
ON Asentia -- insert index to this catalog