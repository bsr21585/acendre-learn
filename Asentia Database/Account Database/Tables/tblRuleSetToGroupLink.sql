IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetToGroupLink] (
	[idRuleSetToGroupLink]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idRuleSet]						INT							NOT NULL,
	[idGroup]						INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToGroupLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToGroupLink ADD CONSTRAINT PK_RuleSetToGroupLink PRIMARY KEY CLUSTERED (idRuleSetToGroupLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToGroupLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToGroupLink ADD CONSTRAINT [FK_RuleSetToGroupLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToGroupLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToGroupLink ADD CONSTRAINT FK_RuleSetToGroupLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToGroupLink_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToGroupLink ADD CONSTRAINT FK_RuleSetToGroupLink_Group FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)