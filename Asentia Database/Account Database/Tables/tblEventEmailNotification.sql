IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotification]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventEmailNotification] (
	[idEventEmailNotification]		INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]						INT									NOT NULL,
	[name]							NVARCHAR(255)						NOT NULL,
	[idEventType]					INT									NOT NULL,
	[idEventTypeRecipient]			INT									NOT NULL,	
	[idObject]						INT									NULL,
	[from]							NVARCHAR(255)						NULL,
	[copyTo]						NVARCHAR(255)						NULL,
	[specificEmailAddress]			NVARCHAR(255)						NULL,
	[isActive]						BIT									NULL,
	[dtActivation]					DATETIME							NULL,
	[interval]						INT									NULL,
	[timeframe]						NVARCHAR(4)							NULL,
	[priority]						INT									NULL,
	[isDeleted]						BIT									NULL,
	[isHTMLBased]					BIT									NULL,
	[attachmentType]				INT
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventEmailNotification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotification ADD CONSTRAINT [PK_EventEmailNotification] PRIMARY KEY CLUSTERED (idEventEmailNotification ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailNotification_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotification ADD CONSTRAINT [FK_EventEmailNotification_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotification]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotification]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEventEmailNotification (
	[name]
)
KEY INDEX PK_EventEmailNotification
ON Asentia -- insert index to this catalog