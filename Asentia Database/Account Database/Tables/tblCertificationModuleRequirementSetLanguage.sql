IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSetLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirementSetLanguage] (
	[idCertificationModuleRequirementSetLanguage]	INT					IDENTITY(1,1)	NOT NULL,
	[idSite]										INT									NOT NULL, 
	[idCertificationModuleRequirementSet]			INT									NOT NULL,	
	[idLanguage]									INT									NOT NULL,
	[label]											NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirementSetLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSetLanguage ADD CONSTRAINT [PK_CertificationModuleRequirementSetLanguage] PRIMARY KEY CLUSTERED (idCertificationModuleRequirementSetLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSetLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSetLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementSetLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSetLanguage_RequirementSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSetLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementSetLanguage_RequirementSet] FOREIGN KEY (idCertificationModuleRequirementSet) REFERENCES [tblCertificationModuleRequirementSet] (idCertificationModuleRequirementSet)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSetLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSetLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementSetLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSetLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSetLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleRequirementSetLanguage (
	label
)
KEY INDEX PK_CertificationModuleRequirementSetLanguage
ON Asentia -- insert index to this catalog