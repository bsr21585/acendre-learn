IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblInboxMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblInboxMessage] (
	[idInboxMessage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NULL,
	[idParentInboxMessage]			INT								NULL,
	[idRecipient]					INT								NOT NULL,
	[idSender]						INT								NOT NULL,
	[subject]						NVARCHAR(255)					NOT NULL,
	[message]						NVARCHAR(MAX)					NOT NULL,
	[isDraft]						BIT								NULL,
	[isSent]						BIT								NULL,
	[dtSent]						DATETIME						NULL,
	[isRead]						BIT								NULL,
	[dtRead]						DATETIME						NULL,
	[isRecipientDeleted]			BIT								NULL,
	[dtRecipientDeleted]			DATETIME						NULL,
	[isSenderDeleted]				BIT								NULL,
	[dtSenderDeleted]				DATETIME						NULL,
	[dtCreated]						DATETIME						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_InboxMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblInboxMessage] ADD CONSTRAINT [PK_InboxMessage] PRIMARY KEY CLUSTERED([idInboxMessage] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_InboxMessage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblInboxMessage ADD CONSTRAINT [FK_InboxMessage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_InboxMessage_ParentInboxMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblInboxMessage ADD CONSTRAINT [FK_InboxMessage_ParentInboxMessage] FOREIGN KEY (idParentInboxMessage) REFERENCES tblInboxMessage (idInboxMessage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_InboxMessage_Recipient]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblInboxMessage ADD CONSTRAINT [FK_InboxMessage_Recipient] FOREIGN KEY(idRecipient) REFERENCES tblUser (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_InboxMessage_Sender]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblInboxMessage ADD CONSTRAINT [FK_InboxMessage_Sender] FOREIGN KEY(idSender) REFERENCES tblUser (idUser)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblInboxMessage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblInboxMessage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblInboxMessage (
	[subject]
)
KEY INDEX PK_InboxMessage
ON Asentia -- insert index to this catalog