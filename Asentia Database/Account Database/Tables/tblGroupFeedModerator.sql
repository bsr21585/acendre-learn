IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupFeedModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupFeedModerator] (
	[idGroupFeedModerator]				INT			IDENTITY (1, 1)		NOT NULL, 
	[idSite]							INT								NOT NULL,
	[idGroup]					      	INT								NOT NULL,
	[idModerator]						INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupFeedModerator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedModerator] ADD CONSTRAINT [PK_GroupFeedModerator] PRIMARY KEY CLUSTERED (idGroupFeedModerator ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedModerator_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedModerator] ADD CONSTRAINT [FK_GroupFeedModerator_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedModerator_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedModerator] ADD CONSTRAINT [FK_GroupFeedModerator_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedModerator_Moderator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedModerator] ADD CONSTRAINT [FK_GroupFeedModerator_Moderator] FOREIGN KEY (idModerator) REFERENCES [tblUser] (idUser)