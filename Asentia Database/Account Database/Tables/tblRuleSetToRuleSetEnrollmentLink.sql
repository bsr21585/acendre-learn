IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetToRuleSetEnrollmentLink] (
	[idRuleSetToRuleSetEnrollmentLink]		INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idRuleSet]								INT							NOT NULL,
	[idRuleSetEnrollment]					INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetEnrollmentLink ADD CONSTRAINT [PK_RuleSetToRuleSetEnrollmentLink] PRIMARY KEY CLUSTERED (idRuleSetToRuleSetEnrollmentLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetEnrollmentLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetEnrollmentLink ADD CONSTRAINT [FK_RuleSetToRuleSetEnrollmentLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetEnrollmentLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetEnrollmentLink ADD CONSTRAINT FK_RuleSetToRuleSetEnrollmentLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetEnrollmentLink_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetEnrollmentLink ADD CONSTRAINT FK_RuleSetToRuleSetEnrollmentLink_RuleSetEnrollment FOREIGN KEY ([idRuleSetEnrollment]) REFERENCES [tblRuleSetEnrollment] ([idRuleSetEnrollment])