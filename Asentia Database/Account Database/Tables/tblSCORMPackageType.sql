IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblSCORMPackageType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSCORMPackageType] (
	[idSCORMPackageType]		INT				IDENTITY(1, 1)		NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]
GO

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SCORMPackageType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblSCORMPackageType] ADD CONSTRAINT [PK_SCORMPackageType] PRIMARY KEY CLUSTERED ([idSCORMPackageType] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblSCORMPackageType] ON

INSERT INTO [tblSCORMPackageType] (
	idSCORMPackageType, 
	name
)
SELECT
	idSCORMPackageType, 
	name
FROM (
	SELECT		 1 AS idSCORMPackageType, 'Content' AS name
	UNION SELECT 2 AS idSCORMPackageType, 'Resource' AS name

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblSCORMPackageType SPT WHERE SPT.idSCORMPackageType = MAIN.idSCORMPackageType)

SET IDENTITY_INSERT [tblSCORMPackageType] OFF