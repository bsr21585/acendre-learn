IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSystem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSystem] (
	[lastEventEmailQueueCascade]	DATETIME	NULL
) ON [PRIMARY]