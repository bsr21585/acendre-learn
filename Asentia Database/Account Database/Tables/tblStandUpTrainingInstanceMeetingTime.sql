IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstanceMeetingTime]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTrainingInstanceMeetingTime] (
	[idStandUpTrainingInstanceMeetingTime]		INT			IDENTITY(1,1)	NOT NULL,
	[idStandUpTrainingInstance]					INT							NOT NULL,
	[idSite]									INT							NOT NULL,
	[dtStart]									DATETIME					NOT NULL,
	[dtEnd]										DATETIME					NOT NULL,
	[minutes]									INT							NOT NULL,
	[idTimezone]								INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTrainingInstanceMeetingTime]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceMeetingTime ADD CONSTRAINT [PK_StandUpTrainingInstanceMeetingTime] PRIMARY KEY CLUSTERED (idStandUpTrainingInstanceMeetingTime ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceMeetingTime_StandUpTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceMeetingTime ADD CONSTRAINT [FK_StandUpTrainingInstanceMeetingTime_StandupTrainingInstance] FOREIGN KEY (idStandUpTrainingInstance) REFERENCES [tblStandUpTrainingInstance] (idStandUpTrainingInstance)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceMeetingTime_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceMeetingTime ADD CONSTRAINT [FK_StandUpTrainingInstanceMeetingTime_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)