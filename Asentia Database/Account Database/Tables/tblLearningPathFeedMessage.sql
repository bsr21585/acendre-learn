/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathFeedMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathFeedMessage] (
	[idLearningPathFeedMessage]				INT				IDENTITY (1, 1)		NOT NULL,
	[idSite]								INT									NOT NULL,
	[idLearningPathFeed]					INT									NOT NULL,
	[idLanguage]							INT									NULL,
	[idAuthor]								INT									NOT NULL,
	[idParentLearningPathFeedMessage]		INT									NULL,
	[message]								NVARCHAR(MAX)						NOT NULL,
	[timestamp]								DATETIME							NOT NULL,
	[dtApproved]							DATETIME							NOT NULL,
	[idApprover]							INT									NULL,
	[isApproved]							BIT 						        NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [PK_LearningPathFeedMessage] PRIMARY KEY CLUSTERED (idLearningPathFeedMessage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_LearningPathFeed]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_LearningPathFeed] FOREIGN KEY (idLearningPathFeed) REFERENCES [tblLearningPathFeed] (idLearningPathFeed)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_ParentLearningPathFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_ParentLearningPathFeedMessage] FOREIGN KEY (idParentLearningPathFeedMessage) REFERENCES [tblLearningPathFeedMessage] (idLearningPathFeedMessage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_Author]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_Author] FOREIGN KEY (idAuthor) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_Approver]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_Approver] FOREIGN KEY (idApprover) REFERENCES [tblUser] (idUser)