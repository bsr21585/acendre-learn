IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSSOToken]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSSOToken] (
	[idSSOToken]	INT			IDENTITY(1,1)	NOT NULL,
	[idSite]		INT							NOT NULL,
	[idUser]		INT							NOT NULL,
	[token]			NVARCHAR(40)				NOT NULL,
	[dtExpires]		DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SSOToken]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSSOToken ADD CONSTRAINT [PK_SSOToken] PRIMARY KEY CLUSTERED (idSSOToken ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SSOToken_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSSOToken ADD CONSTRAINT [FK_SSOToken_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SSOToken_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSSOToken ADD CONSTRAINT [FK_SSOToken_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)