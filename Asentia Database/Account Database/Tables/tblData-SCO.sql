IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-SCO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-SCO] (
	[idData-SCO]						INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idData-Lesson]						INT								NOT NULL, 
	[idTimezone]						INT								NOT NULL,
	[manifestIdentifier]				NVARCHAR(255)					NULL,
	[completionStatus]					INT								NOT NULL,
	[successStatus]						INT								NOT NULL,
	[scoreScaled]						FLOAT							NULL, 
	[totalTime]							FLOAT							NULL, 
	[timestamp]							DATETIME						NULL,
	[actualAttemptCount]				INT								NULL,
	[effectiveAttemptCount]				INT								NULL,
	[proctoringUser]					INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-SCO]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [PK_Data-SCO] PRIMARY KEY CLUSTERED ([idData-SCO] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_Data-Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_Data-Lesson] FOREIGN KEY ([idData-Lesson]) REFERENCES [tblData-Lesson] ([idData-Lesson])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_Timezone] FOREIGN KEY ([idTimezone]) REFERENCES [tblTimezone] ([idTimezone])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_CompletionStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_CompletionStatus] FOREIGN KEY ([completionStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_SuccessStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_SuccessStatus] FOREIGN KEY ([successStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_ProctoringUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_ProctoringUser] FOREIGN KEY (proctoringUser) REFERENCES [tblUser] (idUser)