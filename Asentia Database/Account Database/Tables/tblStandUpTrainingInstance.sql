IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstance]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTrainingInstance] (
	[idStandUpTrainingInstance]			INT				IDENTITY(1,1)	NOT NULL,
	[idStandUpTraining]					INT								NOT NULL,
	[idSite]							INT								NOT NULL,
	[title]								NVARCHAR(255)					NOT NULL,
	[description]						NVARCHAR(MAX)					NULL,
	[seats]								INT								NOT NULL,
	[waitingSeats]						INT								NOT NULL,
	[type]								INT								NOT NULL,
	[isClosed]							BIT								NULL,
	[urlRegistration]					NVARCHAR(255)					NULL,
	[urlAttend]							NVARCHAR(255)					NULL,
	[city]								NVARCHAR(255)					NULL,
	[province]							NVARCHAR(255)					NULL,
	[postalcode]						NVARCHAR(25)					NULL,
	[locationDescription]				NVARCHAR(MAX)					NULL,
	[idInstructor]						INT								NULL,
	[isDeleted]							BIT								NULL,
	[dtDeleted]							DATETIME						NULL,
	[integratedObjectKey]				BIGINT							NULL,
	[hostUrl]							NVARCHAR(1024)					NULL,
	[genericJoinUrl]					NVARCHAR(1024)					NULL,
	[meetingPassword]					NVARCHAR(255)					NULL,
	[idWebMeetingOrganizer]				INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTrainingInstance] ADD CONSTRAINT [PK_StandUpTrainingInstance] PRIMARY KEY CLUSTERED ([idStandUpTrainingInstance] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstance_StandUpTraining]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTrainingInstance] ADD CONSTRAINT [FK_StandUpTrainingInstance_StandUpTraining] FOREIGN KEY (idStandUpTraining) REFERENCES [tblStandUpTraining] (idStandUpTraining)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstance_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTrainingInstance] ADD CONSTRAINT [FK_StandUpTrainingInstance_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstance_Instructor]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTrainingInstance] ADD CONSTRAINT [FK_StandUpTrainingInstance_Instructor] FOREIGN KEY (idInstructor) REFERENCES [tblUser] (idUser)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstance]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstance]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblStandUpTrainingInstance (
	title,
	[description],
	city,
	province,
	locationDescription
)
KEY INDEX PK_StandUpTrainingInstance
ON Asentia -- insert index to this catalog