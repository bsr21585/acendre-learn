IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventEmailQueue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventEmailQueue] (
	[idEventEmailQueue]							INT				IDENTITY(1,1)	NOT NULL,
	[idSite]									INT								NOT NULL,
	[idEventLog]								INT								NOT NULL,
	[idEventType]								INT								NOT NULL,
	[idEventEmailNotification]					INT								NOT NULL,
	[idEventTypeRecipient]						INT								NOT NULL,
	[idObject]									INT								NOT NULL, 
	[idObjectRelated]							INT								NOT NULL, 
	[objectType]								NVARCHAR(25)					NULL, 
	[idObjectUser]								INT								NULL,
	[objectUserFullName]						NVARCHAR(512)					NULL,
	[objectUserFirstName]						NVARCHAR(255)					NULL,
	[objectUserLogin]							NVARCHAR(512)					NULL,
	[objectUserEmail]							NVARCHAR(255)					NULL,
	[idRecipient]								INT								NOT NULL,
	[recipientLangString]						NVARCHAR(10)					NOT NULL,
	[recipientFullName]							NVARCHAR(512)					NOT NULL,
	[recipientFirstName]						NVARCHAR(255)					NOT NULL,
	[recipientLogin]							NVARCHAR(512)					NOT NULL,
	[recipientEmail]							NVARCHAR(255)					NOT NULL,
	[recipientTimezone]							INT								NOT NULL, 
	[recipientTimezoneDotNetName]				NVARCHAR(255)					NOT NULL,
	[from]										NVARCHAR(255)					NULL,
	[copyTo]									NVARCHAR(255)					NULL,
	[priority]									INT								NULL,
	[isHTMLBased]								BIT								NULL,
	[dtEvent]									DATETIME						NOT NULL,	-- the date the event occurred
	[dtAction]									DATETIME						NOT NULL,	-- the date the email should be sent
	[dtActivation]								DATETIME						NOT NULL,   -- the date of the email notification's activation
	[dtSent]									DATETIME						NULL,		-- the record of the date the email ACTUALLY sent
	[statusDescription]							NVARCHAR(512)					NULL,
	[attachmentType]							INT
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventEmailQueue]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailQueue ADD CONSTRAINT [PK_EventEmailQueue] PRIMARY KEY CLUSTERED (idEventEmailQueue ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailQueue_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailQueue ADD CONSTRAINT [FK_EventEmailQueue_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailQueue]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailQueue]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEventEmailQueue (
	recipientFullName
)
KEY INDEX PK_EventEmailQueue
ON Asentia -- insert index to this catalog