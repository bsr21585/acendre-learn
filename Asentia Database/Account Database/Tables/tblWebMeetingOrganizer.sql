IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblWebMeetingOrganizer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblWebMeetingOrganizer] (
	[idWebMeetingOrganizer]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idUser]						INT							NULL,	  -- The user these credentials belong to, null means unlinked.
	[organizerType]					INT							NOT NULL, -- GoToMeeting (2), GoToWebinar (3), GoToTraining (4), WebEx (5) 
																		  -- These are regulated to their "MeetingType" enum int values. 
	[username]						NVARCHAR(255)				NOT NULL,
	[password]						NVARCHAR(255)				NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_WebMeetingOrganizer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblWebMeetingOrganizer ADD CONSTRAINT [PK_WebMeetingOrganizer] PRIMARY KEY CLUSTERED (idWebMeetingOrganizer ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_WebMeetingOrganizer_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblWebmeetingOrganizer ADD CONSTRAINT [FK_WebMeetingOrganizer_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_WebMeetingOrganizer_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblWebMeetingOrganizer ADD CONSTRAINT [FK_WebMeetingOrganizer_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)