IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationLanguage] (
	[idCertificationLanguage]			INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idCertification]					INT									NOT NULL,
	[idLanguage]						INT									NOT NULL,
	[title]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL,
	[searchTags]						NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationLanguage ADD CONSTRAINT [PK_CertificationLanguage] PRIMARY KEY CLUSTERED (idCertificationLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationLanguage ADD CONSTRAINT [FK_CertificationLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationLanguage_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationLanguage ADD CONSTRAINT [FK_CertificationLanguage_Certification] FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationLanguage ADD CONSTRAINT [FK_CertificationLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationLanguage (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_CertificationLanguage
ON Asentia -- insert index to this catalog