IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-Lesson]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-Lesson] (
	[idData-Lesson]									INT				IDENTITY(1,1)	NOT NULL,
	[idSite]										INT								NOT NULL, 
	[idEnrollment]									INT								NOT NULL, 
	[idLesson]										INT								NULL,
	[idTimezone]									INT								NOT NULL,
	[title]											NVARCHAR(255)					NOT NULL,
	[revcode]										NVARCHAR(32)					NULL,
	[order]											INT								NOT NULL,
	[contentTypeCommittedTo]						INT								NULL,
	[dtCommittedToContentType]						DATETIME						NULL,
	[dtCompleted]									DATETIME						NULL,
	[resetForContentChange]							BIT								NULL,		-- flag to reset lesson data when lesson content is changed
	[preventPostCompletionLaunchForContentChange]	BIT								NULL		-- flag to prevent launch of content when content has been changed after lesson completion
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-Lesson] ADD CONSTRAINT [PK_Data-Lesson] PRIMARY KEY CLUSTERED ([idData-Lesson] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-Lesson_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-Lesson] ADD CONSTRAINT [FK_Data-Lesson_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-Lesson_Enrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-Lesson] ADD CONSTRAINT [FK_Data-Lesson_Enrollment] FOREIGN KEY ([idEnrollment]) REFERENCES [tblEnrollment] ([idEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-Lesson_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-Lesson] ADD CONSTRAINT [FK_Data-Lesson_Timezone] FOREIGN KEY ([idTimezone]) REFERENCES [tblTimezone] ([idTimezone])

-- DO NOT FK to idLesson (lessons can be deleted while leaving the data record).

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblData-Lesson]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblData-Lesson]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON [tblData-Lesson] (
	title
)
KEY INDEX [PK_Data-Lesson]
ON Asentia -- insert index to this catalog