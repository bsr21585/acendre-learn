IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstanceLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTrainingInstanceLanguage] (
	[idStandUpTrainingInstanceLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idStandUpTrainingInstance]					INT								NOT NULL,
	[idSite]									INT								NOT NULL,
	[idLanguage]								INT								NOT NULL,
	[title]										NVARCHAR(255)					NOT NULL,
	[description]								NVARCHAR(MAX)					NULL,
	[locationDescription]						NVARCHAR(MAX)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTrainingInstanceLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceLanguage ADD CONSTRAINT [PK_StandUpTrainingInstanceLanguage] PRIMARY KEY CLUSTERED (idStandUpTrainingInstanceLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceLanguage_StandUpTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceLanguage ADD CONSTRAINT [FK_StandUpTrainingInstanceLanguage_StandupTrainingInstance] FOREIGN KEY (idStandUpTrainingInstance) REFERENCES [tblStandUpTrainingInstance] (idStandUpTrainingInstance)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceLanguage ADD CONSTRAINT [FK_StandUpTrainingInstanceLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceLanguage ADD CONSTRAINT [FK_StandUpTrainingInstanceLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstanceLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstanceLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblStandUpTrainingInstanceLanguage (
	title,
	[description],
	locationDescription
)
KEY INDEX PK_StandUpTrainingInstanceLanguage
ON Asentia -- insert index to this catalog