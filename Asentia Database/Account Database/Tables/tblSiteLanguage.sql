IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteLanguage] (
	[idSiteLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idLanguage]					INT								NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL,
	[company]						NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteLanguage ADD CONSTRAINT [PK_SiteLanguage] PRIMARY KEY CLUSTERED (idSiteLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteLanguage ADD CONSTRAINT [FK_SiteLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteLanguage ADD CONSTRAINT [FK_SiteLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)