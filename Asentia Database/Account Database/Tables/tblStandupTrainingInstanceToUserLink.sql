IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandupTrainingInstanceToUserLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandupTrainingInstanceToUserLink] (
	[idStandupTrainingInstanceToUserLink]			INT		 IDENTITY(1,1)	NOT NULL,
	[idUser]										INT						NOT NULL,
	[idStandupTrainingInstance]						INT						NOT NULL, 
	[completionStatus]								INT						NULL,
	[successStatus]									INT						NULL,
	[score]											INT						NULL,
	[isWaitingList]									BIT						NOT NULL,
	[dtCompleted]									DATETIME				NULL,
	[registrantKey]									BIGINT					NULL,
	[registrantEmail]								NVARCHAR(255)			NULL,
	[joinUrl]										NVARCHAR(1024)			NULL,
	[waitlistOrder]									INT						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandupTrainingInstanceToUserLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToUserLink ADD CONSTRAINT [PK_StandupTrainingInstanceToUserLink] PRIMARY KEY CLUSTERED (idStandupTrainingInstanceToUserLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToUserLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToUserLink ADD CONSTRAINT [FK_StandupTrainingInstanceToUserLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToUserLink_StandupTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToUserLink ADD CONSTRAINT [FK_StandupTrainingInstanceToUserLink_StandupTrainingInstance] FOREIGN KEY (idStandupTrainingInstance) REFERENCES [tblStandUpTrainingInstance] (idStandupTrainingInstance)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToUserLink_CompletionStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandupTrainingInstanceToUserLink] ADD CONSTRAINT [FK_StandupTrainingInstanceToUserLink_CompletionStatus] FOREIGN KEY ([completionStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToUserLink_SuccessStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandupTrainingInstanceToUserLink] ADD CONSTRAINT [FK_StandupTrainingInstanceToUserLink_SuccessStatus] FOREIGN KEY ([successStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])
