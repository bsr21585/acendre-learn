IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportProcessorLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportProcessorLog] (
	[idReportProcessorLog]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idCaller]							INT								NOT NULL,
	[datasetProcedure]					NVARCHAR(50)					NOT NULL,
	[dtExecutionBegin]					DATETIME						NOT NULL,
	[dtExecutionEnd]					DATETIME						NOT NULL,
	[executionDurationSeconds]			FLOAT							NOT NULL,
	[sqlQuery]							NVARCHAR(MAX)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportProcessorLog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportProcessorLog ADD CONSTRAINT [PK_ReportProcessorLog] PRIMARY KEY CLUSTERED (idReportProcessorLog ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportProcessorLog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportProcessorLog ADD CONSTRAINT [FK_ReportProcessorLog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)