IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDataset]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDataset] (
	[idDataset]							INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[name]								NVARCHAR(255)						NOT NULL,
	[storedProcedureName]				NVARCHAR(50)						NOT NULL,
	[htmlTemplatePath]					NVARCHAR(255)						NULL,
	[description]						NVARCHAR(512)						NULL,
	[order]								INT									
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Dataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDataset ADD CONSTRAINT [PK_Dataset] PRIMARY KEY CLUSTERED (idDataset ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Dataset_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDataset ADD CONSTRAINT [FK_Dataset_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)


/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblDataset] ON

INSERT INTO tblDataset (
	idDataset,
	idSite,
	name,
	storedProcedureName,
	[description],
	[order]
)
SELECT
	idDataset,
	idSite,
	name,
	storedProcedureName,
	[description],
	[order]
FROM (
	SELECT 1 AS idDataset, 1 AS idSite, 'User Demographics' AS name, '[Dataset.UserDemographics]' AS storedProcedureName, 'Complete user account information for all users including name, username, email, contact information, organizational information and more.' AS [description], 1 AS [order]
	UNION SELECT 2 AS idDataset, 1 AS idSite, 'User Course Transcripts' AS name, '[Dataset.UserCourseTranscripts]' AS storedProcedureName, 'Course and lesson status, and completion information including scores and dates.' AS [description], 2 AS [order]
	UNION SELECT 3 AS idDataset, 1 AS idSite, 'Catalog and Course Information' AS name, '[Dataset.CatalogCourseInformation]' AS storedProcedureName, 'Information for catalogs, courses, lessons, and ILT sessions.' AS [description], 6 AS [order]
	UNION SELECT 4 AS idDataset, 1 AS idSite, 'Certificates' AS name, '[Dataset.Certificates]' AS storedProcedureName, 'Certificate award information. Includes certificates that have been earned as well as those that have been manually awarded.' AS [description], 7 AS [order]
	UNION SELECT 5 AS idDataset, 1 AS idSite, 'xAPI' AS name, '[Dataset.xAPI]' AS storedProcedureName, 'xAPI statments recorded to the system''s learning record store from outside training.' AS [description], 8 AS [order]
	UNION SELECT 6 AS idDataset, 1 AS idSite, 'User Learning Path Transcripts' AS name, '[Dataset.UserLearningPathTranscripts]' AS storedProcedureName, 'Learning Path status and completion information.' AS [description], 3 AS [order]
	UNION SELECT 7 AS idDataset, 1 AS idSite, 'User ILT Transcripts' AS name, '[Dataset.UserInstructorLedTranscripts]' AS storedProcedureName, 'Instructor Led Training status and completion information including scores and dates.' AS [description], 4 AS [order]
	UNION SELECT 8 AS idDataset, 1 AS idSite, 'Purchases' AS name, '[Dataset.Purchases]' AS storedProcedureName, 'Information on purchases made by users within the system.' AS [description], 9 AS [order]
	UNION SELECT 9 AS idDataset, 1 AS idSite, 'User Certification Transcripts' AS name, '[Dataset.UserCertificationTranscripts]' AS storedProcedureName, 'Certification status information.' AS [description], 5 AS [order]
	UNION SELECT 10 AS idDataset, 1 AS idSite, 'All User Logins' AS name, '[Dataset.AllUserLogins]' AS storedProcedureName, 'Complete listing of user account information and login timestamps.' AS [description], 10 AS [order]
	UNION SELECT 11 AS idDataset, 1 AS idSite, 'Unique User Logins' AS name, '[Dataset.UniqueUserLogins]' AS storedProcedureName, 'Unique user login counts by month and year.' AS [description], 11 AS [order]
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblDataset D WHERE D.idDataset = MAIN.idDataset)

SET IDENTITY_INSERT [tblDataset] OFF