IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupToRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupToRoleLink] (
	[idGroupToRoleLink]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL, 
	[idGroup]						INT							NOT NULL,
	[idRole]						INT							NOT NULL,
	[idRuleSet]						INT							NULL,
	[created]						DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupToRoleLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupToRoleLink ADD CONSTRAINT [PK_GroupToRoleLink] PRIMARY KEY CLUSTERED (idGroupToRoleLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupToRoleLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupToRoleLink ADD CONSTRAINT [FK_GroupToRoleLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupToRoleLink_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupToRoleLink ADD CONSTRAINT [FK_GroupToRoleLink_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupToRoleLink_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupToRoleLink ADD CONSTRAINT [FK_GroupToRoleLink_Role] FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)