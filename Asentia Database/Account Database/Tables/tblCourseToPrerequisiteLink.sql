IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseToPrerequisiteLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseToPrerequisiteLink] (
	[idCourseToPrerequisiteLink]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL,
	[idCourse]							INT						NOT NULL, 
	[idPrerequisite]					INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseToPrerequisiteLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToPrerequisiteLink ADD CONSTRAINT [PK_CourseToPrerequisiteLink] PRIMARY KEY CLUSTERED (idCourseToPrerequisiteLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToPrerequisiteLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToPrerequisiteLink ADD CONSTRAINT [FK_CourseToPrerequisiteLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToPrerequisiteLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToPrerequisiteLink ADD CONSTRAINT [FK_CourseToPrerequisiteLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToPrerequisiteLink_Prerequisite]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToPrerequisiteLink ADD CONSTRAINT [FK_CourseToPrerequisiteLink_Prerequisite] FOREIGN KEY (idPrerequisite) REFERENCES [tblCourse] (idCourse)