IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationToCourseCreditLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationToCourseCreditLink] (
	[idCertificationToCourseCreditLink]	INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idCertification]					INT									NOT NULL,
	[idCourse]							INT									NOT NULL,
	[credits]							FLOAT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationToCourseCreditLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToCourseCreditLink ADD CONSTRAINT [PK_CertificationToCourseCreditLink] PRIMARY KEY CLUSTERED (idCertificationToCourseCreditLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToCourseCreditLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToCourseCreditLink ADD CONSTRAINT [FK_CertificationToCourseCreditLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToCourseCreditLink_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToCourseCreditLink ADD CONSTRAINT [FK_CertificationToCourseCreditLink_Certification] FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToCourseCreditLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToCourseCreditLink ADD CONSTRAINT [FK_CertificationToCourseCreditLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)