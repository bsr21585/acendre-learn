IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetEngineQueue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetEngineQueue] (
	[idRuleSetEngineQueue]		INT			IDENTITY(1,1)	NOT NULL,
	[idSite]					INT							NOT NULL,
	[triggerObject]				NVARCHAR(50)				NOT NULL,
	[triggerObjectId]			INT							NOT NULL,
	[eventAction]				NVARCHAR(25)				NOT NULL,
	[eventTable]				NVARCHAR(50)				NOT NULL,
	[dtQueued]					DATETIME					NOT NULL,
	[isProcessing]				BIT							NULL,
	[dtProcessed]				DATETIME					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetEngineQueue]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetEngineQueue ADD CONSTRAINT [PK_RuleSetEngineQueue] PRIMARY KEY CLUSTERED (idRuleSetEngineQueue ASC)