IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-SCOObj]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-SCOObj] (
	[idData-SCOObj]						INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idData-SCO]						INT								NOT NULL, -- we need this so that we can clear and reset upon subsequent attempts
	[identifier]						NVARCHAR(255)					NOT NULL,
	[scoreScaled]						FLOAT							NULL, 
	[completionStatus]					INT								NOT NULL, 
	[successStatus]						INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-SCOObj]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [PK_Data-SCOObj] PRIMARY KEY CLUSTERED ([idData-SCOObj] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOObj_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [FK_Data-SCOObj_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOObj_Data-SCO]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [FK_Data-SCOObj_Data-SCO] FOREIGN KEY ([idData-SCO]) REFERENCES [tblData-SCO] ([idData-SCO])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOObj_CompletionStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [FK_Data-SCOObj_CompletionStatus] FOREIGN KEY ([completionStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOObj_SuccessStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [FK_Data-SCOObj_SuccessStatus] FOREIGN KEY ([successStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])