IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEnrollment] (
	[idEnrollment]							INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL, 
	[idCourse]								INT								NULL, 
	[idUser]								INT								NOT NULL, 
	[idGroupEnrollment]						INT								NULL,		-- indicates the enrollment is inherited from this group enrollment
	[idRuleSetEnrollment]					INT								NULL,		-- indicates the enrollment is inherited from a ruleset enrollment
	[idLearningPathEnrollment]				INT								NULL,		-- indicates the enrollment is directly inherited by a learning path enrollment
																						-- not all enrollments that go for credit toward a learning path enrollment will
																						-- have this column populated, enrollments a learner has prior to enrolling in a
																						-- learning path can still count toward learning path completion
	[idTimezone]							INT								NOT NULL,
	[isLockedByPrerequisites]				BIT								NOT NULL,
	[code]									NVARCHAR(255)					NULL,
	[revcode]								NVARCHAR(32)					NULL,
	[title]									NVARCHAR(255)					NOT NULL,
	[dtStart]								DATETIME						NOT NULL,	-- accounts for inherited delay (after dtCreated). non-inherited records should have identical dtCreated and dtStart.
	[dtDue]									DATETIME						NULL,
	[dtExpiresFromStart]					DATETIME						NULL,
	[dtExpiresFromFirstLaunch]				DATETIME						NULL,
	[dtFirstLaunch]							DATETIME						NULL,
	[dtCompleted]							DATETIME						NULL,
	[dtCreated]								DATETIME						NOT NULL, 
	[dtLastSynchronized]					DATETIME						NULL,
	[dueInterval]							INT								NULL,
	[dueTimeframe]							NVARCHAR(4)						NULL,
	[expiresFromStartInterval]				INT								NULL,
	[expiresFromStartTimeframe]				NVARCHAR(4)						NULL,
	[expiresFromFirstLaunchInterval]		INT								NULL,
	[expiresFromFirstLaunchTimeframe]		NVARCHAR(4)						NULL,
	[idActivityImport]						INT								NULL,
	[credits]								FLOAT							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Enrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [PK_Enrollment] PRIMARY KEY CLUSTERED ([idEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_GroupEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_GroupEnrollment] FOREIGN KEY ([idGroupEnrollment]) REFERENCES [tblGroupEnrollment] ([idGroupEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_RuleSetEnrollment] FOREIGN KEY ([idRuleSetEnrollment]) REFERENCES [tblRuleSetEnrollment] ([idRuleSetEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_LearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_LearningPathEnrollment] FOREIGN KEY ([idLearningPathEnrollment]) REFERENCES [tblLearningPathEnrollment] ([idLearningPathEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_ActivityImport]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_ActivityImport] FOREIGN KEY (idActivityImport) REFERENCES [tblActivityImport] (idActivityImport)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEnrollment]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEnrollment]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEnrollment (
	code,
	title
)
KEY INDEX PK_Enrollment
ON Asentia -- insert index to this catalog