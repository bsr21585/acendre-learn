IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseFeedMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseFeedMessage] (
	[idCourseFeedMessage]				INT				IDENTITY (1, 1)		NOT NULL, 
	[idSite]							INT									NOT NULL,
	[idCourse]						    INT									NOT NULL,
	[idLanguage]						INT									NULL,
	[idAuthor]							INT									NOT NULL,
	[idParentCourseFeedMessage]			INT									NULL,
	[message]							NVARCHAR(MAX)						NOT NULL,
	[timestamp]							DATETIME							NOT NULL,
	[dtApproved]						DATETIME							NULL,
	[idApprover]						INT                                 NULL,
	[isApproved]						BIT 						        NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [PK_CourseFeedMessage] PRIMARY KEY CLUSTERED (idCourseFeedMessage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_ParentCourseFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_ParentCourseFeedMessage] FOREIGN KEY (idParentCourseFeedMessage) REFERENCES [tblCourseFeedMessage] (idCourseFeedMessage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Author]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Author] FOREIGN KEY (idAuthor) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Approver]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Approver] FOREIGN KEY (idApprover) REFERENCES [tblUser] (idUser)