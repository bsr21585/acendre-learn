IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-HomeworkAssignment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-HomeworkAssignment] (
	[idData-HomeworkAssignment]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idData-Lesson]						INT								NOT NULL, 
	[uploadedAssignmentFilename]		NVARCHAR(255)					NOT NULL,
	[dtUploaded]						DATETIME						NOT NULL,
	[completionStatus]					INT								NULL,
	[successStatus]						INT								NULL,
	[score]								INT								NULL,
	[timestamp]							DATETIME						NULL,
	[proctoringUser]					INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-HomeworkAssignment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [PK_Data-HomeworkAssignment] PRIMARY KEY CLUSTERED ([idData-HomeworkAssignment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_Data-Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_Data-Lesson] FOREIGN KEY ([idData-Lesson]) REFERENCES [tblData-Lesson] ([idData-Lesson])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_CompletionStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_CompletionStatus] FOREIGN KEY ([completionStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_SuccessStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_SuccessStatus] FOREIGN KEY ([successStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_ProctoringUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_ProctoringUser] FOREIGN KEY (proctoringUser) REFERENCES [tblUser] (idUser)