IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblExceptionLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblExceptionLog] (
	[idExceptionLog]				INT				IDENTITY(1000, 1)	NOT NULL,
	[idSite]						INT									NOT NULL,
	[idUser]						INT									NULL,
	[timestamp]						DATETIME							NOT NULL,
	[page]							NVARCHAR(512)						NOT NULL,
	[exceptionType]					NVARCHAR(512)						NOT NULL,
	[exceptionMessage]				NVARCHAR(MAX)						NOT NULL,
	[exceptionStackTrace]			NVARCHAR(MAX)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ExceptionLog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblExceptionLog ADD CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED (idExceptionLog ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ExceptionLog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblExceptionLog ADD CONSTRAINT [FK_ExceptionLog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblExceptionLog]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblExceptionLog]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblExceptionLog ( 
	[page],
	exceptionType,
	exceptionMessage
)
KEY INDEX PK_ExceptionLog
ON Asentia -- insert index to this catalog