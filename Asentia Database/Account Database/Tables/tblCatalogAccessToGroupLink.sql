IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCatalogAccessToGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCatalogAccessToGroupLink] (
	[idCatalogAccessToGroupLink]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idGroup]								INT							NOT NULL,
	[idCatalog]								INT							NOT NULL, 	
	[created]								DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CatalogAccessToGroupLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogAccessToGroupLink ADD CONSTRAINT [PK_CatalogAccessToGroupLink] PRIMARY KEY CLUSTERED (idCatalogAccessToGroupLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogAccessToGroupLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogAccessToGroupLink ADD CONSTRAINT [FK_CatalogAccessToGroupLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogAccessToGroupLink_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogAccessToGroupLink ADD CONSTRAINT [FK_CatalogAccessToGroupLink_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)