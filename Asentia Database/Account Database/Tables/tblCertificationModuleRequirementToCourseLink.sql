IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementToCourseLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirementToCourseLink] (
	[idCertificationModuleRequirementToCourseLink]	INT		IDENTITY(1,1)	NOT NULL,
	[idSite]										INT						NOT NULL,
	[idCertificationModuleRequirement]				INT						NOT NULL,
	[idCourse]										INT						NOT NULL,
	[order]											INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirementToCourseLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementToCourseLink ADD CONSTRAINT [PK_CertificationModuleRequirementToCourseLink] PRIMARY KEY CLUSTERED (idCertificationModuleRequirementToCourseLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementToCourseLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementToCourseLink ADD CONSTRAINT [FK_CertificationModuleRequirementToCourseLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementToCourseLink_CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementToCourseLink ADD CONSTRAINT FK_CertificationModuleRequirementToCourseLink_CertificationModuleRequirement FOREIGN KEY (idCertificationModuleRequirement) REFERENCES [tblCertificationModuleRequirement] (idCertificationModuleRequirement)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementToCourseLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementToCourseLink ADD CONSTRAINT FK_CertificationModuleRequirementToCourseLink_Course FOREIGN KEY ([idCourse]) REFERENCES [tblCourse] ([idCourse])