IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblContentPackage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblContentPackage] (
	[idContentPackage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idContentPackageType]			INT								NOT NULL,
	[idSCORMPackageType]			INT								NULL,
	[name]							NVARCHAR(255)					NOT NULL,
	[path]							NVARCHAR(1024)					NOT NULL,
	[kb]							INT								NOT NULL,
	[manifest]						NVARCHAR(MAX)					NULL,
	[dtCreated]						DATETIME						NOT NULL,
	[dtModified]					DATETIME						NOT NULL,
	[isMediaUpload]					BIT								NULL,
	[idMediaType]					INT								NULL,
	[contentTitle]					NVARCHAR(255)					NULL,
	[originalMediaFilename]			NVARCHAR(255)					NULL,
	[isVideoMedia3rdParty]			BIT								NULL,
	[videoMediaEmbedCode]			NVARCHAR(MAX)					NULL,
	[enableAutoplay]				BIT								NULL,
	[allowRewind]					BIT								NULL,
	[allowFastForward]				BIT								NULL,
	[allowNavigation]				BIT								NULL,
	[allowResume]					BIT								NULL,
	[minProgressForCompletion]		FLOAT							NULL,
	[isProcessing]					BIT								NULL,
	[isProcessed]					BIT								NULL,
	[dtProcessed]					DATETIME						NULL,
	[idQuizSurvey]					INT								NULL,
	[hasManifestComplianceErrors]	BIT								NULL,
	[manifestComplianceErrors]		NVARCHAR(MAX)					NULL,
	[openSesameGUID]				NVARCHAR(36)					NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ContentPackage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [PK_ContentPackage] PRIMARY KEY CLUSTERED ([idContentPackage] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_ContentPackageType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_ContentPackageType] FOREIGN KEY (idContentPackageType) REFERENCES [tblContentPackageType] (idContentPackageType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_SCORMPackageType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_SCORMPackageType] FOREIGN KEY (idSCORMPackageType) REFERENCES [tblSCORMPackageType] (idSCORMPackageType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_MediaType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_MediaType] FOREIGN KEY (idMediaType) REFERENCES [tblMediaType] (idMediaType)

-- FK TO tblQuizSurvey IS DONE IN VERSION UPDATES Release_1.7 - IT SHOULD NOT BE DONE HERE BECAUSE IT WILL BE A CIRCULAR DEPENDENCY ON A NEW DATABASE

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblContentPackage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblContentPackage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblContentPackage (
	name,
	manifest
)
KEY INDEX PK_ContentPackage
ON Asentia -- insert index to this catalog