IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRoleLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRoleLanguage] (
	[idRoleLanguage]			INT				IDENTITY(100,1)		NOT NULL, 
	[idSite]					INT									NOT NULL,
	[idRole]					INT									NOT NULL,
	[idLanguage]				INT									NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RoleLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleLanguage ADD CONSTRAINT [PK_RoleLanguage] PRIMARY KEY CLUSTERED (idRoleLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleLanguage ADD CONSTRAINT [FK_RoleLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleLanguage_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleLanguage ADD CONSTRAINT [FK_RoleLanguage_Role] FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleLanguage ADD CONSTRAINT [FK_RoleLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRoleLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRoleLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRoleLanguage (
	name
)
KEY INDEX PK_RoleLanguage
ON Asentia -- insert index to this catalog

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblRoleLanguage] ON

INSERT INTO [tblRoleLanguage] (
	idRoleLanguage,
	idSite,
	idRole,
	idLanguage,
	name
)
SELECT
	idRoleLanguage,
	idSite,
	idRole,
	idLanguage,
	name
FROM (
	SELECT			1 AS idRoleLanguage, 1 AS idSite, 1 AS idRole, 57 AS idLanguage, 'System Administrator' AS name
	UNION SELECT	2 AS idRoleLanguage, 1 AS idSite, 1 AS idRole, 67 AS idLanguage, 'Administrador de Sistemas' AS name
	UNION SELECT	3 AS idRoleLanguage, 1 AS idSite, 1 AS idRole, 89 AS idLanguage, 'Administrateur du Syst�me' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblRoleLanguage RL WHERE RL.idRoleLanguage = MAIN.idRoleLanguage)

SET IDENTITY_INSERT [tblRoleLanguage] OFF