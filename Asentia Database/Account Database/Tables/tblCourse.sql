IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourse]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourse] (
	[idCourse]											INT					IDENTITY(1,1)	NOT NULL,
	[idSite]											INT									NOT NULL, 
	[title]												NVARCHAR(255)						NOT NULL,
	[coursecode]										NVARCHAR(255)						NULL,
	[revcode]											NVARCHAR(32)						NULL,
	[avatar]											NVARCHAR(255)						NULL,
	[cost]												FLOAT								NULL,
	[credits]											FLOAT								NULL,
	[minutes]											INT									NULL,
	[shortDescription]									NVARCHAR(512)						NULL, 
	[longDescription]									NVARCHAR(MAX)						NULL, 
	[objectives]										NVARCHAR(MAX)						NULL, 
	[socialMedia]										NVARCHAR(MAX)						NULL,
	[isPublished]										BIT									NULL,
	[isClosed]											BIT									NULL,
	[isLocked]											BIT									NULL,
	[isPrerequisiteAny]									BIT									NULL,
	[isFeedActive]										BIT								    NULL,
	[isFeedModerated]									BIT                                 NULL,
	[isFeedOpenSubscription]							BIT                                 NULL,
	[dtCreated]											DATETIME							NULL,
	[dtModified]										DATETIME							NULL,
	[isDeleted]											BIT									NOT NULL,
	[dtDeleted]											DATETIME							NULL,
	[order]												INT									NULL,
	[disallowRating]									BIT									NULL,
	[forceLessonCompletionInOrder]						BIT									NULL,
	[selfEnrollmentIsOneTimeOnly]						BIT									NULL,
	[selfEnrollmentDueInterval]							INT									NULL,
	[selfEnrollmentDueTimeframe]						NVARCHAR(4)							NULL,
	[selfEnrollmentExpiresFromStartInterval]			INT									NULL,
	[selfEnrollmentExpiresFromStartTimeframe]			NVARCHAR(4)							NULL,
	[selfEnrollmentExpiresFromFirstLaunchInterval]		INT									NULL,
	[selfEnrollmentExpiresFromFirstLaunchTimeframe]		NVARCHAR(4)							NULL,
	[searchTags]										NVARCHAR(512)						NULL,
	[requireFirstLessonToBeCompletedBeforeOthers]		BIT									NULL,
	[lockLastLessonUntilOthersCompleted]				BIT									NULL,
	[isSelfEnrollmentApprovalRequired]					BIT									NULL,
	[isApprovalAllowedByCourseExperts]					BIT									NULL,
	[isApprovalAllowedByUserSupervisor]					BIT									NULL,
	[shortcode]											NVARCHAR(10)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourse ADD CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED (idCourse ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Course_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourse ADD CONSTRAINT [FK_Course_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourse]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourse]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCourse (
	title,
	coursecode, 
	shortDescription,
	searchTags
)
KEY INDEX PK_Course
ON Asentia -- insert index to this catalog