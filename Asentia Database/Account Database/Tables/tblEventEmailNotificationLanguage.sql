IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotificationLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventEmailNotificationLanguage] (
	[idEventEmailNotificationLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL,
	[idEventEmailNotification]				INT								NOT NULL, 
	[idLanguage]							INT								NOT NULL,
	[name]									NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventEmailNotificationLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotificationLanguage ADD CONSTRAINT [PK_EventEmailNotificationLanguage] PRIMARY KEY CLUSTERED (idEventEmailNotificationLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailNotificationLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotificationLanguage ADD CONSTRAINT [FK_EventEmailNotificationLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailNotificationLanguage_EventEmailNotification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotificationLanguage ADD CONSTRAINT [FK_EventEmailNotificationLanguage_EventEmailNotification] FOREIGN KEY (idEventEmailNotification) REFERENCES [tblEventEmailNotification] (idEventEmailNotification)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailNotificationLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotificationLanguage ADD CONSTRAINT [FK_EventEmailNotificationLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotificationLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotificationLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEventEmailNotificationLanguage (
	name
)
KEY INDEX PK_EventEmailNotificationLanguage
ON Asentia -- insert index to this catalog