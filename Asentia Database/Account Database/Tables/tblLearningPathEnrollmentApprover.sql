IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathEnrollmentApprover] (
	[idLearningPathEnrollmentApprover]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]								INT						NOT NULL,
	[idLearningPath]						INT						NOT NULL, 
	[idUser]								INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathEnrollmentApprover ADD CONSTRAINT [PK_LearningPathEnrollmentApprover] PRIMARY KEY CLUSTERED (idLearningPathEnrollmentApprover ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollmentApprover_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathEnrollmentApprover ADD CONSTRAINT [FK_LearningPathEnrollmentApprover_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollmentApprover_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathEnrollmentApprover ADD CONSTRAINT [FK_LearningPathEnrollmentApprover_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollmentApprover_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathEnrollmentApprover ADD CONSTRAINT [FK_LearningPathEnrollmentApprover_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)