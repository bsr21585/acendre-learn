IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblQuizSurvey]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblQuizSurvey] (
	[idQuizSurvey]		INT					IDENTITY(1,1)	NOT NULL,
	[idSite]			INT									NOT NULL,
	[idAuthor]			INT									NOT NULL,
	[type]				INT									NOT NULL,
	[identifier]		NVARCHAR(255)						NOT NULL,
	[guid]				NVARCHAR(36)						NOT NULL,
	[data]				NVARCHAR(MAX)						NOT NULL,
	[isDraft]			BIT									NULL,
	[idContentPackage]	INT									NULL,
	[dtCreated]			DATETIME							NOT NULL,
	[dtModified]		DATETIME							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_QuizSurvey]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblQuizSurvey ADD CONSTRAINT [PK_QuizSurvey] PRIMARY KEY CLUSTERED (idQuizSurvey ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_QuizSurvey_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblQuizSurvey ADD CONSTRAINT [FK_QuizSurvey_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_QuizSurvey_Author]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblQuizSurvey ADD CONSTRAINT [FK_QuizSurvey_Author] FOREIGN KEY (idAuthor) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_QuizSurvey_IdContentPackage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblQuizSurvey ADD CONSTRAINT [FK_QuizSurvey_IdContentPackage] FOREIGN KEY (idContentPackage) REFERENCES [tblContentPackage] (idContentPackage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblQuizSurvey]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblQuizSurvey]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblQuizSurvey (
	identifier
)
KEY INDEX PK_QuizSurvey
ON Asentia -- insert index to this catalog