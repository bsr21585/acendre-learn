IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserLog] (
	[idUser]							INT										NOT NULL, 
	[idSite]							INT										NOT NULL, 
	[action]							NVARCHAR(25)							NOT NULL,
	[timestamp]							DATETIME								NOT NULL,
) ON [PRIMARY]

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserLog_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLog ADD CONSTRAINT [FK_UserLog_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserLog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLog ADD CONSTRAINT [FK_UserLog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)