IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResourceToObjectLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblResourceToObjectLink] (
	[id]							INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idResource]					INT								NOT NULL, 
	[idObject]						INT								NULL,
	[objectType]					NVARCHAR(255)					NULL,
	[dtStart]	                    DATETIME                        NOT NULL,
	[dtEnd]                         DATETIME                        NOT NULL,
	[isOutsideUse]					BIT								NULL,
	[isMaintenance]					BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ResourceToObjectLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceToObjectLink ADD CONSTRAINT [PK_ResourceToObjectLink] PRIMARY KEY CLUSTERED (id ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceToObjectLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceToObjectLink ADD CONSTRAINT [FK_ResourceToObjectLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceToObjectLink_Resource]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceToObjectLink ADD CONSTRAINT [FK_ResourceToObjectLink_Resource] FOREIGN KEY (idResource) REFERENCES [tblResource] (idResource)