IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToGroupLink] (
	[idUserToGroupLink]					INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idUser]							INT							NOT NULL,
	[idGroup]							INT							NOT NULL,
	[idRuleSet]							INT							NULL,		-- this value indicates the ruleset that is responsible for this link. NULL means the group membership was manually assigned.
	[created]							DATETIME					NOT NULL,
	[selfJoined]						BIT							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToGroupLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToGroupLink ADD CONSTRAINT [PK_UserToGroupLink] PRIMARY KEY CLUSTERED (idUserToGroupLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToGroupLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToGroupLink ADD CONSTRAINT [FK_UserToGroupLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToGroupLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToGroupLink ADD CONSTRAINT [FK_UserToGroupLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToGroupLink_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToGroupLink ADD CONSTRAINT [FK_UserToGroupLink_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)