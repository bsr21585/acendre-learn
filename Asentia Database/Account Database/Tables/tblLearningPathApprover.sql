IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathApprover] (
	[idLearningPathApprover]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]						INT						NOT NULL,
	[idLearningPath]				INT						NOT NULL, 
	[idUser]						INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathApprover]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathApprover ADD CONSTRAINT [PK_LearningPathApprover] PRIMARY KEY CLUSTERED (idLearningPathApprover ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathApprover_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathApprover ADD CONSTRAINT [FK_LearningPathApprover_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathApprover_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathApprover ADD CONSTRAINT [FK_LearningPathApprover_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathApprover_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathApprover ADD CONSTRAINT [FK_LearningPathApprover_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)