IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventTypeRecipient]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventTypeRecipient] (
	[idEventTypeRecipient]			INT					IDENTITY(1,1)	NOT NULL,
	[name]							NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventTypeRecipient]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventTypeRecipient ADD CONSTRAINT [PK_EventTypeRecipient] PRIMARY KEY CLUSTERED (idEventTypeRecipient ASC)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblEventTypeRecipient] ON

INSERT INTO [tblEventTypeRecipient] (
	idEventTypeRecipient,
	name
)
SELECT
	idEventTypeRecipient,
	name
FROM (
	SELECT			1 AS idEventTypeRecipient,	'System Administrator' AS name
	UNION SELECT	2 AS idEventTypeRecipient,	'Learner' AS name
	UNION SELECT	3 AS idEventTypeRecipient,	'User' AS name
	UNION SELECT	4 AS idEventTypeRecipient,	'Supervisor' AS name
	UNION SELECT	5 AS idEventTypeRecipient,	'Instructor' AS name
	UNION SELECT	6 AS idEventTypeRecipient,	'Learner(s)' AS name
	UNION SELECT	7 AS idEventTypeRecipient,	'User(s)' AS name
	UNION SELECT	8 AS idEventTypeRecipient,	'Supervisor(s)' AS name
	UNION SELECT	9 AS idEventTypeRecipient,	'Proctor(s)' AS name
	UNION SELECT	10 AS idEventTypeRecipient,	'Moderator(s)' AS name
	UNION SELECT	11 AS idEventTypeRecipient,	'Approver(s)' AS name
	UNION SELECT	12 AS idEventTypeRecipient,	'Specific Email Address' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblEventTypeRecipient ETR WHERE ETR.idEventTypeRecipient = MAIN.idEventTypeRecipient)

SET IDENTITY_INSERT [tblEventTypeRecipient] OFF