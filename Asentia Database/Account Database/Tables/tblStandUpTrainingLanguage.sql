IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTrainingLanguage] (
	[idStandUpTrainingLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idStandUpTraining]					INT								NOT NULL,
	[idSite]							INT								NOT NULL,
	[idLanguage]						INT								NOT NULL,
	[title]								NVARCHAR(255)					NOT NULL,
	[description]						NVARCHAR(MAX)					NULL,
	[objectives]						NVARCHAR(MAX)					NULL, 
	[searchTags]						NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTrainingLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingLanguage ADD CONSTRAINT [PK_StandUpTrainingLanguage] PRIMARY KEY CLUSTERED (idStandUpTrainingLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingLanguage_StandUpTraining]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingLanguage ADD CONSTRAINT [FK_StandUpTrainingLanguage_StandUpTraining] FOREIGN KEY (idStandUpTraining) REFERENCES [tblStandUpTraining] (idStandUpTraining)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingLanguage ADD CONSTRAINT [FK_StandUpTrainingLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingLanguage ADD CONSTRAINT [FK_StandUpTrainingLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblStandUpTrainingLanguage (
	title,
	[description],
	searchTags
)
KEY INDEX PK_StandUpTrainingLanguage
ON Asentia -- insert index to this catalog