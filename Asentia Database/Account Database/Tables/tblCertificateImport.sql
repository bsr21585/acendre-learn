IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificateImport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificateImport] (
	[idCertificateImport]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idUser]							INT								NOT NULL,
	[timestamp]							DATETIME						NOT NULL,
	[idUserImported]					INT								NULL,
	[importFileName]					NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificateImport]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateImport ADD CONSTRAINT [PK_CertificateImport] PRIMARY KEY CLUSTERED (idCertificateImport ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateImport_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateImport ADD CONSTRAINT [FK_CertificateImport_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateImport_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateImport ADD CONSTRAINT [FK_CertificateImport_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateImport_UserImported]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateImport ADD CONSTRAINT [FK_CertificateImport_UserImported] FOREIGN KEY (idUserImported) REFERENCES [tblUser] (idUser)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificateImport]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificateImport]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificateImport (
	importFileName
)
KEY INDEX PK_CertificateImport
ON Asentia -- insert index to this catalog