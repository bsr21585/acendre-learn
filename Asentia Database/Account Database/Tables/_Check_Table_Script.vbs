Dim sContent
Dim missingFiles : missingFiles = ""
Dim counter : counter = 0
Dim sFileName: sFileName = "_execution order.txt" 
Set objFSO = CreateObject("Scripting.FileSystemObject")
objStartFolder = objFSO.GetAbsolutePathName(".")

Set objFolder = objFSO.GetFolder(objStartFolder)
Set colFiles = objFolder.Files

Set TxtFile = objFSO.OpenTextFile(sFileName,1)
if not TxtFile.atendofstream then
	sContent = TxtFile.ReadAll

	For Each objFile in colFiles
		If ((UCase(objFSO.GetExtensionName(objFile.name)) = "SQL") And (objFile.name <> "_TABLES.sql")  And (objFile.name <> "_TableTypes.sql") And (objFile.name <> "_DELETE_TABLES.sql")) Then
			If InStr(sContent,objFile.name) = 0 Then 
				missingFiles = missingFiles & vbCrLf & objFile.name 
			End If	
		End If
	Next
	If(missingFiles <> "") Then
		Set a = objFSO.CreateTextFile("_Check_Table_Script_Result.txt", True)
		a.WriteLine("Following table(s) are missing in execution order file" & missingFiles)
		a.Close	
		Wscript.Echo  "A text file _Check_Table_Script_Result.txt, with all missing table's name has been created"
	Else
		if objFSO.FileExists("_Check_Table_Script_Result.txt") then
			objFSO.DeleteFile "_Check_Table_Script_Result.txt"
		end if
		Wscript.Echo  "All table(s) are present in execution order file"
	End If
Else
	Wscript.Echo  "_execution order.txt text file is empty.Please add table names in file." 
End If