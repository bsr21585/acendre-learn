IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirement] (
	[idCertificationModuleRequirement]			INT					IDENTITY(1,1)	NOT NULL,
	[idCertificationModuleRequirementSet]		INT									NOT NULL,
	[idSite]									INT									NOT NULL, 	
	[label]										NVARCHAR(255)						NOT NULL,
	[shortDescription]							NVARCHAR(512)						NULL,
	[requirementType]							INT									NOT NULL,
	[courseCompletionIsAny]						BIT									NULL,
	[forceCourseCompletionInOrder]				BIT									NULL,	
	[numberCreditsRequired]						FLOAT								NULL,
	[courseCreditEligibleCoursesIsAll]			BIT									NULL,
	[documentUploadFileType]					INT									NULL,
	[documentationApprovalRequired]				BIT									NULL,
	[allowSupervisorsToApproveDocumentation]	BIT									NULL,
	[allowExpertsToApproveDocumentation]		BIT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirement ADD CONSTRAINT [PK_CertificationModuleRequirement] PRIMARY KEY CLUSTERED (idCertificationModuleRequirement ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirement_CertificationModuleRequirementSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirement ADD CONSTRAINT [FK_CertificationModuleRequirement_CertificationModuleRequirementSet] FOREIGN KEY (idCertificationModuleRequirementSet) REFERENCES [tblCertificationModuleRequirementSet] (idCertificationModuleRequirementSet)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirement_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirement ADD CONSTRAINT [FK_CertificationModuleRequirement_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirement]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirement]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleRequirement (
	label
)
KEY INDEX PK_CertificationModuleRequirement
ON Asentia -- insert index to this catalog