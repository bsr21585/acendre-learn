IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportSubscription]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportSubscription] (
	[idReportSubscription]				INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL,
	[idUser]							INT									NOT NULL,
	[idReport]							INT									NOT NULL,
	[dtStart]							DATETIME							NOT NULL, 
	[dtNextAction]						DATETIME							NOT NULL,
	[recurInterval]						INT									NOT NULL, 
	[recurTimeframe]					NVARCHAR(4)							NOT NULL,
	[token]								UNIQUEIDENTIFIER						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportSubscription]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscription ADD CONSTRAINT [PK_ReportSubscription] PRIMARY KEY CLUSTERED (idReportSubscription ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportSubscription_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscription ADD CONSTRAINT [FK_ReportSubscription_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportSubscription_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscription ADD CONSTRAINT [FK_ReportSubscription_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportSubscription_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscription ADD CONSTRAINT [FK_ReportSubscription_Report] FOREIGN KEY (idReport) REFERENCES [tblReport] (idReport)