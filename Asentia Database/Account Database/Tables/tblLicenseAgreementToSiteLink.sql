IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLicenseAgreementToSiteLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLicenseAgreementToSiteLink] (
		[idLicenseAgreementToSiteLink]		INT		IDENTITY(1,1)	NOT NULL,
		[idSite]							INT						NOT NULL,
		[dtExecuted]						DATETIME				NULL,
		[executeLicenseAgreementGUID]		NVARCHAR(MAX)			NULL,
		[licenseAgreementIPAddress]			NVARCHAR(MAX)			NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LicenseAgreementToSiteLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLicenseAgreementToSiteLink ADD CONSTRAINT [PK_LicenseAgreementToSiteLink] PRIMARY KEY CLUSTERED (idLicenseAgreementToSiteLink ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LicenseAgreementToSiteLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLicenseAgreementToSiteLink ADD CONSTRAINT [FK_LicenseAgreementToSiteLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)