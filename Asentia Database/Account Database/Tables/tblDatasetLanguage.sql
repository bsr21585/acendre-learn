﻿IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDatasetLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDatasetLanguage] (
	[idDatasetLanguage]					INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idDataset]							INT									NOT NULL,
	[idLanguage]						INT									NOT NULL,
	[name]								NVARCHAR(255)						NOT NULL,
	[description]						NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DatasetLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatasetLanguage ADD CONSTRAINT [PK_DatasetLanguage] PRIMARY KEY CLUSTERED (idDatasetLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DatasetLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatasetLanguage ADD CONSTRAINT [FK_DatasetLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DatasetLanguage_Dataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatasetLanguage ADD CONSTRAINT [FK_DatasetLanguage_Dataset] FOREIGN KEY (idDataset) REFERENCES [tblDataset] (idDataset)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DatasetLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatasetLanguage ADD CONSTRAINT [FK_DatasetLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
INSERT VALUES
**/
DELETE FROM tblDatasetLanguage

SET IDENTITY_INSERT [tblDatasetLanguage] ON

INSERT INTO tblDatasetLanguage (
	idDatasetLanguage,
	idSite,
	idDataset,
	idLanguage,
	name,
	[description]
)
SELECT
	idDatasetLanguage,
	idSite,
	idDataset,
	idLanguage,
	name,
	[description]
FROM (
	/* en-US */
	SELECT 1 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 57 AS idLanguage, N'User Demographics' AS name, N'Complete user account information for all users including name, username, email, contact information, organizational information and more.' AS [description]
	UNION SELECT 2 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 57 AS idLanguage, N'User Course Transcripts' AS name, N'Course and lesson status, and completion information including scores and dates.' AS [description]
	UNION SELECT 3 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 57 AS idLanguage, N'Catalog and Course Information' AS name, N'Information for catalogs, courses, lessons, and ILT sessions.' AS [description]
	UNION SELECT 4 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 57 AS idLanguage, N'Certificates' AS name, N'Certificate award information. Includes certificates that have been earned as well as those that have been manually awarded.' AS [description]
	UNION SELECT 5 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 57 AS idLanguage, N'xAPI' AS name, N'xAPI statments recorded to the system''s learning record store from outside training.' AS [description]
	UNION SELECT 6 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 57 AS idLanguage, N'User Learning Path Transcripts' AS name, N'Learning Path status and completion information.' AS [description]
	UNION SELECT 7 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 57 AS idLanguage, N'User ILT Transcripts' AS name, N'Instructor Led Training status and completion information including scores and dates.' AS [description]
	UNION SELECT 8 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 57 AS idLanguage, N'Purchases' AS name, N'Information on purchases made by users within the system.' AS [description]
	UNION SELECT 9 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 57 AS idLanguage, N'User Certification Transcripts' AS name, N'Certification status information.' AS [description]
	/* es-MX */
	UNION SELECT 10 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 70 AS idLanguage, N'Datos demográficos del usuario' AS name, N'Complete la información de la cuenta de usuario para todos los usuarios, incluidos el nombre, el nombre de usuario, el correo electrónico, la información de contacto, la información de la organización y más.' AS [description]
	UNION SELECT 11 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 70 AS idLanguage, N'Transcripciones del curso del usuario' AS name, N'Curso y estado de la lección, e información de finalización que incluye puntajes y fechas.' AS [description]
	UNION SELECT 12 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 70 AS idLanguage, N'Catálogo e información del curso' AS name, N'Información para catálogos, cursos, lecciones y sesiones ILT.' AS [description]
	UNION SELECT 13 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 70 AS idLanguage, N'Certificados' AS name, N'Información de adjudicación de certificado. Incluye los certificados que se han obtenido, así como los que se han otorgado manualmente.' AS [description]
	UNION SELECT 14 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 70 AS idLanguage, N'xAPI' AS name, N'Declaraciones de xAPI registradas en la tienda de discos de aprendizaje del sistema desde fuera del entrenamiento.' AS [description]
	UNION SELECT 15 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 70 AS idLanguage, N'Transcripciones de la ruta de aprendizaje del usuario' AS name, N'Aprendizaje de ruta de acceso e información de finalización.' AS [description]
	UNION SELECT 16 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 70 AS idLanguage, N'Transcripciones ILT de usuario' AS name, N'El instructor llevó el estado del entrenamiento y la información de finalización, incluidos puntajes y fechas.' AS [description]
	UNION SELECT 17 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 70 AS idLanguage, N'Compras' AS name, N'Información sobre las compras realizadas por los usuarios dentro del sistema.' AS [description]
	UNION SELECT 18 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 70 AS idLanguage, N'Transcripciones de certificación de usuario' AS name, N'Información de estado de certificación.' AS [description]
	/* fr-FR */
	UNION SELECT 19 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 89 AS idLanguage, N'Démographie de l''utilisateur' AS name, N'Informations complètes sur le compte utilisateur pour tous les utilisateurs, y compris le nom, le nom d''utilisateur, le courrier électronique, les informations de contact, les informations sur l''organisation et plus encore.' AS [description]
	UNION SELECT 20 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 89 AS idLanguage, N'Transcriptions de cours d''utilisateur' AS name, N'L''état du cours et de la leçon, ainsi que les informations d''achèvement, y compris les scores et les dates.' AS [description]
	UNION SELECT 21 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 89 AS idLanguage, N'Catalogue et informations sur le cours' AS name, N'Informations pour les catalogues, les cours, les leçons et les sessions ILT.' AS [description]
	UNION SELECT 22 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 89 AS idLanguage, N'Certificats' AS name, N'Informations sur le prix du certificat Inclut les certificats qui ont été gagnés ainsi que ceux qui ont été attribués manuellement.' AS [description]
	UNION SELECT 23 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 89 AS idLanguage, N'xAPI' AS name, N'Statements xAPI enregistrés dans le magasin de disques d''apprentissage du système à partir d''une formation externe.' AS [description]
	UNION SELECT 24 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 89 AS idLanguage, N'Transcriptions de parcours d''apprentissage utilisateur' AS name, N'Statut du chemin d''apprentissage et informations d''achèvement.' AS [description]
	UNION SELECT 25 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 89 AS idLanguage, N'Transcriptions d''ILT d''utilisateur' AS name, N'Instructeur dirigé Statut de formation et informations d''achèvement, y compris les scores et les dates.' AS [description]
	UNION SELECT 26 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 89 AS idLanguage, N'Achats' AS name, N'Informations sur les achats effectués par les utilisateurs dans le système.' AS [description]
	UNION SELECT 27 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 89 AS idLanguage, N'Transcriptions de certification d''utilisateur' AS name, N'Informations sur l''état de la certification' AS [description]
	/* it-IT */
	UNION SELECT 28 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 111 AS idLanguage, N'Demografia utente' AS name, N'Informazioni complete sull''account utente per tutti gli utenti tra cui nome, nome utente, email, informazioni di contatto, informazioni organizzative e altro.' AS [description]
	UNION SELECT 29 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 111 AS idLanguage, N'Trascrizioni del corso dell''utente' AS name, N'Stato del corso e della lezione e informazioni sul completamento, compresi punteggi e date.' AS [description]
	UNION SELECT 30 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 111 AS idLanguage, N'Catalogo e informazioni sui corsi' AS name, N'Informazioni per cataloghi, corsi, lezioni e sessioni ILT.' AS [description]
	UNION SELECT 31 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 111 AS idLanguage, N'certificati' AS name, N'Informazioni sui premi del certificato. Include i certificati che sono stati guadagnati e quelli che sono stati assegnati manualmente.' AS [description]
	UNION SELECT 32 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 111 AS idLanguage, N'xAPI' AS name, N'Le registrazioni xAPI registrate nel negozio di record di apprendimento del sistema dall''addestramento esterno.' AS [description]
	UNION SELECT 33 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 111 AS idLanguage, N'Trascrizioni del percorso di apprendimento dell''utente' AS name, N'Stato del percorso di apprendimento e informazioni sul completamento.' AS [description]
	UNION SELECT 34 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 111 AS idLanguage, N'Trascrizioni ILT utente' AS name, N'Stato di istruttore e informazioni sullo stato di completamento del corso inclusi punteggi e date.' AS [description]
	UNION SELECT 35 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 111 AS idLanguage, N'acquisti' AS name, N'Informazioni sugli acquisti effettuati dagli utenti all''interno del sistema.' AS [description]
	UNION SELECT 36 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 111 AS idLanguage, N'Trascrizioni di certificazione utente' AS name, N'Informazioni sullo stato della certificazione.' AS [description]
	/* zh-CN */
	UNION SELECT 37 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 204 AS idLanguage, N'用户属性' AS name, N'为所有用户提供完整的用户帐户信息，包括姓名、用户名、电子邮件、联系信息和组织信息等。' AS [description]
	UNION SELECT 38 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 204 AS idLanguage, N'用户课程成绩' AS name, N'课程、课堂状态和包括分数和日期的完成信息。' AS [description]
	UNION SELECT 39 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 204 AS idLanguage, N'目录课程信息' AS name, N'有关目录、课程、课堂和讲师指导培训场次的信息。' AS [description]
	UNION SELECT 40 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 204 AS idLanguage, N'证书' AS name, N'证书颁发信息。包括已经获得的证书以及人工颁发的证书。' AS [description]
	UNION SELECT 41 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 204 AS idLanguage, N'xAPI' AS name, N'xAPI陈述将系统外培训信息记录到系统的学习记录。' AS [description]
	UNION SELECT 42 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 204 AS idLanguage, N'用户学习路径成绩' AS name, N'学习路径状态和包括分数和日期的完成信息。' AS [description]
	UNION SELECT 43 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 204 AS idLanguage, N'讲师指导培训成绩' AS name, N'讲师指导培训状态和包括分数和日期的完成认证信息。' AS [description]
	UNION SELECT 44 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 204 AS idLanguage, N'购买' AS name, N'用户在系统内的购买信息。' AS [description]
	UNION SELECT 45 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 204 AS idLanguage, N'用户认证成绩' AS name, N'认证状态信息。' AS [description]
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblDatasetLanguage DL WHERE DL.idDatasetLanguage = MAIN.idDatasetLanguage)

SET IDENTITY_INSERT [tblDatasetLanguage] OFF