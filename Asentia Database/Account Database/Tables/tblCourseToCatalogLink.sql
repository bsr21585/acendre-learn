IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseToCatalogLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseToCatalogLink] (
	[idCourseToCatalogLink]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idCourse]							INT							NOT NULL, 
	[idCatalog]							INT							NOT NULL,
	[order]								INT							NULL	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseToCatalogLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToCatalogLink ADD CONSTRAINT [PK_CourseToCatalogLink] PRIMARY KEY CLUSTERED (idCourseToCatalogLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToCatalogLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToCatalogLink ADD CONSTRAINT [FK_CourseToCatalogLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToCatalogLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToCatalogLink ADD CONSTRAINT [FK_CourseToCatalogLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToCatalogLink_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToCatalogLink ADD CONSTRAINT [FK_CourseToCatalogLink_Catalog] FOREIGN KEY (idCatalog) REFERENCES [tblCatalog] (idCatalog)