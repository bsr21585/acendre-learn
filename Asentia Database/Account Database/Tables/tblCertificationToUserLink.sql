IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationToUserLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationToUserLink] (
	[idCertificationToUserLink]			INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idCertification]					INT									NOT NULL,
	[idUser]							INT									NOT NULL,
	[initialAwardDate]					DATETIME							NULL,
	[certificationTrack]				INT									NOT NULL,
	[currentExpirationDate]				DATETIME							NULL, 
	[currentExpirationInterval]			INT									NULL,
	[currentExpirationTimeframe]		NVARCHAR(4)							NULL,
	[certificationId]					NVARCHAR(255)						NULL,
	[lastExpirationDate]				DATETIME							NULL, -- this is the new "start"
	[idRuleSet]							INT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationToUserLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToUserLink ADD CONSTRAINT [PK_CertificationToUserLink] PRIMARY KEY CLUSTERED (idCertificationToUserLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToUserLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToUserLink ADD CONSTRAINT [FK_CertificationToUserLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToUserLink_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToUserLink ADD CONSTRAINT [FK_CertificationToUserLink_Certification] FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToUserLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToUserLink ADD CONSTRAINT [FK_CertificationToUserLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)