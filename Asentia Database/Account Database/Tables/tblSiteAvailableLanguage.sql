IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteAvailableLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteAvailableLanguage] (
	[idSiteAvailableLanguage]			INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL,
	[idLanguage]						INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteAvailableLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteAvailableLanguage ADD CONSTRAINT [PK_SiteAvailableLanguage] PRIMARY KEY CLUSTERED (idSiteAvailableLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteAvailableLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteAvailableLanguage ADD CONSTRAINT [FK_SiteAvailableLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteAvailableLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteAvailableLanguage ADD CONSTRAINT [FK_SiteAvailableLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)