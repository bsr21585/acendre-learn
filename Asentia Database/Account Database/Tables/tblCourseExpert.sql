IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseExpert]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseExpert] (
	[idCourseExpert]			INT		 IDENTITY(1,1)	NOT NULL,
	[idSite]					INT						NOT NULL,
	[idCourse]					INT						NOT NULL, 
	[idUser]					INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseExpert]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseExpert ADD CONSTRAINT [PK_CourseExpert] PRIMARY KEY CLUSTERED (idCourseExpert ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseExpert_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseExpert ADD CONSTRAINT [FK_CourseExpert_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseExpert_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseExpert ADD CONSTRAINT [FK_CourseExpert_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseExpert_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseExpert ADD CONSTRAINT [FK_CourseExpert_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)