IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertification]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertification] (
	[idCertification]					INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[title]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL, 
	[searchTags]						NVARCHAR(512)						NULL, 
	[dtCreated]							DATETIME							NOT NULL,
	[dtModified]						DATETIME							NULL,
	[isDeleted]							BIT									NOT NULL,
	[dtDeleted]							DATETIME							NULL,
	[initialAwardExpiresInterval]		INT									NULL,
	[initialAwardExpiresTimeframe]		NVARCHAR(4)							NULL,
	[renewalExpiresInterval]			INT									NULL,
	[renewalExpiresTimeframe]			NVARCHAR(4)							NULL,
	[accreditingOrganization]			NVARCHAR(255)						NULL,
	[certificationContactName]			NVARCHAR(255)						NULL,
	[certificationContactEmail]			NVARCHAR(255)						NULL,
	[certificationContactPhoneNumber]	NVARCHAR(20)						NULL,
	[isPublished]						BIT									NULL,
	[isClosed]							BIT									NULL,
	[isAnyModule]						BIT									NOT NULL,
	[isRenewalAnyModule]				BIT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertification ADD CONSTRAINT [PK_Certification] PRIMARY KEY CLUSTERED (idCertification ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Certification_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertification ADD CONSTRAINT [FK_Certification_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertification]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertification]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertification (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_Certification
ON Asentia -- insert index to this catalog