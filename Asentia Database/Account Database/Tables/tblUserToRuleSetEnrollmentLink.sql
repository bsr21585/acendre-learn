/* THIS IS GOING TO BE REMOVED */

/**

Records within this table should be unique for user/course. 
The rule ID and priority values will be updated as a user drops/joins 
new rules that are valid for the same course.

**/

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToRuleSetEnrollmentLink] (
	[idUserToRuleSetEnrollmentLink]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idUser]								INT							NOT NULL,
	[idRuleSetEnrollment]					INT							NOT NULL,
	[idCourse]								INT							NOT NULL,
	[idRuleSet]								INT							NULL,
	[created]								DATETIME					NOT NULL
)

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRuleSetEnrollmentLink ADD CONSTRAINT [PK_UserToRuleSetEnrollmentLink] PRIMARY KEY CLUSTERED (idUserToRuleSetEnrollmentLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRuleSetEnrollmentLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRuleSetEnrollmentLink ADD CONSTRAINT [FK_UserToRuleSetEnrollmentLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRuleSetEnrollmentLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRuleSetEnrollmentLink ADD CONSTRAINT FK_UserToRuleSetEnrollmentLink_User FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRuleSetEnrollmentLink_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRuleSetEnrollmentLink ADD CONSTRAINT FK_UserToRuleSetEnrollmentLink_RuleSetEnrollment FOREIGN KEY ([idRuleSetEnrollment]) REFERENCES [tblRuleSetEnrollment] ([idRuleSetEnrollment])
	
-- DO NOT create Foreign Key to idCourse...we only need it to detect changes.