IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventLog] (
	[idEventLog]				INT			IDENTITY(2,1)	NOT NULL,
	[idSite]					INT							NOT NULL, 
	[idEventType]				INT							NOT NULL,
	[timestamp]					DATETIME					NOT NULL,	-- timestamp of the record
	[eventDate]					DATETIME					NULL,		-- timestamp of when the actual event occurred
	[idObject]					INT							NOT NULL,	-- the ID of the record that links the OBJECT, i.e. user, course, lesson, session, learning path, certificate 
																		-- from this, we should be able to join to the object itself to get the correct email notification template
	[idObjectRelated]			INT							NOT NULL,	-- the object related to the event's main object, i.e. user, course enrollment id, lesson data id, session id,
																		-- learning path enrollment id, certificate record id
	[idObjectUser]				INT							NULL,		-- the ID of the user who is affected by the object/event (if applicable), this is stored simply for speed in sending notifications
	[idExecutingUser]			INT							NULL		-- the user who performed/executed the event (if applicable)	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventLog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventLog ADD CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED (idEventLog ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventLog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventLog ADD CONSTRAINT [FK_EventLog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventLog_ObjectUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventLog ADD CONSTRAINT [FK_EventLog_ObjectUser] FOREIGN KEY (idObjectUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventLog_ExecutingUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventLog ADD CONSTRAINT [FK_EventLog_ExecutingUser] FOREIGN KEY (idExecutingUser) REFERENCES [tblUser] (idUser)