IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeToStandupTrainingLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeToStandupTrainingLink] (
	[idCouponCodeToStandupTrainingLink]	[int]		IDENTITY(1,1) NOT NULL,
	[idCouponCode]						[int]		NOT NULL,
	[idStandupTraining]					[int]		NOT NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCodeToStandupTrainingLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToStandupTrainingLink ADD CONSTRAINT [PK_CouponCodeToStandupTrainingLink] PRIMARY KEY CLUSTERED (idCouponCodeToStandupTrainingLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToStandupTrainingLink_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToStandupTrainingLink ADD CONSTRAINT [FK_CouponCodeToStandupTrainingLink_CouponCode] FOREIGN KEY (idCouponCode) REFERENCES [tblCouponCode] (idCouponCode)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToStandupTrainingLink_StandupTraining]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToStandupTrainingLink ADD CONSTRAINT [FK_CouponCodeToStandupTrainingLink_StandupTraining] FOREIGN KEY (idStandUpTraining) REFERENCES [tblStandUpTraining] (idStandupTraining)
