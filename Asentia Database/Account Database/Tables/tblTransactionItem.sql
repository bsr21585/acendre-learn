IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTransactionItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTransactionItem] (
	[idTransactionItem]					INT				IDENTITY(1,1)	NOT NULL,
	[idParentTransactionItem]			INT								NULL,
	[idPurchase]						INT								NULL,
	[idEnrollment]						INT								NULL,
	[idUser]							INT								NULL,
	[userName]							NVARCHAR(255)					NULL,
	[idAssigner]						INT								NULL,
	[assignerName]						NVARCHAR(255)					NULL,
	[itemId]							INT								NULL,
	[itemName]							NVARCHAR(255)					NULL,
	[itemType]							INT								NULL,
	[description]						NVARCHAR(512)					NULL,
	[cost]								FLOAT							NULL,
	[paid]								FLOAT							NULL,
	[idCouponCode]						INT								NULL,
	[couponCode]						NVARCHAR(10)					NULL,
	[dtAdded]							DATETIME						NULL,
	[confirmed]							BIT								NULL,
	[idSite]							INT								NOT NULL,
	[idIltSession]						INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_TransactionItem]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTransactionItem ADD CONSTRAINT [PK_TransactionItem] PRIMARY KEY CLUSTERED (idTransactionItem ASC)

/**
FOREIGN KEYS
*/

-- DO NOT FK idEnrollment TO tblEnrollment

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_tblTransactionItem_tblPurchase]') AND parent_object_id = OBJECT_ID(N'[tblTransactionItem]'))
	ALTER TABLE [tblTransactionItem]  WITH CHECK ADD  CONSTRAINT [FK_tblTransactionItem_tblPurchase] FOREIGN KEY([idPurchase]) REFERENCES [tblPurchase] ([idPurchase])

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_tblTransactionItem_tblTransactionItem]') AND parent_object_id = OBJECT_ID(N'[tblTransactionItem]'))
	ALTER TABLE [tblTransactionItem]  WITH CHECK ADD  CONSTRAINT [FK_tblTransactionItem_tblTransactionItem] FOREIGN KEY([idTransactionItem]) REFERENCES [tblTransactionItem] ([idTransactionItem])

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_tblTransactionItem_tblUser]') AND parent_object_id = OBJECT_ID(N'[tblTransactionItem]'))
	ALTER TABLE [tblTransactionItem]  WITH CHECK ADD  CONSTRAINT [FK_tblTransactionItem_tblUser] FOREIGN KEY([idUser]) REFERENCES [tblUser] ([idUser])

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_TransactionItem_Site]') AND parent_object_id = OBJECT_ID(N'[tblTransactionItem]'))
	ALTER TABLE [tblTransactionItem]  WITH CHECK ADD  CONSTRAINT [FK_TransactionItem_Site] FOREIGN KEY([idSite]) REFERENCES [tblSite] ([idSite])