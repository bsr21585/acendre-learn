IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportToReportShortcutsWidgetLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportToReportShortcutsWidgetLink] (
	[idReportToReportShortcutsWidgetLink]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]											INT								NOT NULL, 
	[idReport]											INT								NOT NULL, 
	[idUser]											INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportToReportShortcutsWidgetLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportToReportShortcutsWidgetLink ADD CONSTRAINT [PK_ReportToReportShortcutsWidgetLink] PRIMARY KEY CLUSTERED (idReportToReportShortcutsWidgetLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportToReportShortcutsWidgetLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportToReportShortcutsWidgetLink ADD CONSTRAINT [FK_ReportToReportShortcutsWidgetLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportToReportShortcutsWidgetLink_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportToReportShortcutsWidgetLink ADD CONSTRAINT [FK_ReportToReportShortcutsWidgetLink_Report] FOREIGN KEY (idReport) REFERENCES [tblReport] (idReport)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportToReportShortcutsWidgetLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportToReportShortcutsWidgetLink ADD CONSTRAINT [FK_ReportToReportShortcutsWidgetLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)



