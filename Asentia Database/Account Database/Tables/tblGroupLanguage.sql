IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupLanguage] (
	[idGroupLanguage]				INT				IDENTITY (1, 1)		NOT NULL, 
	[idSite]						INT									NOT NULL,
	[idGroup]						INT									NOT NULL,
	[idLanguage]					INT									NOT NULL,
	[name]							NVARCHAR(255)						NOT NULL,
	[shortDescription]				NVARCHAR(512)						NULL,
	[longDescription]				NVARCHAR(MAX)						NULL,
	[searchTags]					NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupLanguage ADD CONSTRAINT [PK_GroupLanguage] PRIMARY KEY CLUSTERED (idGroupLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupLanguage ADD CONSTRAINT [FK_GroupLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupLanguage_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupLanguage ADD CONSTRAINT [FK_GroupLanguage_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupLanguage ADD CONSTRAINT [FK_GroupLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblGroupLanguage (
	name,
	shortDescription,
	searchTags
)
KEY INDEX PK_GroupLanguage
ON Asentia -- insert index to this catalog