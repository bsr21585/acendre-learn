IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandupTrainingInstanceToInstructorLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandupTrainingInstanceToInstructorLink] (
	[idStandupTrainingInstanceToInstructorLink]		INT		IDENTITY (1, 1)		NOT NULL,
	[idSite]										INT							NOT NULL,
	[idStandupTrainingInstance]						INT							NOT NULL,
	[idInstructor]									INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandupTrainingInstanceToInstructorLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToInstructorLink ADD CONSTRAINT [PK_StandupTrainingInstanceToInstructorLink] PRIMARY KEY CLUSTERED (idStandupTrainingInstanceToInstructorLink ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToInstructorLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToInstructorLink ADD CONSTRAINT [FK_StandupTrainingInstanceToInstructorLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToInstructorLink_StandupTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToInstructorLink ADD CONSTRAINT [FK_StandupTrainingInstanceToInstructorLink_StandupTrainingInstance] FOREIGN KEY (idStandupTrainingInstance) REFERENCES [tblStandupTrainingInstance] (idStandupTrainingInstance)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToInstructorLink_Instructor]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToInstructorLink ADD CONSTRAINT [FK_StandupTrainingInstanceToInstructorLink_Instructor] FOREIGN KEY (idInstructor) REFERENCES [tblUser] (idUser)