﻿IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSystem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSystem] (
	[lastEventEmailQueueCascade]	DATETIME	NULL
) ON [PRIMARY]
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTimezone]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTimezone] (
	[idTimezone]					INT				IDENTITY(1,1)	NOT NULL,
	[dotNetName]					NVARCHAR(255)	UNIQUE			NOT NULL,
	[displayName]					NVARCHAR(255)					NOT NULL,
	[gmtOffset]						FLOAT							NOT NULL,
	[blnUseDaylightSavings]			BIT								NOT NULL,
	[order]							INT								NULL,
	[isEnabled]						BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezone ADD CONSTRAINT [PK_Timezone] PRIMARY KEY CLUSTERED (idTimezone ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT tblTimezone ON

INSERT INTO tblTimezone (
	idTimezone, 
	dotNetName, 
	displayName, 
	gmtOffset, 
	blnUseDaylightSavings, 
	[order], 
	isEnabled
)
SELECT
	idTimezone, 
	dotNetName, 
	displayName, 
	gmtOffset, 
	blnUseDaylightSavings, 
	[order], 
	isEnabled
FROM (
	SELECT 1 AS idTimezone, 'Dateline Standard Time' AS dotNetName, '(GMT-12:00) International Date Line West' AS displayName, -12 AS gmtOffset, 0 AS blnUseDaylightSavings, 1 AS [order], 1 AS isEnabled
	UNION SELECT 2 AS idTimezone, 'UTC-11' AS dotNetName, '(GMT-11:00) Coordinated Universal Time-11' AS displayName, -11 AS gmtOffset, 0 AS blnUseDaylightSavings, 2 AS [order], 1 AS isEnabled
	UNION SELECT 3 AS idTimezone, 'Hawaiian Standard Time' AS dotNetName, '(GMT-10:00) Hawaii' AS displayName, -10 AS gmtOffset, 0 AS blnUseDaylightSavings, 3 AS [order], 1 AS isEnabled
	UNION SELECT 4 AS idTimezone, 'Alaskan Standard Time' AS dotNetName, '(GMT-09:00) Alaska' AS displayName, -9 AS gmtOffset, 1 AS blnUseDaylightSavings, 4 AS [order], 1 AS isEnabled
	UNION SELECT 5 AS idTimezone, 'Pacific Standard Time (Mexico)' AS dotNetName, '(GMT-08:00) Baja California' AS displayName, -8 AS gmtOffset, 1 AS blnUseDaylightSavings, 5 AS [order], 1 AS isEnabled
	UNION SELECT 6 AS idTimezone, 'Pacific Standard Time' AS dotNetName, '(GMT-08:00) Pacific Time (US & Canada)' AS displayName, -8 AS gmtOffset, 1 AS blnUseDaylightSavings, 6 AS [order], 1 AS isEnabled
	UNION SELECT 7 AS idTimezone, 'US Mountain Standard Time' AS dotNetName, '(GMT-07:00) Arizona' AS displayName, -7 AS gmtOffset, 0 AS blnUseDaylightSavings, 7 AS [order], 1 AS isEnabled
	UNION SELECT 8 AS idTimezone, 'Mountain Standard Time (Mexico)' AS dotNetName, '(GMT-07:00) Chihuahua, La Paz, Mazatlan' AS displayName, -7 AS gmtOffset, 1 AS blnUseDaylightSavings, 8 AS [order], 1 AS isEnabled
	UNION SELECT 9 AS idTimezone, 'Mountain Standard Time' AS dotNetName, '(GMT-07:00) Mountain Time (US & Canada)' AS displayName, -7 AS gmtOffset, 1 AS blnUseDaylightSavings, 9 AS [order], 1 AS isEnabled
	UNION SELECT 10 AS idTimezone, 'Central America Standard Time' AS dotNetName, '(GMT-06:00) Central America' AS displayName, -6 AS gmtOffset, 0 AS blnUseDaylightSavings, 10 AS [order], 1 AS isEnabled
	UNION SELECT 11 AS idTimezone, 'Central Standard Time' AS dotNetName, '(GMT-06:00) Central Time (US & Canada)' AS displayName, -6 AS gmtOffset, 1 AS blnUseDaylightSavings, 11 AS [order], 1 AS isEnabled
	UNION SELECT 12 AS idTimezone, 'Central Standard Time (Mexico)' AS dotNetName, '(GMT-06:00) Guadalajara, Mexico City, Monterrey' AS displayName, -6 AS gmtOffset, 1 AS blnUseDaylightSavings, 12 AS [order], 1 AS isEnabled
	UNION SELECT 13 AS idTimezone, 'Canada Central Standard Time' AS dotNetName, '(GMT-06:00) Saskatchewan' AS displayName, -6 AS gmtOffset, 0 AS blnUseDaylightSavings, 13 AS [order], 1 AS isEnabled
	UNION SELECT 14 AS idTimezone, 'SA Pacific Standard Time' AS dotNetName, '(GMT-05:00) Bogota, Lima, Quito' AS displayName, -5 AS gmtOffset, 0 AS blnUseDaylightSavings, 14 AS [order], 1 AS isEnabled
	UNION SELECT 15 AS idTimezone, 'Eastern Standard Time' AS dotNetName, '(GMT-05:00) Eastern Time (US & Canada)' AS displayName, -5 AS gmtOffset, 1 AS blnUseDaylightSavings, 15 AS [order], 1 AS isEnabled
	UNION SELECT 16 AS idTimezone, 'US Eastern Standard Time' AS dotNetName, '(GMT-05:00) Indiana (East)' AS displayName, -5 AS gmtOffset, 1 AS blnUseDaylightSavings, 16 AS [order], 1 AS isEnabled
	UNION SELECT 17 AS idTimezone, 'Venezuela Standard Time' AS dotNetName, '(GMT-04:30) Caracas' AS displayName, -4.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 17 AS [order], 1 AS isEnabled
	UNION SELECT 18 AS idTimezone, 'Paraguay Standard Time' AS dotNetName, '(GMT-04:00) Asuncion' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 18 AS [order], 1 AS isEnabled
	UNION SELECT 19 AS idTimezone, 'Atlantic Standard Time' AS dotNetName, '(GMT-04:00) Atlantic Time (Canada)' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 19 AS [order], 1 AS isEnabled
	UNION SELECT 20 AS idTimezone, 'Central Brazilian Standard Time' AS dotNetName, '(GMT-04:00) Cuiaba' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 20 AS [order], 1 AS isEnabled
	UNION SELECT 21 AS idTimezone, 'SA Western Standard Time' AS dotNetName, '(GMT-04:00) Georgetown, La Paz, Manaus, San Juan' AS displayName, -4 AS gmtOffset, 0 AS blnUseDaylightSavings, 21 AS [order], 1 AS isEnabled
	UNION SELECT 22 AS idTimezone, 'Pacific SA Standard Time' AS dotNetName, '(GMT-04:00) Santiago' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 22 AS [order], 1 AS isEnabled
	UNION SELECT 23 AS idTimezone, 'Newfoundland Standard Time' AS dotNetName, '(GMT-03:30) Newfoundland' AS displayName, -3.5 AS gmtOffset, 1 AS blnUseDaylightSavings, 23 AS [order], 1 AS isEnabled
	UNION SELECT 24 AS idTimezone, 'E. South America Standard Time' AS dotNetName, '(GMT-03:00) Brasilia' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 24 AS [order], 1 AS isEnabled
	UNION SELECT 25 AS idTimezone, 'Argentina Standard Time' AS dotNetName, '(GMT-03:00) Buenos Aires' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 25 AS [order], 1 AS isEnabled
	UNION SELECT 26 AS idTimezone, 'SA Eastern Standard Time' AS dotNetName, '(GMT-03:00) Cayenne, Fortaleza' AS displayName, -3 AS gmtOffset, 0 AS blnUseDaylightSavings, 26 AS [order], 1 AS isEnabled
	UNION SELECT 27 AS idTimezone, 'Greenland Standard Time' AS dotNetName, '(GMT-03:00) Greenland' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 27 AS [order], 1 AS isEnabled
	UNION SELECT 28 AS idTimezone, 'Montevideo Standard Time' AS dotNetName, '(GMT-03:00) Montevideo' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 28 AS [order], 1 AS isEnabled
	UNION SELECT 29 AS idTimezone, 'Bahia Standard Time' AS dotNetName, '(GMT-03:00) Salvador' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 29 AS [order], 1 AS isEnabled
	UNION SELECT 30 AS idTimezone, 'UTC-02' AS dotNetName, '(GMT-02:00) Coordinated Universal Time-02' AS displayName, -2 AS gmtOffset, 0 AS blnUseDaylightSavings, 30 AS [order], 1 AS isEnabled
	UNION SELECT 31 AS idTimezone, 'Mid-Atlantic Standard Time' AS dotNetName, '(GMT-02:00) Mid-Atlantic' AS displayName, -2 AS gmtOffset, 1 AS blnUseDaylightSavings, 31 AS [order], 1 AS isEnabled
	UNION SELECT 32 AS idTimezone, 'Azores Standard Time' AS dotNetName, '(GMT-01:00) Azores' AS displayName, -1 AS gmtOffset, 1 AS blnUseDaylightSavings, 32 AS [order], 1 AS isEnabled
	UNION SELECT 33 AS idTimezone, 'Cape Verde Standard Time' AS dotNetName, '(GMT-01:00) Cape Verde Is.' AS displayName, -1 AS gmtOffset, 0 AS blnUseDaylightSavings, 33 AS [order], 1 AS isEnabled
	UNION SELECT 34 AS idTimezone, 'Morocco Standard Time' AS dotNetName, '(GMT) Casablanca' AS displayName, 0 AS gmtOffset, 1 AS blnUseDaylightSavings, 34 AS [order], 1 AS isEnabled
	UNION SELECT 35 AS idTimezone, 'UTC' AS dotNetName, '(GMT) Coordinated Universal Time' AS displayName, 0 AS gmtOffset, 0 AS blnUseDaylightSavings, 35 AS [order], 1 AS isEnabled
	UNION SELECT 36 AS idTimezone, 'GMT Standard Time' AS dotNetName, '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London' AS displayName, 0 AS gmtOffset, 1 AS blnUseDaylightSavings, 36 AS [order], 1 AS isEnabled
	UNION SELECT 37 AS idTimezone, 'Greenwich Standard Time' AS dotNetName, '(GMT) Monrovia, Reykjavik' AS displayName, 0 AS gmtOffset, 0 AS blnUseDaylightSavings, 37 AS [order], 1 AS isEnabled
	UNION SELECT 38 AS idTimezone, 'W. Europe Standard Time' AS dotNetName, '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 38 AS [order], 1 AS isEnabled
	UNION SELECT 39 AS idTimezone, 'Central Europe Standard Time' AS dotNetName, '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 39 AS [order], 1 AS isEnabled
	UNION SELECT 40 AS idTimezone, 'Romance Standard Time' AS dotNetName, '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 40 AS [order], 1 AS isEnabled
	UNION SELECT 41 AS idTimezone, 'Central European Standard Time' AS dotNetName, '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 41 AS [order], 1 AS isEnabled
	UNION SELECT 42 AS idTimezone, 'Libya Standard Time' AS dotNetName, '(GMT+01:00) Tripoli' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 42 AS [order], 1 AS isEnabled
	UNION SELECT 43 AS idTimezone, 'W. Central Africa Standard Time' AS dotNetName, '(GMT+01:00) West Central Africa' AS displayName, 1 AS gmtOffset, 0 AS blnUseDaylightSavings, 43 AS [order], 1 AS isEnabled
	UNION SELECT 44 AS idTimezone, 'Namibia Standard Time' AS dotNetName, '(GMT+01:00) Windhoek' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 44 AS [order], 1 AS isEnabled
	UNION SELECT 45 AS idTimezone, 'GTB Standard Time' AS dotNetName, '(GMT+02:00) Athens, Bucharest' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 45 AS [order], 1 AS isEnabled
	UNION SELECT 46 AS idTimezone, 'Middle East Standard Time' AS dotNetName, '(GMT+02:00) Beirut' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 46 AS [order], 1 AS isEnabled
	UNION SELECT 47 AS idTimezone, 'Egypt Standard Time' AS dotNetName, '(GMT+02:00) Cairo' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 47 AS [order], 1 AS isEnabled
	UNION SELECT 48 AS idTimezone, 'Syria Standard Time' AS dotNetName, '(GMT+02:00) Damascus' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 48 AS [order], 1 AS isEnabled
	UNION SELECT 49 AS idTimezone, 'E. Europe Standard Time' AS dotNetName, '(GMT+02:00) E. Europe' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 49 AS [order], 1 AS isEnabled
	UNION SELECT 50 AS idTimezone, 'South Africa Standard Time' AS dotNetName, '(GMT+02:00) Harare, Pretoria' AS displayName, 2 AS gmtOffset, 0 AS blnUseDaylightSavings, 50 AS [order], 1 AS isEnabled
	UNION SELECT 51 AS idTimezone, 'FLE Standard Time' AS dotNetName, '(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 51 AS [order], 1 AS isEnabled
	UNION SELECT 52 AS idTimezone, 'Turkey Standard Time' AS dotNetName, '(GMT+02:00) Istanbul' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 52 AS [order], 1 AS isEnabled
	UNION SELECT 53 AS idTimezone, 'Israel Standard Time' AS dotNetName, '(GMT+02:00) Jerusalem' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 53 AS [order], 1 AS isEnabled
	UNION SELECT 54 AS idTimezone, 'Jordan Standard Time' AS dotNetName, '(GMT+03:00) Amman' AS displayName, 3 AS gmtOffset, 1 AS blnUseDaylightSavings, 54 AS [order], 1 AS isEnabled
	UNION SELECT 55 AS idTimezone, 'Arabic Standard Time' AS dotNetName, '(GMT+03:00) Baghdad' AS displayName, 3 AS gmtOffset, 1 AS blnUseDaylightSavings, 55 AS [order], 1 AS isEnabled
	UNION SELECT 56 AS idTimezone, 'Kaliningrad Standard Time' AS dotNetName, '(GMT+03:00) Kaliningrad, Minsk' AS displayName, 3 AS gmtOffset, 1 AS blnUseDaylightSavings, 56 AS [order], 1 AS isEnabled
	UNION SELECT 57 AS idTimezone, 'Arab Standard Time' AS dotNetName, '(GMT+03:00) Kuwait, Riyadh' AS displayName, 3 AS gmtOffset, 0 AS blnUseDaylightSavings, 57 AS [order], 1 AS isEnabled
	UNION SELECT 58 AS idTimezone, 'E. Africa Standard Time' AS dotNetName, '(GMT+03:00) Nairobi' AS displayName, 3 AS gmtOffset, 0 AS blnUseDaylightSavings, 58 AS [order], 1 AS isEnabled
	UNION SELECT 59 AS idTimezone, 'Iran Standard Time' AS dotNetName, '(GMT+03:30) Tehran' AS displayName, 3.5 AS gmtOffset, 1 AS blnUseDaylightSavings, 59 AS [order], 1 AS isEnabled
	UNION SELECT 60 AS idTimezone, 'Arabian Standard Time' AS dotNetName, '(GMT+04:00) Abu Dhabi, Muscat' AS displayName, 4 AS gmtOffset, 0 AS blnUseDaylightSavings, 60 AS [order], 1 AS isEnabled
	UNION SELECT 61 AS idTimezone, 'Azerbaijan Standard Time' AS dotNetName, '(GMT+04:00) Baku' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 61 AS [order], 1 AS isEnabled
	UNION SELECT 62 AS idTimezone, 'Russian Standard Time' AS dotNetName, '(GMT+04:00) Moscow, St. Petersburg, Volgograd' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 62 AS [order], 1 AS isEnabled
	UNION SELECT 63 AS idTimezone, 'Mauritius Standard Time' AS dotNetName, '(GMT+04:00) Port Louis' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 63 AS [order], 1 AS isEnabled
	UNION SELECT 64 AS idTimezone, 'Georgian Standard Time' AS dotNetName, '(GMT+04:00) Tbilisi' AS displayName, 4 AS gmtOffset, 0 AS blnUseDaylightSavings, 64 AS [order], 1 AS isEnabled
	UNION SELECT 65 AS idTimezone, 'Caucasus Standard Time' AS dotNetName, '(GMT+04:00) Yerevan' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 65 AS [order], 1 AS isEnabled
	UNION SELECT 66 AS idTimezone, 'Afghanistan Standard Time' AS dotNetName, '(GMT+04:30) Kabul' AS displayName, 4.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 66 AS [order], 1 AS isEnabled
	UNION SELECT 67 AS idTimezone, 'West Asia Standard Time' AS dotNetName, '(GMT+05:00) Ashgabat, Tashkent' AS displayName, 5 AS gmtOffset, 0 AS blnUseDaylightSavings, 67 AS [order], 1 AS isEnabled
	UNION SELECT 68 AS idTimezone, 'Pakistan Standard Time' AS dotNetName, '(GMT+05:00) Islamabad, Karachi' AS displayName, 5 AS gmtOffset, 1 AS blnUseDaylightSavings, 68 AS [order], 1 AS isEnabled
	UNION SELECT 69 AS idTimezone, 'India Standard Time' AS dotNetName, '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi' AS displayName, 5.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 69 AS [order], 1 AS isEnabled
	UNION SELECT 70 AS idTimezone, 'Sri Lanka Standard Time' AS dotNetName, '(GMT+05:30) Sri Jayawardenepura' AS displayName, 5.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 70 AS [order], 1 AS isEnabled
	UNION SELECT 71 AS idTimezone, 'Nepal Standard Time' AS dotNetName, '(GMT+05:45) Kathmandu' AS displayName, 5.75 AS gmtOffset, 0 AS blnUseDaylightSavings, 71 AS [order], 1 AS isEnabled
	UNION SELECT 72 AS idTimezone, 'Central Asia Standard Time' AS dotNetName, '(GMT+06:00) Astana' AS displayName, 6 AS gmtOffset, 0 AS blnUseDaylightSavings, 72 AS [order], 1 AS isEnabled
	UNION SELECT 73 AS idTimezone, 'Bangladesh Standard Time' AS dotNetName, '(GMT+06:00) Dhaka' AS displayName, 6 AS gmtOffset, 1 AS blnUseDaylightSavings, 73 AS [order], 1 AS isEnabled
	UNION SELECT 74 AS idTimezone, 'Ekaterinburg Standard Time' AS dotNetName, '(GMT+06:00) Ekaterinburg' AS displayName, 6 AS gmtOffset, 1 AS blnUseDaylightSavings, 74 AS [order], 1 AS isEnabled
	UNION SELECT 75 AS idTimezone, 'Myanmar Standard Time' AS dotNetName, '(GMT+06:30) Yangon (Rangoon)' AS displayName, 6.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 75 AS [order], 1 AS isEnabled
	UNION SELECT 76 AS idTimezone, 'SE Asia Standard Time' AS dotNetName, '(GMT+07:00) Bangkok, Hanoi, Jakarta' AS displayName, 7 AS gmtOffset, 0 AS blnUseDaylightSavings, 76 AS [order], 1 AS isEnabled
	UNION SELECT 77 AS idTimezone, 'N. Central Asia Standard Time' AS dotNetName, '(GMT+07:00) Novosibirsk' AS displayName, 7 AS gmtOffset, 1 AS blnUseDaylightSavings, 77 AS [order], 1 AS isEnabled
	UNION SELECT 78 AS idTimezone, 'China Standard Time' AS dotNetName, '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 78 AS [order], 1 AS isEnabled
	UNION SELECT 79 AS idTimezone, 'North Asia Standard Time' AS dotNetName, '(GMT+08:00) Krasnoyarsk' AS displayName, 8 AS gmtOffset, 1 AS blnUseDaylightSavings, 79 AS [order], 1 AS isEnabled
	UNION SELECT 80 AS idTimezone, 'Singapore Standard Time' AS dotNetName, '(GMT+08:00) Kuala Lumpur, Singapore' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 80 AS [order], 1 AS isEnabled
	UNION SELECT 81 AS idTimezone, 'W. Australia Standard Time' AS dotNetName, '(GMT+08:00) Perth' AS displayName, 8 AS gmtOffset, 1 AS blnUseDaylightSavings, 81 AS [order], 1 AS isEnabled
	UNION SELECT 82 AS idTimezone, 'Taipei Standard Time' AS dotNetName, '(GMT+08:00) Taipei' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 82 AS [order], 1 AS isEnabled
	UNION SELECT 83 AS idTimezone, 'Ulaanbaatar Standard Time' AS dotNetName, '(GMT+08:00) Ulaanbaatar' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 83 AS [order], 1 AS isEnabled
	UNION SELECT 84 AS idTimezone, 'North Asia East Standard Time' AS dotNetName, '(GMT+09:00) Irkutsk' AS displayName, 9 AS gmtOffset, 1 AS blnUseDaylightSavings, 84 AS [order], 1 AS isEnabled
	UNION SELECT 85 AS idTimezone, 'Tokyo Standard Time' AS dotNetName, '(GMT+09:00) Osaka, Sapporo, Tokyo' AS displayName, 9 AS gmtOffset, 0 AS blnUseDaylightSavings, 85 AS [order], 1 AS isEnabled
	UNION SELECT 86 AS idTimezone, 'Korea Standard Time' AS dotNetName, '(GMT+09:00) Seoul' AS displayName, 9 AS gmtOffset, 0 AS blnUseDaylightSavings, 86 AS [order], 1 AS isEnabled
	UNION SELECT 87 AS idTimezone, 'Cen. Australia Standard Time' AS dotNetName, '(GMT+09:30) Adelaide' AS displayName, 9.5 AS gmtOffset, 1 AS blnUseDaylightSavings, 87 AS [order], 1 AS isEnabled
	UNION SELECT 88 AS idTimezone, 'AUS Central Standard Time' AS dotNetName, '(GMT+09:30) Darwin' AS displayName, 9.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 88 AS [order], 1 AS isEnabled
	UNION SELECT 89 AS idTimezone, 'E. Australia Standard Time' AS dotNetName, '(GMT+10:00) Brisbane' AS displayName, 10 AS gmtOffset, 0 AS blnUseDaylightSavings, 89 AS [order], 1 AS isEnabled
	UNION SELECT 90 AS idTimezone, 'AUS Eastern Standard Time' AS dotNetName, '(GMT+10:00) Canberra, Melbourne, Sydney' AS displayName, 10 AS gmtOffset, 1 AS blnUseDaylightSavings, 90 AS [order], 1 AS isEnabled
	UNION SELECT 91 AS idTimezone, 'West Pacific Standard Time' AS dotNetName, '(GMT+10:00) Guam, Port Moresby' AS displayName, 10 AS gmtOffset, 0 AS blnUseDaylightSavings, 91 AS [order], 1 AS isEnabled
	UNION SELECT 92 AS idTimezone, 'Tasmania Standard Time' AS dotNetName, '(GMT+10:00) Hobart' AS displayName, 10 AS gmtOffset, 1 AS blnUseDaylightSavings, 92 AS [order], 1 AS isEnabled
	UNION SELECT 93 AS idTimezone, 'Yakutsk Standard Time' AS dotNetName, '(GMT+10:00) Yakutsk' AS displayName, 10 AS gmtOffset, 1 AS blnUseDaylightSavings, 93 AS [order], 1 AS isEnabled
	UNION SELECT 94 AS idTimezone, 'Central Pacific Standard Time' AS dotNetName, '(GMT+11:00) Solomon Is., New Caledonia' AS displayName, 11 AS gmtOffset, 0 AS blnUseDaylightSavings, 94 AS [order], 1 AS isEnabled
	UNION SELECT 95 AS idTimezone, 'Vladivostok Standard Time' AS dotNetName, '(GMT+11:00) Vladivostok' AS displayName, 11 AS gmtOffset, 1 AS blnUseDaylightSavings, 95 AS [order], 1 AS isEnabled
	UNION SELECT 96 AS idTimezone, 'New Zealand Standard Time' AS dotNetName, '(GMT+12:00) Auckland, Wellington' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 96 AS [order], 1 AS isEnabled
	UNION SELECT 97 AS idTimezone, 'UTC+12' AS dotNetName, '(GMT+12:00) Coordinated Universal Time+12' AS displayName, 12 AS gmtOffset, 0 AS blnUseDaylightSavings, 97 AS [order], 1 AS isEnabled
	UNION SELECT 98 AS idTimezone, 'Fiji Standard Time' AS dotNetName, '(GMT+12:00) Fiji' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 98 AS [order], 1 AS isEnabled
	UNION SELECT 99 AS idTimezone, 'Magadan Standard Time' AS dotNetName, '(GMT+12:00) Magadan' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 99 AS [order], 1 AS isEnabled
	UNION SELECT 100 AS idTimezone, 'Kamchatka Standard Time' AS dotNetName, '(GMT+12:00) Petropavlovsk-Kamchatsky - Old' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 100 AS [order], 1 AS isEnabled
	UNION SELECT 101 AS idTimezone, 'Tonga Standard Time' AS dotNetName, '(GMT+13:00) Nuku''alofa' AS displayName, 13 AS gmtOffset, 0 AS blnUseDaylightSavings, 101 AS [order], 1 AS isEnabled
	UNION SELECT 102 AS idTimezone, 'Samoa Standard Time' AS dotNetName, '(GMT+13:00) Samoa' AS displayName, 13 AS gmtOffset, 1 AS blnUseDaylightSavings, 102 AS [order], 1 AS isEnabled

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblTimezone TZ WHERE TZ.idTimezone = MAIN.idTimezone)

SET IDENTITY_INSERT tblTimezone OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLanguage] (
	[idLanguage]		INT				IDENTITY (1, 1)		NOT NULL, 
	[code]				NVARCHAR(20)						NOT NULL,
	[name]				NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLanguage ADD CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED (idLanguage ASC)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblLanguage] ON

INSERT INTO [tblLanguage] (
	[idLanguage], 
	[code],
	[name]
)
SELECT
	idLanguage,
	code,
	name
FROM (
	SELECT 0 AS idLanguage,'af-ZA' AS code,'Afrikaans (South Africa)' AS name
	UNION SELECT 1 AS idLanguage,'am-ET' AS code,'Amharic (Ethiopia)' AS name
	UNION SELECT 2 AS idLanguage,'ar-AE' AS code,'Arabic (U.A.E.)' AS name
	UNION SELECT 3 AS idLanguage,'ar-BH' AS code,'Arabic (Bahrain)' AS name
	UNION SELECT 4 AS idLanguage,'ar-DZ' AS code,'Arabic (Algeria)' AS name
	UNION SELECT 5 AS idLanguage,'ar-EG' AS code,'Arabic (Egypt)' AS name
	UNION SELECT 6 AS idLanguage,'ar-IQ' AS code,'Arabic (Iraq)' AS name
	UNION SELECT 7 AS idLanguage,'ar-JO' AS code,'Arabic (Jordan)' AS name
	UNION SELECT 8 AS idLanguage,'ar-KW' AS code,'Arabic (Kuwait)' AS name
	UNION SELECT 9 AS idLanguage,'ar-LB' AS code,'Arabic (Lebanon)' AS name
	UNION SELECT 10 AS idLanguage,'ar-LY' AS code,'Arabic (Libya)' AS name
	UNION SELECT 11 AS idLanguage,'ar-MA' AS code,'Arabic (Morocco)' AS name
	UNION SELECT 12 AS idLanguage,'ar-OM' AS code,'Arabic (Oman)' AS name
	UNION SELECT 13 AS idLanguage,'ar-QA' AS code,'Arabic (Qatar)' AS name
	UNION SELECT 14 AS idLanguage,'ar-SA' AS code,'Arabic (Saudi Arabia)' AS name
	UNION SELECT 15 AS idLanguage,'ar-SY' AS code,'Arabic (Syria)' AS name
	UNION SELECT 16 AS idLanguage,'ar-TN' AS code,'Arabic (Tunisia)' AS name
	UNION SELECT 17 AS idLanguage,'ar-YE' AS code,'Arabic (Yemen)' AS name
	UNION SELECT 18 AS idLanguage,'arn-CL' AS code,'Mapudungun (Chile)' AS name
	UNION SELECT 19 AS idLanguage,'as-IN' AS code,'Assamese (India)' AS name
	UNION SELECT 20 AS idLanguage,'az-Cyrl-AZ' AS code,'Azeri (Cyrillic, Azerbaijan)' AS name
	UNION SELECT 21 AS idLanguage,'az-Latn-AZ' AS code,'Azeri (Latin, Azerbaijan)' AS name
	UNION SELECT 22 AS idLanguage,'ba-RU' AS code,'Bashkir (Russia)' AS name
	UNION SELECT 23 AS idLanguage,'be-BY' AS code,'Belarusian (Belarus)' AS name
	UNION SELECT 24 AS idLanguage,'bg-BG' AS code,'Bulgarian (Bulgaria)' AS name
	UNION SELECT 25 AS idLanguage,'bn-BD' AS code,'Bengali (Bangladesh)' AS name
	UNION SELECT 26 AS idLanguage,'bn-IN' AS code,'Bengali (India)' AS name
	UNION SELECT 27 AS idLanguage,'bo-CN' AS code,'Tibetan (PRC)' AS name
	UNION SELECT 28 AS idLanguage,'br-FR' AS code,'Breton (France)' AS name
	UNION SELECT 29 AS idLanguage,'bs-Cyrl-BA' AS code,'Bosnian (Cyrillic, Bosnia and Herzegovina)' AS name
	UNION SELECT 30 AS idLanguage,'bs-Latn-BA' AS code,'Bosnian (Latin, Bosnia and Herzegovina)' AS name
	UNION SELECT 31 AS idLanguage,'ca-ES' AS code,'Catalan (Catalan)' AS name
	UNION SELECT 32 AS idLanguage,'co-FR' AS code,'Corsican (France)' AS name
	UNION SELECT 33 AS idLanguage,'cs-CZ' AS code,'Czech (Czech Republic)' AS name
	UNION SELECT 34 AS idLanguage,'cy-GB' AS code,'Welsh (United Kingdom)' AS name
	UNION SELECT 35 AS idLanguage,'da-DK' AS code,'Danish (Denmark)' AS name
	UNION SELECT 36 AS idLanguage,'de-AT' AS code,'German (Austria)' AS name
	UNION SELECT 37 AS idLanguage,'de-CH' AS code,'German (Switzerland)' AS name
	UNION SELECT 38 AS idLanguage,'de-DE' AS code,'German (Germany)' AS name
	UNION SELECT 39 AS idLanguage,'de-LI' AS code,'German (Liechtenstein)' AS name
	UNION SELECT 40 AS idLanguage,'de-LU' AS code,'German (Luxembourg)' AS name
	UNION SELECT 41 AS idLanguage,'dsb-DE' AS code,'Lower Sorbian (Germany)' AS name
	UNION SELECT 42 AS idLanguage,'dv-MV' AS code,'Divehi (Maldives)' AS name
	UNION SELECT 43 AS idLanguage,'el-GR' AS code,'Greek (Greece)' AS name
	UNION SELECT 44 AS idLanguage,'en-029' AS code,'English (Caribbean)' AS name
	UNION SELECT 45 AS idLanguage,'en-AU' AS code,'English (Australia)' AS name
	UNION SELECT 46 AS idLanguage,'en-BZ' AS code,'English (Belize)' AS name
	UNION SELECT 47 AS idLanguage,'en-CA' AS code,'English (Canada)' AS name
	UNION SELECT 48 AS idLanguage,'en-GB' AS code,'English (United Kingdom)' AS name
	UNION SELECT 49 AS idLanguage,'en-IE' AS code,'English (Ireland)' AS name
	UNION SELECT 50 AS idLanguage,'en-IN' AS code,'English (India)' AS name
	UNION SELECT 51 AS idLanguage,'en-JM' AS code,'English (Jamaica)' AS name
	UNION SELECT 52 AS idLanguage,'en-MY' AS code,'English (Malaysia)' AS name
	UNION SELECT 53 AS idLanguage,'en-NZ' AS code,'English (New Zealand)' AS name
	UNION SELECT 54 AS idLanguage,'en-PH' AS code,'English (Republic of the Philippines)' AS name
	UNION SELECT 55 AS idLanguage,'en-SG' AS code,'English (Singapore)' AS name
	UNION SELECT 56 AS idLanguage,'en-TT' AS code,'English (Trinidad and Tobago)' AS name
	UNION SELECT 57 AS idLanguage,'en-US' AS code,'English (United States)' AS name
	UNION SELECT 58 AS idLanguage,'en-ZA' AS code,'English (South Africa)' AS name
	UNION SELECT 59 AS idLanguage,'en-ZW' AS code,'English (Zimbabwe)' AS name
	UNION SELECT 60 AS idLanguage,'es-AR' AS code,'Spanish (Argentina)' AS name
	UNION SELECT 61 AS idLanguage,'es-BO' AS code,'Spanish (Bolivia)' AS name
	UNION SELECT 62 AS idLanguage,'es-CL' AS code,'Spanish (Chile)' AS name
	UNION SELECT 63 AS idLanguage,'es-CO' AS code,'Spanish (Colombia)' AS name
	UNION SELECT 64 AS idLanguage,'es-CR' AS code,'Spanish (Costa Rica)' AS name
	UNION SELECT 65 AS idLanguage,'es-DO' AS code,'Spanish (Dominican Republic)' AS name
	UNION SELECT 66 AS idLanguage,'es-EC' AS code,'Spanish (Ecuador)' AS name
	UNION SELECT 67 AS idLanguage,'es-ES' AS code,'Spanish (Spain, International Sort)' AS name
	UNION SELECT 68 AS idLanguage,'es-GT' AS code,'Spanish (Guatemala)' AS name
	UNION SELECT 69 AS idLanguage,'es-HN' AS code,'Spanish (Honduras)' AS name
	UNION SELECT 70 AS idLanguage,'es-MX' AS code,'Spanish (Mexico)' AS name
	UNION SELECT 71 AS idLanguage,'es-NI' AS code,'Spanish (Nicaragua)' AS name
	UNION SELECT 72 AS idLanguage,'es-PA' AS code,'Spanish (Panama)' AS name
	UNION SELECT 73 AS idLanguage,'es-PE' AS code,'Spanish (Peru)' AS name
	UNION SELECT 74 AS idLanguage,'es-PR' AS code,'Spanish (Puerto Rico)' AS name
	UNION SELECT 75 AS idLanguage,'es-PY' AS code,'Spanish (Paraguay)' AS name
	UNION SELECT 76 AS idLanguage,'es-SV' AS code,'Spanish (El Salvador)' AS name
	UNION SELECT 77 AS idLanguage,'es-US' AS code,'Spanish (United States)' AS name
	UNION SELECT 78 AS idLanguage,'es-UY' AS code,'Spanish (Uruguay)' AS name
	UNION SELECT 79 AS idLanguage,'es-VE' AS code,'Spanish (Bolivarian Republic of Venezuela)' AS name
	UNION SELECT 80 AS idLanguage,'et-EE' AS code,'Estonian (Estonia)' AS name
	UNION SELECT 81 AS idLanguage,'eu-ES' AS code,'Basque (Basque)' AS name
	UNION SELECT 82 AS idLanguage,'fa-IR' AS code,'Persian' AS name
	UNION SELECT 83 AS idLanguage,'fi-FI' AS code,'Finnish (Finland)' AS name
	UNION SELECT 84 AS idLanguage,'fil-PH' AS code,'Filipino (Philippines)' AS name
	UNION SELECT 85 AS idLanguage,'fo-FO' AS code,'Faroese (Faroe Islands)' AS name
	UNION SELECT 86 AS idLanguage,'fr-BE' AS code,'French (Belgium)' AS name
	UNION SELECT 87 AS idLanguage,'fr-CA' AS code,'French (Canada)' AS name
	UNION SELECT 88 AS idLanguage,'fr-CH' AS code,'French (Switzerland)' AS name
	UNION SELECT 89 AS idLanguage,'fr-FR' AS code,'French (France)' AS name
	UNION SELECT 90 AS idLanguage,'fr-LU' AS code,'French (Luxembourg)' AS name
	UNION SELECT 91 AS idLanguage,'fr-MC' AS code,'French (Monaco)' AS name
	UNION SELECT 92 AS idLanguage,'fy-NL' AS code,'Frisian (Netherlands)' AS name
	UNION SELECT 93 AS idLanguage,'ga-IE' AS code,'Irish (Ireland)' AS name
	UNION SELECT 94 AS idLanguage,'gd-GB' AS code,'Scottish Gaelic (United Kingdom)' AS name
	UNION SELECT 95 AS idLanguage,'gl-ES' AS code,'Galician (Galician)' AS name
	UNION SELECT 96 AS idLanguage,'gsw-FR' AS code,'Alsatian (France)' AS name
	UNION SELECT 97 AS idLanguage,'gu-IN' AS code,'Gujarati (India)' AS name
	UNION SELECT 98 AS idLanguage,'ha-Latn-NG' AS code,'Hausa (Latin, Nigeria)' AS name
	UNION SELECT 99 AS idLanguage,'he-IL' AS code,'Hebrew (Israel)' AS name
	UNION SELECT 100 AS idLanguage,'hi-IN' AS code,'Hindi (India)' AS name
	UNION SELECT 101 AS idLanguage,'hr-BA' AS code,'Croatian (Latin, Bosnia and Herzegovina)' AS name
	UNION SELECT 102 AS idLanguage,'hr-HR' AS code,'Croatian (Croatia)' AS name
	UNION SELECT 103 AS idLanguage,'hsb-DE' AS code,'Upper Sorbian (Germany)' AS name
	UNION SELECT 104 AS idLanguage,'hu-HU' AS code,'Hungarian (Hungary)' AS name
	UNION SELECT 105 AS idLanguage,'hy-AM' AS code,'Armenian (Armenia)' AS name
	UNION SELECT 106 AS idLanguage,'id-ID' AS code,'Indonesian (Indonesia)' AS name
	UNION SELECT 107 AS idLanguage,'ig-NG' AS code,'Igbo (Nigeria)' AS name
	UNION SELECT 108 AS idLanguage,'ii-CN' AS code,'Yi (PRC)' AS name
	UNION SELECT 109 AS idLanguage,'is-IS' AS code,'Icelandic (Iceland)' AS name
	UNION SELECT 110 AS idLanguage,'it-CH' AS code,'Italian (Switzerland)' AS name
	UNION SELECT 111 AS idLanguage,'it-IT' AS code,'Italian (Italy)' AS name
	UNION SELECT 112 AS idLanguage,'iu-Cans-CA' AS code,'Inuktitut (Syllabics, Canada)' AS name
	UNION SELECT 113 AS idLanguage,'iu-Latn-CA' AS code,'Inuktitut (Latin, Canada)' AS name
	UNION SELECT 114 AS idLanguage,'ja-JP' AS code,'Japanese (Japan)' AS name
	UNION SELECT 115 AS idLanguage,'ka-GE' AS code,'Georgian (Georgia)' AS name
	UNION SELECT 116 AS idLanguage,'kk-KZ' AS code,'Kazakh (Kazakhstan)' AS name
	UNION SELECT 117 AS idLanguage,'kl-GL' AS code,'Greenlandic (Greenland)' AS name
	UNION SELECT 118 AS idLanguage,'km-KH' AS code,'Khmer (Cambodia)' AS name
	UNION SELECT 119 AS idLanguage,'kn-IN' AS code,'Kannada (India)' AS name
	UNION SELECT 120 AS idLanguage,'ko-KR' AS code,'Korean (Korea)' AS name
	UNION SELECT 121 AS idLanguage,'kok-IN' AS code,'Konkani (India)' AS name
	UNION SELECT 122 AS idLanguage,'ky-KG' AS code,'Kyrgyz (Kyrgyzstan)' AS name
	UNION SELECT 123 AS idLanguage,'lb-LU' AS code,'Luxembourgish (Luxembourg)' AS name
	UNION SELECT 124 AS idLanguage,'lo-LA' AS code,'Lao (Lao P.D.R.)' AS name
	UNION SELECT 125 AS idLanguage,'lt-LT' AS code,'Lithuanian (Lithuania)' AS name
	UNION SELECT 126 AS idLanguage,'lv-LV' AS code,'Latvian (Latvia)' AS name
	UNION SELECT 127 AS idLanguage,'mi-NZ' AS code,'Maori (New Zealand)' AS name
	UNION SELECT 128 AS idLanguage,'mk-MK' AS code,'Macedonian (Former Yugoslav Republic of Macedonia)' AS name
	UNION SELECT 129 AS idLanguage,'ml-IN' AS code,'Malayalam (India)' AS name
	UNION SELECT 130 AS idLanguage,'mn-MN' AS code,'Mongolian (Cyrillic, Mongolia)' AS name
	UNION SELECT 131 AS idLanguage,'mn-Mong-CN' AS code,'Mongolian (Traditional Mongolian, PRC)' AS name
	UNION SELECT 132 AS idLanguage,'moh-CA' AS code,'Mohawk (Mohawk)' AS name
	UNION SELECT 133 AS idLanguage,'mr-IN' AS code,'Marathi (India)' AS name
	UNION SELECT 134 AS idLanguage,'ms-BN' AS code,'Malay (Brunei Darussalam)' AS name
	UNION SELECT 135 AS idLanguage,'ms-MY' AS code,'Malay (Malaysia)' AS name
	UNION SELECT 136 AS idLanguage,'mt-MT' AS code,'Maltese (Malta)' AS name
	UNION SELECT 137 AS idLanguage,'nb-NO' AS code,'Norwegian, Bokm�l (Norway)' AS name
	UNION SELECT 138 AS idLanguage,'ne-NP' AS code,'Nepali (Nepal)' AS name
	UNION SELECT 139 AS idLanguage,'nl-BE' AS code,'Dutch (Belgium)' AS name
	UNION SELECT 140 AS idLanguage,'nl-NL' AS code,'Dutch (Netherlands)' AS name
	UNION SELECT 141 AS idLanguage,'nn-NO' AS code,'Norwegian, Nynorsk (Norway)' AS name
	UNION SELECT 142 AS idLanguage,'nso-ZA' AS code,'Sesotho sa Leboa (South Africa)' AS name
	UNION SELECT 143 AS idLanguage,'oc-FR' AS code,'Occitan (France)' AS name
	UNION SELECT 144 AS idLanguage,'or-IN' AS code,'Oriya (India)' AS name
	UNION SELECT 145 AS idLanguage,'pa-IN' AS code,'Punjabi (India)' AS name
	UNION SELECT 146 AS idLanguage,'pl-PL' AS code,'Polish (Poland)' AS name
	UNION SELECT 147 AS idLanguage,'prs-AF' AS code,'Dari (Afghanistan)' AS name
	UNION SELECT 148 AS idLanguage,'ps-AF' AS code,'Pashto (Afghanistan)' AS name
	UNION SELECT 149 AS idLanguage,'pt-BR' AS code,'Portuguese (Brazil)' AS name
	UNION SELECT 150 AS idLanguage,'pt-PT' AS code,'Portuguese (Portugal)' AS name
	UNION SELECT 151 AS idLanguage,'qut-GT' AS code,'K''iche (Guatemala)' AS name
	UNION SELECT 152 AS idLanguage,'quz-BO' AS code,'Quechua (Bolivia)' AS name
	UNION SELECT 153 AS idLanguage,'quz-EC' AS code,'Quechua (Ecuador)' AS name
	UNION SELECT 154 AS idLanguage,'quz-PE' AS code,'Quechua (Peru)' AS name
	UNION SELECT 155 AS idLanguage,'rm-CH' AS code,'Romansh (Switzerland)' AS name
	UNION SELECT 156 AS idLanguage,'ro-RO' AS code,'Romanian (Romania)' AS name
	UNION SELECT 157 AS idLanguage,'ru-RU' AS code,'Russian (Russia)' AS name
	UNION SELECT 158 AS idLanguage,'rw-RW' AS code,'Kinyarwanda (Rwanda)' AS name
	UNION SELECT 159 AS idLanguage,'sa-IN' AS code,'Sanskrit (India)' AS name
	UNION SELECT 160 AS idLanguage,'sah-RU' AS code,'Yakut (Russia)' AS name
	UNION SELECT 161 AS idLanguage,'se-FI' AS code,'Sami, Northern (Finland)' AS name
	UNION SELECT 162 AS idLanguage,'se-NO' AS code,'Sami, Northern (Norway)' AS name
	UNION SELECT 163 AS idLanguage,'se-SE' AS code,'Sami, Northern (Sweden)' AS name
	UNION SELECT 164 AS idLanguage,'si-LK' AS code,'Sinhala (Sri Lanka)' AS name
	UNION SELECT 165 AS idLanguage,'sk-SK' AS code,'Slovak (Slovakia)' AS name
	UNION SELECT 166 AS idLanguage,'sl-SI' AS code,'Slovenian (Slovenia)' AS name
	UNION SELECT 167 AS idLanguage,'sma-NO' AS code,'Sami, Southern (Norway)' AS name
	UNION SELECT 168 AS idLanguage,'sma-SE' AS code,'Sami, Southern (Sweden)' AS name
	UNION SELECT 169 AS idLanguage,'smj-NO' AS code,'Sami, Lule (Norway)' AS name
	UNION SELECT 170 AS idLanguage,'smj-SE' AS code,'Sami, Lule (Sweden)' AS name
	UNION SELECT 171 AS idLanguage,'smn-FI' AS code,'Sami, Inari (Finland)' AS name
	UNION SELECT 172 AS idLanguage,'sms-FI' AS code,'Sami, Skolt (Finland)' AS name
	UNION SELECT 173 AS idLanguage,'sq-AL' AS code,'Albanian (Albania)' AS name
	UNION SELECT 174 AS idLanguage,'sr-Cyrl-BA' AS code,'Serbian (Cyrillic, Bosnia and Herzegovina)' AS name
	UNION SELECT 175 AS idLanguage,'sr-Cyrl-CS' AS code,'Serbian (Cyrillic, Serbia and Montenegro (Former))' AS name
	UNION SELECT 176 AS idLanguage,'sr-Cyrl-ME' AS code,'Serbian (Cyrillic, Montenegro)' AS name
	UNION SELECT 177 AS idLanguage,'sr-Cyrl-RS' AS code,'Serbian (Cyrillic, Serbia)' AS name
	UNION SELECT 178 AS idLanguage,'sr-Latn-BA' AS code,'Serbian (Latin, Bosnia and Herzegovina)' AS name
	UNION SELECT 179 AS idLanguage,'sr-Latn-CS' AS code,'Serbian (Latin, Serbia and Montenegro (Former))' AS name
	UNION SELECT 180 AS idLanguage,'sr-Latn-ME' AS code,'Serbian (Latin, Montenegro)' AS name
	UNION SELECT 181 AS idLanguage,'sr-Latn-RS' AS code,'Serbian (Latin, Serbia)' AS name
	UNION SELECT 182 AS idLanguage,'sv-FI' AS code,'Swedish (Finland)' AS name
	UNION SELECT 183 AS idLanguage,'sv-SE' AS code,'Swedish (Sweden)' AS name
	UNION SELECT 184 AS idLanguage,'sw-KE' AS code,'Kiswahili (Kenya)' AS name
	UNION SELECT 185 AS idLanguage,'syr-SY' AS code,'Syriac (Syria)' AS name
	UNION SELECT 186 AS idLanguage,'ta-IN' AS code,'Tamil (India)' AS name
	UNION SELECT 187 AS idLanguage,'te-IN' AS code,'Telugu (India)' AS name
	UNION SELECT 188 AS idLanguage,'tg-Cyrl-TJ' AS code,'Tajik (Cyrillic, Tajikistan)' AS name
	UNION SELECT 189 AS idLanguage,'th-TH' AS code,'Thai (Thailand)' AS name
	UNION SELECT 190 AS idLanguage,'tk-TM' AS code,'Turkmen (Turkmenistan)' AS name
	UNION SELECT 191 AS idLanguage,'tn-ZA' AS code,'Setswana (South Africa)' AS name
	UNION SELECT 192 AS idLanguage,'tr-TR' AS code,'Turkish (Turkey)' AS name
	UNION SELECT 193 AS idLanguage,'tt-RU' AS code,'Tatar (Russia)' AS name
	UNION SELECT 194 AS idLanguage,'tzm-Latn-DZ' AS code,'Tamazight (Latin, Algeria)' AS name
	UNION SELECT 195 AS idLanguage,'ug-CN' AS code,'Uyghur (PRC)' AS name
	UNION SELECT 196 AS idLanguage,'uk-UA' AS code,'Ukrainian (Ukraine)' AS name
	UNION SELECT 197 AS idLanguage,'ur-PK' AS code,'Urdu (Islamic Republic of Pakistan)' AS name
	UNION SELECT 198 AS idLanguage,'uz-Cyrl-UZ' AS code,'Uzbek (Cyrillic, Uzbekistan)' AS name
	UNION SELECT 199 AS idLanguage,'uz-Latn-UZ' AS code,'Uzbek (Latin, Uzbekistan)' AS name
	UNION SELECT 200 AS idLanguage,'vi-VN' AS code,'Vietnamese (Vietnam)' AS name
	UNION SELECT 201 AS idLanguage,'wo-SN' AS code,'Wolof (Senegal)' AS name
	UNION SELECT 202 AS idLanguage,'xh-ZA' AS code,'isiXhosa (South Africa)' AS name
	UNION SELECT 203 AS idLanguage,'yo-NG' AS code,'Yoruba (Nigeria)' AS name
	UNION SELECT 204 AS idLanguage,'zh-CN' AS code,'Chinese (Simplified, PRC)' AS name
	UNION SELECT 205 AS idLanguage,'zh-HK' AS code,'Chinese (Traditional, Hong Kong S.A.R.)' AS name
	UNION SELECT 206 AS idLanguage,'zh-MO' AS code,'Chinese (Traditional, Macao S.A.R.)' AS name
	UNION SELECT 207 AS idLanguage,'zh-SG' AS code,'Chinese (Simplified, Singapore)' AS name
	UNION SELECT 208 AS idLanguage,'zh-TW' AS code,'Chinese (Traditional, Taiwan)' AS name
	UNION SELECT 209 AS idLanguage,'zu-ZA' AS code,'isiZulu (South Africa)' AS name

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblLanguage L WHERE L.idLanguage = MAIN.idLanguage)

SET IDENTITY_INSERT [tblLanguage] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTimezoneLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTimezoneLanguage] (
	[idTimezoneLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idTimezone]					INT								NULL,
	[idLanguage]					INT								NULL,
	[displayName]					NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_TimezoneLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezoneLanguage ADD CONSTRAINT [PK_TimezoneLanguage] PRIMARY KEY CLUSTERED (idTimezoneLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TimezoneLanguage_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezoneLanguage ADD CONSTRAINT [FK_TimezoneLanguage_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TimezoneLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezoneLanguage ADD CONSTRAINT [FK_TimezoneLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
INSERT VALUES
**/

--ENGLISH
INSERT INTO tblTimezoneLanguage (
	idTimezone,
	idLanguage,
	displayName
)
SELECT
	TZ.idTimezone,
	57,
	TZ.displayName
FROM tblTimezone TZ
WHERE NOT EXISTS (
	SELECT 1
	FROM tblTimezoneLanguage TZL
	WHERE TZL.idTimezone = TZ.idTimezone
	AND TZL.idLanguage = 57
)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblPermission]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblPermission] (
	[idPermission]				INT				IDENTITY (1, 1)		NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Permission]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblPermission ADD CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED (idPermission ASC)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblPermission] ON

INSERT INTO [tblPermission] (
	idPermission, 
	name
)
SELECT
	idPermission,
	name
FROM (
	SELECT			1 AS idPermission, 'System_AccountSettings' AS name
	UNION SELECT	2 AS idPermission, 'System_Configuration' AS name
	UNION SELECT	3 AS idPermission, 'System_Ecommerce' AS name
	UNION SELECT	4 AS idPermission, 'System_CouponCodes' AS name
	UNION SELECT	5 AS idPermission, 'System_TrackingCodes' AS name
	UNION SELECT	6 AS idPermission, 'System_EmailNotifications' AS name
	UNION SELECT	7 AS idPermission, 'System_API' AS name
	UNION SELECT	8 AS idPermission, 'System_xAPIEndpoints' AS name
	UNION SELECT	9 AS idPermission, 'System_Logs' AS name
	UNION SELECT	10 AS idPermission, 'System_WebMeetingIntegration' AS name
	UNION SELECT	11 AS idPermission, 'System_RulesEngine' AS name

	UNION SELECT	101 AS idPermission, 'UsersAndGroups_UserFieldConfiguration' AS name
	UNION SELECT	102 AS idPermission, 'UsersAndGroups_UserCreator' AS name
	UNION SELECT	103 AS idPermission, 'UsersAndGroups_UserDeleter' AS name
	UNION SELECT	104 AS idPermission, 'UsersAndGroups_UserEditor' AS name
	UNION SELECT	105 AS idPermission, 'UsersAndGroups_UserManager' AS name
	UNION SELECT	106 AS idPermission, 'UsersAndGroups_UserImpersonator' AS name
	UNION SELECT	107 AS idPermission, 'UsersAndGroups_GroupManager' AS name
	UNION SELECT	108 AS idPermission, 'UsersAndGroups_RoleManager' AS name
	UNION SELECT	109 AS idPermission, 'UsersAndGroups_ActivityImport' AS name
	UNION SELECT	110 AS idPermission, 'UsersAndGroups_CertificateImport' AS name
	UNION SELECT	111 AS idPermission, 'UsersAndGroups_Leaderboards' AS name
	UNION SELECT	112 AS idPermission, 'UsersAndGroups_GroupCreator' AS name
	UNION SELECT	113 AS idPermission, 'UsersAndGroups_GroupDeleter' AS name
	UNION SELECT	114 AS idPermission, 'UsersAndGroups_GroupEditor' AS name
	UNION SELECT	115 AS idPermission, 'UsersAndGroups_UserRegistrationApproval' AS name

	UNION SELECT	201 AS idPermission, 'InterfaceAndLayout_HomePage' AS name
	UNION SELECT	202 AS idPermission, 'InterfaceAndLayout_Masthead' AS name
	UNION SELECT	203 AS idPermission, 'InterfaceAndLayout_Footer' AS name
	UNION SELECT	204 AS idPermission, 'InterfaceAndLayout_CSSEditor' AS name
	UNION SELECT	205 AS idPermission, 'InterfaceAndLayout_ImageEditor' AS name

	UNION SELECT	301 AS idPermission, 'LearningAssets_CourseCatalog' AS name
	UNION SELECT	302 AS idPermission, 'LearningAssets_CourseContentManager' AS name
	UNION SELECT	303 AS idPermission, 'LearningAssets_LearningPathContentManager' AS name
	UNION SELECT	304 AS idPermission, 'LearningAssets_ContentPackageManager' AS name
	UNION SELECT	305 AS idPermission, 'LearningAssets_CertificateTemplateManager' AS name
	UNION SELECT	306 AS idPermission, 'LearningAssets_InstructorLedTrainingManager' AS name
	UNION SELECT	307 AS idPermission, 'LearningAssets_ResourceManager' AS name
	UNION SELECT	308 AS idPermission, 'LearningAssets_CertificationsManager' AS name
	UNION SELECT	309 AS idPermission, 'LearningAssets_QuizAndSurveyManager' AS name
	UNION SELECT	310 AS idPermission, 'LearningAssets_SelfEnrollmentApproval' AS name
	UNION SELECT	311 AS idPermission, 'LearningAssets_CourseEnrollmentManager' AS name
	UNION SELECT	312 AS idPermission, 'LearningAssets_LearningPathEnrollmentManager' AS name

	UNION SELECT	401 AS idPermission, 'Reporting_Reporter_UserDemographics' AS name
	UNION SELECT	402 AS idPermission, 'Reporting_Reporter_UserCourseTranscripts' AS name
	UNION SELECT	403 AS idPermission, 'Reporting_Reporter_CatalogAndCourseInformation' AS name
	UNION SELECT	404 AS idPermission, 'Reporting_Reporter_Certificates' AS name
	UNION SELECT	405 AS idPermission, 'Reporting_Reporter_XAPI' AS name
	UNION SELECT	406 AS idPermission, 'Reporting_Reporter_UserLearningPathTranscripts' AS name
	UNION SELECT	407 AS idPermission, 'Reporting_Reporter_UserInstructorLedTrainingTranscripts' AS name
	UNION SELECT	408 AS idPermission, 'Reporting_Reporter_Purchases' AS name
	UNION SELECT	409 AS idPermission, 'Reporting_Reporter_UserCertificationTranscripts' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblPermission P WHERE P.idPermission = MAIN.idPermission)

SET IDENTITY_INSERT [tblPermission] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblConstant]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblConstant] (
	[idConstant]			INT				IDENTITY(1,1)	NOT NULL,
	[key]					NVARCHAR(32)					NOT NULL, 
	[order]					INT								NOT NULL,
	[value]					NVARCHAR(64)					NOT NULL,
	[caption]				NVARCHAR(128)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Constant]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblConstant ADD CONSTRAINT [PK_Constant] PRIMARY KEY CLUSTERED (idConstant ASC)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSCORMVocabulary]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSCORMVocabulary] (
	[idSCORMVocabulary]			INT				 IDENTITY(1,1)	NOT NULL,
	[value]						NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SCORMVocabulary]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblSCORMVocabulary] ADD CONSTRAINT [PK_SCORMVocabulary] PRIMARY KEY CLUSTERED ([idSCORMVocabulary] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblSCORMVocabulary] ON

INSERT INTO [tblSCORMVocabulary] (
	idSCORMVocabulary,
	value
)
SELECT
	idSCORMVocabulary,
	value
FROM (
	SELECT 0 AS idSCORMVocabulary, 'unknown' AS value
	UNION SELECT 1 AS idSCORMVocabulary, 'not attempted' AS value
	UNION SELECT 2 AS idSCORMVocabulary, 'completed' AS value
	UNION SELECT 3 AS idSCORMVocabulary, 'incomplete' AS value
	UNION SELECT 4 AS idSCORMVocabulary, 'passed' AS value
	UNION SELECT 5 AS idSCORMVocabulary, 'failed' AS value
	UNION SELECT 6 AS idSCORMVocabulary, 'correct' AS value
	UNION SELECT 7 AS idSCORMVocabulary, 'wrong' AS value
	UNION SELECT 8 AS idSCORMVocabulary, 'neutral' AS value
	UNION SELECT 9 AS idSCORMVocabulary, 'unanticipated' AS value
	UNION SELECT 10 AS idSCORMVocabulary, 'incorrect' AS value

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblSCORMVocabulary SV WHERE SV.idSCORMVocabulary = MAIN.idSCORMVocabulary)

SET IDENTITY_INSERT [tblSCORMVocabulary] OFF
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblSCORMPackageType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSCORMPackageType] (
	[idSCORMPackageType]		INT				IDENTITY(1, 1)		NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]
GO

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SCORMPackageType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblSCORMPackageType] ADD CONSTRAINT [PK_SCORMPackageType] PRIMARY KEY CLUSTERED ([idSCORMPackageType] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblSCORMPackageType] ON

INSERT INTO [tblSCORMPackageType] (
	idSCORMPackageType, 
	name
)
SELECT
	idSCORMPackageType, 
	name
FROM (
	SELECT		 1 AS idSCORMPackageType, 'Content' AS name
	UNION SELECT 2 AS idSCORMPackageType, 'Resource' AS name

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblSCORMPackageType SPT WHERE SPT.idSCORMPackageType = MAIN.idSCORMPackageType)

SET IDENTITY_INSERT [tblSCORMPackageType] OFF
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblContentPackageType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblContentPackageType] (
	[idContentPackageType]		INT				IDENTITY (1, 1)		NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]
GO

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ContentPackageType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackageType] ADD CONSTRAINT [PK_ContentPackageType] PRIMARY KEY CLUSTERED ([idContentPackageType] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblContentPackageType] ON

INSERT INTO [tblContentPackageType] (
	idContentPackageType,
	name
)
SELECT
	idContentPackageType,
	name
FROM (
	SELECT		 1 AS idContentPackageType, 'SCORM' AS name
	UNION SELECT 2 AS idContentPackageType, 'xAPI' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblContentPackageType CPT WHERE CPT.idContentPackageType = MAIN.idContentPackageType)

SET IDENTITY_INSERT [tblContentPackageType] OFF
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblMediaType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblMediaType] (
	[idMediaType]		INT				IDENTITY (1, 1)		NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]
GO

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_MediaType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblMediaType] ADD CONSTRAINT [PK_MediaType] PRIMARY KEY CLUSTERED ([idMediaType] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblMediaType] ON

INSERT INTO [tblMediaType] (
	idMediaType,
	name
)
SELECT
	idMediaType,
	name
FROM (
	SELECT		 1 AS idMediaType, 'Video' AS name
	UNION SELECT 2 AS idMediaType, 'PowerPoint' AS name
	UNION SELECT 3 AS idMediaType, 'PDF' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblMediaType MT WHERE MT.idMediaType = MAIN.idMediaType)

SET IDENTITY_INSERT [tblMediaType] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryObjectType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDocumentRepositoryObjectType] (
	[idDocumentRepositoryObjectType]	INT				IDENTITY(1,1)	NOT NULL,
	[name]								NVARCHAR(50)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DocumentRepositoryObjectType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryObjectType] ADD CONSTRAINT [PK_DocumentRepositoryObjectType] PRIMARY KEY CLUSTERED ([idDocumentRepositoryObjectType] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblDocumentRepositoryObjectType] ON

INSERT INTO [tblDocumentRepositoryObjectType] (
	idDocumentRepositoryObjectType, 
	name
)
SELECT
	idDocumentRepositoryObjectType,
	name
FROM (
	SELECT		 1 AS idDocumentRepositoryObjectType, 'Group' AS name
	UNION SELECT 2 AS idDocumentRepositoryObjectType, 'Course' AS name
	UNION SELECT 3 AS idDocumentRepositoryObjectType, 'LearningPath' AS name
	UNION SELECT 4 AS idDocumentRepositoryObjectType, 'Profile' AS name

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblDocumentRepositoryObjectType DROT WHERE DROT.idDocumentRepositoryObjectType = MAIN.idDocumentRepositoryObjectType)

SET IDENTITY_INSERT [tblDocumentRepositoryObjectType] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSite]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSite] (
	[idSite]						INT					IDENTITY(2,1)	NOT NULL,
	[hostname]						NVARCHAR(255)						NOT NULL,
	[favicon]						NVARCHAR(255)						NULL,
	[password]						NVARCHAR(512)						NOT NULL,
	[isActive]						BIT									NOT NULL,
	[dtExpires]						DATETIME							NULL,
	[title]							NVARCHAR(255)						NOT NULL,
	[company]						NVARCHAR(255)						NOT NULL,
	[contactName]					NVARCHAR(255)						NULL,
	[contactEmail]					NVARCHAR(255)						NULL,
	[userLimit]						INT									NULL,
	[kbLimit]						INT									NULL,
	[idLanguage]					INT									NOT NULL,
	[idTimezone]					INT									NOT NULL,
	[dtLicenseAgreementExecuted]	DATETIME							NULL,
	[dtUserAgreementModified]		DATETIME							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSite ADD CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED (idSite ASC)

/**
TEMPLATE SITE
**/

SET IDENTITY_INSERT [tblSite] ON

INSERT INTO tblSite (
	idSite,
	hostname,
	favicon,
	[password],
	isActive,
	dtExpires,
	title,
	company,
	contactName,
	contactEmail,
	userLimit,
	kbLimit,
	idLanguage,
	idTimezone,
	dtLicenseAgreementExecuted
)
SELECT
	1,
	'default',
	'NULL', 
	'[PASSWORD]',
	1,
	NULL,
	'[TITLE]',
	'[COMPANY]',
	'[CONTACTNAME]',
	'[CONTACTEMAIL]',
	NULL,
	NULL,
	57, -- ENGLISH
	15, -- EST
	NULL
WHERE NOT EXISTS (
	SELECT 1
	FROM tblSite
	WHERE idSite = 1
)

SET IDENTITY_INSERT [tblSite] OFF

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblSite]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblSite]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblSite (
	title,
	hostname
)
	KEY INDEX PK_Site
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticDataset]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticDataset] (
	[idDataset]							INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[name]								NVARCHAR(255)						NOT NULL,
	[storedProcedureName]				NVARCHAR(50)						NOT NULL,
	[htmlTemplatePath]					NVARCHAR(255)						NULL,
	[description]						NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticDataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataset ADD CONSTRAINT [PK_AnalyticDataset] PRIMARY KEY CLUSTERED (idDataset ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticDataset_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataset ADD CONSTRAINT [FK_AnalyticDataset_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)


/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblAnalyticDataset] ON

INSERT INTO tblAnalyticDataset (
	idDataset,
	idSite,
	name,
	storedProcedureName,
	[description]
)
SELECT
	idDataset,
	idSite,
	name,
	storedProcedureName,
	[description]
FROM (
	SELECT 1 AS idDataset, 1 AS idSite, 'Login Activity' AS name, '[Dataset.LoginActivity]' AS storedProcedureName, 'Complete information of login activities of all registered users.' AS [description]
	UNION SELECT 2 AS idDataset, 1 AS idSite, 'Course Completions' AS name, '[Dataset.CourseCompletions]' AS storedProcedureName, 'Complete information of course enrolment and completion.' AS [description]
	UNION SELECT 3 AS idDataset, 1 AS idSite, 'User And Group Snapshot' AS name, '[Dataset.UserGroupSnapshot]' AS storedProcedureName, 'Complete information of users aNdd groups informations.' AS [description]
	UNION SELECT 6 AS idDataset, 1 AS idSite, 'Use Statistics' AS name, '[Dataset.UseStatistics]' AS storedProcedureName, 'Complete information of users' AS [description]
	UNION SELECT 7 AS idDataset, 1 AS idSite, 'Certificates' AS name, '[Dataset.Certificates]' AS storedProcedureName, 'Certificate award information. Includes certificates that have been earned as well as those that have been manually awarded.' AS [description]

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblAnalyticDataset D WHERE D.idDataset = MAIN.idDataset)

SET IDENTITY_INSERT [tblAnalyticDataset] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCatalog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCatalog] (
	[idCatalog]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL, 
	[idParent]					INT								NULL,
	[title]						NVARCHAR(255)					NOT NULL,
	[shortDescription]			NVARCHAR(512)					NULL, 
	[longDescription]			NVARCHAR(MAX)					NULL, 
	[isPrivate]					BIT								NOT NULL,
	[isClosed]					BIT								NULL,
	[cost]						FLOAT							NULL,
	[costType]					INT								NULL,
	[dtCreated]					DATETIME						NULL,
	[dtModified]				DATETIME						NULL,
	[order]						INT								NULL,
	[searchTags]				NVARCHAR(512)					NULL,
	[avatar]					NVARCHAR(255)					NULL,
	[shortcode]					NVARCHAR(10)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalog ADD CONSTRAINT [PK_Catalog] PRIMARY KEY CLUSTERED (idCatalog ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Catalog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalog ADD CONSTRAINT [FK_Catalog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Catalog_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalog ADD CONSTRAINT [FK_Catalog_Catalog] FOREIGN KEY (idParent) REFERENCES [tblCatalog] (idCatalog)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalog]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalog]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCatalog (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_Catalog
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificate](
	[idCertificate]							INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL,
	[name]									NVARCHAR(255)					NOT NULL,
	[issuingOrganization]					NVARCHAR(255)					NULL,
	[description]							NVARCHAR(MAX)					NULL,
	[code]									NVARCHAR(25)					NULL,
	[credits]								FLOAT							NULL,
	[filename]								NVARCHAR(255)					NULL,
	[isActive]								BIT								NULL,
	[activationDate]						DATETIME						NULL,
	[expiresInterval]						INT								NULL,
	[expiresTimeframe]						NVARCHAR(4)						NULL,
	[objectType]							INT			                    NULL,
	[idObject]								INT                             NULL,
	[isDeleted]								BIT								NULL,
	[dtDeleted]								DATETIME						NULL,
	[reissueBasedOnMostRecentCompletion]	BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Certificate]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificate ADD CONSTRAINT [PK_Certificate] PRIMARY KEY CLUSTERED (idCertificate ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Certificate_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificate ADD CONSTRAINT [FK_Certificate_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificate]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificate]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificate (
	name,
	[description]
)
KEY INDEX PK_Certificate
ON Asentia -- insert index to this catalog
GO
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblContentPackage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblContentPackage] (
	[idContentPackage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idContentPackageType]			INT								NOT NULL,
	[idSCORMPackageType]			INT								NULL,
	[name]							NVARCHAR(255)					NOT NULL,
	[path]							NVARCHAR(1024)					NOT NULL,
	[kb]							INT								NOT NULL,
	[manifest]						NVARCHAR(MAX)					NULL,
	[dtCreated]						DATETIME						NOT NULL,
	[dtModified]					DATETIME						NOT NULL,
	[isMediaUpload]					BIT								NULL,
	[idMediaType]					INT								NULL,
	[contentTitle]					NVARCHAR(255)					NULL,
	[originalMediaFilename]			NVARCHAR(255)					NULL,
	[isVideoMedia3rdParty]			BIT								NULL,
	[videoMediaEmbedCode]			NVARCHAR(MAX)					NULL,
	[enableAutoplay]				BIT								NULL,
	[allowRewind]					BIT								NULL,
	[allowFastForward]				BIT								NULL,
	[allowNavigation]				BIT								NULL,
	[allowResume]					BIT								NULL,
	[minProgressForCompletion]		FLOAT							NULL,
	[isProcessing]					BIT								NULL,
	[isProcessed]					BIT								NULL,
	[dtProcessed]					DATETIME						NULL,
	[idQuizSurvey]					INT								NULL,
	[hasManifestComplianceErrors]	BIT								NULL,
	[manifestComplianceErrors]		NVARCHAR(MAX)					NULL,
	[openSesameGUID]				NVARCHAR(36)					NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ContentPackage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [PK_ContentPackage] PRIMARY KEY CLUSTERED ([idContentPackage] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_ContentPackageType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_ContentPackageType] FOREIGN KEY (idContentPackageType) REFERENCES [tblContentPackageType] (idContentPackageType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_SCORMPackageType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_SCORMPackageType] FOREIGN KEY (idSCORMPackageType) REFERENCES [tblSCORMPackageType] (idSCORMPackageType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_MediaType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_MediaType] FOREIGN KEY (idMediaType) REFERENCES [tblMediaType] (idMediaType)

-- FK TO tblQuizSurvey IS DONE IN VERSION UPDATES Release_1.7 - IT SHOULD NOT BE DONE HERE BECAUSE IT WILL BE A CIRCULAR DEPENDENCY ON A NEW DATABASE

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblContentPackage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblContentPackage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblContentPackage (
	name,
	manifest
)
KEY INDEX PK_ContentPackage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCode] (
	[idCouponCode]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[code]							NVARCHAR(10)					NOT NULL,
	[comments]						NVARCHAR(MAX)					NULL,
	[usesAllowed]					INT								NOT NULL,
	[discount]						FLOAT							NOT NULL,
	[discountType]					INT						        NOT NULL,
	[forCourse]						INT								NULL,
	[forCatalog]					INT								NULL,
	[forLearningPath]				INT								NULL,
	[dtStart]						DATETIME						NULL,
	[dtEnd]							DATETIME						NULL,
	[isSingleUsePerUser]			BIT								NULL,
	[isDeleted]						BIT								NULL,
	[dtDeleted]						DATETIME						NULL,
	[forStandupTraining]			INT								NULL
) ON [PRIMARY] 

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCode ADD CONSTRAINT [PK_CouponCode] PRIMARY KEY CLUSTERED (idCouponCode ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCode_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCode ADD CONSTRAINT [FK_CouponCode_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCouponCode]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCouponCode]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCouponCode (
	code
)
KEY INDEX PK_CouponCode
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourse]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourse] (
	[idCourse]											INT					IDENTITY(1,1)	NOT NULL,
	[idSite]											INT									NOT NULL, 
	[title]												NVARCHAR(255)						NOT NULL,
	[coursecode]										NVARCHAR(255)						NULL,
	[revcode]											NVARCHAR(32)						NULL,
	[avatar]											NVARCHAR(255)						NULL,
	[cost]												FLOAT								NULL,
	[credits]											FLOAT								NULL,
	[minutes]											INT									NULL,
	[shortDescription]									NVARCHAR(512)						NULL, 
	[longDescription]									NVARCHAR(MAX)						NULL, 
	[objectives]										NVARCHAR(MAX)						NULL, 
	[socialMedia]										NVARCHAR(MAX)						NULL,
	[isPublished]										BIT									NULL,
	[isClosed]											BIT									NULL,
	[isLocked]											BIT									NULL,
	[isPrerequisiteAny]									BIT									NULL,
	[isFeedActive]										BIT								    NULL,
	[isFeedModerated]									BIT                                 NULL,
	[isFeedOpenSubscription]							BIT                                 NULL,
	[dtCreated]											DATETIME							NULL,
	[dtModified]										DATETIME							NULL,
	[isDeleted]											BIT									NOT NULL,
	[dtDeleted]											DATETIME							NULL,
	[order]												INT									NULL,
	[disallowRating]									BIT									NULL,
	[forceLessonCompletionInOrder]						BIT									NULL,
	[selfEnrollmentIsOneTimeOnly]						BIT									NULL,
	[selfEnrollmentDueInterval]							INT									NULL,
	[selfEnrollmentDueTimeframe]						NVARCHAR(4)							NULL,
	[selfEnrollmentExpiresFromStartInterval]			INT									NULL,
	[selfEnrollmentExpiresFromStartTimeframe]			NVARCHAR(4)							NULL,
	[selfEnrollmentExpiresFromFirstLaunchInterval]		INT									NULL,
	[selfEnrollmentExpiresFromFirstLaunchTimeframe]		NVARCHAR(4)							NULL,
	[searchTags]										NVARCHAR(512)						NULL,
	[requireFirstLessonToBeCompletedBeforeOthers]		BIT									NULL,
	[lockLastLessonUntilOthersCompleted]				BIT									NULL,
	[isSelfEnrollmentApprovalRequired]					BIT									NULL,
	[isApprovalAllowedByCourseExperts]					BIT									NULL,
	[isApprovalAllowedByUserSupervisor]					BIT									NULL,
	[shortcode]											NVARCHAR(10)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourse ADD CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED (idCourse ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Course_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourse ADD CONSTRAINT [FK_Course_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourse]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourse]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCourse (
	title,
	coursecode, 
	shortDescription,
	searchTags
)
KEY INDEX PK_Course
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetEnrollment] (
	[idRuleSetEnrollment]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL, 
	[idCourse]								INT								NOT NULL, 
	[idTimezone]							INT								NOT NULL,
	[priority]								INT								NOT NULL,	-- per course. indicates which enrollment 'takes' first based on the rules
	[label]									NVARCHAR(255)					NOT NULL,
	[isLockedByPrerequisites]				BIT								NOT NULL,
	[isFixedDate]							BIT								NOT NULL,
	[dtStart]								DATETIME						NOT NULL,
	[dtEnd]									DATETIME						NULL,
	[dtCreated]								DATETIME						NOT NULL,
	[delayInterval]							INT								NULL,
	[delayTimeframe]						NVARCHAR(4)						NULL,
	[dueInterval]							INT								NULL,
	[dueTimeframe]							NVARCHAR(4)						NULL,
	[recurInterval]							INT								NULL,
	[recurTimeframe]						NVARCHAR(4)						NULL,
	[expiresFromStartInterval]				INT								NULL,
	[expiresFromStartTimeframe]				NVARCHAR(4)						NULL,
	[expiresFromFirstLaunchInterval]		INT								NULL,
	[expiresFromFirstLaunchTimeframe]		NVARCHAR(4)						NULL,
	--[expiresFromCompletionInterval]		INT								NULL,
	--[expiresFromCompletionTimeframe]		NVARCHAR(4)						NULL,
	[forceReassignment]						BIT								NULL,
	[idParentRuleSetEnrollment]				INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetEnrollment] ADD CONSTRAINT [PK_RuleSetEnrollment] PRIMARY KEY CLUSTERED ([idRuleSetEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollment_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetEnrollment] ADD CONSTRAINT [FK_RuleSetEnrollment_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetEnrollment] ADD CONSTRAINT [FK_RuleSetEnrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetEnrollment] ADD CONSTRAINT [FK_RuleSetEnrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollment]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollment]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetEnrollment (
	label
)
KEY INDEX PK_RuleSetEnrollment
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUser] (
	[idUser]						INT					IDENTITY(2, 1)							NOT NULL,
	[firstName]						NVARCHAR(255)												NOT NULL,
	[middleName]					NVARCHAR(255)												NULL,
	[lastName]						NVARCHAR(255)												NOT NULL,
	[displayName]					NVARCHAR(768)												NOT NULL,
	[avatar]						NVARCHAR(255)												NULL,
	[email]							NVARCHAR(255)												NULL,
	[username]						NVARCHAR(512)												NOT NULL,
	[password]						NVARCHAR(512)		COLLATE SQL_Latin1_General_CP1_CS_AS	NOT NULL,
	[idSite]						INT															NOT NULL,
	[idTimezone]					INT															NOT NULL,
	[objectGUID]					UNIQUEIDENTIFIER											NULL,
	[activationGUID]				UNIQUEIDENTIFIER											NULL,
	[distinguishedName]				NVARCHAR(512)												NULL,
	[isActive]						BIT															NOT NULL,
	[mustchangePassword]			BIT															NOT NULL,
	[dtCreated]						DATETIME													NOT NULL,
	[dtExpires]						DATETIME													NULL,
	[isDeleted]						BIT															NOT NULL,
	[dtDeleted]						DATETIME													NULL,
	[idLanguage]					INT															NOT NULL,
	[dtModified]					DATETIME													NULL,
	[dtLastLogin]					DATETIME													NULL,
	[employeeID]					NVARCHAR(255)												NULL,
	[company]						NVARCHAR(255)												NULL,
	[address]						NVARCHAR(512)												NULL,
	[city]							NVARCHAR(255)												NULL,
	[province]						NVARCHAR(255)												NULL,
	[postalcode]					NVARCHAR(25)												NULL,
	[country]						NVARCHAR(50)												NULL,
	[phonePrimary]					NVARCHAR(25)												NULL,
	[phoneWork]						NVARCHAR(25)												NULL,
	[phoneFax]						NVARCHAR(25)												NULL,
	[phoneHome]						NVARCHAR(25)												NULL,
	[phoneMobile]					NVARCHAR(25)												NULL,
	[phonePager]					NVARCHAR(25)												NULL,
	[phoneOther]					NVARCHAR(25)												NULL,
	[department]					NVARCHAR(255)												NULL,
	[division]						NVARCHAR(255)												NULL,
	[region]						NVARCHAR(255)												NULL,
	[jobTitle]						NVARCHAR(255)												NULL,
	[jobClass]						NVARCHAR(255)												NULL,
	[gender]						NVARCHAR(1)													NULL,
	[race]							NVARCHAR(64)												NULL,
	[dtDOB]							DATETIME													NULL,
	[dtHire]						DATETIME													NULL,
	[dtTerm]						DATETIME													NULL,
	[dtSessionExpires]				DATETIME													NULL,
	[activeSessionId]				NVARCHAR(80)												NULL,
	[field00]						NVARCHAR(255)												NULL,
	[field01]						NVARCHAR(255)												NULL,
	[field02]						NVARCHAR(255)												NULL,
	[field03]						NVARCHAR(255)												NULL,
	[field04]						NVARCHAR(255)												NULL,
	[field05]						NVARCHAR(255)												NULL,
	[field06]						NVARCHAR(255)												NULL,
	[field07]						NVARCHAR(255)												NULL,
	[field08]						NVARCHAR(255)												NULL,
	[field09]						NVARCHAR(255)												NULL,
	[field10]						NVARCHAR(255)												NULL,
	[field11]						NVARCHAR(255)												NULL,
	[field12]						NVARCHAR(255)												NULL,
	[field13]						NVARCHAR(255)												NULL,
	[field14]						NVARCHAR(255)												NULL,
	[field15]						NVARCHAR(255)												NULL,
	[field16]						NVARCHAR(255)												NULL,
	[field17]						NVARCHAR(255)												NULL,
	[field18]						NVARCHAR(255)												NULL,
	[field19]						NVARCHAR(255)												NULL,
	[dtLastPermissionCheck]			DATETIME													NULL,
	[isRegistrationApproved]		BIT															NULL,
	[dtApproved]					DATETIME													NULL,
	[dtDenied]						DATETIME													NULL,
	[idApprover]					INT															NULL,
	[rejectionComments]				NVARCHAR(MAX)												NULL,
	[dtMarkedDisabled]				DATETIME													NULL,
	[disableLoginFromLoginForm]		BIT															NULL,
	[exemptFromIPLoginRestriction]	BIT															NULL,
	[optOutOfEmailNotifications]	BIT															NULL,
	[dtUserAgreementAgreed]			DATETIME													NULL,
	[excludeFromLeaderboards]		BIT															NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUser ADD CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED (idUser ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_User_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUser ADD CONSTRAINT [FK_User_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_User_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUser ADD CONSTRAINT [FK_User_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblUser]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblUser]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblUser (
	displayName,
	username, 
	email,
	employeeID 
)
KEY INDEX PK_User
ON Asentia -- insert index to this catalog

/**
TEMPLATE USER
**/

SET IDENTITY_INSERT [tblUser] ON

INSERT INTO tblUser (
	idUser, 
	firstName,
	lastName,
	displayName,
	username,
	[password],
	idSite,
	idTimezone,
	idLanguage,
	isActive,
	mustchangePassword,
	dtCreated,
	isDeleted
	)
SELECT
	1, 
	'[FIRSTNAME]',
	'[LASTNAME]',
	'[DISPLAY_NAME]',
	'[USERNAME]',
	'[PASSWORD]',
	1,
	17,
	57,
	1,
	0,
	GETUTCDATE(),
	0
WHERE NOT EXISTS (
	SELECT 1
	FROM tblUser
	WHERE idUser = 1
)

SET IDENTITY_INSERT [tblUser] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroup] (
	[idGroup]							INT					IDENTITY (1, 1)		NOT NULL, 
	[idSite]							INT										NOT NULL, 
	[name]								NVARCHAR(255)							NOT NULL,
	[avatar]							NVARCHAR(255)							NULL,
	[shortDescription]					NVARCHAR(512)							NULL,
	[longDescription]					NVARCHAR(MAX)							NULL,
	[primaryGroupToken]					NVARCHAR(255)							NULL,
	[objectGUID]						UNIQUEIDENTIFIER						NULL,
	[distinguishedName]					NVARCHAR(512)							NULL,
	[isSelfJoinAllowed]					BIT										NULL,
	[isFeedActive]                      BIT                                     NULL,
	[isFeedModerated]                   BIT                                     NULL,
	[membershipIsPublicized]            BIT                                     NULL,
	[searchTags]						NVARCHAR(512)							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroup ADD CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED (idGroup ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Group_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroup ADD CONSTRAINT [FK_Group_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroup]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroup]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblGroup (
	name,
	shortDescription,
	searchTags
)
KEY INDEX PK_Group
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupEnrollment] (
	[idGroupEnrollment]						INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idCourse]								INT							NOT NULL,
	[idGroup]								INT							NOT NULL,
	[idTimezone]							INT							NOT NULL,	-- indicates what TZ the data should be converted to for display in the 'create/modify/delete' interface
	[isLockedByPrerequisites]				BIT							NOT NULL,
	[dtStart]								DATETIME					NOT NULL,	-- the date in which enrollments should start being inherited by members of the group, there is no "lifespan end," just delete the record when it should stop
	[dtCreated]								DATETIME					NOT NULL,
	[dueInterval]							INT							NULL,
	[dueTimeframe]							NVARCHAR(4)					NULL,
	[expiresFromStartInterval]				INT							NULL,
	[expiresFromStartTimeframe]				NVARCHAR(4)					NULL,
	[expiresFromFirstLaunchInterval]		INT							NULL,
	[expiresFromFirstLaunchTimeframe]		NVARCHAR(4)					NULL
	--[expiresFromCompletionInterval]			INT							NULL,
	--[expiresFromCompletionTimeframe]		NVARCHAR(4)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [PK_GroupEnrollment] PRIMARY KEY CLUSTERED ([idGroupEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollment_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [FK_GroupEnrollment_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollment_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [FK_GroupEnrollment_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [FK_GroupEnrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [FK_GroupEnrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPath] (
	[idLearningPath]					INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[name]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL, 
	[longDescription]					NVARCHAR(MAX)						NULL, 
	[cost]								FLOAT								NULL, 
	[isPublished]						BIT									NULL,
	[isClosed]							BIT									NULL,
	[selfEnrollmentIsOneTimeOnly]		BIT									NULL,
	[forceCompletionInOrder]			BIT									NULL,
	[dtCreated]							DATETIME							NOT NULL,
	[dtModified]						DATETIME							NULL,
	[isDeleted]							BIT									NULL,
	[dtDeleted]							DATETIME							NULL,
	[avatar]							NVARCHAR(255)						NULL,
	[searchTags]						NVARCHAR(512)						NULL,
	[shortcode]							NVARCHAR(10)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPath ADD CONSTRAINT [PK_LearningPath] PRIMARY KEY CLUSTERED (idLearningPath ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPath_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPath ADD CONSTRAINT [FK_LearningPath_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLearningPath (
	name,
	shortDescription,
	searchTags
)
KEY INDEX PK_LearningPath
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetLearningPathEnrollment] (
	[idRuleSetLearningPathEnrollment]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]									INT								NOT NULL, 
	[idLearningPath]							INT								NOT NULL, 
	[idTimezone]								INT								NOT NULL,
	[priority]									INT								NOT NULL,	-- per learning path. indicates which enrollment 'takes' first based on the rules
	[label]										NVARCHAR(255)					NOT NULL,
	[dtStart]									DATETIME						NOT NULL,
	[dtEnd]										DATETIME						NULL,					
	[delayInterval]								INT								NULL,
	[delayTimeframe]							NVARCHAR(4)						NULL,
	[dueInterval]								INT								NULL,
	[dueTimeframe]								NVARCHAR(4)						NULL,
	[expiresFromStartInterval]					INT								NULL,
	[expiresFromStartTimeframe]					NVARCHAR(4)						NULL,
	[expiresFromFirstLaunchInterval]			INT								NULL,
	[expiresFromFirstLaunchTimeframe]			NVARCHAR(4)						NULL,
	[idParentRuleSetLearningPathEnrollment]		NVARCHAR(4)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetLearningPathEnrollment] ADD CONSTRAINT [PK_RuleSetLearningPathEnrollment] PRIMARY KEY CLUSTERED ([idRuleSetLearningPathEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollment_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetLearningPathEnrollment] ADD CONSTRAINT [FK_RuleSetLearningPathEnrollment_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetLearningPathEnrollment] ADD CONSTRAINT [FK_RuleSetLearningPathEnrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblRuleSetLearningPathEnrollment] ADD CONSTRAINT [FK_RuleSetLearningPathEnrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollment]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollment]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetLearningPathEnrollment (
	label
)
KEY INDEX PK_RuleSetLearningPathEnrollment
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathEnrollment] ( 
	[idLearningPathEnrollment]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[idLearningPath]					INT								NOT NULL,
	[idUser]							INT								NOT NULL,
	[idRuleSetLearningPathEnrollment]	INT								NULL,		-- indicates the enrollment is inherited from a ruleset enrollment
	[idTimezone]						INT								NOT NULL,
	[title]								NVARCHAR(255)					NOT NULL,
	[dtStart]							DATETIME						NOT NULL,	-- accounts for inherited delay (after dtCreated).
	[dtDue]								DATETIME						NULL,
	[dtExpiresFromStart]				DATETIME						NULL,
	[dtExpiresFromFirstLaunch]			DATETIME						NULL,
	[dtFirstLaunch]						DATETIME						NULL,
	[dtCompleted]						DATETIME						NULL,
	[dtCreated]							DATETIME						NOT NULL,
	[dueInterval]						INT								NULL,
	[dueTimeframe]						NVARCHAR(4)						NULL,
	[expiresFromStartInterval]			INT								NULL,
	[expiresFromStartTimeframe]			NVARCHAR(4)						NULL,
	[expiresFromFirstLaunchInterval]	INT								NULL,
	[expiresFromFirstLaunchTimeframe]	NVARCHAR(4)						NULL,
	[dtLastSynchronized]				DATETIME						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [PK_LearningPathEnrollment] PRIMARY KEY CLUSTERED ([idLearningPathEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_RuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_RuleSetLearningPathEnrollment] FOREIGN KEY ([idRuleSetLearningPathEnrollment]) REFERENCES [tblRuleSetLearningPathEnrollment] ([idRuleSetLearningPathEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathEnrollment]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathEnrollment]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLearningPathEnrollment (
	title
)
KEY INDEX [PK_LearningPathEnrollment]
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblActivityImport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblActivityImport] (
	[idActivityImport]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idUser]							INT								NOT NULL,
	[timestamp]							DATETIME						NOT NULL,
	[idUserImported]					INT								NULL,
	[importFileName]					NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ActivityImport]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblActivityImport ADD CONSTRAINT [PK_ActivityImport] PRIMARY KEY CLUSTERED (idActivityImport ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ActivityImport_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblActivityImport ADD CONSTRAINT [FK_ActivityImport_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ActivityImport_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblActivityImport ADD CONSTRAINT [FK_ActivityImport_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ActivityImport_UserImported]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblActivityImport ADD CONSTRAINT [FK_ActivityImport_UserImported] FOREIGN KEY (idUserImported) REFERENCES [tblUser] (idUser)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblActivityImport]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblActivityImport]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblActivityImport (
	importFileName
)
KEY INDEX PK_ActivityImport
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEnrollment] (
	[idEnrollment]							INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL, 
	[idCourse]								INT								NULL, 
	[idUser]								INT								NOT NULL, 
	[idGroupEnrollment]						INT								NULL,		-- indicates the enrollment is inherited from this group enrollment
	[idRuleSetEnrollment]					INT								NULL,		-- indicates the enrollment is inherited from a ruleset enrollment
	[idLearningPathEnrollment]				INT								NULL,		-- indicates the enrollment is directly inherited by a learning path enrollment
																						-- not all enrollments that go for credit toward a learning path enrollment will
																						-- have this column populated, enrollments a learner has prior to enrolling in a
																						-- learning path can still count toward learning path completion
	[idTimezone]							INT								NOT NULL,
	[isLockedByPrerequisites]				BIT								NOT NULL,
	[code]									NVARCHAR(255)					NULL,
	[revcode]								NVARCHAR(32)					NULL,
	[title]									NVARCHAR(255)					NOT NULL,
	[dtStart]								DATETIME						NOT NULL,	-- accounts for inherited delay (after dtCreated). non-inherited records should have identical dtCreated and dtStart.
	[dtDue]									DATETIME						NULL,
	[dtExpiresFromStart]					DATETIME						NULL,
	[dtExpiresFromFirstLaunch]				DATETIME						NULL,
	[dtFirstLaunch]							DATETIME						NULL,
	[dtCompleted]							DATETIME						NULL,
	[dtCreated]								DATETIME						NOT NULL, 
	[dtLastSynchronized]					DATETIME						NULL,
	[dueInterval]							INT								NULL,
	[dueTimeframe]							NVARCHAR(4)						NULL,
	[expiresFromStartInterval]				INT								NULL,
	[expiresFromStartTimeframe]				NVARCHAR(4)						NULL,
	[expiresFromFirstLaunchInterval]		INT								NULL,
	[expiresFromFirstLaunchTimeframe]		NVARCHAR(4)						NULL,
	[idActivityImport]						INT								NULL,
	[credits]								FLOAT							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Enrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [PK_Enrollment] PRIMARY KEY CLUSTERED ([idEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_GroupEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_GroupEnrollment] FOREIGN KEY ([idGroupEnrollment]) REFERENCES [tblGroupEnrollment] ([idGroupEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_RuleSetEnrollment] FOREIGN KEY ([idRuleSetEnrollment]) REFERENCES [tblRuleSetEnrollment] ([idRuleSetEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_LearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_LearningPathEnrollment] FOREIGN KEY ([idLearningPathEnrollment]) REFERENCES [tblLearningPathEnrollment] ([idLearningPathEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Enrollment_ActivityImport]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblEnrollment] ADD CONSTRAINT [FK_Enrollment_ActivityImport] FOREIGN KEY (idActivityImport) REFERENCES [tblActivityImport] (idActivityImport)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEnrollment]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEnrollment]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEnrollment (
	code,
	title
)
KEY INDEX PK_Enrollment
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollmentLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetEnrollmentLanguage] (
	[idRuleSetEnrollmentLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idRuleSetEnrollment]				INT								NOT NULL, 
	[idLanguage]						INT								NOT NULL,
	[label]								NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetEnrollmentLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetEnrollmentLanguage ADD CONSTRAINT [PK_RuleSetEnrollmentLanguage] PRIMARY KEY CLUSTERED (idRuleSetEnrollmentLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollmentLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetEnrollmentLanguage ADD CONSTRAINT [FK_RuleSetEnrollmentLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetEnrollmentLanguage_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetEnrollmentLanguage ADD CONSTRAINT [FK_RuleSetEnrollmentLanguage_RuleSetEnrollment] FOREIGN KEY (idRuleSetEnrollment) REFERENCES [tblRuleSetEnrollment] (idRuleSetEnrollment)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollmentLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetEnrollmentLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetEnrollmentLanguage (
	label
)
KEY INDEX PK_RuleSetEnrollmentLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-Lesson]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-Lesson] (
	[idData-Lesson]									INT				IDENTITY(1,1)	NOT NULL,
	[idSite]										INT								NOT NULL, 
	[idEnrollment]									INT								NOT NULL, 
	[idLesson]										INT								NULL,
	[idTimezone]									INT								NOT NULL,
	[title]											NVARCHAR(255)					NOT NULL,
	[revcode]										NVARCHAR(32)					NULL,
	[order]											INT								NOT NULL,
	[contentTypeCommittedTo]						INT								NULL,
	[dtCommittedToContentType]						DATETIME						NULL,
	[dtCompleted]									DATETIME						NULL,
	[resetForContentChange]							BIT								NULL,		-- flag to reset lesson data when lesson content is changed
	[preventPostCompletionLaunchForContentChange]	BIT								NULL		-- flag to prevent launch of content when content has been changed after lesson completion
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-Lesson] ADD CONSTRAINT [PK_Data-Lesson] PRIMARY KEY CLUSTERED ([idData-Lesson] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-Lesson_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-Lesson] ADD CONSTRAINT [FK_Data-Lesson_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-Lesson_Enrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-Lesson] ADD CONSTRAINT [FK_Data-Lesson_Enrollment] FOREIGN KEY ([idEnrollment]) REFERENCES [tblEnrollment] ([idEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-Lesson_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-Lesson] ADD CONSTRAINT [FK_Data-Lesson_Timezone] FOREIGN KEY ([idTimezone]) REFERENCES [tblTimezone] ([idTimezone])

-- DO NOT FK to idLesson (lessons can be deleted while leaving the data record).

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblData-Lesson]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblData-Lesson]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON [tblData-Lesson] (
	title
)
KEY INDEX [PK_Data-Lesson]
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDataset]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDataset] (
	[idDataset]							INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[name]								NVARCHAR(255)						NOT NULL,
	[storedProcedureName]				NVARCHAR(50)						NOT NULL,
	[htmlTemplatePath]					NVARCHAR(255)						NULL,
	[description]						NVARCHAR(512)						NULL,
	[order]								INT									
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Dataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDataset ADD CONSTRAINT [PK_Dataset] PRIMARY KEY CLUSTERED (idDataset ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Dataset_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDataset ADD CONSTRAINT [FK_Dataset_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)


/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblDataset] ON

INSERT INTO tblDataset (
	idDataset,
	idSite,
	name,
	storedProcedureName,
	[description],
	[order]
)
SELECT
	idDataset,
	idSite,
	name,
	storedProcedureName,
	[description],
	[order]
FROM (
	SELECT 1 AS idDataset, 1 AS idSite, 'User Demographics' AS name, '[Dataset.UserDemographics]' AS storedProcedureName, 'Complete user account information for all users including name, username, email, contact information, organizational information and more.' AS [description], 1 AS [order]
	UNION SELECT 2 AS idDataset, 1 AS idSite, 'User Course Transcripts' AS name, '[Dataset.UserCourseTranscripts]' AS storedProcedureName, 'Course and lesson status, and completion information including scores and dates.' AS [description], 2 AS [order]
	UNION SELECT 3 AS idDataset, 1 AS idSite, 'Catalog and Course Information' AS name, '[Dataset.CatalogCourseInformation]' AS storedProcedureName, 'Information for catalogs, courses, lessons, and ILT sessions.' AS [description], 6 AS [order]
	UNION SELECT 4 AS idDataset, 1 AS idSite, 'Certificates' AS name, '[Dataset.Certificates]' AS storedProcedureName, 'Certificate award information. Includes certificates that have been earned as well as those that have been manually awarded.' AS [description], 7 AS [order]
	UNION SELECT 5 AS idDataset, 1 AS idSite, 'xAPI' AS name, '[Dataset.xAPI]' AS storedProcedureName, 'xAPI statments recorded to the system''s learning record store from outside training.' AS [description], 8 AS [order]
	UNION SELECT 6 AS idDataset, 1 AS idSite, 'User Learning Path Transcripts' AS name, '[Dataset.UserLearningPathTranscripts]' AS storedProcedureName, 'Learning Path status and completion information.' AS [description], 3 AS [order]
	UNION SELECT 7 AS idDataset, 1 AS idSite, 'User ILT Transcripts' AS name, '[Dataset.UserInstructorLedTranscripts]' AS storedProcedureName, 'Instructor Led Training status and completion information including scores and dates.' AS [description], 4 AS [order]
	UNION SELECT 8 AS idDataset, 1 AS idSite, 'Purchases' AS name, '[Dataset.Purchases]' AS storedProcedureName, 'Information on purchases made by users within the system.' AS [description], 9 AS [order]
	UNION SELECT 9 AS idDataset, 1 AS idSite, 'User Certification Transcripts' AS name, '[Dataset.UserCertificationTranscripts]' AS storedProcedureName, 'Certification status information.' AS [description], 5 AS [order]
	UNION SELECT 10 AS idDataset, 1 AS idSite, 'All User Logins' AS name, '[Dataset.AllUserLogins]' AS storedProcedureName, 'Complete listing of user account information and login timestamps.' AS [description], 10 AS [order]
	UNION SELECT 11 AS idDataset, 1 AS idSite, 'Unique User Logins' AS name, '[Dataset.UniqueUserLogins]' AS storedProcedureName, 'Unique user login counts by month and year.' AS [description], 11 AS [order]
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblDataset D WHERE D.idDataset = MAIN.idDataset)

SET IDENTITY_INSERT [tblDataset] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblxAPIoAuthConsumer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [dbo].[tblxAPIoAuthConsumer] (
	[idxAPIoAuthConsumer]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[label]							NVARCHAR(200)					NULL,
	[oAuthKey]						NVARCHAR(500)					NULL,
	[oAuthSecret]					NVARCHAR(200)					NULL,
	[isActive]						BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_xAPIoAuthConsumer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblxAPIoAuthConsumer] ADD CONSTRAINT [PK_xAPIoAuthConsumer] PRIMARY KEY CLUSTERED ([idxAPIoAuthConsumer] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_xAPIoAuthConsumer_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblxAPIoAuthConsumer] ADD CONSTRAINT [FK_xAPIoAuthConsumer_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblxAPIoAuthConsumer]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblxAPIoAuthConsumer]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblxAPIoAuthConsumer (
	label
)
KEY INDEX PK_xAPIoAuthConsumer
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblTinCanVerb]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTinCanVerb](
	idVerb              INT					IDENTITY(1,1)	    NOT NULL,
	verb				NVARCHAR(25)						    NOT NULL,
	URI					NVARCHAR(100)							NOT NULL,
	dtCreated			DATETIME								NULL,
	dtModified			DATETIME								NULL,
	isDeleted			BIT										NOT NULL,
	dtDeleted			DATETIME								NULL
) ON [PRIMARY]
GO

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_TinCanVerb]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblTinCanVerb] ADD CONSTRAINT [PK_TinCanVerb] PRIMARY KEY CLUSTERED ([idVerb] ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblTinCanVerb] ON

INSERT INTO [tblTinCanVerb] (
	idVerb ,
	verb,		
	URI,			
   dtCreated,
	dtModified,	
	isDeleted,	
    dtDeleted	
)
SELECT 
       idVerb ,
	   verb,		
	   URI,		
	   dtCreated,
	   dtModified,
	   isDeleted,	
	   dtDeleted	
FROM	 ( 

	SELECT		 1 AS idVerb, 'answered' AS verb, 'http://adlnet.gov/expapi/verbs/answered' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 2 AS idVerb, 'asked' AS verb, 'http://adlnet.gov/expapi/verbs/asked' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 3 AS idVerb, 'attempted' AS verb, 'http://adlnet.gov/expapi/verbs/attempted' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 4 AS idVerb, 'attended' AS verb, 'http://adlnet.gov/expapi/verbs/attended' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 5 AS idVerb, 'commented' AS verb, 'http://adlnet.gov/expapi/verbs/commented' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 6 AS idVerb, 'completed' AS verb, 'http://adlnet.gov/expapi/verbs/completed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 7 AS idVerb, 'exited' AS verb, 'http://adlnet.gov/expapi/verbs/exited' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 8 AS idVerb, 'experienced' AS verb, 'http://adlnet.gov/expapi/verbs/experienced' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 9 AS idVerb, 'failed' AS verb, 'http://adlnet.gov/expapi/verbs/failed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 10 AS idVerb, 'imported' AS verb, 'http://adlnet.gov/expapi/verbs/imported' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 11 AS idVerb, 'initialized' AS verb, 'http://adlnet.gov/expapi/verbs/initialized' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 12 AS idVerb, 'interacted' AS verb, 'http://adlnet.gov/expapi/verbs/interacted' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 13 AS idVerb, 'launched' AS verb, 'http://adlnet.gov/expapi/verbs/launched' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 14 AS idVerb, 'mastered' AS verb, 'http://adlnet.gov/expapi/verbs/mastered' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 15 AS idVerb, 'passed' AS verb, 'http://adlnet.gov/expapi/verbs/passed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 16 AS idVerb, 'preferred' AS verb, 'http://adlnet.gov/expapi/verbs/preferred' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 17 AS idVerb, 'progressed' AS verb, 'http://adlnet.gov/expapi/verbs/progressed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 18 AS idVerb, 'registered' AS verb, 'http://adlnet.gov/expapi/verbs/registered' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 19 AS idVerb, 'responded' AS verb, 'http://adlnet.gov/expapi/verbs/responded' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 20 AS idVerb, 'resumed' AS verb, 'http://adlnet.gov/expapi/verbs/resumed' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 21 AS idVerb, 'scored' AS verb, 'http://adlnet.gov/expapi/verbs/scored' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 22 AS idVerb, 'shared' AS verb, 'http://adlnet.gov/expapi/verbs/shared' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 23 AS idVerb, 'suspended' AS verb, 'http://adlnet.gov/expapi/verbs/suspended' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 24 AS idVerb, 'terminated' AS verb, 'http://adlnet.gov/expapi/verbs/terminated' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted
	UNION SELECT 25 AS idVerb, 'voided' AS verb, 'http://adlnet.gov/expapi/verbs/voided' AS URI, GETUTCDATE() AS dtCreated, NULL AS dtModified, 0 AS isDeleted, NULL AS dtDeleted

) MAIN
	WHERE NOT EXISTS (SELECT 1 FROM tblTinCanVerb TCV WHERE TCV.idVerb = MAIN.idVerb)

SET IDENTITY_INSERT [tblTinCanVerb] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-TinCan]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-TinCan] (
	[idData-TinCan]								INT				IDENTITY(1,1)	NOT NULL,
	[idData-Lesson]                             INT                             NULL,
	[idData-TinCanParent]						INT								NULL,
	[idSite]									INT								NOT NULL,
	[idEndpoint]								INT								NULL,
	[isInternalAPI]								BIT								NOT NULL,
	[statementId]								NVARCHAR(50)					NULL,
	[actor]										NVARCHAR(MAX)					NOT NULL,
	[verbId]									INT					            NOT NULL,
	[verb]										NVARCHAR(200)					NOT NULL,
	[activityId]								NVARCHAR(300)					NULL,
	[mboxObject]								NVARCHAR(100)					NULL,
	[mboxSha1SumObject]							NVARCHAR(100)					NULL,
	[openIdObject]								NVARCHAR(100)					NULL,
	[accountObject]								NVARCHAR(100)					NULL,
	[mboxActor]									NVARCHAR(100)					NULL,
	[mboxSha1SumActor]							NVARCHAR(100)					NULL,
	[openIdActor]								NVARCHAR(100)					NULL,
	[accountActor]								NVARCHAR(100)					NULL,
	[mboxAuthority]								NVARCHAR(100)					NULL,
	[mboxSha1SumAuthority]						NVARCHAR(100)					NULL,
	[openIdAuthority]							NVARCHAR(100)					NULL,
	[accountAuthority]							NVARCHAR(100)					NULL,
	[mboxTeam]									NVARCHAR(100)					NULL,
	[mboxSha1SumTeam]							NVARCHAR(100)					NULL,
	[openIdTeam]								NVARCHAR(100)					NULL,
	[accountTeam]								NVARCHAR(100)					NULL,
	[mboxInstructor]							NVARCHAR(100)					NULL,
	[mboxSha1SumInstructor]						NVARCHAR(100)					NULL,
	[openIdInstructor]							NVARCHAR(100)					NULL,
	[accountInstructor]							NVARCHAR(100)					NULL,
	[object]									NVARCHAR(MAX)					NOT NULL,
	[registration]								NVARCHAR(50)					NULL,
	[statement]									NVARCHAR(MAX)					NOT NULL,
	[isVoidingStatement]						BIT								NOT NULL,
	[isStatementVoided]							BIT								NOT NULL,
	[dtStored]									DATETIME						NOT	NULL,
	[scoreScaled]								FLOAT                           NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-TinCan]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [PK_Data-TinCan] PRIMARY KEY CLUSTERED ([idData-TinCan] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCan_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [FK_Data-TinCan_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCan_IdEndpoint]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [FK_Data-TinCan_IdEndpoint] FOREIGN KEY (idEndpoint) REFERENCES [tblxAPIoAuthConsumer] (idxAPIoAuthConsumer)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCan_Verb]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [FK_Data-TinCan_Verb] FOREIGN KEY (verbId) REFERENCES [tblTinCanVerb] (idVerb)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCan_Data-Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [FK_Data-TinCan_Data-Lesson] FOREIGN KEY ([idData-Lesson]) REFERENCES [tblData-Lesson] ([idData-Lesson])
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-TinCanContextActivities]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-TinCanContextActivities] (
	[idData-TinCanContextActivities]		INT				IDENTITY(1,1)	NOT NULL,
	[idData-TinCan]							INT								NOT NULL,
	[idSite]								INT								NOT NULL,
	[activityId]							NVARCHAR(100)					NOT NULL,
	[activityType]							NVARCHAR(10)					NOT NULL,
	[objectType]							NVARCHAR(30)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-TinCanContextActivities]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanContextActivities] ADD CONSTRAINT [PK_Data-TinCanContextActivities] PRIMARY KEY CLUSTERED ([idData-TinCanContextActivities] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanContextActivities_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanContextActivities] ADD CONSTRAINT [FK_Data-TinCanContextActivities_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanContextActivities_Data-TinCan]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanContextActivities] ADD CONSTRAINT [FK_Data-TinCanContextActivities_Data-TinCan] FOREIGN KEY ([idData-TinCan]) REFERENCES [tblData-TinCan] ([idData-TinCan])
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryFolder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDocumentRepositoryFolder] (
	[idDocumentRepositoryFolder]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[idDocumentRepositoryObjectType]	INT								NOT NULL,
	[idObject]							INT								NOT NULL,
	[name]								NVARCHAR(255)					NOT NULL,
	[isDeleted]							BIT								NULL,
	[dtDeleted]							DATETIME						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DocumentRepositoryFolder]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryFolder] ADD CONSTRAINT [PK_DocumentRepositoryFolder] PRIMARY KEY CLUSTERED ([idDocumentRepositoryFolder] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryFolder_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryFolder] ADD CONSTRAINT [FK_DocumentRepositoryFolder_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryFolder_ObjectType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryFolder] ADD CONSTRAINT [FK_DocumentRepositoryFolder_ObjectType] FOREIGN KEY (idDocumentRepositoryObjectType) REFERENCES [tblDocumentRepositoryObjectType] (idDocumentRepositoryObjectType)

--/**
--FULL TEXT INDEX
--**/
--IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryFolder]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
--AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryFolder]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
--CREATE FULLTEXT INDEX ON tblDocumentRepositoryFolder (
--	name
--	)
--KEY INDEX PK_DocumentRepositoryFolder
--ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotification]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventEmailNotification] (
	[idEventEmailNotification]		INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]						INT									NOT NULL,
	[name]							NVARCHAR(255)						NOT NULL,
	[idEventType]					INT									NOT NULL,
	[idEventTypeRecipient]			INT									NOT NULL,	
	[idObject]						INT									NULL,
	[from]							NVARCHAR(255)						NULL,
	[copyTo]						NVARCHAR(255)						NULL,
	[specificEmailAddress]			NVARCHAR(255)						NULL,
	[isActive]						BIT									NULL,
	[dtActivation]					DATETIME							NULL,
	[interval]						INT									NULL,
	[timeframe]						NVARCHAR(4)							NULL,
	[priority]						INT									NULL,
	[isDeleted]						BIT									NULL,
	[isHTMLBased]					BIT									NULL,
	[attachmentType]				INT
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventEmailNotification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotification ADD CONSTRAINT [PK_EventEmailNotification] PRIMARY KEY CLUSTERED (idEventEmailNotification ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailNotification_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotification ADD CONSTRAINT [FK_EventEmailNotification_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotification]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotification]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEventEmailNotification (
	[name]
)
KEY INDEX PK_EventEmailNotification
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventEmailQueue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventEmailQueue] (
	[idEventEmailQueue]							INT				IDENTITY(1,1)	NOT NULL,
	[idSite]									INT								NOT NULL,
	[idEventLog]								INT								NOT NULL,
	[idEventType]								INT								NOT NULL,
	[idEventEmailNotification]					INT								NOT NULL,
	[idEventTypeRecipient]						INT								NOT NULL,
	[idObject]									INT								NOT NULL, 
	[idObjectRelated]							INT								NOT NULL, 
	[objectType]								NVARCHAR(25)					NULL, 
	[idObjectUser]								INT								NULL,
	[objectUserFullName]						NVARCHAR(512)					NULL,
	[objectUserFirstName]						NVARCHAR(255)					NULL,
	[objectUserLogin]							NVARCHAR(512)					NULL,
	[objectUserEmail]							NVARCHAR(255)					NULL,
	[idRecipient]								INT								NOT NULL,
	[recipientLangString]						NVARCHAR(10)					NOT NULL,
	[recipientFullName]							NVARCHAR(512)					NOT NULL,
	[recipientFirstName]						NVARCHAR(255)					NOT NULL,
	[recipientLogin]							NVARCHAR(512)					NOT NULL,
	[recipientEmail]							NVARCHAR(255)					NOT NULL,
	[recipientTimezone]							INT								NOT NULL, 
	[recipientTimezoneDotNetName]				NVARCHAR(255)					NOT NULL,
	[from]										NVARCHAR(255)					NULL,
	[copyTo]									NVARCHAR(255)					NULL,
	[priority]									INT								NULL,
	[isHTMLBased]								BIT								NULL,
	[dtEvent]									DATETIME						NOT NULL,	-- the date the event occurred
	[dtAction]									DATETIME						NOT NULL,	-- the date the email should be sent
	[dtActivation]								DATETIME						NOT NULL,   -- the date of the email notification's activation
	[dtSent]									DATETIME						NULL,		-- the record of the date the email ACTUALLY sent
	[statusDescription]							NVARCHAR(512)					NULL,
	[attachmentType]							INT
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventEmailQueue]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailQueue ADD CONSTRAINT [PK_EventEmailQueue] PRIMARY KEY CLUSTERED (idEventEmailQueue ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailQueue_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailQueue ADD CONSTRAINT [FK_EventEmailQueue_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailQueue]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailQueue]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEventEmailQueue (
	recipientFullName
)
KEY INDEX PK_EventEmailQueue
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblExceptionLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblExceptionLog] (
	[idExceptionLog]				INT				IDENTITY(1000, 1)	NOT NULL,
	[idSite]						INT									NOT NULL,
	[idUser]						INT									NULL,
	[timestamp]						DATETIME							NOT NULL,
	[page]							NVARCHAR(512)						NOT NULL,
	[exceptionType]					NVARCHAR(512)						NOT NULL,
	[exceptionMessage]				NVARCHAR(MAX)						NOT NULL,
	[exceptionStackTrace]			NVARCHAR(MAX)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ExceptionLog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblExceptionLog ADD CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED (idExceptionLog ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ExceptionLog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblExceptionLog ADD CONSTRAINT [FK_ExceptionLog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblExceptionLog]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblExceptionLog]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblExceptionLog ( 
	[page],
	exceptionType,
	exceptionMessage
)
KEY INDEX PK_ExceptionLog
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportProcessorLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportProcessorLog] (
	[idReportProcessorLog]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idCaller]							INT								NOT NULL,
	[datasetProcedure]					NVARCHAR(50)					NOT NULL,
	[dtExecutionBegin]					DATETIME						NOT NULL,
	[dtExecutionEnd]					DATETIME						NOT NULL,
	[executionDurationSeconds]			FLOAT							NOT NULL,
	[sqlQuery]							NVARCHAR(MAX)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportProcessorLog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportProcessorLog ADD CONSTRAINT [PK_ReportProcessorLog] PRIMARY KEY CLUSTERED (idReportProcessorLog ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportProcessorLog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportProcessorLog ADD CONSTRAINT [FK_ReportProcessorLog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResourceType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblResourceType] (
	[idResourceType]				INT				IDENTITY (101, 1)	NOT NULL, 
	[idSite]						INT									NOT NULL,
	[resourceType]					NVARCHAR(255)						NOT NULL,
	[isDeleted]						BIT									NOT NULL,
	[dtDeleted]						DATETIME							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ResourceType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceType ADD CONSTRAINT [PK_ResourceType] PRIMARY KEY CLUSTERED (idResourceType ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceType_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceType ADD CONSTRAINT [FK_ResourceType_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceType]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceType]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblResourceType (
	[resourceType]
)
KEY INDEX PK_ResourceType
ON Asentia -- insert index to this resource
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRole]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRole] (
	[idRole]					INT				IDENTITY(100,1)		NOT NULL,
	[idSite]					INT									NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRole ADD CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED (idRole ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Role_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRole ADD CONSTRAINT [FK_Role_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRole]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRole]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRole (
	name
)
KEY INDEX PK_Role
ON Asentia -- insert index to this catalog

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblRole] ON

INSERT INTO [tblRole] (
	idRole,
	idSite,
	name
)
SELECT
	idRole,
	idSite,
	name
FROM (
	SELECT			1 AS idRole, 1 AS idSite, 'System Administrator' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblRole R WHERE R.idRole = MAIN.idRole)

SET IDENTITY_INSERT [tblRole] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSet]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSet] (
	[idRuleSet]						INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[isAny]							BIT								NOT NULL,
	[label]							NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSet ADD CONSTRAINT [PK_RuleSet] PRIMARY KEY CLUSTERED (idRuleSet ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSet_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSet ADD CONSTRAINT [FK_RuleSet_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSet]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSet]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSet (
	label
)
KEY INDEX PK_RuleSet
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteAvailableLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteAvailableLanguage] (
	[idSiteAvailableLanguage]			INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL,
	[idLanguage]						INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteAvailableLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteAvailableLanguage ADD CONSTRAINT [PK_SiteAvailableLanguage] PRIMARY KEY CLUSTERED (idSiteAvailableLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteAvailableLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteAvailableLanguage ADD CONSTRAINT [FK_SiteAvailableLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteAvailableLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteAvailableLanguage ADD CONSTRAINT [FK_SiteAvailableLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteLanguage] (
	[idSiteLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idLanguage]					INT								NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL,
	[company]						NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteLanguage ADD CONSTRAINT [PK_SiteLanguage] PRIMARY KEY CLUSTERED (idSiteLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteLanguage ADD CONSTRAINT [FK_SiteLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteLanguage ADD CONSTRAINT [FK_SiteLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteParam]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteParam] (
	[idSiteParam]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[param]						NVARCHAR(255)					NOT NULL,
	[value]						NVARCHAR(255)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteParam]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteParam ADD CONSTRAINT [PK_SiteParam] PRIMARY KEY CLUSTERED (idSiteParam ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteParam_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteParam ADD CONSTRAINT [FK_SiteParam_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)


/**
PUT ALL VALID SITE PARAMS IN A TEMPORARY TABLE
**/

CREATE TABLE #SiteParams (
	idSite	INT,
	[param]	NVARCHAR(255),
	value	NVARCHAR(255)
)

INSERT INTO #SiteParams (
	idSite,
	[param],
	value
)
SELECT
	idSite,
	[param],
	value
FROM (
	SELECT		 1 AS idSite, 'HomePage.Masthead.Logo'										AS [param], 'homepage_logo.png' AS value
	UNION SELECT 1 AS idSite, 'HomePage.Masthead.SecondaryImage'							AS [param], '' AS value
	UNION SELECT 1 AS idSite, 'HomePage.Masthead.BgColor'									AS [param],	'#595d63' AS value
	UNION SELECT 1 AS idSite, 'HomePage.Masthead.BgImage'									AS [param], '' AS value
	UNION SELECT 1 AS idSite, 'Application.Masthead.Icon'									AS [param],	'application_icon.png' AS value
	UNION SELECT 1 AS idSite, 'Application.Masthead.Logo'									AS [param],	'application_logo.png' AS value
	UNION SELECT 1 AS idSite, 'Application.Masthead.BgColor'								AS [param],	'#595d63' AS value
	UNION SELECT 1 AS idSite, 'Footer.Company'												AS [param],	'ICS Learning Group' AS value
	UNION SELECT 1 AS idSite, 'Footer.Email'												AS [param],	'asentia@icslearninggroup.com' AS value
	UNION SELECT 1 AS idSite, 'Footer.Website'												AS [param],	'http://www.icslearninggroup.com' AS value
	UNION SELECT 1 AS idSite, 'System.SelfRegistration.Enabled'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'System.SelfRegistration.Type'								AS [param],	'builtin' AS value
	UNION SELECT 1 AS idSite, 'System.SelfRegistration.UrlPrefix'							AS [param],	'http://' AS value
	UNION SELECT 1 AS idSite, 'System.SelfRegistration.Url'									AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'System.PasswordReset.Enabled'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'System.DefaultLandingPage'									AS [param],	'/dashboard' AS value
	UNION SELECT 1 AS idSite, 'System.DefaultLandingPage.LocalPath'							AS [param],	'/dashboard' AS value
	UNION SELECT 1 AS idSite, 'System.SimultaneousLogin.Enabled'							AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'System.LoginPriority'										AS [param],	'first' AS value
	UNION SELECT 1 AS idSite, 'System.Lockout.Enabled'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.Lockout.Minutes'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'System.Lockout.Attempts'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Purchases.Enabled'							AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Certificates.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Calendar.Enabled'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Feed.Enabled'									AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Transcript.Enabled'							AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.MyCommunities.Enabled'						AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.MyLearningPaths.Enabled'						AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Leaderboards.Enabled'							AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.EnrollmentStatistics.Enabled'			AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.Leaderboards.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.WallModeration.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.TaskProctoring.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.OJTProctoring.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.ILTRosterManagement.Enabled'			AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Reporter.ReportSubscription.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Reporter.ReportShortcuts.Enabled'						AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.ShowPrivateCourseCatalogs'							AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Catalog.RequireLogin'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Catalog.HideCourseCodes'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Catalog.Enable'												AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.Search.Enable'										AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.EventCalendar.Enable'								AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Catalog.Communities.Enable'									AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.StandupTraining.Enable'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.LearningPaths.Enable'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'System.API.Enabled'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.API.Key'												AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'System.API.RequireHTTPS'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.TrackingCode'											AS [param],	'' AS value	
	UNION SELECT 1 AS idSite, 'Ecommerce.Enabled'											AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.ProcessingMethod'									AS [param],	'none' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.ProcessingMethod.Verified'							AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Currency'											AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AuthorizeNet.Mode'									AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AuthorizeNet.Login'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AuthorizeNet.TransactionKey'						AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AuthorizeNet.PublicKey'							AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.PayPal.Mode'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.PayPal.Login'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Visa.Enabled'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.MasterCard.Enabled'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AMEX.Enabled'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.DiscoverCard.Enabled'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Name.Required'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.CreditCardNum.Required'						AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.ExpirationDate.Required'						AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.CVV2.Required'								AS [param],	'' AS value	
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Company.Required'							AS [param],	'' AS value	
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Address.Required'							AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.City.Required'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.StateProvince.Required'						AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.PostalCode.Required'							AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Country.Required'							AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Email.Required'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.ChargeDescription'									AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.CouponCode.GracePeriod'							AS [param],	'' AS value	
	UNION SELECT 1 AS idSite, 'Leaderboards.CourseTotal.Enable'								AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Leaderboards.CourseCredits.Enable'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Leaderboards.CertificateTotal.Enable'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Leaderboards.CertificateCredits.Enable'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Ratings.Course.Enable'										AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Ratings.Course.Publicize'									AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Certifications.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'QuizzesAndSurveys.Enable'									AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.EmailFromAddressOverride'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerNameOverride'							AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerPortOverride'							AS [param],	'0' AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerUseSslOverride'						AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerUsernameOverride'						AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerPasswordOverride'						AS [param],	NULL AS value	
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.On'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Application'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Username'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Password'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.ConsumerSecret'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Plan'											AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.On'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Application'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Username'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Password'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.ConsumerSecret'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Plan'											AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.On'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Application'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Username'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Password'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.ConsumerSecret'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Plan'											AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.On'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Application'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Username'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Password'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Plan'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'System.AccountRegistrationApproval'							AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.PendingUserRegistrations.Enabled'		AS [param], 'False' AS value 
	UNION SELECT 1 AS idSite, 'Widget.Administrator.PendingCourseEnrollments.Enabled'		AS [param], 'True' AS value 
	UNION SELECT 1 AS idSite, 'Widget.Learner.MyCertifications.Enabled'						AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.CertificationTaskProctoring.Enabled'	AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Documents.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.ILTSessions.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.AdministrativeTasks.Enabled'			AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Enrollments.Mode'								AS [param], 'tile' AS value
	UNION SELECT 1 AS idSite, 'System.IPAddressRestriction'									AS [param], 'none' AS value -- possible values are: none, absolute, login	
	UNION SELECT 1 AS idSite, 'Iconset.Active'												AS [param], 'standard' AS value
	UNION SELECT 1 AS idSite, 'Iconset.Preview'												AS [param], '' AS value
	UNION SELECT 1 AS idSite, 'Theme.Active'												AS [param], 'classic' AS value
	UNION SELECT 1 AS idSite, 'Theme.Preview'												AS [param], '' AS value
	---------------------------------------------------------------------------------------------------------------------------------------------------
	-- OpenSesame IntegrationName, IntegrationKey, and IntegrationSecret are set for ICS's OpenSesame reseller account by system default. They're done 
	-- here as site parameters so that we could set client or even portal specific integrations to the client's reseller account if we wanted to (if 
	-- they paid for it). Generally, there should never be portal-specific site params for these, unless we're using the client's OpenSesame reseller 
	-- account.
	UNION SELECT 1 AS idSite, 'OpenSesame.IntegrationName'									AS [param], 'icslearning' AS value
	UNION SELECT 1 AS idSite, 'OpenSesame.IntegrationKey'									AS [param], '4d3ee8a9-35cf-4c79-9235-e6f80f37291f' AS value
	UNION SELECT 1 AS idSite, 'OpenSesame.IntegrationSecret'								AS [param], '99b56932-483c-4b64-a1ec-952ba64c0964' AS value
	---------------------------------------------------------------------------------------------------------------------------------------------------
	UNION SELECT 1 AS idSite, 'OpenSesame.Enable'											AS [param], 'False' AS value -- these are the portal-specific OpenSesame params	
	UNION SELECT 1 AS idSite, 'OpenSesame.TenantID'											AS [param], NULL AS value
	UNION SELECT 1 AS idSite, 'OpenSesame.TenantConsumerKey'								AS [param], NULL AS value
	UNION SELECT 1 AS idSite, 'OpenSesame.TenantConsumerSecret'								AS [param], NULL AS value
	-----------------------------------------------------------------------------------------------------------------------------------------------
	-- Privacy Parameters
	--------------------------------------------------------------------------------------------------------------------------------------------
	UNION SELECT 1 AS idSite, 'Privacy.AllowUserEmailOptOut'								AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Privacy.PermanentlyRemoveDeletedUsers'						AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Privacy.AllowSelfDeletion'									AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Privacy.RequireCookieConsent'								AS [param], 'False' AS value
	-------------------------------------------------------------------------------------------------------------------------------------------------
	-- User Agreement
	--------------------------------------------------------------------------------------------------------------------------------------------
	UNION SELECT 1 AS idSite, 'UserAgreement.Display'										AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'UserAgreement.Required'										AS [param], 'False' AS value

	-------------------------------------------------------------------------------------------------------------------------------------------------
	-- Feature Hooks
	--------------------------------------------------------------------------------------------------------------------------------------------
	UNION SELECT 1 AS idSite, 'Feature.DashboardMode'										AS [param], 'Standard' AS value -- possible values are 
	UNION SELECT 1 AS idSite, 'Feature.System.xAPIEndpoints.Enabled'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.UserAccountMerging.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.UserProfileFiles.Enabled'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.LiteUserFieldConfiguration.Enabled'			AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.UserRegistrationApproval.Enabled'				AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.ObjectSpecificEmailNotifications.Enabled'		AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Interface.ImageEditor.Enabled'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Interface.CSSEditor.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.SelfEnrollmentApproval.Enabled'		AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.TaskModules.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.OJTModules.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.LearningPaths.Enabled'				AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.ILTResources.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.StandaloneILTEnrollment.Enabled'		AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.RestrictedRuleCriteria.Enabled'		AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Feature.Reporting.PDFExport.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.MessageCenter.Enabled'								AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Roles.Rules.Enabled'									AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Groups.CommunityFunctions.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.CourseDiscussion.Enabled'				AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Groups.Documents.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Groups.Discussion.Enabled'							AS [param], 'True' AS value


) MAIN

/**
DELETE UNDEFINED PARAMS FROM TABLE
**/

DELETE FROM tblSiteParam WHERE NOT EXISTS (SELECT 1 FROM #SiteParams WHERE #SiteParams.[param] = tblSiteParam.[param])

/**
INSERT PARAMS INTO TABLE
**/

INSERT INTO tblSiteParam (
	idSite,
	[param],
	value
)
SELECT
	idSite,
	[param],
	value
FROM #SiteParams
WHERE NOT EXISTS (SELECT 1 FROM tblSiteParam SP WHERE SP.idSite = #SiteParams.idSite AND SP.[param] = #SiteParams.[param])

/**
DROP TEMP TABLE
**/
DROP TABLE #SiteParams
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteToDomainAliasLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteToDomainAliasLink] (
	[idSiteToDomainAliasLink]			INT				IDENTITY(1, 1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[domain]							NVARCHAR(255)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteToDomainAliasLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteToDomainAliasLink ADD CONSTRAINT [PK_SiteToDomainAliasLink] PRIMARY KEY CLUSTERED (idSiteToDomainAliasLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteToDomainAliasLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteToDomainAliasLink ADD CONSTRAINT [FK_SiteToDomainAliasLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTraining]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTraining] (
	[idStandUpTraining]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[title]						NVARCHAR(255)					NOT NULL,
	[description]				NVARCHAR(MAX)					NULL,
	[objectives]				NVARCHAR(MAX)					NULL, 
	[isStandaloneEnroll]		BIT								NOT NULL,
	[isRestrictedEnroll]		BIT								NOT NULL,
	[isRestrictedDrop]			BIT								NOT NULL,
	[searchTags]				NVARCHAR(512)					NULL,
	[avatar]					NVARCHAR(255)					NULL,
	[cost]						FLOAT							NULL,
	[dtCreated]					DATETIME						NULL,
	[dtModified]				DATETIME						NULL,
	[isDeleted]					BIT								NULL,
	[dtDeleted]					DATETIME						NULL,
	[shortcode]					NVARCHAR(10)					NULL	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTraining]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTraining] ADD CONSTRAINT [PK_StandUpTraining] PRIMARY KEY CLUSTERED ([idStandUpTraining] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTraining_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTraining] ADD CONSTRAINT [FK_StandUpTraining_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTraining]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTraining]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblStandUpTraining (
	title,
	[description],
	searchTags
)
KEY INDEX PK_StandUpTraining
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticDataSetLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticDataSetLanguage] (
	[idDataSetLanguage]					INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idDataSet]							INT									NOT NULL,
	[idLanguage]						INT									NOT NULL,
	[name]								NVARCHAR(255)						NOT NULL,
	[description]						NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticDataSetLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataSetLanguage ADD CONSTRAINT [PK_AnalyticDataSetLanguage] PRIMARY KEY CLUSTERED (idDataSetLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticDataSetLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataSetLanguage ADD CONSTRAINT [FK_AnalyticDataSetLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticDataSetLanguage_AnalyticDataSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataSetLanguage ADD CONSTRAINT [FK_AnalyticDataSetLanguage_AnalyticDataSet] FOREIGN KEY (idDataSet) REFERENCES [tblAnalyticDataSet] (idDataSet)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticDataSetLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataSetLanguage ADD CONSTRAINT [FK_AnalyticDataSetLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblAnalyticDataSetLanguage] ON

INSERT INTO tblAnalyticDataSetLanguage (
	idDataSetLanguage,
	idSite,
	idDataSet,
	idLanguage,
	name,
	[description]
)
SELECT
	idDataSetLanguage,
	idSite,
	idDataSet,
	idLanguage,
	name,
	[description]
FROM (
	/* ENGLISH */
	SELECT 1 AS idDataSetLanguage, 1 AS idSite, 1 AS idDataSet, 57 AS idLanguage, 'Login Activity' AS name, '[Dataset.LoginActivity]' AS storedProcedureName, 'Complete information of login activities of all registered users.' AS [description]
	UNION SELECT 2 AS idDataSetLanguage, 1 AS idSite, 2 AS idDataSet, 57 AS idLanguage, 'Course Completions' AS name, '[Dataset.CourseCompletions]' AS storedProcedureName, 'Complete information of course enrolment and completion.' AS [description]
	UNION SELECT 3 AS idDataSetLanguage, 1 AS idSite, 3 AS idDataSet, 57 AS idLanguage, 'User And Group Snapshot' AS name, '[Dataset.UserGroupSnapshot]' AS storedProcedureName, 'Complete information of users aNdd groups informations.' AS [description]
	UNION SELECT 6 AS idDataSetLanguage, 1 AS idSite, 6 AS idDataSet, 57 AS idLanguage, 'Use Statistics' AS name, '[Dataset.UseStatistics]' AS storedProcedureName, 'Complete information of users' AS [description]
	UNION SELECT 7 AS idDataSetLanguage, 1 AS idSite, 7 AS idDataSet, 57 AS idLanguage, 'Certificates' AS name, '[Dataset.Certificates]' AS storedProcedureName, 'Certificate award information. Includes certificates that have been earned as well as those that have been manually awarded.' AS [description]
	
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblAnalyticDataSetLanguage DL WHERE DL.idDataSetLanguage = MAIN.idDataSetLanguage)

SET IDENTITY_INSERT [tblAnalyticDataSetLanguage] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCatalogLanguage] (
	[idCatalogLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[idCatalog]					INT								NOT NULL,
	[idLanguage]				INT								NOT NULL,
	[title]						NVARCHAR(255)					NOT NULL,
	[shortDescription]			NVARCHAR(512)					NULL, 
	[longDescription]			NVARCHAR(MAX)					NULL,
	[searchTags]				NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CatalogLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogLanguage ADD CONSTRAINT [PK_CatalogLanguage] PRIMARY KEY CLUSTERED (idCatalogLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogLanguage ADD CONSTRAINT [FK_CatalogLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogLanguage_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogLanguage ADD CONSTRAINT [FK_CatalogLanguage_Catalog] FOREIGN KEY (idCatalog) REFERENCES [tblCatalog] (idCatalog)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogLanguage ADD CONSTRAINT [FK_CatalogLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)
	
/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCatalogLanguage (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_CatalogLanguage
ON Asentia -- insert index to this catalog

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificateLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificateLanguage] (
	[idCertificateLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[idCertificate]				INT								NOT NULL, 
	[idLanguage]				INT								NOT NULL,
	[name]						NVARCHAR(255)					NOT NULL,
	[description]				NVARCHAR(MAX)					NULL,
	[filename]					NVARCHAR(255)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificateLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateLanguage ADD CONSTRAINT [PK_CertificateLanguage] PRIMARY KEY CLUSTERED (idCertificateLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateLanguage ADD CONSTRAINT [FK_CertificateLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateLanguage_Certificate]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateLanguage ADD CONSTRAINT [FK_CertificateLanguage_Certificate] FOREIGN KEY (idCertificate) REFERENCES [tblCertificate] (idCertificate)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateLanguage ADD CONSTRAINT [FK_CertificateLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificateLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificateLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificateLanguage (
	name,
	[description]
)
KEY INDEX PK_CertificateLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeToCatalogLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeToCatalogLink] (
	[idCouponCodeToCatalogLink]				INT		IDENTITY(1,1)		NOT NULL,
	[idCouponCode]							INT							NOT NULL,
	[idCatalog]								INT							NOT NULL	 
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCodeToCatalogLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCatalogLink ADD CONSTRAINT [PK_CouponCodeToCatalogLink] PRIMARY KEY CLUSTERED (idCouponCodeToCatalogLink ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToCatalogLink_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCatalogLink ADD CONSTRAINT [FK_CouponCodeToCatalogLink_CouponCode] FOREIGN KEY (idCouponCode) REFERENCES [tblCouponCode] (idCouponCode)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToCatalogLink_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCatalogLink ADD CONSTRAINT [FK_CouponCodeToCatalogLink_Catalog] FOREIGN KEY (idCatalog) REFERENCES [tblCatalog] (idCatalog)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeToCourseLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeToCourseLink] (
	[idCouponCodeToCourseLink]				INT		IDENTITY(1,1)		NOT NULL,
	[idCouponCode]							INT							NOT NULL,
	[idCourse]								INT							NOT NULL	 
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCodeToCourseLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCourseLink ADD CONSTRAINT [PK_CouponCodeToCourseLink] PRIMARY KEY CLUSTERED (idCouponCodeToCourseLink ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToCourseLink_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCourseLink ADD CONSTRAINT [FK_CouponCodeToCourseLink_CouponCode] FOREIGN KEY (idCouponCode) REFERENCES [tblCouponCode] (idCouponCode)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToCourseLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCourseLink ADD CONSTRAINT [FK_CouponCodeToCourseLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeToStandupTrainingLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeToStandupTrainingLink] (
	[idCouponCodeToStandupTrainingLink]	[int]		IDENTITY(1,1) NOT NULL,
	[idCouponCode]						[int]		NOT NULL,
	[idStandupTraining]					[int]		NOT NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCodeToStandupTrainingLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToStandupTrainingLink ADD CONSTRAINT [PK_CouponCodeToStandupTrainingLink] PRIMARY KEY CLUSTERED (idCouponCodeToStandupTrainingLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToStandupTrainingLink_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToStandupTrainingLink ADD CONSTRAINT [FK_CouponCodeToStandupTrainingLink_CouponCode] FOREIGN KEY (idCouponCode) REFERENCES [tblCouponCode] (idCouponCode)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToStandupTrainingLink_StandupTraining]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToStandupTrainingLink ADD CONSTRAINT [FK_CouponCodeToStandupTrainingLink_StandupTraining] FOREIGN KEY (idStandUpTraining) REFERENCES [tblStandUpTraining] (idStandupTraining)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseApprover] (
	[idCourseApprover]				INT		IDENTITY(1,1)	NOT NULL,
	[idSite]						INT						NOT NULL,
	[idCourse]						INT						NOT NULL, 
	[idUser]						INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseApprover]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseApprover ADD CONSTRAINT [PK_CourseApprover] PRIMARY KEY CLUSTERED (idCourseApprover ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseApprover_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseApprover ADD CONSTRAINT [FK_CourseApprover_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseApprover_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseApprover ADD CONSTRAINT [FK_CourseApprover_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseApprover_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseApprover ADD CONSTRAINT [FK_CourseApprover_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseEnrollmentApprover] (
	[idCourseEnrollmentApprover]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL, 
	[idCourse]							INT						NOT NULL, 
	[idUser]							INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseEnrollmentApprover ADD CONSTRAINT [PK_CourseEnrollmentApprover] PRIMARY KEY CLUSTERED (idCourseEnrollmentApprover ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseEnrollmentApprover_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseEnrollmentApprover ADD CONSTRAINT [FK_CourseEnrollmentApprover_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseEnrollmentApprover_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseEnrollmentApprover ADD CONSTRAINT [FK_CourseEnrollmentApprover_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseEnrollmentApprover_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseEnrollmentApprover ADD CONSTRAINT [FK_CourseEnrollmentApprover_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseExpert]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseExpert] (
	[idCourseExpert]			INT		 IDENTITY(1,1)	NOT NULL,
	[idSite]					INT						NOT NULL,
	[idCourse]					INT						NOT NULL, 
	[idUser]					INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseExpert]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseExpert ADD CONSTRAINT [PK_CourseExpert] PRIMARY KEY CLUSTERED (idCourseExpert ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseExpert_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseExpert ADD CONSTRAINT [FK_CourseExpert_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseExpert_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseExpert ADD CONSTRAINT [FK_CourseExpert_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseExpert_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseExpert ADD CONSTRAINT [FK_CourseExpert_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseFeedMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseFeedMessage] (
	[idCourseFeedMessage]				INT				IDENTITY (1, 1)		NOT NULL, 
	[idSite]							INT									NOT NULL,
	[idCourse]						    INT									NOT NULL,
	[idLanguage]						INT									NULL,
	[idAuthor]							INT									NOT NULL,
	[idParentCourseFeedMessage]			INT									NULL,
	[message]							NVARCHAR(MAX)						NOT NULL,
	[timestamp]							DATETIME							NOT NULL,
	[dtApproved]						DATETIME							NULL,
	[idApprover]						INT                                 NULL,
	[isApproved]						BIT 						        NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [PK_CourseFeedMessage] PRIMARY KEY CLUSTERED (idCourseFeedMessage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_ParentCourseFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_ParentCourseFeedMessage] FOREIGN KEY (idParentCourseFeedMessage) REFERENCES [tblCourseFeedMessage] (idCourseFeedMessage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Author]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Author] FOREIGN KEY (idAuthor) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedMessage_Approver]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedMessage] ADD CONSTRAINT [FK_CourseFeedMessage_Approver] FOREIGN KEY (idApprover) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseFeedModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseFeedModerator] (
	[idCourseFeedModerator]				INT			IDENTITY (1, 1)		NOT NULL,
	[idSite]							INT								NOT NULL,
	[idCourse]						    INT								NOT NULL,
	[idModerator]						INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseFeedModerator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedModerator] ADD CONSTRAINT [PK_CourseFeedModerator] PRIMARY KEY CLUSTERED (idCourseFeedModerator ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedModerator_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedModerator] ADD CONSTRAINT [FK_CourseFeedModerator_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedModerator_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedModerator] ADD CONSTRAINT [FK_CourseFeedModerator_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedModerator_Moderator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedModerator] ADD CONSTRAINT [FK_CourseFeedModerator_Moderator] FOREIGN KEY (idModerator) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseLanguage] (
	[idCourseLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idCourse]						INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL,
	[shortDescription]				NVARCHAR(512)					NULL, 
	[longDescription]				NVARCHAR(MAX)					NULL,
	[objectives]					NVARCHAR(MAX)					NULL,
	[searchTags]					NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseLanguage ADD CONSTRAINT [PK_CourseLanguage] PRIMARY KEY CLUSTERED (idCourseLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseLanguage ADD CONSTRAINT [FK_CourseLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseLanguage_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseLanguage ADD CONSTRAINT [FK_CourseLanguage_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseLanguage ADD CONSTRAINT [FK_CourseLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCourseLanguage (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_CourseLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseRating]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseRating] (
	[idCourseRating]	INT		IDENTITY(1,1)	NOT NULL,
	[idSite]			INT						NOT NULL,
	[idCourse]			INT						NOT NULL, 
	[idUser]			INT						NOT NULL,
	[rating]			INT						NOT NULL,
	[timestamp]			DATETIME				NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseRating]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseRating ADD CONSTRAINT [PK_CourseRating] PRIMARY KEY CLUSTERED ([idCourseRating] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseRating_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseRating ADD CONSTRAINT [FK_CourseRating_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] ([idCourse])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseRating_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseRating ADD CONSTRAINT [FK_CourseRating_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseRating_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseRating ADD CONSTRAINT [FK_CourseRating_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseToCatalogLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseToCatalogLink] (
	[idCourseToCatalogLink]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idCourse]							INT							NOT NULL, 
	[idCatalog]							INT							NOT NULL,
	[order]								INT							NULL	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseToCatalogLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToCatalogLink ADD CONSTRAINT [PK_CourseToCatalogLink] PRIMARY KEY CLUSTERED (idCourseToCatalogLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToCatalogLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToCatalogLink ADD CONSTRAINT [FK_CourseToCatalogLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToCatalogLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToCatalogLink ADD CONSTRAINT [FK_CourseToCatalogLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToCatalogLink_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToCatalogLink ADD CONSTRAINT [FK_CourseToCatalogLink_Catalog] FOREIGN KEY (idCatalog) REFERENCES [tblCatalog] (idCatalog)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseToPrerequisiteLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseToPrerequisiteLink] (
	[idCourseToPrerequisiteLink]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL,
	[idCourse]							INT						NOT NULL, 
	[idPrerequisite]					INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseToPrerequisiteLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToPrerequisiteLink ADD CONSTRAINT [PK_CourseToPrerequisiteLink] PRIMARY KEY CLUSTERED (idCourseToPrerequisiteLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToPrerequisiteLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToPrerequisiteLink ADD CONSTRAINT [FK_CourseToPrerequisiteLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToPrerequisiteLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToPrerequisiteLink ADD CONSTRAINT [FK_CourseToPrerequisiteLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToPrerequisiteLink_Prerequisite]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToPrerequisiteLink ADD CONSTRAINT [FK_CourseToPrerequisiteLink_Prerequisite] FOREIGN KEY (idPrerequisite) REFERENCES [tblCourse] (idCourse)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseToScreenshotLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseToScreenshotLink] (
	[idCourseToScreenshotLink]	INT		 IDENTITY(1,1)	NOT NULL,
	[idSite]					INT						NOT NULL,
	[idCourse]					INT						NOT NULL, 
	[filename]					NVARCHAR(255)			NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseToScreenshotLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToScreenshotLink ADD CONSTRAINT [PK_CourseToScreenshotLink] PRIMARY KEY CLUSTERED (idCourseToScreenshotLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToScreenshotLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToScreenshotLink ADD CONSTRAINT [FK_CourseToScreenshotLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseToScreenshotLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseToScreenshotLink ADD CONSTRAINT [FK_CourseToScreenshotLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEnrollmentRequest]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEnrollmentRequest] (
	[idEnrollmentRequest]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idCourse]							INT							NOT NULL,
	[idUser]							INT							NOT NULL,
	[timestamp]							DATETIME					NOT NULL,
	[dtApproved]						DATETIME					NULL,
	[dtDenied]							DATETIME					NULL,
	[idApprover]						INT							NULL,
	[rejectionComments]					NVARCHAR(MAX)				NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EnrollmentRequest]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [PK_EnrollmentRequest] PRIMARY KEY CLUSTERED (idEnrollmentRequest ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EnrollmentRequest_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [FK_EnrollmentRequest_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EnrollmentRequest_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [FK_EnrollmentRequest_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EnrollmentRequest_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [FK_EnrollmentRequest_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EnrollmentRequest_Approver]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEnrollmentRequest ADD CONSTRAINT [FK_EnrollmentRequest_Approver] FOREIGN KEY (idApprover) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathApprover] (
	[idLearningPathApprover]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]						INT						NOT NULL,
	[idLearningPath]				INT						NOT NULL, 
	[idUser]						INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathApprover]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathApprover ADD CONSTRAINT [PK_LearningPathApprover] PRIMARY KEY CLUSTERED (idLearningPathApprover ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathApprover_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathApprover ADD CONSTRAINT [FK_LearningPathApprover_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathApprover_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathApprover ADD CONSTRAINT [FK_LearningPathApprover_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathApprover_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathApprover ADD CONSTRAINT [FK_LearningPathApprover_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLesson]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLesson] (
	[idLesson]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[idCourse]					INT								NOT NULL, 
	[title]						NVARCHAR(255)					NOT NULL,
	[revcode]					NVARCHAR(32)					NULL,
	[shortDescription]			NVARCHAR(512)					NULL, 
	[longDescription]			NVARCHAR(MAX)					NULL, 
	[dtCreated]					DATETIME						NULL,
	[dtModified]				DATETIME						NULL,
	[isDeleted]					BIT								NOT NULL,
	[dtDeleted]					DATETIME						NULL,
	[order]						INT								NULL,
	[searchTags]				NVARCHAR(512)					NULL,
	[isOptional]				BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLesson ADD CONSTRAINT [PK_Lesson] PRIMARY KEY CLUSTERED ([idLesson] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Lesson_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLesson ADD CONSTRAINT [FK_Lesson_Course] FOREIGN KEY ([idCourse]) REFERENCES [tblCourse] ([idCourse])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Lesson_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLesson ADD CONSTRAINT [FK_Lesson_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLesson]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLesson]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLesson (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_Lesson
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToCourseFeedSubscription]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToCourseFeedSubscription] (
	[idUserToCourseFeedSubscription]		INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idUser]								INT							NOT NULL,
	[idCourse]								INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToCourseFeedSubscription]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToCourseFeedSubscription ADD CONSTRAINT [PK_UserToCourseFeedSubscription] PRIMARY KEY CLUSTERED (idUserToCourseFeedSubscription ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToCourseFeedSubscription_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToCourseFeedSubscription ADD CONSTRAINT [FK_UserToCourseFeedSubscription_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToCourseFeedSubscription_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToCourseFeedSubscription ADD CONSTRAINT [FK_UserToCourseFeedSubscription_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToCourseFeedSubscription_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToCourseFeedSubscription ADD CONSTRAINT [FK_UserToCourseFeedSubscription_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-HomeworkAssignment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-HomeworkAssignment] (
	[idData-HomeworkAssignment]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idData-Lesson]						INT								NOT NULL, 
	[uploadedAssignmentFilename]		NVARCHAR(255)					NOT NULL,
	[dtUploaded]						DATETIME						NOT NULL,
	[completionStatus]					INT								NULL,
	[successStatus]						INT								NULL,
	[score]								INT								NULL,
	[timestamp]							DATETIME						NULL,
	[proctoringUser]					INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-HomeworkAssignment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [PK_Data-HomeworkAssignment] PRIMARY KEY CLUSTERED ([idData-HomeworkAssignment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_Data-Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_Data-Lesson] FOREIGN KEY ([idData-Lesson]) REFERENCES [tblData-Lesson] ([idData-Lesson])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_CompletionStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_CompletionStatus] FOREIGN KEY ([completionStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_SuccessStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_SuccessStatus] FOREIGN KEY ([successStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-HomeworkAssignment_ProctoringUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-HomeworkAssignment] ADD CONSTRAINT [FK_Data-HomeworkAssignment_ProctoringUser] FOREIGN KEY (proctoringUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-SCO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-SCO] (
	[idData-SCO]						INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idData-Lesson]						INT								NOT NULL, 
	[idTimezone]						INT								NOT NULL,
	[manifestIdentifier]				NVARCHAR(255)					NULL,
	[completionStatus]					INT								NOT NULL,
	[successStatus]						INT								NOT NULL,
	[scoreScaled]						FLOAT							NULL, 
	[totalTime]							FLOAT							NULL, 
	[timestamp]							DATETIME						NULL,
	[actualAttemptCount]				INT								NULL,
	[effectiveAttemptCount]				INT								NULL,
	[proctoringUser]					INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-SCO]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [PK_Data-SCO] PRIMARY KEY CLUSTERED ([idData-SCO] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_Data-Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_Data-Lesson] FOREIGN KEY ([idData-Lesson]) REFERENCES [tblData-Lesson] ([idData-Lesson])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_Timezone] FOREIGN KEY ([idTimezone]) REFERENCES [tblTimezone] ([idTimezone])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_CompletionStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_CompletionStatus] FOREIGN KEY ([completionStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_SuccessStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_SuccessStatus] FOREIGN KEY ([successStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCO_ProctoringUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCO] ADD CONSTRAINT [FK_Data-SCO_ProctoringUser] FOREIGN KEY (proctoringUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDatasetLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDatasetLanguage] (
	[idDatasetLanguage]					INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idDataset]							INT									NOT NULL,
	[idLanguage]						INT									NOT NULL,
	[name]								NVARCHAR(255)						NOT NULL,
	[description]						NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DatasetLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatasetLanguage ADD CONSTRAINT [PK_DatasetLanguage] PRIMARY KEY CLUSTERED (idDatasetLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DatasetLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatasetLanguage ADD CONSTRAINT [FK_DatasetLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DatasetLanguage_Dataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatasetLanguage ADD CONSTRAINT [FK_DatasetLanguage_Dataset] FOREIGN KEY (idDataset) REFERENCES [tblDataset] (idDataset)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DatasetLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatasetLanguage ADD CONSTRAINT [FK_DatasetLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
INSERT VALUES
**/
DELETE FROM tblDatasetLanguage

SET IDENTITY_INSERT [tblDatasetLanguage] ON

INSERT INTO tblDatasetLanguage (
	idDatasetLanguage,
	idSite,
	idDataset,
	idLanguage,
	name,
	[description]
)
SELECT
	idDatasetLanguage,
	idSite,
	idDataset,
	idLanguage,
	name,
	[description]
FROM (
	/* en-US */
	SELECT 1 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 57 AS idLanguage, N'User Demographics' AS name, N'Complete user account information for all users including name, username, email, contact information, organizational information and more.' AS [description]
	UNION SELECT 2 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 57 AS idLanguage, N'User Course Transcripts' AS name, N'Course and lesson status, and completion information including scores and dates.' AS [description]
	UNION SELECT 3 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 57 AS idLanguage, N'Catalog and Course Information' AS name, N'Information for catalogs, courses, lessons, and ILT sessions.' AS [description]
	UNION SELECT 4 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 57 AS idLanguage, N'Certificates' AS name, N'Certificate award information. Includes certificates that have been earned as well as those that have been manually awarded.' AS [description]
	UNION SELECT 5 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 57 AS idLanguage, N'xAPI' AS name, N'xAPI statments recorded to the system''s learning record store from outside training.' AS [description]
	UNION SELECT 6 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 57 AS idLanguage, N'User Learning Path Transcripts' AS name, N'Learning Path status and completion information.' AS [description]
	UNION SELECT 7 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 57 AS idLanguage, N'User ILT Transcripts' AS name, N'Instructor Led Training status and completion information including scores and dates.' AS [description]
	UNION SELECT 8 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 57 AS idLanguage, N'Purchases' AS name, N'Information on purchases made by users within the system.' AS [description]
	UNION SELECT 9 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 57 AS idLanguage, N'User Certification Transcripts' AS name, N'Certification status information.' AS [description]
	/* es-MX */
	UNION SELECT 10 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 70 AS idLanguage, N'Datos demográficos del usuario' AS name, N'Complete la información de la cuenta de usuario para todos los usuarios, incluidos el nombre, el nombre de usuario, el correo electrónico, la información de contacto, la información de la organización y más.' AS [description]
	UNION SELECT 11 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 70 AS idLanguage, N'Transcripciones del curso del usuario' AS name, N'Curso y estado de la lección, e información de finalización que incluye puntajes y fechas.' AS [description]
	UNION SELECT 12 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 70 AS idLanguage, N'Catálogo e información del curso' AS name, N'Información para catálogos, cursos, lecciones y sesiones ILT.' AS [description]
	UNION SELECT 13 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 70 AS idLanguage, N'Certificados' AS name, N'Información de adjudicación de certificado. Incluye los certificados que se han obtenido, así como los que se han otorgado manualmente.' AS [description]
	UNION SELECT 14 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 70 AS idLanguage, N'xAPI' AS name, N'Declaraciones de xAPI registradas en la tienda de discos de aprendizaje del sistema desde fuera del entrenamiento.' AS [description]
	UNION SELECT 15 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 70 AS idLanguage, N'Transcripciones de la ruta de aprendizaje del usuario' AS name, N'Aprendizaje de ruta de acceso e información de finalización.' AS [description]
	UNION SELECT 16 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 70 AS idLanguage, N'Transcripciones ILT de usuario' AS name, N'El instructor llevó el estado del entrenamiento y la información de finalización, incluidos puntajes y fechas.' AS [description]
	UNION SELECT 17 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 70 AS idLanguage, N'Compras' AS name, N'Información sobre las compras realizadas por los usuarios dentro del sistema.' AS [description]
	UNION SELECT 18 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 70 AS idLanguage, N'Transcripciones de certificación de usuario' AS name, N'Información de estado de certificación.' AS [description]
	/* fr-FR */
	UNION SELECT 19 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 89 AS idLanguage, N'Démographie de l''utilisateur' AS name, N'Informations complètes sur le compte utilisateur pour tous les utilisateurs, y compris le nom, le nom d''utilisateur, le courrier électronique, les informations de contact, les informations sur l''organisation et plus encore.' AS [description]
	UNION SELECT 20 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 89 AS idLanguage, N'Transcriptions de cours d''utilisateur' AS name, N'L''état du cours et de la leçon, ainsi que les informations d''achèvement, y compris les scores et les dates.' AS [description]
	UNION SELECT 21 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 89 AS idLanguage, N'Catalogue et informations sur le cours' AS name, N'Informations pour les catalogues, les cours, les leçons et les sessions ILT.' AS [description]
	UNION SELECT 22 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 89 AS idLanguage, N'Certificats' AS name, N'Informations sur le prix du certificat Inclut les certificats qui ont été gagnés ainsi que ceux qui ont été attribués manuellement.' AS [description]
	UNION SELECT 23 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 89 AS idLanguage, N'xAPI' AS name, N'Statements xAPI enregistrés dans le magasin de disques d''apprentissage du système à partir d''une formation externe.' AS [description]
	UNION SELECT 24 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 89 AS idLanguage, N'Transcriptions de parcours d''apprentissage utilisateur' AS name, N'Statut du chemin d''apprentissage et informations d''achèvement.' AS [description]
	UNION SELECT 25 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 89 AS idLanguage, N'Transcriptions d''ILT d''utilisateur' AS name, N'Instructeur dirigé Statut de formation et informations d''achèvement, y compris les scores et les dates.' AS [description]
	UNION SELECT 26 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 89 AS idLanguage, N'Achats' AS name, N'Informations sur les achats effectués par les utilisateurs dans le système.' AS [description]
	UNION SELECT 27 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 89 AS idLanguage, N'Transcriptions de certification d''utilisateur' AS name, N'Informations sur l''état de la certification' AS [description]
	/* it-IT */
	UNION SELECT 28 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 111 AS idLanguage, N'Demografia utente' AS name, N'Informazioni complete sull''account utente per tutti gli utenti tra cui nome, nome utente, email, informazioni di contatto, informazioni organizzative e altro.' AS [description]
	UNION SELECT 29 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 111 AS idLanguage, N'Trascrizioni del corso dell''utente' AS name, N'Stato del corso e della lezione e informazioni sul completamento, compresi punteggi e date.' AS [description]
	UNION SELECT 30 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 111 AS idLanguage, N'Catalogo e informazioni sui corsi' AS name, N'Informazioni per cataloghi, corsi, lezioni e sessioni ILT.' AS [description]
	UNION SELECT 31 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 111 AS idLanguage, N'certificati' AS name, N'Informazioni sui premi del certificato. Include i certificati che sono stati guadagnati e quelli che sono stati assegnati manualmente.' AS [description]
	UNION SELECT 32 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 111 AS idLanguage, N'xAPI' AS name, N'Le registrazioni xAPI registrate nel negozio di record di apprendimento del sistema dall''addestramento esterno.' AS [description]
	UNION SELECT 33 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 111 AS idLanguage, N'Trascrizioni del percorso di apprendimento dell''utente' AS name, N'Stato del percorso di apprendimento e informazioni sul completamento.' AS [description]
	UNION SELECT 34 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 111 AS idLanguage, N'Trascrizioni ILT utente' AS name, N'Stato di istruttore e informazioni sullo stato di completamento del corso inclusi punteggi e date.' AS [description]
	UNION SELECT 35 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 111 AS idLanguage, N'acquisti' AS name, N'Informazioni sugli acquisti effettuati dagli utenti all''interno del sistema.' AS [description]
	UNION SELECT 36 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 111 AS idLanguage, N'Trascrizioni di certificazione utente' AS name, N'Informazioni sullo stato della certificazione.' AS [description]
	/* zh-CN */
	UNION SELECT 37 AS idDatasetLanguage, 1 AS idSite, 1 AS idDataset, 204 AS idLanguage, N'用户属性' AS name, N'为所有用户提供完整的用户帐户信息，包括姓名、用户名、电子邮件、联系信息和组织信息等。' AS [description]
	UNION SELECT 38 AS idDatasetLanguage, 1 AS idSite, 2 AS idDataset, 204 AS idLanguage, N'用户课程成绩' AS name, N'课程、课堂状态和包括分数和日期的完成信息。' AS [description]
	UNION SELECT 39 AS idDatasetLanguage, 1 AS idSite, 3 AS idDataset, 204 AS idLanguage, N'目录课程信息' AS name, N'有关目录、课程、课堂和讲师指导培训场次的信息。' AS [description]
	UNION SELECT 40 AS idDatasetLanguage, 1 AS idSite, 4 AS idDataset, 204 AS idLanguage, N'证书' AS name, N'证书颁发信息。包括已经获得的证书以及人工颁发的证书。' AS [description]
	UNION SELECT 41 AS idDatasetLanguage, 1 AS idSite, 5 AS idDataset, 204 AS idLanguage, N'xAPI' AS name, N'xAPI陈述将系统外培训信息记录到系统的学习记录。' AS [description]
	UNION SELECT 42 AS idDatasetLanguage, 1 AS idSite, 6 AS idDataset, 204 AS idLanguage, N'用户学习路径成绩' AS name, N'学习路径状态和包括分数和日期的完成信息。' AS [description]
	UNION SELECT 43 AS idDatasetLanguage, 1 AS idSite, 7 AS idDataset, 204 AS idLanguage, N'讲师指导培训成绩' AS name, N'讲师指导培训状态和包括分数和日期的完成认证信息。' AS [description]
	UNION SELECT 44 AS idDatasetLanguage, 1 AS idSite, 8 AS idDataset, 204 AS idLanguage, N'购买' AS name, N'用户在系统内的购买信息。' AS [description]
	UNION SELECT 45 AS idDatasetLanguage, 1 AS idSite, 9 AS idDataset, 204 AS idLanguage, N'用户认证成绩' AS name, N'认证状态信息。' AS [description]
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblDatasetLanguage DL WHERE DL.idDatasetLanguage = MAIN.idDatasetLanguage)

SET IDENTITY_INSERT [tblDatasetLanguage] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotificationLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventEmailNotificationLanguage] (
	[idEventEmailNotificationLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL,
	[idEventEmailNotification]				INT								NOT NULL, 
	[idLanguage]							INT								NOT NULL,
	[name]									NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventEmailNotificationLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotificationLanguage ADD CONSTRAINT [PK_EventEmailNotificationLanguage] PRIMARY KEY CLUSTERED (idEventEmailNotificationLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailNotificationLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotificationLanguage ADD CONSTRAINT [FK_EventEmailNotificationLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailNotificationLanguage_EventEmailNotification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotificationLanguage ADD CONSTRAINT [FK_EventEmailNotificationLanguage_EventEmailNotification] FOREIGN KEY (idEventEmailNotification) REFERENCES [tblEventEmailNotification] (idEventEmailNotification)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventEmailNotificationLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventEmailNotificationLanguage ADD CONSTRAINT [FK_EventEmailNotificationLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotificationLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventEmailNotificationLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEventEmailNotificationLanguage (
	name
)
KEY INDEX PK_EventEmailNotificationLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCatalogAccessToGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCatalogAccessToGroupLink] (
	[idCatalogAccessToGroupLink]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idGroup]								INT							NOT NULL,
	[idCatalog]								INT							NOT NULL, 	
	[created]								DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CatalogAccessToGroupLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogAccessToGroupLink ADD CONSTRAINT [PK_CatalogAccessToGroupLink] PRIMARY KEY CLUSTERED (idCatalogAccessToGroupLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogAccessToGroupLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogAccessToGroupLink ADD CONSTRAINT [FK_CatalogAccessToGroupLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogAccessToGroupLink_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogAccessToGroupLink ADD CONSTRAINT [FK_CatalogAccessToGroupLink_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupLanguage] (
	[idGroupLanguage]				INT				IDENTITY (1, 1)		NOT NULL, 
	[idSite]						INT									NOT NULL,
	[idGroup]						INT									NOT NULL,
	[idLanguage]					INT									NOT NULL,
	[name]							NVARCHAR(255)						NOT NULL,
	[shortDescription]				NVARCHAR(512)						NULL,
	[longDescription]				NVARCHAR(MAX)						NULL,
	[searchTags]					NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupLanguage ADD CONSTRAINT [PK_GroupLanguage] PRIMARY KEY CLUSTERED (idGroupLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupLanguage ADD CONSTRAINT [FK_GroupLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupLanguage_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupLanguage ADD CONSTRAINT [FK_GroupLanguage_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupLanguage ADD CONSTRAINT [FK_GroupLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblGroupLanguage (
	name,
	shortDescription,
	searchTags
)
KEY INDEX PK_GroupLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetToGroupLink] (
	[idRuleSetToGroupLink]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idRuleSet]						INT							NOT NULL,
	[idGroup]						INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToGroupLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToGroupLink ADD CONSTRAINT PK_RuleSetToGroupLink PRIMARY KEY CLUSTERED (idRuleSetToGroupLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToGroupLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToGroupLink ADD CONSTRAINT [FK_RuleSetToGroupLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToGroupLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToGroupLink ADD CONSTRAINT FK_RuleSetToGroupLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToGroupLink_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToGroupLink ADD CONSTRAINT FK_RuleSetToGroupLink_Group FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeToLearningPathLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeToLearningPathLink] (
	[idCouponCodeToLearningPathLink]	[int]	 IDENTITY(1,1) NOT NULL,
	[idCouponCode]						[int]	 NOT NULL,
	[idLearningPath]					[int]	NOT NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCodeToLearningPathLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToLearningPathLink ADD CONSTRAINT [PK_CouponCodeToLearningPathLink] PRIMARY KEY CLUSTERED (idCouponCodeToLearningPathLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToLearningPathLink_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToLearningPathLink ADD CONSTRAINT [FK_CouponCodeToLearningPathLink_CouponCode] FOREIGN KEY (idCouponCode) REFERENCES [tblCouponCode] (idCouponCode)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToLearningPathLink_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToLearningPathLink ADD CONSTRAINT [FK_CouponCodeToLearningPathLink_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathEnrollmentApprover] (
	[idLearningPathEnrollmentApprover]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]								INT						NOT NULL,
	[idLearningPath]						INT						NOT NULL, 
	[idUser]								INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathEnrollmentApprover ADD CONSTRAINT [PK_LearningPathEnrollmentApprover] PRIMARY KEY CLUSTERED (idLearningPathEnrollmentApprover ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollmentApprover_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathEnrollmentApprover ADD CONSTRAINT [FK_LearningPathEnrollmentApprover_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollmentApprover_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathEnrollmentApprover ADD CONSTRAINT [FK_LearningPathEnrollmentApprover_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollmentApprover_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathEnrollmentApprover ADD CONSTRAINT [FK_LearningPathEnrollmentApprover_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathFeed]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathFeed] (
	[idLearningPathFeed]				INT		IDENTITY (1, 1)		NOT NULL,
	[idSite]							INT							NOT NULL,
	[idLearningPath]					INT							NOT NULL,
	[isActive]							BIT							NULL,
	[isModerated]						BIT							NULL,
	[isOpenSubscription]				BIT							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathFeed]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeed] ADD CONSTRAINT [PK_LearningPathFeed] PRIMARY KEY CLUSTERED (idLearningPathFeed ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeed_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeed] ADD CONSTRAINT [FK_LearningPathFeed_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeed_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeed] ADD CONSTRAINT [FK_LearningPathFeed_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathLanguage] (
	[idLearningPathLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[idLearningPath]					INT								NOT NULL, 
	[idLanguage]						INT								NOT NULL, 
	[name]								NVARCHAR(255)					NOT NULL,
	[shortDescription]					NVARCHAR(512)					NULL, 
	[longDescription]					NVARCHAR(MAX)					NULL,
	[searchTags]						NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathLanguage ADD CONSTRAINT [PK_LearningPathLanguage] PRIMARY KEY CLUSTERED (idLearningPathLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathLanguage ADD CONSTRAINT [FK_LearningPathLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathLanguage_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathLanguage ADD CONSTRAINT [FK_LearningPathLanguage_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathLanguage ADD CONSTRAINT [FK_LearningPathLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLearningPathLanguage (
	name,
	shortDescription,
	searchTags
)
KEY INDEX PK_LearningPathLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathToCourseLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathToCourseLink] (
	[idLearningPathToCourseLink]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL,
	[idLearningPath]					INT						NOT NULL,
	[idCourse]							INT						NOT NULL,
	[order]								INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathToCourseLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathToCourseLink ADD CONSTRAINT [PK_LearningPathToCourseLink] PRIMARY KEY CLUSTERED (idLearningPathToCourseLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathToCourseLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathToCourseLink ADD CONSTRAINT [FK_LearningPathToCourseLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathToCourseLink_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathToCourseLink ADD CONSTRAINT FK_LearningPathToCourseLink_LearningPath FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathToCourseLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathToCourseLink ADD CONSTRAINT FK_LearningPathToCourseLink_Course FOREIGN KEY ([idCourse]) REFERENCES [tblCourse] ([idCourse])
/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToLearningPathLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToLearningPathLink] (
	[idUserToLearningPathLink]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idUser]								INT							NOT NULL,
	[idLearningPath]						INT							NOT NULL,
	[idRuleSet]								INT							NULL,
	[created]								DATETIME					NOT NULL
)

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToLearningPathLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToLearningPathLink ADD CONSTRAINT [PK_UserToLearningPathLink] PRIMARY KEY CLUSTERED (idUserToLearningPathLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToLearningPathLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToLearningPathLink ADD CONSTRAINT [FK_UserToLearningPathLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToLearningPathLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToLearningPathLink ADD CONSTRAINT FK_UserToLearningPathLink_User FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToLearningPathLink_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToLearningPathLink ADD CONSTRAINT FK_UserToLearningPathLink_LearningPath FOREIGN KEY ([idLearningPath]) REFERENCES [tblLearningPath] ([idLearningPath])
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResourceTypeLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblResourceTypeLanguage] (
	[idResourceTypeLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idResourceType]				INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[resourceType]							NVARCHAR(255)					NULL
	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ResourceTypeLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceTypeLanguage ADD CONSTRAINT [PK_ResourceTypeLanguage] PRIMARY KEY CLUSTERED (idResourceTypeLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceTypeLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceTypeLanguage ADD CONSTRAINT [FK_ResourceTypeLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceTypeLanguage_ResourceType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceTypeLanguage ADD CONSTRAINT [FK_ResourceTypeLanguage_ResourceType] FOREIGN KEY (idResourceType) REFERENCES [tblResourceType] (idResourceType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceTypeLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceTypeLanguage ADD CONSTRAINT [FK_ResourceTypeLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceTypeLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceTypeLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblResourceTypeLanguage (
	resourceType
)
KEY INDEX PK_ResourceTypeLanguage
ON Asentia -- insert index to this resource type
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupToRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupToRoleLink] (
	[idGroupToRoleLink]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL, 
	[idGroup]						INT							NOT NULL,
	[idRole]						INT							NOT NULL,
	[idRuleSet]						INT							NULL,
	[created]						DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupToRoleLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupToRoleLink ADD CONSTRAINT [PK_GroupToRoleLink] PRIMARY KEY CLUSTERED (idGroupToRoleLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupToRoleLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupToRoleLink ADD CONSTRAINT [FK_GroupToRoleLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupToRoleLink_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupToRoleLink ADD CONSTRAINT [FK_GroupToRoleLink_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupToRoleLink_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupToRoleLink ADD CONSTRAINT [FK_GroupToRoleLink_Role] FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRoleLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRoleLanguage] (
	[idRoleLanguage]			INT				IDENTITY(100,1)		NOT NULL, 
	[idSite]					INT									NOT NULL,
	[idRole]					INT									NOT NULL,
	[idLanguage]				INT									NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RoleLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleLanguage ADD CONSTRAINT [PK_RoleLanguage] PRIMARY KEY CLUSTERED (idRoleLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleLanguage ADD CONSTRAINT [FK_RoleLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleLanguage_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleLanguage ADD CONSTRAINT [FK_RoleLanguage_Role] FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleLanguage ADD CONSTRAINT [FK_RoleLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRoleLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRoleLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRoleLanguage (
	name
)
KEY INDEX PK_RoleLanguage
ON Asentia -- insert index to this catalog

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblRoleLanguage] ON

INSERT INTO [tblRoleLanguage] (
	idRoleLanguage,
	idSite,
	idRole,
	idLanguage,
	name
)
SELECT
	idRoleLanguage,
	idSite,
	idRole,
	idLanguage,
	name
FROM (
	SELECT			1 AS idRoleLanguage, 1 AS idSite, 1 AS idRole, 57 AS idLanguage, 'System Administrator' AS name
	UNION SELECT	2 AS idRoleLanguage, 1 AS idSite, 1 AS idRole, 67 AS idLanguage, 'Administrador de Sistemas' AS name
	UNION SELECT	3 AS idRoleLanguage, 1 AS idSite, 1 AS idRole, 89 AS idLanguage, 'Administrateur du Syst�me' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblRoleLanguage RL WHERE RL.idRoleLanguage = MAIN.idRoleLanguage)

SET IDENTITY_INSERT [tblRoleLanguage] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRoleToPermissionLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRoleToPermissionLink] (
	[idRoleToPermissionLink]		INT		IDENTITY (1,1)		NOT NULL,
	[idSite]						INT							NOT NULL,
	[idRole]						INT							NOT NULL,
	[idPermission]					INT							NOT NULL,
	[scope]							NVARCHAR(MAX)				NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RoleToPermissionLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleToPermissionLink ADD CONSTRAINT [PK_RoleToPermissionLink] PRIMARY KEY CLUSTERED (idRoleToPermissionLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleToPermissionLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleToPermissionLink ADD CONSTRAINT [FK_RoleToPermissionLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleToPermissionLink_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleToPermissionLink ADD CONSTRAINT [FK_RoleToPermissionLink_Role] FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleToPermissionLink_Permission]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleToPermissionLink ADD CONSTRAINT [FK_RoleToPermissionLink_Permission] FOREIGN KEY (idPermission) REFERENCES [tblPermission] (idPermission)

/**
INSERT VALUES
**/

INSERT INTO [tblRoleToPermissionLink] (
	idSite,
	idRole,
	idPermission,
	scope
)
SELECT
	idSite,
	idRole,
	idPermission,
	scope
FROM (
	SELECT			1 AS idSite, 1 AS idRole, 1 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 2 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 3 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 4 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 5 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 6 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 7 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 8 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 9 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 10 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 11 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 101 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 102 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 103 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 104 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 105 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 106 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 107 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 108 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 109 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 110 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 111 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 112 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 113 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 114 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 115 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 201 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 202 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 203 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 204 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 205 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 301 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 302 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 303 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 304 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 305 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 306 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 307 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 308 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 309 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 310 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 401 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 402 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 403 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 404 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 405 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 406 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 407 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 408 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 409 AS idPermission, NULL AS scope
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM [tblRoleToPermissionLink] RPL WHERE RPL.idPermission = MAIN.idPermission AND RPL.idRole = MAIN.idRole)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE tblRuleSetToRoleLink (
	[idRuleSetToRoleLink]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idRuleSet]						INT							NOT NULL,
	[idRole]						INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToRoleLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRoleLink ADD CONSTRAINT PK_RuleSetToRoleLink PRIMARY KEY CLUSTERED (idRuleSetToRoleLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRoleLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRoleLink ADD CONSTRAINT [FK_RuleSetToRoleLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRoleLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRoleLink ADD CONSTRAINT FK_RuleSetToRoleLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRoleLink_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRoleLink ADD CONSTRAINT FK_RuleSetToRoleLink_Role FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToRoleLink] (
	[idUserToRoleLink]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idUser]						INT							NOT NULL,
	[idRole]						INT							NOT NULL,
	[idRuleSet]						INT							NULL,		-- this value indicates the ruleset that is responsible for this link. NULL means the role was manually assigned.
	[created]						DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserRoleLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRoleLink ADD CONSTRAINT [PK_UserRoleLink] PRIMARY KEY CLUSTERED (idUserToRoleLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRoleLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRoleLink ADD CONSTRAINT [FK_UserToRoleLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRoleLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRoleLink ADD CONSTRAINT [FK_UserToRoleLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRoleLink_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRoleLink ADD CONSTRAINT [FK_UserToRoleLink_Role] FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRule]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRule] (
	[idRule]						INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idRuleSet]						INT								NOT NULL,
	[userField]						NVARCHAR(25)					NULL,
	[operator]						NVARCHAR(25)					NOT NULL,
	[textValue]						NVARCHAR(255)					NULL,
	[dateValue]						DATETIME						NULL,
	[numValue]						INT								NULL,
	[bitValue]						BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Rule]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRule ADD CONSTRAINT [PK_Rule] PRIMARY KEY CLUSTERED (idRule ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Rule_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRule ADD CONSTRAINT [FK_Rule_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Rule_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRule ADD CONSTRAINT [FK_Rule_RuleSet] FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetLanguage] (
	[idRuleSetLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idRuleSet]						INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[label]							NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLanguage ADD CONSTRAINT [PK_RuleSetLanguage] PRIMARY KEY CLUSTERED (idRuleSetLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLanguage ADD CONSTRAINT [FK_RuleSetLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLanguage_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLanguage ADD CONSTRAINT [FK_RuleSetLanguage_RuleSet] FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLanguage ADD CONSTRAINT [FK_RuleSetLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetLanguage (
	label
)
KEY INDEX PK_RuleSetLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTrainingLanguage] (
	[idStandUpTrainingLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idStandUpTraining]					INT								NOT NULL,
	[idSite]							INT								NOT NULL,
	[idLanguage]						INT								NOT NULL,
	[title]								NVARCHAR(255)					NOT NULL,
	[description]						NVARCHAR(MAX)					NULL,
	[objectives]						NVARCHAR(MAX)					NULL, 
	[searchTags]						NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTrainingLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingLanguage ADD CONSTRAINT [PK_StandUpTrainingLanguage] PRIMARY KEY CLUSTERED (idStandUpTrainingLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingLanguage_StandUpTraining]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingLanguage ADD CONSTRAINT [FK_StandUpTrainingLanguage_StandUpTraining] FOREIGN KEY (idStandUpTraining) REFERENCES [tblStandUpTraining] (idStandUpTraining)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingLanguage ADD CONSTRAINT [FK_StandUpTrainingLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingLanguage ADD CONSTRAINT [FK_StandUpTrainingLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblStandUpTrainingLanguage (
	title,
	[description],
	searchTags
)
KEY INDEX PK_StandUpTrainingLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalytic]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalytic] (
	[idAnalytic]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idUser]						INT								NOT NULL, 
	[idDataset]						INT								NOT NULL, 
	[idChartType]                   INT                             NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL,
	[excludeDays]					NVARCHAR(255)				    NULL,
	[isPublic]						BIT								NOT NULL,
	[frequency]                     INT                             NULL,
	[dtStart]						DATETIME						NULL,
	[activity]                      INT                             NULL,
	[dtEnd]				       	    DATETIME						NULL,
	[dtCreated]						DATETIME						NULL,
	[dtModified]					DATETIME						NULL,
	[isDeleted]						BIT								NULL,
	[dtDeleted]						DATETIME						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Analytic]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalytic ADD CONSTRAINT [PK_Analytic] PRIMARY KEY CLUSTERED (idAnalytic ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Analytic_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalytic ADD CONSTRAINT [FK_Analytic_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Analytic_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalytic ADD CONSTRAINT [FK_Analytic_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Report_Dataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalytic ADD CONSTRAINT [FK_Analytic_Dataset] FOREIGN KEY (idDataset) REFERENCES [tblDataset] (idDataset)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAnalytic]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAnalytic]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblAnalytic (
	title
)
KEY INDEX PK_Analytic
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticToCourseLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticToCourseLink] (
	[idAnalyticToCourseLink]			INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL,
	[idAnalytic]						INT						NOT NULL,
	[idCourse]							INT						NOT NULL,
	[order]								INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticToCourseLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticToCourseLink ADD CONSTRAINT [PK_AnalyticToCourseLink] PRIMARY KEY CLUSTERED (idAnalyticToCourseLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticToCourseLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticToCourseLink ADD CONSTRAINT [FK_AnalyticToCourseLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticToCourseLink_Analytic]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticToCourseLink ADD CONSTRAINT FK_AnalyticToCourseLink_Analytic FOREIGN KEY (idAnalytic) REFERENCES [tblAnalytic] (idAnalytic)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticToCourseLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticToCourseLink ADD CONSTRAINT FK_AnalyticToCourseLink_Course FOREIGN KEY ([idCourse]) REFERENCES [tblCourse] ([idCourse])
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificateImport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificateImport] (
	[idCertificateImport]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idUser]							INT								NOT NULL,
	[timestamp]							DATETIME						NOT NULL,
	[idUserImported]					INT								NULL,
	[importFileName]					NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificateImport]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateImport ADD CONSTRAINT [PK_CertificateImport] PRIMARY KEY CLUSTERED (idCertificateImport ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateImport_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateImport ADD CONSTRAINT [FK_CertificateImport_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateImport_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateImport ADD CONSTRAINT [FK_CertificateImport_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateImport_UserImported]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateImport ADD CONSTRAINT [FK_CertificateImport_UserImported] FOREIGN KEY (idUserImported) REFERENCES [tblUser] (idUser)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificateImport]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificateImport]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificateImport (
	importFileName
)
KEY INDEX PK_CertificateImport
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificateRecord]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificateRecord] (
	[idCertificateRecord]		INT					IDENTITY(1,1)	NOT NULL,
    [idSite]					INT									NOT NULL,
	[idCertificate]				INT									NOT NULL,	
	[idUser]					INT									NOT NULL,
	[idTimezone]				INT									NOT NULL,
	[idAwardedBy]				INT									NULL,
	[timestamp]					DATETIME							NOT NULL,
	[expires]					DATETIME							NULL,
	[code]						NVARCHAR(25)						NULL,
	[credits]					FLOAT								NULL,
	[awardData]					NVARCHAR(MAX)						NULL,
	[idCertificateImport]		INT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificateRecord]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [PK_CertificateRecord] PRIMARY KEY CLUSTERED (idCertificateRecord ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_Certificate]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_Certificate] FOREIGN KEY (idCertificate) REFERENCES [tblCertificate] (idCertificate)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_TimeZone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_TimeZone] FOREIGN KEY (idTimezone) REFERENCES [tblTimeZone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_CertificateImport]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_CertificateImport] FOREIGN KEY (idCertificateImport) REFERENCES [tblCertificateImport] (idCertificateImport)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDocumentRepositoryItem] (
	[idDocumentRepositoryItem]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[idDocumentRepositoryObjectType]	INT								NOT NULL,
	[idObject]							INT								NOT NULL,
	[idDocumentRepositoryFolder]		INT								NULL,
	[idOwner]							INT								NOT NULL,
	[label]                             NVARCHAR(255)                   NOT NULL,
	[fileName]							NVARCHAR(255)					NOT NULL,
	[kb]								INT								NOT NULL,
	[searchTags]						NVARCHAR(512)					NULL,
	[isPrivate]							BIT								NULL,
	[idLanguage]						INT								NULL,
	[isAllLanguages]					BIT								NULL,
	[dtCreated]							DATETIME						NOT NULL,
	[isDeleted]							BIT								NULL,
	[dtDeleted]							DATETIME						NULL,
	[isVisibleToUser]					BIT								NULL,
	[isUploadedByUser]					BIT								NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DocumentRepositoryItem]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [PK_DocumentRepositoryItem] PRIMARY KEY CLUSTERED ([idDocumentRepositoryItem] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_ObjectType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_ObjectType] FOREIGN KEY (idDocumentRepositoryObjectType) REFERENCES [tblDocumentRepositoryObjectType] (idDocumentRepositoryObjectType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_DocumentRepositoryFolder]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_DocumentRepositoryFolder] FOREIGN KEY (idDocumentRepositoryFolder) REFERENCES [tblDocumentRepositoryFolder] (idDocumentRepositoryFolder)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_Owner]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_Owner] FOREIGN KEY (idOwner) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItem]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItem]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblDocumentRepositoryItem (
	[fileName],
	label,
	searchTags
)
KEY INDEX PK_DocumentRepositoryItem
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItemLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDocumentRepositoryItemLanguage] (
	[idDocumentRepositoryItemLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]										INT								NOT NULL,
	[idDocumentRepositoryItem]						INT								NOT NULL, 
	[idLanguage]									INT								NOT NULL,
	[label]											NVARCHAR(255)					NOT NULL,
	[searchTags]									NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DocumentRepositoryItemLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDocumentRepositoryItemLanguage ADD CONSTRAINT [PK_DocumentRepositoryItemLanguage] PRIMARY KEY CLUSTERED (idDocumentRepositoryItemLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItemLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDocumentRepositoryItemLanguage ADD CONSTRAINT [FK_DocumentRepositoryItemLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItemLanguage_DocumentRepositoryItem]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDocumentRepositoryItemLanguage ADD CONSTRAINT [FK_DocumentRepositoryItemLanguage_DocumentRepositoryItem] FOREIGN KEY (idDocumentRepositoryItem) REFERENCES [tblDocumentRepositoryItem] (idDocumentRepositoryItem)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItemLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDocumentRepositoryItemLanguage ADD CONSTRAINT [FK_DocumentRepositoryItemLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItemLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItemLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblDocumentRepositoryItemLanguage (
	label,
	searchTags
)
KEY INDEX PK_DocumentRepositoryItemLanguage
ON Asentia -- insert index to this document repository item
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventLog] (
	[idEventLog]				INT			IDENTITY(2,1)	NOT NULL,
	[idSite]					INT							NOT NULL, 
	[idEventType]				INT							NOT NULL,
	[timestamp]					DATETIME					NOT NULL,	-- timestamp of the record
	[eventDate]					DATETIME					NULL,		-- timestamp of when the actual event occurred
	[idObject]					INT							NOT NULL,	-- the ID of the record that links the OBJECT, i.e. user, course, lesson, session, learning path, certificate 
																		-- from this, we should be able to join to the object itself to get the correct email notification template
	[idObjectRelated]			INT							NOT NULL,	-- the object related to the event's main object, i.e. user, course enrollment id, lesson data id, session id,
																		-- learning path enrollment id, certificate record id
	[idObjectUser]				INT							NULL,		-- the ID of the user who is affected by the object/event (if applicable), this is stored simply for speed in sending notifications
	[idExecutingUser]			INT							NULL		-- the user who performed/executed the event (if applicable)	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventLog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventLog ADD CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED (idEventLog ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventLog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventLog ADD CONSTRAINT [FK_EventLog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventLog_ObjectUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventLog ADD CONSTRAINT [FK_EventLog_ObjectUser] FOREIGN KEY (idObjectUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_EventLog_ExecutingUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventLog ADD CONSTRAINT [FK_EventLog_ExecutingUser] FOREIGN KEY (idExecutingUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupEnrollmentApprover] (
	[idGroupEnrollmentApprover]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]						INT						NOT NULL,
	[idGroup]						INT						NOT NULL, 
	[idUser]						INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupEnrollmentApprover ADD CONSTRAINT [PK_GroupEnrollmentApprover] PRIMARY KEY CLUSTERED (idGroupEnrollmentApprover ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollmentApprover_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupEnrollmentApprover ADD CONSTRAINT [FK_GroupEnrollmentApprover_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollmentApprover_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupEnrollmentApprover ADD CONSTRAINT [FK_GroupEnrollmentApprover_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollmentApprover_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroupEnrollmentApprover ADD CONSTRAINT [FK_GroupEnrollmentApprover_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupFeedMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupFeedMessage] (
	[idGroupFeedMessage]			INT				IDENTITY (1, 1)		NOT NULL, 
	[idSite]						INT									NOT NULL,
	[idGroup]						INT									NOT NULL,
	[idLanguage]					INT									NULL,
	[idAuthor]						INT									NOT NULL,
	[idParentGroupFeedMessage]		INT									NULL,
	[message]						NVARCHAR(MAX)						NOT NULL,
	[timestamp]						DATETIME							NOT NULL,
	[dtApproved]					DATETIME							NULL,
	[idApprover]					INT									NULL,
	[isApproved]					BIT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [PK_GroupFeedMessage] PRIMARY KEY CLUSTERED (idGroupFeedMessage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_ParentGroupFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_ParentGroupFeedMessage] FOREIGN KEY (idParentGroupFeedMessage) REFERENCES [tblGroupFeedMessage] (idGroupFeedMessage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Author]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Author] FOREIGN KEY (idAuthor) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedMessage_Approver]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedMessage] ADD CONSTRAINT [FK_GroupFeedMessage_Approver] FOREIGN KEY (idApprover) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupFeedModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupFeedModerator] (
	[idGroupFeedModerator]				INT			IDENTITY (1, 1)		NOT NULL, 
	[idSite]							INT								NOT NULL,
	[idGroup]					      	INT								NOT NULL,
	[idModerator]						INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupFeedModerator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedModerator] ADD CONSTRAINT [PK_GroupFeedModerator] PRIMARY KEY CLUSTERED (idGroupFeedModerator ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedModerator_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedModerator] ADD CONSTRAINT [FK_GroupFeedModerator_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedModerator_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedModerator] ADD CONSTRAINT [FK_GroupFeedModerator_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupFeedModerator_Moderator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupFeedModerator] ADD CONSTRAINT [FK_GroupFeedModerator_Moderator] FOREIGN KEY (idModerator) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblInboxMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblInboxMessage] (
	[idInboxMessage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NULL,
	[idParentInboxMessage]			INT								NULL,
	[idRecipient]					INT								NOT NULL,
	[idSender]						INT								NOT NULL,
	[subject]						NVARCHAR(255)					NOT NULL,
	[message]						NVARCHAR(MAX)					NOT NULL,
	[isDraft]						BIT								NULL,
	[isSent]						BIT								NULL,
	[dtSent]						DATETIME						NULL,
	[isRead]						BIT								NULL,
	[dtRead]						DATETIME						NULL,
	[isRecipientDeleted]			BIT								NULL,
	[dtRecipientDeleted]			DATETIME						NULL,
	[isSenderDeleted]				BIT								NULL,
	[dtSenderDeleted]				DATETIME						NULL,
	[dtCreated]						DATETIME						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_InboxMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblInboxMessage] ADD CONSTRAINT [PK_InboxMessage] PRIMARY KEY CLUSTERED([idInboxMessage] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_InboxMessage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblInboxMessage ADD CONSTRAINT [FK_InboxMessage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_InboxMessage_ParentInboxMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblInboxMessage ADD CONSTRAINT [FK_InboxMessage_ParentInboxMessage] FOREIGN KEY (idParentInboxMessage) REFERENCES tblInboxMessage (idInboxMessage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_InboxMessage_Recipient]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblInboxMessage ADD CONSTRAINT [FK_InboxMessage_Recipient] FOREIGN KEY(idRecipient) REFERENCES tblUser (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_InboxMessage_Sender]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblInboxMessage ADD CONSTRAINT [FK_InboxMessage_Sender] FOREIGN KEY(idSender) REFERENCES tblUser (idUser)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblInboxMessage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblInboxMessage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblInboxMessage (
	[subject]
)
KEY INDEX PK_InboxMessage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblPurchase]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblPurchase] (
	[idPurchase]				INT				IDENTITY(1,1)	NOT NULL,
	[idUser]					INT								NOT NULL,
	[orderNumber]				NVARCHAR(12)					NOT NULL,
	[timeStamp]					DATETIME						NULL,
	[ccnum]						NVARCHAR(4)						NULL,
	[currency]					NVARCHAR(4)						NULL,
	[idSite]					INT								NULL,
	[dtPending]					DATETIME						NULL,
	[failed]					BIT								NULL,
	[transactionId]				NVARCHAR(255)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Purchase]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblPurchase ADD CONSTRAINT [PK_Purchase] PRIMARY KEY CLUSTERED (idPurchase ASC)
	
/**
FOREIGN KEYS
*/	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_tblPurchase_tblUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)	
	ALTER TABLE tblPurchase ADD CONSTRAINT [FK_tblPurchase_tblUser] FOREIGN KEY(idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReport] (
	[idReport]						INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idUser]						INT								NOT NULL, 
	[idDataset]						INT								NOT NULL, 
	[title]							NVARCHAR(255)					NOT NULL,
	[fields]						NVARCHAR(MAX)					NOT NULL,
	[filter]						NVARCHAR(MAX)					NULL,
	[order]							NVARCHAR(1024)					NULL,
	[isPublic]						BIT								NOT NULL,
	[dtCreated]						DATETIME						NULL,
	[dtModified]					DATETIME						NULL,
	[isDeleted]						BIT								NULL,
	[dtDeleted]						DATETIME						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReport ADD CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED (idReport ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Report_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReport ADD CONSTRAINT [FK_Report_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Report_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReport ADD CONSTRAINT [FK_Report_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Report_Dataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReport ADD CONSTRAINT [FK_Report_Dataset] FOREIGN KEY (idDataset) REFERENCES [tblDataset] (idDataset)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReport]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReport]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblReport (
	title
)
KEY INDEX PK_Report
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResource]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	
CREATE TABLE [tblResource] (
	[idResource]					INT				IDENTITY (1, 1)		NOT NULL, 
	[idSite]						INT									NOT NULL, 
	[idParentResource]				INT									NULL,
	[idResourceType]                INT                                 NOT NULL,
	[name]						    NVARCHAR(255)						NOT NULL,
	[description]                   NVARCHAR(512)						NULL,
	[locationRoom]					NVARCHAR(255)						NULL,
	[locationBuilding]				NVARCHAR(255)						NULL,
	[locationCity]					NVARCHAR(255)						NULL,
	[locationProvince]				NVARCHAR(255)						NULL,
	[locationCountry]				NVARCHAR(255)						NULL,
	[identifier]					NVARCHAR(50)						NULL,
	[capacity]						INT									NULL,
	[ownerWithinSystem]             BIT									NOT NULL,
	[idOwner]						INT									NULL,
	[ownerName]						NVARCHAR(255)						NULL,
	[ownerEmail]					NVARCHAR(255)						NULL,
	[ownerContactInformation]		NVARCHAR(512)						NULL,
	[isMoveable]					BIT									NOT NULL,
	[isAvailable]					BIT									NOT NULL,
	[isDeleted]						BIT									NOT NULL,
	[dtDeleted]						DATETIME							NULL
	) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Resource]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [PK_Resource] PRIMARY KEY CLUSTERED (idResource ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Resource_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [FK_Resource_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Recource_ResourceType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [FK_Recource_ResourceType] FOREIGN KEY (idResourceType) REFERENCES [tblResourceType] (idResourceType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Recource_Resource]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [FK_Recource_Resource] FOREIGN KEY (idParentResource) REFERENCES [tblResource] (idResource)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Recource_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResource ADD CONSTRAINT [FK_Recource_User] FOREIGN KEY (idOwner) REFERENCES [tblUser] (idUser)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResource]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResource]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblResource (
	name,
	[description]
)
KEY INDEX PK_Resource
ON Asentia -- insert index to this resource
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstance]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTrainingInstance] (
	[idStandUpTrainingInstance]			INT				IDENTITY(1,1)	NOT NULL,
	[idStandUpTraining]					INT								NOT NULL,
	[idSite]							INT								NOT NULL,
	[title]								NVARCHAR(255)					NOT NULL,
	[description]						NVARCHAR(MAX)					NULL,
	[seats]								INT								NOT NULL,
	[waitingSeats]						INT								NOT NULL,
	[type]								INT								NOT NULL,
	[isClosed]							BIT								NULL,
	[urlRegistration]					NVARCHAR(255)					NULL,
	[urlAttend]							NVARCHAR(255)					NULL,
	[city]								NVARCHAR(255)					NULL,
	[province]							NVARCHAR(255)					NULL,
	[postalcode]						NVARCHAR(25)					NULL,
	[locationDescription]				NVARCHAR(MAX)					NULL,
	[idInstructor]						INT								NULL,
	[isDeleted]							BIT								NULL,
	[dtDeleted]							DATETIME						NULL,
	[integratedObjectKey]				BIGINT							NULL,
	[hostUrl]							NVARCHAR(1024)					NULL,
	[genericJoinUrl]					NVARCHAR(1024)					NULL,
	[meetingPassword]					NVARCHAR(255)					NULL,
	[idWebMeetingOrganizer]				INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTrainingInstance] ADD CONSTRAINT [PK_StandUpTrainingInstance] PRIMARY KEY CLUSTERED ([idStandUpTrainingInstance] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstance_StandUpTraining]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTrainingInstance] ADD CONSTRAINT [FK_StandUpTrainingInstance_StandUpTraining] FOREIGN KEY (idStandUpTraining) REFERENCES [tblStandUpTraining] (idStandUpTraining)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstance_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTrainingInstance] ADD CONSTRAINT [FK_StandUpTrainingInstance_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstance_Instructor]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandUpTrainingInstance] ADD CONSTRAINT [FK_StandUpTrainingInstance_Instructor] FOREIGN KEY (idInstructor) REFERENCES [tblUser] (idUser)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstance]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstance]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblStandUpTrainingInstance (
	title,
	[description],
	city,
	province,
	locationDescription
)
KEY INDEX PK_StandUpTrainingInstance
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTransactionResponse]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTransactionResponse] (
	[id]					        INT				IDENTITY(1, 1)	NOT NULL,
	[idSite]                        INT                             NOT NULL,
	[idUser]                        INT								NOT NULL,
	[cardType]                      NVARCHAR(35)                    NULL,
	[invoiceNumber]					NVARCHAR(25) 					NULL,
	[cardNumber]					NVARCHAR(25)					NULL,
	[idTransaction]                 NVARCHAR(30)                    NULL,
	[amount]                        FLOAT                           NULL,
	[responseCode]                  NVARCHAR(255)                   NULL,
	[responseMessage]               NVARCHAR(MAX)                   NULL,
	[isApproved]                    BIT                             NOT NULL,
	[isValidated]                   BIT                             NOT NULL,
	[dtTransaction]                 DATETIME                        NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_TransactionResponse]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTransactionResponse ADD CONSTRAINT [PK_TransactionResponse] PRIMARY KEY CLUSTERED (id ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TransactionResponse_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTransactionResponse ADD CONSTRAINT [FK_TransactionResponse_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TransactionResponse_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTransactionResponse ADD CONSTRAINT [FK_TransactionResponse_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToGroupLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToGroupLink] (
	[idUserToGroupLink]					INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idUser]							INT							NOT NULL,
	[idGroup]							INT							NOT NULL,
	[idRuleSet]							INT							NULL,		-- this value indicates the ruleset that is responsible for this link. NULL means the group membership was manually assigned.
	[created]							DATETIME					NOT NULL,
	[selfJoined]						BIT							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToGroupLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToGroupLink ADD CONSTRAINT [PK_UserToGroupLink] PRIMARY KEY CLUSTERED (idUserToGroupLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToGroupLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToGroupLink ADD CONSTRAINT [FK_UserToGroupLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToGroupLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToGroupLink ADD CONSTRAINT [FK_UserToGroupLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToGroupLink_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToGroupLink ADD CONSTRAINT [FK_UserToGroupLink_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToSupervisorLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToSupervisorLink] (
		[idUserToSupervisorLink]			INT		IDENTITY(1,1)	NOT NULL,
		[idSite]							INT						NOT NULL,
		[idUser]							INT						NOT NULL,
		[idSupervisor]						INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToSupervisorLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToSupervisorLink ADD CONSTRAINT [PK_UserToSupervisorLink] PRIMARY KEY CLUSTERED (idUserToSupervisorLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToSupervisorLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToSupervisorLink ADD CONSTRAINT [FK_UserToSupervisorLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToSupervisorLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToSupervisorLink ADD CONSTRAINT [FK_UserToSupervisorLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToSupervisorLink_Supervisor]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToSupervisorLink ADD CONSTRAINT [FK_UserToSupervisorLink_Supervisor] FOREIGN KEY (idSupervisor) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-TinCanProfile]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-TinCanProfile] (
	[idData-TinCanProfile]				INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL,
	[idEndpoint]						INT									NULL,
	[profileId]							NVARCHAR(100)						NOT NULL,
	[activityId]						NVARCHAR(100)						NULL,
	[agent]								NVARCHAR(MAX)						NULL,
	[contentType]						NVARCHAR(50)						NOT NULL,
	[docContents]						VARBINARY(MAX)						NOT NULL,
	[dtUpdated]							DATETIME							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-TinCanProfile]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanProfile] ADD CONSTRAINT [PK_Data-TinCanProfile] PRIMARY KEY CLUSTERED ([idData-TinCanProfile] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanProfile_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanProfile] ADD CONSTRAINT [FK_Data-TinCanProfile_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanProfile_IdEndpoint]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanProfile] ADD CONSTRAINT [FK_Data-TinCanProfile_IdEndpoint] FOREIGN KEY (idEndpoint) REFERENCES [tblxAPIoAuthConsumer] (idxAPIoAuthConsumer)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-TinCanState]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-TinCanState] (
	[idData-TinCanState]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idEndpoint]						INT							NULL,
	[stateId]							NVARCHAR(100)				NOT NULL,
	[agent]								NVARCHAR(MAX)				NOT NULL,
	[activityId]						NVARCHAR(100)				NOT NULL,
	[registration]						NVARCHAR(50)				NULL,
	[contentType]						NVARCHAR(50)				NOT NULL,
	[docContents]						VARBINARY(MAX)				NOT NULL,
	[dtUpdated]							DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-TinCanState]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanState] ADD CONSTRAINT [PK_Data-TinCanState] PRIMARY KEY CLUSTERED ([idData-TinCanState] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanState_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanState] ADD CONSTRAINT [FK_Data-TinCanState_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanState_IdEndpoint]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanState] ADD CONSTRAINT [FK_Data-TinCanState_IdEndpoint] FOREIGN KEY (idEndpoint) REFERENCES [tblxAPIoAuthConsumer] (idxAPIoAuthConsumer)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblxAPIoAuthNonce]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [dbo].[tblxAPIoAuthNonce] (
	[idxAPIoAuthNonce]				INT				IDENTITY(1,1)	NOT NULL,
	[idxAPIoAuthConsumer]			INT								NOT NULL,
	[nonce]							NVARCHAR(200)					NOT NULL
 ) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_xAPIoAuthNonce]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblxAPIoAuthNonce] ADD CONSTRAINT [PK_xAPIoAuthNonce] PRIMARY KEY CLUSTERED ([idxAPIoAuthNonce] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_xAPIoAuthNonce_xAPIoAuthConsumer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblxAPIoAuthNonce] ADD CONSTRAINT [FK_xAPIoAuthNonce_xAPIoAuthConsumer] FOREIGN KEY (idxAPIoAuthConsumer) REFERENCES [tblxAPIoAuthConsumer] (idxAPIoAuthConsumer)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLessonLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLessonLanguage] (
	[idLessonLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[idLesson]					INT								NOT NULL,
	[idLanguage]				INT								NOT NULL,
	[title]						NVARCHAR(255)					NOT NULL,
	[shortDescription]			NVARCHAR(512)					NULL,
	[longDescription]			NVARCHAR(MAX)					NULL,
	[searchTags]				NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LessonLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonLanguage ADD CONSTRAINT [PK_LessonLanguage] PRIMARY KEY CLUSTERED ([idLessonLanguage] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonLanguage ADD CONSTRAINT [FK_LessonLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonLanguage_Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonLanguage ADD CONSTRAINT [FK_LessonLanguage_Lesson] FOREIGN KEY ([idLesson]) REFERENCES [tblLesson] ([idLesson])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonLanguage ADD CONSTRAINT [FK_LessonLanguage_Language] FOREIGN KEY ([idLanguage]) REFERENCES [tblLanguage] ([idLanguage])

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLessonLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLessonLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLessonLanguage (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_LessonLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLessonToContentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLessonToContentLink] (
	[idLessonToContentLink]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idLesson]						INT								NOT NULL, 
	[idObject]						INT								NULL,
	[idContentType]					INT								NOT NULL,
	[idAssignmentDocumentType]		INT								NULL,
	[assignmentResourcePath]		NVARCHAR(255)					NULL,
	[allowSupervisorsAsProctor]		BIT								NULL,
	[allowCourseExpertsAsProctor]	BIT								NULL,
	[externalxAPIIdentifier]		NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LessonToContentLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonToContentLink ADD CONSTRAINT [PK_LessonToContentLink] PRIMARY KEY CLUSTERED ([idLessonToContentLink] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonToContentLink_Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonToContentLink ADD CONSTRAINT [FK_LessonToContentLink_Lesson] FOREIGN KEY ([idLesson]) REFERENCES [tblLesson] ([idLesson])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonToContentLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonToContentLink ADD CONSTRAINT [FK_LessonToContentLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetToRuleSetEnrollmentLink] (
	[idRuleSetToRuleSetEnrollmentLink]		INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idRuleSet]								INT							NOT NULL,
	[idRuleSetEnrollment]					INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetEnrollmentLink ADD CONSTRAINT [PK_RuleSetToRuleSetEnrollmentLink] PRIMARY KEY CLUSTERED (idRuleSetToRuleSetEnrollmentLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetEnrollmentLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetEnrollmentLink ADD CONSTRAINT [FK_RuleSetToRuleSetEnrollmentLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetEnrollmentLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetEnrollmentLink ADD CONSTRAINT FK_RuleSetToRuleSetEnrollmentLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetEnrollmentLink_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetEnrollmentLink ADD CONSTRAINT FK_RuleSetToRuleSetEnrollmentLink_RuleSetEnrollment FOREIGN KEY ([idRuleSetEnrollment]) REFERENCES [tblRuleSetEnrollment] ([idRuleSetEnrollment])
/* THIS IS GOING TO BE REMOVED */

/**

Records within this table should be unique for user/course. 
The rule ID and priority values will be updated as a user drops/joins 
new rules that are valid for the same course.

**/

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToRuleSetEnrollmentLink] (
	[idUserToRuleSetEnrollmentLink]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idUser]								INT							NOT NULL,
	[idRuleSetEnrollment]					INT							NOT NULL,
	[idCourse]								INT							NOT NULL,
	[idRuleSet]								INT							NULL,
	[created]								DATETIME					NOT NULL
)

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToRuleSetEnrollmentLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRuleSetEnrollmentLink ADD CONSTRAINT [PK_UserToRuleSetEnrollmentLink] PRIMARY KEY CLUSTERED (idUserToRuleSetEnrollmentLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRuleSetEnrollmentLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRuleSetEnrollmentLink ADD CONSTRAINT [FK_UserToRuleSetEnrollmentLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRuleSetEnrollmentLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRuleSetEnrollmentLink ADD CONSTRAINT FK_UserToRuleSetEnrollmentLink_User FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRuleSetEnrollmentLink_RuleSetEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRuleSetEnrollmentLink ADD CONSTRAINT FK_UserToRuleSetEnrollmentLink_RuleSetEnrollment FOREIGN KEY ([idRuleSetEnrollment]) REFERENCES [tblRuleSetEnrollment] ([idRuleSetEnrollment])
	
-- DO NOT create Foreign Key to idCourse...we only need it to detect changes.
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-SCOInt]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-SCOInt] (
	[idData-SCOInt]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idData-SCO]					INT								NOT NULL, -- we need this so that we can clear and reset upon subsequent attempts
	[identifier]					NVARCHAR(255)					NULL,
	[timestamp]						DATETIME						NULL, 
	[result]						INT								NOT NULL, 
	[latency]						FLOAT							NULL,
	[scoIdentifier]					NVARCHAR(255)					NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-SCOInt]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOInt] ADD CONSTRAINT [PK_Data-SCOInt] PRIMARY KEY CLUSTERED ([idData-SCOInt] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOInt_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOInt] ADD CONSTRAINT [FK_Data-SCOInt_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOInt_Data-SCO]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOInt] ADD CONSTRAINT [FK_Data-SCOInt_Data-SCO] FOREIGN KEY ([idData-SCO]) REFERENCES [tblData-SCO] ([idData-SCO])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOInt_Result]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOInt] ADD CONSTRAINT [FK_Data-SCOInt_Result] FOREIGN KEY ([result]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-SCOObj]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-SCOObj] (
	[idData-SCOObj]						INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idData-SCO]						INT								NOT NULL, -- we need this so that we can clear and reset upon subsequent attempts
	[identifier]						NVARCHAR(255)					NOT NULL,
	[scoreScaled]						FLOAT							NULL, 
	[completionStatus]					INT								NOT NULL, 
	[successStatus]						INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-SCOObj]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [PK_Data-SCOObj] PRIMARY KEY CLUSTERED ([idData-SCOObj] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOObj_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [FK_Data-SCOObj_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOObj_Data-SCO]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [FK_Data-SCOObj_Data-SCO] FOREIGN KEY ([idData-SCO]) REFERENCES [tblData-SCO] ([idData-SCO])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOObj_CompletionStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [FK_Data-SCOObj_CompletionStatus] FOREIGN KEY ([completionStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOObj_SuccessStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOObj] ADD CONSTRAINT [FK_Data-SCOObj_SuccessStatus] FOREIGN KEY ([successStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])
/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathFeedMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathFeedMessage] (
	[idLearningPathFeedMessage]				INT				IDENTITY (1, 1)		NOT NULL,
	[idSite]								INT									NOT NULL,
	[idLearningPathFeed]					INT									NOT NULL,
	[idLanguage]							INT									NULL,
	[idAuthor]								INT									NOT NULL,
	[idParentLearningPathFeedMessage]		INT									NULL,
	[message]								NVARCHAR(MAX)						NOT NULL,
	[timestamp]								DATETIME							NOT NULL,
	[dtApproved]							DATETIME							NOT NULL,
	[idApprover]							INT									NULL,
	[isApproved]							BIT 						        NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [PK_LearningPathFeedMessage] PRIMARY KEY CLUSTERED (idLearningPathFeedMessage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_LearningPathFeed]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_LearningPathFeed] FOREIGN KEY (idLearningPathFeed) REFERENCES [tblLearningPathFeed] (idLearningPathFeed)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_ParentLearningPathFeedMessage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_ParentLearningPathFeedMessage] FOREIGN KEY (idParentLearningPathFeedMessage) REFERENCES [tblLearningPathFeedMessage] (idLearningPathFeedMessage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_Author]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_Author] FOREIGN KEY (idAuthor) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedMessage_Approver]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedMessage] ADD CONSTRAINT [FK_LearningPathFeedMessage_Approver] FOREIGN KEY (idApprover) REFERENCES [tblUser] (idUser)
/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathFeedModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathFeedModerator] (
	[idLearningPathFeedModerator]			INT			IDENTITY (1, 1)		NOT NULL,
	[idSite]								INT								NOT NULL,
	[idLearningPathFeed]					INT								NOT NULL,
	[idModerator]							INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathFeedModerator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedModerator] ADD CONSTRAINT [PK_LearningPathFeedModerator] PRIMARY KEY CLUSTERED (idLearningPathFeedModerator ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedModerator_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedModerator] ADD CONSTRAINT [FK_LearningPathFeedModerator_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedModerator_LearningPathFeed]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedModerator] ADD CONSTRAINT [FK_LearningPathFeedModerator_LearningPathFeed] FOREIGN KEY (idLearningPathFeed) REFERENCES [tblLearningPathFeed] (idLearningPathFeed)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedModerator_Moderator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedModerator] ADD CONSTRAINT [FK_LearningPathFeedModerator_Moderator] FOREIGN KEY (idModerator) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollmentLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetLearningPathEnrollmentLanguage] (
	[idRuleSetLearningPathEnrollmentLanguage]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]										INT								NOT NULL,
	[idRuleSetLearningPathEnrollment]				INT								NOT NULL,
	[idLanguage]									INT								NOT NULL,
	[label]											NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetLearningPathEnrollmentLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLearningPathEnrollmentLanguage ADD CONSTRAINT [PK_RuleSetLearningPathEnrollmentLanguage] PRIMARY KEY CLUSTERED (idRuleSetLearningPathEnrollmentLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollmentLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLearningPathEnrollmentLanguage ADD CONSTRAINT [FK_RuleSetLearningPathEnrollmentLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLearningPathEnrollmentLanguage_RuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLearningPathEnrollmentLanguage ADD CONSTRAINT [FK_RuleSetLearningPathEnrollmentLanguage_RuleSetLearningPathEnrollment] FOREIGN KEY (idRuleSetLearningPathEnrollment) REFERENCES [tblRuleSetLearningPathEnrollment] (idRuleSetLearningPathEnrollment)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollmentLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLearningPathEnrollmentLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetLearningPathEnrollmentLanguage (
	label
)
KEY INDEX PK_RuleSetLearningPathEnrollmentLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToRuleSetLearningPathEnrollmentLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetToRuleSetLearningPathEnrollmentLink] (
	[idRuleSetToRuleSetLearningPathEnrollmentLink]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]											INT						NOT NULL,
	[idRuleSet]											INT						NOT NULL,
	[idRuleSetLearningPathEnrollment]					INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToRuleSetLearningPathEnrollmentLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetLearningPathEnrollmentLink ADD CONSTRAINT [PK_RuleSetToRuleSetLearningPathEnrollmentLink] PRIMARY KEY CLUSTERED (idRuleSetToRuleSetLearningPathEnrollmentLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetLearningPathEnrollmentLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetLearningPathEnrollmentLink ADD CONSTRAINT [FK_RuleSetToRuleSetLearningPathEnrollmentLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetLearningPathEnrollmentLink ADD CONSTRAINT FK_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRuleSetLearningPathEnrollmentLink ADD CONSTRAINT FK_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSetLearningPathEnrollment FOREIGN KEY ([idRuleSetLearningPathEnrollment]) REFERENCES [tblRuleSetLearningPathEnrollment] ([idRuleSetLearningPathEnrollment])
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticLanguage] (
	[idAnalyticLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idAnalytic]					INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL,
	[dtCreated]						DATETIME						NULL,
	[dtModified]                    DATETIME                        NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticLanguage ADD CONSTRAINT [PK_AnalyticLanguage] PRIMARY KEY CLUSTERED (idAnalyticLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticLanguage ADD CONSTRAINT [FK_AnalyticLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticLanguage_Analytic]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticLanguage ADD CONSTRAINT [FK_AnalyticLanguage_Analytic] FOREIGN KEY (idAnalytic) REFERENCES [tblAnalytic] (idAnalytic)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticLanguage ADD CONSTRAINT [FK_AnalyticLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAnalyticLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAnalyticLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblAnalyticLanguage (
	title
)
KEY INDEX PK_AnalyticLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTransactionItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTransactionItem] (
	[idTransactionItem]					INT				IDENTITY(1,1)	NOT NULL,
	[idParentTransactionItem]			INT								NULL,
	[idPurchase]						INT								NULL,
	[idEnrollment]						INT								NULL,
	[idUser]							INT								NULL,
	[userName]							NVARCHAR(255)					NULL,
	[idAssigner]						INT								NULL,
	[assignerName]						NVARCHAR(255)					NULL,
	[itemId]							INT								NULL,
	[itemName]							NVARCHAR(255)					NULL,
	[itemType]							INT								NULL,
	[description]						NVARCHAR(512)					NULL,
	[cost]								FLOAT							NULL,
	[paid]								FLOAT							NULL,
	[idCouponCode]						INT								NULL,
	[couponCode]						NVARCHAR(10)					NULL,
	[dtAdded]							DATETIME						NULL,
	[confirmed]							BIT								NULL,
	[idSite]							INT								NOT NULL,
	[idIltSession]						INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_TransactionItem]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTransactionItem ADD CONSTRAINT [PK_TransactionItem] PRIMARY KEY CLUSTERED (idTransactionItem ASC)

/**
FOREIGN KEYS
*/

-- DO NOT FK idEnrollment TO tblEnrollment

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_tblTransactionItem_tblPurchase]') AND parent_object_id = OBJECT_ID(N'[tblTransactionItem]'))
	ALTER TABLE [tblTransactionItem]  WITH CHECK ADD  CONSTRAINT [FK_tblTransactionItem_tblPurchase] FOREIGN KEY([idPurchase]) REFERENCES [tblPurchase] ([idPurchase])

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_tblTransactionItem_tblTransactionItem]') AND parent_object_id = OBJECT_ID(N'[tblTransactionItem]'))
	ALTER TABLE [tblTransactionItem]  WITH CHECK ADD  CONSTRAINT [FK_tblTransactionItem_tblTransactionItem] FOREIGN KEY([idTransactionItem]) REFERENCES [tblTransactionItem] ([idTransactionItem])

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_tblTransactionItem_tblUser]') AND parent_object_id = OBJECT_ID(N'[tblTransactionItem]'))
	ALTER TABLE [tblTransactionItem]  WITH CHECK ADD  CONSTRAINT [FK_tblTransactionItem_tblUser] FOREIGN KEY([idUser]) REFERENCES [tblUser] ([idUser])

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[FK_TransactionItem_Site]') AND parent_object_id = OBJECT_ID(N'[tblTransactionItem]'))
	ALTER TABLE [tblTransactionItem]  WITH CHECK ADD  CONSTRAINT [FK_TransactionItem_Site] FOREIGN KEY([idSite]) REFERENCES [tblSite] ([idSite])
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportLanguage] (
	[idReportLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idReport]						INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportLanguage ADD CONSTRAINT [PK_ReportLanguage] PRIMARY KEY CLUSTERED (idReportLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportLanguage ADD CONSTRAINT [FK_ReportLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportLanguage_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportLanguage ADD CONSTRAINT [FK_ReportLanguage_Report] FOREIGN KEY (idReport) REFERENCES [tblReport] (idReport)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportLanguage ADD CONSTRAINT [FK_ReportLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReportLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReportLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblReportLanguage (
	title
)
KEY INDEX PK_ReportLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportSubscription]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportSubscription] (
	[idReportSubscription]				INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL,
	[idUser]							INT									NOT NULL,
	[idReport]							INT									NOT NULL,
	[dtStart]							DATETIME							NOT NULL, 
	[dtNextAction]						DATETIME							NOT NULL,
	[recurInterval]						INT									NOT NULL, 
	[recurTimeframe]					NVARCHAR(4)							NOT NULL,
	[token]								UNIQUEIDENTIFIER						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportSubscription]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscription ADD CONSTRAINT [PK_ReportSubscription] PRIMARY KEY CLUSTERED (idReportSubscription ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportSubscription_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscription ADD CONSTRAINT [FK_ReportSubscription_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportSubscription_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscription ADD CONSTRAINT [FK_ReportSubscription_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportSubscription_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscription ADD CONSTRAINT [FK_ReportSubscription_Report] FOREIGN KEY (idReport) REFERENCES [tblReport] (idReport)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportFile]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportFile] (
	[idReportFile]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idReport]						INT								NOT NULL, 
	[idUser]						INT								NOT NULL, 
	[filename]						NVARCHAR(255)					NOT NULL,
	[dtCreated]						DATETIME						NULL,
	[htmlKb]						INT								NULL,
	[csvKb]							INT								NULL,
	[pdfKb]							INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportFile]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportFile ADD CONSTRAINT [PK_ReportFile] PRIMARY KEY CLUSTERED (idReportFile ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportFile_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportFile ADD CONSTRAINT [FK_ReportFile_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportFile_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportFile ADD CONSTRAINT [FK_ReportFile_Report] FOREIGN KEY (idReport) REFERENCES [tblReport] (idReport)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportFile_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportFile ADD CONSTRAINT [FK_ReportFile_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResourceLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblResourceLanguage] (
	[idResourceLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idResource]					INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[name]							NVARCHAR(255)					NOT NULL,
	[description]			       	NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ResourceLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceLanguage ADD CONSTRAINT [PK_ResourceLanguage] PRIMARY KEY CLUSTERED (idResourceLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceLanguage ADD CONSTRAINT [FK_ResourceLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceLanguage_Resource]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceLanguage ADD CONSTRAINT [FK_ResourceLanguage_Resource] FOREIGN KEY (idResource) REFERENCES [tblResource] (idResource)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceLanguage ADD CONSTRAINT [FK_ResourceLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblResourceLanguage (
	name,
	[description]
)
KEY INDEX PK_ResourceLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResourceToObjectLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblResourceToObjectLink] (
	[id]							INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idResource]					INT								NOT NULL, 
	[idObject]						INT								NULL,
	[objectType]					NVARCHAR(255)					NULL,
	[dtStart]	                    DATETIME                        NOT NULL,
	[dtEnd]                         DATETIME                        NOT NULL,
	[isOutsideUse]					BIT								NULL,
	[isMaintenance]					BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ResourceToObjectLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceToObjectLink ADD CONSTRAINT [PK_ResourceToObjectLink] PRIMARY KEY CLUSTERED (id ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceToObjectLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceToObjectLink ADD CONSTRAINT [FK_ResourceToObjectLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceToObjectLink_Resource]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceToObjectLink ADD CONSTRAINT [FK_ResourceToObjectLink_Resource] FOREIGN KEY (idResource) REFERENCES [tblResource] (idResource)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstanceLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTrainingInstanceLanguage] (
	[idStandUpTrainingInstanceLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idStandUpTrainingInstance]					INT								NOT NULL,
	[idSite]									INT								NOT NULL,
	[idLanguage]								INT								NOT NULL,
	[title]										NVARCHAR(255)					NOT NULL,
	[description]								NVARCHAR(MAX)					NULL,
	[locationDescription]						NVARCHAR(MAX)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTrainingInstanceLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceLanguage ADD CONSTRAINT [PK_StandUpTrainingInstanceLanguage] PRIMARY KEY CLUSTERED (idStandUpTrainingInstanceLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceLanguage_StandUpTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceLanguage ADD CONSTRAINT [FK_StandUpTrainingInstanceLanguage_StandupTrainingInstance] FOREIGN KEY (idStandUpTrainingInstance) REFERENCES [tblStandUpTrainingInstance] (idStandUpTrainingInstance)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceLanguage ADD CONSTRAINT [FK_StandUpTrainingInstanceLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceLanguage ADD CONSTRAINT [FK_StandUpTrainingInstanceLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstanceLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstanceLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblStandUpTrainingInstanceLanguage (
	title,
	[description],
	locationDescription
)
KEY INDEX PK_StandUpTrainingInstanceLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandUpTrainingInstanceMeetingTime]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandUpTrainingInstanceMeetingTime] (
	[idStandUpTrainingInstanceMeetingTime]		INT			IDENTITY(1,1)	NOT NULL,
	[idStandUpTrainingInstance]					INT							NOT NULL,
	[idSite]									INT							NOT NULL,
	[dtStart]									DATETIME					NOT NULL,
	[dtEnd]										DATETIME					NOT NULL,
	[minutes]									INT							NOT NULL,
	[idTimezone]								INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandUpTrainingInstanceMeetingTime]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceMeetingTime ADD CONSTRAINT [PK_StandUpTrainingInstanceMeetingTime] PRIMARY KEY CLUSTERED (idStandUpTrainingInstanceMeetingTime ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceMeetingTime_StandUpTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceMeetingTime ADD CONSTRAINT [FK_StandUpTrainingInstanceMeetingTime_StandupTrainingInstance] FOREIGN KEY (idStandUpTrainingInstance) REFERENCES [tblStandUpTrainingInstance] (idStandUpTrainingInstance)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandUpTrainingInstanceMeetingTime_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandUpTrainingInstanceMeetingTime ADD CONSTRAINT [FK_StandUpTrainingInstanceMeetingTime_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandupTrainingInstanceToUserLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandupTrainingInstanceToUserLink] (
	[idStandupTrainingInstanceToUserLink]			INT		 IDENTITY(1,1)	NOT NULL,
	[idUser]										INT						NOT NULL,
	[idStandupTrainingInstance]						INT						NOT NULL, 
	[completionStatus]								INT						NULL,
	[successStatus]									INT						NULL,
	[score]											INT						NULL,
	[isWaitingList]									BIT						NOT NULL,
	[dtCompleted]									DATETIME				NULL,
	[registrantKey]									BIGINT					NULL,
	[registrantEmail]								NVARCHAR(255)			NULL,
	[joinUrl]										NVARCHAR(1024)			NULL,
	[waitlistOrder]									INT						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandupTrainingInstanceToUserLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToUserLink ADD CONSTRAINT [PK_StandupTrainingInstanceToUserLink] PRIMARY KEY CLUSTERED (idStandupTrainingInstanceToUserLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToUserLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToUserLink ADD CONSTRAINT [FK_StandupTrainingInstanceToUserLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToUserLink_StandupTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToUserLink ADD CONSTRAINT [FK_StandupTrainingInstanceToUserLink_StandupTrainingInstance] FOREIGN KEY (idStandupTrainingInstance) REFERENCES [tblStandUpTrainingInstance] (idStandupTrainingInstance)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToUserLink_CompletionStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandupTrainingInstanceToUserLink] ADD CONSTRAINT [FK_StandupTrainingInstanceToUserLink_CompletionStatus] FOREIGN KEY ([completionStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToUserLink_SuccessStatus]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblStandupTrainingInstanceToUserLink] ADD CONSTRAINT [FK_StandupTrainingInstanceToUserLink_SuccessStatus] FOREIGN KEY ([successStatus]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblStandupTrainingInstanceToInstructorLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblStandupTrainingInstanceToInstructorLink] (
	[idStandupTrainingInstanceToInstructorLink]		INT		IDENTITY (1, 1)		NOT NULL,
	[idSite]										INT							NOT NULL,
	[idStandupTrainingInstance]						INT							NOT NULL,
	[idInstructor]									INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_StandupTrainingInstanceToInstructorLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToInstructorLink ADD CONSTRAINT [PK_StandupTrainingInstanceToInstructorLink] PRIMARY KEY CLUSTERED (idStandupTrainingInstanceToInstructorLink ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToInstructorLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToInstructorLink ADD CONSTRAINT [FK_StandupTrainingInstanceToInstructorLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToInstructorLink_StandupTrainingInstance]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToInstructorLink ADD CONSTRAINT [FK_StandupTrainingInstanceToInstructorLink_StandupTrainingInstance] FOREIGN KEY (idStandupTrainingInstance) REFERENCES [tblStandupTrainingInstance] (idStandupTrainingInstance)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_StandupTrainingInstanceToInstructorLink_Instructor]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblStandupTrainingInstanceToInstructorLink ADD CONSTRAINT [FK_StandupTrainingInstanceToInstructorLink_Instructor] FOREIGN KEY (idInstructor) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportSubscriptionQueue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportSubscriptionQueue] (
	[idReportSubscriptionQueue]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL,
	[idReportSubscription]					INT								NOT NULL,
	[idReport]								INT								NOT NULL,
	[idDataset]								INT								NOT NULL,
	[reportName]							NVARCHAR(255)					NOT NULL, 
	[idRecipient]							INT								NOT NULL,
	[recipientLangString]					NVARCHAR(10)					NOT NULL,
	[recipientFullName]						NVARCHAR(512)					NOT NULL,
	[recipientFirstName]					NVARCHAR(255)					NOT NULL,
	[recipientLogin]						NVARCHAR(512)					NOT NULL,
	[recipientEmail]						NVARCHAR(255)					NOT NULL,
	[recipientTimezone]						INT								NOT NULL,
	[recipientTimezoneDotNetName]			NVARCHAR(255)					NOT NULL,
	[priority]								INT								NULL,
	[dtAction]								DATETIME						NOT NULL,	-- the date the email should be sent
	[dtSent]								DATETIME						NULL,		-- the record of the date the email ACTUALLY sent
	[statusDescription]						NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportSubscriptionQueue]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscriptionQueue ADD CONSTRAINT [PK_ReportSubscriptionQueue] PRIMARY KEY CLUSTERED (idReportSubscriptionQueue ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportSubscriptionQueue_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportSubscriptionQueue ADD CONSTRAINT [FK_ReportSubscriptionQueue_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReportSubscriptionQueue]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReportSubscriptionQueue]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblReportSubscriptionQueue (
	reportName,
	recipientFullName
)
KEY INDEX PK_ReportSubscriptionQueue
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticConstants]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticConstants] (
	[idConstant]						INT				IDENTITY(1000,1)	NOT NULL,
	[value]							    INT									NOT NULL, 
	[name]								NVARCHAR(255)						NOT NULL,
	[idConstantType]			  	    INT									NOT NULL
	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticConstants]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticConstants ADD CONSTRAINT [PK_AnalyticConstants] PRIMARY KEY CLUSTERED (idConstant ASC)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblAnalyticConstants] ON

INSERT INTO tblAnalyticConstants (
	idConstant,
	value,
	name,
	idConstantType
	)
SELECT
	idConstant,
	value,
	name,
	idConstantType
	
FROM (
	      SELECT 1 AS idConstant, 1 AS value, 'Sunday'    AS name,  2 AS idConstantType
	UNION SELECT 2 AS idConstant, 2 AS value, 'Monday'    AS name,  2 AS idConstantType
	UNION SELECT 3 AS idConstant, 3 AS value, 'Tuesday'   AS name,  2 AS idConstantType
	UNION SELECT 4 AS idConstant, 4 AS value, 'Wednesday' AS name,  2 AS idConstantType
	UNION SELECT 5 AS idConstant, 5 AS value, 'Thursday'  AS name,  2 AS idConstantType
	UNION SELECT 6 AS idConstant, 6 AS value, 'Friday'    AS name,  2 AS idConstantType
	UNION SELECT 7 AS idConstant, 7 AS value, 'Saturday'  AS name,  2 AS idConstantType

	UNION SELECT 8 AS idConstant,  1 AS value, 'January'    AS name,  4 AS idConstantType
	UNION SELECT 9 AS idConstant,  2 AS value, 'February'    AS name,  4 AS idConstantType
	UNION SELECT 10 AS idConstant,  3 AS value, 'March'      AS name,  4 AS idConstantType
	UNION SELECT 11 AS idConstant,  4 AS value, 'April'      AS name,  4 AS idConstantType
	UNION SELECT 12 AS idConstant,  5 AS value, 'May'         AS name,  4 AS idConstantType
    UNION SELECT 13 AS idConstant,  6 AS value, 'June'       AS name,  4 AS idConstantType
	UNION SELECT 14 AS idConstant,  7 AS value, 'July'       AS name,  4 AS idConstantType
	UNION SELECT 15 AS idConstant,  8 AS value, 'August'     AS name,  4 AS idConstantType
	UNION SELECT 16 AS idConstant,  9 AS value, 'September'  AS name,  4 AS idConstantType
	UNION SELECT 17 AS idConstant, 10 AS value, 'October'   AS name,  4 AS idConstantType
	UNION SELECT 18 AS idConstant, 11 AS value, 'November'   AS name,  4 AS idConstantType
	UNION SELECT 19 AS idConstant, 12 AS value, 'December'  AS name,  4 AS idConstantType

	UNION SELECT 20 AS idConstant, 1  AS value, 'Week 1'    AS name,  3 AS idConstantType
	UNION SELECT 21 AS idConstant, 2  AS value, 'Week 2'    AS name,  3 AS idConstantType
	UNION SELECT 22 AS idConstant,  3 AS value, 'Week 3'      AS name,  3 AS idConstantType
	UNION SELECT 23 AS idConstant,  4 AS value, 'Week 4'      AS name,  3 AS idConstantType
	

	UNION SELECT 24 AS idConstant,  0 AS value, '00:00' AS name,  1 AS idConstantType
    UNION SELECT 25 AS idConstant,  1 AS value, '01:00' AS name,  1 AS idConstantType
	UNION SELECT 26 AS idConstant,  2 AS value, '02:00' AS name,  1 AS idConstantType
	UNION SELECT 27 AS idConstant,  3 AS value, '03:00' AS name,  1 AS idConstantType
	UNION SELECT 28 AS idConstant,  4 AS value, '04:00' AS name,  1 AS idConstantType
	UNION SELECT 29 AS idConstant, 5 AS value, '05:00'  AS name,  1 AS idConstantType
	UNION SELECT 30 AS idConstant, 6 AS value, '06:00'  AS name,  1 AS idConstantType
	UNION SELECT 31 AS idConstant, 7 AS value, '07:00'  AS name,  1 AS idConstantType
	UNION SELECT 32 AS idConstant,  8 AS value, '08:00' AS name,  1 AS idConstantType
	UNION SELECT 33 AS idConstant,  9 AS value, '09:00' AS name,  1 AS idConstantType
	UNION SELECT 34 AS idConstant,  10 AS value, '10:00' AS name,  1 AS idConstantType
	UNION SELECT 35 AS idConstant,  11 AS value, '11:00' AS name,  1 AS idConstantType
	UNION SELECT 36 AS idConstant, 12 AS value, '12:00'  AS name,  1 AS idConstantType
	UNION SELECT 37 AS idConstant, 13 AS value, '13:00'   AS name,  1 AS idConstantType
	UNION SELECT 38 AS idConstant, 14 AS value, '14:00'  AS name,  1 AS idConstantType
	UNION SELECT 39 AS idConstant,  15 AS value, '15:00'       AS name,  1 AS idConstantType
	UNION SELECT 40 AS idConstant,  16 AS value, '16:00'       AS name,  1 AS idConstantType
	UNION SELECT 41 AS idConstant,  17 AS value, '17:00'     AS name,  1 AS idConstantType
	UNION SELECT 42 AS idConstant,  18 AS value, '18:00'  AS name,  1 AS idConstantType
	UNION SELECT 43 AS idConstant, 19 AS value, '19:00'   AS name,  1 AS idConstantType
	UNION SELECT 44 AS idConstant, 20 AS value, '20:00'   AS name,  1 AS idConstantType
	UNION SELECT 45 AS idConstant, 21 AS value, '21:00'  AS name,  1 AS idConstantType
	UNION SELECT 46 AS idConstant, 22 AS value, '22:00'   AS name,  1 AS idConstantType
	UNION SELECT 47 AS idConstant, 23 AS value, '23:00'  AS name,  1 AS idConstantType

	UNION SELECT 48 AS idConstant,  5 AS value, 'Week 4'      AS name,  3 AS idConstantType

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblAnalyticConstants AC WHERE AC.idConstant = MAIN.idConstant)

SET IDENTITY_INSERT [tblAnalyticConstants] OFF
/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeUse]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeUse](
	[idTransactionItem]					INT		NOT NULL,
	[idPurchaseItem]					INT		NOT NULL,
	[idCouponCode]						INT		NOT NULL,
	[confirmed]							BIT		NULL,
	[idEnrollment]						INT		NULL
) ON [PRIMARY]
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventType] (
	[idEventType]					INT					IDENTITY(1,1)	NOT NULL,
	[name]							NVARCHAR(255)						NOT NULL,
	[allowPriorSend]				BIT									NULL,
	[allowPostSend]					BIT									NULL,
	[relativeOrder]					INT									NULL	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventType ADD CONSTRAINT [PK_EventType] PRIMARY KEY CLUSTERED (idEventType ASC)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventType]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventType]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEventType ( 
   name
)
KEY INDEX PK_EventType
ON Asentia -- insert index to this catalog

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblEventType] ON

INSERT INTO [tblEventType] (
	idEventType,
	name,
	allowPriorSend,
	allowPostSend,
	relativeOrder
)
SELECT
	idEventType,
	name,
	allowPriorSend,
	allowPostSend,
	relativeOrder
FROM (
	--USER (100)
	SELECT			101 AS idEventType,	'User Created' AS name,					0 AS allowPriorSend, 0 AS allowPostSend, 101 AS relativeOrder
	UNION SELECT	102 AS idEventType,	'User Deleted' AS name,					0 AS allowPriorSend, 0 AS allowPostSend, 102 AS relativeOrder
	UNION SELECT	103 AS idEventType,	'User Expires' AS name,					1 AS allowPriorSend, 1 AS allowPostSend, 103 AS relativeOrder
	UNION SELECT	104 AS idEventType, 'User Registration Approved' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 104 AS relativeOrder
	UNION SELECT	105 AS idEventType, 'User Registration Rejected' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 105 AS relativeOrder	
	UNION SELECT    106 AS idEventType, 'User Message Received' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 106 AS relativeOrder 
	UNION SELECT    107 AS idEventType, 'User Registration Request Submitted' AS name,	0 AS allowPriorSend, 0 AS allowPostSend, 107 AS relativeOrder 

	--ENROLLMENT (200)
	UNION SELECT	201 AS idEventType,	'Course Enrolled' AS name,				0 AS allowPriorSend, 0 AS allowPostSend, 201 AS relativeOrder
	UNION SELECT	202 AS idEventType,	'Course Expires' AS name,				1 AS allowPriorSend, 1 AS allowPostSend, 205 AS relativeOrder
	UNION SELECT	203 AS idEventType,	'Course Revoked' AS name,				0 AS allowPriorSend, 1 AS allowPostSend, 203 AS relativeOrder
	UNION SELECT	204 AS idEventType,	'Course Due' AS name,					1 AS allowPriorSend, 1 AS allowPostSend, 204 AS relativeOrder
	UNION SELECT	205 AS idEventType,	'Course Completed' AS name,				0 AS allowPriorSend, 1 AS allowPostSend, 206 AS relativeOrder
	UNION SELECT	206 AS idEventType,	'Course Start' AS name,					1 AS allowPriorSend, 1 AS allowPostSend, 202 AS relativeOrder
	UNION SELECT	207 AS idEventType, 'Course Enrollment Request Approved' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 207 AS relativeOrder
	UNION SELECT	208 AS idEventType, 'Course Enrollment Request Rejected' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 208 AS relativeOrder	
	UNION SELECT	209 AS idEventType, 'Course Enrollment Request Submitted' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 209 AS relativeOrder	

	--LESSON (300)
	UNION SELECT	301 AS idEventType,	'Lesson Passed' AS name,			0 AS allowPriorSend, 1 AS allowPostSend, 301 AS relativeOrder
	UNION SELECT	302 AS idEventType,	'Lesson Failed' AS name,			0 AS allowPriorSend, 1 AS allowPostSend, 302 AS relativeOrder
	UNION SELECT	303 AS idEventType,	'Lesson Completed' AS name,			0 AS allowPriorSend, 1 AS allowPostSend, 303 AS relativeOrder
	UNION SELECT	304 AS idEventType,	'OJT Request Submitted' AS name,	0 AS allowPriorSend, 0 AS allowPostSend, 304 AS relativeOrder
	UNION SELECT	305 AS idEventType,	'Task Submitted' AS name,			0 AS allowPriorSend, 0 AS allowPostSend, 305 AS relativeOrder

	--SESSION (400)
	UNION SELECT	401 AS idEventType,	'Session Meets' AS name,							1 AS allowPriorSend, 1 AS allowPostSend, 401 AS relativeOrder
	UNION SELECT	402 AS idEventType,	'Session - Instructor Assigned/Changed' AS name,	0 AS allowPriorSend, 0 AS allowPostSend, 402 AS relativeOrder
	UNION SELECT	403 AS idEventType,	'Session - Instructor Removed' AS name,				0 AS allowPriorSend, 0 AS allowPostSend, 403 AS relativeOrder
	UNION SELECT	404 AS idEventType,	'Session - Learner Joined' AS name,					0 AS allowPriorSend, 0 AS allowPostSend, 404 AS relativeOrder
	UNION SELECT	405 AS idEventType,	'Session - Learner Removed' AS name,				0 AS allowPriorSend, 0 AS allowPostSend, 405 AS relativeOrder
	UNION SELECT	406 AS idEventType,	'Session - Time/Location Changed' AS name,			0 AS allowPriorSend, 0 AS allowPostSend, 406 AS relativeOrder
	UNION SELECT	407 AS idEventType,	'Session - Learner Joined to Waitlist' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 407 AS relativeOrder
	UNION SELECT	408 AS idEventType,	'Session - Learner Removed from Waitlist' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 408 AS relativeOrder
	UNION SELECT	409 AS idEventType,	'Session - Learner Promoted from Waitlist' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 409 AS relativeOrder
	UNION SELECT	410 AS idEventType,	'Session - Learner Demoted to Waitlist' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 410 AS relativeOrder

	--LEARNING PATH (500)
	UNION SELECT	501 AS idEventType,	'Learning Path Enrolled' AS name,	0 AS allowPriorSend, 0 AS allowPostSend, 501 AS relativeOrder
	UNION SELECT	502 AS idEventType,	'Learning Path Expires' AS name,	1 AS allowPriorSend, 1 AS allowPostSend, 505 AS relativeOrder
	UNION SELECT	503 AS idEventType,	'Learning Path Revoked' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 503 AS relativeOrder
	UNION SELECT	504 AS idEventType,	'Learning Path Due' AS name,		1 AS allowPriorSend, 1 AS allowPostSend, 504 AS relativeOrder
	UNION SELECT	505 AS idEventType,	'Learning Path Completed' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 506 AS relativeOrder
	UNION SELECT	506 AS idEventType,	'Learning Path Start' AS name,		1 AS allowPriorSend, 1 AS allowPostSend, 502 AS relativeOrder

	--CERTIFICATION (600)
	UNION SELECT	601 AS idEventType,	'Certification Enrolled' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 601 AS relativeOrder
	UNION SELECT	602 AS idEventType,	'Certification Expires' AS name,		1 AS allowPriorSend, 1 AS allowPostSend, 605 AS relativeOrder
	UNION SELECT	603 AS idEventType,	'Certification Revoked' AS name,		0 AS allowPriorSend, 1 AS allowPostSend, 602 AS relativeOrder
	UNION SELECT	604 AS idEventType,	'Certification Awarded' AS name,		0 AS allowPriorSend, 1 AS allowPostSend, 603 AS relativeOrder -- renamed
	UNION SELECT	605 AS idEventType,	'Certification Renewed' AS name,		0 AS allowPriorSend, 1 AS allowPostSend, 604 AS relativeOrder -- renamed
	UNION SELECT	606 AS idEventType,	'Certification Task Submitted' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 606 AS relativeOrder

	--CERTIFICATE (700)
	UNION SELECT	701 AS idEventType,	'Certificate Earned' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 701 AS relativeOrder
	UNION SELECT	702 AS idEventType,	'Certificate Awarded' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 702 AS relativeOrder
	UNION SELECT	703 AS idEventType,	'Certificate Revoked' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 703 AS relativeOrder
	UNION SELECT	704 AS idEventType,	'Certificate Expires' AS name,	1 AS allowPriorSend, 1 AS allowPostSend, 704 AS relativeOrder

	--PURCHASE (800)

	--DISCUSSION (900)
	UNION SELECT	901 AS idEventType,	'Discussion: Course Moderated Message' AS name, 0 AS allowPriorSend, 0 AS allowPostSend, 901 AS relativeOrder
	UNION SELECT	902 AS idEventType,	'Discussion: Group Moderated Message' AS name, 0 AS allowPriorSend, 0 AS allowPostSend, 902 AS relativeOrder

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblEventType ET WHERE ET.idEventType = MAIN.idEventType)

SET IDENTITY_INSERT [tblEventType] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventTypeRecipient]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventTypeRecipient] (
	[idEventTypeRecipient]			INT					IDENTITY(1,1)	NOT NULL,
	[name]							NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventTypeRecipient]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventTypeRecipient ADD CONSTRAINT [PK_EventTypeRecipient] PRIMARY KEY CLUSTERED (idEventTypeRecipient ASC)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblEventTypeRecipient] ON

INSERT INTO [tblEventTypeRecipient] (
	idEventTypeRecipient,
	name
)
SELECT
	idEventTypeRecipient,
	name
FROM (
	SELECT			1 AS idEventTypeRecipient,	'System Administrator' AS name
	UNION SELECT	2 AS idEventTypeRecipient,	'Learner' AS name
	UNION SELECT	3 AS idEventTypeRecipient,	'User' AS name
	UNION SELECT	4 AS idEventTypeRecipient,	'Supervisor' AS name
	UNION SELECT	5 AS idEventTypeRecipient,	'Instructor' AS name
	UNION SELECT	6 AS idEventTypeRecipient,	'Learner(s)' AS name
	UNION SELECT	7 AS idEventTypeRecipient,	'User(s)' AS name
	UNION SELECT	8 AS idEventTypeRecipient,	'Supervisor(s)' AS name
	UNION SELECT	9 AS idEventTypeRecipient,	'Proctor(s)' AS name
	UNION SELECT	10 AS idEventTypeRecipient,	'Moderator(s)' AS name
	UNION SELECT	11 AS idEventTypeRecipient,	'Approver(s)' AS name
	UNION SELECT	12 AS idEventTypeRecipient,	'Specific Email Address' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblEventTypeRecipient ETR WHERE ETR.idEventTypeRecipient = MAIN.idEventTypeRecipient)

SET IDENTITY_INSERT [tblEventTypeRecipient] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventTypeToEventTypeRecipientLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventTypeToEventTypeRecipientLink] (
	[idEventTypeToEventTypeRecipientLink]		INT		IDENTITY (1, 1)		NOT NULL,
	[idEventType]								INT							NOT NULL,
	[idEventTypeRecipient]						INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventTypeToEventTypeRecipientLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventTypeToEventTypeRecipientLink ADD CONSTRAINT [PK_EventTypeToEventTypeRecipientLink] PRIMARY KEY CLUSTERED (idEventTypeToEventTypeRecipientLink ASC)

/**
INSERT VALUES

DO NOT EVER USE THE IDENTITY VALUES FROM THIS TABLE!! THEY ONLY EXIST TO ENABLE REPLICATION.
INSTEAD, ONLY REFERENCE THE eventType AND eventTypeRecipient ID VALUES.

**/

SET IDENTITY_INSERT [tblEventTypeToEventTypeRecipientLink] ON

-- ID 190 IS THE NEXT ID

INSERT INTO [tblEventTypeToEventTypeRecipientLink] (
	idEventTypeToEventTypeRecipientLink,
	idEventType,
	idEventTypeRecipient
)
SELECT
	idEventTypeToEventTypeRecipientLink,
	idEventType,
	idEventTypeRecipient
FROM (
	-- User Created
	SELECT			1 AS idEventTypeToEventTypeRecipientLink, 101 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	2 AS idEventTypeToEventTypeRecipientLink, 101 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	3 AS idEventTypeToEventTypeRecipientLink, 101 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	141 AS idEventTypeToEventTypeRecipientLink, 101 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Deleted
	UNION SELECT	4 AS idEventTypeToEventTypeRecipientLink, 102 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	5 AS idEventTypeToEventTypeRecipientLink, 102 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	6 AS idEventTypeToEventTypeRecipientLink, 102 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	142 AS idEventTypeToEventTypeRecipientLink, 102 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Expires
	UNION SELECT	7 AS idEventTypeToEventTypeRecipientLink, 103 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	8 AS idEventTypeToEventTypeRecipientLink, 103 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	9 AS idEventTypeToEventTypeRecipientLink, 103 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	143 AS idEventTypeToEventTypeRecipientLink, 103 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Registration Approved
	UNION SELECT	108 AS idEventTypeToEventTypeRecipientLink, 104 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	109 AS idEventTypeToEventTypeRecipientLink, 104 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	144 AS idEventTypeToEventTypeRecipientLink, 104 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)	

	-- User Registration Rejected
	UNION SELECT	110 AS idEventTypeToEventTypeRecipientLink, 105 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	111 AS idEventTypeToEventTypeRecipientLink, 105 AS idEventType, 3 AS idEventTypeRecipient -- User	
	UNION SELECT	145 AS idEventTypeToEventTypeRecipientLink, 105 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Message Center Inbox 
	UNION SELECT	120 AS idEventTypeToEventTypeRecipientLink, 106 AS idEventType, 3 AS idEventTypeRecipient -- User
	UNION SELECT	146 AS idEventTypeToEventTypeRecipientLink, 106 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- User Registration Request Submitted
	UNION SELECT	135 AS idEventTypeToEventTypeRecipientLink, 107 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	136 AS idEventTypeToEventTypeRecipientLink, 107 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	137 AS idEventTypeToEventTypeRecipientLink, 107 AS idEventType, 11 AS idEventTypeRecipient -- Approver
	UNION SELECT	147 AS idEventTypeToEventTypeRecipientLink, 107 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Enrolled
	UNION SELECT	10 AS idEventTypeToEventTypeRecipientLink, 201 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	11 AS idEventTypeToEventTypeRecipientLink, 201 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	12 AS idEventTypeToEventTypeRecipientLink, 201 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	148 AS idEventTypeToEventTypeRecipientLink, 201 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Expires
	UNION SELECT	13 AS idEventTypeToEventTypeRecipientLink, 202 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	14 AS idEventTypeToEventTypeRecipientLink, 202 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	15 AS idEventTypeToEventTypeRecipientLink, 202 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	149 AS idEventTypeToEventTypeRecipientLink, 202 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Revoked
	UNION SELECT	16 AS idEventTypeToEventTypeRecipientLink, 203 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	17 AS idEventTypeToEventTypeRecipientLink, 203 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	18 AS idEventTypeToEventTypeRecipientLink, 203 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	150 AS idEventTypeToEventTypeRecipientLink, 203 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Due
	UNION SELECT	19 AS idEventTypeToEventTypeRecipientLink, 204 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	20 AS idEventTypeToEventTypeRecipientLink, 204 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	21 AS idEventTypeToEventTypeRecipientLink, 204 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	151 AS idEventTypeToEventTypeRecipientLink, 204 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Completed
	UNION SELECT	22 AS idEventTypeToEventTypeRecipientLink, 205 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	23 AS idEventTypeToEventTypeRecipientLink, 205 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	24 AS idEventTypeToEventTypeRecipientLink, 205 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	152 AS idEventTypeToEventTypeRecipientLink, 205 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course/Enrollment Start
	UNION SELECT	102 AS idEventTypeToEventTypeRecipientLink, 206 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	103 AS idEventTypeToEventTypeRecipientLink, 206 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	104 AS idEventTypeToEventTypeRecipientLink, 206 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	153 AS idEventTypeToEventTypeRecipientLink, 206 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Enrollment Request Approved
	UNION SELECT	112 AS idEventTypeToEventTypeRecipientLink, 207 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	113 AS idEventTypeToEventTypeRecipientLink, 207 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	114 AS idEventTypeToEventTypeRecipientLink, 207 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	154 AS idEventTypeToEventTypeRecipientLink, 207 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Enrollment Request Rejected
	UNION SELECT	115 AS idEventTypeToEventTypeRecipientLink, 208 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	116 AS idEventTypeToEventTypeRecipientLink, 208 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	117 AS idEventTypeToEventTypeRecipientLink, 208 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	155 AS idEventTypeToEventTypeRecipientLink, 208 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Enrollment Request Submitted
	UNION SELECT	138 AS idEventTypeToEventTypeRecipientLink, 209 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	139 AS idEventTypeToEventTypeRecipientLink, 209 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	140 AS idEventTypeToEventTypeRecipientLink, 209 AS idEventType, 11 AS idEventTypeRecipient -- Approver
	UNION SELECT	156 AS idEventTypeToEventTypeRecipientLink, 209 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Lesson/Session Passed
	UNION SELECT	25 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	26 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	27 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	28 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	157 AS idEventTypeToEventTypeRecipientLink, 301 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Lesson/Session Failed
	UNION SELECT	29 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	30 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	31 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	32 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	158 AS idEventTypeToEventTypeRecipientLink, 302 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Lesson/Session Completed
	UNION SELECT	33 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	34 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	35 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	36 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	159 AS idEventTypeToEventTypeRecipientLink, 303 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)
	
	-- OJT Request Submitted
	UNION SELECT	98 AS idEventTypeToEventTypeRecipientLink, 304 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	99 AS idEventTypeToEventTypeRecipientLink, 304 AS idEventType, 9 AS idEventTypeRecipient -- Proctor(s)
	UNION SELECT	160 AS idEventTypeToEventTypeRecipientLink, 304 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Task Submitted
	UNION SELECT	100 AS idEventTypeToEventTypeRecipientLink, 305 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	101 AS idEventTypeToEventTypeRecipientLink, 305 AS idEventType, 9 AS idEventTypeRecipient -- Proctor(s)
	UNION SELECT	161 AS idEventTypeToEventTypeRecipientLink, 305 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Meets
	UNION SELECT	37 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	38 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	39 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 6 AS idEventTypeRecipient -- Learner(s)
	UNION SELECT	40 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 8 AS idEventTypeRecipient -- Supervisor(s)
	UNION SELECT	162 AS idEventTypeToEventTypeRecipientLink, 401 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Instructor Assigned/Changed
	UNION SELECT	41 AS idEventTypeToEventTypeRecipientLink, 402 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	42 AS idEventTypeToEventTypeRecipientLink, 402 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	43 AS idEventTypeToEventTypeRecipientLink, 402 AS idEventType, 6 AS idEventTypeRecipient -- Learner(s)
	UNION SELECT	163 AS idEventTypeToEventTypeRecipientLink, 402 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Instructor Removed
	UNION SELECT	44 AS idEventTypeToEventTypeRecipientLink, 403 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	45 AS idEventTypeToEventTypeRecipientLink, 403 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	46 AS idEventTypeToEventTypeRecipientLink, 403 AS idEventType, 6 AS idEventTypeRecipient -- Learner(s)
	UNION SELECT	164 AS idEventTypeToEventTypeRecipientLink, 403 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Joined
	UNION SELECT	47 AS idEventTypeToEventTypeRecipientLink, 404 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	48 AS idEventTypeToEventTypeRecipientLink, 404 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	49 AS idEventTypeToEventTypeRecipientLink, 404 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	165 AS idEventTypeToEventTypeRecipientLink, 404 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Removed
	UNION SELECT	50 AS idEventTypeToEventTypeRecipientLink, 405 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	51 AS idEventTypeToEventTypeRecipientLink, 405 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	52 AS idEventTypeToEventTypeRecipientLink, 405 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	166 AS idEventTypeToEventTypeRecipientLink, 405 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Time/Location Changed
	UNION SELECT	53 AS idEventTypeToEventTypeRecipientLink, 406 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	54 AS idEventTypeToEventTypeRecipientLink, 406 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	55 AS idEventTypeToEventTypeRecipientLink, 406 AS idEventType, 6 AS idEventTypeRecipient -- Learner(s)
	UNION SELECT	167 AS idEventTypeToEventTypeRecipientLink, 406 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Joined
	UNION SELECT	123 AS idEventTypeToEventTypeRecipientLink, 407 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	124 AS idEventTypeToEventTypeRecipientLink, 407 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	125 AS idEventTypeToEventTypeRecipientLink, 407 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	168 AS idEventTypeToEventTypeRecipientLink, 407 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Removed
	UNION SELECT	126 AS idEventTypeToEventTypeRecipientLink, 408 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	127 AS idEventTypeToEventTypeRecipientLink, 408 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	128 AS idEventTypeToEventTypeRecipientLink, 408 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	169 AS idEventTypeToEventTypeRecipientLink, 408 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Promoted
	UNION SELECT	129 AS idEventTypeToEventTypeRecipientLink, 409 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	130 AS idEventTypeToEventTypeRecipientLink, 409 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	131 AS idEventTypeToEventTypeRecipientLink, 409 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	170 AS idEventTypeToEventTypeRecipientLink, 409 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Session Learner Demoted
	UNION SELECT	132 AS idEventTypeToEventTypeRecipientLink, 410 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	133 AS idEventTypeToEventTypeRecipientLink, 410 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	134 AS idEventTypeToEventTypeRecipientLink, 410 AS idEventType, 5 AS idEventTypeRecipient -- Instructor
	UNION SELECT	171 AS idEventTypeToEventTypeRecipientLink, 410 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Enrolled/Starts
	UNION SELECT	56 AS idEventTypeToEventTypeRecipientLink, 501 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	57 AS idEventTypeToEventTypeRecipientLink, 501 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	58 AS idEventTypeToEventTypeRecipientLink, 501 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	172 AS idEventTypeToEventTypeRecipientLink, 501 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Expires
	UNION SELECT	59 AS idEventTypeToEventTypeRecipientLink, 502 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	60 AS idEventTypeToEventTypeRecipientLink, 502 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	61 AS idEventTypeToEventTypeRecipientLink, 502 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	173 AS idEventTypeToEventTypeRecipientLink, 502 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Revoked
	UNION SELECT	62 AS idEventTypeToEventTypeRecipientLink, 503 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	63 AS idEventTypeToEventTypeRecipientLink, 503 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	64 AS idEventTypeToEventTypeRecipientLink, 503 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	174 AS idEventTypeToEventTypeRecipientLink, 503 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Due
	UNION SELECT	65 AS idEventTypeToEventTypeRecipientLink, 504 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	66 AS idEventTypeToEventTypeRecipientLink, 504 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	67 AS idEventTypeToEventTypeRecipientLink, 504 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	175 AS idEventTypeToEventTypeRecipientLink, 504 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Completed
	UNION SELECT	68 AS idEventTypeToEventTypeRecipientLink, 505 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	69 AS idEventTypeToEventTypeRecipientLink, 505 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	70 AS idEventTypeToEventTypeRecipientLink, 505 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	176 AS idEventTypeToEventTypeRecipientLink, 505 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Learning Path Start
	UNION SELECT	105 AS idEventTypeToEventTypeRecipientLink, 506 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	106 AS idEventTypeToEventTypeRecipientLink, 506 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	107 AS idEventTypeToEventTypeRecipientLink, 506 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	177 AS idEventTypeToEventTypeRecipientLink, 506 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Enrolled
	UNION SELECT	71 AS idEventTypeToEventTypeRecipientLink, 601 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	72 AS idEventTypeToEventTypeRecipientLink, 601 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	73 AS idEventTypeToEventTypeRecipientLink, 601 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	178 AS idEventTypeToEventTypeRecipientLink, 601 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Expires
	UNION SELECT	74 AS idEventTypeToEventTypeRecipientLink, 602 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	75 AS idEventTypeToEventTypeRecipientLink, 602 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	76 AS idEventTypeToEventTypeRecipientLink, 602 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	179 AS idEventTypeToEventTypeRecipientLink, 602 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Revoked
	UNION SELECT	77 AS idEventTypeToEventTypeRecipientLink, 603 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	78 AS idEventTypeToEventTypeRecipientLink, 603 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	79 AS idEventTypeToEventTypeRecipientLink, 603 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	180 AS idEventTypeToEventTypeRecipientLink, 603 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Awarded
	UNION SELECT	80 AS idEventTypeToEventTypeRecipientLink, 604 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	81 AS idEventTypeToEventTypeRecipientLink, 604 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	82 AS idEventTypeToEventTypeRecipientLink, 604 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	181 AS idEventTypeToEventTypeRecipientLink, 604 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Renewed
	UNION SELECT	83 AS idEventTypeToEventTypeRecipientLink, 605 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	84 AS idEventTypeToEventTypeRecipientLink, 605 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	85 AS idEventTypeToEventTypeRecipientLink, 605 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	182 AS idEventTypeToEventTypeRecipientLink, 605 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certification Task Submitted
	UNION SELECT	118 AS idEventTypeToEventTypeRecipientLink, 606 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	119 AS idEventTypeToEventTypeRecipientLink, 606 AS idEventType, 9 AS idEventTypeRecipient -- Proctor(s)
	UNION SELECT	183 AS idEventTypeToEventTypeRecipientLink, 606 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certificate Earned
	UNION SELECT	86 AS idEventTypeToEventTypeRecipientLink, 701 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	87 AS idEventTypeToEventTypeRecipientLink, 701 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	88 AS idEventTypeToEventTypeRecipientLink, 701 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	184 AS idEventTypeToEventTypeRecipientLink, 701 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certificate Awarded
	UNION SELECT	89 AS idEventTypeToEventTypeRecipientLink, 702 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	90 AS idEventTypeToEventTypeRecipientLink, 702 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	91 AS idEventTypeToEventTypeRecipientLink, 702 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	185 AS idEventTypeToEventTypeRecipientLink, 702 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certificate Revoked
	UNION SELECT	92 AS idEventTypeToEventTypeRecipientLink, 703 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	93 AS idEventTypeToEventTypeRecipientLink, 703 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	94 AS idEventTypeToEventTypeRecipientLink, 703 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	186 AS idEventTypeToEventTypeRecipientLink, 703 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Certificate Expired
	UNION SELECT	95 AS idEventTypeToEventTypeRecipientLink, 704 AS idEventType, 1 AS idEventTypeRecipient -- System Admin
	UNION SELECT	96 AS idEventTypeToEventTypeRecipientLink, 704 AS idEventType, 2 AS idEventTypeRecipient -- Learner
	UNION SELECT	97 AS idEventTypeToEventTypeRecipientLink, 704 AS idEventType, 4 AS idEventTypeRecipient -- Supervisor
	UNION SELECT	187 AS idEventTypeToEventTypeRecipientLink, 704 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Course Moderated Message
	UNION SELECT	121 AS idEventTypeToEventTypeRecipientLink, 901 AS idEventType, 10 AS idEventTypeRecipient -- Moderator
	UNION SELECT	188 AS idEventTypeToEventTypeRecipientLink, 901 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

	-- Group Moderated Message
	UNION SELECT	122 AS idEventTypeToEventTypeRecipientLink, 902 AS idEventType, 10 AS idEventTypeRecipient -- Moderator
	UNION SELECT	189 AS idEventTypeToEventTypeRecipientLink, 902 AS idEventType, 12 AS idEventTypeRecipient -- Specific Email Address(es)

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblEventTypeToEventTypeRecipientLink ETETR WHERE ETETR.idEventTypeToEventTypeRecipientLink = MAIN.idEventTypeToEventTypeRecipientLink)

SET IDENTITY_INSERT [tblEventTypeToEventTypeRecipientLink] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportToReportShortcutsWidgetLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportToReportShortcutsWidgetLink] (
	[idReportToReportShortcutsWidgetLink]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]											INT								NOT NULL, 
	[idReport]											INT								NOT NULL, 
	[idUser]											INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportToReportShortcutsWidgetLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportToReportShortcutsWidgetLink ADD CONSTRAINT [PK_ReportToReportShortcutsWidgetLink] PRIMARY KEY CLUSTERED (idReportToReportShortcutsWidgetLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportToReportShortcutsWidgetLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportToReportShortcutsWidgetLink ADD CONSTRAINT [FK_ReportToReportShortcutsWidgetLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportToReportShortcutsWidgetLink_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportToReportShortcutsWidgetLink ADD CONSTRAINT [FK_ReportToReportShortcutsWidgetLink_Report] FOREIGN KEY (idReport) REFERENCES [tblReport] (idReport)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportToReportShortcutsWidgetLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportToReportShortcutsWidgetLink ADD CONSTRAINT [FK_ReportToReportShortcutsWidgetLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)




IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertification]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertification] (
	[idCertification]					INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[title]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL, 
	[searchTags]						NVARCHAR(512)						NULL, 
	[dtCreated]							DATETIME							NOT NULL,
	[dtModified]						DATETIME							NULL,
	[isDeleted]							BIT									NOT NULL,
	[dtDeleted]							DATETIME							NULL,
	[initialAwardExpiresInterval]		INT									NULL,
	[initialAwardExpiresTimeframe]		NVARCHAR(4)							NULL,
	[renewalExpiresInterval]			INT									NULL,
	[renewalExpiresTimeframe]			NVARCHAR(4)							NULL,
	[accreditingOrganization]			NVARCHAR(255)						NULL,
	[certificationContactName]			NVARCHAR(255)						NULL,
	[certificationContactEmail]			NVARCHAR(255)						NULL,
	[certificationContactPhoneNumber]	NVARCHAR(20)						NULL,
	[isPublished]						BIT									NULL,
	[isClosed]							BIT									NULL,
	[isAnyModule]						BIT									NOT NULL,
	[isRenewalAnyModule]				BIT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertification ADD CONSTRAINT [PK_Certification] PRIMARY KEY CLUSTERED (idCertification ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Certification_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertification ADD CONSTRAINT [FK_Certification_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertification]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertification]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertification (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_Certification
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationLanguage] (
	[idCertificationLanguage]			INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idCertification]					INT									NOT NULL,
	[idLanguage]						INT									NOT NULL,
	[title]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL,
	[searchTags]						NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationLanguage ADD CONSTRAINT [PK_CertificationLanguage] PRIMARY KEY CLUSTERED (idCertificationLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationLanguage ADD CONSTRAINT [FK_CertificationLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationLanguage_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationLanguage ADD CONSTRAINT [FK_CertificationLanguage_Certification] FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationLanguage ADD CONSTRAINT [FK_CertificationLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationLanguage (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_CertificationLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModule]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModule] (
	[idCertificationModule]				INT					IDENTITY(1,1)	NOT NULL,
	[idCertification]					INT									NOT NULL,
	[idSite]							INT									NOT NULL, 
	[title]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL, 
	[isAnyRequirementSet]				BIT									NOT NULL,
	[dtCreated]							DATETIME							NULL,
	[dtModified]						DATETIME							NULL,
	[isDeleted]							BIT									NOT NULL,
	[dtDeleted]							DATETIME							NULL,
	[isInitialRequirement]				BIT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModule]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModule ADD CONSTRAINT [PK_CertificationModule] PRIMARY KEY CLUSTERED (idCertificationModule ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModule_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModule ADD CONSTRAINT [FK_CertificationModule_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModule_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModule ADD CONSTRAINT [FK_CertificationModule_Certification] FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModule]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModule]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModule (
	title,
	shortDescription
)
KEY INDEX PK_CertificationModule
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleLanguage] (
	[idCertificationModuleLanguage]		INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idCertificationModule]				INT									NOT NULL,
	[idLanguage]						INT									NOT NULL,
	[title]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleLanguage ADD CONSTRAINT [PK_CertificationModuleLanguage] PRIMARY KEY CLUSTERED (idCertificationModuleLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleLanguage ADD CONSTRAINT [FK_CertificationModuleLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleLanguage_CertificationModule]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleLanguage ADD CONSTRAINT [FK_CertificationModuleLanguage_CertificationModule] FOREIGN KEY (idCertificationModule) REFERENCES [tblCertificationModule] (idCertificationModule)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleLanguage ADD CONSTRAINT [FK_CertificationModuleLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleLanguage (
	title,
	shortDescription
)
KEY INDEX PK_CertificationModuleLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSet]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirementSet] (
	[idCertificationModuleRequirementSet]	INT					IDENTITY(1,1)	NOT NULL,
	[idCertificationModule]					INT									NOT NULL,
	[idSite]								INT									NOT NULL, 
	[isAny]									BIT									NOT NULL,
	[label]									NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirementSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSet ADD CONSTRAINT [PK_CertificationModuleRequirementSet] PRIMARY KEY CLUSTERED (idCertificationModuleRequirementSet ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSet_CertificationModule]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSet ADD CONSTRAINT [FK_CertificationModuleRequirementSet_CertificationModule] FOREIGN KEY (idCertificationModule) REFERENCES [tblCertificationModule] (idCertificationModule)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSet_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSet ADD CONSTRAINT [FK_CertificationModuleRequirementSet_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSet]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSet]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleRequirementSet (
	label
)
KEY INDEX PK_CertificationModuleRequirementSet
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSetLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirementSetLanguage] (
	[idCertificationModuleRequirementSetLanguage]	INT					IDENTITY(1,1)	NOT NULL,
	[idSite]										INT									NOT NULL, 
	[idCertificationModuleRequirementSet]			INT									NOT NULL,	
	[idLanguage]									INT									NOT NULL,
	[label]											NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirementSetLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSetLanguage ADD CONSTRAINT [PK_CertificationModuleRequirementSetLanguage] PRIMARY KEY CLUSTERED (idCertificationModuleRequirementSetLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSetLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSetLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementSetLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSetLanguage_RequirementSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSetLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementSetLanguage_RequirementSet] FOREIGN KEY (idCertificationModuleRequirementSet) REFERENCES [tblCertificationModuleRequirementSet] (idCertificationModuleRequirementSet)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSetLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSetLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementSetLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSetLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSetLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleRequirementSetLanguage (
	label
)
KEY INDEX PK_CertificationModuleRequirementSetLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirement] (
	[idCertificationModuleRequirement]			INT					IDENTITY(1,1)	NOT NULL,
	[idCertificationModuleRequirementSet]		INT									NOT NULL,
	[idSite]									INT									NOT NULL, 	
	[label]										NVARCHAR(255)						NOT NULL,
	[shortDescription]							NVARCHAR(512)						NULL,
	[requirementType]							INT									NOT NULL,
	[courseCompletionIsAny]						BIT									NULL,
	[forceCourseCompletionInOrder]				BIT									NULL,	
	[numberCreditsRequired]						FLOAT								NULL,
	[courseCreditEligibleCoursesIsAll]			BIT									NULL,
	[documentUploadFileType]					INT									NULL,
	[documentationApprovalRequired]				BIT									NULL,
	[allowSupervisorsToApproveDocumentation]	BIT									NULL,
	[allowExpertsToApproveDocumentation]		BIT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirement ADD CONSTRAINT [PK_CertificationModuleRequirement] PRIMARY KEY CLUSTERED (idCertificationModuleRequirement ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirement_CertificationModuleRequirementSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirement ADD CONSTRAINT [FK_CertificationModuleRequirement_CertificationModuleRequirementSet] FOREIGN KEY (idCertificationModuleRequirementSet) REFERENCES [tblCertificationModuleRequirementSet] (idCertificationModuleRequirementSet)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirement_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirement ADD CONSTRAINT [FK_CertificationModuleRequirement_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirement]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirement]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleRequirement (
	label
)
KEY INDEX PK_CertificationModuleRequirement
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirementLanguage] (
	[idCertificationModuleRequirementLanguage]	INT					IDENTITY(1,1)	NOT NULL,
	[idSite]									INT									NOT NULL, 
	[idCertificationModuleRequirement]			INT									NOT NULL,	
	[idLanguage]								INT									NOT NULL,
	[label]										NVARCHAR(255)						NOT NULL,
	[shortDescription]							NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirementLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementLanguage ADD CONSTRAINT [PK_CertificationModuleRequirementLanguage] PRIMARY KEY CLUSTERED (idCertificationModuleRequirementLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementLanguage_Requirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementLanguage_Requirement] FOREIGN KEY (idCertificationModuleRequirement) REFERENCES [tblCertificationModuleRequirement] (idCertificationModuleRequirement)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleRequirementLanguage (
	label,
	shortDescription
)
KEY INDEX PK_CertificationModuleRequirementLanguage
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementToCourseLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirementToCourseLink] (
	[idCertificationModuleRequirementToCourseLink]	INT		IDENTITY(1,1)	NOT NULL,
	[idSite]										INT						NOT NULL,
	[idCertificationModuleRequirement]				INT						NOT NULL,
	[idCourse]										INT						NOT NULL,
	[order]											INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirementToCourseLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementToCourseLink ADD CONSTRAINT [PK_CertificationModuleRequirementToCourseLink] PRIMARY KEY CLUSTERED (idCertificationModuleRequirementToCourseLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementToCourseLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementToCourseLink ADD CONSTRAINT [FK_CertificationModuleRequirementToCourseLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementToCourseLink_CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementToCourseLink ADD CONSTRAINT FK_CertificationModuleRequirementToCourseLink_CertificationModuleRequirement FOREIGN KEY (idCertificationModuleRequirement) REFERENCES [tblCertificationModuleRequirement] (idCertificationModuleRequirement)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementToCourseLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementToCourseLink ADD CONSTRAINT FK_CertificationModuleRequirementToCourseLink_Course FOREIGN KEY ([idCourse]) REFERENCES [tblCourse] ([idCourse])
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationToCourseCreditLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationToCourseCreditLink] (
	[idCertificationToCourseCreditLink]	INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idCertification]					INT									NOT NULL,
	[idCourse]							INT									NOT NULL,
	[credits]							FLOAT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationToCourseCreditLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToCourseCreditLink ADD CONSTRAINT [PK_CertificationToCourseCreditLink] PRIMARY KEY CLUSTERED (idCertificationToCourseCreditLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToCourseCreditLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToCourseCreditLink ADD CONSTRAINT [FK_CertificationToCourseCreditLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToCourseCreditLink_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToCourseCreditLink ADD CONSTRAINT [FK_CertificationToCourseCreditLink_Certification] FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToCourseCreditLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToCourseCreditLink ADD CONSTRAINT [FK_CertificationToCourseCreditLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationToUserLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationToUserLink] (
	[idCertificationToUserLink]			INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idCertification]					INT									NOT NULL,
	[idUser]							INT									NOT NULL,
	[initialAwardDate]					DATETIME							NULL,
	[certificationTrack]				INT									NOT NULL,
	[currentExpirationDate]				DATETIME							NULL, 
	[currentExpirationInterval]			INT									NULL,
	[currentExpirationTimeframe]		NVARCHAR(4)							NULL,
	[certificationId]					NVARCHAR(255)						NULL,
	[lastExpirationDate]				DATETIME							NULL, -- this is the new "start"
	[idRuleSet]							INT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationToUserLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToUserLink ADD CONSTRAINT [PK_CertificationToUserLink] PRIMARY KEY CLUSTERED (idCertificationToUserLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToUserLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToUserLink ADD CONSTRAINT [FK_CertificationToUserLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToUserLink_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToUserLink ADD CONSTRAINT [FK_CertificationToUserLink_Certification] FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationToUserLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationToUserLink ADD CONSTRAINT [FK_CertificationToUserLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-CertificationModuleRequirement] (
	[idData-CertificationModuleRequirement]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]									INT								NOT NULL, 
	[idCertificationToUserLink]					INT								NOT NULL, 
	[idCertificationModuleRequirement]			INT								NULL,
	[label]										NVARCHAR(255)					NOT NULL,
	[dtCompleted]								DATETIME						NULL,
	[completionDocumentationFilePath]			NVARCHAR(255)					NULL,
	[dtSubmitted]								DATETIME						NULL,
	[isApproved]								BIT								NULL,
	[dtApproved]								DATETIME						NULL,
	[idApprover]								INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD CONSTRAINT [PK_Data-CertificationModuleRequirement] PRIMARY KEY CLUSTERED ([idData-CertificationModuleRequirement] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-CertificationModuleRequirement_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD CONSTRAINT [FK_Data-CertificationModuleRequirement_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-CertificationModuleRequirement_CertificationToUserLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD CONSTRAINT [FK_Data-CertificationModuleRequirement_CertificationToUserLink] FOREIGN KEY ([idCertificationToUserLink]) REFERENCES [tblCertificationToUserLink] ([idCertificationToUserLink])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-CertificationModuleRequirement_CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD CONSTRAINT [FK_Data-CertificationModuleRequirement_CertificationModuleRequirement] FOREIGN KEY ([idCertificationModuleRequirement]) REFERENCES [tblCertificationModuleRequirement] ([idCertificationModuleRequirement])

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblData-CertificationModuleRequirement]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblData-CertificationModuleRequirement]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON [tblData-CertificationModuleRequirement] (
	label
)
KEY INDEX [PK_Data-CertificationModuleRequirement]
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToCertificationLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetToCertificationLink] (
	[idRuleSetToCertificationLink]	INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idRuleSet]						INT							NOT NULL,
	[idCertification]				INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToCertificationLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToCertificationLink ADD CONSTRAINT PK_RuleSetToCertificationLink PRIMARY KEY CLUSTERED (idRuleSetToCertificationLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToCertificationLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToCertificationLink ADD CONSTRAINT [FK_RuleSetToCertificationLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToCertificationLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToCertificationLink ADD CONSTRAINT FK_RuleSetToCertificationLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToCertificationLink_Certification]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToCertificationLink ADD CONSTRAINT FK_RuleSetToCertificationLink_Certification FOREIGN KEY (idCertification) REFERENCES [tblCertification] (idCertification)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblQuizSurvey]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblQuizSurvey] (
	[idQuizSurvey]		INT					IDENTITY(1,1)	NOT NULL,
	[idSite]			INT									NOT NULL,
	[idAuthor]			INT									NOT NULL,
	[type]				INT									NOT NULL,
	[identifier]		NVARCHAR(255)						NOT NULL,
	[guid]				NVARCHAR(36)						NOT NULL,
	[data]				NVARCHAR(MAX)						NOT NULL,
	[isDraft]			BIT									NULL,
	[idContentPackage]	INT									NULL,
	[dtCreated]			DATETIME							NOT NULL,
	[dtModified]		DATETIME							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_QuizSurvey]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblQuizSurvey ADD CONSTRAINT [PK_QuizSurvey] PRIMARY KEY CLUSTERED (idQuizSurvey ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_QuizSurvey_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblQuizSurvey ADD CONSTRAINT [FK_QuizSurvey_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_QuizSurvey_Author]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblQuizSurvey ADD CONSTRAINT [FK_QuizSurvey_Author] FOREIGN KEY (idAuthor) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_QuizSurvey_IdContentPackage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblQuizSurvey ADD CONSTRAINT [FK_QuizSurvey_IdContentPackage] FOREIGN KEY (idContentPackage) REFERENCES [tblContentPackage] (idContentPackage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblQuizSurvey]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblQuizSurvey]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblQuizSurvey (
	identifier
)
KEY INDEX PK_QuizSurvey
ON Asentia -- insert index to this catalog
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSSOToken]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSSOToken] (
	[idSSOToken]	INT			IDENTITY(1,1)	NOT NULL,
	[idSite]		INT							NOT NULL,
	[idUser]		INT							NOT NULL,
	[token]			NVARCHAR(40)				NOT NULL,
	[dtExpires]		DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SSOToken]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSSOToken ADD CONSTRAINT [PK_SSOToken] PRIMARY KEY CLUSTERED (idSSOToken ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SSOToken_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSSOToken ADD CONSTRAINT [FK_SSOToken_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SSOToken_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSSOToken ADD CONSTRAINT [FK_SSOToken_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblWebMeetingOrganizer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblWebMeetingOrganizer] (
	[idWebMeetingOrganizer]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idUser]						INT							NULL,	  -- The user these credentials belong to, null means unlinked.
	[organizerType]					INT							NOT NULL, -- GoToMeeting (2), GoToWebinar (3), GoToTraining (4), WebEx (5) 
																		  -- These are regulated to their "MeetingType" enum int values. 
	[username]						NVARCHAR(255)				NOT NULL,
	[password]						NVARCHAR(255)				NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_WebMeetingOrganizer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblWebMeetingOrganizer ADD CONSTRAINT [PK_WebMeetingOrganizer] PRIMARY KEY CLUSTERED (idWebMeetingOrganizer ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_WebMeetingOrganizer_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblWebmeetingOrganizer ADD CONSTRAINT [FK_WebMeetingOrganizer_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_WebMeetingOrganizer_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblWebMeetingOrganizer ADD CONSTRAINT [FK_WebMeetingOrganizer_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetEngineQueue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetEngineQueue] (
	[idRuleSetEngineQueue]		INT			IDENTITY(1,1)	NOT NULL,
	[idSite]					INT							NOT NULL,
	[triggerObject]				NVARCHAR(50)				NOT NULL,
	[triggerObjectId]			INT							NOT NULL,
	[eventAction]				NVARCHAR(25)				NOT NULL,
	[eventTable]				NVARCHAR(50)				NOT NULL,
	[dtQueued]					DATETIME					NOT NULL,
	[isProcessing]				BIT							NULL,
	[dtProcessed]				DATETIME					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetEngineQueue]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetEngineQueue ADD CONSTRAINT [PK_RuleSetEngineQueue] PRIMARY KEY CLUSTERED (idRuleSetEngineQueue ASC)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLicenseAgreementToSiteLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLicenseAgreementToSiteLink] (
		[idLicenseAgreementToSiteLink]		INT		IDENTITY(1,1)	NOT NULL,
		[idSite]							INT						NOT NULL,
		[dtExecuted]						DATETIME				NULL,
		[executeLicenseAgreementGUID]		NVARCHAR(MAX)			NULL,
		[licenseAgreementIPAddress]			NVARCHAR(MAX)			NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LicenseAgreementToSiteLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLicenseAgreementToSiteLink ADD CONSTRAINT [PK_LicenseAgreementToSiteLink] PRIMARY KEY CLUSTERED (idLicenseAgreementToSiteLink ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LicenseAgreementToSiteLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLicenseAgreementToSiteLink ADD CONSTRAINT [FK_LicenseAgreementToSiteLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteIPRestriction]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteIPRestriction] (
	[idSiteIPRestriction]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[ip]							NVARCHAR(15)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteIPRestriction]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteIPRestriction ADD CONSTRAINT [PK_SiteIPRestriction] PRIMARY KEY CLUSTERED (idSiteIPRestriction ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteIPRestriction_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteIPRestriction ADD CONSTRAINT [FK_SiteIPRestriction_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserLockout]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserLockout] (
	[idUserLockout]						INT			IDENTITY(1,1)	NOT NULL,
	[idSite]							INT							NOT NULL,
	[idUser]							INT							NOT NULL,
	[failCount]							INT							NULL,		-- this value indicates the number of failed login attempt
	[dtLockoutUntil]					DATETIME					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserLockout]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLockout ADD CONSTRAINT [PK_UserLockout] PRIMARY KEY CLUSTERED (idUserLockout ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserLockout_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLockout ADD CONSTRAINT [FK_UserLockout_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserLockout_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLockout ADD CONSTRAINT [FK_UserLockout_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserLog] (
	[idUser]							INT										NOT NULL, 
	[idSite]							INT										NOT NULL, 
	[action]							NVARCHAR(25)							NOT NULL,
	[timestamp]							DATETIME								NOT NULL,
) ON [PRIMARY]

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserLog_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLog ADD CONSTRAINT [FK_UserLog_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserLog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserLog ADD CONSTRAINT [FK_UserLog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
