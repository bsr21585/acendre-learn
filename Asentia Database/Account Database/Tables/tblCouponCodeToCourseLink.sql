IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeToCourseLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeToCourseLink] (
	[idCouponCodeToCourseLink]				INT		IDENTITY(1,1)		NOT NULL,
	[idCouponCode]							INT							NOT NULL,
	[idCourse]								INT							NOT NULL	 
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCodeToCourseLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCourseLink ADD CONSTRAINT [PK_CouponCodeToCourseLink] PRIMARY KEY CLUSTERED (idCouponCodeToCourseLink ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToCourseLink_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCourseLink ADD CONSTRAINT [FK_CouponCodeToCourseLink_CouponCode] FOREIGN KEY (idCouponCode) REFERENCES [tblCouponCode] (idCouponCode)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToCourseLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCourseLink ADD CONSTRAINT [FK_CouponCodeToCourseLink_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)