IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblSiteParam]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblSiteParam] (
	[idSiteParam]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[param]						NVARCHAR(255)					NOT NULL,
	[value]						NVARCHAR(255)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_SiteParam]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteParam ADD CONSTRAINT [PK_SiteParam] PRIMARY KEY CLUSTERED (idSiteParam ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_SiteParam_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblSiteParam ADD CONSTRAINT [FK_SiteParam_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)


/**
PUT ALL VALID SITE PARAMS IN A TEMPORARY TABLE
**/

CREATE TABLE #SiteParams (
	idSite	INT,
	[param]	NVARCHAR(255),
	value	NVARCHAR(255)
)

INSERT INTO #SiteParams (
	idSite,
	[param],
	value
)
SELECT
	idSite,
	[param],
	value
FROM (
	SELECT		 1 AS idSite, 'HomePage.Masthead.Logo'										AS [param], 'homepage_logo.png' AS value
	UNION SELECT 1 AS idSite, 'HomePage.Masthead.SecondaryImage'							AS [param], '' AS value
	UNION SELECT 1 AS idSite, 'HomePage.Masthead.BgColor'									AS [param],	'#595d63' AS value
	UNION SELECT 1 AS idSite, 'HomePage.Masthead.BgImage'									AS [param], '' AS value
	UNION SELECT 1 AS idSite, 'Application.Masthead.Icon'									AS [param],	'application_icon.png' AS value
	UNION SELECT 1 AS idSite, 'Application.Masthead.Logo'									AS [param],	'application_logo.png' AS value
	UNION SELECT 1 AS idSite, 'Application.Masthead.BgColor'								AS [param],	'#595d63' AS value
	UNION SELECT 1 AS idSite, 'Footer.Company'												AS [param],	'ICS Learning Group' AS value
	UNION SELECT 1 AS idSite, 'Footer.Email'												AS [param],	'asentia@icslearninggroup.com' AS value
	UNION SELECT 1 AS idSite, 'Footer.Website'												AS [param],	'http://www.icslearninggroup.com' AS value
	UNION SELECT 1 AS idSite, 'System.SelfRegistration.Enabled'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'System.SelfRegistration.Type'								AS [param],	'builtin' AS value
	UNION SELECT 1 AS idSite, 'System.SelfRegistration.UrlPrefix'							AS [param],	'http://' AS value
	UNION SELECT 1 AS idSite, 'System.SelfRegistration.Url'									AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'System.PasswordReset.Enabled'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'System.DefaultLandingPage'									AS [param],	'/dashboard' AS value
	UNION SELECT 1 AS idSite, 'System.DefaultLandingPage.LocalPath'							AS [param],	'/dashboard' AS value
	UNION SELECT 1 AS idSite, 'System.SimultaneousLogin.Enabled'							AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'System.LoginPriority'										AS [param],	'first' AS value
	UNION SELECT 1 AS idSite, 'System.Lockout.Enabled'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.Lockout.Minutes'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'System.Lockout.Attempts'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Purchases.Enabled'							AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Certificates.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Calendar.Enabled'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Feed.Enabled'									AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Transcript.Enabled'							AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.MyCommunities.Enabled'						AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.MyLearningPaths.Enabled'						AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Leaderboards.Enabled'							AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.EnrollmentStatistics.Enabled'			AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.Leaderboards.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.WallModeration.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.TaskProctoring.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.OJTProctoring.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.ILTRosterManagement.Enabled'			AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Reporter.ReportSubscription.Enabled'					AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Reporter.ReportShortcuts.Enabled'						AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.ShowPrivateCourseCatalogs'							AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Catalog.RequireLogin'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Catalog.HideCourseCodes'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Catalog.Enable'												AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.Search.Enable'										AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.EventCalendar.Enable'								AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Catalog.Communities.Enable'									AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.StandupTraining.Enable'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Catalog.LearningPaths.Enable'								AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'System.API.Enabled'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.API.Key'												AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'System.API.RequireHTTPS'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.TrackingCode'											AS [param],	'' AS value	
	UNION SELECT 1 AS idSite, 'Ecommerce.Enabled'											AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.ProcessingMethod'									AS [param],	'none' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.ProcessingMethod.Verified'							AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Currency'											AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AuthorizeNet.Mode'									AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AuthorizeNet.Login'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AuthorizeNet.TransactionKey'						AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AuthorizeNet.PublicKey'							AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.PayPal.Mode'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.PayPal.Login'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Visa.Enabled'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.MasterCard.Enabled'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.AMEX.Enabled'										AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.DiscoverCard.Enabled'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Name.Required'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.CreditCardNum.Required'						AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.ExpirationDate.Required'						AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.CVV2.Required'								AS [param],	'' AS value	
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Company.Required'							AS [param],	'' AS value	
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Address.Required'							AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.City.Required'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.StateProvince.Required'						AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.PostalCode.Required'							AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Country.Required'							AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.Field.Email.Required'								AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.ChargeDescription'									AS [param],	'' AS value
	UNION SELECT 1 AS idSite, 'Ecommerce.CouponCode.GracePeriod'							AS [param],	'' AS value	
	UNION SELECT 1 AS idSite, 'Leaderboards.CourseTotal.Enable'								AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Leaderboards.CourseCredits.Enable'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Leaderboards.CertificateTotal.Enable'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Leaderboards.CertificateCredits.Enable'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Ratings.Course.Enable'										AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Ratings.Course.Publicize'									AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Certifications.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'QuizzesAndSurveys.Enable'									AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.EmailFromAddressOverride'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerNameOverride'							AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerPortOverride'							AS [param],	'0' AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerUseSslOverride'						AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerUsernameOverride'						AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'System.EmailSmtpServerPasswordOverride'						AS [param],	NULL AS value	
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.On'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Application'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Username'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Password'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.ConsumerSecret'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTM.Plan'											AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.On'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Application'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Username'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Password'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.ConsumerSecret'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTW.Plan'											AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.On'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Application'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Username'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Password'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.ConsumerSecret'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.GTT.Plan'											AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Enable'										AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.On'											AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Application'								AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Username'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Password'									AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'WebMeeting.WebEx.Plan'										AS [param],	NULL AS value
	UNION SELECT 1 AS idSite, 'System.AccountRegistrationApproval'							AS [param],	'False' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.PendingUserRegistrations.Enabled'		AS [param], 'False' AS value 
	UNION SELECT 1 AS idSite, 'Widget.Administrator.PendingCourseEnrollments.Enabled'		AS [param], 'True' AS value 
	UNION SELECT 1 AS idSite, 'Widget.Learner.MyCertifications.Enabled'						AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.CertificationTaskProctoring.Enabled'	AS [param],	'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Documents.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.ILTSessions.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Administrator.AdministrativeTasks.Enabled'			AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Widget.Learner.Enrollments.Mode'								AS [param], 'tile' AS value
	UNION SELECT 1 AS idSite, 'System.IPAddressRestriction'									AS [param], 'none' AS value -- possible values are: none, absolute, login	
	UNION SELECT 1 AS idSite, 'Iconset.Active'												AS [param], 'standard' AS value
	UNION SELECT 1 AS idSite, 'Iconset.Preview'												AS [param], '' AS value
	UNION SELECT 1 AS idSite, 'Theme.Active'												AS [param], 'classic' AS value
	UNION SELECT 1 AS idSite, 'Theme.Preview'												AS [param], '' AS value
	---------------------------------------------------------------------------------------------------------------------------------------------------
	-- OpenSesame IntegrationName, IntegrationKey, and IntegrationSecret are set for ICS's OpenSesame reseller account by system default. They're done 
	-- here as site parameters so that we could set client or even portal specific integrations to the client's reseller account if we wanted to (if 
	-- they paid for it). Generally, there should never be portal-specific site params for these, unless we're using the client's OpenSesame reseller 
	-- account.
	UNION SELECT 1 AS idSite, 'OpenSesame.IntegrationName'									AS [param], 'icslearning' AS value
	UNION SELECT 1 AS idSite, 'OpenSesame.IntegrationKey'									AS [param], '4d3ee8a9-35cf-4c79-9235-e6f80f37291f' AS value
	UNION SELECT 1 AS idSite, 'OpenSesame.IntegrationSecret'								AS [param], '99b56932-483c-4b64-a1ec-952ba64c0964' AS value
	---------------------------------------------------------------------------------------------------------------------------------------------------
	UNION SELECT 1 AS idSite, 'OpenSesame.Enable'											AS [param], 'False' AS value -- these are the portal-specific OpenSesame params	
	UNION SELECT 1 AS idSite, 'OpenSesame.TenantID'											AS [param], NULL AS value
	UNION SELECT 1 AS idSite, 'OpenSesame.TenantConsumerKey'								AS [param], NULL AS value
	UNION SELECT 1 AS idSite, 'OpenSesame.TenantConsumerSecret'								AS [param], NULL AS value
	-----------------------------------------------------------------------------------------------------------------------------------------------
	-- Privacy Parameters
	--------------------------------------------------------------------------------------------------------------------------------------------
	UNION SELECT 1 AS idSite, 'Privacy.AllowUserEmailOptOut'								AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Privacy.PermanentlyRemoveDeletedUsers'						AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Privacy.AllowSelfDeletion'									AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Privacy.RequireCookieConsent'								AS [param], 'False' AS value
	-------------------------------------------------------------------------------------------------------------------------------------------------
	-- User Agreement
	--------------------------------------------------------------------------------------------------------------------------------------------
	UNION SELECT 1 AS idSite, 'UserAgreement.Display'										AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'UserAgreement.Required'										AS [param], 'False' AS value

	-------------------------------------------------------------------------------------------------------------------------------------------------
	-- Feature Hooks
	--------------------------------------------------------------------------------------------------------------------------------------------
	UNION SELECT 1 AS idSite, 'Feature.DashboardMode'										AS [param], 'Standard' AS value -- possible values are 
	UNION SELECT 1 AS idSite, 'Feature.System.xAPIEndpoints.Enabled'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.UserAccountMerging.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.UserProfileFiles.Enabled'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.LiteUserFieldConfiguration.Enabled'			AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.UserRegistrationApproval.Enabled'				AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.System.ObjectSpecificEmailNotifications.Enabled'		AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Interface.ImageEditor.Enabled'						AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Interface.CSSEditor.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.SelfEnrollmentApproval.Enabled'		AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.TaskModules.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.OJTModules.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.LearningPaths.Enabled'				AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.ILTResources.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.StandaloneILTEnrollment.Enabled'		AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.RestrictedRuleCriteria.Enabled'		AS [param], 'False' AS value
	UNION SELECT 1 AS idSite, 'Feature.Reporting.PDFExport.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.MessageCenter.Enabled'								AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Roles.Rules.Enabled'									AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Groups.CommunityFunctions.Enabled'					AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.LearningAssets.CourseDiscussion.Enabled'				AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Groups.Documents.Enabled'							AS [param], 'True' AS value
	UNION SELECT 1 AS idSite, 'Feature.Groups.Discussion.Enabled'							AS [param], 'True' AS value


) MAIN

/**
DELETE UNDEFINED PARAMS FROM TABLE
**/

DELETE FROM tblSiteParam WHERE NOT EXISTS (SELECT 1 FROM #SiteParams WHERE #SiteParams.[param] = tblSiteParam.[param])

/**
INSERT PARAMS INTO TABLE
**/

INSERT INTO tblSiteParam (
	idSite,
	[param],
	value
)
SELECT
	idSite,
	[param],
	value
FROM #SiteParams
WHERE NOT EXISTS (SELECT 1 FROM tblSiteParam SP WHERE SP.idSite = #SiteParams.idSite AND SP.[param] = #SiteParams.[param])

/**
DROP TEMP TABLE
**/
DROP TABLE #SiteParams