IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirementLanguage] (
	[idCertificationModuleRequirementLanguage]	INT					IDENTITY(1,1)	NOT NULL,
	[idSite]									INT									NOT NULL, 
	[idCertificationModuleRequirement]			INT									NOT NULL,	
	[idLanguage]								INT									NOT NULL,
	[label]										NVARCHAR(255)						NOT NULL,
	[shortDescription]							NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirementLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementLanguage ADD CONSTRAINT [PK_CertificationModuleRequirementLanguage] PRIMARY KEY CLUSTERED (idCertificationModuleRequirementLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementLanguage_Requirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementLanguage_Requirement] FOREIGN KEY (idCertificationModuleRequirement) REFERENCES [tblCertificationModuleRequirement] (idCertificationModuleRequirement)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementLanguage ADD CONSTRAINT [FK_CertificationModuleRequirementLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleRequirementLanguage (
	label,
	shortDescription
)
KEY INDEX PK_CertificationModuleRequirementLanguage
ON Asentia -- insert index to this catalog