IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblEventType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblEventType] (
	[idEventType]					INT					IDENTITY(1,1)	NOT NULL,
	[name]							NVARCHAR(255)						NOT NULL,
	[allowPriorSend]				BIT									NULL,
	[allowPostSend]					BIT									NULL,
	[relativeOrder]					INT									NULL	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_EventType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblEventType ADD CONSTRAINT [PK_EventType] PRIMARY KEY CLUSTERED (idEventType ASC)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventType]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblEventType]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblEventType ( 
   name
)
KEY INDEX PK_EventType
ON Asentia -- insert index to this catalog

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblEventType] ON

INSERT INTO [tblEventType] (
	idEventType,
	name,
	allowPriorSend,
	allowPostSend,
	relativeOrder
)
SELECT
	idEventType,
	name,
	allowPriorSend,
	allowPostSend,
	relativeOrder
FROM (
	--USER (100)
	SELECT			101 AS idEventType,	'User Created' AS name,					0 AS allowPriorSend, 0 AS allowPostSend, 101 AS relativeOrder
	UNION SELECT	102 AS idEventType,	'User Deleted' AS name,					0 AS allowPriorSend, 0 AS allowPostSend, 102 AS relativeOrder
	UNION SELECT	103 AS idEventType,	'User Expires' AS name,					1 AS allowPriorSend, 1 AS allowPostSend, 103 AS relativeOrder
	UNION SELECT	104 AS idEventType, 'User Registration Approved' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 104 AS relativeOrder
	UNION SELECT	105 AS idEventType, 'User Registration Rejected' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 105 AS relativeOrder	
	UNION SELECT    106 AS idEventType, 'User Message Received' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 106 AS relativeOrder 
	UNION SELECT    107 AS idEventType, 'User Registration Request Submitted' AS name,	0 AS allowPriorSend, 0 AS allowPostSend, 107 AS relativeOrder 

	--ENROLLMENT (200)
	UNION SELECT	201 AS idEventType,	'Course Enrolled' AS name,				0 AS allowPriorSend, 0 AS allowPostSend, 201 AS relativeOrder
	UNION SELECT	202 AS idEventType,	'Course Expires' AS name,				1 AS allowPriorSend, 1 AS allowPostSend, 205 AS relativeOrder
	UNION SELECT	203 AS idEventType,	'Course Revoked' AS name,				0 AS allowPriorSend, 1 AS allowPostSend, 203 AS relativeOrder
	UNION SELECT	204 AS idEventType,	'Course Due' AS name,					1 AS allowPriorSend, 1 AS allowPostSend, 204 AS relativeOrder
	UNION SELECT	205 AS idEventType,	'Course Completed' AS name,				0 AS allowPriorSend, 1 AS allowPostSend, 206 AS relativeOrder
	UNION SELECT	206 AS idEventType,	'Course Start' AS name,					1 AS allowPriorSend, 1 AS allowPostSend, 202 AS relativeOrder
	UNION SELECT	207 AS idEventType, 'Course Enrollment Request Approved' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 207 AS relativeOrder
	UNION SELECT	208 AS idEventType, 'Course Enrollment Request Rejected' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 208 AS relativeOrder	
	UNION SELECT	209 AS idEventType, 'Course Enrollment Request Submitted' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 209 AS relativeOrder	

	--LESSON (300)
	UNION SELECT	301 AS idEventType,	'Lesson Passed' AS name,			0 AS allowPriorSend, 1 AS allowPostSend, 301 AS relativeOrder
	UNION SELECT	302 AS idEventType,	'Lesson Failed' AS name,			0 AS allowPriorSend, 1 AS allowPostSend, 302 AS relativeOrder
	UNION SELECT	303 AS idEventType,	'Lesson Completed' AS name,			0 AS allowPriorSend, 1 AS allowPostSend, 303 AS relativeOrder
	UNION SELECT	304 AS idEventType,	'OJT Request Submitted' AS name,	0 AS allowPriorSend, 0 AS allowPostSend, 304 AS relativeOrder
	UNION SELECT	305 AS idEventType,	'Task Submitted' AS name,			0 AS allowPriorSend, 0 AS allowPostSend, 305 AS relativeOrder

	--SESSION (400)
	UNION SELECT	401 AS idEventType,	'Session Meets' AS name,							1 AS allowPriorSend, 1 AS allowPostSend, 401 AS relativeOrder
	UNION SELECT	402 AS idEventType,	'Session - Instructor Assigned/Changed' AS name,	0 AS allowPriorSend, 0 AS allowPostSend, 402 AS relativeOrder
	UNION SELECT	403 AS idEventType,	'Session - Instructor Removed' AS name,				0 AS allowPriorSend, 0 AS allowPostSend, 403 AS relativeOrder
	UNION SELECT	404 AS idEventType,	'Session - Learner Joined' AS name,					0 AS allowPriorSend, 0 AS allowPostSend, 404 AS relativeOrder
	UNION SELECT	405 AS idEventType,	'Session - Learner Removed' AS name,				0 AS allowPriorSend, 0 AS allowPostSend, 405 AS relativeOrder
	UNION SELECT	406 AS idEventType,	'Session - Time/Location Changed' AS name,			0 AS allowPriorSend, 0 AS allowPostSend, 406 AS relativeOrder
	UNION SELECT	407 AS idEventType,	'Session - Learner Joined to Waitlist' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 407 AS relativeOrder
	UNION SELECT	408 AS idEventType,	'Session - Learner Removed from Waitlist' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 408 AS relativeOrder
	UNION SELECT	409 AS idEventType,	'Session - Learner Promoted from Waitlist' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 409 AS relativeOrder
	UNION SELECT	410 AS idEventType,	'Session - Learner Demoted to Waitlist' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 410 AS relativeOrder

	--LEARNING PATH (500)
	UNION SELECT	501 AS idEventType,	'Learning Path Enrolled' AS name,	0 AS allowPriorSend, 0 AS allowPostSend, 501 AS relativeOrder
	UNION SELECT	502 AS idEventType,	'Learning Path Expires' AS name,	1 AS allowPriorSend, 1 AS allowPostSend, 505 AS relativeOrder
	UNION SELECT	503 AS idEventType,	'Learning Path Revoked' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 503 AS relativeOrder
	UNION SELECT	504 AS idEventType,	'Learning Path Due' AS name,		1 AS allowPriorSend, 1 AS allowPostSend, 504 AS relativeOrder
	UNION SELECT	505 AS idEventType,	'Learning Path Completed' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 506 AS relativeOrder
	UNION SELECT	506 AS idEventType,	'Learning Path Start' AS name,		1 AS allowPriorSend, 1 AS allowPostSend, 502 AS relativeOrder

	--CERTIFICATION (600)
	UNION SELECT	601 AS idEventType,	'Certification Enrolled' AS name,		0 AS allowPriorSend, 0 AS allowPostSend, 601 AS relativeOrder
	UNION SELECT	602 AS idEventType,	'Certification Expires' AS name,		1 AS allowPriorSend, 1 AS allowPostSend, 605 AS relativeOrder
	UNION SELECT	603 AS idEventType,	'Certification Revoked' AS name,		0 AS allowPriorSend, 1 AS allowPostSend, 602 AS relativeOrder
	UNION SELECT	604 AS idEventType,	'Certification Awarded' AS name,		0 AS allowPriorSend, 1 AS allowPostSend, 603 AS relativeOrder -- renamed
	UNION SELECT	605 AS idEventType,	'Certification Renewed' AS name,		0 AS allowPriorSend, 1 AS allowPostSend, 604 AS relativeOrder -- renamed
	UNION SELECT	606 AS idEventType,	'Certification Task Submitted' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 606 AS relativeOrder

	--CERTIFICATE (700)
	UNION SELECT	701 AS idEventType,	'Certificate Earned' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 701 AS relativeOrder
	UNION SELECT	702 AS idEventType,	'Certificate Awarded' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 702 AS relativeOrder
	UNION SELECT	703 AS idEventType,	'Certificate Revoked' AS name,	0 AS allowPriorSend, 1 AS allowPostSend, 703 AS relativeOrder
	UNION SELECT	704 AS idEventType,	'Certificate Expires' AS name,	1 AS allowPriorSend, 1 AS allowPostSend, 704 AS relativeOrder

	--PURCHASE (800)

	--DISCUSSION (900)
	UNION SELECT	901 AS idEventType,	'Discussion: Course Moderated Message' AS name, 0 AS allowPriorSend, 0 AS allowPostSend, 901 AS relativeOrder
	UNION SELECT	902 AS idEventType,	'Discussion: Group Moderated Message' AS name, 0 AS allowPriorSend, 0 AS allowPostSend, 902 AS relativeOrder

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblEventType ET WHERE ET.idEventType = MAIN.idEventType)

SET IDENTITY_INSERT [tblEventType] OFF