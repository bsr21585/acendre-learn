/* THIS IS GOING TO BE REMOVED */

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathFeedModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathFeedModerator] (
	[idLearningPathFeedModerator]			INT			IDENTITY (1, 1)		NOT NULL,
	[idSite]								INT								NOT NULL,
	[idLearningPathFeed]					INT								NOT NULL,
	[idModerator]							INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathFeedModerator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedModerator] ADD CONSTRAINT [PK_LearningPathFeedModerator] PRIMARY KEY CLUSTERED (idLearningPathFeedModerator ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedModerator_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedModerator] ADD CONSTRAINT [FK_LearningPathFeedModerator_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedModerator_LearningPathFeed]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedModerator] ADD CONSTRAINT [FK_LearningPathFeedModerator_LearningPathFeed] FOREIGN KEY (idLearningPathFeed) REFERENCES [tblLearningPathFeed] (idLearningPathFeed)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathFeedModerator_Moderator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathFeedModerator] ADD CONSTRAINT [FK_LearningPathFeedModerator_Moderator] FOREIGN KEY (idModerator) REFERENCES [tblUser] (idUser)