IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-TinCan]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-TinCan] (
	[idData-TinCan]								INT				IDENTITY(1,1)	NOT NULL,
	[idData-Lesson]                             INT                             NULL,
	[idData-TinCanParent]						INT								NULL,
	[idSite]									INT								NOT NULL,
	[idEndpoint]								INT								NULL,
	[isInternalAPI]								BIT								NOT NULL,
	[statementId]								NVARCHAR(50)					NULL,
	[actor]										NVARCHAR(MAX)					NOT NULL,
	[verbId]									INT					            NOT NULL,
	[verb]										NVARCHAR(200)					NOT NULL,
	[activityId]								NVARCHAR(300)					NULL,
	[mboxObject]								NVARCHAR(100)					NULL,
	[mboxSha1SumObject]							NVARCHAR(100)					NULL,
	[openIdObject]								NVARCHAR(100)					NULL,
	[accountObject]								NVARCHAR(100)					NULL,
	[mboxActor]									NVARCHAR(100)					NULL,
	[mboxSha1SumActor]							NVARCHAR(100)					NULL,
	[openIdActor]								NVARCHAR(100)					NULL,
	[accountActor]								NVARCHAR(100)					NULL,
	[mboxAuthority]								NVARCHAR(100)					NULL,
	[mboxSha1SumAuthority]						NVARCHAR(100)					NULL,
	[openIdAuthority]							NVARCHAR(100)					NULL,
	[accountAuthority]							NVARCHAR(100)					NULL,
	[mboxTeam]									NVARCHAR(100)					NULL,
	[mboxSha1SumTeam]							NVARCHAR(100)					NULL,
	[openIdTeam]								NVARCHAR(100)					NULL,
	[accountTeam]								NVARCHAR(100)					NULL,
	[mboxInstructor]							NVARCHAR(100)					NULL,
	[mboxSha1SumInstructor]						NVARCHAR(100)					NULL,
	[openIdInstructor]							NVARCHAR(100)					NULL,
	[accountInstructor]							NVARCHAR(100)					NULL,
	[object]									NVARCHAR(MAX)					NOT NULL,
	[registration]								NVARCHAR(50)					NULL,
	[statement]									NVARCHAR(MAX)					NOT NULL,
	[isVoidingStatement]						BIT								NOT NULL,
	[isStatementVoided]							BIT								NOT NULL,
	[dtStored]									DATETIME						NOT	NULL,
	[scoreScaled]								FLOAT                           NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-TinCan]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [PK_Data-TinCan] PRIMARY KEY CLUSTERED ([idData-TinCan] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCan_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [FK_Data-TinCan_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCan_IdEndpoint]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [FK_Data-TinCan_IdEndpoint] FOREIGN KEY (idEndpoint) REFERENCES [tblxAPIoAuthConsumer] (idxAPIoAuthConsumer)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCan_Verb]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [FK_Data-TinCan_Verb] FOREIGN KEY (verbId) REFERENCES [tblTinCanVerb] (idVerb)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCan_Data-Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [tblData-TinCan] ADD CONSTRAINT [FK_Data-TinCan_Data-Lesson] FOREIGN KEY ([idData-Lesson]) REFERENCES [tblData-Lesson] ([idData-Lesson])