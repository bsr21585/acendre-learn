IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCatalog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCatalog] (
	[idCatalog]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL, 
	[idParent]					INT								NULL,
	[title]						NVARCHAR(255)					NOT NULL,
	[shortDescription]			NVARCHAR(512)					NULL, 
	[longDescription]			NVARCHAR(MAX)					NULL, 
	[isPrivate]					BIT								NOT NULL,
	[isClosed]					BIT								NULL,
	[cost]						FLOAT							NULL,
	[costType]					INT								NULL,
	[dtCreated]					DATETIME						NULL,
	[dtModified]				DATETIME						NULL,
	[order]						INT								NULL,
	[searchTags]				NVARCHAR(512)					NULL,
	[avatar]					NVARCHAR(255)					NULL,
	[shortcode]					NVARCHAR(10)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalog ADD CONSTRAINT [PK_Catalog] PRIMARY KEY CLUSTERED (idCatalog ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Catalog_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalog ADD CONSTRAINT [FK_Catalog_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Catalog_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalog ADD CONSTRAINT [FK_Catalog_Catalog] FOREIGN KEY (idParent) REFERENCES [tblCatalog] (idCatalog)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalog]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalog]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCatalog (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_Catalog
ON Asentia -- insert index to this catalog