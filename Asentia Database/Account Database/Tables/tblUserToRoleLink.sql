IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToRoleLink] (
	[idUserToRoleLink]				INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idUser]						INT							NOT NULL,
	[idRole]						INT							NOT NULL,
	[idRuleSet]						INT							NULL,		-- this value indicates the ruleset that is responsible for this link. NULL means the role was manually assigned.
	[created]						DATETIME					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserRoleLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRoleLink ADD CONSTRAINT [PK_UserRoleLink] PRIMARY KEY CLUSTERED (idUserToRoleLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRoleLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRoleLink ADD CONSTRAINT [FK_UserToRoleLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRoleLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRoleLink ADD CONSTRAINT [FK_UserToRoleLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToRoleLink_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToRoleLink ADD CONSTRAINT [FK_UserToRoleLink_Role] FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)