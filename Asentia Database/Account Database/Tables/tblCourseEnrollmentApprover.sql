IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseEnrollmentApprover] (
	[idCourseEnrollmentApprover]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL, 
	[idCourse]							INT						NOT NULL, 
	[idUser]							INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseEnrollmentApprover]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseEnrollmentApprover ADD CONSTRAINT [PK_CourseEnrollmentApprover] PRIMARY KEY CLUSTERED (idCourseEnrollmentApprover ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseEnrollmentApprover_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseEnrollmentApprover ADD CONSTRAINT [FK_CourseEnrollmentApprover_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseEnrollmentApprover_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseEnrollmentApprover ADD CONSTRAINT [FK_CourseEnrollmentApprover_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseEnrollmentApprover_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseEnrollmentApprover ADD CONSTRAINT [FK_CourseEnrollmentApprover_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)