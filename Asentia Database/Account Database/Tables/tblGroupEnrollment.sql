IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroupEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroupEnrollment] (
	[idGroupEnrollment]						INT			IDENTITY(1,1)	NOT NULL,
	[idSite]								INT							NOT NULL,
	[idCourse]								INT							NOT NULL,
	[idGroup]								INT							NOT NULL,
	[idTimezone]							INT							NOT NULL,	-- indicates what TZ the data should be converted to for display in the 'create/modify/delete' interface
	[isLockedByPrerequisites]				BIT							NOT NULL,
	[dtStart]								DATETIME					NOT NULL,	-- the date in which enrollments should start being inherited by members of the group, there is no "lifespan end," just delete the record when it should stop
	[dtCreated]								DATETIME					NOT NULL,
	[dueInterval]							INT							NULL,
	[dueTimeframe]							NVARCHAR(4)					NULL,
	[expiresFromStartInterval]				INT							NULL,
	[expiresFromStartTimeframe]				NVARCHAR(4)					NULL,
	[expiresFromFirstLaunchInterval]		INT							NULL,
	[expiresFromFirstLaunchTimeframe]		NVARCHAR(4)					NULL
	--[expiresFromCompletionInterval]			INT							NULL,
	--[expiresFromCompletionTimeframe]		NVARCHAR(4)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_GroupEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [PK_GroupEnrollment] PRIMARY KEY CLUSTERED ([idGroupEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollment_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [FK_GroupEnrollment_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollment_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [FK_GroupEnrollment_Group] FOREIGN KEY (idGroup) REFERENCES [tblGroup] (idGroup)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [FK_GroupEnrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_GroupEnrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblGroupEnrollment] ADD CONSTRAINT [FK_GroupEnrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)