IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRuleSetLanguage] (
	[idRuleSetLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idRuleSet]						INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[label]							NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLanguage ADD CONSTRAINT [PK_RuleSetLanguage] PRIMARY KEY CLUSTERED (idRuleSetLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLanguage ADD CONSTRAINT [FK_RuleSetLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLanguage_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLanguage ADD CONSTRAINT [FK_RuleSetLanguage_RuleSet] FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetLanguage ADD CONSTRAINT [FK_RuleSetLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRuleSetLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRuleSetLanguage (
	label
)
KEY INDEX PK_RuleSetLanguage
ON Asentia -- insert index to this catalog