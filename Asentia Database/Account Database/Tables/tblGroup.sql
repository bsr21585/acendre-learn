IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblGroup] (
	[idGroup]							INT					IDENTITY (1, 1)		NOT NULL, 
	[idSite]							INT										NOT NULL, 
	[name]								NVARCHAR(255)							NOT NULL,
	[avatar]							NVARCHAR(255)							NULL,
	[shortDescription]					NVARCHAR(512)							NULL,
	[longDescription]					NVARCHAR(MAX)							NULL,
	[primaryGroupToken]					NVARCHAR(255)							NULL,
	[objectGUID]						UNIQUEIDENTIFIER						NULL,
	[distinguishedName]					NVARCHAR(512)							NULL,
	[isSelfJoinAllowed]					BIT										NULL,
	[isFeedActive]                      BIT                                     NULL,
	[isFeedModerated]                   BIT                                     NULL,
	[membershipIsPublicized]            BIT                                     NULL,
	[searchTags]						NVARCHAR(512)							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Group]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroup ADD CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED (idGroup ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Group_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblGroup ADD CONSTRAINT [FK_Group_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroup]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroup]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblGroup (
	name,
	shortDescription,
	searchTags
)
KEY INDEX PK_Group
ON Asentia -- insert index to this catalog