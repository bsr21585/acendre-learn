IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTransactionResponse]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTransactionResponse] (
	[id]					        INT				IDENTITY(1, 1)	NOT NULL,
	[idSite]                        INT                             NOT NULL,
	[idUser]                        INT								NOT NULL,
	[cardType]                      NVARCHAR(35)                    NULL,
	[invoiceNumber]					NVARCHAR(25) 					NULL,
	[cardNumber]					NVARCHAR(25)					NULL,
	[idTransaction]                 NVARCHAR(30)                    NULL,
	[amount]                        FLOAT                           NULL,
	[responseCode]                  NVARCHAR(255)                   NULL,
	[responseMessage]               NVARCHAR(MAX)                   NULL,
	[isApproved]                    BIT                             NOT NULL,
	[isValidated]                   BIT                             NOT NULL,
	[dtTransaction]                 DATETIME                        NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_TransactionResponse]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTransactionResponse ADD CONSTRAINT [PK_TransactionResponse] PRIMARY KEY CLUSTERED (id ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TransactionResponse_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTransactionResponse ADD CONSTRAINT [FK_TransactionResponse_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TransactionResponse_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTransactionResponse ADD CONSTRAINT [FK_TransactionResponse_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)