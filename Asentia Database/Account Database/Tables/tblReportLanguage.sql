IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReportLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReportLanguage] (
	[idReportLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idReport]						INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ReportLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportLanguage ADD CONSTRAINT [PK_ReportLanguage] PRIMARY KEY CLUSTERED (idReportLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportLanguage ADD CONSTRAINT [FK_ReportLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportLanguage_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportLanguage ADD CONSTRAINT [FK_ReportLanguage_Report] FOREIGN KEY (idReport) REFERENCES [tblReport] (idReport)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ReportLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReportLanguage ADD CONSTRAINT [FK_ReportLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReportLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReportLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblReportLanguage (
	title
)
KEY INDEX PK_ReportLanguage
ON Asentia -- insert index to this catalog