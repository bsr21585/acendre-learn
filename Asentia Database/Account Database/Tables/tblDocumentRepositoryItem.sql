IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDocumentRepositoryItem] (
	[idDocumentRepositoryItem]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[idDocumentRepositoryObjectType]	INT								NOT NULL,
	[idObject]							INT								NOT NULL,
	[idDocumentRepositoryFolder]		INT								NULL,
	[idOwner]							INT								NOT NULL,
	[label]                             NVARCHAR(255)                   NOT NULL,
	[fileName]							NVARCHAR(255)					NOT NULL,
	[kb]								INT								NOT NULL,
	[searchTags]						NVARCHAR(512)					NULL,
	[isPrivate]							BIT								NULL,
	[idLanguage]						INT								NULL,
	[isAllLanguages]					BIT								NULL,
	[dtCreated]							DATETIME						NOT NULL,
	[isDeleted]							BIT								NULL,
	[dtDeleted]							DATETIME						NULL,
	[isVisibleToUser]					BIT								NULL,
	[isUploadedByUser]					BIT								NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DocumentRepositoryItem]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [PK_DocumentRepositoryItem] PRIMARY KEY CLUSTERED ([idDocumentRepositoryItem] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_ObjectType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_ObjectType] FOREIGN KEY (idDocumentRepositoryObjectType) REFERENCES [tblDocumentRepositoryObjectType] (idDocumentRepositoryObjectType)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_DocumentRepositoryFolder]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_DocumentRepositoryFolder] FOREIGN KEY (idDocumentRepositoryFolder) REFERENCES [tblDocumentRepositoryFolder] (idDocumentRepositoryFolder)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_Owner]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_Owner] FOREIGN KEY (idOwner) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItem_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblDocumentRepositoryItem] ADD CONSTRAINT [FK_DocumentRepositoryItem_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItem]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItem]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblDocumentRepositoryItem (
	[fileName],
	label,
	searchTags
)
KEY INDEX PK_DocumentRepositoryItem
ON Asentia -- insert index to this catalog