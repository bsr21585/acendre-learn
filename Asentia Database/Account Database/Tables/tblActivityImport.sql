IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblActivityImport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblActivityImport] (
	[idActivityImport]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL, 
	[idUser]							INT								NOT NULL,
	[timestamp]							DATETIME						NOT NULL,
	[idUserImported]					INT								NULL,
	[importFileName]					NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ActivityImport]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblActivityImport ADD CONSTRAINT [PK_ActivityImport] PRIMARY KEY CLUSTERED (idActivityImport ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ActivityImport_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblActivityImport ADD CONSTRAINT [FK_ActivityImport_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ActivityImport_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblActivityImport ADD CONSTRAINT [FK_ActivityImport_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ActivityImport_UserImported]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblActivityImport ADD CONSTRAINT [FK_ActivityImport_UserImported] FOREIGN KEY (idUserImported) REFERENCES [tblUser] (idUser)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblActivityImport]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblActivityImport]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblActivityImport (
	importFileName
)
KEY INDEX PK_ActivityImport
ON Asentia -- insert index to this catalog