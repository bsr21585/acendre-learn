IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-SCOInt]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-SCOInt] (
	[idData-SCOInt]					INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idData-SCO]					INT								NOT NULL, -- we need this so that we can clear and reset upon subsequent attempts
	[identifier]					NVARCHAR(255)					NULL,
	[timestamp]						DATETIME						NULL, 
	[result]						INT								NOT NULL, 
	[latency]						FLOAT							NULL,
	[scoIdentifier]					NVARCHAR(255)					NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-SCOInt]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOInt] ADD CONSTRAINT [PK_Data-SCOInt] PRIMARY KEY CLUSTERED ([idData-SCOInt] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOInt_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOInt] ADD CONSTRAINT [FK_Data-SCOInt_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOInt_Data-SCO]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOInt] ADD CONSTRAINT [FK_Data-SCOInt_Data-SCO] FOREIGN KEY ([idData-SCO]) REFERENCES [tblData-SCO] ([idData-SCO])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-SCOInt_Result]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-SCOInt] ADD CONSTRAINT [FK_Data-SCOInt_Result] FOREIGN KEY ([result]) REFERENCES [tblSCORMVocabulary] ([idSCORMVocabulary])