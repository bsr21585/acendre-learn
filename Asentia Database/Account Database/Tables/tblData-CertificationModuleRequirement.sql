IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-CertificationModuleRequirement] (
	[idData-CertificationModuleRequirement]		INT				IDENTITY(1,1)	NOT NULL,
	[idSite]									INT								NOT NULL, 
	[idCertificationToUserLink]					INT								NOT NULL, 
	[idCertificationModuleRequirement]			INT								NULL,
	[label]										NVARCHAR(255)					NOT NULL,
	[dtCompleted]								DATETIME						NULL,
	[completionDocumentationFilePath]			NVARCHAR(255)					NULL,
	[dtSubmitted]								DATETIME						NULL,
	[isApproved]								BIT								NULL,
	[dtApproved]								DATETIME						NULL,
	[idApprover]								INT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD CONSTRAINT [PK_Data-CertificationModuleRequirement] PRIMARY KEY CLUSTERED ([idData-CertificationModuleRequirement] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-CertificationModuleRequirement_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD CONSTRAINT [FK_Data-CertificationModuleRequirement_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-CertificationModuleRequirement_CertificationToUserLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD CONSTRAINT [FK_Data-CertificationModuleRequirement_CertificationToUserLink] FOREIGN KEY ([idCertificationToUserLink]) REFERENCES [tblCertificationToUserLink] ([idCertificationToUserLink])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-CertificationModuleRequirement_CertificationModuleRequirement]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD CONSTRAINT [FK_Data-CertificationModuleRequirement_CertificationModuleRequirement] FOREIGN KEY ([idCertificationModuleRequirement]) REFERENCES [tblCertificationModuleRequirement] ([idCertificationModuleRequirement])

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblData-CertificationModuleRequirement]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblData-CertificationModuleRequirement]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON [tblData-CertificationModuleRequirement] (
	label
)
KEY INDEX [PK_Data-CertificationModuleRequirement]
ON Asentia -- insert index to this catalog