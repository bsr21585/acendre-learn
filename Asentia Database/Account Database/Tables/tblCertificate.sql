IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificate](
	[idCertificate]							INT				IDENTITY(1,1)	NOT NULL,
	[idSite]								INT								NOT NULL,
	[name]									NVARCHAR(255)					NOT NULL,
	[issuingOrganization]					NVARCHAR(255)					NULL,
	[description]							NVARCHAR(MAX)					NULL,
	[code]									NVARCHAR(25)					NULL,
	[credits]								FLOAT							NULL,
	[filename]								NVARCHAR(255)					NULL,
	[isActive]								BIT								NULL,
	[activationDate]						DATETIME						NULL,
	[expiresInterval]						INT								NULL,
	[expiresTimeframe]						NVARCHAR(4)						NULL,
	[objectType]							INT			                    NULL,
	[idObject]								INT                             NULL,
	[isDeleted]								BIT								NULL,
	[dtDeleted]								DATETIME						NULL,
	[reissueBasedOnMostRecentCompletion]	BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Certificate]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificate ADD CONSTRAINT [PK_Certificate] PRIMARY KEY CLUSTERED (idCertificate ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Certificate_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificate ADD CONSTRAINT [FK_Certificate_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificate]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificate]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificate (
	name,
	[description]
)
KEY INDEX PK_Certificate
ON Asentia -- insert index to this catalog
GO