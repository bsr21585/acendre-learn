IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPath] (
	[idLearningPath]					INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[name]								NVARCHAR(255)						NOT NULL,
	[shortDescription]					NVARCHAR(512)						NULL, 
	[longDescription]					NVARCHAR(MAX)						NULL, 
	[cost]								FLOAT								NULL, 
	[isPublished]						BIT									NULL,
	[isClosed]							BIT									NULL,
	[selfEnrollmentIsOneTimeOnly]		BIT									NULL,
	[forceCompletionInOrder]			BIT									NULL,
	[dtCreated]							DATETIME							NOT NULL,
	[dtModified]						DATETIME							NULL,
	[isDeleted]							BIT									NULL,
	[dtDeleted]							DATETIME							NULL,
	[avatar]							NVARCHAR(255)						NULL,
	[searchTags]						NVARCHAR(512)						NULL,
	[shortcode]							NVARCHAR(10)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPath ADD CONSTRAINT [PK_LearningPath] PRIMARY KEY CLUSTERED (idLearningPath ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPath_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPath ADD CONSTRAINT [FK_LearningPath_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLearningPath (
	name,
	shortDescription,
	searchTags
)
KEY INDEX PK_LearningPath
ON Asentia -- insert index to this catalog