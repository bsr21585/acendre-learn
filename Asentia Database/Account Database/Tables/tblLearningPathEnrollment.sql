IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathEnrollment] ( 
	[idLearningPathEnrollment]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[idLearningPath]					INT								NOT NULL,
	[idUser]							INT								NOT NULL,
	[idRuleSetLearningPathEnrollment]	INT								NULL,		-- indicates the enrollment is inherited from a ruleset enrollment
	[idTimezone]						INT								NOT NULL,
	[title]								NVARCHAR(255)					NOT NULL,
	[dtStart]							DATETIME						NOT NULL,	-- accounts for inherited delay (after dtCreated).
	[dtDue]								DATETIME						NULL,
	[dtExpiresFromStart]				DATETIME						NULL,
	[dtExpiresFromFirstLaunch]			DATETIME						NULL,
	[dtFirstLaunch]						DATETIME						NULL,
	[dtCompleted]						DATETIME						NULL,
	[dtCreated]							DATETIME						NOT NULL,
	[dueInterval]						INT								NULL,
	[dueTimeframe]						NVARCHAR(4)						NULL,
	[expiresFromStartInterval]			INT								NULL,
	[expiresFromStartTimeframe]			NVARCHAR(4)						NULL,
	[expiresFromFirstLaunchInterval]	INT								NULL,
	[expiresFromFirstLaunchTimeframe]	NVARCHAR(4)						NULL,
	[dtLastSynchronized]				DATETIME						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [PK_LearningPathEnrollment] PRIMARY KEY CLUSTERED ([idLearningPathEnrollment] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_RuleSetLearningPathEnrollment]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_RuleSetLearningPathEnrollment] FOREIGN KEY ([idRuleSetLearningPathEnrollment]) REFERENCES [tblRuleSetLearningPathEnrollment] ([idRuleSetLearningPathEnrollment])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathEnrollment_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblLearningPathEnrollment] ADD CONSTRAINT [FK_LearningPathEnrollment_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathEnrollment]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathEnrollment]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLearningPathEnrollment (
	title
)
KEY INDEX [PK_LearningPathEnrollment]
ON Asentia -- insert index to this catalog