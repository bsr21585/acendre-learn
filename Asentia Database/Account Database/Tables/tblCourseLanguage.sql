IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseLanguage] (
	[idCourseLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL,
	[idCourse]						INT								NOT NULL, 
	[idLanguage]					INT								NOT NULL,
	[title]							NVARCHAR(255)					NOT NULL,
	[shortDescription]				NVARCHAR(512)					NULL, 
	[longDescription]				NVARCHAR(MAX)					NULL,
	[objectives]					NVARCHAR(MAX)					NULL,
	[searchTags]					NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseLanguage ADD CONSTRAINT [PK_CourseLanguage] PRIMARY KEY CLUSTERED (idCourseLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseLanguage ADD CONSTRAINT [FK_CourseLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseLanguage_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseLanguage ADD CONSTRAINT [FK_CourseLanguage_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCourseLanguage ADD CONSTRAINT [FK_CourseLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCourseLanguage (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_CourseLanguage
ON Asentia -- insert index to this catalog