IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeToLearningPathLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeToLearningPathLink] (
	[idCouponCodeToLearningPathLink]	[int]	 IDENTITY(1,1) NOT NULL,
	[idCouponCode]						[int]	 NOT NULL,
	[idLearningPath]					[int]	NOT NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCodeToLearningPathLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToLearningPathLink ADD CONSTRAINT [PK_CouponCodeToLearningPathLink] PRIMARY KEY CLUSTERED (idCouponCodeToLearningPathLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToLearningPathLink_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToLearningPathLink ADD CONSTRAINT [FK_CouponCodeToLearningPathLink_CouponCode] FOREIGN KEY (idCouponCode) REFERENCES [tblCouponCode] (idCouponCode)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToLearningPathLink_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToLearningPathLink ADD CONSTRAINT [FK_CouponCodeToLearningPathLink_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)
