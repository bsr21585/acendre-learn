IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathLanguage] (
	[idLearningPathLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]							INT								NOT NULL,
	[idLearningPath]					INT								NOT NULL, 
	[idLanguage]						INT								NOT NULL, 
	[name]								NVARCHAR(255)					NOT NULL,
	[shortDescription]					NVARCHAR(512)					NULL, 
	[longDescription]					NVARCHAR(MAX)					NULL,
	[searchTags]						NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathLanguage ADD CONSTRAINT [PK_LearningPathLanguage] PRIMARY KEY CLUSTERED (idLearningPathLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathLanguage ADD CONSTRAINT [FK_LearningPathLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathLanguage_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathLanguage ADD CONSTRAINT [FK_LearningPathLanguage_LearningPath] FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathLanguage ADD CONSTRAINT [FK_LearningPathLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLearningPathLanguage (
	name,
	shortDescription,
	searchTags
)
KEY INDEX PK_LearningPathLanguage
ON Asentia -- insert index to this catalog