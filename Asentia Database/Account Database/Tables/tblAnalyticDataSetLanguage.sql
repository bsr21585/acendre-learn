IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticDataSetLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticDataSetLanguage] (
	[idDataSetLanguage]					INT				IDENTITY(1000,1)	NOT NULL,
	[idSite]							INT									NOT NULL, 
	[idDataSet]							INT									NOT NULL,
	[idLanguage]						INT									NOT NULL,
	[name]								NVARCHAR(255)						NOT NULL,
	[description]						NVARCHAR(512)						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticDataSetLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataSetLanguage ADD CONSTRAINT [PK_AnalyticDataSetLanguage] PRIMARY KEY CLUSTERED (idDataSetLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticDataSetLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataSetLanguage ADD CONSTRAINT [FK_AnalyticDataSetLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticDataSetLanguage_AnalyticDataSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataSetLanguage ADD CONSTRAINT [FK_AnalyticDataSetLanguage_AnalyticDataSet] FOREIGN KEY (idDataSet) REFERENCES [tblAnalyticDataSet] (idDataSet)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AnalyticDataSetLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticDataSetLanguage ADD CONSTRAINT [FK_AnalyticDataSetLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblAnalyticDataSetLanguage] ON

INSERT INTO tblAnalyticDataSetLanguage (
	idDataSetLanguage,
	idSite,
	idDataSet,
	idLanguage,
	name,
	[description]
)
SELECT
	idDataSetLanguage,
	idSite,
	idDataSet,
	idLanguage,
	name,
	[description]
FROM (
	/* ENGLISH */
	SELECT 1 AS idDataSetLanguage, 1 AS idSite, 1 AS idDataSet, 57 AS idLanguage, 'Login Activity' AS name, '[Dataset.LoginActivity]' AS storedProcedureName, 'Complete information of login activities of all registered users.' AS [description]
	UNION SELECT 2 AS idDataSetLanguage, 1 AS idSite, 2 AS idDataSet, 57 AS idLanguage, 'Course Completions' AS name, '[Dataset.CourseCompletions]' AS storedProcedureName, 'Complete information of course enrolment and completion.' AS [description]
	UNION SELECT 3 AS idDataSetLanguage, 1 AS idSite, 3 AS idDataSet, 57 AS idLanguage, 'User And Group Snapshot' AS name, '[Dataset.UserGroupSnapshot]' AS storedProcedureName, 'Complete information of users aNdd groups informations.' AS [description]
	UNION SELECT 6 AS idDataSetLanguage, 1 AS idSite, 6 AS idDataSet, 57 AS idLanguage, 'Use Statistics' AS name, '[Dataset.UseStatistics]' AS storedProcedureName, 'Complete information of users' AS [description]
	UNION SELECT 7 AS idDataSetLanguage, 1 AS idSite, 7 AS idDataSet, 57 AS idLanguage, 'Certificates' AS name, '[Dataset.Certificates]' AS storedProcedureName, 'Certificate award information. Includes certificates that have been earned as well as those that have been manually awarded.' AS [description]
	
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblAnalyticDataSetLanguage DL WHERE DL.idDataSetLanguage = MAIN.idDataSetLanguage)

SET IDENTITY_INSERT [tblAnalyticDataSetLanguage] OFF