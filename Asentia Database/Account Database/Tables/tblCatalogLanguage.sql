IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCatalogLanguage] (
	[idCatalogLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[idCatalog]					INT								NOT NULL,
	[idLanguage]				INT								NOT NULL,
	[title]						NVARCHAR(255)					NOT NULL,
	[shortDescription]			NVARCHAR(512)					NULL, 
	[longDescription]			NVARCHAR(MAX)					NULL,
	[searchTags]				NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CatalogLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogLanguage ADD CONSTRAINT [PK_CatalogLanguage] PRIMARY KEY CLUSTERED (idCatalogLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogLanguage ADD CONSTRAINT [FK_CatalogLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogLanguage_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogLanguage ADD CONSTRAINT [FK_CatalogLanguage_Catalog] FOREIGN KEY (idCatalog) REFERENCES [tblCatalog] (idCatalog)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CatalogLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCatalogLanguage ADD CONSTRAINT [FK_CatalogLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)
	
/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCatalogLanguage (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_CatalogLanguage
ON Asentia -- insert index to this catalog
