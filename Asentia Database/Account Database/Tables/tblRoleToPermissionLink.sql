IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRoleToPermissionLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRoleToPermissionLink] (
	[idRoleToPermissionLink]		INT		IDENTITY (1,1)		NOT NULL,
	[idSite]						INT							NOT NULL,
	[idRole]						INT							NOT NULL,
	[idPermission]					INT							NOT NULL,
	[scope]							NVARCHAR(MAX)				NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RoleToPermissionLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleToPermissionLink ADD CONSTRAINT [PK_RoleToPermissionLink] PRIMARY KEY CLUSTERED (idRoleToPermissionLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleToPermissionLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleToPermissionLink ADD CONSTRAINT [FK_RoleToPermissionLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleToPermissionLink_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleToPermissionLink ADD CONSTRAINT [FK_RoleToPermissionLink_Role] FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RoleToPermissionLink_Permission]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRoleToPermissionLink ADD CONSTRAINT [FK_RoleToPermissionLink_Permission] FOREIGN KEY (idPermission) REFERENCES [tblPermission] (idPermission)

/**
INSERT VALUES
**/

INSERT INTO [tblRoleToPermissionLink] (
	idSite,
	idRole,
	idPermission,
	scope
)
SELECT
	idSite,
	idRole,
	idPermission,
	scope
FROM (
	SELECT			1 AS idSite, 1 AS idRole, 1 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 2 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 3 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 4 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 5 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 6 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 7 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 8 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 9 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 10 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 11 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 101 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 102 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 103 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 104 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 105 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 106 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 107 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 108 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 109 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 110 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 111 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 112 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 113 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 114 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 115 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 201 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 202 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 203 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 204 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 205 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 301 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 302 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 303 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 304 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 305 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 306 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 307 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 308 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 309 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 310 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 401 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 402 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 403 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 404 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 405 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 406 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 407 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 408 AS idPermission, NULL AS scope
	UNION SELECT	1 AS idSite, 1 AS idRole, 409 AS idPermission, NULL AS scope
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM [tblRoleToPermissionLink] RPL WHERE RPL.idPermission = MAIN.idPermission AND RPL.idRole = MAIN.idRole)