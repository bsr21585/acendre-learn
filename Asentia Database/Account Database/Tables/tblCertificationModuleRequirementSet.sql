IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSet]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificationModuleRequirementSet] (
	[idCertificationModuleRequirementSet]	INT					IDENTITY(1,1)	NOT NULL,
	[idCertificationModule]					INT									NOT NULL,
	[idSite]								INT									NOT NULL, 
	[isAny]									BIT									NOT NULL,
	[label]									NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificationModuleRequirementSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSet ADD CONSTRAINT [PK_CertificationModuleRequirementSet] PRIMARY KEY CLUSTERED (idCertificationModuleRequirementSet ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSet_CertificationModule]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSet ADD CONSTRAINT [FK_CertificationModuleRequirementSet_CertificationModule] FOREIGN KEY (idCertificationModule) REFERENCES [tblCertificationModule] (idCertificationModule)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificationModuleRequirementSet_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificationModuleRequirementSet ADD CONSTRAINT [FK_CertificationModuleRequirementSet_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSet]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCertificationModuleRequirementSet]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblCertificationModuleRequirementSet (
	label
)
KEY INDEX PK_CertificationModuleRequirementSet
ON Asentia -- insert index to this catalog