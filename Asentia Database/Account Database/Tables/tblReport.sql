IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblReport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblReport] (
	[idReport]						INT				IDENTITY(1,1)	NOT NULL,
	[idSite]						INT								NOT NULL, 
	[idUser]						INT								NOT NULL, 
	[idDataset]						INT								NOT NULL, 
	[title]							NVARCHAR(255)					NOT NULL,
	[fields]						NVARCHAR(MAX)					NOT NULL,
	[filter]						NVARCHAR(MAX)					NULL,
	[order]							NVARCHAR(1024)					NULL,
	[isPublic]						BIT								NOT NULL,
	[dtCreated]						DATETIME						NULL,
	[dtModified]					DATETIME						NULL,
	[isDeleted]						BIT								NULL,
	[dtDeleted]						DATETIME						NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Report]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReport ADD CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED (idReport ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Report_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReport ADD CONSTRAINT [FK_Report_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Report_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReport ADD CONSTRAINT [FK_Report_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Report_Dataset]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblReport ADD CONSTRAINT [FK_Report_Dataset] FOREIGN KEY (idDataset) REFERENCES [tblDataset] (idDataset)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReport]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblReport]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblReport (
	title
)
KEY INDEX PK_Report
ON Asentia -- insert index to this catalog