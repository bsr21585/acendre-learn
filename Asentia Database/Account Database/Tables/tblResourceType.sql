IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblResourceType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblResourceType] (
	[idResourceType]				INT				IDENTITY (101, 1)	NOT NULL, 
	[idSite]						INT									NOT NULL,
	[resourceType]					NVARCHAR(255)						NOT NULL,
	[isDeleted]						BIT									NOT NULL,
	[dtDeleted]						DATETIME							NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_ResourceType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceType ADD CONSTRAINT [PK_ResourceType] PRIMARY KEY CLUSTERED (idResourceType ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ResourceType_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblResourceType ADD CONSTRAINT [FK_ResourceType_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceType]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblResourceType]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblResourceType (
	[resourceType]
)
KEY INDEX PK_ResourceType
ON Asentia -- insert index to this resource