IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItemLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDocumentRepositoryItemLanguage] (
	[idDocumentRepositoryItemLanguage]				INT				IDENTITY(1,1)	NOT NULL,
	[idSite]										INT								NOT NULL,
	[idDocumentRepositoryItem]						INT								NOT NULL, 
	[idLanguage]									INT								NOT NULL,
	[label]											NVARCHAR(255)					NOT NULL,
	[searchTags]									NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_DocumentRepositoryItemLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDocumentRepositoryItemLanguage ADD CONSTRAINT [PK_DocumentRepositoryItemLanguage] PRIMARY KEY CLUSTERED (idDocumentRepositoryItemLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItemLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDocumentRepositoryItemLanguage ADD CONSTRAINT [FK_DocumentRepositoryItemLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItemLanguage_DocumentRepositoryItem]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDocumentRepositoryItemLanguage ADD CONSTRAINT [FK_DocumentRepositoryItemLanguage_DocumentRepositoryItem] FOREIGN KEY (idDocumentRepositoryItem) REFERENCES [tblDocumentRepositoryItem] (idDocumentRepositoryItem)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_DocumentRepositoryItemLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDocumentRepositoryItemLanguage ADD CONSTRAINT [FK_DocumentRepositoryItemLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
FULL TEXT INDEX
*/

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItemLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItemLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblDocumentRepositoryItemLanguage (
	label,
	searchTags
)
KEY INDEX PK_DocumentRepositoryItemLanguage
ON Asentia -- insert index to this document repository item