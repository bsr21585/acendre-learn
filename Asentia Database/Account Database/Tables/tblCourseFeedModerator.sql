IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCourseFeedModerator]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCourseFeedModerator] (
	[idCourseFeedModerator]				INT			IDENTITY (1, 1)		NOT NULL,
	[idSite]							INT								NOT NULL,
	[idCourse]						    INT								NOT NULL,
	[idModerator]						INT								NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CourseFeedModerator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedModerator] ADD CONSTRAINT [PK_CourseFeedModerator] PRIMARY KEY CLUSTERED (idCourseFeedModerator ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedModerator_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedModerator] ADD CONSTRAINT [FK_CourseFeedModerator_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedModerator_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedModerator] ADD CONSTRAINT [FK_CourseFeedModerator_Course] FOREIGN KEY (idCourse) REFERENCES [tblCourse] (idCourse)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CourseFeedModerator_Moderator]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblCourseFeedModerator] ADD CONSTRAINT [FK_CourseFeedModerator_Moderator] FOREIGN KEY (idModerator) REFERENCES [tblUser] (idUser)