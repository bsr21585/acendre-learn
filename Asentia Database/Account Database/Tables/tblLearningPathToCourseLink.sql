IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLearningPathToCourseLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLearningPathToCourseLink] (
	[idLearningPathToCourseLink]		INT		IDENTITY(1,1)	NOT NULL,
	[idSite]							INT						NOT NULL,
	[idLearningPath]					INT						NOT NULL,
	[idCourse]							INT						NOT NULL,
	[order]								INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LearningPathToCourseLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathToCourseLink ADD CONSTRAINT [PK_LearningPathToCourseLink] PRIMARY KEY CLUSTERED (idLearningPathToCourseLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathToCourseLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathToCourseLink ADD CONSTRAINT [FK_LearningPathToCourseLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathToCourseLink_LearningPath]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathToCourseLink ADD CONSTRAINT FK_LearningPathToCourseLink_LearningPath FOREIGN KEY (idLearningPath) REFERENCES [tblLearningPath] (idLearningPath)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LearningPathToCourseLink_Course]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLearningPathToCourseLink ADD CONSTRAINT FK_LearningPathToCourseLink_Course FOREIGN KEY ([idCourse]) REFERENCES [tblCourse] ([idCourse])