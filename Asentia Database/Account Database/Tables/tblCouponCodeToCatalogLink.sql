IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCouponCodeToCatalogLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCouponCodeToCatalogLink] (
	[idCouponCodeToCatalogLink]				INT		IDENTITY(1,1)		NOT NULL,
	[idCouponCode]							INT							NOT NULL,
	[idCatalog]								INT							NOT NULL	 
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CouponCodeToCatalogLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCatalogLink ADD CONSTRAINT [PK_CouponCodeToCatalogLink] PRIMARY KEY CLUSTERED (idCouponCodeToCatalogLink ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToCatalogLink_CouponCode]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCatalogLink ADD CONSTRAINT [FK_CouponCodeToCatalogLink_CouponCode] FOREIGN KEY (idCouponCode) REFERENCES [tblCouponCode] (idCouponCode)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CouponCodeToCatalogLink_Catalog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCouponCodeToCatalogLink ADD CONSTRAINT [FK_CouponCodeToCatalogLink_Catalog] FOREIGN KEY (idCatalog) REFERENCES [tblCatalog] (idCatalog)