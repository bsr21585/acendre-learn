IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAnalyticConstants]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAnalyticConstants] (
	[idConstant]						INT				IDENTITY(1000,1)	NOT NULL,
	[value]							    INT									NOT NULL, 
	[name]								NVARCHAR(255)						NOT NULL,
	[idConstantType]			  	    INT									NOT NULL
	
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AnalyticConstants]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAnalyticConstants ADD CONSTRAINT [PK_AnalyticConstants] PRIMARY KEY CLUSTERED (idConstant ASC)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblAnalyticConstants] ON

INSERT INTO tblAnalyticConstants (
	idConstant,
	value,
	name,
	idConstantType
	)
SELECT
	idConstant,
	value,
	name,
	idConstantType
	
FROM (
	      SELECT 1 AS idConstant, 1 AS value, 'Sunday'    AS name,  2 AS idConstantType
	UNION SELECT 2 AS idConstant, 2 AS value, 'Monday'    AS name,  2 AS idConstantType
	UNION SELECT 3 AS idConstant, 3 AS value, 'Tuesday'   AS name,  2 AS idConstantType
	UNION SELECT 4 AS idConstant, 4 AS value, 'Wednesday' AS name,  2 AS idConstantType
	UNION SELECT 5 AS idConstant, 5 AS value, 'Thursday'  AS name,  2 AS idConstantType
	UNION SELECT 6 AS idConstant, 6 AS value, 'Friday'    AS name,  2 AS idConstantType
	UNION SELECT 7 AS idConstant, 7 AS value, 'Saturday'  AS name,  2 AS idConstantType

	UNION SELECT 8 AS idConstant,  1 AS value, 'January'    AS name,  4 AS idConstantType
	UNION SELECT 9 AS idConstant,  2 AS value, 'February'    AS name,  4 AS idConstantType
	UNION SELECT 10 AS idConstant,  3 AS value, 'March'      AS name,  4 AS idConstantType
	UNION SELECT 11 AS idConstant,  4 AS value, 'April'      AS name,  4 AS idConstantType
	UNION SELECT 12 AS idConstant,  5 AS value, 'May'         AS name,  4 AS idConstantType
    UNION SELECT 13 AS idConstant,  6 AS value, 'June'       AS name,  4 AS idConstantType
	UNION SELECT 14 AS idConstant,  7 AS value, 'July'       AS name,  4 AS idConstantType
	UNION SELECT 15 AS idConstant,  8 AS value, 'August'     AS name,  4 AS idConstantType
	UNION SELECT 16 AS idConstant,  9 AS value, 'September'  AS name,  4 AS idConstantType
	UNION SELECT 17 AS idConstant, 10 AS value, 'October'   AS name,  4 AS idConstantType
	UNION SELECT 18 AS idConstant, 11 AS value, 'November'   AS name,  4 AS idConstantType
	UNION SELECT 19 AS idConstant, 12 AS value, 'December'  AS name,  4 AS idConstantType

	UNION SELECT 20 AS idConstant, 1  AS value, 'Week 1'    AS name,  3 AS idConstantType
	UNION SELECT 21 AS idConstant, 2  AS value, 'Week 2'    AS name,  3 AS idConstantType
	UNION SELECT 22 AS idConstant,  3 AS value, 'Week 3'      AS name,  3 AS idConstantType
	UNION SELECT 23 AS idConstant,  4 AS value, 'Week 4'      AS name,  3 AS idConstantType
	

	UNION SELECT 24 AS idConstant,  0 AS value, '00:00' AS name,  1 AS idConstantType
    UNION SELECT 25 AS idConstant,  1 AS value, '01:00' AS name,  1 AS idConstantType
	UNION SELECT 26 AS idConstant,  2 AS value, '02:00' AS name,  1 AS idConstantType
	UNION SELECT 27 AS idConstant,  3 AS value, '03:00' AS name,  1 AS idConstantType
	UNION SELECT 28 AS idConstant,  4 AS value, '04:00' AS name,  1 AS idConstantType
	UNION SELECT 29 AS idConstant, 5 AS value, '05:00'  AS name,  1 AS idConstantType
	UNION SELECT 30 AS idConstant, 6 AS value, '06:00'  AS name,  1 AS idConstantType
	UNION SELECT 31 AS idConstant, 7 AS value, '07:00'  AS name,  1 AS idConstantType
	UNION SELECT 32 AS idConstant,  8 AS value, '08:00' AS name,  1 AS idConstantType
	UNION SELECT 33 AS idConstant,  9 AS value, '09:00' AS name,  1 AS idConstantType
	UNION SELECT 34 AS idConstant,  10 AS value, '10:00' AS name,  1 AS idConstantType
	UNION SELECT 35 AS idConstant,  11 AS value, '11:00' AS name,  1 AS idConstantType
	UNION SELECT 36 AS idConstant, 12 AS value, '12:00'  AS name,  1 AS idConstantType
	UNION SELECT 37 AS idConstant, 13 AS value, '13:00'   AS name,  1 AS idConstantType
	UNION SELECT 38 AS idConstant, 14 AS value, '14:00'  AS name,  1 AS idConstantType
	UNION SELECT 39 AS idConstant,  15 AS value, '15:00'       AS name,  1 AS idConstantType
	UNION SELECT 40 AS idConstant,  16 AS value, '16:00'       AS name,  1 AS idConstantType
	UNION SELECT 41 AS idConstant,  17 AS value, '17:00'     AS name,  1 AS idConstantType
	UNION SELECT 42 AS idConstant,  18 AS value, '18:00'  AS name,  1 AS idConstantType
	UNION SELECT 43 AS idConstant, 19 AS value, '19:00'   AS name,  1 AS idConstantType
	UNION SELECT 44 AS idConstant, 20 AS value, '20:00'   AS name,  1 AS idConstantType
	UNION SELECT 45 AS idConstant, 21 AS value, '21:00'  AS name,  1 AS idConstantType
	UNION SELECT 46 AS idConstant, 22 AS value, '22:00'   AS name,  1 AS idConstantType
	UNION SELECT 47 AS idConstant, 23 AS value, '23:00'  AS name,  1 AS idConstantType

	UNION SELECT 48 AS idConstant,  5 AS value, 'Week 4'      AS name,  3 AS idConstantType

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblAnalyticConstants AC WHERE AC.idConstant = MAIN.idConstant)

SET IDENTITY_INSERT [tblAnalyticConstants] OFF