IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblxAPIoAuthNonce]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [dbo].[tblxAPIoAuthNonce] (
	[idxAPIoAuthNonce]				INT				IDENTITY(1,1)	NOT NULL,
	[idxAPIoAuthConsumer]			INT								NOT NULL,
	[nonce]							NVARCHAR(200)					NOT NULL
 ) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_xAPIoAuthNonce]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblxAPIoAuthNonce] ADD CONSTRAINT [PK_xAPIoAuthNonce] PRIMARY KEY CLUSTERED ([idxAPIoAuthNonce] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_xAPIoAuthNonce_xAPIoAuthConsumer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblxAPIoAuthNonce] ADD CONSTRAINT [FK_xAPIoAuthNonce_xAPIoAuthConsumer] FOREIGN KEY (idxAPIoAuthConsumer) REFERENCES [tblxAPIoAuthConsumer] (idxAPIoAuthConsumer)