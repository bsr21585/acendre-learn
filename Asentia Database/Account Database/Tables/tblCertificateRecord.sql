IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblCertificateRecord]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblCertificateRecord] (
	[idCertificateRecord]		INT					IDENTITY(1,1)	NOT NULL,
    [idSite]					INT									NOT NULL,
	[idCertificate]				INT									NOT NULL,	
	[idUser]					INT									NOT NULL,
	[idTimezone]				INT									NOT NULL,
	[idAwardedBy]				INT									NULL,
	[timestamp]					DATETIME							NOT NULL,
	[expires]					DATETIME							NULL,
	[code]						NVARCHAR(25)						NULL,
	[credits]					FLOAT								NULL,
	[awardData]					NVARCHAR(MAX)						NULL,
	[idCertificateImport]		INT									NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_CertificateRecord]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [PK_CertificateRecord] PRIMARY KEY CLUSTERED (idCertificateRecord ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_Certificate]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_Certificate] FOREIGN KEY (idCertificate) REFERENCES [tblCertificate] (idCertificate)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_TimeZone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_TimeZone] FOREIGN KEY (idTimezone) REFERENCES [tblTimeZone] (idTimezone)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_CertificateRecord_CertificateImport]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblCertificateRecord ADD CONSTRAINT [FK_CertificateRecord_CertificateImport] FOREIGN KEY (idCertificateImport) REFERENCES [tblCertificateImport] (idCertificateImport)
	