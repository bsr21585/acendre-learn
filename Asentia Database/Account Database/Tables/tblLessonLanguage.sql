IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLessonLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLessonLanguage] (
	[idLessonLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idSite]					INT								NOT NULL,
	[idLesson]					INT								NOT NULL,
	[idLanguage]				INT								NOT NULL,
	[title]						NVARCHAR(255)					NOT NULL,
	[shortDescription]			NVARCHAR(512)					NULL,
	[longDescription]			NVARCHAR(MAX)					NULL,
	[searchTags]				NVARCHAR(512)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_LessonLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonLanguage ADD CONSTRAINT [PK_LessonLanguage] PRIMARY KEY CLUSTERED ([idLessonLanguage] ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonLanguage_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonLanguage ADD CONSTRAINT [FK_LessonLanguage_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonLanguage_Lesson]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonLanguage ADD CONSTRAINT [FK_LessonLanguage_Lesson] FOREIGN KEY ([idLesson]) REFERENCES [tblLesson] ([idLesson])

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_LessonLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLessonLanguage ADD CONSTRAINT [FK_LessonLanguage_Language] FOREIGN KEY ([idLanguage]) REFERENCES [tblLanguage] ([idLanguage])

/**
FULL TEXT INDEX
*/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLessonLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLessonLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblLessonLanguage (
	title,
	shortDescription,
	searchTags
)
KEY INDEX PK_LessonLanguage
ON Asentia -- insert index to this catalog