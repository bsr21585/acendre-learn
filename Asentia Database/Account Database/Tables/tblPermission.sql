IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblPermission]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblPermission] (
	[idPermission]				INT				IDENTITY (1, 1)		NOT NULL,
	[name]						NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Permission]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblPermission ADD CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED (idPermission ASC)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblPermission] ON

INSERT INTO [tblPermission] (
	idPermission, 
	name
)
SELECT
	idPermission,
	name
FROM (
	SELECT			1 AS idPermission, 'System_AccountSettings' AS name
	UNION SELECT	2 AS idPermission, 'System_Configuration' AS name
	UNION SELECT	3 AS idPermission, 'System_Ecommerce' AS name
	UNION SELECT	4 AS idPermission, 'System_CouponCodes' AS name
	UNION SELECT	5 AS idPermission, 'System_TrackingCodes' AS name
	UNION SELECT	6 AS idPermission, 'System_EmailNotifications' AS name
	UNION SELECT	7 AS idPermission, 'System_API' AS name
	UNION SELECT	8 AS idPermission, 'System_xAPIEndpoints' AS name
	UNION SELECT	9 AS idPermission, 'System_Logs' AS name
	UNION SELECT	10 AS idPermission, 'System_WebMeetingIntegration' AS name
	UNION SELECT	11 AS idPermission, 'System_RulesEngine' AS name

	UNION SELECT	101 AS idPermission, 'UsersAndGroups_UserFieldConfiguration' AS name
	UNION SELECT	102 AS idPermission, 'UsersAndGroups_UserCreator' AS name
	UNION SELECT	103 AS idPermission, 'UsersAndGroups_UserDeleter' AS name
	UNION SELECT	104 AS idPermission, 'UsersAndGroups_UserEditor' AS name
	UNION SELECT	105 AS idPermission, 'UsersAndGroups_UserManager' AS name
	UNION SELECT	106 AS idPermission, 'UsersAndGroups_UserImpersonator' AS name
	UNION SELECT	107 AS idPermission, 'UsersAndGroups_GroupManager' AS name
	UNION SELECT	108 AS idPermission, 'UsersAndGroups_RoleManager' AS name
	UNION SELECT	109 AS idPermission, 'UsersAndGroups_ActivityImport' AS name
	UNION SELECT	110 AS idPermission, 'UsersAndGroups_CertificateImport' AS name
	UNION SELECT	111 AS idPermission, 'UsersAndGroups_Leaderboards' AS name
	UNION SELECT	112 AS idPermission, 'UsersAndGroups_GroupCreator' AS name
	UNION SELECT	113 AS idPermission, 'UsersAndGroups_GroupDeleter' AS name
	UNION SELECT	114 AS idPermission, 'UsersAndGroups_GroupEditor' AS name
	UNION SELECT	115 AS idPermission, 'UsersAndGroups_UserRegistrationApproval' AS name

	UNION SELECT	201 AS idPermission, 'InterfaceAndLayout_HomePage' AS name
	UNION SELECT	202 AS idPermission, 'InterfaceAndLayout_Masthead' AS name
	UNION SELECT	203 AS idPermission, 'InterfaceAndLayout_Footer' AS name
	UNION SELECT	204 AS idPermission, 'InterfaceAndLayout_CSSEditor' AS name
	UNION SELECT	205 AS idPermission, 'InterfaceAndLayout_ImageEditor' AS name

	UNION SELECT	301 AS idPermission, 'LearningAssets_CourseCatalog' AS name
	UNION SELECT	302 AS idPermission, 'LearningAssets_CourseContentManager' AS name
	UNION SELECT	303 AS idPermission, 'LearningAssets_LearningPathContentManager' AS name
	UNION SELECT	304 AS idPermission, 'LearningAssets_ContentPackageManager' AS name
	UNION SELECT	305 AS idPermission, 'LearningAssets_CertificateTemplateManager' AS name
	UNION SELECT	306 AS idPermission, 'LearningAssets_InstructorLedTrainingManager' AS name
	UNION SELECT	307 AS idPermission, 'LearningAssets_ResourceManager' AS name
	UNION SELECT	308 AS idPermission, 'LearningAssets_CertificationsManager' AS name
	UNION SELECT	309 AS idPermission, 'LearningAssets_QuizAndSurveyManager' AS name
	UNION SELECT	310 AS idPermission, 'LearningAssets_SelfEnrollmentApproval' AS name
	UNION SELECT	311 AS idPermission, 'LearningAssets_CourseEnrollmentManager' AS name
	UNION SELECT	312 AS idPermission, 'LearningAssets_LearningPathEnrollmentManager' AS name

	UNION SELECT	401 AS idPermission, 'Reporting_Reporter_UserDemographics' AS name
	UNION SELECT	402 AS idPermission, 'Reporting_Reporter_UserCourseTranscripts' AS name
	UNION SELECT	403 AS idPermission, 'Reporting_Reporter_CatalogAndCourseInformation' AS name
	UNION SELECT	404 AS idPermission, 'Reporting_Reporter_Certificates' AS name
	UNION SELECT	405 AS idPermission, 'Reporting_Reporter_XAPI' AS name
	UNION SELECT	406 AS idPermission, 'Reporting_Reporter_UserLearningPathTranscripts' AS name
	UNION SELECT	407 AS idPermission, 'Reporting_Reporter_UserInstructorLedTrainingTranscripts' AS name
	UNION SELECT	408 AS idPermission, 'Reporting_Reporter_Purchases' AS name
	UNION SELECT	409 AS idPermission, 'Reporting_Reporter_UserCertificationTranscripts' AS name
) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblPermission P WHERE P.idPermission = MAIN.idPermission)

SET IDENTITY_INSERT [tblPermission] OFF