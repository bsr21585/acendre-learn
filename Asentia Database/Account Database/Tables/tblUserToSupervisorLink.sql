IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblUserToSupervisorLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblUserToSupervisorLink] (
		[idUserToSupervisorLink]			INT		IDENTITY(1,1)	NOT NULL,
		[idSite]							INT						NOT NULL,
		[idUser]							INT						NOT NULL,
		[idSupervisor]						INT						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_UserToSupervisorLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToSupervisorLink ADD CONSTRAINT [PK_UserToSupervisorLink] PRIMARY KEY CLUSTERED (idUserToSupervisorLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToSupervisorLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToSupervisorLink ADD CONSTRAINT [FK_UserToSupervisorLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToSupervisorLink_User]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToSupervisorLink ADD CONSTRAINT [FK_UserToSupervisorLink_User] FOREIGN KEY (idUser) REFERENCES [tblUser] (idUser)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_UserToSupervisorLink_Supervisor]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblUserToSupervisorLink ADD CONSTRAINT [FK_UserToSupervisorLink_Supervisor] FOREIGN KEY (idSupervisor) REFERENCES [tblUser] (idUser)