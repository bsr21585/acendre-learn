IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRuleSetToRoleLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE tblRuleSetToRoleLink (
	[idRuleSetToRoleLink]			INT			IDENTITY(1,1)	NOT NULL,
	[idSite]						INT							NOT NULL,
	[idRuleSet]						INT							NOT NULL,
	[idRole]						INT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_RuleSetToRoleLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRoleLink ADD CONSTRAINT PK_RuleSetToRoleLink PRIMARY KEY CLUSTERED (idRuleSetToRoleLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRoleLink_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRoleLink ADD CONSTRAINT [FK_RuleSetToRoleLink_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRoleLink_RuleSet]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRoleLink ADD CONSTRAINT FK_RuleSetToRoleLink_RuleSet FOREIGN KEY (idRuleSet) REFERENCES [tblRuleSet] (idRuleSet)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_RuleSetToRoleLink_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRuleSetToRoleLink ADD CONSTRAINT FK_RuleSetToRoleLink_Role FOREIGN KEY (idRole) REFERENCES [tblRole] (idRole)