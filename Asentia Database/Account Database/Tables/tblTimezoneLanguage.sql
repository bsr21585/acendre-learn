IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTimezoneLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTimezoneLanguage] (
	[idTimezoneLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idTimezone]					INT								NULL,
	[idLanguage]					INT								NULL,
	[displayName]					NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_TimezoneLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezoneLanguage ADD CONSTRAINT [PK_TimezoneLanguage] PRIMARY KEY CLUSTERED (idTimezoneLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TimezoneLanguage_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezoneLanguage ADD CONSTRAINT [FK_TimezoneLanguage_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TimezoneLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezoneLanguage ADD CONSTRAINT [FK_TimezoneLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
INSERT VALUES
**/

--ENGLISH
INSERT INTO tblTimezoneLanguage (
	idTimezone,
	idLanguage,
	displayName
)
SELECT
	TZ.idTimezone,
	57,
	TZ.displayName
FROM tblTimezone TZ
WHERE NOT EXISTS (
	SELECT 1
	FROM tblTimezoneLanguage TZL
	WHERE TZL.idTimezone = TZ.idTimezone
	AND TZL.idLanguage = 57
)