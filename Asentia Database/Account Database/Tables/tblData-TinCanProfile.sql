IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblData-TinCanProfile]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblData-TinCanProfile] (
	[idData-TinCanProfile]				INT					IDENTITY(1,1)	NOT NULL,
	[idSite]							INT									NOT NULL,
	[idEndpoint]						INT									NULL,
	[profileId]							NVARCHAR(100)						NOT NULL,
	[activityId]						NVARCHAR(100)						NULL,
	[agent]								NVARCHAR(MAX)						NULL,
	[contentType]						NVARCHAR(50)						NOT NULL,
	[docContents]						VARBINARY(MAX)						NOT NULL,
	[dtUpdated]							DATETIME							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Data-TinCanProfile]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanProfile] ADD CONSTRAINT [PK_Data-TinCanProfile] PRIMARY KEY CLUSTERED ([idData-TinCanProfile] ASC)

/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanProfile_Site]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanProfile] ADD CONSTRAINT [FK_Data-TinCanProfile_Site] FOREIGN KEY (idSite) REFERENCES [tblSite] (idSite)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Data-TinCanProfile_IdEndpoint]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblData-TinCanProfile] ADD CONSTRAINT [FK_Data-TinCanProfile_IdEndpoint] FOREIGN KEY (idEndpoint) REFERENCES [tblxAPIoAuthConsumer] (idxAPIoAuthConsumer)