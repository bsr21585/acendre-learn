IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblPurchase]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblPurchase] (
	[idPurchase]				INT				IDENTITY(1,1)	NOT NULL,
	[idUser]					INT								NOT NULL,
	[orderNumber]				NVARCHAR(12)					NOT NULL,
	[timeStamp]					DATETIME						NULL,
	[ccnum]						NVARCHAR(4)						NULL,
	[currency]					NVARCHAR(4)						NULL,
	[idSite]					INT								NULL,
	[dtPending]					DATETIME						NULL,
	[failed]					BIT								NULL,
	[transactionId]				NVARCHAR(255)					NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Purchase]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblPurchase ADD CONSTRAINT [PK_Purchase] PRIMARY KEY CLUSTERED (idPurchase ASC)
	
/**
FOREIGN KEYS
*/	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_tblPurchase_tblUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)	
	ALTER TABLE tblPurchase ADD CONSTRAINT [FK_tblPurchase_tblUser] FOREIGN KEY(idUser) REFERENCES [tblUser] (idUser)