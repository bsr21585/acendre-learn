SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ValidateCallerPermission]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [ValidateCallerPermission]
GO

/*

Determines if the caller of a procedure is a member of the specified site.

*/

CREATE FUNCTION [ValidateCallerPermission]
(
	@idCaller		INT,
	@idPermission	INT,
	@idObject		INT
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @callerHasPermission BIT
	SET @callerHasPermission = 0

	/*

	if the permission is null or 0, return false cause there is nothing to validate
	note that @idObject will be null in some cases, so we do not need to validate it

	*/

	IF @idPermission IS NULL OR @idPermission = 0
		BEGIN
		SET @callerHasPermission = 0
		RETURN @callerHasPermission
		END

	/*
	
	if the caller is 1 (admin) don't bother validating permissions, just return true
	else, validate that caller has permission
	
	*/

	IF @idCaller = 1
		BEGIN
		SET @callerHasPermission = 1
		RETURN @callerHasPermission
		END

	ELSE
		BEGIN

		-- for now, we always return true - there is work to be done here
		SET @callerHasPermission = 1
		RETURN @callerHasPermission

		END

	RETURN @callerHasPermission
	
END
GO