SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[IDateAdd]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [IDateAdd]
GO

/*
  Returns a datetime after adding interval and timeframe to it.
*/
CREATE FUNCTION [IDateAdd]
(
	@timeframe	NVARCHAR(10),
	@interval	INT,
	@datetime	DATETIME
)
RETURNS DATETIME
AS
BEGIN
	DECLARE @return DATETIME

	IF @timeframe IS NULL OR @interval IS NULL
		BEGIN
		RETURN @datetime
		END

	-- DAY
	IF @timeframe = 'd'
		BEGIN
		SET @return = DATEADD(d, @interval, @datetime)
		RETURN @return
		END

	-- WEEK
	IF @timeframe = 'ww'
		BEGIN
		SET @return = DATEADD(ww, @interval, @datetime)
		RETURN @return
		END

	-- MONTH
	IF @timeframe = 'm'
		BEGIN
		SET @return = DATEADD(m, @interval, @datetime)
		RETURN @return
		END

	-- YEAR
	IF @timeframe = 'yyyy'
		BEGIN
		SET @return = DATEADD(yyyy, @interval, @datetime)
		RETURN @return
		END

	-- IF ALL ELSE FAILS, JUST RETURN THE SAME DATETIME
	SET @return = @datetime
	RETURN @return
END
GO