SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EvaluateEnrollmentPrerequisiteLock]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [EvaluateEnrollmentPrerequisiteLock]
GO

CREATE FUNCTION [EvaluateEnrollmentPrerequisiteLock]
(
	@idEnrollment	INT
)
RETURNS BIT
AS
BEGIN

	/*

	declare variables

	*/

    DECLARE @idCourse INT
	DECLARE @idUser INT
	DECLARE @dtCompleted DATETIME
	DECLARE @isLockedByPrerequisites BIT
	DECLARE @isPrerequisiteAny BIT

	/*

	get enrollment information

	*/
	
	SELECT
		@idCourse = idCourse,
		@idUser = idUser,
		@dtCompleted = dtCompleted,
		@isLockedByPrerequisites = isLockedByPrerequisites
	FROM tblEnrollment
	WHERE idEnrollment = @idEnrollment

	/*

	if the enrollment is already unlocked or completed, return 0

	*/

	IF (@isLockedByPrerequisites = 0 OR @dtCompleted IS NOT NULL)
		BEGIN
		SET @isLockedByPrerequisites = 0
		RETURN @isLockedByPrerequisites
		END

	/*

	if there are no prerequisites, return 0

	*/

	IF (SELECT COUNT(1) FROM tblCourseToPrerequisiteLink WHERE idCourse = @idCourse) = 0
		BEGIN
		SET @isLockedByPrerequisites = 0
		RETURN @isLockedByPrerequisites
		END

	/*

	get the setting for "isPrerequisiteAny" for the course

	*/

	SELECT
		@isPrerequisiteAny = isPrerequisiteAny
	FROM tblCourse
	WHERE idCourse = @idCourse

	/*

	get the prerequisites

	*/

	DECLARE @CoursePrerequisites TABLE (idPrerequisite INT)

	INSERT INTO @CoursePrerequisites (
		idPrerequisite
	)
	SELECT DISTINCT
		idPrerequisite
	FROM tblCourseToPrerequisiteLink
	WHERE idCourse = @idCourse

	/*

	get the prerequisites completed by the user

	*/

	DECLARE @CompletedPrerequisiteEnrollments TABLE (idCourse INT)

	INSERT INTO @CompletedPrerequisiteEnrollments (
		idCourse
	)
	SELECT DISTINCT
		idCourse
	FROM tblEnrollment
	WHERE idUser = @idUser
	AND dtCompleted IS NOT NULL
	AND idCourse IN (SELECT idPrerequisite FROM @CoursePrerequisites)

	/*

	evaluate prerequisite lock

	*/

	IF (@isPrerequisiteAny = 1) -- "any", the user must have completed at least 1 prerequisite

		BEGIN

		IF (SELECT COUNT(1) FROM @CompletedPrerequisiteEnrollments) > 0
			BEGIN
			SET @isLockedByPrerequisites = 0
			END
		ELSE
			BEGIN
			SET @isLockedByPrerequisites = 1
			END

		END

	ELSE -- "all", the user must have completed all prerequisites

		BEGIN

		IF (SELECT COUNT(1) FROM @CompletedPrerequisiteEnrollments) = (SELECT COUNT(1) FROM @CoursePrerequisites)
			BEGIN
			SET @isLockedByPrerequisites = 0
			END
		ELSE
			BEGIN
			SET @isLockedByPrerequisites = 1
			END

		END

	RETURN @isLockedByPrerequisites

END
GO