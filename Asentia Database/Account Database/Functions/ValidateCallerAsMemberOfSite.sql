SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ValidateCallerAsMemberOfSite]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [ValidateCallerAsMemberOfSite]
GO

/*

Determines if the caller of a procedure is a member of the specified site.

*/

CREATE FUNCTION [ValidateCallerAsMemberOfSite]
(
	@idCaller	INT,
	@idSite		INT
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @callerIsMemberOfSite BIT
	SET @callerIsMemberOfSite = 0

	/*

	if the caller and site is null or 0, return false cause there is nothing to validate

	*/

	IF (@idCaller IS NULL OR @idCaller = 0) AND (@idSite IS NULL OR @idSite = 0)
		BEGIN
		SET @callerIsMemberOfSite = 0
		RETURN @callerIsMemberOfSite
		END

	/*
	
	if the caller is 1 (admin) don't bother validating site membership, just return true
	else, validate caller's site membership
	
	*/

	IF @idCaller = 1
		BEGIN
		SET @callerIsMemberOfSite = 1
		RETURN @callerIsMemberOfSite
		END

	ELSE
		BEGIN

		IF (SELECT idSite FROM tblUser WHERE idUser = @idCaller) = @idSite
			BEGIN
			SET @callerIsMemberOfSite = 1
			RETURN @callerIsMemberOfSite
			END

		END

	RETURN @callerIsMemberOfSite
	
END
GO