SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EvaluateLessonFirstAndLastLaunchLocks]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [EvaluateLessonFirstAndLastLaunchLocks]
GO

CREATE FUNCTION [EvaluateLessonFirstAndLastLaunchLocks]
(
	@idDataLesson									INT,
	@requireFirstLessonToBeCompletedBeforeOthers	BIT,
	@lockLastLessonUntilOthersCompleted				BIT
)
RETURNS BIT
AS
BEGIN

	/*

	declare variables

	*/

    DECLARE @idEnrollment INT
	DECLARE @lessonDataOrdinal INT
	DECLARE @lessonDataMaxOrdinal INT
	DECLARE @allowLessonLaunch BIT

	-- default "allow lesson launch" to true
	SET @allowLessonLaunch = 1
	
	/*

	get enrollment id, lesson data ordinal, and max lesson data ordinal

	*/

	SELECT @idEnrollment = idEnrollment, @lessonDataOrdinal = [order] FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson
	SELECT @lessonDataMaxOrdinal = MAX([order]) FROM [tblData-Lesson] WHERE idEnrollment = @idEnrollment

	/*

	if the ordinal is 1, then return 1

	*/

	IF (@lessonDataOrdinal = 1)
		BEGIN
		RETURN @allowLessonLaunch
		END

	/*

	if the ordinal is greater than 1 and @requireFirstLessonToBeCompletedBeforeOthers = 1, ensure that the first lesson was completed

	*/

	IF (@lessonDataOrdinal > 1 AND @requireFirstLessonToBeCompletedBeforeOthers = 1)
		BEGIN
		
		SELECT 
			@allowLessonLaunch = CASE WHEN dtCompleted IS NOT NULL THEN 1 ELSE 0 END
		FROM [tblData-Lesson]
		WHERE idEnrollment = @idEnrollment
		AND [order] = 1 

		END

	/*

	if the ordinal is max (last lesson) and @lockLastLessonUntilOthersCompleted = 1, ensure all other lessons are completed

	*/

	IF (@lessonDataOrdinal = @lessonDataMaxOrdinal AND @lockLastLessonUntilOthersCompleted = 1)
		BEGIN
		
		IF (SELECT COUNT(1) FROM [tblData-Lesson] WHERE idEnrollment = @idEnrollment AND [order] < @lessonDataMaxOrdinal AND dtCompleted IS NULL) > 0
			BEGIN
			SET @allowLessonLaunch = 0
			END
		ELSE
			BEGIN
			SET @allowLessonLaunch = 1
			END

		END
	
	/* return */
	RETURN @allowLessonLaunch

END
GO