SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[isDateDST]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[isDateDST]
GO

/*
 User Defined Function isDateDST
*/
CREATE FUNCTION dbo.isDateDST
(
	@timestamp			DATETIME
)
RETURNS BIT
AS
BEGIN
DECLARE @year INT
DECLARE @r BIT

/*

non date returns FALSE

*/

IF ISDATE(@timestamp) = 0

	SET @R = 0

/*

get the year that we are calculating for

*/

SET @year = year(@timestamp)

/*

find it

*/

IF @timestamp >= dbo.dstStart(@year) AND @timestamp < dbo.dstEnd(@year)

	SET @r = 1

ELSE
	
	SET @r = 0

RETURN @r

END
GO