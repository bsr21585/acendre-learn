SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dstEnd]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[dstEnd]
GO

/*
 User Defined Function dstEnd
*/
CREATE FUNCTION dbo.dstEnd(
	@year			INT
)RETURNS DATETIME
AS

BEGIN

DECLARE @week int
DECLARE @d datetime

IF @year <= 2006

	BEGIN

	-- last day in october

	SET @d = dateAdd(year, @year - 1800, '1/1/1800')
	SET @d = dateAdd(month, 9, @d)
	SET @d = dateAdd(day, 30, @d)

	-- backward to last sunday

	WHILE datepart(WEEKDAY, @d) <> 1

		SET @d = dateAdd(day, -1, @d)
	
	END

ELSE

	BEGIN

	-- first day in november
	SET @d = dateAdd(year, @year - 1800, '1/1/1800')
	SET @d = dateAdd(month, 10, @d)
	
	-- forward to first sunday
	
	WHILE datepart(WEEKDAY, @d) <> 1

		SET @d = dateAdd(day, 1, @d)

	END

-- forward to 2 AM

SET @d = dateAdd(hour, 2, @d)

RETURN @d

END
GO