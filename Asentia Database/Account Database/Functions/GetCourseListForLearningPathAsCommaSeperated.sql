SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[GetCourseListForLearningPathAsCommaSeperated]') and xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION [dbo].[GetCourseListForLearningPathAsCommaSeperated]
GO

CREATE FUNCTION [dbo].[GetCourseListForLearningPathAsCommaSeperated]
(
    @idLearningPath INT,
	@idCallerSite INT,
	@idCallerLanguage INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	
	DECLARE @courseTitles TABLE (title NVARCHAR(255))
	DECLARE @courseTitlesString NVARCHAR(MAX)

	-- get titles of all courses that belong to the learning path
	INSERT INTO @courseTitles (
		title
	)
	SELECT
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END
	FROM tblCourse C
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	WHERE C.idSite = @idCallerSite	
	AND EXISTS (SELECT 1 FROM tblLearningPathToCourseLink LPCL
				WHERE LPCL.idLearningPath = @idLearningPath
				AND LPCL.idCourse = C.idCourse)
	
	-- stuff the course titles into a string
	SELECT @courseTitlesString = STUFF( (SELECT ', ' + title FROM @courseTitles ORDER BY title FOR XML PATH('')), 1, 1, '')	

	-- return the string
	RETURN LTRIM(RTRIM(@courseTitlesString))

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO