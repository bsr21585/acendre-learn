SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EvaluateLessonOrderLaunchLock]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [EvaluateLessonOrderLaunchLock]
GO

CREATE FUNCTION [EvaluateLessonOrderLaunchLock]
(
	@idDataLesson	INT
)
RETURNS BIT
AS
BEGIN

	/*

	declare variables

	*/

    DECLARE @idEnrollment INT
	DECLARE @lessonDataOrdinal INT
	DECLARE @allowLessonLaunch BIT

	-- default "allow lesson launch" to false
	SET @allowLessonLaunch = 0
	
	/*

	get enrollment id and lesson data ordinal

	*/

	SELECT @idEnrollment = idEnrollment, @lessonDataOrdinal = [order] FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson

	/*

	if the ordinal is 1, then return 1

	*/

	IF (@lessonDataOrdinal = 1)
		BEGIN
		SET @allowLessonLaunch = 1
		RETURN @allowLessonLaunch
		END

	/*

	evaluate against the lesson data at the previous ordinal, if complete return 1, if not return 0

	*/

	DECLARE @isLessonDataIdAtPreviousOrdinalCompleted BIT

	SELECT
		@isLessonDataIdAtPreviousOrdinalCompleted = CASE WHEN dtCompleted IS NOT NULL THEN 1 ELSE 0 END
	FROM [tblData-Lesson]
	WHERE idEnrollment = @idEnrollment
	AND [order] = @lessonDataOrdinal - 1

	IF (@isLessonDataIdAtPreviousOrdinalCompleted = 1)
		BEGIN
		SET @allowLessonLaunch = 1
		RETURN @allowLessonLaunch
		END

	ELSE
		BEGIN
		SET @allowLessonLaunch = 0
		RETURN @allowLessonLaunch
		END

	RETURN @allowLessonLaunch

END
GO