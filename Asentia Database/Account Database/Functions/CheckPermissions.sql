SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CheckPermissions]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [CheckPermissions]
GO

CREATE FUNCTION [CheckPermissions]
(
	@idPermissionList	NVARCHAR(MAX),
	@idScopeList		NVARCHAR(MAX),
	@idSite				INT,
	@idUser				INT
)
RETURNS BIT
AS
BEGIN

    RETURN 1

	--IF @idUser = 1
	--	BEGIN
	--	RETURN 1
	--	END

END
GO