SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EvaluateLearningPathEnrollmentOrderingLock]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [EvaluateLearningPathEnrollmentOrderingLock]
GO

CREATE FUNCTION [EvaluateLearningPathEnrollmentOrderingLock]
(
	@idEnrollment	INT
)
RETURNS BIT
AS
BEGIN

	/*

	declare variables

	*/

	DECLARE @idCourse INT
	DECLARE @idUser INT
	DECLARE @idLearningPathEnrollment INT
    DECLARE @idLearningPath INT
	DECLARE @isLearningPathForcedOrder BIT
	DECLARE @isLockedByLearningPathOrdering BIT

	/*

	get the enrollment information

	*/

	SELECT
		@idCourse = idCourse,
		@idUser = idUser,
		@idLearningPathEnrollment = idLearningPathEnrollment
	FROM tblEnrollment
	WHERE idEnrollment = @idEnrollment

	/*

	if this enrollment is not directly inherited by a learning path enrollment, return 0

	*/

	IF (@idLearningPathEnrollment IS NULL)
		BEGIN
		SET @isLockedByLearningPathOrdering = 0
		RETURN @isLockedByLearningPathOrdering
		END

	/*

	get learning path information from learning path enrollment

	*/
	
	SELECT
		@idLearningPath = idLearningPath
	FROM tblLearningPathEnrollment
	WHERE idLearningPathEnrollment = @idLearningPathEnrollment

	/*

	get learning path forced order setting

	*/

	SELECT
		@isLearningPathForcedOrder = forceCompletionInOrder
	FROM tblLearningPath
	WHERE idLearningPath = @idLearningPath


	/*

	if the learning path is not forced order, return 0

	*/

	IF (@isLearningPathForcedOrder IS NULL OR @isLearningPathForcedOrder = 0)
		BEGIN
		SET @isLockedByLearningPathOrdering = 0
		RETURN @isLockedByLearningPathOrdering
		END

	/*

	get the ordering

	*/

	DECLARE @LearningPathCourseOrder TABLE (idCourse INT, [order] INT)

	INSERT INTO @LearningPathCourseOrder (
		idCourse,
		[order]
	)
	SELECT DISTINCT
		idCourse,
		[order]
	FROM tblLearningPathToCourseLink
	WHERE idLearningPath = @idLearningPath

	/*

	get the learning path order of the course enrollment we are evaluating for

	*/

	DECLARE @enrollmentCoursesOrder INT

	SELECT
		@enrollmentCoursesOrder = [order]
	FROM @LearningPathCourseOrder
	WHERE idCourse = @idCourse

	/*

	if the order we grabbed is number 1, return 0

	*/

	IF (@enrollmentCoursesOrder = 1)
		BEGIN
		SET @isLockedByLearningPathOrdering = 0
		RETURN @isLockedByLearningPathOrdering
		END

	/*

	evaluate against the course at the previous ordinal, if complete return 0, if not return 1

	*/

	DECLARE @courseIdAtPreviousOrdinal INT

	SELECT
		@courseIdAtPreviousOrdinal = idCourse
	FROM @LearningPathCourseOrder
	WHERE [order] = @enrollmentCoursesOrder - 1

	IF (
		SELECT COUNT(1) 
		FROM tblEnrollment 
		WHERE idUser = @idUser 
		AND idCourse = @courseIdAtPreviousOrdinal 
		AND dtCompleted IS NOT NULL
	   ) > 0

		BEGIN
		SET @isLockedByLearningPathOrdering = 0
		RETURN @isLockedByLearningPathOrdering
		END

	ELSE

		BEGIN
		SET @isLockedByLearningPathOrdering = 1
		RETURN @isLockedByLearningPathOrdering
		END

	RETURN @isLockedByLearningPathOrdering

END
GO