﻿SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CheckPermissions]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [CheckPermissions]
GO

CREATE FUNCTION [CheckPermissions]
(
	@idPermissionList	NVARCHAR(MAX),
	@idScopeList		NVARCHAR(MAX),
	@idSite				INT,
	@idUser				INT
)
RETURNS BIT
AS
BEGIN

    RETURN 1

	--IF @idUser = 1
	--	BEGIN
	--	RETURN 1
	--	END

END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dstEnd]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[dstEnd]
GO

/*
 User Defined Function dstEnd
*/
CREATE FUNCTION dbo.dstEnd(
	@year			INT
)RETURNS DATETIME
AS

BEGIN

DECLARE @week int
DECLARE @d datetime

IF @year <= 2006

	BEGIN

	-- last day in october

	SET @d = dateAdd(year, @year - 1800, '1/1/1800')
	SET @d = dateAdd(month, 9, @d)
	SET @d = dateAdd(day, 30, @d)

	-- backward to last sunday

	WHILE datepart(WEEKDAY, @d) <> 1

		SET @d = dateAdd(day, -1, @d)
	
	END

ELSE

	BEGIN

	-- first day in november
	SET @d = dateAdd(year, @year - 1800, '1/1/1800')
	SET @d = dateAdd(month, 10, @d)
	
	-- forward to first sunday
	
	WHILE datepart(WEEKDAY, @d) <> 1

		SET @d = dateAdd(day, 1, @d)

	END

-- forward to 2 AM

SET @d = dateAdd(hour, 2, @d)

RETURN @d

END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dstStart]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[dstStart]
GO

/*
 User Defined Function dstStart
*/
CREATE FUNCTION dbo.dstStart(
	@year			INT
)RETURNS DATETIME
AS

BEGIN

DECLARE @month		INT
DECLARE @week		INT
DECLARE @d			DATETIME

IF @year <= 2006

	BEGIN

	SET @month = 4
	SET @week = 1

	END

ELSE

	BEGIN

	SET @month = 3
	SET @week = 2

	END

SET @d = DATEADD(YEAR, @year - 1800, '1/1/1800')

SET @d = DATEADD(MONTH, @month - 1, @d)

-- move forward to get the first sunday

WHILE DATEPART(weekday, @d) <> 1

	 SET @d = DATEADD(DAY, 1, @d)

-- move to the correct week

	 SET @d = DATEADD(WEEK, @week - 1, @d)

-- forward to 2 AM
	 SET @d = DATEADD(HOUR, 2, @d)

RETURN @d

END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EvaluateEnrollmentPrerequisiteLock]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [EvaluateEnrollmentPrerequisiteLock]
GO

CREATE FUNCTION [EvaluateEnrollmentPrerequisiteLock]
(
	@idEnrollment	INT
)
RETURNS BIT
AS
BEGIN

	/*

	declare variables

	*/

    DECLARE @idCourse INT
	DECLARE @idUser INT
	DECLARE @dtCompleted DATETIME
	DECLARE @isLockedByPrerequisites BIT
	DECLARE @isPrerequisiteAny BIT

	/*

	get enrollment information

	*/
	
	SELECT
		@idCourse = idCourse,
		@idUser = idUser,
		@dtCompleted = dtCompleted,
		@isLockedByPrerequisites = isLockedByPrerequisites
	FROM tblEnrollment
	WHERE idEnrollment = @idEnrollment

	/*

	if the enrollment is already unlocked or completed, return 0

	*/

	IF (@isLockedByPrerequisites = 0 OR @dtCompleted IS NOT NULL)
		BEGIN
		SET @isLockedByPrerequisites = 0
		RETURN @isLockedByPrerequisites
		END

	/*

	if there are no prerequisites, return 0

	*/

	IF (SELECT COUNT(1) FROM tblCourseToPrerequisiteLink WHERE idCourse = @idCourse) = 0
		BEGIN
		SET @isLockedByPrerequisites = 0
		RETURN @isLockedByPrerequisites
		END

	/*

	get the setting for "isPrerequisiteAny" for the course

	*/

	SELECT
		@isPrerequisiteAny = isPrerequisiteAny
	FROM tblCourse
	WHERE idCourse = @idCourse

	/*

	get the prerequisites

	*/

	DECLARE @CoursePrerequisites TABLE (idPrerequisite INT)

	INSERT INTO @CoursePrerequisites (
		idPrerequisite
	)
	SELECT DISTINCT
		idPrerequisite
	FROM tblCourseToPrerequisiteLink
	WHERE idCourse = @idCourse

	/*

	get the prerequisites completed by the user

	*/

	DECLARE @CompletedPrerequisiteEnrollments TABLE (idCourse INT)

	INSERT INTO @CompletedPrerequisiteEnrollments (
		idCourse
	)
	SELECT DISTINCT
		idCourse
	FROM tblEnrollment
	WHERE idUser = @idUser
	AND dtCompleted IS NOT NULL
	AND idCourse IN (SELECT idPrerequisite FROM @CoursePrerequisites)

	/*

	evaluate prerequisite lock

	*/

	IF (@isPrerequisiteAny = 1) -- "any", the user must have completed at least 1 prerequisite

		BEGIN

		IF (SELECT COUNT(1) FROM @CompletedPrerequisiteEnrollments) > 0
			BEGIN
			SET @isLockedByPrerequisites = 0
			END
		ELSE
			BEGIN
			SET @isLockedByPrerequisites = 1
			END

		END

	ELSE -- "all", the user must have completed all prerequisites

		BEGIN

		IF (SELECT COUNT(1) FROM @CompletedPrerequisiteEnrollments) = (SELECT COUNT(1) FROM @CoursePrerequisites)
			BEGIN
			SET @isLockedByPrerequisites = 0
			END
		ELSE
			BEGIN
			SET @isLockedByPrerequisites = 1
			END

		END

	RETURN @isLockedByPrerequisites

END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EvaluateLearningPathEnrollmentOrderingLock]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [EvaluateLearningPathEnrollmentOrderingLock]
GO

CREATE FUNCTION [EvaluateLearningPathEnrollmentOrderingLock]
(
	@idEnrollment	INT
)
RETURNS BIT
AS
BEGIN

	/*

	declare variables

	*/

	DECLARE @idCourse INT
	DECLARE @idUser INT
	DECLARE @idLearningPathEnrollment INT
    DECLARE @idLearningPath INT
	DECLARE @isLearningPathForcedOrder BIT
	DECLARE @isLockedByLearningPathOrdering BIT

	/*

	get the enrollment information

	*/

	SELECT
		@idCourse = idCourse,
		@idUser = idUser,
		@idLearningPathEnrollment = idLearningPathEnrollment
	FROM tblEnrollment
	WHERE idEnrollment = @idEnrollment

	/*

	if this enrollment is not directly inherited by a learning path enrollment, return 0

	*/

	IF (@idLearningPathEnrollment IS NULL)
		BEGIN
		SET @isLockedByLearningPathOrdering = 0
		RETURN @isLockedByLearningPathOrdering
		END

	/*

	get learning path information from learning path enrollment

	*/
	
	SELECT
		@idLearningPath = idLearningPath
	FROM tblLearningPathEnrollment
	WHERE idLearningPathEnrollment = @idLearningPathEnrollment

	/*

	get learning path forced order setting

	*/

	SELECT
		@isLearningPathForcedOrder = forceCompletionInOrder
	FROM tblLearningPath
	WHERE idLearningPath = @idLearningPath


	/*

	if the learning path is not forced order, return 0

	*/

	IF (@isLearningPathForcedOrder IS NULL OR @isLearningPathForcedOrder = 0)
		BEGIN
		SET @isLockedByLearningPathOrdering = 0
		RETURN @isLockedByLearningPathOrdering
		END

	/*

	get the ordering

	*/

	DECLARE @LearningPathCourseOrder TABLE (idCourse INT, [order] INT)

	INSERT INTO @LearningPathCourseOrder (
		idCourse,
		[order]
	)
	SELECT DISTINCT
		idCourse,
		[order]
	FROM tblLearningPathToCourseLink
	WHERE idLearningPath = @idLearningPath

	/*

	get the learning path order of the course enrollment we are evaluating for

	*/

	DECLARE @enrollmentCoursesOrder INT

	SELECT
		@enrollmentCoursesOrder = [order]
	FROM @LearningPathCourseOrder
	WHERE idCourse = @idCourse

	/*

	if the order we grabbed is number 1, return 0

	*/

	IF (@enrollmentCoursesOrder = 1)
		BEGIN
		SET @isLockedByLearningPathOrdering = 0
		RETURN @isLockedByLearningPathOrdering
		END

	/*

	evaluate against the course at the previous ordinal, if complete return 0, if not return 1

	*/

	DECLARE @courseIdAtPreviousOrdinal INT

	SELECT
		@courseIdAtPreviousOrdinal = idCourse
	FROM @LearningPathCourseOrder
	WHERE [order] = @enrollmentCoursesOrder - 1

	IF (
		SELECT COUNT(1) 
		FROM tblEnrollment 
		WHERE idUser = @idUser 
		AND idCourse = @courseIdAtPreviousOrdinal 
		AND dtCompleted IS NOT NULL
	   ) > 0

		BEGIN
		SET @isLockedByLearningPathOrdering = 0
		RETURN @isLockedByLearningPathOrdering
		END

	ELSE

		BEGIN
		SET @isLockedByLearningPathOrdering = 1
		RETURN @isLockedByLearningPathOrdering
		END

	RETURN @isLockedByLearningPathOrdering

END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EvaluateLessonOrderLaunchLock]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [EvaluateLessonOrderLaunchLock]
GO

CREATE FUNCTION [EvaluateLessonOrderLaunchLock]
(
	@idDataLesson	INT
)
RETURNS BIT
AS
BEGIN

	/*

	declare variables

	*/

    DECLARE @idEnrollment INT
	DECLARE @lessonDataOrdinal INT
	DECLARE @allowLessonLaunch BIT

	-- default "allow lesson launch" to false
	SET @allowLessonLaunch = 0
	
	/*

	get enrollment id and lesson data ordinal

	*/

	SELECT @idEnrollment = idEnrollment, @lessonDataOrdinal = [order] FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson

	/*

	if the ordinal is 1, then return 1

	*/

	IF (@lessonDataOrdinal = 1)
		BEGIN
		SET @allowLessonLaunch = 1
		RETURN @allowLessonLaunch
		END

	/*

	evaluate against the lesson data at the previous ordinal, if complete return 1, if not return 0

	*/

	DECLARE @isLessonDataIdAtPreviousOrdinalCompleted BIT

	SELECT
		@isLessonDataIdAtPreviousOrdinalCompleted = CASE WHEN dtCompleted IS NOT NULL THEN 1 ELSE 0 END
	FROM [tblData-Lesson]
	WHERE idEnrollment = @idEnrollment
	AND [order] = @lessonDataOrdinal - 1

	IF (@isLessonDataIdAtPreviousOrdinalCompleted = 1)
		BEGIN
		SET @allowLessonLaunch = 1
		RETURN @allowLessonLaunch
		END

	ELSE
		BEGIN
		SET @allowLessonLaunch = 0
		RETURN @allowLessonLaunch
		END

	RETURN @allowLessonLaunch

END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[GetCourseListForCatalogAsCommaSeperated]') and xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION [dbo].[GetCourseListForCatalogAsCommaSeperated]
GO

CREATE FUNCTION [dbo].[GetCourseListForCatalogAsCommaSeperated]
(
    @idCatalog INT,
	@idCallerSite INT,
	@idCallerLanguage INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	
	DECLARE @courseTitles TABLE (title NVARCHAR(255))
	DECLARE @courseTitlesString NVARCHAR(MAX)

	-- get titles of all courses that are published and belong to the catalog as direct decendants
	INSERT INTO @courseTitles (
		title
	)
	SELECT
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END
	FROM tblCourse C
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	WHERE C.idSite = @idCallerSite
	AND (C.isDeleted IS NULL OR C.isDeleted = 0)
	AND C.isPublished = 1
	AND EXISTS (SELECT 1 FROM tblCourseToCatalogLink CCL
				WHERE CCL.idCatalog = @idCatalog
				AND CCL.idCourse = C.idCourse)
	
	-- stuff the course titles into a string
	SELECT @courseTitlesString = STUFF( (SELECT ', ' + title FROM @courseTitles ORDER BY title FOR XML PATH('')), 1, 1, '')	

	-- return the string
	RETURN LTRIM(RTRIM(@courseTitlesString))

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[GetCourseListForLearningPathAsCommaSeperated]') and xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION [dbo].[GetCourseListForLearningPathAsCommaSeperated]
GO

CREATE FUNCTION [dbo].[GetCourseListForLearningPathAsCommaSeperated]
(
    @idLearningPath INT,
	@idCallerSite INT,
	@idCallerLanguage INT
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	
	DECLARE @courseTitles TABLE (title NVARCHAR(255))
	DECLARE @courseTitlesString NVARCHAR(MAX)

	-- get titles of all courses that belong to the learning path
	INSERT INTO @courseTitles (
		title
	)
	SELECT
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END
	FROM tblCourse C
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	WHERE C.idSite = @idCallerSite	
	AND EXISTS (SELECT 1 FROM tblLearningPathToCourseLink LPCL
				WHERE LPCL.idLearningPath = @idLearningPath
				AND LPCL.idCourse = C.idCourse)
	
	-- stuff the course titles into a string
	SELECT @courseTitlesString = STUFF( (SELECT ', ' + title FROM @courseTitles ORDER BY title FOR XML PATH('')), 1, 1, '')	

	-- return the string
	RETURN LTRIM(RTRIM(@courseTitlesString))

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GetHashedString]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [GetHashedString]
GO

/*
  Hashes a string using a specified hashing algorithm
  and returns the hashed string value.
*/
CREATE FUNCTION [GetHashedString]
(
	@hashingAlgorithm		NVARCHAR(8),
	@stringToHash			NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	-- validate hash algorithm variable, valid values are
	-- MD2, MD4, MD5, SHA, SHA1, SHA2_256, and SHA2_512
	IF (@hashingAlgorithm <> 'MD2' AND
		@hashingAlgorithm <> 'MD4' AND
		@hashingAlgorithm <> 'MD5' AND
		@hashingAlgorithm <> 'SHA' AND
		@hashingAlgorithm <> 'SHA1' AND
		@hashingAlgorithm <> 'SHA2_256' AND
		@hashingAlgorithm <> 'SHA2_512')
		BEGIN
			-- default hashing algorithm to SHA1
			SET @hashingAlgorithm = 'SHA1'
		END
		
	-- hash the string
	DECLARE @passwordHash VARBINARY(MAX)
	SELECT @passwordHash = HASHBYTES(@hashingAlgorithm, @stringToHash)
 
	--return the hashed string as an nvarchar
    --RETURN CONVERT(NVARCHAR(MAX), @passwordHash, 2) -- SQL SERVER 2008+ ONLY
    RETURN CAST('' AS XML).value('xs:hexBinary(sql:variable("@passwordHash"))', 'NVARCHAR(MAX)') -- COMPATIBLE WITH SQL SERVER 2005
END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[IDateAdd]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [IDateAdd]
GO

/*
  Returns a datetime after adding interval and timeframe to it.
*/
CREATE FUNCTION [IDateAdd]
(
	@timeframe	NVARCHAR(10),
	@interval	INT,
	@datetime	DATETIME
)
RETURNS DATETIME
AS
BEGIN
	DECLARE @return DATETIME

	IF @timeframe IS NULL OR @interval IS NULL
		BEGIN
		RETURN @datetime
		END

	-- DAY
	IF @timeframe = 'd'
		BEGIN
		SET @return = DATEADD(d, @interval, @datetime)
		RETURN @return
		END

	-- WEEK
	IF @timeframe = 'ww'
		BEGIN
		SET @return = DATEADD(ww, @interval, @datetime)
		RETURN @return
		END

	-- MONTH
	IF @timeframe = 'm'
		BEGIN
		SET @return = DATEADD(m, @interval, @datetime)
		RETURN @return
		END

	-- YEAR
	IF @timeframe = 'yyyy'
		BEGIN
		SET @return = DATEADD(yyyy, @interval, @datetime)
		RETURN @return
		END

	-- IF ALL ELSE FAILS, JUST RETURN THE SAME DATETIME
	SET @return = @datetime
	RETURN @return
END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[isDateDST]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[isDateDST]
GO

/*
 User Defined Function isDateDST
*/
CREATE FUNCTION dbo.isDateDST
(
	@timestamp			DATETIME
)
RETURNS BIT
AS
BEGIN
DECLARE @year INT
DECLARE @r BIT

/*

non date returns FALSE

*/

IF ISDATE(@timestamp) = 0

	SET @R = 0

/*

get the year that we are calculating for

*/

SET @year = year(@timestamp)

/*

find it

*/

IF @timestamp >= dbo.dstStart(@year) AND @timestamp < dbo.dstEnd(@year)

	SET @r = 1

ELSE
	
	SET @r = 0

RETURN @r

END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ValidateCallerAsMemberOfSite]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [ValidateCallerAsMemberOfSite]
GO

/*

Determines if the caller of a procedure is a member of the specified site.

*/

CREATE FUNCTION [ValidateCallerAsMemberOfSite]
(
	@idCaller	INT,
	@idSite		INT
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @callerIsMemberOfSite BIT
	SET @callerIsMemberOfSite = 0

	/*

	if the caller and site is null or 0, return false cause there is nothing to validate

	*/

	IF (@idCaller IS NULL OR @idCaller = 0) AND (@idSite IS NULL OR @idSite = 0)
		BEGIN
		SET @callerIsMemberOfSite = 0
		RETURN @callerIsMemberOfSite
		END

	/*
	
	if the caller is 1 (admin) don't bother validating site membership, just return true
	else, validate caller's site membership
	
	*/

	IF @idCaller = 1
		BEGIN
		SET @callerIsMemberOfSite = 1
		RETURN @callerIsMemberOfSite
		END

	ELSE
		BEGIN

		IF (SELECT idSite FROM tblUser WHERE idUser = @idCaller) = @idSite
			BEGIN
			SET @callerIsMemberOfSite = 1
			RETURN @callerIsMemberOfSite
			END

		END

	RETURN @callerIsMemberOfSite
	
END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ValidateCallerPermission]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [ValidateCallerPermission]
GO

/*

Determines if the caller of a procedure is a member of the specified site.

*/

CREATE FUNCTION [ValidateCallerPermission]
(
	@idCaller		INT,
	@idPermission	INT,
	@idObject		INT
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @callerHasPermission BIT
	SET @callerHasPermission = 0

	/*

	if the permission is null or 0, return false cause there is nothing to validate
	note that @idObject will be null in some cases, so we do not need to validate it

	*/

	IF @idPermission IS NULL OR @idPermission = 0
		BEGIN
		SET @callerHasPermission = 0
		RETURN @callerHasPermission
		END

	/*
	
	if the caller is 1 (admin) don't bother validating permissions, just return true
	else, validate that caller has permission
	
	*/

	IF @idCaller = 1
		BEGIN
		SET @callerHasPermission = 1
		RETURN @callerHasPermission
		END

	ELSE
		BEGIN

		-- for now, we always return true - there is work to be done here
		SET @callerHasPermission = 1
		RETURN @callerHasPermission

		END

	RETURN @callerHasPermission
	
END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DelimitedStringToTable]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [DelimitedStringToTable]
GO

/*

Takes in a delimited string and converts it to a table.

*/

CREATE FUNCTION [DelimitedStringToTable]
(
	@string			NVARCHAR(MAX),
	@delimiter		NVARCHAR(10)
)
RETURNS @table TABLE (s NVARCHAR(MAX))
AS
BEGIN

	DECLARE @i INT, @j INT

	SELECT @i = 1

	WHILE @i <= DATALENGTH(@string)
		BEGIN

		SELECT @j = CHARINDEX(@delimiter, @string, @i)
		
		IF @j = 0
			BEGIN
			SELECT @j = DATALENGTH(@string) + 1
			END
		
		INSERT @table SELECT LTRIM(RTRIM(SUBSTRING(@string, @i, @j - @i)))
		
		SELECT @i = @j + LEN(@delimiter)
		
		END

	RETURN

END
GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EvaluateLessonFirstAndLastLaunchLocks]') AND xtype IN (N'FN', N'IF', N'TF'))
	DROP FUNCTION [EvaluateLessonFirstAndLastLaunchLocks]
GO

CREATE FUNCTION [EvaluateLessonFirstAndLastLaunchLocks]
(
	@idDataLesson									INT,
	@requireFirstLessonToBeCompletedBeforeOthers	BIT,
	@lockLastLessonUntilOthersCompleted				BIT
)
RETURNS BIT
AS
BEGIN

	/*

	declare variables

	*/

    DECLARE @idEnrollment INT
	DECLARE @lessonDataOrdinal INT
	DECLARE @lessonDataMaxOrdinal INT
	DECLARE @allowLessonLaunch BIT

	-- default "allow lesson launch" to true
	SET @allowLessonLaunch = 1
	
	/*

	get enrollment id, lesson data ordinal, and max lesson data ordinal

	*/

	SELECT @idEnrollment = idEnrollment, @lessonDataOrdinal = [order] FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson
	SELECT @lessonDataMaxOrdinal = MAX([order]) FROM [tblData-Lesson] WHERE idEnrollment = @idEnrollment

	/*

	if the ordinal is 1, then return 1

	*/

	IF (@lessonDataOrdinal = 1)
		BEGIN
		RETURN @allowLessonLaunch
		END

	/*

	if the ordinal is greater than 1 and @requireFirstLessonToBeCompletedBeforeOthers = 1, ensure that the first lesson was completed

	*/

	IF (@lessonDataOrdinal > 1 AND @requireFirstLessonToBeCompletedBeforeOthers = 1)
		BEGIN
		
		SELECT 
			@allowLessonLaunch = CASE WHEN dtCompleted IS NOT NULL THEN 1 ELSE 0 END
		FROM [tblData-Lesson]
		WHERE idEnrollment = @idEnrollment
		AND [order] = 1 

		END

	/*

	if the ordinal is max (last lesson) and @lockLastLessonUntilOthersCompleted = 1, ensure all other lessons are completed

	*/

	IF (@lessonDataOrdinal = @lessonDataMaxOrdinal AND @lockLastLessonUntilOthersCompleted = 1)
		BEGIN
		
		IF (SELECT COUNT(1) FROM [tblData-Lesson] WHERE idEnrollment = @idEnrollment AND [order] < @lessonDataMaxOrdinal AND dtCompleted IS NULL) > 0
			BEGIN
			SET @allowLessonLaunch = 0
			END
		ELSE
			BEGIN
			SET @allowLessonLaunch = 1
			END

		END
	
	/* return */
	RETURN @allowLessonLaunch

END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF EXISTS (SELECT * from dbo.sysobjects where id = object_id(N'[dbo].[virtualTable]') and xtype in (N'FN', N'IF', N'TF'))
DROP FUNCTION [dbo].[virtualTable]
GO

CREATE FUNCTION virtualTable(
	@str NVARCHAR(MAX), 
	@delim NVARCHAR(10)
) RETURNS @tbl TABLE (s NVARCHAR(MAX))
AS 

BEGIN

	DECLARE @i INT 
	DECLARE @j INT

	SELECT 	@i = 1
	
	WHILE @i <= DATALENGTH(@str)
	BEGIN

		SELECT @j = CHARINDEX(@delim, @str, @i)

		IF @j = 0
		BEGIN
			SELECT	@j = DATALENGTH(@str) + 1
		END
		
		INSERT	@tbl SELECT LTRIM(RTRIM(SUBSTRING(@str, @i, @j - @i)))

		SELECT	@i = @j + LEN(@delim)
	END
	RETURN

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
