Dim sContent
Dim missingFiles : missingFiles = ""
Dim counter : counter = 0
Dim sFileName: sFileName = "_execution order.txt" 
Set objFSO = CreateObject("Scripting.FileSystemObject")
objStartFolder = objFSO.GetAbsolutePathName(".")

Set objFolder = objFSO.GetFolder(objStartFolder)
Set colFiles = objFolder.Files

Set TxtFile = objFSO.OpenTextFile(sFileName,1)
sContent = TxtFile.ReadAll

For Each objFile in colFiles
    If ((UCase(objFSO.GetExtensionName(objFile.name)) = "SQL") And (objFile.name <> "_FUNCTIONS.sql")) Then
        If InStr(sContent,objFile.name) = 0 Then 
			missingFiles = missingFiles & vbCrLf & objFile.name 
		End If	
    End If
Next
If(missingFiles <> "") Then
	Set a = objFSO.CreateTextFile("_Check_Function_Script_Result.txt", True)
    a.WriteLine("Following Function(s) are missing in Execution Order file" & missingFiles)
    a.Close		
	Wscript.Echo  "A text file _Check_Function_Script_Result.txt, with all missing Function's name has been created"
Else
		if objFSO.FileExists("_Check_Function_Script_Result.txt") then
            objFSO.DeleteFile "_Check_Function_Script_Result.txt"
        end if
	Wscript.Echo  "All Function(s) are present in execution order file"
End If