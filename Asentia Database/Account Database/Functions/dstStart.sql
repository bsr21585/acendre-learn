SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dstStart]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[dstStart]
GO

/*
 User Defined Function dstStart
*/
CREATE FUNCTION dbo.dstStart(
	@year			INT
)RETURNS DATETIME
AS

BEGIN

DECLARE @month		INT
DECLARE @week		INT
DECLARE @d			DATETIME

IF @year <= 2006

	BEGIN

	SET @month = 4
	SET @week = 1

	END

ELSE

	BEGIN

	SET @month = 3
	SET @week = 2

	END

SET @d = DATEADD(YEAR, @year - 1800, '1/1/1800')

SET @d = DATEADD(MONTH, @month - 1, @d)

-- move forward to get the first sunday

WHILE DATEPART(weekday, @d) <> 1

	 SET @d = DATEADD(DAY, 1, @d)

-- move to the correct week

	 SET @d = DATEADD(WEEK, @week - 1, @d)

-- forward to 2 AM
	 SET @d = DATEADD(HOUR, 2, @d)

RETURN @d

END
GO