SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF EXISTS (SELECT * from dbo.sysobjects where id = object_id(N'[dbo].[virtualTable]') and xtype in (N'FN', N'IF', N'TF'))
DROP FUNCTION [dbo].[virtualTable]
GO

CREATE FUNCTION virtualTable(
	@str NVARCHAR(MAX), 
	@delim NVARCHAR(10)
) RETURNS @tbl TABLE (s NVARCHAR(MAX))
AS 

BEGIN

	DECLARE @i INT 
	DECLARE @j INT

	SELECT 	@i = 1
	
	WHILE @i <= DATALENGTH(@str)
	BEGIN

		SELECT @j = CHARINDEX(@delim, @str, @i)

		IF @j = 0
		BEGIN
			SELECT	@j = DATALENGTH(@str) + 1
		END
		
		INSERT	@tbl SELECT LTRIM(RTRIM(SUBSTRING(@str, @i, @j - @i)))

		SELECT	@i = @j + LEN(@delim)
	END
	RETURN

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO