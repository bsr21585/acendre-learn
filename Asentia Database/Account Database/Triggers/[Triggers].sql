-- [tblUser] - TRIGGER ON INSERT TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.RulesExecQueueOnInsert]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [User.RulesExecQueueOnInsert]
GO

CREATE TRIGGER [User.RulesExecQueueOnInsert] ON [tblUser] AFTER INSERT
AS	
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		U.idSite,
		'user',
		U.idUser,
		'INSERT',
		'tblUser',
		GETUTCDATE()		
	FROM inserted U
	WHERE (U.isDeleted IS NULL OR U.isDeleted = 0)
	AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'user'
					AND triggerObjectId = U.idUser
					AND dtProcessed IS NULL)

GO

-- [tblUser] - TRIGGER ON UPDATE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.RulesExecQueueOnUpdate]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [User.RulesExecQueueOnUpdate]
GO

CREATE TRIGGER [User.RulesExecQueueOnUpdate] ON [tblUser] AFTER UPDATE
AS	
	
	-- only execute this trigger if it isn't blocked
	IF (CONTEXT_INFO() <> CONVERT(VARBINARY(128), 'BLOCK TRIGGER') OR CONTEXT_INFO() IS NULL)
		BEGIN
		 
		/*

		TRIGGER A RULES ENGINE EXECUTION

		*/

		INSERT INTO [tblRuleSetEngineQueue] (
			idSite,
			triggerObject,
			triggerObjectId,
			eventAction,
			eventTable,
			dtQueued	
		)
		SELECT
			U.idSite,
			'user',
			U.idUser,
			'UPDATE',
			'tblUser',
			GETUTCDATE()		
		FROM deleted U
		WHERE (U.isDeleted IS NULL OR U.isDeleted = 0)
		AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)
		AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
						WHERE triggerObject = 'user'
						AND triggerObjectId = U.idUser
						AND dtProcessed IS NULL)

		/*

		IF THIS UPDATE AFFECTED USER STATUS (isActive) WE NEED TO SET/UNSET dtMarkedDisabled

		*/

		-- block the trigger from executing in tblUser UPDATE - otherwise the world will end with recursive calls to this trigger
		DECLARE @CONTEXT VARBINARY(128)

		SET @CONTEXT = CONVERT(VARBINARY(128), 'BLOCK TRIGGER')
		SET CONTEXT_INFO @CONTEXT

		-- set/unset dtMarkedDisabled
		UPDATE tblUser SET
			dtMarkedDisabled = CASE WHEN U.isActive = 0 THEN GETUTCDATE() ELSE NULL END
		FROM deleted UU		
		LEFT JOIN tblUser U ON U.idUser = UU.idUser
		WHERE UU.isActive <> U.isActive

		-- unblock the trigger
		SET @CONTEXT = CONVERT(VARBINARY(128), '')
		SET CONTEXT_INFO @CONTEXT

		END		

GO

-- [tblUserToGroupLink] - TRIGGER ON INSERT TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[UserToGroupLink.RulesExecQueueOnInsert]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [UserToGroupLink.RulesExecQueueOnInsert]
GO

CREATE TRIGGER [UserToGroupLink.RulesExecQueueOnInsert] ON [tblUserToGroupLink] AFTER INSERT
AS
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		UGL.idSite,
		'user',
		UGL.idUser,
		'INSERT',
		'tblUserToGroupLink',
		GETUTCDATE()		
	FROM inserted UGL
	WHERE UGL.idRuleSet IS NULL
	AND UGL.idUser IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'user'
					AND triggerObjectId = UGL.idUser
					AND dtProcessed IS NULL)

GO

-- [tblUserToGroupLink] - TRIGGER ON UPDATE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[UserToGroupLink.RulesExecQueueOnUpdate]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [UserToGroupLink.RulesExecQueueOnUpdate]
GO

CREATE TRIGGER [UserToGroupLink.RulesExecQueueOnUpdate] ON [tblUserToGroupLink] AFTER UPDATE
AS	
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		UGL.idSite,
		'user',
		UGL.idUser,
		'UPDATE',
		'tblUserToGroupLink',
		GETUTCDATE()		
	FROM deleted UGL
	WHERE UGL.idRuleSet IS NULL
	AND UGL.idUser IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'user'
					AND triggerObjectId = UGL.idUser
					AND dtProcessed IS NULL)

GO

-- [tblUserToGroupLink] - TRIGGER ON DELETE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[UserToGroupLink.RulesExecQueueOnDelete]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [UserToGroupLink.RulesExecQueueOnDelete]
GO

CREATE TRIGGER [UserToGroupLink.RulesExecQueueOnDelete] ON [tblUserToGroupLink] AFTER DELETE
AS

	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		UGL.idSite,
		'user',
		UGL.idUser,
		'DELETE',
		'tblUserToGroupLink',
		GETUTCDATE()		
	FROM deleted UGL
	WHERE NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					  WHERE triggerObject = 'user'
					  AND triggerObjectId = UGL.idUser
					  AND dtProcessed IS NULL)

GO

-- [tblGroup] - TRIGGER ON UPDATE TO QUEUE RULES EXECUTION - This is triggered because a Group name change can affect rules. 

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.RulesExecQueueOnUpdate]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [Group.RulesExecQueueOnUpdate]
GO

CREATE TRIGGER [Group.RulesExecQueueOnUpdate] ON [tblGroup] AFTER UPDATE
AS	
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		G.idSite,
		'group',
		G.idGroup,
		'UPDATE',
		'tblGroup',
		GETUTCDATE()		
	FROM deleted G
	WHERE NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					  WHERE triggerObject = 'group'
					  AND triggerObjectId = G.idGroup
					  AND dtProcessed IS NULL)

GO

-- [tblRuleSet] - TRIGGER ON UPDATE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSet.RulesExecQueueOnUpdate]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSet.RulesExecQueueOnUpdate]
GO

CREATE TRIGGER [RuleSet.RulesExecQueueOnUpdate] ON [tblRuleSet] AFTER UPDATE
AS	

	-- get the objects the ruleset change affects
	DECLARE @RuleSetTriggeredObjects TABLE (idSite INT, triggerObject NVARCHAR(50), triggerObjectId INT, dtEffectiveQueue DATETIME)

	INSERT INTO @RuleSetTriggeredObjects (
		idSite,
		triggerObject,
		triggerObjectId,
		dtEffectiveQueue
	)
	SELECT
		RS.idSite,
		CASE WHEN RSCL.idCertification IS NOT NULL THEN 'certification'
			 WHEN RSGL.idGroup IS NOT NULL THEN 'group'
			 WHEN RSRL.idRole IS NOT NULL THEN 'role'
			 WHEN RSRSEL.idRuleSetEnrollment IS NOT NULL THEN 'rulesetenrollment'
			 WHEN RSRSLPEL.idRuleSetLearningPathEnrollment IS NOT NULL THEN 'rulesetlearningpathenrollment'
		ELSE NULL END,
		CASE WHEN RSCL.idCertification IS NOT NULL THEN RSCL.idCertification
			 WHEN RSGL.idGroup IS NOT NULL THEN RSGL.idGroup
			 WHEN RSRL.idRole IS NOT NULL THEN RSRL.idRole
			 WHEN RSRSEL.idRuleSetEnrollment IS NOT NULL THEN RSRSEL.idRuleSetEnrollment
			 WHEN RSRSLPEL.idRuleSetLearningPathEnrollment IS NOT NULL THEN RSRSLPEL.idRuleSetLearningPathEnrollment
		ELSE NULL END,
		CASE WHEN RSRSEL.idRuleSetEnrollment IS NOT NULL AND RSE.dtStart > GETUTCDATE() THEN RSE.dtStart
			 WHEN RSRSLPEL.idRuleSetLearningPathEnrollment IS NOT NULL AND RSLPE.dtStart > GETUTCDATE() THEN RSLPE.dtStart
		ELSE GETUTCDATE() END
	FROM deleted RS
	LEFT JOIN tblRuleSetToCertificationLink RSCL ON RSCL.idRuleSet = RS.idRuleSet
	LEFT JOIN tblRuleSetToGroupLink RSGL ON RSGL.idRuleSet = RS.idRuleSet
	LEFT JOIN tblRuleSetToRoleLink RSRL ON RSRL.idRuleSet = RS.idRuleSet
	LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSRSEL ON RSRSEL.idRuleSet = RS.idRuleSet
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSRSEL.idRuleSetEnrollment
	LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSRSLPEL ON RSRSLPEL.idRuleSet = RS.idRuleSet
	LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = RSRSLPEL.idRuleSetLearningPathEnrollment

	-- queue it up
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RSTO.idSite,
		RSTO.triggerObject,
		RSTO.triggerObjectId,
		'UPDATE',
		'tblRuleSet',
		RSTO.dtEffectiveQueue		
	FROM @RuleSetTriggeredObjects RSTO		
	WHERE RSTO.idSite IS NOT NULL
	AND RSTO.triggerObject IS NOT NULL
	AND RSTO.triggerObjectId IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = RSTO.triggerObject
					AND triggerObjectId = RSTO.triggerObjectId
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToCertificationLink] - TRIGGER ON INSERT TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToCertificationLink.RulesExecQueueOnInsert]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToCertificationLink.RulesExecQueueOnInsert]
GO

CREATE TRIGGER [RuleSetToCertificationLink.RulesExecQueueOnInsert] ON [tblRuleSetToCertificationLink] AFTER INSERT
AS
			
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'certification',
		RL.idCertification,
		'INSERT',
		'tblRuleSetToCertificationLink',
		GETUTCDATE()		
	FROM inserted RL
	WHERE RL.idCertification IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'certification'
					AND triggerObjectId = RL.idCertification
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToGroupLink] - TRIGGER ON INSERT TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToGroupLink.RulesExecQueueOnInsert]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToGroupLink.RulesExecQueueOnInsert]
GO

CREATE TRIGGER [RuleSetToGroupLink.RulesExecQueueOnInsert] ON [tblRuleSetToGroupLink] AFTER INSERT
AS	
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'group',
		RL.idGroup,
		'INSERT',
		'tblRuleSetToGroupLink',
		GETUTCDATE()		
	FROM inserted RL
	WHERE RL.idGroup IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'group'
					AND triggerObjectId = RL.idGroup
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToRoleLink] - TRIGGER ON INSERT TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToRoleLink.RulesExecQueueOnInsert]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToRoleLink.RulesExecQueueOnInsert]
GO

CREATE TRIGGER [RuleSetToRoleLink.RulesExecQueueOnInsert] ON [tblRuleSetToRoleLink] AFTER INSERT
AS
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'role',
		RL.idRole,
		'INSERT',
		'tblRuleSetToRoleLink',
		GETUTCDATE()		
	FROM inserted RL
	WHERE RL.idRole IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'role'
					AND triggerObjectId = RL.idRole
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToRuleSetEnrollmentLink] - TRIGGER ON INSERT TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToRuleSetEnrollmentLink.RulesExecQueueOnInsert]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToRuleSetEnrollmentLink.RulesExecQueueOnInsert]
GO

CREATE TRIGGER [RuleSetToRuleSetEnrollmentLink.RulesExecQueueOnInsert] ON [tblRuleSetToRuleSetEnrollmentLink] AFTER INSERT
AS	
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'rulesetenrollment',
		RL.idRuleSetEnrollment,
		'INSERT',
		'tblRuleSetToRuleSetEnrollmentLink',		
		CASE WHEN RSE.dtStart > GETUTCDATE() THEN RSE.dtStart ELSE GETUTCDATE() END
	FROM inserted RL
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RL.idRuleSetEnrollment
	WHERE RL.idRuleSetEnrollment IS NOT NULL
	AND RSE.dtStart IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'rulesetenrollment'
					AND triggerObjectId = RL.idRuleSetEnrollment
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToRuleSetLearningPathEnrollmentLink] - TRIGGER ON INSERT TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToRuleSetLearningPathEnrollmentLink.RulesExecQueueOnInsert]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToRuleSetLearningPathEnrollmentLink.RulesExecQueueOnInsert]
GO

CREATE TRIGGER [RuleSetToRuleSetLearningPathEnrollmentLink.RulesExecQueueOnInsert] ON [tblRuleSetToRuleSetLearningPathEnrollmentLink] AFTER INSERT
AS
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'rulesetlearningpathenrollment',
		RL.idRuleSetLearningPathEnrollment,
		'INSERT',
		'tblRuleSetToRuleSetLearningPathEnrollmentLink',
		CASE WHEN RSE.dtStart > GETUTCDATE() THEN RSE.dtStart ELSE GETUTCDATE() END
	FROM inserted RL
	LEFT JOIN tblRuleSetLearningPathEnrollment RSE ON RSE.idRuleSetLearningPathEnrollment = RL.idRuleSetLearningPathEnrollment
	WHERE RL.idRuleSetLearningPathEnrollment IS NOT NULL
	AND RSE.dtStart IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'rulesetlearningpathenrollment'
					AND triggerObjectId = RL.idRuleSetLearningPathEnrollment
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToCertificationLink] - TRIGGER ON DELETE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToCertificationLink.RulesExecQueueOnDelete]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToCertificationLink.RulesExecQueueOnDelete]
GO

CREATE TRIGGER [RuleSetToCertificationLink.RulesExecQueueOnDelete] ON [tblRuleSetToCertificationLink] AFTER DELETE
AS
			
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'certification',
		RL.idCertification,
		'DELETE',
		'tblRuleSetToCertificationLink',
		GETUTCDATE()		
	FROM deleted RL
	WHERE RL.idCertification IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'certification'
					AND triggerObjectId = RL.idCertification
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToGroupLink] - TRIGGER ON DELETE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToGroupLink.RulesExecQueueOnDelete]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToGroupLink.RulesExecQueueOnDelete]
GO

CREATE TRIGGER [RuleSetToGroupLink.RulesExecQueueOnDelete] ON [tblRuleSetToGroupLink] AFTER DELETE
AS	
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'group',
		RL.idGroup,
		'DELETE',
		'tblRuleSetToGroupLink',
		GETUTCDATE()		
	FROM deleted RL
	WHERE RL.idGroup IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'group'
					AND triggerObjectId = RL.idGroup
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToRoleLink] - TRIGGER ON DELETE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToRoleLink.RulesExecQueueOnDelete]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToRoleLink.RulesExecQueueOnDelete]
GO

CREATE TRIGGER [RuleSetToRoleLink.RulesExecQueueOnDelete] ON [tblRuleSetToRoleLink] AFTER DELETE
AS
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'role',
		RL.idRole,
		'DELETE',
		'tblRuleSetToRoleLink',
		GETUTCDATE()		
	FROM deleted RL
	WHERE RL.idRole IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'role'
					AND triggerObjectId = RL.idRole
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToRuleSetEnrollmentLink] - TRIGGER ON DELETE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToRuleSetEnrollmentLink.RulesExecQueueOnDelete]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToRuleSetEnrollmentLink.RulesExecQueueOnDelete]
GO

CREATE TRIGGER [RuleSetToRuleSetEnrollmentLink.RulesExecQueueOnDelete] ON [tblRuleSetToRuleSetEnrollmentLink] AFTER DELETE
AS	
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'rulesetenrollment',
		RL.idRuleSetEnrollment,
		'DELETE',
		'tblRuleSetToRuleSetEnrollmentLink',
		CASE WHEN RSE.dtStart > GETUTCDATE() THEN RSE.dtStart ELSE GETUTCDATE() END
	FROM deleted RL
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RL.idRuleSetEnrollment
	WHERE RL.idRuleSetEnrollment IS NOT NULL
	AND RSE.dtStart IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'rulesetenrollment'
					AND triggerObjectId = RL.idRuleSetEnrollment
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetToRuleSetLearningPathEnrollmentLink] - TRIGGER ON DELETE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetToRuleSetLearningPathEnrollmentLink.RulesExecQueueOnDelete]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetToRuleSetLearningPathEnrollmentLink.RulesExecQueueOnDelete]
GO

CREATE TRIGGER [RuleSetToRuleSetLearningPathEnrollmentLink.RulesExecQueueOnDelete] ON [tblRuleSetToRuleSetLearningPathEnrollmentLink] AFTER DELETE
AS
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RL.idSite,
		'rulesetlearningpathenrollment',
		RL.idRuleSetLearningPathEnrollment,
		'DELETE',
		'tblRuleSetToRuleSetLearningPathEnrollmentLink',
		CASE WHEN RSE.dtStart > GETUTCDATE() THEN RSE.dtStart ELSE GETUTCDATE() END		
	FROM deleted RL
	LEFT JOIN tblRuleSetLearningPathEnrollment RSE ON RSE.idRuleSetLearningPathEnrollment = RL.idRuleSetLearningPathEnrollment
	WHERE RL.idRuleSetLearningPathEnrollment IS NOT NULL
	AND RSE.dtStart IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'rulesetlearningpathenrollment'
					AND triggerObjectId = RL.idRuleSetLearningPathEnrollment
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetEnrollment] - TRIGGER ON UPDATE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.RulesExecQueueOnUpdate]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetEnrollment.RulesExecQueueOnUpdate]
GO

CREATE TRIGGER [RuleSetEnrollment.RulesExecQueueOnUpdate] ON [tblRuleSetEnrollment] AFTER UPDATE
AS	
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RSE.idSite,
		'rulesetenrollment',
		RSE.idRuleSetEnrollment,
		'UPDATE',
		'tblRuleSetEnrollment',
		CASE WHEN RSE.dtStart > GETUTCDATE() THEN RSE.dtStart ELSE GETUTCDATE() END
	FROM deleted RSE
	WHERE RSE.dtStart IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'rulesetenrollment'
					AND triggerObjectId = RSE.idRuleSetEnrollment
					AND dtProcessed IS NULL)

GO

-- [tblRuleSetLearningPathEnrollment] - TRIGGER ON UPDATE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.RulesExecQueueOnUpdate]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [RuleSetLearningPathEnrollment.RulesExecQueueOnUpdate]
GO

CREATE TRIGGER [RuleSetLearningPathEnrollment.RulesExecQueueOnUpdate] ON [tblRuleSetLearningPathEnrollment] AFTER UPDATE
AS
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		RSE.idSite,
		'rulesetlearningpathenrollment',
		RSE.idRuleSetLearningPathEnrollment,
		'UPDATE',
		'tblRuleSetLearningPathEnrollment',
		CASE WHEN RSE.dtStart > GETUTCDATE() THEN RSE.dtStart ELSE GETUTCDATE() END		
	FROM deleted RSE
	WHERE RSE.dtStart IS NOT NULL
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'rulesetlearningpathenrollment'
					AND triggerObjectId = RSE.idRuleSetLearningPathEnrollment
					AND dtProcessed IS NULL)

GO

-- [tblEnrollment] - TRIGGER ON INSERT TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.RulesExecQueueOnInsert]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [Enrollment.RulesExecQueueOnInsert]
GO

CREATE TRIGGER [Enrollment.RulesExecQueueOnInsert] ON [tblEnrollment] AFTER INSERT
AS
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		E.idSite,
		'course',
		E.idCourse,
		'INSERT',
		'tblEnrollment',
		GETUTCDATE()
	FROM inserted E	
	WHERE E.idRuleSetEnrollment IS NULL -- don't queue for inserted ruleset enrollments as those inserts are triggered from rules engine
	AND E.idActivityImport IS NULL -- don't queue for id activity import enrollments, there is no point
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'courseenrollment'
					AND triggerObjectId = E.idEnrollment
					AND dtProcessed IS NULL)

GO

-- [tblLearningPathEnrollment] - TRIGGER ON INSERT TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPathEnrollment.RulesExecQueueOnInsert]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [LearningPathEnrollment.RulesExecQueueOnInsert]
GO

CREATE TRIGGER [LearningPathEnrollment.RulesExecQueueOnInsert] ON [tblLearningPathEnrollment] AFTER INSERT
AS
		
	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		LPE.idSite,
		'learningpath',
		LPE.idLearningPath,
		'INSERT',
		'tblLearningPathEnrollment',
		GETUTCDATE()
	FROM inserted LPE	
	WHERE LPE.idRuleSetLearningPathEnrollment IS NULL -- don't queue for inserted ruleset enrollments as those inserts are triggered from rules engine	
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'learningpathenrollment'
					AND triggerObjectId = LPE.idLearningPathEnrollment
					AND dtProcessed IS NULL)

GO

-- [tblEnrollment] - TRIGGER ON DELETE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.RulesExecQueueOnDelete]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [Enrollment.RulesExecQueueOnDelete]
GO

CREATE TRIGGER [Enrollment.RulesExecQueueOnDelete] ON [tblEnrollment] AFTER DELETE
AS

	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		E.idSite,
		'course',
		E.idCourse,
		'DELETE',
		'tblEnrollment',
		GETUTCDATE()		
	FROM deleted E
	WHERE E.idRuleSetEnrollment IS NULL -- don't queue for deleted ruleset enrollments as those deletes are triggered from rules engine
	AND E.idActivityImport IS NULL -- don't queue for id activity import enrollments, there is no point
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'courseenrollment'
					AND triggerObjectId = E.idEnrollment
					AND dtProcessed IS NULL)	

GO

-- [tblLearningPathEnrollment] - TRIGGER ON DELETE TO QUEUE RULES EXECUTION

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPathEnrollment.RulesExecQueueOnDelete]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [LearningPathEnrollment.RulesExecQueueOnDelete]
GO

CREATE TRIGGER [LearningPathEnrollment.RulesExecQueueOnDelete] ON [tblLearningPathEnrollment] AFTER DELETE
AS

	INSERT INTO [tblRuleSetEngineQueue] (
		idSite,
		triggerObject,
		triggerObjectId,
		eventAction,
		eventTable,
		dtQueued		
	)
	SELECT
		LPE.idSite,
		'learningpath',
		LPE.idLearningPath,
		'DELETE',
		'tblLearningPathEnrollment',
		GETUTCDATE()		
	FROM deleted LPE
	WHERE LPE.idRuleSetLearningPathEnrollment IS NULL -- don't queue for deleted ruleset enrollments as those deletes are triggered from rules engine
	AND NOT EXISTS (SELECT 1 FROM [tblRuleSetEngineQueue]
					WHERE triggerObject = 'learningpathenrollment'
					AND triggerObjectId = LPE.idLearningPathEnrollment
					AND dtProcessed IS NULL)
						
GO