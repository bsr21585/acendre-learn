DECLARE	@Return_Code int
DECLARE @DetailsAsLanguage NVARCHAR(10)

SET @DetailsAsLanguage = 'fr-fr'

/*

Create the SITE if it does not exist

*/
DECLARE @idSite INT

SELECT @idSite = idSite FROM tblSite WHERE hostname = 'ics'

EXEC	[dbo].[Site.Save]
		@Return_Code	OUTPUT,
		NULL, --idCallerSite
		NULL, --callerLanguageString
		1, --idCaller
		@idSite			OUTPUT,
		N'ics',
		N'w15rich',
		1, --isActive
		NULL, --dtExpires
		N'BlackBird LMS',
		N'ICS Learning Group',
		N'Brian Kleeman',
		N'brian.kleeman@icslearninggroup.com',
		200, --userlimit
		1024, --kbLimit
		'en-US', --idLanguage
		NULL --token

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Site.Save'
ELSE
	PRINT 'Site.Save SUCCESS'
	
/*

Site Languages

*/

EXEC	[dbo].[Site.SaveLang]
		@Return_Code OUTPUT,
		NULL, --idCallerSite
		'fr-FR', -- callerLanguageString
		1, --idCaller
		@idSite,
		'Le ICS LMS', --title
		'Le ICS Learning Group' --company

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Site.SaveLang (fr-FR)'
ELSE
	PRINT 'Site.SaveLang (fr-FR) SUCCESS'
	
EXEC	[dbo].[Site.SaveLang]
		@Return_Code OUTPUT,
		NULL, --idCallerSite
		'es-ES',
		1, --idCaller
		@idSite,
		'El ICS LMSO', --title
		'El ICS LEarning Groupo' --company

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Site.SaveLang (es-ES)'
ELSE
	PRINT 'Site.SaveLang (es-ES) SUCCESS'
	
/*

DISPLAY SITE INFO

*/

DECLARE @siteHostname NVARCHAR(255)
DECLARE @siteTitle NVARCHAR(255)
DECLARE @siteCompany NVARCHAR(255)
DECLARE @siteContactName NVARCHAR(255)

EXEC	[Site.Details]
		@Return_Code		OUTPUT,
		@idSite,
		@DetailsAsLanguage,
		1,
		@idSite, 
		@siteHostname		OUTPUT,
		null,
		null,
		null,
		@siteTitle			OUTPUT,
		@siteCompany		OUTPUT,
		@siteContactName	OUTPUT,
		null,
		null,
		null,
		null,
		null
	
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Site.Details'
ELSE
	BEGIN
		
	PRINT '---------------'
	PRINT 'SITE INFO:'
	PRINT 'id: ' + CONVERT(NVARCHAR(10), @idSite)
	PRINT 'hostname: ' + @siteHostname
	PRINT 'title: ' + @siteTitle
	PRINT 'company: ' + @siteCompany
	PRINT 'contact: ' + @siteContactName
	PRINT '---------------'
	
	END
	
/*

ROLES

*/
	
DECLARE @idRole1 INT
DECLARE @idRole2 INT

SELECT @idRole1 = idRole FROM tblRole WHERE name = 'Master Big Cheese' AND idSite = @idSite
SELECT @idRole2 = idRole FROM tblRole WHERE name = 'Small Fry' AND idSite = @idSite

EXEC	[Role.Save]
		@Return_Code OUTPUT,
		@idSite,
		'en-US', --language
		1, --caller
		@idRole1 OUTPUT, --idRole
		'Master Big Cheese'

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Role.Save #1'
ELSE
	PRINT 'Role.Save SUCCESS #1'

/*

Role Languages

*/

EXEC	[Role.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'fr-FR', --language
		1, --caller
		@idRole1, --idRole
		'Le Master Big Cheese'

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Role.SaveLang #1'
ELSE
	PRINT 'Role.SaveLang SUCCESS (fr-FR) #1'
	
EXEC	[Role.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'es-ES', --language
		1, --caller
		@idRole1, --idRole
		'El Master Big Cheeso'

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Role.SaveLang #1'
ELSE
	PRINT 'Role.SaveLang SUCCESS (es-ES) #1'
	
/*

ROLE 2

*/

EXEC	[Role.Save]
		@Return_Code OUTPUT,
		@idSite,
		'en-US', --language
		1, --caller
		@idRole2 OUTPUT, --idRole
		'Small Fry'

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Role.Save #2'
ELSE
	PRINT 'Role.Save SUCCESS #2'

/*

Role Languages

*/

EXEC	[Role.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'fr-FR', --language
		1, --caller
		@idRole2, --idRole
		'Le Small Fry'

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Role.SaveLang #2'
ELSE
	PRINT 'Role.SaveLang SUCCESS (fr-FR) #2'
	
EXEC	[Role.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'es-ES', --language
		1, --caller
		@idRole2, --idRole
		'El Poquito Fry'

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Role.SaveLang #2'
ELSE
	PRINT 'Role.SaveLang SUCCESS (es-ES) #2'
	
/*

DISPLAY THE ROLE INFO

*/

DECLARE @roleName1 NVARCHAR(255)
DECLARE @roleName2 NVARCHAR(255)

EXEC	[Role.Details]
		@Return_Code		OUTPUT,
		@idSite,
		@DetailsAsLanguage,
		1,
		@idRole1			OUTPUT,
		@roleName1			OUTPUT
	
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Role.Details'
ELSE
	BEGIN
		
	PRINT '---------------'
	PRINT 'FIRST ROLE INFO:'
	PRINT 'id: ' + CONVERT(NVARCHAR(10), @idRole1)
	PRINT 'name: ' + @roleName1
	PRINT '---------------'
	
	END

EXEC	[Role.Details]
		@Return_Code		OUTPUT,
		@idSite,
		@DetailsAsLanguage,
		1,
		@idRole2			OUTPUT,
		@roleName2			OUTPUT
	
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Role.Details'
ELSE
	BEGIN
		
	PRINT '---------------'
	PRINT 'SECOND ROLE INFO:'
	PRINT 'id: ' + CONVERT(NVARCHAR(10), @idRole2)
	PRINT 'name: ' + @roleName2
	PRINT '---------------'
	
	END
	
	
/*

GROUPS

*/
	
DECLARE @idGroup INT
DECLARE @identifierGroup UNIQUEIDENTIFIER

SELECT @idGroup = idGroup FROM tblGroup WHERE name = 'Big Cheese Users' AND idSite = @idSite
SELECT @identifierGroup = NEWID()

EXEC	[Group.Save]
		@Return_Code OUTPUT,
		@idSite,
		'en-US', --language
		1, --caller
		@idGroup OUTPUT, --idGroup
		'Big Cheese Users',
		'grp-token-1',
		@identifierGroup,
		'CN=big_cheese'

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Group.Save'
ELSE
	PRINT 'Group.Save SUCCESS'

/*

Group Languages

*/

EXEC	[Group.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'fr-FR', --language
		1, --caller
		@idGroup,
		'Le Big Cheese Users'

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Group.SaveLang'
ELSE
	PRINT 'Group.SaveLang SUCCESS (fr-FR)'
	
EXEC	[Group.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'es-ES', --language
		1, --caller
		@idGroup,
		'El Big Cheeso Usaros'

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Group.SaveLang'
ELSE
	PRINT 'Group.SaveLang SUCCESS (es-ES)'
	
/*

GRANT Group Permissions

*/

EXEC	[Group.GrantPermissionWithScope]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1, -- id Caller
		@idGroup,
		141, -- idPermission (Catalogs.Content)
		'1,3' -- scopes
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Group.GrantPermissionWithScope (141)'
ELSE
	PRINT 'Group.GrantPermissionWithScope (141) SUCCESS'
	
EXEC	[Group.RemovePermissions]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1, -- id Caller
		@idGroup,
		141 -- idPermission (Catalogs.Content)
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Group.RemovePermissions (141)'
ELSE
	PRINT 'Group.RemovePermissions (141) SUCCESS'
	
/*

DISPLAY THE GROUP INFO

*/

DECLARE @groupName NVARCHAR(255)
DECLARE @primaryGroupToken NVARCHAR(255)
DECLARE @objectGUID NVARCHAR(36)
DECLARE @distinguishedName NVARCHAR(255)

EXEC	[Group.Details]
		@Return_Code		OUTPUT,
		@idSite,
		@DetailsAsLanguage,
		1,
		@idGroup			OUTPUT,
		@groupName			OUTPUT,
		@primaryGroupToken	OUTPUT,
		@objectGUID			OUTPUT,
		@distinguishedName	OUTPUT
	
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Group.Details'
ELSE
	BEGIN
		
	PRINT '---------------'
	PRINT 'GROUP INFO:'
	PRINT 'id: ' + CONVERT(NVARCHAR(10), @idGroup)
	PRINT 'name: ' + @groupName
	PRINT 'primaryGroupToken: ' + @primaryGroupToken
	PRINT 'objectGUID: ' + @objectGUID
	PRINT 'distinguishedName: ' + @distinguishedName
	PRINT '---------------'
	
	END
	
/*

USER

*/

DECLARE @idUser INT
SELECT @idUser = idUser FROM tblUser WHERE username = 'bkleeman' AND idSite = @idSite

EXEC	[User.Save]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1,
		@idUser		OUTPUT,
		'Brian', -- firstName
		'Andrew', -- middleName
		'Kleeman', -- lastname
		'bkleeman@gmail.com', --@email
		'bkleeman', --@username
		'w15rich', --@password
		17, --@idTimezone
		null, -- objectGUID
		null, -- activationGUID
		null, -- distinguishedName
		1, -- isActive
		0, -- mustchangePassword
		null, -- dtExpires
		null, -- language
		null, -- employeeID
		null, -- company
		null, -- address
		null, -- city
		null, -- province
		null, -- postalcode
		null, -- country
		null, -- phonePrimary
		null, -- phoneWork
		null, -- phoneFax
		null, -- phoneHome
		null, -- phoneMobile
		null, -- phonePager
		null, -- phoneOther
		null, -- department
		null, -- division
		null, -- region
		null, -- jobTitle
		null, -- jobClass
		null, -- ssn
		null, -- gender
		null, -- race
		null, -- dtDOB
		null, -- dtHire
		null, -- dtTerm
		null, -- field00
		null, -- field01
		null, -- field02
		null, -- field03
		null, -- field04
		null, -- field05
		null, -- field06
		null, -- field07
		null, -- field08
		null -- field09
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on User.Save'
ELSE
	PRINT 'User.Save SUCCESS'
	
DECLARE @username NVARCHAR(512)
DECLARE @wholeName NVARCHAR(765)
DECLARE @email NVARCHAR(255)

EXEC	[User.Details]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1,
		@idSite OUTPUT,
		@idUser OUTPUT,
		null,
		null,
		null,
		@wholeName OUTPUT,
		@email OUTPUT,
		@username OUTPUT,
		@password = null,
		@idTimezone = null,
		@objectGUID = null,
		@activationGUID = null,
		@distinguishedName = null,
		@isActive = null,
		@mustchangePassword = null,
		@dtCreated = null,
		@dtExpires = null,
		@language = null,
		@dtModified = null,
		@employeeID = null,
		@company = null,
		@address = null,
		@city = null,
		@province = null,
		@postalcode = null,
		@country = null,
		@phonePrimary = null,
		@phoneWork = null,
		@phoneFax = null,
		@phoneHome = null,
		@phoneMobile = null,
		@phonePager = null,
		@phoneOther = null,
		@department = null,
		@division = null,
		@region = null,
		@jobTitle = null,
		@jobClass = null,
		@ssn = null,
		@gender = null,
		@race = null,
		@dtDOB = null,
		@dtHire = null,
		@dtTerm = null,
		@field00 = null,
		@field01 = null,
		@field02 = null,
		@field03 = null,
		@field04 = null,
		@field05 = null,
		@field06 = null,
		@field07 = null,
		@field08 = null,
		@field09 = null
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on User.Details'
ELSE
	BEGIN
		
	PRINT '---------------'
	PRINT 'USER INFO:'
	PRINT 'id: ' + CONVERT(NVARCHAR(10), @idUser)
	PRINT 'name: ' + @wholeName
	PRINT 'username: ' + @username
	PRINT 'email: ' + @email
	PRINT '---------------'
	
	END
	
/*

GRANT USER PERMISSIONS

*/

EXEC	[User.GrantPermissionWithScope]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1, -- id Caller
		@idUser,
		141, -- idPermission (Catalogs.Content)
		null -- scopes
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on User.GrantPermissionWithScope (141)'
ELSE
	PRINT 'User.GrantPermissionWithScope (141) SUCCESS'
	
EXEC	[User.GrantPermissionWithScope]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1, -- id Caller
		@idUser,
		21, -- idPermission (Interface.Configuration)
		null -- scopes
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on User.GrantPermissionWithScope (21)'
ELSE
	PRINT 'User.GrantPermissionWithScope (21) SUCCESS'	

/*

MAIN CATALOG

*/

DECLARE @idCatalog INT
SELECT @idCatalog = idCatalog FROM tblCatalog WHERE idSite = @idSite AND title = 'Main Catalog'

EXEC	[Catalog.Save]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1, --idCaller
		@idCatalog OUTPUT,
		'Main Catalog', -- title
		NULL, -- idParent
		0,
		'CODE-1',
		'This is the short description.',
		'<b>This is the long description</b>',
		1, -- isPublished
		0, -- isPrivate
		0 -- isClosed
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.Save (MAIN)'
ELSE
	PRINT 'Catalog.Save SUCCESS (MAIN)'
	
/* 

catalog languages

*/
	
EXEC	[Catalog.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'es-ES', --language
		1, --caller
		@idCatalog, 
		'Catalogo Primero', 
		'Esta es el descripcion poquito', 
		'<b>Esta es el descripcion poquito</b>'
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.SaveLang (es-ES)'
ELSE
	PRINT 'Catalog.SaveLang SUCCESS (es-ES)'
	
EXEC	[Catalog.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'fr-FR', --language
		1, --caller
		@idCatalog, 
		'Le Catalogue Un', 
		'Frenchy short description', 
		'<b>Frenchy short description</b>'
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.SaveLang (fr-FR)'
ELSE
	PRINT 'Catalog.SaveLang SUCCESS (fr-FR)'

/*

Sub Catalogs

*/

DECLARE @idCatalogSub1 INT
DECLARE @idCatalogSub2 INT
SELECT @idCatalogSub1 = idCatalog FROM tblCatalog WHERE idSite = @idSite AND idParent = @idCatalog AND title = 'Sub Catalog A'
SELECT @idCatalogSub2 = idCatalog FROM tblCatalog WHERE idSite = @idSite AND idParent = @idCatalog AND title = 'Sub Catalog B'

EXEC	[Catalog.Save]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1, --idCaller
		@idCatalogSub1 OUTPUT,
		'Sub Catalog A', -- title
		@idCatalog, -- idParent
		0,
		'CODE-SUBA',
		'This is the short description of the first sub catalog.',
		'<b>This is the long description of the first sub catalog</b>',
		1, -- isPublished
		0, -- isPrivate
		0 -- isClosed
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.Save (Sub A)'
ELSE
	PRINT 'Catalog.Save SUCCESS (Sub A)'
	
EXEC	[Catalog.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'fr-FR', --language
		1, --caller
		@idCatalogSub1, 
		'Le Sub Catalogue A', 
		'Frenchy short description for sub cat A', 
		'<b>Frenchy short description for sub cat A</b>'
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.SaveLang (SUB A, fr-FR)'
ELSE
	PRINT 'Catalog.SaveLang SUCCESS (SUB A, fr-FR)'
	
EXEC	[Catalog.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'es-ES', --language
		1, --caller
		@idCatalogSub1,
		'El Catalog Por A', 
		'Spanish short description for sub cat A', 
		'<b>Spanish short description for sub cat A</b>'
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.SaveLang (SUB A, es-ES)'
ELSE
	PRINT 'Catalog.SaveLang SUCCESS (SUB A, es-ES)'
	
EXEC	[Catalog.Save]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1, --idCaller
		@idCatalogSub2 OUTPUT,
		'Sub Catalog B', -- title
		@idCatalog, -- idParent
		0,
		'CODE-SUBB',
		'This is the short description of the second sub catalog.',
		'<b>This is the long description of the second sub catalog</b>',
		1, -- isPublished
		0, -- isPrivate
		0 -- isClosed
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.Save (Sub B)'
ELSE
	PRINT 'Catalog.Save SUCCESS (Sub B)'
	
EXEC	[Catalog.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'fr-FR', --language
		1, --caller
		@idCatalogSub2, 
		'Le Sub Catalogue B', 
		'Frenchy short description for sub cat B', 
		'<b>Frenchy short description for sub cat B</b>'
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.SaveLang (SUB B, fr-FR)'
ELSE
	PRINT 'Catalog.SaveLang SUCCESS (SUB B, fr-FR)'
	
EXEC	[Catalog.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'es-ES', --language
		1, --caller
		@idCatalogSub2, 
		'El Catalog Por B', 
		'Spanish short description for sub cat B', 
		'<b>Spanish short description for sub cat B</b>'
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.SaveLang (SUB B, es-ES)'
ELSE
	PRINT 'Catalog.SaveLang SUCCESS (SUB B, es-ES)'
	
DECLARE @idCatalogSubSub1 INT
SELECT @idCatalogSubSub1 = idCatalog FROM tblCatalog WHERE idSite = @idSite AND idParent = @idCatalogSub1 AND title = 'Bottom Level'

EXEC	[Catalog.Save]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		1, --idCaller
		@idCatalogSubSub1	OUTPUT,
		'Bottom Level', -- title
		@idCatalogSub1, -- idParent
		0,
		'CODE-SUBSUBAAA',
		'This is the short description of the bottom sub catalog.',
		'<b>This is the long description of the bottom sub catalog</b>',
		1, -- isPublished
		0, -- isPrivate
		0 -- isClosed
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.Save (Sub Sub AAA)'
ELSE
	PRINT 'Catalog.Save SUCCESS (Sub Sub AAA)'
	
EXEC	[Catalog.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'fr-FR', --language
		1, --caller
		@idCatalogSubSub1, 
		'Le Bottom Catalogue', 
		'Frenchy short description for the bottom catalog', 
		'<b>Frenchy short description for the bottom catalog</b>'
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.SaveLang (Sub Sub AAA, fr-FR)'
ELSE
	PRINT 'Catalog.SaveLang SUCCESS (Sub Sub AAA, fr-FR)'
	
EXEC	[Catalog.SaveLang]
		@Return_Code OUTPUT,
		@idSite,
		'es-ES', --language
		1, --caller
		@idCatalogSubSub1, 
		'El Catalog Bottom', 
		'Spanish short description for bottom catalog', 
		'<b>Spanish short description for bottom cat B</b>'
		
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.SaveLang (Sub Sub AAA, es-ES)'
ELSE
	PRINT 'Catalog.SaveLang SUCCESS (Sub Sub AAA, es-ES)'
	
/*

DISPLAY THE CATALOG INFO

*/

DECLARE @catalogTitle1 NVARCHAR(255)
DECLARE @catalogCode1 NVARCHAR(255)
DECLARE @catalogSDecription1 NVARCHAR(512)
DECLARE @catalogLDescription1 NVARCHAR(MAX)
DECLARE @catalogParent INT

EXEC	[Catalog.Details]
		@Return_Code			OUTPUT,
		@idSite,
		@DetailsAsLanguage,
		1, -- idCaller
		@idCatalog,
		@catalogTitle1			OUTPUT, 
		null, -- idParent
		null, -- order
		@catalogCode1			OUTPUT,
		@catalogSDecription1	OUTPUT, 
		@catalogLDescription1	OUTPUT, 
		null, -- isPublished
		null, -- isPrivate
		null -- isClosed
	
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.Details'
ELSE
	BEGIN
		
	PRINT '---------------'
	PRINT 'FIRST CATALOG INFO:'
	PRINT 'id: ' + CONVERT(NVARCHAR(10), @idCatalog)
	PRINT 'title: ' + @catalogTitle1
	PRINT 'code: ' + @catalogCode1
	PRINT 'short Description: ' + @catalogSDecription1
	PRINT 'long Descripion: ' + @catalogLDescription1
	PRINT '---------------'
	
	END
	

/*

Get Sub Catalog details 

*/

EXEC	[Catalog.Details]
		@Return_Code			OUTPUT,
		@idSite,
		@DetailsAsLanguage,
		1, -- idCaller
		@idCatalogSub1,
		@catalogTitle1			OUTPUT, 
		@catalogParent			OUTPUT, -- idParent
		null, -- order
		@catalogCode1			OUTPUT,
		@catalogSDecription1	OUTPUT, 
		@catalogLDescription1	OUTPUT, 
		null, -- isPublished
		null, -- isPrivate
		null -- isClosed
	
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.Details (Sub A)'
ELSE
	BEGIN
		
	PRINT '---------------'
	PRINT 'SUB A CATALOG INFO:'
	PRINT 'id: ' + CONVERT(NVARCHAR(10), @idCatalogSub1)
	PRINT 'title: ' + @catalogTitle1
	PRINT 'code: ' + @catalogCode1
	PRINT 'parent: ' + CONVERT(NVARCHAR(10), @catalogParent)
	PRINT 'short Description: ' + @catalogSDecription1
	PRINT 'long Descripion: ' + @catalogLDescription1
	PRINT '---------------'
	
	END

EXEC	[Catalog.Details]
		@Return_Code			OUTPUT,
		@idSite,
		@DetailsAsLanguage,
		1, -- idCaller
		@idCatalogSub2,
		@catalogTitle1			OUTPUT, 
		@catalogParent			OUTPUT, -- idParent
		null, -- order
		@catalogCode1			OUTPUT,
		@catalogSDecription1	OUTPUT, 
		@catalogLDescription1	OUTPUT, 
		null, -- isPublished
		null, -- isPrivate
		null -- isClosed
	
IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.Details (Sub B)'
ELSE
	BEGIN
		
	PRINT '---------------'
	PRINT 'SUB B CATALOG INFO:'
	PRINT 'id: ' + CONVERT(NVARCHAR(10), @idCatalogSub2)
	PRINT 'title: ' + @catalogTitle1
	PRINT 'code: ' + @catalogCode1
	PRINT 'parent: ' + CONVERT(NVARCHAR(10), @catalogParent)
	PRINT 'short Description: ' + @catalogSDecription1
	PRINT 'long Descripion: ' + @catalogLDescription1
	PRINT '---------------'
	
	END

		
/*

Role DELETES

*/

EXEC	[Role.Delete]
		@Return_Code OUTPUT,
		@idSite,
		'en-US',
		@idUser,
		@idRole1

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Role.Delete'
ELSE
	PRINT 'Role.Delete SUCCESS'	
	
/*

Catalog deletes
	
*/


DECLARE @DeleteCatalogs IDTable
INSERT INTO @DeleteCatalogs (id) VALUES (@idCatalogSub2)

EXEC	[Catalog.Delete]
		@Return_Code OUTPUT,
		@idSite,
		@DetailsAsLanguage,
		1, -- idCaller
		@DeleteCatalogs

IF (@Return_Code <> 0) 
	PRINT 'ERROR (' + CONVERT(NVARCHAR(4), @Return_Code) + ') on Catalog.Delete'
ELSE
	PRINT 'Catalog.Delete SUCCESS'	


