
-- DROP [DataLesson.ClearHomeworkAssignment] AS IT HAS BEEN CHANGED TO [DataLesson.ClearTask]

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DataLesson.ClearHomeworkAssignment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.ClearHomeworkAssignment]
GO

-- DROP [DataLesson.ProctorHomeworkAssignment] AS IT HAS BEEN CHANGED TO [DataLesson.ProctorTask]

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DataLesson.ProctorHomeworkAssignment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.ProctorHomeworkAssignment]
GO

-- DROP [DataLesson.SaveHomeworkAssignment] AS IT HAS BEEN CHANGED TO [DataLesson.SaveTask]

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DataLesson.SaveHomeworkAssignment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.SaveHomeworkAssignment]
GO

-- DROP [Widget.HomeworkAssignmentProctor] AS IT HAS BEEN CHANGED TO [Widget.TaskProctor]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Widget.HomeworkAssignmentProctor]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.HomeworkAssignmentProctor]
GO

-- DROP [Purchase.GetDetailsByUserId] AS IT HAS BEEN CHANGED TO [Purchase.GetActiveCartForUser]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Purchase.GetDetailsByUserId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Purchase.GetDetailsByUserId]
GO

-- DROP [Purchase.GetDetailsByOrderNumber]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Purchase.GetDetailsByOrderNumber]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Purchase.GetDetailsByOrderNumber]
GO

-- DROP [Purchase.Confirm]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Purchase.Confirm]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Purchase.Confirm]
GO

-- DROP [TransactionItem.GetUnconfirmedTransactions] AS IT HAS BEEN CHANGED TO [TransactionItem.GetItemsForPurchase]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.GetUnconfirmedTransactions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.GetUnconfirmedTransactions]
GO

-- DROP [TransactionItem.Update] AS IT HAS BEEN CHANGED TO [TransactionItem.ConfirmTransactionItems]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.Update]
GO

-- DROP [TransactionItem.GetIdsForUser]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.GetIdsForUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.GetIdsForUser]
GO

-- DROP [TransactionItem.IsAddedToCart]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.IsAddedToCart]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.IsAddedToCart]
GO

-- DROP [TransactionItem.GetTotalAmount]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.GetTotalAmount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.GetTotalAmount]
GO

-- DROP [TransactionItem.GetPaidItems]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.GetPaidItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.GetPaidItems]
GO

-- DROP [TransactionItem.GetPurchasedItemsGrid]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.GetPurchasedItemsGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.GetPurchasedItemsGrid]
GO

-- DROP [StandupTraining.GetStandupTrainings]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.GetStandupTrainings]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.GetStandupTrainings]
GO

/*

ADDITIONS TO [tblCertificationModuleRequirement]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertificationModuleRequirement' AND column_name = 'documentationApprovalRequired')
	ALTER TABLE [tblCertificationModuleRequirement] ADD [documentationApprovalRequired] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertificationModuleRequirement' AND column_name = 'allowSupervisorsToApproveDocumentation')
	ALTER TABLE [tblCertificationModuleRequirement] ADD [allowSupervisorsToApproveDocumentation] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertificationModuleRequirement' AND column_name = 'allowExpertsToApproveDocumentation')
	ALTER TABLE [tblCertificationModuleRequirement] ADD [allowExpertsToApproveDocumentation] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertificationModuleRequirement' AND column_name = 'courseCreditEligibleCoursesIsAll')
	ALTER TABLE [tblCertificationModuleRequirement] ADD [courseCreditEligibleCoursesIsAll] BIT NULL
GO

/*

ADDITIONS TO [tblPurchase]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblPurchase' AND column_name = 'transactionId')
	ALTER TABLE [tblPurchase] ADD [transactionId] NVARCHAR(255) NULL
GO

/*

ADDITIONS TO [tblStandUpTraining]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTraining' AND column_name = 'avatar')
	ALTER TABLE [tblStandUpTraining] ADD [avatar] NVARCHAR(255) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTraining' AND column_name = 'cost')
	ALTER TABLE [tblStandUpTraining] ADD [cost] FLOAT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTraining' AND column_name = 'isDeleted')
	ALTER TABLE [tblStandUpTraining] ADD [isDeleted] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTraining' AND column_name = 'dtDeleted')
	ALTER TABLE [tblStandUpTraining] ADD [dtDeleted] DATETIME NULL
GO

/*

ADDITIONS TO [tblStandUpTrainingInstance]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTrainingInstance' AND column_name = 'isDeleted')
	ALTER TABLE [tblStandUpTrainingInstance] ADD [isDeleted] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTrainingInstance' AND column_name = 'dtDeleted')
	ALTER TABLE [tblStandUpTrainingInstance] ADD [dtDeleted] DATETIME NULL
GO

/*

ADDITIONS TO [tblCouponCode]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCouponCode' AND column_name = 'forStandupTraining')
	ALTER TABLE [tblCouponCode] ADD [forStandupTraining] INT NULL
GO