-- UPDATE EVENT TYPES FOR THE "LESSON" TO "MODULE" RENAME
UPDATE tblEventType SET name = 'Module Passed' WHERE idEventType = 301
UPDATE tblEventType SET name = 'Module Failed' WHERE idEventType = 302
UPDATE tblEventType SET name = 'Module Completed' WHERE idEventType = 303

/*

ADDITIONS TO [tblCourse]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'requireFirstLessonToBeCompletedBeforeOthers')
	ALTER TABLE [tblCourse] ADD [requireFirstLessonToBeCompletedBeforeOthers] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'lockLastLessonUntilOthersCompleted')
	ALTER TABLE [tblCourse] ADD [lockLastLessonUntilOthersCompleted] BIT NULL
GO

/*

ADDITIONS TO [tblCertificate]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertificate' AND column_name = 'reissueBasedOnMostRecentCompletion')
	ALTER TABLE [tblCertificate] ADD [reissueBasedOnMostRecentCompletion] BIT NULL
GO