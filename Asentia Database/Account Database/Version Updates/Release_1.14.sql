/*

DROP THE [Site.DeleteSite] PROCEDURE

*/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Site.DeleteSite]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.DeleteSite]
GO

/*

ADDITIONS TO [tblEventEmailNotification]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEventEmailNotification' AND column_name = 'isHTMLBased')
	ALTER TABLE [tblEventEmailNotification] ADD [isHTMLBased] BIT NULL
GO

/*

ADDITIONS TO [tblEventEmailQueue]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEventEmailQueue' AND column_name = 'isHTMLBased')
	ALTER TABLE [tblEventEmailQueue] ADD [isHTMLBased] BIT NULL
GO

/*

ADDITIONS TO [tblRuleSetLearningPathEnrollment]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblRuleSetLearningPathEnrollment' AND column_name = 'expiresFromStartInterval')
	ALTER TABLE tblRuleSetLearningPathEnrollment ADD [expiresFromStartInterval] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblRuleSetLearningPathEnrollment' AND column_name = 'expiresFromStartTimeframe')
	ALTER TABLE tblRuleSetLearningPathEnrollment ADD [expiresFromStartTimeframe] NVARCHAR(4) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblRuleSetLearningPathEnrollment' AND column_name = 'expiresFromFirstLaunchInterval')
	ALTER TABLE tblRuleSetLearningPathEnrollment ADD [expiresFromFirstLaunchInterval] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblRuleSetLearningPathEnrollment' AND column_name = 'expiresFromFirstLaunchTimeframe')
	ALTER TABLE tblRuleSetLearningPathEnrollment ADD [expiresFromFirstLaunchTimeframe] NVARCHAR(4)	NULL
GO

/*

ADDITIONS TO [tblLearningPathEnrollment]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPathEnrollment' AND column_name = 'dtExpiresFromStart')
	ALTER TABLE tblLearningPathEnrollment ADD [dtExpiresFromStart] DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPathEnrollment' AND column_name = 'dtExpiresFromFirstLaunch')
	ALTER TABLE tblLearningPathEnrollment ADD [dtExpiresFromFirstLaunch] DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPathEnrollment' AND column_name = 'dtFirstLaunch')
	ALTER TABLE tblLearningPathEnrollment ADD [dtFirstLaunch] DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPathEnrollment' AND column_name = 'expiresFromStartInterval')
	ALTER TABLE tblLearningPathEnrollment ADD [expiresFromStartInterval] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPathEnrollment' AND column_name = 'expiresFromStartTimeframe')
	ALTER TABLE tblLearningPathEnrollment ADD [expiresFromStartTimeframe] NVARCHAR(4)	 NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPathEnrollment' AND column_name = 'expiresFromFirstLaunchInterval')
	ALTER TABLE tblLearningPathEnrollment ADD [expiresFromFirstLaunchInterval] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPathEnrollment' AND column_name = 'expiresFromFirstLaunchTimeframe')
	ALTER TABLE tblLearningPathEnrollment ADD [expiresFromFirstLaunchTimeframe] NVARCHAR(4)	 NULL
GO

/*

ADDITIONS TO [tblTransactionItem]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblTransactionItem' AND column_name = 'idIltSession')
	ALTER TABLE tblTransactionItem ADD [idIltSession] INT NULL
GO

/*

ADDITIONS TO [tblUser]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'optOutOfEmailNotifications')
	ALTER TABLE tblUser ADD [optOutOfEmailNotifications] BIT NULL
GO

/*

ADDITIONS TO [tblStandupTraining]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTraining' AND column_name = 'dtCreated')
	ALTER TABLE tblStandupTraining ADD [dtCreated] DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTraining' AND column_name = 'dtModified')
	ALTER TABLE tblStandupTraining ADD [dtModified] DATETIME NULL
GO

-- UPDATE dtCreated AND dtModified TO EARLIEST SESSION START DATETIME WHERE NULL, JUST TO SEED IT
CREATE TABLE #StandupTrainingCreatedDates (
	idStandupTraining	INT,
	dtCreated			DATETIME
)

INSERT INTO #StandupTrainingCreatedDates (
	idStandupTraining,
	dtCreated
)
SELECT 
	STI.idStandupTraining, 
	CASE WHEN MIN(STIMT.dtStart) IS NOT NULL THEN MIN(STIMT.dtStart) ELSE GETUTCDATE() END
FROM tblStandUpTraining ST
LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandUpTraining = ST.idStandUpTraining
LEFT JOIN tblStandUpTrainingInstanceMeetingTime STIMT ON STIMT.idStandUpTrainingInstance = STI.idStandUpTrainingInstance
WHERE ST.dtCreated IS NULL AND ST.dtModified IS NULL
GROUP BY STI.idStandUpTraining

UPDATE tblStandUpTraining SET
	dtCreated = #StandupTrainingCreatedDates.dtCreated,
	dtModified = #StandupTrainingCreatedDates.dtCreated
FROM #StandupTrainingCreatedDates
WHERE #StandupTrainingCreatedDates.idStandupTraining = tblStandUpTraining.idStandUpTraining

DROP TABLE #StandupTrainingCreatedDates

/*

ADDITIONS TO [tblLesson]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLesson' AND column_name = 'isOptional')
	ALTER TABLE tblLesson ADD [isOptional] BIT NULL
GO