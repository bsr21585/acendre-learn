/*

ADDITIONS TO [tblEventType]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEventType' AND column_name = 'relativeOrder')
	ALTER TABLE [tblEventType] ADD [relativeOrder] INT NULL
GO

-- UPDATE EVENT TYPES FOR "RELATIVE ORDERING"

--USER (100)
UPDATE tblEventType SET relativeOrder = 101 WHERE idEventType = 101
UPDATE tblEventType SET relativeOrder = 102 WHERE idEventType = 102
UPDATE tblEventType SET relativeOrder = 103 WHERE idEventType = 103
UPDATE tblEventType SET relativeOrder = 104 WHERE idEventType = 104
UPDATE tblEventType SET relativeOrder = 105 WHERE idEventType = 105

--ENROLLMENT (200)
UPDATE tblEventType SET relativeOrder = 201 WHERE idEventType = 201
UPDATE tblEventType SET relativeOrder = 205 WHERE idEventType = 202
UPDATE tblEventType SET relativeOrder = 203 WHERE idEventType = 203
UPDATE tblEventType SET relativeOrder = 204 WHERE idEventType = 204
UPDATE tblEventType SET relativeOrder = 206 WHERE idEventType = 205
UPDATE tblEventType SET relativeOrder = 202 WHERE idEventType = 206
UPDATE tblEventType SET relativeOrder = 207 WHERE idEventType = 207
UPDATE tblEventType SET relativeOrder = 208 WHERE idEventType = 208

--LESSON (300)
UPDATE tblEventType SET relativeOrder = 301 WHERE idEventType = 301
UPDATE tblEventType SET relativeOrder = 302 WHERE idEventType = 302
UPDATE tblEventType SET relativeOrder = 303 WHERE idEventType = 303
UPDATE tblEventType SET relativeOrder = 304 WHERE idEventType = 304
UPDATE tblEventType SET relativeOrder = 305 WHERE idEventType = 305

--SESSION (400)
UPDATE tblEventType SET relativeOrder = 401 WHERE idEventType = 401
UPDATE tblEventType SET relativeOrder = 402 WHERE idEventType = 402
UPDATE tblEventType SET relativeOrder = 403 WHERE idEventType = 403
UPDATE tblEventType SET relativeOrder = 404 WHERE idEventType = 404
UPDATE tblEventType SET relativeOrder = 405 WHERE idEventType = 405
UPDATE tblEventType SET relativeOrder = 406 WHERE idEventType = 406

--LEARNING PATH (500)
UPDATE tblEventType SET relativeOrder = 501 WHERE idEventType = 501
UPDATE tblEventType SET relativeOrder = 505 WHERE idEventType = 502
UPDATE tblEventType SET relativeOrder = 503 WHERE idEventType = 503
UPDATE tblEventType SET relativeOrder = 504 WHERE idEventType = 504
UPDATE tblEventType SET relativeOrder = 506 WHERE idEventType = 505
UPDATE tblEventType SET relativeOrder = 502 WHERE idEventType = 506

--CERTIFICATION (600)
UPDATE tblEventType SET name = 'Certification Awarded' WHERE idEventType = 604
UPDATE tblEventType SET name = 'Certification Renewed' WHERE idEventType = 605

UPDATE tblEventType SET relativeOrder = 601 WHERE idEventType = 601
UPDATE tblEventType SET relativeOrder = 605 WHERE idEventType = 602
UPDATE tblEventType SET relativeOrder = 602 WHERE idEventType = 603
UPDATE tblEventType SET relativeOrder = 603 WHERE idEventType = 604
UPDATE tblEventType SET relativeOrder = 604 WHERE idEventType = 605
UPDATE tblEventType SET relativeOrder = 606 WHERE idEventType = 606

--CERTIFICATE (700)
UPDATE tblEventType SET relativeOrder = 701 WHERE idEventType = 701
UPDATE tblEventType SET relativeOrder = 702 WHERE idEventType = 702
UPDATE tblEventType SET relativeOrder = 703 WHERE idEventType = 703
UPDATE tblEventType SET relativeOrder = 704 WHERE idEventType = 704

/*

ADDITIONS TO [tblStandupTrainingInstance]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstance' AND column_name = 'integratedObjectKey')
	ALTER TABLE [tblStandupTrainingInstance] ADD [integratedObjectKey] BIGINT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstance' AND column_name = 'hostUrl')
	ALTER TABLE [tblStandupTrainingInstance] ADD [hostUrl] NVARCHAR(1024) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstance' AND column_name = 'genericJoinUrl')
	ALTER TABLE [tblStandupTrainingInstance] ADD [genericJoinUrl] NVARCHAR(1024) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstance' AND column_name = 'meetingPassword')
	ALTER TABLE [tblStandupTrainingInstance] ADD [meetingPassword] NVARCHAR(255) NULL
GO

/*

ADDITIONS TO [tblStandupTrainingInstanceToUserLink]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstanceToUserLink' AND column_name = 'registrantKey')
	ALTER TABLE [tblStandupTrainingInstanceToUserLink] ADD [registrantKey] BIGINT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstanceToUserLink' AND column_name = 'registrantEmail')
	ALTER TABLE [tblStandupTrainingInstanceToUserLink] ADD [registrantEmail] NVARCHAR(255) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstanceToUserLink' AND column_name = 'joinUrl')
	ALTER TABLE [tblStandupTrainingInstanceToUserLink] ADD [joinUrl] NVARCHAR(1024) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstanceToUserLink' AND column_name = 'waitlistOrder')
	ALTER TABLE [tblStandupTrainingInstanceToUserLink] ADD [waitlistOrder] INT NULL
GO

/*

ADDITIONS TO [tblUser]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'isRegistrationApproved')
	ALTER TABLE [tblUser] ADD [isRegistrationApproved] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'dtApproved')
	ALTER TABLE [tblUser] ADD [dtApproved] DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'dtDenied')
	ALTER TABLE [tblUser] ADD [dtDenied] DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'idApprover')
	ALTER TABLE [tblUser] ADD [idApprover] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'rejectionComments')
	ALTER TABLE [tblUser] ADD [rejectionComments] NVARCHAR(MAX) NULL
GO

/*

ADDITIONS TO [tblCourse]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'isSelfEnrollmentApprovalRequired')
	ALTER TABLE [tblCourse] ADD isSelfEnrollmentApprovalRequired BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'isApprovalAllowedByCourseExperts')
	ALTER TABLE [tblCourse] ADD isApprovalAllowedByCourseExperts BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'isApprovalAllowedByUserSupervisor')
	ALTER TABLE [tblCourse] ADD isApprovalAllowedByUserSupervisor BIT NULL
GO

/*

ADDITIONS TO [tblEnrollmentRequest]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEnrollmentRequest' AND column_name = 'rejectionComments')
	ALTER TABLE tblEnrollmentRequest ADD rejectionComments NVARCHAR(MAX) NULL
GO

/*

ADDITIONS TO [tblCertification]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertification' AND column_name = 'isRenewalAnyModule')
	ALTER TABLE tblCertification ADD isRenewalAnyModule BIT NULL
GO

/*

ADDITIONS TO [tblCertificationModule]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertificationModule' AND column_name = 'isInitialRequirement')
	ALTER TABLE tblCertificationModule ADD isInitialRequirement BIT NULL
GO

/*

ADDITIONS TO [tblData-CertificationModuleRequirement]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblData-CertificationModuleRequirement' AND column_name = 'dtSubmitted')
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD dtSubmitted DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblData-CertificationModuleRequirement' AND column_name = 'isApproved')
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD isApproved BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblData-CertificationModuleRequirement' AND column_name = 'dtApproved')
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD dtApproved DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblData-CertificationModuleRequirement' AND column_name = 'idApprover')
	ALTER TABLE [tblData-CertificationModuleRequirement] ADD idApprover INT NULL
GO

/*

ADDITIONS TO [tblCertificationToUserLink]

*/
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertificationToUserLink' AND column_name = 'lastExpirationDate')
	ALTER TABLE [tblCertificationToUserLink] ADD lastExpirationDate DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertificationToUserLink' AND column_name = 'idRuleSet')
	ALTER TABLE [tblCertificationToUserLink] ADD idRuleSet INT NULL
GO

/*

REMOVALS FROM [tblCertification]

*/

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertification' AND column_name = 'idCertificationType')
	BEGIN
	ALTER TABLE [tblCertification] DROP CONSTRAINT [FK_Certification_Type]
	ALTER TABLE [tblCertification] DROP COLUMN [idCertificationType]
	END
GO

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertification' AND column_name = 'idCertificationContact')
	BEGIN
	ALTER TABLE [tblCertification] DROP CONSTRAINT [FK_Certification_Contact]
	ALTER TABLE [tblCertification] DROP COLUMN [idCertificationContact]
	END
GO

/*

REMOVALS FROM [tblData-CertificationModuleRequirement]

*/

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblData-CertificationModuleRequirement' AND column_name = 'revcode')
	BEGIN	
	ALTER TABLE [tblData-CertificationModuleRequirement] DROP COLUMN [revcode]
	END
GO

/*

TABLE DROPS

*/

-- DROP [tblCertificationType] - IT IS NOT USED

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[tblCertificationType]') and OBJECTPROPERTY(1947414257, N'IsTable') = 1)
	DROP TABLE [tblCertificationType]
GO

/*

PROCEDURE DROPS

*/

-- DROP [Course.RemoveApprovers] - IT IS NOT USED

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Course.RemoveApprovers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.RemoveApprovers]
GO

-- DROP [Course.RemoveEnrollmentApprovers] - IT IS NOT USED

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Course.RemoveEnrollmentApprovers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.RemoveEnrollmentApprovers]
GO

-- DROP [User.RemovePermissions] - IT IS NOT USED

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[User.RemovePermissions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.RemovePermissions]
GO