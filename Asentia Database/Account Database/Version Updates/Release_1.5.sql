/*

ADDITIONS TO [tblContentPackage]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'isMediaUpload')
	ALTER TABLE [tblContentPackage] ADD [isMediaUpload] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'idMediaType')
	ALTER TABLE [tblContentPackage] ADD [idMediaType] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'contentTitle')
	ALTER TABLE [tblContentPackage] ADD [contentTitle] NVARCHAR(255) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'originalMediaFilename')
	ALTER TABLE [tblContentPackage] ADD [originalMediaFilename] NVARCHAR(255) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'isVideoMedia3rdParty')
	ALTER TABLE [tblContentPackage] ADD [isVideoMedia3rdParty] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'videoMediaEmbedCode')
	ALTER TABLE [tblContentPackage] ADD [videoMediaEmbedCode] NVARCHAR(MAX) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'enableAutoplay')
	ALTER TABLE [tblContentPackage] ADD [enableAutoplay] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'allowRewind')
	ALTER TABLE [tblContentPackage] ADD [allowRewind] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'allowFastForward')
	ALTER TABLE [tblContentPackage] ADD [allowFastForward] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'allowNavigation')
	ALTER TABLE [tblContentPackage] ADD [allowNavigation] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'allowResume')
	ALTER TABLE [tblContentPackage] ADD [allowResume] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'minProgressForCompletion')
	ALTER TABLE [tblContentPackage] ADD [minProgressForCompletion] FLOAT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'isProcessing')
	ALTER TABLE [tblContentPackage] ADD [isProcessing] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'isProcessed')
	ALTER TABLE [tblContentPackage] ADD [isProcessed] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'dtProcessed')
	ALTER TABLE [tblContentPackage] ADD [dtProcessed] DATETIME NULL
GO

-- FK for MediaType
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_MediaType]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_MediaType] FOREIGN KEY (idMediaType) REFERENCES [tblMediaType] (idMediaType)
GO

/*

ADDITIONS TO [tblCourse]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'selfEnrollmentDueInterval')
	ALTER TABLE [tblCourse] ADD [selfEnrollmentDueInterval] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'selfEnrollmentDueTimeframe')
	ALTER TABLE [tblCourse] ADD [selfEnrollmentDueTimeframe] NVARCHAR(4) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'selfEnrollmentExpiresFromStartInterval')
	ALTER TABLE [tblCourse] ADD [selfEnrollmentExpiresFromStartInterval] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'selfEnrollmentExpiresFromStartTimeframe')
	ALTER TABLE [tblCourse] ADD [selfEnrollmentExpiresFromStartTimeframe] NVARCHAR(4) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'selfEnrollmentExpiresFromFirstLaunchInterval')
	ALTER TABLE [tblCourse] ADD [selfEnrollmentExpiresFromFirstLaunchInterval] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'selfEnrollmentExpiresFromFirstLaunchTimeframe')
	ALTER TABLE [tblCourse] ADD [selfEnrollmentExpiresFromFirstLaunchTimeframe] NVARCHAR(4) NULL
GO

/*

ADDITIONS TO [tblData-Lesson]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblData-Lesson' AND column_name = 'resetForContentChange')
	ALTER TABLE [tblData-Lesson] ADD [resetForContentChange] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblData-Lesson' AND column_name = 'preventPostCompletionLaunchForContentChange')
	ALTER TABLE [tblData-Lesson] ADD [preventPostCompletionLaunchForContentChange] BIT NULL
GO