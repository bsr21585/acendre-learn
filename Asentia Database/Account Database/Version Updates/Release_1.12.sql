/*

ADDITIONS TO [tblStandupTraining]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTraining' AND column_name = 'objectives')
	ALTER TABLE [tblStandupTraining] ADD [objectives] NVARCHAR(MAX) NULL
GO

/*

ADDITIONS TO [tblStandupTrainingLanguage]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingLanguage' AND column_name = 'objectives')
	ALTER TABLE [tblStandupTrainingLanguage] ADD [objectives] NVARCHAR(MAX) NULL
GO

/*

ALTER [description] FIELD OF tblStandupTrainingInstance AND tblStandupTrainingInstanceLanguage TO NVARCHAR(MAX)

*/

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstance' AND column_name = 'description' AND CHARACTER_MAXIMUM_LENGTH = 512)
	EXEC sp_fulltext_column 'tblStandupTrainingInstance', 'description', 'drop'
	ALTER TABLE [tblStandupTrainingInstance] ALTER COLUMN [description] NVARCHAR(MAX)
	EXEC sp_fulltext_column 'tblStandupTrainingInstance', 'description', 'add'
GO

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstanceLanguage' AND column_name = 'description' AND CHARACTER_MAXIMUM_LENGTH = 512)
	EXEC sp_fulltext_column 'tblStandupTrainingInstanceLanguage', 'description', 'drop'
	ALTER TABLE [tblStandupTrainingInstanceLanguage] ALTER COLUMN [description] NVARCHAR(MAX)
	EXEC sp_fulltext_column 'tblStandupTrainingInstanceLanguage', 'description', 'add'
GO

/*

ADDITIONS TO [tblUser]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'dtMarkedDisabled')
	ALTER TABLE [tblUser] ADD [dtMarkedDisabled] DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'disableLoginFromLoginForm')
	ALTER TABLE [tblUser] ADD [disableLoginFromLoginForm] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'exemptFromIPLoginRestriction')
	ALTER TABLE [tblUser] ADD [exemptFromIPLoginRestriction] BIT NULL
GO

/*

ADDITIONS TO [tblSite]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblSite' AND column_name = 'dtLicenseAgreementExecuted')
	ALTER TABLE [tblSite] ADD [dtLicenseAgreementExecuted] DATETIME NULL
GO

/*

ADDITIONS TO [tblContentPackage]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'hasManifestComplianceErrors')
	ALTER TABLE [tblContentPackage] ADD [hasManifestComplianceErrors] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'manifestComplianceErrors')
	ALTER TABLE [tblContentPackage] ADD [manifestComplianceErrors] NVARCHAR(MAX) NULL
GO