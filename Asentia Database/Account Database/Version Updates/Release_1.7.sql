/* DELETE THE [User.DeleteByGroup] PROCEDURE SINCE THE DELETE USERS BY GROUP FUNCTIONALITY HAS BEEN REMOVED */
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.DeleteByGroup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.DeleteByGroup]
GO

/*

ADDITIONS TO [tblContentPackage]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'idQuizSurvey')
	ALTER TABLE [tblContentPackage] ADD [idQuizSurvey] INT NULL
GO

-- FK for QuizSurvey
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_ContentPackage_IdQuizSurvey]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE [tblContentPackage] ADD CONSTRAINT [FK_ContentPackage_IdQuizSurvey] FOREIGN KEY (idQuizSurvey) REFERENCES [tblQuizSurvey] (idQuizSurvey)
GO