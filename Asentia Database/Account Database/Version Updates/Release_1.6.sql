/*

ADDITIONS TO [tblDataset]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblDataset' AND column_name = 'order')
	ALTER TABLE [tblDataset] ADD [order] INT
GO

UPDATE tblDataset SET name = 'User Course Transcripts' WHERE idDataset = 2

UPDATE tblDataset SET [order] = 1 WHERE idDataset = 1
UPDATE tblDataset SET [order] = 2 WHERE idDataset = 2
UPDATE tblDataset SET [order] = 5 WHERE idDataset = 3
UPDATE tblDataset SET [order] = 6 WHERE idDataset = 4
UPDATE tblDataset SET [order] = 7 WHERE idDataset = 5
UPDATE tblDataset SET [order] = 3 WHERE idDataset = 6
UPDATE tblDataset SET [order] = 4 WHERE idDataset = 7

/*

CHANGE THE NAME OF THE "UserTranscript" REPORTING PERMISSION, ITS NOW "UserCourseTranscript"

*/

UPDATE tblPermission SET name = 'Reporting_Reporter_UserCourseTranscripts' WHERE idPermission = 402


/*

DROP THE [Dataset.UserTranscripts] PROCEDURE SINCE IT HAS BEEN RENAMED

*/

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Dataset.UserTranscripts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Dataset.UserTranscripts]
GO


/*

ADDITIONS TO FULL TEXT INDEXES

*/

/* tblCatalog */

-- searchTags column
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCatalog' AND column_name = 'searchTags')
	ALTER TABLE [tblCatalog] ADD [searchTags] NVARCHAR(512)
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCatalogLanguage' AND column_name = 'searchTags')
	ALTER TABLE [tblCatalogLanguage] ADD [searchTags] NVARCHAR(512)
GO

-- searchTags fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalog]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalog]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblCatalog]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblCatalog ADD (searchTags)
	END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCatalogLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblCatalogLanguage]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblCatalogLanguage ADD (searchTags)
	END

/* tblCourse */

-- searchTags column
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'searchTags')
	ALTER TABLE [tblCourse] ADD [searchTags] NVARCHAR(512)
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourseLanguage' AND column_name = 'searchTags')
	ALTER TABLE [tblCourseLanguage] ADD [searchTags] NVARCHAR(512)
GO

-- searchTags fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourse]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourse]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblCourse]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblCourse ADD (searchTags)
	END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblCourseLanguage]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblCourseLanguage ADD (searchTags)
	END

-- shortDescription fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblCourseLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblCourseLanguage]') AND C.name = N'shortDescription')
	BEGIN
	ALTER FULLTEXT INDEX ON tblCourseLanguage ADD (shortDescription)
	END

/* tblDocumentRepositoryItem */

-- searchTags column
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblDocumentRepositoryItemLanguage' AND column_name = 'searchTags')
	ALTER TABLE [tblDocumentRepositoryItemLanguage] ADD [searchTags] NVARCHAR(512)
GO

-- searchTags fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItemLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItemLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblDocumentRepositoryItemLanguage]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblDocumentRepositoryItemLanguage ADD (searchTags)
	END

-- label fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItem]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDocumentRepositoryItem]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblDocumentRepositoryItem]') AND C.name = N'label')
	BEGIN
	ALTER FULLTEXT INDEX ON tblDocumentRepositoryItem ADD (label)
	END

/* tblGroup */

-- searchTags column
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblGroup' AND column_name = 'searchTags')
	ALTER TABLE [tblGroup] ADD [searchTags] NVARCHAR(512)
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblGroupLanguage' AND column_name = 'searchTags')
	ALTER TABLE [tblGroupLanguage] ADD [searchTags] NVARCHAR(512)
GO

-- searchTags fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroup]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroup]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblGroup]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblGroup ADD (searchTags)
	END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblGroupLanguage]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblGroupLanguage ADD (searchTags)
	END

-- shortDescription fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroup]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroup]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblGroup]') AND C.name = N'shortDescription')
	BEGIN
	ALTER FULLTEXT INDEX ON tblGroup ADD (shortDescription)
	END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblGroupLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblGroupLanguage]') AND C.name = N'shortDescription')
	BEGIN
	ALTER FULLTEXT INDEX ON tblGroupLanguage ADD (shortDescription)
	END

/* tblLearningPath */

-- searchTags column
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPath' AND column_name = 'searchTags')
	ALTER TABLE [tblLearningPath] ADD [searchTags] NVARCHAR(512)
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPathLanguage' AND column_name = 'searchTags')
	ALTER TABLE [tblLearningPathLanguage] ADD [searchTags] NVARCHAR(512)
GO

-- searchTags fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblLearningPath]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblLearningPath ADD (searchTags)
	END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPathLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblLearningPathLanguage]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblLearningPathLanguage ADD (searchTags)
	END

-- shortDescription fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLearningPath]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblLearningPath]') AND C.name = N'shortDescription')
	BEGIN
	ALTER FULLTEXT INDEX ON tblLearningPath ADD (shortDescription)
	END

/* tblLesson */

-- searchTags column
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLesson' AND column_name = 'searchTags')
	ALTER TABLE [tblLesson] ADD [searchTags] NVARCHAR(512)
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLessonLanguage' AND column_name = 'searchTags')
	ALTER TABLE [tblLessonLanguage] ADD [searchTags] NVARCHAR(512)
GO

-- searchTags fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLesson]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLesson]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblLesson]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblLesson ADD (searchTags)
	END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLessonLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLessonLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblLessonLanguage]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblLessonLanguage ADD (searchTags)
	END

-- shortDescription fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLesson]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblLesson]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblLesson]') AND C.name = N'shortDescription')
	BEGIN
	ALTER FULLTEXT INDEX ON tblLesson ADD (shortDescription)
	END

/* tblStandupTraining */

-- searchTags column
IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTraining' AND column_name = 'searchTags')
	ALTER TABLE [tblStandupTraining] ADD [searchTags] NVARCHAR(512)
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingLanguage' AND column_name = 'searchTags')
	ALTER TABLE [tblStandupTrainingLanguage] ADD [searchTags] NVARCHAR(512)
GO

-- searchTags fulltext index
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandupTraining]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandupTraining]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblStandupTraining]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblStandupTraining ADD (searchTags)
	END

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandupTrainingLanguage]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblStandupTrainingLanguage]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
AND NOT EXISTS (SELECT * FROM sys.fulltext_index_columns FIC 
				INNER JOIN sys.columns C ON C.[object_id] = FIC.[object_id] AND C.[column_id] = FIC.column_id 
				WHERE FIC.[object_id] = object_id(N'[tblStandupTrainingLanguage]') AND C.name = N'searchTags')
	BEGIN
	ALTER FULLTEXT INDEX ON tblStandupTrainingLanguage ADD (searchTags)
	END