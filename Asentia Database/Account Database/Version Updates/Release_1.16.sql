/*

ENSURE THAT ALL SITE-SPECIFIC REFERENCES TO PORTAL STATISTICS WIDGET ARE REMOVED
THE DEFAULT (ID SITE 1) IS TAKEN CARE OF IN ITS TABLE SCRIPT

*/

DELETE FROM tblSiteParam WHERE [param] = 'Widget.Administrator.PortalStatistics.Enabled' AND idSite > 1

/*

ADDITIONS TO [tblCourse]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'selfEnrollmentIsOneTimeOnly')
	ALTER TABLE tblCourse ADD [selfEnrollmentIsOneTimeOnly] BIT NULL
GO


/*

ADDITIONS TO [tblLearningPath]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPath' AND column_name = 'selfEnrollmentIsOneTimeOnly')
	ALTER TABLE tblLearningPath ADD [selfEnrollmentIsOneTimeOnly] BIT NULL
GO

/*

ADDITIONS TO [tblStandUpTrainingInstance]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTrainingInstance' AND column_name = 'isClosed')
	ALTER TABLE tblStandUpTrainingInstance ADD [isClosed] BIT NULL
GO


/*

ADDITIONS TO [tblCatalog]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCatalog' AND column_name = 'shortcode')
	ALTER TABLE tblCatalog ADD [shortcode] NVARCHAR(10) NULL
GO

/*

ADDITIONS TO [tblSite]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblSite' AND column_name = 'favicon')
	ALTER TABLE tblSite ADD [favicon] NVARCHAR(255) NULL
GO

/*

ADDITIONS TO [tblRuleSetEnrollment]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblRuleSetEnrollment' AND column_name = 'idParentRuleSetEnrollment')
	ALTER TABLE tblRuleSetEnrollment ADD idParentRuleSetEnrollment INT NULL
GO

/*

ADDITIONS TO [tblRuleSetLearningPathEnrollment]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblRuleSetLearningPathEnrollment' AND column_name = 'idParentRuleSetLearningPathEnrollment')
	ALTER TABLE tblRuleSetLearningPathEnrollment ADD idParentRuleSetLearningPathEnrollment INT NULL
GO

/*

MODIFY [tblStandupTrainingInstance] Column [locationDescription]

*/

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstance' AND column_name = 'locationDescription' AND CHARACTER_MAXIMUM_LENGTH = 512)
	EXEC sp_fulltext_column 'tblStandupTrainingInstance', 'locationDescription', 'drop'
	ALTER TABLE [tblStandupTrainingInstance] ALTER COLUMN [locationDescription] NVARCHAR(MAX)
	EXEC sp_fulltext_column 'tblStandupTrainingInstance', 'locationDescription', 'add'
GO

/*

MODIFY [tblStandUpTrainingInstanceLanguage] Column [locationDescription]

*/

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTrainingInstanceLanguage' AND column_name = 'locationDescription' AND CHARACTER_MAXIMUM_LENGTH = 512)
	EXEC sp_fulltext_column 'tblStandUpTrainingInstanceLanguage', 'locationDescription', 'drop'
	ALTER TABLE [tblStandUpTrainingInstanceLanguage] ALTER COLUMN [locationDescription] NVARCHAR(MAX)
	EXEC sp_fulltext_column 'tblStandUpTrainingInstanceLanguage', 'locationDescription', 'add'
GO

/*

ADDITIONS TO [tblUser]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'excludeFromLeaderboards')
	ALTER TABLE tblUser ADD [excludeFromLeaderboards] BIT NULL
GO

/*

ADDITIONS TO [tblStandUpTrainingInstance]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTrainingInstance' AND column_name = 'postalcode')
	ALTER TABLE tblStandUpTrainingInstance ADD [postalcode] NVARCHAR(25) NULL
GO