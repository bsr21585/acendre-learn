/*

RE-ORDER [tblDataset]

*/

UPDATE tblDataset SET [storedProcedureName] = '[Dataset.UserDemographics]', [order] = 1 WHERE idDataset = 1
UPDATE tblDataset SET [storedProcedureName] = '[Dataset.UserCourseTranscripts]', [order] = 2 WHERE idDataset = 2
UPDATE tblDataset SET [storedProcedureName] = '[Dataset.CatalogCourseInformation]', [order] = 6 WHERE idDataset = 3
UPDATE tblDataset SET [storedProcedureName] = '[Dataset.Certificates]', [order] = 7 WHERE idDataset = 4
UPDATE tblDataset SET [storedProcedureName] = '[Dataset.xAPI]', [order] = 8 WHERE idDataset = 5
UPDATE tblDataset SET [storedProcedureName] = '[Dataset.UserLearningPathTranscripts]', [order] = 3 WHERE idDataset = 6
UPDATE tblDataset SET [storedProcedureName] = '[Dataset.UserInstructorLedTranscripts]', [order] = 4 WHERE idDataset = 7
UPDATE tblDataset SET [storedProcedureName] = '[Dataset.Purchases]', [order] = 9 WHERE idDataset = 8
UPDATE tblDataset SET [storedProcedureName] = '[Dataset.UserCertificationTranscripts]', [order] = 5 WHERE idDataset = 9

/*

ADDITIONS TO [tblStandupTrainingInstance]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstance' AND column_name = 'idWebMeetingOrganizer')
	ALTER TABLE [tblStandupTrainingInstance] ADD [idWebMeetingOrganizer] INT NULL
GO

/*

ADDITIONS TO [tblDocumentRepositoryItem]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblDocumentRepositoryItem' AND column_name = 'isVisibleToUser')
	ALTER TABLE [tblDocumentRepositoryItem] ADD [isVisibleToUser] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblDocumentRepositoryItem' AND column_name = 'isUploadedByUser')
	ALTER TABLE [tblDocumentRepositoryItem] ADD [isUploadedByUser] BIT NULL
GO

/*

PROCEDURE DROPS

*/

-- DROP [User.CheckAvailabilityForSession] - NAME CHANGED TO [StandupTrainingInstance.CheckInstructorAvailability]

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[User.CheckAvailabilityForSession]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.CheckAvailabilityForSession]
GO

/*

ALTER [description] FIELD OF tblStandupTraining AND tblStandupTrainingLanguage TO NVARCHAR(MAX)

*/

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTraining' AND column_name = 'description' AND CHARACTER_MAXIMUM_LENGTH = 512)
	EXEC sp_fulltext_column 'tblStandupTraining', 'description', 'drop'
	ALTER TABLE [tblStandupTraining] ALTER COLUMN [description] NVARCHAR(MAX)
	EXEC sp_fulltext_column 'tblStandupTraining', 'description', 'add'
GO

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingLanguage' AND column_name = 'description' AND CHARACTER_MAXIMUM_LENGTH = 512)
	EXEC sp_fulltext_column 'tblStandupTrainingLanguage', 'description', 'drop'
	ALTER TABLE [tblStandupTrainingLanguage] ALTER COLUMN [description] NVARCHAR(MAX)
	EXEC sp_fulltext_column 'tblStandupTrainingLanguage', 'description', 'add'
GO

/*

ALTER [shortDescription] FIELD OF tblGroupLanguage TO MAKE IT NULLABLE

*/

ALTER TABLE tblGroupLanguage ALTER COLUMN shortDescription NVARCHAR(512) NULL

/*

ALTER [identifier], [timestamp], [latency] FIELDS OF [tblData-SCOInt] TO MAKE THEM NULLABLE

*/

ALTER TABLE [tblData-SCOInt] ALTER COLUMN identifier NVARCHAR(255) NULL
ALTER TABLE [tblData-SCOInt] ALTER COLUMN [timestamp] DATETIME NULL
ALTER TABLE [tblData-SCOInt] ALTER COLUMN result INT NULL
ALTER TABLE [tblData-SCOInt] ALTER COLUMN latency FLOAT NULL