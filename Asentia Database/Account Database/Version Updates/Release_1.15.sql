/*

ADDITIONS TO [tblEventEmailNotification]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEventEmailNotification' AND column_name = 'attachmentType')
	ALTER TABLE tblEventEmailNotification ADD [attachmentType] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEventEmailNotification' AND column_name = 'copyTo')
	ALTER TABLE tblEventEmailNotification ADD [copyTo] NVARCHAR(255) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEventEmailNotification' AND column_name = 'specificEmailAddress')
	ALTER TABLE tblEventEmailNotification ADD [specificEmailAddress] NVARCHAR(255) NULL
GO

/*

ADDITIONS TO [tblEventEmailQueue]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEventEmailQueue' AND column_name = 'attachmentType')
	ALTER TABLE tblEventEmailQueue ADD [attachmentType] INT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEventEmailQueue' AND column_name = 'copyTo')
	ALTER TABLE tblEventEmailQueue ADD [copyTo] NVARCHAR(255) NULL
GO

/*

UPDATES TO [tblPermission]

*/

UPDATE tblPermission SET name = 'LearningAssets_CourseContentManager' WHERE idPermission = 302
UPDATE tblPermission SET name = 'LearningAssets_LearningPathContentManager' WHERE idPermission = 303

/*

UPDATES TO [tblEventType]

*/

UPDATE tblEventType SET allowPostSend = 1 WHERE idEventType = 401 -- allow post send for "Session Meets"

/*

ADDITIONS TO [tblCatalog]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCatalog' AND column_name = 'avatar')
	ALTER TABLE tblCatalog ADD [avatar] NVARCHAR(255) NULL
GO

/*

ADDITIONS TO [tblUser]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblUser' AND column_name = 'dtUserAgreementAgreed')
	ALTER TABLE tblUser ADD [dtUserAgreementAgreed] DATETIME NULL
GO

/*

ADDITIONS TO [tblSite]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblSite' AND column_name = 'dtUserAgreementModified')
	ALTER TABLE tblSite ADD [dtUserAgreementModified] DATETIME NULL
GO

/*

ADDITIONS TO [tblCertificateLanguage]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCertificateLanguage' AND column_name = 'filename')
	ALTER TABLE tblCertificateLanguage ADD [filename] NVARCHAR(255) NULL
GO