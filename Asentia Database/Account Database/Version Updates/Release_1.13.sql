/*

ADDITIONS TO [tblLearningPathEnrollment]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPathEnrollment' AND column_name = 'dtLastSynchronized')
	ALTER TABLE [tblLearningPathEnrollment] ADD [dtLastSynchronized] DATETIME NULL
GO

/*

ADDITIONS TO [tblContentPackage]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblContentPackage' AND column_name = 'openSesameGUID')
	ALTER TABLE [tblContentPackage] ADD [openSesameGUID] NVARCHAR(36) NULL
GO

/*

ADDITIONS TO [tblEventEmailNotification]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblEventEmailNotification' AND column_name = 'isDeleted')
	ALTER TABLE [tblEventEmailNotification] ADD [isDeleted] BIT NULL
GO

/*

SHORTCODES ADDED TO [tblCourse], [tblLearningPath], [tblStandUpTraining]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblCourse' AND column_name = 'shortcode')
	ALTER TABLE [tblCourse] ADD [shortcode] NVARCHAR(10) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblLearningPath' AND column_name = 'shortcode')
	ALTER TABLE [tblLearningPath] ADD [shortcode] NVARCHAR(10) NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTraining' AND column_name = 'shortcode')
	ALTER TABLE [tblStandUpTraining] ADD [shortcode] NVARCHAR(10) NULL
GO

IF EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandupTrainingInstance' AND column_name = 'idInstructor')
	AND EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblStandUpTrainingInstanceToInstructorLink')
	BEGIN

	/*

	MOVE INSTRUCTOR INFORMATION

	*/

	INSERT INTO tblStandUpTrainingInstanceToInstructorLink (
		idSite,
		idStandUpTrainingInstance, 
		idInstructor
	)
	SELECT 
		idSite,
		idStandUpTrainingInstance, 
		idInstructor 
	FROM tblStandUpTrainingInstance	STI	
	WHERE NOT EXISTS (SELECT 1
					  FROM tblStandUpTrainingInstanceToInstructorLink STIL
					  WHERE STIL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance
					  AND STIL.idInstructor = STI.idInstructor)
	AND STI.idInstructor IS NOT NULL

	/*  

	REMOVALS FROM [tblStandupTrainingInstance]

	*/

	ALTER TABLE [tblStandupTrainingInstance] DROP CONSTRAINT [FK_StandupTrainingInstance_Instructor]
	ALTER TABLE [tblStandupTrainingInstance] DROP COLUMN [idInstructor]

	END
GO