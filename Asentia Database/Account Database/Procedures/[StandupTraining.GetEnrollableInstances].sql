-- =====================================================================
-- PROCEDURE: [StandupTraining.GetEnrollableInstances]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.GetEnrollableInstances]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.GetEnrollableInstances]
GO

/*

Returns a recordset of standup training instances that are available for a learner to join.

*/

CREATE PROCEDURE [StandupTraining.GetEnrollableInstances]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idStandupTraining		INT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	put all standup training instance's earliest start dates, and available seats in a temp table

	*/

	CREATE TABLE #StandupTrainingInstanceCalculatedValues (
		idStandUpTrainingInstance INT, 
		dtFirstStart DATETIME, 
		availableSeats INT, 
		availableWaitingseats INT
	)
	
	-- start times
	INSERT INTO #StandupTrainingInstanceCalculatedValues (
		idStandUpTrainingInstance,
		dtFirstStart
	) 
	SELECT 
		STIMT.idStandUpTrainingInstance,
		MIN(STIMT.dtStart)
	FROM tblStandUpTrainingInstanceMeetingTime STIMT
	LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandUpTrainingInstance = STIMT.idStandUpTrainingInstance
	WHERE STI.idStandUpTraining = @idStandupTraining
	GROUP BY STIMT.idStandUpTrainingInstance

	-- available seats
	UPDATE #StandupTrainingInstanceCalculatedValues SET
		availableSeats = (STI.seats - (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink STIUL WHERE STIUL.idStandupTrainingInstance = STI.idStandUpTrainingInstance AND STIUL.isWaitingList = 0)),
		availableWaitingSeats = (STI.waitingSeats - (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink STIUL WHERE STIUL.idStandupTrainingInstance = STI.idStandUpTrainingInstance AND STIUL.isWaitingList = 1))
	FROM tblStandUpTrainingInstance STI
	WHERE STI.idStandUpTrainingInstance = #StandupTrainingInstanceCalculatedValues.idStandUpTrainingInstance
	
	/*

	get the enrollable standup training instances

	*/

	SELECT DISTINCT
		STI.idStandupTrainingInstance,
		CASE WHEN STIL.title IS NOT NULL THEN STIL.title ELSE STI.title END AS title,
		STIMT.dtStart,
		STIMT.dtEnd,
		STIMT.idTimezone,
		STI.[type],
		STI.city,
		STI.province,
		STUFF((SELECT '|' + INS.displayName FROM tblStandupTrainingInstanceToInstructorLink STII LEFT JOIN tblUser INS ON INS.idUser = STII.idInstructor AND STII.idStandupTrainingInstance = STI.idStandUpTrainingInstance FOR XML PATH('')), 1, 1, '') AS instructorDisplayNames,
		STICV.availableSeats,
		STI.seats,
		STICV.availableWaitingSeats,
		STI.waitingSeats
	FROM tblStandUpTrainingInstance STI
	LEFT JOIN tblStandupTraining ST ON ST.idStandUpTraining = STI.idStandUpTraining
	LEFT JOIN tblStandUpTrainingInstanceLanguage STIL ON STIL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance AND STIL.idLanguage = @idCallerLanguage
	LEFT JOIN tblStandUpTrainingInstanceMeetingTime STIMT ON STIMT.idStandUpTrainingInstance = STI.idStandUpTrainingInstance
	LEFT JOIN #StandupTrainingInstanceCalculatedValues STICV ON STICV.idStandUpTrainingInstance = STI.idStandUpTrainingInstance AND STICV.dtFirstStart > GETUTCDATE()
	WHERE (ST.isRestrictedEnroll IS NULL OR ST.isRestrictedEnroll = 0)
	AND (STICV.availableSeats > 0 OR STICV.availableWaitingSeats > 0)
	AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)
	AND (STI.isClosed = 0 OR STI.isClosed IS NULL)
	ORDER BY STI.idStandupTrainingInstance ASC, STIMT.dtStart ASC

	/*

	drop the temp table

	*/

	DROP TABLE #StandupTrainingInstanceCalculatedValues

	/*

	return

	*/
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO