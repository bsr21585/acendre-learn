-- =====================================================================
-- PROCEDURE: [User.GetGroups]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.GetGroups]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.GetGroups]
GO

/*

Returns a recordset of group ids and names that a user is a member of.

*/

CREATE PROCEDURE [User.GetGroups]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idUser					INT,
	@excludeAutoJoined		BIT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString
		
	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		IF @excludeAutoJoined = 1

			BEGIN

			SELECT
				DISTINCT
				G.idGroup, 
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
				(SELECT COUNT(1) FROM tblUserToGroupLink RULES_UGL WHERE RULES_UGL.idUser = @idUser AND RULES_UGL.idGroup = G.idGroup AND RULES_UGL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblGroup G
			LEFT JOIN tblUserToGroupLink UGL ON UGL.idGroup = G.idGroup
			LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
			WHERE G.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblUserToGroupLink UGL
						WHERE UGL.idUser = @idUser
						AND UGL.idGroup = G.idGroup
						AND UGL.idRuleSet IS NULL)
			ORDER BY name

			END

		ELSE

			BEGIN

			SELECT
				DISTINCT
				G.idGroup, 
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
				(SELECT COUNT(1) FROM tblUserToGroupLink RULES_UGL WHERE RULES_UGL.idUser = @idUser AND RULES_UGL.idGroup = G.idGroup AND RULES_UGL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblGroup G
			LEFT JOIN tblUserToGroupLink UGL ON UGL.idGroup = G.idGroup
			LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
			WHERE G.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblUserToGroupLink UGL
						WHERE UGL.idUser = @idUser
						AND UGL.idGroup = G.idGroup)
			ORDER BY name

			END

		END

	ELSE

		BEGIN

		IF @excludeAutoJoined = 1

			BEGIN

			SELECT
				DISTINCT
				G.idGroup, 
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
				(SELECT COUNT(1) FROM tblUserToGroupLink RULES_UGL WHERE RULES_UGL.idUser = @idUser AND RULES_UGL.idGroup = G.idGroup AND RULES_UGL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblGroup G
			INNER JOIN CONTAINSTABLE(tblGroup, *, @searchParam) K ON K.[key] = G.idGroup
			LEFT JOIN tblUserToGroupLink UGL ON UGL.idGroup = G.idGroup
			LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
			WHERE G.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblUserToGroupLink UGL
						WHERE UGL.idUser = @idUser
						AND UGL.idGroup = G.idGroup
						AND UGL.idRuleSet IS NULL)
			ORDER BY name

			END

		ELSE

			BEGIN

			SELECT
				DISTINCT
				G.idGroup, 
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
				(SELECT COUNT(1) FROM tblUserToGroupLink RULES_UGL WHERE RULES_UGL.idUser = @idUser AND RULES_UGL.idGroup = G.idGroup AND RULES_UGL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblGroup G
			INNER JOIN CONTAINSTABLE(tblGroup, *, @searchParam) K ON K.[key] = G.idGroup
			LEFT JOIN tblUserToGroupLink UGL ON UGL.idGroup = G.idGroup
			LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
			WHERE G.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblUserToGroupLink UGL
						WHERE UGL.idUser = @idUser
						AND UGL.idGroup = G.idGroup)
			ORDER BY name

			END

		END
		
		SET @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	