-- =====================================================================
-- PROCEDURE: [Group.JoinSelfJoinedUser]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.JoinSelfJoinedUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.JoinSelfJoinedUser]
GO

CREATE PROCEDURE [Group.JoinSelfJoinedUser]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	--   default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, 
		
	@idGroup				INT,
	@idUser					INT
)
AS

	BEGIN
	
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	validate group exists
	
	*/

	IF (
		SELECT COUNT(1)
		FROM tblGroup
		WHERE idGroup = @idGroup
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'GroupJoinSelfJoinedUser_DetailsNotFound'
		RETURN 1
		END
		
	/*
	
	validate that the user id exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUser U 
		WHERE  U.idUser = @idUser
		AND U.idSite = @idCallerSite
		) = 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'GroupJoinSelfJoinedUser_UsersNotInSameSite'
		RETURN 1
		END
	
	/*
	
	attach the group to users where they do not already exist
	
	*/

	INSERT INTO tblUserToGroupLink (
		idSite,
		idGroup,
		idUser,
		created,
		selfJoined
	)
	SELECT 
		@idCallerSite,
		@idGroup, 
		@idUser,
		GETUTCDATE(),
		1	
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblUserToGroupLink UGL
		WHERE UGL.idGroup = @idGroup
		AND UGL.idUser = @idUser
		AND idRuleSet IS NULL -- manual join
	)

	SELECT @Return_Code = 0
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

	
