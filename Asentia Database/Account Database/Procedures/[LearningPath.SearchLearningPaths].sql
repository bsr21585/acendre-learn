-- =====================================================================
-- PROCEDURE: [LearningPath.SearchLearningPaths]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.SearchLearningPaths]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.SearchLearningPaths]
GO

/*

Return all the courses containg a given specific string in title.

*/
CREATE PROCEDURE [LearningPath.SearchLearningPaths]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@searchParam			NVARCHAR(30)	= ''
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	--IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
	--	BEGIN
	--	SET @Return_Code = 3 -- sort of (caller not member of specified site).
	--	SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
	--	RETURN 1
	--	END

	/*
	
	validate caller permission
	
	*/

	--IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
	--	BEGIN
	--	SET @Return_Code = 3
	--	SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
	--	RETURN 1
	--	END

	
	/*
	
	get the data 
	
	*/
	IF(@searchParam <> '')	
	BEGIN
		SELECT	LP.idLearningPath,
			CASE WHEN LPL.name IS NULL OR LPL.name = '' THEN LP.name ELSE LPL.name END AS name,
			LP.idSite,
			CASE WHEN LPL.shortDescription IS NULL OR LPL.shortDescription = '' THEN LP.shortDescription ELSE LPL.shortDescription END AS shortDescription,	
			CASE WHEN LPL.longDescription IS NULL OR LPL.longDescription = '' THEN LP.longDescription ELSE LPL.longDescription END AS longDescription	,			
			LP.cost
		FROM tblLearningPath LP
		LEFT JOIN tblLearningPathLanguage LPL ON LP.idLearningPath = LPL.idLearningPath
		WHERE LP.name LIKE '%'+ @searchParam +'%'		
		AND LP.isPublished = 1
		AND LP.dtDeleted IS NULL
		AND LP.idSite = @idCallerSite
	END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO