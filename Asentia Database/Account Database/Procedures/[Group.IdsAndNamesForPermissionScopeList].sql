-- =====================================================================
-- PROCEDURE: [Group.IdsAndNamesForPermissionScopeList]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.IdsAndNamesForPermissionScopeList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.IdsAndNamesForPermissionScopeList]
GO

/*

Gets a listing of all groups within a site for permission scoping.

*/

CREATE PROCEDURE [Group.IdsAndNamesForPermissionScopeList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, 
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
AS
	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString
			 
	/*

	get the data

	*/
		
	SELECT 
		G.idGroup, 
		CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name
	FROM tblGroup G
	LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
	WHERE G.idSite = @idCallerSite
	ORDER BY name
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END					

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO