-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.GetGrid]
GO

/*

Gets a listing of Standup Training Instances that belong to a Standup Training Module.

*/

CREATE PROCEDURE [StandupTrainingInstance.GetGrid]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@searchParam				NVARCHAR(4000),
	@pageNum					INT, 
	@pageSize					INT, 
	@orderColumn				NVARCHAR(255),
	@orderAsc					BIT,
	@idStandupTraining			INT	
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		put the standup training instances into a temp table with their start dates

		*/

		CREATE TABLE #StandupTrainingInstances (
			idStandupTrainingInstance	INT,
			title						NVARCHAR(255),
			seats						INT,
			seatsFilled					INT,
			waitingSeats				INT,
			waitingSeatsFilled			INT,
			[type]						INT,
			dtStart						DATETIME,
			idTimezone					INT
		)

		INSERT INTO #StandupTrainingInstances (
			idStandupTrainingInstance,
			title,
			seats,
			seatsFilled,
			waitingSeats,
			waitingSeatsFilled,
			[type],
			dtStart,
			idTimezone
		)
		SELECT
			STI.idStandupTrainingInstance,
			STI.title,
			STI.seats,
			(SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = STI.idStandUpTrainingInstance AND isWaitingList = 0),
			STI.waitingSeats,
			(SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = STI.idStandUpTrainingInstance AND isWaitingList = 1),
			STI.[type],
			(SELECT MIN(dtStart) FROM tblStandUpTrainingInstanceMeetingTime WHERE idStandUpTrainingInstance = STI.idStandUpTrainingInstance),
			(SELECT DISTINCT idTimezone FROM tblStandUpTrainingInstanceMeetingTime WHERE idStandUpTrainingInstance = STI.idStandUpTrainingInstance)
		FROM tblStandUpTrainingInstance STI
		WHERE 
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND STI.idSite = @idCallerSite)
			)
		AND STI.idStandUpTraining = @idStandupTraining
		AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #StandupTrainingInstances STI			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						STI.idStandupTrainingInstance,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC							
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN STI.dtStart END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'seats' THEN STI.seats END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'waitingSeats' THEN STI.waitingSeats END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STI.title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtStart', 'seats', 'waitingSeats') THEN STI.title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'seats', 'waitingSeats') THEN STI.dtStart END) END DESC,
							
							-- FIRST ORDER ASC							
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN STI.dtStart END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'seats' THEN STI.seats END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'waitingSeats' THEN STI.waitingSeats END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STI.title END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtStart', 'seats', 'waitingSeats') THEN STI.title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'seats', 'waitingSeats') THEN STI.dtStart END) END
						)
						AS [row_number]
					FROM #StandupTrainingInstances STI
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idStandupTrainingInstance, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				STI.idStandupTrainingInstance,
				CASE WHEN STIL.title IS NOT NULL THEN STIL.title ELSE STI.title END AS title,
				STI.[type],
				STI.seats,
				STI.seatsFilled,
				STI.waitingSeats,
				STI.waitingSeatsFilled,
				STI.dtStart,
				STI.idTimezone,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #StandupTrainingInstances STI ON STI.idStandupTrainingInstance = SelectedKeys.idStandUpTrainingInstance
			LEFT JOIN tblStandupTrainingInstanceLanguage STIL ON STIL.idStandupTrainingInstance = STI.idStandUpTrainingInstance AND STIL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM #StandupTrainingInstances STI
				INNER JOIN CONTAINSTABLE(tblStandupTrainingInstance, *, @searchParam) K ON K.[key] = STI.idStandUpTrainingInstance				
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							STI.idStandupTrainingInstance,
							ROW_NUMBER() OVER (ORDER BY								
								-- FIRST ORDER DESC							
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN STI.dtStart END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'seats' THEN STI.seats END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'waitingSeats' THEN STI.waitingSeats END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STI.title END) END DESC,
								-- SECOND ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtStart', 'seats', 'waitingSeats') THEN STI.title END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'seats', 'waitingSeats') THEN STI.dtStart END) END DESC,
							
								-- FIRST ORDER ASC							
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN STI.dtStart END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'seats' THEN STI.seats END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'waitingSeats' THEN STI.waitingSeats END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STI.title END) END,
								-- SECOND ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtStart', 'seats', 'waitingSeats') THEN STI.title END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'seats', 'waitingSeats') THEN STI.dtStart END) END
							)
							AS [row_number]
						FROM #StandupTrainingInstances STI
						INNER JOIN CONTAINSTABLE(tblStandupTrainingInstance, *, @searchParam) K ON K.[key] = STI.idStandupTrainingInstance
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idStandupTrainingInstance, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					STI.idStandupTrainingInstance, 
					STI.title,
					STI.[type],
					STI.seats,
					STI.seatsFilled,
					STI.waitingSeats,
					STI.waitingSeatsFilled,
					STI.dtStart,
					STI.idTimezone,
					CONVERT(BIT, 1) AS isModifyOn,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN #StandupTrainingInstances STI ON STI.idStandupTrainingInstance = SelectedKeys.idStandUpTrainingInstance
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblStandupTrainingInstanceLanguage STIL
				LEFT JOIN #StandupTrainingInstances STI ON STI.idStandUpTrainingInstance = STIL.idStandUpTrainingInstance
				INNER JOIN CONTAINSTABLE(tblStandupTrainingInstanceLanguage, *, @searchParam) K ON K.[key] = STIL.idStandupTrainingInstanceLanguage
				WHERE STIL.idStandUpTrainingInstance IN (SELECT idStandupTrainingInstance FROM #StandupTrainingInstances)
				AND STIL.idLanguage = @idCallerLanguage
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							STIL.idStandupTrainingInstanceLanguage,
							STI.idStandupTrainingInstance,
							ROW_NUMBER() OVER (ORDER BY								
								-- FIRST ORDER DESC							
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN STI.dtStart END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'seats' THEN STI.seats END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'waitingSeats' THEN STI.waitingSeats END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STIL.title END) END DESC,
								-- SECOND ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtStart', 'seats', 'waitingSeats') THEN STIL.title END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'seats', 'waitingSeats') THEN STI.dtStart END) END DESC,
							
								-- FIRST ORDER ASC							
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN STI.dtStart END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'seats' THEN STI.seats END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'waitingSeats' THEN STI.waitingSeats END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STIL.title END) END,
								-- SECOND ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtStart', 'seats', 'waitingSeats') THEN STIL.title END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'seats', 'waitingSeats') THEN STI.dtStart END) END
							)
							AS [row_number]
						FROM tblStandupTrainingInstanceLanguage STIL
						LEFT JOIN #StandupTrainingInstances STI ON STI.idStanduptrainingInstance = STIL.idStandUpTrainingInstance
						INNER JOIN CONTAINSTABLE(tblStandupTrainingInstanceLanguage, *, @searchParam) K ON K.[key] = STIL.idStandupTrainingInstanceLanguage
						WHERE STIL.idStandUpTrainingInstance IN (SELECT idStandupTrainingInstance FROM #StandupTrainingInstances)
						AND STIL.idLanguage = @idCallerLanguage
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idStandupTrainingInstance, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					STI.idStandupTrainingInstance, 
					STIL.title,
					STI.[type],
					STI.seats,
					STI.seatsFilled,
					STI.waitingSeats,
					STI.waitingSeatsFilled,
					STI.dtStart,
					STI.idTimezone,
					CONVERT(BIT, 1) AS isModifyOn,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblStandupTrainingInstanceLanguage STIL ON STIL.idStandupTrainingInstance = SelectedKeys.idStandupTrainingInstance AND STIL.idLanguage = @idCallerLanguage
				LEFT JOIN #StandupTrainingInstances STI ON STI.idStandupTrainingInstance = STIL.idStandupTrainingInstance
				ORDER BY SelectedKeys.[row_number]

				END
			
			END

			-- drop the temp table
			DROP TABLE #StandupTrainingInstances
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO