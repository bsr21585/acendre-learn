-- =====================================================================
-- PROCEDURE: [Course.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.SaveLang]
GO

/*

Saves course "language specific" properties for specific language
in course language table.

*/

CREATE PROCEDURE [Course.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCourse				INT, 
	@languageString			NVARCHAR(10),
	@title					NVARCHAR(255), 
	@shortDescription		NVARCHAR(512), 
	@longDescription		NVARCHAR(MAX),
	@objectives				NVARCHAR(MAX),
	@searchTags				NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
			 
	/*
	
	validate that the course exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourse
		WHERE idCourse = @idCourse
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseSaveLang_DetailsNotFound'
		RETURN 1 
		END

	/*

	check XSS vulnerabilities

	*/

	-- longDescription

	IF (@longDescription LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSaveLang_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSaveLang_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSaveLang_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSaveLang_DescTagNotAllowed_Iframe'
	RETURN 1 
	END

	-- objectives

	IF (@objectives LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSaveLang_ObjTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@objectives LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSaveLang_ObjTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@objectives LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSaveLang_ObjTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@objectives LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSaveLang_ObjTagNotAllowed_Iframe'
	RETURN 1 
	END
		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblCourseLanguage CL WHERE CL.idCourse = @idCourse AND CL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblCourseLanguage SET
			title = @title,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			objectives = @objectives,
			searchTags = @searchTags
		WHERE idCourse = @idCourse
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCourseLanguage (
			idSite,
			idCourse,
			idLanguage,
			title,
			shortDescription,
			longDescription,
			objectives,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idCourse,
			@idLanguage,
			@title,
			@shortDescription,
			@longDescription,
			@objectives,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCourseLanguage CL
			WHERE CL.idCourse = @idCourse
			AND CL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblCourse SET
			title = @title,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			objectives = @objectives,
			searchTags = @searchTags
		WHERE idCourse = @idCourse

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO