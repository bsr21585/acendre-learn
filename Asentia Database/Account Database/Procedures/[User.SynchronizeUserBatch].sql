-- =====================================================================
-- PROCEDURE: [User.SynchronizeUserBatch]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.SynchronizeUserBatch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.SynchronizeUserBatch]
GO

/*

Inserts and updates records into user table from
a user batch upload.

*/

CREATE PROCEDURE [User.SynchronizeUserBatch]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	--   default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, 
		
	@UserBatch				UserImport		READONLY
)
AS

	BEGIN
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	make sure there are objects in the table parameter
	
	*/
	
	IF (SELECT COUNT(1) FROM @UserBatch) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'UserSynchronizeUserBatch_DetailsNotFound'
		RETURN 1
		END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()
		
	/*
	
	get site default timezone and language ids

	*/

	DECLARE @idDefaultTimezone INT
	DECLARE @idDefaultLanguage INT

	SELECT
		@idDefaultTimezone = S.idTimezone,
		@idDefaultLanguage = S.idLanguage
	FROM tblSite S
	WHERE S.idSite = @idCallerSite

	/*

	insert new users

	*/

	INSERT INTO tblUser 
	(
		firstName,
		middleName,
		lastName,
		displayName,
		avatar,
		email,
		username,
		[password],
		idSite,
		idTimezone,
		objectGUID,
		activationGUID,
		distinguishedName,
		isActive,
		mustchangePassword,
		dtCreated,
		dtExpires,
		isDeleted,
		dtDeleted,
		idLanguage,
		dtModified,
		dtLastLogin,
		employeeID,
		company,
		[address],
		city,
		province,
		postalcode,
		country,
		phonePrimary,
		phoneWork,
		phoneFax,
		phoneHome,
		phoneMobile,
		phonePager,
		phoneOther,
		department,
		division,
		region,
		jobTitle,
		jobClass,
		gender,
		race,
		dtDOB,
		dtHire,
		dtTerm,
		field00,
		field01,
		field02,
		field03,
		field04,
		field05,
		field06,
		field07,
		field08,
		field09,
		field10,
		field11,
		field12,
		field13,
		field14,
		field15,
		field16,
		field17,
		field18,
		field19
	)
	SELECT 
		UB.firstName,
		UB.middleName,
		UB.lastName,
		UB.lastName + ', ' + UB.firstName + CASE WHEN UB.middleName IS NOT NULL AND UB.middleName <> '' THEN ' ' + UB.middleName ELSE '' END,
		NULL,
		UB.email,
		UB.username,
		dbo.GetHashedString('SHA1', UB.[password]),
		@idCallerSite,
		CASE WHEN T.idTimezone IS NOT NULL THEN T.idTimezone ELSE @idDefaultTimezone END,
		NULL,
		NULL,
		NULL,
		UB.isActive,
		CASE WHEN UB.[mustchangePassword] IS NOT NULL AND UB.[mustchangePassword] <> '' THEN UB.mustChangePassword ELSE 0 END,
		@utcNow,
		UB.dtExpires,
		0,
		NULL,
		CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE @idDefaultLanguage END,
		@utcNow,
		NULL,
		UB.employeeID,
		UB.company,
		UB.[address],
		UB.city,
		UB.province,
		UB.postalcode,
		UB.country,
		UB.phonePrimary,
		UB.phoneWork,
		UB.phoneFax,
		UB.phoneHome,
		UB.phoneMobile,
		UB.phonePager,
		UB.phoneOther,
		UB.department,
		UB.division,
		UB.region,
		UB.jobTitle,
		UB.jobClass,
		UB.gender,
		UB.race,
		UB.dtDOB,
		UB.dtHire,
		UB.dtTerm,
		UB.field00,
		UB.field01,
		UB.field02,
		UB.field03,
		UB.field04,
		UB.field05,
		UB.field06,
		UB.field07,
		UB.field08,
		UB.field09,
		UB.field10,
		UB.field11,
		UB.field12,
		UB.field13,
		UB.field14,
		UB.field15,
		UB.field16,
		UB.field17,
		UB.field18,
		UB.field19
	FROM @UserBatch UB
	LEFT JOIN tblTimezone T ON T.dotNetName = UB.timezoneString
	LEFT JOIN tblLanguage L ON L.code = UB.languageString
	WHERE NOT EXISTS (SELECT 1 FROM tblUser U 
					  WHERE UB.username = U.username 
					  AND U.idSite = @idCallerSite 
					  AND (U.isDeleted IS NULL OR U.isDeleted = 0))
	
	/*

	update existing users

	*/
	
	UPDATE tblUser SET
		firstName = UB.firstName,
		middleName = UB.middleName,
		lastName = UB.lastName,
		displayName = UB.lastName + ', ' + UB.firstName + CASE WHEN UB.middleName IS NOT NULL AND UB.middleName <> '' THEN ' ' + UB.middleName ELSE '' END,
		email = UB.email,
		username = UB.username,
		[password] = CASE WHEN UB.[password] IS NOT NULL AND UB.[password] <> '' THEN dbo.GetHashedString('SHA1', UB.[password]) ELSE U.[password] END,
		idTimezone = CASE WHEN T.idTimezone IS NOT NULL THEN T.idTimezone ELSE U.idTimezone END,
		isActive = UB.isActive,
		mustchangePassword = CASE WHEN UB.[mustchangePassword] IS NOT NULL AND UB.[mustchangePassword] <> '' THEN UB.mustChangePassword ELSE 0 END,
		dtExpires = UB.dtExpires,
		idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE U.idLanguage END,
		dtModified = @utcNow,
		employeeID = UB.employeeId,
		company = UB.company,
		[address] = UB.[address],
		city = UB.city,
		province = UB.province,
		postalcode = UB.postalCode,
		country = UB.country,
		phonePrimary = UB.phonePrimary,
		phoneWork = UB.phoneWork,
		phoneFax = UB.phoneFax,
		phoneHome = UB.phoneHome,
		phoneMobile = UB.phoneMobile,
		phonePager = UB.phonePager,
		phoneOther = UB.phoneOther,
		department = UB.department,
		division = UB.division,
		region = UB.region,
		jobTitle = UB.jobTitle,
		jobClass = UB.jobClass,
		gender = UB.gender,
		race = UB.race,
		dtDOB = UB.dtDOB,
		dtHire = UB.dtHire,
		dtTerm = UB.dtTerm,
		field00 = UB.field00,
		field01 = UB.field01,
		field02 = UB.field02,
		field03 = UB.field03,
		field04 = UB.field04,
		field05 = UB.field05,
		field06 = UB.field06,
		field07 = UB.field07,
		field08 = UB.field08,
		field09 = UB.field09,
		field10 = UB.field10,
		field11 = UB.field11,
		field12 = UB.field12,
		field13 = UB.field13,
		field14 = UB.field14,
		field15 = UB.field15,
		field16 = UB.field16,
		field17 = UB.field17,
		field18 = UB.field18,
		field19 = UB.field19,
		isRegistrationApproved = NULL,
		dtApproved = NULL,
		dtDenied = NULL,
		idApprover = NULL,		
		rejectionComments = NULL		
	FROM @UserBatch UB
	LEFT JOIN tblUser U ON U.idSite = @idCallerSite 
		AND U.username = UB.username
	LEFT JOIN tblTimezone T ON T.dotNetName = UB.timezoneString
	LEFT JOIN tblLanguage L ON L.code = UB.languageString
	WHERE U.idSite = @idCallerSite 
	AND U.idUser IS NOT NULL
	AND (U.isDeleted IS NULL OR U.isDeleted = 0)

	/*
	
	do update supervisor links
		
	*/

	INSERT INTO tblUserToSupervisorLink
	(
		idSite,
		idUser,
		idSupervisor
	) 
	SELECT
		@idCallerSite,
		U.idUser,
		SUPER.idUser
	FROM @UserBatch UB 
	LEFT JOIN tblUser U ON U.username = UB.username AND U.idSite = @idCallerSite 
	LEFT JOIN tblUser SUPER ON SUPER.idSite = @idCallerSite AND SUPER.username in 
				(SELECT * FROM [dbo].[DelimitedStringToTable](UB.supervisors, '|'))
	WHERE UB.supervisors IS NOT NULL AND UB.supervisors <> '' AND NOT EXISTS 
			(SELECT 1 FROM tblUserToSupervisorLink USL 
				WHERE USL.idUser = U.idUser AND USL.idSupervisor = SUPER.idUser) 
		  AND U.idUser IS NOT NULL AND SUPER.idUser IS NOT NULL

	/*

	do event log entries for newly created users

	*/

	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		U.idUser,
		U.idUser,
		U.idUser
	FROM tblUser U
	WHERE U.idSite = @idCallerSite
	AND U.dtCreated = @utcNow
	AND EXISTS (SELECT 1 
				FROM @UserBatch UB
				WHERE UB.username = U.username)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 101, @utcNow, @eventLogItems

	-- return successfully
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO