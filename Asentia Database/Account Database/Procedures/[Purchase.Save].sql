-- =====================================================================
-- PROCEDURE: [Purchase.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Purchase.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Purchase.Save]
GO

/*

Adds new Purchase or updates existing Purchase.

*/

CREATE PROCEDURE [Purchase.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idPurchase				INT				OUTPUT,
	@idUser					INT, 
	@orderNumber			NVARCHAR(12),
	@timeStamp				DATETIME,
	@ccnum					NVARCHAR(4),
	@currency				NVARCHAR(4),
	@dtPending				DATETIME, 
	@failed					BIT,
	@transactionId			NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END					
			 
	/*
	
	save the data
	
	*/
	
	IF (@idPurchase = 0 OR @idPurchase IS NULL)

		BEGIN

		-- new purchase inserts get an order number generated
		-- order number is first 4 of last name followed by a random number

		DECLARE @lastNamePortion NVARCHAR(4)
		SELECT @lastNamePortion = UPPER(LEFT(lastName, 4)) FROM tblUser WHERE idUser = @idUser AND idSite = @idCallerSite

		SELECT @orderNumber = @lastNamePortion + '-' + CONVERT(NVARCHAR, ROUND(((99999 - 10000 -1) * RAND() + 10000), 0))

		-- ensure uniqueness

		WHILE EXISTS (SELECT 1 FROM tblPurchase WHERE idSite = @idCallerSite AND orderNumber = @orderNumber)
			BEGIN
			SELECT @orderNumber = @lastNamePortion + '-' + CONVERT(NVARCHAR, ROUND(((99999 - 10000 -1) * RAND() + 10000), 0))
			END	

		-- insert the new purchase

		INSERT INTO tblPurchase (
			idUser,
			orderNumber,
			[timeStamp],
			ccnum,
			currency,
			idSite,
			dtPending,
			failed,
			transactionId
		)
		VALUES (
			@idUser,
			@orderNumber,
			null,
			null,
			@currency,
			@idCallerSite,
			null,
			null,
			null
		)

		-- get the new purchase's id

		SET @idPurchase = SCOPE_IDENTITY()

		END

	ELSE
		
		BEGIN
		
		-- check that the purchase id exists
		IF (SELECT COUNT(1) FROM tblPurchase WHERE idPurchase = @idPurchase AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idPurchase = @idPurchase
			SET @Return_Code = 1
			SET @Error_Description_Code = 'PurchaseSave_NoRecordFound'
			RETURN 1

			END

		-- update existing purchase's properties

		UPDATE tblPurchase SET
			[timeStamp] = @timeStamp,
			ccnum = @ccnum,
			dtPending = @dtPending,
			failed = @failed,
			transactionId = @transactionId
		WHERE idPurchase = @idPurchase

		-- get the purchase's id 

		SET @idPurchase = @idPurchase

		END
			
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO