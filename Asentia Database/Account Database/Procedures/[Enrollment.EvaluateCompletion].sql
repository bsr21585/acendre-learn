-- =====================================================================
-- PROCEDURE: [Enrollment.EvaluateCompletion]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.EvaluateCompletion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.EvaluateCompletion]
GO

/*

Evaluates completion status of an enrollment based on completion of its lesson data,
and marks the enrollment accordingly.

Note, this should not be called from procedural code, only from other procedures that
affect lesson data.

*/

CREATE PROCEDURE [Enrollment.EvaluateCompletion]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idEnrollment			INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/* NO PERMISSIONS CHECKS - WE CANNOT RESTRICT THIS BASED ON PERMISSIONS SINCE IT CAN BE CALLED AS A RESULT OF LEARNER ACTIONS */

	/*
	
	validate that the enrollment exists within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblEnrollment E
		WHERE E.idEnrollment = @idEnrollment  
		AND (E.idSite IS NULL OR E.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentEvaluateCompletion_NoRecordFound'
		RETURN 1 
		END

	/*
	
	declare variables we're going to be working with
	
	*/
			
	DECLARE @isEnrollmentCompleted				BIT
	DECLARE @enrollmentMeetsCompletionCriteria	BIT
	DECLARE @idEnrollmentUser					INT
	DECLARE @idEnrollmentCourse					INT
	DECLARE @idEnrollmentTimezone				INT

	SET @isEnrollmentCompleted = 0
	SET @enrollmentMeetsCompletionCriteria = 0

	/*

	get the enrollment's user, course, and timezone

	*/

	SELECT 
		@idEnrollmentUser = idUser,
		@idEnrollmentCourse = idCourse,
		@idEnrollmentTimezone = idTimezone
	FROM tblEnrollment
	WHERE idEnrollment = @idEnrollment

	/* 

	get the enrollment's current completion status 

	*/

	IF (SELECT COUNT(1) FROM tblEnrollment WHERE idEnrollment = @idEnrollment AND dtCompleted IS NOT NULL) = 1
		BEGIN
		SET @isEnrollmentCompleted = 1
		END

	/* 

	evaluate the completion status of the enrollment by checking to ensure all lesson data is completed

	*/

	CREATE TABLE #LessonData (
		[idData-Lesson]		INT,
		[dtCompleted]		DATETIME
	)

	INSERT INTO #LessonData (
		[idData-Lesson],
		dtCompleted
	)
	SELECT
		[idData-Lesson],
		dtCompleted
	FROM [tblData-Lesson] DL
	LEFT JOIN [tblLesson] L ON L.idLesson = DL.idLesson
	WHERE DL.idEnrollment = @idEnrollment AND (L.isOptional = 0 OR L.isOptional IS NULL) AND (L.isDeleted = 0 OR L.isDeleted IS NULL)
	ORDER BY DL.[order]

	IF ((SELECT COUNT(1) FROM #LessonData) > 0 AND (SELECT COUNT(1) FROM #LessonData WHERE dtCompleted IS NOT NULL) = (SELECT COUNT(1) FROM #LessonData))
		BEGIN
		SET @enrollmentMeetsCompletionCriteria = 1
		END

	/*

	if the enrollment meets completion criteria and is not already completed, complete it
	then award any certificates that need to be awarded

	*/

	IF @enrollmentMeetsCompletionCriteria = 1 AND @isEnrollmentCompleted = 0
		BEGIN
		
		DECLARE @utcNow DATETIME
		SET @utcNow = GETUTCDATE()

		-- update the enrollment record
		UPDATE tblEnrollment SET dtCompleted = @utcNow WHERE idEnrollment = @idEnrollment

		-- write event log entry for completion
		DECLARE @eventLogItem EventLogItemObjects
		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, idCourse, idEnrollment, idUser FROM tblEnrollment WHERE idEnrollment = @idEnrollment
		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 205, @utcNow, @eventLogItem

		-- evaluate certificates
		EXEC [Certificate.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollmentUser, @idEnrollmentCourse, 'course', 'completed', @idEnrollmentTimezone

		END

	/*

	if the enrollment is already completed but does not meet completion criteria, remove completion
	this would happen in the case of a lesson data reset then revoke any certificates that need to be revoked

	*/

	IF @isEnrollmentCompleted = 1 AND @enrollmentMeetsCompletionCriteria = 0
		BEGIN

		-- update the enrollment record
		UPDATE tblEnrollment SET dtCompleted = NULL WHERE idEnrollment = @idEnrollment

		-- evaluate certificates
		EXEC [Certificate.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollmentUser, @idEnrollmentCourse, 'course', 'revoked', @idEnrollmentTimezone		

		END

	-- drop the temporary table
	DROP TABLE #LessonData

	/*

	check for learning path enrollment completions

	*/

	EXEC [LearningPathEnrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollmentUser, @idEnrollmentCourse

	/*

	check for certification awards/renewals

	*/

	EXEC [Certification.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, NULL, @idEnrollmentUser

	/*

	return

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO