-- =====================================================================
-- PROCEDURE: [Resource.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Resource.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Resource.Details]
GO

/*

Return all the properties for a given Resource id.

*/
CREATE PROCEDURE [Resource.Details]
(
	@Return_Code			INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)		OUTPUT,
	@idCallerSite			INT					= 0, --default if not specified
	@callerLangString		NVARCHAR (10)			,
	@idCaller				INT					= 0,
	
	@idResource				INT					OUTPUT,
	@name					NVARCHAR(255)		OUTPUT,
	@idResourceType			INT         		OUTPUT,
	@resourceType			NVARCHAR(255)		OUTPUT,
	@description		    NVARCHAR(MAX)		OUTPUT,
	@identifier				NVARCHAR(50)		OUTPUT,
	@isMovable				BIT					OUTPUT,
	@isAvailable			BIT					OUTPUT,
	@capacity				NVARCHAR(255)		OUTPUT,
	@room					NVARCHAR(255)		OUTPUT,
	@building				NVARCHAR(255)		OUTPUT,
	@city					NVARCHAR(255)		OUTPUT,
	@province				NVARCHAR(255)		OUTPUT,
	@country				NVARCHAR(255)		OUTPUT,
	@idOwner                INT                 OUTPUT,
	@ownerName				NVARCHAR(255)		OUTPUT,
	@ownerEmail				NVARCHAR(255)		OUTPUT,
	@ownerContactInfo		NVARCHAR(512)		OUTPUT,
	@ownerWithinSystem		BIT					OUTPUT,
	@idParentResource       INT                 OUTPUT
)											
AS											
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblResource
		WHERE idResource = @idResource
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ResourceDetails_NoRecordFound'
		RETURN 1
	    END
		
	
		
	/*
	
	get the data 
	
	*/
		
		DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	SELECT
		@idResource			= R.idResource,
		@name				= CASE WHEN RL.name IS NULL OR RL.name = '' THEN R.name ELSE RL.name END,
		@idResourceType		= R.idResourceType,
		@resourceType		= RT.resourceType,
		@description		= CASE WHEN RL.[description] IS NULL THEN R.[description] ELSE RL.[description] END,
		@identifier			= ISNULL( R.identifier,'') ,
		@isMovable			= R.isMoveable,
		@isAvailable		= R.isAvailable,
		@capacity			= R.capacity,
		@room				= ISNULL( R.locationRoom,'') ,
		@building			= ISNULL( R.locationBuilding,'') ,
		@city				= ISNULL( R.locationCity,'') ,
		@province			= ISNULL( R.locationProvince,''),
		@country			= ISNULL( R.locationCountry,''),
		@idOwner			= ISNULL( R.idOwner,0) ,
		@ownerName			= ISNULL( R.ownerName,'') ,
		@ownerEmail			= ISNULL( R.ownerEmail,''),
		@ownerContactInfo	= ISNULL( R.ownerContactInformation,''),
		@ownerWithinSystem  = R.ownerWithinSystem,
		@idParentResource   = ISNULL( R.idParentResource,0) 
	FROM tblResource R 
	INNER JOIN tblResourceType RT on R.idResourceType = RT.idResourceType
	LEFT JOIN tblResourceLanguage RL 
		ON RL.idResource = R.idResource 
		AND RL.idLanguage = @idCallerLanguage
	WHERE R.idResource = @idResource 
	AND R.idSite = @idCallerSite
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'ResourceDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO