-- =====================================================================
-- PROCEDURE: [StandupTrainingInstanceToUserLink.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstanceToUserLink.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstanceToUserLink.GetGrid]
GO

/*

Gets a listing of Standup Training Instances that are linked to a user.

*/

CREATE PROCEDURE [StandupTrainingInstanceToUserLink.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblStandupTrainingInstance STI
			LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND STI.idSite = @idCallerSite)
				)
			AND STIUL.idUser = @idCaller
			AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize)
						STIUL.idStandupTrainingInstanceToUserLink,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STI.title END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STI.title END) END
							
						)
						AS [row_number]
					FROM tblStandupTrainingInstance STI
					LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND STI.idSite = @idCallerSite)
						)
					AND STIUL.idUser = @idCaller
					AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idStandupTrainingInstanceToUserLink, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				STIUL.idStandupTrainingInstanceToUserLink,
				STIUL.idStandupTrainingInstance,
				ST.title + ' - ' + STI.title AS title,
				STI.[type],
				STI.urlRegistration,
				STI.urlAttend,
				STI.genericJoinUrl,
				STI.meetingPassword,
				STIUL.joinUrl AS specificJoinUrl,
				CASE WHEN STIUL.isWaitingList <> 1 THEN 1 ELSE 0 END AS isCallerEnrolled,
				STI.seats,
				STI.waitingSeats,
				CONVERT(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandupTrainingInstanceToUserLink = SelectedKeys.idStandUpTrainingInstanceToUserLink
			LEFT JOIN tblStandupTrainingInstance STI ON STI.idStandupTrainingInstance = STIUL.idStandUpTrainingInstance
			LEFT JOIN tblStandupTraining ST ON ST.idStandUpTraining = STI.idStandUpTraining
			LEFT JOIN tblStandupTrainingInstanceLanguage STIL ON STIL.idStandupTrainingInstance = STI.idStandUpTrainingInstance  
		    AND STIL.idLanguage = @idCallerLanguage
			WHERE (STI.isDeleted IS NULL OR STI.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblStandupTrainingInstance STI
				INNER JOIN CONTAINSTABLE(tblStandupTrainingInstance, *, @searchParam) K ON K.[key] = STI.idStandUpTrainingInstance
				LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND STI.idSite = @idCallerSite)
					)
				AND STIUL.idUser = @idCaller
				AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							STIUL.idStandupTrainingInstanceToUserLink,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STI.title END) END DESC,
								
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STI.title END) END
							)
							AS [row_number]
						FROM tblStandupTrainingInstance STI
						INNER JOIN CONTAINSTABLE(tblStandupTrainingInstance, *, @searchParam) K ON K.[key] = STI.idStandupTrainingInstance
						LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND STI.idSite = @idCallerSite
							)
						AND STIUL.idUser = @idCaller
						AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idStandupTrainingInstanceToUserLink, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					STIUL.idStandupTrainingInstanceToUserLink, 
					STIUL.idStandupTrainingInstance,
					ST.title + ' - ' + STI.title,
					STI.[type],
					STI.urlRegistration,
					STI.urlAttend,
					STI.genericJoinUrl,
					STI.meetingPassword,
					STIUL.joinUrl AS specificJoinUrl,
					CASE WHEN STIUL.isWaitingList <> 1 THEN 1 ELSE 0 END AS isCallerEnrolled,
					STI.seats,
					STI.waitingSeats,
					CONVERT(bit, 1) AS isModifyOn,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblStandUpTrainingInstanceToUserLink STIUL ON STIUL.idStandupTrainingInstanceToUserLink = SelectedKeys.idStandUpTrainingInstanceToUserLink
				LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandupTrainingInstance = STIUL.idStandupTrainingInstance
				LEFT JOIN tblStandUpTraining ST ON ST.idStandUptraining = STI.idStandUpTraining
				WHERE (STI.isDeleted IS NULL OR STI.isDeleted = 0)
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblStandupTrainingInstanceLanguage STIL
				LEFT JOIN tblStandupTrainingInstance STI ON STI.idStandUpTrainingInstance = STIL.idStandUpTrainingInstance
				LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance
				INNER JOIN CONTAINSTABLE(tblStandupTrainingInstanceLanguage, *, @searchParam) K ON K.[key] = STIL.idStandupTrainingInstanceLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND STI.idSite = @idCallerSite)
					)
				AND STIUL.idUser = @idCaller
				AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)
				AND STIL.idLanguage = @idCallerLanguage
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							STIL.idStandupTrainingInstanceLanguage,
							STI.idStandupTrainingInstance,
							STIUL.idStandupTrainingInstanceToUserLink,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STIL.title END) END DESC,
								
							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STIL.title END) END
							)
							AS [row_number]
						FROM tblStandupTrainingInstanceLanguage STIL
						LEFT JOIN tblStandupTrainingInstance STI ON STI.idStanduptrainingInstance = STIL.idStandUpTrainingInstance
						LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance
						INNER JOIN CONTAINSTABLE(tblStandupTrainingInstanceLanguage, *, @searchParam) K ON K.[key] = STIL.idStandupTrainingInstanceLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND STI.idSite = @idCallerSite
							)
						AND STIUL.idUser = @idCaller
						AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)
						AND STIL.idLanguage = @idCallerLanguage
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idStandupTrainingInstanceToUserLink, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					STIUL.idStandupTrainingInstanceToUserLink,
					STIUL.idStandupTrainingInstance,
					ST.title + ' - ' + STI.title,
					STI.[type],
					STI.urlRegistration,
					STI.urlAttend,
					STI.genericJoinUrl,
					STI.meetingPassword,
					STIUL.joinUrl AS specificJoinUrl,
					CASE WHEN STIUL.isWaitingList <> 1 THEN 1 ELSE 0 END AS isCallerEnrolled,
					STI.seats,
					STI.waitingSeats,
					CONVERT(bit, 1) AS isModifyOn,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandupTrainingInstanceToUserLink = SelectedKeys.idStandupTrainingInstanceToUserLink-- AND STIL.idLanguage = @idCallerLanguage
				LEFT JOIN tblStandupTrainingInstance STI ON STI.idStandupTrainingInstance = STIUL.idStandupTrainingInstance
				LEFT JOIN tblStandupTraining ST ON ST.idStandUpTraining = STI.idStandUpTraining
				ORDER BY SelectedKeys.[row_number]

				END
			
			END
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO