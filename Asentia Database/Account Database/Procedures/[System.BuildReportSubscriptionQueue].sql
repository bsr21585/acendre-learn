-- =====================================================================
-- PROCEDURE: [System.BuildReportSubscriptionQueue]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.BuildReportSubscriptionQueue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.BuildReportSubscriptionQueue]
GO

/*

Builds the report subscription queue for the system.

*/

CREATE PROCEDURE [System.BuildReportSubscriptionQueue]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON

	-- TODO (MAYBE): Delete report subscriptions for users who do not have reporting permissions.

	-- cascade the report subscription items to queue
	
	INSERT INTO tblReportSubscriptionQueue (
		idSite,
		idReportSubscription,
		idReport,
		idDataset,
		reportName,
		idRecipient,
		recipientLangString,
		recipientFullName,
		recipientFirstName,
		recipientLogin,
		recipientEmail,
		recipientTimezone,
		recipientTimezoneDotNetName,
		[priority],
		dtAction
	)
	SELECT
		RS.idSite,
		RS.idReportSubscription,
		RS.idReport,
		R.idDataset,
		CASE WHEN RL.title IS NOT NULL THEN RL.title ELSE R.title END,
		RS.idUser,
		RUL.code,
		RU.firstName + ' ' + RU.lastName,
		RU.firstName,
		RU.username,
		RU.email,
		RU.idTimezone,
		TZ.dotNetName,
		0, -- normal priority
		RS.dtNextAction
	FROM tblReportSubscription RS
	LEFT JOIN tblUser RU ON RU.idUser = RS.idUser
	LEFT JOIN tblLanguage RUL ON RUL.idLanguage = RU.idLanguage
	LEFT JOIN tblTimezone TZ ON TZ.idTimezone = RU.idTimezone
	LEFT JOIN tblReport R ON R.idReport = RS.idReport
	LEFT JOIN tblReportLanguage RL ON RL.idReport = R.idReport AND RL.idLanguage = RU.idLanguage
	LEFT JOIN tblSite S ON S.idSite = RS.idSite
	WHERE RS.dtNextAction < GETUTCDATE() -- the next issue is due to be sent
	AND (S.dtExpires IS NULL OR S.dtExpires > GETUTCDATE()) AND S.isActive = 1 -- LMS account is still active
	AND ((RU.isDeleted IS NULL OR RU.isDeleted = 0) AND (RU.isRegistrationApproved IS NULL OR RU.isRegistrationApproved = 1)) -- recipient user is not deleted or pending
	AND (RU.dtExpires IS NULL OR RU.dtExpires > GETUTCDATE()) AND RU.isActive = 1 -- user account is still active
	AND RU.email IS NOT NULL -- user has an email address
	AND NOT EXISTS ( -- the currently 'due' issue has not been previously sent
		SELECT 1
		FROM tblReportSubscriptionQueue RSQ
		WHERE RSQ.idReport = RS.idReport
		AND RSQ.idRecipient = RS.idUser
		AND RSQ.dtAction = RS.dtNextAction
	)
	
	-- update the next target send date for the report subscriptions

	UPDATE tblReportSubscription SET
		dtNextAction = CASE recurTimeframe
						WHEN 'yyyy' THEN DATEADD(YEAR, recurInterval, dtNextAction)
						WHEN 'm' THEN DATEADD(MONTH, recurInterval, dtNextAction)
						WHEN 'ww' THEN DATEADD(WEEK, recurInterval, dtNextAction)
						WHEN 'd' THEN DATEADD(DAY, recurInterval, dtNextAction)
						END			
	WHERE idReportSubscription IN (
		SELECT RS.idReportSubscription
		FROM tblReportSubscriptionQueue RSQ
		LEFT JOIN tblReportSubscription RS ON RS.idReportSubscription = RSQ.idReportSubscription AND RS.dtNextAction = RSQ.dtAction
	)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO