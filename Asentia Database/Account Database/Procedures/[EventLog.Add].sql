-- =====================================================================
-- PROCEDURE: [EventLog.Add]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventLog.Add]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventLog.Add]
GO

/*

Adds entries for a specific event and object to the event log.

This should only be called from stored procedures, NOT procedural code.

*/
CREATE PROCEDURE [EventLog.Add]
(
	@Return_Code			INT							OUTPUT,
	@Error_Description_Code	NVARCHAR(50)				OUTPUT,
	@idCallerSite			INT							= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT							= 0,
	
	@idEventLog				INT							OUTPUT,
	@idEventType			INT,
	@eventDate				DATETIME,
	@ItemObjects			EventLogItemObjects			READONLY
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	insert the event(s) into the log

	*/

	INSERT INTO tblEventLog (
		idSite, 
		idEventType,
		[timestamp],
		[eventDate],
		idObject,
		idObjectRelated,
		idObjectUser,
		idExecutingUser
	)
	SELECT
		OBJ.idSite, 
		@idEventType,
		GETUTCDATE(),
		@eventDate,
		OBJ.idObject,
		OBJ.idObjectRelated,
		OBJ.idObjectUser,
		@idCaller
	FROM @ItemObjects OBJ
	WHERE NOT EXISTS (SELECT 1
					  FROM tblEventLog EL
					  WHERE EL.idSite = OBJ.idSite
					  AND EL.idEventType = @idEventType
					  AND EL.eventDate = @eventDate
					  AND EL.idObject = OBJ.idObject
					  AND EL.idObjectRelated = OBJ.idObjectRelated
					  AND EL.idObjectUser = OBJ.idObjectUser
					  AND EL.idExecutingUser = @idCaller)			-- this will ensure that no duplicate entries can be created in the event log
	
	-- get the new exception log record's id and return successfully, this may not need to be done
	-- SET @idEventLog = SCOPE_IDENTITY()

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
			
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO