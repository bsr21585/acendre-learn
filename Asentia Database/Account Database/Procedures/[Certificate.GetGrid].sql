-- =====================================================================
-- PROCEDURE: [Certificate.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certificate.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certificate.GetGrid]
GO

/*

Gets a listing of Certificates that are templates (do not belong to an object) or
that are part of an object, depending on the values of @objectType and @idobject params.

*/

CREATE PROCEDURE [Certificate.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

 	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,
	@objectType				INT				= NULL,
	@idObject				INT				= NULL
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblCertificate C
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
				)
			AND (C.isDeleted IS NULL OR C.isDeleted = 0)
			AND (
				(C.idObject IS NULL AND @idObject IS NULL AND C.objectType IS NULL AND @objectType IS NULL)
				OR
				(@idObject IS NOT NULL AND C.idObject = @idObject AND @objectType IS NOT NULL AND C.objectType = @objectType)
				)

			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						C.idCertificate,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN C.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'issuingOrganization' THEN C.issuingOrganization END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN C.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'issuingOrganization' THEN C.issuingOrganization END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END
						)
						AS [row_number]
					FROM tblCertificate C
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
						)
					AND (C.isDeleted IS NULL OR C.isDeleted = 0)
					AND (
					(C.idObject IS NULL AND @idObject IS NULL AND C.objectType IS NULL AND @objectType IS NULL)
					OR
					(@idObject IS NOT NULL AND C.idObject = @idObject AND @objectType IS NOT NULL AND C.objectType = @objectType)
					)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idCertificate, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				C.idCertificate,
				CASE WHEN CL.name IS NOT NULL THEN CL.name ELSE C.name END AS name,
				C.issuingOrganization,
				C.credits, 
				C.code,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblCertificate C ON C.idCertificate = SelectedKeys.idCertificate
			LEFT JOIN tblCertificateLanguage CL ON CL.idCertificate = C.idCertificate AND CL.idLanguage = @idCallerLanguage
			WHERE (C.isDeleted IS NULL OR C.isDeleted = 0)
			AND (
				(C.idObject IS NULL AND @idObject IS NULL AND C.objectType IS NULL AND @objectType IS NULL)
				OR
				(@idObject IS NOT NULL AND C.idObject = @idObject AND @objectType IS NOT NULL AND C.objectType = @objectType)
				)
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblCertificate C
				INNER JOIN CONTAINSTABLE(tblCertificate, *, @searchParam) K ON K.[key] = C.idCertificate
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
					)
				AND (C.isDeleted IS NULL OR C.isDeleted = 0)
				AND (
					(C.idObject IS NULL AND @idObject IS NULL AND C.objectType IS NULL AND @objectType IS NULL)
					OR
					(@idObject IS NOT NULL AND C.idObject = @idObject AND @objectType IS NOT NULL AND C.objectType = @objectType)
					)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							C.idCertificate,
							ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN C.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'issuingOrganization' THEN C.issuingOrganization END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN C.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'issuingOrganization' THEN C.issuingOrganization END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END
							)
							AS [row_number]
						FROM tblCertificate C
						INNER JOIN CONTAINSTABLE(tblCertificate, *, @searchParam) K ON K.[key] = C.idCertificate
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite
							)
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND (
							(C.idObject IS NULL AND @idObject IS NULL AND C.objectType IS NULL AND @objectType IS NULL)
							OR
							(@idObject IS NOT NULL AND C.idObject = @idObject AND @objectType IS NOT NULL AND C.objectType = @objectType)
							)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idCertificate, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					C.idCertificate,
					C.name,
					C.issuingOrganization,
					C.credits, 
					C.code,
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblCertificate C ON C.idCertificate = SelectedKeys.idCertificate
				WHERE (C.isDeleted IS NULL OR C.isDeleted = 0)
				AND (
					(C.idObject IS NULL AND @idObject IS NULL AND C.objectType IS NULL AND @objectType IS NULL)
					OR
					(@idObject IS NOT NULL AND C.idObject = @idObject AND @objectType IS NOT NULL AND C.objectType = @objectType)
					)
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblCertificateLanguage CL
				LEFT JOIN tblCertificate C ON C.idCertificate = CL.idCertificate
				INNER JOIN CONTAINSTABLE(tblCertificateLanguage, *, @searchParam) K ON K.[key] = CL.idCertificateLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
					)
				AND CL.idLanguage = @idCallerLanguage
				AND (C.isDeleted IS NULL OR C.isDeleted = 0)
				AND (
					(C.idObject IS NULL AND @idObject IS NULL AND C.objectType IS NULL AND @objectType IS NULL)
					OR
					(@idObject IS NOT NULL AND C.idObject = @idObject AND @objectType IS NOT NULL AND C.objectType = @objectType)
					)

				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							CL.idCertificateLanguage,
							C.idCertificate,
							ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN CL.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'issuingOrganization' THEN C.issuingOrganization END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN CL.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'issuingOrganization' THEN C.issuingOrganization END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END
							)
							AS [row_number]
						FROM tblCertificateLanguage CL
						LEFT JOIN tblCertificate C ON C.idCertificate = CL.idCertificate
						INNER JOIN CONTAINSTABLE(tblCertificateLanguage, *, @searchParam) K ON K.[key] = CL.idCertificateLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite
							)
						AND CL.idLanguage = @idCallerLanguage
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND (
							(C.idObject IS NULL AND @idObject IS NULL AND C.objectType IS NULL AND @objectType IS NULL)
							OR
							(@idObject IS NOT NULL AND C.idObject = @idObject AND @objectType IS NOT NULL AND C.objectType = @objectType)
							)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idCertificate, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					C.idCertificate,
					C.name,
					C.issuingOrganization,
					C.credits, 
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblCertificateLanguage CL ON CL.idCertificate = SelectedKeys.idCertificate AND CL.idLanguage = @idCallerLanguage
				LEFT JOIN tblCertificate C ON C.idCertificate = CL.idCertificate
				AND (
					(C.idObject IS NULL AND @idObject IS NULL AND C.objectType IS NULL AND @objectType IS NULL)
					OR
					(@idObject IS NOT NULL AND C.idObject = @idObject AND @objectType IS NOT NULL AND C.objectType = @objectType)
					)
				ORDER BY SelectedKeys.[row_number]
	
				END
			
			END
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO