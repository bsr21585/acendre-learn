-- =====================================================================
-- PROCEDURE: [WebMeetingOrganizer.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[WebMeetingOrganizer.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [WebMeetingOrganizer.Delete]
GO

/*

"Deletes" web meeting organizer(s) by detaching them from the user they're attached to.

*/

CREATE PROCEDURE [WebMeetingOrganizer.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@WebMeetingOrganizers		IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @WebMeetingOrganizers) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'WebMeetingOrganizerDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all web meeting organizer's exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @WebMeetingOrganizers WMOWMO
		LEFT JOIN tblWebMeetingOrganizer WMO ON WMO.idWebMeetingOrganizer = WMOWMO.id
		WHERE WMO.idSite IS NULL
		OR WMO.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'WebMeetingOrganizerDelete_NoRecordFound'
		RETURN 1 
		END

	/*

	"DELETE" the web meeting organizer(s) by detaching from the user they're attached to

	*/

	UPDATE tblWebMeetingOrganizer SET idUser = NULL WHERE idWebMeetingOrganizer IN (SELECT id FROM @WebMeetingOrganizers)
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO