-- =====================================================================
-- PROCEDURE: [Calendar.GetEventDetails]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Calendar.GetEventDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Calendar.GetEventDetails]
GO

/*

Returns a listing of items coming up on the calendar.

*/

CREATE PROCEDURE [Calendar.GetEventDetails]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified

	@idStandupTrainingInstance			INT				= 0 -- will fail if not specified
)
AS
	
	BEGIN
	SET NOCOUNT ON
		
	/*

	do not check caller site membership or caller permissions as
	this can be called by a non-logged-in user

	*/

		
	/*
	
	get the user's language
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	/* 
	
	get the calendar items

	*/

	SELECT 
		@idCaller AS idUser,
		STI.idStandupTrainingInstance AS [id],
		ST.idStandupTraining,
		'session' AS [event],
		CASE WHEN STL.title IS NOT NULL THEN STL.title ELSE ST.title END
		+ ' - ' +
		CASE WHEN STIL.title IS NOT NULL THEN STIL.title ELSE STI.title END
		AS [eventTitle],
		STIMT.dtStart,
		STIMT.dtEnd,
		CASE WHEN STIL.[description] IS NOT NULL THEN STIL.[description] ELSE STI.[description] END AS [description],
		STI.[type],
		STI.urlRegistration,
		STI.urlAttend,
		STI.city,		
		STI.province,
		CASE WHEN STIL.locationDescription IS NOT NULL THEN STIL.locationDescription ELSE STI.locationDescription END AS locationDescription,
		STI.genericJoinUrl,
		STI.meetingPassword,
		STUFF((SELECT ', ' + INS.displayName FROM tblStandupTrainingInstanceToInstructorLink STII LEFT JOIN tblUser INS ON INS.idUser = STII.idInstructor AND STII.idStandupTrainingInstance = STI.idStandUpTrainingInstance FOR XML PATH('')), 1, 1, '') AS [instructor],
		STIMT.idTimezone AS [timezone],
		STIUL.joinUrl AS specificJoinUrl,
		STIUL.registrantKey,
		CASE WHEN STIUL.isWaitingList <> 1 THEN 1 ELSE 0 END AS isCallerEnrolled,
		CASE WHEN STIUL.isWaitingList = 1 THEN 1 ELSE 0 END AS isCallerWaitlisted,
		CASE WHEN ST.isRestrictedDrop = 1 THEN 1 ELSE 0 END AS isRestrictedDrop,
		CASE WHEN ST.isStandaloneEnroll IS NOT NULL THEN ST.isStandaloneEnroll ELSE 0 END AS isStandaloneEnroll,
		REPLACE(REPLACE(
			STUFF(( 
					SELECT DISTINCT
						'||' + CONVERT(NVARCHAR, C.idCourse) + '##' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
					FROM tblCourse C 
					LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
					LEFT JOIN tblLesson L ON L.idCourse = C.idCourse AND (L.isDeleted IS NULL OR L.isDeleted = 0)
					LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = L.idLesson AND LCL.idContentType = 2
					LEFT JOIN tblStandUpTraining ST ON ST.idStandUpTraining = LCL.idObject
					WHERE ST.idStandUpTraining = STI.idStandUpTraining
					AND (C.isDeleted = 0 OR C.isDeleted IS NULL)
					AND C.isPublished = 1
					ORDER BY title				
			FOR XML PATH('')), 1, 9, '') 
		, '<title>', ''), '</title>', '') AS [containedWithinCourses]
	FROM tblStandUpTrainingInstance STI
	LEFT JOIN tblStandupTrainingInstanceLanguage STIL ON STIL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance AND STIL.idLanguage = @idCallerLanguage
	LEFT JOIN tblStandUpTrainingInstanceMeetingTime STIMT ON STIMT.idStandupTrainingInstance = STI.idStandupTrainingInstance
	LEFT JOIN tblStandUpTraining ST ON ST.idStandUpTraining = STI.idStandupTraining
	LEFT JOIN tblStandupTrainingLanguage STL ON STL.idStandUpTraining = ST.idStandUpTraining AND STL.idLanguage = @idCallerLanguage
	LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idUser = @idCaller AND STIUL.idStandupTrainingInstance = @idStandupTrainingInstance
	WHERE STI.idStandupTrainingInstance = @idStandupTrainingInstance
	AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
