-- =====================================================================
-- PROCEDURE: [LearningPath.RemoveFeedModerators]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.RemoveFeedModerators]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.RemoveFeedModerators]
GO

/*

Adds new feed moderators to the course

*/

CREATE PROCEDURE [LearningPath.RemoveFeedModerators]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idLearningPath			INT,
	@Moderators				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the learning path exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLearningPath
		WHERE idSite = @idCallerSite
		AND @idLearningPath = idLearningPath
		) <> 1
		BEGIN
		SELECT @Return_Code = 2
		RETURN 1 
		END
	
	/*
	
	validate that the learning path has a feed
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLearningPathFeed LPF
		LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPF.idLearningPath
		WHERE LP.idSite = @idCallerSite
		AND @idLearningPath = LP.idLearningPath
		) <> 1
		BEGIN
		SELECT @Return_Code = 2
		RETURN 1 
		END
			
	/*
	
	remove the moderators 
	
	*/
	
	DELETE FROM tblLearningPathFeedModerator
	WHERE EXISTS (
		SELECT 1
		FROM @Moderators M
		LEFT JOIN tblLearningPathFeed LPF ON LPF.idLearningPath = @idLearningPath
		WHERE tblLearningPathFeedModerator.idModerator = M.id
		AND tblLearningPathFeedModerator.idLearningPathFeed = LPF.idLearningPathFeed
	)
		
	SELECT @Return_Code = 0
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO