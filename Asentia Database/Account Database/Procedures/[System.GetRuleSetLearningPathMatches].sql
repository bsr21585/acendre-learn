-- =====================================================================
-- PROCEDURE: [System.GetRuleSetLearningPathMatches]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetRuleSetLearningPathMatches]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetRuleSetLearningPathMatches]
GO

/*

Gets RuleSet matches for LearningPath RuleSet Enrollments.

*/
CREATE PROCEDURE [System.GetRuleSetLearningPathMatches]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR (50)	OUTPUT,
	@idCallerSite					INT				= 0,	-- default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT,

	@LearningPaths					IDTable			READONLY,
	@Filters						IDTable			READONLY,
	@filterBy						NVARCHAR(10),
	@executeForRevokeProecdure		BIT				= 0		-- When true, this gets all matches regardless of priority or lifespan.
															-- The results are then used to revoke inherited ruleset enrollments where 
															-- the user no longer matches ANY ruleset enrollment for the LearningPath.
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* SET FILTER TO 'user' IF IT IS ANYTHING OTHER THAN 'group' */
	IF @filterBy IS NULL OR LOWER(@filterBy) <> LOWER('group')
		SET @filterBy = 'user'

	/* CHECK LearningPath AND FILTER LISTS FOR RECORDS */
	DECLARE @LearningPathsRecordCount INT
	DECLARE @FiltersRecordCount INT
	SELECT @LearningPathsRecordCount = COUNT(1) FROM @LearningPaths
	SELECT @FiltersRecordCount = COUNT(1) FROM @Filters

	/* GET THE CURRENT UTC DATE */
	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/* CREATE TEMPORARY TABLE TO STORE USER-TO-RULESETENROLLMENT JOINS RESULTING FROM RULES EXECUTION */
	CREATE TABLE #UserToRuleSetLearningPathEnrollmentJoins 
	(
		idRuleSetLearningPathEnrollment		INT,
		[priority]							INT,
		idRuleSet							INT, 
		idLearningPath						INT, 
		idUser								INT
	)

	/* CREATE TEMPORARY TABLE FOR RULESET ENROLLMENT AUTO-JOIN RULES */
	CREATE TABLE #RuleSetLearningPathEnrollmentRules
	(
		idSite								INT,
		idRuleSet							INT,
		idRuleSetLearningPathEnrollment		INT,
		idLearningPath						INT,
		[priority]							INT,
		isAny								BIT,
		field								NVARCHAR(25),
		operator							NVARCHAR(25),
		value								NVARCHAR(255)
	)

	/* PUT RULESET ENROLLMENT AUTO-JOIN RULESETS IN TEMP TABLE */
	IF (@executeForRevokeProecdure = 0)
		BEGIN

		INSERT INTO #RuleSetLearningPathEnrollmentRules
		(
			idSite,
			idRuleSet,
			idRuleSetLearningPathEnrollment,
			idLearningPath,
			[priority],
			isAny,
			field,
			operator,
			value
		)
		SELECT
			RS.idSite,
			RS.idRuleSet,
			RSRSEL.idRuleSetLearningPathEnrollment,
			RSE.idLearningPath,
			RSE.[priority],
			RS.isAny,
			R.userField,
			R.operator,
			CASE WHEN R.textValue IS NOT NULL THEN 
				REPLACE(R.textValue, '''', '''''')
			ELSE
				CASE WHEN R.dateValue IS NOT NULL THEN
					CONVERT(NVARCHAR(255), R.dateValue)
				ELSE
					CASE WHEN R.numValue IS NOT NULL THEN
						CONVERT(NVARCHAR(255), R.numValue)
					ELSE
						CASE WHEN R.bitValue IS NOT NULL THEN
							CONVERT(NVARCHAR(255), R.bitValue)
						ELSE
							NULL
						END
					END
				END
			END
		FROM tblRuleSet RS
		LEFT JOIN tblRule R ON R.idRuleSet = RS.idRuleSet		
		LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSRSEL ON RSRSEL.idRuleSet = RS.idRuleSet
		LEFT JOIN tblRuleSetLearningPathEnrollment RSE ON RSRSEL.idRuleSetLearningPathEnrollment = RSE.idRuleSetLearningPathEnrollment
		LEFT JOIN tblLearningPath LP ON LP.idLearningPath = RSE.idLearningPath
		WHERE RSRSEL.idRuleSetToRuleSetLearningPathEnrollmentLink IS NOT NULL
		AND (
				(@LearningPathsRecordCount = 0)
				OR 
				(@LearningPathsRecordCount > 0 AND EXISTS (SELECT 1 FROM @LearningPaths CP WHERE CP.id = RSE.idLearningPath))
			)
		-- NO RULES SHOULD BE EVALUATED OR MATCHED FOR RULESET ENROLLMENTS THAT ARE NOT WITHIN LIFESPAN
		AND (RSE.dtStart <= @utcNow)
		AND (RSE.dtEnd IS NULL OR RSE.dtEnd > @utcNow)
		-- NO RULES SHOULD BE EVALUATED OR MATCHED FOR LEARNING PATHS THAT HAVE BEEN DELETED
		AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)

		END

	ELSE
		BEGIN

		INSERT INTO #RuleSetLearningPathEnrollmentRules
		(
			idSite,
			idRuleSet,
			idRuleSetLearningPathEnrollment,
			idLearningPath,
			[priority],
			isAny,
			field,
			operator,
			value
		)
		SELECT
			RS.idSite,
			RS.idRuleSet,
			RSRSEL.idRuleSetLearningPathEnrollment,
			RSE.idLearningPath,
			RSE.[priority],
			RS.isAny,
			R.userField,
			R.operator,
			CASE WHEN R.textValue IS NOT NULL THEN 
				REPLACE(R.textValue, '''', '''''')
			ELSE
				CASE WHEN R.dateValue IS NOT NULL THEN
					CONVERT(NVARCHAR(255), R.dateValue)
				ELSE
					CASE WHEN R.numValue IS NOT NULL THEN
						CONVERT(NVARCHAR(255), R.numValue)
					ELSE
						CASE WHEN R.bitValue IS NOT NULL THEN
							CONVERT(NVARCHAR(255), R.bitValue)
						ELSE
							NULL
						END
					END
				END
			END
		FROM tblRuleSet RS
		LEFT JOIN tblRule R ON R.idRuleSet = RS.idRuleSet		
		LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSRSEL ON RSRSEL.idRuleSet = RS.idRuleSet
		LEFT JOIN tblRuleSetLearningPathEnrollment RSE ON RSRSEL.idRuleSetLearningPathEnrollment = RSE.idRuleSetLearningPathEnrollment
		LEFT JOIN tblLearningPath LP ON LP.idLearningPath = RSE.idLearningPath
		WHERE RSRSEL.idRuleSetToRuleSetLearningPathEnrollmentLink IS NOT NULL
		AND (
				(@LearningPathsRecordCount = 0)
				OR 
				(@LearningPathsRecordCount > 0 AND EXISTS (SELECT 1 FROM @LearningPaths CP WHERE CP.id = RSE.idLearningPath))
			)
		-- NO RULES SHOULD BE EVALUATED OR MATCHED FOR LEARNING PATHS THAT HAVE BEEN DELETED
		AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)

		END

	/* DECLARE VARIABLES FOR BUILDING RULE EXECUTION QUERIES */
	DECLARE @ruleSetEnrollmentsWithRuleSets TABLE (id INT, idSite INT)
	DECLARE @ruleSetsForRuleSetEnrollment IDTable
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @idRuleSetLearningPathEnrollment INT
	DECLARE @idLearningPath INT
	DECLARE @priority INT
	DECLARE @idSite INT
	DECLARE @idRuleSet INT
	DECLARE @isAny BIT
	DECLARE @field NVARCHAR(25)
	DECLARE @operator NVARCHAR(25)
	DECLARE @value NVARCHAR(255)
	DECLARE @ruleCounter INT

	/* GET IDS OF RULESET ENROLLMENTS WITH RULESETS */
	INSERT INTO @ruleSetEnrollmentsWithRuleSets ( 
		id,
		idSite
	)
	SELECT DISTINCT
		idRuleSetLearningPathEnrollment,
		idSite
	FROM #RuleSetLearningPathEnrollmentRules

	/* CURSOR FOR RULESET ENROLLMENTS WITH RULESETS */
	DECLARE ruleSetEnrollmentsCursor CURSOR FOR
	SELECT DISTINCT
		RS.id,
		RSER.idLearningPath,
		RSER.[priority],
		RS.idSite
	FROM @ruleSetEnrollmentsWithRuleSets RS
	LEFT JOIN #RuleSetLearningPathEnrollmentRules RSER ON RSER.idRuleSetLearningPathEnrollment = id

	OPEN ruleSetEnrollmentsCursor

	FETCH NEXT FROM ruleSetEnrollmentsCursor INTO @idRuleSetLearningPathEnrollment, @idLearningPath, @priority, @idSite

	/* LOOP THROUGH RULESET ENROLLMENT RULESETS */
	WHILE (@@FETCH_STATUS = 0)

		BEGIN

		/* GET IDS OF RULESETS FOR THE RULESET ENROLLMENT */
		INSERT INTO @ruleSetsForRuleSetEnrollment
		( id )
		SELECT DISTINCT
			idRuleSet
		FROM #RuleSetLearningPathEnrollmentRules
		WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment

		/* CURSOR FOR RULESETS FOR THE RULESET ENROLLMENT */
		DECLARE ruleSetEnrollmentRuleSetsCursor CURSOR FOR
		SELECT DISTINCT
			id,
			RSER.isAny
		FROM @ruleSetsForRuleSetEnrollment
		LEFT JOIN #RuleSetLearningPathEnrollmentRules RSER ON RSER.idRuleSet = id

		OPEN ruleSetEnrollmentRuleSetsCursor

		FETCH NEXT FROM ruleSetEnrollmentRuleSetsCursor INTO @idRuleSet, @isAny
	
		/* LOOP THROUGH RULESETS FOR THE RULESET ENROLLMENT */
		WHILE (@@FETCH_STATUS = 0)

			BEGIN

			/* START QUERY SQL STRING FOR RULESET */
			SET @sql = 'INSERT INTO #UserToRuleSetLearningPathEnrollmentJoins (idRuleSetLearningPathEnrollment, [priority], idRuleSet, idLearningPath, idUser) SELECT ' + CONVERT(NVARCHAR, @idRuleSetLearningPathEnrollment) + ', ' + CONVERT(NVARCHAR, @priority) + ', ' + CONVERT(NVARCHAR, @idRuleSet) + ', ' + CONVERT(NVARCHAR, @idLearningPath) + ', idUser ' + 'FROM tblUser U WHERE ('
			
			/* RESET THE RULE COUNTER */
			SET @ruleCounter = 0

			/* CURSOR FOR RULESET RULES */
			DECLARE ruleSetRulesCursor CURSOR FOR
			SELECT
				field,
				operator,
				value
			FROM #RuleSetLearningPathEnrollmentRules
			WHERE idRuleSet = @idRuleSet

			OPEN ruleSetRulesCursor
		
			FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
	
			/* LOOP THROUGH RULESET RULES */
			WHILE (@@FETCH_STATUS = 0)

				BEGIN

				/* IF THIS IS NOT THE FIRST RULE, EXAMINE isAny LOGIC TO DETERMINE AND/OR OPERATOR */
				IF @ruleCounter > 0
					BEGIN
						IF @isAny = 1
							BEGIN
							SET @sql = @sql + ' OR '
							END
						ELSE
							BEGIN
							SET @sql = @sql + ' AND '
							END
					END

				/* 
				
				BUILD WHERE CLAUSE BASED ON OPERATOR - DIFFERENT LOGIC FOR GROUP 
				GROUP EVALUATES ON THE "name" IN THE BASE TABLE, NOT EVERY LANGUAGE JUST THE DEFAULT

				*/

				IF @field = 'group'
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					END

				ELSE
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' = ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' <> ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) < ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) < ' + '''' + @value + '''' + ')'
						END

					END
			
				/* INCREMENT RULE COUNTER */
				SET @ruleCounter = @ruleCounter + 1

				FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
				END

			/* CLOSE AND DEALLOCATE CURSOR FOR RULESET RULES */
			CLOSE ruleSetRulesCursor
			DEALLOCATE ruleSetRulesCursor

			/* APEND ANDs FOR THE SITE AND USER DELETED STATUS */
			SET @sql = @sql + ') AND (U.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)))'
			
			/* EXECUTE THE SQL TO INSERT RECORDS IN THE #UserToRuleSetLearningPathEnrollmentJoins TABLE */
			EXECUTE sp_executesql @sql
		
			FETCH NEXT FROM ruleSetEnrollmentRuleSetsCursor INTO @idRuleSet, @isAny
			END

		/* CLOSE AND DEALLOCATE CURSOR FOR RULESETS FOR THE RULESET ENROLLMENT */
		DELETE FROM @ruleSetsForRuleSetEnrollment
		CLOSE ruleSetEnrollmentRuleSetsCursor
		DEALLOCATE ruleSetEnrollmentRuleSetsCursor

		FETCH NEXT FROM ruleSetEnrollmentsCursor INTO @idRuleSetLearningPathEnrollment, @idLearningPath, @priority, @idSite
		END

	/* CLOSE AND DEALLOCATE CURSOR FOR RULESET ENROLLMENTS WITH RULESETS */
	CLOSE ruleSetEnrollmentsCursor
	DEALLOCATE ruleSetEnrollmentsCursor
				  
	/* RETURN RECORDS */
	IF (@executeForRevokeProecdure = 0)
		BEGIN

		SELECT DISTINCT
			C.idSite,
			URSEJ.idRuleSetLearningPathEnrollment,
			URSEJ.[priority],
			URSEJ.idRuleSet,
			URSEJ.idLearningPath,
			URSEJ.idUser
		FROM #UserToRuleSetLearningPathEnrollmentJoins URSEJ
		LEFT JOIN tblLearningPath C ON C.idLearningPath = URSEJ.idLearningPath
		LEFT JOIN tblUserToGroupLink UGL ON UGL.idUser = URSEJ.idUser
		WHERE (  
				  (
					  @filterBy = 'user' AND
					  (
						(@FiltersRecordCount = 0)
						OR 
						(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = URSEJ.idUser))
					  )
				  )
				  OR
				  (
					  @filterBy = 'group' AND
					  (
						(@FiltersRecordCount = 0)
						OR 
						(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = UGL.idGroup))
					  )
				  )
			  )
		AND EXISTS (
			SELECT MIN(URSEJ2.[priority])
			FROM #UserToRuleSetLearningPathEnrollmentJoins URSEJ2
			GROUP BY URSEJ2.idUser, URSEJ2.idLearningPath
			HAVING MIN(URSEJ2.[priority]) = URSEJ.[priority] -- having the 'highest' (lowest number) priority
				AND URSEJ2.idUser = URSEJ.idUser
				AND URSEJ2.idLearningPath = URSEJ.idLearningPath		
		)

		END

	ELSE
		BEGIN

		SELECT DISTINCT
			C.idSite,
			URSEJ.idRuleSetLearningPathEnrollment,
			URSEJ.[priority],
			URSEJ.idRuleSet,
			URSEJ.idLearningPath,
			URSEJ.idUser
		FROM #UserToRuleSetLearningPathEnrollmentJoins URSEJ
		LEFT JOIN tblLearningPath C ON C.idLearningPath = URSEJ.idLearningPath
		LEFT JOIN tblUserToGroupLink UGL ON UGL.idUser = URSEJ.idUser
		WHERE (  
				  (
					  @filterBy = 'user' AND
					  (
						(@FiltersRecordCount = 0)
						OR 
						(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = URSEJ.idUser))
					  )
				  )
				  OR
				  (
					  @filterBy = 'group' AND
					  (
						(@FiltersRecordCount = 0)
						OR 
						(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = UGL.idGroup))
					  )
				  )
			  )

		END

	/* DROP TEMPORARY TABLE(S) */
	DROP TABLE #RuleSetLearningPathEnrollmentRules
	DROP TABLE #UserToRuleSetLearningPathEnrollmentJoins

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO