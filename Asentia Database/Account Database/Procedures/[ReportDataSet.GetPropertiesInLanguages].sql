-- =====================================================================
-- PROCEDURE: [ReportDataSet.GetPropertiesInLanguages]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[ReportDataSet.GetPropertiesInLanguages]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportDataSet.GetPropertiesInLanguages]
GO

/*

Return all language specific properties for a given report dataset id.

*/

CREATE PROCEDURE [ReportDataSet.GetPropertiesInLanguages]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idReportDataSet		INT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141	

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDataset RDS
		WHERE RDS.idDataset = @idReportDataSet
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ReportDataSetGPIL_DetailsNotFound'
		RETURN 1
		END
	
	SELECT 
		L.code as [langString],
		RDSL.name,
		RDSL.[description]		
	FROM tblDatasetLanguage RDSL
	LEFT JOIN tblLanguage L ON L.idLanguage = RDSL.idLanguage
	WHERE RDSL.idDataset = @idReportDataSet
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO