-- =====================================================================
-- PROCEDURE: [Course.GetCourseMaterials]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.GetCourseMaterials]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.GetCourseMaterials]
GO

/*

Returns a recordset of course material ids and names that belong to a course.

*/

CREATE PROCEDURE [Course.GetCourseMaterials]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCourse				INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END


	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString	


	IF @searchParam IS NULL
			
		BEGIN

		SELECT
			DISTINCT
			DRI.idDocumentRepositoryItem,
			DRI.[filename],
			CASE WHEN DRIL.label IS NOT NULL THEN DRIL.label ELSE DRI.label END AS label, 
			CASE WHEN DRI.isPrivate IS NULL THEN CONVERT(BIT, 0) ELSE DRI.isPrivate END AS isPrivate,
			DRI.kb
		FROM tblDocumentRepositoryItem DRI
		LEFT JOIN tblDocumentRepositoryItemLanguage DRIL ON DRIL.idDocumentRepositoryItem = DRI.idDocumentRepositoryItem AND DRIL.idLanguage = @idCallerLanguage
		WHERE DRI.idSite = @idCallerSite
		AND DRI.idDocumentRepositoryObjectType = 2 -- 2 is a course material
		AND DRI.idObject = @idCourse
		AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)
		ORDER BY [filename]

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			DRI.idDocumentRepositoryItem,
			DRI.[filename],
			CASE WHEN DRIL.label IS NOT NULL THEN DRIL.label ELSE DRI.label END AS label, 
			CASE WHEN DRI.isPrivate IS NULL THEN CONVERT(BIT, 0) ELSE DRI.isPrivate END AS isPrivate,
			DRI.kb
		FROM tblDocumentRepositoryItem DRI
		INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItem, *, @searchParam) K ON K.[key] = DRI.idDocumentRepositoryItem
		LEFT JOIN tblDocumentRepositoryItemLanguage DRIL ON DRIL.idDocumentRepositoryItem = DRI.idDocumentRepositoryItem AND DRIL.idLanguage = @idCallerLanguage
		WHERE DRI.idSite = @idCallerSite
		AND DRI.idDocumentRepositoryObjectType = 2 -- 2 is a course material
		AND DRI.idObject = @idCourse
		AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)
		ORDER BY [filename]

		END
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO