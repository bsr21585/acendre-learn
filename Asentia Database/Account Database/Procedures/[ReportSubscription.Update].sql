-- =====================================================================
-- PROCEDURE: [ReportSubscription.Update]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportSubscription.Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportSubscription.Update]
GO

/*

Adds new Report or updates existing Report.

*/

CREATE PROCEDURE [ReportSubscription.Update]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idReportSubscription	INT				OUTPUT, 
	@idReport				INT,
	@dtStart				DATETIME,
	@recurInterval			INT,
	@recurTimeframe			NVARCHAR(4)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	/*
	
	save/update the data
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblReportSubscription
		WHERE idReport = @idReport
		AND idSite = @idCallerSite
		AND idUser = @idCaller
		) > 0

		BEGIN
		
		UPDATE tblReportSubscription 
			SET
				dtStart = @dtStart,
				dtNextAction = 
					CASE WHEN dtStart <> @dtStart THEN @dtStart
				ELSE
					CASE WHEN MAIN.actionDate is null THEN
						@dtStart
					ELSE
						CASE @recurTimeframe
						WHEN 'yyyy' THEN dateAdd(year, @recurInterval, MAIN.actionDate)
						WHEN 'm' THEN dateAdd(month, @recurInterval, MAIN.actionDate)
						WHEN 'ww' THEN dateAdd(week, @recurInterval, MAIN.actionDate)
						WHEN 'd' THEN dateAdd(day, @recurInterval, MAIN.actionDate)
						END
					END
				END,
				recurInterval = @recurInterval, 
				recurTimeframe = @recurTimeframe
			FROM tblReportSubscription, 
			(
			SELECT 
				RS.idReportSubscription, 
				CASE WHEN MAX(RSQ.dtAction) IS NULL THEN MAX(RS.dtStart) ELSE MAX(RSQ.dtAction) END AS actionDate
			FROM tblReportSubscription RS
			LEFT JOIN tblReportSubscriptionQueue RSQ ON RS.idReportSubscription = RSQ.idReportSubscription AND RSQ.dtSent IS NOT NULL
			GROUP BY RS.idReportSubscription
			) MAIN
		WHERE MAIN.idReportSubscription = tblReportSubscription.idReportSubscription
		AND tblReportSubscription.idReport = @idReport
		AND tblReportSubscription.idUser = @idCaller
				
		END
				
	ELSE		
		
		BEGIN
		
		-- insert the new Report Subscription
		
		INSERT INTO tblReportSubscription (
			idUser,
			idSite,
			idReport,
			dtStart,
			dtNextAction,
			recurInterval,
			recurTimeframe, 
			token
		)
		VALUES (
			@idCaller,
			@idCallerSite,
			@idReport,
			@dtStart,
			@dtStart,
			@recurInterval,
			@recurTimeframe, 
			NEWID()
		)
		
		-- get the new Report's id and return successfully
		
		SELECT @idReportSubscription = SCOPE_IDENTITY()
		
		END		

	SELECT @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO