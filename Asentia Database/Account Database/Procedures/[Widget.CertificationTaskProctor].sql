-- =====================================================================
-- PROCEDURE: [Widget.CertificationTaskProctor]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Widget.CertificationTaskProctor]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.CertificationTaskProctor]
GO

/*

Gets a listing of lesson data for certification task proctor widget.

*/

CREATE PROCEDURE [Widget.CertificationTaskProctor]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	-- NOTE THAT WE ARE NOT USING THE SEARCH, PAGINATION, OR SORT HERE BUT
	-- SINCE THESE ARE COMMON PARAMETERS FOR GRIDS, WE NEED TO KEEP THEM
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	determine if caller has "global" permission to proctor, i.e. is the caller an admin

	if the caller does have permission, we'll skip figuring out what he can proctor and
	just list everything that needs proctoring
	
	*/

	DECLARE @callerHasGlobalPermission BIT
	SET @callerHasGlobalPermission = [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL)

	-- THIS IS TEMPORARY
	IF @idCaller > 1
		BEGIN
		SET @callerHasGlobalPermission = 0
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	if the user does not have "global" permission then get the users the caller is a supervisor 
	of and the courses the caller is an expert for

	*/

	CREATE TABLE #AuthorizedUsers (idUser INT)

	IF (@callerHasGlobalPermission = 0)
		
		BEGIN
		
		INSERT INTO #AuthorizedUsers (idUser)
		SELECT DISTINCT idUser FROM tblUserToSupervisorLink WHERE idSupervisor = @idCaller

		END

	/*

	begin getting the grid data

	*/

	CREATE TABLE #CertificationTaskDataForProctor (
		[idData-CertificationModuleRequirement]		INT,
		idCertificationToUserLink					INT,
		idCertificationModuleRequirement			INT,
		idCertificationModule						INT,
		idCertification								INT,
		idUser										INT,
		certificationName							NVARCHAR(255),
		requirementName								NVARCHAR(255),		
		userDisplayName								NVARCHAR(255),
		completionDocumentationFilePath				NVARCHAR(255),
		dtSubmitted									DATETIME
	)

	-- get the grid data

	INSERT INTO #CertificationTaskDataForProctor (
		[idData-CertificationModuleRequirement],
		idCertificationToUserLink,
		idCertificationModuleRequirement,
		idCertificationModule,
		idCertification,
		idUser,
		certificationName,
		requirementName,
		userDisplayName,
		completionDocumentationFilePath,
		dtSubmitted
	)
	SELECT
		CRD.[idData-CertificationModuleRequirement],
		CRD.idCertificationToUserLink,
		CRD.idCertificationModuleRequirement,
		CM.idCertificationModule,
		C.idCertification,
		U.idUser,
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END,
		CASE WHEN CMRL.label IS NOT NULL THEN CMRL.label ELSE CMR.label END,
		U.displayName,
		CRD.completionDocumentationFilePath,
		CRD.dtSubmitted
	FROM [tblData-CertificationModuleRequirement] CRD
	LEFT JOIN tblCertificationToUserLink CUL ON CUL.idCertificationToUserLink = CRD.idCertificationToUserLink
	LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirement = CRD.idCertificationModuleRequirement
	LEFT JOIN tblCertificationModuleRequirementLanguage CMRL ON CMRL.idCertificationModuleRequirement = CMR.idCertificationModuleRequirement AND CMRL.idLanguage = @idCallerLanguage
	LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModuleRequirementSet = CMR.idCertificationModuleRequirementSet
	LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = CMRS.idCertificationModule
	LEFT JOIN tblCertification C ON C.idCertification = CM.idCertification
	LEFT JOIN tblCertificationLanguage CL ON CL.idCertification = C.idCertification AND CL.idLanguage = @idCallerLanguage	
	LEFT JOIN tblUser U ON U.idUser = CUL.idUser	
	WHERE CRD.idSite = @idCallerSite	
	AND ( -- user must have uploaded a task or it's a non-upload task that requires signoff
			(CMR.documentUploadFileType IS NOT NULL AND CMR.documentationApprovalRequired = 1 AND CRD.dtSubmitted IS NOT NULL)
			OR
			(CMR.documentUploadFileType IS NULL AND CMR.documentationApprovalRequired = 1)
		)
	AND CRD.dtCompleted IS NULL	 -- requirement data is not complete 
	AND CRD.idApprover IS NULL	 -- has not been proctored
	AND ( -- only allow proctoring for initial award or renweal within valid window
			(CUL.certificationTrack = 0 AND CUL.initialAwardDate IS NULL AND CUL.currentExpirationDate IS NULL)
			OR
			(CUL.certificationTrack = 1 AND CUL.lastExpirationDate IS NOT NULL AND GETUTCDATE() >= CUL.lastExpirationDate)
		)
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- users must not be deleted or pending
	AND U.isActive = 1 -- users must be active
	AND (	-- caller is an admin, or caller is not an admin and IS a supervisor for respective objects
			@callerHasGlobalPermission = 1			
			OR
			(
				@callerHasGlobalPermission = 0
				AND
				U.idUser IN (SELECT idUser FROM #AuthorizedUsers)
			)
		)

	-- return the rowcount and grid data

	SELECT COUNT(1) AS row_count
	FROM #CertificationTaskDataForProctor

	;WITH
		Keys AS (
			SELECT TOP (@pageNum * @pageSize)
				CTDFP.[idData-CertificationModuleRequirement],
				ROW_NUMBER() OVER (ORDER BY CTDFP.dtSubmitted)
				AS [row_number]
			FROM #CertificationTaskDataForProctor CTDFP
		),

		SelectedKeys AS (
			SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
				[idData-CertificationModuleRequirement], 
				[row_number]
			FROM Keys
			WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
		)
	SELECT
		CTDFP.[idData-CertificationModuleRequirement],
		CTDFP.idCertificationToUserLink,
		CTDFP.idCertificationModuleRequirement,
		CTDFP.idCertificationModule,
		CTDFP.idCertification,
		CTDFP.idUser,
		CTDFP.certificationName,
		CTDFP.requirementName,
		CTDFP.userDisplayName,
		CTDFP.completionDocumentationFilePath,
		CTDFP.dtSubmitted,
		SelectedKeys.[row_number]
	FROM SelectedKeys
	JOIN #CertificationTaskDataForProctor CTDFP ON CTDFP.[idData-CertificationModuleRequirement] = SelectedKeys.[idData-CertificationModuleRequirement]
	ORDER BY SelectedKeys.[row_number]

	-- drop temporary tables
	DROP TABLE #AuthorizedUsers
	DROP TABLE #CertificationTaskDataForProctor

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO