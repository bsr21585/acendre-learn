-- =====================================================================
-- PROCEDURE: [WebMeetingOrganizer.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[WebMeetingOrganizer.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [WebMeetingOrganizer.Save]
GO

/*

Adds new web meeting organizer or updates existing web meeting organizer.

*/

CREATE PROCEDURE [WebMeetingOrganizer.Save]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idWebMeetingOrganizer	INT				OUTPUT,
	@idUser					INT,
	@organizerType			INT,
	@username				NVARCHAR(255),
	@password				NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	save the data
	
	*/
	
	IF (@idWebMeetingOrganizer = 0 OR @idWebMeetingOrganizer IS NULL)
		
		BEGIN
		
		-- insert the new web meeting organizer
		
		INSERT INTO tblWebMeetingOrganizer (			
			idSite,
			idUser,
			organizerType,
			username,
			[password]
		)			
		VALUES (
			@idCallerSite,
			@idUser,
			@organizerType,
			@username,
			@password
		)
		
		-- get the new web meeting organizer's id
		
		SELECT @idWebMeetingOrganizer = SCOPE_IDENTITY()

		END
		
	ELSE
		
		BEGIN
		
		-- check that the web meeting organizer id exists
		IF (SELECT COUNT(1) FROM tblWebMeetingOrganizer WHERE idWebMeetingOrganizer = @idWebMeetingOrganizer AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idWebMeetingOrganizer = @idWebMeetingOrganizer
			SET @Return_Code = 1
			SET @Error_Description_Code = 'WebMeetingOrganizerSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing web meeting organizer's properties
		
		UPDATE tblWebMeetingOrganizer SET
			idUser = @idUser,
			organizerType = @organizerType,
			username = @username,
			[password] = @password
		WHERE idWebMeetingOrganizer = @idWebMeetingOrganizer
		AND idSite = @idCallerSite
		
		-- get the web meeting organizer's id 

		SELECT @idWebMeetingOrganizer = @idWebMeetingOrganizer
				
		END

	SET @Error_Description_Code = ''
	SET @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO