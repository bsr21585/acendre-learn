-- =====================================================================
-- PROCEDURE: [GroupEnrollment.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GroupEnrollment.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GroupEnrollment.Details]
GO

/*

Return all the properties for a given group enrollment id.

*/

CREATE PROCEDURE [GroupEnrollment.Details]
(
	@Return_Code						INT					OUTPUT,
	@Error_Description_Code				NVARCHAR(50)		OUTPUT,
	@idCallerSite						INT					= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT					= 0,
	
	@idGroupEnrollment					INT					OUTPUT,
	@idSite								INT					OUTPUT,
	@idCourse							INT					OUTPUT,
	@idGroup							INT					OUTPUT,
	@idTimezone							INT					OUTPUT,
	@isLockedByPrerequisites			BIT					OUTPUT,
	@dtStart							DATETIME			OUTPUT,
	@dtCreated							DATETIME			OUTPUT,
	@dueInterval						INT					OUTPUT,
	@dueTimeframe						NVARCHAR(4)			OUTPUT,
	@expiresFromStartInterval			INT					OUTPUT,
	@expiresFromStartTimeframe			NVARCHAR(4)			OUTPUT,
	@expiresFromFirstLaunchInterval		INT					OUTPUT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4)			OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblGroupEnrollment
		WHERE idGroupEnrollment = @idGroupEnrollment
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupEnrollmentDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data
	
	*/	
		
	SELECT 
		@idGroupEnrollment					=	GE.idGroupEnrollment,
		@idSite								=	GE.idSite,
		@idCourse							=	GE.idCourse,
		@idGroup							=	GE.idGroup,
		@idTimezone							=	GE.idTimezone,
		@isLockedByPrerequisites			=	GE.isLockedByPrerequisites,
		@dtStart							=	GE.dtStart,
		@dtCreated							=	GE.dtCreated,
		@dueInterval						=	GE.dueInterval,
		@dueTimeframe						=	GE.dueTimeframe,
		@expiresFromStartInterval			=	GE.expiresFromStartInterval,
		@expiresFromStartTimeframe			=	GE.expiresFromStartTimeframe,
		@expiresFromFirstLaunchInterval		=	GE.expiresFromFirstLaunchInterval,
		@expiresFromFirstLaunchTimeframe	=	GE.expiresFromFirstLaunchTimeframe
	FROM tblGroupEnrollment GE
	WHERE GE.idGroupEnrollment = @idGroupEnrollment
		
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupEnrollmentDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO