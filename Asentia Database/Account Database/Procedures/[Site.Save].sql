-- =====================================================================
-- PROCEDURE: [Site.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Site.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.Save]
GO

/*

***THIS procedure can only be called when setting the DEFAULT language for an object 

*/

CREATE PROCEDURE [Site.Save]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT				= 0,
	
	@idSite							INT				OUTPUT, 
	@hostname						NVARCHAR(255),
	@favicon						NVARCHAR(255),
	@password						NVARCHAR(512),
	@isActive						BIT,
	@dtExpires						DATETIME,
	@title							NVARCHAR(255),
	@company						NVARCHAR(255),
	@contactName					NVARCHAR(255),
	@contactEmail					NVARCHAR(255),
	@userLimit						INT,
	@kbLimit						INT,
	@languageString					NVARCHAR(10),
	@idTimezone						INT,
	@dtLicenseAgreementExecuted		DATETIME,
	@dtUserAgreementModified		DATETIME
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*
	
	get the language ID
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	/*
	
	validate the unique hostname
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSite 
		WHERE hostname = @hostname
		AND (
			@idSite IS NULL
			OR idSite <> @idSite
			)
		) > 0
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'SiteSave_HostNameNotUnique'
		RETURN 1
		END
	
	/*
	
	save the data
	
	*/
	
	IF (@idSite = 0 OR @idSite is null)
		
		BEGIN
		
		-- insert the new object
		
		INSERT INTO tblSite (
			hostname,
			favicon,
			[password],
			isActive,
			dtExpires,
			title,
			company,
			contactName,
			contactEmail,
			userLimit,
			kbLimit,
			idLanguage,
			idTimezone,
			dtLicenseAgreementExecuted,
			dtUserAgreementModified
		)			
		VALUES (
			@hostname,
			@favicon,
			dbo.GetHashedString('SHA1', @password),
			@isActive,
			@dtExpires,
			@title,
			@company,
			@contactName,
			@contactEmail,
			@userLimit,
			@kbLimit,
			@idLanguage,
			@idTimezone,
			@dtLicenseAgreementExecuted,
			@dtUserAgreementModified
		)
		
		-- get the new object's id and return successfully
		
		SELECT @idSite = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the catalog id exists
		
		IF (SELECT COUNT(1) FROM tblSite WHERE idSite = @idSite) < 1
			
			BEGIN
				SELECT @idSite = @idSite
				SELECT @Return_Code = 1
				SET @Error_Description_Code = 'SiteSave_RecordNotFound'
				RETURN 1
			END
			
		-- update existing object's properties
		
		UPDATE tblSite SET
			favicon = @favicon,
			isactive = @isActive,
			dtExpires = @dtExpires,
			title = @title,
			hostname = @hostname,
			company = @company,
			contactName = @contactName,
			contactEmail = @contactEmail,
			userLimit = @userLimit,
			kbLimit = @kbLimit,
			idLanguage = @idLanguage,
			idTimezone = @idTimezone,
			dtLicenseAgreementExecuted = @dtLicenseAgreementExecuted,
			dtUserAgreementModified = @dtUserAgreementModified
		WHERE idSite = @idSite
		
		IF (@password IS NOT NULL AND @password <> '')
			BEGIN
				UPDATE tblSite SET
					[password] = dbo.GetHashedString('SHA1', @password)
				WHERE idSite = @idSite
			END
		
		-- get the id and return successfully
		
		SELECT @idSite = @idSite
				
		END
		
	-- update the multi-language
	
	UPDATE tblSiteLanguage SET
		title = @title,
		company = @company
	WHERE idSite = @idSite
	AND idLanguage = @idLanguage
	
	-- insert multi-language
	
	INSERT INTO tblSiteLanguage (
		idSite,
		idLanguage,
		title,
		company
	)
	SELECT
		@idSite,
		@idLanguage,
		@title, 
		@company
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblSiteLanguage SL
		WHERE SL.idSite = @idSite
		AND SL.idLanguage = @idLanguage
	)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO