-- =====================================================================
-- PROCEDURE: [User.Save]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.Save]
GO

/*

Adds new user or updates existing user.

*/

CREATE PROCEDURE [User.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idUser					INT				OUTPUT, 
	@firstName				NVARCHAR (255),
	@middleName				NVARCHAR (255),
	@lastName				NVARCHAR (255),
	@avatar					NVARCHAR (255),
	@email					NVARCHAR (255),
	@username				NVARCHAR (512),
	@password				NVARCHAR (512),
	@idTimezone				INT,
	@objectGUID				UNIQUEIDENTIFIER,
	@activationGUID			UNIQUEIDENTIFIER,
	@distinguishedName		NVARCHAR (512),
	@isActive				BIT,
	@mustChangePassword		BIT,
	@dtExpires				DATETIME,
	@languageString			NVARCHAR (10),
	@employeeId				NVARCHAR (255), 
	@company				NVARCHAR (255), 
	@address				NVARCHAR (512), 
	@city					NVARCHAR (255), 
	@province				NVARCHAR (255), 
	@postalCode				NVARCHAR (25), 
	@country				NVARCHAR (50), 
	@phonePrimary			NVARCHAR (25), 
	@phoneWork				NVARCHAR (25), 
	@phoneFax				NVARCHAR (25), 
	@phoneHome				NVARCHAR (25), 
	@phoneMobile			NVARCHAR (25), 
	@phonePager				NVARCHAR (25), 
	@phoneOther				NVARCHAR (25), 
	@department				NVARCHAR (255), 
	@division				NVARCHAR (255), 
	@region					NVARCHAR (255), 
	@jobTitle				NVARCHAR (255), 
	@jobClass				NVARCHAR (255), 
	@gender					NVARCHAR (1), 
	@race					NVARCHAR (64), 
	@dtDOB					DATETIME,
	@dtHire					DATETIME,
	@dtTerm					DATETIME,
	@field00				NVARCHAR (255), 
	@field01				NVARCHAR (255), 
	@field02				NVARCHAR (255), 
	@field03				NVARCHAR (255), 
	@field04				NVARCHAR (255), 
	@field05				NVARCHAR (255), 
	@field06				NVARCHAR (255), 
	@field07				NVARCHAR (255), 
	@field08				NVARCHAR (255),  
	@field09				NVARCHAR (255),
	@field10				NVARCHAR (255),
	@field11				NVARCHAR (255),
	@field12				NVARCHAR (255),
	@field13				NVARCHAR (255),
	@field14				NVARCHAR (255),
	@field15				NVARCHAR (255),
	@field16				NVARCHAR (255),
	@field17				NVARCHAR (255),
	@field18				NVARCHAR (255),
	@field19				NVARCHAR (255),
	@isRegistrationApproved	BIT,
	@dtApproved				DATETIME,
	@dtDenied				DATETIME,
	@idApprover				INT,
	@rejectionComments		NVARCHAR(MAX),
	@disableLoginFromLoginForm	BIT,
	@optOutOfEmailNotifications	BIT,
	@excludeFromLeaderboards	BIT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the specified language is the default language for the site
	
	*/
	
	-- NOT DONE. USERS DO NOT HAVE LANGUAGE VARIATIONS
	
	/*
	
	validate uniqueness
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUser
		WHERE idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0) -- do not look at deleted users
		AND (
			username = @username
			OR objectGUID = @objectGUID
			OR activationGUID = @activationGUID
			)
		AND (
			@idUser IS NULL
			OR @idUser <> idUser
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'UserSave_UserNotUnique'
		RETURN 1 
		END

	/*

	get the language id for this user from the languageString that was passed

	*/

	DECLARE @idLanguage INT
	SELECT @idLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @languageString
	
	/*

	insert/update the user

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	IF (@idUser = 0 OR @idUser IS NULL)
		BEGIN
			
		-- insert the new user
		
		INSERT INTO tblUser
		(
			firstName,
			middleName,
			lastName,
			displayName,
			avatar,
			email,
			username,
			[password],
			idSite,
			idTimezone,
			objectGUID,
			activationGUID,
			distinguishedName,
			isActive,
			mustchangePassword,
			dtCreated,
			dtExpires,
			isDeleted,
			dtDeleted,
			idLanguage,
			dtModified,
			dtLastLogin,
			employeeID,
			company,
			[address],
			city,
			province,
			postalcode,
			country,
			phonePrimary,
			phoneWork,
			phoneFax,
			phoneHome,
			phoneMobile,
			phonePager,
			phoneOther,
			department,
			division,
			region,
			jobTitle,
			jobClass,
			gender,
			race,
			dtDOB,
			dtHire,
			dtTerm,
			field00,
			field01,
			field02,
			field03,
			field04,
			field05,
			field06,
			field07,
			field08,
			field09,
			field10,
			field11,
			field12,
			field13,
			field14,
			field15,
			field16,
			field17,
			field18,
			field19,
			isRegistrationApproved,
			dtApproved,
			dtDenied,
			idApprover,
			rejectionComments,
			disableLoginFromLoginForm,
			optOutOfEmailNotifications,
			excludeFromLeaderboards
		)
		VALUES
		(
			@firstName,
			@middleName,
			@lastName,
			@lastName + ', ' + @firstName + CASE WHEN @middleName IS NOT NULL AND @middleName <> '' THEN ' ' + @middleName ELSE '' END,
			@avatar,
			@email,
			@username,
			dbo.GetHashedString('SHA1', @password),
			@idCallerSite,
			@idTimezone,
			@objectGUID,
			@activationGUID,
			@distinguishedName,
			@isActive,
			@mustchangePassword,
			@utcNow,
			@dtExpires,
			0,
			NULL,
			@idLanguage,
			@utcNow,
			NULL,
			@employeeID,
			@company,
			@address,
			@city,
			@province,
			@postalcode,
			@country,
			@phonePrimary,
			@phoneWork,
			@phoneFax,
			@phoneHome,
			@phoneMobile,
			@phonePager,
			@phoneOther,
			@department,
			@division,
			@region,
			@jobTitle,
			@jobClass,
			@gender,
			@race,
			@dtDOB,
			@dtHire,
			@dtTerm,
			@field00,
			@field01,
			@field02,
			@field03,
			@field04,
			@field05,
			@field06,
			@field07,
			@field08,
			@field09,
			@field10,
			@field11,
			@field12,
			@field13,
			@field14,
			@field15,
			@field16,
			@field17,
			@field18,
			@field19,
			@isRegistrationApproved,
			@dtApproved,
			@dtDenied,
			@idApprover,
			@rejectionComments,
			@disableLoginFromLoginForm,
			@optOutOfEmailNotifications,
			@excludeFromLeaderboards
		)
		
		/*
		
		get the new user's id

		*/

		SET @idUser = SCOPE_IDENTITY()

		/*

		do the event log entry

		*/

		DECLARE @eventLogItem EventLogItemObjects
		DECLARE @idEvent INT

		IF (@isRegistrationApproved IS NULL OR @isRegistrationApproved = 1)
			SET @idEvent = 101
		ELSE
			SET @idEvent = 107

		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idUser, @idUser, @idUser

		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, @idEvent, @utcNow, @eventLogItem

		-- return successfully
		SET @Return_Code = 0 --(0 is 'success')
		SET @Error_Description_Code = ''
				
		END
		
	ELSE
		BEGIN
		
		-- check that the user id exists
		IF (SELECT COUNT(1) FROM tblUser WHERE idUser = @idUser AND idSite = @idCallerSite) < 1
			BEGIN                               
			
			SET @idUser = @idUser
			SET @Return_Code = 1 --(1 is 'details not found')
			SET @Error_Description_Code = 'UserSave_NoRecordFound'
			RETURN 1
			
			END
			
		
		-- update existing user's properties
		
		UPDATE tblUser SET
			firstName = @firstName,
			middleName = @middleName,
			lastName = @lastName,
			displayName = @lastName + ', ' + @firstName + CASE WHEN @middleName IS NOT NULL AND @middleName <> '' THEN ' ' + @middleName ELSE '' END,
			avatar = @avatar,
			email = @email,
			username = @username,
			idTimezone = @idTimezone,
			objectGUID = @objectGUID,
			activationGUID = @activationGUID,
			distinguishedName = @distinguishedName,
			isActive = @isActive,
			mustchangePassword = @mustChangePassword,
			dtExpires = @dtExpires,
			idLanguage = @idLanguage,
			dtModified = @utcNow,
			employeeID = @employeeId,
			company = @company,
			[address] = @address,
			city = @city,
			province = @province,
			postalcode = @postalCode,
			country = @country,
			phonePrimary = @phonePrimary,
			phoneWork = @phoneWork,
			phoneFax = @phoneFax,
			phoneHome = @phoneHome,
			phoneMobile = @phoneMobile,
			phonePager = @phonePager,
			phoneOther = @phoneOther,
			department = @department,
			division = @division,
			region = @region,
			jobTitle = @jobTitle,
			jobClass = @jobClass,
			gender = @gender,
			race = @race,
			dtDOB = @dtDOB,
			dtHire = @dtHire,
			dtTerm = @dtTerm,
			field00 = @field00,
			field01 = @field01,
			field02 = @field02,
			field03 = @field03,
			field04 = @field04,
			field05 = @field05,
			field06 = @field06,
			field07 = @field07,
			field08 = @field08,
			field09 = @field09,
			field10 = @field10,
			field11 = @field11,
			field12 = @field12,
			field13 = @field13,
			field14 = @field14,
			field15 = @field15,
			field16 = @field16,
			field17 = @field17,
			field18 = @field18,
			field19 = @field19,
			isRegistrationApproved = @isRegistrationApproved,
			dtApproved = @dtApproved,
			dtDenied = @dtDenied,
			idApprover = @idApprover,
			rejectionComments = @rejectionComments,
			disableLoginFromLoginForm = @disableLoginFromLoginForm,
			optOutOfEmailNotifications = @optOutOfEmailNotifications,
			excludeFromLeaderboards = @excludeFromLeaderboards
		WHERE idUser = @idUser
		
		-- update the user's password, if necessary
		
		IF (@password IS NOT NULL AND @password <> '')
			BEGIN
			
			UPDATE tblUser SET
				[password] = dbo.GetHashedString('SHA1', @password)
			WHERE idUser = @idUser
			
			END
		
		-- get the user's id and return successfully
		SET @idUser = @idUser
		SET @Return_Code = 0 --(0 is 'success')
		SET @Error_Description_Code = ''
				
		END
	END			
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
