-- =====================================================================
-- PROCEDURE: [Enrollment.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.Delete]
GO

/*

Deletes enrollments

*/

CREATE PROCEDURE [Enrollment.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@Enrollments			IDTable			READONLY,
	@writeToEventLog		BIT				= 1
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Enrollments) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'EnrollmentDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	DO NOT validate that all enrollments exist within the site as
	this may be called indirectly from automated "job" procedures
	
	*/
		
	/*
	
	Validate if any of the enrollments have already been completed.
	
	*/

	IF (
		SELECT COUNT(1)
		FROM tblEnrollment E
		WHERE E.dtCompleted IS NOT NULL 
		AND E.idEnrollment IN (
			SELECT EE.id
			FROM @Enrollments EE
			)
		) > 0

		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = 'EnrollmentDelete_EnrollmentCompleted'
		RETURN 1 
		END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()
		
	/*
	
	delete the enrollments and all dependent referenced table records
	
	*/
	
	-- delete SCORM interactions
	DELETE FROM [tblData-SCOInt] 
	WHERE [idData-SCO] IN (SELECT [idData-SCO] 
						   FROM [tblData-SCO] 
						   WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
													 FROM [tblData-Lesson] 
													 WHERE idEnrollment IN (SELECT id FROM @Enrollments)
													 )
						  )
	
	-- delete SCORM objectives
	DELETE FROM [tblData-SCOObj] 
	WHERE [idData-SCO] IN (SELECT [idData-SCO] 
						   FROM [tblData-SCO] 
						   WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
													 FROM [tblData-Lesson] 
													 WHERE idEnrollment IN (SELECT id FROM @Enrollments)
													)
						   )

	-- delete SCO lesson data
	DELETE FROM [tblData-SCO] 
	WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
						      FROM [tblData-Lesson] 
							  WHERE idEnrollment IN (SELECT id FROM @Enrollments)
							 )

	-- delete any "internally-launched" TinCan lesson data
	DELETE FROM [tblData-TinCanContextActivities] 
	WHERE [idData-TinCan] IN (SELECT [idData-TinCan] 
							  FROM [tblData-TinCan] 
							  WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
														FROM [tblData-Lesson] 
														WHERE idEnrollment IN (SELECT id FROM @Enrollments)
														)
							 )

	DELETE FROM [tblData-TinCan] WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
														   FROM [tblData-Lesson] 
														   WHERE idEnrollment IN (SELECT id FROM @Enrollments)
														   )

	-- delete task data
	DELETE FROM [tblData-HomeworkAssignment] WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
																	   FROM [tblData-Lesson] 
																	   WHERE idEnrollment IN (SELECT id FROM @Enrollments)
																	   )

	-- delete lesson data
	DELETE FROM [tblData-Lesson] 
	WHERE idEnrollment IN (SELECT id FROM @Enrollments)

	/*

	log the event log entries prior to actually deleting the enrollments so that we can log the information

	*/

	IF (@writeToEventLog = 1)
		BEGIN
		
		DECLARE @eventLogItems EventLogItemObjects

		INSERT INTO @eventLogItems (
			idSite,
			idObject, 
			idObjectRelated, 
			idObjectUser
		) 
		SELECT 
			E.idSite,
			E.idCourse,
			EE.id, 
			E.idUser
		FROM @Enrollments EE
		LEFT JOIN tblEnrollment E ON E.idEnrollment = EE.id

		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 203, @utcNow, @eventLogItems

		END

	-- delete enrollments
	DELETE FROM tblEnrollment  
	WHERE idEnrollment IN (SELECT id FROM @Enrollments)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO