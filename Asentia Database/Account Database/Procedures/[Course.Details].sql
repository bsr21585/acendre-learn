-- =====================================================================
-- PROCEDURE: [Course.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.Details]
GO

/*

Return all the properties for a given course id.

*/

CREATE PROCEDURE [Course.Details]
(
	@Return_Code									INT				OUTPUT,
	@Error_Description_Code							NVARCHAR(50)	OUTPUT,
	@idCallerSite									INT				= 0, --default if not specified
	@callerLangString								NVARCHAR(10),
	@idCaller										INT				= 0,
	
	@idCourse										INT				OUTPUT,
	@idSite											INT				OUTPUT, 
	@title											NVARCHAR(255)	OUTPUT,
	@coursecode										NVARCHAR(255)	OUTPUT,
	@revcode										NVARCHAR(32)	OUTPUT,
	@avatar											NVARCHAR(255)	OUTPUT,
	@cost											FLOAT			OUTPUT,	
	@credits										FLOAT           OUTPUT,
	@minutes										INT				OUTPUT,
	@shortDescription								NVARCHAR(512)	OUTPUT, 
	@longDescription								NVARCHAR(MAX)	OUTPUT,
	@objectives										NVARCHAR(MAX)	OUTPUT,
	@socialMedia									NVARCHAR(MAX)	OUTPUT, 
	@isPublished									BIT				OUTPUT,
	@isClosed										BIT				OUTPUT,
	@isLocked										BIT				OUTPUT,
	@isPrerequisiteAny								BIT				OUTPUT,
	@isFeedActive									BIT             OUTPUT,
	@isFeedModerated								BIT             OUTPUT,
	@isFeedOpenSubscription							BIT             OUTPUT,
	@dtCreated										DATETIME		OUTPUT,
	@dtModified										DATETIME		OUTPUT,
	@isDeleted										BIT				OUTPUT,
	@dtDeleted										DATETIME		OUTPUT,
	@order											INT				OUTPUT,
	@disallowRating									BIT				OUTPUT,
	@rating											FLOAT			OUTPUT,
	@votes											INT				OUTPUT,
	@forceLessonCompletionInOrder					BIT				OUTPUT,
	@requireFirstLessonToBeCompletedBeforeOthers	BIT				OUTPUT,
	@lockLastLessonUntilOthersCompleted				BIT				OUTPUT,
	@selfEnrollmentIsOneTimeOnly					BIT				OUTPUT,
	@selfEnrollmentDueInterval						INT				OUTPUT,
	@selfEnrollmentDueTimeframe						NVARCHAR(4)		OUTPUT,
	@selfEnrollmentExpiresFromStartInterval			INT				OUTPUT,
	@selfEnrollmentExpiresFromStartTimeframe		NVARCHAR(4)		OUTPUT,
	@selfEnrollmentExpiresFromFirstLaunchInterval	INT				OUTPUT,
	@selfEnrollmentExpiresFromFirstLaunchTimeframe	NVARCHAR(4)		OUTPUT,
	@searchTags										NVARCHAR(512)	OUTPUT, 
	@isCallingUserEnrolled							BIT				OUTPUT,
	@isSelfEnrollmentApprovalRequired				BIT				OUTPUT,
	@isApprovalAllowedByCourseExperts				BIT				OUTPUT,
	@isApprovalAllowedByUserSupervisor				BIT				OUTPUT,
	@shortcode										NVARCHAR(10)	OUTPUT,
	@isSelfEnrollmentLockedByPrerequisites			BIT				OUTPUT,
	@isMultipleSelfEnrollmentEligible				BIT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourse
		WHERE idCourse = @idCourse
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseDetails_NoRecordFound'
		RETURN 1
	    END
		
	/*

	get course subscription status

	*/
	
     DECLARE @isCoursefeedSubscriptionOpen BIT 
     SELECT @isCoursefeedSubscriptionOpen = CF.IsFeedOpenSubscription FROM tblCourse CF WHERE CF.idCourse = @idCourse AND CF.idSite = @idCallerSite 				

	/*
	
	get the data 
	
	*/
		
	SELECT
		@idCourse										= C.idCourse,
		@idSite											= C.idSite,
		@title											= C.title,
		@coursecode										= C.courseCode,
		@revcode										= C.revcode,
		@avatar											= C.avatar,
		@cost											= C.cost,
		@credits										= C.credits,
		@minutes										= C.[minutes],
		@shortDescription								= C.shortDescription,
		@longDescription								= C.longDescription,
		@objectives										= C.objectives,
		@socialMedia									= C.socialMedia,
		@isPublished									= C.isPublished,
		@isClosed										= CASE WHEN C.isClosed IS NOT NULL THEN C.isClosed ELSE 0 END,
		@isLocked										= CASE WHEN C.isLocked IS NOT NULL THEN C.isLocked ELSE 0 END,
		@isPrerequisiteAny								= CASE WHEN C.isPrerequisiteAny IS NOT NULL THEN C.isPrerequisiteAny ELSE 0 END,
		@isFeedActive									= C.isFeedActive,
		@isFeedModerated								= C.isFeedModerated,
		@isFeedOpenSubscription							= C.isFeedOpenSubscription,
		@dtCreated										= C.dtCreated,
		@dtModified										= C.dtModified,
		@isDeleted										= C.isDeleted,
		@dtDeleted										= C.dtDeleted,
		@order											= C.[order],
		@disallowRating									= CASE WHEN C.disallowRating IS NULL THEN 0 ELSE C.disallowRating END,
		@rating											= RATING.rating,
		@votes											= RATING.votes,
		@forceLessonCompletionInOrder					= CASE WHEN C.forceLessonCompletionInOrder IS NULL THEN 0 ELSE C.forceLessonCompletionInOrder END,
		@requireFirstLessonToBeCompletedBeforeOthers	= CASE WHEN C.requireFirstLessonToBeCompletedBeforeOthers IS NULL THEN 0 ELSE C.requireFirstLessonToBeCompletedBeforeOthers END,
		@lockLastLessonUntilOthersCompleted				= CASE WHEN C.lockLastLessonUntilOthersCompleted IS NULL THEN 0 ELSE C.lockLastLessonUntilOthersCompleted END,
		@selfEnrollmentIsOneTimeOnly					= C.selfEnrollmentIsOneTimeOnly,
		@selfEnrollmentDueInterval						= C.selfEnrollmentDueInterval,
		@selfEnrollmentDueTimeframe						= C.selfEnrollmentDueTimeframe,
		@selfEnrollmentExpiresFromStartInterval			= C.selfEnrollmentExpiresFromStartInterval,
		@selfEnrollmentExpiresFromStartTimeframe		= C.selfEnrollmentExpiresFromStartTimeframe,
		@selfEnrollmentExpiresFromFirstLaunchInterval	= C.selfEnrollmentExpiresFromFirstLaunchInterval,
		@selfEnrollmentExpiresFromFirstLaunchTimeframe	= C.selfEnrollmentExpiresFromFirstLaunchTimeframe,
		@searchTags										= C.searchTags,
		@isSelfEnrollmentApprovalRequired				= C.isSelfEnrollmentApprovalRequired,
		@isApprovalAllowedByCourseExperts				= C.isApprovalAllowedByCourseExperts,
		@isApprovalAllowedByUserSupervisor				= C.isApprovalAllowedByUserSupervisor,
		@shortcode										= C.shortcode		
	FROM tblCourse C
	LEFT JOIN (
		SELECT 
			idCourse, 
			ROUND(AVG(CAST(rating AS FLOAT)), 1) AS rating,
			COUNT(rating) AS votes
		FROM tblCourseRating
		WHERE rating IS NOT NULL
		GROUP BY idCourse
	) RATING ON RATING.idCourse = C.idCourse
	WHERE C.idCourse = @idCourse

	-- determine if the calling user is currently enrolled in the course
	IF (
		SELECT COUNT(1) 
		FROM tblEnrollment E 
		WHERE E.idCourse = @idCourse
		AND E.idUser = @idCaller 
		AND (
				(E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
				AND
				(E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
			)
		AND E.dtCompleted IS NULL) > 0
		BEGIN
		SET @isCallingUserEnrolled = 1
		END
	ELSE
		BEGIN
		SET @isCallingUserEnrolled = 0
		END

	-- determine if the calling user has ever been enrolled in the course and if they can enroll again
	IF (
		SELECT COUNT(1) 
		FROM tblEnrollment E 
		WHERE E.idCourse = @idCourse
		AND E.idUser = @idCaller
		AND E.idRuleSetEnrollment IS NULL
		AND E.idGroupEnrollment IS NULL) = 0
		BEGIN
		SET @isMultipleSelfEnrollmentEligible = 1
		END
		
	ELSE IF (
		SELECT COUNT(1) 
		FROM tblEnrollment E 
		WHERE E.idCourse = @idCourse
		AND E.idUser = @idCaller
		AND E.idRuleSetEnrollment IS NULL
		AND E.idGroupEnrollment IS NULL) > 0
		AND (@selfEnrollmentIsOneTimeOnly <> 0 OR @selfEnrollmentIsOneTimeOnly IS NOT NULL)
		BEGIN
		SET @isMultipleSelfEnrollmentEligible = 1
		END
	ELSE
		BEGIN
		SET @isMultipleSelfEnrollmentEligible = 0
		END

	-- return
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	-- determine if self-enrollment should be locked for prerequisites
	SET @isSelfEnrollmentLockedByPrerequisites = 0
	
	IF (@idCaller > 1) AND ((SELECT COUNT(1) FROM tblCourseToPrerequisiteLink WHERE idCourse = @idCourse) > 0)
		BEGIN
		
		IF @isPrerequisiteAny = 1
			BEGIN
			IF (SELECT COUNT(1) 
				FROM tblEnrollment 
				WHERE idUser = @idCaller 
				AND dtCompleted IS NOT NULL 
				AND idCourse IN (SELECT idPrerequisite FROM tblCourseToPrerequisiteLink WHERE idCourse = @idCourse)) = 0
				BEGIN
				SET @isSelfEnrollmentLockedByPrerequisites = 1
				END
			END
		ELSE
			BEGIN
			IF (SELECT COUNT(DISTINCT idCourse) 
				FROM tblEnrollment 
				WHERE idUser = @idCaller 
				AND dtCompleted IS NOT NULL 
				AND idCourse IN (SELECT idPrerequisite FROM tblCourseToPrerequisiteLink WHERE idCourse = @idCourse))
				<>
				(SELECT COUNT(1) FROM tblCourseToPrerequisiteLink WHERE idCourse = @idCourse)
				BEGIN
				SET @isSelfEnrollmentLockedByPrerequisites = 1
				END
			END

		END
	ELSE
		BEGIN
		SET @isSelfEnrollmentLockedByPrerequisites = 0
		END

	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO