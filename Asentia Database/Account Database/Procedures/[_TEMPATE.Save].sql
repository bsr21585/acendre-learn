-- =====================================================================
-- PROCEDURE: [[OBJECT].Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[[OBJECT].Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [[OBJECT].Save]
GO

/*

Adds new [OBJECT] or updates existing [OBJECT].
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [[OBJECT].Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@id[OBJECT]				INT				OUTPUT, 
	-- LIST OBJECT SPECIFIC PARAMS BELOW AND REMOVE THIS COMMENT, DO NOT INCLUDE @idSite AS A PARAM, IT SHOULD BE SET TO @idCallerSite
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
		
	/*
	
	validate caller permission
	
	*/
	
	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED


	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = '[OBJECT]Save_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/
			 
	/*

	PERFORM ANY VALIDATIONS FOR BUSINESS RULES - UNIQUENESS, XSS VULNERABILITIES FOR DESCTIPTIONS, ETC.
	REMOVE THIS COMMENT BLOCK, REPLACING IT WITH YOUR VALIDATIONS FOR THE OBJECT AND APPROPRIATE COMMENTS

	*/
		
	/*
	
	save the data
	
	*/
	
	IF (@id[OBJECT] = 0 OR @id[OBJECT] is null)
		
		BEGIN
		
		-- insert the new [OBJECT]
		
		INSERT INTO tbl[OBJECT] (
			idSite, 
			-- FIELDS - REMOVE THIS COMMENT
		)			
		VALUES (
			@idCallerSite, 
			-- FIELD PARAMS - REMOVE THIS COMMENT
		)
		
		-- get the new object id and return successfully
		
		SET @id[OBJECT] = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the object id exists and it's not deleted
		-- IF THE OBJECT'S TABLE DOES NOT HAVE DELETED FLAGS, REMOVE THE CHECK FOR DELETED FLAG, AND REMOVE THIS COMMENT
		IF (SELECT COUNT(1) FROM tbl[OBJECT] WHERE id[OBJECT] = @id[OBJECT] AND idSite = @idCallerSite AND (isDeleted IS NULL OR isDeleted = 0)) < 1
			
			BEGIN

			SET @id[OBJECT] = @id[OBJECT]
			SET @Return_Code = 1
			SET @Error_Description_Code = '[OBJECT]Save_NoRecordFound'
			RETURN 1

			END
			
		-- update existing [OBJECT]'s properties
		
		UPDATE tbl[OBJECT] SET
			-- FIELDS - REMOVE THIS COMMENT
		WHERE id[OBJECT] = @id[OBJECT] 
		
		-- get the [OBJECT]'s id
		
		SET @id[OBJECT] = @id[OBJECT]
				
		END
	
	/* 
	
	BEGIN LANGUAGE SPECIFIC INSERT/UPDATE
	IF THERE IS NO LANGUAGE TABLE FOR THE OBJECT, REMOVE ALL CODE BETWEEN "BEGIN LANGUAGE SPECIFIC INSERT/UPDATE"
	AND "END LANGUAGE SPECIFIC INSERT/UPDATE" COMMENT BLOCKS, INCLUDING THE COMMENT BLOCKS THEMSELVES.

	IF THERE IS A LANGUAGE SPECIFIC TABLE, REMOVE BOTH THE "BEGIN LANGUAGE SPECIFIC INSERT/UPDATE" AND "END 
	LANGUAGE SPECIFIC INSERT/UPDATE" COMMENT BLOCKS.

	*/
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tbl[OBJECT]Language _L WHERE _L.id[OBJECT] = @id[OBJECT] AND _L.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tbl[OBJECT]Language SET
			-- LANGUAGE SPECIFIC FIELDS - REMOVE THIS COMMENT
		WHERE id[OBJECT] = @id[OBJECT]
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tbl[OBJECT]Language (
			idSite,
			id[OBJECT],
			idLanguage,
			-- LANGUAGE SPECIFIC FIELDS - REMOVE THIS COMMENT
		)
		SELECT
			@idCallerSite,
			@id[OBJECT],
			@idDefaultLanguage,
			-- LANGUAGE SPECIFIC FIELD PARAMS - REMOVE THIS COMMENT
		WHERE NOT EXISTS (
			SELECT 1
			FROM tbl[OBJECT]Language _L
			WHERE _L.id[OBJECT] = @id[OBJECT]
			AND _L.idLanguage = @idDefaultLanguage
		)

		END
	/* 
	
	END LANGUAGE SPECIFIC INSERT/UPDATE

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO