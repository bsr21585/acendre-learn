-- =====================================================================
-- PROCEDURE: [User.GetRoles]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.GetRoles]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.GetRoles]
GO

/*

Returns a recordset of role ids and names that a user is a member of.

*/

CREATE PROCEDURE [User.GetRoles]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idUser					INT,
	@excludeAutoJoined		BIT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString
		
	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		IF @excludeAutoJoined = 1

			BEGIN

			SELECT
				DISTINCT
				R.idRole, 
				CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name,
				(SELECT COUNT(1) FROM tblUserToRoleLink RULES_URL WHERE RULES_URL.idUser = @idUser AND RULES_URL.idRole = R.idRole AND RULES_URL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblRole R
			LEFT JOIN tblUserToRoleLink URL ON URL.idRole = R.idRole
			LEFT JOIN tblRoleLanguage RL ON RL.idRole = R.idRole AND RL.idLanguage = @idCallerLanguage
			WHERE (R.idSite = @idCallerSite OR R.idSite = 1) -- allow roles belonging to site id 1 to be viewed
			AND EXISTS (SELECT 1 FROM tblUserToRoleLink URL
						WHERE URL.idUser = @idUser
						AND URL.idRole = R.idRole
						AND URL.idRuleSet IS NULL)
			ORDER BY name

			END

		ELSE

			BEGIN

			SELECT
				DISTINCT
				R.idRole, 
				CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name,
				(SELECT COUNT(1) FROM tblUserToRoleLink RULES_URL WHERE RULES_URL.idUser = @idUser AND RULES_URL.idRole = R.idRole AND RULES_URL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblRole R
			LEFT JOIN tblUserToRoleLink URL ON URL.idRole = R.idRole
			LEFT JOIN tblRoleLanguage RL ON RL.idRole = R.idRole AND RL.idLanguage = @idCallerLanguage
			WHERE (R.idSite = @idCallerSite OR R.idSite = 1) -- allow roles belonging to site id 1 to be viewed
			AND EXISTS (SELECT 1 FROM tblUserToRoleLink URL
						WHERE URL.idUser = @idUser
						AND URL.idRole = R.idRole)
			ORDER BY name

			END

		END

	ELSE

		BEGIN

		IF @excludeAutoJoined = 1

			BEGIN

			SELECT
				DISTINCT
				R.idRole, 
				CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name,
				(SELECT COUNT(1) FROM tblUserToRoleLink RULES_URL WHERE RULES_URL.idUser = @idUser AND RULES_URL.idRole = R.idRole AND RULES_URL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblRole R
			INNER JOIN CONTAINSTABLE(tblRole, *, @searchParam) K ON K.[key] = R.idRole
			LEFT JOIN tblUserToRoleLink URL ON URL.idRole = R.idRole
			LEFT JOIN tblRoleLanguage RL ON RL.idRole = R.idRole AND RL.idLanguage = @idCallerLanguage
			WHERE (R.idSite = @idCallerSite OR R.idSite = 1) -- allow roles belonging to site id 1 to be viewed
			AND EXISTS (SELECT 1 FROM tblUserToRoleLink URL
						WHERE URL.idUser = @idUser
						AND URL.idRole = R.idRole
						AND URL.idRuleSet IS NULL)
			ORDER BY name

			END

		ELSE

			BEGIN

			SELECT
				DISTINCT
				R.idRole, 
				CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name,
				(SELECT COUNT(1) FROM tblUserToRoleLink RULES_URL WHERE RULES_URL.idUser = @idUser AND RULES_URL.idRole = R.idRole AND RULES_URL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblRole R
			INNER JOIN CONTAINSTABLE(tblRole, *, @searchParam) K ON K.[key] = R.idRole
			LEFT JOIN tblUserToRoleLink URL ON URL.idRole = R.idRole
			LEFT JOIN tblRoleLanguage RL ON RL.idRole = R.idRole AND RL.idLanguage = @idCallerLanguage
			WHERE (R.idSite = @idCallerSite OR R.idSite = 1) -- allow roles belonging to site id 1 to be viewed
			AND EXISTS (SELECT 1 FROM tblUserToRoleLink URL
						WHERE URL.idUser = @idUser
						AND URL.idRole = R.idRole)
			ORDER BY name

			END

		END
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	