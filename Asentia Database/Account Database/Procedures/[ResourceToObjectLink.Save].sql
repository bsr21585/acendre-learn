-- =====================================================================
-- PROCEDURE: [ResourceToObjectLink.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ResourceToObjectLink.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ResourceToObjectLink.Save]
GO

/*

adds new resource with object or updates existing resource with object.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [ResourceToObjectLink.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idResourceToObject		INT				OUTPUT,
	@idResource             INT,
	@idObject               INT,
	@objectType				NVARCHAR(255),
	@dtStart		        DATETIME,
	@dtEnd	                DATETIME,
	@isOutsideUse			BIT,
	@isMaintenance			BIT
	)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the specified language is the default language for the site
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1)
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			RETURN 1 
		END
	*/	
		

	
	
	/*
	
	save the data
	
	*/
	
	IF (@idResourceToObject = 0 OR @idResourceToObject IS NULL)
		
		BEGIN
		
		/*
	
		check that the resource already booked
		
		*/
		IF(		
			SELECT COUNT(1)
			FROM tblResourceToObjectLink
			WHERE (idResource=@idResource
			OR idResource=(select idParentResource from tblResource where idResource=@idResource)
			OR idResource in (select idResource from tblResource where idParentResource=@idResource))
			AND (	(@dtStart <= dtStart AND @dtEnd >= dtEnd)
					OR(@dtStart	 BETWEEN dtStart AND  dtEnd)
					OR(@dtEnd	 BETWEEN dtStart AND  dtEnd)
				)	
			AND idSite = @idCallerSite	
		 )>0
		 BEGIN
		 SELECT @Return_Code = 3
		 SET @Error_Description_Code = 'RToOLinkSave_ResourceAlreadyBookedForGivenTimeSpan'
		 RETURN 1
		 END
		
		-- insert the new resource with object
		
		INSERT INTO tblResourceToObjectLink (
			idSite, 
			idResource,
			idObject,
			objectType,
			dtStart,
			dtEnd,
			isOutsideUse,
			isMaintenance
		)			
		VALUES (
			@idCallerSite, 
			@idResource,
			@idObject,
			@objectType,
			@dtStart,
			@dtEnd,
			@isOutsideUse,
			@isMaintenance
			)
		
		-- get the new ResourceToObjectLink id and return successfully
		
		SELECT @idResourceToObject = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the Resource with object id exists 
		IF (SELECT COUNT(1) FROM tblResourceToObjectLink WHERE id = @idResourceToObject AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idResourceToObject = @idResourceToObject
			SELECT @Return_Code = 1
			SET @Error_Description_Code = 'ResourceToObjectLinkSave_NoRecordFound'  
			RETURN 1

			END
			
			
		
		/*
	
		check the resource booking update availablity
		
		*/
		IF(		
			SELECT COUNT(1)
			FROM tblResourceToObjectLink
			WHERE (idResource=@idResource
			OR idResource=(select idParentResource from tblResource where idResource=@idResource)
			OR idResource in (select idResource from tblResource where idParentResource=@idResource))
			AND (	(@dtStart <= dtStart AND @dtEnd >= dtEnd)
					OR(@dtStart	 BETWEEN dtStart AND  dtEnd)
					OR(@dtEnd	 BETWEEN dtStart AND  dtEnd)
				)	
			AND id!=@idResourceToObject	
			AND idSite = @idCallerSite	
		 )>0
		 BEGIN
		 SELECT @Return_Code = 3
		 SET @Error_Description_Code = 'RToOLinkSave_ResourceAlreadyBookedForGivenTimeSpan'
		 RETURN 1
		 END
		 
		
		-- update existing  properties
		
		UPDATE tblResourceToObjectLink SET
			idResource	 = @idResource,
			idObject	 = @idObject,
			objectType   = @ObjectType,
		    dtStart	     = @dtStart,
			dtEnd        = @dtEnd,
			isOutsideUse = @isOutsideUse,
			isMaintenance= @isMaintenance
		WHERE id = @idResourceToObject
		AND idSite = @idCallerSite	
		
		-- get the Resource's id 
		SELECT @idResourceToObject = @idResourceToObject
				
		END
		

	
	SELECT @Error_Description_Code = ''
	SELECT @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO