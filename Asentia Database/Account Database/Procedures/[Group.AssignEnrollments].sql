-- =====================================================================
-- PROCEDURE: [Group.AssignEnrollments]

/*

Group Enrollment Inheritence Rules

1. User has no enrollment(s) of the course, manually-assigned or otherwise.
	- Grant Group-inherited Enrollment

2. User has a manually-assigned (direct) or Group-inherited enrollment that has been completed.
	- Do nothing.

3. User has a manually-assigned (direct) enrollment that has expired.
	- Grant Group-inherited Enrollment

4. User has a Group-inherited enrollment that has expired.
	- Do nothing.

5. User has a manually-assigned (direct) or Group-inherited enrollment that is currently active.
	- Do nothing.

6. User has a RuleSet-inherited enrollment.
	- If one-time, active or expired; change the Enrollment's parameters to match the parameters of the Group Enrollment.
	- If one-time, completed, grant a new Group-inherited Enrollment.
	- If recurring, look at the most recent instance.
		- If active or expired, change the Enrollment's parameters to match the parameters of the Group Enrollment.
		- If completed, grant a new Group-inherited Enrollment. The presence of the new Group-inherited should stop recurrence of the RuleSet-inherited.



IMPORTANT NOTE: 

SYNCHRONIZATION OF COURSE DATA TO ENROLLMENT, AND SYNCHRONIZATION OF LESSON DATA TO ENROLLMENT 
WILL ALL TAKE PLACE IN OTHER PROCEDURES. THIS PROCEDURE ONLY HANDLES THE ASSIGNMENT OF ENROLLMENTS.

*/

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.AssignEnrollments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.AssignEnrollments]
GO

/*

Assigns group enrollments to group members.

Notes:

@Groups  -  IDTable of Group ids to assign Enrollments for.
		    Required to be passed, but not required to be populated.
		    If not populated, this will be executed on ALL Groups
		    that have Group Enrollments.

*/

CREATE PROCEDURE [Group.AssignEnrollments]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,

	@Groups						IDTable			READONLY
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* CHECK GROUP LIST FOR RECORDS */
	DECLARE @GroupsRecordCount INT
	SELECT @GroupsRecordCount = COUNT(1) FROM @Groups

	/*

	Get all of the group enrollment matched users for the group(s) and dump the results into a temp table.

	*/

	CREATE TABLE #GroupEnrollmentMatches
	(
		idSite INT,
		idGroupEnrollment INT,
		idCourse INT,
		idUser INT
	)

	INSERT INTO #GroupEnrollmentMatches
	(
		idSite,
		idGroupEnrollment,
		idCourse,
		idUser
	)
	SELECT DISTINCT
		GE.idSite,
		GE.idGroupEnrollment,
		GE.idCourse,
		UGL.idUser
	FROM tblGroupEnrollment GE
	LEFT JOIN tblCourse C ON C.idCourse = GE.idCourse
	LEFT JOIN tblUserToGroupLink UGL ON UGL.idGroup = GE.idGroup
	WHERE (
			(@GroupsRecordCount = 0)
			OR
			(@GroupsRecordCount > 0 AND EXISTS (SELECT 1 FROM @Groups GG WHERE GG.id = GE.idGroup))
		  )
	AND (C.isDeleted IS NULL OR C.isDeleted = 0)
	AND UGL.idUser IS NOT NULL

	/*

	Get all enrollments for all users and courses returned by the group enrollment matches execution
	and place them in a temp table. We do this to create a working environment for applying the Group Enrollment
	inheritance rules so we are not constantly reading from (and therefore locking) the Enrollment table.

	*/

	DECLARE @MatchedCourses IDTable
	INSERT INTO @MatchedCourses (id) SELECT DISTINCT idCourse FROM #GroupEnrollmentMatches

	DECLARE @MatchedUsers IDTable
	INSERT INTO @MatchedUsers (id) SELECT DISTINCT idUser FROM #GroupEnrollmentMatches

	/* 
	
	Replica of tblEnrollment minus idTimezone, code, revcode, title, all intervals, and idActivityImport. 
	We may weed out more fields later as we discover they are not needed. 

	*/

	CREATE TABLE #CurrentEnrollments
	(
		idEnrollment				INT,
		idSite						INT,
		idCourse					INT,
		idUser						INT,
		idGroupEnrollment			INT,
		idRuleSetEnrollment			INT,
		isLockedByPrerequisites		BIT,
		dtStart						DATETIME,
		dtDue						DATETIME,
		dtExpiresFromStart			DATETIME,
		dtExpiresFromFirstLaunch	DATETIME,
		dtFirstLaunch				DATETIME,
		dtCompleted					DATETIME,
		dtCreated					DATETIME,
		dtLastSynchronized			DATETIME
	)

	INSERT INTO #CurrentEnrollments
	(
		idEnrollment,
		idSite,
		idCourse,
		idUser,
		idGroupEnrollment,
		idRuleSetEnrollment,
		isLockedByPrerequisites,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtExpiresFromFirstLaunch,
		dtFirstLaunch,
		dtCompleted,
		dtCreated,
		dtLastSynchronized
	)
	SELECT
		E.idEnrollment,
		E.idSite,
		E.idCourse,
		E.idUser,
		E.idGroupEnrollment,
		E.idRuleSetEnrollment,
		E.isLockedByPrerequisites,
		E.dtStart,
		E.dtDue,
		E.dtExpiresFromStart,
		E.dtExpiresFromFirstLaunch,
		E.dtFirstLaunch,
		E.dtCompleted,
		E.dtCreated,
		E.dtLastSynchronized
	FROM tblEnrollment E
	WHERE E.idCourse IN (SELECT id FROM @MatchedCourses)
	AND E.idUser IN (SELECT id FROM @MatchedUsers)
	AND E.idActivityImport IS NULL -- WE SHOULD WEED OUT ENROLLMENTS CREATED FROM AN ACTIVITY IMPORT, FOR NOW

	/*

	Get UTC NOW.

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	Eliminate Group Enrollment matches where the user already has a manually-assigned (direct or group)
	enrollment of the course that is currently active or completed. This is the enforcement of Rules #2,
	#4, and #5, where the action is "do nothing," which means no Group Enrollment will be granted.

	*/

	DELETE FROM #GroupEnrollmentMatches
	WHERE idUser IN (
					 SELECT idUser FROM
				     #CurrentEnrollments CE
					 WHERE CE.idUser = #GroupEnrollmentMatches.idUser
					 AND CE.idCourse = #GroupEnrollmentMatches.idCourse
					 AND (
							(	-- active
								CE.dtCompleted IS NULL
								AND (CE.dtExpiresFromStart IS NULL OR CE.dtExpiresFromStart < @utcNow)
								AND (CE.dtExpiresFromFirstLaunch IS NULL OR CE.dtExpiresFromFirstLaunch < @utcNow)
							)
							OR
							(	-- completed
								CE.dtCompleted IS NOT NULL
							)
							OR
							(	-- Group-inherited and expired
								CE.dtCompleted IS NULL
								AND (CE.dtExpiresFromStart >= @utcNow OR CE.dtExpiresFromFirstLaunch >= @utcNow)
								AND CE.idGroupEnrollment IS NOT NULL
							)
						 )
					 AND CE.idRuleSetEnrollment IS NULL
					)
	AND idCourse IN (
				     SELECT idCourse FROM
				     #CurrentEnrollments CE
					 WHERE CE.idUser = #GroupEnrollmentMatches.idUser
					 AND CE.idCourse = #GroupEnrollmentMatches.idCourse
					 AND (
							(	-- active
								CE.dtCompleted IS NULL
								AND (CE.dtExpiresFromStart IS NULL OR CE.dtExpiresFromStart < @utcNow)
								AND (CE.dtExpiresFromFirstLaunch IS NULL OR CE.dtExpiresFromFirstLaunch < @utcNow)
							)
							OR
							(	-- completed
								CE.dtCompleted IS NOT NULL
							)
							OR
							(	-- Group-inherited and expired
								CE.dtCompleted IS NULL
								AND (CE.dtExpiresFromStart >= @utcNow OR CE.dtExpiresFromFirstLaunch >= @utcNow)
								AND CE.idGroupEnrollment IS NOT NULL
							)
						 )
					 AND CE.idRuleSetEnrollment IS NULL
					)



	/*

	Create temporary table for NEW INSERTS.

	*/

	CREATE TABLE #NewInserts
	(
		idSite				INT,
		idGroupEnrollment	INT,
		idCourse			INT,
		idUser				INT
	)

	/*

	IMPORTANT NOTE: The INSERTS below are followed by DELETES from the GroupEnrollmentMatches
	table, which appear to be redundant and at first glance look like they could just be one 
	statement at the end. But it is necessary to do a DELETE after each INSERT as a safeguard
	against potential duplicate enrollments being created later on.

	*/

	/*

	Put the Group enrollment matches where the user has no enrollments of the course(s) into the 
	NewInserts temporary table since we KNOW we have to grant these. Then, remove them from the
	GroupEnrollmentMatches working table. This is the enforcement of Rule #1.

	*/

	-- INSERT
	INSERT INTO #NewInserts
	(
		idSite,
		idGroupEnrollment,
		idCourse,
		idUser
	)
	SELECT
		GEM.idSite,
		GEM.idGroupEnrollment,
		GEM.idCourse,
		GEM.idUser
	FROM #GroupEnrollmentMatches GEM
	WHERE NOT EXISTS (
					  SELECT 1 FROM #CurrentEnrollments CE
					  WHERE CE.idUser = GEM.idUser
					  AND CE.idCourse = GEM.idCourse
					 )

	-- DELETE
	DELETE FROM #GroupEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #NewInserts NI
				  WHERE NI.idSite = #GroupEnrollmentMatches.idSite
				  AND NI.idGroupEnrollment = #GroupEnrollmentMatches.idGroupEnrollment
				  AND NI.idCourse = #GroupEnrollmentMatches.idCourse
				  AND NI.idUser = #GroupEnrollmentMatches.idUser
				 )

	/*

	Put the Group enrollment matches where the user has a manually-assigned (direct) enrollment 
	of the course that has not been completed, but has expired, into the NewInserts temporary table. 
	Then, remove them from the GroupEnrollmentMatches working table. This is the enforcement of Rule #3.

	*/

	-- INSERT
	INSERT INTO #NewInserts
	(
		idSite,
		idGroupEnrollment,
		idCourse,
		idUser
	)
	SELECT
		GEM.idSite,
		GEM.idGroupEnrollment,
		GEM.idCourse,
		GEM.idUser
	FROM #GroupEnrollmentMatches GEM
	WHERE EXISTS (
				  SELECT 1 FROM #CurrentEnrollments CE
				  WHERE CE.idUser = GEM.idUser
				  AND CE.idCourse = GEM.idCourse
				  AND CE.idRuleSetEnrollment IS NULL
				  AND CE.idGroupEnrollment IS NULL
				  AND CE.dtCompleted IS NULL
				  AND (CE.dtExpiresFromStart >= @utcNow OR CE.dtExpiresFromFirstLaunch >= @utcNow)
				 )

	-- DELETE
	DELETE FROM #GroupEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #NewInserts NI
				  WHERE NI.idSite = #GroupEnrollmentMatches.idSite
				  AND NI.idGroupEnrollment = #GroupEnrollmentMatches.idGroupEnrollment
				  AND NI.idCourse = #GroupEnrollmentMatches.idCourse
				  AND NI.idUser = #GroupEnrollmentMatches.idUser
				 )

	/*

	"COMPLEX" ENFORCEMENT OF RULE #6 WHERE WE REPLACE THE INHERITANCE OF RULESET ENROLLMENTS
	WITH THE INHERITANCE OF GROUP ENROLLMENTS

	NOTE THAT ALL GROUP ENROLLMENTS ARE ONE-TIME

	*/

	/*

		From RuleSet-inherited recurring to Group-inherited
		
		If current enrollment of old RuleSet Enrollment is active or expired:
			- Grant new Group-inherited Enrollment.
			- Link lesson data for current enrollment to the new Group-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.
		
		If current enrollment of old RuleSet Enrollment is completed:
			- Grant new Group-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.	

	*/

	/*

	Create temporary table to hold the information for Group Enrollments to be inherited
	where the old Enrollment is a recurring RuleSet Enrollment.

	*/

	CREATE TABLE #RecurringToGroupInherited
	(
		idSite						INT,
		idCourse					INT,
		idUser						INT,
		idGroupEnrollment			INT, -- Group Enrollment Id of the new Group Enrollment being inherited
		idRuleSetEnrollmentOld		INT, -- RuleSet Enrollment Id that is currently inherited
		idEnrollmentCurrent			INT, -- The latest Enrollment occurring from the currently inherited RuleSet Enrollment (latest is defined by the latest dtStart)
		removeOldRSELinks			BIT, -- Remove the link to the old RuleSet Enrollment from previous Enrollments?
		updateCurrentEnrollment		BIT  -- Do we update the current Enrollment with the data from the new RuleSet Enrollment?
										 -------------------------------------------------------------------------------------
										 -- If no, we just create a new Enrollment for the Group Enrollment. Note that when
										 -- we say "Grant new..." followed by "Link lesson data..." we're just going to update
										 -- the current Enrollment with the new Group Enrollment data. It is much less complicated
										 -- if we do it that way.
	)

	-- INSERT
	INSERT INTO #RecurringToGroupInherited
	(
		idSite,
		idCourse,
		idUser,
		idGroupEnrollment,
		idRuleSetEnrollmentOld,
		idEnrollmentCurrent,
		removeOldRSELinks,
		updateCurrentEnrollment
	)
	SELECT
		GEM.idSite,
		GEM.idCourse,
		GEM.idUser,
		GEM.idGroupEnrollment,
		CE.idRuleSetEnrollment,
		CE.idEnrollment,
		1,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			0
		ELSE
			1
		END
	FROM #GroupEnrollmentMatches GEM
	LEFT JOIN #CurrentEnrollments CE ON CE.idSite = GEM.idSite 
		AND CE.idUser = GEM.idUser 
		AND CE.idCourse = GEM.idCourse 
		AND CE.idGroupEnrollment <> GEM.idGroupEnrollment 
		AND CE.idRuleSetEnrollment IS NOT NULL
		AND NOT EXISTS (
						SELECT 1 FROM #CurrentEnrollments CE2 
						WHERE CE2.idUser = CE.idUser 
						AND CE2.idCourse = CE.idCourse
						AND CE2.idRuleSetEnrollment = CE.idRuleSetEnrollment
						AND CE2.dtStart > CE.dtStart
					   )
	LEFT JOIN tblRuleSetEnrollment RSEC ON RSEC.idRuleSetEnrollment = CE.idRuleSetEnrollment
	WHERE RSEC.recurInterval IS NOT NULL
	AND RSEC.recurInterval > 0

	-- DELETE
	DELETE FROM #GroupEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #RecurringToGroupInherited RTGI
				  WHERE RTGI.idSite = #GroupEnrollmentMatches.idSite
				  AND RTGI.idGroupEnrollment = #GroupEnrollmentMatches.idGroupEnrollment
				  AND RTGI.idCourse = #GroupEnrollmentMatches.idCourse
				  AND RTGI.idUser = #GroupEnrollmentMatches.idUser
				 )

	/*

		From RuleSet-inherited one-time to Group-inherited
		
		If current enrollment of old RuleSet Enrollment is active or expired:
			- Grant new Group-inherited Enrollment.
			- Link lesson data for current enrollment to the new Group-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.
		
		If current enrollment of old RuleSet Enrollment is completed:
			- Grant new Group-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.	

	*/

	/*

	Create temporary table to hold the information for Group Enrollments to be inherited
	where the old Enrollment is a one-time RuleSet Enrollment.

	*/

	CREATE TABLE #OneTimeToGroupInherited
	(
		idSite						INT,
		idCourse					INT,
		idUser						INT,
		idGroupEnrollment			INT, -- Group Enrollment Id of the new Group Enrollment being inherited
		idRuleSetEnrollmentOld		INT, -- RuleSet Enrollment Id that is currently inherited
		idEnrollmentCurrent			INT, -- The latest Enrollment occurring from the currently inherited RuleSet Enrollment (latest is defined by the latest dtStart)
		removeOldRSELinks			BIT, -- Remove the link to the old RuleSet Enrollment from previous Enrollments?
		updateCurrentEnrollment		BIT  -- Do we update the current Enrollment with the data from the new RuleSet Enrollment?
										 -------------------------------------------------------------------------------------
										 -- If no, we just create a new Enrollment for the Group Enrollment. Note that when
										 -- we say "Grant new..." followed by "Link lesson data..." we're just going to update
										 -- the current Enrollment with the new Group Enrollment data. It is much less complicated
										 -- if we do it that way.
	)

	-- INSERT
	INSERT INTO #OneTimeToGroupInherited
	(
		idSite,
		idCourse,
		idUser,
		idGroupEnrollment,
		idRuleSetEnrollmentOld,
		idEnrollmentCurrent,
		removeOldRSELinks,
		updateCurrentEnrollment
	)
	SELECT
		GEM.idSite,
		GEM.idCourse,
		GEM.idUser,
		GEM.idGroupEnrollment,
		CE.idRuleSetEnrollment,
		CE.idEnrollment,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			1
		ELSE
			0
		END,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			0
		ELSE
			1
		END
	FROM #GroupEnrollmentMatches GEM
	LEFT JOIN #CurrentEnrollments CE ON CE.idSite = GEM.idSite 
		AND CE.idUser = GEM.idUser 
		AND CE.idCourse = GEM.idCourse 
		AND CE.idGroupEnrollment <> GEM.idGroupEnrollment 
		AND CE.idRuleSetEnrollment IS NOT NULL
		AND NOT EXISTS (
						SELECT 1 FROM #CurrentEnrollments CE2 
						WHERE CE2.idUser = CE.idUser 
						AND CE2.idCourse = CE.idCourse
						AND CE2.idRuleSetEnrollment = CE.idRuleSetEnrollment
						AND CE2.dtStart > CE.dtStart
					   )
	LEFT JOIN tblRuleSetEnrollment RSEC ON RSEC.idRuleSetEnrollment = CE.idRuleSetEnrollment
	WHERE (RSEC.recurInterval IS NULL OR RSEC.recurInterval = 0)

	-- DELETE
	DELETE FROM #GroupEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #OneTimeToGroupInherited OTTGI
				  WHERE OTTGI.idSite = #GroupEnrollmentMatches.idSite
				  AND OTTGI.idGroupEnrollment = #GroupEnrollmentMatches.idGroupEnrollment
				  AND OTTGI.idCourse = #GroupEnrollmentMatches.idCourse
				  AND OTTGI.idUser = #GroupEnrollmentMatches.idUser
				 )

	/*

	Do work for RuleSet Recurring to Group-inherited Enrollment shift.

	*/

	-- UPDATE Group Enrollment link for inherited RuleSet Enrollments where the current Enrollment is active or expired.
	UPDATE tblEnrollment SET
		idGroupEnrollment = RTGI.idGroupEnrollment,
		idRuleSetEnrollment = NULL,
		idTimezone = GE.idTimezone,
		isLockedByPrerequisites = GE.isLockedByPrerequisites,
		dtStart = @utcNow,
		dtDue = CASE WHEN GE.dueInterval IS NOT NULL AND GE.dueInterval > 0 THEN
					dbo.[IDateAdd](GE.dueTimeframe, GE.dueInterval, @utcNow)
				ELSE
					NULL
				END,
		dtExpiresFromStart = CASE WHEN GE.expiresFromStartInterval IS NOT NULL AND GE.expiresFromStartInterval > 0 THEN
								dbo.[IDateAdd](GE.expiresFromStartTimeframe, GE.expiresFromStartInterval, @utcNow)
							 ELSE
								NULL
							 END,
		dtExpiresFromFirstLaunch = NULL,
		dtFirstLaunch = NULL,
		dtCreated = @utcNow,
		dtLastSynchronized = NULL,
		dueInterval = GE.dueInterval,
		dueTimeframe = GE.dueTimeframe,
		expiresFromStartInterval = GE.expiresFromStartInterval,
		expiresFromStartTimeframe = GE.expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = GE.expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = GE.expiresFromFirstLaunchTimeframe
	FROM #RecurringToGroupInherited RTGI
	LEFT JOIN tblGroupEnrollment GE ON GE.idGroupEnrollment = RTGI.idGroupEnrollment
	WHERE RTGI.idEnrollmentCurrent = idEnrollment
	AND RTGI.updateCurrentEnrollment = 1

	-- REMOVE old RuleSet Enrollment link for inherited Enrollments that have been completed.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = NULL
	FROM #RecurringToGroupInherited RTGI
	WHERE RTGI.idUser = tblEnrollment.idUser
	AND RTGI.idCourse = tblEnrollment.idCourse
	AND RTGI.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
	AND RTGI.idEnrollmentCurrent <> tblEnrollment.idEnrollment
	AND RTGI.removeOldRSELinks = 1
	AND tblEnrollment.dtCompleted IS NOT NULL

	-- DELETE Enrollments of the old RuleSet Enrollment where they are expired and NOT COMPLETED.
	-- Use the Enrollment.Delete procedure so that lesson data gets cleaned up.
	DECLARE @RecurringToGroupInheritedEnrollmentsToDelete IDTable

	INSERT INTO @RecurringToGroupInheritedEnrollmentsToDelete (
		id
	)
	SELECT
		idEnrollment
	FROM tblEnrollment
	WHERE idUser IN (
					 SELECT idUser FROM
				     #RecurringToGroupInherited RTGI
					 WHERE RTGI.idUser = tblEnrollment.idUser
					 AND RTGI.idCourse = tblEnrollment.idCourse
					 AND RTGI.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idCourse IN (
				     SELECT idCourse FROM
				     #RecurringToGroupInherited RTGI
					 WHERE RTGI.idUser = tblEnrollment.idUser
					 AND RTGI.idCourse = tblEnrollment.idCourse
					 AND RTGI.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idRuleSetEnrollment IN (
							    SELECT idRuleSetEnrollment FROM
								#RecurringToGroupInherited RTGI
								WHERE RTGI.idUser = tblEnrollment.idUser
								AND RTGI.idCourse = tblEnrollment.idCourse
								AND RTGI.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
							   )
	AND (
			(dtExpiresFromStart IS NOT NULL AND dtExpiresFromStart < @utcNow) 
			OR 
			(dtExpiresFromFirstLaunch IS NOT NULL AND dtExpiresFromFirstLaunch < @utcNow)
		)
	AND dtCompleted IS NULL

	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @RecurringToGroupInheritedEnrollmentsToDelete, 0

	-- INSERT Enrollments of the new Group Enrollment where we have not UPDATED an old one.
	INSERT INTO tblEnrollment
	(
		idSite,
		idCourse,
		idUser,
		idGroupEnrollment,
		idTimezone,
		isLockedByPrerequisites,
		code,
		revcode,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT
		RTGI.idSite,
		RTGI.idCourse,
		RTGI.idUser,
		GE.idGroupEnrollment,
		GE.idTimezone,
		GE.isLockedByPrerequisites,
		C.coursecode,
		C.revcode,
		C.title,
		@utcNow,
		CASE WHEN GE.dueInterval IS NOT NULL AND GE.dueInterval > 0 THEN
			dbo.[IDateAdd](GE.dueTimeframe, GE.dueInterval, @utcNow)
		ELSE
			NULL
		END,
		CASE WHEN GE.expiresFromStartInterval IS NOT NULL AND GE.expiresFromStartInterval > 0 THEN
			dbo.[IDateAdd](GE.expiresFromStartTimeframe, GE.expiresFromStartInterval, @utcNow)
		ELSE
			NULL
		END,
		dtCreated = @utcNow,
		GE.dueInterval,
		GE.dueTimeframe,
		GE.expiresFromStartInterval,
		GE.expiresFromStartTimeframe,
		GE.expiresFromFirstLaunchInterval,
		GE.expiresFromFirstLaunchTimeframe
	FROM #RecurringToGroupInherited RTGI
	LEFT JOIN tblGroupEnrollment GE ON GE.idGroupEnrollment = RTGI.idGroupEnrollment
	LEFT JOIN tblCourse C ON C.idCourse = RTGI.idCourse
	WHERE RTGI.updateCurrentEnrollment = 0

	/*

	Do work for RuleSet One-Time to Group-inherited Enrollment shift.

	*/

	-- UPDATE Group Enrollment link for inherited RuleSet Enrollments where the current Enrollment is active or expired.
	UPDATE tblEnrollment SET
		idGroupEnrollment = OTTGI.idGroupEnrollment,
		idRuleSetEnrollment = NULL,
		idTimezone = GE.idTimezone,
		isLockedByPrerequisites = GE.isLockedByPrerequisites,
		dtStart = @utcNow,
		dtDue = CASE WHEN GE.dueInterval IS NOT NULL AND GE.dueInterval > 0 THEN
					dbo.[IDateAdd](GE.dueTimeframe, GE.dueInterval, @utcNow)
				ELSE
					NULL
				END,
		dtExpiresFromStart = CASE WHEN GE.expiresFromStartInterval IS NOT NULL AND GE.expiresFromStartInterval > 0 THEN
								dbo.[IDateAdd](GE.expiresFromStartTimeframe, GE.expiresFromStartInterval, @utcNow)
							 ELSE
								NULL
							 END,
		dtExpiresFromFirstLaunch = NULL,
		dtFirstLaunch = NULL,
		dtCreated = @utcNow,
		dtLastSynchronized = NULL,
		dueInterval = GE.dueInterval,
		dueTimeframe = GE.dueTimeframe,
		expiresFromStartInterval = GE.expiresFromStartInterval,
		expiresFromStartTimeframe = GE.expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = GE.expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = GE.expiresFromFirstLaunchTimeframe
	FROM #OneTimeToGroupInherited OTTGI
	LEFT JOIN tblGroupEnrollment GE ON GE.idGroupEnrollment = OTTGI.idGroupEnrollment
	WHERE OTTGI.idEnrollmentCurrent = idEnrollment
	AND OTTGI.updateCurrentEnrollment = 1

	-- REMOVE old RuleSet Enrollment link for inherited Enrollments that have been completed.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = NULL
	FROM #OneTimeToGroupInherited OTTGI
	WHERE OTTGI.idUser = tblEnrollment.idUser
	AND OTTGI.idCourse = tblEnrollment.idCourse
	AND OTTGI.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
	AND OTTGI.idEnrollmentCurrent <> tblEnrollment.idEnrollment
	AND OTTGI.removeOldRSELinks = 1
	AND tblEnrollment.dtCompleted IS NOT NULL

	-- DELETE Enrollments of the old RuleSet Enrollment where they are expired and NOT COMPLETED.
	-- Use the Enrollment.Delete procedure so that lesson data gets cleaned up.
	DECLARE @OneTimeToGroupInheritedEnrollmentsToDelete IDTable

	INSERT INTO @OneTimeToGroupInheritedEnrollmentsToDelete (
		id
	)
	SELECT
		idEnrollment
	FROM tblEnrollment
	WHERE idUser IN (
					 SELECT idUser FROM
				     #OneTimeToGroupInherited OTTGI
					 WHERE OTTGI.idUser = tblEnrollment.idUser
					 AND OTTGI.idCourse = tblEnrollment.idCourse
					 AND OTTGI.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idCourse IN (
				     SELECT idCourse FROM
				     #OneTimeToGroupInherited OTTGI
					 WHERE OTTGI.idUser = tblEnrollment.idUser
					 AND OTTGI.idCourse = tblEnrollment.idCourse
					 AND OTTGI.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idRuleSetEnrollment IN (
							    SELECT idRuleSetEnrollment FROM
								#OneTimeToGroupInherited OTTGI
								WHERE OTTGI.idUser = tblEnrollment.idUser
								AND OTTGI.idCourse = tblEnrollment.idCourse
								AND OTTGI.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
							   )
	AND (
			(dtExpiresFromStart IS NOT NULL AND dtExpiresFromStart < @utcNow) 
			OR 
			(dtExpiresFromFirstLaunch IS NOT NULL AND dtExpiresFromFirstLaunch < @utcNow)
		)
	AND dtCompleted IS NULL

	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @OneTimeToGroupInheritedEnrollmentsToDelete, 0

	-- INSERT Enrollments of the new Group Enrollment where we have not UPDATED an old one.
	INSERT INTO tblEnrollment
	(
		idSite,
		idCourse,
		idUser,
		idGroupEnrollment,
		idTimezone,
		isLockedByPrerequisites,
		code,
		revcode,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT
		OTTGI.idSite,
		OTTGI.idCourse,
		OTTGI.idUser,
		GE.idGroupEnrollment,
		GE.idTimezone,
		GE.isLockedByPrerequisites,
		C.coursecode,
		C.revcode,
		C.title,
		@utcNow,
		CASE WHEN GE.dueInterval IS NOT NULL AND GE.dueInterval > 0 THEN
			dbo.[IDateAdd](GE.dueTimeframe, GE.dueInterval, @utcNow)
		ELSE
			NULL
		END,
		CASE WHEN GE.expiresFromStartInterval IS NOT NULL AND GE.expiresFromStartInterval > 0 THEN
			dbo.[IDateAdd](GE.expiresFromStartTimeframe, GE.expiresFromStartInterval, @utcNow)
		ELSE
			NULL
		END,
		dtCreated = @utcNow,
		GE.dueInterval,
		GE.dueTimeframe,
		GE.expiresFromStartInterval,
		GE.expiresFromStartTimeframe,
		GE.expiresFromFirstLaunchInterval,
		GE.expiresFromFirstLaunchTimeframe
	FROM #OneTimeToGroupInherited OTTGI
	LEFT JOIN tblGroupEnrollment GE ON GE.idGroupEnrollment = OTTGI.idGroupEnrollment
	LEFT JOIN tblCourse C ON C.idCourse = OTTGI.idCourse
	WHERE OTTGI.updateCurrentEnrollment = 0

	/*

	Do work for brand new INSERTS.

	*/

	INSERT INTO tblEnrollment
	(
		idSite,
		idCourse,
		idUser,
		idGroupEnrollment,
		idTimezone,
		isLockedByPrerequisites,
		code,
		revcode,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT
		NI.idSite,
		NI.idCourse,
		NI.idUser,
		NI.idGroupEnrollment,
		GE.idTimezone,
		GE.isLockedByPrerequisites,
		C.coursecode,
		C.revcode,
		C.title,
		@utcNow,
		CASE WHEN GE.dueInterval IS NOT NULL AND GE.dueInterval > 0 THEN
			dbo.[IDateAdd](GE.dueTimeframe, GE.dueInterval, @utcNow)
		ELSE
			NULL
		END,
		CASE WHEN GE.expiresFromStartInterval IS NOT NULL AND GE.expiresFromStartInterval > 0 THEN	
			dbo.[IDateAdd](GE.expiresFromStartTimeframe, GE.expiresFromStartInterval, @utcNow)
		ELSE
			NULL
		END,
		@utcNow,
		GE.dueInterval,
		GE.dueTimeframe,
		GE.expiresFromStartInterval,
		GE.expiresFromStartTimeframe,
		GE.expiresFromFirstLaunchInterval,
		GE.expiresFromFirstLaunchTimeframe
	FROM #NewInserts NI
	LEFT JOIN tblGroupEnrollment GE ON GE.idGroupEnrollment = NI.idGroupEnrollment
	LEFT JOIN tblCourse C ON C.idCourse = NI.idCourse

	/* 
	
	do the event log entries for brand new inserts only
	
	*/

	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT
		E.idSite,
		NI.idCourse,
		E.idEnrollment, 
		NI.idUser
	FROM #NewInserts NI
	LEFT JOIN tblEnrollment E ON E.idCourse = NI.idCourse AND E.idUser = NI.idUser
	WHERE E.dtCreated = @utcNow

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 201, @utcNow, @eventLogItems
				   
	/* DROP THE TEMPORARY TABLES */
	DROP TABLE #CurrentEnrollments
	DROP TABLE #GroupEnrollmentMatches
	DROP TABLE #NewInserts
	DROP TABLE #RecurringToGroupInherited
	DROP TABLE #OneTimeToGroupInherited

	/* FINAL STEP: REVOKE GROUP ENROLLMENTS */
	EXEC [Group.RevokeEnrollments] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @Groups

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO