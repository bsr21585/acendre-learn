-- =====================================================================
-- PROCEDURE: [System.ExecuteRuleSetLearningPathEnrollmentAssignment]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ExecuteRuleSetLearningPathEnrollmentAssignment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ExecuteRuleSetLearningPathEnrollmentAssignment]
GO

/*

Executes the RuleSetLearningPathEnrollment.AssignEnrollments procedure for the entire system.

*/
CREATE PROCEDURE [System.ExecuteRuleSetLearningPathEnrollmentAssignment]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,
	@LearningPaths				IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* 
	
	Execute the RuleSetLearningPathEnrollment.AssignEnrollments procedure with filters, the procedure gets the rules matches.
	
	*/	

	EXEC [RuleSetLearningPathEnrollment.AssignEnrollments] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @LearningPaths, @Filters, @filterBy

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO