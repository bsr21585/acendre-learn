-- =====================================================================
-- PROCEDURE: [RuleSetEnrollment.AssignEnrollments]

/*

RuleSet Enrollment Inheritence Rules

1. User has no enrollment(s) of the course, manually-assigned or otherwise.
	- Grant RuleSet-inherited Enrollment

2. User has a manually-assigned (direct or group) enrollment that has been completed.
	- Grant RuleSet-inherited Enrollment only if it is recurring, or if "force reassignment" flag is set.

3. User has a manually-assigned (direct or group) enrollment that has expired.
	- Grant RuleSet-inherited Enrollment

4. User has a manually-assigned (direct or group) enrollment that is currently active.
	- Do nothing.

5. User has a RuleSet-inherited enrollment that is the same as the RuleSet Enrollment they are matched to.
	- Do nothing.

6. User has a RuleSet-inherited enrollment that is different than the RuleSet Enrollment they are matched to.

	If new is recurring and old is recurring:
		
		If current enrollment of old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.
		
		If current enrollment of old RuleSet Enrollment is completed or expired:
			- Grant new RuleSet-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.		

	If new is recurring and old is one-time:
		
		If old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment
		
		If old RuleSet Enrollment is completed:
			- Grant new RuleSet-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from completed enrollment, i.e. make it appear manually-assigned.
		
		If old RuleSet Enrollment is expired:
			- Grant new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment.
	
	If new is one-time and old is recurring:
		
		If current enrollment of old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.
		
		If current enrollment of old RuleSet Enrollment is completed or expired:
			- Grant new RuleSet-inherited Enrollment only if "force reassignment" flag is set.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.
	
	If new is one-time and old is one-time:
		
		If old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment
		
		If old RuleSet Enrollment is completed:
			- Grant new RuleSet-inherited Enrollment only if "force reassignment" flag is set.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from completed enrollment, i.e. make it appear manually-assigned.
		
		If old RuleSet Enrollment is expired:
			- Grant new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment.


IMPORTANT NOTE: 

ASSIGNMENT OF ENROLLMENT RECURRENCES, SYNCHRONIZATION OF COURSE DATA TO ENROLLMENT, AND SYNCHRONIZATION
OF LESSON DATA TO ENROLLMENT WILL ALL TAKE PLACE IN OTHER PROCEDURES. THIS PROCEDURE ONLY HANDLES THE
ASSIGNMENT OF RULESET-INHERITED ENROLLMENTS.

*/

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.AssignEnrollments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetEnrollment.AssignEnrollments]
GO

/*

Assigns course RuleSet Enrollment enrollments to users based on rule matches and 
RuleSet Enrollment priorities using the rules outlined above.

Notes:

@Courses  - IDTable of Course ids to assign RuleSet Enrollments for.
		    Required to be passed, but not required to be populated.
		    If not populated, this will be executed on ALL Courses
		    that have RuleSet Enrollments.

@Filters  - IDTable of User or Group ids (see @filterBy param) to
		    assign RuleSet Enrollments for.
		    Required to be passed, but not required to be populated.
		    If not populated, this will be executed on ALL Users or
		    Groups (see @filterBy param).

@filterBy - Defines the object(s), User or Group, that we are executing
			this for. If "Group," we are just getting the users that are
			members of the specified Group(s) to execute this on. There
			is no direct RuleSet Enrollment to Group inheritance, just
			rules that can say "if user is a member of ...". Essentially,
			the "Group" filter only gets used when we are calling this
			after adding or removing User(s) from Group(s).

The parameters described above are all passed through to the [System.GetRuleSetCourseMatches]
procedure, which then returns a recordset of prioritized user-to-ruleset enrollment matches
filtered down to the Course(s) and User(s) resulting from applying the filters above.

*/

CREATE PROCEDURE [RuleSetEnrollment.AssignEnrollments]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,

	@Courses					IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/*

	Execute [System.GetRuleSetCourseMatches] and dump the results into a temp table.

	*/

	CREATE TABLE #RuleSetEnrollmentMatches
	(
		idSite INT, 
		idRuleSetEnrollment INT, 
		[priority] INT, 
		idRuleSet INT, 
		idCourse INT, 
		idUser INT
	)

	INSERT INTO #RuleSetEnrollmentMatches
	EXEC [System.GetRuleSetCourseMatches] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @Courses, @Filters, @filterBy

	/*

	Get all enrollments for all users and courses returned by the [System.GetRuleSetCourseMatches] execution
	and place them in a temp table. We do this to create a working environment for applying the RuleSet Enrollment
	inheritance rules so we are not constantly reading from (and therefore locking) the Enrollment table.

	*/

	DECLARE @MatchedCourses IDTable
	INSERT INTO @MatchedCourses (id) SELECT DISTINCT idCourse FROM #RuleSetEnrollmentMatches

	DECLARE @MatchedUsers IDTable
	INSERT INTO @MatchedUsers (id) SELECT DISTINCT idUser FROM #RuleSetEnrollmentMatches

	/* 
	
	Replica of tblEnrollment minus idTimezone, code, revcode, title, all intervals, and idActivityImport. 
	We may weed out more fields later as we discover they are not needed. 

	*/

	CREATE TABLE #CurrentEnrollments
	(
		idEnrollment				INT,
		idSite						INT,
		idCourse					INT,
		idUser						INT,
		idGroupEnrollment			INT,
		idRuleSetEnrollment			INT,
		isLockedByPrerequisites		BIT,
		dtStart						DATETIME,
		dtDue						DATETIME,
		dtExpiresFromStart			DATETIME,
		dtExpiresFromFirstLaunch	DATETIME,
		dtFirstLaunch				DATETIME,
		dtCompleted					DATETIME,
		dtCreated					DATETIME,
		dtLastSynchronized			DATETIME
	)

	INSERT INTO #CurrentEnrollments
	(
		idEnrollment,
		idSite,
		idCourse,
		idUser,
		idGroupEnrollment,
		idRuleSetEnrollment,
		isLockedByPrerequisites,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtExpiresFromFirstLaunch,
		dtFirstLaunch,
		dtCompleted,
		dtCreated,
		dtLastSynchronized
	)
	SELECT
		E.idEnrollment,
		E.idSite,
		E.idCourse,
		E.idUser,
		E.idGroupEnrollment,
		E.idRuleSetEnrollment,
		E.isLockedByPrerequisites,
		E.dtStart,
		E.dtDue,
		E.dtExpiresFromStart,
		E.dtExpiresFromFirstLaunch,
		E.dtFirstLaunch,
		E.dtCompleted,
		E.dtCreated,
		E.dtLastSynchronized
	FROM tblEnrollment E
	WHERE E.idCourse IN (SELECT id FROM @MatchedCourses)
	AND E.idUser IN (SELECT id FROM @MatchedUsers)
	AND E.idActivityImport IS NULL -- WE SHOULD WEED OUT ENROLLMENTS CREATED FROM AN ACTIVITY IMPORT, FOR NOW

	/*

	Get UTC NOW.

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	Eliminate RuleSet Enrollment matches where the user already has a manually-assigned (direct or group)
	enrollment of the course that is currently active. This is the enforcement of Rule #4, where the action
	is "do nothing," which means no RuleSet Enrollment will be granted.

	*/

	DELETE FROM #RuleSetEnrollmentMatches
	WHERE idUser IN (
					 SELECT idUser FROM
				     #CurrentEnrollments CE
					 WHERE CE.idUser = #RuleSetEnrollmentMatches.idUser
					 AND CE.idCourse = #RuleSetEnrollmentMatches.idCourse
					 AND CE.dtCompleted IS NULL
					 AND (CE.dtExpiresFromStart IS NULL OR CE.dtExpiresFromStart < @utcNow)
					 AND (CE.dtExpiresFromFirstLaunch IS NULL OR CE.dtExpiresFromFirstLaunch < @utcNow)
					 AND CE.idRuleSetEnrollment IS NULL
					)
	AND idCourse IN (
				     SELECT idCourse FROM
				     #CurrentEnrollments CE
					 WHERE CE.idUser = #RuleSetEnrollmentMatches.idUser
					 AND CE.idCourse = #RuleSetEnrollmentMatches.idCourse
					 AND CE.dtCompleted IS NULL
					 AND (CE.dtExpiresFromStart IS NULL OR CE.dtExpiresFromStart < @utcNow)
					 AND (CE.dtExpiresFromFirstLaunch IS NULL OR CE.dtExpiresFromFirstLaunch < @utcNow)
					 AND CE.idRuleSetEnrollment IS NULL
					)

	/*

	Eliminate RuleSet Enrollment matches where the user already has the same RuleSet enrollment of the
	course. This is the enforcement of Rule #5, where the action is "do nothing," which means no RuleSet 
	Enrollment will be granted.

	*/
	
	DELETE FROM #RuleSetEnrollmentMatches
	WHERE idUser IN (
					 SELECT idUser FROM
				     #CurrentEnrollments CE
					 WHERE CE.idUser = #RuleSetEnrollmentMatches.idUser
					 AND CE.idCourse = #RuleSetEnrollmentMatches.idCourse
					 AND CE.idRuleSetEnrollment IS NOT NULL
					)
	AND idCourse IN (
				     SELECT idCourse FROM
				     #CurrentEnrollments CE
					 WHERE CE.idUser = #RuleSetEnrollmentMatches.idUser
					 AND CE.idCourse = #RuleSetEnrollmentMatches.idCourse
					 AND CE.idRuleSetEnrollment IS NOT NULL
					)
	AND idRuleSetEnrollment IN (
							    SELECT idRuleSetEnrollment FROM
								#CurrentEnrollments CE
								WHERE CE.idUser = #RuleSetEnrollmentMatches.idUser
								AND CE.idCourse = #RuleSetEnrollmentMatches.idCourse
								AND CE.idRuleSetEnrollment = #RuleSetEnrollmentMatches.idRuleSetEnrollment
							   )

	/*

	Create temporary table for NEW INSERTS.

	*/

	CREATE TABLE #NewInserts
	(
		idSite				INT,
		idRuleSetEnrollment	INT,
		idCourse			INT,
		idUser				INT
	)

	/*

	IMPORTANT NOTE: The INSERTS below are followed by DELETES from the RuleSetEnrollmentMatches
	table, which appear to be redundant and at first glance look like they could just be one 
	statement at the end. But it is necessary to do a DELETE after each INSERT as a safeguard
	against potential duplicate enrollments being created later on.

	Essentially, the enforcement of our inheritance rules are done in a specific order and 
	folowed up by removing the match where we have already identified that an enrollment
	will be granted so that we do not have to go through even more complex logic than we
	already have to identify where an enrollment is to be granted.

	*/

	/*

	Put the RuleSet enrollment matches where the user has no enrollments of the course into the 
	NewInserts temporary table since we KNOW we have to grant these. Then, remove them from the
	RuleSetEnrollmentMatches working table. This is the enforcement of Rule #1.

	*/

	-- INSERT
	INSERT INTO #NewInserts
	(
		idSite,
		idRuleSetEnrollment,
		idCourse,
		idUser
	)
	SELECT
		RSEM.idSite,
		RSEM.idRuleSetEnrollment,
		RSEM.idCourse,
		RSEM.idUser
	FROM #RuleSetEnrollmentMatches RSEM
	WHERE NOT EXISTS (
					  SELECT 1 FROM #CurrentEnrollments CE
					  WHERE CE.idUser = RSEM.idUser
					  AND CE.idCourse = RSEM.idCourse
					 )

	-- DELETE
	DELETE FROM #RuleSetEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #NewInserts NI
				  WHERE NI.idSite = #RuleSetEnrollmentMatches.idSite
				  AND NI.idRuleSetEnrollment = #RuleSetEnrollmentMatches.idRuleSetEnrollment
				  AND NI.idCourse = #RuleSetEnrollmentMatches.idCourse
				  AND NI.idUser = #RuleSetEnrollmentMatches.idUser
				 )

	/*

	Put the RuleSet enrollment matches where the user has ONLY a manually-assigned (direct or group)
	enrollment of the course that has been completed into the NewInserts temporary table if the 
	RuleSet Enrollment is recurring or if the "force reassignment" flag is set. Then, remove them 
	from the RuleSetEnrollmentMatches working table. This is the enforcement of Rule #2.

	*/

	-- INSERT
	INSERT INTO #NewInserts
	(
		idSite,
		idRuleSetEnrollment,
		idCourse,
		idUser
	)
	SELECT
		RSEM.idSite,
		RSEM.idRuleSetEnrollment,
		RSEM.idCourse,
		RSEM.idUser
	FROM #RuleSetEnrollmentMatches RSEM
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEM.idRuleSetEnrollment
	WHERE (
		  EXISTS (
				  SELECT 1 FROM #CurrentEnrollments CE
				  WHERE CE.idUser = RSEM.idUser
				  AND CE.idCourse = RSEM.idCourse
				  AND CE.idRuleSetEnrollment IS NULL
				  AND CE.dtCompleted IS NOT NULL
				  AND RSE.recurInterval IS NOT NULL
				 )
		  OR EXISTS (
					 SELECT 1 FROM #CurrentEnrollments CE
					 WHERE CE.idUser = RSEM.idUser
					 AND CE.idCourse = RSEM.idCourse
					 AND CE.idRuleSetEnrollment IS NULL
					 AND CE.dtCompleted IS NOT NULL
					 AND RSE.recurInterval IS NULL
					 AND RSE.forceReassignment = 1
					)
		  )
	-- HAVING A RULESET ENROLLMENT OF ANY KIND DISQUALIFIES THE USER FROM GETTING AN INHERITANCE HERE
	AND NOT EXISTS (
				    SELECT 1 FROM #CurrentEnrollments CE
				    WHERE CE.idUser = RSEM.idUser
				    AND CE.idCourse = RSEM.idCourse
				    AND CE.idRuleSetEnrollment IS NOT NULL
				   )

	-- DELETE
	DELETE FROM #RuleSetEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #NewInserts NI
				  WHERE NI.idSite = #RuleSetEnrollmentMatches.idSite
				  AND NI.idRuleSetEnrollment = #RuleSetEnrollmentMatches.idRuleSetEnrollment
				  AND NI.idCourse = #RuleSetEnrollmentMatches.idCourse
				  AND NI.idUser = #RuleSetEnrollmentMatches.idUser
				 )

	-- ALSO DELETE RULESET ENROLLMENT MATCHES WHERE THERE IS NO FORCED REASSIGNMENT
	-- AND THERE IS A MANUALLY-ASSIGNED COMPLETED ENROLLMENT OF THE COURSE
	DELETE FROM #RuleSetEnrollmentMatches
	WHERE EXISTS (
			      SELECT 1 FROM #CurrentEnrollments CE
				  WHERE CE.idSite = #RuleSetEnrollmentMatches.idSite
				  AND CE.idRuleSetEnrollment IS NULL
				  AND CE.idCourse = #RuleSetEnrollmentMatches.idCourse
				  AND CE.idUser = #RuleSetEnrollmentMatches.idUser
				 )
	AND idRuleSetEnrollment IN (
							    SELECT idRuleSetEnrollment 
								FROM tblRuleSetEnrollment 
								WHERE (forceReassignment IS NULL OR forceReassignment = 0)
								AND (recurInterval IS NULL OR recurInterval = 0)
							   )

	/*

	Put the RuleSet enrollment matches where the user has a manually-assigned (direct or group)
	enrollment of the course that has not been completed, but has expired, into the NewInserts 
	temporary table. Then, remove them from the RuleSetEnrollmentMatches working table. 
	This is the enforcement of Rule #3.

	*/

	-- INSERT
	INSERT INTO #NewInserts
	(
		idSite,
		idRuleSetEnrollment,
		idCourse,
		idUser
	)
	SELECT
		RSEM.idSite,
		RSEM.idRuleSetEnrollment,
		RSEM.idCourse,
		RSEM.idUser
	FROM #RuleSetEnrollmentMatches RSEM
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEM.idRuleSetEnrollment
	WHERE EXISTS (
				  SELECT 1 FROM #CurrentEnrollments CE
				  WHERE CE.idUser = RSEM.idUser
				  AND CE.idCourse = RSEM.idCourse
				  AND CE.idRuleSetEnrollment IS NULL
				  AND CE.dtCompleted IS NULL
				  AND (CE.dtExpiresFromStart >= @utcNow OR CE.dtExpiresFromFirstLaunch >= @utcNow)
				 )

	-- DELETE
	DELETE FROM #RuleSetEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #NewInserts NI
				  WHERE NI.idSite = #RuleSetEnrollmentMatches.idSite
				  AND NI.idRuleSetEnrollment = #RuleSetEnrollmentMatches.idRuleSetEnrollment
				  AND NI.idCourse = #RuleSetEnrollmentMatches.idCourse
				  AND NI.idUser = #RuleSetEnrollmentMatches.idUser
				 )

	/*

	"COMPLEX" ENFORCEMENT OF RULE #6 WHERE WE REPLACE THE INHERITANCE OF ONE RULESET ENROLLMENT 
	WITH THE INHERITANCE OF ANOTHER RULESET ENROLLMENT

	*/

	/*

		New is recurring and old is recurring.
		
		If current enrollment of old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.
		
		If current enrollment of old RuleSet Enrollment is completed or expired:
			- Grant new RuleSet-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.	

	*/

	/*

	Create temporary table to hold the information for RuleSet Enrollments to be inherited
	where both the new and old are for recurring RuleSet Enrollments.

	*/

	CREATE TABLE #RecurringToRecurring
	(
		idSite						INT,
		idCourse					INT,
		idUser						INT,
		idRuleSetEnrollmentNew		INT, -- RuleSet Enrollment Id of the new RuleSet Enrollment being inherited
		idRuleSetEnrollmentOld		INT, -- RuleSet Enrollment Id that is currently inherited
		idEnrollmentCurrent			INT, -- The latest Enrollment occurring from the currently inherited RuleSet Enrollment (latest is defined by the latest dtStart)
		removeOldRSELinks			BIT, -- Remove the link to the old RuleSet Enrollment from previous Enrollments?
		updateCurrentEnrollment		BIT  -- Do we update the current Enrollment with the data from the new RuleSet Enrollment?
										 -------------------------------------------------------------------------------------
										 -- If no, we just create a new Enrollment for the RuleSet Enrollment. Note that when
										 -- we say "Grant new..." followed by "Link lesson data..." we're just going to update
										 -- the current Enrollment with the new RuleSet Enrollment data. It is much less complicated
										 -- if we do it that way.
	)

	-- INSERT
	INSERT INTO #RecurringToRecurring
	(
		idSite,
		idCourse,
		idUser,
		idRuleSetEnrollmentNew,
		idRuleSetEnrollmentOld,
		idEnrollmentCurrent,
		removeOldRSELinks,
		updateCurrentEnrollment
	)
	SELECT
		RSEM.idSite,
		RSEM.idCourse,
		RSEM.idUser,
		RSEM.idRuleSetEnrollment,
		CE.idRuleSetEnrollment,
		CE.idEnrollment,
		1,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			0
		ELSE
			CASE WHEN (CE.dtExpiresFromStart IS NOT NULL AND CE.dtExpiresFromStart < @utcNow) OR (CE.dtExpiresFromFirstLaunch IS NOT NULL AND CE.dtExpiresFromFirstLaunch < @utcNow) THEN
				0
			ELSE
				1
			END
		END
	FROM #RuleSetEnrollmentMatches RSEM
	LEFT JOIN #CurrentEnrollments CE ON CE.idSite = RSEM.idSite 
		AND CE.idUser = RSEM.idUser 
		AND CE.idCourse = RSEM.idCourse 
		AND CE.idRuleSetEnrollment <> RSEM.idRuleSetEnrollment 
		AND CE.idRuleSetEnrollment IS NOT NULL
		AND NOT EXISTS (
						SELECT 1 FROM #CurrentEnrollments CE2 
						WHERE CE2.idUser = CE.idUser 
						AND CE2.idCourse = CE.idCourse
						AND CE2.idRuleSetEnrollment = CE.idRuleSetEnrollment
						AND CE2.dtStart > CE.dtStart
					   )
	LEFT JOIN tblRuleSetEnrollment RSEN ON RSEN.idRuleSetEnrollment = RSEM.idRuleSetEnrollment
	LEFT JOIN tblRuleSetEnrollment RSEC ON RSEC.idRuleSetEnrollment = CE.idRuleSetEnrollment
	WHERE RSEN.recurInterval IS NOT NULL
	AND RSEN.recurInterval > 0
	AND RSEC.recurInterval IS NOT NULL
	AND RSEC.recurInterval > 0

	-- DELETE
	DELETE FROM #RuleSetEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #RecurringToRecurring RTR
				  WHERE RTR.idSite = #RuleSetEnrollmentMatches.idSite
				  AND RTR.idRuleSetEnrollmentNew = #RuleSetEnrollmentMatches.idRuleSetEnrollment
				  AND RTR.idCourse = #RuleSetEnrollmentMatches.idCourse
				  AND RTR.idUser = #RuleSetEnrollmentMatches.idUser
				 )

	/*

		New is recurring and old is one-time.
		
		If old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment
		
		If old RuleSet Enrollment is completed:
			- Grant new RuleSet-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from completed enrollment, i.e. make it appear manually-assigned.
		
		If old RuleSet Enrollment is expired:
			- Grant new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment.	

	*/

	/*

	Create temporary table to hold the information for RuleSet Enrollments to be inherited
	where the new is a recurring and the old is a one-time RuleSet Enrollment.

	*/

	CREATE TABLE #OneTimeToRecurring
	(
		idSite						INT,
		idCourse					INT,
		idUser						INT,
		idRuleSetEnrollmentNew		INT, -- RuleSet Enrollment Id of the new RuleSet Enrollment being inherited
		idRuleSetEnrollmentOld		INT, -- RuleSet Enrollment Id that is currently inherited
		idEnrollmentCurrent			INT, -- The latest Enrollment occurring from the currently inherited RuleSet Enrollment (latest is defined by the latest dtStart)
		removeOldRSELinks			BIT, -- Remove the link to the old RuleSet Enrollment from previous Enrollments?
		updateCurrentEnrollment		BIT  -- Do we update the current Enrollment with the data from the new RuleSet Enrollment?
										 -------------------------------------------------------------------------------------
										 -- If no, we just create a new Enrollment for the RuleSet Enrollment. Note that when
										 -- we say "Grant new..." followed by "Link lesson data..." we're just going to update
										 -- the current Enrollment with the new RuleSet Enrollment data. It is much less complicated
										 -- if we do it that way.
	)

	-- INSERT
	INSERT INTO #OneTimeToRecurring
	(
		idSite,
		idCourse,
		idUser,
		idRuleSetEnrollmentNew,
		idRuleSetEnrollmentOld,
		idEnrollmentCurrent,
		removeOldRSELinks,
		updateCurrentEnrollment
	)
	SELECT
		RSEM.idSite,
		RSEM.idCourse,
		RSEM.idUser,
		RSEM.idRuleSetEnrollment,
		CE.idRuleSetEnrollment,
		CE.idEnrollment,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			1
		ELSE
			0
		END,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			0
		ELSE
			CASE WHEN (CE.dtExpiresFromStart IS NOT NULL AND CE.dtExpiresFromStart < @utcNow) OR (CE.dtExpiresFromFirstLaunch IS NOT NULL AND CE.dtExpiresFromFirstLaunch < @utcNow) THEN
				0
			ELSE
				1
			END
		END
	FROM #RuleSetEnrollmentMatches RSEM
	LEFT JOIN #CurrentEnrollments CE ON CE.idSite = RSEM.idSite 
		AND CE.idUser = RSEM.idUser 
		AND CE.idCourse = RSEM.idCourse 
		AND CE.idRuleSetEnrollment <> RSEM.idRuleSetEnrollment 
		AND CE.idRuleSetEnrollment IS NOT NULL
		AND NOT EXISTS (
						SELECT 1 FROM #CurrentEnrollments CE2 
						WHERE CE2.idUser = CE.idUser 
						AND CE2.idCourse = CE.idCourse
						AND CE2.idRuleSetEnrollment = CE.idRuleSetEnrollment
						AND CE2.dtStart > CE.dtStart
					   )
	LEFT JOIN tblRuleSetEnrollment RSEN ON RSEN.idRuleSetEnrollment = RSEM.idRuleSetEnrollment
	LEFT JOIN tblRuleSetEnrollment RSEC ON RSEC.idRuleSetEnrollment = CE.idRuleSetEnrollment
	WHERE RSEN.recurInterval IS NOT NULL
	AND RSEN.recurInterval > 0
	AND (RSEC.recurInterval IS NULL OR RSEC.recurInterval = 0)

	-- DELETE
	DELETE FROM #RuleSetEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #OneTimeToRecurring OTTR
				  WHERE OTTR.idSite = #RuleSetEnrollmentMatches.idSite
				  AND OTTR.idRuleSetEnrollmentNew = #RuleSetEnrollmentMatches.idRuleSetEnrollment
				  AND OTTR.idCourse = #RuleSetEnrollmentMatches.idCourse
				  AND OTTR.idUser = #RuleSetEnrollmentMatches.idUser
				 )

	/*

		New is one-time and old is recurring.
		
		If current enrollment of old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.
		
		If current enrollment of old RuleSet Enrollment is completed or expired:
			- Grant new RuleSet-inherited Enrollment only if "force reassignment" flag is set.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from previously completed enrollments, i.e. make them appear manually-assigned.
			- Delete previously expired enrollments of the old RuleSet Enrollment.

	*/

	/*

	Create temporary table to hold the information for RuleSet Enrollments to be inherited
	where the new is a one-time and the old is a recurring RuleSet Enrollment.

	*/

	CREATE TABLE #RecurringToOneTime
	(
		idSite						INT,
		idCourse					INT,
		idUser						INT,
		idRuleSetEnrollmentNew		INT, -- RuleSet Enrollment Id of the new RuleSet Enrollment being inherited
		idRuleSetEnrollmentOld		INT, -- RuleSet Enrollment Id that is currently inherited
		idEnrollmentCurrent			INT, -- The latest Enrollment occurring from the currently inherited RuleSet Enrollment (latest is defined by the latest dtStart)
		removeOldRSELinks			BIT, -- Remove the link to the old RuleSet Enrollment from previous Enrollments?
		updateCurrentEnrollment		BIT  -- Do we update the current Enrollment with the data from the new RuleSet Enrollment?
										 -------------------------------------------------------------------------------------
										 -- If no, we just create a new Enrollment for the RuleSet Enrollment. Note that when
										 -- we say "Grant new..." followed by "Link lesson data..." we're just going to update
										 -- the current Enrollment with the new RuleSet Enrollment data. It is much less complicated
										 -- if we do it that way.
	)

	-- INSERT
	INSERT INTO #RecurringToOneTime
	(
		idSite,
		idCourse,
		idUser,
		idRuleSetEnrollmentNew,
		idRuleSetEnrollmentOld,
		idEnrollmentCurrent,
		removeOldRSELinks,
		updateCurrentEnrollment
	)
	SELECT
		RSEM.idSite,
		RSEM.idCourse,
		RSEM.idUser,
		RSEM.idRuleSetEnrollment,
		CE.idRuleSetEnrollment,
		CE.idEnrollment,
		1,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			0
		ELSE
			CASE WHEN (CE.dtExpiresFromStart IS NOT NULL AND CE.dtExpiresFromStart < @utcNow) OR (CE.dtExpiresFromFirstLaunch IS NOT NULL AND CE.dtExpiresFromFirstLaunch < @utcNow) THEN
				0
			ELSE
				1
			END
		END
	FROM #RuleSetEnrollmentMatches RSEM
	LEFT JOIN #CurrentEnrollments CE ON CE.idSite = RSEM.idSite 
		AND CE.idUser = RSEM.idUser 
		AND CE.idCourse = RSEM.idCourse 
		AND CE.idRuleSetEnrollment <> RSEM.idRuleSetEnrollment 
		AND CE.idRuleSetEnrollment IS NOT NULL
		AND NOT EXISTS (
						SELECT 1 FROM #CurrentEnrollments CE2 
						WHERE CE2.idUser = CE.idUser 
						AND CE2.idCourse = CE.idCourse
						AND CE2.idRuleSetEnrollment = CE.idRuleSetEnrollment
						AND CE2.dtStart > CE.dtStart
					   )
	LEFT JOIN tblRuleSetEnrollment RSEN ON RSEN.idRuleSetEnrollment = RSEM.idRuleSetEnrollment
	LEFT JOIN tblRuleSetEnrollment RSEC ON RSEC.idRuleSetEnrollment = CE.idRuleSetEnrollment
	WHERE (RSEN.recurInterval IS NULL OR RSEN.recurInterval = 0)
	AND RSEC.recurInterval IS NOT NULL
	AND RSEC.recurInterval > 0
	

	-- DELETE
	DELETE FROM #RuleSetEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #RecurringToOneTime RTOT
				  WHERE RTOT.idSite = #RuleSetEnrollmentMatches.idSite
				  AND RTOT.idRuleSetEnrollmentNew = #RuleSetEnrollmentMatches.idRuleSetEnrollment
				  AND RTOT.idCourse = #RuleSetEnrollmentMatches.idCourse
				  AND RTOT.idUser = #RuleSetEnrollmentMatches.idUser
				 )

	/*

		New is one-time and old is one-time.
		
		If old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment
		
		If old RuleSet Enrollment is completed:
			- Grant new RuleSet-inherited Enrollment only if "force reassignment" flag is set.
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from completed enrollment, i.e. make it appear manually-assigned.
		
		If old RuleSet Enrollment is expired:
			- Grant new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment.

	*/

	/*

	Create temporary table to hold the information for RuleSet Enrollments to be inherited
	where both the new and the old are one-time RuleSet Enrollments.

	*/

	CREATE TABLE #OneTimeToOneTime
	(
		idSite						INT,
		idCourse					INT,
		idUser						INT,
		idRuleSetEnrollmentNew		INT, -- RuleSet Enrollment Id of the new RuleSet Enrollment being inherited
		idRuleSetEnrollmentOld		INT, -- RuleSet Enrollment Id that is currently inherited
		idEnrollmentCurrent			INT, -- The latest Enrollment occurring from the currently inherited RuleSet Enrollment (latest is defined by the latest dtStart)
		removeOldRSELinks			BIT, -- Remove the link to the old RuleSet Enrollment from previous Enrollments?
		updateCurrentEnrollment		BIT  -- Do we update the current Enrollment with the data from the new RuleSet Enrollment?
										 -------------------------------------------------------------------------------------
										 -- If no, we just create a new Enrollment for the RuleSet Enrollment. Note that when
										 -- we say "Grant new..." followed by "Link lesson data..." we're just going to update
										 -- the current Enrollment with the new RuleSet Enrollment data. It is much less complicated
										 -- if we do it that way.
	)

	-- INSERT
	INSERT INTO #OneTimeToOneTime
	(
		idSite,
		idCourse,
		idUser,
		idRuleSetEnrollmentNew,
		idRuleSetEnrollmentOld,
		idEnrollmentCurrent,
		removeOldRSELinks,
		updateCurrentEnrollment
	)
	SELECT
		RSEM.idSite,
		RSEM.idCourse,
		RSEM.idUser,
		RSEM.idRuleSetEnrollment,
		CE.idRuleSetEnrollment,
		CE.idEnrollment,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			1
		ELSE
			0
		END,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			0
		ELSE
			CASE WHEN (CE.dtExpiresFromStart IS NOT NULL AND CE.dtExpiresFromStart < @utcNow) OR (CE.dtExpiresFromFirstLaunch IS NOT NULL AND CE.dtExpiresFromFirstLaunch < @utcNow) THEN
				0
			ELSE
				1
			END
		END
	FROM #RuleSetEnrollmentMatches RSEM
	LEFT JOIN #CurrentEnrollments CE ON CE.idSite = RSEM.idSite 
		AND CE.idUser = RSEM.idUser 
		AND CE.idCourse = RSEM.idCourse 
		AND CE.idRuleSetEnrollment <> RSEM.idRuleSetEnrollment 
		AND CE.idRuleSetEnrollment IS NOT NULL
		AND NOT EXISTS (
						SELECT 1 FROM #CurrentEnrollments CE2 
						WHERE CE2.idUser = CE.idUser 
						AND CE2.idCourse = CE.idCourse
						AND CE2.idRuleSetEnrollment = CE.idRuleSetEnrollment
						AND CE2.dtStart > CE.dtStart
					   )
	LEFT JOIN tblRuleSetEnrollment RSEN ON RSEN.idRuleSetEnrollment = RSEM.idRuleSetEnrollment
	LEFT JOIN tblRuleSetEnrollment RSEC ON RSEC.idRuleSetEnrollment = CE.idRuleSetEnrollment
	WHERE (RSEN.recurInterval IS NULL OR RSEN.recurInterval = 0)
	AND (RSEC.recurInterval IS NULL OR RSEC.recurInterval = 0)

	-- DELETE
	DELETE FROM #RuleSetEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #OneTimeToOneTime OTTOT
				  WHERE OTTOT.idSite = #RuleSetEnrollmentMatches.idSite
				  AND OTTOT.idRuleSetEnrollmentNew = #RuleSetEnrollmentMatches.idRuleSetEnrollment
				  AND OTTOT.idCourse = #RuleSetEnrollmentMatches.idCourse
				  AND OTTOT.idUser = #RuleSetEnrollmentMatches.idUser
				 )

	/*

	Do work for Recurring to Recurring RuleSet Enrollment shift.

	*/

	-- UPDATE RuleSet Enrollment link for inherited RuleSet Enrollments where the current Enrollment is still active.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = RTR.idRuleSetEnrollmentNew,
		idTimezone = RSE.idTimezone,
		isLockedByPrerequisites = RSE.isLockedByPrerequisites,
		dtStart = CASE WHEN RSE.isFixedDate = 1 THEN
					RSE.dtStart
				  ELSE
					CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
						dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
					ELSE
						@utcNow
					END
				  END,
		dtDue = CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
					CASE WHEN RSE.isFixedDate = 1 THEN
						dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, RSE.dtStart)
					ELSE
						CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
							dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
						ELSE
							dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
						END
					END
				ELSE
					NULL
				END,
		dtExpiresFromStart = CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
								CASE WHEN RSE.isFixedDate = 1 THEN
									dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
								ELSE
									CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
										dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
									ELSE
										dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, @utcNow)
									END
								END
							 ELSE
								NULL
							 END,
		dtExpiresFromFirstLaunch = NULL,
		dtFirstLaunch = NULL,
		dtCreated = @utcNow,
		dtLastSynchronized = NULL,
		dueInterval = RSE.dueInterval,
		dueTimeframe = RSE.dueTimeframe,
		expiresFromStartInterval = RSE.expiresFromStartInterval,
		expiresFromStartTimeframe = RSE.expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = RSE.expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = RSE.expiresFromFirstLaunchTimeframe
	FROM #RecurringToRecurring RTR
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RTR.idRuleSetEnrollmentNew
	WHERE RTR.idEnrollmentCurrent = idEnrollment
	AND RTR.updateCurrentEnrollment = 1

	-- REMOVE old RuleSet Enrollment link for inherited Enrollments that have been completed.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = NULL
	FROM #RecurringToRecurring RTR
	WHERE RTR.idUser = tblEnrollment.idUser
	AND RTR.idCourse = tblEnrollment.idCourse
	AND RTR.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
	AND RTR.idEnrollmentCurrent <> tblEnrollment.idEnrollment
	AND RTR.removeOldRSELinks = 1
	AND tblEnrollment.dtCompleted IS NOT NULL

	-- DELETE Enrollments of the old RuleSet Enrollment where they are expired and NOT COMPLETED.
	-- Use the Enrollment.Delete procedure so that lesson data gets cleaned up.
	DECLARE @RecurringToRecurringEnrollmentsToDelete IDTable

	INSERT INTO @RecurringToRecurringEnrollmentsToDelete (
		id
	)
	SELECT
		idEnrollment
	FROM tblEnrollment
	WHERE idUser IN (
					 SELECT idUser FROM
				     #RecurringToRecurring RTR
					 WHERE RTR.idUser = tblEnrollment.idUser
					 AND RTR.idCourse = tblEnrollment.idCourse
					 AND RTR.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idCourse IN (
				     SELECT idCourse FROM
				     #RecurringToRecurring RTR
					 WHERE RTR.idUser = tblEnrollment.idUser
					 AND RTR.idCourse = tblEnrollment.idCourse
					 AND RTR.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idRuleSetEnrollment IN (
							    SELECT idRuleSetEnrollment FROM
								#RecurringToRecurring RTR
								WHERE RTR.idUser = tblEnrollment.idUser
								AND RTR.idCourse = tblEnrollment.idCourse
								AND RTR.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
							   )
	AND (
			(dtExpiresFromStart IS NOT NULL AND dtExpiresFromStart < @utcNow) 
			OR 
			(dtExpiresFromFirstLaunch IS NOT NULL AND dtExpiresFromFirstLaunch < @utcNow)
		)
	AND dtCompleted IS NULL

	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @RecurringToRecurringEnrollmentsToDelete, 0

	-- INSERT Enrollments of the new RuleSet Enrollment where we have not UPDATED an old one.
	INSERT INTO tblEnrollment
	(
		idSite,
		idCourse,
		idUser,
		idRuleSetEnrollment,
		idTimezone,
		isLockedByPrerequisites,
		code,
		revcode,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT DISTINCT
		RTR.idSite,
		RTR.idCourse,
		RTR.idUser,
		RTR.idRuleSetEnrollmentNew,
		RSE.idTimezone,
		RSE.isLockedByPrerequisites,
		C.coursecode,
		C.revcode,
		C.title,
		CASE WHEN RSE.isFixedDate = 1 THEN
			RSE.dtStart
		ELSE
			CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
				dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
			ELSE
				@utcNow
			END
		END,
		CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
			CASE WHEN RSE.isFixedDate = 1 THEN
				dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, RSE.dtStart)
			ELSE
				CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
				ELSE
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
				END
			END
		ELSE
			NULL
		END,
		CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
			CASE WHEN RSE.isFixedDate = 1 THEN
				dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
			ELSE
				CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
					dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
				ELSE
					dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, @utcNow)
				END
			END
		ELSE
			NULL
		END,
		dtCreated = @utcNow,
		RSE.dueInterval,
		RSE.dueTimeframe,
		RSE.expiresFromStartInterval,
		RSE.expiresFromStartTimeframe,
		RSE.expiresFromFirstLaunchInterval,
		RSE.expiresFromFirstLaunchTimeframe
	FROM #RecurringToRecurring RTR
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RTR.idRuleSetEnrollmentNew
	LEFT JOIN tblCourse C ON C.idCourse = RTR.idCourse
	WHERE RTR.updateCurrentEnrollment = 0

	/*

	Do work for One-Time to Recurring RuleSet Enrollment shift.

	*/

	-- UPDATE RuleSet Enrollment link for inherited RuleSet Enrollments where the current Enrollment is still active.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = OTTR.idRuleSetEnrollmentNew,
		idTimezone = RSE.idTimezone,
		isLockedByPrerequisites = RSE.isLockedByPrerequisites,
		dtStart = CASE WHEN RSE.isFixedDate = 1 THEN
					RSE.dtStart
				  ELSE
					CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
						dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
					ELSE
						@utcNow
					END
				  END,
		dtDue = CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
					CASE WHEN RSE.isFixedDate = 1 THEN
						dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, RSE.dtStart)
					ELSE
						CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
							dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
						ELSE
							dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
						END
					END
				ELSE
					NULL
				END,
		dtExpiresFromStart = CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
								CASE WHEN RSE.isFixedDate = 1 THEN
									dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
								ELSE
									CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
										dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
									ELSE
										dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, @utcNow)
									END
								END
							 ELSE
								NULL
							 END,
		dtExpiresFromFirstLaunch = NULL,
		dtFirstLaunch = NULL,
		dtCreated = @utcNow,
		dtLastSynchronized = NULL,
		dueInterval = RSE.dueInterval,
		dueTimeframe = RSE.dueTimeframe,
		expiresFromStartInterval = RSE.expiresFromStartInterval,
		expiresFromStartTimeframe = RSE.expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = RSE.expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = RSE.expiresFromFirstLaunchTimeframe
	FROM #OneTimeToRecurring OTTR
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = OTTR.idRuleSetEnrollmentNew
	WHERE OTTR.idEnrollmentCurrent = idEnrollment
	AND OTTR.updateCurrentEnrollment = 1

	-- REMOVE old RuleSet Enrollment link for inherited Enrollments that have been completed.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = NULL
	FROM #OneTimeToRecurring OTTR
	WHERE OTTR.idUser = tblEnrollment.idUser
	AND OTTR.idCourse = tblEnrollment.idCourse
	AND OTTR.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
	AND OTTR.idEnrollmentCurrent <> tblEnrollment.idEnrollment
	AND OTTR.removeOldRSELinks = 1
	AND tblEnrollment.dtCompleted IS NOT NULL

	-- DELETE Enrollments of the old RuleSet Enrollment where they are expired and NOT COMPLETED.
	-- Use the Enrollment.Delete procedure so that lesson data gets cleaned up.
	DECLARE @OneTimeToRecurringEnrollmentsToDelete IDTable

	INSERT INTO @OneTimeToRecurringEnrollmentsToDelete (
		id
	)
	SELECT
		idEnrollment
	FROM tblEnrollment
	WHERE idUser IN (
					 SELECT idUser FROM
				     #OneTimeToRecurring OTTR
					 WHERE OTTR.idUser = tblEnrollment.idUser
					 AND OTTR.idCourse = tblEnrollment.idCourse
					 AND OTTR.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idCourse IN (
				     SELECT idCourse FROM
				     #OneTimeToRecurring OTTR
					 WHERE OTTR.idUser = tblEnrollment.idUser
					 AND OTTR.idCourse = tblEnrollment.idCourse
					 AND OTTR.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idRuleSetEnrollment IN (
							    SELECT idRuleSetEnrollment FROM
								#OneTimeToRecurring OTTR
								WHERE OTTR.idUser = tblEnrollment.idUser
								AND OTTR.idCourse = tblEnrollment.idCourse
								AND OTTR.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
							   )
	AND (
			(dtExpiresFromStart IS NOT NULL AND dtExpiresFromStart < @utcNow) 
			OR 
			(dtExpiresFromFirstLaunch IS NOT NULL AND dtExpiresFromFirstLaunch < @utcNow)
		)
	AND dtCompleted IS NULL

	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @OneTimeToRecurringEnrollmentsToDelete, 0

	-- INSERT Enrollments of the new RuleSet Enrollment where we have not UPDATED an old one.
	INSERT INTO tblEnrollment
	(
		idSite,
		idCourse,
		idUser,
		idRuleSetEnrollment,
		idTimezone,
		isLockedByPrerequisites,
		code,
		revcode,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT DISTINCT
		OTTR.idSite,
		OTTR.idCourse,
		OTTR.idUser,
		OTTR.idRuleSetEnrollmentNew,
		RSE.idTimezone,
		RSE.isLockedByPrerequisites,
		C.coursecode,
		C.revcode,
		C.title,
		CASE WHEN RSE.isFixedDate = 1 THEN
			RSE.dtStart
		ELSE
			CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
				dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
			ELSE
				@utcNow
			END
		END,
		CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
			CASE WHEN RSE.isFixedDate = 1 THEN
				dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, RSE.dtStart)
			ELSE
				CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
				ELSE
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
				END
			END
		ELSE
			NULL
		END,
		CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
			CASE WHEN RSE.isFixedDate = 1 THEN
				dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
			ELSE
				CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
					dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
				ELSE
					dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, @utcNow)
				END
			END
		ELSE
			NULL
		END,
		dtCreated = @utcNow,
		RSE.dueInterval,
		RSE.dueTimeframe,
		RSE.expiresFromStartInterval,
		RSE.expiresFromStartTimeframe,
		RSE.expiresFromFirstLaunchInterval,
		RSE.expiresFromFirstLaunchTimeframe
	FROM #OneTimeToRecurring OTTR
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = OTTR.idRuleSetEnrollmentNew
	LEFT JOIN tblCourse C ON C.idCourse = OTTR.idCourse
	WHERE OTTR.updateCurrentEnrollment = 0

	/*

	Do work for Recurring to One-Time RuleSet Enrollment shift.

	*/

	-- UPDATE RuleSet Enrollment link for inherited RuleSet Enrollments where the current Enrollment is still active.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = RTOT.idRuleSetEnrollmentNew,
		idTimezone = RSE.idTimezone,
		isLockedByPrerequisites = RSE.isLockedByPrerequisites,
		dtStart = CASE WHEN RSE.isFixedDate = 1 THEN
					RSE.dtStart
				  ELSE
					CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
						dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
					ELSE
						@utcNow
					END
				  END,
		dtDue = CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
					CASE WHEN RSE.isFixedDate = 1 THEN
						dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, RSE.dtStart)
					ELSE
						CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
							dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
						ELSE
							dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
						END
					END
				ELSE
					NULL
				END,
		dtExpiresFromStart = CASE WHEN RSE.isFixedDate = 1 AND (RSE.recurInterval IS NULL OR RSE.recurInterval = 0) THEN
								 RSE.dtEnd
							 ELSE
								 CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
									CASE WHEN RSE.isFixedDate = 1 THEN
										dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
									ELSE
										CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
											dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
										ELSE
											dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, @utcNow)
										END
									END
								 ELSE
									NULL
								 END
							 END,
		dtExpiresFromFirstLaunch = NULL,
		dtFirstLaunch = NULL,
		dtCreated = @utcNow,
		dtLastSynchronized = NULL,
		dueInterval = RSE.dueInterval,
		dueTimeframe = RSE.dueTimeframe,
		expiresFromStartInterval = RSE.expiresFromStartInterval,
		expiresFromStartTimeframe = RSE.expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = RSE.expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = RSE.expiresFromFirstLaunchTimeframe
	FROM #RecurringToOneTime RTOT
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RTOT.idRuleSetEnrollmentNew
	WHERE RTOT.idEnrollmentCurrent = idEnrollment
	AND RTOT.updateCurrentEnrollment = 1

	-- REMOVE old RuleSet Enrollment link for inherited Enrollments that have been completed.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = NULL
	FROM #RecurringToOneTime RTOT
	WHERE RTOT.idUser = tblEnrollment.idUser
	AND RTOT.idCourse = tblEnrollment.idCourse
	AND RTOT.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
	AND RTOT.idEnrollmentCurrent <> tblEnrollment.idEnrollment
	AND RTOT.removeOldRSELinks = 1
	AND tblEnrollment.dtCompleted IS NOT NULL

	-- DELETE Enrollments of the old RuleSet Enrollment where they are expired and NOT COMPLETED.
	-- Use the Enrollment.Delete procedure so that lesson data gets cleaned up.
	DECLARE @RecurringToOneTimeEnrollmentsToDelete IDTable

	INSERT INTO @RecurringToOneTimeEnrollmentsToDelete (
		id
	)
	SELECT
		idEnrollment
	FROM tblEnrollment
	WHERE idUser IN (
					 SELECT idUser FROM
				     #RecurringToOneTime RTOT
					 WHERE RTOT.idUser = tblEnrollment.idUser
					 AND RTOT.idCourse = tblEnrollment.idCourse
					 AND RTOT.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idCourse IN (
				     SELECT idCourse FROM
				     #RecurringToOneTime RTOT
					 WHERE RTOT.idUser = tblEnrollment.idUser
					 AND RTOT.idCourse = tblEnrollment.idCourse
					 AND RTOT.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idRuleSetEnrollment IN (
							    SELECT idRuleSetEnrollment FROM
								#RecurringToOneTime RTOT
								WHERE RTOT.idUser = tblEnrollment.idUser
								AND RTOT.idCourse = tblEnrollment.idCourse
								AND RTOT.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
							   )
	AND (
			(dtExpiresFromStart IS NOT NULL AND dtExpiresFromStart < @utcNow) 
			OR 
			(dtExpiresFromFirstLaunch IS NOT NULL AND dtExpiresFromFirstLaunch < @utcNow)
		)
	AND dtCompleted IS NULL

	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @RecurringToOneTimeEnrollmentsToDelete, 0

	-- INSERT Enrollments of the new RuleSet Enrollment where we have not UPDATED an old one.
	INSERT INTO tblEnrollment
	(
		idSite,
		idCourse,
		idUser,
		idRuleSetEnrollment,
		idTimezone,
		isLockedByPrerequisites,
		code,
		revcode,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT DISTINCT
		RTOT.idSite,
		RTOT.idCourse,
		RTOT.idUser,
		RTOT.idRuleSetEnrollmentNew,
		RSE.idTimezone,
		RSE.isLockedByPrerequisites,
		C.coursecode,
		C.revcode,
		C.title,
		CASE WHEN RSE.isFixedDate = 1 THEN
			RSE.dtStart
		ELSE
			CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
				dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
			ELSE
				@utcNow
			END
		END,
		CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
			CASE WHEN RSE.isFixedDate = 1 THEN
				dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, RSE.dtStart)
			ELSE
				CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
				ELSE
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
				END
			END
		ELSE
			NULL
		END,
		CASE WHEN RSE.isFixedDate = 1 AND (RSE.recurInterval IS NULL OR RSE.recurInterval = 0) THEN
			RSE.dtEnd
		ELSE
			CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
				CASE WHEN RSE.isFixedDate = 1 THEN
					dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
				ELSE
					CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
						dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
					ELSE
						dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, @utcNow)
					END
				END
			ELSE
				NULL
			END
		END,
		dtCreated = @utcNow,
		RSE.dueInterval,
		RSE.dueTimeframe,
		RSE.expiresFromStartInterval,
		RSE.expiresFromStartTimeframe,
		RSE.expiresFromFirstLaunchInterval,
		RSE.expiresFromFirstLaunchTimeframe
	FROM #RecurringToOneTime RTOT
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RTOT.idRuleSetEnrollmentNew
	LEFT JOIN tblCourse C ON C.idCourse = RTOT.idCourse
	WHERE RTOT.updateCurrentEnrollment = 0
	AND RSE.forceReassignment = 1

	/*

	Do work for One-Time to One-Time RuleSet Enrollment shift.

	*/

	-- UPDATE RuleSet Enrollment link for inherited RuleSet Enrollments where the current Enrollment is still active.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = OTTOT.idRuleSetEnrollmentNew,
		idTimezone = RSE.idTimezone,
		isLockedByPrerequisites = RSE.isLockedByPrerequisites,
		dtStart = CASE WHEN RSE.isFixedDate = 1 THEN
					RSE.dtStart
				  ELSE
					CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
						dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
					ELSE
						@utcNow
					END
				  END,
		dtDue = CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
					CASE WHEN RSE.isFixedDate = 1 THEN
						dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, RSE.dtStart)
					ELSE
						CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
							dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
						ELSE
							dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
						END
					END
				ELSE
					NULL
				END,
		dtExpiresFromStart = CASE WHEN RSE.isFixedDate = 1 AND (RSE.recurInterval IS NULL OR RSE.recurInterval = 0) THEN
								 RSE.dtEnd
							 ELSE
								 CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
									CASE WHEN RSE.isFixedDate = 1 THEN
										dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
									ELSE
										CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
											dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
										ELSE
											dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, @utcNow)
										END
									END
								 ELSE
									NULL
								 END
							 END,
		dtExpiresFromFirstLaunch = NULL,
		dtFirstLaunch = NULL,
		dtCreated = @utcNow,
		dtLastSynchronized = NULL,
		dueInterval = RSE.dueInterval,
		dueTimeframe = RSE.dueTimeframe,
		expiresFromStartInterval = RSE.expiresFromStartInterval,
		expiresFromStartTimeframe = RSE.expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = RSE.expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = RSE.expiresFromFirstLaunchTimeframe
	FROM #OneTimeToOneTime OTTOT
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = OTTOT.idRuleSetEnrollmentNew
	WHERE OTTOT.idEnrollmentCurrent = idEnrollment
	AND OTTOT.updateCurrentEnrollment = 1

	-- REMOVE old RuleSet Enrollment link for inherited Enrollments that have been completed.
	UPDATE tblEnrollment SET
		idRuleSetEnrollment = NULL
	FROM #OneTimeToOneTime OTTOT
	WHERE OTTOT.idUser = tblEnrollment.idUser
	AND OTTOT.idCourse = tblEnrollment.idCourse
	AND OTTOT.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
	AND OTTOT.idEnrollmentCurrent <> tblEnrollment.idEnrollment
	AND OTTOT.removeOldRSELinks = 1
	AND tblEnrollment.dtCompleted IS NOT NULL

	-- DELETE Enrollments of the old RuleSet Enrollment where they are expired and NOT COMPLETED.
	-- Use the Enrollment.Delete procedure so that lesson data gets cleaned up.
	DECLARE @OneTimeToOneTimeEnrollmentsToDelete IDTable

	INSERT INTO @OneTimeToOneTimeEnrollmentsToDelete (
		id
	)
	SELECT
		idEnrollment
	FROM tblEnrollment
	WHERE idUser IN (
					 SELECT idUser FROM
				     #OneTimeToOneTime OTTOT
					 WHERE OTTOT.idUser = tblEnrollment.idUser
					 AND OTTOT.idCourse = tblEnrollment.idCourse
					 AND OTTOT.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idCourse IN (
				     SELECT idCourse FROM
				     #OneTimeToOneTime OTTOT
					 WHERE OTTOT.idUser = tblEnrollment.idUser
					 AND OTTOT.idCourse = tblEnrollment.idCourse
					 AND OTTOT.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
					)
	AND idRuleSetEnrollment IN (
							    SELECT idRuleSetEnrollment FROM
								#OneTimeToOneTime OTTOT
								WHERE OTTOT.idUser = tblEnrollment.idUser
								AND OTTOT.idCourse = tblEnrollment.idCourse
								AND OTTOT.idRuleSetEnrollmentOld = tblEnrollment.idRuleSetEnrollment
							   )
	AND (
			(dtExpiresFromStart IS NOT NULL AND dtExpiresFromStart < @utcNow) 
			OR 
			(dtExpiresFromFirstLaunch IS NOT NULL AND dtExpiresFromFirstLaunch < @utcNow)
		)
	AND dtCompleted IS NULL

	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @OneTimeToOneTimeEnrollmentsToDelete, 0

	-- INSERT Enrollments of the new RuleSet Enrollment where we have not UPDATED an old one.
	INSERT INTO tblEnrollment
	(
		idSite,
		idCourse,
		idUser,
		idRuleSetEnrollment,
		idTimezone,
		isLockedByPrerequisites,
		code,
		revcode,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT DISTINCT
		OTTOT.idSite,
		OTTOT.idCourse,
		OTTOT.idUser,
		OTTOT.idRuleSetEnrollmentNew,
		RSE.idTimezone,
		RSE.isLockedByPrerequisites,
		C.coursecode,
		C.revcode,
		C.title,
		CASE WHEN RSE.isFixedDate = 1 THEN
			RSE.dtStart
		ELSE
			CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
				dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
			ELSE
				@utcNow
			END
		END,
		CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
			CASE WHEN RSE.isFixedDate = 1 THEN
				dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, RSE.dtStart)
			ELSE
				CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
				ELSE
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
				END
			END
		ELSE
			NULL
		END,
		CASE WHEN RSE.isFixedDate = 1 AND (RSE.recurInterval IS NULL OR RSE.recurInterval = 0) THEN
			RSE.dtEnd
		ELSE
			CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
				CASE WHEN RSE.isFixedDate = 1 THEN
					dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
				ELSE
					CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
						dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
					ELSE
						dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, @utcNow)
					END
				END
			ELSE
				NULL
			END
		END,
		dtCreated = @utcNow,
		RSE.dueInterval,
		RSE.dueTimeframe,
		RSE.expiresFromStartInterval,
		RSE.expiresFromStartTimeframe,
		RSE.expiresFromFirstLaunchInterval,
		RSE.expiresFromFirstLaunchTimeframe
	FROM #OneTimeToOneTime OTTOT
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = OTTOT.idRuleSetEnrollmentNew
	LEFT JOIN tblCourse C ON C.idCourse = OTTOT.idCourse
	WHERE OTTOT.updateCurrentEnrollment = 0
	AND RSE.forceReassignment = 1

	/*

	Do work for brand new INSERTS.

	*/

	INSERT INTO tblEnrollment
	(
		idSite,
		idCourse,
		idUser,
		idRuleSetEnrollment,
		idTimezone,
		isLockedByPrerequisites,
		code,
		revcode,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT DISTINCT
		NI.idSite,
		NI.idCourse,
		NI.idUser,
		NI.idRuleSetEnrollment,
		RSE.idTimezone,
		RSE.isLockedByPrerequisites,
		C.coursecode,
		C.revcode,
		C.title,
		CASE WHEN RSE.isFixedDate = 1 THEN
			RSE.dtStart
		ELSE
			CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
				dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
			ELSE
				@utcNow
			END
		END,
		CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
			CASE WHEN RSE.isFixedDate = 1 THEN
				dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, RSE.dtStart)
			ELSE
				CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
				ELSE
					dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
				END
			END
		ELSE
			NULL
		END,
		CASE WHEN RSE.isFixedDate = 1 AND (RSE.recurInterval IS NULL OR RSE.recurInterval = 0) THEN
			RSE.dtEnd
		ELSE
			CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
				CASE WHEN RSE.isFixedDate = 1 THEN
					dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
				ELSE
					CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
						dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
					ELSE
						dbo.[IDateAdd](RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, @utcNow)
					END
				END
			ELSE
				NULL
			END
		END,
		@utcNow,
		RSE.dueInterval,
		RSE.dueTimeframe,
		RSE.expiresFromStartInterval,
		RSE.expiresFromStartTimeframe,
		RSE.expiresFromFirstLaunchInterval,
		RSE.expiresFromFirstLaunchTimeframe
	FROM #NewInserts NI
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = NI.idRuleSetEnrollment
	LEFT JOIN tblCourse C ON C.idCourse = NI.idCourse

	/* 
	
	do the event log entries for brand new inserts only
	
	*/

	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		E.idSite,
		NI.idCourse,
		E.idEnrollment, 
		NI.idUser
	FROM #NewInserts NI
	LEFT JOIN tblEnrollment E ON E.idCourse = NI.idCourse AND E.idUser = NI.idUser
	WHERE E.dtCreated = @utcNow

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 201, @utcNow, @eventLogItems
				   
	/* DROP THE TEMPORARY TABLES */
	DROP TABLE #CurrentEnrollments
	DROP TABLE #RuleSetEnrollmentMatches
	DROP TABLE #NewInserts
	DROP TABLE #RecurringToRecurring
	DROP TABLE #RecurringToOneTime
	DROP TABLE #OneTimeToRecurring
	DROP TABLE #OneTimeToOneTime

	/* FINAL STEP: REVOKE RULESET ENROLLMENTS */
	EXEC [RuleSetEnrollment.RevokeEnrollments] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @Courses, @Filters, @filterBy

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO