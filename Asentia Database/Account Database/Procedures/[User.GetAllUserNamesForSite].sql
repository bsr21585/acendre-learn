-- =====================================================================
-- PROCEDURE: [User.GetAllUsernamesForSite]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.GetAllUsernamesForSite]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.GetAllUsernamesForSite]
GO

/*

Returns a recordset of all usernames for the site.

*/

CREATE PROCEDURE [User.GetAllUsernamesForSite]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the data

	*/

	SELECT
		DISTINCT
		U.username
	FROM tblUser U
	WHERE U.idSite = @idCallerSite
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0)) -- DO NOT weed out pending users
	ORDER BY username
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	