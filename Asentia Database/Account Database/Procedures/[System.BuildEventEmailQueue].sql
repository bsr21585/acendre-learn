-- =====================================================================
-- PROCEDURE: [System.BuildEventEmailQueue]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.BuildEventEmailQueue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.BuildEventEmailQueue]
GO

/*

Builds the event email queue for the system.

*/

CREATE PROCEDURE [System.BuildEventEmailQueue]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idEventLog				INT
)
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON

	/*

	create temporary tables to hold user, enrollment, etc data so that
	we aren't querying from live tables more than once

	*/

	CREATE TABLE #Users (
		idUser						INT,
		idSite						INT,
		fullName					NVARCHAR(512),
		firstName					NVARCHAR(255),
		username					NVARCHAR(512),
		email						NVARCHAR(255),
		idLanguage					INT,
		idTimezone					INT,
		languageString				NVARCHAR(10),
		timezoneDotNetName			NVARCHAR(255),
		dtExpires					DATETIME,
		isActive					BIT,
		isDeleted					BIT,
		optOutOfEmailNotifications	BIT
	)

	CREATE TABLE #Enrollments (
		idEnrollment				INT,
		idCourse					INT,
		idUser						INT,
		dtCreated					DATETIME,
		dtStart						DATETIME,
		dtDue						DATETIME,
		dtEffectiveExpires			DATETIME,
		dtCompleted					DATETIME
	)

	CREATE TABLE #LearningPathEnrollments (
		idLearningPathEnrollment	INT,
		idLearningPath				INT,
		idUser						INT,
		dtCreated					DATETIME,
		dtStart						DATETIME,
		dtDue						DATETIME,
		dtEffectiveExpires			DATETIME,
		dtCompleted					DATETIME
	)

	CREATE TABLE #CertificateRecord (
		idCertificateRecord			INT,
		idCertificate				INT,
		idUser						INT,
		dtExpires					DATETIME
	)

	CREATE TABLE #StandupTrainingInstances (
		idStandupTrainingInstance	INT,
		idStandupTraining			INT,
		idSite						INT,
		dtStart						DATETIME,
		idInstructor				INT
	)

	CREATE TABLE #OJTProctors (
		idLearner	INT,
		idLesson	INT,
		idProctor	INT
	)

	CREATE TABLE #TaskProctors (
		idLearner	INT,
		idLesson	INT,
		idProctor	INT
	)

	CREATE TABLE #CertificationToUserLink (
		idCertificationToUserLink	INT,
		idCertification				INT,
		idUser						INT,
		currentExpirationDate		DATETIME
	)

	CREATE TABLE #CertificationTaskProctors (
		idLearner							INT,
		idCertificationModuleRequirement	INT,
		idProctor							INT
	)

	CREATE TABLE #InboxMessages (
		idInboxMessage				INT,
		idSite						INT,
		idSender					INT,
		idRecipient					INT,
		dtSent						DATETIME	
	)

	CREATE TABLE #CourseModeratedMessages (
		idCourseFeedMessage		INT,		
		idSite					INT,
		idCourse				INT,
		idAuthor				INT,
		[timestamp]				DATETIME		
	)

	CREATE TABLE #GroupModeratedMessages (
		idGroupFeedMessage		INT,
		idSite					INT,
		idGroup					INT,
		idAuthor				INT,
		[timestamp]				DATETIME		
	)

	CREATE TABLE #CourseModerators (
		idCourse				INT,		
		idModerator				INT	
	)

	CREATE TABLE #GroupModerators (
		idGroup					INT,
		idModerator				INT	
	)

	/*

	populate users

	*/

	INSERT INTO #Users (
		idUser,
		idSite,
		fullName,
		firstName,
		username,
		email,
		idLanguage,
		idTimezone,
		languageString,
		timezoneDotNetName,
		dtExpires,
		isActive,
		isDeleted,
		optOutOfEmailNotifications
	)
	SELECT
		U.idUser,
		U.idSite,
		U.firstName + ' ' + U.lastName,
		U.firstName,
		U.username,
		U.email,
		U.idLanguage,
		U.idTimezone,
		L.code,
		TZ.dotNetName,
		U.dtExpires,
		U.isActive,
		CASE WHEN U.isDeleted IS NULL THEN 0 ELSE U.isDeleted END,
		CASE WHEN U.optOutOfEmailNotifications IS NULL THEN 0 ELSE U.optOutOfEmailNotifications END
	FROM tblUser U
	LEFT JOIN tblLanguage L ON L.idLanguage = U.idLanguage
	LEFT JOIN tblTimezone TZ ON TZ.idTimezone = U.idTimezone
	-- we actually need to include deleted users so "User Deleted" event notifications fire
	-- WHERE (U.isDeleted IS NULL OR U.isDeleted = 0) -- only non-deleted users

	/*

	populate enrollments

	*/

	INSERT INTO #Enrollments (
		idEnrollment,
		idCourse,
		idUser,
		dtCreated,
		dtStart,
		dtDue,
		dtEffectiveExpires,
		dtCompleted
	)
	SELECT
		E.idEnrollment,
		E.idCourse,
		E.idUser,
		E.dtCreated,
		E.dtStart,
		E.dtDue,
		CASE WHEN E.dtExpiresFromFirstLaunch IS NOT NULL OR E.dtExpiresFromStart IS NOT NULL THEN
			CASE WHEN E.dtExpiresFromFirstLaunch IS NULL THEN
				E.dtExpiresFromStart
			ELSE
				CASE WHEN E.dtExpiresFromStart IS NULL THEN
					E.dtExpiresFromFirstLaunch
				ELSE
					CASE WHEN E.dtExpiresFromFirstLaunch > E.dtExpiresFromStart THEN
						E.dtExpiresFromStart
					ELSE
						E.dtExpiresFromFirstLaunch
					END
				END
			END
		ELSE
			NULL
		END AS dtEffectiveExpires,
		E.dtCompleted
	FROM tblEnrollment E
	WHERE EXISTS (SELECT 1 FROM #Users U WHERE U.idUser = E.idUser AND U.isDeleted = 0) -- only non-deleted users
	AND E.idActivityImport IS NULL -- no activity import records
	AND E.dtCompleted IS NULL -- no completed enrollments, dynamic event information where this is needed does not apply to completed enrollments

	/*

	populate learning path enrollments

	*/

	INSERT INTO #LearningPathEnrollments (
		idLearningPathEnrollment,
		idLearningPath,
		idUser,
		dtCreated,
		dtStart,
		dtDue,
		dtEffectiveExpires,
		dtCompleted
	)
	SELECT
		LPE.idLearningPathEnrollment,
		LPE.idLearningPath,
		LPE.idUser,
		LPE.dtCreated,
		LPE.dtStart,
		LPE.dtDue,
		CASE WHEN LPE.dtExpiresFromFirstLaunch IS NOT NULL OR LPE.dtExpiresFromStart IS NOT NULL THEN
			CASE WHEN LPE.dtExpiresFromFirstLaunch IS NULL THEN
				LPE.dtExpiresFromStart
			ELSE
				CASE WHEN LPE.dtExpiresFromStart IS NULL THEN
					LPE.dtExpiresFromFirstLaunch
				ELSE
					CASE WHEN LPE.dtExpiresFromFirstLaunch > LPE.dtExpiresFromStart THEN
						LPE.dtExpiresFromStart
					ELSE
						LPE.dtExpiresFromFirstLaunch
					END
				END
			END
		ELSE
			NULL
		END AS dtEffectiveExpires,
		LPE.dtCompleted
	FROM tblLearningPathEnrollment LPE
	WHERE EXISTS (SELECT 1 FROM #Users U WHERE U.idUser = LPE.idUser AND U.isDeleted = 0) -- only non-deleted users
	AND LPE.dtCompleted IS NULL -- no completed enrollments, dynamic event information where this is needed does not apply to completed enrollments

	/*

	populate certificate records

	*/

	INSERT INTO #CertificateRecord (
		idCertificateRecord,
		idCertificate,
		idUser,
		dtExpires
	)
	SELECT
		CR.idCertificateRecord,
		CR.idCertificate,
		CR.idUser,
		CR.expires
	FROM tblCertificateRecord CR
	WHERE EXISTS (SELECT 1 FROM #Users U WHERE U.idUser = CR.idUser AND U.isDeleted = 0) -- only non-deleted users

	/*

	populate standup training instance records

	*/

	INSERT INTO #StandupTrainingInstances (
		idStandupTrainingInstance,
		idStandupTraining,
		idSite,
		dtStart,
		idInstructor
	)
	SELECT
		STI.idStandUpTrainingInstance,
		STI.idStandUpTraining,
		STI.idSite,
		(SELECT MIN(dtStart) FROM tblStandUpTrainingInstanceMeetingTime STIMT WHERE STIMT.idStandUpTrainingInstance = STI.idStandUpTrainingInstance),
		STII.idInstructor
	FROM tblStandUpTrainingInstance STI
	LEFT JOIN tblStandupTrainingInstanceToInstructorLink STII ON STII.idStandupTrainingInstance = STI.idStandUpTrainingInstance

	/*

	populate ojt proctors based on events related to ojt proctoring

	*/

	-- course experts
	INSERT INTO #OJTProctors (
		idLearner,
		idLesson,
		idProctor
	)
	SELECT
		EL.idObjectUser AS idLearner,
		EL.idObject AS idLesson,
		CE.idUser AS idProctor
	FROM tblEventLog EL
	LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = EL.idObject AND LCL.idContentType = 4
	LEFT JOIN tblCourseExpert CE ON LCL.allowCourseExpertsAsProctor = 1 AND CE.idCourse = (SELECT L.idCourse FROM tblLesson L WHERE L.idLesson = LCL.idLesson)
	WHERE idEventType = 304 -- ojt request submitted
	AND EL.eventDate > (SELECT TOP 1 lastEventEmailQueueCascade FROM tblSystem) -- event occuring after the last cascade, so we only get what is necessary
	AND CE.idUser IS NOT NULL
	AND CE.idUser <> EL.idObjectUser -- if the learner is a course expert himself, he should not get a notification for his own ojt proctor event

	-- supervisors
	INSERT INTO #OJTProctors (
		idLearner,
		idLesson,
		idProctor
	)
	SELECT
		EL.idObjectUser AS idLearner,
		EL.idObject AS idLesson,
		USL.idSupervisor AS idProctor
	FROM tblEventLog EL
	LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = EL.idObject AND LCL.idContentType = 4
	LEFT JOIN tblUserToSupervisorLink USL ON LCL.allowSupervisorsAsProctor = 1 AND USL.idUser = EL.idObjectUser
	WHERE idEventType = 304 -- ojt request submitted
	AND EL.eventDate >= (SELECT TOP 1 lastEventEmailQueueCascade FROM tblSystem) -- event occuring after the last cascade, so we only get what is necessary
	AND USL.idSupervisor IS NOT NULL

	/*

	populate task proctors based on events related to task proctoring

	*/

	-- course experts
	INSERT INTO #TaskProctors (
		idLearner,
		idLesson,
		idProctor
	)
	SELECT
		EL.idObjectUser AS idLearner,
		EL.idObject AS idLesson,
		CE.idUser AS idProctor
	FROM tblEventLog EL
	LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = EL.idObject AND LCL.idContentType = 3
	LEFT JOIN tblCourseExpert CE ON LCL.allowCourseExpertsAsProctor = 1 AND CE.idCourse = (SELECT L.idCourse FROM tblLesson L WHERE L.idLesson = LCL.idLesson)
	WHERE idEventType = 305 -- task submitted
	AND EL.eventDate > (SELECT TOP 1 lastEventEmailQueueCascade FROM tblSystem) -- event occuring after the last cascade, so we only get what is necessary
	AND CE.idUser IS NOT NULL
	AND CE.idUser <> EL.idObjectUser -- if the learner is a course expert himself, he should not get a notification for his own task proctor event

	-- supervisors
	INSERT INTO #TaskProctors (
		idLearner,
		idLesson,
		idProctor
	)
	SELECT
		EL.idObjectUser AS idLearner,
		EL.idObject AS idLesson,
		USL.idSupervisor AS idProctor
	FROM tblEventLog EL
	LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = EL.idObject AND LCL.idContentType = 3
	LEFT JOIN tblUserToSupervisorLink USL ON LCL.allowSupervisorsAsProctor = 1 AND USL.idUser = EL.idObjectUser
	WHERE idEventType = 305 -- task submitted
	AND EL.eventDate >= (SELECT TOP 1 lastEventEmailQueueCascade FROM tblSystem) -- event occuring after the last cascade, so we only get what is necessary
	AND USL.idSupervisor IS NOT NULL

	/*

	populate certification to user links

	*/

	INSERT INTO #CertificationToUserLink (
		idCertificationToUserLink,
		idCertification,
		idUser,
		currentExpirationDate		
	)
	SELECT
		CUL.idCertificationToUserLink,
		CUL.idCertification,
		CUL.idUser,
		CUL.currentExpirationDate		
	FROM tblCertificationToUserLink CUL
	LEFT JOIN #Users U ON U.idUser = CUL.idUser
	WHERE EXISTS (SELECT 1 FROM #Users U WHERE U.idUser = CUL.idUser AND U.isDeleted = 0) -- only non-deleted users	

	/*

	populate certification task proctors based on events related to task proctoring

	*/	

	-- supervisors
	INSERT INTO #CertificationTaskProctors (
		idLearner,
		idCertificationModuleRequirement,
		idProctor
	)
	SELECT
		EL.idObjectUser AS idLearner,
		EL.idObject AS idCertificationModuleRequirement,
		USL.idSupervisor AS idProctor
	FROM tblEventLog EL	
	LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirement = EL.idObject
	LEFT JOIN tblUserToSupervisorLink USL ON CMR.allowSupervisorsToApproveDocumentation = 1 AND USL.idUser = EL.idObjectUser
	WHERE idEventType = 606 -- task submitted
	AND EL.eventDate >= (SELECT TOP 1 lastEventEmailQueueCascade FROM tblSystem) -- event occuring after the last cascade, so we only get what is necessary
	AND USL.idSupervisor IS NOT NULL	

	/*

	populate message center messages

	*/

	INSERT INTO #InboxMessages (
		idInboxMessage,
		idSite,
		idSender,
		idRecipient,
		dtSent
	)
	SELECT
		IM.idInboxMessage,
		IM.idSite,
		IM.idSender,
		IM.idRecipient,
		IM.dtSent
	FROM tblInboxMessage IM
	LEFT JOIN #Users U ON U.idUser = IM.idRecipient
	WHERE EXISTS (SELECT 1 FROM #Users U WHERE U.idUser = IM.idRecipient AND U.isDeleted = 0) -- only non-deleted users
	AND IM.isDraft = 0 AND IM.isSent = 1 AND (IM.isRead = 0 OR IM.isRead IS NULL) AND (IM.isRecipientDeleted IS NULL OR IM.isRecipientDeleted = 0) AND (IM.isSenderDeleted IS NULL OR IM.isSenderDeleted = 0)

	/*

	populate new moderated messages -- Course

	*/

	INSERT INTO #CourseModeratedMessages (		
		idCourseFeedMessage,
		idSite,
		idCourse,
		idAuthor,
		[timestamp]
	)
	SELECT
		CFM.idCourseFeedMessage,
		CFM.idSite,
		CFM.idCourse,
		CFM.idAuthor,
		CFM.[timestamp]
	FROM tblCourseFeedMessage CFM
	WHERE (CFM.isApproved IS NULL OR CFM.isApproved = 0)

	/*

	populate new moderated messages -- Group

	*/
	
	INSERT INTO #GroupModeratedMessages (		
		idGroupFeedMessage,
		idSite,
		idGroup,
		idAuthor,
		[timestamp]
	)
	SELECT		
		GFM.idGroupFeedMessage,
		GFM.idSite,
		GFM.idGroup,
		GFM.idAuthor,
		GFM.[timestamp]
	FROM tblGroupFeedMessage GFM
	WHERE (GFM.isApproved IS NULL OR GFM.isApproved = 0)

	/*

	populate course discussion moderators

	*/
	
	INSERT INTO #CourseModerators (		
		idCourse,
		idModerator
	)
	SELECT		
		CFM.idCourse,
		CFM.idModerator
	FROM tblCourseFeedModerator CFM
	WHERE EXISTS (SELECT 1 FROM #Users U WHERE U.idUser = CFM.idModerator AND U.isDeleted = 0) -- only non-deleted users

	/*

	populate group discussion moderators

	*/
	
	INSERT INTO #GroupModerators (		
		idGroup,
		idModerator
	)
	SELECT		
		GFM.idGroup,
		GFM.idModerator
	FROM tblGroupFeedModerator GFM
	WHERE EXISTS (SELECT 1 FROM #Users U WHERE U.idUser = GFM.idModerator AND U.isDeleted = 0) -- only non-deleted users

	/*

	get the event log data

	*/

	CREATE TABLE #Events (
		idEventLog		INT,
		idSite			INT,
		idEventType		INT,
		[timestamp]		DATETIME,
		eventDate		DATETIME,
		idObject		INT,
		idObjectRelated	INT,
		idObjectUser	INT,
		idExecutingUser	INT
	)

	INSERT INTO #Events (
		idEventLog,
		idSite,
		idEventType,
		[timestamp],
		eventDate,
		idObject,
		idObjectRelated,
		idObjectUser,
		idExecutingUser
	)
	SELECT 
		EVENTLOG_MAIN.idEventLog,
		EVENTLOG_MAIN.idSite, 
		EVENTLOG_MAIN.idEventType, 
		EVENTLOG_MAIN.[timestamp],
		EVENTLOG_MAIN.eventDate,
		EVENTLOG_MAIN.idObject,
		EVENTLOG_MAIN.idObjectRelated,
		EVENTLOG_MAIN.idObjectUser, 
		EVENTLOG_MAIN.idExecutingUser 
	FROM (
		SELECT 
			idEventLog,
			idSite, 
			idEventType, 
			[timestamp],
			eventDate,
			idObject,
			idObjectRelated,
			idObjectUser, 
			idExecutingUser
		FROM tblEventLog

		UNION -- join to dynamic enrollment information

		SELECT
			1 AS idEventLog,
			U.idSite,
			ENROLLMENTS_MAIN.idEventType,
			ENROLLMENTS_MAIN.[timestamp],
			ENROLLMENTS_MAIN.eventDate,
			ENROLLMENTS_MAIN.idObject,
			ENROLLMENTS_MAIN.idObjectRelated,
			ENROLLMENTS_MAIN.idObjectUser, 
			NULL AS idExecutingUser
		FROM (
			SELECT
				206 AS idEventType, -- course enrollment start
				E.idEnrollment AS idObjectRelated,
				E.dtCreated AS [timestamp],
				E.dtStart AS eventDate,
				E.idCourse AS idObject,
				E.idUser AS idObjectUser
			FROM #Enrollments E
			WHERE E.dtStart >= DATEADD(yyyy, -1, GETUTCDATE()) -- start is not more than a year in the past - this is arbitrary, but NO ONE should have a start notification that happens AFTER 1 year
			UNION
			SELECT 
				202 AS idEventType, -- course enrollment expires
				E.idEnrollment AS idObjectRelated,
				E.dtCreated AS [timestamp],
				E.dtEffectiveExpires AS eventDate,	
				E.idCourse AS idObject,
				E.idUser AS idObjectUser 
			FROM #Enrollments E
			WHERE E.dtEffectiveExpires IS NOT NULL
			UNION
			SELECT 
				204 AS idEventType, -- enrollment due
				E.idEnrollment AS idObjectRelated,
				E.dtCreated AS [timestamp],
				E.dtDue AS eventDate,	
				E.idCourse AS idObject,
				E.idUser AS idObjectUser
			FROM #Enrollments E
			WHERE E.dtDue IS NOT NULL
		) ENROLLMENTS_MAIN
		LEFT JOIN #Users U ON U.idUser = ENROLLMENTS_MAIN.idObjectUser

		UNION -- join to dynamic learning path enrollment information

		SELECT
			1 AS idEventLog,
			U.idSite,
			LEARNINGPATHENROLLMENTS_MAIN.idEventType,
			LEARNINGPATHENROLLMENTS_MAIN.[timestamp],
			LEARNINGPATHENROLLMENTS_MAIN.eventDate,
			LEARNINGPATHENROLLMENTS_MAIN.idObject,
			LEARNINGPATHENROLLMENTS_MAIN.idObjectRelated,
			LEARNINGPATHENROLLMENTS_MAIN.idObjectUser, 
			NULL AS idExecutingUser
		FROM (
			SELECT
				506 AS idEventType, -- learning path enrollment start
				LPE.idLearningPathEnrollment AS idObjectRelated,
				LPE.dtCreated AS [timestamp],
				LPE.dtStart AS eventDate,
				LPE.idLearningPath AS idObject,
				LPE.idUser AS idObjectUser
			FROM #LearningPathEnrollments LPE
			WHERE LPE.dtStart >= DATEADD(yyyy, -1, GETUTCDATE()) -- start is not more than a year in the past - this is arbitrary, but NO ONE should have a start notification that happens AFTER 1 year
			UNION
			SELECT 
				502 AS idEventType, -- learning path enrollment expires
				LPE.idLearningPathEnrollment AS idObjectRelated,
				LPE.dtCreated AS [timestamp],
				LPE.dtEffectiveExpires AS eventDate,	
				LPE.idLearningPath AS idObject,
				LPE.idUser AS idObjectUser 
			FROM #LearningPathEnrollments LPE
			WHERE LPE.dtEffectiveExpires IS NOT NULL
			UNION
			SELECT 
				504 AS idEventType, -- learning path enrollment due
				LPE.idLearningPathEnrollment AS idObjectRelated,
				LPE.dtCreated AS [timestamp],
				LPE.dtDue AS eventDate,	
				LPE.idLearningPath AS idObject,
				LPE.idUser AS idObjectUser
			FROM #LearningPathEnrollments LPE
			WHERE LPE.dtDue IS NOT NULL
		) LEARNINGPATHENROLLMENTS_MAIN
		LEFT JOIN #Users U ON U.idUser = LEARNINGPATHENROLLMENTS_MAIN.idObjectUser

		UNION -- join to user expiration

		SELECT 
			1 AS idEventLog, 
			U.idSite AS idSite,
			103 AS idEventType, 
			U.dtExpires AS [timestamp],
			U.dtExpires AS eventDate,
			U.idUser AS idObject,
			U.idUser AS idObjectRelated,
			U.idUser AS idObjectUser,
			NULL AS idExecutingUser -- U.idUser
		FROM #Users U
		WHERE U.dtExpires IS NOT NULL

		UNION -- join to certificate expirations

		SELECT
			1 AS idEventLog,
			U.idSite AS idSite,
			704 AS idEventType,
			CR.dtExpires AS [timestamp],
			CR.dtExpires AS eventDate,
			CR.idCertificate AS idObject,
			CR.idCertificateRecord AS idObjectRelated,
			CR.idUser AS idObjectUser,
			NULL AS idExecutingUser -- CR.idUser
		FROM #CertificateRecord CR
		LEFT JOIN #Users U ON U.idUser = CR.idUser

		UNION -- join to session reminder

		SELECT

			1 AS idEventLog,
			STI.idSite,
			401 AS idEventType,
			STI.dtStart AS [timestamp],
			STI.dtStart AS eventDate,
			STI.idStandupTraining AS idObject,
			STI.idStandupTrainingInstance AS idObjectRelated,
			NULL AS idObjectUser,
			NULL AS idExecutingUser
		FROM #StandupTrainingInstances STI

		UNION -- join to dynamic certification information

		SELECT
			1 AS idEventLog,
			U.idSite,
			CERTIFICATIONS_MAIN.idEventType,
			CERTIFICATIONS_MAIN.[timestamp],
			CERTIFICATIONS_MAIN.eventDate,
			CERTIFICATIONS_MAIN.idObject,
			CERTIFICATIONS_MAIN.idObjectRelated,
			CERTIFICATIONS_MAIN.idObjectUser, 
			NULL AS idExecutingUser
		FROM (			
			SELECT 
				602 AS idEventType, -- certification expires
				CUL.idCertificationToUserLink AS idObjectRelated,
				CUL.currentExpirationDate AS [timestamp],
				CUL.currentExpirationDate AS eventDate,	
				CUL.idCertification AS idObject,
				CUL.idUser AS idObjectUser 
			FROM #CertificationToUserLink CUL
			WHERE CUL.currentExpirationDate IS NOT NULL			
		) CERTIFICATIONS_MAIN
		LEFT JOIN #Users U ON U.idUser = CERTIFICATIONS_MAIN.idObjectUser

		UNION -- join to message center for unread messages

		SELECT
			1 AS idEventLog,
			IM.idSite,
			106 AS idEventType,
			IM.dtSent AS [timestamp],
			IM.dtSent AS eventDate,
			IM.idInboxMessage AS idObject,
			IM.idRecipient AS idObjectRelated,
			IM.idRecipient AS idObjectUser,
			NULL AS idExecutingUser
		FROM #InboxMessages IM		

		UNION -- join to new course moderated messages

		SELECT
			1 AS idEventLog,
			CMM.idSite,
			901 AS idEventType,
			CMM.[timestamp] AS [timestamp],
			CMM.[timestamp] AS eventDate,
			CMM.idCourseFeedMessage AS idObject,
			CMM.idCourse AS idObjectRelated,
			CMM.idAuthor AS idObjectUser,
			NULL AS idExecutingUser
		FROM #CourseModeratedMessages CMM

		UNION -- join to new group moderated messages

		SELECT
			1 AS idEventLog,
			GMM.idSite,
			902 AS idEventType,
			GMM.[timestamp] AS [timestamp],
			GMM.[timestamp] AS eventDate,
			GMM.idGroupFeedMessage AS idObject,
			GMM.idGroup AS idObjectRelated,
			GMM.idAuthor AS idObjectUser,
			NULL AS idExecutingUser
		FROM #GroupModeratedMessages GMM
		
		----------------- ADD MORE EVENTS BELOW THIS LINE -----------------		
	
	) EVENTLOG_MAIN
	ORDER BY EVENTLOG_MAIN.idEventType

	/* 

	build the email queue

	*/

	DECLARE @lastCascade DATETIME
	DECLARE @thisCascade DATETIME

	SELECT @lastCascade = CASE WHEN lastEventEmailQueueCascade IS NULL THEN DATEADD(minute, -15, GETUTCDATE()) ELSE lastEventEmailQueueCascade END FROM tblSystem
	SET @thisCascade = GETUTCDATE()

	CREATE TABLE #EventEmailQueue (
		idSite						INT,
		idEventLog					INT,
		idEventType					INT,
		idEventEmailNotification	INT,
		idEventTypeRecipient		INT,
		idObject					INT,
		idObjectRelated				INT,
		objectType					NVARCHAR(25),
		idObjectUser				INT,
		objectUserFullName			NVARCHAR(512),
		objectUserFirstName			NVARCHAR(255),
		objectUserLogin				NVARCHAR(512),
		objectUserEmail				NVARCHAR(255),
		idRecipient					INT,
		recipientLangString			NVARCHAR(10),
		recipientFullName			NVARCHAR(512),
		recipientFirstName			NVARCHAR(255),
		recipientLogin				NVARCHAR(512),
		recipientEmail				NVARCHAR(255),
		recipientTimezone			INT,
		recipientTimezoneDotNetName	NVARCHAR(255),
		[from]						NVARCHAR(255),
		copyTo						NVARCHAR(255),
		[priority]					INT,
		isHTMLBased					BIT,
		dtEvent						DATETIME,
		dtAction					DATETIME,
		dtActivation				DATETIME,
		attachmentType				INT
	)

	INSERT INTO #EventEmailQueue (
		idSite,
		idEventLog,
		idEventType,
		idEventEmailNotification,
		idEventTypeRecipient,
		idObject,
		idObjectRelated,
		objectType,
		idObjectUser,
		objectUserFullName,
		objectUserFirstName,
		objectUserLogin,
		objectUserEmail,
		idRecipient,
		recipientLangString,
		recipientFullName,
		recipientFirstName,
		recipientLogin,
		recipientEmail,
		recipientTimezone,
		recipientTimezoneDotNetName,
		[from],
		copyTo,
		[priority],
		isHTMLBased,
		dtEvent,
		dtAction,
		dtActivation,
		attachmentType
	)
	SELECT
		MAIN.idSite,
		MAIN.idEventLog,
		MAIN.idEventType,
		MAIN.idEventEmailNotification,
		MAIN.idEventTypeRecipient,
		MAIN.idObject,
		MAIN.idObjectRelated,
		MAIN.objectType,
		MAIN.idObjectUser,
		MAIN.objectUserFullName,
		MAIN.objectUserFirstName,
		MAIN.objectUserLogin,
		MAIN.objectUserEmail,
		CASE WHEN MAIN.specificEmailAddress IS NOT NULL THEN
			0
		ELSE
			MAIN.idRecipient
		END	AS idRecipient,
		CASE WHEN MAIN.idRecipient = 1 OR MAIN.specificEmailAddress IS NOT NULL THEN
			SL.code
		ELSE
			RU.languageString		
		END AS recipientLangString,
		CASE WHEN MAIN.idRecipient = 1 THEN
			'[Administrator]'
		ELSE
			CASE WHEN MAIN.specificEmailAddress IS NOT NULL THEN
				''
			ELSE
				RU.fullName
			END				
		END AS recipientFullName,
		CASE WHEN MAIN.idRecipient = 1 THEN
			'[Administrator]'
		ELSE
			CASE WHEN MAIN.specificEmailAddress IS NOT NULL THEN
				''
			ELSE
				RU.firstName
			END				
		END AS recipientFirstName,
		CASE WHEN MAIN.idRecipient = 1 THEN
			'administrator'
		ELSE
			CASE WHEN MAIN.specificEmailAddress IS NOT NULL THEN
				''
			ELSE
				RU.username
			END			
		END AS recipientLogin,
		CASE WHEN MAIN.idRecipient = 1 THEN
			S.contactEmail
		ELSE
			CASE WHEN MAIN.specificEmailAddress IS NOT NULL THEN
				MAIN.specificEmailAddress
			ELSE
				RU.email
			END
		END AS recipientEmail,
		CASE WHEN MAIN.idRecipient = 1 OR MAIN.specificEmailAddress IS NOT NULL THEN
			S.idTimezone
		ELSE
			RU.idTimezone
		END AS recipientTimezone,
		CASE WHEN MAIN.idRecipient = 1 OR MAIN.specificEmailAddress IS NOT NULL THEN
			STZ.dotNetName
		ELSE
			RU.timezoneDotNetName
		END AS recipientTimezoneDotNetName,
		MAIN.[from],
		MAIN.copyTo,
		MAIN.[priority],
		MAIN.isHTMLBased,
		MAIN.dtEvent,
		MAIN.dtAction,
		MAIN.dtActivation,
		MAIN.attachmentType
	FROM (
		SELECT
			EV.idSite AS idSite,
			EV.idEventLog AS idEventLog,
			EV.idEventType AS idEventType,
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN EEN.idEventEmailNotification ELSE EENDEFAULT.idEventEmailNotification END AS idEventEmailNotification,
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN EEN.idEventTypeRecipient ELSE EENDEFAULT.idEventTypeRecipient END AS idEventTypeRecipient,
			EV.idObject AS idObject,
			EV.idObjectRelated AS idObjectRelated,
			CASE WHEN EV.idEventType >= 100 AND EV.idEventType < 200 THEN 'user'
				 WHEN EV.idEventType >= 200 AND EV.idEventType < 300 AND EV.idEventType <> 207 AND EV.idEventType <> 208 THEN 'courseenrollment'
				 WHEN EV.idEventType  = 207 OR  EV.idEventType = 208 OR  EV.idEventType = 209 THEN 'courseenrollmentrequest'
				 WHEN EV.idEventType >= 300 AND EV.idEventType < 400 THEN 'lesson'
				 WHEN EV.idEventType >= 400 AND EV.idEventType < 500 THEN 'standuptraininginstance'
				 WHEN EV.idEventType >= 500 AND EV.idEventType < 600 THEN 'learningpathenrollment'
				 WHEN EV.idEventType >= 600 AND EV.idEventType < 700 THEN 'certification'
				 WHEN EV.idEventType >= 700 AND EV.idEventType < 800 THEN 'certificate'
				 WHEN EV.idEventType  = 901 THEN 'coursediscussionmessage'
				 WHEN EV.idEventType  = 902 THEN 'groupdiscussionmessage'
			ELSE
				NULL
			END AS objectType,
			CASE WHEN EV.idEventType IN (401, 402, 403, 406) AND ((EEN.idEventEmailNotification IS NOT NULL AND EEN.idEventTypeRecipient NOT IN (1,5)) OR (EENDEFAULT.idEventEmailNotification IS NOT NULL AND EENDEFAULT.idEventTypeRecipient NOT IN (1,5))) THEN
				STIUL.idUser
			ELSE
				EV.idObjectUser
			END AS idObjectUser,
			CASE WHEN EV.idEventType IN (401, 402, 403, 406) AND ((EEN.idEventEmailNotification IS NOT NULL AND EEN.idEventTypeRecipient NOT IN (1,5)) OR (EENDEFAULT.idEventEmailNotification IS NOT NULL AND EENDEFAULT.idEventTypeRecipient NOT IN (1,5))) THEN
				STIULU.fullName
			ELSE
				OU.fullName 
			END AS objectUserFullName,
			CASE WHEN EV.idEventType IN (401, 402, 403, 406) AND ((EEN.idEventEmailNotification IS NOT NULL AND EEN.idEventTypeRecipient NOT IN (1,5)) OR (EENDEFAULT.idEventEmailNotification IS NOT NULL AND EENDEFAULT.idEventTypeRecipient NOT IN (1,5))) THEN
				STIULU.firstName
			ELSE
				OU.firstName 
			END AS objectUserFirstName,
			CASE WHEN EV.idEventType IN (401, 402, 403, 406) AND ((EEN.idEventEmailNotification IS NOT NULL AND EEN.idEventTypeRecipient NOT IN (1,5)) OR (EENDEFAULT.idEventEmailNotification IS NOT NULL AND EENDEFAULT.idEventTypeRecipient NOT IN (1,5))) THEN
				STIULU.username
			ELSE
				OU.username 
			END AS objectUserLogin,
			CASE WHEN EV.idEventType IN (401, 402, 403, 406) AND ((EEN.idEventEmailNotification IS NOT NULL AND EEN.idEventTypeRecipient NOT IN (1,5)) OR (EENDEFAULT.idEventEmailNotification IS NOT NULL AND EENDEFAULT.idEventTypeRecipient NOT IN (1,5))) THEN
				STIULU.email
			ELSE			
				OU.email 
			END AS objectUserEmail,
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN									-- specific notification
				CASE WHEN EEN.idEventTypeRecipient = 1 THEN 1										-- system administrator
					 WHEN EEN.idEventTypeRecipient IN (2, 3, 6, 7) THEN								-- user(s)/learner(s)
						CASE WHEN EV.idEventType IN (401, 402, 403, 406) THEN						-- session meets, instructor added/changed, instructor removed, date/time changed
							STIUL.idUser
						ELSE
							EV.idObjectUser
						END
					 WHEN EEN.idEventTypeRecipient IN (4, 8) THEN										-- supervisor(s)
						CASE WHEN EV.idEventType IN (401) THEN											-- session meets
							STIULSU.idSupervisor
						ELSE
							SU.idSupervisor
						END
					 WHEN EEN.idEventTypeRecipient = 5 THEN STI.idInstructor							-- instructor
					 WHEN EEN.idEventTypeRecipient = 9 THEN												-- proctor(s)
						CASE WHEN EV.idEventType = 304 THEN OJTPR.idProctor -- ojt request submitted
							 WHEN EV.idEventType = 305 THEN TAPR.idProctor  -- task submitted
							 WHEN EV.idEventType = 606 THEN CTPR.idProctor  -- certification task submitted
						ELSE
							NULL
						END
					 WHEN EEN.idEventTypeRecipient = 11 THEN												-- approver(s)
						CASE WHEN EV.idEventType = 107 THEN URLink.idUser	-- user registration request
							 WHEN EV.idEventType = 209 THEN CEA.idUser		-- course enrollment request
						ELSE
							NULL
						END
				ELSE
					NULL
				END
			ELSE																					-- default notification
				CASE WHEN EENDEFAULT.idEventTypeRecipient = 1 THEN 1									-- system administrator
					 WHEN EENDEFAULT.idEventTypeRecipient IN (2, 3, 6, 7) THEN							-- user(s)/learner(s)
						CASE WHEN EV.idEventType IN (401, 402, 403, 406) THEN							-- session meets, instructor added/changed, instructor removed, date/time changed
							STIUL.idUser
						ELSE 
							EV.idObjectUser 
						END
					 WHEN EENDEFAULT.idEventTypeRecipient IN (4, 8) THEN								-- supervisor(s)
						CASE WHEN EV.idEventType IN (401) THEN											-- session meets
							STIULSU.idSupervisor
						ELSE
							SU.idSupervisor
						END
					 WHEN EENDEFAULT.idEventTypeRecipient = 5	THEN STI.idInstructor					-- instructor
					 WHEN EENDEFAULT.idEventTypeRecipient = 9 THEN										-- proctor(s)
						CASE WHEN EV.idEventType = 304 THEN OJTPR.idProctor -- ojt request submitted
							 WHEN EV.idEventType = 305 THEN TAPR.idProctor  -- task submitted
							 WHEN EV.idEventType = 606 THEN CTPR.idProctor  -- certification task submitted
						ELSE
							NULL
						END
					 WHEN EENDEFAULT.idEventTypeRecipient = 10 THEN	
						CASE WHEN EV.idEventType = 901 THEN CMOD.idModerator	-- course discussion message
							 WHEN EV.idEventType = 902 THEN GMOD.idModerator	-- group discussion message
						ELSE 
							NULL
						END
					 WHEN EENDEFAULT.idEventTypeRecipient = 11 THEN												-- approver(s)
						CASE WHEN EV.idEventType = 107 THEN URLink.idUser	-- user registration request
							 WHEN EV.idEventType = 209 THEN CEA.idUser		-- course enrollment request
						ELSE
							NULL
						END
				ELSE
					NULL
				END
			END AS idRecipient,
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN -- specific notification
				EEN.[from]
			ELSE													-- default notification
				EENDEFAULT.[from]
			END AS [from],
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN -- specific notification
				EEN.copyTo
			ELSE													-- default notification
				EENDEFAULT.copyTo
			END AS copyTo,
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN -- specific notification
				EEN.specificEmailAddress
			ELSE													-- default notification
				EENDEFAULT.specificEmailAddress
			END AS specificEmailAddress,
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN -- specific notification
				EEN.[priority]
			ELSE													-- default notification
				EENDEFAULT.[priority]
			END AS [priority],
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN -- specific notification
				EEN.isHTMLBased
			ELSE													-- default notification
				EENDEFAULT.isHTMLBased
			END AS isHTMLBased,
			EV.eventDate AS dtEvent,
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN														-- specific notification
				CASE WHEN EEN.interval IS NULL THEN
					EV.eventDate
				ELSE
					CASE WHEN EEN.timeFrame = 'yyyy' THEN DATEADD(year, EEN.interval, EV.eventDate)						-- "before" intervals are negative numbers
						 WHEN EEN.timeFrame = 'm' THEN DATEADD(month, EEN.interval, EV.eventDate)
						 WHEN EEN.timeFrame = 'ww' THEN DATEADD(week, EEN.interval, EV.eventDate)
						 WHEN EEN.timeFrame = 'd' THEN DATEADD(day, EEN.interval, EV.eventDate)
					ELSE
						NULL
					END
				END
			ELSE																										-- default notification
				CASE WHEN EENDEFAULT.interval IS NULL THEN
					EV.eventDate
				ELSE
					CASE WHEN EENDEFAULT.timeFrame = 'yyyy' THEN DATEADD(year, EENDEFAULT.interval, EV.eventDate)		-- "before" intervals are negative numbers
						 WHEN EENDEFAULT.timeFrame = 'm' THEN DATEADD(month, EENDEFAULT.interval, EV.eventDate)
						 WHEN EENDEFAULT.timeFrame = 'ww' THEN DATEADD(week, EENDEFAULT.interval, EV.eventDate)
						 WHEN EENDEFAULT.timeFrame = 'd' THEN DATEADD(day, EENDEFAULT.interval, EV.eventDate)
					ELSE
						NULL
					END
				END
			END AS dtAction,
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN -- specific notification
				EEN.dtActivation
			ELSE													-- default notification
				EENDEFAULT.dtActivation
			END AS dtActivation,
			CASE WHEN EEN.idEventEmailNotification IS NOT NULL THEN -- specific notification
				EEN.attachmentType
			ELSE													-- default notification
				EENDEFAULT.attachmentType
			END AS attachmentType
		FROM #Events EV
		LEFT JOIN tblEventEmailNotification EEN ON EEN.idEventType = EV.idEventType AND EEN.idSite = EV.idSite AND EEN.idObject = EV.idObject AND (EEN.isDeleted IS NULL OR EEN.isDeleted = 0) AND EEN.isActive = 1
		LEFT JOIN tblEventEmailNotification EENDEFAULT ON EENDEFAULT.idEventType = EV.idEventType AND EENDEFAULT.idSite = EV.idSite AND EENDEFAULT.idObject IS NULL AND (EENDEFAULT.isDeleted IS NULL OR EENDEFAULT.isDeleted = 0) AND EENDEFAULT.isActive = 1
		LEFT JOIN #Users OU ON OU.idUser = EV.idObjectUser		
		LEFT JOIN tblUserToSupervisorLink SU ON SU.idUser = EV.idObjectUser
		LEFT JOIN tblUserToRoleLink URLink ON URLink.idRole IN (SELECT distinct idRole FROM tblRoleToPermissionLink WHERE idPermission = 115 AND idSite = EV.idSite) AND URLink.idSite = EV.idSite AND EV.idEventType = 107 -- User Registration Request Approvers, 115 is idPermission for UsersAndGroups_UserRegistrationApproval
		LEFT JOIN #StandupTrainingInstances STI ON STI.idStandupTrainingInstance = EV.idObjectRelated AND EV.idEventType >= 400 AND EV.idEventType < 500
		LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandupTrainingInstance = STI.idStandupTrainingInstance AND STIUL.isWaitingList = 0 AND EV.idEventType IN (401, 402, 403, 406) -- session meets, instructor added/changed, instructor removed, date/time changed
		LEFT JOIN tblUserToSupervisorLink STIULSU ON STIULSU.idUser = STIUL.idUser -- for session based events, we need to get the rostered user's supervisor
		LEFT JOIN #Users STIULU ON STIULU.idUser = STIUL.idUser -- for session based events, we need to get the rostered user
		LEFT JOIN #OJTProctors OJTPR ON OJTPR.idLesson = EV.idObject AND OJTPR.idLearner = OU.idUser AND EV.idEventType = 304 -- ojt request submitted
		LEFT JOIN #TaskProctors TAPR ON TAPR.idLesson = EV.idObject AND TAPR.idLearner = OU.idUser AND EV.idEventType = 305 -- task submitted
		LEFT JOIN #CertificationTaskProctors CTPR ON CTPR.idCertificationModuleRequirement = EV.idObject AND CTPR.idLearner = OU.idUser AND EV.idEventType = 606 -- certification task submitted
		LEFT JOIN tblCourseEnrollmentApprover CEA ON CEA.idCourse = EV.idObject AND EV.idEventType = 209 -- course enrollment approvers
		LEFT JOIN #CourseModerators CMOD ON CMOD.idCourse = EV.idObjectRelated AND EV.idEventType = 901 -- course discussion moderators
		LEFT JOIN #GroupModerators GMOD ON GMOD.idGroup = EV.idObjectRelated AND EV.idEventType = 902 -- group discussion moderators
		WHERE (EEN.idEventEmailNotification IS NOT NULL OR EENDEFAULT.idEventEmailNotification IS NOT NULL)
		AND (
				@idEventLog IS NULL -- all events cascaded
				OR
				@idEventLog = EV.idEventLog -- only sepcific event cascaded
			)
	) MAIN
	LEFT JOIN #Users RU ON RU.idUser = MAIN.idRecipient AND MAIN.idRecipient > 1
	LEFT JOIN tblSite S ON S.idSite = MAIN.idSite
	LEFT JOIN tblLanguage SL ON SL.idLanguage = S.idLanguage
	LEFT JOIN tblTimezone STZ ON STZ.idTimezone = S.idTimezone
	WHERE NOT EXISTS ( -- entry has not already been cascaded
		SELECT 1
		FROM tblEventEmailQueue	AS EEQ
		WHERE 1 = 1	
		AND	EEQ.idEventLog = MAIN.idEventLog
		AND	EEQ.idObject = MAIN.idObject
		AND	EEQ.idObjectRelated = MAIN.idObjectRelated
		AND EEQ.idEventEmailNotification = MAIN.idEventEmailNotification 
		AND (EEQ.idRecipient = MAIN.idRecipient OR EEQ.idRecipient = 0)
	)
	AND (MAIN.idRecipient IS NOT NULL OR MAIN.specificEmailAddress IS NOT NULL)  -- recipient exists
	AND (MAIN.idRecipient = 1 OR (RU.email IS NOT NULL AND (RU.dtExpires IS NULL OR RU.dtExpires > @thisCascade) AND RU.isActive = 1 AND RU.optOutOfEmailNotifications = 0)  OR MAIN.specificEmailAddress IS NOT NULL) -- recipient is admin or has an email address and is active
	AND MAIN.dtAction >= DATEADD(MINUTE, -5, @lastCascade) -- targeted send date has not already passed (5 minutes prior to previous cascade)
	AND MAIN.dtAction <= DATEADD(MINUTE, 5, @thisCascade) -- targeted send date is within the next 5 minutes.
	AND MAIN.dtAction >= MAIN.dtActivation -- the targeted send date must occur after the notification was activated
	AND (
			MAIN.idEventType NOT IN (404, 405) 
			OR (
				(MAIN.idEventType = 404 AND MAIN.idEventTypeRecipient IN (2,3,6,7) AND (SELECT MIN(dtStart) FROM tblStandUpTrainingInstanceMeetingTime WHERE idStandUpTrainingInstance = MAIN.idObjectRelated) >= GETUTCDATE())
				OR 
				(MAIN.idEventType = 404 AND MAIN.idEventTypeRecipient IN (1,4,5,8,9))
				OR
				(MAIN.idEventType = 405 AND MAIN.idEventTypeRecipient IN (2,3,6,7) AND (SELECT MIN(dtStart) FROM tblStandUpTrainingInstanceMeetingTime WHERE idStandUpTrainingInstance = MAIN.idObjectRelated) >= GETUTCDATE())
				OR 
				(MAIN.idEventType = 405 AND MAIN.idEventTypeRecipient IN (1,4,5,8,9))
			   )
		) -- for "Session Joined" and "Session Revoked", only send to learner if the session is in the future	

	/*

	insert items into the event email queue

	*/

	INSERT INTO tblEventEmailQueue (
		idSite,
		idEventLog,
		idEventType,
		idEventEmailNotification,
		idEventTypeRecipient,
		idObject,
		idObjectRelated,
		objectType,
		idObjectUser,
		objectUserFullName,
		objectUserFirstName,
		objectUserLogin,
		objectUserEmail,
		idRecipient,
		recipientLangString,
		recipientFullName,
		recipientFirstName,
		recipientLogin,
		recipientEmail,
		recipientTimezone,
		recipientTimezoneDotNetName,
		[from],
		copyTo,
		[priority],
		isHTMLBased,
		dtEvent,
		dtAction,
		dtActivation,
		attachmentType
	)
	SELECT DISTINCT
		idSite,
		idEventLog,
		idEventType,
		idEventEmailNotification,
		idEventTypeRecipient,
		idObject,
		idObjectRelated,
		objectType,
		idObjectUser,
		objectUserFullName,
		objectUserFirstName,
		objectUserLogin,
		objectUserEmail,
		idRecipient,
		recipientLangString,
		recipientFullName,
		recipientFirstName,
		recipientLogin,
		recipientEmail,
		recipientTimezone,
		recipientTimezoneDotNetName,
		[from],
		copyTo,
		[priority],
		isHTMLBased,
		dtEvent,
		dtAction,
		dtActivation,
		attachmentType
	FROM #EventEmailQueue

	/*

	update the last cascade date in tblSystem

	*/

	IF (SELECT COUNT(1) FROM tblSystem) = 1
		UPDATE tblSystem SET lastEventEmailQueueCascade = @thisCascade
	ELSE
		INSERT INTO tblSystem (lastEventEmailQueueCascade) VALUES (@thisCascade)

	/*

	drop the temporary tables

	*/

	DROP TABLE #Users
	DROP TABLE #Enrollments
	DROP TABLE #LearningPathEnrollments
	DROP TABLE #CertificateRecord
	DROP TABLE #StandupTrainingInstances
	DROP TABLE #OJTProctors
	DROP TABLE #TaskProctors
	DROP TABLE #CertificationToUserLink
	DROP TABLE #CertificationTaskProctors
	DROP TABLE #InboxMessages
	DROP TABLE #CourseModeratedMessages
	DROP TABLE #GroupModeratedMessages
	DROP TABLE #CourseModerators
	DROP TABLE #GroupModerators
	DROP TABLE #Events
	DROP TABLE #EventEmailQueue

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO