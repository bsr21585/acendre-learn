-- =====================================================================
-- PROCEDURE: [Site.GetAvailableLanguages]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Site.GetAvailableLanguages]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.GetAvailableLanguages]
GO

/*
Return all available languages for a site.
*/
CREATE PROCEDURE [Site.GetAvailableLanguages]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idSite					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSite
		WHERE idSite = @idSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'SiteGetAvailableLanguages_DetailsNotFound'
		RETURN 1
		END
	
	SELECT 
		L.code as [langString]
	FROM tblSiteAvailableLanguage SAL
	LEFT JOIN tblLanguage L ON L.idLanguage = SAL.idLanguage
	WHERE SAL.idSite = @idSite
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO