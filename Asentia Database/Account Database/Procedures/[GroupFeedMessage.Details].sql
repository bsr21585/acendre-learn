-- =====================================================================
-- PROCEDURE: [GroupFeedMessage.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GroupFeedMessage.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GroupFeedMessage.Details]
GO

/*

Return all the properties for a given group feed message id. 

*/

CREATE PROCEDURE [GroupFeedMessage.Details]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, -- default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0, -- will fail if not specified
	
	@idGroupFeedMessage			INT					OUTPUT,
	@idSite						INT					OUTPUT,
	@idGroup					INT					OUTPUT,
	@idLanguage					INT					OUTPUT,
	@idAuthor					INT					OUTPUT,
	@idParentGroupFeedMessage	INT					OUTPUT,
	@message					NVARCHAR(MAX)		OUTPUT,
	@timestamp					DATETIME			OUTPUT,
	@dtApproved					DATETIME			OUTPUT,
	@idApprover					INT					OUTPUT,
	@isApproved					BIT					OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that the object exists

	*/

	IF (
		SELECT COUNT(1)
		FROM tblGroupFeedMessage
		WHERE idGroupFeedMessage = @idGroupFeedMessage
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupFeedMessageDetails_NoRecordFound'
		RETURN 1
		END
		
	/* get the data */

	SELECT
		@idGroupFeedMessage			= GFM.idGroupFeedMessage,
		@idSite						= GFM.idSite,
		@idGroup					= GFM.idGroup,
		@idLanguage					= GFM.idLanguage,
		@idAuthor					= GFM.idAuthor,
		@idParentGroupFeedMessage	= GFM.idParentGroupFeedMessage,
		@message					= GFM.[message],
		@timestamp					= GFM.[timestamp],
		@dtApproved					= GFM.dtApproved,
		@idApprover					= GFM.idApprover,
		@isApproved					= GFM.isApproved
	FROM tblGroupFeedMessage GFM
	WHERE GFM.idGroupFeedMessage = @idGroupFeedMessage
	AND GFM.idSite = @idCallerSite
		
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'GroupFeedMessageDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO