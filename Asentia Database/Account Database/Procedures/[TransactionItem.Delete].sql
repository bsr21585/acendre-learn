-- =====================================================================
-- PROCEDURE: [TransactionItem.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.Delete]
GO

/*

Deletes transaction item(s).

*/

CREATE PROCEDURE [TransactionItem.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@TransactionItems		IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @TransactionItems) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'TransactionItemDelete_NoRecordsSelected'
		RETURN 1
		END
			
	/*
	
	validate that all transaction items exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @TransactionItems TI
		LEFT JOIN tblTransactionItem T ON T.idTransactionItem = TI.id
		WHERE T.idSite IS NULL
		OR T.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemDelete_NoRecordFound'
		RETURN 1 
		END
		
	/*

	delete the transaction items

	*/

	DELETE FROM tblTransactionItem
	WHERE idTransactionItem IN (
		SELECT id
		FROM @TransactionItems
	)
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
