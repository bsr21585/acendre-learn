-- =====================================================================
-- PROCEDURE: [Course.LessonStatistics]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Course.LessonStatistics]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.LessonStatistics]
GO

/*

Gets lesson statistics for a course.

*/

CREATE PROCEDURE [Course.LessonStatistics]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCourse				INT,
	@totalModules			INT				OUTPUT,
	@easiestLesson			NVARCHAR(255)	OUTPUT,
	@hardestLesson			NVARCHAR(255)	OUTPUT,	
	@averageTimePerLesson	NVARCHAR(8)		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	get the total number of modules

	*/

	SELECT @totalModules = COUNT(1) FROM tblLesson L WHERE L.idCourse = @idCourse AND (L.isDeleted = 0 OR L.isDeleted IS NULL)

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	aggragate all lesson data into a temporary table, we'll work from that table

	*/

	CREATE TABLE #LessonData (
		idLesson			INT,		
		averageScoreScaled	FLOAT,
		averageTotalTime	FLOAT
	)

	INSERT INTO #LessonData (
		idLesson,		
		averageScoreScaled,
		averageTotalTime
	)
	SELECT
		LD.idLesson,		
		AVG(DSCO.scoreScaled),
		AVG(DSCO.totalTime)
	FROM [tblData-Lesson] LD
	LEFT JOIN [tblData-SCO] DSCO ON DSCO.[idData-Lesson] = LD.[idData-Lesson]
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson	
	WHERE L.idCourse = @idCourse
	AND (L.isDeleted = 0 OR L.isDeleted IS NULL)
	AND DSCO.totalTime IS NOT NULL
	GROUP BY LD.idLesson
	
	/*

	get the easiest lesson

	*/
	
	SELECT TOP 1 
		@easiestLesson = CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE L.title END 
	FROM #LessonData LD
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson
	LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
	ORDER BY averageScoreScaled DESC, averageTotalTime ASC
		
	/*

	get the hardest lesson

	*/

	SELECT TOP 1 
		@hardestLesson = CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE L.title END 
	FROM #LessonData LD
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson
	LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
	ORDER BY averageScoreScaled ASC, averageTotalTime DESC	

	/*

	get the average total time over all lessons

	*/

	SELECT @averageTimePerLesson = CONVERT(NVARCHAR(8), DATEADD(SECOND, AVG(averageTotalTime), 0), 108) FROM #LessonData	
		
	-- DROP THE TEMPORARY TABLES
	DROP TABLE #LessonData		

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO