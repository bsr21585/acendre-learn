-- =====================================================================
-- PROCEDURE: [User.SaveSupervisors]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.SaveSupervisors]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.SaveSupervisors]
GO

/*

Saves experts for courses

*/

CREATE PROCEDURE [User.SaveSupervisors]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idUser					INT,
	@Supervisors			IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
			
	/*
	
	validate that all experts exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Supervisors SV
		LEFT JOIN tblUser U ON U.idUser = SV.id
		WHERE U.idSite IS NULL
		OR U.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'UserSaveSupervisors_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	validate that the course belongs to the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUser U
		WHERE U.idUser = @idUser
		AND (U.idSite IS NULL OR U.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'UserSaveSupervisors_DetailsNotFound'  
		RETURN 1 
		END
		
	/*
	
	remove all supervisors for this user from the user to supervisor link table
	(if there are any in the table variable, otherwise it means there are none)
	
	*/
	
	DELETE FROM tblUserToSupervisorLink
	WHERE idUser = @idUser
	
	/*

	insert the user supervisors for ths user into user to supervisor link table

	*/

	IF (SELECT COUNT(1) FROM @Supervisors) > 0
		BEGIN

		INSERT INTO tblUserToSupervisorLink (
			idSite,
			idUser,
			idSupervisor
		)
		SELECT
			@idCallerSite,
			@idUser,
			EX.id
		FROM @Supervisors EX
		WHERE NOT EXISTS (SELECT 1 
						  FROM tblUserToSupervisorLink
						  WHERE idUser = @idUser
						  AND idSupervisor = EX.id)

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO