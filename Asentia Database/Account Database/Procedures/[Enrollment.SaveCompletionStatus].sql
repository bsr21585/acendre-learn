-- =====================================================================
-- PROCEDURE: [Enrollment.SaveCompletionStatus]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.SaveCompletionStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.SaveCompletionStatus]
GO

/*

Saves the completion status of an enrollment.

*/

CREATE PROCEDURE [Enrollment.SaveCompletionStatus]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idEnrollment						INT				OUTPUT,
	@dtCompleted						DATETIME
)
AS

	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	check that the enrollment id exists

	*/

	IF (SELECT COUNT(1) FROM tblEnrollment WHERE idEnrollment = @idEnrollment AND idSite = @idCallerSite) < 1
		BEGIN
		SET @idEnrollment = @idEnrollment
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentSaveCompletionStatus_NoRecordFound'
		RETURN 1
		END
	
	/*

	determine if we're going to write an event log entry for this completion

	*/

	DECLARE @writeEventLogEntry BIT
	SET @writeEventLogEntry = 0

	IF (SELECT dtCompleted FROM tblEnrollment WHERE idEnrollment = @idEnrollment) IS NULL AND @dtCompleted IS NOT NULL
		BEGIN
		SET @writeEventLogEntry = 1
		END

	/*

	save the data

	*/

	-- update completion status
	UPDATE tblEnrollment SET
		dtCompleted = @dtCompleted
	WHERE idEnrollment = @idEnrollment

	/*

	write the event log entry if it needs to be written

	*/

	IF @writeEventLogEntry = 1
		BEGIN

		DECLARE @eventLogItem EventLogItemObjects
		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, idCourse, idEnrollment, idUser FROM tblEnrollment WHERE idEnrollment = @idEnrollment
		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 205, @dtCompleted, @eventLogItem

		END

	/* 

	declare and set variables we need to work with for certificate award and learning path enrollment evaluation

	*/

	DECLARE @idEnrollmentUser		INT
	DECLARE @idEnrollmentCourse		INT
	DECLARE @idEnrollmentTimezone	INT

	SELECT
		@idEnrollmentUser = idUser,
		@idEnrollmentCourse = idCourse,
		@idEnrollmentTimezone = idTimezone
	FROM tblEnrollment
	WHERE idEnrollment = @idEnrollment

	/*

	award certificates

	*/

	EXEC [Certificate.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollmentUser, @idEnrollmentCourse, 'course', 'completed', @idEnrollmentTimezone

	/*

	check for learning path enrollment completions

	*/

	EXEC [LearningPathEnrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollmentUser, @idEnrollmentCourse


	-- set the enrollment's id
	SET @idEnrollment = @idEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		