-- =====================================================================
-- PROCEDURE: [Course.GetLessons]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.GetLessons]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.GetLessons]
GO

/*

Returns a recordset of lesson ids and titles that belong to a course.

*/

CREATE PROCEDURE [Course.GetLessons]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idCourse				INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		SELECT
			DISTINCT
			L.idLesson, 
			CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE L.title END AS title,
			CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 1 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasOnlineContent,
			CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasILT,
			CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 3 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasTask,
			CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 4 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasOJT,
			(SELECT TOP 1 idObject FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson = L.idLesson) AS idStandupTraining,
			L.[order]
		FROM tblLesson L
		LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
		WHERE L.idSite = @idCallerSite
		AND (L.isDeleted IS NULL OR L.isDeleted = 0)
		AND L.idCourse = @idCourse
		ORDER BY [order]

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			L.idLesson, 
			CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE L.title END AS title,
			CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 1 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasOnlineContent,
			CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasILT,
			CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 3 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasTask,
			CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 4 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasOJT,
			(SELECT TOP 1 idObject FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson = L.idLesson) AS idStandupTraining,
			L.[order]
		FROM tblLessonLanguage LL
		INNER JOIN CONTAINSTABLE(tblLessonLanguage, *, @searchParam) K ON K.[key] = LL.idLessonLanguage AND LL.idLanguage = @idCallerLanguage
		LEFT JOIN tblLesson L ON L.idLesson = LL.idLesson
		WHERE L.idSite = @idCallerSite
		AND (L.isDeleted IS NULL OR L.isDeleted = 0)
		AND L.idCourse = @idCourse
		ORDER BY [order]

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO