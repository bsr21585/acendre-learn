-- =====================================================================
-- PROCEDURE: [Lesson.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Lesson.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Lesson.Save]
GO

/*

Adds a new lesson or updates an existing one.

*/

CREATE PROCEDURE [Lesson.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idLesson				INT				OUTPUT,
	@idCourse				INT,
	@title					NVARCHAR(255),
	@shortDescription		NVARCHAR(512),
	@longDescription		NVARCHAR(MAX),
	@searchTags				NVARCHAR(512),
	@isOptional				BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1)
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'LessonSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/

	/*
	
	validate uniqueness within same parent object
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLesson
		WHERE (isDeleted IS NULL OR isDeleted = 0) -- ignore deleted records
		AND idSite = @idCallerSite
		AND idCourse = @idCourse
		AND title = @title
		AND (
			@idLesson IS NULL
			OR @idLesson <> idLesson
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'LessonSave_FieldNotUnique'
		RETURN 1 
		END

	/*
	
	validate uniqueness within language, and same parent object
	
	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF( 
		SELECT COUNT(1)
		FROM tblLesson L
		LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idDefaultLanguage -- same language
		WHERE L.idSite = @idCallerSite -- same site
		AND L.idLesson <> @idLesson -- not the same lesson
		AND L.idCourse = @idCourse
		AND LL.title = @title -- validate parameter: title
		AND (L.isDeleted IS NULL OR L.isDeleted = 0) -- ignore deleted records
		) > 0 
		
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'LessonSave_FieldNotUnique'
		RETURN 1
		END

	/*

	check XSS vulnerabilities

	*/

	-- longDescription

	IF (@longDescription LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LessonSave_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LessonSave_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LessonSave_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LessonSave_DescTagNotAllowed_Iframe'
	RETURN 1 
	END

	/*

	generate random revision code
	revision code is changed each time a course is saved

	*/

	DECLARE @revcode NVARCHAR(32)
	DECLARE @randomIsUnique BIT
	DECLARE @i INT

	SET @i = 0
	SET @randomIsUnique = 1

	WHILE @randomIsUnique > 0 AND @i < 100

		BEGIN

		EXEC [System.GenerateRandomString] 32, @revcode OUTPUT

		SET @randomIsUnique = (SELECT COUNT(1) FROM tblLesson WHERE revcode = @revcode AND idSite = @idCallerSite)
		SET @i = @i + 1

		END

	IF @i >= 100

		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'LessonSave_RevCodeNotUnique'
		RETURN 1
		END
		
	/*
	
	save the data
	
	*/
	
	IF (@idLesson = 0 OR @idLesson IS NULL)
		
		BEGIN

		-- get the "order" number for the new lesson

		DECLARE @order INT

		SELECT 
			@order = CASE WHEN MAX([order]) IS NULL THEN 1 ELSE MAX([order]) + 1 END
		FROM tblLesson
		WHERE idCourse = @idCourse
		
		-- insert the new lesson
		
		INSERT INTO tblLesson (
			idSite,
			idCourse,
			title,
			revcode,
			shortDescription,
			longDescription,
			dtCreated,
			dtModified,
			isDeleted,
			dtDeleted,
			[order],
			searchTags,
			isOptional
		)
		VALUES (
			@idCallerSite,
			@idCourse,
			@title,
			@revcode,
			@shortDescription,
			@longDescription,
			GETUTCDATE(),
			GETUTCDATE(),
			0,
			NULL,
			@order,
			@searchTags,
			@isOptional
		)
		
		-- get the new id

		SET @idLesson = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the lesson id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblLesson WHERE idLesson = @idLesson AND idSite = @idCallerSite AND (isDeleted IS NULL OR isDeleted = 0)) < 1	
			BEGIN

			SET @idLesson = @idLesson
			SET @Return_Code = 1
			SET @Error_Description_Code = 'LessonSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing lesson's properties
		
		UPDATE tblLesson SET
			title = @title,
			revcode = @revcode,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			dtModified = GETUTCDATE(),
			searchTags = @searchTags,
			isOptional = @isOptional
		WHERE idLesson = @idLesson
		
		-- get the lesson's id

		SET @idLesson = @idLesson
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/
	
	IF (SELECT COUNT(1) FROM tblLessonLanguage LL WHERE LL.idLesson = @idLesson AND LL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblLessonLanguage SET
			title = @title,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			searchTags = @searchTags
		WHERE idLesson = @idLesson
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblLessonLanguage (
			idSite,
			idLesson,
			idLanguage,
			title,
			shortDescription,
			longDescription,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idLesson,
			@idDefaultLanguage,
			@title,
			@shortDescription,
			@longDescription,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblLessonLanguage LL
			WHERE LL.idLesson = @idLesson
			AND LL.idLanguage = @idDefaultLanguage
		)

		END

	/*

	update the course's last modified date/time

	*/

	UPDATE tblCourse SET dtModified = GETUTCDATE() WHERE idCourse = @idCourse

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO