-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.GetResources]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.GetResources]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.GetResources]
GO

/*

Returns a recordset of resource ids and names that belong to a standup training instance.

*/

CREATE PROCEDURE [StandupTrainingInstance.GetResources]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@idStandupTrainingInstance	INT,
	@searchParam				NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		SELECT
			DISTINCT
			R.idResource, 
			CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name
		FROM tblResource R
		LEFT JOIN tblResourceLanguage RL ON RL.idResource = R.idResource AND RL.idLanguage = @idCallerLanguage
		LEFT JOIN tblResourceToObjectLink ROL ON ROL.idResource = R.idResource AND ROL.objectType = 'standuptraining'
		WHERE R.idSite = @idCallerSite
		AND ROL.idObject = @idStandupTrainingInstance
		ORDER BY name

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			R.idResource,
			CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name
		FROM tblResourceLanguage RL
		INNER JOIN CONTAINSTABLE(tblResourceLanguage, *, @searchParam) K ON K.[key] = RL.idResourceLanguage AND RL.idLanguage = @idCallerLanguage
		LEFT JOIN tblResource R ON R.idResource = RL.idResource
		LEFT JOIN tblResourceToObjectLink ROL ON ROL.idResource = R.idResource AND ROL.objectType = 'standuptraining'
		WHERE R.idSite = @idCallerSite
		AND ROL.idObject = @idStandupTrainingInstance
		ORDER BY name

		END
		
	SET @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	