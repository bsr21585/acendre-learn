-- =====================================================================
-- PROCEDURE: [System.CleanUp]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CleanUp]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CleanUp]
GO

/*

Does general maintenance and clean up tasks for the system.
Any tasks that go in here should be very small tasks that are only a few 
blocks of code at most. Anything larger probably deserves its own procedure.

*/

CREATE PROCEDURE [System.CleanUp]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON

	/*

	SYNCHRONIZE COURSE DATA FOR ALL UNSYNCHRONIZED LEARNING PATH ENROLLMENTS THAT HAVE COURSES CONTAINING ILT

	*/

	-- get the learning path enrollment ids
	DECLARE @LearningPathEnrollmentsToSynchronize TABLE (idLearningPathEnrollment INT, idSite INT)

	INSERT INTO @LearningPathEnrollmentsToSynchronize (
		idLearningPathEnrollment,
		idSite
	)
	SELECT DISTINCT
		LPE.idLearningPathEnrollment,
		LPE.idSite
	FROM tblLearningPathEnrollment LPE
	LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
	LEFT JOIN tblLearningPathToCourseLink LPCL ON LPCL.idLearningPath = LP.idLearningPath
	LEFT JOIN tblCourse C ON C.idCourse = LPCL.idCourse
	WHERE LPCL.idCourse IN (SELECT L.idCourse 
							FROM tblLessonToContentLink LCL
							LEFT JOIN tblLesson L ON LCL.idLesson = L.idLesson
							WHERE LCL.idContentType = 2
							AND (L.isDeleted IS NULL OR L.isDeleted = 0))
	AND LPE.dtLastSynchronized IS NULL
	AND LPE.dtCompleted IS NULL
	AND (C.isDeleted IS NULL OR C.isDeleted = 0)

	-- cursor through the learning path enrollments and synchronize them
	DECLARE @idLearningPathEnrollment INT
	DECLARE @idLearningPathEnrollmentSite INT
	DECLARE learningPathEnrollmentSynchronizationCursor CURSOR LOCAL FOR
			SELECT
				idLearningPathEnrollment,
				idSite
			FROM @LearningPathEnrollmentsToSynchronize

	OPEN learningPathEnrollmentSynchronizationCursor

	FETCH NEXT FROM learningPathEnrollmentSynchronizationCursor INTO @idLearningPathEnrollment, @idLearningPathEnrollmentSite

	WHILE @@FETCH_STATUS = 0
		BEGIN
		EXEC [LearningPathEnrollment.SynchronizeCourseData] @Return_Code, @Error_Description_Code, @idLearningPathEnrollmentSite, @callerLangString, @idCaller, @idLearningPathEnrollment
		FETCH NEXT FROM learningPathEnrollmentSynchronizationCursor INTO @idLearningPathEnrollment, @idLearningPathEnrollmentSite
		END

	-- kill the cursor
	CLOSE learningPathEnrollmentSynchronizationCursor
	DEALLOCATE learningPathEnrollmentSynchronizationCursor


	/* 
	
	SYNCHRONIZE LESSON DATA FOR ALL UNSYNCHRONIZED ENROLLMENTS FOR COURSES THAT CONTAIN ILT 
	
	*/

	-- get the enrollment ids
	DECLARE @EnrollmentsToSynchronize TABLE (idEnrollment INT, idSite INT)

	INSERT INTO @EnrollmentsToSynchronize (
		idEnrollment,
		idSite
	)
	SELECT 
		E.idEnrollment,
		E.idSite
	FROM tblEnrollment E
	LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
	WHERE E.idCourse IN (SELECT L.idCourse 
						 FROM tblLessonToContentLink LCL
						 LEFT JOIN tblLesson L ON LCL.idLesson = L.idLesson
						 WHERE LCL.idContentType = 2
						 AND (L.isDeleted IS NULL OR L.isDeleted = 0))
	AND E.dtLastSynchronized IS NULL
	AND E.idActivityImport IS NULL
	AND E.dtCompleted IS NULL
	AND (C.isDeleted IS NULL OR C.isDeleted = 0)
	
	-- cursor through the enrollments and synchronize them
	DECLARE @idEnrollment INT
	DECLARE @idEnrollmentSite INT
	DECLARE enrollmentSynchronizationCursor CURSOR LOCAL FOR
			SELECT
				idEnrollment,
				idSite
			FROM @EnrollmentsToSynchronize

	OPEN enrollmentSynchronizationCursor

	FETCH NEXT FROM enrollmentSynchronizationCursor INTO @idEnrollment, @idEnrollmentSite

	WHILE @@FETCH_STATUS = 0
		BEGIN
		EXEC [Enrollment.SynchronizeLessonData] @Return_Code, @Error_Description_Code, @idEnrollmentSite, @callerLangString, @idCaller, @idEnrollment
		FETCH NEXT FROM enrollmentSynchronizationCursor INTO @idEnrollment, @idEnrollmentSite
		END

	-- kill the cursor
	CLOSE enrollmentSynchronizationCursor
	DEALLOCATE enrollmentSynchronizationCursor	


	/* 
	
	CLEAN UP ALL EXPIRED SSO TOKENS 
	
	*/

	DELETE FROM tblSSOToken WHERE dtExpires < GETUTCDATE()	

	/* 
	
	CLEAN UP ALL EXPIRED USER LOCKOUT 
	
	*/

	DELETE FROM tblUserLockout WHERE dtLockoutUntil < GETUTCDATE()	

	/* 
	
	CLEAN UP USER LOG
	
	*/

	DELETE FROM tblUserLog
	WHERE NOT EXISTS (
	SELECT 1
	FROM tblSite
	WHERE tblSite.idSite = tblUserLog.idSite
	)
	OR tblUserLog.timestamp < DATEADD(YEAR, -2, GETUTCDATE())

	/* 
		
	HARD DELETE ALL DELETED USERS FROM SITES WHERE Privacy.PermanentlyDeleteUsers IS SET TO TRUE
	AND THE USERS HAVE BEEN DELETED FOR LONGER THAN 1 HOUR
	
	*/

	DECLARE @UsersToHardDelete IDTable

	INSERT INTO @UsersToHardDelete (
		id
	) 
	SELECT 
		U.idUser 
	FROM tblUser U
	WHERE U.isDeleted = 1 
	AND U.dtDeleted < DATEADD(HOUR, -1, GETUTCDATE()) 
	AND U.idSite IN (SELECT DISTINCT SP.idSite FROM tblSiteParam SP WHERE SP.[param] = 'Privacy.PermanentlyRemoveDeletedUsers' AND SP.value = 'True' AND SP.idSite > 1)
	
	EXEC [User.HardDelete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @UsersToHardDelete


	/* RETURN */
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO