-- =====================================================================
-- PROCEDURE: [RuleSetEnrollment.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetEnrollment.Details]
GO

/*

Returns all the properties for a given ruleset enrollment id.

*/

CREATE PROCEDURE [RuleSetEnrollment.Details]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idRuleSetEnrollment				INT				OUTPUT,
	@idSite								INT				OUTPUT,
	@idCourse							INT				OUTPUT,
	@idTimezone 						INT				OUTPUT,
	@priority 							INT				OUTPUT,
	@label								NVARCHAR(255)	OUTPUT,
	@isLockedByPrerequisites			BIT				OUTPUT,
	@isFixedDate 						BIT				OUTPUT,
	@dtStart 							DATETIME		OUTPUT,
	@dtEnd 								DATETIME		OUTPUT,
	@dtCreated							DATETIME		OUTPUT,
	@delayInterval 						INT				OUTPUT,
	@delayTimeframe						NVARCHAR(4)		OUTPUT,
	@dueInterval						INT				OUTPUT,
	@dueTimeframe						NVARCHAR(4)		OUTPUT,
	@recurInterval 						INT				OUTPUT,
	@recurTimeframe						NVARCHAR(4)		OUTPUT,
	@expiresFromStartInterval 			INT				OUTPUT,
	@expiresFromStartTimeframe			NVARCHAR(4)		OUTPUT,
	@expiresFromFirstLaunchInterval 	INT				OUTPUT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4)		OUTPUT,
	@forceReassignment					BIT				OUTPUT,
	@idParentRuleSetEnrollment			INT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblRuleSetEnrollment
		WHERE idRuleSetEnrollment = @idRuleSetEnrollment
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetEnrollmentDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
	
	SELECT
		@idRuleSetEnrollment				= RSE.idRuleSetEnrollment,
		@idSite								= RSE.idSite,
		@idCourse							= RSE.idCourse,
		@idTimezone							= RSE.idTimezone,
		@priority							= RSE.[priority],
		@label								= RSE.label,
		@isLockedByPrerequisites			= RSE.isLockedByPrerequisites,
		@isFixedDate						= RSE.isFixedDate,
		@dtStart							= RSE.dtStart,
		@dtEnd								= RSE.dtEnd,
		@dtCreated							= RSE.dtCreated,
		@delayInterval						= RSE.delayInterval,
		@delayTimeframe						= RSE.delayTimeframe,
		@dueInterval						= RSE.dueInterval,
		@dueTimeframe						= RSE.dueTimeframe,
		@recurInterval						= RSE.recurInterval,
		@recurTimeframe						= RSE.recurTimeframe,
		@expiresFromStartInterval			= RSE.expiresFromStartInterval,
		@expiresFromStartTimeframe			= RSE.expiresFromStartTimeframe,
		@expiresFromFirstLaunchInterval		= RSE.expiresFromFirstLaunchInterval,
		@expiresFromFirstLaunchTimeframe	= RSE.expiresFromFirstLaunchTimeframe,
		@forceReassignment					= RSE.forceReassignment,
		@idParentRuleSetEnrollment			= RSE.idParentRuleSetEnrollment
	FROM tblRuleSetEnrollment RSE
	WHERE RSE.idRuleSetEnrollment = @idRuleSetEnrollment
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetEnrollmentDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO