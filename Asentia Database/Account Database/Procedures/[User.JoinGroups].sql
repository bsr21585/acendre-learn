-- =====================================================================
-- PROCEDURE: [User.JoinGroups]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[User.JoinGroups]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.JoinGroups]
GO

/*

Joins a user to one or more groups.

*/

CREATE PROCEDURE [User.JoinGroups]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idUser					INT,
	@Groups					IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	make sure there are objects in the table parameter
	
	*/
	
	IF (SELECT COUNT(1) FROM @Groups) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'UserJoinGroups_NoGroupsFound'
		RETURN 1
		END
		
	/*
	
	validate that the user exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUser
		WHERE idSite = @idCallerSite
		AND @idUser = idUser
		) <> 1
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'UserJoinGroups_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	validate that all the groups exist in the same site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Groups GG
		LEFT JOIN tblGroup G ON G.idGroup = GG.id
		WHERE G.idGroup IS NULL -- group does not exist at all
		OR G.idSite <> @idCallerSite -- group does not exist within the same site
		) > 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'UserJoinGroups_GroupsNotInSameSite'
		RETURN 1 
		END
	
	/*
	
	attach the user to groups where they do not already exist
	
	*/
	
	INSERT INTO tblUserToGroupLink (
		idSite,
		idUser, 
		idGroup,
		created
		)
	SELECT 
		@idCallerSite,
		@idUser, 
		GG.id,
		GETUTCDATE()
	FROM @Groups GG
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblUserToGroupLink UGL
		WHERE UGL.idUser = @idUser
		AND UGL.idGroup = GG.id
		AND UGL.idRuleSet IS NULL -- manual join
	)
	
	SELECT @Return_Code = 0
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

