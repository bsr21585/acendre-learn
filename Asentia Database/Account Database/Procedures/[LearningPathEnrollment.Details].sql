-- =====================================================================
-- PROCEDURE: [LearningPathEnrollment.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPathEnrollment.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPathEnrollment.Details]
GO

/*

Return all the properties for a given learning path enrollment id.

*/

CREATE PROCEDURE [LearningPathEnrollment.Details]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idLearningPathEnrollment			INT				OUTPUT,
	@idSite								INT				OUTPUT,
	@idLearningPath						INT				OUTPUT,
	@idUser								INT				OUTPUT,
	@idRuleSetLearningPathEnrollment	INT				OUTPUT,	
	@idTimezone							INT				OUTPUT,
	@title								NVARCHAR(255)	OUTPUT,
	@dtStart							DATETIME		OUTPUT,
	@dtDue								DATETIME		OUTPUT,
	@dtExpiresFromStart					DATETIME		OUTPUT,
	@dtExpiresFromFirstLaunch			DATETIME		OUTPUT,
	@dtFirstLaunch						DATETIME		OUTPUT,
	@dtCompleted						DATETIME		OUTPUT,
	@dtCreated							DATETIME		OUTPUT,
	@dueInterval						INT				OUTPUT,
	@dueTimeFrame						NVARCHAR(4)		OUTPUT,
	@expiresFromStartInterval			INT				OUTPUT,
	@expiresFromStartTimeframe			NVARCHAR(4)		OUTPUT,
	@expiresFromFirstLaunchInterval		INT				OUTPUT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4)		OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLearningPathEnrollment
		WHERE idLearningPathEnrollment = @idLearningPathEnrollment
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LearningPathEnrollmentDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data
	
	*/		
	
	SELECT
		@idLearningPathEnrollment			= LPE.idLearningPathEnrollment,
		@idSite								= LPE.idSite,
		@idLearningPath						= LPE.idLearningPath,
		@idUser								= LPE.idUser,
		@idRuleSetLearningPathEnrollment	= LPE.idRuleSetLearningPathEnrollment,
		@idTimezone							= LPE.idTimezone,
		@title								= LPE.title,
		@dtStart							= LPE.dtStart,
		@dtDue								= LPE.dtDue,
		@dtCompleted						= LPE.dtCompleted,
		@dtCreated							= LPE.dtCreated,
		@dueInterval						= LPE.dueInterval,
		@dueTimeFrame						= LPE.dueTimeFrame,
		@expiresFromStartInterval			= LPE.expiresFromStartInterval,
		@expiresFromStartTimeframe			= LPE.expiresFromStartTimeframe,
		@expiresFromFirstLaunchInterval		= LPE.expiresFromFirstLaunchInterval,
		@expiresFromFirstLaunchTimeframe	= LPE.expiresFromFirstLaunchTimeframe
	FROM tblLearningPathEnrollment LPE
	WHERE LPE.idLearningPathEnrollment = @idLearningPathEnrollment
		
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LearningPathEnrollmentDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO