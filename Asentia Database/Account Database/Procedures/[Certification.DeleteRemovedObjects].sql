-- =====================================================================
-- PROCEDURE: [Certification.DeleteRemovedObjects]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Certification.DeleteRemovedObjects]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) 
	DROP PROCEDURE [Certification.DeleteRemovedObjects]
GO

/*

Deletes ulderlying module, requirement set, and requirement objects that were removed in the editor interface.
This works by giving a list of objects that should be kept, and removing the ones not in the list. 

*/

CREATE PROCEDURE [Certification.DeleteRemovedObjects]
(
	@Return_Code					INT					OUTPUT,
	@Error_Description_Code			NVARCHAR(50)		OUTPUT,
	@idCallerSite					INT					= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT					= 0,
	
	@idCertification				INT,
	@Modules						IDTable	READONLY,
	@ModuleRequirementSets			IDTable	READONLY,
	@ModuleRequirements				IDTable	READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END	

	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertification
		WHERE idCertification = @idCertification
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationDeleteRemovedObjects_NoRecordFound'
		RETURN 1
	    END

	/*

	get the ids of objects to delete

	*/

	-- declare temp tables
	DECLARE @RequirementsToDelete IDTable
	DECLARE @RequirementSetsToDelete IDTable
	DECLARE @ModulesToDelete IDTable

	-- get module requirements
	INSERT INTO @RequirementsToDelete (
		id
	)
	SELECT
		CMR.idCertificationModuleRequirement
	FROM tblCertificationModuleRequirement CMR
	LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModuleRequirementSet = CMR.idCertificationModuleRequirementSet
	LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = CMRS.idCertificationModule
	WHERE CM.idCertification = @idCertification
	AND NOT EXISTS (SELECT 1 FROM @ModuleRequirements MR
					WHERE MR.id = CMR.idCertificationModuleRequirement)

	-- get module requirement sets
	INSERT INTO @RequirementSetsToDelete (
		id
	)
	SELECT
		CMRS.idCertificationModuleRequirementSet
	FROM tblCertificationModuleRequirementSet CMRS
	LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = CMRS.idCertificationModule
	WHERE CM.idCertification = @idCertification
	AND NOT EXISTS (SELECT 1 FROM @ModuleRequirementSets MRS
					WHERE MRS.id = CMRS.idCertificationModuleRequirementSet)

	-- get modules
	INSERT INTO @ModulesToDelete (
		id
	)
	SELECT
		CM.idCertificationModule
	FROM tblCertificationModule CM
	WHERE CM.idCertification = @idCertification
	AND NOT EXISTS (SELECT 1 FROM @Modules M
					WHERE M.id = CM.idCertificationModule)

	/*

	delete the objects

	*/

	EXEC [CertificationModuleRequirement.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @RequirementsToDelete
	EXEC [CertificationModuleRequirementSet.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @RequirementSetsToDelete
	EXEC [CertificationModule.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @ModulesToDelete

	/*

	RETURN

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO