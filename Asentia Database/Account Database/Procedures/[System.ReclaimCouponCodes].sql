-- =====================================================================
-- PROCEDURE: [System.ReclaimCouponCodes]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ReclaimCouponCodes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ReclaimCouponCodes]
GO

/*

Reclaims coupon codes for purchases that have not been completed within coupon code grace periods.
This is so coupon codes aren't left hanging in a pending state if someone does not go through with purchase.

*/

CREATE PROCEDURE [System.ReclaimCouponCodes]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON

	/*

	first, reclaim all coupon codes for failed purchases
	RELEASE 1.16 - NOV 2019, DO NOT RECLAIM COUPON CODES FOR FAILED PURCHASES

	*/

	UPDATE tblTransactionItem SET
		idCouponCode = NULL,
		couponCode = NULL,
		paid = cost
	FROM tblTransactionItem TI
	LEFT JOIN tblPurchase P ON P.idPurchase = TI.idPurchase
	WHERE TI.idPurchase IS NOT NULL
	AND (TI.confirmed IS NULL OR TI.confirmed = 0)
	AND P.[timestamp] IS NULL
	AND P.dtPending IS NULL
	AND P.failed = 1

	/*

	get all unconfirmed, non pending, non failed purchases that have items using a coupon code

	*/

	CREATE TABLE #Purchases (
		idPurchase	INT,
		idSite		INT
	)

	INSERT INTO #Purchases (
		idPurchase,
		idSite
	)
	SELECT DISTINCT
		P.idPurchase,
		P.idSite
	FROM tblPurchase P
	LEFT JOIN tblTransactionItem TI ON TI.idPurchase = P.idPurchase
	WHERE P.[timestamp] IS NULL
	AND P.dtPending IS NULL
	AND (P.failed = 0 OR P.failed IS NULL)
	AND TI.idCouponCode IS NOT NULL
	AND TI.idCouponCode > 0

	/*

	cursor through the purchases, reclaim coupon codes where:
		- grace period has lapsed
		- coupon code has expired
		- no grace period set or grace period 0, and user no longer logged in

	*/

	DECLARE @cursor CURSOR
	DECLARE @idSite INT
	DECLARE @idPurchase INT
	DECLARE @gracePeriod INT

	SET @cursor = CURSOR FOR
		SELECT idPurchase, idSite FROM #Purchases

	OPEN @cursor
		FETCH NEXT FROM @cursor INTO @idPurchase, @idSite

	WHILE (@@fetch_status = 0)

		BEGIN

		-- get the grace period
		SELECT
			@gracePeriod = CASE WHEN SP.value IS NULL THEN NULL ELSE SP.value END
		FROM tblSiteParam SP
		WHERE SP.[param] = 'Ecommerce.CouponCode.GracePeriod'
		AND SP.idSite = @idSite

		-- no grace period, reclaim on user logout
		IF (@gracePeriod IS NULL OR @gracePeriod = '0')

			BEGIN

			UPDATE tblTransactionItem SET
				idCouponCode = NULL,
				couponCode = NULL,
				paid = cost
			FROM tblTransactionItem TI
			LEFT JOIN tblPurchase P ON P.idPurchase = TI.idPurchase			
			WHERE TI.idPurchase = @idPurchase
			AND (TI.confirmed IS NULL OR TI.confirmed = 0)
			AND P.[timestamp] IS NULL
			AND P.dtPending IS NULL
			AND (P.failed IS NULL OR P.failed = 0)
			AND NOT EXISTS (SELECT 1 FROM tblUser U
							WHERE U.dtSessionExpires > GETUTCDATE()
							AND U.idUser = P.idUser)

			END

		ELSE -- grace period, reclaim after grace period lapse

			BEGIN

			UPDATE tblTransactionItem SET
				idCouponCode = NULL,
				couponCode = NULL,
				paid = cost
			FROM tblTransactionItem TI
			LEFT JOIN tblPurchase P ON P.idPurchase = TI.idPurchase			
			WHERE TI.idPurchase = @idPurchase
			AND (TI.confirmed IS NULL OR TI.confirmed = 0)
			AND P.[timestamp] IS NULL
			AND P.dtPending IS NULL
			AND (P.failed IS NULL OR P.failed = 0)
			AND DATEADD(dd, @gracePeriod, TI.dtAdded) <= DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 0)

			END
			
		-- reclaim expired coupon codes
		UPDATE tblTransactionItem SET
			idCouponCode = NULL,
			couponCode = NULL,
			paid = cost
		FROM tblTransactionItem TI
		LEFT JOIN tblPurchase P ON P.idPurchase = TI.idPurchase
		LEFT JOIN tblCouponCode CC ON CC.idCouponCode = TI.idCouponCode
		WHERE TI.idPurchase = @idPurchase
		AND (TI.confirmed IS NULL OR TI.confirmed = 0)
		AND P.[timestamp] IS NULL
		AND P.dtPending IS NULL
		AND (P.failed IS NULL OR P.failed = 0)
		AND (
				(
					CC.dtStart IS NOT NULL
					AND CC.dtStart > DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 0)
				)
				OR
				(
					CC.dtEnd IS NOT NULL
					AND CC.dtEnd <= DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 0)
				)
			)
		
		-- fetch next
		FETCH NEXT FROM @cursor INTO @idPurchase, @idSite

		END	

	/*

	close and deallocate cursor

	*/
	
	CLOSE @cursor
	DEALLOCATE @cursor

	/*

	return

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO