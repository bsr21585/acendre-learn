-- =====================================================================
-- PROCEDURE: [LearningPath.CourseStatistics]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[LearningPath.CourseStatistics]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.CourseStatistics]
GO

/*

Gets course statistics for a learning path.

*/

CREATE PROCEDURE [LearningPath.CourseStatistics]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idLearningPath			INT,
	@totalCourses			INT				OUTPUT,
	@easiestCourse			NVARCHAR(255)	OUTPUT,
	@hardestCourse			NVARCHAR(255)	OUTPUT,	
	@averageTimePerCourse	NVARCHAR(8)		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	get the total number of courses

	*/

	SELECT @totalCourses = COUNT(1) FROM tblLearningPathToCourseLink LPCL WHERE LPCL.idLearningPath = @idLearningPath

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	aggragate all course lesson data into a temporary table, we'll work from that table

	*/

	CREATE TABLE #CourseLessonData (
		idCourse			INT,
		averageScoreScaled	FLOAT,
		averageTotalTime	FLOAT
	)

	INSERT INTO #CourseLessonData (
		idCourse,
		averageScoreScaled,
		averageTotalTime
	)
	SELECT
		L.idCourse,
		AVG(DSCO.scoreScaled),
		AVG(DSCO.totalTime)
	FROM [tblData-Lesson] LD
	LEFT JOIN [tblData-SCO] DSCO ON DSCO.[idData-Lesson] = LD.[idData-Lesson]
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson	
	WHERE L.idCourse IN (SELECT idCourse FROM tblLearningPathToCourseLink WHERE idLearningPath = @idLearningPath)
	AND (L.isDeleted = 0 OR L.isDeleted IS NULL)
	AND DSCO.totalTime IS NOT NULL
	GROUP BY L.idCourse
	
	/*

	get the easiest course

	*/
	
	SELECT TOP 1 
		@easiestCourse = CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
	FROM #CourseLessonData CLD
	LEFT JOIN tblCourse C ON C.idCourse = CLD.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	ORDER BY averageScoreScaled DESC, averageTotalTime ASC
		
	/*

	get the hardest course

	*/
	
	SELECT TOP 1 
		@hardestCourse = CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
	FROM #CourseLessonData CLD
	LEFT JOIN tblCourse C ON C.idCourse = CLD.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	ORDER BY averageScoreScaled ASC, averageTotalTime DESC

	/*

	get the average total time over all lessons

	*/

	SELECT @averageTimePerCourse = CONVERT(NVARCHAR(8), DATEADD(SECOND, AVG(averageTotalTime), 0), 108) FROM #CourseLessonData	
		
	-- DROP THE TEMPORARY TABLES
	DROP TABLE #CourseLessonData		

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO