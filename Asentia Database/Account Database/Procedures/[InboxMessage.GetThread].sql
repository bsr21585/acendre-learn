-- =====================================================================
-- PROCEDURE: [InboxMessage.GetThread]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[InboxMessage.GetThread]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [InboxMessage.GetThread]
GO

/*

Gets a thread of messages from the base message.

*/
CREATE PROCEDURE [InboxMessage.GetThread]
(
	@Return_Code				INT						OUTPUT,
	@Error_Description_Code		NVARCHAR(50)			OUTPUT,
	@idCallerSite				INT				= 0, 
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0, 
	
	@idMessage					INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/* check if idInboxMessage has idParentInboxMessage */
	DECLARE @idInboxMessage  INT
	
	SELECT @idInboxMessage=t.idParentInboxMessage 
	FROM tblInboxMessage t
	WHERE t.idInboxMessage = @idMessage 	

	IF 		@idInboxMessage IS NULL
	BEGIN
		SET @idInboxMessage = @idMessage	
		END	

	/*

	validate that the message exists

	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblInboxMessage
		WHERE idInboxMessage = @idInboxMessage	
		) = 0 

		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'InboxMessageGetThread_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	validate caller permissions to messages in thread
	
	*/
		
	IF (
		SELECT COUNT(1)
		FROM tblInboxMessage IM
		WHERE (
				IM.idInboxMessage = @idInboxMessage  
				OR
				IM.idParentInboxMessage = @idInboxMessage
			  )
			  AND (
					(
						IM.idRecipient = @idCaller
						AND
						IM.isRecipientDeleted IS NULL
					)
					OR
					(
						IM.idSender = @idCaller
						AND
						IM.isSenderDeleted IS NULL
					)
				  )
			  AND IM.isSent = 1
			  AND IM.idSite = @idCallerSite) = 0 
			
		BEGIN
		SELECT @Return_Code = 3
		SET @Error_Description_Code = 'InboxMessageGetThread_PermissionError'
		RETURN 1
		END
	
	
	/*
	
	if the caller is the recipient, and the message is sent, mark the message read
	
	*/
	
	UPDATE tblInboxMessage SET
	      isRead = 1,
	      dtRead = GETUTCDATE()
	WHERE (idInboxMessage = @idInboxMessage OR idParentInboxMessage =  idInboxMessage OR idParentInboxMessage = @idInboxMessage) 
	AND isSent = 1
	AND (isRead = 0 OR isRead IS NULL)
	AND idRecipient = @idCaller
	AND idSite = @idCallerSite
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		IM.idInboxMessage,
		IM.idParentInboxMessage,
		CASE WHEN IM.idSender = 1 THEN 'Administrator' ELSE S.displayName END AS [sender],
		CASE WHEN IM.idRecipient = 1 THEN 'Administrator' ELSE R.displayName END AS [recipient], 
		IM.[message],
		IM.[dtSent],
		IM.[subject]	 
	FROM tblInboxMessage IM
	LEFT JOIN tblUser S ON S.idUser = IM.idSender
	LEFT JOIN tblUser R ON R.idUser = IM.idRecipient
	WHERE (
			IM.idInboxMessage = @idInboxMessage
			OR 
			IM.idParentInboxMessage = IM.idInboxMessage 
			OR 
			IM.idParentInboxMessage = @idInboxMessage
		  )
		  AND (
				(
					IM.idRecipient = @idCaller
					AND
					IM.isRecipientDeleted IS NULL
				)
				OR
				(
					IM.idSender = @idCaller
					AND
					IM.isSenderDeleted IS NULL
				)
			  )
		   AND (IM.isSent = 1)
		  AND (IM.isDraft IS NULL OR isDraft = 0)
		  AND IM.idSite = @idCallerSite
	ORDER BY [dtSent] desc
	
	IF @@ROWCOUNT = 0
		SELECT @Return_Code = 1,@Error_Description_Code = 'InboxMessageGetThread_NoRecordFound'
	ELSE
		SELECT @Return_Code = 0
		
	END