-- =====================================================================
-- PROCEDURE: [RulesEngine.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RulesEngine.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RulesEngine.GetGrid]
GO

/*

Gets a listing of rule sets.

*/

CREATE PROCEDURE [RulesEngine.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END
			
		IF @searchParam IS NULL
		
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblRuleSet RS
			LEFT JOIN tblRuleSetToGroupLink RSG ON RSG.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToCertificationLink RSC ON RSC.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSEL ON RSEL.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSLPE ON RSLPE.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRoleLink RSR ON RSR.idRuleSet = RS.idRuleSet
			LEFT JOIN tblGroup G ON G.idGroup = RSG.idGroup
			LEFT JOIN tblRole R ON R.idRole = RSR.idRole
			LEFT JOIN tblRuleSetLearningPathEnrollment LPE ON LPE.idRuleSetLearningPathEnrollment = RSLPE.idRuleSetLearningPathEnrollment
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
			LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
			LEFT JOIN tblCourse C ON C.idCourse = RSE.idCourse
			WHERE RSG.idGroup IS NOT NULL 
				  OR 
				  RSC.idCertification IS NOT NULL 
				  OR 
				  RSE.idRuleSetEnrollment IS NOT NULL 
				  OR 
				  RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL 
				  OR 
				  RSR.idRole IS NOT NULL 
				  AND
				  (
						@idCallerSite IS NULL 
						OR 
						@idCallerSite IS NOT NULL 
						AND 
						RS.idSite = @idCallerSite
				  )			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						RS.idRuleSet,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN RS.label END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RS.label END
						)
						AS [row_number]
					FROM tblRuleSet RS
					LEFT JOIN tblRuleSetToGroupLink RSG ON RSG.idRuleSet = RS.idRuleSet
					LEFT JOIN tblRuleSetToCertificationLink RSC ON RSC.idRuleSet = RS.idRuleSet
					LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSEL ON RSEL.idRuleSet = RS.idRuleSet
					LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSLPE ON RSLPE.idRuleSet = RS.idRuleSet
					LEFT JOIN tblRuleSetToRoleLink RSR ON RSR.idRuleSet = RS.idRuleSet
					LEFT JOIN tblGroup G ON G.idGroup = RSG.idGroup
					LEFT JOIN tblRole R ON R.idRole = RSR.idRole
					LEFT JOIN tblRuleSetLearningPathEnrollment LPE ON LPE.idRuleSetLearningPathEnrollment = RSLPE.idRuleSetLearningPathEnrollment
					LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
					LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
					LEFT JOIN tblCourse C ON C.idCourse = RSE.idCourse
					WHERE 
						(
							RSG.idGroup IS NOT NULL 
							OR 
							RSC.idCertification IS NOT NULL 
							OR 
							RSE.idRuleSetEnrollment IS NOT NULL 
							OR 
							RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL 
							OR 
							RSR.idRole IS NOT NULL
						)
					AND
						(
							@idCallerSite IS NULL 
							OR 
							@idCallerSite IS NOT NULL 
							AND 
							RS.idSite = @idCallerSite
						)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idRuleSet, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				RS.idRuleSet,
				RS.label, 
				STUFF((SELECT DISTINCT char(10) + R.userField + ', '
							FROM tblRule R WHERE RS.idRuleSet = R.idRuleSet FOR XML PATH('')), 1, 1, '') 
				AS [fields], 
				CASE WHEN RSG.idGroup IS NOT NULL THEN 'group'
						WHEN RSC.idCertification IS NOT NULL THEN 'certification'
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN 'course'
						WHEN RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN 'learningpath'
						WHEN RSR.idRole IS NOT NULL THEN 'role' END 
				AS [object], 
				CASE WHEN RSG.idGroup IS NOT NULL THEN RSG.idGroup
						WHEN RSC.idCertification IS NOT NULL THEN RSC.idCertification
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN RSE.idCourse
						WHEN RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN LPE.idLearningPath
						WHEN RSR.idRole IS NOT NULL THEN RSR.idRole END 
				AS [idObject], 
				CASE WHEN LPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN LPE.idRuleSetLearningPathEnrollment
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN RSE.idRuleSetEnrollment END 
				AS [idRulesetEnrollment], 
				CASE WHEN RSG.idGroup IS NOT NULL THEN G.avatar
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN C.avatar
						WHEN RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN LP.avatar END 
				AS [avatar], 
				CASE WHEN RSG.idGroup IS NOT NULL THEN G.name
						WHEN RSC.idCertification IS NOT NULL THEN CER.title
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN C.title
						WHEN RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN LP.name
						WHEN RSR.idRole IS NOT NULL THEN R.name END 
				AS [objectName]
			FROM selectedKeys
			JOIN tblRuleSet RS ON RS.idRuleSet = selectedKeys.idRuleSet
			LEFT JOIN tblRuleSetToGroupLink RSG ON RSG.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToCertificationLink RSC ON RSC.idRuleSet = RS.idRuleSet
			LEFT JOIN tblCertification CER ON CER.idCertification = RSC.idCertification
			LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSEL ON RSEL.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSLPE ON RSLPE.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRoleLink RSR ON RSR.idRuleSet = RS.idRuleSet
			LEFT JOIN tblGroup G ON G.idGroup = RSG.idGroup
			LEFT JOIN tblRole R ON R.idRole = RSR.idRole
			LEFT JOIN tblRuleSetLearningPathEnrollment LPE ON LPE.idRuleSetLearningPathEnrollment = RSLPE.idRuleSetLearningPathEnrollment
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
			LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
			LEFT JOIN tblCourse C ON C.idCourse = RSE.idCourse
			WHERE 
				(
					RSG.idGroup IS NOT NULL 
					OR 
					RSC.idCertification IS NOT NULL 
					OR 
					RSE.idRuleSetEnrollment IS NOT NULL 
					OR 
					RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL 
					OR 
					RSR.idRole IS NOT NULL
				)
			AND
				(
					@idCallerSite IS NULL 
					OR 
					@idCallerSite IS NOT NULL 
					AND 
					RS.idSite = @idCallerSite
				)
			ORDER BY SelectedKeys.[row_number]
			
			END
		
		ELSE
		
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblRuleSet RS
			LEFT JOIN tblRuleSetToGroupLink RSG ON RSG.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToCertificationLink RSC ON RSC.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSEL ON RSEL.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSLPE ON RSLPE.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRoleLink RSR ON RSR.idRuleSet = RS.idRuleSet
			LEFT JOIN tblGroup G ON G.idGroup = RSG.idGroup
			LEFT JOIN tblRole R ON R.idRole = RSR.idRole
			LEFT JOIN tblRuleSetLearningPathEnrollment LPE ON LPE.idRuleSetLearningPathEnrollment = RSLPE.idRuleSetLearningPathEnrollment
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
			LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
			LEFT JOIN tblCourse C ON C.idCourse = RSE.idCourse
			INNER JOIN CONTAINSTABLE(tblRuleSet, *, @searchParam) K ON K.[key] = RS.idRuleSet
			WHERE RSG.idGroup IS NOT NULL 
				  OR 
				  RSC.idCertification IS NOT NULL 
				  OR 
				  RSE.idRuleSetEnrollment IS NOT NULL 
				  OR 
				  RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL 
				  OR 
				  RSR.idRole IS NOT NULL 
				  AND
					(
						@idCallerSite IS NULL 
						OR 
						@idCallerSite IS NOT NULL 
						AND 
						RS.idSite = @idCallerSite
					)
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						RS.idRuleSet,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN RS.label END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RS.label END
						)
						AS [row_number]
					FROM tblRuleSet RS
					LEFT JOIN tblRuleSetToGroupLink RSG ON RSG.idRuleSet = RS.idRuleSet
					LEFT JOIN tblRuleSetToCertificationLink RSC ON RSC.idRuleSet = RS.idRuleSet
					LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSEL ON RSEL.idRuleSet = RS.idRuleSet
					LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSLPE ON RSLPE.idRuleSet = RS.idRuleSet
					LEFT JOIN tblRuleSetToRoleLink RSR ON RSR.idRuleSet = RS.idRuleSet
					LEFT JOIN tblGroup G ON G.idGroup = RSG.idGroup
					LEFT JOIN tblRole R ON R.idRole = RSR.idRole
					LEFT JOIN tblRuleSetLearningPathEnrollment LPE ON LPE.idRuleSetLearningPathEnrollment = RSLPE.idRuleSetLearningPathEnrollment
					LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
					LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
					LEFT JOIN tblCourse C ON C.idCourse = RSE.idCourse
					INNER JOIN CONTAINSTABLE(tblRuleSet, *, @searchParam) K ON K.[key] = RS.idRuleSet
					WHERE RSG.idGroup IS NOT NULL 
						  OR 
						  RSC.idCertification IS NOT NULL 
						  OR 
						  RSE.idRuleSetEnrollment IS NOT NULL 
						  OR 
						  RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL 
						  OR 
						  RSR.idRole IS NOT NULL 
						  AND
							(
								@idCallerSite IS NULL 
								OR 
								@idCallerSite IS NOT NULL 
								AND 
								RS.idSite = @idCallerSite
							)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idRuleSet, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT RS.idRuleSet, 
				   RS.label, 
				   STUFF((SELECT DISTINCT char(10) + R.userField + ', '
								FROM tblRule R WHERE RS.idRuleSet = R.idRuleSet FOR XML PATH('')), 1, 1, '') 
				   AS [fields], 
				   CASE WHEN RSG.idGroup IS NOT NULL THEN 'group'
						WHEN RSC.idCertification IS NOT NULL THEN 'certification'
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN 'course'
						WHEN RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN 'learningpath'
						WHEN RSR.idRole IS NOT NULL THEN 'role' END 
				   AS [object], 
				   CASE WHEN RSG.idGroup IS NOT NULL THEN RSG.idGroup
						WHEN RSC.idCertification IS NOT NULL THEN RSC.idCertification
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN RSE.idCourse
						WHEN RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN LPE.idLearningPath
						WHEN RSR.idRole IS NOT NULL THEN RSR.idRole END 
				   AS [idObject], 
				   CASE WHEN LPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN LPE.idRuleSetLearningPathEnrollment
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN RSE.idRuleSetEnrollment END 
				   AS [idRulesetEnrollment], 
				   CASE WHEN RSG.idGroup IS NOT NULL THEN G.avatar
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN C.avatar
						WHEN RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN LP.avatar END 
				   AS [avatar], 
				   CASE WHEN RSG.idGroup IS NOT NULL THEN G.name
						WHEN RSC.idCertification IS NOT NULL THEN CER.title
						WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN C.title
						WHEN RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN LP.name
						WHEN RSR.idRole IS NOT NULL THEN R.name END 
				   AS [objectName]
			FROM selectedKeys
			JOIN tblRuleSet RS ON RS.idRuleSet = selectedKeys.idRuleSet
			LEFT JOIN tblRuleSetToGroupLink RSG ON RSG.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToCertificationLink RSC ON RSC.idRuleSet = RS.idRuleSet
			LEFT JOIN tblCertification CER ON CER.idCertification = RSC.idCertification
			LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSEL ON RSEL.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSLPE ON RSLPE.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRoleLink RSR ON RSR.idRuleSet = RS.idRuleSet
			LEFT JOIN tblGroup G ON G.idGroup = RSG.idGroup
			LEFT JOIN tblRole R ON R.idRole = RSR.idRole
			LEFT JOIN tblRuleSetLearningPathEnrollment LPE ON LPE.idRuleSetLearningPathEnrollment = RSLPE.idRuleSetLearningPathEnrollment
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
			LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
			LEFT JOIN tblCourse C ON C.idCourse = RSE.idCourse
			ORDER BY SelectedKeys.[row_number]
			
			END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO