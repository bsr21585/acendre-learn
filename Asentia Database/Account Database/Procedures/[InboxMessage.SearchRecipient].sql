-- =====================================================================
-- PROCEDURE: [InboxMessage.SearchRecipient]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[InboxMessage.SearchRecipient]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [InboxMessage.SearchRecipient]
GO

CREATE PROCEDURE [InboxMessage.SearchRecipient]
(
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT						= 0, 
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,

	@searchParam			NVARCHAR(4000)
) 
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	get the site's default language id

	*/

	DECLARE @idSiteLanguage INT
	SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	/*
	
	declare a temporary table for the search results
	
	*/

	CREATE TABLE #RecipientSearchResults (
		idObject INT,
		objectName NVARCHAR(768),
		objectType NVARCHAR(10)
	)

	/* 

	do the search

	*/

	IF (@searchParam IS NOT NULL)

		BEGIN

		/* GROUPS */

		-- if the caller's language is the same as the site's language,
		-- search and display groups on the base table
		IF (@idCallerLanguage = @idSiteLanguage)
	
			BEGIN

			INSERT INTO #RecipientSearchResults (
				idObject,
				objectName,
				objectType
			)
			SELECT 
				G.idGroup,
				G.name,
				'group'
			FROM tblGroup G
			INNER JOIN CONTAINSTABLE(tblGroup, *, @searchParam) K ON K.[key] = G.idGroup			
			WHERE G.idSite = @idCallerSite 
			
			END

		ELSE -- search and display groups on the language table

			BEGIN

			INSERT INTO #RecipientSearchResults (
				idObject,
				objectName,
				objectType
			)
			SELECT 
				G.idGroup,
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END,
				'group'
			FROM tblGroupLanguage GL
			LEFT JOIN tblGroup G ON G.idGroup = GL.idGroup
			INNER JOIN CONTAINSTABLE(tblGroupLanguage, *, @searchParam) K ON K.[key] = GL.idGroupLanguage			
			WHERE G.idSite = @idCallerSite 
			AND GL.idLanguage = @idCallerLanguage
			
			END

		/* USERS */
		-- always search users on the base table, they are not language-specific

		INSERT INTO #RecipientSearchResults (
			idObject,
			objectName,
			objectType
		)
		SELECT 
			U.idUser,
			U.displayName + ' (' + U.username + ')',
			'user'
		FROM tblUser U
		INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = U.idUser
		WHERE U.idSite = @idCallerSite
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- no deleted or pending users
		AND (U.isActive = 1) -- only active users
		AND (U.dtExpires IS NULL OR U.dtExpires > GETUTCDATE()) -- no expired users

		/* ADMIN */
		-- if the search param is admin, then insert a system admin
		IF ( LOWER(SUBSTRING(@searchParam, 2, LEN(@searchParam)-3)) = SUBSTRING('administrator', 1, LEN(@searchParam)-3))  
			BEGIN
			INSERT INTO #RecipientSearchResults (
				idObject,
				objectName,
				objectType
			)
			SELECT 
				1,
				'Administrator (admin)',
				'user'		

			END
		END
	/*

	return the data

	*/

	SELECT
		idObject,
		objectName,
		objectType
	FROM #RecipientSearchResults
	ORDER BY objectName, objectType

	-- destroy the temp table
	DROP TABLE #RecipientSearchResults

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO