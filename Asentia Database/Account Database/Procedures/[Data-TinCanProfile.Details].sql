-- =====================================================================
-- PROCEDURE: [Data-TinCanProfile.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCanProfile.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCanProfile.Details]
GO

/*

Returns tin can profile for the given ids (profileId & activityId/agent).

*/

CREATE PROCEDURE [Data-TinCanProfile.Details]
(
	@Return_Code							INT						OUTPUT,
	@Error_Description_Code					NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idData_TinCanProfile					INT						OUTPUT,
	@idSite									INT						OUTPUT,
	@idEndpoint								INT						OUTPUT,
	@profileId								NVARCHAR(100)			OUTPUT,
	@activityId								NVARCHAR(100)			OUTPUT,
	@agent									NVARCHAR(MAX)			OUTPUT,
	@contentType							NVARCHAR(50)			OUTPUT,
	@docContents							VARBINARY(MAX)			OUTPUT,
	@dtUpdated								DATETIME				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that tin can profile exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-TinCanProfile]
		WHERE idSite = @idCallerSite
		AND profileId = @profileId
		AND (@activityId IS NULL OR activityId = @activityId)
		AND (@agent IS NULL OR agent = @agent)
		) = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataTinCanProfileDetails_ProfileNotFound'
		RETURN 1
		END
	
	/*
	
	validate caller permission to the specified tin can profile
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-TinCanProfile]
		WHERE idSite = @idCallerSite
		AND idEndpoint = @idEndpoint
		AND profileId = @profileId
		AND (@activityId IS NULL OR activityId = @activityId)
		AND (@agent IS NULL OR agent = @agent)
		) = 0
		BEGIN
		SELECT @Return_Code = 3
		SET @Error_Description_Code = 'DataTinCanProfileDetails_PermissionError'
		RETURN 1
		END
	
	/*
	
	get the data 
	
	*/
	
	SELECT	@idData_TinCanProfile = [idData-TinCanProfile],
			@idSite = idSite,
			@idEndpoint = idEndpoint,
			@profileId = profileId,
			@activityId = activityId,
			@contentType = contentType,
			@docContents = docContents,
			@dtUpdated = dtUpdated
	FROM	[tblData-TinCanProfile]
	WHERE	idSite = @idCallerSite
	AND		idEndpoint = @idEndpoint
	AND		profileId = @profileId
	AND		(@activityId IS NULL OR activityId = @activityId)
	AND		(@agent IS NULL OR agent = @agent)
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataTinCanProfileDetails_ProfileNotFound'
		END
	ELSE
		BEGIN
		SELECT @Return_Code = 0 --Success
		SELECT @Error_Description_Code = NULL
		END
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO