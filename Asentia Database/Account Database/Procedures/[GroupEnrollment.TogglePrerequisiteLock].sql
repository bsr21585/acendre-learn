-- =====================================================================
-- PROCEDURE: [GroupEnrollment.TogglePrerequisiteLock]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GroupEnrollment.TogglePrerequisiteLock]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GroupEnrollment.TogglePrerequisiteLock]
GO

/*

Turns the isLockedByPrerequisites flag on/off for a group enrollment.

*/

CREATE PROCEDURE [GroupEnrollment.TogglePrerequisiteLock]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idGroupEnrollment			INT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1) 
		FROM tblGroupEnrollment
		WHERE idGroupEnrollment = @idGroupEnrollment
		AND idSite = @idCallerSite
		) < 1
		
		BEGIN
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'GEnrollmentTogglePrerequisiteLock_NoRecordFound'
		RETURN 1
		END
		
	/* 
	
	toggle the isLockedByPrerequisites flag
	
	*/	
	
	UPDATE tblGroupEnrollment SET
		isLockedByPrerequisites = CASE WHEN GE.isLockedByPrerequisites IS NULL OR GE.isLockedByPrerequisites = 0 THEN 1 ELSE 0 END
	FROM tblGroupEnrollment GE
	WHERE GE.idGroupEnrollment = @idGroupEnrollment
	AND GE.idGroupEnrollment = idGroupEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO