-- =====================================================================
-- PROCEDURE: [Resource.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Resource.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Resource.GetGrid]
GO

/*

Gets a listing of Coupon codes.

*/

Create PROCEDURE [dbo].[Resource.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@idCaller				INT				= 0,
	@callerLangString		NVARCHAR (10),
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/
		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblResource R
			INNER JOIN tblResourceType RT ON R.idResourceType = RT.idResourceType
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
				)
				AND (R.isDeleted = 0 OR R.isDeleted IS NULL) 
				AND R.dtDeleted IS NULL
				AND (RT.isDeleted = 0 OR RT.isDeleted IS NULL)
				AND RT.dtDeleted IS NULL
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						R.idResource,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN R.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'description' THEN R.[description] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'resourceType' THEN RT.ResourceType END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN R.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'description' THEN R.[description] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'resourceType' THEN RT.ResourceType  END) END
						)
						AS [row_number]
					FROM tblResource R
					INNER JOIN tblResourceType RT ON R.idResourceType = RT.idResourceType
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
						)
					AND (R.isDeleted = 0 OR R.isDeleted IS NULL) 
					AND R.dtDeleted IS NULL
					AND (RT.isDeleted = 0 OR RT.isDeleted IS NULL)
					AND RT.dtDeleted IS NULL
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idResource, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				R.idResource,
				CAST(1 AS BIT) as isModifyOn,
				CAST(1 AS BIT) as showCalendar,
				(CASE WHEN RL.name IS NULL OR RL.name = '' THEN R.name ELSE RL.name END	)	    AS name,
				(CASE WHEN RL.[description] IS NULL THEN R.[description] ELSE RL.[description] END) AS [description],
				RT.ResourceType
			FROM SelectedKeys
			JOIN tblResource R ON R.idResource = SelectedKeys.idResource
			INNER JOIN tblResourceType RT ON R.idResourceType = RT.idResourceType
			LEFT JOIN tblResourceLanguage RL 
		    ON RL.idResource = R.idResource 
		    AND RL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE

			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)
			
				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblResource R
				INNER JOIN CONTAINSTABLE(tblResource, *, @searchParam) K ON K.[key] = R.idResource
				INNER JOIN tblResourceType RT ON R.idResourceType = RT.idResourceType
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
					)
					AND (R.isDeleted = 0 OR R.isDeleted IS NULL) 
					AND R.dtDeleted IS NULL
					AND (RT.isDeleted = 0 OR RT.isDeleted IS NULL)
					AND RT.dtDeleted IS NULL
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							R.idResource,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN R.name END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'description' THEN R.[description] END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'resourceType' THEN RT.ResourceType END) END DESC,
								
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN R.name END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'description' THEN R.[description] END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'resourceType' THEN RT.ResourceType  END) END
							)
							AS [row_number]
						FROM tblResource R
						INNER JOIN CONTAINSTABLE(tblResource, *, @searchParam) K ON K.[key] = R.idResource
						INNER JOIN tblResourceType RT ON R.idResourceType = RT.idResourceType
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite
							)
							AND (R.isDeleted = 0 OR R.isDeleted IS NULL) 
							AND R.dtDeleted IS NULL
							AND (RT.isDeleted = 0 OR RT.isDeleted IS NULL)
							AND RT.dtDeleted IS NULL
						), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idResource, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
					
				SELECT 
					R.idResource,
					CAST(1 AS BIT) as isModifyOn,
					CAST(1 AS BIT) as showCalendar,
					R.name		 AS name,
					R.[description] AS [description],
					RT.ResourceType
				FROM SelectedKeys
				JOIN tblResource R ON R.idResource = SelectedKeys.idResource
				INNER JOIN tblResourceType RT ON R.idResourceType = RT.idResourceType
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN
				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblResourceLanguage RL
				LEFT JOIN tblResource R ON R.idResource = RL.idResource
				INNER JOIN CONTAINSTABLE(tblResourceLanguage, *, @searchParam) K ON K.[key] = RL.idResourceLanguage
				INNER JOIN tblResourceType RT ON R.idResourceType = RT.idResourceType
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
					)
					AND RL.idLanguage = @idCallerLanguage
					AND (R.isDeleted = 0 OR R.isDeleted IS NULL) 
					AND R.dtDeleted IS NULL
					AND (RT.isDeleted = 0 OR RT.isDeleted IS NULL)
					AND RT.dtDeleted IS NULL

				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							RL.idResource,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN RL.name END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'description' THEN RL.[description] END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'resourceType' THEN RT.ResourceType END) END DESC,
								
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN RL.name END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'description' THEN RL.[description] END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'resourceType' THEN RT.ResourceType  END) END
							)
							AS [row_number]
						FROM tblResourceLanguage RL
						LEFT JOIN tblResource R ON R.idResource = RL.idResource
						INNER JOIN CONTAINSTABLE(tblResourceLanguage, *, @searchParam) K ON K.[key] = RL.idResourceLanguage
						INNER JOIN tblResourceType RT ON R.idResourceType = RT.idResourceType
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite
							)
							AND RL.idLanguage = @idCallerLanguage
							AND (R.isDeleted = 0 OR R.isDeleted IS NULL) 
							AND R.dtDeleted IS NULL
							AND (RT.isDeleted = 0 OR RT.isDeleted IS NULL)
							AND RT.dtDeleted IS NULL
						), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idResource, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
					
				SELECT 
					R.idResource,
					CAST(1 AS BIT) as isModifyOn,
					CAST(1 AS BIT) as showCalendar,
					RL.name AS name,
					RL.[description] AS [description],
					RT.ResourceType
				FROM SelectedKeys
				JOIN tblResourceLanguage RL ON RL.idResource = SelectedKeys.idResource AND RL.idLanguage = @idCallerLanguage
				LEFT JOIN tblResource R ON RL.idResource = R.idResource 
				INNER JOIN tblResourceType RT ON R.idResourceType = RT.idResourceType

				ORDER BY SelectedKeys.[row_number]

				END

			END

	END			
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO