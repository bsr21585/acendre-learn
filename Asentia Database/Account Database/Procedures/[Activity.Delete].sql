-- =====================================================================
-- PROCEDURE: [Activity.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Activity.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Activity.Delete]
GO

/*

Deletes all the properties for a given activity ids.

*/

CREATE PROCEDURE [Activity.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@Activities				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Activities) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		RETURN 1
		END
			
	/*
	
	validate that all activities exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Activities AA
		LEFT JOIN tblActivityImport A ON A.idActivityImport = AA.id
		WHERE A.idSite IS NULL
		OR A.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		RETURN 1 
		END
	
	/*

	DELETE the enrollments attached to this import, and all dependent referenced table records

	*/

	DECLARE @EnrollmentsToDelete IDTable

	INSERT INTO @EnrollmentsToDelete (
		id
	)
	SELECT
		E.idEnrollment
	FROM tblEnrollment E
	WHERE E.idActivityImport IN (SELECT id FROM @Activities)

	-- delete SCO lesson data
	DELETE FROM [tblData-SCO] 
	WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
						      FROM [tblData-Lesson] 
							  WHERE idEnrollment IN (SELECT id FROM @EnrollmentsToDelete)
							 )

	-- delete lesson data
	DELETE FROM [tblData-Lesson] 
	WHERE idEnrollment IN (SELECT id FROM @EnrollmentsToDelete)

	-- delete enrollments
	DELETE FROM tblEnrollment  
	WHERE idEnrollment IN (SELECT id FROM @EnrollmentsToDelete)

	/*
	
	DELETE the data from tblActivityImport
	
	*/
	
	DELETE FROM tblActivityImport
	WHERE idActivityImport IN (SELECT id FROM @Activities)
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO