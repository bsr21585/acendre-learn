-- =====================================================================
-- PROCEDURE: [EventEmailQueue.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventEmailQueue.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventEmailQueue.Delete]
GO

/*

Deletes email queue item(s).

*/

CREATE PROCEDURE [EventEmailQueue.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@EmailQueueItems		IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @EmailQueueItems) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		RETURN 1
		END
			
	/*
	
	validate that all email queue item(s) exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @EmailQueueItems EE
		LEFT JOIN tblEventEmailQueue E ON EE.id = E.idEventEmailQueue
		WHERE E.idSite IS NULL
		OR E.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		RETURN 1 
		END
	
		
	/*
	
	Delete the email queue item(s).

	*/
	
	DELETE FROM tblEventEmailQueue
	WHERE idEventEmailQueue IN (
		SELECT id
		FROM @EmailQueueItems
	)
	
	/*
	
	DO DELETE the all the other user-linked/based information
	
	*/
	
	--XXX - fill this in as it becomes evident
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO