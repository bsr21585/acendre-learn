-- =====================================================================
-- PROCEDURE: [Enrollment.ProcessContentChangeLessonDataFlags]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.ProcessContentChangeLessonDataFlags]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.ProcessContentChangeLessonDataFlags]
GO


/*

Processes the lesson data content change flags for an enrollment.

This procedure will:

1) Reset lesson data for lesson data records that have the resetForContentChange flag set and 
the lesson data and enrollment are not completed. Completion check is requred due to the fact 
that the learner may have been interacting with the original lesson content when the flag was 
set and subsequently completed it.

2) Unsets the resetForContentChange flag and sets the preventPostCompletionLaunchForContentChange
flag for lesson data that has the resetForContentChange flag set and has been completed or has had
the enrollment completed. This covers the scenario described above where the learner was interacting 
with the original content when the resetForContentChange was set.

3) The procedure will return a recordset of lesson data ids that it has reset. This is so that
we can delete any lesson data files on the file system.

Note: Checks on enrollment completion where lesson data may not be completed are necessary because
an administrator has the ability to mark an enrollment as completed which does not mark the lesson
data as completed, and we do not want to reset lesson data for those cases because the enrollment's
completion would be unset as a result. 

*/

CREATE PROCEDURE [Enrollment.ProcessContentChangeLessonDataFlags]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idEnrollment			INT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblEnrollment]
		WHERE [idEnrollment] = @idEnrollment
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentProcessContentChangeLDF_NoRecordFound'
		RETURN 1
		END

	/* declare and get information for enrollment */

	DECLARE @dtEnrollmentCompleted	DATETIME
	DECLARE @idCourse				INT

	SELECT
		@dtEnrollmentCompleted = E.dtCompleted,
		@idCourse = E.idCourse
	FROM tblEnrollment E
	WHERE E.idEnrollment = @idEnrollment

	/* make a temporary table to store the lesson data ids we reset */

	CREATE TABLE #ResetLessonDataIds (
		[idData-Lesson] INT
	)

	/* put the enrollment's lesson data into a temporary table */

	CREATE TABLE #LessonData (
		[idData-Lesson]								INT,
		idSite										INT,
		idEnrollment								INT,
		idLesson									INT,
		dtCompleted									DATETIME,
		resetForContentChange						BIT,
		preventPostCompletionLaunchForContentChange	BIT
	)

	INSERT INTO #LessonData (
		[idData-Lesson],
		idSite,
		idEnrollment,
		idLesson,
		dtCompleted,
		resetForContentChange,
		preventPostCompletionLaunchForContentChange
	)
	SELECT
		LD.[idData-Lesson],
		LD.idSite,
		LD.idEnrollment,
		LD.idLesson,
		LD.dtCompleted,
		LD.resetForContentChange,
		LD.preventPostCompletionLaunchForContentChange
	FROM [tblData-Lesson] LD
	WHERE idEnrollment = @idEnrollment

	/*

	Reset lesson data for lesson data records that have the resetForContentChange flag set and 
	the lesson data and enrollment are not completed.

	*/

	DECLARE @idLessonDataToReset INT
	DECLARE lessonDataResetCursor CURSOR LOCAL FOR
		SELECT LD.[idData-Lesson] FROM #LessonData LD WHERE LD.resetForContentChange = 1 AND LD.dtCompleted IS NULL AND @dtEnrollmentCompleted IS NULL

	OPEN lessonDataResetCursor

	FETCH NEXT FROM lessonDataResetCursor INTO @idLessonDataToReset

	WHILE @@FETCH_STATUS = 0
		BEGIN

		EXEC [DataLesson.Reset] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollment, @idLessonDataToReset

		INSERT INTO #ResetLessonDataIds ([idData-Lesson]) VALUES (@idLessonDataToReset)

		FETCH NEXT FROM lessonDataResetCursor INTO @idLessonDataToReset

		END

	-- kill the cursor
	CLOSE lessonDataResetCursor
	DEALLOCATE lessonDataResetCursor

	/*

	Unset the resetForContentChange flag and sets the preventPostCompletionLaunchForContentChange
	flag for lesson data that has the resetForContentChange flag set and has been completed or has had
	the enrollment completed.

	*/

	UPDATE [tblData-Lesson] SET
		resetForContentChange = 0,
		preventPostCompletionLaunchForContentChange = 1
	FROM #LessonData LD
	WHERE LD.[idData-Lesson] = [tblData-Lesson].[idData-Lesson]
	AND LD.resetForContentChange = 1
	AND (
			(LD.dtCompleted IS NOT NULL)
			OR
			(LD.dtCompleted IS NULL AND @dtEnrollmentCompleted IS NOT NULL)
		)

	/*

	Return a recordset of lesson data ids that have been reset.

	*/

	SELECT 
		[idData-Lesson] 
	FROM #ResetLessonDataIds

	/* drop the temporary tables */

	DROP TABLE #LessonData
	DROP TABLE #ResetLessonDataIds

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO