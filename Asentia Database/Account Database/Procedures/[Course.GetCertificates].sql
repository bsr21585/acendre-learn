-- =====================================================================
-- PROCEDURE: [Course.GetCertificates]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.GetCertificates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.GetCertificates]
GO

/*

Returns a recordset of certificate ids and names that belong to a course.

*/

CREATE PROCEDURE [Course.GetCertificates]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCourse				INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*

	do not check caller site membership or caller permissions as
	this can be called by a non-logged-in user

	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		SELECT
			DISTINCT
			C.idCertificate,
			CASE WHEN CL.name IS NOT NULL THEN CL.name ELSE C.name END AS name
		FROM tblCertificate C
		LEFT JOIN tblCertificateLanguage CL ON CL.idCertificate = C.idCertificate AND CL.idLanguage = @idCallerLanguage
		WHERE C.idSite = @idCallerSite
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)
		AND C.idObject = @idCourse
		AND C.objectType = 1
		ORDER BY name

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			C.idCertificate,
			CASE WHEN CL.name IS NOT NULL THEN CL.name ELSE C.name END AS name
		FROM tblCertificateLanguage CL
		INNER JOIN CONTAINSTABLE(tblCertificateLanguage, *, @searchParam) K ON K.[key] = CL.idCertificateLanguage AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCertificate C ON C.idCertificate = CL.idCertificate
		WHERE C.idSite = @idCallerSite
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)
		AND C.idObject = @idCourse
		AND C.objectType = 1
		ORDER BY name

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	