-- =====================================================================
-- PROCEDURE: [GridAnalytic.Users]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[GridAnalytic.Users]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GridAnalytic.Users]
GO

/*

Gets grid analytic data for Users grid page.

*/

CREATE PROCEDURE [GridAnalytic.Users]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
AS
	BEGIN
		SET NOCOUNT ON

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		create a temp table for base user data, and populate it
		we will run the analytics off of that table so as not to tie up the real table

		*/

		CREATE TABLE #UserBase (
			idUser INT,
			idSite INT,
			isActive BIT,
			isOnline BIT,
			dtCreated DATETIME
		)

		INSERT INTO #UserBase (
			idUser,
			idSite,
			isActive,
			isOnline,
			dtCreated
		)
		SELECT
			U.idUser,
			U.idSite,
			CASE WHEN ((U.isActive IS NULL OR U.isActive = 1) AND (U.dtExpires IS NULL OR U.dtExpires > GETUTCDATE())) THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			CASE WHEN U.dtSessionExpires > GETUTCDATE() THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			U.dtCreated
		FROM tblUser U
		WHERE U.idSite = @idCallerSite
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL) -- non-deleted users
		AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1) -- approved users only

		/*

		run the analytics

		*/

		SELECT
			(SELECT COUNT(1) FROM #UserBase) AS total,
			(SELECT COUNT(1) FROM #UserBase WHERE isActive = 1) AS active,
			(SELECT COUNT(1) FROM #UserBase WHERE isActive = 0) AS inactive,
			(SELECT COUNT(1) FROM #UserBase WHERE isOnline = 1) AS [online],
			(SELECT COUNT(1) FROM #UserBase WHERE dtCreated >= DATEADD(WEEK, DATEDIFF(WEEK, '1905-01-01', GETUTCDATE()), '1905-01-01')) AS createdThisWeek,
			(SELECT COUNT(1) FROM #UserBase WHERE dtCreated >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETUTCDATE()), 0)) AS createdThisMonth,
			(SELECT COUNT(1) FROM #UserBase WHERE dtCreated >= DATEADD(YEAR, DATEDIFF(YEAR, 0, GETUTCDATE()), 0)) AS createdThisYear

		-- DROP THE TEMPORARY TABLES
		DROP TABLE #UserBase

		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO