-- =====================================================================
-- PROCEDURE: [Enrollment.GetLessonDataIds]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.GetLessonDataIds]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.GetLessonDataIds]
GO

/*

Returns a recordset of lesson data ids along with the user id the enrollment belongs to.
This is used for the purpose of deleting SCO data for lesson data from the filesystem for enrollment reset.

*/

CREATE PROCEDURE [Enrollment.GetLessonDataIds]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idEnrollment			INT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
				
	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that enrollment exists
	
	*/

	IF (SELECT COUNT(1) 
	    FROM tblEnrollment 
		WHERE idEnrollment = @idEnrollment
		AND idSite = @idCallerSite
		) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'EnrollmentGetLessonDataIds_NoRecordFound'
		RETURN 1
		END

	/*

	get the user id of the user the enrollment belongs to

	*/

	DECLARE @idUser INT
	SELECT @idUser = idUser FROM tblEnrollment WHERE idEnrollment = @idEnrollment

	/*

	get the data

	*/

	SELECT
		@idUser AS idUser,
		[idData-Lesson]
	FROM [tblData-Lesson]
	WHERE idEnrollment = @idEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO