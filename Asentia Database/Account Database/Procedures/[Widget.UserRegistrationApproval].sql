-- =====================================================================
-- PROCEDURE: [Widget.UserRegistrationApproval]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Widget.UserRegistrationApproval]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.UserRegistrationApproval]
GO

/*

Gets a listing of user accounts that need to be approved.

*/

CREATE PROCEDURE [Widget.UserRegistrationApproval]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	-- NOTE THAT WE ARE NOT USING THE SEARCH, PAGINATION, OR SORT HERE BUT
	-- SINCE THESE ARE COMMON PARAMETERS FOR GRIDS, WE NEED TO KEEP THEM
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	begin getting the grid data

	*/

	CREATE TABLE #UserRegistrationApprovals (
		idUser						INT,
		displayName					NVARCHAR(768),
		username					NVARCHAR(255),
		email						NVARCHAR(255)
	)

	-- get the grid data

	INSERT INTO #UserRegistrationApprovals (
		idUser,
		displayName,
		username,
		email
	)
	SELECT
		U.idUser,
		U.displayName,
		U.username,
		U.email
	FROM tblUser U
	WHERE U.idSite = @idCallerSite
	AND U.isRegistrationApproved = 0 
	AND U.dtDenied IS NULL 
	AND U.idApprover IS NULL

	-- return the rowcount and grid data

	SELECT COUNT(1) AS row_count
	FROM #UserRegistrationApprovals

	;WITH
		Keys AS (
			SELECT TOP (@pageNum * @pageSize)
				URA.idUser,
				ROW_NUMBER() OVER (ORDER BY 
					-- ORDER DESC
					CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'displayName' THEN URA.displayName END) END DESC,
					CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'username' THEN URA.username END) END DESC,
					CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'email' THEN URA.email END) END DESC,
							
					-- ORDER ASC
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'displayName' THEN URA.displayName END) END,
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'username' THEN URA.username END) END,
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'email' THEN URA.email END) END
				)
				AS [row_number]
			FROM #UserRegistrationApprovals URA
		),

		SelectedKeys AS (
			SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
				idUser, 
				[row_number]
			FROM Keys
			WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
		)
	SELECT
		URA.idUser,
		URA.displayName,
		URA.username,
		URA.email
	FROM SelectedKeys 
	LEFT JOIN #UserRegistrationApprovals URA ON URA.idUser = SelectedKeys.idUser
	ORDER BY SelectedKeys.[row_number]

	-- drop temporary tables
	DROP TABLE #UserRegistrationApprovals

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO