-- =====================================================================
-- PROCEDURE: [Purchase.GetActiveCartForUser]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Purchase.GetActiveCartForUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Purchase.GetActiveCartForUser]
GO

/*

Returns the user's active cart details.
Active cart is defined as a purchase that has not been confirmed, is not pending, and is not failed.

*/
CREATE PROCEDURE [Purchase.GetActiveCartForUser]
(	
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@idUser					INT				OUTPUT,
	@idPurchase				INT				OUTPUT,	
	@orderNumber			NVARCHAR(12)	OUTPUT,
	@timeStamp				DATETIME		OUTPUT,
	@ccnum					NVARCHAR(4)		OUTPUT,
	@currency				NVARCHAR(4)		OUTPUT,
	@idSite					INT				OUTPUT,
	@dtPending				DATETIME		OUTPUT, 
	@failed					BIT				OUTPUT,
	@transactionId			NVARCHAR(255)	OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idPurchase		= P.idPurchase,
		@idUser			= P.idUser,
		@orderNumber	= P.orderNumber,
		@timeStamp		= P.[timeStamp],
		@ccnum			= P.ccnum,
		@currency		= P.currency,
		@idSite			= P.idSite,
		@dtPending		= P.dtPending,
		@failed			= P.failed
	FROM tblPurchase P	
	WHERE
		P.idUser		= @idUser
	AND	p.[timeStamp] IS NULL
	AND P.failed	  IS NULL
	AND P.idSite  =   @idCallerSite
	
	SELECT TOP 1 -- there should only ever be 1
		@idPurchase		= P.idPurchase,
		@idUser			= P.idUser,
		@orderNumber	= P.orderNumber,
		@timeStamp		= P.[timeStamp],
		@ccnum			= P.ccnum,
		@currency		= P.currency,
		@idSite			= P.idSite,
		@dtPending		= P.dtPending,
		@failed			= P.failed,
		@transactionId	= P.transactionId
	FROM tblPurchase P	
	WHERE P.idUser = @idUser
	AND P.[timeStamp] IS NULL
	AND (P.failed = 0 OR P.failed IS NULL)
	AND (P.dtPending IS NULL)
	AND P.idSite = @idCallerSite
	
	IF @@ROWCOUNT = 0
		BEGIN
		-- set purchase id to 0 so that we can notify the caller that there is not an active cart
		SET @idPurchase = 0
		END
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

