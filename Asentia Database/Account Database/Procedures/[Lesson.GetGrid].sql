-- =====================================================================
-- PROCEDURE: [Lesson.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Lesson.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Lesson.GetGrid]
GO

/*

Gets a listing of lessons.

*/

CREATE PROCEDURE [Lesson.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,

	@idCourse				INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END
		
		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
		
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblLesson L
			WHERE 
				(
					(@idCallerSite IS NULL)
					OR 
					(@idCallerSite IS NOT NULL AND L.idSite = @idCallerSite)
				)
			AND L.idCourse = @idCourse
			AND (L.isDeleted IS NULL OR L.isDeleted = 0)

			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						L.idLesson,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN L.[order] END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN L.[order] END
						)
						AS [row_number]
					FROM tblLesson L
					WHERE 
						(
							(@idCallerSite IS NULL)
							OR 
							(@idCallerSite IS NOT NULL AND L.idSite = @idCallerSite)
						)
					AND L.idCourse = @idCourse
					AND (L.isDeleted IS NULL OR L.isDeleted = 0)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idLesson, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				L.idLesson,
				L.[order] AS [order],
				CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE L.title END AS title,				
				CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 1 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasContentPackage,
				CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasStandupTraining,
				CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 3 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasTask,
				CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 4 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasOJT,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				L.isOptional,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblLesson L ON L.idLesson = SelectedKeys.idLesson
			LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
			WHERE (L.isDeleted IS NULL OR L.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
			
			END
		
		ELSE
		
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblLesson L
				INNER JOIN CONTAINSTABLE(tblLesson, *, @searchParam) K ON K.[key] = L.idLesson
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND L.idSite = @idCallerSite)
					)
				AND L.idCourse = @idCourse
				AND (L.isDeleted IS NULL OR L.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							L.idLesson,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN L.[order] END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN L.[order] END
							)
							AS [row_number]
						FROM tblLesson L
						INNER JOIN CONTAINSTABLE(tblLesson, *, @searchParam) K ON K.[key] = L.idLesson
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND L.idSite = @idCallerSite
							)
						AND L.idCourse = @idCourse
						AND (L.isDeleted IS NULL OR L.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idLesson, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					L.idLesson,
					L.[order] AS [order],
					L.title AS title,					
					CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 1 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasContentPackage,
					CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasStandupTraining,
					CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 3 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasTask,
					CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 4 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasOJT,
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					L.isOptional,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblLesson L ON L.idLesson = SelectedKeys.idLesson
				WHERE (L.isDeleted IS NULL OR L.isDeleted = 0)
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblLessonLanguage LL
				LEFT JOIN tblLesson L ON L.idLesson = LL.idLesson
				INNER JOIN CONTAINSTABLE(tblLessonLanguage, *, @searchParam) K ON K.[key] = LL.idLessonLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND L.idSite = @idCallerSite)
					)
				AND LL.idLanguage = @idCallerLanguage
				AND L.idCourse = @idCourse
				AND (L.isDeleted IS NULL OR L.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							LL.idLessonLanguage,
							L.idLesson,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN L.[order] END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN L.[order] END
							)
							AS [row_number]
						FROM tblLessonLanguage LL
						LEFT JOIN tblLesson L ON L.idLesson = LL.idLesson
						INNER JOIN CONTAINSTABLE(tblLessonLanguage, *, @searchParam) K ON K.[key] = LL.idLessonLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND L.idSite = @idCallerSite
							)
						AND LL.idLanguage = @idCallerLanguage
						AND L.idCourse = @idCourse
						AND (L.isDeleted IS NULL OR L.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idLesson, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					L.idLesson, 
					L.[order] AS [order],
					LL.title AS title,					
					CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 1 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasContentPackage,
					CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasStandupTraining,
					CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 3 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasTask,
					CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 4 AND LCL.idLesson = L.idLesson) = 1 THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS hasOJT,
					CONVERT(bit, 1) AS isModifyOn,
					L.isOptional,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblLessonLanguage LL ON LL.idLesson = SelectedKeys.idLesson AND LL.idLanguage = @idCallerLanguage
				LEFT JOIN tblLesson L ON L.idLesson = LL.idLesson
				ORDER BY SelectedKeys.[row_number]

				END
			
			END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO