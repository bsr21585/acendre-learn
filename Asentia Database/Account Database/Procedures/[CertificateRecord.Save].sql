-- =====================================================================
-- PROCEDURE: [CertificateRecord.Save]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificateRecord.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificateRecord.Save]
GO

/*

Adds new certificate or updates existing certificate.

*/

CREATE PROCEDURE [CertificateRecord.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idCertificateRecord    INT				OUTPUT,
	@idCertificate			INT,
	@idUser					INT,
	@timestamp				DATETIME,
	@idTimezone				INT,
	@isEarned				BIT				= 0
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/* NO PERMISSIONS CHECKS - WE CANNOT RESTRICT THIS BASED ON PERMISSIONS SINCE IT CAN BE CALLED AS A RESULT OF LEARNER ACTIONS */
	
	/*
	
	save the data
	
	*/	
	
	IF (
		SELECT COUNT(1) FROM tblCertificateRecord
		WHERE idUser = @idUser 
		AND idCertificate = @idCertificate
		AND (expires > GETUTCDATE() OR expires IS NULL)
	) > 0
		BEGIN
		
		UPDATE tblCertificateRecord 
		SET	[timestamp] = @timestamp,
			idTimezone	= @idTimeZone,
			expires = (
					   SELECT 
							CASE WHEN expiresTimeframe = 'd'
								THEN DATEADD(day, expiresInterval, @timestamp)
							WHEN expiresTimeframe = 'm'
								THEN DATEADD(month, expiresInterval, @timestamp)
							WHEN expiresTimeframe = 'yyyy'
								THEN DATEADD(year, expiresInterval, @timestamp)
							WHEN expiresTimeframe = 'ww'
								THEN DATEADD(week, expiresInterval, @timestamp)
							ELSE 
								NULL
							END
							FROM tblCertificate
							WHERE idCertificate = @idCertificate	
					  )
		WHERE idUser = @idUser 
		AND idCertificate = @idCertificate	
		
		END
	
	ELSE		
		BEGIN

		-- insert the new object		
		INSERT INTO tblCertificateRecord (			
			idSite,
			idCertificate,
			idUser,
			idAwardedBy,
			[timestamp],
			expires,
			idTimezone,
			code,
			credits,
			awardData
		)			
		SELECT 
			@idCallerSite,
			@idCertificate,
			@idUser,
			@idCaller,
			@timestamp,
			CASE WHEN expiresTimeframe = 'd'
					THEN DATEADD(day, expiresInterval, @timestamp)
				 WHEN expiresTimeframe = 'm'
					THEN DATEADD(month, expiresInterval, @timestamp)
				 WHEN expiresTimeframe = 'yyyy'
					THEN DATEADD(year, expiresInterval, @timestamp)
				 WHEN expiresTimeframe = 'ww'
					THEN DATEADD(week, expiresInterval, @timestamp)
			ELSE 
				NULL
			END,
			@idTimezone,
			code,
			credits,
			NULL -- this is null for now; eventually this will store the certificate's layout XML so that certificates can retain state 
		FROM tblCertificate C
		WHERE C.idCertificate = @idCertificate 
		AND C.idSite = @idCallerSite
		AND NOT EXISTS (SELECT 1
						FROM tblCertificateRecord CR
						WHERE CR.idCertificate = @idCertificate
						AND CR.idUser = @idUser
						AND CR.[timestamp] = @timestamp) -- do not award the same exact certificate twice
		
		-- get the new certificate record id and return successfully
		SELECT @idCertificateRecord = SCOPE_IDENTITY()

		/*

		do the event log entry for certificate awarded/earned

		*/

		DECLARE @idEventType INT

		IF (@isEarned = 1)
			BEGIN
			SET @idEventType = 701 -- earned
			END
		ELSE
			BEGIN
			SET @idEventType = 702 -- awarded
			END

		DECLARE @eventLogItem EventLogItemObjects

		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idCertificate, @idCertificateRecord, @idUser

		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, @idEventType, @timestamp, @eventLogItem
		
		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
