-- =====================================================================
-- PROCEDURE: [System.GenerateRandomString]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GenerateRandomString]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GenerateRandomString]
GO

/*

Returns a randomly generated string of specified length. 

*/

CREATE PROCEDURE [System.GenerateRandomString]
(
	@Length INT,
	@Return NVARCHAR(255) OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
			
	IF @Length > 255
	
		BEGIN
		RAISERROR('Cannot generate random string longer than 255 characters.', 16, 1)
		RETURN 0
		END

	DECLARE @Random FLOAT

	SET @Return = ''

	WHILE LEN(@Return) < @Length
		
		BEGIN

		SET @Random = Rand()

		IF CAST((2 * @Random + 1) AS INTEGER) = 1
			
			BEGIN
			SET @Return = @Return + CHAR((90 - 65 + 1) * @Random + 65)
			END

		ELSE

			BEGIN
			SET @Return = @Return + CHAR((57 - 48 + 1) * @Random + 48)
			END

		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO