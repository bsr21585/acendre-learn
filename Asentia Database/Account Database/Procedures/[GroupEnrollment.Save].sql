-- =====================================================================
-- PROCEDURE: [GroupEnrollment.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GroupEnrollment.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GroupEnrollment.Save]
GO

/*

Adds new group enrollment or updates existing group enrollment.

*/

CREATE PROCEDURE [GroupEnrollment.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idGroupEnrollment					INT				OUTPUT,
	@idCourse							INT,
	@idGroup							INT,
	@idTimezone							INT,
	@isLockedByPrerequisites			BIT,
	@dtStart							DATETIME,
	@dueInterval						INT,
	@dueTimeframe						NVARCHAR(4),
	@expiresFromStartInterval			INT,
	@expiresFromStartTimeframe			NVARCHAR(4),
	@expiresFromFirstLaunchInterval		INT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4)
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	check for duplicate course to group (only one allowed for a given group/course)

	*/
		
	IF (
		SELECT COUNT(1) 
		FROM tblGroupEnrollment 
		WHERE idCourse = @idCourse					-- same course
		AND idGroup = @idGroup						-- same group
		AND idGroupEnrollment <> @idGroupEnrollment -- but different enrollment
		) <> 0

		BEGIN	
		SET @Return_Code = 2
		SET @Error_Description_Code = 'GroupEnrollmentSave_DuplicateRecord'
		RETURN 1
		END
		
	/*

	save the data

	*/

	IF (@idGroupEnrollment = 0 OR @idGroupEnrollment is null)

		BEGIN
		
		-- insert the new group enrollment
		
		INSERT INTO tblGroupEnrollment (
			idSite,
			idCourse,
			idGroup,
			idTimezone,
			isLockedByPrerequisites,
			dtStart,
			dtCreated,
			dueInterval,
			dueTimeframe,
			expiresFromStartInterval,
			expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe
		)
		SELECT
			@idCallerSite,
			@idCourse,
			@idGroup,
			@idTimezone,
			@isLockedByPrerequisites,
			@dtStart,
			GETUTCDATE(),
			@dueInterval,
			@dueTimeFrame,
			@expiresFromStartInterval,
			@expiresFromStartTimeframe,
			@expiresFromFirstLaunchInterval,
			@expiresFromFirstLaunchTimeframe
		FROM tblCourse C
		WHERE C.idCourse = @idCourse
		
		-- get the new group enrollmemt's id
		
		SET @idGroupEnrollment = SCOPE_IDENTITY()
		
		END
		
	ELSE
	
		BEGIN
		
		-- check that the group enrollment id exists
		IF (SELECT COUNT(1) FROM tblGroupEnrollment WHERE idGroupEnrollment = @idGroupEnrollment AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idGroupEnrollment = @idGroupEnrollment
			SET @Return_Code = 1 
			SET @Error_Description_Code = 'GroupEnrollmentSave_NoRecordFound'
			RETURN 1
			
			END
			
		-- update existing enrollment's properties

		UPDATE tblGroupEnrollment SET
			idTimezone							= @idTimezone,
			isLockedByPrerequisites				= @isLockedByPrerequisites,
			dtStart								= @dtStart,
			dueInterval							= @dueInterval,
			dueTimeFrame						= @dueTimeFrame,
			expiresFromStartInterval			= @expiresFromStartInterval,
			expiresFromStartTimeframe			= @expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval		= @expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe		= @expiresFromFirstLaunchTimeframe
		WHERE idGroupEnrollment = @idGroupEnrollment
		
		-- get the enrollment's id

		SET @idGroupEnrollment = @idGroupEnrollment
		
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO