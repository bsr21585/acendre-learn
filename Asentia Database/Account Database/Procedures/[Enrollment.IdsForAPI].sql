-- =====================================================================
-- PROCEDURE: [Enrollment.IdsForAPI]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.IdsForAPI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.IdsForAPI]
GO

/*

Returns a recordset of enrollment ids to populate a list with no exclusions for a specific object in API.

*/

CREATE PROCEDURE [Enrollment.IdsForAPI]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idUser					INT,
	@idCourse				INT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
				
		BEGIN
		SELECT DISTINCT E.idEnrollment
		FROM tblEnrollment E
		WHERE E.idUser = CASE WHEN @idUser > 0  THEN  @idUser ELSE E.idUser END
		AND E.idCourse = CASE WHEN @idCourse > 0  THEN  @idCourse ELSE E.idCourse END
		END



	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO