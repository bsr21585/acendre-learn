-- =====================================================================
-- PROCEDURE: [Group.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.GetGrid]
GO

/*

Gets a listing of Groups.

*/

CREATE PROCEDURE [Group.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblGroup G
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND G.idSite = @idCallerSite)
				)			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						G.idGroup,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN G.name END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN G.name END
						)
						AS [row_number]
					FROM tblGroup G
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND G.idSite = @idCallerSite)
						)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idGroup, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				G.idGroup, 
				G.avatar,
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name, 
				(SELECT COUNT(1) FROM (SELECT DISTINCT idUser FROM tblUserToGroupLink UGL WHERE UGL.idGroup = G.idGroup) AS [count]) AS memberCount,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblGroup G ON G.idGroup = SelectedKeys.idGroup
			LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblGroup G
				INNER JOIN CONTAINSTABLE(tblGroup, *, @searchParam) K ON K.[key] = G.idGroup
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND G.idSite = @idCallerSite)
					)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							G.idGroup,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN G.name END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN G.name END
							)
							AS [row_number]
						FROM tblGroup G
						INNER JOIN CONTAINSTABLE(tblGroup, *, @searchParam) K ON K.[key] = G.idGroup
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND G.idSite = @idCallerSite
							)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idGroup, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					G.idGroup, 
					G.avatar,
					G.name AS name,
					(SELECT COUNT(1) FROM (SELECT DISTINCT idUser FROM tblUserToGroupLink UGL WHERE UGL.idGroup = G.idGroup) AS [count]) AS memberCount,
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblGroup G ON G.idGroup = SelectedKeys.idGroup
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblGroupLanguage GL
				LEFT JOIN tblGroup G ON G.idGroup = GL.idGroup
				INNER JOIN CONTAINSTABLE(tblGroupLanguage, *, @searchParam) K ON K.[key] = GL.idGroupLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND G.idSite = @idCallerSite)
					)
					AND GL.idLanguage = @idCallerLanguage
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							GL.idGroupLanguage,
							G.idGroup,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN GL.name END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN GL.name END
							)
							AS [row_number]
						FROM tblGroupLanguage GL
						LEFT JOIN tblGroup  G ON G.idGroup = GL.idGroup
						INNER JOIN CONTAINSTABLE(tblGroupLanguage, *, @searchParam) K ON K.[key] = GL.idGroupLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND G.idSite = @idCallerSite
							)
							AND GL.idLanguage = @idCallerLanguage
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idGroup, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					G.idGroup, 
					G.avatar,
					GL.name,
					(SELECT COUNT(1) FROM (SELECT DISTINCT idUser FROM tblUserToGroupLink UGL WHERE UGL.idGroup = G.idGroup) AS [count]) AS memberCount,
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblGroupLanguage GL ON GL.idGroup = SelectedKeys.idGroup AND GL.idLanguage = @idCallerLanguage
				LEFT JOIN tblGroup G ON G.idGroup = GL.idGroup
				ORDER BY SelectedKeys.[row_number]

				END
			
			END

	END					

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO