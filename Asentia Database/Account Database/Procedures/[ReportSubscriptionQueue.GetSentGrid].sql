SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportSubscriptionQueue.GetSentGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportSubscriptionQueue.GetSentGrid]
GO

/*

Gets a listing of report subscription log for sent Grid.

*/

CREATE PROCEDURE [ReportSubscriptionQueue.GetSentGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblReportSubscriptionQueue R
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
				)
				AND R.dtSent IS NOT NULL AND R.statusDescription IS NULL
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idReportSubscriptionQueue,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'fullName' THEN RSQ.recipientFullName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'reportName' THEN (CASE WHEN RL.title IS NOT NULL THEN RL.title ELSE R.title END) END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtSent' THEN RSQ.dtSent END) END DESC,

							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'fullName' THEN RSQ.recipientFullName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'reportName' THEN (CASE WHEN RL.title IS NOT NULL THEN RL.title ELSE R.title END) END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtSent' THEN RSQ.dtSent END) END
						)
						AS [row_number]
					FROM tblReportSubscriptionQueue RSQ
					LEFT JOIN tblReport R ON R.idReport = RSQ.idReport
					LEFT JOIN tblReportLanguage RL ON RL.idReport = RSQ.idReport AND RL.idLanguage = @idCallerLanguage		
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND RSQ.idSite = @idCallerSite)
						)
						AND RSQ.dtSent IS NOT NULL AND RSQ.statusDescription IS NULL
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idReportSubscriptionQueue, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				RSQ.idReportSubscriptionQueue, 
				RSQ.recipientFullName AS fullName,
				RSQ.recipientEmail AS email,
				CASE WHEN RL.title IS NOT NULL THEN RL.title ELSE R.title END AS reportName, 
				RSQ.dtSent,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblReportSubscriptionQueue RSQ ON RSQ.idReportSubscriptionQueue = SelectedKeys.idReportSubscriptionQueue			
			LEFT JOIN tblReport R ON R.idReport = RSQ.idReport
			LEFT JOIN tblReportLanguage RL ON RL.idReport = RSQ.idReport AND RL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblReportSubscriptionQueue
			INNER JOIN CONTAINSTABLE(tblReportSubscriptionQueue, *, @searchParam) K ON K.[key] = tblReportSubscriptionQueue.idReportSubscriptionQueue
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
				)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idReportSubscriptionQueue,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'fullName' THEN RSQ.recipientFullName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'reportName' THEN (CASE WHEN RL.title IS NOT NULL THEN RL.title ELSE R.title END) END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtSent' THEN RSQ.dtSent END) END DESC,

							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'fullName' THEN RSQ.recipientFullName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'reportName' THEN (CASE WHEN RL.title IS NOT NULL THEN RL.title ELSE R.title END) END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtSent' THEN RSQ.dtSent END) END
						)
						AS [row_number]
					FROM tblReportSubscriptionQueue RSQ
					INNER JOIN CONTAINSTABLE(tblReportSubscriptionQueue, *, @searchParam) K ON K.[key] = RSQ.idReportSubscriptionQueue
					LEFT JOIN tblReport R ON R.idReport = RSQ.idReport
					LEFT JOIN tblReportLanguage RL ON RL.idReport = RSQ.idReport AND RL.idLanguage = @idCallerLanguage
					WHERE 
						(
						@idCallerSite IS NULL
						OR 
						@idCallerSite IS NOT NULL AND RSQ.idSite = @idCallerSite
						)
						AND RSQ.dtSent IS NOT NULL AND RSQ.statusDescription IS NULL
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idReportSubscriptionQueue, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				RSQ.idReportSubscriptionQueue, 
				RSQ.recipientFullName AS fullName,
				RSQ.recipientEmail AS email,
				CASE WHEN RL.title IS NOT NULL THEN RL.title ELSE R.title END AS reportName, 
				RSQ.dtSent,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblReportSubscriptionQueue RSQ ON RSQ.idReportSubscriptionQueue = SelectedKeys.idReportSubscriptionQueue			
			LEFT JOIN tblReport R ON R.idReport = RSQ.idReport
			LEFT JOIN tblReportLanguage RL ON RL.idReport = RSQ.idReport AND RL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
			
			END

		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END
