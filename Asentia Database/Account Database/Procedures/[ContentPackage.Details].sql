-- =====================================================================
-- PROCEDURE: [ContentPackage.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ContentPackage.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ContentPackage.Details]
GO

/*

Return all the properties for a given content package id.

*/
CREATE PROCEDURE [ContentPackage.Details]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0,
	
	
	@idContentPackage				INT				OUTPUT,
	@idSite							INT				OUTPUT, 
    @name							NVARCHAR(255)	OUTPUT,
    @path							NVARCHAR(MAX)	OUTPUT,
    @kb								INT				OUTPUT,
    @idContentPackageType			INT				OUTPUT,
	@idSCORMPackageType				INT				OUTPUT,
    @manifest						NVARCHAR(MAX)	OUTPUT,
    @dtCreated						DATETIME		OUTPUT,
    @dtModified						DATETIME		OUTPUT,
	@isMediaUpload					BIT				OUTPUT,
	@idMediaType					INT				OUTPUT,
	@contentTitle					NVARCHAR(255)	OUTPUT,
	@originalMediaFilename			NVARCHAR(255)	OUTPUT,
	@isVideoMedia3rdParty			BIT				OUTPUT,
	@videoMediaEmbedCode			NVARCHAR(MAX)	OUTPUT,
	@enableAutoplay					BIT				OUTPUT,
	@allowRewind					BIT				OUTPUT,
	@allowFastForward				BIT				OUTPUT,
	@allowNavigation				BIT				OUTPUT,
	@allowResume					BIT				OUTPUT,
	@minProgressForCompletion		FLOAT			OUTPUT,
	@isProcessing					BIT				OUTPUT,
	@isProcessed					BIT				OUTPUT,
	@dtProcessed					DATETIME		OUTPUT,
	@idQuizSurvey					INT				OUTPUT,
	@hasManifestComplianceErrors	BIT				OUTPUT,
	@manifestComplianceErrors		NVARCHAR(MAX)	OUTPUT,
	@openSesameGUID					NVARCHAR(36)	OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblContentPackage
		WHERE idContentPackage = @idContentPackage
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ContentPackageDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idContentPackage				= CP.idContentPackage,
		@idSite							= CP.idSite,
		@name							= CP.name,
		@path							= CP.[path],
		@kb								= CP.kb,
		@idContentPackageType			= CP.idContentPackageType,
		@idSCORMPackageType				= CP.idSCORMPackageType,
		@manifest						= CP.manifest,
		@dtCreated						= CP.dtCreated,
		@dtModified						= CP.dtModified,
		@isMediaUpload					= CP.isMediaUpload,
		@idMediaType					= CP.idMediaType,
		@contentTitle					= CP.contentTitle,
		@originalMediaFilename			= CP.originalMediaFilename,
		@isVideoMedia3rdParty			= CP.isVideoMedia3rdParty,
		@videoMediaEmbedCode			= CP.videoMediaEmbedCode,
		@enableAutoplay					= CP.enableAutoplay,
		@allowRewind					= CP.allowRewind,
		@allowFastForward				= CP.allowFastForward,
		@allowNavigation				= CP.allowNavigation,
		@allowResume					= CP.allowResume,
		@minProgressForCompletion		= CP.minProgressForCompletion,
		@isProcessing					= CP.isProcessing,
		@isProcessed					= CP.isProcessed,
		@dtProcessed					= CP.dtProcessed,
		@idQuizSurvey					= CP.idQuizSurvey,
		@hasManifestComplianceErrors	= CASE WHEN CP.hasManifestComplianceErrors IS NULL THEN 0 ELSE CP.hasManifestComplianceErrors END,
		@manifestComplianceErrors		= CP.manifestComplianceErrors,
		@openSesameGUID					= CP.openSesameGUID
	FROM tblContentPackage CP	
	WHERE CP.idContentPackage = @idContentPackage
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ContentPackageDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO