-- =====================================================================
-- PROCEDURE: [Widget.ILTRosterManagement]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Widget.ILTRosterManagement]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.ILTRosterManagement]
GO

/*

Gets a listing of lesson data for ilt roster management widget.

*/

CREATE PROCEDURE [Widget.ILTRosterManagement]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	-- NOTE THAT WE ARE NOT USING THE SEARCH, PAGINATION, OR SORT HERE BUT
	-- SINCE THESE ARE COMMON PARAMETERS FOR GRIDS, WE NEED TO KEEP THEM
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	begin getting the grid data

	*/

	CREATE TABLE #ILTSessions (
		idStandupTrainingInstance	INT,
		idStandupTraining			INT,
		moduleTitle					NVARCHAR(255),
		sessionTitle				NVARCHAR(255),
		dtStart						DATETIME,
		[type]						INT,
		hostUrl						NVARCHAR(1024)
	)

	-- get the grid data

	INSERT INTO #ILTSessions (
		idStandupTrainingInstance,
		idStandupTraining,
		moduleTitle,
		sessionTitle,
		dtStart,
		[type],
		hostUrl
	)
	SELECT
		STI.idStandUpTrainingInstance,
		STI.idStandUpTraining,
		CASE WHEN STL.title IS NOT NULL THEN STL.title ELSE ST.title END AS moduleTitle,
		CASE WHEN STIL.title IS NOT NULL THEN STIL.title ELSE STI.title END AS sessionTitle,
		(SELECT MIN(dtStart) FROM tblStandUpTrainingInstanceMeetingTime STIMT WHERE STIMT.idStandUpTrainingInstance = STI.idStandUpTrainingInstance) AS dtStart,
		STI.[type] AS [type],
		STI.hostUrl
	FROM tblStandUpTrainingInstance STI
	LEFT JOIN tblStandupTraining ST ON ST.idStandUpTraining = STI.idStandUpTraining
	LEFT JOIN tblStandUpTrainingInstanceLanguage STIL ON STIL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance AND STIL.idLanguage = @idCallerLanguage
	LEFT JOIN tblStandUpTrainingLanguage STL ON STL.idStandUpTraining = ST.idStandUpTraining AND STL.idLanguage = @idCallerLanguage
	WHERE (
				@idCaller = 1	-- administrator gets to see all sessions
				OR
				(   -- other users only get to see sessions in which they are instructing
					@idCaller > 1
					AND
					STI.idStandUpTrainingInstance IN (SELECT idStandupTrainingInstance FROM tblStandupTrainingInstanceToInstructorLink WHERE idInstructor = @idCaller)
				)
		  )
	AND STI.idSite = @idCallerSite
	AND (STI.isDeleted = 0 OR STI.isDeleted IS NULL)
	AND (ST.isDeleted = 0 OR ST.isDeleted IS NULL)

	-- return the rowcount and grid data

	SELECT COUNT(1) AS row_count
	FROM #ILTSessions

	;WITH
		Keys AS (
			SELECT TOP (@pageNum * @pageSize)
				ILTS.idStandUpTrainingInstance,
				ROW_NUMBER() OVER (ORDER BY ILTS.dtStart DESC)
				AS [row_number]
			FROM #ILTSessions ILTS
		),

		SelectedKeys AS (
			SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
				idStandUpTrainingInstance, 
				[row_number]
			FROM Keys
			WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
		)
	SELECT
		ILTS.idStandupTrainingInstance,
		ILTS.idStandupTraining,
		ILTS.moduleTitle,
		ILTS.sessionTitle,
		ILTS.dtStart,			
		CONVERT(BIT, 1) AS manage,
		ILTS.[type] AS [type],
		CASE WHEN ILTS.[type] IN (2, 3, 4, 5) AND GETUTCDATE() >= DATEADD(minute, -15, ILTS.dtStart) THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS launch,
		ILTS.hostUrl,
		SelectedKeys.[row_number]
	FROM SelectedKeys
	JOIN #ILTSessions ILTS ON ILTS.idStandupTrainingInstance = SelectedKeys.idStandupTrainingInstance
	ORDER BY SelectedKeys.[row_number]

	-- drop temporary tables
	DROP TABLE #ILTSessions

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO