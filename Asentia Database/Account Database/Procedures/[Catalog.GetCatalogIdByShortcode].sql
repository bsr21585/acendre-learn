-- =====================================================================
-- PROCEDURE: [Catalog.GetCatalogIdByShortcode]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.GetCatalogIdByShortcode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.GetCatalogIdByShortcode]
GO

/*

Gets a catalog id based on the shortcode

*/
CREATE PROCEDURE [Catalog.GetCatalogIdByShortcode]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@shortcode					NVARCHAR(10),
	@idCatalog					INT				OUTPUT
AS

	BEGIN	
	SET NOCOUNT ON

    SELECT
		@idCatalog = C.idCatalog
	FROM tblCatalog C
	WHERE C.shortcode = @shortcode
	ANd C.idSite = @idCallerSite

	IF (@idCatalog IS NULL)
		BEGIN
		SET @idCatalog = 0
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		
