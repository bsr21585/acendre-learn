-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.Delete]
GO

/*

Deletes standup training instances

*/

CREATE PROCEDURE [StandupTrainingInstance.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@StandupTrainingInstances	IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @StandupTrainingInstances) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'StandupTrainingInstanceDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all standup training instances exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @StandupTrainingInstances STISTI
		LEFT JOIN tblStandupTrainingInstance STI ON STI.idStandUpTrainingInstance = STISTI.id
		WHERE STI.idSite IS NULL
		OR STI.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'StandupTrainingInstanceDelete_NoRecordFound'
		RETURN 1 
		END
	
	/*
	
	DELETE the standup training instance(s), and all related objects.
	
	*/

	/*

	SOFT DELETE the standup training instance(s), just mark them as deleted, DO NOT remove the record!

	*/

	UPDATE tblStandUpTrainingInstance SET isDeleted = 1, dtDeleted = GETUTCDATE() WHERE idStandUpTrainingInstance IN (SELECT id FROM @StandupTrainingInstances)

	/* DO NOT DELETE THE MEETING TIMES - AT LEAST FOR NOW */

	DELETE FROM tblStandupTrainingInstanceMeetingTime WHERE idStandUpTrainingInstance IN (SELECT id FROM @StandupTrainingInstances)

	/*

	DELETE all links to resources

	*/

	DELETE FROM tblResourceToObjectLink WHERE idObject IN (SELECT id FROM @StandupTrainingInstances)

	/*

	DELETE all links to users where they are not completed

	*/

	DELETE FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance IN (SELECT id FROM @StandupTrainingInstances) AND dtCompleted IS NULL
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO