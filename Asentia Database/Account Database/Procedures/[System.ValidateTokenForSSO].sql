-- =====================================================================
-- PROCEDURE: [System.ValidateTokenForSSO]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF exists (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ValidateTokenForSSO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ValidateTokenForSSO]
GO

/*

Validates that a given SSO token is valid.

*/

CREATE PROCEDURE [System.ValidateTokenForSSO]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
		
	@token					NVARCHAR(40),	   
	@idUser					INT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	get the user the token belongs to and validate
	
	*/
		
	SELECT 
		@idUser = idUser
	FROM tblSSOToken
	WHERE idSite = @idCallerSite
	AND token = @token
	AND dtExpires > GETUTCDATE()

	IF (@idUser IS NULL)
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'SystemValidateTokenForSSO_InvalidToken'
		RETURN 1
		END
			
	/*

	return

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''			

	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO