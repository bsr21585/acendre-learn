-- =====================================================================
-- PROCEDURE: [ContentPackage.GetOpenSesameGUIDs]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ContentPackage.GetOpenSesameGUIDs]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ContentPackage.GetOpenSesameGUIDs]
GO

/*

Gets a listing of Open Sesame GUIDs for packages synched from Open Sesame.

*/
CREATE PROCEDURE [ContentPackage.GetOpenSesameGUIDs]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0	
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END	

	/*

	get the Open Sesame GUIDs for the content packages

	*/

	SELECT DISTINCT
		CP.idContentPackage,
		CP.openSesameGUID
	FROM tblContentPackage CP
	WHERE CP.idSite = @idCallerSite
	AND CP.openSesameGUID IS NOT NULL

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO