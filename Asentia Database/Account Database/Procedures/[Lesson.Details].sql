-- =====================================================================
-- PROCEDURE: [Lesson.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Lesson.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Lesson.Details]
GO

/*

Returns all the properties for a given lesson id.

*/

CREATE PROCEDURE [Lesson.Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idLesson				INT				OUTPUT,
	@idSite					INT				OUTPUT,
	@idCourse				INT				OUTPUT,
	@title					NVARCHAR(255)	OUTPUT,
	@revcode				NVARCHAR(32)	OUTPUT,
	@shortDescription		NVARCHAR(512)	OUTPUT, 
	@longDescription		NVARCHAR(MAX)	OUTPUT, 
	@dtCreated				DATETIME		OUTPUT,
	@dtModified				DATETIME		OUTPUT,
	@isDeleted				BIT				OUTPUT,
	@dtDeleted				DATETIME		OUTPUT,
	@order					INT				OUTPUT,
	@searchTags				NVARCHAR(512)	OUTPUT,
	@isOptional				BIT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLesson
		WHERE idLesson = @idLesson
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LessonDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idLesson			= L.idLesson,
		@idSite				= L.idSite,
		@idCourse			= L.idCourse,
		@title				= L.title,
		@revcode			= L.revcode,
		@shortDescription	= L.shortDescription,
		@longDescription	= L.longDescription,
		@dtCreated			= L.dtCreated,
		@dtModified			= L.dtModified,
		@isDeleted			= L.isDeleted,
		@dtDeleted			= L.dtDeleted,
		@order				= L.[order],
		@searchTags			= L.searchTags,
		@isOptional			= L.isOptional
	FROM tblLesson L
	WHERE L.idLesson = @idLesson
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LessonDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO