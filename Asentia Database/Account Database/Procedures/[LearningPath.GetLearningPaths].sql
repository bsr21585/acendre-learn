-- =====================================================================
-- PROCEDURE: [[LearningPath.GetLearningPaths]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[LearningPath.GetLearningPaths]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.GetLearningPaths]
GO

/*

Return all the properties for a given group id.

*/
CREATE PROCEDURE [LearningPath.GetLearningPaths]
(
	@Return_Code			INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)		OUTPUT,
	@idCallerSite			INT					= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	@isPublished			BIT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END	

	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite

	/*
	
	get the data
	
	*/
	
	SELECT	LP.idLearningPath,
			CASE WHEN LPL.name IS NULL OR LPL.name = '' THEN LP.name ELSE LPL.name END AS name,
			LP.idSite,
			CASE WHEN LPL.shortDescription IS NULL OR LPL.shortDescription = '' THEN LP.shortDescription ELSE LPL.shortDescription END AS shortDescription,	
			CASE WHEN LPL.longDescription IS NULL OR LPL.longDescription = '' THEN LP.longDescription ELSE LPL.longDescription END AS longDescription				
	FROM tblLearningPath LP
	LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = @idCallerLanguage
	WHERE LP.isPublished = @isPublished
	AND LP.dtDeleted IS NULL
	AND LP.idSite = @idCallerSite

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO