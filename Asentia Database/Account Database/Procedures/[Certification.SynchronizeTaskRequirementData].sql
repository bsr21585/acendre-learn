-- =====================================================================
-- PROCEDURE: [Certification.SynchronizeTaskRequirementData]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.SynchronizeTaskRequirementData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.SynchronizeTaskRequirementData]
GO

/*

Synchronizes task requirement data for a user's certification.

*/

CREATE PROCEDURE [Certification.SynchronizeTaskRequirementData]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0,

	@idCertificationToUserLink		INT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	declare variables

	*/

	DECLARE @idUser					INT
	DECLARE @idCertification		INT
	DECLARE @certificationTrack		INT
	DECLARE @initialAwardDate		DATETIME
	DECLARE @lastExpirationDate		DATETIME
	DECLARE @currentExpirationDate	DATETIME	

	SELECT
		@idUser = CUL.idUser,
		@idCertification = CUL.idCertification,
		@certificationTrack = CUL.certificationTrack,
		@initialAwardDate = CUL.initialAwardDate,
		@lastExpirationDate = CUL.lastExpirationDate,
		@currentExpirationDate = CUL.currentExpirationDate		
	FROM tblCertificationToUserLink  CUL
	WHERE CUL.idCertificationToUserLink = @idCertificationToUserLink

	/*

	if this is an initial track, get all initial requirements and ensure there is data for the task ones

	*/

	IF (@certificationTrack = 0)
		BEGIN

		-- get task requirements
		DECLARE @initialTaskRequirementIds TABLE (idCertificationModuleRequirement INT)

		INSERT INTO @initialTaskRequirementIds (
			idCertificationModuleRequirement
		)
		SELECT
			CMR.idCertificationModuleRequirement
		FROM tblCertificationModuleRequirement CMR
		LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModuleRequirementSet = CMR.idCertificationModuleRequirementSet
		LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = CMRS.idCertificationModule
		WHERE CM.idCertification = @idCertification
		AND CM.isInitialRequirement = 1
		AND CMR.requirementType = 2

		-- insert the data
		INSERT INTO [tblData-CertificationModuleRequirement] (
			idSite,
			idCertificationToUserLink,
			idCertificationModuleRequirement,
			label
		)
		SELECT
			@idCallerSite,
			@idCertificationToUserLink,
			TRI.idCertificationModuleRequirement,
			CMR.label
		FROM @initialTaskRequirementIds TRI
		LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirement = TRI.idCertificationModuleRequirement
		WHERE NOT EXISTS (SELECT 1 
						  FROM [tblData-CertificationModuleRequirement] DCMR
						  WHERE DCMR.idCertificationToUserLink = @idCertificationToUserLink
						  AND DCMR.idCertificationModuleRequirement = TRI.idCertificationModuleRequirement)

		END

	/*

	if this is a renewal track, get all renewal requirements and ensure there is data for the task ones
	then, if the current date is past the last expiration, reset the requirements

	*/

	IF (@certificationTrack = 1)
		BEGIN

		-- get task requirements
		DECLARE @renewalTaskRequirementIds TABLE (idCertificationModuleRequirement INT)

		INSERT INTO @renewalTaskRequirementIds (
			idCertificationModuleRequirement
		)
		SELECT
			CMR.idCertificationModuleRequirement
		FROM tblCertificationModuleRequirement CMR
		LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModuleRequirementSet = CMR.idCertificationModuleRequirementSet
		LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = CMRS.idCertificationModule
		WHERE CM.idCertification = @idCertification
		AND CM.isInitialRequirement = 0
		AND CMR.requirementType = 2

		-- insert the data
		INSERT INTO [tblData-CertificationModuleRequirement] (
			idSite,
			idCertificationToUserLink,
			idCertificationModuleRequirement,
			label
		)
		SELECT
			@idCallerSite,
			@idCertificationToUserLink,
			TRI.idCertificationModuleRequirement,
			CMR.label
		FROM @renewalTaskRequirementIds TRI
		LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirement = TRI.idCertificationModuleRequirement
		WHERE NOT EXISTS (SELECT 1 
						  FROM [tblData-CertificationModuleRequirement] DCMR
						  WHERE DCMR.idCertificationToUserLink = @idCertificationToUserLink
						  AND DCMR.idCertificationModuleRequirement = TRI.idCertificationModuleRequirement)

		-- reset if we're past the last expiration date
		IF (@lastExpirationDate IS NOT NULL AND GETUTCDATE() >= @lastExpirationDate)
			BEGIN

			UPDATE [tblData-CertificationModuleRequirement] SET
				label = CMR.label,
				dtCompleted = NULL,
				completionDocumentationFilePath = NULL,
				dtSubmitted = NULL,
				isApproved = NULL,
				dtApproved = NULL,
				idApprover = NULL
			FROM @renewalTaskRequirementIds TRI
			LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirement = TRI.idCertificationModuleRequirement
			WHERE TRI.idCertificationModuleRequirement = [tblData-CertificationModuleRequirement].idCertificationModuleRequirement
			AND [tblData-CertificationModuleRequirement].idCertificationToUserLink = @idCertificationToUserLink

			END

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	