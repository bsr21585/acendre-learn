-- =====================================================================
-- PROCEDURE: [RuleSetLearningPathEnrollment.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetLearningPathEnrollment.Details]
GO

/*

Returns all the properties for a given ruleset learning path enrollment id.

*/

CREATE PROCEDURE [RuleSetLearningPathEnrollment.Details]
(
	@Return_Code							INT				OUTPUT,
	@Error_Description_Code					NVARCHAR(50)	OUTPUT,
	@idCallerSite							INT				= 0, --default if not specified
	@callerLangString						NVARCHAR(10),
	@idCaller								INT				= 0,
	
	@idRuleSetLearningPathEnrollment		INT				OUTPUT,
	@idSite									INT				OUTPUT,
	@idLearningPath							INT				OUTPUT,
	@idTimezone 							INT				OUTPUT,
	@priority 								INT				OUTPUT,
	@label									NVARCHAR(255)	OUTPUT,
	@dtStart 								DATETIME		OUTPUT,
	@dtEnd 									DATETIME		OUTPUT,
	@delayInterval 							INT				OUTPUT,
	@delayTimeframe							NVARCHAR(4)		OUTPUT,
	@dueInterval							INT				OUTPUT,
	@dueTimeframe							NVARCHAR(4)		OUTPUT,
	@expiresFromStartInterval 				INT				OUTPUT,
	@expiresFromStartTimeframe				NVARCHAR(4)		OUTPUT,
	@expiresFromFirstLaunchInterval 		INT				OUTPUT,
	@expiresFromFirstLaunchTimeframe		NVARCHAR(4)		OUTPUT,
	@idParentRuleSetLearningPathEnrollment	INT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblRuleSetLearningPathEnrollment
		WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetLearningPathEnrollmentDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
	
	SELECT
		@idRuleSetLearningPathEnrollment		= RSLPE.idRuleSetLearningPathEnrollment,
		@idSite									= RSLPE.idSite,
		@idLearningPath							= RSLPE.idLearningPath,
		@idTimezone								= RSLPE.idTimezone,
		@priority								= RSLPE.[priority],
		@label									= RSLPE.label,
		@dtStart								= RSLPE.dtStart,
		@dtEnd									= RSLPE.dtEnd,
		@delayInterval							= RSLPE.delayInterval,
		@delayTimeframe							= RSLPE.delayTimeframe,
		@dueInterval							= RSLPE.dueInterval,
		@dueTimeframe							= RSLPE.dueTimeframe,
		@expiresFromStartInterval 				= RSLPE.expiresFromStartInterval,
		@expiresFromStartTimeframe				= RSLPE.expiresFromStartTimeframe,
		@expiresFromFirstLaunchInterval 		= RSLPE.expiresFromFirstLaunchInterval,
		@expiresFromFirstLaunchTimeframe		= RSLPE.expiresFromFirstLaunchTimeframe,
		@idParentRuleSetLearningPathEnrollment	= RSLPE.idParentRuleSetLearningPathEnrollment
	FROM tblRuleSetLearningPathEnrollment RSLPE
	WHERE RSLPE.idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetLearningPathEnrollmentDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO