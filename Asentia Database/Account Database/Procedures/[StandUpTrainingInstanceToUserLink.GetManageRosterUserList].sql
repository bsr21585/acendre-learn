-- =====================================================================
-- PROCEDURE: [StandUpTrainingInstanceToUserLink.GetManageRosterUserList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandUpTrainingInstanceToUserLink.GetManageRosterUserList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandUpTrainingInstanceToUserLink.GetManageRosterUserList]
GO

/*

Returns a recordset of user ids and names that are in enrolledList/waitingList of a standup training roster.

*/

CREATE PROCEDURE [StandUpTrainingInstanceToUserLink.GetManageRosterUserList]
(
	@Return_Code							INT				OUTPUT,
	@Error_Description_Code					NVARCHAR(50)	OUTPUT,
	@idCallerSite							INT				= 0, --default if not specified
	@callerLangString						NVARCHAR(10),
	@idCaller								INT				= 0,

	@idStandupTrainingInstance				INT,
	@isWaitingList							BIT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the data

	*/

	SELECT DISTINCT
		STIUL.idStandupTrainingInstanceToUserLink,
		STIUL.idStandupTrainingInstance,
		U.idUser, 
		U.displayName + ' (' + U.username + ')' AS displayName,
		U.username,
		U.email,
		STIUL.completionStatus,
		STIUL.score,
		STIUL.isWaitingList,
		STIUL.registrantKey,
		CASE WHEN STIUL.registrantEmail IS NULL AND STIUL.isWaitingList = 1 THEN U.email ELSE STIUL.registrantEmail END AS registrantEmail,
		STIUL.joinUrl,
		STIUL.waitlistOrder
	FROM tblStandupTrainingInstanceToUserLink STIUL
	LEFT JOIN tblUser U ON U.idUser = STIUL.idUser
	WHERE U.idSite = @idCallerSite	
	AND STIUL.idStandupTrainingInstance = @idStandupTrainingInstance
	AND STIUL.isWaitingList = @isWaitingList	
	ORDER BY waitlistOrder, displayName

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	