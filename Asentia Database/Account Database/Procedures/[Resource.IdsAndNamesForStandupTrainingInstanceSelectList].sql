-- =====================================================================
-- PROCEDURE: [Resource.IdsAndNamesForStandupTrainingInstanceSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Resource.IdsAndNamesForStandupTrainingInstanceSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Resource.IdsAndNamesForStandupTrainingInstanceSelectList]
GO

/*

Returns a recordset of resource ids and names to populate
"attach resource" select list for a standup training instance.

*/

CREATE PROCEDURE [Resource.IdsAndNamesForStandupTrainingInstanceSelectList]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@idStandupTrainingInstance	INT,
	@searchParam				NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT
			R.idResource, 
			CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name
		FROM tblResource R
		LEFT JOIN tblResourceLanguage RL ON RL.idResource = R.idResource AND RL.idLanguage = @idCallerLanguage
		WHERE R.idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		AND R.dtDeleted IS NULL
		AND NOT EXISTS (SELECT 1 FROM tblResourceToObjectLink ROL
						WHERE ROL.idObject = @idStandupTrainingInstance
						AND ROL.objectType = 'standuptraining'
						AND ROL.idResource = R.idResource
						AND ROL.idSite = @idCallerSite	)
		ORDER BY name

		END

	ELSE

		BEGIN

		SELECT DISTINCT
			R.idResource, 
			CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name
		FROM tblResourceLanguage RL
		INNER JOIN CONTAINSTABLE(tblResourceLanguage, *, @searchParam) K ON K.[key] = RL.idResourceLanguage AND RL.idLanguage = @idCallerLanguage
		LEFT JOIN tblResource R ON R.idResource = RL.idResource
		WHERE R.idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		AND R.dtDeleted IS NULL
		AND NOT EXISTS (SELECT 1 FROM tblResourceToObjectLink ROL
						WHERE ROL.idObject = @idStandupTrainingInstance
						AND ROL.objectType = 'standuptraining'
						AND ROL.idResource = R.idResource
						AND ROL.idSite = @idCallerSite	)
		ORDER BY name

		END
		
	SET @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	