-- =====================================================================
-- PROCEDURE: [Course.GetFeedMessages]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.GetFeedMessages]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.GetFeedMessages]
GO

/*

Gets wall feed messages for a course.

*/

CREATE PROCEDURE [Course.GetFeedMessages]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idCourse						INT,
	@pageSize						INT				= 10,
	@dtQuery						DATETIME,
	@getMessagesNewerThanDtQuery	BIT				= 0, -- default if not specified
	@getApprovedMessagesOnly		BIT				= 1, -- default if not specified
	@dtLastRecord					DATETIME		OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/* if dtQuery is not specified use utc now */

	IF (@dtQuery IS NULL)
		BEGIN
		SET @dtQuery = GETUTCDATE()
		END
		
	/* get the data */

	SELECT TOP (@pageSize)
		CFM.idCourseFeedMessage,
		CFM.idCourse,
		CFM.idAuthor,
		CFM.[message],
		CFM.[timestamp],
		CFM.dtApproved,
		CFM.idApprover,
		CASE WHEN CFM.isApproved IS NULL THEN 0 ELSE CFM.isApproved END AS isApproved,
		CASE WHEN CFM.idAuthor > 1 THEN A.firstName + ' ' + A.lastName ELSE '##administrator##' END AS authorName,
		A.avatar,
		A.gender
	INTO #CourseFeedMessages
	FROM tblCourseFeedMessage CFM
	LEFT JOIN tblUser A ON A.idUser = CFM.idAuthor
	WHERE CFM.idCourse = @idCourse
	AND CFM.idParentCourseFeedMessage IS NULL
	AND	(
			(@getApprovedMessagesOnly IS NULL OR @getApprovedMessagesOnly = 0)
			OR
			(@getApprovedMessagesOnly IS NOT NULL AND @getApprovedMessagesOnly = 1 AND CFM.isApproved = 1)
		)
	AND (
			(@getMessagesNewerThanDtQuery = 0 AND CFM.[timestamp] < @dtQuery)
			OR
			(@getMessagesNewerThanDtQuery = 1 AND CFM.[timestamp] > @dtQuery)
		)
	ORDER BY 
		CASE WHEN @getMessagesNewerThanDtQuery = 0 THEN [timestamp] ELSE '' END DESC,
		CASE WHEN @getMessagesNewerThanDtQuery = 1 THEN [timestamp] ELSE '' END ASC

	/* 
	
	get the timestamp of the last record, use min when getting records older than dtQuery
	and max when getting records newer than dtQuery

	*/

	IF (SELECT COUNT(1) FROM #CourseFeedMessages) > 0

		BEGIN

		IF (@getMessagesNewerThanDtQuery = 0)
			BEGIN
			SELECT @dtLastRecord = MIN([timestamp]) FROM #CourseFeedMessages
			END
		ELSE
			BEGIN
			SELECT @dtLastRecord = MAX([timestamp]) FROM #CourseFeedMessages
			END

		END

	ELSE

		BEGIN
		SET @dtLastRecord = @dtQuery
		END

	/* return the data */

	IF (@getMessagesNewerThanDtQuery = 0)
		BEGIN

		SELECT 
			* 
		FROM #CourseFeedMessages
		ORDER BY [timestamp] DESC

		END

	ELSE
		BEGIN

		SELECT 
			* 
		FROM #CourseFeedMessages
		ORDER BY [timestamp] ASC

		END

	/* drop the temp table */

	DROP TABLE #CourseFeedMessages

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO