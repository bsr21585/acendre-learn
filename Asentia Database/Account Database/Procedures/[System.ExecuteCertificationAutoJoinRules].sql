-- =====================================================================
-- PROCEDURE: [System.ExecuteCertificationAutoJoinRules]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ExecuteCertificationAutoJoinRules]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ExecuteCertificationAutoJoinRules]
GO

/*

Executes the Auto-Join Rules for Certifications.

*/
CREATE PROCEDURE [System.ExecuteCertificationAutoJoinRules]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,
	@Certifications				IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* GET UTC TIMESTAMP */
	DECLARE @dtExecuted DATETIME
	SET @dtExecuted = GETUTCDATE()

	/* 
		
	Create temporary table to store user-to-certification joins resulting from rules execution.
	
	*/

	CREATE TABLE #UserToCertificationMatches
	(
		idSite				INT,
		idRuleSet			INT,
		idCertification		INT,
		idUser				INT
	)
	
	/*

	Get the ruleset to certification matches, and place them in the temporary table.

	*/

	INSERT INTO #UserToCertificationMatches
	EXEC [System.GetRuleSetCertificationMatches] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @Certifications, @Filters, @filterBy
	
	/*

	Join users that need to be joined, remove users that need to be removed.

	*/	

	/* PUT RECORDS THAT ARE GOING TO BE JOINED INTO A TEMP TABLE SO WE CAN SEND NOTIFICATIONS */
	DECLARE @newUserToCertificationJoinsForEventLog TABLE (idCertification INT, idUser INT)

	INSERT INTO @newUserToCertificationJoinsForEventLog (
		idCertification,
		idUser
	)
	SELECT
		UCM.idCertification,
		UCM.idUser
	FROM #UserToCertificationMatches UCM
	WHERE NOT EXISTS (SELECT 1 FROM tblCertificationToUserLink CUL WHERE CUL.idUser = UCM.idUser AND CUL.idCertification = UCM.idCertification)
				  
	/* ADD RECORDS TO CERTIFICATION TO USER LINK */
	INSERT INTO tblCertificationToUserLink
	(
		idSite,
		idCertification,
		idUser,
		certificationTrack,
		idRuleSet	
	)
	SELECT
		C.idSite,
		UCM.idCertification,
		UCM.idUser,
		0,
		UCM.idRuleSet
	FROM #UserToCertificationMatches UCM
	LEFT JOIN tblCertification C ON C.idCertification = UCM.idCertification
	WHERE NOT EXISTS (SELECT 1 FROM tblCertificationToUserLink CUL
					  WHERE CUL.idUser = UCM.idUser
					  AND CUL.idCertification = UCM.idCertification)

	/*

	do the event log entries for joins

	*/

	DECLARE @eventLogJoinedItems EventLogItemObjects

	INSERT INTO @eventLogJoinedItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		CUL.idSite,
		UCJ.idCertification,
		CUL.idCertificationToUserLink, 
		UCJ.idUser
	FROM @newUserToCertificationJoinsForEventLog UCJ
	LEFT JOIN tblCertificationToUserLink CUL ON CUL.idCertification = UCJ.idCertification AND CUL.idUser = UCJ.idUser

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 601, @dtExecuted, @eventLogJoinedItems
		
	/* REMOVE RECORDS FROM CERTIFICATION TO USER LINK AND IT'S DATA TABLE */

	/*

	do the event log entries for revocations first

	*/

	DECLARE @eventLogRevokedItems EventLogItemObjects

	INSERT INTO @eventLogRevokedItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		CUL.idSite,
		CUL.idCertification,
		CUL.idCertificationToUserLink, 
		CUL.idUser
	FROM tblCertificationToUserLink CUL
	WHERE idCertificationToUserLink IN (
								SELECT
									CUL.idCertificationToUserLink
								FROM tblCertificationToUserLink CUL
								WHERE NOT EXISTS (SELECT 1 FROM #UserToCertificationMatches UCM
												  WHERE UCM.idUser = CUL.idUser
												  AND UCM.idCertification = CUL.idCertification)
								AND CUL.idRuleSet IS NOT NULL
								AND (
										((SELECT COUNT(1) FROM @Filters) = 0)
										OR
										(@filterBy = 'group' AND CUL.idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT id FROM @Filters)))
										OR
										(@filterBy = 'user' AND CUL.idUser IN (SELECT DISTINCT id FROM @Filters))
									)									
							   )

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 603, @dtExecuted, @eventLogRevokedItems

	/* NOW, delete the records */

	DELETE FROM [tblData-CertificationModuleRequirement]
	WHERE idCertificationToUserLink IN (
								SELECT
									CUL.idCertificationToUserLink
								FROM tblCertificationToUserLink CUL
								WHERE NOT EXISTS (SELECT 1 FROM #UserToCertificationMatches UCM
												  WHERE UCM.idUser = CUL.idUser
												  AND UCM.idCertification = CUL.idCertification)
								AND CUL.idRuleSet IS NOT NULL
								AND (
										((SELECT COUNT(1) FROM @Filters) = 0)
										OR
										(@filterBy = 'group' AND CUL.idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT id FROM @Filters)))
										OR
										(@filterBy = 'user' AND CUL.idUser IN (SELECT DISTINCT id FROM @Filters))
									)
							   )

	DELETE FROM tblCertificationToUserLink
	WHERE idCertificationToUserLink IN (
								SELECT
									CUL.idCertificationToUserLink
								FROM tblCertificationToUserLink CUL
								WHERE NOT EXISTS (SELECT 1 FROM #UserToCertificationMatches UCM
												  WHERE UCM.idUser = CUL.idUser
												  AND UCM.idCertification = CUL.idCertification)
								AND CUL.idRuleSet IS NOT NULL
								AND (
										((SELECT COUNT(1) FROM @Filters) = 0)
										OR
										(@filterBy = 'group' AND CUL.idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT id FROM @Filters)))
										OR
										(@filterBy = 'user' AND CUL.idUser IN (SELECT DISTINCT id FROM @Filters))
									)
							   )

	/* DROP TEMPORARY TABLES */
	DROP TABLE #UserToCertificationMatches

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO