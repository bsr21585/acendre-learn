-- =====================================================================
-- PROCEDURE: [ExceptionLog.Add]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[ExceptionLog.Add]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [ExceptionLog.Add]
GO

/*
Adds new entry to exception log.
*/
CREATE PROCEDURE [ExceptionLog.Add]
(
	@Return_Code				INT					OUTPUT,
	@Error_Description_Code		NVARCHAR(50)		OUTPUT,
	@idCallerSite				INT					= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT					= 0,

	@idExceptionLog				INT					OUTPUT,
	@idSite						INT,
	@idUser						INT,
	@page						NVARCHAR(512),
	@exceptionType				NVARCHAR(512),
	@exceptionMessage			NVARCHAR(MAX),
	@exceptionStackTrace		NVARCHAR(MAX)
)
AS
	BEGIN
		SET NOCOUNT ON
	
			-- insert the exception log record
			INSERT INTO tblExceptionLog
				(
				idSite,
				idUser,
				[timestamp],
				page,
				exceptionType,
				exceptionMessage,
				exceptionStackTrace
				)
			VALUES
				(
				@idSite,
				@idUser,
				GETUTCDATE(),
				@page,
				@exceptionType,
				@exceptionMessage,
				@exceptionStackTrace
				)
			
			-- get the new exception log record's id and return successfully
			SELECT @idExceptionLog = SCOPE_IDENTITY()
			SELECT @Return_Code = 0 --(0 is 'success')
			RETURN
	END
			
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO