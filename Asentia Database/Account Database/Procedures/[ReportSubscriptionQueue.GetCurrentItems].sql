-- =====================================================================
-- PROCEDURE: [ReportSubscriptionQueue.GetCurrentItems]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportSubscriptionQueue.GetCurrentItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportSubscriptionQueue.GetCurrentItems]
GO

/*

Gets the current (not yet sent) items in the report subscription queue.

*/

CREATE PROCEDURE [ReportSubscriptionQueue.GetCurrentItems]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idSite					INT,
	@numItems				INT
)
AS

	BEGIN
	SET NOCOUNT ON

	IF (@numItems IS NULL)
		BEGIN
		SET @numItems = 5 -- default is 5
		END

	SELECT TOP (@numItems)
		RSQ.idReportSubscriptionQueue,
		RSQ.idSite,
		S.hostname,
		SL.code AS siteDefaultLanguageString,
		RSQ.idReportSubscription,
		RSQ.idReport,
		RSQ.idDataset,
		R.fields AS [fields],
		R.filter AS filter,
		R.[order] AS [order],
		RSQ.reportName,
		RSQ.idRecipient,
		RS.token AS recipientSubscriptionToken,
		RSQ.recipientLangString,
		RSQ.recipientFullName,
		RSQ.recipientFirstName,
		RSQ.recipientLogin,
		RSQ.recipientEmail,
		RSQ.recipientTimezone,
		RSQ.recipientTimezoneDotNetName,
		CASE WHEN SP1.value IS NOT NULL AND SP1.value <> '' THEN SP1.value ELSE NULL END AS systemFromAddressOverride,
		CASE WHEN SP2.value IS NOT NULL AND SP2.value <> '' THEN SP2.value ELSE NULL END AS systemSmtpServerNameOverride,
		CASE WHEN SP3.value IS NOT NULL AND SP3.value <> '' THEN SP3.value ELSE NULL END AS systemSmtpServerPortOverride,
		CASE WHEN SP4.value IS NOT NULL AND SP4.value <> '' THEN SP4.value ELSE NULL END AS systemSmtpServerUseSslOverride,
		CASE WHEN SP5.value IS NOT NULL AND SP5.value <> '' THEN SP5.value ELSE NULL END AS systemSmtpServerUsernameOverride,
		CASE WHEN SP6.value IS NOT NULL AND SP6.value <> '' THEN SP6.value ELSE NULL END AS systemSmtpServerPasswordOverride,
		RSQ.[priority],
		RSQ.dtAction,
		RSQ.dtSent,
		RSQ.statusDescription
	FROM tblReportSubscriptionQueue RSQ
	LEFT JOIN tblReportSubscription RS ON RS.idReportSubscription = RSQ.idReportSubscription
	LEFT JOIN tblReport R ON R.idReport = RSQ.idReport
	LEFT JOIN tblSite S ON S.idSite = RSQ.idSite
	LEFT JOIN tblSiteParam SP1 ON SP1.idSite = 1 AND SP1.[param] = 'System.EmailFromAddressOverride'
	LEFT JOIN tblSiteParam SP2 ON SP2.idSite = 1 AND SP2.[param] = 'System.EmailSmtpServerNameOverride'
	LEFT JOIN tblSiteParam SP3 ON SP3.idSite = 1 AND SP3.[param] = 'System.EmailSmtpServerPortOverride'
	LEFT JOIN tblSiteParam SP4 ON SP4.idSite = 1 AND SP4.[param] = 'System.EmailSmtpServerUseSslOverride'
	LEFT JOIN tblSiteParam SP5 ON SP5.idSite = 1 AND SP5.[param] = 'System.EmailSmtpServerUsernameOverride'
	LEFT JOIN tblSiteParam SP6 ON SP6.idSite = 1 AND SP6.[param] = 'System.EmailSmtpServerPasswordOverride'
	LEFT JOIN tblLanguage SL ON SL.idLanguage = S.idLanguage
	WHERE RSQ.dtAction <= GETUTCDATE()
	AND RSQ.dtSent IS NULL
	AND RSQ.statusDescription IS NULL
	AND (
			@idSite IS NULL	-- all sites
			OR
			RSQ.idSite = @idSite -- just a specified site
		)
	AND R.idReport IS NOT NULL -- report still exists and at least the fields are intact 
	AND R.fields IS NOT NULL
	ORDER BY RSQ.dtAction ASC

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO