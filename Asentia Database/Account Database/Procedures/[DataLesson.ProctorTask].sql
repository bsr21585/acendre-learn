-- =====================================================================
-- PROCEDURE: [DataLesson.ProctorTask]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DataLesson.ProctorTask]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.ProctorTask]
GO

/*

Updates completion, success, and score for a learner's task.
Used when a proctor grades a learner's task.

*/

CREATE PROCEDURE [DataLesson.ProctorTask]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT				= 0,
	
	@idEnrollment					INT,
	@idDataLesson					INT,
	@idDataTask						INT,
	@completionStatus				INT,
	@successStatus					INT,
	@score							INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that the lesson data exists and is not completed

	*/

	IF (
		SELECT COUNT(1)
		FROM [tblData-Lesson]
		WHERE [idData-Lesson] = @idDataLesson
		AND idSite = @idCallerSite
		AND dtCompleted IS NULL
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataLessonPT_NoLessonDataRecordFound'
		RETURN 1
	    END

	/*

	validate that the task data exists

	*/

	IF (
		SELECT COUNT(1)
		FROM [tblData-HomeworkAssignment]
		WHERE [idData-HomeworkAssignment] = @idDataTask
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataLessonPT_NoTaskRecordFound'
		RETURN 1
	    END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	update the task data

	*/

	UPDATE [tblData-HomeworkAssignment] SET
		completionStatus = @completionStatus,
		successStatus = @successStatus,
		score = @score,
		[timestamp] = @utcNow,
		proctoringUser = @idCaller
	WHERE [idData-Lesson] = @idDataLesson

	

	/*

	if completed and not failed, mark lesson data as completed

	*/
	
	IF (@completionStatus = 2 AND @successStatus <> 5) OR @successStatus = 4
		BEGIN
		UPDATE [tblData-Lesson] SET dtCompleted = @utcNow
		WHERE [idData-Lesson] = @idDataLesson
		AND idSite = @idCallerSite 
		END

	/*

	do event log entry for completed, passed, or failed

	*/

	DECLARE @eventLogItem EventLogItemObjects
	DECLARE @idLesson INT
	DECLARE @idUser INT
	DECLARE @idEventType INT

	SELECT
		@idLesson = LD.idLesson,
		@idUser = E.idUser
	FROM [tblData-Lesson] LD
	LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
	WHERE LD.[idData-Lesson] = @idDataLesson

	INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idLesson, @idDataLesson, @idUser

	IF (@completionStatus = 2 AND @successStatus <> 5 AND @successStatus <> 4)
		BEGIN
		SET @idEventType = 303 -- completed
		END

	IF (@successStatus = 4)
		BEGIN
		SET @idEventType = 301 -- passed
		END

	IF (@successStatus = 5)
		BEGIN
		SET @idEventType = 302 -- failed
		END

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, @idEventType, @utcNow, @eventLogItem

	/*

	check for course completion

	*/

	EXEC [Enrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idEnrollment

	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

