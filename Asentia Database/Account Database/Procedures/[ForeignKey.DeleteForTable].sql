-- =====================================================================
-- PROCEDURE: [ForeignKey.DeleteFromTable]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[ForeignKey.DeleteFromTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [ForeignKey.DeleteFromTable]
GO

/*
Adds new user or updates existing user.
*/
CREATE PROCEDURE [ForeignKey.DeleteFromTable] (
	@tableName NVARCHAR(255)
)
AS
SELECT name
FROM sys.objects
WHERE type_desc = 'FOREIGN_KEY_CONSTRAINT'
AND name like '%_' + @tableName + '_%'

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

