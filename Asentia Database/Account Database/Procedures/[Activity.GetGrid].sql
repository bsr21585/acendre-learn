-- =====================================================================
-- PROCEDURE: [Activity.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Activity.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Activity.GetGrid]
GO

/*

Gets a listing of Activities

*/

CREATE PROCEDURE [Activity.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/
		
		-- return the rowcount
			
		SELECT COUNT(1) AS row_count 
		FROM tblActivityImport A
		WHERE 
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND A.idSite = @idCallerSite)
			)
		;WITH 
			Keys AS (
				SELECT TOP (@pageNum * @pageSize) 
					A.idActivityImport,
					ROW_NUMBER() OVER (ORDER BY
						-- ORDER DESC
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'importFileName' THEN A.importFileName END) END DESC,
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'timestamp' THEN A.[timestamp] END) END DESC,
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'performedBy' THEN PB.displayName END) END DESC,
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'performedFor' THEN PF.displayName END) END DESC,

						-- ORDER ASC
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'importFileName' THEN A.importFileName END) END,
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'timestamp' THEN A.[timestamp] END) END,
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'performedBy' THEN PB.displayName END) END,
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'performedFor' THEN PF.displayName END) END
					)
					AS [row_number]

				FROM tblActivityImport A
						LEFT JOIN tblUser PB ON PB.idUser = A.idUser
						LEFT JOIN tblUser PF ON PF.idUser = A.idUserImported
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND A.idSite = @idCallerSite)
					)
			),
				
			SelectedKeys AS (
				SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
					idActivityImport, 
					[row_number]
				FROM Keys
				WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
			)
		SELECT 
			A.idActivityImport AS idActivityImport,
			A.importFileName AS importFileName,
			A.[timestamp] AS [timestamp],
			CASE WHEN A.idUser = 1 THEN 'Administrator' ELSE PB.displayName END AS performedBy,
			CASE WHEN A.idUserImported = 1 THEN 'Administrator' ELSE PF.displayName END AS performedFor,
			CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
			SelectedKeys.[row_number]
		FROM SelectedKeys
		JOIN tblActivityImport A ON A.idActivityImport = SelectedKeys.idActivityImport
		LEFT JOIN tblUser PB ON PB.idUser = A.idUser
		LEFT JOIN tblUser PF ON PF.idUser = A.idUserImported
		ORDER BY SelectedKeys.[row_number]				
		
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO