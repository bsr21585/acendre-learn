-- =====================================================================
-- PROCEDURE: [EnrollmentRequest.IsEnrollmentRequestPending]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EnrollmentRequest.IsEnrollmentRequestPending]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EnrollmentRequest.IsEnrollmentRequestPending]
GO

/*

Determines if there is a pending enrollment request for the course for the user.

*/

CREATE PROCEDURE [EnrollmentRequest.IsEnrollmentRequestPending]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@isEnrollmentRequestPending	BIT				OUTPUT,
	@idCourse					INT,
	@idUser						INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	Is there a pending enrollment?
	
	*/
	
	IF(SELECT COUNT(idEnrollmentRequest) FROM tblEnrollmentRequest WHERE idSite = @idCallerSite AND idUser = @idUser AND idCourse = @idCourse AND dtApproved IS NULL AND dtDenied IS NULL AND idApprover IS NULL) > 0
		SET @isEnrollmentRequestPending = 1
	ELSE
		SET @isEnrollmentRequestPending = 0
	
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO