-- =====================================================================
-- PROCEDURE: [DataLesson.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataLesson.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.GetGrid]
GO

/*

Gets a listing of lesson data for a specified enrollment.

*/

CREATE PROCEDURE [DataLesson.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	-- NOTE THAT WE ARE NOT USING THE SEARCH, PAGINATION, OR SORT HERE BUT
	-- SINCE THESE ARE COMMON PARAMETERS FOR GRIDS, WE NEED TO KEEP THEM
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,
	
	@idEnrollment			INT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	-- get the force lesson completion in order setting for the course

	DECLARE @idCourse INT
	DECLARE @forceLessonCompletionInOrder BIT
		
	SELECT @idCourse = idCourse FROM tblEnrollment WHERE idEnrollment = @idEnrollment
	SELECT @forceLessonCompletionInOrder = CASE WHEN forceLessonCompletionInOrder IS NOT NULL THEN forceLessonCompletionInOrder ELSE 0 END FROM tblCourse WHERE idCourse = @idCourse

	/*

	begin getting the grid data

	*/

	-- return the rowcount

	SELECT COUNT(1) AS row_count
	FROM [tblData-Lesson] LD
	WHERE LD.idEnrollment = @idEnrollment

	-- get the grid data

	SELECT
		LD.[idData-Lesson],
		DSCO.[idData-SCO],
		DHWA.[idData-HomeworkAssignment] AS [idData-Task],
		STIUL.idStandupTrainingInstanceToUserLink,
		LD.idLesson,
		CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE LD.title END AS title,
		L.[order],
		CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 1 AND LCL.idLesson = L.idLesson) = 1 THEN '1' ELSE '' END +
		CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson = L.idLesson) = 1 THEN '2' ELSE '' END +
		CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 3 AND LCL.idLesson = L.idLesson) = 1 THEN '3' ELSE '' END +
		CASE WHEN (SELECT 1 FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 4 AND LCL.idLesson = L.idLesson) = 1 THEN '4' ELSE '' END AS contentTypes,
		LD.dtCompleted,
		CASE WHEN (SELECT COUNT(1) FROM tblLessonToContentLink WHERE idLesson = LD.idLesson) > 1 THEN 1 ELSE 0 END AS hasMultipleContentMethods,
		CASE WHEN LCLCP.idObject IS NOT NULL THEN 1 ELSE 0 END AS hasContentPackage,
		CASE WHEN LCLST.idObject IS NOT NULL THEN 1 ELSE 0 END AS hasStandupTraining,
		CASE WHEN LCLHWA.idAssignmentDocumentType IS NOT NULL THEN 1 ELSE 0 END AS hasTask,
		CASE WHEN LCLOJT.idObject IS NOT NULL THEN 1 ELSE 0 END AS hasOJT,
		LD.contentTypeCommittedTo,
		LCLCP.idObject AS idContentPackage,
		CP.idContentPackageType AS contentPackageType,
		LCLOJT.idObject AS idContentPackageOJT,
		CPOJT.idContentPackageType AS contentPackageTypeOJT,
		--LCLST.idObject AS idStandupTraining,
		--STIUL.idStandupTrainingInstance,
		--LCLHWA.idAssignmentDocumentType AS idTaskDocumentType,
		--LCLHWA.assignmentResourcePath AS taskResourcePath,
		DHWA.uploadedAssignmentFilename AS uploadedTaskFilename,
		DHWA.dtUploaded AS taskUploadedDate,
		CASE WHEN (LD.contentTypeCommittedTo = 0 AND LD.dtCompleted IS NOT NULL) THEN -- for administrator override
			'completed'
		ELSE
			CASE WHEN (DSCO.completionStatus IS NULL OR DSCO.completionStatus = 0) 
						AND (STIUL.completionStatus IS NULL OR STIUL.completionStatus = 0) 
						AND (DHWA.completionStatus IS NULL OR DHWA.completionStatus = 0) THEN
				'unknown'
			ELSE
				CASE WHEN DSCO.completionStatus = 2 THEN
					'completed'
				ELSE
					CASE WHEN STIUL.completionStatus = 2 THEN
						'completed'
					ELSE
						CASE WHEN DHWA.completionStatus = 2 THEN
							'completed'
						ELSE
							CASE WHEN DSCO.completionStatus = 3 THEN
								'incomplete'
							ELSE
								CASE WHEN STIUL.completionStatus = 3 THEN
									'incomplete'
								ELSE
									CASE WHEN DHWA.completionStatus = 3 THEN
										'incomplete'
									ELSE
										'unknown'
									END
								END
							END
						END
					END
				END
			END
		END AS contentCompletionStatus,
		CASE WHEN (DSCO.successStatus IS NULL OR DSCO.successStatus = 0) 
					AND (STIUL.successStatus IS NULL OR STIUL.successStatus = 0) 
					AND (DHWA.successStatus IS NULL OR DHWA.successStatus = 0) THEN
			'unknown'
		ELSE
			CASE WHEN DSCO.successStatus = 4 THEN
				'passed'
			ELSE
				CASE WHEN STIUL.successStatus = 4 THEN
					'passed'
				ELSE
					CASE WHEN DHWA.successStatus = 4 THEN
						'passed'
					ELSE
						CASE WHEN DSCO.successStatus = 5 THEN
							'failed'
						ELSE
							CASE WHEN STIUL.successStatus = 5 THEN
								'failed'
							ELSE
								CASE WHEN DHWA.successStatus = 5 THEN
									'failed'
								ELSE
									'unknown'
								END
							END
						END
					END
				END
			END
		END AS contentSuccessStatus,
		CASE WHEN (DSCO.completionStatus IS NULL OR DSCO.completionStatus = 0)				-- WE NEED TO BASE SCORE OFF OF WHICH CONTENT RECORDED A COMPLETION STATUS
					AND (STIUL.completionStatus IS NULL OR STIUL.completionStatus = 0)		-- SO THAT WE KNOW WHICH CONTENT THE LEARNER HAS RECORDED A SCORE FOR
					AND (DHWA.completionStatus IS NULL OR DHWA.completionStatus = 0) THEN
			NULL
		ELSE
			CASE WHEN DSCO.completionStatus = 2 THEN
				CASE WHEN DSCO.scoreScaled IS NOT NULL THEN
					ROUND(DSCO.scoreScaled * 100, 0)
				ELSE
					NULL
				END
			ELSE
				CASE WHEN STIUL.completionStatus = 2 THEN
					CASE WHEN STIUL.score IS NOT NULL THEN
						ROUND(STIUL.score, 0)
					ELSE
						NULL
					END
				ELSE
					CASE WHEN DHWA.completionStatus = 2 THEN
						CASE WHEN DHWA.score IS NOT NULL THEN
							ROUND(DHWA.score, 0)
						ELSE
							NULL
						END
					ELSE
						CASE WHEN DSCO.completionStatus = 3 THEN
							CASE WHEN DSCO.scoreScaled IS NOT NULL THEN
								ROUND(DSCO.scoreScaled * 100, 0)
							ELSE
								NULL
							END
						ELSE
							CASE WHEN STIUL.completionStatus = 3 THEN
								CASE WHEN STIUL.score IS NOT NULL THEN
									ROUND(STIUL.score, 0)
								ELSE
									NULL
								END
							ELSE
								CASE WHEN DHWA.completionStatus = 3 THEN
									CASE WHEN DHWA.score IS NOT NULL THEN
										ROUND(DHWA.score, 0)
									ELSE
										NULL
									END
								ELSE
									NULL
								END
							END
						END
					END
				END
			END
		END AS contentScore,
		DSCO.effectiveAttemptCount AS attemptCount,
		DSCO.totalTime AS contentPackageTotalTime,
		CASE WHEN LD.dtCompleted IS NOT NULL THEN
			CONVERT(BIT, 1)
		ELSE
			CASE WHEN (LD.contentTypeCommittedTo IS NOT NULL) OR 
					  (DSCO.[timestamp] IS NOT NULL) -- AND DSCO.totalTime > 0
			THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END
		END AS isResetOn,
		CASE WHEN @forceLessonCompletionInOrder = 1 THEN -- use for "status" if ever implemented
				CONVERT(BIT, [dbo].[EvaluateLessonOrderLaunchLock](LD.[idData-Lesson]))
			ELSE
				CONVERT(BIT, 1)
			END AS [allowLaunch],
		ROW_NUMBER() OVER (ORDER BY L.[order]) AS [row_number]
	FROM [tblData-Lesson] LD
	LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson
	LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
	LEFT JOIN tblLessonToContentLink LCLCP ON LCLCP.idLesson = L.idLesson AND LCLCP.idContentType = 1
	LEFT JOIN tblContentPackage CP ON CP.idContentPackage = LCLCP.idObject
	LEFT JOIN tblLessonToContentLink LCLST ON LCLST.idLesson = L.idLesson AND LCLST.idContentType = 2
	LEFT JOIN tblLessonToContentLink LCLHWA ON LCLHWA.idLesson = L.idLesson AND LCLHWA.idContentType = 3
	LEFT JOIN tblLessonToContentLink LCLOJT ON LCLOJT.idLesson = L.idLesson AND LCLOJT.idContentType = 4
	LEFT JOIN tblContentPackage CPOJT ON CPOJT.idContentPackage = LCLOJT.idObject
	LEFT JOIN [tblData-SCO] DSCO ON DSCO.[idData-Lesson] = LD.[idData-Lesson]
	LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandupTrainingInstance = (
																								SELECT TOP 1 STIUL2.idStandupTrainingInstance 
																								FROM tblStandUpTrainingInstanceToUserLink STIUL2 
																								LEFT JOIN tblStandUpTrainingInstance STI2 ON STIUL2.idStandupTrainingInstance = STI2.idStandUpTrainingInstance
																								LEFT JOIN (SELECT idStandupTrainingInstance, MIN(dtStart) AS dtStart FROM tblStandUpTrainingInstanceMeetingTime GROUP BY idStandUpTrainingInstance) STIMT ON STIMT.idStandUpTrainingInstance = STIUL2.idStandUpTrainingInstance
																								WHERE STI2.idStandupTraining = LCLST.idObject 
																								AND STIUL2.idUser = E.idUser
																								-- where the standup training instance start
																								-- is within 30 days prior to the enrollment start
																								AND STIMT.dtStart >= DATEADD(d, -30, E.dtStart)
																							   ) 
																							   AND STIUL.idUser = E.idUser
	LEFT JOIN [tblData-HomeworkAssignment] DHWA ON DHWA.[idData-Lesson] = LD.[idData-Lesson]
	WHERE LD.idEnrollment = @idEnrollment
	ORDER BY L.[order]

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO