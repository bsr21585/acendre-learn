-- =====================================================================
-- PROCEDURE: [System.GetLoggedInUsersNewMessageCount]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF exists (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetLoggedInUsersNewMessageCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetLoggedInUsersNewMessageCount]
GO

/*

Returns the number of new messages the logged in user has.

*/

CREATE PROCEDURE [System.GetLoggedInUsersNewMessageCount]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@newMessageCount		INT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*

	get the message count

	*/

	SELECT @newMessageCount = COUNT(1) FROM tblInboxMessage 
	WHERE idRecipient = @idCaller
	AND idSite = @idCallerSite
	AND isSent = 1
	AND (isRead = 0 OR isRead IS NULL)
	AND (isRecipientDeleted IS NULL OR isRecipientDeleted = 0) 
	AND (isSenderDeleted IS NULL OR isSenderDeleted = 0)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO