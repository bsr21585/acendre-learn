-- =====================================================================
-- PROCEDURE: [Resource.Delete]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Resource.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Resource.Delete]
GO

/*

Deletes a Resource

*/

CREATE PROCEDURE [dbo].[Resource.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (50)	OUTPUT, 
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	@Resources					IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Resources) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'ResourceDelete_NoRecordsSelected'
		RETURN 1
		END
			
	/*
	
	validate that all Resources exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Resources CC
		LEFT JOIN tblResource C ON C.idResource = CC.id
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'ResourceDelete_NoRecordFound'			
		RETURN 1 
		END
			
	/*
	
	Delete the object links
	
	*/
	
	DELETE FROM tblResourceToObjectLink
	WHERE idResource IN (
		SELECT id
		FROM @Resources
	)
	
	/*
	
	Delete the language records 
	
	*/
	
	DELETE FROM tblResourceLanguage
	WHERE idResource IN (
		SELECT id
		FROM @Resources
	)
	
	/*
	
	Delete the Resource itself
	
	*/
	
	UPDATE tblResource SET
		isDeleted = 1,
		dtDeleted = GETUTCDATE()
	WHERE idResource IN (
		SELECT id
		FROM @Resources
	)
	
	SELECT @Return_Code = 0
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO