-- =====================================================================
-- PROCEDURE: [LearningPath.IdsAndNamesForSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.IdsAndNamesForSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.IdsAndNamesForSelectList]
GO

/*

Returns a recordset of course ids and names to populate a select list with no exclusions for a specific object.

*/

CREATE PROCEDURE [LearningPath.IdsAndNamesForSelectList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT
			L.idLearningPath, 
			CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE L.name END AS title
		FROM tblLearningPath L
		LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = L.idLearningPath AND LPL.idLanguage = @idCallerLanguage
		WHERE L.idSite = @idCallerSite
		AND (L.isDeleted IS NULL OR L.isDeleted = 0)
		ORDER BY title

		END
			
	ELSE

		BEGIN

		SELECT DISTINCT
			L.idLearningPath, 
			CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE L.name END AS title
		FROM tblLearningPathLanguage LPL
		INNER JOIN CONTAINSTABLE(tblLearningPathLanguage, *, @searchParam) K ON K.[key] = LPL.idLearningPathLanguage AND LPL.idLanguage = @idCallerLanguage
		LEFT JOIN tblLearningPath L ON L.idLearningPath = LPL.idLearningPath
		WHERE L.idSite = @idCallerSite
	    AND (L.isDeleted IS NULL OR L.isDeleted = 0)
		ORDER BY title

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO