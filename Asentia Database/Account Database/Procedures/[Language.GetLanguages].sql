-- =====================================================================
-- PROCEDURE: [Language.GetLanguages]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT
		*
	FROM dbo.sysobjects
	WHERE id = OBJECT_ID(N'[Language.GetLanguages]')
	AND OBJECTPROPERTY(id, N'IsProcedure') = 1) DROP PROCEDURE [Language.GetLanguages]
GO

/*
Return all available languages for a site. 
*/
CREATE PROCEDURE [Language.GetLanguages]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0
	
)
AS
	BEGIN
SET NOCOUNT ON


/*
Get all available languages
*/
SELECT 
		L.code as [langString],
		L.name 
	        from  tblLanguage  L

    SET @Return_Code = 0
	SET @Error_Description_Code = ''
	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO