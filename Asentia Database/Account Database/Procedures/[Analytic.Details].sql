-- =====================================================================
-- PROCEDURE: [Analytic.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Analytic.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Analytic.Details]
GO

/*

Return all the properties for a given Analytic id.

*/

CREATE PROCEDURE [Analytic.Details]
(
	@Return_Code		INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite		INT					= 0, --default if not specified
	@callerLangString	NVARCHAR (10),
	@idCaller			INT					= 0,
	
	@idAnalytic			INT					OUTPUT, 
	@idSite				INT					OUTPUT, 
	@idUser				INT					OUTPUT, 
	@idDataset			INT					OUTPUT, 
	@title				NVARCHAR(255)		OUTPUT,
	@idChartType		INT 		        OUTPUT,		
	@frequency          INT					OUTPUT,	
	@excludeDays        NVARCHAR(255)		OUTPUT,	
	@isPublic			BIT					OUTPUT,
	@dtStart			DATETIME            OUTPUT,
	@dtEnd				DATETIME            OUTPUT,
	@dtCreated			DATETIME			OUTPUT,
	@dtModified			DATETIME			OUTPUT,
	@isDeleted			BIT					OUTPUT,
	@dtDeleted			DATETIME			OUTPUT,
	@activity           INT                 OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAnalytic
		WHERE idAnalytic = @idAnalytic
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'AnalyticDetails_NoRecordFound'
		RETURN 1
		END

	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
	
	
	/*
	
	get the data
	
	*/
	
	SELECT
		@idAnalytic			= A.idAnalytic,
		@idSite				= A.idSite,
		@idUser				= A.idUser, 
		@idDataset			= A.idDataset, 		
		@title				= CASE WHEN AL.title IS NULL OR AL.title = '' THEN A.title ELSE AL.title END,
		@idChartType		= A.idChartType,		
		@frequency          = A.frequency,					
		@excludeDays        = A.excludeDays,
		@isPublic			= A.isPublic,
		@dtStart			= A.dtStart,
		@dtEnd			    = A.dtEnd,
		@dtCreated			= A.dtCreated,
		@dtModified			= A.dtModified,
		@isDeleted			= A.isDeleted,
		@dtDeleted			= A.dtDeleted,
		@activity           = A.activity
	FROM tblAnalytic A
	LEFT JOIN tblAnalyticLanguage AL 
		ON AL.idAnalytic = A.idAnalytic
		AND AL.idLanguage = @idCallerLanguage
	WHERE 
		A.idAnalytic = @idAnalytic
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'AnalyticDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO