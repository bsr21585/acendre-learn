-- =====================================================================
-- PROCEDURE: [Data-TinCanProfile.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCanProfile.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCanProfile.Save]
GO

/*

Adds a new tin can profile or updates an existing tin can profile.

*/

CREATE PROCEDURE [Data-TinCanProfile.Save]
(
	@Return_Code			INT								OUTPUT,
	@Error_Description_Code	NVARCHAR(50)					OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idEndpoint				INT,
	@idData_TinCanProfile	INT								OUTPUT,
	@profileId				NVARCHAR(100),
	@activityId				NVARCHAR(100),
	@agent					NVARCHAR(MAX),
	@contentType			NVARCHAR(50),
	@docContents			VARBINARY(MAX),
	@dtUpdated				DATETIME
)
AS
	BEGIN
	SET NOCOUNT ON

	-- Get id if record exists
	SELECT @idData_TinCanProfile = [idData-TinCanProfile]
	FROM [tblData-TinCanProfile]
	WHERE idSite = @idCallerSite
	AND profileId = @profileId
	AND (@activityId IS NULL OR activityId = @activityId)
	AND (@agent IS NULL OR agent = @agent)
	
	-- Update record if already exists otherwise insert a new record.
	IF (@idData_TinCanProfile IS NULL OR @idData_TinCanProfile = 0)
		BEGIN
		INSERT INTO [tblData-TinCanProfile] (
			idSite,
			idEndpoint,
			profileId,
			activityId,
			agent,
			contentType,
			docContents,
			dtUpdated
		)
		VALUES (
			@idCallerSite,
			@idEndpoint,
			@profileId,
			@activityId,
			@agent,
			@contentType,
			@docContents,
			@dtUpdated
		)
		
		-- get the new tin can profile id and return successfully
		SELECT @idData_TinCanProfile = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
		
		/*
		
		validate caller permission to the specified record
		
		*/
		
		IF(
			SELECT COUNT(1)
			FROM [tblData-TinCanProfile]
			WHERE idSite = @idCallerSite
			AND idEndpoint = @idEndpoint
			AND profileId = @profileId
			AND (@activityId IS NULL OR activityId = @activityId)
			AND (@agent IS NULL OR agent = @agent)
			) = 0
			BEGIN
			SELECT @Return_Code = 3
			SET @Error_Description_Code = 'DataTinCanProfileSave_PermissionError'  
			RETURN 1
			END
			
		UPDATE [tblData-TinCanProfile] SET
			docContents = @docContents,
			contentType = @contentType,
			dtUpdated = @dtUpdated
		WHERE [idData-TinCanProfile] = @idData_TinCanProfile
		END
	
	SELECT @Return_Code = 0 --Success
	SELECT @Error_Description_Code = NULL
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO