-- =====================================================================
-- PROCEDURE: [WebMeetingOrganizer.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[WebMeetingOrganizer.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [WebMeetingOrganizer.Details]
GO

/*

Return all the properties for a given web meeting organizer id.

*/
CREATE PROCEDURE [WebMeetingOrganizer.Details]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idWebMeetingOrganizer		INT				OUTPUT,	
	@idSite						INT				OUTPUT, 
	@idUser						INT				OUTPUT, 
	@organizerType				INT				OUTPUT, 
	@username					NVARCHAR(255)	OUTPUT,
	@password					NVARCHAR(255)	OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/

	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblWebMeetingOrganizer
		WHERE idWebMeetingOrganizer = @idWebMeetingOrganizer
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'WebMeetingOrganizerDetails_NoRecordFound'
		RETURN 1
	    END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idWebMeetingOrganizer	= WMO.idWebMeetingOrganizer,
		@idSite					= WMO.idSite,
		@idUser					= WMO.idUser,
		@organizerType			= WMO.organizerType,
		@username				= WMO.username,
		@password				= WMO.[password]
	FROM tblWebMeetingOrganizer WMO
	WHERE WMO.idWebMeetingOrganizer = @idWebMeetingOrganizer
	AND WMO.idSite = @idCallerSite
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'WebMeetingOrganizerDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO