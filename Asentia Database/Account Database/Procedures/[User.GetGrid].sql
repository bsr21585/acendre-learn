-- =====================================================================
-- PROCEDURE: [User.GetGrid]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[User.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.GetGrid]
GO

CREATE PROCEDURE [User.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON

		DECLARE @GroupsWithPermission VARCHAR(MAX)

		SELECT @GroupsWithPermission = 
			STUFF(( SELECT ',' + RPL.scope
				FROM tblRoleToPermissionLink RPL
				LEFT JOIN tblUserToRoleLink UR ON UR.idRole = RPL.idRole
				LEFT JOIN tblPermission P ON P.idPermission = RPL.idPermission
				WHERE UR.idUser = @idCaller AND RPL.idSite = @idCallerSite
				AND (P.name = 'UsersAndGroups_GroupManager' OR P.name = 'UsersAndGroups_UserManager' OR P.name = 'UsersAndGroups_UserEditor')
			FOR XML PATH('')), 1, 1, '')
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblUser U
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND U.idSite = @idCallerSite)
				)
			AND U.idUser <> 1 -- not the template admin user
			AND ((isDeleted IS NULL OR isDeleted = 0) AND (isRegistrationApproved IS NULL OR isRegistrationApproved = 1))
			AND (@GroupsWithPermission IS NULL 
				 OR EXISTS(SELECT 1 FROM tblUserToGroupLink 
						    WHERE idUser = U.idUser AND idGroup IN (  SELECT s FROM dbo.DelimitedStringToTable(@GroupsWithPermission,','))
						   )
				)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN middlename END) END DESC,
							--FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN middlename END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN username END) END DESC,
							--FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN email END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN username END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN email END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN lastname END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN firstname END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN middlename END) END,
							--FOURTH ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN middlename END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN username END) END,
							--FIFTH ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN email END) END
						)
						AS [row_number]
					FROM tblUser U
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
						)
					AND idUser <> 1 -- not the template admin user
					AND ((isDeleted IS NULL OR isDeleted = 0) AND (isRegistrationApproved IS NULL OR isRegistrationApproved = 1))
					AND (@GroupsWithPermission IS NULL 
							OR EXISTS(SELECT 1 FROM tblUserToGroupLink 
						    WHERE idUser = U.idUser AND idGroup IN (  SELECT s FROM dbo.DelimitedStringToTable(@GroupsWithPermission,','))
						   )
				)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idUser, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				U.idUser, 
				U.displayName,
				U.username, 
				U.email,
				U.isActive,
				convert(bit, 1) AS isDisconnectOn, -- needs to be calculated
				convert(bit, 1) AS isLogonOn, -- needs to be calculated
				convert(bit, 1) AS isModifyOn, -- needs to be calculated
				STUFF((SELECT DISTINCT ',' + CAST(UGL.idGroup AS VARCHAR(10)) [text()] FROM tblUserToGroupLink UGL WHERE UGL.idUser = U.idUser FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,1,'') AS groupMemberships,
				U.avatar,
				CASE WHEN U.dtSessionExpires > GETUTCDATE() THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS isOnline,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblUser U ON U.idUser = SelectedKeys.idUser
			WHERE ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
			AND (@GroupsWithPermission IS NULL 
				 OR EXISTS(SELECT 1 FROM tblUserToGroupLink 
						    WHERE idUser = U.idUser AND idGroup IN (  SELECT s FROM dbo.DelimitedStringToTable(@GroupsWithPermission,','))
						   )
				)
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblUser
			INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = tblUser.idUser
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
				)
			AND idUser <> 1 -- not the template admin user
			AND ((isDeleted IS NULL OR isDeleted = 0) AND (isRegistrationApproved IS NULL OR isRegistrationApproved = 1))
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN middlename END) END DESC,
							--FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN middlename END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN username END) END DESC,
							--FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN email END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN username END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN email END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN lastname END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN firstname END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN middlename END) END,
							--FOURTH ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN middlename END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN username END) END,
							--FIFTH ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN email END) END
						)
						AS [row_number]
					FROM tblUser
					INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = tblUser.idUser
					WHERE 
						(
						@idCallerSite IS NULL
						OR 
						@idCallerSite IS NOT NULL AND idSite = @idCallerSite
						)
					AND idUser <> 1 -- not the template admin user
					AND ((isDeleted IS NULL OR isDeleted = 0) AND (isRegistrationApproved IS NULL OR isRegistrationApproved = 1))
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idUser, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				U.idUser,
				U.displayName,
				U.username, 
				U.email,
				U.isActive,
				convert(bit, 1) AS isDisconnectOn, -- needs to be calculated
				convert(bit, 1) AS isLogonOn, -- needs to be calculated
				convert(bit, 1) AS isModifyOn, -- needs to be calculated
				STUFF((SELECT DISTINCT ',' + CAST(UGL.idGroup AS VARCHAR(10)) [text()] FROM tblUserToGroupLink UGL WHERE UGL.idUser = U.idUser FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,1,'') AS groupMemberships,
				U.avatar,
				CASE WHEN U.dtSessionExpires > GETUTCDATE() THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS isOnline,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblUser U ON U.idUser = SelectedKeys.idUser
			WHERE ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
			ORDER BY SelectedKeys.[row_number]
			
			END
		
	END
	


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


