-- =====================================================================
-- PROCEDURE: [Catalog.GetParentsForGroup]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.GetParentsForGroup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.GetParentsForGroup]
GO

/*
Return all the records from the catalog table who have no parents.
*/
CREATE PROCEDURE [Catalog.GetParentsForGroup]
(
	@Return_Code				INT					OUTPUT,
	@Error_Description_Code		NVARCHAR (255)		OUTPUT,
	@idCaller					INT,
	@idCallerSite				INT					= 0,	--default if not specified
	@callerLangString			NVARCHAR (10),
	@languageString				NVARCHAR (10),
	@idGroup					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	/*
	
	get the data 
	
	*/
		
	;WITH CatalogTable AS
	(
	SELECT
		C.idCatalog,
		CASE WHEN CL.title IS NULL OR CL.title = '' THEN C.title ELSE CL.title END AS title,
		C.idParent,
		C.[order],
		CASE WHEN cGroup.idCatalog = C.idCatalog THEN 0 ELSE C.isPrivate END AS isPrivateAfterJoin,
		C.isPrivate,
		COUNT(cChildren.idCatalog) as catalogCount
	FROM tblCatalog C
	LEFT JOIN tblCatalog cChildren ON cChildren.idParent = c.idCatalog
	LEFT JOIN tblCatalogAccessToGroupLink cGroup ON cGroup.idGroup =  @idGroup AND cGroup.idCatalog = C.idCatalog
	LEFT JOIN tblCatalogLanguage CL 
		ON CL.idCatalog = C.idCatalog 
		AND CL.idLanguage = @idCallerLanguage
	WHERE
		C.idParent IS NULL
		AND C.idSite = @idCallerSite
		GROUP BY C.idCatalog , CL.title,C.title,C.idParent,C.[order],C.isPrivate,cGroup.idCatalog
	)
	
	SELECT	CT.idCatalog,
			CT.title,
			CT.idParent,
			CT.[order],
			CT.catalogCount,
			CT.isPrivateAfterJoin,
			CT.isPrivate,
			COUNT(CCL.idCourse) AS courseCount
	FROM CatalogTable CT
	LEFT JOIN tblCourseToCatalogLink CCL ON CCL.idCatalog = CT.idCatalog
	GROUP BY CT.idCatalog,CT.title,CT.idParent,CT.[order],CT.catalogCount,CT.isPrivateAfterJoin, CT.isPrivate
	ORDER BY CT.[order], CT.title
	
	SET @Return_Code = 0
	
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO