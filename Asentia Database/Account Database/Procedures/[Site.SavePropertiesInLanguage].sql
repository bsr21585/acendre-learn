-- =====================================================================
-- PROCEDURE: [Site.SavePropertiesInLanguage]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Site.SavePropertiesInLanguage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.SavePropertiesInLanguage]
GO

/*

***THIS procedure can only be called when setting the DEFAULT language for an object

*/

CREATE PROCEDURE [Site.SavePropertiesInLanguage]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idSite					INT,
	@languageString			NVARCHAR (10),
	@title					NVARCHAR(255),
	@company				NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
			SELECT @Return_Code = 1
			RETURN 1 
		END
	
	
	/*
	
	validate that the site exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSite
		WHERE idSite = @idSite
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 1
			RETURN 1 
		END
		
	-- update the multi-language
	
	UPDATE tblSiteLanguage SET
		title = @title,
		company = @company
	WHERE idSite = @idSite
	AND idLanguage = @idLanguage
	
	-- insert multi-language
	
	INSERT INTO tblSiteLanguage (
		idSite,
		idLanguage,
		title,
		company
	)
	SELECT
		@idSite,
		@idLanguage,
		@title, 
		@company
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblSiteLanguage SL
		WHERE SL.idSite = @idSite
		AND SL.idLanguage = @idLanguage
	)
	
	SELECT @Return_Code = 0
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO