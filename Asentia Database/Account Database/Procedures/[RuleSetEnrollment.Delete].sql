-- =====================================================================
-- PROCEDURE: [RuleSetEnrollment.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetEnrollment.Delete]
GO

/*

Deletes ruleset enrollment(s).

*/

CREATE PROCEDURE [RuleSetEnrollment.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCourse				INT,
	@RuleSetEnrollments		IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @RuleSetEnrollments) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'RuleSetEnrollmentDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all ruleset enrollments exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @RuleSetEnrollments RR
		LEFT JOIN tblRuleSetEnrollment R ON RR.id = R.idRuleSetEnrollment
		WHERE R.idSite IS NULL
		OR R.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetEnrollmentDelete_NoRecordFound'
		RETURN 1 
		END

	/*
	
	DELETE child record(s) from the language table.
	
	*/
	
	DELETE FROM tblRuleSetEnrollmentLanguage
	WHERE idRuleSetEnrollment IN (SELECT id FROM @RuleSetEnrollments)

	/*

	GET ruleset ids to remove rules, rulesets, and ruleset links

	*/

	DECLARE @RuleSets IDTable

	INSERT INTO @RuleSets (
		id
	)
	SELECT
		idRuleSet
	FROM tblRuleSetToRuleSetEnrollmentLink
	WHERE idRuleSetEnrollment IN (SELECT id FROM @RuleSetEnrollments)

	/*

	DELETE ruleset to ruleset enrollment links.
	
	*/
	
	DELETE FROM tblRuleSetToRuleSetEnrollmentLink
	WHERE idRuleSetEnrollment IN (SELECT id FROM @RuleSetEnrollments)

	/*

	DELETE the rules 

	*/

	DELETE FROM tblRule 
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the ruleset language records

	*/

	DELETE FROM tblRuleSetLanguage
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the rulesets

	*/

	DELETE FROM tblRuleSet
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE open ruleset engine queue items

	*/

	DELETE FROM tblRuleSetEngineQueue
	WHERE triggerObjectId IN (SELECT id FROM @RuleSetEnrollments) 
	AND triggerObject = 'rulesetenrollment'
	AND isProcessing IS NULL
	AND dtProcessed IS NULL

	/*

	DELETE enrollments inherited by the ruleset by using the Enrollment.Delete procedure
	Only delete incomplete ones, leave the completed enrollments for transcript purposes.

	*/

	DECLARE @EnrollmentsToDelete IDTable

	INSERT INTO @EnrollmentsToDelete (
		id
	)
	SELECT
		E.idEnrollment
	FROM tblEnrollment E
	WHERE E.idRuleSetEnrollment IN (SELECT id FROM  @RuleSetEnrollments)
	AND E.dtCompleted IS NULL
	AND E.idActivityImport IS NULL -- exclude imported records

	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EnrollmentsToDelete, 0

	/*

	REMOVE ruleset enrollment link for completed enrollments

	*/

	UPDATE tblEnrollment SET
		idRuleSetEnrollment = NULL
	WHERE idRuleSetEnrollment IN (SELECT id FROM @RuleSetEnrollments)
	AND dtCompleted IS NOT NULL

	/*
	
	DELETE the record(s) from ruleset enrollment table.
	
	*/

	DELETE FROM tblRuleSetEnrollment
	WHERE idRuleSetEnrollment IN (SELECT id FROM @RuleSetEnrollments)

	/*

	UPDATE the ruleset enrollment priorities for the remaining, undeleted ruleset enrollments belonging to the course

	*/

	DECLARE @RuleSetEnrollmentOrdering TABLE ([priority] INT IDENTITY(1, 1), idRuleSetEnrollment INT)

	INSERT INTO @RuleSetEnrollmentOrdering (
		idRuleSetEnrollment
	)
	SELECT
		RSE.idRuleSetEnrollment
	FROM tblRuleSetEnrollment RSE
	WHERE RSE.idCourse = @idCourse

	ORDER BY [priority]
	
	UPDATE tblRuleSetEnrollment SET
		[priority] = RSEO.[priority]
	FROM @RuleSetEnrollmentOrdering RSEO
	WHERE RSEO.idRuleSetEnrollment = tblRuleSetEnrollment.idRuleSetEnrollment

	/*

	delete synced ruleset enrollments

	*/

	DECLARE @idRuleSetEnrollment						INT

	DECLARE idRuleSetEnrollmentCursor					CURSOR LOCAL
	FOR 
	SELECT id FROM @RuleSetEnrollments

	OPEN idRuleSetEnrollmentCursor
	FETCH NEXT FROM idRuleSetEnrollmentCursor INTO @idRuleSetEnrollment
	WHILE @@FETCH_STATUS = 0  
	BEGIN

		SELECT 
			idCourse
		INTO #idCoursesToDeleteFrom
		FROM tblRuleSetEnrollment
		WHERE idParentRuleSetEnrollment = @idRuleSetEnrollment

		DECLARE idCourseCursor							CURSOR LOCAL
		FOR 
		SELECT idCourse FROM #idCoursesToDeleteFrom

		DECLARE @idSyncedCourse							INT

		OPEN idCourseCursor
		FETCH NEXT FROM idCourseCursor INTO @idSyncedCourse
		WHILE @@FETCH_STATUS = 0  
		BEGIN

			DECLARE @ChildRuleSetEnrollments IDTable

			INSERT INTO @ChildRuleSetEnrollments (
				id
			)
			SELECT
				idRuleSetEnrollment
			FROM tblRuleSetEnrollment
			WHERE idParentRuleSetEnrollment IN (SELECT id FROM @RuleSetEnrollments)
			AND idCourse = @idSyncedCourse

			EXEC [RuleSetEnrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idSyncedCourse, @ChildRuleSetEnrollments

			FETCH NEXT FROM idCourseCursor INTO @idSyncedCourse

		END
		
		CLOSE idCourseCursor
		DEALLOCATE idCourseCursor

		FETCH NEXT FROM idRuleSetEnrollmentCursor INTO @idRuleSetEnrollment

	END

	CLOSE idRuleSetEnrollmentCursor
	DEALLOCATE idRuleSetEnrollmentCursor

	DROP TABLE #idCoursesToDeleteFrom
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO