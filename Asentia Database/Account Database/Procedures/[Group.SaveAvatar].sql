-- =====================================================================
-- PROCEDURE: [Group.SaveAvatar]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.SaveAvatar]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.SaveAvatar]
GO

/*

Saves a group's avatar path to the database.

*/
CREATE PROCEDURE [Group.SaveAvatar]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idGroup				INT, 
	@avatar					NVARCHAR (255)
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	update the group's avatar path

	*/
		
	-- check that the group id exists
	IF (SELECT COUNT(1) FROM tblGroup WHERE idGroup = @idGroup AND idSite = @idCallerSite) < 1
		BEGIN                               
			
		SET @idGroup = @idGroup
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'GroupSaveAvatar_NoRecordFound'
		RETURN 1
			
		END
			
		
	-- update existing group's properties
		
	UPDATE tblGroup SET
		avatar = @avatar
	WHERE idGroup = @idGroup

	SET @Return_Code = 0 --(0 is 'success')

	END			

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
