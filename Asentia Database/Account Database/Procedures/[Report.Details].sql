-- =====================================================================
-- PROCEDURE: [Report.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Report.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Report.Details]
GO

/*

Return all the properties for a given Report id.

*/

CREATE PROCEDURE [Report.Details]
(
	@Return_Code		INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite		INT					= 0, --default if not specified
	@callerLangString	NVARCHAR (10),
	@idCaller			INT					= 0,
	
	@idReport			INT					OUTPUT, 
	@idSite				INT					OUTPUT, 
	@idUser				INT					OUTPUT, 
	@idDataset			INT					OUTPUT, 
	@title				NVARCHAR(255)		OUTPUT,
	@fields				NVARCHAR(MAX)		OUTPUT,		
	@filter				NVARCHAR(MAX)		OUTPUT,	
	@order				NVARCHAR(255)		OUTPUT,	
	@isPublic			BIT					OUTPUT,
	@dtCreated			DATETIME			OUTPUT,
	@dtModified			DATETIME			OUTPUT,
	@isDeleted			BIT					OUTPUT,
	@dtDeleted			DATETIME			OUTPUT,
	@hasShortcut		BIT					OUTPUT

)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblReport
		WHERE idReport = @idReport
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ReportDetails_NoRecordFound'
		RETURN 1
		END

	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
	
	
	/*
	
	get the data
	
	*/
	
	SELECT
		@idReport			= R.idReport,
		@idSite				= R.idSite,
		@idUser				= R.idUser, 
		@idDataset			= R.idDataset, 		
		@title				= CASE WHEN RL.title IS NULL OR RL.title = '' THEN R.title ELSE RL.title END,
		@fields				= R.fields,
		@filter				= R.filter,	
		@order				= R.[order],
		@isPublic			= R.isPublic,
		@dtCreated			= R.dtCreated,
		@dtModified			= R.dtModified,
		@isDeleted			= R.isDeleted,
		@dtDeleted			= R.dtDeleted,
		@hasShortcut		= CASE WHEN RS.idReportToReportShortcutsWidgetLink IS NULL THEN 0 ELSE 1 END

	FROM tblReport R
	LEFT JOIN tblReportLanguage RL 
		ON RL.idReport = R.idReport
		AND RL.idLanguage = @idCallerLanguage
	LEFT JOIN tblReportToReportShortcutsWidgetLink RS
		ON RS.idReport = R.idReport AND RS.idUser = @idCaller
	WHERE 
		R.idReport = @idReport
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'ReportDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO