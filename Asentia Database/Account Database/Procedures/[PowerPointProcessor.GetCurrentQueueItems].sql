-- =====================================================================
-- PROCEDURE: [PowerPointProcessor.GetCurrentQueueItems]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[PowerPointProcessor.GetCurrentQueueItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [PowerPointProcessor.GetCurrentQueueItems]
GO

/*

Gets the current (not yet processed) items in the PowerPoint processor queue.

*/

CREATE PROCEDURE [PowerPointProcessor.GetCurrentQueueItems]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idSite					INT,
	@numItems				INT
)
AS

	BEGIN
	SET NOCOUNT ON

	IF (@numItems IS NULL)
		BEGIN
		SET @numItems = 1000 -- default is 1000
		END

	SELECT TOP (@numItems)
		CP.idContentPackage,
		CP.idSite,
		CP.[path],
		CP.contentTitle,
		CP.originalMediaFilename,
		CP.enableAutoplay,
		CP.allowRewind,
		CP.allowFastForward,
		CP.allowNavigation,
		CP.allowResume,
		CP.minProgressForCompletion
	FROM tblContentPackage CP
	WHERE CP.isMediaUpload = 1
	AND CP.idMediaType = 2  -- PowerPoint media type
	AND CP.isProcessed = 0  -- not yet processed
	AND CP.isProcessing = 0 -- is not currently processing
	AND (
			@idSite IS NULL	-- all sites
			OR
			CP.idSite = @idSite -- just a specified site
		)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO