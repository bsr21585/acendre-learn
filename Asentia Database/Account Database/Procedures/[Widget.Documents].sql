-- =====================================================================
-- PROCEDURE: [Widget.Documents]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Widget.Documents]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.Documents]
GO

/*

Gets a listing of documents that the caller has access to

*/

CREATE PROCEDURE [Widget.Documents]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	get the site's default language id

	*/

	DECLARE @idSiteLanguage INT
	SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END


	/*

	begin getting the grid data

	*/

	CREATE TABLE #Documents (
		idDocumentRepositoryItem	INT,
		filename					NVARCHAR(255),
		objectType					NVARCHAR(50),
		objectName					NVARCHAR(255),
		idObject					INT,
		label						NVARCHAR(255)
	)

	-- get the grid data

	INSERT INTO #Documents (
		idDocumentRepositoryItem,
		filename,
		objectType,
		objectName,
		idObject,
		label
	)
	-- GROUP
	SELECT
		DRI.idDocumentRepositoryItem,
		DRI.filename,
		DROT.name,
		G.name,
		DRI.idObject,
		DRI.label
	FROM tblDocumentRepositoryItem DRI
	LEFT JOIN tblDocumentRepositoryObjectType DROT ON DRI.idDocumentRepositoryObjectType = DROT.idDocumentRepositoryObjectType
	LEFT JOIN tblGroup G ON DRI.idObject = G.idGroup
	LEFT JOIN tblUserToGroupLink UGL ON DRI.idObject = UGL.idGroup
	WHERE DRI.idDocumentRepositoryObjectType = 1 
	AND DRI.idSite = @idCallerSite
	AND UGL.idUser = @idCaller 
	AND (DRI.isDeleted = 0 OR DRI.isDeleted IS NULL)

	UNION

	-- COURSE
	SELECT
		DRI.idDocumentRepositoryItem,
		DRI.filename,
		DROT.name,
		C.title,
		DRI.idObject,
		DRI.label
	FROM tblDocumentRepositoryItem DRI
	LEFT JOIN tblDocumentRepositoryObjectType DROT ON DRI.idDocumentRepositoryObjectType = DROT.idDocumentRepositoryObjectType
	LEFT JOIN tblCourse C ON DRI.idObject = C.idCourse
	LEFT JOIN tblEnrollment E ON DRI.idObject = E.idCourse
	WHERE DRI.idDocumentRepositoryObjectType = 2 
	AND DRI.idSite = @idCallerSite
	AND E.idUser = @idCaller 
	AND (DRI.isDeleted = 0 OR DRI.isDeleted IS NULL)

	UNION

	-- LEARNING PATH
	SELECT
		DRI.idDocumentRepositoryItem,
		DRI.filename,
		DROT.name,
		LP.name,
		DRI.idObject,
		DRI.label
	FROM tblDocumentRepositoryItem DRI
	LEFT JOIN tblDocumentRepositoryObjectType DROT ON DRI.idDocumentRepositoryObjectType = DROT.idDocumentRepositoryObjectType
	LEFT JOIN tblLearningPath LP ON DRI.idObject = LP.idLearningPath
	LEFT JOIN tblUserToLearningPathLink ULPL ON DRI.idObject = ULPL.idLearningPath
	WHERE DRI.idDocumentRepositoryObjectType = 3 
	AND DRI.idSite = @idCallerSite
	AND ULPL.idUser = @idCaller 
	AND (DRI.isDeleted = 0 OR DRI.isDeleted IS NULL)

	-- return the rowcount and grid data

	IF @searchParam IS NULL
		BEGIN

		SELECT COUNT(1) AS row_count
		FROM #Documents

		;WITH
			Keys AS (
				SELECT TOP (@pageNum * @pageSize)
					D.idDocumentRepositoryItem,
					ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'filename' THEN D.filename END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'filename' THEN D.filename END) END
						)
					AS [row_number]
				FROM #Documents D
			),

			SelectedKeys AS (
				SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
					idDocumentRepositoryItem, 
					[row_number]
				FROM Keys
				WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
			)
		SELECT
			D.idDocumentRepositoryItem,
			D.filename,
			D.objectType,
			D.objectName,
			D.idObject,
			D.label
		FROM SelectedKeys
		LEFT JOIN #Documents D ON D.idDocumentRepositoryItem = SelectedKeys.idDocumentRepositoryItem
		ORDER BY SelectedKeys.[row_number]
		END
	ELSE

		BEGIN

		-- if the caller's language is the same as the site's language,
		-- search and display on the base table
		IF (@idCallerLanguage = @idSiteLanguage)

			BEGIN
			SELECT COUNT(1) AS row_count
			FROM #Documents D
			INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItem, *, @searchParam) K On K.[Key] = D.idDocumentRepositoryItem

			;WITH
				Keys AS (
					SELECT TOP (@pageNum * @pageSize)
						D.idDocumentRepositoryItem,
						ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'filename' THEN D.filename END) END DESC,
							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'filename' THEN D.filename END) END
							)
						AS [row_number]
					FROM #Documents D
					INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItem, *, @searchParam) K On K.[Key] = D.idDocumentRepositoryItem
				),

				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDocumentRepositoryItem, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				D.idDocumentRepositoryItem,
				D.filename,
				D.objectType,
				D.objectName,
				D.idObject,
				D.label
			FROM SelectedKeys
			LEFT JOIN #Documents D ON D.idDocumentRepositoryItem = SelectedKeys.idDocumentRepositoryItem
			ORDER BY SelectedKeys.[row_number]
			END

		-- if the caller's language is not the same as the site's
		-- default language, we will need to search and display on
		-- the language table, and join to the base table for the
		-- rest of the properties
		ELSE

			BEGIN
			SELECT COUNT(1) AS row_count
			FROM #Documents D
			INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItemLanguage, *, @searchParam) K On K.[Key] = D.idDocumentRepositoryItem

			;WITH
				Keys AS (
					SELECT TOP (@pageNum * @pageSize)
						D.idDocumentRepositoryItem,
						ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'filename' THEN D.filename END) END DESC,
							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'filename' THEN D.filename END) END
							)
						AS [row_number]
					FROM #Documents D
					INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItemLanguage, *, @searchParam) K On K.[Key] = D.idDocumentRepositoryItem
				),

				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDocumentRepositoryItem, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				D.idDocumentRepositoryItem,
				D.filename,
				D.objectType,
				D.objectName,
				D.idObject,
				D.label
			FROM SelectedKeys
			LEFT JOIN #Documents D ON D.idDocumentRepositoryItem = SelectedKeys.idDocumentRepositoryItem
			ORDER BY SelectedKeys.[row_number]
		END
	END
	-- drop temporary table
	DROP TABLE #Documents

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO