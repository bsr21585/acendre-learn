-- =====================================================================
-- PROCEDURE: [StandUpTrainingInstanceToUserLink.IsUserEnrolled]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandUpTrainingInstanceToUserLink.IsUserEnrolled]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandUpTrainingInstanceToUserLink.IsUserEnrolled]
GO
	
/*

Determines if a user is enrolled in a session.

*/

CREATE PROCEDURE [dbo].[StandUpTrainingInstanceToUserLink.IsUserEnrolled]
(
	@Return_Code							INT				OUTPUT,
	@Error_Description_Code					NVARCHAR(50)	OUTPUT,
	@idCallerSite							INT				= 0, --default if not specified
	@callerLangString						NVARCHAR(10),
	@idCaller								INT				= 0,

	@idStandupTrainingInstance				INT,
	@idUser									INT				OUTPUT,
	@username								NVARCHAR(512)	OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	-- check if the user is enrolled in the session
	IF(EXISTS (SELECT 1 FROM tblStandUpTrainingInstanceToUserLink SUTIUL
				LEFT JOIN tblUser U ON U.idUser = SUTIUL.idUser
				WHERE SUTIUL.idStandUpTrainingInstance = @idStandupTrainingInstance AND U.username = @username))
		BEGIN
		SET @idUser = 0
		
		END
		ELSE
		BEGIN
			SELECT
				@idUser = idUser
			FROM tblUser
			WHERE username = @username AND isDeleted = 0 AND isActive = 1
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
