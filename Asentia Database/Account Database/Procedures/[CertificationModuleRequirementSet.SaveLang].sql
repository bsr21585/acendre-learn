-- =====================================================================
-- PROCEDURE: [CertificationModuleRequirementSet.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificationModuleRequirementSet.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModuleRequirementSet.SaveLang]
GO

/*

Saves certification module requirement set "language specific" properties for specific language
in certification module requirement set language table.

*/

CREATE PROCEDURE [CertificationModuleRequirementSet.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCertificationModuleRequirementSet	INT, 
	@languageString							NVARCHAR(10),
	@label									NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleRequiremenSetSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
			 
	/*
	
	validate that the certification module requirement set exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificationModuleRequirementSet
		WHERE idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleRequirementSetSaveLang_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblCertificationModuleRequirementSetLanguage CMRSL WHERE CMRSL.idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet AND CMRSL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblCertificationModuleRequirementSetLanguage SET
			label = @label
		WHERE idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCertificationModuleRequirementSetLanguage (
			idSite,
			idCertificationModuleRequirementSet,
			idLanguage,
			label
		)
		SELECT
			@idCallerSite,
			@idCertificationModuleRequirementSet,
			@idLanguage,
			@label
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCertificationModuleRequirementSetLanguage CMRSL
			WHERE CMRSL.idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet
			AND CMRSL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblCertificationModuleRequirementSet SET
			label = @label
		WHERE idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO