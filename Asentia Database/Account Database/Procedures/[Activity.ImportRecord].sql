-- =====================================================================
-- PROCEDURE: [Activity.ImportRecord]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Activity.ImportRecord]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Activity.ImportRecord]
GO

/*

Record the activity instance

*/

CREATE PROCEDURE [dbo].[Activity.ImportRecord]
(
	@Return_Code		INT					OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite		INT					= 0,	-- default if not specified
	@callerLangString	NVARCHAR(10),
	@idCaller			INT					= 0,	-- will fail if not specified

	@idActivityImport	INT					OUTPUT, 
	@idUserImported		INT,
	@importFileName		NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	Insert Activity Import Record
	
	*/
	
	INSERT INTO tblActivityImport
	(
	[idSite],
	[idUser],
	[timestamp],
	[idUserImported],
	[importFileName]
	)
	VALUES
	(
	@idCallerSite,
	@idCaller,
	GETUTCDATE(),
	@idUserImported,
	@importFileName
	)

	-- get the new activityImport's id and return successfully
		
	SELECT @idActivityImport = SCOPE_IDENTITY()
	
	SELECT @Return_Code = 0
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		
