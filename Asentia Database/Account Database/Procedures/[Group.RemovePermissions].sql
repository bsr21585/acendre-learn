-- =====================================================================
-- PROCEDURE: [Group.RemovePermissions]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.RemovePermissions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.RemovePermissions]
GO

/*

Add or updates the optional language fields for the object 

*/

CREATE PROCEDURE [Group.RemovePermissions]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idGroup				INT, 
	@idPermissions			NVARCHAR(MAX)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	DECLARE @Permissions TABLE (idPermission INT)
	INSERT INTO @Permissions (idPermission)
	SELECT V.s
	FROM [dbo].virtualIDTable (@idPermissions, ',') V
		
	/*
	
	validate that the caller is a member of the site
	
	*/
	
		IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
		
	/*
	
	Validate caller permission
	
	*/
	
	IF @idCaller <> 1
	
		BEGIN
		
		IF (
			SELECT COUNT(1)
			FROM tblUserToPermissionLink UPL 
			WHERE UPL.idUser = @idCaller
			AND UPL.idPermission = @idPermission
			AND (UPL.idScope = @idGroup OR UPL.idScope IS NULL) -- global permission or scoped to this object
			) = 0 
			
			BEGIN
			SELECT @Return_Code = 3
			RETURN 1
			END
		
				
		END
		
	/*
	
	validate that the specified caller language exists
	
	*/
	
	-- not used.
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblGroup
		WHERE idGroup = @idGroup
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
		SELECT @Return_Code = 1
		RETURN 1 
		END
		
	IF (
		SELECT COUNT(1)
		FROM @Permissions PP
		LEFT JOIN tblPermission P ON P.idPermission = PP.idPermission
		WHERE P.idPermission IS NULL
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 1
		RETURN 1 
		END
		
	-- delete the permissions
	
	DELETE FROM tblGroupToPermissionLink
	WHERE idGroup = @idGroup
	AND idPermission IN (
		SELECT idPermission
		FROM @Permissions
	)
		
	SELECT @Return_Code = 0 --(0 is 'success')
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO