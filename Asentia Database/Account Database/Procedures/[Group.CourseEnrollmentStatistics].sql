-- =====================================================================
-- PROCEDURE: [Group.CourseEnrollmentStatistics]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Group.CourseEnrollmentStatistics]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.CourseEnrollmentStatistics]
GO

/*

Gets course enrollment statistics for all members of a group.

*/

CREATE PROCEDURE [Group.CourseEnrollmentStatistics]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idGroup				INT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	put all users that are a member of the group into a temporary table

	*/

	DECLARE @GroupUsers IDTable
	INSERT INTO @GroupUsers (id) SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup = @idGroup

	/*

	put all course enrollments into a temporary table, we'll work from that table

	*/

	CREATE TABLE #Enrollments (
		idEnrollment			INT,
		idCourse				INT,
		dtStart					DATETIME,
		dtDue					DATETIME,
		dtExpires				DATETIME,
		dtCompleted				DATETIME
	)

	INSERT INTO #Enrollments (
		idEnrollment,
		idCourse,
		dtStart,
		dtDue,
		dtExpires,
		dtCompleted
	)
	SELECT
		E.idEnrollment,
		E.idCourse,
		E.dtStart,
		E.dtDue,
		CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
			NULL
		ELSE
			CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NOT NULL THEN
				E.dtExpiresFromFirstLaunch
			ELSE
				CASE WHEN E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
					E.dtExpiresFromStart
				ELSE
					CASE WHEN E.dtExpiresFromStart >= E.dtExpiresFromFirstLaunch THEN
						E.dtExpiresFromFirstLaunch
					ELSE
						E.dtExpiresFromStart
					END
				END
			END
		END AS [dtExpires],
		E.dtCompleted
	FROM tblEnrollment E
	LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
	LEFT JOIN tblUser U ON U.idUser = E.idUser	
	WHERE E.idSite = @idCallerSite
	AND E.idUser IN (SELECT id FROM @GroupUsers)
	AND (C.isDeleted = 0 OR C.isDeleted IS NULL)
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- enrollments for deleted and unapproved users should not be reflected
	AND U.isActive = 1 -- only active users should be included
	AND E.dtStart <= GETUTCDATE()
	AND E.idActivityImport IS NULL
		
	/*

	COURSE ENROLLMENT STATISTICS BY STATUS

	*/

	SELECT
		(SELECT COUNT(1) FROM #Enrollments E WHERE (E.dtDue > GETUTCDATE() OR E.dtDue IS NULL) AND (E.dtExpires > GETUTCDATE() OR E.dtExpires IS NULL) AND E.dtCompleted IS NULL) AS [##Enrolled##],
		(SELECT COUNT(1) FROM #Enrollments E WHERE E.dtCompleted IS NOT NULL) AS [##Completed##],
		(SELECT COUNT(1) FROM #Enrollments E WHERE E.dtDue <= GETUTCDATE() AND (E.dtExpires > GETUTCDATE() OR E.dtExpires IS NULL) AND E.dtCompleted IS NULL) AS [##Overdue##],
		(SELECT COUNT(1) FROM #Enrollments E WHERE E.dtExpires IS NOT NULL AND E.dtExpires <= GETUTCDATE() AND E.dtCompleted IS NULL) AS [##Expired##],
		(SELECT COUNT(1) FROM #Enrollments E) AS _Total_
		
	-- DROP THE TEMPORARY TABLES
	DROP TABLE #Enrollments		

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO