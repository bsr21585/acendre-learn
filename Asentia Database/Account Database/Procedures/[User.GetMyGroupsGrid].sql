-- =====================================================================
-- PROCEDURE: [User.GetMyGroupsGrid]
-- =====================================================================

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.GetMyGroupsGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.GetMyGroupsGrid]
GO

/*

Gets a grid of distinct groups a user belongs to.

*/

CREATE PROCEDURE [User.GetMyGroupsGrid] 
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,

	@idUser					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141
		
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = NULL
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	get the site's default language id

	*/

	DECLARE @idSiteLanguage INT
	SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	/*

	first make a temp table and get a distinct list of groups the user belongs to, this will be the base table we query off of

	*/

	CREATE TABLE #UserToGroupLinkDistinct (idGroup INT)

	INSERT INTO #UserToGroupLinkDistinct (
		idGroup
	)
	SELECT DISTINCT
		UGL.idGroup
	FROM tblUserToGroupLink UGL
	LEFT JOIN tblGroup G ON G.idGroup = UGL.idGroup
	WHERE G.idSite = @idCallerSite
	AND UGL.idUser = @idUser
	AND G.membershipIsPublicized = 1

	/*

	begin getting the grid data

	*/

	IF @searchParam IS NULL

		BEGIN	
		
		-- return the rowcount

		SELECT COUNT(1) AS row_count 
		FROM #UserToGroupLinkDistinct
			
		;WITH 
			Keys AS (
				SELECT TOP (@pageNum * @pageSize)
					UGLD.idGroup,
					ROW_NUMBER() OVER (ORDER BY
						-- FIRST ORDER DESC
						CASE WHEN @orderAsc = 0 THEN G.name END DESC,
							
						-- FIRST ORDER ASC
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN G.name END							
					) 
					AS [row_number]
				FROM #UserToGroupLinkDistinct UGLD
				LEFT JOIN tblGroup G ON G.idGroup = UGLD.idGroup	
			),

			SelectedKeys AS (
				SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
					idGroup, 
					[row_number]
				FROM Keys
				WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
			)
		SELECT
			G.idGroup,
			CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
			CONVERT(BIT, ISNULL(G.IsFeedActive, 0)) AS [showGroupWall],
			SelectedKeys.[row_number]
		FROM SelectedKeys
		JOIN tblGroup G ON G.idGroup = SelectedKeys.idGroup
		LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
		ORDER BY SelectedKeys.[row_number]

		END

	ELSE

		BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM #UserToGroupLinkDistinct UGLD
				INNER JOIN CONTAINSTABLE(tblGroup, *, @searchParam) K ON K.[key] = UGLD.idGroup
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							UGLD.idGroup,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN G.name END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN G.name END
							)
							AS [row_number]
						FROM #UserToGroupLinkDistinct UGLD
						LEFT JOIN tblGroup G ON G.idGroup = UGLD.idGroup
						INNER JOIN CONTAINSTABLE(tblGroup, *, @searchParam) K ON K.[key] = UGLD.idGroup
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idGroup, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					G.idGroup, 
					G.name AS name,
					CONVERT(BIT, ISNULL(G.IsFeedActive, 0)) AS [showGroupWall],
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblGroup G ON G.idGroup = SelectedKeys.idGroup
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblGroupLanguage GL
				INNER JOIN #UserToGroupLinkDistinct UGLD ON UGLD.idGroup = GL.idGroup
				INNER JOIN CONTAINSTABLE(tblGroupLanguage, *, @searchParam) K ON K.[key] = GL.idGroupLanguage
				WHERE GL.idLanguage = @idCallerLanguage
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							GL.idGroupLanguage,
							UGLD.idGroup,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN GL.name END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN GL.name END
							)
							AS [row_number]
						FROM tblGroupLanguage GL
						INNER JOIN #UserToGroupLinkDistinct UGLD ON UGLD.idGroup = GL.idGroup
						INNER JOIN CONTAINSTABLE(tblGroupLanguage, *, @searchParam) K ON K.[key] = GL.idGroupLanguage
						WHERE GL.idLanguage = @idCallerLanguage
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idGroup, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					G.idGroup, 
					GL.name,
					CONVERT(BIT, ISNULL(G.IsFeedActive, 0)) AS [showGroupWall],
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblGroupLanguage GL ON GL.idGroup = SelectedKeys.idGroup AND GL.idLanguage = @idCallerLanguage
				LEFT JOIN tblGroup G ON G.idGroup = GL.idGroup
				ORDER BY SelectedKeys.[row_number]

				END
			
			END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
