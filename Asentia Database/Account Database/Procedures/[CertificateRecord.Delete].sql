-- =====================================================================
-- PROCEDURE: [CertificateRecord.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificateRecord.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificateRecord.Delete]
GO


CREATE PROCEDURE [CertificateRecord.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,
	
	@CertificateRecords			IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	IF (SELECT COUNT(1) FROM @CertificateRecords) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CertificateRecordDelete_NoRecordsSelected' -- nothing to do
		RETURN 1
		END
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
			
	/*
	
	validate that all certificates exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @CertificateRecords CC
		LEFT JOIN tblCertificateRecord CR ON CR.idCertificateRecord = CC.id
		WHERE CR.idSite IS NULL
		OR CR.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'CertificateRecordDelete_UsersNotInSameSite'
		RETURN 1 
		END
	
	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	do event log entries for certificate revocation prior to deleting the records

	*/

	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		CR.idCertificate,
		CRCR.id,
		CR.idUser
	FROM @CertificateRecords CRCR
	LEFT JOIN tblCertificateRecord CR ON CR.idCertificateRecord = CRCR.id

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 703, @utcNow, @eventLogItems
	
	/*
	
	delete the certificate records
	
	*/
	
	DELETE FROM tblCertificateRecord
	WHERE idCertificateRecord IN (SELECT id FROM @CertificateRecords)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO