-- =====================================================================
-- PROCEDURE: [Data-TinCan.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCan.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCan.Save]
GO

/*

Adds Tin Can Statement(s). 

*/

CREATE PROCEDURE [Data-TinCan.Save]
(
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idDataLesson			INT,
	@idEndpoint				INT,
	@isInternalAPI			BIT,
	@statements				TinCanStatementTable	READONLY
)
AS
	BEGIN
	SET NOCOUNT ON

	--Inserting New verb if any which is not present in our Verb table
	INSERT INTO tblTinCanVerb(verb,URI, dtCreated, dtModified, isDeleted, dtDeleted)
	SELECT REVERSE( SUBSTRING(REVERSE(VerbId),0, CHARINDEX('/',REVERSE(VerbId)))),VerbId, GETUTCDATE(), GETUTCDATE(), 0, NULL
	FROM @statements ST
	WHERE ST.verbId NOT IN(
				Select URI FROM tblTinCanVerb WHERE isDeleted = 0
				)
	 

	DECLARE @idDataTinCanParent				INT,
			@rollback						BIT = 0,
			
			@statementId					NVARCHAR(50),
			@parentStatementId				NVARCHAR(50),
			@actor							NVARCHAR(max),
			@verbId							NVARCHAR(100),
			@verb							NVARCHAR(200),
			@activityId						NVARCHAR(300),
			
			@mboxObject						NVARCHAR(100),
			@mboxSha1SumObject				NVARCHAR(100),
			@openIdObject					NVARCHAR(100),
			@accountObject					NVARCHAR(100),
			
			@mboxActor						NVARCHAR(100),
			@mboxSha1SumActor				NVARCHAR(100),
			@openIdActor					NVARCHAR(100),
			@accountActor					NVARCHAR(100),
			
			@mboxAuthority					NVARCHAR(100),
			@mboxSha1SumAuthority			NVARCHAR(100),
			@openIdAuthority				NVARCHAR(100),
			@accountAuthority				NVARCHAR(100),
			
			@mboxTeam						NVARCHAR(100),
			@mboxSha1SumTeam				NVARCHAR(100),
			@openIdTeam						NVARCHAR(100),
			@accountTeam					NVARCHAR(100),
			
			@mboxInstructor					NVARCHAR(100),
			@mboxSha1SumInstructor			NVARCHAR(100),
			@openIdInstructor				NVARCHAR(100),
			@accountInstructor				NVARCHAR(100),
			
			@object							NVARCHAR(max),
			@registration					NVARCHAR(50),
			@statement						NVARCHAR(max),
			@statementIdToBeVoided			NVARCHAR(50),
			@scoreScaled				    FLOAT                  =      NULL
	
	DECLARE statementCursor CURSOR FOR
		SELECT	statementId,
		       	parentStatementId,
				actor,
				V.idVerb,
				S.verb,
				activityId,
				mboxObject,
				mboxSha1SumObject,
				openIdObject,
				accountObject,
				mboxActor,
				mboxSha1SumActor,
				openIdActor,
				accountActor,
				mboxAuthority,
				mboxSha1SumAuthority,
				openIdAuthority,
				accountAuthority,
				mboxTeam,
				mboxSha1SumTeam,
				openIdTeam,
				accountTeam,
				mboxInstructor,
				mboxSha1SumInstructor,
				openIdInstructor,
				accountInstructor,
				[object],
				registration,
				[statement],
				statementIdToBeVoided,
				scoreScaled
		FROM	@statements S
				LEFT JOIN tblTinCanVerb V ON S.verbId = V.URI
	
	OPEN statementCursor
	
	FETCH NEXT FROM statementCursor
	INTO	@statementId,
			@parentStatementId,
			@actor,
			@verbId,
			@verb,
			@activityId,
			@mboxObject,
			@mboxSha1SumObject,
			@openIdObject,
			@accountObject,
			@mboxActor,
			@mboxSha1SumActor,
			@openIdActor,
			@accountActor,
			@mboxAuthority,
			@mboxSha1SumAuthority,
			@openIdAuthority,
			@accountAuthority,
			@mboxTeam,
			@mboxSha1SumTeam,
			@openIdTeam,
			@accountTeam,
			@mboxInstructor,
			@mboxSha1SumInstructor,
			@openIdInstructor,
			@accountInstructor,
			@object,
			@registration,
			@statement,
			@statementIdToBeVoided,
			@scoreScaled
	
	BEGIN TRANSACTION
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
		
		--Validate uniqueness of statement
		IF (
			SELECT COUNT(1)
			FROM [tblData-TinCan]
			WHERE idSite = @idCallerSite
			AND statementId = @statementId
			) > 0
			
			BEGIN
			SET @rollback = 1
			SELECT @Return_Code = 2
			SET @Error_Description_Code = 'DataTinCanSave_StatementIdNotUnique'
			BREAK;
			END
		
		--Validate uniqueness of activity id
		/*IF (
			SELECT COUNT(1)
			FROM [tblData-TinCan]
			WHERE idSite = @idCallerSite
			AND activityId = @activityId
			) > 0
			
			BEGIN
			SET @rollback = 1
			SELECT @Return_Code = 2
			SET @Error_Description_Code = 'TinCanSave_ActivityIdNotUnique'
			BREAK;
			END*/
			
		--Check whether this statement is voiding another statement.
		IF (@statementIdToBeVoided IS NOT NULL)
			BEGIN
			
			--Check whether the statement (to be voided) exists.
			IF (
				SELECT COUNT(1)
				FROM [tblData-TinCan]
				WHERE idSite = @idCallerSite
				AND statementId = @statementIdToBeVoided
				) = 0
				BEGIN
				
				SET @rollback = 1
				SELECT @Return_Code = 1
				SET @Error_Description_Code = 'DataTinCanSave_StatementToBeVoidedNotFound'
				BREAK;
				
				END
			
			--Validate caller permission to this statement
			IF (
				SELECT COUNT(1)
				FROM [tblData-TinCan]
				WHERE idSite = @idCallerSite
				AND statementId = @statementIdToBeVoided
				AND idEndpoint = @idEndpoint
				) = 0
				BEGIN
				
				SET @rollback = 1
				SELECT @Return_Code = 3
				SET @Error_Description_Code = 'DataTinCanSave_VoidStatementPermissionError'
				BREAK;
				
				END
			
			UPDATE	[tblData-TinCan]
			SET		isStatementVoided = 1
			WHERE	idSite = @idCallerSite
			AND		statementId = @statementIdToBeVoided
			
			END
		
		--Get parent statement id
		IF @parentStatementId IS NOT NULL
			BEGIN
			SELECT	@idDataTinCanParent = [idData-TinCan]
			FROM	[tblData-TinCan]
			WHERE	idSite = @idCallerSite
			AND		statementId = @parentStatementId
			END
		
		--Save the data
		INSERT INTO [tblData-TinCan] (
		    [idData-Lesson],
			[idData-TinCanParent],
			idSite,
			idEndpoint,
			isInternalAPI,
			statementId,
			actor,
			verbId,
			verb,
			activityId,
			mboxObject,
			mboxSha1SumObject,
			openIdObject,
			accountObject,
			mboxActor,
			mboxSha1SumActor,
			openIdActor,
			accountActor,
			mboxAuthority,
			mboxSha1SumAuthority,
			openIdAuthority,
			accountAuthority,
			mboxTeam,
			mboxSha1SumTeam,
			openIdTeam,
			accountTeam,
			mboxInstructor,
			mboxSha1SumInstructor,
			openIdInstructor,
			accountInstructor,
			[object],
			registration,
			[statement],
			isVoidingStatement,
			isStatementVoided,
			dtStored,
			scoreScaled
		)
		VALUES (
		    @idDataLesson,
			@idDataTinCanParent,
			@idCallerSite,
			@idEndpoint,
			@isInternalAPI,
			@statementId,
			@actor,
			@verbId,
			@verb,
			@activityId,
			@mboxObject,
			@mboxSha1SumObject,
			@openIdObject,
			@accountObject,
			@mboxActor,
			@mboxSha1SumActor,
			@openIdActor,
			@accountActor,
			@mboxAuthority,
			@mboxSha1SumAuthority,
			@openIdAuthority,
			@accountAuthority,
			@mboxTeam,
			@mboxSha1SumTeam,
			@openIdTeam,
			@accountTeam,
			@mboxInstructor,
			@mboxSha1SumInstructor,
			@openIdInstructor,
			@accountInstructor,
			@object,
			@registration,
			@statement,
			CASE WHEN @statementIdToBeVoided IS NOT NULL THEN 1 ELSE 0 END,
			0, --Not voided
			GETUTCDATE(),
			@scoreScaled
		)
		
		FETCH NEXT FROM statementCursor
		INTO	@statementId,
				@parentStatementId,
				@actor,
				@verbId,
				@verb,
				@activityId,
				@mboxObject,
				@mboxSha1SumObject,
				@openIdObject,
				@accountObject,
				@mboxActor,
				@mboxSha1SumActor,
				@openIdActor,
				@accountActor,
				@mboxAuthority,
				@mboxSha1SumAuthority,
				@openIdAuthority,
				@accountAuthority,
				@mboxTeam,
				@mboxSha1SumTeam,
				@openIdTeam,
				@accountTeam,
				@mboxInstructor,
				@mboxSha1SumInstructor,
				@openIdInstructor,
				@accountInstructor,
				@object,
				@registration,
				@statement,
				@statementIdToBeVoided,
				@scoreScaled
		END
	
	CLOSE statementCursor
	DEALLOCATE statementCursor
	
	IF @rollback = 1
		BEGIN
		ROLLBACK -- Rollback all the changes.
		END
	ELSE
		BEGIN
		COMMIT -- Commit all the changes.
		SELECT @Return_Code = 0 --Success
		SELECT @Error_Description_Code = NULL
		END
	
	END

