-- =====================================================================
-- PROCEDURE: [EnrollmentRequest.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EnrollmentRequest.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EnrollmentRequest.Details]
GO

/*

Return all the properties for a given enrollment request id.

*/

CREATE PROCEDURE [EnrollmentRequest.Details]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idEnrollmentRequest				INT				OUTPUT,
	@idSite								INT				OUTPUT,
	@idCourse							INT				OUTPUT,
	@idUser								INT				OUTPUT,
	@timestamp							DATETIME		OUTPUT,
	@dtApproved							DATETIME		OUTPUT,
	@dtDenied							DATETIME		OUTPUT,
	@idApprover							INT				OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblEnrollmentRequest
		WHERE idEnrollmentRequest = @idEnrollmentRequest
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentRequest_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data
	
	*/		
	
	SELECT 
		@idEnrollmentRequest				= ER.idEnrollmentRequest,
		@idSite								= ER.idSite,
		@idCourse							= ER.idCourse,
		@idUser								= ER.idUser,
		@timestamp							= ER.[timestamp],
		@dtApproved							= ER.dtApproved,
		@dtDenied							= ER.dtDenied,
		@idApprover							= ER.idApprover
	 FROM tblEnrollmentRequest ER
	 WHERE ER.idEnrollmentRequest = @idEnrollmentRequest
		
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentRequest_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO