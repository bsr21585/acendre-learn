-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.Save]
GO

/*

Adds new standup training instance or updates existing standup training instance.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [StandupTrainingInstance.Save]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idStandupTrainingInstance	INT				OUTPUT,
	@idStandupTraining			INT,
	@title						NVARCHAR(255),
	@description				NVARCHAR(MAX),
	@seats						INT,
	@waitingSeats				INT,
	@type						INT,
	@isClosed					BIT,
	@urlRegistration			NVARCHAR(255),
	@urlAttend					NVARCHAR(255),
	@city						NVARCHAR(255),
	@province					NVARCHAR(255),
	@postalcode					NVARCHAR(25),
	@locationDescription		NVARCHAR(MAX),
	@integratedObjectKey		BIGINT,
	@hostUrl					NVARCHAR(1024),
	@genericJoinUrl				NVARCHAR(1024),
	@meetingPassword			NVARCHAR(255),
	@idWebMeetingOrganizer		INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED


	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			SET @Error_Description_Code = 'CourseSave_SpecifiedLanguageNotDefault'
			RETURN 1 
		END

	*/

	/*

	check XSS vulnerabilities

	*/

	-- description

	IF (@description LIKE '%<script%')
	BEGIN
	SELECT @Return_Code = 4
	SET @Error_Description_Code = 'STISave_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@description LIKE '%<object%')
	BEGIN
	SELECT @Return_Code = 4
	SET @Error_Description_Code = 'STISave_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@description LIKE '%<frame%')
	BEGIN
	SELECT @Return_Code = 4
	SET @Error_Description_Code = 'STISave_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@description LIKE '%<iframe%')
	BEGIN
	SELECT @Return_Code = 4
	SET @Error_Description_Code = 'STISave_DescTagNotAllowed_Iframe'
	RETURN 1 
	END
		
	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*
	
	save the data
	
	*/
	
	IF (@idStandupTrainingInstance = 0 OR @idStandupTrainingInstance IS NULL)
		
		BEGIN
		
		-- insert the new standup training instance
		
		INSERT INTO tblStandupTrainingInstance (
			idStandupTraining,
			idSite,
			title,
			[description],
			seats,
			waitingSeats,
			[type],
			isClosed,
			urlRegistration,
			urlAttend,
			city,
			province,
			postalcode,
			locationDescription,
			integratedObjectKey,
			hostUrl,
			genericJoinUrl,
			meetingPassword,
			idWebMeetingOrganizer
		)			
		VALUES (
			@idStandupTraining,
			@idCallerSite,
			@title,
			@description,
			@seats,
			@waitingSeats,
			@type,
			@isClosed,
			@urlRegistration,
			@urlAttend,
			@city,
			@province,
			@postalcode,
			@locationDescription,
			@integratedObjectKey,
			@hostUrl,
			@genericJoinUrl,
			@meetingPassword,
			@idWebMeetingOrganizer
		)
		
		-- get the new standup training instance's id
		
		SELECT @idStandupTrainingInstance = SCOPE_IDENTITY()
	
		END
		
	ELSE
		
		BEGIN
		
		-- check that the standup training instance id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblStandUpTrainingInstance WHERE idStandUpTrainingInstance = @idStandupTrainingInstance AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idStandupTrainingInstance = @idStandupTrainingInstance
			SET @Return_Code = 1
			SET @Error_Description_Code = 'StandupTrainingInstanceSave_NoRecordFound'
			RETURN 1

			END	
		
		UPDATE tblStandUpTrainingInstance SET
			title = @title,
			[description] = @description,
			seats = @seats,
			[type] =@type,
			isClosed = @isClosed,
			waitingSeats = @waitingSeats,
			urlRegistration = @urlRegistration,
			urlAttend = @urlAttend,
			city = @city,
			province = @province,
			postalcode = @postalcode,
			locationDescription = @locationDescription,
			integratedObjectKey = @integratedObjectKey,
			hostUrl = @hostUrl,
			genericJoinUrl = @genericJoinUrl,
			meetingPassword = @meetingPassword,
			idWebMeetingOrganizer = @idWebMeetingOrganizer
		WHERE idStandUpTrainingInstance = @idStandupTrainingInstance
		AND idSite = @idCallerSite
		
		-- get the standup training instance's id 

		SELECT @idStandupTrainingInstance = @idStandupTrainingInstance
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblStandupTrainingInstanceLanguage STIL WHERE STIL.idStandUpTrainingInstance = @idStandupTrainingInstance AND STIL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblStandupTrainingInstanceLanguage SET
			title = @title,
			[description] = @description,
			locationDescription = @locationDescription
		WHERE idStandUpTrainingInstance = @idStandupTrainingInstance
		AND idLanguage = @idDefaultLanguage
		AND idSite = @idCallerSite

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblStandupTrainingInstanceLanguage (
			idSite,
			idStandUpTrainingInstance,
			idLanguage,
			title,
			[description],
			locationDescription
		)
		SELECT
			@idCallerSite,
			@idStandupTrainingInstance,
			@idDefaultLanguage,
			@title,
			@description,
			@locationDescription
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblStandupTrainingInstanceLanguage STIL
			WHERE STIL.idStandUpTrainingInstance = @idStandupTrainingInstance
			AND STIL.idLanguage = @idDefaultLanguage
			AND STIL.idSite = @idCallerSite
		)

		END

	SET @Error_Description_Code = ''
	SET @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO