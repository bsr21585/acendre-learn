-- =====================================================================
-- PROCEDURE: [Site.GetStatisticsForAllSites]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Site.GetStatisticsForAllSites]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.GetStatisticsForAllSites]
GO

/*
Returns the user count, disk usage, limits, and expiration information for
all sites in the database.
*/
CREATE PROCEDURE [Site.GetStatisticsForAllSites]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	CREATE TABLE #Counts
	(
		idSite INT,
		userCount INT,
		usersCurrentlyLoggedInCount INT,
		contentPackagesKb INT,
		repositoryFilesKb INT,
		reportFilesKb INT
	)

	INSERT INTO #Counts
	(
		idSite,
		userCount,
		usersCurrentlyLoggedInCount,
		contentPackagesKb,
		repositoryFilesKb,
		reportFilesKb
	)
	SELECT
		S.idSite,
		(SELECT ISNULL(COUNT(1), 0) FROM tblUser U WHERE U.idSite = S.idSite),
		(SELECT COUNT(1) FROM tblUser U WHERE U.idSite = S.idSite AND U.dtSessionExpires >= GETUTCDATE()),
		(SELECT ISNULL(SUM(kb), 0) FROM tblContentPackage CP WHERE CP.idSite = S.idSite),
		(SELECT ISNULL(SUM(kb), 0) FROM tblDocumentRepositoryItem DRI WHERE DRI.idSite = S.idSite),
		(SELECT ISNULL(SUM(htmlKb), 0) + ISNULL(SUM(csvKb), 0) + ISNULL(SUM(pdfKb), 0) FROM tblReportFile RF WHERE RF.idSite = S.idSite)
	FROM tblSite S
	WHERE S.idSite > 1

	SELECT
		S.hostname AS hostname,
		C.userCount AS userCount,
		S.userLimit AS usersAllowed,
		C.usersCurrentlyLoggedInCount AS usersCurrentlyLoggedInCount,
		CASE WHEN S.userLimit IS NULL OR S.userLimit = 0 THEN NULL ELSE CONVERT(DECIMAL(10,2), (CONVERT(DECIMAL(10,2), C.userCount) / S.userLimit)) * 100 END AS pctUserLimitUsed,
		C.contentPackagesKb + C.repositoryFilesKb + C.reportFilesKb AS kbUsed,
		S.kbLimit AS kbLimit,
		CASE WHEN S.kbLimit IS NULL OR S.kbLimit = 0 THEN NULL ELSE CONVERT(DECIMAL(10,2), ((CONVERT(DECIMAL(10,2), C.contentPackagesKb + C.repositoryFilesKb + C.reportFilesKb)) / S.kbLimit)) * 100 END AS pctkbLimitUsed,
		S.dtExpires AS dtExpires
	FROM tblSite S
	LEFT JOIN #Counts C ON C.idSite = S.idSite
	WHERE S.idSite > 1
	UNION SELECT
		'[TOTALS]' AS hostname,
		ISNULL(SUM(userCount), 0) AS userCount,
		NULL AS usersAllowed,
		ISNULL(SUM(usersCurrentlyLoggedInCount), 0) AS usersCurrentlyLoggedInCount,
		NULL AS pctUserLimitUsed,
		ISNULL(SUM(C.contentPackagesKb), 0) + ISNULL(SUM(C.repositoryFilesKb), 0) + ISNULL(SUM(C.reportFilesKb), 0)AS kbUsed,
		NULL AS kbLimit,
		NULL AS pctkbLimitUsed,
		NULL AS dtExpires
	FROM #Counts C 

	DROP TABLE #Counts
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO