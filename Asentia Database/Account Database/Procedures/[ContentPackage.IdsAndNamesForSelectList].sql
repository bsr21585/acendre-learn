-- =====================================================================
-- PROCEDURE: [ContentPackage.IdsAndNamesForSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ContentPackage.IdsAndNamesForSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ContentPackage.IdsAndNamesForSelectList]
GO

/*

Returns a recordset of content package ids and names to populate select lists.

*/
CREATE PROCEDURE [ContentPackage.IdsAndNamesForSelectList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT
			CP.idContentPackage,
			CP.name,
			CP.dtCreated
		FROM tblContentPackage CP
		WHERE CP.idSite = @idCallerSite
		ORDER BY name

		END

	ELSE

		BEGIN

		SELECT DISTINCT
			CP.idContentPackage,
			CP.name,
			CP.dtCreated
		FROM tblContentPackage CP
		INNER JOIN CONTAINSTABLE(tblContentPackage, *, @searchParam) K ON K.[key] = CP.idContentPackage
		WHERE CP.idSite = @idCallerSite
		ORDER BY name

		END
		
	SET @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO