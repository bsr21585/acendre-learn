-- =====================================================================
-- PROCEDURE: [SCORMPackage.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[SCORMPackage.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SCORMPackage.Details]
GO

/*
Return all the properties for a given id.
*/
CREATE PROCEDURE [SCORMPackage.Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idSCORMPackage			INT				OUTPUT ,
    @name					NVARCHAR(255)	OUTPUT,
    @path					NVARCHAR(MAX)	OUTPUT,
    @kb						INT				OUTPUT,
    @idSCORMPackageType		INT				OUTPUT,
    @imsManifest			NVARCHAR(MAX)	OUTPUT,
    @dtCreated				DATETIME		OUTPUT,
    @dtModified				DATETIME		OUTPUT
	
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSCORMPackage
		WHERE idSCORMPackage = @idSCORMPackage
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'SCORMPackageDetails_NoRecordFound'
		RETURN 1
		END
	
	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	/*
	
	get the data 
	
	*/
		
	SELECT
	
	@idSCORMPackage			=SP.idSCORMPackage ,
    @name					=SP.name,
    @path					=SP.path,
    @kb						=SP.kb,
    @idSCORMPackageType		=SP.idSCORMPackageType,
    @imsManifest			=SP.imsManifest,
    @dtCreated				=SP.dtCreated,
    @dtModified				=SP.dtModified
	
	FROM tblSCORMPackage SP	
	WHERE
		SP.idSCORMPackage = @idSCORMPackage
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'SCORMPackageDetails_NoRecordFound'
		END
	ELSE
		SELECT @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO