 -- =====================================================================
-- PROCEDURE: [Enrollment.SaveForMultipleCourses]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.SaveForMultipleCourses]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.SaveForMultipleCourses]
GO

/*

Adds new enrollments for multiple courses.

*/

CREATE PROCEDURE [Enrollment.SaveForMultipleCourses]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@Courses							IDTable			READONLY,
	@idUser								INT,
	@idTimezone							INT,
	@isLockedByPrerequisites			BIT,
	@dtStart							DATETIME,
	@dueInterval						INT,
	@dueTimeframe						NVARCHAR(4),
	@expiresFromStartInterval			INT,
	@expiresFromStartTimeframe			NVARCHAR(4),
	@expiresFromFirstLaunchInterval		INT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4)
)
AS

	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF (SELECT COUNT(1) FROM @Courses) = 0
		BEGIN	
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'EnrollmentSaveForMultipleCourses_NoRecordFound'
		RETURN 1
		END

	/*
	
	validate that all courses exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Courses CC
		LEFT JOIN tblCourse C ON C.idCourse = CC.id 
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
		) > 0
		
		BEGIN 	
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentSaveForMultipleCourses_NoRecordFound'
		RETURN 1 
		END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()
	
	/*

	save the data

	*/

	-- insert the new enrollments

	INSERT INTO tblEnrollment (
		idSite,
		idCourse,
		idUser,
		idTimezone,
		isLockedByPrerequisites,
		code,
		revcode,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT
		@idCallerSite,
		CC.id,
		@idUser,
		@idTimezone,
		@isLockedByPrerequisites,
		C.coursecode,
		C.revcode,
		C.title,
		@dtStart,
		CASE WHEN @dueInterval > 0 THEN
			dbo.IDateAdd(@dueTimeframe, @dueInterval, @dtStart)
		ELSE
			NULL
		END,
		CASE WHEN @expiresFromStartInterval > 0 THEN
			dbo.IDateAdd(@expiresFromStartTimeframe, @expiresFromStartInterval, @dtStart)
		ELSE
			NULL
		END,
		@utcNow,
		@dueInterval,
		@dueTimeframe,
		@expiresFromStartInterval,
		@expiresFromStartTimeframe,
		@expiresFromFirstLaunchInterval,
		@expiresFromFirstLaunchTimeframe
	FROM @Courses CC
	LEFT JOIN tblCourse C ON C.idCourse = CC.id

	/* 
	
	do the event log entries
	
	*/

	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		E.idSite,
		E.idCourse, 
		E.idEnrollment, 
		@idUser
	FROM tblEnrollment E
	WHERE E.idSite = @idCallerSite
	AND E.dtCreated = @utcNow
	AND E.idUser = @idUser
	AND EXISTS (SELECT 1 
				FROM @Courses CC
				WHERE CC.id = E.idCourse)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 201, @utcNow, @eventLogItems
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		