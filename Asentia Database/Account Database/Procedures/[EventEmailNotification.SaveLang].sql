-- =====================================================================
-- PROCEDURE: [EventEmailNotification.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventEmailNotification.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventEmailNotification.SaveLang]
GO

/*

Saves email notification "language specific" properties for specific language
in email notification language table.

*/

CREATE PROCEDURE [EventEmailNotification.SaveLang]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0, --will fail if not specified
	
	@idEventEmailNotification	INT, 
	@languageString				NVARCHAR(10),
	@name						NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EENSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
		
	/*
	
	validate that the email notification exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblEventEmailNotification
		WHERE idEventEmailNotification = @idEventEmailNotification
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EventEmailNotificationSaveLang_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblEventEmailNotificationLanguage EL WHERE EL.idEventEmailNotification = @idEventEmailNotification AND EL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblEventEmailNotificationLanguage SET
			name = @name
		WHERE idEventEmailNotification = @idEventEmailNotification
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblEventEmailNotificationLanguage (
			idSite,
			idEventEmailNotification,
			idLanguage,
			name
		)
		SELECT
			@idCallerSite,
			@idEventEmailNotification,
			@idLanguage,
			@name
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblEventEmailNotificationLanguage EL
			WHERE EL.idEventEmailNotification = @idEventEmailNotification
			AND EL.idLanguage = @idLanguage
		)

		END
	
	/*

	if the language we're saving in is also the site's default language, save it in the base table
	note: uniqueness should already be validated

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblEventEmailNotification SET
			name = @name
		WHERE idEventEmailNotification = @idEventEmailNotification

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO