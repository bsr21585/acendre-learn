-- =====================================================================
-- PROCEDURE: [StandUpTrainingInstanceToUserLink.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandUpTrainingInstanceToUserLink.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandUpTrainingInstanceToUserLink.Delete]
GO

/*

Deletes document repository item(s)

*/

CREATE PROCEDURE [StandUpTrainingInstanceToUserLink.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT,
	@idUser						INT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
		
	/*
	
	Delete the records for the corresponding session and id
	
	*/
	
	DELETE FROM [dbo].[tblStandupTrainingInstanceToUserLink]
	WHERE idStandupTrainingInstance = @idStandupTrainingInstance
	AND idUser = @idUser
	
	SELECT @Return_Code = 0
	SELECT @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO