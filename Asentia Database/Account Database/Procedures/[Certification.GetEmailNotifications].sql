-- =====================================================================
-- PROCEDURE: [Certification.GetEmailNotifications]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.GetEmailNotifications]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.GetEmailNotifications]
GO

/*

Returns a recordset of email notification ids and names that belong to a certification.

*/

CREATE PROCEDURE [Certification.GetEmailNotifications]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCertification		INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL

		BEGIN

		SELECT
			DISTINCT
			EN.idEventEmailNotification,
			CASE WHEN ENL.name IS NOT NULL THEN ENL.name ELSE EN.name END AS name
		FROM tblEventEmailNotification EN
		LEFT JOIN tblEventEmailNotificationLanguage ENL ON ENL.idEventEmailNotification = EN.idEventEmailNotification AND ENL.idLanguage = @idCallerLanguage
		WHERE EN.idSite = @idCallerSite
		AND EN.idObject = @idCertification
		AND (EN.idEventType >= 600 AND EN.idEventType < 700)
		ORDER BY name

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			EN.idEventEmailNotification,
			CASE WHEN ENL.name IS NOT NULL THEN ENL.name ELSE EN.name END AS name
		FROM tblEventEmailNotificationLanguage ENL
		INNER JOIN CONTAINSTABLE(tblEventEmailNotificationLanguage, *, @searchParam) K ON K.[key] = ENL.idEventEmailNotificationLanguage AND ENL.idLanguage = @idCallerLanguage
		LEFT JOIN tblEventEmailNotification EN ON EN.idEventEmailNotification = ENL.idEventEmailNotification
		WHERE EN.idSite = @idCallerSite
		AND EN.idObject = @idCertification
		AND (EN.idEventType >= 600 AND EN.idEventType < 700)
		ORDER BY name

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	