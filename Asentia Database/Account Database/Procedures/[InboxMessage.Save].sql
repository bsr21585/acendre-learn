-- =====================================================================
-- PROCEDURE: [InboxMessage.Save]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[InboxMessage.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [InboxMessage.Save]
GO
/*

Saves the composition of an inbox message that has NOT been sent

*/

CREATE PROCEDURE [InboxMessage.Save]
(
	@Return_Code				INT						OUTPUT,
	@Error_Description_Code		NVARCHAR(50)			OUTPUT,
	@idCallerSite				INT				= 0, 
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,
	
	@idInboxMessage				INT						OUTPUT, 
	@idParentInboxMessage		INT, 
	@idRecipient				INT,
	@idSender					INT,
	@subject					NVARCHAR(255),
	@message					NVARCHAR(MAX),
	@idSite                     INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	save the data
	
	*/
	
	IF (@idInboxMessage = 0 OR @idInboxMessage is null)
		
		BEGIN
		
		-- compose the new message
		
		INSERT INTO tblInboxMessage (
			idParentInboxMessage, 
			idRecipient,
			idSender,
			[subject],
			[message],
			[dtCreated],
			idSite,
			isDraft,
			isSent
		)			
		VALUES (
			@idParentInboxMessage, 
			@idRecipient,
			@idSender,
			@subject,
			@message,
			GETUTCDATE(),
			@idCallerSite,
			1,
			0
			
		)
		
		-- get the new learning path's id and return successfully
		
		SELECT @idInboxMessage = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the message id exists and that it can still be modified
		
		IF (
			SELECT COUNT(1) 
			FROM tblInboxMessage 
			WHERE idInboxMessage = @idInboxMessage
			) < 1
			
			BEGIN
				SELECT @Return_Code = 1
				SET @Error_Description_Code = 'InboxMessageSave_NoRecordFound'
				RETURN 1
			END
			
		-- update existing message composition
		-- NOTE: recipient ID will not be changed when updating a reply
		
		UPDATE tblInboxMessage SET
			idRecipient		= CASE WHEN idParentInboxMessage IS NULL THEN @idRecipient ELSE idRecipient END,
			[subject]		= @subject,
			[message]		= @message,
			[dtCreated]		= GETUTCDATE(),
			 idSite = @idCallerSite,
			 isDraft = 1,
			 isSent = 0
			
		WHERE idInboxMessage = @idInboxMessage
		-- get the id and return successfully
		
		SELECT @idInboxMessage = @idInboxMessage
				
		END
	
	SELECT @Return_Code = 0
		
	END
		
