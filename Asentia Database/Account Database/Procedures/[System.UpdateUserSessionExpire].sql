-- =====================================================================
-- PROCEDURE: [System.UpdateUserSessionExpire]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF exists (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.UpdateUserSessionExpire]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.UpdateUserSessionExpire]
GO

/*

Updates dtSessionExpires value for user.

*/
CREATE PROCEDURE [System.UpdateUserSessionExpire]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@dtSessionExpires		DATETIME		   
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	just return if the caller is 1 (admin)

	*/

	IF (@idCaller = 1)
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		RETURN 1
		END
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUser
		WHERE idUser = @idCaller
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'SystemUpdateUserSessionExpire_NoRecordFound'
		RETURN 1
		END

	-- block the trigger from executing in tblUser UPDATE
	DECLARE @CONTEXT VARBINARY(128)

	SET @CONTEXT = CONVERT(VARBINARY(128), 'BLOCK TRIGGER')
	SET CONTEXT_INFO @CONTEXT

	-- update user last login date
	UPDATE tblUser SET 
		dtSessionExpires = @dtSessionExpires 
	WHERE idUser = @idCaller

	-- unblock the trigger
	SET @CONTEXT = CONVERT(VARBINARY(128), '')
	SET CONTEXT_INFO @CONTEXT
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1

	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO