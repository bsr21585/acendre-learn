-- =====================================================================
-- PROCEDURE: [[OBJECT].SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[[OBJECT].SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [[OBJECT].SaveLang]
GO

/*

Saves [OBJECT] "language specific" properties for specific language
in [OBJECT] language table.

*/

CREATE PROCEDURE [[OBJECT].SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@id[OBJECT]				INT, 
	@languageString			NVARCHAR(10),
	-- LIST OBJECT'S LANGUAGE SPECIFIC PARAMS BELOW AND REMOVE THIS COMMENT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
		
	/*
	
	validate caller permission
	
	*/
	
	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = '[OBJECT]SaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
		
	/*
	
	validate that the [OBJECT] exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tbl[OBJECT]
		WHERE id[OBJECT] = @id[OBJECT]
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0) -- REMOVE THIS WHERE THE OBJECT'S TABLE DOES NOT HAVE A DELETE FLAG
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = '[OBJECT]SaveLang_DetailsNotFound'
		RETURN 1 
		END
		
	/*

	PERFORM ANY VALIDATIONS FOR BUSINESS RULES - UNIQUENESS, XSS VULNERABILITIES FOR DESCTIPTIONS, ETC.
	REMOVE THIS COMMENT BLOCK, REPLACING IT WITH YOUR VALIDATIONS FOR THE OBJECT AND APPROPRIATE COMMENTS

	*/
		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tbl[OBJECT]Language _L WHERE _L.id[OBJECT] = @id[OBJECT] AND _L.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tbl[OBJECT]Language SET
			-- LANGUAGE SPECIFIC FIELDS - REMOVE THIS COMMENT
		WHERE id[OBJECT] = @id[OBJECT]
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tbl[OBJECT]Language (
			idSite,
			id[OBJECT],
			idLanguage,
			-- LANGUAGE SPECIFIC FIELDS - REMOVE THIS COMMENT
		)
		SELECT
			@idCallerSite,
			@id[OBJECT],
			@idLanguage,
			-- LANGUAGE SPECIFIC FIELD PARAMS - REMOVE THIS COMMENT
		WHERE NOT EXISTS (
			SELECT 1
			FROM tbl[OBJECT]Language _L
			WHERE _L.id[OBJECT] = @id[OBJECT]
			AND _L.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tbl[OBJECT] SET
			-- LANGUAGE SPECIFIC FIELDS - REMOVE THIS COMMENT
		WHERE id[OBJECT] = @id[OBJECT]

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO