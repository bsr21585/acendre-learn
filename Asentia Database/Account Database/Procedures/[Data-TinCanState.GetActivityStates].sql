-- =====================================================================
-- PROCEDURE: [Data-TinCanState.GetActivityStates]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCanState.GetActivityStates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCanState.GetActivityStates]
GO

/*

Returns activity states.

*/

CREATE PROCEDURE [Data-TinCanState.GetActivityStates]
(
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idEndpoint				INT,
	@activityId				NVARCHAR(100),
	@agent					NVARCHAR(MAX),
	@registration			NVARCHAR(50)
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @end INT
	
	/*
	
	Get the data
	
	*/
	
	SELECT	DISTINCT stateId
	FROM	[tblData-TinCanState]
	WHERE	idSite = @idCallerSite
	AND		idEndpoint = @idEndpoint
	AND		activityId = @activityId
	AND		agent = @agent
	AND		(@registration IS NULL OR registration = @registration)
	
	SELECT @Return_Code = 0 --Success
	SELECT @Error_Description_Code = NULL
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO