-- =====================================================================
-- PROCEDURE: [StandupTraining.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.Delete]
GO

/*

Deletes standup training

*/

CREATE PROCEDURE [StandupTraining.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@StandupTrainings		IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @StandupTrainings) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'StandupTrainingDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all standup training exists within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @StandupTrainings STST
		LEFT JOIN tblStandupTraining ST ON ST.idStandUpTraining = STST.id
		WHERE ST.idSite IS NULL
		OR ST.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'StandupTrainingDelete_NoRecordFound'
		RETURN 1 
		END
	
	/*
	
	DELETE the standup training(s), and all related objects.
	
	*/
	
	/*

	SOFT DELETE the standup training(s), just mark them as deleted, DO NOT remove the record!

	*/

	UPDATE tblStandUpTraining SET isDeleted = 1, dtDeleted = GETUTCDATE() WHERE idStandUpTraining IN (SELECT id from @StandupTrainings)

	/*

	SOFT DELETE the standup training instance(s) related to the standup training(s) that we are deleting, where they are not already deleted

	*/

	UPDATE tblStandUpTrainingInstance SET isDeleted = 1, dtDeleted = GETUTCDATE() WHERE idStandUpTraining IN (SELECT id FROM @StandupTrainings) AND (isDeleted IS NULL OR isDeleted = 0)

	/* DO NOT DELETE THE MEETING TIMES - AT LEAST FOR NOW */

	--DELETE FROM tblStandupTrainingInstanceMeetingTime
	--WHERE idStandUpTrainingInstance IN (
	--	SELECT idStandUpTrainingInstance
	--	FROM tblStandupTrainingInstance
	--	WHERE idStandUpTraining IN (
	--		SELECT id
	--		FROM @StandupTrainings
	--	)
	--)

	/*

	DELETE all links to lesson content

	*/

	DELETE FROM tblLessonToContentLink WHERE idObject IN (SELECT id FROM @StandupTrainings) AND idContentType = 2 -- 2 is standup training content

	/*

	DELETE all instance links to resources

	*/

	DELETE FROM tblResourceToObjectLink WHERE idObject IN (SELECT idStandUpTrainingInstance FROM tblStandupTrainingInstance WHERE idStandUpTraining IN (SELECT id FROM @StandupTrainings))	

	/*

	DELETE all instance links to users where they are not completed

	*/

	DELETE FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance IN (SELECT idStandUpTrainingInstance FROM tblStandupTrainingInstance WHERE idStandUpTraining IN (SELECT id FROM @StandupTrainings)) AND dtCompleted IS NULL

	/*

	SOFT DELETE all the entries in [tblEventEmailNotification] related to standup training(s) which we are deleting, where they are not already deleted

	*/

	UPDATE tblEventEmailNotification SET isDeleted = 1 WHERE idObject IN (SELECT id FROM @StandupTrainings) AND (isDeleted IS NULL OR isDeleted = 0)
	AND idEventType >= 400 AND idEventType < 500 -- 400 - 500 is standup training	
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO