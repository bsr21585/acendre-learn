-- =====================================================================
-- PROCEDURE: [Catalog.SaveAvatar]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.SaveAvatar]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.SaveAvatar]
GO

/*

Saves a catalog's avatar path to the database.

*/

CREATE PROCEDURE [Catalog.SaveAvatar]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCatalog				INT, 
	@avatar					NVARCHAR(255)
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	update the catalog's avatar path

	*/
		
	-- check that the catalog id exists
	IF (SELECT COUNT(1) FROM tblCatalog WHERE idCatalog = @idCatalog AND idSite = @idCallerSite) < 1
		BEGIN                               
			
		SET @idCatalog = @idCatalog
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'CatalogSaveAvatar_NoRecordFound'
		RETURN 1
			
		END
			
		
	-- update existing catalog's properties
		
	UPDATE tblCatalog SET
		avatar = @avatar
	WHERE idCatalog = @idCatalog

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END			

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO