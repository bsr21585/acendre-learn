-- =====================================================================
-- PROCEDURE: [InboxMessage.DeleteDraft]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[InboxMessage.DeleteDraft]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [InboxMessage.DeleteDraft]
GO

/*

Deletes Draft Messages

*/

CREATE PROCEDURE [InboxMessage.DeleteDraft]
(
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, 
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@InboxMessages			IDTable					READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @InboxMessages) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'InboxMessageDeleteDraft_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all messages belong to the caller
	
	*/
	
	IF (
	SELECT COUNT(1)
		FROM @InboxMessages III
		LEFT JOIN tblInboxMessage IM ON IM.idInboxMessage = III.id
		WHERE  IM.idSender <> @idCaller
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'InboxMessageDeleteDraft_NoRecordFound'
		RETURN 1 
		END
		
		
	DELETE FROM tblInboxMessage        
	WHERE idInboxMessage IN (
		SELECT id
		FROM @InboxMessages)
		
		
	SELECT @Return_Code = 0
	
	END
		
