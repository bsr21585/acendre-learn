-- =====================================================================
-- PROCEDURE: [CertificateImport.Import]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificateImport.Import]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificateImport.Import]
GO

-- =============================================
-- Author:		Daniel Ko
-- Modified date: July 22nd 2015
-- Description:	This Stored Procedure is for the purpose of Importing the Certificate Sheet in Batch
-- =============================================
CREATE PROCEDURE [dbo].[CertificateImport.Import]
	-- Add the parameters for the stored procedure here	
    @Return_Code				INT				OUTPUT ,
    @Error_Description_Code		NVARCHAR(50)	OUTPUT ,
    @idCallerSite				INT				= 0,
    @callerLangString			NVARCHAR(10) ,
    @idCaller					INT				= 0,
    
	@idCertificateImport			INT ,
    @CertificateImportTable		CertificateImport	READONLY

AS 
   BEGIN
    SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
    SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

		/*

		initialize
		get import record, validate site, get timezone information

		*/

		DECLARE @idSite INT
		DECLARE @idUser INT			-- the importing user
		DECLARE @idUserImported INT -- the user the import is for (null = multiple users)
		DECLARE @idTimeZone INT
		DECLARE @gmtOffset INT
		DECLARE @tzUseDST BIT

		SELECT 
			@idSite = idSite,
			@idUser = idUser,
			@idUserImported = idUserImported
		FROM
			tblCertificateImport
		WHERE
			idCertificateImport = @idCertificateImport 

		-- grab the importing user's timezone; if admin, grab the site timezone
		IF @idUser = 1
			BEGIN	
			SELECT @idTimezone = idTimezone 
			FROM tblSite
			WHERE idSite = @idSite
			END
	
		IF @idUser > 1
			BEGIN	
			SELECT @idTimezone = idTimezone
			FROM tblUser
			WHERE idUser = @idUser
			AND idSite = @idSite
			END
	
		-- if user doesnt have a timezone, get the site's
		IF @idTimeZone IS NULL
			BEGIN	
			SELECT @idTimezone = idTimezone 
			FROM tblSite
			WHERE idSite = @idSite
			END
				
		-- if all else fails, bye bye
		IF @idTimezone is null
			BEGIN	
			-- remove the record of the Certificate import
			-- another procedure will clean up raw data
			DELETE FROM tblCertificateImport 
			WHERE idCertificateImport = @idCertificateImport
	
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertificateImportImport_SiteTimezoneNotFound'
			RETURN 1
				
			END
	
		-- get the gmt offset from the timezone
		SELECT @gmtOffset = gmtOffset
		FROM tblTimezone
		WHERE idTimezone = @idTimezone

		-- get whether or not the timezone uses dst
		SELECT @tzUseDST = blnUseDaylightSavings
		FROM tblTimeZone
		WHERE idTimezone = @idTimezone
					
	/*
	
	validate that the specified language is the default language for the site
	
	*/
	
        DECLARE @idCallerLanguage INT
        SELECT  @idCallerLanguage = idLanguage
        FROM    tblLanguage
        WHERE   code = @callerLangString
	
        IF ( SELECT COUNT(1)
             FROM   tblSite S
             WHERE  idSite = @idCallerSite
                    AND idLanguage = @idCallerLanguage
                    AND @idCallerLanguage IS NOT NULL
           ) <> 1 
            BEGIN 
                SELECT  @Return_Code = 5
                SET @Error_Description_Code = 'CertificateImportImport_LanguageNotDefault'
                RETURN 1 
            END			 
		
	/*	
	save the data	
	*/   
    
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.  
	-- INSERT statements for tblCertificateRecord
       	INSERT INTO tblCertificateRecord (
		idCertificate,
		idSite,
		idUser,
		idAwardedBy, 
		[timestamp], 
		expires, 
		idTimezone, 
		code, 
		credits, 
		idCertificateImport
		)
	SELECT
		C.idCertificate, 
		@idSite,
		U.idUser,
		@idUser, 
		--CIRD.certificateAwardDate, -- needs to be converted still
		CASE WHEN dbo.isDateDST(CI.certificateAwardDate) = 1 AND @tzUseDST = 1 
			THEN DATEADD(HOUR, -1, DATEADD(HOUR, @gmtOffset * -1, CI.certificateAwardDate)) 
			ELSE DATEADD(HOUR, @gmtOffset * -1, CI.certificateAwardDate)
			END AS certificateAwardDate,
		CASE C.expiresTimeframe
			WHEN 'yyyy' THEN DATEADD(YEAR, C.expiresInterval, CI.certificateAwardDate)
			WHEN 'm' THEN DATEADD(MONTH, C.expiresInterval, CI.certificateAwardDate)
			WHEN 'ww' THEN DATEADD(WEEK, C.expiresInterval, CI.certificateAwardDate)
			WHEN 'd' THEN DATEADD(DAY, C.expiresInterval, CI.certificateAwardDate)
			ELSE null
			END as expires,
		@idTimezone AS idTimezone, 
		CI.certificateCode,
		C.credits,
		@idCertificateImport
	FROM  @CertificateImportTable CI
	LEFT JOIN tblCertificate C ON C.code = CI.certificateCode AND C.idSite = @idSite
	LEFT JOIN tblUser U ON U.username = CI.username AND U.idSite = @idSite
                                       
		SELECT  @Return_Code = 0
		SET @Error_Description_Code = 'CertificateImportImport_SuccessfullyComplete'
		RETURN 1                                   

    END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

