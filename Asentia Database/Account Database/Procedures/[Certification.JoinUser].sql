-- =====================================================================
-- PROCEDURE: [Certification.JoinUser]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.JoinUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.JoinUser]
GO

CREATE PROCEDURE [Certification.JoinUser]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	--   default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, 
		
	@idCertification		INT,
	@idUser					INT	
)
AS

	BEGIN
	
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*

	validate certification exists
	
	*/

	IF (
		SELECT COUNT(1)
		FROM tblCertification
		WHERE idCertification = @idCertification
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationJoinUser_DetailsNotFound'
		RETURN 1
		END
	
	/*
	
	attach the certification to the user where it does not already exist
	
	*/

	INSERT INTO tblCertificationToUserLink (
		idSite,
		idCertification,
		idUser,
		certificationTrack
	)
	SELECT 
		@idCallerSite,
		@idCertification, 
		@idUser,
		0
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblCertificationToUserLink CUL
		WHERE CUL.idCertification = @idCertification
		AND CUL.idUser = @idUser
	)

	/*

	do the event log entry for join

	*/

	DECLARE @idCertificationToUserLink INT
	SET @idCertificationToUserLink = SCOPE_IDENTITY()

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	DECLARE @eventLogJoinedItems EventLogItemObjects

	INSERT INTO @eventLogJoinedItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		@idCertification,
		@idCertificationToUserLink,
		@idUser	

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 601, @utcNow, @eventLogJoinedItems

	/*

	evaluate certification completion

	*/	

	EXEC [Certification.EvaluateCompletion] NULL, NULL, @idCallerSite, @callerLangString, 1, @idCertificationToUserLink, @idUser

	/*

	return

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

	
