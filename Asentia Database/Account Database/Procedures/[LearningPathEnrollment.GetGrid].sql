-- =====================================================================
-- PROCEDURE: [LearningPathEnrollment.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPathEnrollment.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPathEnrollment.GetGrid]
GO

/*

Gets a listing of learning path enrollments for a specified user.

*/

CREATE PROCEDURE [LearningPathEnrollment.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,
	
	@idUser					INT
)
AS
	BEGIN
		SET NOCOUNT ON

		CREATE TABLE #LearningPathEnrollments (
			idLearningPathEnrollment			INT,
			idLearningPath						INT,
			title								NVARCHAR(255),
			idRuleSetLearningPathEnrollment		INT,
			dtStart								DATETIME,
			dtDue								DATETIME,
			dtExpires							DATETIME,
			dtCompleted							DATETIME,
			dtFirstLaunch						DATETIME
		)

		INSERT INTO #LearningPathEnrollments (
			idLearningPathEnrollment,
			idLearningPath,
			title,
			idRuleSetLearningPathEnrollment,
			dtStart,
			dtDue,
			dtExpires,
			dtCompleted,
			dtFirstLaunch
		)
		SELECT	
			LPE.idLearningPathEnrollment,
			LPE.idLearningPath,
			LPE.title,
			LPE.idRuleSetLearningPathEnrollment,
			LPE.dtStart,
			LPE.dtDue,
			CASE WHEN LPE.dtExpiresFromStart IS NULL AND LPE.dtExpiresFromFirstLaunch IS NULL THEN
				NULL
			ELSE
				CASE WHEN LPE.dtExpiresFromStart IS NULL AND LPE.dtExpiresFromFirstLaunch IS NOT NULL THEN
					LPE.dtExpiresFromFirstLaunch
				ELSE
					CASE WHEN LPE.dtExpiresFromStart IS NOT NULL AND LPE.dtExpiresFromFirstLaunch IS NULL THEN
						LPE.dtExpiresFromStart
					ELSE
						CASE WHEN LPE.dtExpiresFromStart >= LPE.dtExpiresFromFirstLaunch THEN
							LPE.dtExpiresFromFirstLaunch
						ELSE
							LPE.dtExpiresFromStart
						END
					END
				END
			END AS [dtExpires],
			LPE.dtCompleted,
			LPE.dtFirstLaunch
		FROM tblLearningPathEnrollment LPE
		WHERE
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND LPE.idSite = @idCallerSite)
			)
		AND LPE.idUser = @idUser	

		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #LearningPathEnrollments LPE			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						LPE.idLearningPathEnrollment,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN dtStart END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtStart', 'dtExpires', 'dtDue') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtStart END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtDue END) END DESC,
							-- FORTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtExpires END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN dtStart END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue') THEN title END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtStart', 'dtExpires', 'dtExpires') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtStart END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtDue END) END,
							-- FORTH ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtExpires END) END
						)
						AS [row_number]
					FROM #LearningPathEnrollments LPE									
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idLearningPathEnrollment, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				LPE.idLearningPathEnrollment,	
				LPE.idLearningPath,
				LPE.title,
				CASE WHEN LPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN
					'rse_onetime'
				ELSE
					'onetime'
				END AS [type],
				LPE.dtStart,
				LPE.dtDue,
				LPE.dtExpires,
				CASE WHEN LPE.dtCompleted IS NOT NULL THEN
					2
				ELSE
					CASE WHEN (LPE.dtExpires IS NOT NULL AND LPE.dtExpires <= GETUTCDATE()) THEN
						4
					ELSE
						CASE WHEN LPE.dtStart > GETUTCDATE() THEN
							5
						ELSE
							CASE WHEN (LPE.dtDue IS NOT NULL AND LPE.dtDue <= GETUTCDATE()) THEN
								3
							ELSE
								1
							END
						END
					END
				END AS [status],
				convert(bit, 1) AS isActivityOn,
				convert(bit, 1) AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #LearningPathEnrollments LPE ON LPE.idLearningPathEnrollment = SelectedKeys.idLearningPathEnrollment
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
			LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = LPE.idRuleSetLearningPathEnrollment
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #LearningPathEnrollments LPE
			INNER JOIN CONTAINSTABLE(tblLearningPathEnrollment, *, @searchParam) K ON K.[key] = LPE.idLearningPathEnrollment			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						LPE.idLearningPathEnrollment,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN dtStart END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtStart', 'dtExpires', 'dtDue') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtStart END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtDue END) END DESC,
							-- FORTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtExpires END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN dtStart END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue') THEN title END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtStart', 'dtExpires', 'dtExpires') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtStart END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtDue END) END,
							-- FORTH ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtExpires END) END
						)
						AS [row_number]
					FROM #LearningPathEnrollments LPE
					INNER JOIN CONTAINSTABLE(tblLearningPathEnrollment, *, @searchParam) K ON K.[key] = LPE.idLearningPathEnrollment						
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idLearningPathEnrollment, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				LPE.idLearningPathEnrollment,	
				LPE.idLearningPath,
				LPE.title,
				CASE WHEN LPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN
					'rse_onetime'
				ELSE
					'onetime'
				END AS [type],
				LPE.dtStart,
				LPE.dtDue,
				LPE.dtExpires,
				CASE WHEN LPE.dtCompleted IS NOT NULL THEN
					2
				ELSE
					CASE WHEN (LPE.dtExpires IS NOT NULL AND LPE.dtExpires <= GETUTCDATE()) THEN
						4
					ELSE
						CASE WHEN LPE.dtStart > GETUTCDATE() THEN
							5
						ELSE
							CASE WHEN (LPE.dtDue IS NOT NULL AND LPE.dtDue <= GETUTCDATE()) THEN
								3
							ELSE
								1
							END
						END
					END
				END AS [status],
				convert(bit, 1) AS isActivityOn,
				convert(bit, 1) AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #LearningPathEnrollments LPE ON LPE.idLearningPathEnrollment = SelectedKeys.idLearningPathEnrollment
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
			LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = LPE.idRuleSetLearningPathEnrollment
			ORDER BY SelectedKeys.[row_number]
			
			END

		-- DROP THE TEMPORARY TABLE
		DROP TABLE #LearningPathEnrollments
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO