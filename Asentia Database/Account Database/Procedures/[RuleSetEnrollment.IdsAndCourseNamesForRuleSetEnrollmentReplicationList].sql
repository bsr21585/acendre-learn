-- =====================================================================
-- PROCEDURE: [RuleSetEnrollment.IdsAndCourseNamesForRuleSetEnrollmentReplicationList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.IdsAndCourseNamesForRuleSetEnrollmentReplicationList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetEnrollment.IdsAndCourseNamesForRuleSetEnrollmentReplicationList]
GO

/*

Returns a recordset of course ids and course titles to populate
"replicate to other courses" checkbox list for a course ruleset enrollment.

*/

CREATE PROCEDURE [RuleSetEnrollment.IdsAndCourseNamesForRuleSetEnrollmentReplicationList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idRuleSetEnrollment	INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT
			(SELECT idRuleSetEnrollment FROM tblRuleSetEnrollment WHERE idParentRuleSetEnrollment = @idRuleSetEnrollment and idCourse = C.idCourse) AS [idRuleSetEnrollment],
			C.idCourse, 
			CASE WHEN C.coursecode IS NOT NULL THEN
				C.coursecode + ' ' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			ELSE
				CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			END AS title,
			(SELECT idParentRuleSetEnrollment FROM tblRuleSetEnrollment WHERE idParentRuleSetEnrollment = @idRuleSetEnrollment and idCourse = C.idCourse) AS [idParentRuleSetEnrollment]
		FROM tblCourse C
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage 
		WHERE C.idSite = @idCallerSite 
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)
		ORDER BY title
		END
			
	ELSE

		BEGIN

		SELECT DISTINCT
			(SELECT idRuleSetEnrollment FROM tblRuleSetEnrollment WHERE idParentRuleSetEnrollment = @idRuleSetEnrollment and idCourse = C.idCourse) AS [idRuleSetEnrollment],
			C.idCourse, 
			CASE WHEN C.coursecode IS NOT NULL THEN
				C.coursecode + ' ' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			ELSE
				CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			END AS title,
			(SELECT idParentRuleSetEnrollment FROM tblRuleSetEnrollment WHERE idParentRuleSetEnrollment = @idRuleSetEnrollment and idCourse = C.idCourse) AS [idParentRuleSetEnrollment]
		FROM tblCourse C
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		INNER JOIN CONTAINSTABLE(tblCourseLanguage, *, @searchParam) K ON K.[key] = CL.idCourseLanguage AND CL.idLanguage = @idCallerLanguage
		WHERE C.idSite = @idCallerSite
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)
		ORDER BY title

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO