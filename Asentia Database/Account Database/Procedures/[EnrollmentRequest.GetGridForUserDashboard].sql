-- =====================================================================
-- PROCEDURE: [EnrollmentRequest.GetGridForUserDashboard]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[EnrollmentRequest.GetGridForUserDashboard]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EnrollmentRequest.GetGridForUserDashboard]
GO

/*

Gets a listing of user enrollment requests for display on user's dashboard. 

*/

CREATE PROCEDURE [EnrollmentRequest.GetGridForUserDashboard]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	-- NOTE THAT WE ARE NOT USING THE SEARCH, PAGINATION, OR SORT HERE BUT
	-- SINCE THESE ARE COMMON PARAMETERS FOR GRIDS, WE NEED TO KEEP THEM
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	put the pending requests into a temporary table

	*/

	CREATE TABLE #PendingEnrollmentRequests (
		idEnrollmentRequest						INT,
		idSite									INT,
		idCourse								INT,
		idUser									INT,
		dtRequested								DATETIME,
		title									NVARCHAR(255)
	)

	INSERT INTO #PendingEnrollmentRequests (
		idEnrollmentRequest,
		idSite,
		idCourse,
		idUser,
		dtRequested,
		title
	)
	SELECT
		ER.idEnrollmentRequest,
		ER.idSite,
		ER.idCourse,
		ER.idUser,
		ER.[timestamp],
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
	FROM tblEnrollmentRequest ER
	LEFT JOIN tblCourse C ON ER.idCourse = C.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	WHERE ER.idUser = @idCaller 
	AND ER.idSite = @idCallerSite
	AND ER.dtApproved IS NULL 
	AND ER.dtDenied IS NULL				
		
	-- return the rowcount and grid data

	SELECT COUNT(1) AS row_count
	FROM #PendingEnrollmentRequests

	;WITH
		Keys AS (
			SELECT TOP (@pageNum * @pageSize)
				ER.idEnrollmentRequest,
				ROW_NUMBER() OVER (ORDER BY ER.idEnrollmentRequest DESC)
				AS [row_number]
			FROM #PendingEnrollmentRequests ER
		),

		SelectedKeys AS (
			SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
				idEnrollmentRequest, 
				[row_number]
			FROM Keys
			WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
		)
	SELECT
		ER.idEnrollmentRequest,
		ER.idSite,
		ER.idCourse,
		ER.idUser,
		ER.dtRequested,
		ER.title,
		C.avatar	
	FROM #PendingEnrollmentRequests ER
	LEFT JOIN tblCourse C ON C.idCourse = ER.idCourse

	-- DROP THE TEMPORARY TABLE
	DROP TABLE #PendingEnrollmentRequests
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO