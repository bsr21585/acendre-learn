-- =====================================================================
-- PROCEDURE: [User.Details]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.Details]
GO

/*

Returns all the properties for a given user id.

*/
CREATE PROCEDURE [User.Details]
(
	@Return_Code				INT					OUTPUT,
	@Error_Description_Code		NVARCHAR(50)		OUTPUT,
	@idCallerSite				INT					= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT					= 0,
	
	@idUser						INT					OUTPUT,
	@firstName					NVARCHAR(255)		OUTPUT,
	@middleName					NVARCHAR(255)		OUTPUT,
	@lastName					NVARCHAR(255)		OUTPUT,
	@displayName				NVARCHAR(768)		OUTPUT,
	@avatar						NVARCHAR(255)		OUTPUT,
	@email						NVARCHAR(255)		OUTPUT,
	@username					NVARCHAR(512)		OUTPUT,
	--@password					NVARCHAR(512)		OUTPUT,
	@idSite						INT					OUTPUT,
	@idTimezone					INT					OUTPUT,
	@objectGUID					UNIQUEIDENTIFIER	OUTPUT,
	@activationGUID				UNIQUEIDENTIFIER	OUTPUT,
	@distinguishedName			NVARCHAR(512)		OUTPUT,
	@isActive					BIT					OUTPUT,
	@mustChangePassword			BIT					OUTPUT,
	@dtCreated					DATETIME			OUTPUT,
	@dtExpires					DATETIME			OUTPUT,
	@isDeleted					INT					OUTPUT,
	@dtDeleted					DATETIME			OUTPUT,
	@idLanguage					INT					OUTPUT,
	@languageString				NVARCHAR(10)		OUTPUT,
	@dtModified					DATETIME			OUTPUT,
	@dtLastLogin				DATETIME			OUTPUT,
	@employeeId					NVARCHAR(255)		OUTPUT,
	@company					NVARCHAR(255)		OUTPUT,
	@address					NVARCHAR(512)		OUTPUT,
	@city						NVARCHAR(255)		OUTPUT,
	@province					NVARCHAR(255)		OUTPUT,
	@postalCode					NVARCHAR(25)		OUTPUT,
	@country					NVARCHAR(50)		OUTPUT,
	@phonePrimary				NVARCHAR(25)		OUTPUT,
	@phoneWork					NVARCHAR(25)		OUTPUT,
	@phoneFax					NVARCHAR(25)		OUTPUT,
	@phoneHome					NVARCHAR(25)		OUTPUT,
	@phoneMobile				NVARCHAR(25)		OUTPUT,
	@phonePager					NVARCHAR(25)		OUTPUT,
	@phoneOther					NVARCHAR(25)		OUTPUT,
	@department					NVARCHAR(255)		OUTPUT,
	@division					NVARCHAR(255)		OUTPUT,
	@region						NVARCHAR(255)		OUTPUT,
	@jobTitle					NVARCHAR(255)		OUTPUT,
	@jobClass					NVARCHAR(255)		OUTPUT,
	@gender						NVARCHAR(1)			OUTPUT,
	@race						NVARCHAR(64)		OUTPUT,
	@dtDOB						DATETIME			OUTPUT,
	@dtHire						DATETIME			OUTPUT,
	@dtTerm						DATETIME			OUTPUT,
	@field00					NVARCHAR(255)		OUTPUT,
	@field01					NVARCHAR(255)		OUTPUT,
	@field02					NVARCHAR(255)		OUTPUT,
	@field03					NVARCHAR(255)		OUTPUT,
	@field04					NVARCHAR(255)		OUTPUT,
	@field05					NVARCHAR(255)		OUTPUT,
	@field06					NVARCHAR(255)		OUTPUT,
	@field07					NVARCHAR(255)		OUTPUT,
	@field08					NVARCHAR(255)		OUTPUT,
	@field09					NVARCHAR(255)		OUTPUT,
	@field10					NVARCHAR(255)		OUTPUT,
	@field11					NVARCHAR(255)		OUTPUT,
	@field12					NVARCHAR(255)		OUTPUT,
	@field13					NVARCHAR(255)		OUTPUT,
	@field14					NVARCHAR(255)		OUTPUT,
	@field15					NVARCHAR(255)		OUTPUT,
	@field16					NVARCHAR(255)		OUTPUT,
	@field17					NVARCHAR(255)		OUTPUT,
	@field18					NVARCHAR(255)		OUTPUT,
	@field19					NVARCHAR(255)		OUTPUT,
	@isUserACourseExpert		BIT					OUTPUT,	
	@isUserACourseApprover		BIT					OUTPUT,
	@isUserASupervisor			BIT					OUTPUT,
	@isUserAWallModerator		BIT					OUTPUT,
	@isUserAnILTInstructor		BIT					OUTPUT,
	@groupMemberships			NVARCHAR(MAX)		OUTPUT,
	@isRegistrationApproved		BIT					OUTPUT,
	@dtApproved					DATETIME			OUTPUT,
	@dtDenied					DATETIME			OUTPUT,
	@idApprover					INT					OUTPUT,
	@rejectionComments			NVARCHAR(MAX)		OUTPUT,
	@disableLoginFromLoginForm	BIT					OUTPUT,
	@optOutOfEmailNotifications	BIT					OUTPUT,
	@excludeFromLeaderboards	BIT					OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the specified language is the default language for the site
	
	*/
	
	-- NOT DONE. USERS DO NOT HAVE LANGUAGE VARIATIONS
	
	/*
	
	load the data
	
	*/
		
	SELECT
		@idUser					=	U.idUser,
		@firstName				=	U.firstName,
		@middleName				=	U.middleName,
		@lastName				=	U.lastname,
		@displayName			=	U.displayName,
		@avatar					=	U.avatar,
		@email					=	U.email,
		@username				=	U.username,
		--@password				=	U.[password],
		@idSite					=	U.idSite,
		@idTimezone				=	U.idTimezone,
		@objectGUID				=	U.objectGUID,
		@activationGUID			=	U.activationGUID,
		@distinguishedName		=	U.distinguishedName,
		@isActive				=	U.isActive,
		@mustChangePassword		=	U.mustchangePassword,
		@dtCreated				=	U.dtCreated,
		@dtExpires				=	U.dtExpires,
		@isDeleted				=	U.isDeleted,
		@dtDeleted				=	U.dtDeleted,
		@idLanguage				=	U.idLanguage,
		@languageString			=	L.code,
		@dtModified				=	U.dtModified,
		@dtLastLogin			=	U.dtLastLogin,
		@employeeId				=	U.employeeID,
		@company				=	U.company,
		@address				=	U.[address],
		@city					=	U.city,
		@province				=	U.province,
		@postalCode				=	U.postalcode,
		@country				=	U.country,
		@phonePrimary			=	U.phonePrimary,
		@phoneWork				=	U.phoneWork,
		@phoneFax				=	U.phoneFax,
		@phoneHome				=	U.phoneHome,
		@phoneMobile			=	U.phoneMobile,
		@phonePager				=	U.phonePager,
		@phoneOther				=	U.phoneOther,
		@department				=	U.department,
		@division				=	U.division,
		@region					=	U.region,
		@jobTitle				=	U.jobTitle,
		@jobClass				=	U.jobClass,
		@gender					=	U.gender,
		@race					=	U.race,
		@dtDOB					=	U.dtDOB,
		@dtHire					=	U.dtHire,
		@dtTerm					=	U.dtTerm,
		@field00				=	U.field00,
		@field01				=	U.field01,
		@field02				=	U.field02,
		@field03				=	U.field03,
		@field04				=	U.field04,
		@field05				=	U.field05,
		@field06				=	U.field06,
		@field07				=	U.field07,
		@field08				=	U.field08,
		@field09				=	U.field09,
		@field10				=	U.field10,
		@field11				=	U.field11,
		@field12				=	U.field12,
		@field13				=	U.field13,
		@field14				=	U.field14,
		@field15				=	U.field15,
		@field16				=	U.field16,
		@field17				=	U.field17,
		@field18				=	U.field18,
		@field19				=	U.field19,
		@isUserACourseExpert	= CASE WHEN (SELECT COUNT(1) FROM tblCourseExpert WHERE idUser = U.idUser) > 0 THEN 1 ELSE 0 END,
		@isUserACourseApprover	= CASE WHEN (SELECT COUNT(1) FROM tblCourseEnrollmentApprover WHERE idUser = U.idUser) > 0 THEN 1 ELSE 0 END,
		@isUserASupervisor		= CASE WHEN (SELECT COUNT(1) FROM tblUserToSupervisorLink WHERE idSupervisor = U.idUser) > 0 THEN 1 ELSE 0 END,
		@isUserAWallModerator	= CASE WHEN 
									(SELECT COUNT(1) FROM tblCourseFeedModerator WHERE idModerator = U.idUser) > 0 
									OR (SELECT COUNT(1) FROM tblGroupFeedModerator WHERE idModerator = U.idUser) > 0 
									THEN 1 ELSE 0 END,
		@isUserAnILTInstructor	= CASE WHEN (SELECT COUNT(1) FROM tblStandupTrainingInstanceToInstructorLink WHERE idInstructor = U.idUser AND idSite = U.idSite) > 0 THEN 1 ELSE 0 END,
		@groupMemberships		= STUFF((SELECT DISTINCT ',' + CAST(UGL.idGroup AS VARCHAR(10)) [text()] FROM tblUserToGroupLink UGL WHERE UGL.idUser = U.idUser FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,1,''),
		@isRegistrationApproved = U.isRegistrationApproved,
		@dtApproved = U.dtApproved,
		@dtDenied = U.dtDenied,
		@idApprover = U.idApprover,
		@rejectionComments = U.rejectionComments,
		@disableLoginFromLoginForm = CASE WHEN U.disableLoginFromLoginForm IS NOT NULL THEN U.disableLoginFromLoginForm ELSE 0 END,
		@optOutOfEmailNotifications = CASE WHEN U.optOutOfEmailNotifications IS NOT NULL THEN U.optOutOfEmailNotifications ELSE 0 END,
		@excludeFromLeaderboards = CASE WHEN U.excludeFromLeaderboards IS NOT NULL THEN U.excludeFromLeaderboards ELSE 0 END
	FROM tblUser U
	LEFT JOIN tblLanguage L ON L.idLanguage = U.idLanguage
	WHERE U.idUser = @idUser
	AND U.isDeleted = 0 -- not deleted
				
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1 -- no record was found
		SET @Error_Description_Code = 'UserDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
