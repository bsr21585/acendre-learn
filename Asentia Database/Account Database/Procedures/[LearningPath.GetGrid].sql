-- =====================================================================
-- PROCEDURE: [LearningPath.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.GetGrid]
GO

/*

Gets a listing of Learning Paths. 

*/

CREATE PROCEDURE [LearningPath.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblLearningPath LP
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND LP.idSite = @idCallerSite)
				)
			AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)

			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						LP.idLearningPath,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN LP.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN LP.dtCreated END) END DESC,

							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN LP.name END) END,
						    CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN LP.dtCreated END) END DESC
						)
						AS [row_number]
					FROM tblLearningPath LP
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND LP.idSite = @idCallerSite)
						)
					AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idLearningPath,
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				LP.idLearningPath,
				LP.avatar,
				CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE LP.name END AS name,
				(SELECT COUNT(1) FROM tblLearningPathToCourseLink LCL WHERE LCL.idLearningPath = LP.idLearningPath) AS courseCount,
				CASE WHEN LP.isPublished IS NULL THEN CONVERT(bit, 0) ELSE LP.isPublished END AS isPublished,
				CASE WHEN LP.isClosed IS NULL THEN CONVERT(bit, 0) ELSE LP.isClosed END AS isClosed,
				LP.dtCreated,
				CONVERT(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblLearningPath LP ON LP.idLearningPath = SelectedKeys.idLearningPath
			LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = @idCallerLanguage
			WHERE (LP.isDeleted IS NULL OR LP.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblLearningPath LP
				INNER JOIN CONTAINSTABLE(tblLearningPath, *, @searchParam) K ON K.[key] = LP.idLearningPath
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND LP.idSite = @idCallerSite)
					)
				AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							LP.idLearningPath,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN LP.name END) END DESC,
							    CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN LP.dtCreated END) END DESC,

								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN LP.name END) END,
						        CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN LP.dtCreated END) END DESC
							)
							AS [row_number]
						FROM tblLearningPath LP
						INNER JOIN CONTAINSTABLE(tblLearningPath, *, @searchParam) K ON K.[key] = LP.idLearningPath
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND LP.idSite = @idCallerSite
							)
						AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idLearningPath, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					LP.idLearningPath, 
					LP.avatar,
					LP.name,
					(SELECT COUNT(1) FROM tblLearningPathToCourseLink LCL WHERE LCL.idLearningPath = LP.idLearningPath) AS courseCount,
					CASE WHEN LP.isPublished IS NULL THEN CONVERT(bit, 0) ELSE LP.isPublished END AS isPublished,
					CASE WHEN LP.isClosed IS NULL THEN CONVERT(bit, 0) ELSE LP.isClosed END AS isClosed,
					LP.dtCreated,					
					CONVERT(bit, 1) AS isModifyOn,
					SelectedKeys.[row_number]					
				FROM SelectedKeys
				JOIN tblLearningPath LP ON LP.idLearningPath = SelectedKeys.idLearningPath
				WHERE (LP.isDeleted IS NULL OR LP.isDeleted = 0)
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblLearningPathLanguage LPL
				LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPL.idLearningPath
				INNER JOIN CONTAINSTABLE(tblLearningPathLanguage, *, @searchParam) K ON K.[key] = LPL.idLearningPathLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND LP.idSite = @idCallerSite)
					)
				AND LPL.idLanguage = @idCallerLanguage
				AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							LPL.idLearningPathLanguage,
							LP.idLearningPath,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN LPL.name END) END DESC,
							    CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN LP.dtCreated END) END DESC,

								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN LPL.name END) END,
						        CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN LP.dtCreated END) END DESC
							)
							AS [row_number]
						FROM tblLearningPathLanguage LPL
						LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPL.idLearningPath
						INNER JOIN CONTAINSTABLE(tblLearningPathLanguage, *, @searchParam) K ON K.[key] = LPL.idLearningPathLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND LP.idSite = @idCallerSite
							)
						AND LPL.idLanguage = @idCallerLanguage
						AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idLearningPath, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					LP.idLearningPath, 
					LP.avatar,
					LPL.name,
					(SELECT COUNT(1) FROM tblLearningPathToCourseLink LCL WHERE LCL.idLearningPath = LP.idLearningPath) AS courseCount,
					CASE WHEN LP.isPublished IS NULL THEN CONVERT(bit, 0) ELSE LP.isPublished END AS isPublished,
					CASE WHEN LP.isClosed IS NULL THEN CONVERT(bit, 0) ELSE LP.isClosed END AS isClosed,
					LP.dtCreated,
					CONVERT(bit, 1) AS isModifyOn,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = SelectedKeys.idLearningPath AND LPL.idLanguage = @idCallerLanguage
				LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPL.idLearningPath
				ORDER BY SelectedKeys.[row_number]

				END
			
			END
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO