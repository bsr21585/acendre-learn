-- =====================================================================
-- PROCEDURE: [ResourceType.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ResourceType.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ResourceType.Save]
GO

/*

Adds new ResourceType or updates existing ResourceType.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [ResourceType.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idResourceType			INT				OUTPUT,
	@resourceType			NVARCHAR(255)
	)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
				 
	/*
	
	validate uniqueness for ResourceType, if specified
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblResourceType
		WHERE idSite = @idCallerSite
		AND ISNULL(isDeleted,0) = 0 
		AND resourceType = @resourceType
		AND (
			@idResourceType IS NULL
			OR @idResourceType <> idResourceType
			)
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'ResourceTypeSave_ResourceTypeNotUnique'		
		RETURN 1 
		END

	
	/*
	
	save the data
	
	*/
	
	IF (@idResourceType = 0 OR @idResourceType IS NULL)
		
		BEGIN
		
		-- insert the new ResourceType
		
		INSERT INTO tblResourceType (
			idSite, 
			resourceType,
			isDeleted
			)			
		VALUES (
			@idCallerSite, 
			@resourceType,
			0
			)
		
		-- get the new ResourceType's id and return successfully
		
		SELECT @idResourceType = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the ResourceType id exists 
		IF (SELECT COUNT(1) FROM tblResourceType WHERE idResourceType = @idResourceType AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idResourceType = @idResourceType
			SELECT @Return_Code = 1
			SET @Error_Description_Code = 'ResourceTypeSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing ResourceType's properties
		
		UPDATE tblResourceType SET
			   resourceType = @resourceType
		WHERE idResourceType = @idResourceType
		
		-- get the ResourceType's id 
		SELECT @idResourceType = @idResourceType
				
		END
	
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblResourceTypeLanguage RTL WHERE RTL.idResourceType = @idResourceType AND RTL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblResourceTypeLanguage SET
			resourceType = @resourceType
			
		WHERE idResourceType = @idResourceType
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblResourceTypeLanguage (
			idSite,
			idResourceType,
			idLanguage,
			resourceType			
		)
		SELECT
			@idCallerSite,
			@idResourceType,
			@idDefaultLanguage,
			@resourceType
			
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblResourceTypeLanguage RTL
			WHERE RTL.idResourceType = @idResourceType
			AND RTL.idLanguage = @idDefaultLanguage
			AND RTL.idSite = @idCallerSite
		)

		END

	SELECT @Error_Description_Code = ''
	SELECT @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO