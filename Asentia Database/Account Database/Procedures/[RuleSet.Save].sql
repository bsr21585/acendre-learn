-- =====================================================================
-- PROCEDURE: [RuleSet.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSet.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSet.Save]
GO

/*

Adds a new record or updates an existing one.

*/

CREATE PROCEDURE [RuleSet.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idRuleSet				INT				OUTPUT,
	@id						INT				= 0,
	@idType					INT				= 0,
	@isAny					BIT,
	@label					NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED -- took reference from Group.Save
	
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1)
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			SET @Error_Description_Code = 'RuleSetSave_SpecifiedLanguageNotDefault'			
			RETURN 1 
		END

	*/
	
			
	/*
	
	validate uniqueness within same parent object
	
	*/

	IF	(@idType	= 4)	--Learning path
	BEGIN
		IF (
		SELECT COUNT(1)
		FROM tblRuleSetToRuleSetLearningPathEnrollmentLink RLPE
		INNER JOIN tblRuleSet RS ON RS.idRuleSet = RLPE.idRuleSet
		WHERE  RLPE.idRuleSetLearningPathEnrollment= @id
		AND RS.label = @label
		AND RS.idSite = @idCallerSite
		AND RS.idRuleSet <> @idRuleSet
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_RuleSetNotUnique'
		RETURN 1 
		END
	END
	ELSE IF (@idType	= 3)--Course
	BEGIN
	IF (
		SELECT COUNT(1)
		FROM tblRuleSetToRuleSetEnrollmentLink RLPE
		INNER JOIN tblRuleSet RS ON RS.idRuleSet = RLPE.idRuleSet
		WHERE RLPE.idRuleSetEnrollment = @id
		AND RS.label = @label
		AND RS.idSite = @idCallerSite
		AND RS.idRuleSet <> @idRuleSet
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_RuleSetNotUnique'
		RETURN 1 
		END
	END
	ELSE IF (@idType	= 2) --Role
	BEGIN
	IF (
		SELECT COUNT(1)
		FROM tblRuleSetToRoleLink RLPE
		INNER JOIN tblRuleSet RS ON RS.idRuleSet = RLPE.idRuleSet
		WHERE RLPE.idRole = @id
		AND RS.label = @label
		AND RS.idSite = @idCallerSite
		AND RS.idRuleSet <> @idRuleSet
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_RuleSetNotUnique'
		RETURN 1 
		END
	END
	ELSE IF (@idType	= 1)--Group
	BEGIN
	IF (
		SELECT COUNT(1)--Group
		FROM tblRuleSetToGroupLink RLPE
		INNER JOIN tblRuleSet RS ON RS.idRuleSet = RLPE.idRuleSet
		WHERE RLPE.idGroup = @id
		AND RS.label = @label
		AND RS.idSite = @idCallerSite
		AND RS.idRuleSet <> @idRuleSet
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_RuleSetNotUnique'
		RETURN 1 
		END
	END
	ELSE IF (@idType	= 5)--Certification
	BEGIN
	IF (
		SELECT COUNT(1)--Certification
		FROM tblRuleSetToCertificationLink RLPE
		INNER JOIN tblRuleSet RS ON RS.idRuleSet = RLPE.idRuleSet
		WHERE RLPE.idCertification = @id
		AND RS.label = @label
		AND RS.idSite = @idCallerSite
		AND RS.idRuleSet <> @idRuleSet
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_RuleSetNotUnique'
		RETURN 1 
		END
	END

	/*
	
	validate uniqueness within language
	
	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF	(@idType	= 4)--learning path
	BEGIN
	IF( 
		SELECT COUNT(1)
		FROM tblRuleSet R
		LEFT JOIN tblRuleSetLanguage RSL ON RSL.idRuleSet = R.idRuleSet AND RSL.idLanguage = @idDefaultLanguage -- same language
		LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSLPEL ON R.idRuleSet = RSLPEL.idRuleSet
		WHERE R.idSite	 =	 @idCallerSite -- same site
		AND RSL.label	=	 @label -- validate parameter: name
		AND RSLPEL.idRuleSetLearningPathEnrollment	= @id
		AND R.idRuleSet <> @idRuleSet

		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_FieldNotUnique'
		RETURN 1
		END
	END
	ELSE IF (@idType	= 3)--Course
	BEGIN
	IF( 
		SELECT COUNT(1)
		FROM tblRuleSet R
		LEFT JOIN tblRuleSetLanguage RSL ON RSL.idRuleSet = R.idRuleSet AND RSL.idLanguage = @idDefaultLanguage -- same language
		LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSLPEL ON R.idRuleSet = RSLPEL.idRuleSet
		WHERE R.idSite	 =	 @idCallerSite -- same site
		AND RSL.label	=	 @label -- validate parameter: name
		AND RSLPEL.idRuleSetEnrollment	= @id
		AND R.idRuleSet <> @idRuleSet

		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_FieldNotUnique'
		RETURN 1
		END
	END
	ELSE IF (@idType = 2)--Role
	BEGIN
	IF( 
		SELECT COUNT(1)
		FROM tblRuleSet R
		LEFT JOIN tblRuleSetLanguage RSL ON RSL.idRuleSet = R.idRuleSet AND RSL.idLanguage = @idDefaultLanguage -- same language
		LEFT JOIN tblRuleSetToRoleLink RSLPEL ON R.idRuleSet = RSLPEL.idRuleSet
		WHERE R.idSite	=	@idCallerSite -- same site
		AND RSL.label	=	@label -- validate parameter: name
		AND RSLPEL.idRole	=	@id
		AND R.idRuleSet <> @idRuleSet

		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_FieldNotUnique'
		RETURN 1
		END
	END
	ELSE IF (@idType	= 1)--Group
	BEGIN
	IF( 
		SELECT COUNT(1)
		FROM tblRuleSet R
		LEFT JOIN tblRuleSetLanguage RSL ON RSL.idRuleSet = R.idRuleSet AND RSL.idLanguage = @idDefaultLanguage -- same language
		LEFT JOIN tblRuleSetToGroupLink RSLPEL ON R.idRuleSet = RSLPEL.idRuleSet
		WHERE R.idSite = @idCallerSite -- same site
		AND RSL.label = @label -- validate parameter: name
		AND RSLPEL.idGroup	= @id
		AND R.idRuleSet <> @idRuleSet

		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_FieldNotUnique'
		RETURN 1
		END
	END
	ELSE IF (@idType	= 5)--Certification
	BEGIN
	IF( 
		SELECT COUNT(1)
		FROM tblRuleSet R
		LEFT JOIN tblRuleSetLanguage RSL ON RSL.idRuleSet = R.idRuleSet AND RSL.idLanguage = @idDefaultLanguage -- same language
		LEFT JOIN tblRuleSetToCertificationLink RSLPEL ON R.idRuleSet = RSLPEL.idRuleSet
		WHERE R.idSite = @idCallerSite -- same site
		AND RSL.label = @label -- validate parameter: name
		AND RSLPEL.idCertification	= @id
		AND R.idRuleSet <> @idRuleSet

		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_FieldNotUnique'
		RETURN 1
		END
	END
				
	/*
	
	save the data
	
	*/
	
	IF (@idRuleSet = 0 OR @idRuleSet is null)
		
		BEGIN
		
		-- insert new record
		
		INSERT INTO tblRuleSet (
			[idSite],
			[isAny],
			[label]
		)
		VALUES (
			@idCallerSite,
			@isAny,
			@label
		)
		
		-- get the new id and return successfully
		
		SELECT @idRuleSet = SCOPE_IDENTITY()
		
		-- If idRuleSetEnrollment is specified then link the Rule Set to Rule Set Enrollment
		IF @id <> 0 AND @idType <> 0
			BEGIN
			
			IF @idType = 1
				BEGIN
				INSERT INTO tblRuleSetToGroupLink (
					idSite,
					idRuleSet,
					idGroup
				)
				VALUES (
					@idCallerSite,
					@idRuleSet,
					@id
				)
				END
			ELSE IF @idType = 2
				BEGIN
				INSERT INTO tblRuleSetToRoleLink (
					idSite,
					idRuleSet,
					idRole
				)
				VALUES (
					@idCallerSite,
					@idRuleSet,
					@id
				)				
				END
			ELSE IF @idType = 3
				BEGIN
				INSERT INTO tblRuleSetToRuleSetEnrollmentLink (
					idSite,
					idRuleSet,
					idRuleSetEnrollment
				)
				VALUES (
					@idCallerSite,
					@idRuleSet,
					@id
				)
				END		
			ELSE IF @idType = 4
				BEGIN
				INSERT INTO tblRuleSetToRuleSetLearningPathEnrollmentLink (
					idSite,
					idRuleSet,
					idRuleSetLearningPathEnrollment
				)
				VALUES (
					@idCallerSite,
					@idRuleSet,
					@id
				)
				END
			ELSE IF @idType = 5
				BEGIN
				INSERT INTO tblRuleSetToCertificationLink (
					idSite,
					idRuleSet,
					idCertification
				)
				VALUES (
					@idCallerSite,
					@idRuleSet,
					@id
				)
				END												

			END
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the record exists
		
		IF (SELECT COUNT(1) FROM tblRuleSet WHERE idRuleSet = @idRuleSet) < 1
			
			BEGIN
				SELECT @Return_Code = 1
				SET @Error_Description_Code = 'RuleSetSave_NoRecordFound'
				RETURN 1
			END

		-- record label name prior to change (to be able to match the ruleset to synced rulesets)

		DECLARE @initialLabel			NVARCHAR(255)
		SET @initialLabel = (SELECT label FROM tblRuleSet WHERE idRuleSet = @idRuleSet)
			
		-- update existing record

		UPDATE tblRuleSet SET
			idSite = @idCallerSite,
			isAny = @isAny,
			label = @label
		WHERE idRuleSet = @idRuleSet
		
		-- get the id and return successfully
		
		SELECT @idRuleSet = @idRuleSet
		
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblRuleSetLanguage RSL WHERE RSL.idRuleSet = @idRuleSet AND RSL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN
		
		UPDATE tblRuleSetLanguage SET
			label = @label
		WHERE idRuleSet = @idRuleSet
		AND idLanguage = @idDefaultLanguage		
		
		END
	ELSE
	
		BEGIN
	
		INSERT INTO tblRuleSetLanguage (
			idSite,
			idRuleSet,
			idLanguage,
			label
		)
		SELECT
			@idCallerSite,
			@idRuleSet,
			@idDefaultLanguage,
			@label
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblRuleSetLanguage RL
			WHERE RL.idRuleSet = @idRuleSet
			AND RL.idLanguage = @idDefaultLanguage
		)	
		
		END

	/*

	sync child rulesets

	*/

	DECLARE @idParentRuleSetEnrollment					INT
	DECLARE @idParentRuleSetLearningPathEnrollment		INT

	DECLARE @idChildRuleSetEnrollment					INT
	DECLARE @idChildRuleSetLearningPathEnrollment		INT

	DECLARE @idChildRuleSet								INT

	-- get parent ruleset enrollment id

	IF @idType = 3				-- course
	BEGIN
		SET @idParentRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idRuleSet)
	END
		
	ELSE IF @idType = 4			-- learning path
	BEGIN
		SET @idParentRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = @idRuleSet)
	END

	-- get child rulesets

	IF @idType = 3				-- course
	BEGIN
		SELECT 
			RS.idRuleSet 
		INTO #SyncedCourseRuleSetsToUpdate 
		FROM tblRuleSetToRuleSetEnrollmentLink RSTRSEL
		LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSTRSEL.idRuleSetEnrollment 
		LEFT JOIN tblRuleSet RS on RS.idRuleSet = RSTRSEL.idRuleSet
		WHERE idParentRuleSetEnrollment = @idParentRuleSetEnrollment
		AND RS.label = @initialLabel
	END

	ELSE IF @idType = 4			-- learning path
	BEGIN
		SELECT 
			RS.idRuleSet 
		INTO #SyncedLearningPathRuleSetsToUpdate 
		FROM tblRuleSetToRuleSetLearningPathEnrollmentLink RSTRSLPEL
		LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = RSTRSLPEL.idRuleSetLearningPathEnrollment 
		LEFT JOIN tblRuleSet RS on RS.idRuleSet = RSTRSLPEL.idRuleSet
		WHERE idParentRuleSetLearningPathEnrollment = @idParentRuleSetLearningPathEnrollment
		AND RS.label = @initialLabel
	END

	-- get child ruleset enrollments

	IF @idType = 3				-- course
	BEGIN
		SELECT 
			idRuleSetEnrollment
		INTO #SyncedRuleSetEnrollmentsToUpdate 
		FROM tblRuleSetEnrollment
		WHERE idParentRuleSetEnrollment = @idParentRuleSetEnrollment
	END

	ELSE IF @idType = 4			-- learning path
	BEGIN
		SELECT 
			idRuleSetLearningPathEnrollment
		INTO #SyncedRuleSetLearningPathEnrollmentsToUpdate 
		FROM tblRuleSetLearningPathEnrollment
		WHERE idParentRuleSetLearningPathEnrollment = @idParentRuleSetLearningPathEnrollment
	END

	IF @idType = 3				-- course
	BEGIN

	
		-- if syncing a ruleset that already exists in child ruleset enrollment (label or rules change)

		IF EXISTS (SELECT 1 FROM #SyncedCourseRuleSetsToUpdate)
		BEGIN

			DECLARE idChildRuleSetCursor							CURSOR LOCAL
			FOR 
			SELECT idRuleSet FROM #SyncedCourseRuleSetsToUpdate	

			OPEN idChildRuleSetCursor
			FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet
			WHILE @@FETCH_STATUS = 0  
			BEGIN
		
			SET @idChildRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idChildRuleSet)

			EXEC [RuleSet.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idChildRuleSet, @idChildRuleSetEnrollment, @idType, @isAny, @label
		
			-- sync child ruleset enrollment back to parent ruleset enrollment (calling RuleSet.Save unsyncs the ruleset enrollment)

			UPDATE tblRuleSetEnrollment
			SET idParentRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idRuleSet)
			WHERE idRuleSetEnrollment = @idChildRuleSetEnrollment

			FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet

			END

		CLOSE idChildRuleSetCursor
		DEALLOCATE idChildRuleSetCursor

		END

		-- if syncing a ruleset that does not already exist in child ruleset enrollment (new ruleset)

		ELSE IF EXISTS (SELECT 1 FROM #SyncedRuleSetEnrollmentsToUpdate)
		BEGIN

			DECLARE idChildRuleSetEnrollmentCursor					CURSOR LOCAL
			FOR 
			SELECT idRuleSetEnrollment FROM #SyncedRuleSetEnrollmentsToUpdate

			OPEN idChildRuleSetEnrollmentCursor
			FETCH NEXT FROM idChildRuleSetEnrollmentCursor INTO @idChildRuleSetEnrollment
			WHILE @@FETCH_STATUS = 0  
			BEGIN

			EXEC [RuleSet.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, null, @idChildRuleSetEnrollment, @idType, @isAny, @label
		
			SET @idChildRuleSet = (SELECT RS.idRuleSet FROM tblRuleSet RS LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSTRSEL ON RSTRSEL.idRuleSet = RS.idRuleSet WHERE idRuleSetEnrollment = @idChildRuleSetEnrollment AND label = @label)
		
			-- sync child ruleset enrollment back to parent ruleset enrollment (calling RuleSet.Save unsyncs the ruleset enrollment)

			UPDATE tblRuleSetEnrollment
			SET idParentRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idRuleSet)
			WHERE idRuleSetEnrollment = @idChildRuleSetEnrollment

			FETCH NEXT FROM idChildRuleSetEnrollmentCursor INTO @idChildRuleSetEnrollment

			END

		CLOSE idChildRuleSetEnrollmentCursor
		DEALLOCATE idChildRuleSetEnrollmentCursor

		END

	END

	ELSE IF @idType = 4			-- learning path
	BEGIN

	    -- if syncing a ruleset that already exists in child ruleset learning path enrollment (label or rules change)

		IF EXISTS (SELECT 1 FROM #SyncedLearningPathRuleSetsToUpdate)
		BEGIN

			DECLARE idChildRuleSetCursor							CURSOR LOCAL
			FOR 
			SELECT idRuleSet FROM #SyncedLearningPathRuleSetsToUpdate	

			OPEN idChildRuleSetCursor
			FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet
			WHILE @@FETCH_STATUS = 0  
			BEGIN
		
			SET @idChildRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = @idChildRuleSet)

			EXEC [RuleSet.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idChildRuleSet, @idChildRuleSetLearningPathEnrollment, @idType, @isAny, @label
		
			-- sync child ruleset enrollment back to parent ruleset enrollment (calling RuleSet.Save unsyncs the ruleset enrollment)

			UPDATE tblRuleSetLearningPathEnrollment
			SET idParentRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = @idRuleSet)
			WHERE idRuleSetLearningPathEnrollment = @idChildRuleSetLearningPathEnrollment

			FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet

			END

		CLOSE idChildRuleSetCursor
		DEALLOCATE idChildRuleSetCursor

		END

		-- if syncing a ruleset that does not already exist in child ruleset learning path enrollment (new ruleset)

		ELSE IF EXISTS (SELECT 1 FROM #SyncedRuleSetLearningPathEnrollmentsToUpdate)
		BEGIN

			DECLARE idChildRuleSetLearningPathEnrollmentCursor		CURSOR LOCAL
			FOR 
			SELECT idRuleSetLearningPathEnrollment FROM #SyncedRuleSetLearningPathEnrollmentsToUpdate

			OPEN idChildRuleSetLearningPathEnrollmentCursor
			FETCH NEXT FROM idChildRuleSetLearningPathEnrollmentCursor INTO @idChildRuleSetLearningPathEnrollment
			WHILE @@FETCH_STATUS = 0  
			BEGIN

			EXEC [RuleSet.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, null, @idChildRuleSetLearningPathEnrollment, @idType, @isAny, @label
		
			SET @idChildRuleSet = 
				(SELECT RS.idRuleSet 
				FROM tblRuleSet RS 
				LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSTRSLPEL ON RSTRSLPEL.idRuleSet = RS.idRuleSet 
				WHERE idRuleSetLearningPathEnrollment = @idChildRuleSetLearningPathEnrollment AND label = @label)
		
			-- sync child ruleset enrollment back to parent ruleset enrollment (calling RuleSet.Save unsyncs the ruleset enrollment)

			UPDATE tblRuleSetLearningPathEnrollment
			SET idParentRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment 
														FROM tblRuleSetToRuleSetLearningPathEnrollmentLink 
														WHERE idRuleSet = @idRuleSet)
			WHERE idRuleSetLearningPathEnrollment = @idChildRuleSetLearningPathEnrollment

			FETCH NEXT FROM idChildRuleSetLearningPathEnrollmentCursor INTO @idChildRuleSetLearningPathEnrollment

			END

		CLOSE idChildRuleSetLearningPathEnrollmentCursor
		DEALLOCATE idChildRuleSetLearningPathEnrollmentCursor

		END

	END

	
	-- drop tables

	IF @idType = 3				-- course
	BEGIN
		DROP TABLE #SyncedCourseRuleSetsToUpdate
		DROP TABLE #SyncedRuleSetEnrollmentsToUpdate
	END

	ELSE IF @idType = 4			-- learning path
	BEGIN
		DROP TABLE #SyncedLearningPathRuleSetsToUpdate
		DROP TABLE #SyncedRuleSetLearningPathEnrollmentsToUpdate
	END
	
	-- unsync ruleset enrollment if updating a child ruleset

	IF @idType = 3				-- course
	BEGIN
		UPDATE tblRuleSetEnrollment
		SET idParentRuleSetEnrollment = NULL
		WHERE idRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idRuleSet)
	END

	ELSE IF @idType = 4			-- learning path
	BEGIN
		UPDATE tblRuleSetLearningPathEnrollment
		SET idParentRuleSetLearningPathEnrollment = NULL
		WHERE idRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = @idRuleSet)
	END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
