-- =====================================================================
-- PROCEDURE: [DocumentRepositoryFolder.IdsAndNamesObjectSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DocumentRepositoryFolder.IdsAndNamesObjectSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DocumentRepositoryFolder.IdsAndNamesObjectSelectList]
GO

/*

Returns a recordset of Document Repository Folder ids and names to populate
"Folder" select list for object (group,course,learning path).

*/

CREATE PROCEDURE [DocumentRepositoryFolder.IdsAndNamesObjectSelectList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	--default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idObject				INT,
	@idObjectType           INT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	BEGIN

		SELECT DISTINCT
		    DFR.idDocumentRepositoryFolder,
			DFR.name
		FROM tblDocumentRepositoryFolder DFR
		    WHERE DFR.idObject	=	@idObject
			AND   DFR.idDocumentRepositoryObjectType	=	@idObjectType
			AND (DFR.isDeleted IS NULL OR DFR.isDeleted =0)
		ORDER BY name

	END
			
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO