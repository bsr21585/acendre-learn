-- =====================================================================
-- PROCEDURE: [TransactionItem.GetItemsForPurchase]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.GetItemsForPurchase]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.GetItemsForPurchase]
GO

/*

Gets all transaction items for a purchase id.

*/
CREATE PROCEDURE [TransactionItem.GetItemsForPurchase]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,

	@idPurchase					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblPurchase
		WHERE idSite = @idCallerSite		
		AND	idPurchase = @idPurchase
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TIGetItemsForPurchase_PurchaseNotFound'
		RETURN 1
		END
		
		
	/*
	
	get the data 
	
	*/
		
	SELECT 
		TI.idTransactionItem,
		TI.idSite,
		TI.idParentTransactionItem,
		TI.idPurchase,
		TI.idEnrollment,
		TI.idUser,
		TI.userName,
		TI.idAssigner,
		TI.assignerName,
		TI.itemId,
		TI.itemName,
		TI.itemType,
		TI.[description],
		TI.cost,
		TI.paid,
		TI.idCouponCode,
		TI.couponCode,
		TI.dtAdded,
		TI.confirmed,
		TI.idIltSession
	FROM tblTransactionItem TI	
	WHERE TI.idPurchase = @idPurchase			

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO