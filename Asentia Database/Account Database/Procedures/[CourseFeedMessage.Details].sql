-- =====================================================================
-- PROCEDURE: [CourseFeedMessage.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CourseFeedMessage.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CourseFeedMessage.Details]
GO

/*

Return all the properties for a given course feed message id. 

*/

CREATE PROCEDURE [CourseFeedMessage.Details]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, -- default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0, -- will fail if not specified
	
	@idCourseFeedMessage		INT					OUTPUT,
	@idSite						INT					OUTPUT,
	@idCourse					INT					OUTPUT,
	@idLanguage					INT					OUTPUT,
	@idAuthor					INT					OUTPUT,
	@idParentCourseFeedMessage	INT					OUTPUT,
	@message					NVARCHAR(MAX)		OUTPUT,
	@timestamp					DATETIME			OUTPUT,
	@dtApproved					DATETIME			OUTPUT,
	@idApprover					INT					OUTPUT,
	@isApproved					BIT					OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that the object exists

	*/

	IF (
		SELECT COUNT(1)
		FROM tblCourseFeedMessage
		WHERE idCourseFeedMessage = @idCourseFeedMessage
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseFeedMessageDetails_NoRecordFound'
		RETURN 1
		END
		
	/* get the data */

	SELECT
		@idCourseFeedMessage		= GFM.idCourseFeedMessage,
		@idSite						= GFM.idSite,
		@idCourse					= GFM.idCourse,
		@idLanguage					= GFM.idLanguage,
		@idAuthor					= GFM.idAuthor,
		@idParentCourseFeedMessage	= GFM.idParentCourseFeedMessage,
		@message					= GFM.[message],
		@timestamp					= GFM.[timestamp],
		@dtApproved					= GFM.dtApproved,
		@idApprover					= GFM.idApprover,
		@isApproved					= GFM.isApproved
	FROM tblCourseFeedMessage GFM
	WHERE GFM.idCourseFeedMessage = @idCourseFeedMessage
	AND GFM.idSite = @idCallerSite
		
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'CourseFeedMessageDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO