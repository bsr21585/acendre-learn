-- =====================================================================
-- PROCEDURE: [Course.SaveApprovers]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.SaveApprovers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.SaveApprovers]
GO

/*

Saves approvers for courses

*/

CREATE PROCEDURE [Course.SaveApprovers]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCourse				INT,
	@Approvers				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
			
	/*
	
	validate that all approvers exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Approvers A
		LEFT JOIN tblUser U ON U.idUser = A.id
		WHERE U.idSite IS NULL
		OR U.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseSaveApprovers_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	validate that the course belongs to the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourse C
		WHERE C.idCourse = @idCourse
		AND (C.idSite IS NULL OR C.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseSaveApprovers_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	remove approvers for this course from the course enrollment approver table 
	where they are not in the table variable
	(if there are any in the table variable, otherwise it means there are none)
	
	*/
	
	DELETE FROM tblCourseEnrollmentApprover
	WHERE idCourse = @idCourse
	AND NOT EXISTS (SELECT 1 FROM @Approvers A
					WHERE A.id = tblCourseEnrollmentApprover.idUser)
	
	/*

	insert the course enrollment approvers for ths course into the course expert table
	where they do not already exist
	(if there are any in the table variable, otherwise it means there are none)

	*/

	IF (SELECT COUNT(1) FROM @Approvers) > 0
		BEGIN

		INSERT INTO tblCourseEnrollmentApprover (
			idSite,
			idCourse,
			idUser
		)
		SELECT
			@idCallerSite,
			@idCourse,
			A.id
		FROM @Approvers A
		WHERE NOT EXISTS (SELECT 1 
						  FROM tblCourseEnrollmentApprover
						  WHERE idCourse = @idCourse
						  AND idUser = A.id)

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO