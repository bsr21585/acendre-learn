-- =====================================================================
-- PROCEDURE: [LicenseAgreementToSiteLink.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LicenseAgreementToSiteLink.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LicenseAgreementToSiteLink.Save]
GO

/*

saves license agreement to site link

*/

CREATE PROCEDURE [LicenseAgreementToSiteLink.Save]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0,
	
	@idSite							INT, 
	@dtExecuted						DATETIME,
	@executeLicenseAgreementGUID	NVARCHAR(MAX),
	@licenseAgreementIPAddress		NVARCHAR(MAX)
)
AS	
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	save the data
	
	*/	
		
	INSERT INTO tblLicenseAgreementToSiteLink (
		idSite,
		dtExecuted,
		executeLicenseAgreementGUID,
		licenseAgreementIPAddress
	)			
	SELECT
		@idSite,
		@dtExecuted,
		@executeLicenseAgreementGUID,
		@licenseAgreementIPAddress		
			
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END		

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO