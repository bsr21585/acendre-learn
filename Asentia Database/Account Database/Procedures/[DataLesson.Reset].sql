-- =====================================================================
-- PROCEDURE: [DataLesson.Reset]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataLesson.Reset]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.Reset]
GO

/*

Resets lesson data.

*/

CREATE PROCEDURE [DataLesson.Reset]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idEnrollment			INT,
	@idDataLesson			INT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate that lesson data exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-Lesson]
		WHERE [idData-Lesson] = @idDataLesson
		AND idSite = @idCallerSite
		) = 0
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataLessonReset_NoLessonDataRecordFound'
		RETURN 1 
		END
		
	/*
	
	Reset the lesson data by resetting the lesson data record, and removing and linked content-specific data,
	i.e. SCO, Task, etc.
	
	*/
	
	-- reset lesson data record
	UPDATE [tblData-Lesson] SET 
		dtCompleted = NULL, 
		contentTypeCommittedTo = NULL, 
		dtCommittedToContentType = NULL,
		resetForContentChange = NULL,
		preventPostCompletionLaunchForContentChange = NULL
	WHERE [idData-Lesson] = @idDataLesson

	-- delete any SCO interactions and objectives, then delete SCO data
	-- note that this takes care of both content package and ojt data

	-- delete SCORM interaction data
	DELETE FROM [tblData-SCOInt]
	WHERE [idData-SCO] IN (SELECT [idData-SCO]
						   FROM [tblData-SCO]
						   WHERE [idData-Lesson] = @idDataLesson)

	-- delete SCORM objective data
	DELETE FROM [tblData-SCOObj]
	WHERE [idData-SCO] IN (SELECT [idData-SCO]
						   FROM [tblData-SCO]
						   WHERE [idData-Lesson] = @idDataLesson)

	-- delete SCO lesson data
	DELETE FROM [tblData-SCO] WHERE [idData-Lesson] = @idDataLesson

	-- delete task data
	DELETE FROM [tblData-HomeworkAssignment] WHERE [idData-Lesson] = @idDataLesson
	

	/*

	check for course completion

	*/

	EXEC [Enrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO