-- =====================================================================
-- PROCEDURE: [System.MatchRequestedUrlToSite]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.MatchRequestedUrlToSite]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.MatchRequestedUrlToSite]
GO

/*

Matches a url to a site id and hostname.

*/
CREATE PROCEDURE [System.MatchRequestedUrlToSite]
(
	@Return_Code						INT					OUTPUT,
	@Error_Description_Code				NVARCHAR(50)		OUTPUT,
	
	@idSite								INT					OUTPUT,
	@hostname							NVARCHAR(255)		OUTPUT,
	@langString							NVARCHAR(10)		OUTPUT,
	@url								NVARCHAR(255),
	@clientIPAddress					NVARCHAR(15),
	@bounceForNoAccessIPRestriction		BIT					OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
		SELECT DISTINCT 
			@idSite = S.idSite,
			@hostname = S.hostname,
			@langString = L.code
		FROM tblSiteToDomainAliasLink SDAL
		LEFT JOIN tblSite S on S.idSite = SDAL.idSite
		LEFT JOIN tblLanguage L on L.idLanguage = S.idLanguage
		WHERE @url = S.hostname
		OR @url = SDAL.domain

		-- if we have no site id, return with the default site information
		IF (@idSite is null)
			BEGIN
			SET @idSite = 1
			SET @hostname = 'default'
			SET @langString = 'en-US'
			SET @bounceForNoAccessIPRestriction = 0
			
			SET @Return_Code = 0 --(0 is 'success')
			RETURN
			END

		-- check IP restriction settings for site
		SET @bounceForNoAccessIPRestriction = 0

		DECLARE @ipRestrictionSetting NVARCHAR(10)		

		SELECT 		
			@ipRestrictionSetting = CASE WHEN SP.[idSiteParam] IS NOT NULL THEN SP.[value] ELSE DSP.[value] END
		FROM tblSiteParam DSP
		LEFT JOIN tblSiteParam SP ON SP.[param] = DSP.[param] AND SP.idSite = @idSite
		WHERE DSP.idSite = 1 AND DSP.[param] = 'System.IPAddressRestriction'
		
		IF (@ipRestrictionSetting = 'absolute')
			BEGIN
			SELECT @bounceForNoAccessIPRestriction = CASE WHEN (SELECT COUNT(1) FROM tblSiteIPRestriction WHERE ip = @clientIPAddress AND idSite = @idSite) = 0 THEN 1 ELSE 0 END
			END
		
		-- return
		SET @Return_Code = 0 --(0 is 'success')
		RETURN
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO