-- =====================================================================
-- PROCEDURE: [Site.SaveUserAgreementModifiedDate]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Site.SaveUserAgreementModifiedDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.SaveUserAgreementModifiedDate]
GO

/*
Saves User Agreement Last Modified Date
*/
CREATE PROCEDURE [dbo].[Site.SaveUserAgreementModifiedDate]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
AS
	BEGIN
	SET NOCOUNT ON
	
	-- update user agreement last modified date
		
	UPDATE tblSite SET
		dtUserAgreementModified = GETUTCDATE()
	WHERE idSite = @idCallerSite

	SET @Return_Code = 0 --(0 is 'success')

	END			

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

