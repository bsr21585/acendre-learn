-- =====================================================================
-- PROCEDURE: [ResourceToObjectLink.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ResourceToObjectLink.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ResourceToObjectLink.Details]
GO

/*

Return all the properties for a given Resource id.

*/
CREATE PROCEDURE [ResourceToObjectLink.Details]
(
	@Return_Code			INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)		OUTPUT,
	@idCallerSite			INT					= 0, --default if not specified
	@callerLangString		NVARCHAR (10)			,
	@idCaller				INT					= 0,
	
	@idResourceToObject		INT					OUTPUT,
	@idResource				INT					OUTPUT,
	@name					NVARCHAR(255)		OUTPUT,	
	@idObject				INT         		OUTPUT,
	@objectType				NVARCHAR(255)		OUTPUT,
	@dtStart				DATETIME			OUTPUT,
	@dtEnd					DATETIME			OUTPUT,
	@isOutsideUse			BIT					OUTPUT,
	@isMaintenance			BIT					OUTPUT
)											
AS											
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/ 
	
	IF (
		SELECT COUNT(1)
		FROM tblResourceToObjectLink
		WHERE id = @idResourceToObject
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ResourceToObjectLinkDetails_NoRecordFound' 
		RETURN 1
	    END
		
	
		
	/*
	
	get the data 
	
	*/
		
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	SELECT
		@idResourceToObject	= RO.id,
		@idResource			= R.idResource,
		@name				= CASE WHEN RL.name IS NULL OR RL.name = '' THEN R.name ELSE RL.name END,
		@idObject			= RO.idObject,
		@objectType			= RO.objectType,
		@dtStart			= RO.dtStart,
		@dtEnd				= RO.dtEnd,
		@isOutsideUse		= RO.isOutsideUse,
		@isMaintenance		= RO.isMaintenance
	FROM tblResourceToObjectLink RO
	INNER JOIN tblResource R on RO.idResource=R.idResource
	LEFT JOIN tblResourceLanguage RL 
		ON RL.idResource = R.idResource 
		AND RL.idLanguage = @idCallerLanguage
	WHERE RO.id = @idResourceToObject
	AND RO.idSite = @idCallerSite	
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'ResourceToObjectLinkDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO