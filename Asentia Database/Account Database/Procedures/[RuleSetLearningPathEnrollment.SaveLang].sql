-- =====================================================================
-- PROCEDURE: [RuleSetLearningPathEnrollment.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetLearningPathEnrollment.SaveLang]
GO

/*

Saves ruleset learning path enrollment "language specific" properties for specific language
in ruleset learning Path enrollment language table.

*/

CREATE PROCEDURE [RuleSetLearningPathEnrollment.SaveLang]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idRuleSetLearningPathEnrollment	INT, 
	@languageString						NVARCHAR(10),
	@label								NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RSLPEnrollmentSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
			 
	/*
	
	validate that the ruleset learning path enrollment exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblRuleSetLearningPathEnrollment
		WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RSLPEnrollmentSaveLang_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblRuleSetLearningPathEnrollmentLanguage RSLPEL WHERE RSLPEL.idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment AND RSLPEL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblRuleSetLearningPathEnrollmentLanguage SET
			label = @label
		WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblRuleSetLearningPathEnrollmentLanguage (
			idSite,
			idRuleSetLearningPathEnrollment,
			idLanguage,
			label
		)
		SELECT
			@idCallerSite,
			@idRuleSetLearningPathEnrollment,
			@idLanguage,
			@label
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblRuleSetLearningPathEnrollmentLanguage RSLPEL
			WHERE RSLPEL.idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
			AND RSLPEL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblRuleSetLearningPathEnrollment SET
			label = @label
		WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment

		END

	/*

	sync children ruleset learning path languages

	*/

	DECLARE @idChildRuleSetLearningPathEnrollment		INT
	DECLARE @idChildLearningPath						INT

	SELECT 
		idRuleSetLearningPathEnrollment
	INTO #SyncedRuleSetLearningPathEnrollmentsToUpdate
	FROM tblRuleSetLearningPathEnrollment
	WHERE idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment	      
								  
	IF EXISTS (SELECT 1 FROM #SyncedRuleSetLearningPathEnrollmentsToUpdate)	
	BEGIN
		DECLARE idChildRuleSetLearningPathEnrollmentCursor							CURSOR LOCAL
		FOR 
		SELECT idRuleSetLearningPathEnrollment FROM #SyncedRuleSetLearningPathEnrollmentsToUpdate	

		OPEN idChildRuleSetLearningPathEnrollmentCursor
		FETCH NEXT FROM idChildRuleSetLearningPathEnrollmentCursor INTO @idChildRuleSetLearningPathEnrollment
		WHILE @@FETCH_STATUS = 0  
		BEGIN
		
		SET @idChildLearningPath = (SELECT idLearningPath FROM tblRuleSetLearningPathEnrollment WHERE idRuleSetLearningPathEnrollment = @idChildRuleSetLearningPathEnrollment)

		EXEC [RuleSetLearningPathEnrollment.SaveLang] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idChildRuleSetLearningPathEnrollment, @languageString, @label
		
		-- sync child ruleset enrollment back to parent ruleset enrollment (calling RuleSet.Save unsyncs the ruleset enrollment)

		UPDATE tblRuleSetLearningPathEnrollment
		SET idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
		WHERE idRuleSetLearningPathEnrollment = @idChildRuleSetLearningPathEnrollment

		FETCH NEXT FROM idChildRuleSetLearningPathEnrollmentCursor INTO @idChildRuleSetLearningPathEnrollment

		END

	CLOSE idChildRuleSetLearningPathEnrollmentCursor
	DEALLOCATE idChildRuleSetLearningPathEnrollmentCursor

	END
	
	UPDATE tblRuleSetLearningPathEnrollment
	SET idParentRuleSetLearningPathEnrollment = NULL
	WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO