-- =====================================================================
-- PROCEDURE: [DataLesson.CommitToContentType]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DataLesson.CommitToContentType]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.CommitToContentType]
GO

/*

Sets the lesson data's contentTypeCommittedTo flag. Set when a learner launches/interacts with a specific content type for the lesson.

*/

CREATE PROCEDURE [DataLesson.CommitToContentType]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idDataLesson			INT,
	@contentType			INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that the lesson data exists and is not completed

	*/

	IF (
		SELECT COUNT(1)
		FROM [tblData-Lesson]
		WHERE [idData-Lesson] = @idDataLesson
		AND idSite = @idCallerSite
		AND dtCompleted IS NULL
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataLessonCommitToContentType_NoRecordFound'
		RETURN 1
	    END

	/*

	set the flag

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	UPDATE [tblData-Lesson] SET
		contentTypeCommittedTo = @contentType,
		dtCommittedToContentType = @utcNow
	WHERE [idData-Lesson] = @idDataLesson

	/*

	do event log entry if task submitted or ojt request

	*/

	DECLARE @eventLogItem EventLogItemObjects
	DECLARE @idLesson INT

	IF @contentType = 3
		BEGIN
		SELECT @idLesson = idLesson FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson
		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idLesson, @idDataLesson, @idCaller
		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 305, @utcNow, @eventLogItem
		END

	IF @contentType = 4
		BEGIN
		SELECT @idLesson = idLesson FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson
		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idLesson, @idDataLesson, @idCaller
		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 304, @utcNow, @eventLogItem
		END
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

