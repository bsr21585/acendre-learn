-- =====================================================================
-- PROCEDURE: [Data-TinCan.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCan.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCan.Details]
GO

/*

Returns statement for a given id.

*/

CREATE PROCEDURE [Data-TinCan.Details]
(
	@Return_Code							INT						OUTPUT,
	@Error_Description_Code					NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idDataTinCan							INT						OUTPUT,
	@idSite									INT						OUTPUT,
	@idEndpoint								INT						OUTPUT,
	@isInternalAPI							BIT						OUTPUT,
	@statementId							NVARCHAR(50)			OUTPUT,
	@actor									NVARCHAR(max)			OUTPUT,
	@verbId									NVARCHAR(100)			OUTPUT,
	@verb									NVARCHAR(200)			OUTPUT,
	@activityId								NVARCHAR(100)			OUTPUT,
	@object									NVARCHAR(max)			OUTPUT,
	
	@mboxObject								NVARCHAR(100)			OUTPUT,
	@mboxSha1SumObject						NVARCHAR(100)			OUTPUT,
	@openIdObject							NVARCHAR(100)			OUTPUT,
	@accountObject							NVARCHAR(100)			OUTPUT,
	
	@mboxActor								NVARCHAR(100)			OUTPUT,
	@mboxSha1SumActor						NVARCHAR(100)			OUTPUT,
	@openIdActor							NVARCHAR(100)			OUTPUT,
	@accountActor							NVARCHAR(100)			OUTPUT,
	
	@mboxAuthority							NVARCHAR(100)			OUTPUT,
	@mboxSha1SumAuthority					NVARCHAR(100)			OUTPUT,
	@openIdAuthority						NVARCHAR(100)			OUTPUT,
	@accountAuthority						NVARCHAR(100)			OUTPUT,
	
	@mboxTeam								NVARCHAR(100)			OUTPUT,
	@mboxSha1SumTeam						NVARCHAR(100)			OUTPUT,
	@openIdTeam								NVARCHAR(100)			OUTPUT,
	@accountTeam							NVARCHAR(100)			OUTPUT,
	
	@mboxInstructor							NVARCHAR(100)			OUTPUT,
	@mboxSha1SumInstructor					NVARCHAR(100)			OUTPUT,
	@openIdInstructor						NVARCHAR(100)			OUTPUT,
	@accountInstructor						NVARCHAR(100)			OUTPUT,
	
	@registration							NVARCHAR(100)			OUTPUT,
	@statement								NVARCHAR(max)			OUTPUT,
	@isVoidingStatement						BIT						OUTPUT,
	@isStatementVoided						BIT						OUTPUT,
	@dtStored								DATETIME				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that the statement exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-TinCan]
		WHERE idSite = @idCallerSite
		AND statementId = @statementId
		AND isStatementVoided = @isStatementVoided
		) = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataTinCanDetails_StatementNotFound'
		RETURN 1
		END
	
	/*
	
	 validate uniqueness for Data TinCan, if specified
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-TinCan]
		WHERE idSite = @idCallerSite
		AND statementId = @statementId
		AND idEndpoint = @idEndpoint
		) = 0
		BEGIN
		SELECT @Return_Code = 3
		SET @Error_Description_Code = 'DataTinCanDetails_NoRecordFound' 
		RETURN 1
		END
	
	/*
	
	get the data 
	
	*/
	
	SELECT	@idDataTinCan = [idData-TinCan],
			@idSite = idSite,
			@idEndpoint = idEndpoint,
			@isInternalAPI = isInternalAPI,
			@statementId = statementId,
			@actor = actor,
			@verbId = verbId,
			@verb = verb,
			@activityId = activityId,
			@object = [object],
			@mboxObject = mboxObject,
			@mboxSha1SumObject = mboxSha1SumObject,
			@openIdObject = openIdObject,
			@accountObject = accountObject,
			@mboxActor = mboxActor,
			@mboxSha1SumActor = mboxSha1SumActor,
			@openIdActor = openIdActor,
			@accountActor = accountActor,
			@mboxAuthority = mboxAuthority,
			@mboxSha1SumAuthority = mboxSha1SumAuthority,
			@openIdAuthority = openIdAuthority,
			@accountAuthority = accountAuthority,
			@mboxTeam = mboxTeam,
			@mboxSha1SumTeam = mboxSha1SumTeam,
			@openIdTeam = openIdTeam,
			@accountTeam = accountTeam,
			@mboxInstructor = mboxInstructor,
			@mboxSha1SumInstructor = mboxSha1SumInstructor,
			@openIdInstructor = openIdInstructor,
			@accountInstructor = accountInstructor,
			@registration = registration,
			@statement = [statement],
			@dtStored = dtStored,
			@isVoidingStatement = isVoidingStatement,
			@isStatementVoided = isStatementVoided
	FROM	[tblData-TinCan]
	WHERE	idSite = @idCallerSite
	AND		statementId = @statementId
	AND		isStatementVoided = @isStatementVoided
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataTinCanDetails_StatementNotFound'
		END
	ELSE
		BEGIN
		SELECT @Return_Code = 0 --Success
		SELECT @Error_Description_Code = NULL
		END
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO