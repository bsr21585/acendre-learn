-- =====================================================================
-- PROCEDURE: [ReportDataSet.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportDataSet.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportDataSet.Details]
GO

/*

Return all the properties for a given report dataset id.

*/

CREATE PROCEDURE [ReportDataSet.Details]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idReportDataSet			INT				OUTPUT,
	@idSite						INT				OUTPUT, 
	@name						NVARCHAR(255)	OUTPUT,
	@storedProcedureName		NVARCHAR(50)	OUTPUT,
	@htmlTemplatePath			NVARCHAR(255)	OUTPUT,
	@description				NVARCHAR(512)	OUTPUT,	
	@order						INT				OUTPUT	
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141	

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDataset
		WHERE idDataset = @idReportDataSet
		AND (idSite = @idCallerSite OR idSite = 1)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ReportDataSetDetails_NoRecordFound'
		RETURN 1
	    END		

	/*
	
	get the data 
	
	*/
		
	SELECT
		@idReportDataSet		= RDS.idDataset,
		@idSite					= RDS.idSite,
		@name					= RDS.name,
		@storedProcedureName	= RDS.storedProcedureName,
		@htmlTemplatePath		= RDS.htmlTemplatePath,
		@description			= RDS.[description],
		@order					= RDS.[order]
	FROM tblDataset RDS	
	WHERE RDS.idDataset = @idReportDataSet	
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ReportDataSetDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO