-- =====================================================================
-- PROCEDURE: [User.HardDelete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.HardDelete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.HardDelete]
GO

/*

Hard deletes user(s)

*/

CREATE PROCEDURE [User.HardDelete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@Users					IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Users) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'UserHardDelete_NoRecordsSelected'
		RETURN 1
		END				
		
	/*
	
	Ensure the user(s) are marked as deleted. We do this prior to hard deletion to ensure that no events
	can happen for the user in the time it takes to hard delete all dependent records.
	
	*/
	
	UPDATE tblUser SET isDeleted = 1 WHERE idUser IN (SELECT id FROM @Users)
			
	/*
	
	DELETE (or REASSIGN in some cases) all objects dependent on the user(s) in order of dependency
	
	*/

	-- CERTIFICATE RECORDS
	DELETE FROM tblCertificateRecord WHERE idUser IN (SELECT id FROM @Users)

	UPDATE tblCertificateRecord SET idAwardedBy = 1 WHERE idAwardedBy IN (SELECT id FROM @Users)
	
	-- CERTIFICATE IMPORT RECORDS
	DELETE FROM tblCertificateImport WHERE idUserImported IN (SELECT id FROM @Users)
	
	UPDATE tblCertificateImport SET idUser = 1 WHERE idUser IN (SELECT id FROM @Users)
	
	-- COURSE ENROLLMENT APPROVERS
	DELETE FROM tblCourseApprover WHERE idUser IN (SELECT id FROM @Users) -- TABLE PENDING DELETION?
	
	DELETE FROM tblCourseEnrollmentApprover WHERE idUser IN (SELECT id FROM @Users)
	
	-- COURSE EXPERTS
	DELETE FROM tblCourseExpert WHERE idUser IN (SELECT id FROM @Users)
	
	-- COURSE FEED MESSAGES
	DELETE FROM tblCourseFeedMessage WHERE idParentCourseFeedMessage IN (
		SELECT idCourseFeedMessage FROM tblCourseFeedMessage WHERE idAuthor IN (SELECT id FROM @Users)
	)

	DELETE FROM tblCourseFeedMessage WHERE idAuthor IN (SELECT id FROM @Users)
	
	UPDATE tblCourseFeedMessage SET idApprover = 1 WHERE idApprover IN (SELECT id FROM @Users)
	
	-- COURSE FEED MODERATORS
	DELETE FROM tblCourseFeedModerator WHERE idModerator IN (SELECT id FROM @Users)
	
	-- COURSE RATINGS
	DELETE FROM tblCourseRating WHERE idUser IN (SELECT id FROM @Users)
	
	-- CERTIFICATION TO USER LINKS
	DELETE FROM [tblData-CertificationModuleRequirement] WHERE idCertificationToUserLink IN (
		SELECT idCertificationToUserLink FROM tblCertificationToUserLink WHERE idUser IN (SELECT id FROM @Users)
	)
	
	UPDATE [tblData-CertificationModuleRequirement] SET idApprover = 1 WHERE idApprover IN (SELECT id FROM @Users)
	
	DELETE FROM tblCertificationToUserLink WHERE idUser IN (SELECT id FROM @Users)

	-- LESSON DATA AND ENROLLMENTS
	DELETE FROM [tblData-SCOInt] WHERE [idData-SCO] IN (
		SELECT [idData-SCO] FROM [tblData-SCO] WHERE [idData-Lesson] IN (
			SELECT [idData-Lesson] FROM [tblData-Lesson] WHERE idEnrollment IN (
				SELECT idEnrollment FROM tblEnrollment WHERE idUser IN (SELECT id FROM @Users)
			)
		)
	)

	DELETE FROM [tblData-SCOObj] WHERE [idData-SCO] IN (
		SELECT [idData-SCO] FROM [tblData-SCO] WHERE [idData-Lesson] IN (
			SELECT [idData-Lesson] FROM [tblData-Lesson] WHERE idEnrollment IN (
				SELECT idEnrollment FROM tblEnrollment WHERE idUser IN (SELECT id FROM @Users)
			)
		)
	)

	DELETE FROM [tblData-TinCanContextActivities] WHERE [idData-TinCan] IN (
		SELECT [idData-TinCan] FROM [tblData-TinCan] WHERE [idData-Lesson] IN (
			SELECT [idData-Lesson] FROM [tblData-Lesson] WHERE idEnrollment IN (
				SELECT idEnrollment FROM tblEnrollment WHERE idUser IN (SELECT id FROM @Users)
			)
		)
	)

	DELETE FROM [tblData-SCO] WHERE [idData-Lesson] IN (
		SELECT [idData-Lesson] FROM [tblData-Lesson] WHERE idEnrollment IN (
			SELECT idEnrollment FROM tblEnrollment WHERE idUser IN (SELECT id FROM @Users)
		)
	)

	DELETE FROM [tblData-TinCan] WHERE [idData-Lesson] IN (
		SELECT [idData-Lesson] FROM [tblData-Lesson] WHERE idEnrollment IN (
			SELECT idEnrollment FROM tblEnrollment WHERE idUser IN (SELECT id FROM @Users)
		)
	)

	DELETE FROM [tblData-HomeworkAssignment] WHERE [idData-Lesson] IN (
		SELECT [idData-Lesson] FROM [tblData-Lesson] WHERE idEnrollment IN (
			SELECT idEnrollment FROM tblEnrollment WHERE idUser IN (SELECT id FROM @Users)
		)
	)

	DELETE FROM [tblData-Lesson] WHERE idEnrollment IN (
		SELECT idEnrollment FROM tblEnrollment WHERE idUser IN (SELECT id FROM @Users)
	)
	
	DELETE FROM tblEnrollment WHERE idUser IN (SELECT id FROM @Users)

	-- ENROLLMENT REQUESTS
	DELETE FROM tblEnrollmentRequest WHERE idUser IN (SELECT id FROM @Users)

	UPDATE tblEnrollmentRequest SET idApprover = 1 WHERE idApprover IN (SELECT id FROM @Users)

	-- ACTIVITY IMPORTS
	DELETE FROM tblActivityImport WHERE idUserImported IN (SELECT id FROM @Users)
	
	UPDATE tblActivityImport SET idUser = 1 WHERE idUser IN (SELECT id FROM @Users)

	-- EXCEPTION LOG
	DELETE FROM tblExceptionLog WHERE idUser IN (SELECT id FROM @Users)

	-- GROUP ENROLLMENT APPROVERS
	DELETE FROM tblGroupEnrollmentApprover WHERE idUser IN (SELECT id FROM @Users) -- TABLE PENDING DELETION?

	-- GROUP FEED MESSAGES
	DELETE FROM tblGroupFeedMessage WHERE idParentGroupFeedMessage IN (
		SELECT idGroupFeedMessage FROM tblGroupFeedMessage WHERE idAuthor IN (SELECT id FROM @Users)
	)

	DELETE FROM tblGroupFeedMessage WHERE idAuthor IN (SELECT id FROM @Users)
	
	UPDATE tblGroupFeedMessage SET idApprover = 1 WHERE idApprover IN (SELECT id FROM @Users)
	
	-- GROUP FEED MODERATORS
	DELETE FROM tblGroupFeedModerator WHERE idModerator IN (SELECT id FROM @Users)

	-- INBOX MESSAGES
	DELETE FROM tblInboxMessage WHERE idParentInboxMessage IN (
		SELECT idInboxMessage FROM tblInboxMessage WHERE idSender IN (SELECT id FROM @Users)
	)

	DELETE FROM tblInboxMessage WHERE idSender IN (SELECT id FROM @Users)

	DELETE FROM tblInboxMessage WHERE idParentInboxMessage IN (
		SELECT idInboxMessage FROM tblInboxMessage WHERE idRecipient IN (SELECT id FROM @Users)
	)

	DELETE FROM tblInboxMessage WHERE idRecipient IN (SELECT id FROM @Users)

	-- LEARNING PATH ENROLLMENT APPROVERS
	DELETE FROM tblLearningPathApprover WHERE idUser IN (SELECT id FROM @Users) -- TABLE PENDING DELETION?
	DELETE FROM tblLearningPathEnrollmentApprover WHERE idUser IN (SELECT id FROM @Users) -- TABLE PENDING DELETION?

	-- LEARNING PATH FEED MESSAGES
	DELETE FROM tblLearningPathFeedMessage WHERE idParentLearningPathFeedMessage IN (
		SELECT idLearningPathFeedMessage FROM tblLearningPathFeedMessage WHERE idAuthor IN (SELECT id FROM @Users) 
	) -- TABLE PENDING DELETION?

	DELETE FROM tblLearningPathFeedMessage WHERE idAuthor IN (SELECT id FROM @Users) -- TABLE PENDING DELETION?
	
	UPDATE tblLearningPathFeedMessage SET idApprover = 1 WHERE idApprover IN (SELECT id FROM @Users) -- TABLE PENDING DELETION?
	
	-- LEARNING PATH FEED MODERATORS
	DELETE FROM tblLearningPathFeedModerator WHERE idModerator IN (SELECT id FROM @Users) -- TABLE PENDING DELETION?

	-- LEARNING PATH ENROLLMENTS
	DELETE FROM tblLearningPathEnrollment WHERE idUser IN (SELECT id FROM @Users)

	-- TRANSACTION ITEMS, RESPONSES, AND PURCHASES
	DELETE FROM tblTransactionResponse WHERE idUser IN (SELECT id FROM @Users)

	DELETE FROM tblTransactionItem WHERE idUser IN (SELECT id FROM @Users)

	DELETE FROM tblTransactionItem WHERE idPurchase IN (
		SELECT idPurchase FROM tblPurchase WHERE idUser IN (SELECT id FROM @Users)
	)

	DELETE FROM tblPurchase WHERE idUser IN (SELECT id FROM @Users)

	-- QUIZZES AND SURVEYS
	UPDATE tblQuizSurvey SET idAuthor = 1 WHERE idAuthor IN (SELECT id FROM @Users)

	-- REPORT FILES, SUBSCRIPTIONS, AND REPORTS
	DELETE FROM tblReportFile WHERE idUser IN (SELECT id FROM @Users)

	DELETE FROM tblReportFile WHERE idReport IN (
		SELECT idReport FROM tblReport WHERE idUser IN (SELECT id FROM @Users)
	)

	DELETE FROM tblReportSubscriptionQueue WHERE idRecipient IN (SELECT id FROM @Users)

	DELETE FROM tblReportSubscription WHERE idUser IN (SELECT id FROM @Users)

	DELETE FROM tblReportSubscription WHERE idReport IN (
		SELECT idReport FROM tblReport WHERE idUser IN (SELECT id FROM @Users)
	)

	DELETE FROM tblReportToReportShortcutsWidgetLink WHERE idUser IN (SELECT id FROM @Users)

	DELETE FROM tblReportToReportShortcutsWidgetLink WHERE idReport IN (
		SELECT idReport FROM tblReport WHERE idUser IN (SELECT id FROM @Users)
	)

	DELETE FROM tblReportLanguage WHERE idReport IN (
		SELECT idReport FROM tblReport WHERE idUser IN (SELECT id FROM @Users)
	)

	DELETE FROM tblReport WHERE idUser IN (SELECT id FROM @Users)

	-- SSO TOKENS
	DELETE FROM tblSSOToken WHERE idUser IN (SELECT id FROM @Users)

	-- DOCUMENT REPOSITORY ITEMS - DELETE USER PROFILE FILES, REASSIGN OTHER OBJECTS
	DELETE FROM tblDocumentRepositoryItemLanguage WHERE idDocumentRepositoryItem IN (
		SELECT idDocumentRepositoryItem FROM tblDocumentRepositoryItem WHERE idDocumentRepositoryObjectType = 4 AND idOwner IN (SELECT id FROM @Users)
	)

	DELETE FROM tblDocumentRepositoryItem WHERE idDocumentRepositoryObjectType = 4 AND idOwner IN (SELECT id FROM @Users)

	UPDATE tblDocumentRepositoryItem SET idOwner = 1 WHERE idDocumentRepositoryObjectType <> 4 AND idOwner IN (SELECT id FROM @Users)

	-- RESOURCES
	UPDATE tblResource SET idOwner = 1 WHERE idOwner IN (SELECT id FROM @Users)

	-- ILT TO USER LINKS
	DELETE FROM tblStandupTrainingInstanceToUserLink WHERE idUser IN (SELECT id FROM @Users)

	-- ILT TO INSTRUCTOR LINKS	
	DELETE FROM tblStandupTrainingInstanceToInstructorLink WHERE idInstructor IN (SELECT id FROM @Users)

	-- USER TO COURSE FEED SUBSCRIPTIONS
	DELETE FROM tblUserToCourseFeedSubscription WHERE idUser IN (SELECT id FROM @Users)

	-- USER TO GROUP LINKS
	DELETE FROM tblUserToGroupLink WHERE idUser IN (SELECT id FROM @Users)

	-- USER TO LEARNING PATH LINKS
	DELETE FROM tblUserToLearningPathLink WHERE idUser IN (SELECT id FROM @Users) -- TABLE PENDING DELETION

	-- USER TO ROLE LINKS
	DELETE FROM tblUserToRoleLink WHERE idUser IN (SELECT id FROM @Users)

	-- USER TO RULESET ENROLLMENT LINKS
	DELETE FROM tblUserToRuleSetEnrollmentLink WHERE idUser IN (SELECT id FROM @Users) -- TABLE PENDING DELETION

	-- USER TO SUPERVISOR LINKS
	DELETE FROM tblUserToSupervisorLink WHERE idUser IN (SELECT id FROM @Users)

	DELETE FROM tblUserToSupervisorLink WHERE idSupervisor IN (SELECT id FROM @Users)

	-- WEB MEETING ORGANIZERS
	UPDATE tblWebMeetingOrganizer SET idUser = 1 WHERE idUser IN (SELECT id FROM @Users)

	-- EVENT EMAIL QUEUE
	DELETE FROM tblEventEmailQueue WHERE idObjectUser IN (SELECT id FROM @Users)

	DELETE FROM tblEventEmailQueue WHERE idRecipient IN (SELECT id FROM @Users)

	-- EVENT LOG
	DELETE FROM tblEventLog WHERE idObjectUser IN (SELECT id FROM @Users)

	UPDATE tblEventLog SET idExecutingUser = 1 WHERE idExecutingUser IN (SELECT id FROM @Users)

	-- USER TO APPROVER REASSIGNMENT
	UPDATE tblUser SET idApprover = 1 WHERE idApprover IN (SELECT id FROM @Users)

	-- NOTE: POSSIBLY CLEAN UP tblRuleSetEngineQueue, MONITOR TO SEE IF THERE ARE ANY ISSUES REQUIRING IT TO BE CLEANED UP
	
	/*

	DELETE the user record

	*/

	DELETE FROM tblUser WHERE idUser IN (SELECT id FROM @Users)

	/* RETURN */
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO