-- =====================================================================
-- PROCEDURE: [Report.AddShortcuts]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Report.AddShortcuts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Report.AddShortcuts]
GO

/*

Adds report shortcuts to the user dashboard

*/

CREATE PROCEDURE [Report.AddShortcuts]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idReports				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the user exists
	
	*/
	
	IF ((@idCaller <> 1) AND (
		SELECT COUNT(1)
		FROM tblUser
		WHERE idSite = @idCallerSite
		AND idUser = @idCaller
		) <> 1 )
		BEGIN
		SELECT @Return_Code = 2
		RETURN 1
		END
		
	/*
	
	validate that all the reports exist in the same site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @idReports RR
		LEFT JOIN tblReport R ON R.idReport = RR.id
		WHERE R.idReport IS NULL -- report does not exist at all
		OR R.idSite <> @idCallerSite -- report does not exist within the same site
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		RETURN 1 
		END
	
	/*
	
	insert the report shortcuts where they do not already exist
	
	*/
	
	INSERT INTO tblReportToReportShortcutsWidgetLink (idSite, idReport, idUser)
	SELECT @idCallerSite, RR.id, @idCaller
	FROM @idReports RR
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblReportToReportShortcutsWidgetLink RS
		WHERE RS.idUser = @idCaller	
		AND RS.idReport = RR.id
		AND RS.idSite = @idCallerSite
	)
	
	SELECT @Return_Code = 0
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO