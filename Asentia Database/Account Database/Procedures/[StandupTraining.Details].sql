-- =====================================================================
-- PROCEDURE: [StandupTraining.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.Details]
GO

/*

Return all the properties for a given standup training id.

*/
CREATE PROCEDURE [StandupTraining.Details]
(
	@Return_Code							INT				OUTPUT,
	@Error_Description_Code					NVARCHAR(50)	OUTPUT,
	@idCallerSite							INT				= 0, --default if not specified
	@callerLangString						NVARCHAR(10),
	@idCaller								INT				= 0,
	
	@idStandupTraining						INT				OUTPUT,
	@idSite									INT				OUTPUT, 
	@title									NVARCHAR(255)	OUTPUT,
	@avatar									NVARCHAR(255)	OUTPUT,
	@cost									FLOAT			OUTPUT,
	@description							NVARCHAR(MAX)	OUTPUT,
	@objectives								NVARCHAR(MAX)	OUTPUT,
	@isStandaloneEnroll						BIT				OUTPUT,
	@isRestrictedEnroll						BIT				OUTPUT,
	@isRestrictedDrop						BIT				OUTPUT,
	@searchTags								NVARCHAR(512)	OUTPUT,
	@dtCreated								DATETIME		OUTPUT,
	@dtModified								DATETIME		OUTPUT,
	@isDeleted								BIT				OUTPUT,
	@dtDeleted								DATETIME		OUTPUT,
	@isCallingUserEnrolledInUpcomingSession	BIT				OUTPUT,
	@shortcode								NVARCHAR(10)	OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	--IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
	--	BEGIN
	--	SET @Return_Code = 3 -- sort of (caller not member of specified site).
	--	SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
	--	RETURN 1
	--	END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblStandupTraining
		WHERE idStandUpTraining = @idStandupTraining
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'StandupTrainingDetails_NoRecordFound'
		RETURN 1
	    END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idStandupTraining	= ST.idStandupTraining,
		@idSite				= ST.idSite,
		@title				= ST.title,
		@description		= ST.[description],
		@objectives			= ST.objectives,
		@isStandaloneEnroll	= ST.isStandaloneEnroll,
		@isRestrictedEnroll	= ST.isRestrictedEnroll,
		@isRestrictedDrop	= ST.isRestrictedDrop,
		@searchTags			= ST.searchTags,
		@avatar				= ST.avatar,
		@cost				= ST.cost,
		@dtCreated			= ST.dtCreated,
		@dtModified			= ST.dtModified,
		@isDeleted			= ST.isDeleted,
		@dtDeleted			= ST.dtDeleted,
		@shortcode			= ST.shortcode
	FROM tblStandupTraining ST
	WHERE ST.idStandUpTraining = @idStandupTraining
	AND ST.idSite = @idCallerSite

	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'StandupTrainingDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

	/*
	
	determine if the calling user is currently enrolled in an upcoming session

	*/

	-- get all of the session ids and start times
	CREATE TABLE #StandupTrainingInstanceCalculatedValues (
		idStandUpTrainingInstance INT, 
		dtFirstStart DATETIME
	)
	
	-- start times
	INSERT INTO #StandupTrainingInstanceCalculatedValues (
		idStandUpTrainingInstance,
		dtFirstStart
	) 
	SELECT 
		STIMT.idStandUpTrainingInstance,
		MIN(STIMT.dtStart)
	FROM tblStandUpTrainingInstanceMeetingTime STIMT
	LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandUpTrainingInstance = STIMT.idStandUpTrainingInstance
	WHERE STI.idStandUpTraining = @idStandupTraining
	GROUP BY STIMT.idStandUpTrainingInstance

	-- delete the ones that have already started
	DELETE FROM #StandupTrainingInstanceCalculatedValues WHERE dtFirstStart <= GETUTCDATE()

	-- see if the caller is enrolled in any of the upcoming sessions
	IF (
		SELECT COUNT(1) 
		FROM tblStandupTrainingInstanceToUserLink STIUL
		WHERE STIUL.idStandupTrainingInstance IN (SELECT idStandupTrainingInstance FROM #StandupTrainingInstanceCalculatedValues)
		AND STIUL.idUser = @idCaller) > 0
		BEGIN
		SET @isCallingUserEnrolledInUpcomingSession = 1
		END
	ELSE
		BEGIN
		SET @isCallingUserEnrolledInUpcomingSession = 0
		END
	
	-- drop the temp table
	DROP TABLE #StandupTrainingInstanceCalculatedValues

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO