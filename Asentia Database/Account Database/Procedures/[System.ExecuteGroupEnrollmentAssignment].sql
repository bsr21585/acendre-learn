-- =====================================================================
-- PROCEDURE: [System.ExecuteGroupEnrollmentAssignment]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ExecuteGroupEnrollmentAssignment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ExecuteGroupEnrollmentAssignment]
GO

/*

Executes the Group.AssignEnrollments procedure for the entire system.

*/
CREATE PROCEDURE [System.ExecuteGroupEnrollmentAssignment]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* 
	
	Execute the Group.AssignEnrollments procedure with an empty @Groups filter 
	
	*/
	
	DECLARE @Groups IDTable

	EXEC [Group.AssignEnrollments] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @Groups

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO