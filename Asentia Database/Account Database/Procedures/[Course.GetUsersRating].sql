-- =====================================================================
-- PROCEDURE: [Course.GetUsersRating]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.GetUsersRating]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.GetUsersRating]
GO

/*

Returns the rating a specific user has given a course.

*/

CREATE PROCEDURE [Course.GetUsersRating]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCourse				INT,
	@idUser					INT, 
	@rating					INT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourse
		WHERE idCourse = @idCourse
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseGetUsersRating_NoRecordFound'
		RETURN 1
	    END

	/*
	
	get the data 
	
	*/
		
	SELECT
		@rating = CR.rating
	FROM tblCourseRating CR
	WHERE CR.idCourse = @idCourse
	AND CR.idUser = @idUser
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @rating = NULL
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO