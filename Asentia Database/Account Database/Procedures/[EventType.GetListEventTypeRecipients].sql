SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventType.GetListEventTypeRecipients]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventType.GetListEventTypeRecipients]
GO

/*
Returns a list of Event Types Information with Related Recipient ID
*/
CREATE PROCEDURE [EventType.GetListEventTypeRecipients]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@idEventTypePrefix		INT				
)
AS

	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*

	get the data

	*/
	
	SELECT
		ET.idEventType AS idEventType,
		ET.name AS name,
		ET.allowPriorSend AS allowPriorSend,
		ET.allowPostSend AS allowPostSend,
		STUFF((SELECT ',' + CAST(idEventTypeRecipient AS VARCHAR(10)) [text()] FROM tblEventTypeToEventTypeRecipientLink  WHERE idEventType = ET.idEventType FOR XML PATH(''), TYPE).value('.','NVARCHAR(50)'),1,1,'') idEventTypeRecipients
	FROM tblEventType ET	
	WHERE SUBSTRING(CAST(ET.idEventType AS VARCHAR(3)), 1, 1) = 
			CASE WHEN @idEventTypePrefix IS NOT NULL 
					THEN CAST(@idEventTypePrefix AS VARCHAR(1))
					ELSE SUBSTRING(CAST(ET.idEventType AS VARCHAR(3)), 1, 1) 
			END	
	ORDER BY ET.relativeOrder

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO