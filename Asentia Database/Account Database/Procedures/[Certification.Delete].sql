-- =====================================================================
-- PROCEDURE: [Certification.Delete]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Certification.Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) 
	DROP PROCEDURE [Certification.Delete]
GO

/*

Deletes certification

*/

CREATE PROCEDURE [Certification.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@Certifications			IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @Certifications) = 0 
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CertificationDelete_NoRecordFound'
		RETURN 1
		END

	/*

	validate that all certifications exist within the site

	*/

	IF (
		SELECT COUNT(1) 
		FROM @Certifications CC
		LEFT JOIN tblCertification C ON C.idCertification = CC.id
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
	   ) > 0 
	   
	   BEGIN
	   SET @Return_Code = 1
	   SET @Error_Description_Code = 'CertificationDelete_NoRecordFound'
	   RETURN 1
	   END

	/*

	ORIGINALLY WE WERE GOING TO SOFT DELETE THESE, HOWEVER IT MAKES MORE SENSE TO COMPLETELY DELETE THEM 
	AS THERE IS NO POINT IN TRACKING OR REPORTING ON A CERTIFICATION THAT THE ADMIN CHOOSES TO DELETE.
	THIS MAY CHANGE IN THE FUTURE
	
	DELETE IN ORDER OF DEPENDENCIES

	*/

	-- DELETE FROM [tblRulesetToCertificationLink]
	DELETE FROM [tblRulesetToCertificationLink] WHERE idCertification IN (SELECT id FROM @Certifications)
	
	--DELETE FROM [tblRuleSetEngineQueue]
	DELETE FROM tblRuleSetEngineQueue WHERE triggerObjectId IN (SELECT id FROM @Certifications) 
	AND triggerObject = 'certification' AND isProcessing IS NULL AND dtProcessed IS NULL

	/*

	SOFT DELETE all the entries in [tblEventEmailNotification] related to learning path(s) which we are deleting, where they are not already deleted

	*/

	UPDATE tblEventEmailNotification SET isDeleted = 1 WHERE idObject IN (SELECT id FROM @Certifications) AND (isDeleted IS NULL OR isDeleted = 0)
	AND idEventType >= 600 AND idEventType < 700 -- 600 - 700 is certification	

	-- DELETE FROM [tblData-CertificationModuleRequirement]
	DELETE FROM [tblData-CertificationModuleRequirement] WHERE idCertificationToUserLink IN (SELECT idCertificationToUserLink 
																							 FROM tblCertificationToUserLink 
																							 WHERE idCertification IN (SELECT id FROM @Certifications))

	-- DELETE FROM [tblCertificationToUserLink]
	DELETE FROM [tblCertificationToUserLink] WHERE idCertification IN (SELECT id FROM @Certifications)	

	-- DELETE FROM [tblCertificationToCourseCreditLink]
	DELETE FROM [tblCertificationToCourseCreditLink] WHERE idCertification IN (SELECT id FROM @Certifications)

	-- DELETE FROM [tblCertificationModuleRequirementToCourseLink]
	DELETE FROM [tblCertificationModuleRequirementToCourseLink] 
	WHERE idCertificationModuleRequirement IN (SELECT idCertificationModuleRequirement 
											   FROM tblCertificationModuleRequirement
											   WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
																							 FROM tblCertificationModuleRequirementSet
																							 WHERE idCertificationModule IN (SELECT idCertificationModule
																															 FROM tblCertificationModule
																															 WHERE idCertification IN (SELECT id FROM @Certifications))))

	-- DELETE FROM [tblCertificationModuleRequirementLanguage]
	DELETE FROM [tblCertificationModuleRequirementLanguage]
	WHERE idCertificationModuleRequirement IN (SELECT idCertificationModuleRequirement 
											   FROM tblCertificationModuleRequirement
											   WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
																							 FROM tblCertificationModuleRequirementSet
																							 WHERE idCertificationModule IN (SELECT idCertificationModule
																															 FROM tblCertificationModule
																															 WHERE idCertification IN (SELECT id FROM @Certifications))))

	-- DELETE FROM [tblCertificationModuleRequirement]
	DELETE FROM [tblCertificationModuleRequirement]
	WHERE idCertificationModuleRequirement IN (SELECT idCertificationModuleRequirement 
											   FROM tblCertificationModuleRequirement
											   WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
																							 FROM tblCertificationModuleRequirementSet
																							 WHERE idCertificationModule IN (SELECT idCertificationModule
																															 FROM tblCertificationModule
																															 WHERE idCertification IN (SELECT id FROM @Certifications))))

	-- DELETE FROM [tblCertificationModuleRequirementSetLanguage]
	DELETE FROM [tblCertificationModuleRequirementSetLanguage]
	WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
												  FROM tblCertificationModuleRequirementSet
												  WHERE idCertificationModule IN (SELECT idCertificationModule
																				  FROM tblCertificationModule
																				  WHERE idCertification IN (SELECT id FROM @Certifications)))

	-- DELETE FROM [tblCertificationModuleRequirementSet]
	DELETE FROM [tblCertificationModuleRequirementSet]
	WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
												  FROM tblCertificationModuleRequirementSet
												  WHERE idCertificationModule IN (SELECT idCertificationModule
																				  FROM tblCertificationModule
																				  WHERE idCertification IN (SELECT id FROM @Certifications)))

	-- DELETE FROM [tblCertificationModuleLanguage]
	DELETE FROM [tblCertificationModuleLanguage]
	WHERE idCertificationModule IN (SELECT idCertificationModule
								    FROM tblCertificationModule
								    WHERE idCertification IN (SELECT id FROM @Certifications))

	-- DELETE FROM [tblCertificationModule]
	DELETE FROM [tblCertificationModule]
	WHERE idCertificationModule IN (SELECT idCertificationModule
								    FROM tblCertificationModule
								    WHERE idCertification IN (SELECT id FROM @Certifications))

	-- DELETE FROM [tblCertificationLanguage]
	DELETE FROM [tblCertificationLanguage] WHERE idCertification IN (SELECT id FROM @Certifications)

	-- DELETE FROM [tblCertification]
	DELETE FROM [tblCertification] WHERE idCertification IN (SELECT id FROM @Certifications)

	/*

	RETURN

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO