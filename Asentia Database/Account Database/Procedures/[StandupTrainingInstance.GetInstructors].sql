-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.GetInstructors]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.GetInstructors]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.GetInstructors]
GO

/*

Returns a recordset of instructor ids and names that belong to a standup training instance.

*/

CREATE PROCEDURE [StandupTrainingInstance.GetInstructors]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@idStandupTrainingInstance	INT,
	@searchParam				NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/

	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		SELECT 
			DISTINCT
			I.idUser AS idInstructor, 
			I.displayName AS displayName,
			I.displayName + ' (' + I.username + ')' AS displayNameWithUsername
		FROM tblUser I
		LEFT JOIN tblStandupTrainingInstanceToInstructorLink STII ON STII.idInstructor = I.idUser
		WHERE I.idSite = @idCallerSite
		AND STII.idStandupTrainingInstance = @idStandupTrainingInstance
		AND ((I.isDeleted IS NULL OR I.isDeleted = 0) AND (I.isRegistrationApproved IS NULL OR I.isRegistrationApproved = 1))
		ORDER BY displayName

		END

	ELSE

		BEGIN

		SELECT 
			DISTINCT
			I.idUser AS idInstructor, 
			I.displayName AS displayName,
			I.displayName + ' (' + I.username + ')' AS displayNameWithUsername
		FROM tblUser I
		INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = I.idUser
		LEFT JOIN tblStandupTrainingInstanceToInstructorLink STII ON STII.idInstructor = I.idUser
		WHERE I.idSite = @idCallerSite
		AND STII.idStandupTrainingInstance = @idStandupTrainingInstance
		AND ((I.isDeleted IS NULL OR I.isDeleted = 0) AND (I.isRegistrationApproved IS NULL OR I.isRegistrationApproved = 1))
		ORDER BY displayName

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	