-- =====================================================================
-- PROCEDURE: [Dataset.UserGroupSnapshot]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT
	*
FROM dbo.sysobjects
WHERE id = OBJECT_ID(N'[Dataset.UserGroupSnapshot]')
AND OBJECTPROPERTY(id, N'IsProcedure') = 1) DROP PROCEDURE [Dataset.UserGroupSnapshot]
GO

/*

Certificates Analytics Dataset

*/

CREATE PROCEDURE [Dataset.UserGroupSnapshot]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@frequency                  INT             = 0,
	@groupByClause				NVARCHAR(768),
	@dateFilter                 NVARCHAR(255),
    @whereClause                NVARCHAR(MAX),
	@orderClause				NVARCHAR(MAX),
	@activity					INT
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END



	/*

	get the id of the caller's language

	*/

	DECLARE @idCallerLanguage INT
	SELECT
		@idCallerLanguage = CASE
							WHEN idLanguage IS NULL THEN 57
							ELSE idLanguage
							END
	FROM tblLanguage
	WHERE code = @callerLangString



	/*

		build sql query

	*/
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @active NVARCHAR(30)='active'
	DECLARE @inactive NVARCHAR(30)='inactive'
	DECLARE @disabled NVARCHAR(30)='disabled'

	DECLARE @userString NVARCHAR(30)='Users'

	IF @activity = 1
	BEGIN
		SET @sql = 'CREATE TABLE #temp (Frequency NVARCHAR(20),LegendName NVARCHAR(30),Count INT)'
			 
				 + ' INSERT INTO #temp VALUES'
				 + '(''' + CONVERT(NVARCHAR, @active) + ''''
				 + ',''' + CONVERT(NVARCHAR, @userString) + ''', (SELECT COUNT(*) FROM tblUser WHERE '
				 + 'idSite = '+ CONVERT(NVARCHAR, @idCallerSite) + ' AND isActive = 1 AND (isDeleted = 0 OR isDeleted IS NULL) AND dtLastLogin > DATEADD(mm, -2, GETDATE()))) '

				 + ' INSERT INTO #temp VALUES'
				 + '('''+ CONVERT(NVARCHAR, @inactive) + ''', '
				 + '''' + CONVERT(NVARCHAR, @userString) + ''', (SELECT COUNT(*) FROM tblUser WHERE idSite = '+ CONVERT	(NVARCHAR, @idCallerSite) + ' AND  isActive = 0 OR							 dtLastLogin < DATEADD(mm, -2, GETDATE())) )'
			 
				 + ' INSERT INTO #temp VALUES'
				 + '(''' + CONVERT(NVARCHAR, @disabled) + ''' ,'
				 + '''' + CONVERT(NVARCHAR, @userString) + ''',(SELECT COUNT(*) FROM tblUser WHERE idSite = '+ CONVERT(NVARCHAR, @idCallerSite) + 'AND isDeleted = 1))'
			 			 
				 + 'SELECT '
				 + 'Frequency, '
				 + 'LegendName, '
				 + 'Count ' 
				 + 'FROM #temp'
	END

	ELSE IF @activity = 2
	BEGIN 
			SET @sql = 'SELECT ' 
					 + 'G.name AS Frequency, '
					 + '''Groups'' AS LegendName, '
					 + 'COUNT(UGL.iduser) AS Count '
					 + 'FROM tblusertogrouplink UGL '
					 + 'INNER JOIN tblgroup G '
					 + 'ON UGL.idgroup = G.idgroup '
					 + 'WHERE '
					 + 'UGL.idgroup IN ('
					 + 'SELECT '
					 + 'idgroup '
					 + 'FROM tblgroup '
					 + ')'
					 + ' AND UGL.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '
					 + 'GROUP BY UGL.idgroup,G.name '
				 

		IF(@orderClause IS NOT NULL)
		BEGIN 
		SET @sql = @sql + @orderClause
		END

	END

	/*

	execute the sql statement and log its execution

	*/

	DECLARE @dtExecutionBegin DATETIME
	DECLARE @dtExecutionEnd DATETIME
	DECLARE @executionDurationMS INT

	SET @dtExecutionBegin = GETUTCDATE()

	EXEC sp_executesql @sql

	SET @dtExecutionEnd = GETUTCDATE()

	SET @executionDurationMS = DATEDIFF(ms, @dtExecutionBegin, @dtExecutionEnd)

	INSERT INTO tblReportProcessorLog (idSite,
	idCaller,
	datasetProcedure,
	dtExecutionBegin,
	dtExecutionEnd,
	executionDurationSeconds,
	sqlQuery)
		SELECT
			@idCallerSite,
			@idCaller,
			'[Dataset.UserGroupSnapshot]',
			@dtExecutionBegin,
			@dtExecutionEnd,
			CAST((CAST(@executionDurationMS AS DECIMAL) / 1000) AS DECIMAL(9, 2)),
			@sql

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO