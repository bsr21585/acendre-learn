-- =====================================================================
-- PROCEDURE: [LearningPathEnrollment.SynchronizeCourseData]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPathEnrollment.SynchronizeCourseData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPathEnrollment.SynchronizeCourseData]
GO


/*

Synchronizes course data for a specified learning path enrollment.

*/

CREATE PROCEDURE [LearningPathEnrollment.SynchronizeCourseData]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idLearningPathEnrollment	INT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblLearningPathEnrollment]
		WHERE [idLearningPathEnrollment] = @idLearningPathEnrollment
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LPESynchronizeCourseData_NoRecordFound'
		RETURN 1
		END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/* declare and get information for learning path enrollment */

	DECLARE @dtStart				DATETIME
	DECLARE @idTimezone				INT
	DECLARE @dtCompleted			DATETIME
	DECLARE @idLearningPath			INT
	DECLARE @learningPathTitle		NVARCHAR(255)	
	DECLARE @learningPathModified	DATETIME
	DECLARE @dtLastSynchronized		DATETIME

	SELECT
		@dtStart = LPE.dtStart,
		@idTimezone = LPE.idTimezone,
		@dtCompleted = LPE.dtCompleted,
		@idLearningPath = LPE.idLearningPath,
		@learningPathTitle = LP.name,		
		@learningPathModified = LP.dtModified,
		@dtLastSynchronized = LPE.dtLastSynchronized
	FROM tblLearningPathEnrollment LPE
	LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
	WHERE LPE.idLearningPathEnrollment = @idLearningPathEnrollment

	/* exit if learning path does not exist (marked deleted / no course id) */

	IF @idLearningPath IS NULL
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		RETURN 1
		END

	/* exit if learning path enrollment has been completed */

	IF @dtCompleted IS NOT NULL
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		RETURN 1
		END

	/* exit if learning path enrollment synchronization has been done and is current */

	IF (
		@learningPathModified IS NOT NULL
		AND @dtLastSynchronized IS NOT NULL
		AND @dtLastSynchronized >= @learningPathModified		
		)
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		RETURN 1
		END

	/* SYNCHRONIZE: update learning path information for learning path enrollment */

	UPDATE tblLearningPathEnrollment SET		
		title = CASE WHEN @learningPathTitle IS NOT NULL AND @learningPathTitle <> '' THEN @learningPathTitle ELSE title END
	WHERE idLearningPathEnrollment = @idLearningPathEnrollment

	/* SYNCHRONIZE: remove course enrollments inherited by learning path, but no longer part of the learning path */

	DECLARE @CourseEnrollmentIdsToDelete IDTable

	INSERT INTO @CourseEnrollmentIdsToDelete 
	(
		id
	)
	SELECT 
		E.idEnrollment
	FROM tblLearningPathEnrollment LPE
	LEFT JOIN tblEnrollment E ON E.idLearningPathEnrollment = LPE.idLearningPathEnrollment	
	WHERE LPE.idLearningPathEnrollment = @idLearningPathEnrollment
	AND E.idCourse NOT IN (SELECT LPCL.idCourse
						   FROM tblLearningPathToCourseLink LPCL
						   WHERE idLearningPath = @idLearningPath)
	AND E.dtCompleted IS NULL
	
	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @CourseEnrollmentIdsToDelete, 0	

	/* SYNCHRONIZE: insert course enrollments for courses that have been added to the learning path since last sync */

	INSERT INTO tblEnrollment (
		idSite,
		idCourse,
		revcode,
		idUser,
		idLearningPathEnrollment,
		isLockedByPrerequisites,
		idTimezone,
		dtCreated,
		dtStart,
		dtDue,
		dueInterval,
		dueTimeframe,
		dtExpiresFromStart,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe,
		title,
		code,
		credits
	)
	SELECT
		LPE.idSite,
		LPCL.idCourse,
		C.revcode,
		LPE.idUser,
		LPE.idLearningPathEnrollment,
		1,
		LPE.idTimezone,
		LPE.dtCreated,
		LPE.dtStart,
		LPE.dtDue,
		LPE.dueInterval,
		LPE.dueTimeFrame,
		LPE.dtExpiresFromStart,
		LPE.expiresFromStartInterval,
		LPE.expiresFromStartTimeframe,
		LPE.expiresFromFirstLaunchInterval,
		LPE.expiresFromFirstLaunchTimeframe,
		C.title,
		C.coursecode,
		C.credits
	FROM tblLearningPathEnrollment LPE
	LEFT JOIN tblLearningPathToCourseLink LPCL ON LPCL.idLearningPath = LPE.idLearningPath
	LEFT JOIN tblCourse C ON C.idCourse = LPCL.idCourse
	WHERE LPE.idLearningPathEnrollment = @idLearningPathEnrollment
	AND NOT EXISTS (SELECT 1 FROM tblEnrollment E
					WHERE E.idCourse = LPCL.idCourse
					AND E.idUser = LPE.idUser
					AND 
					(
						-- incompete/active
						(
						E.dtCompleted IS NULL
						AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
						AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
						)
					OR
						-- completed and within a year prior to the learning path enrollment's start
						(
						E.dtCompleted IS NOT NULL
						AND E.dtCompleted >= DATEADD(yyyy, -1, LPE.dtStart)
						)
					)
				)

	/*

	do the event log entries for course enrollments

	*/

	DECLARE @enrollmentEventLogItems EventLogItemObjects

	INSERT INTO @enrollmentEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		LPE.idSite,
		E.idCourse,
		E.idEnrollment, 
		LPE.idUser
	FROM tblLearningPathEnrollment LPE
	LEFT JOIN tblEnrollment E ON E.idLearningPathEnrollment = LPE.idLearningPathEnrollment AND E.idUser = LPE.idUser
	WHERE LPE.idLearningPathEnrollment = @idLearningPathEnrollment
	AND E.idCourse IS NOT NULL
	AND E.idEnrollment IS NOT NULL

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 201, @utcNow, @enrollmentEventLogItems

	/* SYNCHRONIZE: update the last synchronized datetime for the learning path enrollment */

	UPDATE tblLearningPathEnrollment SET
		dtLastSynchronized = @utcNow
	WHERE idLearningPathEnrollment = @idLearningPathEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO