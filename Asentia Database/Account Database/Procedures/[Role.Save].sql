-- =====================================================================
-- PROCEDURE: [Role.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Role.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Role.Save]
GO

/*

Adds new role or updates existing role.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [Role.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idRole					INT				OUTPUT, 
	@name					NVARCHAR (255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED
	
	
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			SET @Error_Description_Code = 'RoleSave_SpecifiedLanguageNotDefault'
			RETURN 1 
		END
	
	*/
			 
	/*
	
	validate uniqueness
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblRole
		WHERE idSite = @idCallerSite
		AND name = @name
		AND (
			@idRole IS NULL
			OR @idRole <> idRole
			)
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RoleSave_FieldNotUnique'
		RETURN 1 
		END

	/*
	
	validate uniqueness within language
	
	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF( 
		SELECT COUNT(1)
		FROM tblRole R
		LEFT JOIN tblRoleLanguage RL ON RL.idRole = R.idRole AND RL.idLanguage = @idDefaultLanguage -- same language
		WHERE R.idSite = @idCallerSite -- same site
		AND R.idRole <> @idRole -- not the same role
		AND RL.name = @name -- validate parameter: name
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RoleSave_FieldNotUnique'
		RETURN 1
		END
	
	/*
	
	save the data
	
	*/
	
	IF (@idRole = 0 OR @idRole is null)
		
		BEGIN
		
		-- insert the new object
		
		INSERT INTO tblRole (
			idSite, 
			name
		)			
		VALUES (
			@idCallerSite, 
			@name
		)
		
		-- get the new object id and return successfully
		
		SELECT @idRole = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the object id exists
		
		IF (SELECT COUNT(1) FROM tblRole WHERE idRole = @idRole AND idSite = @idCallerSite) < 1
			
			BEGIN
				SET @idRole = @idRole
				SET @Return_Code = 1 --(1 is 'details not found')
				SET @Error_Description_Code = 'RoleSave_NoRecordFound'
				RETURN 1
			END
			
		-- update existing role's properties
		
		UPDATE tblRole SET
			name = @name
		WHERE idRole = @idRole 
		
		-- get the id and return successfully
		
		SELECT @idRole = @idRole
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblRoleLanguage RL WHERE RL.idRole = @idRole AND RL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblRoleLanguage SET
			name = @name
		WHERE idRole = @idRole
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblRoleLanguage (
			idSite,
			idRole,
			idLanguage,
			name
		)
		SELECT
			@idCallerSite,
			@idRole,
			@idDefaultLanguage,
			@name
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblRoleLanguage RL
			WHERE RL.idRole = @idRole
			AND RL.idLanguage = @idDefaultLanguage
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO