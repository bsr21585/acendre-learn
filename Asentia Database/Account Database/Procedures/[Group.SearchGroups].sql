-- =====================================================================
-- PROCEDURE: [Group.SearchGroups]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.SearchGroups]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.SearchGroups]
GO

/*

Return all the courses containg a given specific string in title.

*/
CREATE PROCEDURE [Group.SearchGroups]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@searchParam			NVARCHAR(30),
	@selfJoinAllowedGroups	BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	--IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
	--	BEGIN
	--	SET @Return_Code = 3 -- sort of (caller not member of specified site).
	--	SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
	--	RETURN 1
	--	END

	/*
	
	validate caller permission
	
	*/

	--IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
	--	BEGIN
	--	SET @Return_Code = 3
	--	SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
	--	RETURN 1
	--	END


	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite

			
	/*
	
	get the data 
	
	*/
	IF(@searchParam <> '')	
	BEGIN
		SELECT 
		G.idGroup,
		CASE WHEN GL.name IS NULL OR GL.name = '' THEN G.name ELSE GL.name END AS name,
		G.avatar,
		CASE WHEN GL.shortDescription IS NULL OR GL.shortDescription = '' THEN G.shortDescription ELSE GL.shortDescription END AS shortDescription,
		CASE WHEN GL.longDescription IS NULL OR GL.longDescription = '' THEN G.longDescription ELSE GL.longDescription END AS longDescription
		FROM tblGroup G
		LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
		WHERE G.name LIKE '%'+ @searchParam +'%'
		AND	  G.isSelfJoinAllowed = @selfJoinAllowedGroups	
		AND G.idSite = @idCallerSite

	END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''			
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO