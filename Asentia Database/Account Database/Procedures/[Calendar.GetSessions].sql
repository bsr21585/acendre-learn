-- =====================================================================
-- PROCEDURE: [Calendar.GetSessions]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Calendar.GetSessions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Calendar.GetSessions]
GO

/*

Returns a listing of ILT sessions that occur within the same month as a specified date.
This will pull sessions that may be available for enrollment either by standalone or as part of a course.

*/

CREATE PROCEDURE [Calendar.GetSessions]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified

	@calendarDate			DATETIME			
)
AS
	
	BEGIN
	SET NOCOUNT ON	
			
	/*
	
	get the caller's language
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	/*
	
	get the month timespan, plus and minus 1 day, based off of the specified date
	this is to grab only the events that occur within the same month as the specified date
	
	*/

	DECLARE @startDate DATETIME	
	SET @startDate = DATEADD(DAY, -(DAY(@calendarDate)), @calendarDate)
	SET @startDate = DATEADD(DAY, DATEDIFF(DAY, '19000101', @startDate), '19000101')
	
	DECLARE @endDate DATETIME
	SET @endDate = DATEADD(DAY, -(DAY(DATEADD(mm, 1, @calendarDate)) - 1), DATEADD(mm, 1, @calendarDate))
	SET @endDate = DATEADD(DAY, DATEDIFF(DAY, '19000101', @endDate), '23:59:59')

	/* 
	
	get the calendar items

	*/	
	
	SELECT DISTINCT
		STI.idStandupTrainingInstance AS [id],
		'session' AS [event],
		CASE WHEN STL.title IS NOT NULL THEN STL.title ELSE ST.title END
		+ ' - ' +
		CASE WHEN STIL.title IS NOT NULL THEN STIL.title ELSE STI.title END
		AS [eventTitle],
		STIMT.dtStart AS [dtEvent]
	FROM tblStandUpTrainingInstanceMeetingTime STIMT
	LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandupTrainingInstance = STIMT.idStandupTrainingInstance
	LEFT JOIN tblStandUpTraining ST ON ST.idStandUpTraining = STI.idStandupTraining
	LEFT JOIN tblStandupTrainingLanguage STL ON STL.idStandUpTraining = ST.idStandUpTraining AND STL.idLanguage = @idCallerLanguage
	LEFT JOIN tblStandupTrainingInstanceLanguage STIL ON STIL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance AND STIL.idLanguage = @idCallerLanguage
	LEFT JOIN tblLessonToContentLink LCL ON LCL.idObject = ST.idStandUpTraining AND LCL.idContentType = 2
	LEFT JOIN tblLesson L ON L.idLesson = LCL.idLesson
	LEFT JOIN tblCourse C ON C.idCourse = L.idCourse
	WHERE STIMT.dtStart >= @startDate 
	AND STIMT.dtStart <= @endDate
	AND STI.idSite = @idCallerSite
	AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)
	AND (
			(ST.isStandaloneEnroll = 1)
			OR
			(
			 ST.isStandaloneEnroll = 0
			 AND C.idCourse IS NOT NULL
			 AND C.isPublished = 1
			 AND (C.isDeleted = 0 OR C.isDeleted IS NULL)
			 AND L.idLesson IS NOT NULL
			 AND (L.isDeleted = 0 OR L.isDeleted IS NULL)
			)
		)
	AND ST.isRestrictedEnroll = 0
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
