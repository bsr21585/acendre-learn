-- =====================================================================
-- PROCEDURE: [DataSCO.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataSCO.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataSCO.Save]
GO


/*

Returns all the properties for a lesson data given id.

*/

CREATE PROCEDURE [DataSCO.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idDataSCO				INT,	
	@idDataLesson			INT,
	@dotNetName				NVARCHAR(510),	--value of dotnetName
	@manifestIdentifier		NVARCHAR(255),
	@launchType				NVARCHAR(510),
	@completionStatus		INT,
	@successStatus			INT,
	@scoreScaled			FLOAT,
	@totalTime				FLOAT,	
	@actualAttemptCount		INT,
	@effectiveAttemptCount	INT				
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF @idCaller = 1
		SET @idCaller = 1 -- dont validate admin login
	ELSE IF @idCaller IS NULL
		OR @idCallerSite IS NULL
		OR @idCallerSite = 0
		OR @idCaller = 0
		OR (SELECT idSite FROM tblUser WHERE idUser = @idCaller) IS NULL 
		OR (SELECT idSite FROM tblUser WHERE idUser = @idCaller) <> @idCallerSite
	
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = 'DataSCOSave_PermissionError'
		RETURN 1
		END

	DECLARE @idTimezone INT
	DECLARE @currentCompletionStatusValue NVARCHAR(20)
	DECLARE @currentSuccessStatusValue NVARCHAR(20)
	DECLARE @currentScoreScaled FLOAT
	DECLARE @currentTotalTime FLOAT	
	DECLARE @completionStatusValue NVARCHAR(20)
	DECLARE @successStatusValue NVARCHAR(20)

	-- get the current completion and success statuses
	SELECT
		@currentCompletionStatusValue = CASE WHEN completionStatus IS NOT NULL THEN
											(SELECT [value] FROM  [tblSCORMVocabulary] WHERE idSCORMVocabulary = completionStatus)
										ELSE
											''
										END,
		@currentSuccessStatusValue = CASE WHEN successStatus IS NOT NULL THEN
											(SELECT [value] FROM  [tblSCORMVocabulary] WHERE idSCORMVocabulary = successStatus)
										ELSE
											''
										END,
		@currentScoreScaled = scoreScaled,
		@currentTotalTime = totalTime		
	FROM [tblData-SCO]
	WHERE idSite = @idCallerSite 
	AND  [idData-SCO] = @idDataSCO
	AND [idData-Lesson] = @idDataLesson

	-- SET @utcNow
	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	--SET @idTimezone
	SET @idTimezone = (SELECT idTimezone 
						FROM  [tblTimezone]
					    WHERE dotNetName = @dotNetName)

	--SET @completionStatus
	SET @completionStatusValue = (SELECT [value]  
								  FROM  [tblSCORMVocabulary]
								  WHERE idSCORMVocabulary = @completionStatus)
	--SET @successStatus
	SET @successStatusValue = (SELECT [value]
							   FROM [tblSCORMVocabulary]
							   WHERE idSCORMVocabulary = @successStatus)

	-- if success is passed, and completion is unknown, make completion completed
	if (@successStatusValue = 'passed' AND @completionStatusValue = 'unknown')
		BEGIN
		SET @completionStatus = 2
		SET @completionStatusValue = 'completed'
		END		

	-- declare variable for doing the event log entry and enrollment completion evaluation, 
	-- those things will only be done when when SCO data is updated
	DECLARE @doEventLogEntry BIT
	DECLARE @doEnrollmentCompletionEvaluation BIT

	SET @doEventLogEntry = 0
	SET @doEnrollmentCompletionEvaluation = 0

	-- if the sco data exists and was not already completed, update the SCO data record
	IF EXISTS(SELECT 1 FROM [tblData-SCO] WHERE [idData-Lesson] = @idDataLesson)
	   AND @currentCompletionStatusValue <> 'completed'
		BEGIN
			-- update the record
			UPDATE [tblData-Sco]
			SET 	completionStatus = @completionStatus,
					successStatus = @successStatus,
					scoreScaled = CONVERT(DECIMAL(10,2), @scoreScaled),
					totalTime = @totalTime,
					[timeStamp] = @utcNow,
					actualAttemptCount = @actualAttemptCount,
					effectiveAttemptCount = @effectiveAttemptCount
					WHERE idSite = @idCallerSite 
					AND [idData-SCO] = @idDataSCO
					AND [idData-Lesson] = @idDataLesson

			-- set the event log entry and enrollment completion evaluation flags
			SET @doEventLogEntry = 1
			SET @doEnrollmentCompletionEvaluation = 1
		END

	-- if the SCO data is completed but not failed, or was passed and completion status is completed or unknown, 
	-- and was not already completed, update the lesson data record
	IF ((@completionStatusValue = 'completed' AND @successStatusValue <> 'failed') OR (@successStatusValue = 'passed' AND (@completionStatusValue = 'completed' OR @completionStatusValue = 'unknown')))
	   AND @currentCompletionStatusValue <> 'completed'
		BEGIN
			UPDATE [tblData-Lesson] SET dtCompleted = @utcNow 
			WHERE [idData-Lesson] = @idDataLesson
			AND idSite = @idCallerSite 

			-- If launcher is a learner then set contentTypeCommittedTo flag on  
			IF @launchType = 'learner' 
			BEGIN
				UPDATE [tblData-Lesson] SET contentTypeCommittedTo = 1 
				WHERE [idData-Lesson] = @idDataLesson
				AND idSite = @idCallerSite	
			END
			-- OJT launch, update proctoring user
			ELSE
			BEGIN
				UPDATE [tblData-SCO] SET 
					proctoringUser = @idCaller
				WHERE idSite = @idCallerSite 
				AND [idData-SCO] = @idDataSCO
				AND [idData-Lesson] = @idDataLesson
			END

			-- set the enrollment completion evaluation flag
			SET @doEnrollmentCompletionEvaluation = 1
		END

	-- if the current SCO's status is completed, and the success, score, or total time are not in sync,
	-- update with the latest data. this should only happen in cases where"
	--		1) it is a single-SCO package
	--		2) the status was switched from incomplete to completed in the SCO, thus triggering a database save
	--		3) that status was set before success and score were set
	IF (((@currentSuccessStatusValue <> @successStatusValue) OR (@currentTotalTime <> @totalTime) OR (@currentScoreScaled <> @scoreScaled)) AND @currentCompletionStatusValue = 'completed')
		BEGIN

			-- update the record
			UPDATE [tblData-Sco]
			SET 	successStatus = @successStatus,
					scoreScaled = CONVERT(DECIMAL(10,2), @scoreScaled),
					totalTime = @totalTime
					--[timeStamp] = @utcNow,					
					WHERE idSite = @idCallerSite 
					AND [idData-SCO] = @idDataSCO
					AND [idData-Lesson] = @idDataLesson

		END

	/*

	do event log entry for completed, passed, or failed

	*/

	IF (@doEventLogEntry = 1)
		BEGIN

		DECLARE @eventLogItem EventLogItemObjects
		DECLARE @idLesson INT
		DECLARE @idUser INT
		DECLARE @idEventType INT

		SELECT
			@idLesson = LD.idLesson,
			@idUser = E.idUser
		FROM [tblData-Lesson] LD
		LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
		WHERE LD.[idData-Lesson] = @idDataLesson

		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idLesson, @idDataLesson, @idUser

		IF (@completionStatus = 2 AND @successStatus <> 5 AND @successStatus <> 4)
			BEGIN
			SET @idEventType = 303 -- completed
			END
			
		IF (@successStatus = 4 AND (@completionStatus = 2 OR @completionStatus = 0))
			BEGIN
			SET @idEventType = 301 -- passed
			END

		IF (@successStatus = 5)
			BEGIN
			SET @idEventType = 302 -- failed
			END

		IF @idEventType IS NOT NULL
			BEGIN
			EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, @idEventType, @utcNow, @eventLogItem
			END
		END

	/*

	check for course completion

	*/

	IF (@doEnrollmentCompletionEvaluation = 1)
		BEGIN

		DECLARE @idEnrollment INT
		SELECT @idEnrollment = idEnrollment FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson

		EXEC [Enrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idEnrollment

		END

	/*
	
	update first launch date and expiration from first launch date for enrollment

	*/

	-- first launch
	UPDATE tblEnrollment SET
		dtFirstLaunch = GETUTCDATE() 		   
	WHERE idEnrollment = @idEnrollment
	AND dtFirstLaunch IS NULL

	-- expiration from first launch
	UPDATE tblEnrollment SET
		dtExpiresFromFirstLaunch = CASE WHEN dtExpiresFromFirstLaunch IS NULL AND expiresFromFirstLaunchInterval IS NOT NULL AND expiresFromFirstLaunchTimeframe IS NOT NULL THEN 
										CASE WHEN expiresFromFirstLaunchTimeframe = 'd' THEN
											DATEADD(d, expiresFromFirstLaunchInterval, dtFirstLaunch)
										WHEN expiresFromFirstLaunchTimeframe = 'ww' THEN
											DATEADD(ww, expiresFromFirstLaunchInterval, dtFirstLaunch)
										WHEN expiresFromFirstLaunchTimeframe = 'm' THEN
											DATEADD(m, expiresFromFirstLaunchInterval, dtFirstLaunch)
										WHEN expiresFromFirstLaunchTimeframe = 'yyyy' THEN
											DATEADD(yyyy, expiresFromFirstLaunchInterval, dtFirstLaunch)
										ELSE
											dtExpiresFromFirstLaunch
										END
								   ELSE
										dtExpiresFromFirstLaunch
								   END
	FROM tblEnrollment E
	WHERE E.idEnrollment = @idEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO