-- =====================================================================
-- PROCEDURE: [Data-TinCanState.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCanState.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCanState.Details]
GO

/*

Returns tin can activity state for the given ids (stateId, activityId, agent & registration).

*/

CREATE PROCEDURE [Data-TinCanState.Details]
(
	@Return_Code							INT						OUTPUT,
	@Error_Description_Code					NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idData_TinCanState						INT						OUTPUT,
	@idSite									INT						OUTPUT,
	@idEndpoint								INT						OUTPUT,
	@stateId								NVARCHAR(100)			OUTPUT,
	@activityId								NVARCHAR(100)			OUTPUT,
	@agent									NVARCHAR(MAX)			OUTPUT,
	@registration							NVARCHAR(50)			OUTPUT,
	@contentType							NVARCHAR(50)			OUTPUT,
	@docContents							VARBINARY(MAX)			OUTPUT,
	@dtUpdated								DATETIME				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON

	IF (@idEndpoint = 0)
     SET @idEndpoint	=	NUll


	/*
	
	validate that tin can activity state exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-TinCanState]
		WHERE idSite = @idCallerSite
		AND activityId = @activityId
		AND agent = @agent
		AND stateId = @stateId
		AND (registration = @registration OR (@registration IS NULL AND registration IS NULL))
		) = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataTinCanStateDetails_ActivityStateNotFound'
		RETURN 1
		END
	
	/*
	
	validate caller permission to the specified tin can activity state
	
	*/
	
	--IF (
	--	SELECT COUNT(1)
	--	FROM [tblData-TinCanState]
	--	WHERE idSite = @idCallerSite
	--	AND idEndpoint = @idEndpoint
	--	AND activityId = @activityId
	--	AND agent = @agent
	--	AND stateId = @stateId
	--	AND (registration = @registration OR (@registration IS NULL AND registration IS NULL))
	--	) = 0
	--	BEGIN
	--	SELECT @Return_Code = 3
	--	SET @Error_Description_Code = 'TinCanStateDetails_PermissionError'
	--	RETURN 1
	--	END
	
	/*
	
	get the data 
	
	*/
	
	SELECT	@idData_TinCanState = [idData-TinCanState],
			@idSite = idSite,
			@idEndpoint = idEndpoint,
			@stateId = stateId,
			@activityId = activityId,
			@agent = agent,
			@registration = registration,
			@contentType = contentType,
			@docContents = docContents,
			@dtUpdated = dtUpdated
	FROM	[tblData-TinCanState]
	WHERE	idSite = @idCallerSite
	AND		(idEndpoint = @idEndpoint OR idEndpoint IS NULL)
	AND		activityId = @activityId
	AND		agent = @agent
	AND		stateId = @stateId
	AND		(registration = @registration OR (@registration IS NULL AND registration IS NULL))
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataTinCanStateDetails_ActivityStateNotFound'
		END
	ELSE
		BEGIN
		SELECT @Return_Code = 0 --Success
		SELECT @Error_Description_Code = NULL
		END
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO