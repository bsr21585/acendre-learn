-- =====================================================================
-- PROCEDURE: [LearningPath.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.SaveLang]
GO

/*

Saves learning path "language specific" properties for specific language
in learning path language table.

*/

CREATE PROCEDURE [LearningPath.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idLearningPath			INT,
	@languageString			NVARCHAR(10),
	@name					NVARCHAR(255), 
	@shortDescription		NVARCHAR(512), 
	@longDescription		NVARCHAR(MAX),
	@searchTags				NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LearningPathSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
			 
	/*
	
	validate that the learning path exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLearningPath
		WHERE idLearningPath = @idLearningPath
		AND idSite = @idCallerSite
		AND dtDeleted IS NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LearningPathSaveLang_DetailsNotFound'
		RETURN 1 
		END

	/*

	check XSS vulnerabilities

	*/

	-- longDescription

	IF (@longDescription LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LearningPathSaveLang_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LearningPathSaveLang_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LearningPathSaveLang_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LearningPathSaveLang_DescTagNotAllowed_Iframe'
	RETURN 1 
	END
		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblLearningPathLanguage LPL WHERE LPL.idLearningPath = @idLearningPath AND LPL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblLearningPathLanguage SET
			name = @name,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			searchTags = @searchTags
		WHERE idLearningPath = @idLearningPath
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblLearningPathLanguage (
			idSite,
			idLearningPath,
			idLanguage,
			name,
			shortDescription,
			longDescription,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idLearningPath,
			@idLanguage,
			@name,
			@shortDescription,
			@longDescription,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblLearningPathLanguage LPL
			WHERE LPL.idLearningPath = @idLearningPath
			AND LPL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblLearningPath SET
			name = @name,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			searchTags = @searchTags
		WHERE idLearningPath = @idLearningPath

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO