-- =====================================================================
-- PROCEDURE: [Enrollment.SynchronizeLessonData]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.SynchronizeLessonData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.SynchronizeLessonData]
GO


/*

Synchronizes lesson data for a specified enrollment.

*/

CREATE PROCEDURE [Enrollment.SynchronizeLessonData]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idEnrollment			INT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblEnrollment]
		WHERE [idEnrollment] = @idEnrollment
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentSynchronizeLessonData_NoRecordFound'
		RETURN 1
		END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/* declare and get information for enrollment */

	DECLARE @dtStart			DATETIME
	DECLARE @idTimezone			INT
	DECLARE @dtCompleted		DATETIME
	DECLARE @idCourse			INT
	DECLARE @courseTitle		NVARCHAR(255)
	DECLARE @courseCode			NVARCHAR(255)
	DECLARE @courseCredits		FLOAT
	DECLARE @courseRevCode		NVARCHAR(32)
	DECLARE @courseModified		DATETIME
	DECLARE @dtLastSynchronized	DATETIME

	SELECT
		@dtStart = E.dtStart,
		@idTimezone = E.idTimezone,
		@dtCompleted = E.dtCompleted,
		@idCourse = E.idCourse,
		@courseTitle = C.title,
		@courseCode = C.coursecode,
		@courseRevCode = C.revcode,
		@courseModified = C.dtModified,
		@dtLastSynchronized = E.dtLastSynchronized
	FROM tblEnrollment E
	LEFT JOIN tblCourse C ON C.idCourse = E.idCourse AND (C.isDeleted IS NULL OR C.isDeleted = 0)
	WHERE E.idEnrollment = @idEnrollment

	/* exit if course does not exist (marked deleted / no course id) */

	IF @idCourse IS NULL
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		RETURN 1
		END

	/* exit if enrollment has been completed */

	IF @dtCompleted IS NOT NULL
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		RETURN 1
		END

	/* exit if enrollment synchronization has been done and is current */

	IF (
		@courseModified IS NOT NULL
		AND @dtLastSynchronized IS NOT NULL
		AND @dtLastSynchronized >= @courseModified
		AND (SELECT COUNT(1) FROM [tblData-Lesson] WHERE idEnrollment = @idEnrollment) > 0
		)
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		RETURN 1
		END

	/* SYNCHRONIZE: update course information for enrollment */

	UPDATE tblEnrollment SET
		revcode = CASE WHEN @courseRevCode IS NOT NULL AND @courseRevCode <> '' THEN @courseRevCode ELSE revcode END,
		title = CASE WHEN @courseTitle IS NOT NULL AND @courseTitle <> '' THEN @courseTitle ELSE title END,
		code = CASE WHEN @courseCode IS NOT NULL AND @courseCode <> '' THEN @courseCode ELSE code END
	WHERE idEnrollment = @idEnrollment

	/* SYNCHRONIZE: remove lesson data for lessons no longer part of course */

	CREATE TABLE #LessonDataIdsToDelete ([idData-Lesson] INT, [idData-SCO] INT)

	INSERT INTO #LessonDataIdsToDelete 
	(
		[idData-Lesson],
		[idData-SCO]
	)
	SELECT 
		DL.[idData-Lesson],
		DS.[idData-SCO]
	FROM [tblData-Lesson] DL
	LEFT JOIN [tblData-SCO] DS ON DS.[idData-Lesson] = DL.[idData-Lesson]
	WHERE DL.idEnrollment = @idEnrollment
	AND DL.idLesson IN (SELECT L.idLesson 
						FROM tblLesson L
						WHERE idCourse = @idCourse
						AND isDeleted = 1)
	
	-- delete SCO lesson data
	DELETE FROM [tblData-SCOInt] WHERE [idData-SCO] IN (SELECT DISTINCT [idData-SCO] FROM #LessonDataIdsToDelete)

	DELETE FROM [tblData-SCOObj] WHERE [idData-SCO] IN (SELECT DISTINCT [idData-SCO] FROM #LessonDataIdsToDelete)

	DELETE FROM [tblData-SCO] WHERE [idData-SCO] IN (SELECT DISTINCT [idData-SCO] FROM #LessonDataIdsToDelete)

	-- delete any "internally-launched" TinCan lesson data
	DELETE FROM [tblData-TinCanContextActivities] 
	WHERE [idData-TinCan] IN (SELECT [idData-TinCan] 
							  FROM [tblData-TinCan] 
							  WHERE [idData-Lesson] IN (SELECT [idData-Lesson] FROM #LessonDataIdsToDelete)
							 )

	DELETE FROM [tblData-TinCan] WHERE [idData-Lesson] IN (SELECT [idData-Lesson] FROM #LessonDataIdsToDelete)

	-- delete task data
	DELETE FROM [tblData-HomeworkAssignment] WHERE [idData-Lesson] IN (SELECT [idData-Lesson] FROM #LessonDataIdsToDelete)

	-- delete lesson data
	DELETE FROM [tblData-Lesson] WHERE [idData-Lesson] IN (SELECT DISTINCT [idData-Lesson] FROM #LessonDataIdsToDelete)

	DROP TABLE #LessonDataIdsToDelete

	/* SYNCHRONIZE: update lesson data information and ordering */

	UPDATE [tblData-Lesson] SET
		[tblData-Lesson].title = L.title,
		[tblData-Lesson].revcode = L.revcode,
		[tblData-Lesson].[order] = L.[order]
	FROM [tblData-Lesson], tblLesson L
	WHERE [tblData-Lesson].idEnrollment = @idEnrollment
	AND [tblData-Lesson].idLesson = L.idLesson
	AND (L.isDeleted IS NULL OR L.isDeleted = 0)

	/* SYNCHRONIZE: insert lesson data for lessons that have been added since last sync */

	INSERT INTO [tblData-Lesson] (
		idSite,
		idEnrollment,
		idLesson,
		title,
		revcode,
		[order],
		idTimezone
	)
	SELECT
		@idCallerSite,
		@idEnrollment,
		L.idLesson,
		L.title,
		L.revcode,
		L.[order],
		@idTimezone
	FROM tblLesson L
	WHERE L.idCourse = @idCourse
	AND (L.isDeleted IS NULL OR L.isDeleted = 0)
	AND NOT EXISTS ( -- lesson data record does not already exist
		SELECT 1
		FROM [tblData-Lesson]
		WHERE [tblData-Lesson].idEnrollment = @idEnrollment
		AND [tblData-Lesson].idLesson = L.idLesson
	)

	/* SYNCHRONIZE: update the last synchronized datetime for the enrollment */

	UPDATE tblEnrollment SET
		dtLastSynchronized = @utcNow
	WHERE idEnrollment = @idEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO