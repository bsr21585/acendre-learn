-- =====================================================================
-- PROCEDURE: [Site.GetDomains]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Site.GetDomains]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.GetDomains]
GO

/*

Returns a recordset of  ids and names that are domains of a site. 

*/

CREATE PROCEDURE [Site.GetDomains]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idSite                INT,
	@hostName              NVARCHAR(255)
)
AS

	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSite
		WHERE idSite = @idSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		RETURN 1
		END



		/*
		Gets the list of domain aaliases 
		*/
	
		SELECT
			DISTINCT
			STDAL.idSiteToDomainAliasLink AS id, 
			STDAL.domain AS displayName
		FROM tblSiteToDomainAliasLink STDAL
		WHERE STDAL.idSite = @idSite
		AND STDAL.domain <> @hostName
		AND (STDAL.domain IS NOT NULL AND STDAL.domain <> '')

		ORDER BY displayName

		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	