-- =====================================================================
-- PROCEDURE: [DataLesson.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataLesson.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.Save]
GO

/*

Adds new lesson data or updates existing lesson data.

*/

CREATE PROCEDURE [DataLesson.Save]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idDataLesson				INT				OUTPUT,
	@idEnrollment				INT,
	@idLesson					INT,
	@idTimezone					INT
)
AS

	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	save the data

	*/

	IF (@idDataLesson = 0 OR @idDataLesson IS NULL)

		BEGIN

		-- insert the new lesson data

		INSERT INTO [tblData-Lesson] (
			idSite,
			idEnrollment,
			idLesson,
			idTimezone,
			title,
			revcode,
			[order]
		)
		SELECT
			@idCallerSite,
			@idEnrollment,
			@idLesson,
			@idTimezone,
			L.title,
			L.revcode,
			L.[order]
		FROM tblLesson L
		WHERE L.idLesson = @idLesson

		-- get the new lesson data's id

		SET @idDataLesson = SCOPE_IDENTITY()

		END

	ELSE

		BEGIN

		-- check that the lesson data id exists
		IF (SELECT COUNT(1) FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idDataLesson = @idDataLesson
			SET @Return_Code = 1
			SET @Error_Description_Code = 'DataLessonSave_NoRecordFound'
			RETURN 1

			END

		-- update existing enrollment's properties

		UPDATE [tblData-Lesson] SET
			idTimezone = @idTimezone
		WHERE [idData-Lesson] = @idDataLesson

		-- get the lesson data's id

		SET @idDataLesson = @idDataLesson

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		