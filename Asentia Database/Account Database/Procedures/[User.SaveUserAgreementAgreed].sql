-- =====================================================================
-- PROCEDURE: [User.SaveUserAgreementAgreed]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.SaveUserAgreementAgreed]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.SaveUserAgreementAgreed]
GO

/*

Saves a user's avatar path to the database.

*/
CREATE PROCEDURE [User.SaveUserAgreementAgreed]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idUser					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	

	-- update existing user's properties
	UPDATE tblUser SET
		dtUserAgreementAgreed = GETUTCDATE()
	WHERE idUser = @idUser

	SET @Return_Code = 0 --(0 is 'success')
	SET @Error_Description_Code = ''

	END			

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
