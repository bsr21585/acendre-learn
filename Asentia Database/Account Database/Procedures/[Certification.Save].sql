-- =====================================================================
-- PROCEDURE: [Certification.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.Save]
GO

/*

Adds new certification or updates existing certification.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [Certification.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idCertification					INT				OUTPUT,	
	@title								NVARCHAR(255),
	@shortDescription					NVARCHAR(512),
	@searchTags							NVARCHAR(512),
	@initialAwardExpiresInterval		INT,
	@initialAwardExpiresTimeframe		NVARCHAR(4),
	@renewalExpiresInterval				INT,
	@renewalExpiresTimeframe			NVARCHAR(4),
	@accreditingOrganization			NVARCHAR(255),	
	@certificationContactName			NVARCHAR(255),
	@certificationContactEmail			NVARCHAR(255),
	@certificationContactPhoneNumber	NVARCHAR(20),
	@isPublished						BIT,
	@isClosed							BIT,
	@isAnyModule						BIT,
	@isRenewalAnyModule					BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED


	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'CertificationSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/
		
	/*
	
	save the data
	
	*/
	
	IF (@idCertification = 0 OR @idCertification IS NULL)
		
		BEGIN
		
		-- insert the new certification
		
		INSERT INTO tblCertification (
			idSite,			
			title,
			shortDescription,
			searchTags,
			dtCreated,
			dtModified,
			isDeleted,
			dtDeleted,
			initialAwardExpiresInterval,
			initialAwardExpiresTimeframe,
			renewalExpiresInterval,
			renewalExpiresTimeframe,
			accreditingOrganization,			
			certificationContactName,
			certificationContactEmail,
			certificationContactPhoneNumber,
			isPublished,
			isClosed,
			isAnyModule,
			isRenewalAnyModule
		)			
		VALUES (
			@idCallerSite,			
			@title,
			@shortDescription,
			@searchTags,
			GETUTCDATE(),
			GETUTCDATE(),
			0,
			NULL,
			@initialAwardExpiresInterval,
			@initialAwardExpiresTimeframe,
			@renewalExpiresInterval,
			@renewalExpiresTimeframe,
			@accreditingOrganization,			
			@certificationContactName,
			@certificationContactEmail,
			@certificationContactPhoneNumber,
			@isPublished,
			@isClosed,
			@isAnyModule,
			@isRenewalAnyModule
		)
		
		-- get the new certification's id
		
		SET @idCertification = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the certification id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblCertification WHERE idCertification = @idCertification AND idSite = @idCallerSite AND (isDeleted IS NULL OR isDeleted = 0)) < 1
			BEGIN
			
			SET @idCertification = @idCertification
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertificationSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing certification's properties
		
		UPDATE tblCertification SET			
			title = @title,
			shortDescription = @shortDescription,
			searchTags = @searchTags,
			dtModified = GETUTCDATE(),
			initialAwardExpiresInterval = @initialAwardExpiresInterval,
			initialAwardExpiresTimeframe = @initialAwardExpiresTimeframe,
			renewalExpiresInterval = @renewalExpiresInterval,
			renewalExpiresTimeframe = @renewalExpiresTimeframe,
			accreditingOrganization = @accreditingOrganization,			
			certificationContactName = @certificationContactName,
			certificationContactEmail = @certificationContactEmail,
			certificationContactPhoneNumber = @certificationContactPhoneNumber, 
			isPublished = @isPublished,
			isClosed = @isClosed,
			isAnyModule = @isAnyModule,
			isRenewalAnyModule = @isRenewalAnyModule
		WHERE idCertification = @idCertification

		-- get the certification's id 

		SET @idCertification = @idCertification
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblCertificationLanguage CL WHERE CL.idCertification = @idCertification AND CL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblCertificationLanguage SET
			title = @title,
			shortDescription = @shortDescription,
			searchTags = @searchTags
		WHERE idCertification = @idCertification
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCertificationLanguage (
			idSite,
			idCertification,
			idLanguage,
			title,
			shortDescription,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idCertification,
			@idDefaultLanguage,
			@title,
			@shortDescription,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCertificationLanguage CL
			WHERE CL.idCertification = @idCertification
			AND CL.idLanguage = @idDefaultLanguage
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO