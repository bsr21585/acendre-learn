-- =====================================================================
-- PROCEDURE: [Dataset.CourseCompletions]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Dataset.CourseCompletions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Dataset.CourseCompletions]
GO

/*

course completions Dataset

*/

CREATE PROCEDURE [Dataset.CourseCompletions]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@frequency                  INT             = 0,
	@dateFilter				    NVARCHAR(255),
	@groupByClause				NVARCHAR(255),
	@whereClause                NVARCHAR(MAX),
	@activity                   INT ,
	@orderClause                NVARCHAR(MAX)  = NULL
	        
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END


	/*

	get the id of the caller's language

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage =
		CASE WHEN idLanguage IS NULL THEN 
			57
		ELSE 
			idLanguage 
		END
		FROM tblLanguage WHERE code = @callerLangString

		/*
		DESCRIPTION

		@activity = 1 means Total Course Completions per (day, week, month, and year)
		@activity = 1 means Number of Course Completions by Course
		@orderClause IS NOT NULL means there is atleast one course has been selected
		@orderClause IS NULL means there is no course selected so we have to get data for all courses
		@frequency <> 5 means selected frequency is not year it should be one of day, week, month, and year
		@frequency  = 5 means selected frequency is year
		@loopType is used to decide loop is required or not if yes then in what way
		every returen data must have three columns :
		               COUNT : to tnumber of records
					   Frequency : Selected frequency it should be day, week, month, and year or course which is used to apply group by clause of query
					   LegendName : It will be used when we display the analytic at it will describe whet it is
		*/



	/*

		build sql query

	*/
		DECLARE @sql NVARCHAR(MAX)

	  SET @sql = ' SELECT COUNT(E.idCourse)  AS Count , '


	   IF @groupByClause IS NOT NULL AND @groupByClause <> ''
		 BEGIN
		 SET @groupByClause = REPLACE(@groupByClause,'XXX','E.dtCompleted')
		 END

		 --Build select list of sql query 
		 IF(@frequency > 0 AND @frequency <> 5 AND @activity = 1 AND @orderClause IS NOT   NULL) --group by frequency (day,hour,month,week)  when atleast one course is selected
			BEGIN 
			SET @sql = @sql +' AC.value AS Frequency  ,E.idcourse '
			END
		IF(@frequency > 0 AND @frequency = 5 AND @activity = 1 AND @orderClause IS NOT NULL) --group by frequency (year) when atleast one course is selected
			BEGIN 
			SET @sql = @sql + @groupByClause +' AS Frequency ,E.idcourse ,C.title AS LegendName '
			END
		Else IF (@activity = 2) 
			BEGIN
			SET @sql = @sql +'  C.title AS Frequency ,''Courses''' + ' AS LegendName '
			END

			--Check when there is no course selected means @orderClause is  null
			IF(@activity = 1 AND @orderClause IS   NULL AND @frequency = 5)
			BEGIN
			SET  @sql = @sql + @groupByClause +' AS Frequency ,''Courses''' + ' AS LegendName '
			END
			ELSE IF(@activity = 1 AND @orderClause IS   NULL AND @frequency <> 5)
			BEGIN
			SET  @sql = @sql + @groupByClause +' AS Frequency '
			END


	 IF @dateFilter IS NOT NULL AND @dateFilter <> ''
		BEGIN
		SET @dateFilter = REPLACE(@dateFilter,'XXX','E.dtCompleted')
		END

		--validate if frequency is not year and greater than 0  and activity is Total Course Completions per frequency selected
		--@frequency =1 =Hour
		--@frequency =2 =Day
		--@frequency =3 =Week
		--@frequency =4 =Month
		--@frequency =5 =year
		IF(@frequency > 0 AND @frequency <> 5 AND @activity = 1 AND @orderClause IS  NOT NULL)
		 BEGIN
		     --Where atleast one course is selected
			   SET @sql =          @sql					+ '  ,AC.idconstant ,C.title FROM  tblCourse C  '
														+ ' LEFT JOIN tblEnrollment E ON E.idCourse = C.idCourse '
														+ ' LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
														+ ' lEFT JOIN tblanalyticConstants AC  ON AC.value = '+ @groupByClause+ ' '
														+ ' LEFT JOIN tblTimezone TZ ON TZ.idTimezone = E.idTimezone '
														+ ' LEFT JOIN tblTimezoneLanguage TZL ON TZL.idTimezone = TZ.idTimezone AND TZL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
														+ ' WHERE (C.isDeleted IS NULL OR C.isDeleted = 0) '
														+ ' AND E.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '
														+ ' AND E.dtCompleted IS NOT NULL AND ' + @dateFilter + ' '
														+ ' AND AC.idconstantType = ' + CONVERT(NVARCHAR, @frequency) + ' '
																		
						
		END 
		--When no course is selected
		ELSE IF(@activity = 1 AND @orderClause IS NULL)
		BEGIN
				 SET @sql        = @sql 	            + ' FROM  tblCourse C  '
														+ ' LEFT JOIN tblEnrollment E ON E.idCourse = C.idCourse  '
													    + ' LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = 57 '
														+ ' LEFT JOIN tblTimezone TZ ON TZ.idTimezone = E.idTimezone '
														+ ' LEFT JOIN tblTimezoneLanguage TZL ON TZL.idTimezone = TZ.idTimezone AND TZL.idLanguage = 57 '
														+ ' WHERE (C.isDeleted IS NULL OR C.isDeleted = 0) '
														+ ' AND E.dtCompleted IS NOT NULL AND ' + @dateFilter + ' '
													    + ' AND E.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '
														

		END

		ELSE --IF (@activity = 2)
		BEGIN

		        SET @sql =			 @sql				+ ' FROM '
														+ ' tblCourse C  '
														+ ' LEFT JOIN tblEnrollment E ON E.idCourse = C.idCourse AND E.dtCompleted IS NOT NULL AND ' + @dateFilter + ' '
														+ ' LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
														+ ' LEFT JOIN tblTimezone TZ ON TZ.idTimezone = E.idTimezone '
														+ ' LEFT JOIN tblTimezoneLanguage TZL ON TZL.idTimezone = TZ.idTimezone AND TZL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
														+ ' WHERE (C.isDeleted IS NULL OR C.isDeleted = 0) '
														+ ' AND C.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '

		END

		 /*
		build group by clause
	     */					

	 IF @whereClause IS NOT NULL AND @whereClause <> ''
		BEGIN
	    SET @whereClause = REPLACE(@whereClause,'CCC','C.idCourse')
		SET @sql = @sql +' '+ @whereClause
		END
	
		/*
		build group by clause
	     */

	IF(@frequency > 0 AND @frequency <> 5 AND @activity = 1 AND @orderClause IS NOT NULL) --group by frequency (day,hour,month,week)
		BEGIN 
		SET @groupByClause = ' AC.value, E.idcourse, AC.idconstant ,C.title'
		END
   ELSE IF(@frequency > 0 AND @frequency = 5 AND @activity = 1 AND @orderClause IS NOT NULL)
        BEGIN
		SET @groupByClause = REPLACE(@groupByClause,'XXX','E.dtCompleted') 
		SET @groupByClause =	@groupByClause + ' ,E.idCourse , C.title '
		
		END
	Else IF (@activity = 2) 
		BEGIN
		SET  @groupByClause = ' C.idCourse , C.title '
		END

			IF(@activity = 1 AND @orderClause IS  NULL)
			BEGIN
		    SET @groupByClause =  @groupByClause 
			END
	
		 SET @sql = @sql+ ' GROUP BY ' + @groupByClause

	   /*
		build order by clause
		*/
		
		IF(@frequency > 0 AND @frequency <> 5 AND @activity = 1 AND @orderClause IS NOT NULL) 
		BEGIN 
		SET @sql = @sql+ ' ORDER BY AC.idConstant ASC '
		END
	Else IF (@activity = 2) 
		BEGIN
	    SET @sql = @sql+ ' ORDER BY C.idCourse ASC '
		END

		--decide loop is required or not if yes then in what way
      DECLARE @loopType INT =0
	  IF (@orderClause IS NOT  NULL AND (@frequency = 5 OR @activity = 2))
	  BEGIN
	  SET @loopType = 0
	  END
	  IF(@orderClause IS NULL AND @activity = 1 AND @frequency <> 5)
	  BEGIN
	  SET @loopType = 1
	  END
	  IF(@orderClause IS NOT NULL AND @activity = 1 AND @frequency <> 5)
	  BEGIN
	  SET @loopType = 2
	  END
	 
	IF(@loopType = 1)
	BEGIN
	SET @sql =' DECLARE @temp table(
									Count INT,
									Frequency INT
									) '
								+ ' INSERT INTO @temp '
								+   @sql
								+ ' SELECT ISNULL(T.[Count],0) AS Count , '
								+ ' AC.value AS Frequency , '
								+ '''Courses''' + ' AS LegendName '
								+ ' FROM  tblAnalyticConstants AC LEFT JOIN @temp T ON AC.value = T.Frequency '
							    + ' WHERE  AC.idConstantType = ' + CONVERT(NVARCHAR, @frequency) + ' '
	END







	ELSE IF(@loopType = 2)
	BEGIN
	SET @sql =' DECLARE @temp table(
									Count INT,
									Frequency INT,
									idCourse INT,
									idConstant INT,
									LegendName NVARCHAR(100)
									) '
								+ ' INSERT INTO @temp '
								+   @sql
								+ ' DECLARE @idCourse INT '
								+ ' DECLARE CursorResult Cursor FOR '
                                + ' SELECT Distinct idCourse from @temp WHERE idCourse IS NOT NULL '
								+ ' OPEN CursorResult '
								+ ' FETCH NEXT FROM CursorResult  '
								+ ' INTO @idCourse '
								+ ' WHILE @@FETCH_STATUS = 0 '
								+ ' BEGIN '
								+ ' SELECT ISNULL(T.[Count],0) AS Count , '
								+ ' AC.value AS Frequency , '
								+ ' T.idCourse , '
								+ ' T.idConstant , '
								+ ' (select C.title from tblCourse C where C.idCourse = @idCourse) AS LegendName  '
								+ ' FROM  tblAnalyticConstants AC LEFT JOIN @temp T ON AC.value = T.Frequency AND idCourse  = @idCourse '
								+ ' WHERE  AC.idConstantType = ' + CONVERT(NVARCHAR, @frequency) + ' '
								+ ' ORDER BY Frequency ASC '
								+ ' FETCH NEXT FROM CursorResult '
								+ ' INTO @idCourse ' 
								+ ' END '
								+ ' CLOSE CursorResult; '
								+ ' DEALLOCATE CursorResult; '
	END 
	
	/*

	execute the sql statement and log its execution

	*/

	DECLARE @dtExecutionBegin DATETIME
	DECLARE @dtExecutionEnd DATETIME
	DECLARE @executionDurationMS INT

	SET @dtExecutionBegin = GETUTCDATE()

	EXEC sp_executesql @sql

	SET @dtExecutionEnd = GETUTCDATE()

	SET @executionDurationMS = DATEDIFF(ms, @dtExecutionBegin, @dtExecutionEnd)

	INSERT INTO tblReportProcessorLog
	(
		idSite,
		idCaller,
		datasetProcedure,
		dtExecutionBegin,
		dtExecutionEnd,
		executionDurationSeconds,
		sqlQuery
	)
	SELECT
		@idCallerSite,
		@idCaller,
		'[Dataset.CourseCompletions]',
		@dtExecutionBegin,
		@dtExecutionEnd,
		CAST((CAST(@executionDurationMS AS DECIMAL)/1000) AS DECIMAL(9,2)),
		@sql

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO