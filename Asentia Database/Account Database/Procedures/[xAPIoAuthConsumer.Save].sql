-- =====================================================================
-- PROCEDURE: [xAPIoAuthConsumer.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[xAPIoAuthConsumer.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [xAPIoAuthConsumer.Save]
GO

/*

To store the record of Token

*/

CREATE PROCEDURE [xAPIoAuthConsumer.Save]
(
	-- Add the parameters for the stored procedure here	
    @Return_Code			INT				OUTPUT ,
    @Error_Description_Code NVARCHAR(50)	OUTPUT ,
    @idCallerSite			INT				= 0,
    @callerLangString		NVARCHAR(10),
    @idCaller				INT				= 0,
    
    @idxAPIoAuthConsumer	INT OUTPUT,
    @label					NVARCHAR(255),
    @oAuthKey				NVARCHAR(255),
    @oAuthSecret			NVARCHAR(255),
    @isActive				BIT
)
AS 
    BEGIN
        SET NOCOUNT ON

    DECLARE @idPermission INT -- define the permission required to perform this function
    SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	
	*/
	
        DECLARE @idCallerLanguage INT
        SELECT  @idCallerLanguage = idLanguage
        FROM    tblLanguage
        WHERE   code = @callerLangString
	
        IF ( SELECT COUNT(1)
             FROM   tblSite S
             WHERE  idSite = @idCallerSite
                    AND idLanguage = @idCallerLanguage
                    AND @idCallerLanguage IS NOT NULL
           ) <> 1 
            BEGIN 
                SELECT  @Return_Code = 5
                SET @Error_Description_Code = 'xAPIoAuthConsumerSave_NotDefault'
                RETURN 1 
            END
			 


	 IF (
		SELECT COUNT(1)
		FROM tblxAPIoAuthConsumer XAPI
		WHERE XAPI.label = @label
		AND XAPI.idSite = @idCallerSite
		AND XAPI.idxAPIoAuthConsumer != @idxAPIoAuthConsumer
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'xAPIoAuthConsumerSave_NotUnique'
		RETURN 1 
		END
	




        IF ( @idxAPIoAuthConsumer = 0
             OR @idxAPIoAuthConsumer IS NULL
           ) 
            BEGIN
		
		-- insert the new object
		
			INSERT  INTO tblxAPIoAuthConsumer(
					  [idSite],
					  [label],
					  [oAuthKey],
					  [oAuthSecret],
					  [isActive]
			)
			VALUES  (
					  @idCallerSite,
					  @label,
					  @oAuthKey,
					  @oAuthSecret,
					  @isActive
			)
			
			-- get the new object id and return successfully
			
			SELECT  @idxAPIoAuthConsumer = SCOPE_IDENTITY()
	                
			SELECT  @Return_Code = 0
        END
		
        ELSE 
            BEGIN
			
		-- update existing object's properties
		
                UPDATE  tblxAPIoAuthConsumer
                SET     [idSite] = @idCallerSite,
                        [label] = @label,
                        [oAuthKey] = @oAuthKey,
                        [oAuthSecret] = @oAuthSecret,
                        [isActive] = @isActive                
                WHERE   idxAPIoAuthConsumer = @idxAPIoAuthConsumer
		
		-- get the id and return successfully
		
                SELECT  @idxAPIoAuthConsumer 
               
				SELECT  @Return_Code = 0
            END
    END
		

