 -- =====================================================================
-- PROCEDURE: [LearningPathEnrollment.SaveForMultipleLearningPaths]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPathEnrollment.SaveForMultipleLearningPaths]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPathEnrollment.SaveForMultipleLearningPaths]
GO

/*

Adds new enrollments for multiple learning paths.

*/

CREATE PROCEDURE [LearningPathEnrollment.SaveForMultipleLearningPaths]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@LearningPaths						IDTable			READONLY,
	@idUser								INT,
	@idTimezone							INT,
	@dtStart							DATETIME,
	@dueInterval						INT,
	@dueTimeframe						NVARCHAR(4),
	@expiresFromStartInterval			INT,
	@expiresFromStartTimeframe			NVARCHAR(4),
	@expiresFromFirstLaunchInterval		INT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4)
)
AS

	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF (SELECT COUNT(1) FROM @LearningPaths) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'LPEnrollmentSaveForMultipleLP_NoRecordFound'
		RETURN 1
		END

	/*
	
	validate that all learning paths exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @LearningPaths LPLP
		LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPLP.id 
		WHERE LP.idSite IS NULL
		OR LP.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LPEnrollmentSaveForMultipleLP_NoRecordFound'
		RETURN 1 
		END
	
	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	save the data

	*/

	-- insert the new learning path enrollments

	INSERT INTO tblLearningPathEnrollment (
		idSite,
		idLearningPath,
		idUser,
		idTimezone,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT
		@idCallerSite,
		LPLP.id,
		@idUser,
		@idTimezone,
		LP.name,
		@dtStart,
	    CASE WHEN @dueInterval > 0 THEN
			dbo.IDateAdd(@dueTimeframe, @dueInterval, @dtStart)
		ELSE
			NULL
		END,
		CASE WHEN @expiresFromStartInterval > 0 THEN
				dbo.IDateAdd(@expiresFromStartTimeframe, @expiresFromStartInterval, @dtStart)
			ELSE
				NULL
			END,
		@utcNow,
		@dueInterval,
		@dueTimeframe,
		@expiresFromStartInterval,
		@expiresFromStartTimeframe,
		@expiresFromFirstLaunchInterval,
		@expiresFromFirstLaunchTimeframe
	FROM @LearningPaths LPLP
	LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPLP.id
	WHERE LP.idSite =  @idCallerSite

	/*

	For new learning path enrollment inserts, also insert course enrollments for the learning path courses where
	the user does not already have one that is active or has been completed in the last year.

	*/

	CREATE TABLE #NewlyInsertedLearningPathEnrollments (
		idLearningPathEnrollment	    INT,
		idSite						    INT,
		idUser						    INT,
		idLearningPath				    INT,
		idTimezone					    INT,
		dtCreated					    DATETIME,
		dtStart						    DATETIME,
		dtDue						    DATETIME,
		dtExpiresFromStart				DATETIME,
		dueInterval					    INT,
		dueTimeframe				    NVARCHAR(4),
		expiresFromStartInterval		INT,
		expiresFromStartTimeframe		NVARCHAR(4),
		expiresFromFirstLaunchInterval	INT,
		expiresFromFirstLaunchTimeframe	NVARCHAR(4)
	)

	INSERT INTO #NewlyInsertedLearningPathEnrollments (
		idLearningPathEnrollment,
		idSite,
		idUser,
		idLearningPath,
		idTimezone,
		dtCreated,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT
		LPE.idLearningPathEnrollment,
		LPE.idSite,
		LPE.idUser,
		LPE.idLearningPath,
		LPE.idTimezone,
		LPE.dtCreated,
		LPE.dtStart,
		LPE.dtDue,
		LPE.dtExpiresFromStart,
		LPE.dueInterval,
		LPE.dueTimeframe,
		LPE.expiresFromStartInterval,
		LPE.expiresFromStartTimeframe,
		LPE.expiresFromFirstLaunchInterval,
		LPE.expiresFromFirstLaunchTimeframe
	FROM @LearningPaths LPLP
	LEFT JOIN tblLearningPathEnrollment LPE ON LPE.idLearningPath = LPLP.id AND LPE.idUser = @idUser
	WHERE LPE.dtCreated = @utcNow


	CREATE TABLE #EnrollmentsToInsert
	(
		idEnrollmentToInsert				INT		IDENTITY(1,1),
		idSite								INT,
		idCourse							INT,
		revcode								NVARCHAR(32),
		idUser								INT,
		idLearningPathEnrollment			INT,
		isLockedByPrerequisites				BIT,
		idTimezone							INT,
		dtCreated							DATETIME,				
		dtStart								DATETIME,
		dtDue								DATETIME,
		dueInterval							INT,
		dueTimeframe						NVARCHAR(4),
		dtExpiresFromStart					DATETIME,
		expiresFromStartInterval			INT,
		expiresFromStartTimeframe			NVARCHAR(4),
		expiresFromFirstLaunchInterval		INT,
		expiresFromFirstLaunchTimeframe		NVARCHAR(4),
		title								NVARCHAR(255),
		code								NVARCHAR(255),
		credits								FLOAT
	)

	INSERT INTO #EnrollmentsToInsert (
		idSite,
		idCourse,
		revcode,
		idUser,
		idLearningPathEnrollment,
		isLockedByPrerequisites,
		idTimezone,
		dtCreated,
		dtStart,
		dtDue,
		dueInterval,
		dueTimeframe,
		dtExpiresFromStart,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe,
		title,
		code,
		credits
	)
	SELECT
		NILPE.idSite,
		LPCL.idCourse,
		C.revcode,
		NILPE.idUser,
		NILPE.idLearningPathEnrollment,
		1,
		NILPE.idTimezone,
		NILPE.dtCreated,
		NILPE.dtStart,
		NILPE.dtDue,
		NILPE.dueInterval,
		NILPE.dueTimeframe,
		NILPE.dtExpiresFromStart,
		NILPE.expiresFromStartInterval,
		NILPE.expiresFromStartTimeframe,
		NILPE.expiresFromFirstLaunchInterval,
		NILPE.expiresFromFirstLaunchTimeframe,
		C.title,
		C.coursecode,
		C.credits
	FROM #NewlyInsertedLearningPathEnrollments NILPE
	LEFT JOIN tblLearningPathToCourseLink LPCL ON LPCL.idLearningPath = NILPE.idLearningPath
	LEFT JOIN tblCourse C ON C.idCourse = LPCL.idCourse
	WHERE NOT EXISTS (SELECT 1 FROM tblEnrollment E
					  WHERE E.idCourse = LPCL.idCourse
					  AND E.idUser = NILPE.idUser
					  AND 
					  (
							-- incompete/active
							(
							E.dtCompleted IS NULL
							AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
							AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
							)
						OR
							-- completed and within a year prior to the learning path enrollment's start
							(
							E.dtCompleted IS NOT NULL
							AND E.dtCompleted >= DATEADD(yyyy, -1, NILPE.dtStart)
							)
					 )
					 )

	/*
	
	remove duplicated enrolled courses and keep the one with the minimal idLearningPathEnrollment
	
	*/
		
	DELETE FROM #EnrollmentsToInsert
	WHERE idEnrollmentToInsert NOT IN (
		SELECT * 
		FROM (
			SELECT MIN(idEnrollmentToInsert) idMin
			FROM #EnrollmentsToInsert
			GROUP BY idCourse
		) temp
	)


	INSERT INTO tblEnrollment (
		idSite,
		idCourse,
		revcode,
		idUser,
		idLearningPathEnrollment,
		isLockedByPrerequisites,
		idTimezone,
		dtCreated,
		dtStart,
		dtDue,
		dueInterval,
		dueTimeframe,
		dtExpiresFromStart,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe,
		title,
		code,
		credits
	)
	SELECT
		idSite,
		idCourse,
		revcode,
		idUser,
		idLearningPathEnrollment,
		isLockedByPrerequisites,
		idTimezone,
		dtCreated,
		dtStart,
		dtDue,
		dueInterval,
		dueTimeframe,
		dtExpiresFromStart,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe,
		title,
		code,
		credits

	FROM #EnrollmentsToInsert

	DROP Table #EnrollmentsToInsert

	/*

	do the event log entries for the learning path enrollments

	*/

	DECLARE @learningPathEnrollmentEventLogItems EventLogItemObjects

	INSERT INTO @learningPathEnrollmentEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		NILPE.idSite,
		NILPE.idLearningPath,
		NILPE.idLearningPathEnrollment, 
		NILPE.idUser
	FROM #NewlyInsertedLearningPathEnrollments NILPE

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 501, @utcNow, @learningPathEnrollmentEventLogItems

	/*

	do the event log entries for course enrollments

	*/

	DECLARE @enrollmentEventLogItems EventLogItemObjects

	INSERT INTO @enrollmentEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		NILPE.idSite,
		E.idCourse,
		E.idEnrollment, 
		NILPE.idUser
	FROM #NewlyInsertedLearningPathEnrollments NILPE
	LEFT JOIN tblEnrollment E ON E.idLearningPathEnrollment = NILPE.idLearningPathEnrollment AND E.idUser = NILPE.idUser
	WHERE E.dtCreated = @utcNow

	DROP Table #NewlyInsertedLearningPathEnrollments

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 201, @utcNow, @enrollmentEventLogItems

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		