-- =====================================================================
-- PROCEDURE: [LearningPathEnrollment.GetGridForLearnersStatus]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[LearningPathEnrollment.GetGridForLearnersStatus]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPathEnrollment.GetGridForLearnersStatus]
GO

/*

Gets a listing of a user's course enrollments for a given learning path enrollment.

*/

CREATE PROCEDURE [LearningPathEnrollment.GetGridForLearnersStatus]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@searchParam				NVARCHAR(4000),
	@pageNum					INT, 
	@pageSize					INT, 
	@orderColumn				NVARCHAR(255),
	@orderAsc					BIT,
	
	@idLearningPathEnrollment	INT
)
AS
	BEGIN
		SET NOCOUNT ON

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/* 
				
		create and populate a temp table showing the high level status of courses within the learning path enrollment

		*/

		CREATE TABLE #LearningPathCourses (
			idLearningPathEnrollment	INT,
			idCourse					INT,
			courseTitle					NVARCHAR(255),
			isCompleted					INT,
			[order]						INT
		)

		INSERT INTO #LearningPathCourses (
			idLearningPathEnrollment,
			idCourse,
			courseTitle,
			isCompleted,
			[order]
		)
		SELECT DISTINCT
			@idLearningPathEnrollment,
			C.idCourse,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END,
			CASE WHEN (
					   SELECT COUNT(1) FROM tblEnrollment 
					   WHERE idUser = LPE.idUser 
					   AND idCourse = LPCL.idCourse 
					   AND dtCompleted IS NOT NULL
					   AND dtCompleted >= DATEADD(yyyy, -1, LPE.dtStart)
					  ) > 0 THEN
				1
			ELSE
				0
			END,
			LPCL.[order]
		FROM tblLearningPathEnrollment LPE
		LEFT JOIN tblLearningPathToCourseLink LPCL ON LPCL.idLearningPath = LPE.idLearningPath
		LEFT JOIN tblCourse C ON C.idCourse = LPCL.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		WHERE LPE.idLearningPathEnrollment = @idLearningPathEnrollment
		AND C.idSite = @idCallerSite

		/*
		
		create and populate a temp table with all of the course enrollments of courses belonging to the learning path

		*/

		DECLARE @idUser INT
		SELECT @idUser = idUser FROM tblLearningPathEnrollment WHERE idLearningPathEnrollment = @idLearningPathEnrollment

		CREATE TABLE #CourseEnrollmentsForLearningPath (
			idEnrollment							INT,
			idLearningPathEnrollment				INT,
			idCourse								INT,
			dtStart									DATETIME,
			dtCompleted								DATETIME,
			isExpired								BIT,
			isLockedByPrerequisites					BIT,
			isLockedByLearningPathOrderEnforcement	BIT
		)

		INSERT INTO #CourseEnrollmentsForLearningPath (
			idEnrollment,
			idLearningPathEnrollment,
			idCourse,
			dtStart,
			dtCompleted,
			isExpired,
			isLockedByPrerequisites,
			isLockedByLearningPathOrderEnforcement
		)
		SELECT
			E.idEnrollment,
			E.idLearningPathEnrollment,
			E.idCourse,
			E.dtStart,
			E.dtCompleted,
			CASE WHEN (
						(E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromStart <= GETUTCDATE()) 
						OR 
						(E.dtExpiresFromFirstLaunch IS NOT NULL AND E.dtExpiresFromFirstLaunch <= GETUTCDATE())
					  ) THEN
				1
			ELSE 
				0 
			END,
			CASE WHEN E.isLockedByPrerequisites = 1 THEN
				CONVERT(BIT, [dbo].[EvaluateEnrollmentPrerequisiteLock](E.idEnrollment))
			ELSE
				CONVERT(BIT, 0)
			END,
			CASE WHEN E.idLearningPathEnrollment IS NOT NULL THEN
				CONVERT(BIT, [dbo].[EvaluateLearningPathEnrollmentOrderingLock](E.idEnrollment))
			ELSE
				CONVERT(BIT, 0)
			END
		FROM tblEnrollment E
		WHERE E.idUser = @idUser
		AND E.idCourse IN (SELECT DISTINCT idCourse FROM #LearningPathCourses)
		AND E.idActivityImport IS NULL

		/*

		create and populate a temp table to hold the final output

		*/

		CREATE TABLE #LearningPathCoursesFinal (
			idLearningPathEnrollment	INT,
			idCourse					INT,
			courseTitle					NVARCHAR(255),
			isCompleted					BIT,
			idEnrollmentLaunchable		INT,
			idEnrollmentLocked			INT,
			[order]						INT
		)

		INSERT INTO #LearningPathCoursesFinal (
			idLearningPathEnrollment,
			idCourse,
			courseTitle,
			isCompleted,
			idEnrollmentLaunchable,
			idEnrollmentLocked,
			[order]	
		)
		SELECT 
			LPC.idLearningPathEnrollment,
			LPC.idCourse,
			LPC.courseTitle,
			LPC.isCompleted,
			CASE WHEN LPC.isCompleted = 1 THEN -- completed, try to grab the most recently completed enrollment we can launch for review
				CASE WHEN (	-- directly associated with the learning path enrollment
							SELECT COUNT(1) 
							FROM #CourseEnrollmentsForLearningPath CELP 
							WHERE CELP.idCourse = LPC.idCourse 
							AND CELP.dtCompleted IS NOT NULL 
							AND CELP.isExpired = 0 
							AND CELP.idLearningPathEnrollment = LPC.idLearningPathEnrollment
						  ) >= 1 THEN			
					(
					 SELECT TOP 1 idEnrollment 
					 FROM #CourseEnrollmentsForLearningPath CELP 
					 WHERE CELP.idCourse = LPC.idCourse 
					 AND CELP.dtCompleted IS NOT NULL 
					 AND CELP.isExpired = 0  
					 AND CELP.idLearningPathEnrollment = LPC.idLearningPathEnrollment 
					 ORDER BY CELP.dtStart DESC
					)
				ELSE
					-- get the most recent enrollment id for the course that is loosely associated with the learning path enrollment
					(
					 SELECT TOP 1 idEnrollment 
					 FROM #CourseEnrollmentsForLearningPath CELP 
					 WHERE CELP.idCourse = LPC.idCourse 
					 AND CELP.dtCompleted IS NOT NULL 
					 AND CELP.isExpired = 0  
					 AND CELP.idLearningPathEnrollment IS NULL 
					 ORDER BY CELP.dtStart DESC
					)
				END
			ELSE -- not completed, try to grab the oldest enrollment we can launch
				CASE WHEN ( -- directly associated with the learning path enrollment
						   SELECT COUNT(1) FROM 
						   #CourseEnrollmentsForLearningPath CELP 
						   WHERE CELP.idCourse = LPC.idCourse
						   AND CELP.dtCompleted IS NULL
						   AND CELP.isExpired = 0
						   AND CELP.isLockedByPrerequisites = 0
						   AND CELP.isLockedByLearningPathOrderEnforcement = 0
						   AND CELP.idLearningPathEnrollment = LPC.idLearningPathEnrollment
						  ) >= 1 THEN
					(
					 SELECT TOP 1 idEnrollment
					 FROM #CourseEnrollmentsForLearningPath CELP
					 WHERE CELP.idCourse = LPC.idCourse
					 AND CELP.dtCompleted IS NULL
					 AND CELP.isExpired = 0
					 AND CELP.isLockedByPrerequisites = 0
					 AND CELP.isLockedByLearningPathOrderEnforcement = 0
					 AND CELP.idLearningPathEnrollment = LPC.idLearningPathEnrollment
					 ORDER BY CELP.dtStart ASC
					)
				ELSE
					-- get the oldest enrollment id for the course that is loosely associated with the learning path enrollment
					(
					 SELECT TOP 1 idEnrollment
					 FROM #CourseEnrollmentsForLearningPath CELP
					 WHERE CELP.idCourse = LPC.idCourse
					 AND CELP.dtCompleted IS NULL
					 AND CELP.isExpired = 0
					 AND CELP.isLockedByPrerequisites = 0
					 AND CELP.isLockedByLearningPathOrderEnforcement = 0
					 AND CELP.idLearningPathEnrollment IS NULL
					 ORDER BY CELP.dtStart ASC
					)
				END
			END AS idEnrollmentLaunchable,
			CASE WHEN LPC.isCompleted = 1 THEN -- completed, there is no locked enrollment
				NULL
			ELSE 
			-- not completed, try to grab the oldest, non-launchable (locked) enrollment 
			-- note, we get this one to show that the enrollment associated to this learning path course is locked, the launchable enrollment column will be null if this is populated
				CASE WHEN ( -- directly associated with the learning path enrollment
						   SELECT TOP 1 idEnrollment
						   FROM #CourseEnrollmentsForLearningPath CELP
						   WHERE CELP.idCourse = LPC.idCourse
						   AND CELP.dtCompleted IS NULL
						   AND CELP.isExpired = 0
						   AND (
								CELP.isLockedByPrerequisites = 1
								OR
								CELP.isLockedByLearningPathOrderEnforcement = 1
							   )
						   AND CELP.idLearningPathEnrollment = LPC.idLearningPathEnrollment
						  ) >= 1 THEN
					(
					 SELECT TOP 1 idEnrollment
					 FROM #CourseEnrollmentsForLearningPath CELP
					 WHERE CELP.idCourse = LPC.idCourse
					 AND CELP.dtCompleted IS NULL
					 AND CELP.isExpired = 0
					 AND CELP.isLockedByPrerequisites = 0
					 AND (
						  CELP.isLockedByPrerequisites = 1
						  OR
						  CELP.isLockedByLearningPathOrderEnforcement = 1
						 )
					 AND CELP.idLearningPathEnrollment = LPC.idLearningPathEnrollment
					 ORDER BY CELP.dtStart ASC
					)
				ELSE
					(
					 SELECT TOP 1 idEnrollment
					 FROM #CourseEnrollmentsForLearningPath CELP
					 WHERE CELP.idCourse = LPC.idCourse
					 AND CELP.dtCompleted IS NULL
					 AND CELP.isExpired = 0
					 AND CELP.isLockedByPrerequisites = 0
					 AND (
						  CELP.isLockedByPrerequisites = 1
						  OR
						  CELP.isLockedByLearningPathOrderEnforcement = 1
						 )
					 AND CELP.idLearningPathEnrollment IS NULL
					 ORDER BY CELP.dtStart ASC
					)
				END
			END AS idEnrollmentLocked,
			LPC.[order]
		FROM #LearningPathCourses LPC


		/* NO SEARCH */
		SET @searchParam = NULL
		
		/*

		begin getting the grid data

		*/
			
		-- return the rowcount
			
		SELECT COUNT(1) AS row_count 
		FROM #LearningPathCoursesFinal LPE
		
		/*	
		;WITH 
			Keys AS (
				SELECT TOP (@pageNum * @pageSize) 
					LPE.idLearningPathEnrollment,
					ROW_NUMBER() OVER (ORDER BY 
						-- FIRST ORDER DESC
						CASE WHEN @orderAsc = 0 THEN [order] END DESC,						

						-- FIRST ORDER ASC
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN [order] END
					)
					AS [row_number]
				FROM #LearningPathCourses LPE
			), 
				
			SelectedKeys AS (
				SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
					idLearningPathEnrollment,
					[row_number]
				FROM Keys
				WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
			)
		*/
		
		SELECT
			LPE.idLearningPathEnrollment,	
			LPE.idCourse,
			LPE.courseTitle,
			LPE.isCompleted,
			LPE.idEnrollmentLaunchable,
			LPE.idEnrollmentLocked,
			LPE.[order]
			--SelectedKeys.[row_number]
		--FROM SelectedKeys
		--JOIN #LearningPathCourses LPE ON LPE.idLearningPathEnrollment = SelectedKeys.idLearningPathEnrollment
		FROM #LearningPathCoursesFinal LPE
		ORDER BY LPE.[order]

		-- DROP THE TEMPORARY TABLE
		DROP TABLE #CourseEnrollmentsForLearningPath
		DROP TABLE #LearningPathCourses
		DROP TABLE #LearningPathCoursesFinal
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO