-- =====================================================================
-- PROCEDURE: [Role.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Role.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Role.Delete]
GO

CREATE PROCEDURE [Role.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@Roles					IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Roles) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'RoleDelete_NoRecordsSelected'
		RETURN 1
		END

	/*
	
	validate that all roles exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Roles RR
		LEFT JOIN tblRole R ON R.idRole = RR.id
		WHERE R.idSite IS NULL
		OR R.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'RoleDelete_DetailsNotFound'
		RETURN 1 
		END
	
	/*
	
	GET ruleset ids to remove rules, rulesets, and ruleset links
	
	*/

	DECLARE @RuleSets IDTable

	INSERT INTO @RuleSets (
		id
	)
	SELECT idRuleSet
	FROM tblRuleSetToRoleLink
	WHERE EXISTS (
		SELECT 1
		FROM @Roles RR
		WHERE tblRuleSetToRoleLink.idRole = RR.id
	)

	/*

	DELETE ruleset to ruleset role links.
	
	*/
	
	DELETE FROM tblRuleSetToRoleLink
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the rules 

	*/

	DELETE FROM tblRule 
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the ruleset language records

	*/

	DELETE FROM tblRuleSetLanguage
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the rulesets

	*/

	DELETE FROM tblRuleSet
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)
	
	/*

	DELETE open ruleset engine queue items

	*/

	DELETE FROM tblRuleSetEngineQueue
	WHERE triggerObjectId IN (SELECT id FROM @Roles) 
	AND triggerObject = 'role'
	AND isProcessing IS NULL
	AND dtProcessed IS NULL
	
	/*
	
	Delete the Permission to Role Links
	
	*/
	
	DELETE FROM tblRoleToPermissionLink
	WHERE EXISTS (
		SELECT 1
		FROM @Roles RR
		WHERE tblRoleToPermissionLink.idRole = RR.id
	)

	/*
	
	DELETE the Group to Role Links
	
	*/
	
	DELETE FROM tblGroupToRoleLink
	WHERE EXISTS (
		SELECT 1
		FROM @Roles RR
		WHERE tblGroupToRoleLink.idRole = RR.id
	)
	
	/*
	
	DELETE the User to Role Links
	
	*/
	
	DELETE FROM tblUserToRoleLink
	WHERE EXISTS (
		SELECT 1
		FROM @Roles RR
		WHERE tblUserToRoleLink.idRole = RR.id
	)
	
	/*
	
	DELETE the Roles
	
	*/
	
	DELETE FROM tblRoleLanguage
	WHERE EXISTS (
		SELECT 1
		FROM @Roles RR
		WHERE tblRoleLanguage.idRole = RR.id
	)
	
	DELETE FROM tblRole
	WHERE EXISTS (
		SELECT 1
		FROM @Roles RR
		WHERE tblRole.idRole = RR.id
	)
	
	SELECT @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	
