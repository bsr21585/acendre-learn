 -- =====================================================================
-- PROCEDURE: [LearningPathEnrollment.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPathEnrollment.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPathEnrollment.Save]
GO

/*

Adds new learning path enrollment or updates existing learning path enrollment.

*/

CREATE PROCEDURE [LearningPathEnrollment.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idLearningPathEnrollment			INT            OUTPUT,
	@idLearningPath						INT,
	@idRuleSetLearningPathEnrollment	INT,
	@idUser								INT,
	@idTimezone							INT,
	@dtStart							DATETIME,
	@dueInterval						INT,
	@dueTimeFrame						NVARCHAR(4),
	@expiresFromStartInterval			INT,
	@expiresFromStartTimeframe			NVARCHAR(4),
	@expiresFromFirstLaunchInterval		INT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4),
	@cascadeCourseEnrollments			BIT				= 0 -- default behavior is to not cascade course enrollments
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()
		
	/*

	save the data

	*/

	IF (@idLearningPathEnrollment = 0 OR @idLearningPathEnrollment IS NULL)

		BEGIN
		
		-- insert the new learning path enrollment

		INSERT INTO tblLearningPathEnrollment (
			idSite,	
			idLearningPath,
			idUser,
			idTimezone,
			title,
			dtStart,
			dtDue,
			dtExpiresFromStart,
			dtCreated,
			dueInterval,
			dueTimeframe,
			expiresFromStartInterval,
			expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe
		)
		SELECT
			@idCallerSite,	
			@idLearningPath,
			@idUser,
			@idTimezone,
			LP.name,
			@dtStart,
			CASE WHEN @dueInterval > 0 THEN
				dbo.IDateAdd(@dueTimeframe, @dueInterval, @dtStart)
			ELSE
				NULL
			END,
			CASE WHEN @expiresFromStartInterval > 0 THEN
				dbo.IDateAdd(@expiresFromStartTimeframe, @expiresFromStartInterval, @dtStart)
			ELSE
				NULL
			END,
			@utcNow,
			@dueInterval,
			@dueTimeframe,
			@expiresFromStartInterval,
			@expiresFromStartTimeframe,
			@expiresFromFirstLaunchInterval,
			@expiresFromFirstLaunchTimeframe
		FROM tblLearningPath LP
		WHERE LP.idLearningPath = @idLearningPath
		
		-- get the new learning path enrollment's id
		
		SET @idLearningPathEnrollment = SCOPE_IDENTITY()

		/* do the event log entry for the learning path enrollment*/

		DECLARE @eventLogItem EventLogItemObjects

		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idLearningPath, @idLearningPathEnrollment, @idUser

		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 501, @utcNow, @eventLogItem

		/*

		cascade course enrollments if the flag is set, if its not, cascade happens in procedural code

		*/

		IF (@cascadeCourseEnrollments = 1)

			BEGIN

			INSERT INTO tblEnrollment (
				idSite,
				idCourse,
				revcode,
				idUser,
				idLearningPathEnrollment,
				isLockedByPrerequisites,
				idTimezone,
				dtCreated,
				dtStart,
				dtDue,
				dueInterval,
				dueTimeframe,
				dtExpiresFromStart,
				expiresFromStartInterval,
				expiresFromStartTimeframe,
				expiresFromFirstLaunchInterval,
				expiresFromFirstLaunchTimeframe,
				title,
				code,
				credits
			)
			SELECT
				LPE.idSite,
				LPCL.idCourse,
				C.revcode,
				LPE.idUser,
				LPE.idLearningPathEnrollment,
				1,
				LPE.idTimezone,
				LPE.dtCreated,
				LPE.dtStart,
				LPE.dtDue,
				LPE.dueInterval,
				LPE.dueTimeFrame,
				LPE.dtExpiresFromStart,
				LPE.expiresFromStartInterval,
				LPE.expiresFromStartTimeframe,
				LPE.expiresFromFirstLaunchInterval,
				LPE.expiresFromFirstLaunchTimeframe,
				C.title,
				C.coursecode,
				C.credits
			FROM tblLearningPathEnrollment LPE
			LEFT JOIN tblLearningPathToCourseLink LPCL ON LPCL.idLearningPath = LPE.idLearningPath
			LEFT JOIN tblCourse C ON C.idCourse = LPCL.idCourse
			WHERE LPE.idLearningPathEnrollment = @idLearningPathEnrollment
			AND NOT EXISTS (SELECT 1 FROM tblEnrollment E
							WHERE E.idCourse = LPCL.idCourse
							AND E.idUser = LPE.idUser
							AND 
							(
								-- incompete/active
								(
								E.dtCompleted IS NULL
								AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
								AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
								)
							OR
								-- completed and within a year prior to the learning path enrollment's start
								(
								E.dtCompleted IS NOT NULL
								AND E.dtCompleted >= DATEADD(yyyy, -1, LPE.dtStart)
								)
							)
						)
			AND C.idCourse IS NOT NULL

			/*

			do the event log entries for course enrollments

			*/

			DECLARE @enrollmentEventLogItems EventLogItemObjects

			INSERT INTO @enrollmentEventLogItems (
				idSite,
				idObject, 
				idObjectRelated, 
				idObjectUser
			) 
			SELECT 
				LPE.idSite,
				E.idCourse,
				E.idEnrollment, 
				LPE.idUser
			FROM tblLearningPathEnrollment LPE
			LEFT JOIN tblEnrollment E ON E.idLearningPathEnrollment = LPE.idLearningPathEnrollment AND E.idUser = LPE.idUser
			WHERE LPE.idLearningPathEnrollment = @idLearningPathEnrollment
			AND E.idCourse IS NOT NULL
			AND E.idEnrollment IS NOT NULL

			EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 201, @utcNow, @enrollmentEventLogItems

			END
		
		END
		
	ELSE
	
		BEGIN
		
		-- check that the learning path enrollment id exists
		IF (SELECT COUNT(1) FROM tblLearningPathEnrollment WHERE idLearningPathEnrollment = @idLearningPathEnrollment AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idLearningPathEnrollment = @idLearningPathEnrollment
			SET @Return_Code = 1
			SET @Error_Description_Code = 'LearningPathEnrollmentSave_NoRecordFound'
			RETURN 1
			
			END
			
		-- update existing learning path enrollment's properties

		UPDATE tblLearningPathEnrollment SET
			idRuleSetLearningPathEnrollment		= @idRuleSetLearningPathEnrollment, -- can be updated to detach a one-off change from its inheritance
			idTimezone							= @idTimezone,
			dtStart								= @dtStart,
			dtDue								= CASE WHEN @dueInterval > 0 THEN
													dbo.IDateAdd(@dueTimeframe, @dueInterval, @dtStart)
												ELSE
													NULL
												END,
			dtExpiresFromStart					= CASE WHEN @expiresFromStartInterval > 0 THEN
													dbo.IDateAdd(@expiresFromStartTimeframe, @expiresFromStartInterval, @dtStart)
												ELSE
													NULL
												END,
			dueInterval							= @dueInterval,
			dueTimeframe						= @dueTimeframe,
			expiresFromStartInterval			= @expiresFromStartInterval,
			expiresFromStartTimeframe			= @expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval		= @expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe		= @expiresFromFirstLaunchTimeframe
		WHERE idLearningPathEnrollment	= @idLearningPathEnrollment

	    -- update existing course enrollments which inherit from the learning path enrollment

		UPDATE tblEnrollment SET
			idTimezone							= @idTimezone,
			dtStart								= @dtStart,
			dtDue								= CASE WHEN @dueInterval > 0 THEN
													dbo.IDateAdd(@dueTimeframe, @dueInterval, @dtStart)
												ELSE
													NULL
												END,
			dtExpiresFromStart					= CASE WHEN @expiresFromStartInterval > 0 THEN
													dbo.IDateAdd(@expiresFromStartTimeframe, @expiresFromStartInterval, @dtStart)
												ELSE
													NULL
												END,
			dueInterval							= @dueInterval,
			dueTimeframe						= @dueTimeframe,
			expiresFromStartInterval			= @expiresFromStartInterval,
			expiresFromStartTimeframe			= @expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval		= @expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe		= @expiresFromFirstLaunchTimeframe
		WHERE idLearningPathEnrollment	= @idLearningPathEnrollment
		
		-- get the learning path enrollment's id

		SET @idLearningPathEnrollment = @idLearningPathEnrollment
		
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO	