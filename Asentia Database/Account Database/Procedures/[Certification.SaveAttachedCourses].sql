-- =====================================================================
-- PROCEDURE: [Certification.SaveAttachedCourses]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Certification.SaveAttachedCourses]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) 
	DROP PROCEDURE [Certification.SaveAttachedCourses]
GO

/*

Saves course and credit information for courses attached to this certification.

*/

CREATE PROCEDURE [Certification.SaveAttachedCourses]
(
	@Return_Code					INT					OUTPUT,
	@Error_Description_Code			NVARCHAR(50)		OUTPUT,
	@idCallerSite					INT					= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT					= 0,
	
	@idCertification				INT,
	@CertificationAttachedCourses	CourseCreditTable	READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END	

	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertification
		WHERE idCertification = @idCertification
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationSaveAttachedCourses_NoRecordFound'
		RETURN 1
	    END

	/* 

	delete attached courses that arent in the attached courses param

	*/

	DELETE FROM tblCertificationToCourseCreditLink 
	WHERE idCertification = @idCertification
	AND NOT EXISTS (SELECT 1 FROM @CertificationAttachedCourses CAC
					WHERE CAC.idCourse = tblCertificationToCourseCreditLink.idCourse)

	/*

	update existing attached courses

	*/

	UPDATE tblCertificationToCourseCreditLink SET
		credits = CAC.credits
	FROM @CertificationAttachedCourses CAC
	WHERE tblCertificationToCourseCreditLink.idCertification = @idCertification
	AND CAC.idCourse = tblCertificationToCourseCreditLink.idCourse
	
	/*

	insert new attached courses

	*/

	INSERT INTO tblCertificationToCourseCreditLink (
		idSite,
		idCertification,
		idCourse,
		credits
	)
	SELECT
		@idCallerSite,
		@idCertification,
		CAC.idCourse,
		CAC.credits
	FROM @CertificationAttachedCourses CAC
	WHERE NOT EXISTS (SELECT 1 FROM tblCertificationToCourseCreditLink CCCL
					  WHERE CCCL.idCertification = @idCertification
					  AND CCCL.idCourse = CAC.idCourse)

	/*

	RETURN

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO