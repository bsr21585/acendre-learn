-- =====================================================================
-- PROCEDURE: [GridAnalytic.Rulesets]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[GridAnalytic.Rulesets]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GridAnalytic.Rulesets]
GO

/*

Gets grid analytic data for Users grid page.

*/

CREATE PROCEDURE [GridAnalytic.Rulesets]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
AS
	BEGIN
		SET NOCOUNT ON

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		create a temp table for base user data, and populate it
		we will run the analytics off of that table so as not to tie up the real table

		*/

		CREATE TABLE #RulesetBase (
			idRuleset INT,
			idSite INT,
			[object] NVARCHAR(20)
		)

		INSERT INTO #RulesetBase (
			idRuleset,
			idSite,
			[object]
		)
		SELECT
			RS.idRuleset,
			RS.idSite,
			CASE
			WHEN RSG.idGroup IS NOT NULL THEN 'group'
			WHEN RSC.idCertification IS NOT NULL THEN 'certification'
			WHEN RSE.idRuleSetEnrollment IS NOT NULL THEN 'course'
			WHEN RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL THEN 'learningpath'
			WHEN RSR.idRole IS NOT NULL THEN 'role' END
			FROM tblRuleSet RS
			LEFT JOIN tblRuleSetToGroupLink RSG ON RSG.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToCertificationLink RSC ON RSC.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSEL ON RSEL.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRuleSetLearningPathEnrollmentLink RSLPE ON RSLPE.idRuleSet = RS.idRuleSet
			LEFT JOIN tblRuleSetToRoleLink RSR ON RSR.idRuleSet = RS.idRuleSet
			LEFT JOIN tblGroup G ON G.idGroup = RSG.idGroup
			LEFT JOIN tblRole R ON R.idRole = RSR.idRole
			LEFT JOIN tblRuleSetLearningPathEnrollment LPE ON LPE.idRuleSetLearningPathEnrollment = RSLPE.idRuleSetLearningPathEnrollment
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
			LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
			LEFT JOIN tblCourse C ON C.idCourse = RSE.idCourse
			WHERE (RSG.idGroup IS NOT NULL OR RSC.idCertification IS NOT NULL OR RSE.idRuleSetEnrollment IS NOT NULL OR RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL OR RSR.idRole IS NOT NULL) AND RS.idSite = @idCallerSite

		/*

		run the analytics

		*/

		SELECT
			(SELECT COUNT(1) FROM #RulesetBase) AS total,
			(SELECT COUNT(1) FROM #RulesetBase WHERE [object] = 'group') AS [group],
			(SELECT COUNT(1) FROM #RulesetBase WHERE [object] = 'certification') AS [certification],
			(SELECT COUNT(1) FROM #RulesetBase WHERE [object] = 'course') AS [course],
			(SELECT COUNT(1) FROM #RulesetBase WHERE [object] = 'learningpath') AS [learningpath],
			(SELECT COUNT(1) FROM #RulesetBase WHERE [object] = 'role') AS [role]

		-- DROP THE TEMPORARY TABLES
		DROP TABLE #RulesetBase

		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO