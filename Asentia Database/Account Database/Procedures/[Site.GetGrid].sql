-- =====================================================================
-- PROCEDURE: [Site.GetGrid]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Site.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.GetGrid]
GO

CREATE PROCEDURE [Site.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblSite
			WHERE idSite <> 1
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idSite,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN hostname END DESC,
							CASE WHEN @orderAsc = 0 THEN title END DESC,
							CASE WHEN @orderAsc = 0 THEN userLimit END DESC,
							CASE WHEN @orderAsc = 0 THEN kbLimit END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN hostname END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN title END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN userLimit END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN kbLimit END

						)
						AS [row_number]
					FROM tblSite
					WHERE idSite <> 1
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idSite, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idSite, 
				A.hostname, 
				A.company, 
				A.title, 
				A.dtExpires, 
				A.contactName,
				A.contactEmail,
				(CASE WHEN A.userLimit = 0 OR A.userLimit IS NULL THEN 'Unlimited' ELSE CAST(A.userLimit AS NVARCHAR) END) AS 'userLimit',
				(CASE WHEN A.kbLimit = 0 OR A.kbLimit IS NULL THEN 'Unlimited' ELSE CAST(A.kbLimit AS NVARCHAR)  END) AS 'kbLimit',
				A.isActive,
				SelectedKeys.[row_number],
				CONVERT(BIT, 1) AS isLogonOn,
				CONVERT(BIT, 1) AS isModifyOn -- needs to be calculated
			FROM SelectedKeys
			JOIN tblSite A ON A.idSite = SelectedKeys.idSite
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblSite
			INNER JOIN CONTAINSTABLE(tblSite, *, @searchParam) K ON K.[key] = tblSite.idSite
			AND idSite <> 1
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idSite,
						ROW_NUMBER() OVER (ORDER BY 
									-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN hostname END DESC,
							CASE WHEN @orderAsc = 0 THEN title END DESC,
							CASE WHEN @orderAsc = 0 THEN userLimit END DESC,
							CASE WHEN @orderAsc = 0 THEN kbLimit END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN hostname END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN title END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN userLimit END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN kbLimit END

						)
						AS [row_number]
					FROM tblSite
					INNER JOIN CONTAINSTABLE(tblSite, *, @searchParam) K ON K.[key] = tblSite.idSite
					AND idSite <> 1
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idSiTe, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idSite, 
				A.hostname, 
				A.company, 
				A.title, 
				A.dtExpires, 
				A.contactName,
				A.contactEmail,
				(CASE WHEN A.userLimit = 0 OR A.userLimit IS NULL THEN 'Unlimited' ELSE CAST(A.userLimit AS NVARCHAR)  END) AS 'userLimit',
				(CASE WHEN A.kbLimit = 0 OR A.kbLimit IS NULL THEN 'Unlimited' ELSE CAST(A.kbLimit AS NVARCHAR)  END) AS 'kbLimit',
				A.isActive,
				SelectedKeys.[row_number],
				CONVERT(BIT, 1) AS isLogonOn,
				CONVERT(BIT, 1) AS isModifyOn -- needs to be calculated
			FROM SelectedKeys
			JOIN tblSite A ON A.idSite = SelectedKeys.idSite
			ORDER BY SelectedKeys.[row_number]
			
			END
		
	END
GO


