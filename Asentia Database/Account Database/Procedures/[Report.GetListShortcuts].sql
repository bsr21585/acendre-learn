
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Report.GetListShortcuts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Report.GetListShortcuts]
GO

/*
Returns a list of Report ID with shortcut with Related User ID 
*/
CREATE PROCEDURE [Report.GetListShortcuts]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idUser					INT,
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT		
)
AS

	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/
			
		-- return the rowcount
		SELECT COUNT(1) AS row_count 
		FROM tblReportToReportShortcutsWidgetLink RS
		INNER JOIN tblReport R ON R.idReport = RS.idReport
		WHERE 
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite)
			)
		AND (R.isDeleted IS NULL OR R.isDeleted = 0)
		AND (R.idUser = RS.idUser OR R.isPublic = 1)
		AND RS.idUser = @idUser
		;WITH 
			Keys AS (
				SELECT TOP (@pageNum * @pageSize) 
					RS.idReportToReportShortcutsWidgetLink,
					ROW_NUMBER() OVER (ORDER BY
						-- ORDER DESC
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'reportName' THEN R.title END) END DESC,
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'ownerName' THEN O.lastname END) END DESC,
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'ownerName' THEN O.firstname END) END DESC,
							
						-- ORDER ASC
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'reportName' THEN R.title END) END,
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'ownerName' THEN O.lastname END) END,
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'ownerName' THEN O.firstname END) END
					)
					AS [row_number]
				FROM tblReportToReportShortcutsWidgetLink RS
				INNER JOIN tblReport R ON R.idReport = RS.idReport
				LEFT JOIN tblUser O ON O.idUser = R.idUser
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite)
					)
				AND (R.isDeleted IS NULL OR R.isDeleted = 0)
				AND (R.idUser = RS.idUser OR R.isPublic = 1)
				AND RS.idUser = @idUser
			),
				
			SelectedKeys AS (
				SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
					idReportToReportShortcutsWidgetLink,
					[row_number]
				FROM Keys
				WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
			)
		SELECT
		RS.idReportToReportShortcutsWidgetLink, 
		RS.idReport AS idReport, 
		CASE WHEN RL.title IS NULL OR RL.title = '' THEN
			R.title
		ELSE
			RL.title
		END AS reportName,
		CASE WHEN R.idUser = 1 THEN
			'Administrator'
		ELSE
			CASE WHEN O.idUser is null THEN
				'Unknown' -- should never happen
			ELSE
				O.lastname + ', ' + O.firstname + (CASE WHEN O.middlename IS NOT NULL AND O.middlename <> '' THEN ' ' + O.middlename ELSE '' END)
			END
		END AS ownerName,
		CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
		SelectedKeys.[row_number]
		FROM SelectedKeys
		LEFT JOIN tblReportToReportShortcutsWidgetLink RS ON RS.idReportToReportShortcutsWidgetLink = SelectedKeys.idReportToReportShortcutsWidgetLink
		LEFT JOIN tblReport R ON R.idReport = RS.idReport
		LEFT JOIN tblReportLanguage RL ON RL.idReport = RS.idReport AND RL.idLanguage = @idCallerLanguage
		LEFT JOIN tblUser O ON O.idUser = R.idUser
		WHERE (R.idUser = RS.idUser OR R.isPublic = 1) -- report belongs to the user or is public
		AND RS.idUser = @idUser
		ORDER BY SelectedKeys.[row_number]						
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO