-- =====================================================================
-- PROCEDURE: [TransactionItem.ApplyCoupon]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.ApplyCoupon]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.ApplyCoupon]
GO

/*

Validates the coupon and applies on the transaction item if validated successfully.

*/

CREATE PROCEDURE [TransactionItem.ApplyCoupon]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idTransactionItem		INT,
	@couponCode				NVARCHAR(10),
	@effectiveCost			FLOAT			OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON	

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate that the transaction item exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblTransactionItem
		WHERE idTransactionItem = @idTransactionItem
		AND idSite = @idCallerSite		
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemApplyCoupon_NoTI'
		RETURN 1
		END
	
	/*

	get coupon id along with other parameters from coupon code

	*/

	DECLARE @idCouponCode INT
	DECLARE @usesAllowed INT
	DECLARE @couponDiscount FLOAT
	DECLARE @couponDiscountType INT
	DECLARE @validForCourses INT
	DECLARE @validForCatalogs INT
	DECLARE @validForLearningPaths INT
	DECLARE @validForStandupTraining INT
	DECLARE @dtStart DATETIME
	DECLARE @dtEnd DATETIME
	DECLARE @isSingleUsePerUser BIT

	SET @idCouponCode = NULL
	
	SELECT TOP 1
		@idCouponCode = idCouponCode,
		@usesAllowed = usesAllowed,
		@couponDiscount = discount,
		@couponDiscountType = discountType,
		@validForCourses = CASE WHEN forCatalog IS NULL THEN 1 ELSE forCourse END,
		@validForCatalogs = CASE WHEN forCatalog IS NULL THEN 1 ELSE forCatalog END,
		@validForLearningPaths = CASE WHEN forLearningPath IS NULL THEN 1 ELSE forLearningPath END,
		@validForStandupTraining = CASE WHEN forStandupTraining IS NULL THEN 1 ELSE forStandupTraining END,
		@dtStart = dtStart,
		@dtEnd = dtEnd,
		@isSingleUsePerUser = isSingleUsePerUser
	FROM tblCouponCode 
	WHERE code = @couponCode
	AND idSite = @idCallerSite
	AND (isDeleted = 0 OR isDeleted IS NULL)
	
	/*

	if there is no coupon code id, the coupon code is invalid

	*/

	IF (@idCouponCode IS NULL)
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemApplyCoupon_InvalidCode'
		RETURN 1
		END

	/* 
	
	check if the coupon code is within its valid timeframe

	*/

	IF ((@dtStart IS NOT NULL AND @dtStart > GETUTCDATE()) OR (@dtEnd IS NOT NULL AND @dtEnd <= GETUTCDATE()))
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemApplyCoupon_InvalidTimeframe'
		RETURN 1
		END

	/* 
	
	we've come far enough to get transaction item details

	*/

	DECLARE @idUser INT
	DECLARE @itemId INT
	DECLARE @itemType INT
	DECLARE @cost FLOAT

	SELECT TOP 1
		@idUser = idUser,
		@itemId = itemId,
		@itemType = itemType,
		@cost = cost,
		@effectiveCost = cost
	FROM tblTransactionItem
	WHERE idTransactionItem = @idTransactionItem
	AND idSite = @idCallerSite

	/*

	check if the coupon code is single use per user and if it's already been used

	*/

	IF (@isSingleUsePerUser = 1 AND ((SELECT COUNT(1) FROM tblTransactionItem WHERE idUser = @idUser AND idCouponCode = @idCouponCode) > 0))
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemApplyCoupon_SingleUseOnly'
		RETURN 1
		END

	/*

	validate the number of uses allowed

	*/

	IF (@usesAllowed > -1 AND ((SELECT COUNT(1) FROM tblTransactionItem WHERE idCouponCode = @idCouponCode) >= @usesAllowed))
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemApplyCoupon_MaxUsesMet'
		RETURN 1
		END

	/*

	validate that the coupon code is valid for the item it is being applied to	

	*/
	
	IF @itemType = 1 -- 1 is course
		BEGIN

		IF (@validForCourses = 1) -- 1 is NONE
			OR (
				@validForCourses = 3 -- 3 is SELECTED ITEMS
				AND NOT EXISTS (SELECT 1 FROM tblCouponCodeToCourseLink CCL
								WHERE CCL.idCouponCode = @idCouponCode
								AND CCL.idCourse = @itemId)
			   )
			BEGIN
			SET @Return_Code = 1
			SET @Error_Description_Code = 'TransactionItemApplyCoupon_NotValidForItem'			
			RETURN 1
			END

		END

	IF @itemType = 2 -- 2 is catalog
		BEGIN
		
		IF (@validForCatalogs = 1) -- 1 is NONE
			OR (
				@validForCatalogs = 3 -- 3 is SELECTED ITEMS
				AND NOT EXISTS (SELECT 1 FROM tblCouponCodeToCatalogLink CCL
								WHERE CCL.idCouponCode = @idCouponCode
								AND CCL.idCatalog = @itemId)
			   )
			BEGIN
			SET @Return_Code = 1
			SET @Error_Description_Code = 'TransactionItemApplyCoupon_NotValidForItem'			
			RETURN 1
			END

		END

	IF @itemType = 3 -- 3 is learning path
		BEGIN

		IF (@validForLearningPaths = 1) -- 1 is NONE
			OR (
				@validForLearningPaths = 3 -- 3 is SELECTED ITEMS
				AND NOT EXISTS (SELECT 1 FROM tblCouponCodeToLearningPathLink CCL
								WHERE CCL.idCouponCode = @idCouponCode
								AND CCL.idLearningPath = @itemId)
			   )
			BEGIN
			SET @Return_Code = 1
			SET @Error_Description_Code = 'TransactionItemApplyCoupon_NotValidForItem'			
			RETURN 1
			END

		END

	IF @itemType = 4 -- 4 is ilt
		BEGIN

		IF (@validForStandupTraining = 1) -- 1 is NONE
			OR (
				@validForStandupTraining = 3 -- 3 is SELECTED ITEMS
				AND NOT EXISTS (SELECT 1 FROM tblCouponCodeToStandupTrainingLink CCL
								WHERE CCL.idCouponCode = @idCouponCode
								AND CCL.idStandupTraining= @itemId)
			   )
			BEGIN
			SET @Return_Code = 1
			SET @Error_Description_Code = 'TransactionItemApplyCoupon_NotValidForItem'			
			RETURN 1
			END

		END
	
	/*
	
	figure out the effective price and apply the coupon code
	
	*/
	
	SELECT @effectiveCost =
		CASE 
			WHEN @couponDiscountType = 1 THEN 0										-- FREE
			WHEN @couponDiscountType = 2 THEN @couponDiscount						-- NEW PRICE
			WHEN @couponDiscountType = 3 THEN										-- AMOUNT OFF
				CASE WHEN @couponDiscount > @cost THEN 0
				ELSE @cost - @couponDiscount END
			WHEN @couponDiscountType = 4 AND @couponDiscount BETWEEN 1 AND 99		-- PERCENTAGE OFF
				THEN @cost - (@cost * @couponDiscount/100)	
		ELSE @cost
		END		

	UPDATE tblTransactionItem SET
		couponCode = @couponCode,
		idCouponCode = @idCouponCode,
		paid = @effectiveCost
	WHERE idTransactionItem = @idTransactionItem

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO