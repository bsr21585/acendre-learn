-- =====================================================================
-- PROCEDURE: [LearningPath.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.Delete]
GO

/*

Deletes learning paths

*/

CREATE PROCEDURE [LearningPath.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@LearningPaths			IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @LearningPaths) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'LearningPathDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all learning paths exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @LearningPaths LPLP
		LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPLP.id
		WHERE LP.idSite IS NULL
		OR LP.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LearningPathDelete_NoRecordFound'
		RETURN 1 
		END
		
	/*
	
	Mark the learning path(s) as deleted. DO NOT remove the record. Do NOT delete language or other sub records either.
	SOFT DELETE the learning path(s), just mark the learning path(s) as deleted. DO NOT remove the record!
	
	*/

	UPDATE tblLearningPath SET isDeleted = 1, dtDeleted = GETUTCDATE() WHERE idLearningPath IN (SELECT id FROM @LearningPaths)
	
	/*

	DELETE the course links

	*/

	DELETE FROM tblLearningPathToCourseLink WHERE idLearningPath IN (SELECT id FROM @LearningPaths)

	/*

	EVENTIALLY THERE WILL BE A LEARNING PATH FEED, THERE ARE TABLES BUT THEY ARE NOT IN USE
	WHEN THEY BECOME ACTIVE, THE DELETES FOR THOSE WILL GO HERE

	*/

    /*

	DELETE all the entries in [tblDocumentRepositoryItemLanguage] related to course(s) which we are deleting

	*/

	DELETE FROM tblDocumentRepositoryItemLanguage WHERE idDocumentRepositoryItem IN (SELECT idDocumentRepositoryItem FROM tblDocumentRepositoryItem WHERE idObject IN (SELECT id FROM @LearningPaths) AND idDocumentRepositoryObjectType = 3)  -- 3 is for learning path


	/*

	DELETE all the entries in [tblDocumentRepositoryItem] related to learning path(s) which we are deleting

	*/

	DELETE FROM tblDocumentRepositoryItem WHERE idObject IN (SELECT id FROM @LearningPaths) AND idDocumentRepositoryObjectType = 3  -- 3 is for learning path

	/*

	DELETE all the entries in [tblDocumentRepositoryFolder] related to learning path(s) which we are deleting

	*/

	DELETE FROM tblDocumentRepositoryFolder WHERE idObject IN (SELECT id FROM @LearningPaths) AND idDocumentRepositoryObjectType = 3 -- 3 is for learning path

	/*

	SOFT DELETE all the entries in [tblEventEmailNotification] related to learning path(s) which we are deleting, where they are not already deleted

	*/

	UPDATE tblEventEmailNotification SET isDeleted = 1 WHERE idObject IN (SELECT id FROM @LearningPaths) AND (isDeleted IS NULL OR isDeleted = 0)
	AND idEventType >= 500 AND idEventType < 600 -- 500 - 600 is learning path		

	/*

	SOFT DELETE all the entries in [tblCertificate] related to learning path(s) which we are deleting

	*/

	UPDATE tblCertificate SET isDeleted = 1, dtDeleted = GETUTCDATE() WHERE idObject IN (SELECT id FROM @LearningPaths) AND objectType = 2 -- 2 is for learning path

	/*

	DELETE all the entries in [tblCouponCodeToLearningPathLink] related to learning path(s) which we are deleting

	*/

	DELETE FROM tblCouponCodeToLearningPathLink WHERE idLearningPath IN (SELECT id FROM @LearningPaths)

	/*

	GET the ruleset learning path enrollment ids to remove

	*/

	DECLARE @RuleSetLearningPathEnrollments IDTable

	INSERT INTO @RuleSetLearningPathEnrollments (
		id
	)
	SELECT
		idRuleSetLearningPathEnrollment
	FROM tblRuleSetLearningPathEnrollment
	WHERE idLearningPath IN (SELECT id FROM @LearningPaths)

	/*

	GET ruleset ids to remove rules, rulesets, and ruleset links

	*/

	DECLARE @RuleSets IDTable

	INSERT INTO @RuleSets (
		id
	)
	SELECT 
		idRuleSet
	FROM tblRuleSetToRuleSetLearningPathEnrollmentLink
	WHERE idRuleSetLearningPathEnrollment IN (SELECT id FROM @RuleSetLearningPathEnrollments)

	/*

	DELETE ruleset to ruleset learning path enrollment links.
	
	*/
	
	DELETE FROM tblRuleSetToRuleSetLearningPathEnrollmentLink
	WHERE idRuleSetLearningPathEnrollment IN (SELECT id FROM @RuleSetLearningPathEnrollments)

	/*

	DELETE the rules 

	*/

	DELETE FROM tblRule 
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the ruleset language records

	*/

	DELETE FROM tblRuleSetLanguage
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the rulesets

	*/

	DELETE FROM tblRuleSet
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE open ruleset engine queue items

	*/

	DELETE FROM tblRuleSetEngineQueue
	WHERE triggerObjectId IN (SELECT id FROM @RuleSetLearningPathEnrollments) 
	AND triggerObject = 'rulesetlearningpathenrollment'
	AND isProcessing IS NULL
	AND dtProcessed IS NULL

	/*

	DELETE enrollments for the learning path(s) by using the LearningPathEnrollment.Delete Procedure
	Only delete incomplete ones, leave the completed enrollments for transcript purposes.

	*/

	DECLARE @LearningPathEnrollmentsToDelete IDTable

	INSERT INTO @LearningPathEnrollmentsToDelete (
		id
	)
	SELECT
		LPE.idLearningPathEnrollment
	FROM tblLearningPathEnrollment LPE
	WHERE LPE.idLearningPath IN (SELECT id FROM @LearningPaths)
	AND LPE.dtCompleted IS NULL

	EXEC [LearningPathEnrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @LearningPathEnrollmentsToDelete, 0	

	/*

	SET idRuleSetLearningPathEnrollment to NULL for all COMPLETED enrollments of the learning path(s) which we are deleting

	*/

	UPDATE tblLearningPathEnrollment SET idRuleSetLearningPathEnrollment = NULL WHERE idLearningPath IN (SELECT id FROM @LearningPaths) AND dtCompleted IS NOT NULL

	/*

	DELETE all the entries in [tblRuleSetLearningPathEnrollmentLanguage] related to learning path(s) which we are deleting

	*/

	DELETE FROM tblRuleSetLearningPathEnrollmentLanguage
	WHERE idRuleSetLearningPathEnrollment IN (SELECT idRuleSetLearningPathEnrollment
											  FROM tblRuleSetLearningPathEnrollment
											  WHERE idLearningPath IN (SELECT id FROM @LearningPaths))

	/*

	DELETE all the entries in [tblRuleSetLearningPathEnrollment] related to learning path(s) which we are deleting

	*/

	DELETE FROM tblRuleSetLearningPathEnrollment WHERE idLearningPath IN (SELECT id FROM @LearningPaths)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO