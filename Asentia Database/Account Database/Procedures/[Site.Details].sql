-- =====================================================================
-- PROCEDURE: [Site.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Site.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.Details]
GO

/*

Return all the properties for a given site id.

*/
CREATE PROCEDURE [Site.Details]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0,
	
	@idSite							INT				OUTPUT, 
	@hostname						NVARCHAR(255)	OUTPUT,
	@favicon						NVARCHAR(255)	OUTPUT,
	@password						NVARCHAR(512)	OUTPUT,
	@isActive						BIT				OUTPUT,
	@dtExpires						DATETIME		OUTPUT,
	@title							NVARCHAR(255)	OUTPUT,
	@company						NVARCHAR(255)	OUTPUT,
	@contactName					NVARCHAR(255)	OUTPUT,
	@contactEmail					NVARCHAR(255)	OUTPUT,
	@userLimit						INT				OUTPUT,
	@userCount						INT				OUTPUT,
	@usersCurrentlyLoggedInCount	INT				OUTPUT,
	@kbLimit						INT				OUTPUT,
	@kbCount						INT				OUTPUT,
	@languageString					NVARCHAR(10)	OUTPUT,
	@idLanguage						INT				OUTPUT,
	@idTimezone						INT				OUTPUT,
	@dtLicenseAgreementExecuted		DATETIME		OUTPUT,
	@dtUserAgreementModified		DATETIME		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSite
		WHERE idSite = @idSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idSite
	
	SELECT
		@hostname					= S.hostname,
		@favicon					= S.favicon,
		@isActive					= S.isActive,
		@dtExpires					= S.dtExpires,
		@title						= CASE WHEN SL.title IS NULL or SL.title = '' THEN S.title ELSE SL.title END,
		@company					= CASE WHEN SL.company IS NULL or SL.company = '' THEN S.company ELSE SL.company END,
		@contactName				= S.contactName,
		@contactEmail				= S.contactEmail,
		@userLimit					= S.userLimit,
		@kbLimit					= S.kbLimit,
		@languageString				= L.code,
		@idLanguage					= S.idLanguage,
		@idTimezone					= S.idTimezone,
		@dtLicenseAgreementExecuted	= S.dtLicenseAgreementExecuted,
		@dtUserAgreementModified	= S.dtUserAgreementModified
	FROM tblSite S
	LEFT JOIN tblSiteLanguage SL 
		ON SL.idSite = S.idSite 
		AND SL.idLanguage = @idCallerLanguage
	LEFT JOIN tblLanguage L 
		ON L.idLanguage = S.idLanguage	
	WHERE S.idSite = @idSite
	
	/* 
	
	get the user and kb counts
	
	*/
	
	-- user count

	SELECT @userCount = ISNULL(COUNT(1), 0) FROM tblUser U WHERE U.idSite = @idSite AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))

	-- users currently logged in count

	SELECT @usersCurrentlyLoggedInCount = ISNULL(COUNT(1), 0) FROM tblUser WHERE idSite = @idSite AND dtSessionExpires >= GETUTCDATE()
	
	DECLARE @contentPackageKbCount INT
	DECLARE @repositoryFileKbCount INT
	DECLARE @reportFileKbCount INT

	-- packages kb count

	SELECT @contentPackageKbCount = 
		ISNULL(SUM(CP.kb), 0)
	FROM tblContentPackage CP
	WHERE CP.idSite = @idSite

	-- repository file kb count

	SELECT @repositoryFileKbCount =
		ISNULL(SUM(RI.kb), 0)
	FROM tblDocumentRepositoryItem RI
	WHERE RI.idSite = @idSite
	AND (RI.isDeleted IS NULL OR RI.isDeleted = 0)

	-- report file kb count
	SELECT @reportFileKbCount =
		ISNULL(SUM(RF.htmlKb), 0) + ISNULL(SUM(RF.csvKb), 0) + ISNULL(SUM(RF.pdfKb), 0)
	FROM tblReportFile RF
	WHERE RF.idSite = @idSite
	
	-- total kb count

	SELECT @kbCount = @contentPackageKbCount + @repositoryFileKbCount + @reportFileKbCount
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO