-- =====================================================================
-- PROCEDURE: [Report.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Report.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Report.Save]
GO

/*

Adds new Report or updates existing Report.

*/

CREATE PROCEDURE [Report.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idReport				INT				OUTPUT, 
	@idSite					INT, 
	@idUser					INT,
	@idDataset				INT,
	@title					NVARCHAR(255),
	@fields					NVARCHAR(MAX),
	@filter					NVARCHAR(MAX),
	@order					NVARCHAR(255),
	@isPublic				BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	
	MAY NOT BE NEEDED
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			RETURN 1 
		END

	*/
			 
		
	/*
	
	save the data
	
	*/
	
	IF (
		@idReport = 0 
		OR @idReport IS NULL 
		OR (
			@idCaller <> @idUser 
			AND
			(
				SELECT COUNT(1)
				FROM tblReport
				WHERE idReport = @idReport
				AND idSite = @idCallerSite
				AND (isDeleted IS NULL OR isDeleted = 0)
			) = 0
		   )
		)		
		BEGIN
		
		-- insert the new Report
		
		INSERT INTO tblReport (
			idSite, 
			idUser,
			idDataset,
			title,
			fields,
			filter,
			[order],
			isPublic,
			dtCreated,
			dtModified
		)			
		VALUES (
			@idSite, 
			@idCaller,
			@idDataset,
			@title,
			@fields,
			@filter,
			@order,
			@isPublic,
			GETUTCDATE(),
			GETUTCDATE()
		)
		
		-- get the new Report's id and return successfully
		
		SET @idReport = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the Report id exists and it's not a deleted Report
		
		IF (SELECT COUNT(1) FROM tblReport WHERE idReport = @idReport AND (isDeleted IS NULL OR isDeleted = 0)) < 1
			
			BEGIN
				SELECT @Return_Code = 1
				SET @Error_Description_Code = 'ReportSave_NoRecordFound'
				RETURN 1
			END
			
		-- update existing Report's properties
		
		UPDATE tblReport SET
			title = @title,
			fields = @fields,
			filter = @filter,
			[order] = @order,
			isPublic = @isPublic,
			dtModified = GETUTCDATE()
		WHERE idReport = @idReport

		DELETE 
		FROM tblReportToReportShortcutsWidgetLink
		WHERE idReport = @idReport
		AND idUser <> @idUser
		AND idUser <> 1
		AND @isPublic = 0

		DELETE 
		FROM tblReportSubscription
		WHERE idReport = @idReport
		AND idUser <> @idUser
		AND idUser <> 1
		AND @isPublic = 0

		DELETE 
		FROM tblReportFile
		WHERE idReport = @idReport
		AND idUser <> @idUser
		AND idUser <> 1
		AND @isPublic = 0
		
		-- get the id and return successfully
		
		SET @idReport = @idReport
				
		END

	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
		
	IF (SELECT COUNT(1) FROM tblReportLanguage RL WHERE RL.idReport = @idReport AND RL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblReportLanguage SET
			title = @title
		WHERE idReport = @idReport
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblReportLanguage (
			idSite,
			idReport,
			idLanguage,
			title
		)
		SELECT
			@idCallerSite,
			@idReport,
			@idDefaultLanguage,
			@title
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblReportLanguage RL
			WHERE RL.idReport = @idReport
			AND RL.idLanguage = @idDefaultLanguage
		)

		END
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO