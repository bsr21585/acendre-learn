-- =====================================================================
-- PROCEDURE: [Course.CourseCodeListNotExistForCurrentSite]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.CourseCodeListNotExistForCurrentSite]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.CourseCodeListNotExistForCurrentSite]
GO

/*

Returns a recordset of course codes

*/

CREATE PROCEDURE [Course.CourseCodeListNotExistForCurrentSite]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@CourseCodeFile			CourseCodeTable	READONLY
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	SELECT DISTINCT	courseCode
	FROM @CourseCodeFile C
	WHERE NOT EXISTS
	(SELECT 1 
		FROM tblCourse CO
		WHERE CO.idSite = @idCallerSite
				AND (CO.isDeleted IS NULL OR CO.isDeleted = 0)
				AND CO.coursecode = C.courseCode)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	