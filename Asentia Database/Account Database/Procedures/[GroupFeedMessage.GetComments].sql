-- =====================================================================
-- PROCEDURE: [GroupFeedMessage.GetComments]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GroupFeedMessage.GetComments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GroupFeedMessage.GetComments]
GO

/*

Gets all wall feed message comments for a group feed message.

*/

CREATE PROCEDURE [GroupFeedMessage.GetComments]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, -- default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0, -- will fail if not specified
	
	@idGroupFeedMessage				INT,	
	@getApprovedMessagesOnly		BIT				= 1  -- default if not specified	
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/* get the data */

	SELECT
		GFM.idGroupFeedMessage,
		GFM.idGroup,
		GFM.idAuthor,
		GFM.[message],
		GFM.[timestamp],
		GFM.dtApproved,
		GFM.idApprover,
		CASE WHEN GFM.isApproved IS NULL THEN 0 ELSE GFM.isApproved END AS isApproved,
		CASE WHEN GFM.idAuthor > 1 THEN A.firstName + ' ' + A.lastName ELSE '##administrator##' END AS authorName,
		A.avatar,
		A.gender
	INTO #GroupFeedMessageComments
	FROM tblGroupFeedMessage GFM
	LEFT JOIN tblUser A ON A.idUser = GFM.idAuthor
	WHERE GFM.idParentGroupFeedMessage = @idGroupFeedMessage
	AND	(
			(@getApprovedMessagesOnly IS NULL OR @getApprovedMessagesOnly = 0)
			OR
			(@getApprovedMessagesOnly IS NOT NULL AND @getApprovedMessagesOnly = 1 AND GFM.isApproved = 1)
		)
	ORDER BY GFM.[timestamp] ASC

	/* return the data */

	SELECT 
		* 
	FROM #GroupFeedMessageComments
	ORDER BY [timestamp] ASC

	/* drop the temp table */

	DROP TABLE #GroupFeedMessageComments

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO