-- =====================================================================
-- PROCEDURE: [ReportSubscription.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[ReportSubscription.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportSubscription.Details]
GO

/*

Return all the properties for a given Report id and idCaller.

*/

CREATE PROCEDURE [ReportSubscription.Details]
(
	@Return_Code			INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT					= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT					= 0,
	
	@idReport				INT					OUTPUT, 
	@idReportSubscription	INT					OUTPUT, 
	@dtStart				DATETIME			OUTPUT,
	@recurInterval			INT					OUTPUT, 
	@recurTimeframe			NVARCHAR(4)			OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblReportSubscription
		WHERE idReport = @idReport
		AND idUser = @idCaller
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ReportSubscriptionDetails_NoRecordFound' 
		RETURN 1
		END

	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
	
	
	/*
	
	get the data
	
	*/
	
	SELECT
		@idReport				= RS.idReport,
		@idReportSubscription	= RS.idReportSubscription,
		@dtStart				= RS.dtStart,
		@recurInterval			= RS.recurInterval, 
		@recurTimeframe			= RS.recurTimeframe

	FROM tblReportSubscription RS
	WHERE 
		RS.idReport = @idReport AND RS.idSite = @idCallerSite AND RS.idUser = @idCaller
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'ReportSubscriptionDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO