-- =====================================================================
-- PROCEDURE: [User.EnrollStandupTraining]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[User.EnrollStandupTraining]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.EnrollStandupTraining]
GO

/*

Enroll a user to standup training

*/

CREATE PROCEDURE [User.EnrollStandupTraining]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idUser					INT,
	@sessions				IDTable			READONLY,
	@isWaitingList          BIT             = 0
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	make sure there are objects in the table parameter
	
	*/
	
	IF (SELECT COUNT(1) FROM @sessions) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'UserEnrollStandupTraining_NoSessionFound'
		RETURN 1
		END
		
	/*
	
	validate that the user exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUser
		WHERE idSite = @idCallerSite
		AND @idUser = idUser
		) <> 1
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'UserEnrollStandupTraining_NoUserFound'
		RETURN 1 
		END
		
	
	/*
	
	attach the user to groups where they do not already exist
	
	*/
	 DECLARE @idSession INT
	 DECLARE CursorResult CURSOR FOR 
	 SELECT id FROM @sessions
	 OPEN CursorResult 
     FETCH NEXT FROM CursorResult INTO @idSession 
     WHILE @@FETCH_STATUS = 0
	 BEGIN
		 IF(
			SELECT S.seats -
			(SELECT COUNT (1) FROM tblStandupTrainingInstanceToUserLink WHERE idStandUpTrainingInstance = @idSession AND isWaitingList = 0)
			FROM  tblStandUpTrainingInstance S
			WHERE S.idStandUpTrainingInstance = @idSession
			) > 0
		BEGIN
		IF ((Select Count(1) FROM tblStandupTrainingInstanceToUserLink STIUL 
			WHERE STIUL.idUser = @idUser 
			AND STIUL.idStandupTrainingInstance = @idSession
			AND STIUL.isWaitingList = 1) > 0)
			BEGIN
			UPDATE tblStandupTrainingInstanceToUserLink SET isWaitingList = 0
			WHERE idUSer = @idUser AND idStandupTrainingInstance= @idSession 					
			END

			ELSE
			BEGIN
				INSERT INTO tblStandupTrainingInstanceToUserLink (
				idUser, 
				idStandupTrainingInstance,
				isWaitingList
				)
				SELECT 
					@idUser, 
					@idSession,
					@isWaitingList
				WHERE NOT EXISTS (
					SELECT 1
					FROM tblStandupTrainingInstanceToUserLink STIUL
					WHERE STIUL.idUser = @idUser
					AND STIUL.idStandupTrainingInstance = @idSession
				)
			END
		END
	 FETCH NEXT FROM CursorResult INTO @idSession 
	 END
	 CLOSE CursorResult; 
	 DEALLOCATE CursorResult; 
	
	SELECT @Return_Code = 0
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

