-- =====================================================================
-- PROCEDURE: [Lesson.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Lesson.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Lesson.Delete]
GO

/*

Deletes lesson(s)

*/

CREATE PROCEDURE [Lesson.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@Lessons				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Lessons) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'LessonDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all lessons exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Lessons LL
		LEFT JOIN tblLesson L ON LL.id = L.idLesson
		WHERE L.idSite IS NULL
		OR L.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LessonDelete_NoRecordFound'
		RETURN 1 
		END
		
	/*
	
	Mark the records as deleted. DO NOT remove the record. Do NOT delete language or other sub records either.
	
	*/
	
	UPDATE tblLesson SET
		isDeleted = 1,
		dtDeleted = GETUTCDATE(),
		[order] = NULL
	WHERE idLesson IN (
		SELECT id
		FROM @Lessons
	)

	/*

	Update the lesson ordering for the remaining, undeleted lessons belonging to the course

	*/

	DECLARE @idCourse INT
	DECLARE @LessonOrdering TABLE ([order] INT IDENTITY(1,1), idLesson INT)

	SELECT @idCourse = L.idCourse FROM tblLesson L WHERE L.idLesson = (SELECT TOP 1 id FROM @Lessons)

	INSERT INTO @LessonOrdering (
		idLesson
	)
	SELECT
		L.idLesson
	FROM tblLesson L
	WHERE L.idCourse = @idCourse
	AND (L.isDeleted IS NULL OR L.isDeleted = 0)
	ORDER BY [order]
	
	UPDATE tblLesson SET
		[order] = LO.[order],
		dtModified = GETUTCDATE()
	FROM @LessonOrdering LO
	WHERE LO.idLesson = tblLesson.idLesson

	/*

	UPDATE the course's modified date

	*/

	UPDATE tblCourse SET dtModified = GETUTCDATE() WHERE idCourse = @idCourse
		
	/*
	
	DELETE the all the other user-linked/based information
	
	*/

	-- delete lesson content links for the deleted lesson
	DELETE FROM tblLessonToContentLink WHERE idLesson IN (SELECT id FROM @Lessons)
	
	--XXX - fill this in as it becomes evident
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO