-- =====================================================================
-- PROCEDURE: [Feed.GetMessagesForUser]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Feed.GetMessagesForUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Feed.GetMessagesForUser]
GO

CREATE PROCEDURE [Feed.GetMessagesForUser]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@pageNum				INT, 
	@pageSize				INT
)
AS
	BEGIN
		SET NOCOUNT ON		
			
		-- return the rowcount
			
		SELECT COUNT(1) AS row_count 
		FROM (
				SELECT
				CFM.idCourseFeedMessage [idFeedMessage],
				'course' AS [feedSourceType],
				C.title AS [feedSourceName],
				C.idCourse AS [id],
				C.idCourse AS [idFeed],
				CASE WHEN CFM.idAuthor = 1 THEN 'Administrator' ELSE A.displayName END AS [author],
				CFM.[message] AS [message],
				CFM.[timestamp] AS [timestamp],
				CASE WHEN CFMChildren.commentsCount IS NOT NULL THEN CFMChildren.commentsCount ELSE 0 END AS [commentCount]
				FROM tblEnrollment E
				LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
				LEFT JOIN tblCourseFeedMessage CFM ON CFM.idCourse = C.idCourse
				LEFT JOIN tblUser A ON A.idUser = CFM.idAuthor
				LEFT JOIN (
					SELECT idParentCourseFeedMessage, COUNT(1) AS commentsCount FROM tblCourseFeedMessage GROUP BY idParentCourseFeedMessage
					) CFMChildren ON CFMChildren.idParentCourseFeedMessage = CFM.idCourseFeedMessage
				WHERE E.idUser = @idCaller
				AND C.isFeedActive = 1
				AND CFM.isApproved = 1
				AND (CFM.idParentCourseFeedMessage = 0 OR CFM.idParentCourseFeedMessage IS NULL)

				UNION

				SELECT
				GFM.idGroupFeedMessage [idFeedMessage],
				'group' AS [feedSourceType],
				G.name AS [feedSourceName],
				G.idGroup AS [id],
				G.idGroup AS [idFeed],
				CASE WHEN GFM.idAuthor = 1 THEN 'Administrator' ELSE A.displayName END AS [author],
				GFM.[message] AS [message],
				GFM.[timestamp] AS [timestamp],
				CASE WHEN GFMChildren.commentsCount IS NOT NULL THEN GFMChildren.commentsCount ELSE 0 END AS [commentCount]
				FROM tblUserToGroupLink UGL
				LEFT JOIN tblGroup G ON G.idGroup = UGL.idGroup
				LEFT JOIN tblGroupFeedMessage GFM ON GFM.idGroup = G.idGroup
				LEFT JOIN tblUser A on A.idUser = GFM.idAuthor
				LEFT JOIN (
					SELECT idParentGroupFeedMessage, COUNT(1) AS commentsCount FROM tblGroupFeedMessage GROUP BY idParentGroupFeedMessage
					) GFMChildren ON GFMChildren.idParentGroupFeedMessage = GFM.idGroupFeedMessage
				WHERE UGL.idUser = @idCaller
				AND ISNULL(G.IsFeedActive,0) = 1
				AND ISNULL(GFM.isApproved,0) = 1
				AND ISNULL(GFM.idParentGroupFeedMessage,0) = 0
			) MAIN
			
		;WITH 
			Keys AS (
				SELECT TOP (@pageNum * @pageSize) 
					MAIN.idFeedMessage,
					MAIN.[feedSourceType],
					MAIN.[id],
					MAIN.[idFeed],
					ROW_NUMBER() OVER (ORDER BY MAIN.[timestamp] DESC) AS [row_number]
				FROM (
						SELECT
						CFM.idCourseFeedMessage [idFeedMessage],
						'course' AS [feedSourceType],
						C.title AS [feedSourceName],
						C.idCourse AS [id],
						C.idCourse AS [idFeed],
						CASE WHEN CFM.idAuthor = 1 THEN 'Administrator' ELSE A.displayName END AS [author],
						CFM.[message] AS [message],
						CFM.[timestamp] AS [timestamp],
						CASE WHEN CFMChildren.commentsCount IS NOT NULL THEN CFMChildren.commentsCount ELSE 0 END AS [commentCount]
						FROM tblEnrollment E
						LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
						LEFT JOIN tblCourseFeedMessage CFM ON CFM.idCourse = C.idCourse
						LEFT JOIN tblUser A ON A.idUser = CFM.idAuthor
						LEFT JOIN (
							SELECT idParentCourseFeedMessage, COUNT(1) AS commentsCount FROM tblCourseFeedMessage GROUP BY idParentCourseFeedMessage
							) CFMChildren ON CFMChildren.idParentCourseFeedMessage = CFM.idCourseFeedMessage
						WHERE E.idUser = @idCaller
						AND C.isFeedActive = 1
						AND CFM.isApproved = 1
						AND (CFM.idParentCourseFeedMessage = 0 OR CFM.idParentCourseFeedMessage IS NULL)

						UNION

						SELECT
						GFM.idGroupFeedMessage [idFeedMessage],
						'group' AS [feedSourceType],
						G.name AS [feedSourceName],
						G.idGroup AS [id],
						G.idGroup AS [idFeed],
						CASE WHEN GFM.idAuthor = 1 THEN 'Administrator' ELSE A.displayName END AS [author],
						GFM.[message] AS [message],
						GFM.[timestamp] AS [timestamp],
						CASE WHEN GFMChildren.commentsCount IS NOT NULL THEN GFMChildren.commentsCount ELSE 0 END AS [commentCount]
						FROM tblUserToGroupLink UGL
						LEFT JOIN tblGroup G ON G.idGroup = UGL.idGroup
						LEFT JOIN tblGroupFeedMessage GFM ON GFM.idGroup = G.idGroup
						LEFT JOIN tblUser A ON A.idUser = GFM.idAuthor
						LEFT JOIN (
							SELECT idParentGroupFeedMessage, COUNT(1) AS commentsCount FROM tblGroupFeedMessage GROUP BY idParentGroupFeedMessage
							) GFMChildren ON GFMChildren.idParentGroupFeedMessage = GFM.idGroupFeedMessage
						WHERE UGL.idUser = @idCaller
						AND ISNULL(G.IsFeedActive,0) = 1
						AND ISNULL(GFM.isApproved,0) = 1
						AND ISNULL(GFM.idParentGroupFeedMessage,0) = 0
					) MAIN
					), 
				
			SelectedKeys AS (
				SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
					idFeedMessage,
					[feedSourceType],
					[id],
					[idFeed],
					[row_number]
				FROM Keys
				WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
			)
		SELECT
			MAIN.[idFeedMessage],
			MAIN.[feedSourceType],
			MAIN.[id],
			MAIN.[idFeed],
			MAIN.[feedSourceName],
			MAIN.[author],
			MAIN.[message],
			MAIN.[timestamp],
			MAIN.[commentCount],
			SelectedKeys.[row_number]
		FROM SelectedKeys
		JOIN (
				SELECT
				CFM.idCourseFeedMessage [idFeedMessage],
				'course' AS [feedSourceType],
				C.title AS [feedSourceName],
				C.idCourse AS [id],
				C.idCourse AS [idFeed],
				CASE WHEN CFM.idAuthor = 1 THEN 'Administrator' ELSE A.displayName END AS [author],
				CFM.[message] AS [message],
				CFM.[timestamp] AS [timestamp],
				CASE WHEN CFMChildren.commentsCount IS NOT NULL THEN CFMChildren.commentsCount ELSE 0 END AS [commentCount]
				FROM tblEnrollment E
				LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
				LEFT JOIN tblCourseFeedMessage CFM ON CFM.idCourse = C.idCourse
				LEFT JOIN tblUser A ON A.idUser = CFM.idAuthor
				LEFT JOIN (
					SELECT idParentCourseFeedMessage, COUNT(1) AS commentsCount FROM tblCourseFeedMessage GROUP BY idParentCourseFeedMessage
					) CFMChildren ON CFMChildren.idParentCourseFeedMessage = CFM.idCourseFeedMessage
				WHERE E.idUser = @idCaller
				AND C.isFeedActive = 1
				AND CFM.isApproved = 1
				AND (CFM.idParentCourseFeedMessage = 0 OR CFM.idParentCourseFeedMessage IS NULL)

				UNION

				SELECT
				GFM.idGroupFeedMessage [idFeedMessage],
				'group' AS [feedSourceType],
				G.name AS [feedSourceName],
				G.idGroup AS [id],
				G.idGroup AS [idFeed],
				CASE WHEN GFM.idAuthor = 1 THEN 'Administrator' ELSE A.displayName END AS [author],
				GFM.[message] AS [message],
				GFM.[timestamp] AS [timestamp],
				CASE WHEN GFMChildren.commentsCount IS NOT NULL THEN GFMChildren.commentsCount ELSE 0 END AS [commentCount]
				FROM tblUserToGroupLink UGL
				LEFT JOIN tblGroup G ON G.idGroup = UGL.idGroup
				LEFT JOIN tblGroupFeedMessage GFM ON GFM.idGroup = G.idGroup
				LEFT JOIN tblUser A ON A.idUser = GFM.idAuthor
				LEFT JOIN (
					SELECT idParentGroupFeedMessage, COUNT(1) AS commentsCount FROM tblGroupFeedMessage GROUP BY idParentGroupFeedMessage
					) GFMChildren ON GFMChildren.idParentGroupFeedMessage = GFM.idGroupFeedMessage
				WHERE UGL.idUser = @idCaller
				AND ISNULL(G.IsFeedActive,0) = 1
				AND ISNULL(GFM.isApproved,0) = 1
				AND ISNULL(GFM.idParentGroupFeedMessage,0) = 0
			) MAIN ON MAIN.idFeedMessage = SelectedKeys.idFeedMessage AND MAIN.[feedSourceType] = SelectedKeys.[feedSourceType]
		ORDER BY SelectedKeys.[row_number]
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


