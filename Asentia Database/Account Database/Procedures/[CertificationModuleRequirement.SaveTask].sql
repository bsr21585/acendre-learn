-- =====================================================================
-- PROCEDURE: [CertificationModuleRequirement.SaveTask]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[CertificationModuleRequirement.SaveTask]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModuleRequirement.SaveTask]
GO

/*

Saves task information for a learner's certification task.
Used when a learner uploads their task.

*/

CREATE PROCEDURE [CertificationModuleRequirement.SaveTask]
(
	@Return_Code							INT				OUTPUT,
	@Error_Description_Code					NVARCHAR(50)	OUTPUT,
	@idCallerSite							INT				= 0, --default if not specified
	@callerLangString						NVARCHAR(10),
	@idCaller								INT				= 0,
	
	@idDataCertificationModuleRequirement	INT,
	@uploadedTaskFilename					NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that the certification task data exists and is not completed and approved

	*/

	IF (
		SELECT COUNT(1)
		FROM [tblData-CertificationModuleRequirement]
		WHERE [idData-CertificationModuleRequirement] = @idDataCertificationModuleRequirement
		AND idSite = @idCallerSite
		AND dtCompleted IS NULL
		AND dtApproved IS NULL
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationSaveTask_NoRecordFound'
		RETURN 1
	    END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	update the task data, it should exist, if it doesn't that is a problem

	*/
		
	UPDATE [tblData-CertificationModuleRequirement] SET
		completionDocumentationFilePath = @uploadedTaskFilename,
		dtSubmitted = @utcNow	
	WHERE [idData-CertificationModuleRequirement] = @idDataCertificationModuleRequirement

	/*

	do event log entry for certification task submitted

	*/

	DECLARE @eventLogItem EventLogItemObjects

	DECLARE @idCertification INT
	DECLARE @idCertificationToUserLink INT
	
	SELECT 
		@idCertification = CUL.idCertification,
		@idCertificationToUserLink = DCMR.idCertificationToUserLink
	FROM [tblData-CertificationModuleRequirement] DCMR
	LEFT JOIN tblCertificationToUserLink CUL ON CUL.idCertificationToUserLink = DCMR.idCertificationToUserLink
	WHERE [idData-CertificationModuleRequirement] = @idDataCertificationModuleRequirement		

	INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idCertification, @idCertificationToUserLink, @idCaller
	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 606, @utcNow, @eventLogItem
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

