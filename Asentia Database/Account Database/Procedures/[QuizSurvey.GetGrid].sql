-- =====================================================================
-- PROCEDURE: [QuizSurvey.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[QuizSurvey.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [QuizSurvey.GetGrid]
GO

/*

Gets a listing of Quizzes and Surveys.

*/

CREATE PROCEDURE [QuizSurvey.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblQuizSurvey QS
			WHERE
				(
				(@idCaller > 1 AND QS.idAuthor = @idCaller)
				OR
				(@idCaller = 1)
				)
				AND
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND QS.idSite = @idCallerSite)
				)
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						QS.idQuizSurvey,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'identifier' THEN QS.identifier END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'author' THEN U.displayName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'type' THEN QS.[type] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN QS.dtModified END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtModified' THEN QS.dtModified END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'identifier' THEN QS.identifier END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'author' THEN U.displayName END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'type' THEN QS.[type] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN QS.dtCreated END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtModified' THEN QS.dtModified END) END
						)
						AS [row_number]
					FROM tblQuizSurvey QS
					LEFT JOIN tblUser U ON U.idUser = QS.idAuthor
					WHERE
						(
						(@idCaller > 1 AND QS.idAuthor = @idCaller)
						OR
						(@idCaller = 1)
						)
						AND
						(
						(@idCallerSite IS NULL)
						OR 
						(@idCallerSite IS NOT NULL AND QS.idSite = @idCallerSite)
						)
				),
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idQuizSurvey,
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				QS.idQuizSurvey,
				QS.identifier,
				CASE WHEN QS.idAuthor = 1 THEN '##Administrator##' ELSE U.displayName END AS author,
				QS.[type],
				QS.dtCreated,
				QS.dtModified,
				QS.idContentPackage,
				CASE WHEN QS.idContentPackage IS NULL THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS isPublished,
				QS.isDraft,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				(SELECT 
					ISNULL(CONVERT(FLOAT, AVG(ISNULL(DSCO.scoreScaled, 0) * 100)), NULL)
				FROM [tblData-SCO] DSCO
				LEFT JOIN [tblData-Lesson] DL ON DL.[idData-Lesson] = DSCO.[idData-Lesson]
				LEFT JOIN tblLesson L ON L.idLesson = DL.idLesson
				LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = L.idLesson AND LCL.idContentType = 1
				LEFT JOIN tblContentPackage CP ON LCL.idObject = CP.idContentPackage
				WHERE CP.idQuizSurvey = QS.idQuizSurvey
				AND DSCO.[timestamp] IS NOT NULL) AS averageScore,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblQuizSurvey QS ON QS.idQuizSurvey = SelectedKeys.idQuizSurvey
			JOIN tblUser U ON U.idUser = QS.idAuthor
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblQuizSurvey QS
			INNER JOIN CONTAINSTABLE(tblQuizSurvey, *, @searchParam) K ON K.[key] = QS.idQuizSurvey
			WHERE
				(
				(@idCaller > 1 AND QS.idAuthor = @idCaller)
				OR
				(@idCaller = 1)
				)
				AND
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND QS.idSite = @idCallerSite)
				)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						QS.idQuizSurvey,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'identifier' THEN QS.identifier END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'author' THEN U.displayName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'type' THEN QS.[type] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN QS.dtModified END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtModified' THEN QS.dtModified END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'identifier' THEN QS.identifier END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'author' THEN U.displayName END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'type' THEN QS.[type] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtCreated' THEN QS.dtCreated END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtModified' THEN QS.dtModified END) END
						)
						AS [row_number]
					FROM tblQuizSurvey QS
					LEFT JOIN tblUser U ON U.idUser = QS.idAuthor
					INNER JOIN CONTAINSTABLE(tblQuizSurvey, *, @searchParam) K ON K.[key] = QS.idQuizSurvey
					WHERE 
						(
						(@idCaller > 1 AND QS.idAuthor = @idCaller)
						OR
						(@idCaller = 1)
						)
						AND
						(
						@idCallerSite IS NULL
						OR 
						@idCallerSite IS NOT NULL AND QS.idSite = @idCallerSite
						)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idQuizSurvey, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				QS.idQuizSurvey,
				QS.identifier,
				CASE WHEN QS.idAuthor = 1 THEN '##Administrator##' ELSE U.displayName END AS author,
				QS.[type],
				QS.dtCreated,
				QS.dtModified,
				QS.idContentPackage,
				CASE WHEN QS.idContentPackage IS NULL THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS isPublished,
				QS.isDraft,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				(SELECT 
					ISNULL(CONVERT(FLOAT, AVG(ISNULL(DSCO.scoreScaled, 0) * 100)), NULL)
				FROM [tblData-SCO] DSCO
				LEFT JOIN [tblData-Lesson] DL ON DL.[idData-Lesson] = DSCO.[idData-Lesson]
				LEFT JOIN tblLesson L ON L.idLesson = DL.idLesson
				LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = L.idLesson AND LCL.idContentType = 1
				LEFT JOIN tblContentPackage CP ON LCL.idObject = CP.idContentPackage
				WHERE CP.idQuizSurvey = QS.idQuizSurvey
				AND DSCO.[timestamp] IS NOT NULL) AS averageScore,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblQuizSurvey QS ON QS.idQuizSurvey = SelectedKeys.idQuizSurvey
			JOIN tblUser U ON U.idUser = QS.idAuthor
			ORDER BY SelectedKeys.[row_number]
			
			END
			
	END