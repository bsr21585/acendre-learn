-- =====================================================================
-- PROCEDURE: [StandUpTrainingInstanceToUserLink.UpdateUsers]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandUpTrainingInstanceToUserLink.UpdateUsers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandUpTrainingInstanceToUserLink.UpdateUsers]
GO
	
/*

Updates the rows for enrollment and wait list users of manage roster in the  tables

*/

CREATE PROCEDURE [StandUpTrainingInstanceToUserLink.UpdateUsers]
(
	@Return_Code				INT								OUTPUT,
	@Error_Description_Code		NVARCHAR(50)					OUTPUT,
	@idCallerSite				INT								= 0,	--   default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT, 

	@idStandupTrainingInstance	INT, 
	@updatedUsersRecord			StandupTrainingInstanceRoster	READONLY
)
AS

	BEGIN
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	declare and set utcNow

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	get the id of the ILT module

	*/

	DECLARE @idStandupTraining INT
	SELECT @idStandupTraining = idStandupTraining FROM tblStandupTrainingInstance WHERE idStandupTrainingInstance = @idStandupTrainingInstance			

	/*

	lets get a snapshot of all existing records for this standup training instance so that
	we can use it to only change data that has been modified and use it in the bubble up to lesson data

	*/

	CREATE TABLE #CurrentStandupTrainingInstanceData (
		idStandupTrainingInstanceToUserLink	INT,
		idUser								INT,
		idStandupTrainingInstance			INT,
		completionStatus					INT,
		successStatus						INT,
		score								INT,
		isWaitingList						BIT,
		dtCompleted							DATETIME,
		registrantKey						BIGINT,
		registrantEmail						NVARCHAR(255),
		joinUrl								NVARCHAR(1024),
		waitlistOrder						INT
	)

	INSERT INTO #CurrentStandupTrainingInstanceData (
		idStandupTrainingInstanceToUserLink,
		idUser,
		idStandupTrainingInstance,
		completionStatus,
		successStatus,
		score,
		isWaitingList,
		dtCompleted,
		registrantKey,
		registrantEmail,
		joinUrl,
		waitlistOrder
	)
	SELECT
		idStandupTrainingInstanceToUserLink,
		idUser,
		idStandupTrainingInstance,
		completionStatus,
		successStatus,
		score,
		isWaitingList,
		dtCompleted,
		registrantKey,
		registrantEmail,
		joinUrl,
		waitlistOrder
	FROM tblStandupTrainingInstanceToUserLink
	WHERE idStandupTrainingInstance = @idStandupTrainingInstance

	/*
	
	log the learner removed from "enrolled" list events

	*/

	DECLARE @learnerRemovedEventLogItems EventLogItemObjects

	INSERT INTO @learnerRemovedEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		@idStandupTraining,
		@idStandupTrainingInstance,
		idUser
	FROM tblStandupTrainingInstanceToUserLink 
	WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND isWaitingList = 0
	AND NOT EXISTS (SELECT 1 FROM @updatedUsersRecord UUR WHERE tblStandupTrainingInstanceToUserLink.idUser = UUR.idUser)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 405, @utcNow, @learnerRemovedEventLogItems


	/*

	log the learner removed from "waitlist" list events

	*/

	DECLARE @learnerRemovedEventLogWaitlistItems EventLogItemObjects

	INSERT INTO @learnerRemovedEventLogWaitlistItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		@idStandupTraining,
		@idStandupTrainingInstance,
		idUser
	FROM tblStandupTrainingInstanceToUserLink 
	WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND isWaitingList = 1
	AND NOT EXISTS (SELECT 1 FROM @updatedUsersRecord UUR WHERE tblStandupTrainingInstanceToUserLink.idUser = UUR.idUser)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 408, @utcNow, @learnerRemovedEventLogWaitlistItems


	/*

	do the deletes for learners removed

	*/

	DELETE FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND NOT EXISTS (SELECT 1 FROM @updatedUsersRecord UUR WHERE tblStandupTrainingInstanceToUserLink.idUser = UUR.idUser)	
	
	/*

	NOTE: On our updates and inserts, we do a lot of checks for isWaitListRow <> 1 and make the column null if
	the value is 1 (waitlist). This is probably not necessary as the data should be clean coming from the table
	param in current calling code. But we do it anyway as a safeguard in case this procedure were to be called 
	from code that has not ensured data integrity per the structure of how the tblStandupTrainingInstanceToUserLink
	is utilized. In other words, the checks are redundant, but leave them alone. 

	*/

	/*
	
	update records only where they need updating, i.e. where information has changed
	
	*/
	
	UPDATE tblStandupTrainingInstanceToUserLink SET
		completionStatus = CASE WHEN UUR.isWaitListRow <> 1 THEN UUR.completionStatus ELSE NULL END,
		successStatus = NULL, -- not used at the moment
		score = CASE WHEN UUR.isWaitListRow <> 1 THEN UUR.score ELSE NULL END,
		isWaitingList = UUR.isWaitListRow,
		dtCompleted = CASE 
						WHEN UUR.isWaitListRow <> 1 AND UUR.completionStatus = 2 AND (UUR.completionStatus <> CSTID.completionStatus OR CSTID.completionStatus IS NULL) THEN @utcNow
						WHEN UUR.isWaitListRow <> 1 AND UUR.completionStatus = 2 AND UUR.completionStatus = CSTID.completionStatus THEN tblStandupTrainingInstanceToUserLink.dtCompleted
						ELSE NULL END,
		registrantKey = CASE WHEN UUR.isWaitListRow <> 1 THEN UUR.registrantKey ELSE NULL END,
		registrantEmail = CASE WHEN UUR.isWaitListRow <> 1 THEN UUR.registrantEmail ELSE NULL END,
		joinUrl = CASE WHEN UUR.isWaitListRow <> 1 THEN 
					CASE WHEN UUR.listStatus = 'promoted' THEN
						UUR.joinUrl 
					ELSE
						CSTID.joinUrl
					END
				  ELSE NULL END,
		waitlistOrder = UUR.[order]
	FROM @updatedUsersRecord UUR
	LEFT JOIN #CurrentStandupTrainingInstanceData CSTID ON CSTID.idUser = UUR.idUser
	WHERE tblStandupTrainingInstanceToUserLink.idUser = UUR.idUser
	AND tblStandupTrainingInstanceToUserLink.idStandupTrainingInstance = @idStandupTrainingInstance
	AND (UUR.listStatus = 'existing' OR UUR.listStatus = 'promoted' OR UUR.listStatus = 'demoted')
	AND ( -- fields that can change through the interface have changed
			UUR.completionStatus <> CSTID.completionStatus
			OR
			(UUR.completionStatus IS NULL AND CSTID.completionStatus IS NOT NULL)
			OR
			(UUR.completionStatus IS NOT NULL AND CSTID.completionStatus IS NULL)
			OR
			UUR.successStatus <> CSTID.successStatus -- not used at the moment
			OR
			(UUR.successStatus IS NULL AND CSTID.successStatus IS NOT NULL)
			OR
			(UUR.successStatus IS NOT NULL AND CSTID.successStatus IS NULL)
			OR
			UUR.score <> CSTID.score
			OR
			(UUR.score IS NULL AND CSTID.score IS NOT NULL)
			OR
			(UUR.score IS NOT NULL AND CSTID.score IS NULL)
			OR
			UUR.isWaitListRow <> CSTID.isWaitingList
			OR
			(UUR.isWaitListRow IS NULL AND CSTID.isWaitingList IS NOT NULL)
			OR
			(UUR.isWaitListRow IS NOT NULL AND CSTID.isWaitingList IS NULL)
			OR
			UUR.registrantKey <> CSTID.registrantKey
			OR
			(UUR.registrantKey IS NULL AND CSTID.registrantKey IS NOT NULL)
			OR
			(UUR.registrantKey IS NOT NULL AND CSTID.registrantKey IS NULL)
			OR
			UUR.registrantEmail <> CSTID.registrantEmail
			OR
			(UUR.registrantEmail IS NULL AND CSTID.registrantEmail IS NOT NULL)
			OR
			(UUR.registrantEmail IS NOT NULL AND CSTID.registrantEmail IS NULL)
			OR
			UUR.joinUrl <> CSTID.joinUrl
			OR
			(UUR.joinUrl IS NULL AND CSTID.joinUrl IS NOT NULL)
			OR
			(UUR.joinUrl IS NOT NULL AND CSTID.joinUrl IS NULL)
			OR
			UUR.[order] <> CSTID.waitlistOrder
			OR
			(UUR.[order] IS NULL AND CSTID.waitlistOrder IS NOT NULL)
			OR
			(UUR.[order] IS NOT NULL AND CSTID.waitlistOrder IS NULL)
		)

	/*

	log demoted events for demoted learners

	*/

	DECLARE @learnerDemotedEventLogItems EventLogItemObjects

	INSERT INTO @learnerDemotedEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		@idStandupTraining,
		@idStandupTrainingInstance,
		UUR.idUser
	FROM @updatedUsersRecord UUR 
	WHERE UUR.listStatus = 'demoted'
	AND EXISTS (SELECT 1 FROM #CurrentStandupTrainingInstanceData CSTID WHERE CSTID.idUser = UUR.idUser AND CSTID.isWaitingList <> 1)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 410, @utcNow, @learnerDemotedEventLogItems

	/*

	log session joined events for learners added to the roster

	*/

	DECLARE @learnerAddedEventLogItems EventLogItemObjects

	INSERT INTO @learnerAddedEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		@idStandupTraining,
		@idStandupTrainingInstance,
		UUR.idUser
	FROM @updatedUsersRecord UUR 
	WHERE UUR.listStatus = 'added' 
	AND UUR.isWaitListRow = 0

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 404, @utcNow, @learnerAddedEventLogItems	

	/*

	log session joined events for learners added to the waitlist

	*/

	DECLARE @learnerAddedEventWaitlistLogItems EventLogItemObjects

	INSERT INTO @learnerAddedEventWaitlistLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		@idStandupTraining,
		@idStandupTrainingInstance,
		UUR.idUser
	FROM @updatedUsersRecord UUR 
	WHERE UUR.listStatus = 'added' 
	AND UUR.isWaitListRow = 1

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 407, @utcNow, @learnerAddedEventWaitlistLogItems	

	/*

	log session promoted events for promoted learners

	*/

	DECLARE @learnerPromotedEventLogItems EventLogItemObjects

	INSERT INTO @learnerPromotedEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		@idStandupTraining,
		@idStandupTrainingInstance,
		UUR.idUser
	FROM @updatedUsersRecord UUR 
	WHERE UUR.listStatus = 'promoted'

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 409, @utcNow, @learnerPromotedEventLogItems
			
	/*

	insert any new records where they do not exist

	*/
	
	INSERT INTO tblStandupTrainingInstanceToUserLink (
		idUser,
		idStandupTrainingInstance,
		completionStatus,
		successStatus,
		score,
		isWaitingList,
		dtCompleted,
		registrantKey,
		registrantEmail,
		joinUrl,
		waitlistOrder
	)
	SELECT
		UUR.idUser,
		@idStandupTrainingInstance,	
		CASE WHEN UUR.isWaitListRow <> 1 THEN UUR.completionStatus ELSE NULL END,
		NULL, -- not used at the moment
		CASE WHEN UUR.isWaitListRow <> 1 THEN UUR.score ELSE NULL END,
		UUR.isWaitListRow,
		CASE WHEN UUR.isWaitListRow <> 1 AND UUR.completionStatus = 2 THEN @utcNow ELSE NULL END,
		CASE WHEN UUR.isWaitListRow <> 1 THEN UUR.registrantKey ELSE NULL END,
		CASE WHEN UUR.isWaitListRow <> 1 THEN UUR.registrantEmail ELSE NULL END,
		CASE WHEN UUR.isWaitListRow <> 1 THEN UUR.joinUrl ELSE NULL END,
		UUR.[order]
	FROM @updatedUsersRecord UUR
	WHERE NOT EXISTS (SELECT 1 FROM tblStandupTrainingInstanceToUserLink STIUL
					  WHERE STIUL.idUser = UUR.idUser
					  AND STIUL.idStandupTrainingInstance = @idStandupTrainingInstance)

	/*

	now let's start evaluating for bubble up to lesson data, this means checking and processing records that have
	gone from not completed (unknown or incomplete) to completed and vice versa, checking for lesson data records
	this standup training instance may apply to and performing the appropriate action (set completion or unset completion),
	then bubbling up further to evaluations for enrollment and learning path completion

	notes: 
	
	this only effects lesson data for CURRENT enrollments, where the enrollment has already started and has not expired

	application of lesson data updates and enrollment completion evaluation where the learner has completed a standup training
	instance prior to enrollment start will happen in another procedure

	*/

	/*

	get lesson data records that are tied to this standup training instance

	*/

	CREATE TABLE #LessonDataRecords (
		idUser						INT,
		[idData-Lesson]				INT,
		idEnrollment				INT,
		dtCompleted					DATETIME,
		contentTypeCommittedTo		INT,
		dtCommittedToContentType	DATETIME
	)

	INSERT INTO #LessonDataRecords (
		idUser,
		[idData-Lesson],
		idEnrollment,
		dtCompleted,
		contentTypeCommittedTo,
		dtCommittedToContentType
	)
	SELECT
		E.idUser,
		LD.[idData-Lesson],
		LD.idEnrollment,
		LD.dtCompleted,
		LD.contentTypeCommittedTo,
		LD.dtCommittedToContentType
	FROM [tblData-Lesson] LD
	LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = LD.idLesson AND LCL.idContentType = 2 -- standup training modules
	LEFT JOIN tblStandupTrainingInstance STI ON STI.idStandupTraining = LCL.idObject
	LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
	WHERE STI.idStandupTrainingInstance = @idStandupTrainingInstance
	AND E.idUser IN (SELECT idUser FROM @updatedUsersRecord)
	AND (contentTypeCommittedTo = 2 OR contentTypeCommittedTo IS NULL) -- not committed to any other content types but standup training
	AND E.dtStart <= @utcNow										   -- enrollment has started
	-- enrollment has not expired
	AND (E.dtExpiresFromStart > @utcNow OR E.dtExpiresFromStart IS NULL)
	AND (E.dtExpiresFromFirstLaunch > @utcNow OR E.dtExpiresFromFirstLaunch IS NULL)

	/*

	mark lesson data as completed for standup training records that have gone from not completed to completed

	*/

	UPDATE [tblData-Lesson] SET
		dtCompleted = @utcNow,
		contentTypeCommittedTo = CASE WHEN [tblData-Lesson].contentTypeCommittedTo IS NULL THEN 2 ELSE [tblData-Lesson].contentTypeCommittedTo END,
		dtCommittedToContentType = CASE WHEN [tblData-Lesson].dtCommittedToContentType IS NULL THEN @utcNow ELSE [tblData-Lesson].dtCommittedToContentType END
	FROM #LessonDataRecords LDR
	LEFT JOIN #CurrentStandupTrainingInstanceData CSTID ON CSTID.idUser = LDR.idUser
	LEFT JOIN @updatedUsersRecord UUR ON UUR.idUser = LDR.idUser
	WHERE [tblData-Lesson].[idData-Lesson] = LDR.[idData-Lesson]
	AND LDR.dtCompleted IS NULL -- dont update completion date if it is already completed
	AND (CSTID.completionStatus <> 2 OR CSTID.completionStatus IS NULL)
	AND UUR.completionStatus = 2
	AND UUR.isWaitListRow <> 1

	/*

	do event log entries for lesson data completions

	*/

	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		LD.idLesson,
		LDR.[idData-Lesson],
		E.idUser
	FROM #LessonDataRecords LDR
	LEFT JOIN [tblData-Lesson] LD ON LD.[idData-Lesson] = LDR.[idData-Lesson]
	LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
	LEFT JOIN #CurrentStandupTrainingInstanceData CSTID ON CSTID.idUser = LDR.idUser
	LEFT JOIN @updatedUsersRecord UUR ON UUR.idUser = LDR.idUser
	WHERE LDR.dtCompleted IS NULL -- dont update completion date if it is already completed
	AND (CSTID.completionStatus <> 2 OR CSTID.completionStatus IS NULL)
	AND UUR.completionStatus = 2
	AND UUR.isWaitListRow <> 1

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 303, @utcNow, @eventLogItems

	/*

	mark lesson data as completed for standup training records that have gone from completed to not completed

	*/

	UPDATE [tblData-Lesson] SET
		dtCompleted = NULL,
		contentTypeCommittedTo = CASE WHEN [tblData-Lesson].contentTypeCommittedTo IS NULL THEN 2 ELSE [tblData-Lesson].contentTypeCommittedTo END,
		dtCommittedToContentType = CASE WHEN [tblData-Lesson].dtCommittedToContentType IS NULL THEN @utcNow ELSE [tblData-Lesson].dtCommittedToContentType END
	FROM #LessonDataRecords LDR
	LEFT JOIN #CurrentStandupTrainingInstanceData CSTID ON CSTID.idUser = LDR.idUser
	LEFT JOIN @updatedUsersRecord UUR ON UUR.idUser = LDR.idUser
	WHERE [tblData-Lesson].[idData-Lesson] = LDR.[idData-Lesson]
	AND CSTID.completionStatus = 2 
	AND (UUR.completionStatus <> 2 OR UUR.completionStatus IS NULL)
	AND UUR.isWaitListRow <> 1	

	/*

	cursor through the enrollment records for our lesson data and evaluate completion for each
	
	note that we're only going to evaluate the enrollments for lesson data that we updated
	also note that enrollment completion is the only thing we need to do here, that procedure 
	will bubble up to certificate and learning path completion

	*/

	DECLARE @idEnrollment INT
	DECLARE enrollmentEvaluationCursor CURSOR LOCAL FOR
			SELECT
				idEnrollment
			FROM #LessonDataRecords LDR
			LEFT JOIN #CurrentStandupTrainingInstanceData CSTID ON CSTID.idUser = LDR.idUser
			LEFT JOIN @updatedUsersRecord UUR ON UUR.idUser = LDR.idUser
			WHERE ( -- not complete to complete
					LDR.dtCompleted IS NULL
					AND (CSTID.completionStatus <> 2 OR CSTID.completionStatus IS NULL)
					AND UUR.completionStatus = 2
					)
			OR	  ( -- complete to not complete
					CSTID.completionStatus = 2 
					AND (UUR.completionStatus <> 2 OR UUR.completionStatus IS NULL)
					)
			AND UUR.isWaitListRow <> 1

	OPEN enrollmentEvaluationCursor

	FETCH NEXT FROM enrollmentEvaluationCursor INTO @idEnrollment

	WHILE @@FETCH_STATUS = 0
		BEGIN
		EXEC [Enrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollment
		FETCH NEXT FROM enrollmentEvaluationCursor INTO @idEnrollment
		END

	-- kill the cursor
	CLOSE enrollmentEvaluationCursor
	DEALLOCATE enrollmentEvaluationCursor

	-- drop the temportary tables
	DROP TABLE #CurrentStandupTrainingInstanceData
	DROP TABLE #LessonDataRecords

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
