-- =====================================================================
-- PROCEDURE: [TimeZone.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TimeZone.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TimeZone.Details]
GO
/*

Returns a recordset of timezone ids and names.

*/

CREATE PROCEDURE [TimeZone.Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idTimezone				INT,
	@dotNetName				NVARCHAR(255)	OUTPUT,
	@displayName			NVARCHAR(255)	OUTPUT,
	@gmtOffset				FLOAT			OUTPUT,
	@blnUseDaylightSavings	BIT				OUTPUT,
	@order					INT				OUTPUT,
	@isEnabled				BIT				OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM dbo.tblTimezone
		WHERE idTimezone = @idTimezone
		) = 0 
		BEGIN
		SET @Error_Description_Code = 'TimeZoneDetails_NoRecordFound'
		SET @Return_Code = 1
		RETURN 1
		END
	
	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	/*
	
	get the data
	
	*/	
		
	SELECT 
		@dotNetName				=	dotNetName,
		@displayName			=	displayName,
		@gmtOffset				=	gmtOffset,
		@blnUseDaylightSavings	=	blnUseDaylightSavings,
		@order					=	[order],
		@isEnabled				=	isEnabled
	 FROM dbo.tblTimezone 
	 WHERE idTimezone = @idTimezone
		
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'TimeZoneDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

