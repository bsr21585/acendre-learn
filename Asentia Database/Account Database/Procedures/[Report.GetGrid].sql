-- =====================================================================
-- PROCEDURE: [Report.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Report.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Report.GetGrid]
GO

/*

Gets a listing of Reports

*/

CREATE PROCEDURE [Report.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idDataset				INT				= 0,
	@idUser					INT				= 0,
	@isPublicRequired		BIT				= 0,

	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/* DATASET TO PERMISSION LINKS */

		CREATE TABLE #DatasetToPermission (
			idDataset INT,
			idPermission INT
		)
		INSERT INTO #DatasetToPermission (
			idDataset,
			idPermission
		)
		SELECT 1, 401
		UNION SELECT 2, 402
		UNION SELECT 3, 403
		UNION SELECT 4, 404
		UNION SELECT 5, 405
		UNION SELECT 6, 406
		UNION SELECT 7, 407
		UNION SELECT 8, 408
		UNION SELECT 9, 409

		/* EFFECTIVE PERMISSIONS */

		CREATE TABLE #UserEffectivePermissions (
			idPermission INT
		)

		INSERT INTO #UserEffectivePermissions (
			idPermission
		)
		SELECT DISTINCT
			RPL.idPermission
		FROM tblUserToRoleLink URL
		LEFT JOIN tblRole R ON R.idRole = URL.idRole
		LEFT JOIN tblRoleToPermissionLink RPL ON RPL.idRole = R.idRole
		WHERE URL.idUser = @idCaller
		AND RPL.idPermission >= 400

		INSERT INTO #UserEffectivePermissions (
			idPermission
		)
		SELECT DISTINCT
			RPL.idPermission
		FROM tblGroupToRoleLink GRL
		LEFT JOIN tblRole R ON R.idRole = GRL.idRole
		LEFT JOIN tblRoleToPermissionLink RPL ON RPL.idRole = R.idRole
		WHERE GRL.idGroup IN (SELECT DISTINCT idGroup FROM tblUserToGroupLink WHERE idUser = @idCaller)
		AND RPL.idPermission >= 400
		AND NOT EXISTS (SELECT 1
						FROM #UserEffectivePermissions UEP
						WHERE UEP.idPermission = RPL.idPermission)

		INSERT INTO #UserEffectivePermissions (
			idPermission
		)
		SELECT idPermission
		FROM tblPermission
		WHERE idPermission >= 400
		AND @idCaller = 1

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblReport R
			LEFT JOIN tblReportLanguage RL 
				ON RL.idReport = R.idReport AND RL.idLanguage = @idCallerLanguage
			LEFT JOIN tblDataset DS
				ON DS.idDataset = R.idDataset
			LEFT JOIN tblUser O
				ON O.idUser = R.idUser
			LEFT JOIN tblDatasetLanguage DSL
				ON DSL.idDataset = R.idDataset
				AND DSL.idLanguage = @idCallerLanguage
				WHERE R.idSite = CASE WHEN @idCallerSite > 0  THEN  @idCallerSite ELSE R.idSite END 
				  AND R.idDataset  = CASE WHEN @idDataset > 0  THEN  @idDataset ELSE R.idDataset END 
				  AND R.idUser = CASE WHEN @idUser > 0  THEN  @idUser ELSE R.idUser END  
				  AND R.isPublic = CASE WHEN @isPublicRequired > 0  THEN  1 ELSE R.isPublic END
				  AND R.idUser <> CASE WHEN @isPublicRequired > 0  THEN  @idCaller ELSE 0 END
				  AND (R.isDeleted IS NULL OR R.isDeleted = 0)
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						R.idReport,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN R.title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dataSet' THEN DS.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'ownerName' THEN O.displayName END) END DESC,

							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN R.title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dataSet' THEN DS.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'ownerName' THEN O.displayName END) END
						)
						AS [row_number]

					FROM tblReport R
					LEFT JOIN tblReportLanguage RL 
						ON RL.idReport = R.idReport AND RL.idLanguage = @idCallerLanguage
					LEFT JOIN tblDataset DS
						ON DS.idDataset = R.idDataset
					LEFT JOIN tblUser O
						ON O.idUser = R.idUser
					LEFT JOIN tblDatasetLanguage DSL
						ON DSL.idDataset = R.idDataset
						AND DSL.idLanguage = @idCallerLanguage
						WHERE R.idSite = CASE WHEN @idCallerSite > 0  THEN  @idCallerSite ELSE R.idSite END 
						  AND R.idDataset  = CASE WHEN @idDataset > 0  THEN  @idDataset ELSE R.idDataset END  
						  AND R.idUser = CASE WHEN @idUser > 0  THEN  @idUser ELSE R.idUser END  
						  AND R.isPublic = CASE WHEN @isPublicRequired > 0  THEN  1 ELSE R.isPublic END
						  AND R.idUser <> CASE WHEN @isPublicRequired > 0  THEN  @idCaller ELSE 0 END
						  AND (R.isDeleted IS NULL OR R.isDeleted = 0)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idReport, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				R.idReport AS idReport,
				CASE WHEN R.idUser = 1 THEN
					'Administrator'
				ELSE
				CASE WHEN O.idUser is null THEN
					'Unknown' -- should never happen
				ELSE
					O.lastname + ', ' + O.firstname + (CASE WHEN O.middlename IS NOT NULL AND O.middlename <> '' THEN ' ' + O.middlename ELSE '' END)
				END
				END AS ownerName,
				DS.idDataSet, 
				CASE WHEN DSL.name IS NULL OR DSL.name = '' THEN DS.name ELSE DSL.name END AS dataSet,
				CASE WHEN RL.title IS NULL OR RL.title = '' THEN R.title ELSE RL.title END AS title,
				(SELECT COUNT(1) FROM tblReportSubscription RS WHERE RS.idReport = R.idReport) AS subscriptionCount,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblReport R ON R.idReport = SelectedKeys.idReport
				LEFT JOIN tblReportLanguage RL 
					ON RL.idReport = R.idReport AND RL.idLanguage = @idCallerLanguage
				LEFT JOIN tblDataset DS
					ON DS.idDataset = R.idDataset
				LEFT JOIN tblUser O
					ON O.idUser = R.idUser
				LEFT JOIN tblDatasetLanguage DSL
					ON DSL.idDataset = R.idDataset
					AND DSL.idLanguage = @idCallerLanguage
				WHERE R.idSite = CASE WHEN @idCallerSite > 0  THEN  @idCallerSite ELSE R.idSite END 
					AND ((@idDataset > 0 AND R.idDataset = @idDataset)  
							OR (@idDataset = 0 AND (R.idDataset >= 1000 OR (R.idDataset IN (
									SELECT idDataset FROM #DatasetToPermission DP WHERE DP.idPermission IN(
										   SELECT idPermission FROM #UserEffectivePermissions)))))) 
					AND R.idUser = CASE WHEN @idUser > 0  THEN  @idUser ELSE R.idUser END  
					AND R.isPublic = CASE WHEN @isPublicRequired > 0  THEN  1 ELSE R.isPublic END
					AND R.idUser <> CASE WHEN @isPublicRequired > 0  THEN  @idCaller ELSE 0 END
					AND (R.isDeleted IS NULL OR R.isDeleted = 0)

			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblReport R
				INNER JOIN CONTAINSTABLE(tblReport, *, @searchParam) K ON K.[key] = R.idReport
				LEFT JOIN tblReportLanguage RL 
					ON RL.idReport = R.idReport AND RL.idLanguage = @idCallerLanguage
				LEFT JOIN tblDataset DS
					ON DS.idDataset = R.idDataset
				LEFT JOIN tblUser O
					ON O.idUser = R.idUser
				LEFT JOIN tblDatasetLanguage DSL
				ON DSL.idDataset = R.idDataset
				AND DSL.idLanguage = @idCallerLanguage
				WHERE R.idSite = CASE WHEN @idCallerSite > 0  THEN  @idCallerSite ELSE R.idSite END 
				  AND R.idDataset  = CASE WHEN @idDataset > 0  THEN  @idDataset ELSE R.idDataset END  
				  AND R.idUser = CASE WHEN @idUser > 0  THEN  @idUser ELSE R.idUser END  
				  AND R.isPublic = CASE WHEN @isPublicRequired > 0  THEN  1 ELSE R.isPublic END
				  AND R.idUser <> CASE WHEN @isPublicRequired > 0  THEN  @idCaller ELSE 0 END
				  AND (R.isDeleted IS NULL OR R.isDeleted = 0)
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							R.idReport,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN R.title END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dataSet' THEN DS.name END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'ownerName' THEN O.displayName END) END DESC,

								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN R.title END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dataSet' THEN DS.name END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'ownerName' THEN O.displayName END) END
							)
							AS [row_number]
						FROM tblReport R
						LEFT JOIN tblReportLanguage RL 
							ON RL.idReport = R.idReport AND RL.idLanguage = @idCallerLanguage
						LEFT JOIN tblDataset DS
							ON DS.idDataset = R.idDataset
						LEFT JOIN tblUser O
							ON O.idUser = R.idUser
						LEFT JOIN tblDatasetLanguage DSL
							ON DSL.idDataset = R.idDataset
							AND DSL.idLanguage = @idCallerLanguage
							WHERE R.idSite = CASE WHEN @idCallerSite > 0  THEN  @idCallerSite ELSE R.idSite END 
							  AND R.idDataset  = CASE WHEN @idDataset > 0  THEN  @idDataset ELSE R.idDataset END  
							  AND R.idUser = CASE WHEN @idUser > 0  THEN  @idUser ELSE R.idUser END  
							  AND R.isPublic = CASE WHEN @isPublicRequired > 0  THEN  1 ELSE R.isPublic END
							  AND R.idUser <> CASE WHEN @isPublicRequired > 0  THEN  @idCaller ELSE 0 END
							  AND (R.isDeleted IS NULL OR R.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idReport, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
				R.idReport AS idReport,
				CASE WHEN R.idUser = 1 THEN
					'Administrator'
				ELSE
				CASE WHEN O.idUser is null THEN
					'Unknown' -- should never happen
				ELSE
					O.lastname + ', ' + O.firstname + (CASE WHEN O.middlename IS NOT NULL AND O.middlename <> '' THEN ' ' + O.middlename ELSE '' END)
				END
				END AS ownerName,
				DS.idDataSet, 
				CASE WHEN DSL.name IS NULL OR DSL.name = '' THEN DS.name ELSE DSL.name END AS dataSet,
				CASE WHEN RL.title IS NULL OR RL.title = '' THEN R.title ELSE RL.title END AS title,
				(SELECT COUNT(1) FROM tblReportSubscription RS WHERE RS.idReport = R.idReport) AS subscriptionCount,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblReport R ON R.idReport = SelectedKeys.idReport
				LEFT JOIN tblReportLanguage RL 
					ON RL.idReport = R.idReport AND RL.idLanguage = @idCallerLanguage
				LEFT JOIN tblDataset DS
					ON DS.idDataset = R.idDataset
				LEFT JOIN tblUser O
					ON O.idUser = R.idUser
				LEFT JOIN tblDatasetLanguage DSL
					ON DSL.idDataset = R.idDataset
					AND DSL.idLanguage = @idCallerLanguage
				WHERE R.idSite = CASE WHEN @idCallerSite > 0  THEN  @idCallerSite ELSE R.idSite END 
					  AND ((@idDataset > 0 AND R.idDataset = @idDataset)  
								OR (@idDataset = 0 AND (R.idDataset >= 1000 OR (R.idDataset IN (
										SELECT idDataset FROM #DatasetToPermission DP WHERE DP.idPermission IN(
											SELECT idPermission FROM #UserEffectivePermissions))))))  
					  AND R.idUser = CASE WHEN @idUser > 0  THEN  @idUser ELSE R.idUser END  
					  AND R.isPublic = CASE WHEN @isPublicRequired > 0  THEN  1 ELSE R.isPublic END
					  AND R.idUser <> CASE WHEN @isPublicRequired > 0  THEN  @idCaller ELSE 0 END
					  AND (R.isDeleted IS NULL OR R.isDeleted = 0)

			ORDER BY SelectedKeys.[row_number]
						
			END

        DROP TABLE #DatasetToPermission
		DROP TABLE #UserEffectivePermissions
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO