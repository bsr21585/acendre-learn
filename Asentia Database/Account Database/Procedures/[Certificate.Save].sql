-- =====================================================================
-- PROCEDURE: [Certificate.Save]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certificate.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certificate.Save]
GO

/*

Adds new certificate or updates existing certificate.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [Certificate.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idCertificate						INT				OUTPUT,
	@name								NVARCHAR(255),
	@issuingOrganization				NVARCHAR(255),
	@description						NVARCHAR(MAX),
	@credits							FLOAT,
	@code								NVARCHAR(25), 
	@filename							NVARCHAR(255),
	@isActive							BIT,
	@expiresInterval					INT,
	@expiresTimeframe					NVARCHAR(4),
	@objectType							INT,
	@idObject							INT,
	@reissueBasedOnMostRecentCompletion	BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED
	
	
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'CertificateSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END
		
	*/
	
			 
	/*
	
	validate uniqueness for name, code
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificate
		WHERE idSite = @idCallerSite
		AND name = @name
		AND (
			@idCertificate IS NULL
			OR 
			@idCertificate <> idCertificate
			)
		AND (isDeleted IS NULL OR isDeleted = 0)
		) > 0
		
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'CertificateSave_FieldNameNotUnique'
		RETURN 1 
		END

	IF (
		SELECT COUNT(1)
		FROM tblCertificate
		WHERE idSite = @idCallerSite
		AND code = @code
		AND (
			@idCertificate IS NULL
			OR 
			@idCertificate <> idCertificate
			)
		AND (isDeleted IS NULL OR isDeleted = 0)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'CertificateSave_FieldCodeNotUnique'
		RETURN 1 
		END

	/*

	check XSS vulnerabilities

	*/

	-- description

	IF (@description LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CertificateSave_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@description LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CertificateSave_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@description LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CertificateSave_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@description LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CertificateSave_DescTagNotAllowed_Iframe'
	RETURN 1 
	END

	/*
	
	save the data
	
	*/
	
	IF (@idCertificate = 0 OR @idCertificate IS NULL)
		
		BEGIN
		
		-- insert the new certificate
		
		INSERT INTO tblCertificate (			
			idSite,
			name,
			issuingOrganization,
			[description],
			credits,
			code,
			[filename],
			isActive,
			activationDate,
			expiresInterval,
			expiresTimeframe,
			objectType,
			idObject,
			reissueBasedOnMostRecentCompletion
		)			
		VALUES (
			@idCallerSite,
			@name,
			@issuingOrganization,
			@description,
			@credits,
			@code,
			@filename,
			@isActive,
			CASE WHEN @isActive = 1 THEN GETUTCDATE() ELSE NULL END,
			@expiresInterval,
			@expiresTimeframe,
			@objectType,
			@idObject,
			@reissueBasedOnMostRecentCompletion
		)
		
		-- get the new certificate's id
		
		SELECT @idCertificate = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the certificate id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblCertificate WHERE idCertificate = @idCertificate AND idSite = @idCallerSite AND (isDeleted IS NULL OR isDeleted = 0)) < 1
			BEGIN
			
			SET @idCertificate = @idCertificate
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertificateSave_NoRecordFound'
			RETURN 1
			
			END
			
		-- update existing certificate's properties

		-- get the current isActive flag for the certificate so we can set (or unset) activationDate
		DECLARE @currentIsActiveFlag BIT
		SELECT @currentIsActiveFlag = isActive FROM tblCertificate WHERE idCertificate = @idCertificate
		
		UPDATE tblCertificate SET
			name = @name,
			issuingOrganization = @issuingOrganization,
			[description] = @description,
			credits = @credits,
			code = @code,
			[filename] = @filename,
			isActive = @isActive,
			activationDate = CASE WHEN @currentIsActiveFlag = @isActive THEN
								activationDate
							 ELSE
								CASE WHEN @currentIsActiveFlag = 1 AND @isActive = 0 THEN
									NULL
								ELSE
									GETUTCDATE()
								END
							 END,
			expiresInterval = @expiresInterval,
			expiresTimeframe = @expiresTimeframe,
			reissueBasedOnMostRecentCompletion = @reissueBasedOnMostRecentCompletion
		WHERE idCertificate = @idCertificate
		
		-- get the certificate's id 

		SET @idCertificate = @idCertificate
		
		END
	
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF (SELECT COUNT(1) FROM tblCertificateLanguage CL WHERE CL.idCertificate = @idCertificate AND CL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN
		
		UPDATE tblCertificateLanguage SET
			name = @name,
			[description] = @description,
			[filename] = @filename
		WHERE idCertificate = @idCertificate
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
		
		INSERT INTO tblCertificateLanguage (
			idSite,
			idCertificate,
			idLanguage,
			name,
			[description],
			[filename]
		)
		SELECT
			@idCallerSite,
			@idCertificate,
			@idDefaultLanguage,
			@name,
			@description,
			@filename
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCertificateLanguage CL
			WHERE CL.idCertificate = @idCertificate
			AND CL.idLanguage = @idDefaultLanguage
		)
		
		END
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO