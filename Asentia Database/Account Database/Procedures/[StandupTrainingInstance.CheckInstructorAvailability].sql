-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.CheckInstructorAvailability]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.CheckInstructorAvailability]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.CheckInstructorAvailability]
GO

/*

Check whether or not a user is availabile to instruct a session during
specified times. Essentially, this checks for scheduling conflicts against
other sessions.

*/

CREATE PROCEDURE [StandupTrainingInstance.CheckInstructorAvailability]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT,
	@idInstructor				INT,
	@MeetingTimes				DateRange		READONLY,
	@isAvailable				BIT             OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	set default value

	*/

	SET @isAvailable = 1

	/*

	check user/instructor availability

	*/

	CREATE TABLE #tblInstructorToObjectLink (
		idStandupTrainingInstance	INT,
		idInstructor				INT,
		dtStart						DATETIME,
		dtEnd						DATETIME
	)

	INSERT INTO #tblInstructorToObjectLink (
		idStandupTrainingInstance,
		idInstructor,
		dtStart,
		dtEnd
	)
	SELECT
		STI.idStandUpTrainingInstance,
		STII.idInstructor,
		STIMT.dtStart,
		STIMT.dtEnd
	FROM tblStandUpTrainingInstance STI
	LEFT JOIN tblStandupTrainingInstanceToInstructorLink STII ON STII.idStandupTrainingInstance = STI.idStandUpTrainingInstance
	INNER JOIN tblStandUpTrainingInstanceMeetingTime STIMT ON STI.idStandUpTrainingInstance = STIMT.idStandUpTrainingInstance
	WHERE (STI.isDeleted IS NULL OR STI.isDeleted = 0)

	IF (
		SELECT COUNT(1)
        FROM #tblInstructorToObjectLink IOL
        CROSS JOIN @MeetingTimes DR WHERE IOL.idInstructor = @idInstructor   
									AND IOL.idStandupTrainingInstance != @idStandupTrainingInstance 
									AND (  
											(DR.dtStart <= IOL.dtStart AND DR.dtEnd >= IOL.dtEnd)
											OR (DR.dtStart BETWEEN IOL.dtStart AND IOL.dtEnd)
											OR (DR.dtEnd   BETWEEN IOL.dtStart AND IOL.dtEnd)
									)      
		) > 0		
		
		BEGIN 
		SET @isAvailable = 0
		END

	DROP TABLE #tblInstructorToObjectLink
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO