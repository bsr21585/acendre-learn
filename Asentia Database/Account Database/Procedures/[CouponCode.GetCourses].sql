-- =====================================================================
-- PROCEDURE: [CouponCode.GetCourses]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CouponCode.GetCourses]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CouponCode.GetCourses]
GO

/*

Returns a recordset of course ids and titles that belong to a coupon code.

*/

CREATE PROCEDURE [CouponCode.GetCourses]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCouponCode			INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL

		BEGIN

		SELECT
			DISTINCT
			C.idCourse, 
			CASE WHEN C.coursecode IS NOT NULL THEN
				C.coursecode + ' ' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			ELSE
				CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			END AS title
		FROM tblCourse C
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage				
		WHERE C.idSite = @idCallerSite
		AND (C.isDeleted = 0 OR C.isDeleted IS NULL)
		AND EXISTS (SELECT 1 FROM tblCouponCodeToCourseLink CCCL
					WHERE CCCL.idCouponCode = @idCouponCode
					AND CCCL.idCourse = C.idCourse)
		ORDER BY title

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			C.idCourse,
			CASE WHEN C.coursecode IS NOT NULL THEN
				C.coursecode + ' ' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			ELSE
				CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			END AS title
		FROM tblCourseLanguage CL
		INNER JOIN CONTAINSTABLE(tblCourseLanguage, *, @searchParam) K ON K.[key] = CL.idCourseLanguage AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCourse C ON C.idCourse = CL.idCourse
		WHERE C.idSite = @idCallerSite
		AND (C.isDeleted = 0 OR C.isDeleted IS NULL)
		AND EXISTS (SELECT 1 FROM tblCouponCodeToCourseLink CCCL
					WHERE CCCL.idCouponCode = @idCouponCode
					AND CCCL.idCourse = C.idCourse)
		ORDER BY title

		END
	
	SELECT @Error_Description_Code = ''
	SELECT @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	