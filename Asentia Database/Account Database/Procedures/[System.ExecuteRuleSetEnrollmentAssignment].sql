-- =====================================================================
-- PROCEDURE: [System.ExecuteRuleSetEnrollmentAssignment]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ExecuteRuleSetEnrollmentAssignment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ExecuteRuleSetEnrollmentAssignment]
GO

/*

Executes the RuleSetEnrollment.AssignEnrollments procedure for the entire system.

*/
CREATE PROCEDURE [System.ExecuteRuleSetEnrollmentAssignment]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,
	@Courses					IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* 
	
	Execute the RuleSetEnrollment.AssignEnrollments procedure with filters, the procedure gets the rules matches.
	
	*/	

	EXEC [RuleSetEnrollment.AssignEnrollments] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @Courses, @Filters, @filterBy

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO