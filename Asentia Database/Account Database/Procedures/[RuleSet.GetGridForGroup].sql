-- =====================================================================
-- PROCEDURE: [RuleSet.GetGridForGroup]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSet.GetGridForGroup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSet.GetGridForGroup]
GO

/*

Gets a listing of rule sets.

*/

CREATE PROCEDURE [RuleSet.GetGridForGroup]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@id						INT,
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/
					
		IF @searchParam IS NULL
		
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblRuleSet RS
			INNER JOIN tblRuleSetToGroupLink RGL ON RS.idRuleSet = RGL.idRuleSet
			WHERE 
				(
					@idCallerSite IS NULL
					OR 
					@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite
				)
			AND RGL.idGroup = @id
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						RS.idRuleSet,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN RS.label END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RS.label END
						)
						AS [row_number]
					FROM tblRuleSet RS
					INNER JOIN tblRuleSetToGroupLink RGL ON RS.idRuleSet = RGL.idRuleSet
					WHERE 
						(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite
						)
					AND RGL.idGroup = @id
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idRuleSet, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				R.idRuleSet,
				CASE WHEN RSL.label IS NOT NULL THEN RSL.label ELSE R.label END AS label,
				R.isAny,
				Convert(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblRuleSet R ON R.idRuleSet = SelectedKeys.idRuleSet
			LEFT JOIN tblRuleSetLanguage RSL ON RSL.idRuleSet = R.idRuleSet AND RSL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
			
			END
		
		ELSE
		
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN
				-- return the rowcount
				
				SELECT COUNT(1) AS row_count 
				FROM tblRuleSet RS
				INNER JOIN tblRuleSetToGroupLink RGL ON RS.idRuleSet = RGL.idRuleSet
				INNER JOIN CONTAINSTABLE(tblRuleSet, *, @searchParam) K ON K.[key] = RS.idRuleSet
				WHERE 
					(
						@idCallerSite IS NULL
						OR 
						@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite
					)
				AND RGL.idGroup = @id
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							RS.idRuleSet,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN RS.label END DESC,
								
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RS.label END
							)
							AS [row_number]
						FROM tblRuleSet RS
						INNER JOIN tblRuleSetToGroupLink RGL ON RS.idRuleSet = RGL.idRuleSet
						INNER JOIN CONTAINSTABLE(tblRuleSet, *, @searchParam) K ON K.[key] = RS.idRuleSet
						WHERE
							(
								@idCallerSite IS NULL
								OR 
								@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite
							)
						AND RGL.idGroup = @id
					),
					
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idRuleSet, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT
					R.idRuleSet,
					R.label,
					R.isAny,
					Convert(bit, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblRuleSet R ON R.idRuleSet = SelectedKeys.idRuleSet
				ORDER BY SelectedKeys.[row_number]
				
				END
				
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN
				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblRuleSetLanguage RSL
				LEFT JOIN tblRuleSet RS ON RS.idRuleSet = RSL.idRuleSet
				INNER JOIN tblRuleSetToGroupLink RGL ON RS.idRuleSet = RGL.idRuleSet
				INNER JOIN CONTAINSTABLE(tblRuleSetLanguage, *, @searchParam) K ON K.[key] = RSL.idRuleSetLanguage
				WHERE 
					(
					(@idCallerSite IS NULL)
					OR 
					(@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite)
					)
					AND RSL.idLanguage = @idCallerLanguage
					AND RGL.idGroup = @id
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							RSL.idRuleSetLanguage,
							RS.idRuleSet,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN RSL.label END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RSL.label END
							)
							AS [row_number]
						FROM tblRuleSetLanguage RSL
						LEFT JOIN tblRuleSet  RS ON RS.idRuleSet = RSL.idRuleSet
						INNER JOIN tblRuleSetToGroupLink RGL ON RS.idRuleSet = RGL.idRuleSet
						INNER JOIN CONTAINSTABLE(tblRuleSetLanguage, *, @searchParam) K ON K.[key] = RSL.idRuleSetLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite
							)
							AND RSL.idLanguage = @idCallerLanguage
							AND RGL.idGroup = @id							
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idRuleSet, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					RS.idRuleSet, 
					RSL.label,
					RS.isAny,
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblRuleSetLanguage RSL ON RSL.idRuleSet = SelectedKeys.idRuleSet AND RSL.idLanguage = @idCallerLanguage
				LEFT JOIN tblRuleSet RS ON RS.idRuleSet = RSL.idRuleSet
				ORDER BY SelectedKeys.[row_number]
								
				END
							
			END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO