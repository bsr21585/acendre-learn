-- =====================================================================
-- PROCEDURE: [Group.RemoveRoles]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.RemoveRoles]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.RemoveRoles]
GO

CREATE PROCEDURE [Group.RemoveRoles]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	--   default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
		
	@idGroup				INT,
	@Roles					IDTable			READONLY
)
AS

	BEGIN
	
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	make sure there are objects in the table parameter
	
	*/
	
	IF (SELECT COUNT(1) FROM @Roles) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'GroupRemoveRoles_NoRolesFound'
		RETURN 1
		END
		
	/*

	validate group exists
	
	*/

	IF (
		SELECT COUNT(1)
		FROM tblGroup
		WHERE idGroup = @idGroup
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'GroupRemoveRoles_DetailsNotFound'
		RETURN 1
		END
		
	/*
	
	remove the objects
	
	*/		

	DELETE FROM tblGroupToRoleLink
	WHERE idGroup = @idGroup
	AND EXISTS (
		SELECT 1
		FROM @Roles RR
		WHERE tblGroupToRoleLink.idRole = RR.id
	)
	
	SELECT @Return_Code = 0

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
