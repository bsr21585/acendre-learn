-- =====================================================================
-- PROCEDURE: [LearningPath.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.Details]
GO

/*

Return all the properties for a given learning path id.

*/

CREATE PROCEDURE [LearningPath.Details]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idLearningPath						INT				OUTPUT,
	@idSite								INT				OUTPUT, 
	@name								NVARCHAR(255)	OUTPUT,
	@shortDescription					NVARCHAR(512)	OUTPUT, 
	@longDescription					NVARCHAR(MAX)	OUTPUT, 
	@cost								FLOAT			OUTPUT,
	@isPublished						BIT				OUTPUT,
	@isClosed							BIT				OUTPUT,
	@selfEnrollmentIsOneTimeOnly		BIT				OUTPUT,
	@forceCompletionInOrder				BIT				OUTPUT,
	@dtCreated							DATETIME		OUTPUT,
	@dtModified							DATETIME		OUTPUT,
	@isDeleted							BIT				OUTPUT,
	@dtDeleted							DATETIME		OUTPUT,
	@avatar								NVARCHAR(255)	OUTPUT,
	@searchTags							NVARCHAR(512)	OUTPUT,
	@isCallingUserEnrolled				BIT				OUTPUT,
	@shortcode							NVARCHAR(10)	OUTPUT,
	@isMultipleSelfEnrollmentEligible	BIT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLearningPath
		WHERE idLearningPath = @idLearningPath
		AND idSite = @idCallerSite
		AND (dtDeleted IS NULL)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LearningPathDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idLearningPath					= LP.idLearningPath,
		@idSite							= LP.idSite,
		@name							= LP.name,
		@shortDescription				= LP.shortDescription,
		@longDescription				= LP.longDescription,
		@cost							= LP.cost,
		@isPublished					= LP.isPublished,
		@isClosed						= LP.isClosed,
		@selfEnrollmentIsOneTimeOnly	= LP.selfEnrollmentIsOneTimeOnly,
		@forceCompletionInOrder			= LP.forceCompletionInOrder,
		@dtCreated						= LP.dtCreated,
		@dtModified						= LP.dtModified,
		@isDeleted						= LP.isDeleted,
		@dtDeleted						= LP.dtDeleted,
		@avatar							= LP.avatar,
		@searchTags						= LP.searchTags,
		@shortcode						= LP.shortcode
	FROM tblLearningPath LP
	WHERE LP.idLearningPath = @idLearningPath

	-- determine if the calling user is currently enrolled in the learning path
	IF (
		SELECT COUNT(1) 
		FROM tblLearningPathEnrollment LPE 
		WHERE LPE.idLearningPath = @idLearningPath
		AND LPE.idUser = @idCaller 
		AND LPE.dtCompleted IS NULL) > 0
		BEGIN
		SET @isCallingUserEnrolled = 1
		END
	ELSE
		BEGIN
		SET @isCallingUserEnrolled = 0
		END

	-- determine if the calling user has ever been enrolled in the learning path and if they can enroll again
	IF (
		SELECT COUNT(1) 
		FROM tblLearningPathEnrollment LPE 
		WHERE LPE.idLearningPath = @idLearningPath
		AND LPE.idUser = @idCaller
		AND LPE.idRuleSetLearningPathEnrollment IS NULL ) > 0
		AND (@selfEnrollmentIsOneTimeOnly <> 0 OR @selfEnrollmentIsOneTimeOnly IS NOT NULL)
		BEGIN
		SET @isMultipleSelfEnrollmentEligible = 0
		END
	ELSE
		BEGIN
		SET @isMultipleSelfEnrollmentEligible = 1
		END
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LearningPathDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO