-- =====================================================================
-- PROCEDURE: [Analytic.GetTitles]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Analytic.GetTitles]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Analytic.GetTitles]
GO

/*

Returns a list of Analytic with its title and assocaited dataset name (language specific)

*/

CREATE PROCEDURE [Analytic.GetTitles]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idDataset				INT				= 0,
	@idUser					INT				= 0,
	@isPublicRequired		BIT				= 0
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*
	
	get list of Analytic titles with AnalyticID 
	
	*/

		
		SELECT DISTINCT
			A.idAnalytic,
			CASE WHEN A.idUser = 1 THEN
				'Administrator'
			ELSE
			CASE WHEN O.idUser is null THEN
				'Unknown' -- should never happen
			ELSE
				O.lastname + ', ' + O.firstname + (CASE WHEN O.middlename IS NOT NULL AND O.middlename <> '' THEN ' ' + O.middlename ELSE '' END)
			END
			END AS ownerName,
			CASE WHEN ADSL.name IS NULL OR ADSL.name = '' THEN ADS.name ELSE ADSL.name END AS dataSet,
			CASE WHEN AL.title IS NULL OR AL.title = '' THEN A.title ELSE AL.title END AS title,
			A.frequency,
			A.idChartType,
			A.dtStart,
			A.dtEnd,
			A.excludeDays
		FROM tblAnalytic A
		LEFT JOIN tblAnalyticLanguage AL 
			ON AL.idAnalytic = A.idAnalytic
			AND AL.idLanguage = @idCallerLanguage
		LEFT JOIN tblAnalyticDataset ADS
			ON ADS.idDataset = A.idDataset
		LEFT JOIN tblUser O
			ON O.idUser = A.idUser
		LEFT JOIN tblAnalyticDatasetLanguage ADSL
			ON ADSL.idDataset = A.idDataset
			AND ADSL.idLanguage = @idCallerLanguage

		WHERE A.idDataset  = CASE WHEN @idDataset > 0  THEN  @idDataset ELSE A.idDataset END  
		  AND A.idUser = CASE WHEN @idUser > 0  THEN  @idUser ELSE A.idUser END  
		  AND A.isPublic = CASE WHEN @isPublicRequired > 0  THEN  1 ELSE A.isPublic END
		  AND A.idUser <> CASE WHEN @isPublicRequired > 0  THEN  @idCaller ELSE 0 END
		  AND (A.isDeleted IS NULL OR A.isDeleted = 0)
		
		SET @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	