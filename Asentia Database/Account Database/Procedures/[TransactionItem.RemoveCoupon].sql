-- =====================================================================
-- PROCEDURE: [TransactionItem.RemoveCoupon]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.RemoveCoupon]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.RemoveCoupon]
GO

/*

Removes applied coupon from a transaction item.

*/


CREATE PROCEDURE [TransactionItem.RemoveCoupon]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified

	@idTransactionItem      INT			
)
AS	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
		
	/*
	
	validate that the transaction item exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblTransactionItem
		WHERE idTransactionItem = @idTransactionItem
		AND idSite = @idCallerSite		
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemRemoveCoupon_NoTI'
		RETURN 1
		END

	/*

	validate that the transaction item has not been confirmed

	*/

	IF (
		SELECT COUNT(1)
		FROM tblTransactionItem
		WHERE idTransactionItem = @idTransactionItem
		AND idSite = @idCallerSite
		AND (confirmed IS NULL OR confirmed = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemRemoveCoupon_CannotRemove'
		RETURN 1
		END
		
	/*
	
	remove the coupon code

	*/

	UPDATE tblTransactionItem SET 
		idCouponCode = NULL,
	    couponCode = NULL,
		paid = cost
	WHERE idTransactionItem = @idTransactionItem
	      

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO