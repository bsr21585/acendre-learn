-- =====================================================================
-- PROCEDURE: [Widget.AdministrativeTasks]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Widget.AdministrativeTasks]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.AdministrativeTasks]
GO

/*

Gets a listing of lesson data for the administrative tasks widget, which includes OJT Proctoring, Task Proctoring,
User Registrations Approval, and Self Enrollment Approval.

*/

CREATE PROCEDURE [Widget.AdministrativeTasks]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	-- NOTE THAT WE ARE NOT USING THE SEARCH, PAGINATION, OR SORT HERE BUT
	-- SINCE THESE ARE COMMON PARAMETERS FOR GRIDS, WE NEED TO KEEP THEM
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	determine if caller has "global" permission to proctor, i.e. is the caller an admin

	if the caller does have permission, we'll skip figuring out what he can proctor and
	just list everything that needs proctoring
	
	*/

	DECLARE @callerHasGlobalPermission BIT
	SET @callerHasGlobalPermission = [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL)

	-- THIS IS TEMPORARY
	IF @idCaller > 1
		BEGIN
		SET @callerHasGlobalPermission = 0
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	if the user does not have "global" permission then get the users the caller is a supervisor 
	of and the courses the caller is an expert for

	*/

	CREATE TABLE #AuthorizedUsers (idUser INT)
	CREATE TABLE #AuthorizedCourses (idCourse INT)

	IF (@callerHasGlobalPermission = 0)
		
		BEGIN
		
		INSERT INTO #AuthorizedUsers (idUser)
		SELECT DISTINCT idUser FROM tblUserToSupervisorLink WHERE idSupervisor = @idCaller

		INSERT INTO #AuthorizedCourses (idCourse)
		SELECT DISTINCT idCourse FROM tblCourseExpert WHERE idUser = @idCaller

		END

	/* 

	ADMINISTRATIVE TASKS TEMP TABLE

	*/
	CREATE TABLE #AdministrativeTasks(
		idAdministrativeTask						INT	IDENTITY(1, 1),
		[type]										NVARCHAR(100),
		task										NVARCHAR(255),
		idEnrollmentRequest							INT,				-- SELF ENROLLMENT
		idEnrollment								INT,				-- OJT / TASK
		idUser										INT,				-- OJT / TASK / USER REGISTRATION / CERTIFICATION
		idCourse									INT,				-- OJT / TASK
		idLesson									INT,				-- OJT / TASK
		[idData-Lesson]								INT,				-- OJT / TASK
		[idData-SCO]								INT,				-- OJT
		idContentPackage							INT,				-- OJT
		courseName									NVARCHAR(255),		-- OJT / TASK
		lessonName									NVARCHAR(255),		-- OJT / TASK / SELF ENROLLMENT
		userDisplayName								NVARCHAR(255),		-- OJT / TASK / USER REGISTRATION / SELF ENROLLMENT / CERTIFICATION
		idContentPackageType						INT,				-- OJT
		contentPackageName							NVARCHAR(255),		-- OJT
		contentPackagePath							NVARCHAR(1024),		-- OJT
		[idData-Task]								INT,				-- TASK
		uploadedTaskFilename						NVARCHAR(255),		-- TASK
		dtUploaded									DATETIME,			-- TASK
		username									NVARCHAR(255),		-- USER REGISTRATION
		email										NVARCHAR(255),		-- USER REGISTRATION
		[idData-CertificationModuleRequirement]		INT,				-- CERTIFICATION
		idCertificationToUserLink					INT,				-- CERTIFICATION
		idCertificationModuleRequirement			INT,				-- CERTIFICATION
		idCertificationModule						INT,				-- CERTIFICATION
		idCertification								INT,				-- CERTIFICATION
		certificationName							NVARCHAR(255),		-- CERTIFICATION
		requirementName								NVARCHAR(255),		-- CERTIFICATION
		completionDocumentationFilePath				NVARCHAR(255),		-- CERTIFICATION
		dtSubmitted									DATETIME			-- CERTIFICATION
	)

	-- get the grid data for task proctor

	INSERT INTO #AdministrativeTasks (
		[type],
		task,
		idEnrollment,
		idUser,
		idCourse,
		idLesson,
		[idData-Lesson],
		[idData-Task],
		courseName,
		lessonName,
		userDisplayName,
		uploadedTaskFilename,
		dtUploaded
	)
	SELECT
		'Task Proctor',
		U.displayName,
		E.idEnrollment,
		U.idUser,
		C.idCourse,
		L.idLesson,
		LD.[idData-Lesson],
		DHWA.[idData-HomeworkAssignment] AS [idData-Task],
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE E.title END,
		CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE LD.title END,
		U.displayName,
		DHWA.uploadedAssignmentFilename AS uploadedTaskFilename,
		DHWA.dtUploaded
	FROM [tblData-Lesson] LD
	LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
	LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson
	LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
	LEFT JOIN tblUser U ON U.idUser = E.idUser
	LEFT JOIN [tblData-HomeworkAssignment] DHWA ON DHWA.[idData-Lesson] = LD.[idData-Lesson]
	WHERE LD.idSite = @idCallerSite
	AND LD.contentTypeCommittedTo = 3														-- committed to task
	AND DHWA.dtUploaded IS NOT NULL															-- user must have uploaded a task
	AND LD.dtCompleted IS NULL																-- lesson data is not complete 
	AND DHWA.proctoringUser IS NULL															-- and has not already been proctored (incomplete and proctored means the learner failed and can retake it)
	AND E.dtCompleted IS NULL																-- enrollment is not complete
	AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)				-- enrollment is not expired
	AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- users must not be deleted or pending
	AND U.isActive = 1																		-- users must be active
	AND (	-- caller is an admin, or caller is not an admin and IS an expert or supervisor for respective objects
			@callerHasGlobalPermission = 1
			OR
			(
				@callerHasGlobalPermission = 0
				AND
				C.idCourse IN (SELECT idCourse FROM #AuthorizedCourses)
			)
			OR
			(
				@callerHasGlobalPermission = 0
				AND
				U.idUser IN (SELECT idUser FROM #AuthorizedUsers)
			)
		)

	
	-- get the OJT Proctor grid data

	INSERT INTO #AdministrativeTasks(
		[type],
		task,
		idEnrollment,
		idUser,
		idCourse,
		idLesson,
		[idData-Lesson],
		[idData-SCO],
		idContentPackage,
		courseName,
		lessonName,
		userDisplayName,
		idContentPackageType,
		contentPackageName,
		contentPackagePath
	)
	SELECT
		'OJT Proctor',
		U.displayName,
		E.idEnrollment,
		U.idUser,
		C.idCourse,
		L.idLesson,
		LD.[idData-Lesson],
		DSCO.[idData-SCO],
		CP.idContentPackage,
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE E.title END,
		CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE LD.title END,
		U.displayName,
		CP.idContentPackageType,
		CP.name,
		CP.[path]
	FROM [tblData-Lesson] LD
	LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
	LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson
	LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
	LEFT JOIN tblLessonToContentLink LCLOJT ON LCLOJT.idLesson = L.idLesson AND LCLOJT.idContentType = 4
	LEFT JOIN tblContentPackage CP ON CP.idContentPackage = LCLOJT.idObject
	LEFT JOIN tblUser U ON U.idUser = E.idUser
	LEFT JOIN [tblData-SCO] DSCO ON DSCO.[idData-Lesson] = LD.[idData-Lesson]
	WHERE LD.idSite = @idCallerSite
	AND LD.contentTypeCommittedTo = 4														-- committed to ojt
	AND LD.dtCompleted IS NULL																-- lesson data is not complete
	AND (DSCO.proctoringUser IS NULL OR DSCO.proctoringUser = @idCaller)					-- not yet proctored or proctoring user is the calling user
	AND E.dtCompleted IS NULL																-- enrollment is not complete
	AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)				-- enrollment is not expired
	AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))	-- users must not be deleted or unapproved
	AND U.isActive = 1																		-- users must be active
	AND (	-- caller is an admin, or caller is not an admin and IS an expert or supervisor for respective objects
			@callerHasGlobalPermission = 1
			OR
			(
				@callerHasGlobalPermission = 0
				AND
				C.idCourse IN (SELECT idCourse FROM #AuthorizedCourses)
			)
			OR
			(
				@callerHasGlobalPermission = 0
				AND
				U.idUser IN (SELECT idUser FROM #AuthorizedUsers)
			)
		)

		-- get the certification task proctoring grid data

	INSERT INTO #AdministrativeTasks (
		[type],
		task,
		[idData-CertificationModuleRequirement],
		idCertificationToUserLink,
		idCertificationModuleRequirement,
		idCertificationModule,
		idCertification,
		idUser,
		certificationName,
		requirementName,
		userDisplayName,
		completionDocumentationFilePath,
		dtSubmitted
	)
	SELECT
		'Certification Task Proctor',
		U.displayName,
		CRD.[idData-CertificationModuleRequirement],
		CRD.idCertificationToUserLink,
		CRD.idCertificationModuleRequirement,
		CM.idCertificationModule,
		C.idCertification,
		U.idUser,
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END,
		CASE WHEN CMRL.label IS NOT NULL THEN CMRL.label ELSE CMR.label END,
		U.displayName,
		CRD.completionDocumentationFilePath,
		CRD.dtSubmitted
	FROM [tblData-CertificationModuleRequirement] CRD
	LEFT JOIN tblCertificationToUserLink CUL ON CUL.idCertificationToUserLink = CRD.idCertificationToUserLink
	LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirement = CRD.idCertificationModuleRequirement
	LEFT JOIN tblCertificationModuleRequirementLanguage CMRL ON CMRL.idCertificationModuleRequirement = CMR.idCertificationModuleRequirement AND CMRL.idLanguage = @idCallerLanguage
	LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModuleRequirementSet = CMR.idCertificationModuleRequirementSet
	LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = CMRS.idCertificationModule
	LEFT JOIN tblCertification C ON C.idCertification = CM.idCertification
	LEFT JOIN tblCertificationLanguage CL ON CL.idCertification = C.idCertification AND CL.idLanguage = @idCallerLanguage	
	LEFT JOIN tblUser U ON U.idUser = CUL.idUser	
	WHERE CRD.idSite = @idCallerSite	
	AND ( -- user must have uploaded a task or it's a non-upload task that requires signoff
			(CMR.documentUploadFileType IS NOT NULL AND CMR.documentationApprovalRequired = 1 AND CRD.dtSubmitted IS NOT NULL)
			OR
			(CMR.documentUploadFileType IS NULL AND CMR.documentationApprovalRequired = 1)
		)
	AND CRD.dtCompleted IS NULL	 -- requirement data is not complete 
	AND CRD.idApprover IS NULL	 -- has not been proctored
	AND ( -- only allow proctoring for initial award or renweal within valid window
			(CUL.certificationTrack = 0 AND CUL.initialAwardDate IS NULL AND CUL.currentExpirationDate IS NULL)
			OR
			(CUL.certificationTrack = 1 AND CUL.lastExpirationDate IS NOT NULL AND GETUTCDATE() >= CUL.lastExpirationDate)
		)
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- users must not be deleted or pending
	AND U.isActive = 1 -- users must be active
	AND (	-- caller is an admin, or caller is not an admin and IS a supervisor for respective objects
			@callerHasGlobalPermission = 1			
			OR
			(
				@callerHasGlobalPermission = 0
				AND
				U.idUser IN (SELECT idUser FROM #AuthorizedUsers)
			)
		)

	-- get the grid data for user registration approval

	INSERT INTO #AdministrativeTasks(
		[type],
		[task],
		idUser,
		userDisplayName,
		username,
		email
	)
	SELECT
		'User Registration Approval',
		U.displayName + ' (' + U.username + ')',
		U.idUser,
		U.displayName,
		U.username,
		U.email
	FROM tblUser U
	WHERE U.idSite = @idCallerSite
	AND U.isRegistrationApproved = 0 
	AND U.dtDenied IS NULL 
	AND U.idApprover IS NULL

	-- get the grid data for SELF ENROLLMENT APPROVAL

	DECLARE @globalPermissions INT

	-- GLOBAL SELF ENROLLMENT PERMISSION

	SELECT 
		@globalPermissions = COUNT(RPL.idRoleToPermissionLink)
	FROM tblRoleToPermissionLink RPL 
	LEFT JOIN tblUserToRoleLink URL ON RPL.idRole = URL.idRole
	WHERE RPL.idPermission = 310
	AND (URL.idUser = @idCaller OR @idCaller = 1)	

	IF @globalPermissions > 0

		BEGIN
		
		--INSERT INTO #SelfEnrollmentApprovals (
		INSERT INTO #AdministrativeTasks(
			[type],
			task,
			idEnrollmentRequest,
			userDisplayName,
			courseName
		)
		SELECT
			'Self Enrollment Approval',
			CASE WHEN CL.title IS NOT NULL THEN U.displayName + ' - ' + CL.title ELSE U.displayName + ' - ' + C.title END,
			ER.idEnrollmentRequest,
			U.displayName,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblEnrollmentRequest ER
		LEFT JOIN tblUser U ON ER.idUser = U.idUser
		LEFT JOIN tblCourse C ON ER.idCourse = C.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		WHERE ER.idSite = @idCallerSite
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
		AND (U.isRegistrationApproved = 1 OR U.isRegistrationApproved IS NULL)
		AND ER.dtApproved IS NULL 
		AND ER.dtDenied IS NULL

		END
	ELSE

		BEGIN

		-- APPROVERS APPROVE
		
		INSERT INTO #AdministrativeTasks(
			[type],
			task,
			idEnrollmentRequest,
			userDisplayName,
			courseName
		)
		SELECT
			'Self Enrollment Approval',
			CASE WHEN CL.title IS NOT NULL THEN U.displayName + ' - ' + CL.title ELSE U.displayName + ' - ' + C.title END,
			ER.idEnrollmentRequest,
			U.displayName,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblEnrollmentRequest ER
		LEFT JOIN tblUser U ON ER.idUser = U.idUser
		LEFT JOIN tblCourse C ON ER.idCourse = C.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCourseEnrollmentApprover CEA ON CEA.idCourse = ER.idCourse
		WHERE ER.idSite = @idCallerSite
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
		AND (U.isRegistrationApproved = 1 OR U.isRegistrationApproved IS NULL)
		AND ER.dtApproved IS NULL 
		AND ER.dtDenied IS NULL 
		AND CEA.idUser = @idCaller
				
		UNION
				
		-- SUPERVISORS APPROVE
	
		SELECT
			'Self Enrollment Approval',
			CASE WHEN CL.title IS NOT NULL THEN U.displayName + ' - ' + CL.title ELSE U.displayName + ' - ' + C.title END,
			ER.idEnrollmentRequest,
			U.displayName,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblEnrollmentRequest ER
		LEFT JOIN tblUser U ON ER.idUser = U.idUser
		LEFT JOIN tblCourse C ON ER.idCourse = C.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblUserToSupervisorLink USL ON USL.idUser = ER.idUser
		WHERE ER.idSite = @idCallerSite
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
		AND (U.isRegistrationApproved = 1 OR U.isRegistrationApproved IS NULL)
		AND ER.dtApproved IS NULL 
		AND ER.dtDenied IS NULL 
		AND C.isApprovalAllowedByUserSupervisor = 1 
		AND USL.idSupervisor = @idCaller

		UNION

		-- EXPERTS APPROVE
	
		SELECT
			'Self Enrollment Approval',
			CASE WHEN CL.title IS NOT NULL THEN U.displayName + ' - ' + CL.title ELSE U.displayName + ' - ' + C.title END,
			ER.idEnrollmentRequest,
			U.displayName,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblEnrollmentRequest ER
		LEFT JOIN tblUser U ON ER.idUser = U.idUser
		LEFT JOIN tblCourse C ON ER.idCourse = C.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCourseExpert CE ON CE.idCourse = ER.idCourse
		WHERE ER.idSite = @idCallerSite
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
		AND (U.isRegistrationApproved = 1 OR U.isRegistrationApproved IS NULL)
		AND ER.dtApproved IS NULL 
		AND ER.dtDenied IS NULL 
		AND C.isApprovalAllowedByCourseExperts = 1 
		AND CE.idUser = @idCaller
		
		END

	SELECT COUNT(1) AS row_count
	FROM #AdministrativeTasks

	;WITH
		Keys AS (
			SELECT TOP (@pageSize * @pageSize)
				AT.idAdministrativeTask,
				ROW_NUMBER() OVER (ORDER BY AT.idAdministrativeTask)
				AS [row_number]
			FROM #AdministrativeTasks AT
		),

		SelectedKeys AS (
			SELECT --TOP (@pageSize) ; this is unnecessary but lets leave it for now
				idAdministrativeTask,
				[row_number]
			FROM Keys
			where Keys.[row_number] > ((@pageNum - 1) * @pageSize)
		)
	
	SELECT DISTINCT
		AT.idAdministrativeTask,
		AT.[type],
		AT.task,
		AT.idEnrollmentRequest,
		AT.idEnrollment,
		AT.idUser,
		AT.idCourse,
		AT.idLesson,
		AT.[idData-Lesson],
		AT.[idData-SCO],
		AT.idContentPackage,
		AT.courseName,
		AT.lessonName,
		AT.userDisplayName,
		AT.idContentPackageType,
		AT.contentPackageName,
		AT.contentPackagePath,
		AT.[idData-Task],
		AT.uploadedTaskFilename,
		AT.dtUploaded,
		AT.username,
		AT.email,
		AT.[idData-CertificationModuleRequirement],	
		AT.idCertificationToUserLink,
		AT.idCertificationModuleRequirement,
		AT.idCertificationModule,
		AT.idCertification,
		AT.certificationName,
		AT.requirementName,
		AT.completionDocumentationFilePath,
		AT.dtSubmitted,
		SelectedKeys.[row_number]
	FROM SelectedKeys
	JOIN #AdministrativeTasks AT ON AT.idAdministrativeTask = SelectedKeys.idAdministrativeTask
	ORDER BY SelectedKeys.[row_number]

	-- drop temporary tables
	DROP TABLE #AuthorizedCourses
	DROP TABLE #AuthorizedUsers
	DROP TABLE #AdministrativeTasks

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO