-- =====================================================================
-- PROCEDURE: [Dataset.UserCourseTranscripts]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Dataset.UserCourseTranscripts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Dataset.UserCourseTranscripts]
GO

/*

User Transcripts Dataset

*/

CREATE PROCEDURE [dbo].[Dataset.UserCourseTranscripts]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@fields						NVARCHAR(MAX),
	@whereClause				NVARCHAR(MAX),
	@orderByClause				NVARCHAR(768),
	@maxRecords					INT,
	@logReportExecution			BIT
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*

	declare and assign local variables to prevent "parameter sniffing"

	*/

	DECLARE @fieldsLOC					NVARCHAR(MAX)
	DECLARE @whereClauseLOC				NVARCHAR(MAX)
	DECLARE @orderByClauseLOC			NVARCHAR(768)
	DECLARE @maxRecordsLOC				INT
	DECLARE @logReportExecutionLOC		BIT

	SET @fieldsLOC				= @fields
	SET @whereClauseLOC			= @whereClause
	SET @orderByClauseLOC		= @orderByClause
	SET @maxRecordsLOC			= @maxRecords
	SET @logReportExecutionLOC	= @logReportExecution

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
		if no fields defined, select * (all)

	*/

	IF @fieldsLOC IS NULL OR @fieldsLOC = ''
		BEGIN
		SET @fieldsLOC = '*'
		END

	/*

	get the id of the caller's language

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage =
		CASE WHEN idLanguage IS NULL THEN 
			57
		ELSE 
			idLanguage 
		END
		FROM tblLanguage WHERE code = @callerLangString

	/*

	get the group scope so we can filter on what the calling user is allowed to see
	note that NULL scope means the caller can see anything, and idCaller 1 automatically has null scope and permission to anything
	
	*/

	DECLARE @idDatasetPermission INT
	SET @idDatasetPermission = 402

	DECLARE @groupScope NVARCHAR(MAX)
	SET @groupScope = NULL

	IF (@idCaller > 1)
		BEGIN 

		CREATE TABLE #CallerRoles (idRole INT)
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT URL.idRole FROM tblUserToRoleLink URL WHERE URL.idUser = @idCaller -- directly assigned roles
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT GRL.idRole FROM tblGroupToRoleLink GRL -- group inherited roles
									      WHERE GRL.idGroup IN (SELECT DISTINCT UGL.idGroup FROM tblUserToGroupLink UGL WHERE UGL.idUser = @idCaller)
										  AND NOT EXISTS (SELECT 1 FROM #CallerRoles CR WHERE CR.idRole = GRL.idRole)
		
		-- if the caller does not have any permissions to this dataset with NULL scope, get the defined scopes
		IF (
		    SELECT COUNT(1) FROM tblRoleToPermissionLink RPL 
		    WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles)
			AND RPL.idPermission = @idDatasetPermission
			AND RPL.scope IS NULL
		   ) = 0
			BEGIN

			-- get all scope items into the group scope variable			
			DECLARE @scopeItems NVARCHAR(MAX)
			SET @groupScope = ''

			DECLARE scopeListCursor CURSOR LOCAL FOR
				SELECT scope FROM tblRoleToPermissionLink RPL WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles) AND RPL.idPermission = @idDatasetPermission AND RPL.scope IS NOT NULL

			OPEN scopeListCursor

			FETCH NEXT FROM scopeListCursor INTO @scopeItems

			WHILE @@FETCH_STATUS = 0
				BEGIN

				IF (@scopeItems <> '')
					BEGIN
					IF (@groupScope <> '')
						BEGIN
						SET @groupScope = @groupScope + ',' + @scopeItems
						END
					ELSE
						BEGIN
						SET @groupScope = @scopeItems
						END
						
					END

				FETCH NEXT FROM scopeListCursor INTO @scopeItems

				END

			-- kill the cursor
			CLOSE scopeListCursor
			DEALLOCATE scopeListCursor

			END

		END

	/*

		build sql query

	*/

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCont NVARCHAR(MAX)

	SET @sql = 'SELECT DISTINCT '
				+ CASE WHEN (@maxRecordsLOC IS NOT NULL AND @maxRecordsLOC > 0) THEN + ' TOP ' + convert(nvarchar, @maxRecordsLOC) + ' ' ELSE '' END
    SET @sql = @sql +  @fieldsLOC + ',_idUser'

	-- if one of the requested fields is "Interaction", get the supporting data of lesson data id and sco identifier
	IF (CHARINDEX('[Interaction', @fieldsLOC) > 0)
		BEGIN
		SET @sql = @sql + ',CASE WHEN [Interaction] IS NOT NULL THEN _idDataLesson ELSE NULL END AS _idDataLesson'
		SET @sql = @sql + ',CASE WHEN [Interaction] IS NOT NULL THEN _scoIdentifier ELSE NULL END AS _scoIdentifier'
		END

    SET @sql = @sql +  ' FROM ('
				+ 'SELECT DISTINCT '
				+ '	U.idSite as _idSite,  '
				+ ' U.idUser as _idUser,  '
				+ '	U.lastname + '', '' + U.firstName + (case when U.middlename is not null then '' '' + U.middlename else '''' end) as [Full Name],  '
				+ '	U.lastname as [##lastName##],  '
				+ '	U.middleName as [##middleName##], '
				+ '	U.firstname as [##firstName##],  '
				+ '	U.email as [##email##],  '
				+ ' U.username as [##username##],  '
				+ ' U.jobTitle as [##jobTitle##],  '
				+ '	U.jobClass as [##jobClass##],  '
				+ '	U.company as [##company##],  '
				+ '	replace(replace(U.address, CHAR(10), '' ''), CHAR(13), '' '') as [##address##], '
				+ '	U.city as [##city##],  '
				+ '	U.province as [##province##],  '
				+ '	U.postalcode as [##postalcode##],  '
				+ '	U.country as [##country##], '
				+ '	U.phonePrimary as [##phonePrimary##], ' 
				+ '	U.phoneWork as [##phoneWork##],  '
				+ '	U.phoneHome as [##phoneHome##],  '
				+ '	U.phoneFax as [##phoneFax##],  '
				+ '	U.phoneMobile as [##phoneMobile##], '
				+ '	U.phonePager as [##phonePager##],  '
				+ '	U.phoneOther as [##phoneOther##],  '
				+ '	U.division as [##division##],  '
				+ '	U.department as [##department##],  '
           --+ '	SUPER.lastname + ', ' + SUPER.firstName + (case when SUPER.middlename is not null then ' ' + SUPER.middlename else '' end) as [##supervisor##],
				+ '	U.region as [##region##],  '
				+ '	U.employeeID as [##employeeID##], '
				+ '	U.dtHire as [##dtHire##], '
				+ '	U.dtTerm as [##dtTerm##], '
				+ '	U.gender as [##gender##], '
				+ '	U.race as [##race##], '
				+ '	U.dtDOB as [##dtDOB##], '
				+ ' CONVERT(BIT, U.isActive) as [##isActive##],	'
				+ ' cast (U.isActive as int) as [_sb##isactive##],  '
				+ ' U.dtCreated as [##dtCreated##], '
				+ ' U.dtExpires as [##dtExpires##], '
				+ ' U.dtLastLogin as [##dtLastLogin##], '
				+ '	L.idLanguage AS [##idLanguage##], '
				+ '	L.code AS [_sb##languageCode##], '
				+ '	U.idTimezone AS [##idTimezone##], '

				-- if "Group" fields are in the selected fields or filters, select tblUserToGroupLink fields
				IF (CHARINDEX('[Group]', @fieldsLOC) > 0 OR CHARINDEX('[Group]', @whereClauseLOC) > 0)
					BEGIN
					SET @sql = @sql + '	CASE WHEN GL.[name] IS NOT NULL THEN GL.[name] ELSE G.[name] END AS [Group], '
					+ '	UGL.idGroup as _sbGroup, ' -- hidden field from reports; used to filter only users that the report-runner is permitted to see
					END

				SET @sql = @sql + '	SUPER.displayName AS [Supervisor], '
				+ '	SUPER.idUser AS _idSupervisor, '
				+ '	U.field00 AS [##field00##], '
				+ '	U.field01 AS [##field01##], '
				+ '	U.field02 AS [##field02##], '
				+ '	U.field03 AS [##field03##], '
				+ '	U.field04 AS [##field04##], '
				+ '	U.field05 AS [##field05##], '
				+ '	U.field06 AS [##field06##], '
				+ '	U.field07 AS [##field07##], '
				+ '	U.field08 AS [##field08##], '
				+ '	U.field09 AS [##field09##], '
				+ '	U.field10 AS [##field10##], '
				+ '	U.field11 AS [##field11##], '
				+ '	U.field12 AS [##field12##], '
				+ '	U.field13 AS [##field13##], '
				+ '	U.field14 AS [##field14##], '
				+ '	U.field15 AS [##field15##], '
				+ '	U.field16 AS [##field16##], '
				+ '	U.field17 AS [##field17##], '
				+ '	U.field18 AS [##field18##], '
				+ '	U.field19 AS [##field19##], '
				+ '	E.[code] as [Code], '
				+ '	case when CL.title is not null then '
				+ '		CL.title + '' (#'' + cast(E.idEnrollment as varchar(10)) + '')'' '
				+ '	else '
				+ '		case when C.title is not null then '
				+ '			C.[title] + '' (#'' + cast(E.idEnrollment as varchar(10)) + '')'' '
				+ '		else '
				+ '			E.[title] + '' (#'' + cast(E.idEnrollment as varchar(10)) + '')'' '
				+ '		end '
				+ '	end as [Course], '
				+ '	C.[title] + ''_'' + cast(E.idEnrollment as varchar(10)) + ''_'' + case when DL.[order] < 10 then ''0'' + cast(DL.[order] as varchar(10)) else cast(DL.[order] as varchar(10)) end as [_order_Course], '
				+ '	case when E.[idCourse] is null or E.[idCourse] = 0 then '
				+ '		E.credits '
				+ '	else '
				+ '		case when E.credits is not null then '
				+ '			E.credits '
				+ '		else '
				+ '			C.credits '
				+ '		end '
				+ '	end as [Credits], '
				-- + 'E.idCourse as _idCourse, 
				+ '	E.dtStart as [Enroll Date], '
				+ '	case when E.idEnrollment is null then  '
				+ '		null  '
				+ '	else '
				+ '		case when E.dtCompleted is not null then '
				+ '			''completed'' '
				+ '		else	 '
				+ '			case when (E.dtExpiresFromStart is not null and E.dtExpiresFromStart <= getutcdate()) OR (E.dtExpiresFromFirstLaunch IS NOT NULL AND E.dtExpiresFromFirstLaunch <= GETUTCDATE()) then '
				+ '				 ''expired'' '
				+ '			else '
				+ '				case when E.dtDue is not null and E.dtDue <= getutcdate() then '
				+ '					''overdue'' '
				+ '				else '
				+ '					case when E.dtStart > getutcdate() then '
				+ '						''future'' '
				+ '					else '
				+ '						''enrolled'' '
				+ '					end '
				+ '				end '
				+ '			end '
				+ '		end '
				+ '	end as [Course Status], ' 
				+ '	case when E.idEnrollment is null then '
				+ '		null '
				+ '	else '
				+ '		case when E.dtCompleted is not null then '
				+ '			1 '
				+ '		else	'
				+ '			case when (E.dtExpiresFromStart is not null and E.dtExpiresFromStart <= getutcdate()) OR (E.dtExpiresFromFirstLaunch IS NOT NULL AND E.dtExpiresFromFirstLaunch <= GETUTCDATE()) then '
				+ '				2 '
				+ '			else '
				+ '				case when E.dtDue is not null and E.dtDue <= getutcdate() then '
				+ '					3 '
				+ '				else '
				+ '					case when E.dtStart > getutcdate() then '
				+ '						4 '
				+ '					else '
				+ '						5 '
				+ '					end '
				+ '				end '
				+ '			end '
				+ '		end  '
				+ '	end as [_sbCourse Status],  '
				+ ' case when E.dtExpiresFromStart is null AND E.dtExpiresFromFirstLaunch is null then '
				+ '		null '
				+ ' else '
				+ '		case when E.dtExpiresFromStart is null AND E.dtExpiresFromFirstLaunch is not null then '
				+ '			E.dtExpiresFromFirstLaunch '
				+ '		else '
				+ '			case when E.dtExpiresFromStart is not null AND E.dtExpiresFromFirstLaunch is null then '
				+ '				E.dtExpiresFromStart '
				+ '			else '
				+ '				case when E.dtExpiresFromStart >= E.dtExpiresFromFirstLaunch then '
				+ '					E.dtExpiresFromFirstLaunch '
				+ '				else '
				+ '					E.dtExpiresFromStart '
				+ '				end '
				+ '			end '
				+ '		end '
				+ '	end AS [Date End], '
				+ '	E.dtDue as [Date Due],	 '
				+ '	E.dtCompleted as [Date Completed],	 '
				+ '	case when LL.title is not null then '
				+ '			LL.title '
				+ '	else '
				+ '		case when LN.title is not null then '
				+ '			LN.title '
				+ '		else '
				+ '			DL.title '
				+ '		end '
				+ '	end as [Lesson], '
				+ ' (SELECT MIN(dtStart) FROM tblStandupTrainingInstanceMeetingTime WHERE idStandupTrainingInstance = STIUL.idStandupTrainingInstance) as [Session Date/Time], '
				+ '	case when DS.[idData-SCO] is null then  '
				+ '		null '
				+ '	else  '
				+ '		DL.title + '' / '' + DS.manifestIdentifier  '
				+ '	end as [Content Resource], '   -- SQL won't allow the string go beyond 4000 characters in buffer
				-- + 'LD.idLesson as _idLesson, 
   SET @sql = @sql + ' case when (DS.completionStatus is null or DS.completionStatus = 0) '
				+ '				and (STIUL.completionStatus is null or STIUL.completionStatus = 0) '
				+ '		        and (DHWA.completionStatus is null or DHWA.completionStatus = 0) then '
				+ '				''unknown'' '
				+ '		else '
				+ '			case when DS.completionStatus = 2 then '
				+ '				''completed'' '
				+ '			else '
				+ '				case when STIUL.completionStatus = 2 then '
				+ '					''completed'' '
				+ '				else '
				+ '					case when DHWA.completionStatus = 2 then '
				+ '						''completed'' '
				+ '					else '
				+ '						case when DS.completionStatus = 3 then '
				+ '							''incomplete'' '
				+ '						else '
				+ '							case when STIUL.completionStatus = 3 then '
				+ '								''incomplete'' '
				+ '							else '
				+ '								case when DHWA.completionStatus = 3 then '
				+ '									''incomplete'' '
				+ '								else '
				+ '									''unknown'' '
				+ '								end '
				+ '							end '
				+ '						end '
				+ '					end '
				+ '				end '
				+ '			end '
				+ '	end as [Lesson Completion], '
				-- + 'LD.progressMeasure as [Progress Measure],
				+ '	case when (DS.successStatus is null or DS.successStatus = 0) ' 
				+ '				and (STIUL.successStatus is null or STIUL.successStatus = 0) '
				+ '				and (DHWA.successStatus is null or DHWA.successStatus = 0) then '
				+ '					''unknown'' '
				+ '		else '
				+ '			case when DS.successStatus = 4 then '
				+ '				''passed'' '
				+ '			else '
				+ '				case when STIUL.successStatus = 4 then '
				+ '					''passed'' '
				+ '				else '
				+ '					case when DHWA.successStatus = 4 then '
				+ '						''passed'' '
				+ '					else '
				+ '						case when DS.successStatus = 5 then '
				+ '							''failed'' '
				+ '						else '
				+ '							case when STIUL.successStatus = 5 then '
				+ '								''failed'' '
				+ '							else '
				+ '								case when DHWA.successStatus = 5 then '
				+ '									''failed'' '
				+ '								else '
				+ '									''unknown'' '
				+ '								end '
				+ '							end '
				+ '						end '
				+ '					end '
				+ '				end '
				+ '			end '
				+ ' end as [Lesson Success], '
				+ ' DL.[idData-Lesson] as [_idDataLesson], '
				+ ' case when (DS.completionStatus is null or DS.completionStatus = 0) '
				+ '	  and (STIUL.completionStatus is null or STIUL.completionStatus = 0) '
				+ '	  and (DHWA.completionStatus is null or DHWA.completionStatus = 0) then '
				+ '	  				null '
				+ '	  else '
				+ '	  		case when DS.completionStatus = 2 then '
				+ '	  			case when DS.scoreScaled is not null then '
				+ '	  				round(DS.scoreScaled * 100, 0) '
				+ '	  			else '
				+ '	  				null '
				+ '	  			end '
				+ '	  		else '
				+ '	  			case when STIUL.completionStatus = 2 then '
				+ '	  				case when STIUL.score is not null then '
				+ '	  					round(STIUL.score, 0) '
				+ '	  				else '
				+ '	  					null '
				+ '	  				end '
				+ '	  			else '
				+ '	  				case when DHWA.completionStatus = 2 then '
				+ '	  					case when DHWA.score is not null then '
				+ '	  						round(DHWA.score, 0) '
				+ '	  					else '
				+ '	  						null '
				+ '	  					end '
				+ '	  				else '
				+ '	  					case when DS.completionStatus = 3 then '
				+ '	  						case WHEN DS.scoreScaled is not null then '
				+ '	  							round(DS.scoreScaled * 100, 0) '
				+ '	  						else '
				+ '	  							null '
				+ '	  						end '
				+ '	  					else '
				+ '	  						case when STIUL.completionStatus = 3 then '
				+ '	  							case when STIUL.score is not null then '
				+ '	  								round(STIUL.score, 0) '
				+ '	  							else '
				+ '	  								null '
				+ '	  							end '
				+ '	  						else '
				+ '	  							case when DHWA.completionStatus = 3 then '
				+ '	  								case when DHWA.score is not null then '
				+ '	  									round(DHWA.score, 0) '
				+ '	  								else '
				+ '	  									null '
				+ '	  								end '
				+ '	  							else '
				+ '	  								null '
				+ '	  							end '
				+ '	  						end '
				+ '	  					end '
				+ '	  				end '
				+ '	  			end '
				+ '	  		end '
			    + '	end as [Score], '
				+ '	DS.[timestamp] as [Lesson Timestamp],  '
				+ '	cast(round(DS.totalTime, 2) as numeric(36, 2)) as [Lesson Time], '
				+ ' convert(char(8), dateadd(second, DS.totalTime, 0), 108) as [Lesson Time Minutes], '
				+ '	case when DS.actualAttemptCount = 0 then null else DS.actualAttemptCount end as [Attempts], '
				
				-- if "Interaction" fields are in the selected fields or filters, select tblData-SCO fields
				IF (CHARINDEX('[Interaction', @fieldsLOC) > 0 OR CHARINDEX('[Result]', @fieldsLOC) > 0 OR CHARINDEX('[Interaction', @whereClauseLOC) > 0 OR CHARINDEX('[Result]', @whereClauseLOC) > 0)
					BEGIN
					SET @sql = @sql + '	SI.identifier as [Interaction],  '
									+ '	SI.scoIdentifier as [_scoIdentifier],  '
									+ ' null as [Interaction Description],  '
									+ ' null as [Interaction Type],  '
									+ '	SI.timestamp as [Interaction Timestamp],  '
									+ '	SI.latency as [Interaction Time], '
									+ ' null as [Learner Response],  '
									+ ' null as [Correct Responses],  '
									+ '	RS.value as [Result],  '
					END

				SET @sql = @sql + '	SO.identifier as [Objective],  '
				+ '	case when SO.[idData-SCOObj] is null then  '
				+ '		null '
				+ '	else '
				+ '		case when SO.scoreScaled is not null then round(SO.scoreScaled * 100, 0) end '					
				+ '	end	as [Objective Score], '
				+ '	case when (SO.completionStatus is null or SO.completionStatus = 0) then '
				+ '			''unknown'' '
				+ '	when SO.completionStatus = 2 then '
				+ '			''completed'' '
				+ '	when SO.completionStatus = 3 then '
				+ '			''incomplete'' '
				+ '		else '
				+ '			''unknown'' '
				+ '	end as [Objective Completion Status], '
				+ '	case when (SO.successStatus is null or SO.successStatus = 0) then '
				+ '			''unknown'' '
				+ '	when SO.successStatus = 4 then '
				+ '			''passed'' '
				+ '	when SO.successStatus = 5 then '
				+ '			''failed'' '
				+ '		else '
				+ '			''unknown'' '
				+ '	end as [Objective Success Status], '
				+ '	CONVERT(NVARCHAR, DL.contentTypeCommittedTo) as [Committed Content Type]  '
				+ 'FROM tblUser U '

				-- if "Group" fields are in the selected fields or filters, or there is group scope, join to group tables
				IF (CHARINDEX('[Group]', @fieldsLOC) > 0 OR CHARINDEX('[Group]', @whereClauseLOC) > 0 OR (CHARINDEX('_idUser =', @whereClauseLOC) <= 0 AND @groupScope IS NOT NULL AND @groupScope <> ''))				
					BEGIN
					SET @sql = @sql + 'LEFT JOIN tblUserToGroupLink UGL on UGL.idUser = U.idUser '
					+ 'LEFT JOIN tblGroup G on UGL.idGroup = G.idGroup '
					+ 'LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
					END
				
				SET @sql = @sql + 'LEFT JOIN tblLanguage L on L.idLanguage = U.idLanguage '
				+ 'LEFT JOIN tblUserToSupervisorLink USL ON USL.idUser = U.idUser '
                + 'LEFT JOIN tblUser SUPER ON USL.idSupervisor = SUPER.idUser '
				+ 'LEFT JOIN tblEnrollment E on E.idUser = U.idUser '
				+ 'LEFT JOIN [tblData-Lesson] DL on DL.idEnrollment = E.idEnrollment '				
				+ 'LEFT JOIN [tblData-SCO] DS on DS.[idData-Lesson] = DL.[idData-Lesson] '
				+ 'LEFT JOIN [tblData-HomeworkAssignment] DHWA ON DHWA.[idData-Lesson] = DL.[idData-Lesson] '
				+ 'LEFT JOIN tblCourse C on C.idCourse = E.idCourse '
				+ 'LEFT JOIN tblCourseLanguage CL on CL.idCourse = C.idCourse AND CL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
				+ 'LEFT JOIN tblLesson LN on LN.idLesson = DL.idLesson '
				+ 'LEFT JOIN tblLessonLanguage LL on LL.idLesson = LN.idLesson AND LL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
				+ 'LEFT JOIN [tblLessonToContentLink] LCLST ON LCLST.idLesson = LN.idLesson AND LCLST.idContentType = 2 '
				+ 'LEFT JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idStandupTrainingInstance = ('
				+ '																				SELECT TOP 1 STIUL2.idStandupTrainingInstance '
				+ '																				FROM tblStandUpTrainingInstanceToUserLink STIUL2 '
				+ '																				LEFT JOIN tblStandUpTrainingInstance STI2 ON STIUL2.idStandupTrainingInstance = STI2.idStandUpTrainingInstance'
				+ '																				LEFT JOIN (SELECT idStandupTrainingInstance, MIN(dtStart) AS dtStart FROM tblStandUpTrainingInstanceMeetingTime GROUP BY idStandUpTrainingInstance) STIMT ON STIMT.idStandUpTrainingInstance = STIUL2.idStandUpTrainingInstance'
				+ '																				WHERE STI2.idStandupTraining = LCLST.idObject '
				+ '																				AND STIUL2.idUser = E.idUser'
				--																				where the standup training instance start
				--																				is within 30 days prior to the enrollment start
				+ '																				AND STIMT.dtStart >= DATEADD(d, -30, E.dtStart)'
				+ '																			   ) '
				+ '																			   AND STIUL.idUser = E.idUser '

				-- if "Interaction" fields are in the selected fields or filters, join to tblData-SCOInt
				IF (CHARINDEX('[Interaction', @fieldsLOC) > 0 OR CHARINDEX('[Result]', @fieldsLOC) > 0 OR CHARINDEX('[Interaction', @whereClauseLOC) > 0 OR CHARINDEX('[Result]', @whereClauseLOC) > 0)
					BEGIN
					SET @sql = @sql + 'LEFT JOIN [tblData-SCOInt] SI on SI.[idData-SCO] = DS.[idData-SCO] '
									+ 'LEFT JOIN [tblSCORMVocabulary] RS on RS.idSCORMVocabulary = SI.result '
					END
				
				SET @sql = @sql + 'LEFT JOIN [tblData-SCOObj] SO on SO.[idData-SCO] = DS.[idData-SCO] '
				+ 'WHERE (E.dtStart <= getutcdate() OR E.idEnrollment IS NULL)  '-- this is required so that we don't filter out non-enrolled users (required so that we can find users that have NO enrollments). 
				+ ' AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) '
				+ ' AND U.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '
				
				IF (CHARINDEX('_idUser =', @whereClauseLOC) <= 0 AND @groupScope IS NOT NULL AND @groupScope <> '')
					BEGIN
					SET @sql = @sql + 'AND UGL.idgroup IN (' + @groupScope + ') '
					END
							
				SET @sql = @sql + ') MAIN'    

	IF @whereClauseLOC IS NOT NULL AND @whereClauseLOC <> ''
		BEGIN
		SET @sql = @sql + ' WHERE ' + @whereClauseLOC
		END

	IF @orderByClauseLOC IS NOT NULL AND @orderByClauseLOC <> ''
		BEGIN
		
		-- for security reasons, we will always pass @orderByClauseLOC with a trailing comma, remove that comma
		-- this will prevent another sql statement from being attached to this query
		SET @orderByClauseLOC = LEFT(@orderByClauseLOC, LEN(@orderByClauseLOC) - 1)

		SET @sql = @sql + ' ORDER BY ' + @orderByClauseLOC

		END

	/*

	execute the sql statement and log its execution

	*/

	DECLARE @dtExecutionBegin DATETIME
	DECLARE @dtExecutionEnd DATETIME
	DECLARE @executionDurationMS INT

	SET @dtExecutionBegin = GETUTCDATE()

	EXEC sp_executesql @sql

	SET @dtExecutionEnd = GETUTCDATE()

	SET @executionDurationMS = DATEDIFF(ms, @dtExecutionBegin, @dtExecutionEnd)

	IF (@logReportExecutionLOC = 1)
	BEGIN
		INSERT INTO tblReportProcessorLog
		(
			idSite,
			idCaller,
			datasetProcedure,
			dtExecutionBegin,
			dtExecutionEnd,
			executionDurationSeconds,
			sqlQuery
		)
		SELECT
			@idCallerSite,
			@idCaller,
			'[Dataset.UserCourseTranscripts]',
			@dtExecutionBegin,
			@dtExecutionEnd,
			CAST((CAST(@executionDurationMS AS DECIMAL)/1000) AS DECIMAL(9,2)),
			@sql	
	END

	SET @Return_Code = 0

	END
	


GO