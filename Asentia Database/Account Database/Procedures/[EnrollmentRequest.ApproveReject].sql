-- =====================================================================
-- PROCEDURE: [EnrollmentRequest.ApproveReject]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EnrollmentRequest.ApproveReject]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EnrollmentRequest.ApproveReject]
GO

/*

Approves or rejects a user's pending course enrollment.

*/

CREATE PROCEDURE [EnrollmentRequest.ApproveReject]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idEnrollmentRequest		INT,
	@isEnrollmentApproved		BIT,			--1 for approved, 0 for rejected
	@idApprover					INT,
	@rejectionComments			NVARCHAR(MAX)	= NULL
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	declare utc now

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	update the enrollment request with approval or rejection info

	*/

	UPDATE tblEnrollmentRequest SET
		dtApproved = CASE WHEN @isEnrollmentApproved = 1 THEN @utcNow ELSE NULL END,
		dtDenied = CASE WHEN @isEnrollmentApproved = 0 THEN @utcNow ELSE NULL END,
		idApprover = @idApprover,
		rejectionComments = @rejectionComments
	WHERE idEnrollmentRequest = @idEnrollmentRequest
	AND idSite = @idCallerSite

	/*

	do the event log entry

	*/
	
	DECLARE @eventLogItems EventLogItemObjects
	DECLARE @idEvent INT
	DECLARE @idUser INT
	DECLARE @idCourse INT

	IF @isEnrollmentApproved = 1
		SET @idEvent = 207
	ELSE
		SET @idEvent = 208
	
	SELECT @idUser = idUser, @idCourse = idCourse FROM tblEnrollmentRequest WHERE idEnrollmentRequest = @idEnrollmentRequest AND idSite = @idCallerSite	

	INSERT INTO @eventLogItems (idSite, idObject, idObjectRelated, idObjectUser) VALUES (@idCallerSite, @idCourse, @idEnrollmentRequest, @idUser)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, @idEvent, @utcNow, @eventLogItems
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO