-- =====================================================================
-- PROCEDURE: [Dataset.TinCan]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Dataset.TinCan]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Dataset.TinCan]
GO

/*

TinCan Dataset

*/

CREATE PROCEDURE [Dataset.TinCan]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@fields						NVARCHAR(MAX),
	@whereClause				NVARCHAR(MAX),
	@orderByClause				NVARCHAR(768),
	@maxRecords					INT,
	@logReportExecution			BIT
)
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*

	declare and assign local variables to prevent "parameter sniffing"

	*/

	DECLARE @fieldsLOC					NVARCHAR(MAX)
	DECLARE @whereClauseLOC				NVARCHAR(MAX)
	DECLARE @orderByClauseLOC			NVARCHAR(768)
	DECLARE @maxRecordsLOC				INT
	DECLARE @logReportExecutionLOC		BIT

	SET @fieldsLOC				= @fields
	SET @whereClauseLOC			= @whereClause
	SET @orderByClauseLOC		= @orderByClause
	SET @maxRecordsLOC			= @maxRecords
	SET @logReportExecutionLOC	= @logReportExecution

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the id of the caller's language

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage =
		CASE WHEN idLanguage IS NULL THEN 
			57
		ELSE 
			idLanguage 
		END
		FROM tblLanguage WHERE code = @callerLangString

	/*

	get the group scope so we can filter on what the calling user is allowed to see
	note that NULL scope means the caller can see anything, and idCaller 1 automatically has null scope and permission to anything
	
	*/

	DECLARE @idDatasetPermission INT
	SET @idDatasetPermission = 405

	DECLARE @groupScope NVARCHAR(MAX)
	SET @groupScope = NULL

	IF (@idCaller > 1)
		BEGIN 

		CREATE TABLE #CallerRoles (idRole INT)
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT URL.idRole FROM tblUserToRoleLink URL WHERE URL.idUser = @idCaller -- directly assigned roles
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT GRL.idRole FROM tblGroupToRoleLink GRL -- group inherited roles
									      WHERE GRL.idGroup IN (SELECT DISTINCT UGL.idGroup FROM tblUserToGroupLink UGL WHERE UGL.idUser = @idCaller)
										  AND NOT EXISTS (SELECT 1 FROM #CallerRoles CR WHERE CR.idRole = GRL.idRole)
		
		-- if the caller does not have any permissions to this dataset with NULL scope, get the defined scopes
		IF (
		    SELECT COUNT(1) FROM tblRoleToPermissionLink RPL 
		    WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles)
			AND RPL.idPermission = @idDatasetPermission
			AND RPL.scope IS NULL
		   ) = 0
			BEGIN

			-- get all scope items into the group scope variable			
			DECLARE @scopeItems NVARCHAR(MAX)
			SET @groupScope = ''

			DECLARE scopeListCursor CURSOR LOCAL FOR
				SELECT scope FROM tblRoleToPermissionLink RPL WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles) AND RPL.idPermission = @idDatasetPermission AND RPL.scope IS NOT NULL

			OPEN scopeListCursor

			FETCH NEXT FROM scopeListCursor INTO @scopeItems

			WHILE @@FETCH_STATUS = 0
				BEGIN

				IF (@scopeItems <> '')
					BEGIN
					IF (@groupScope <> '')
						BEGIN
						SET @groupScope = @groupScope + ',' + @scopeItems
						END
					ELSE
						BEGIN
						SET @groupScope = @scopeItems
						END
						
					END

				FETCH NEXT FROM scopeListCursor INTO @scopeItems

				END

			-- kill the cursor
			CLOSE scopeListCursor
			DEALLOCATE scopeListCursor

			END

		END

		/*

		build sql query

		*/

		DECLARE @sql NVARCHAR(MAX)
		    --Where atleast one course is selected
			SET @sql = 'SELECT DISTINCT '
			+ CASE WHEN (@maxRecordsLOC IS NOT NULL AND @maxRecordsLOC > 0) THEN + ' TOP ' + convert(nvarchar, @maxRecordsLOC) + ' ' ELSE '' END
			SET @sql = @sql +  @fieldsLOC
			SET @sql = @sql +  ' FROM ('
							+ ' SELECT  '
			   				+ 'T.idSite as _idSite, '
							+ 'T.[idData-TinCan] as [_idData-TinCan], '
			   				+ 'T.[idData-Lesson] as [_idData-Lesson], '
			   				+ 'T.[idData-TinCanParent] as [_idData-TinCanParent], '
			   				+ 'oAuth.label as [label], '
			   				+ 'U.lastname + '', '' + U.firstName + (case when U.middlename is not null then '' '' + U.middlename else '''' end) as [Full Name],  '
							+ 'U.lastname as [##lastName##], '
							+ 'U.middleName as [##middleName##], '
							+ 'U.firstname as [##firstName##], '
							+ 'U.email as [##email##], '
			   				+ 'T.[isInternalAPI], '
			   				+ 'T.[statementId], '
			   				+ 'T.[actor], '
			   				+ 'TV.[verb] as [verb], '
			   				+ 'T.[activityId] as [TinCan.activityId], '
			   				+ 'T.[mboxObject] as [mboxObject], '
			   				+ 'T.[mboxSha1SumObject] as [mboxSha1SumObject], '
			   				+ 'T.[openIdObject] as [openIdObject], '
			   				+ 'T.[accountObject] as [accountObject], '
			   				+ 'T.[mboxActor] as [mboxActor], '
			   				+ 'T.[mboxSha1SumActor] as [mboxSha1SumActor], '
			   				+ 'T.[openIdActor] as [openIdActor], '
			   				+ 'T.[accountActor] as [accountActor], '
			   				+ 'T.[mboxAuthority] as [mboxAuthority], '
			   				+ 'T.[mboxSha1SumAuthority] as [mboxSha1SumAuthority], '
			   				+ 'T.[openIdAuthority] as [openIdAuthority], '
			   				+ 'T.[accountAuthority] as [accountAuthority], '
			   				+ 'T.[mboxTeam] as [mboxTeam], '
			   				+ 'T.[mboxSha1SumTeam] as [mboxSha1SumTeam], '
			   				+ 'T.[openIdTeam] as [openIdTeam], '
			   				+ 'T.[accountTeam] as [accountTeam], '
			   				+ 'T.[mboxInstructor] as [mboxInstructor], '
			   				+ 'T.[mboxSha1SumInstructor] as [mboxSha1SumInstructor], '
			   				+ 'T.[openIdInstructor] as [openIdInstructor], '
			   				+ 'T.[accountInstructor] as [accountInstructor], '
			   				+ 'T.[object] as [TinCan.object], '
			   				+ 'T.[registration] as [registration], '
			   				+ 'T.[statement] as [statement], '
			   				+ 'T.[isVoidingStatement] as [isVoidingStatement], '
			   				+ 'T.[isStatementVoided] as [isStatementVoided], '
			   				+ 'T.[dtStored] as [dtStored], '
							+ 'TP.[profileId] as [Profile ID], '
							+ 'TP.[activityId] as [Profile.activityID], '
							+ 'TP.[agent] as [Profile.agent], '
							+ 'TP.[contentType] as [Profile.contentType], '
							+ 'TP.[docContents] as [Profile.docContents], '
							+ 'TP.[dtUpdated] as [Profile.UpdateDate], '
							+ 'TS.[stateID] as [State ID], '
							+ 'TS.[agent] as [State.agent], '
							+ 'TS.[activityID] as [State.activityID], '
							+ 'TS.[registration] as [State.registration], '
							+ 'TS.[contentType] as [State.contentType], '
							+ 'TS.[docContents] as [State.docContents], '
							+ 'TS.[dtUpdated] as [State.UpdateDate], '
							+ 'TCA.[activityID] as [TinCanContext.activityID], '
							+ 'TCA.[activityType] as [TinCanContext.activityType], '
							+ 'TCA.[objectType] as [TinCanContext.objectType] '
							+ 'FROM [tblData-TinCan] T '
							+ 'LEFT JOIN [tblTinCanVerb] TV on TV.idVerb = T.verbID '
							+ 'LEFT JOIN [tblxAPIoAuthConsumer] oAuth on oAuth.[idxAPIoAuthConsumer] = T.idEndpoint '
							+ 'LEFT JOIN [tblData-TinCanProfile] TP on TP.idEndpoint = T.idEndpoint '
							+ 'LEFT JOIN [tblData-TinCanState] TS on TS.idEndpoint = T.idEndpoint '
							+ 'LEFT JOIN [tblData-TinCanContextActivities] TCA on TCA.[idData-TinCan] = T.[idData-TinCan] '
							+ 'LEFT JOIN tblUser U on U.email = SUBSTRING(T.mboxActor, CHARINDEX('':'', T.mboxActor)+1, LEN(T.mboxActor)) '
							+ 'LEFT JOIN tblUserToGroupLink UGL on UGL.idUser = U.idUser '
							+ 'WHERE T.[idData-Lesson] is null '
							
							IF (@groupScope IS NOT NULL AND @groupScope <> '')
								BEGIN
								SET @sql = @sql + 'AND UGL.idgroup IN (' + @groupScope + ') '
								END
							
							SET @sql = @sql + ') MAIN'  

	IF @whereClauseLOC IS NOT NULL AND @whereClauseLOC <> ''
		BEGIN
		SET @sql = @sql + ' WHERE ' + @whereClauseLOC
		END	
				
	IF @orderByClauseLOC IS NOT NULL AND @orderByClauseLOC <> ''
		BEGIN
		
		-- for security reasons, we will always pass @orderByClauseLOC with a trailing comma, remove that comma
		-- this will prevent another sql statement from being attached to this query
		SET @orderByClauseLOC = LEFT(@orderByClauseLOC, LEN(@orderByClauseLOC) - 1)

		SET @sql = @sql + ' ORDER BY ' + @orderByClauseLOC

		END
														

	/*

	execute the sql statement and log its execution

	*/

	DECLARE @dtExecutionBegin DATETIME
	DECLARE @dtExecutionEnd DATETIME
	DECLARE @executionDurationMS INT

	SET @dtExecutionBegin = GETUTCDATE()

	EXEC sp_executesql @sql

	SET @dtExecutionEnd = GETUTCDATE()

	SET @executionDurationMS = DATEDIFF(ms, @dtExecutionBegin, @dtExecutionEnd)

	IF (@logReportExecutionLOC = 1)
	BEGIN
		INSERT INTO tblReportProcessorLog
		(
			idSite,
			idCaller,
			datasetProcedure,
			dtExecutionBegin,
			dtExecutionEnd,
			executionDurationSeconds,
			sqlQuery
		)
		SELECT
			@idCallerSite,
			@idCaller,
			'[Dataset.TinCan]',
			@dtExecutionBegin,
			@dtExecutionEnd,
			CAST((CAST(@executionDurationMS AS DECIMAL)/1000) AS DECIMAL(9,2)),
			@sql
    END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO