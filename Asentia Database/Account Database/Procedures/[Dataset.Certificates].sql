-- =====================================================================
-- PROCEDURE: [Dataset.Certificates]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Dataset.Certificates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Dataset.Certificates]
GO

/*

Certificate Transcript Dataset

*/

CREATE PROCEDURE [Dataset.Certificates]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@fields						NVARCHAR(MAX),
	@whereClause				NVARCHAR(MAX),
	@orderByClause				NVARCHAR(768),
	@maxRecords					INT,
	@logReportExecution			BIT
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*

	declare and assign local variables to prevent "parameter sniffing"

	*/

	DECLARE @fieldsLOC					NVARCHAR(MAX)
	DECLARE @whereClauseLOC				NVARCHAR(MAX)
	DECLARE @orderByClauseLOC			NVARCHAR(768)
	DECLARE @maxRecordsLOC				INT
	DECLARE @logReportExecutionLOC		BIT

	SET @fieldsLOC				= @fields
	SET @whereClauseLOC			= @whereClause
	SET @orderByClauseLOC		= @orderByClause
	SET @maxRecordsLOC			= @maxRecords
	SET @logReportExecutionLOC	= @logReportExecution

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
		if no fields defined, select * (all)

	*/

	IF @fieldsLOC IS NULL OR @fieldsLOC = ''
		BEGIN
		SET @fieldsLOC = '*'
		END

	/*

	get the id of the caller's language

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage =
		CASE WHEN idLanguage IS NULL THEN 
			57
		ELSE 
			idLanguage 
		END
		FROM tblLanguage WHERE code = @callerLangString

	/*

	get the group scope so we can filter on what the calling user is allowed to see
	note that NULL scope means the caller can see anything, and idCaller 1 automatically has null scope and permission to anything
	
	*/

	DECLARE @idDatasetPermission INT
	SET @idDatasetPermission = 404

	DECLARE @groupScope NVARCHAR(MAX)
	SET @groupScope = NULL

	IF (@idCaller > 1)
		BEGIN 

		CREATE TABLE #CallerRoles (idRole INT)
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT URL.idRole FROM tblUserToRoleLink URL WHERE URL.idUser = @idCaller -- directly assigned roles
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT GRL.idRole FROM tblGroupToRoleLink GRL -- group inherited roles
									      WHERE GRL.idGroup IN (SELECT DISTINCT UGL.idGroup FROM tblUserToGroupLink UGL WHERE UGL.idUser = @idCaller)
										  AND NOT EXISTS (SELECT 1 FROM #CallerRoles CR WHERE CR.idRole = GRL.idRole)
		
		-- if the caller does not have any permissions to this dataset with NULL scope, get the defined scopes
		IF (
		    SELECT COUNT(1) FROM tblRoleToPermissionLink RPL 
		    WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles)
			AND RPL.idPermission = @idDatasetPermission
			AND RPL.scope IS NULL
		   ) = 0
			BEGIN

			-- get all scope items into the group scope variable			
			DECLARE @scopeItems NVARCHAR(MAX)
			SET @groupScope = ''

			DECLARE scopeListCursor CURSOR LOCAL FOR
				SELECT scope FROM tblRoleToPermissionLink RPL WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles) AND RPL.idPermission = @idDatasetPermission AND RPL.scope IS NOT NULL

			OPEN scopeListCursor

			FETCH NEXT FROM scopeListCursor INTO @scopeItems

			WHILE @@FETCH_STATUS = 0
				BEGIN

				IF (@scopeItems <> '')
					BEGIN
					IF (@groupScope <> '')
						BEGIN
						SET @groupScope = @groupScope + ',' + @scopeItems
						END
					ELSE
						BEGIN
						SET @groupScope = @scopeItems
						END
						
					END

				FETCH NEXT FROM scopeListCursor INTO @scopeItems

				END

			-- kill the cursor
			CLOSE scopeListCursor
			DEALLOCATE scopeListCursor

			END

		END

	/*

		build sql query

	*/

	DECLARE @sql NVARCHAR(MAX)

	SET @sql = 'SELECT DISTINCT '
				+ CASE WHEN (@maxRecordsLOC IS NOT NULL AND @maxRecordsLOC > 0) THEN + ' TOP ' + convert(nvarchar, @maxRecordsLOC) + ' ' ELSE '' END
    SET @sql = @sql +  @fieldsLOC + ',_idUser '
    SET @sql = @sql +  ' FROM ('
				+ ' SELECT DISTINCT '  	
				+ ' 	U.idSite as _idSite, '
				+ ' 	U.lastname + '', '' + U.firstName + (case when U.middlename is not null then '' '' + U.middlename else '''' end) as [Full Name], '
				+ ' 	U.lastname as [##lastName##], '
				+ '		U.middleName as [##middleName##], '
				+ ' 	U.firstname as [##firstName##], '
				+ ' 	U.idUser as _idUser, '
				+ ' 	U.email as [##email##], '
				+ ' 	U.username as [##username##], '
				+ ' 	U.jobTitle as [##jobTitle##], '
				+ ' 	U.jobClass as [##jobClass##], '
				+ ' 	U.company as [##company##], '
				+ ' 	replace(replace(U.address, CHAR(10), '' ''), CHAR(13), '' '') as [##address##], '
				+ ' 	U.city as [##city##],  '
				+ ' 	U.province as [##province##], '
				+ ' 	U.postalcode as [##postalcode##], '
				+ ' 	U.country as [##country##], '
				+ ' 	U.phonePrimary as [##phonePrimary##], ' 
				+ ' 	U.phoneWork as [##phoneWork##],  '
				+ ' 	U.phoneHome as [##phoneHome##],  '
				+ ' 	U.phoneFax as [##phoneFax##],  '
				+ ' 	U.phoneMobile as [##phoneMobile##],  '
				+ ' 	U.phonePager as [##phonePager##],  '
				+ ' 	U.phoneOther as [##phoneOther##],  '
				+ ' 	U.division as [##division##],  '
				+ ' 	U.department as [##department##],  '
					--+ 'SUPER.lastname + ', ' + SUPER.firstName + (case when SUPER.middlename is not null then ' ' + SUPER.middlename else '' end) as [##supervisor##],
				+ ' 	U.region as [##region##],  '
				+ ' 	U.employeeID as [##employeeID##],  '
				+ ' 	U.dtHire as [##dtHire##],  '
				+ ' 	U.dtTerm as [##dtTerm##],  '
				+ ' 	U.gender as [##gender##],  '
				+ ' 	U.race as [##race##],  '
				+ ' 	U.dtDOB as [##dtDOB##],  '
				+ ' 	CONVERT(BIT, U.isActive) as [##isactive##],	'
				+ ' 	cast (U.isActive as int) as [_sb##isactive##],	'
				+ ' 	U.dtCreated as [##dtCreated##],	'
				+ ' 	U.dtExpires as [##dtExpires##],	'
				+ ' 	U.dtLastLogin as [##dtLastLogin##],	'
				+ ' 	L.name AS [##language##], '
				+ ' 	U.[idLanguage] AS [_sb##language##], '
				+ ' 	CASE WHEN TZL.[displayName] IS NOT NULL THEN TZL.[displayName] ELSE TZ.[displayName] END AS [##timezone##], '
				+ ' 	TZ.idTimezone AS [_sb##timezone##],  '
				
				-- if "Group" fields are in the selected fields or filters, select tblUserToGroupLink fields
				IF (CHARINDEX('[Group]', @fieldsLOC) > 0 OR CHARINDEX('[Group]', @whereClauseLOC) > 0)
					BEGIN
					SET @sql = @sql + '	CASE WHEN GL.[name] IS NOT NULL THEN GL.[name] ELSE G.[name] END AS [Group], '
					+ '	UGL.idGroup as _sbGroup, ' -- hidden field from reports; used to filter only users that the report-runner is permitted to see
					END

				SET @sql = @sql + '	SUPER.displayName AS [Supervisor], '
				+ '		SUPER.idUser AS _idSupervisor, '
				+ ' 	U.field00 as [##field00##],	' 
				+ ' 	U.field01 as [##field01##],	'	
				+ ' 	U.field02 as [##field02##], '  
				+ ' 	U.field03 as [##field03##], '  
				+ ' 	U.field04 as [##field04##], ' 
				+ ' 	U.field05 as [##field05##], ' 
				+ ' 	U.field06 as [##field06##], ' 
				+ ' 	U.field07 as [##field07##], ' 
				+ ' 	U.field08 as [##field08##], '
				+ ' 	U.field09 as [##field09##],	'
				+ '		U.field10 as [##field10##], '
				+ '		U.field11 as [##field11##], '
				+ '		U.field12 as [##field12##], '
				+ '		U.field13 as [##field13##], '
				+ '		U.field14 as [##field14##], '
				+ '		U.field15 as [##field15##], '
				+ '		U.field16 as [##field16##], '
				+ '		U.field17 as [##field17##], '
				+ '		U.field18 as [##field18##], '
				+ '		U.field19 as [##field19##], '
				+ ' 	CR.idCertificateRecord as _idCertificateRecord,	'
				+ ' 	case when CR.idCertificateRecord is not null then	'
				+ ' 		C.[name]  + '' (#'' + convert(nvarchar(10),CR.idCertificateRecord) + '')''	'
				+ ' 	else '
				+ ' 		C.[name]	'
				+ ' 	end as [Certificate Name],	'
				+ ' 	C.issuingOrganization as [Issuing Organization], '
				+ ' 	case when CR.idAwardedBy is not null then	'
				+ ' 		case when AWARDER.idUser is null then	'
				+ ' 			''[Name Unavailable]''	'
				+ ' 		else	'
				+ ' 			case when AWARDER.idUser = 1 then   '
				+ ' 					''Administrator''			'
				+ '				else    '
				+ ' 			        AWARDER.lastname +'', '' + AWARDER.firstname	'
				+ '    		    end    '
				+ ' 		end '
				+ ' 	else	'
				+ ' 		''[Earned]'' '
				+ ' 	end as [Awarded By],	'
				+ ' 	CR.[timestamp] as [Award Date],	'
				+ ' 	CR.[expires] as [Expiration Date],	'
				+ ' 	case when C.credits is null then	'
				+ ' 		0	'
				+ ' 	else	'
				+ ' 		C.credits	'
				+ ' 	end as [Credits],	'
				+ ' 	CR.[code] as [Code]	'
				+ ' FROM tblUser U '

				-- if "Group" fields are in the selected fields or filters, or there is group scope, join to group tables
				IF (CHARINDEX('[Group]', @fieldsLOC) > 0 OR CHARINDEX('[Group]', @whereClauseLOC) > 0 OR (CHARINDEX('_idUser =', @whereClauseLOC) <= 0 AND @groupScope IS NOT NULL AND @groupScope <> ''))				
					BEGIN
					SET @sql = @sql + 'LEFT JOIN tblUserToGroupLink UGL on UGL.idUser = U.idUser '
					+ 'LEFT JOIN tblGroup G on UGL.idGroup = G.idGroup '
					+ 'LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
					END
				
				SET @sql = @sql + 'LEFT JOIN tblLanguage L on L.idLanguage = U.idLanguage '
				+ ' LEFT JOIN tblTimezone TZ ON TZ.idTimezone = U.idTimezone '
				+ ' LEFT JOIN tblTimezoneLanguage TZL ON TZL.idTimezone = TZ.idTimezone '				
				+ ' LEFT JOIN tblUserToSupervisorLink USL ON USL.idUser = U.idUser '
                + ' LEFT JOIN tblUser SUPER ON USL.idSupervisor = SUPER.idUser '
				+ ' LEFT JOIN tblCertificateRecord CR on CR.idUser = U.idUser'
				+ ' LEFT JOIN tblCertificate C on C.idCertificate = CR.idCertificate '
				+ ' LEFT JOIN tblUser AWARDER on AWARDER.idUser = CR.idAwardedBy '				
				+ ' WHERE ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) '
				+ ' AND U.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '
				
				IF (@groupScope IS NOT NULL AND @groupScope <> '')
					BEGIN
					SET @sql = @sql + 'AND UGL.idgroup IN (' + @groupScope + ') '
					END
							
				SET @sql = @sql + ') MAIN'

	IF @whereClauseLOC IS NOT NULL AND @whereClauseLOC <> ''
		BEGIN
		SET @sql = @sql + ' WHERE ' + @whereClauseLOC
		END

	IF @orderByClauseLOC IS NOT NULL AND @orderByClauseLOC <> ''
		BEGIN
		
		-- for security reasons, we will always pass @orderByClauseLOC with a trailing comma, remove that comma
		-- this will prevent another sql statement from being attached to this query
		SET @orderByClauseLOC = LEFT(@orderByClauseLOC, LEN(@orderByClauseLOC) - 1)

		SET @sql = @sql + ' ORDER BY ' + @orderByClauseLOC
		END

	/*

	execute the sql statement and log its execution

	*/

	DECLARE @dtExecutionBegin DATETIME
	DECLARE @dtExecutionEnd DATETIME
	DECLARE @executionDurationMS INT

	SET @dtExecutionBegin = GETUTCDATE()

	EXEC sp_executesql @sql

	SET @dtExecutionEnd = GETUTCDATE()

	SET @executionDurationMS = DATEDIFF(ms, @dtExecutionBegin, @dtExecutionEnd)

	IF (@logReportExecutionLOC = 1)
	BEGIN
		INSERT INTO tblReportProcessorLog
		(
			idSite,
			idCaller,
			datasetProcedure,
			dtExecutionBegin,
			dtExecutionEnd,
			executionDurationSeconds,
			sqlQuery
		)
		SELECT
			@idCallerSite,
			@idCaller,
			'[Dataset.CertificateTranscript]',
			@dtExecutionBegin,
			@dtExecutionEnd,
			CAST((CAST(@executionDurationMS AS DECIMAL)/1000) AS DECIMAL(9,2)),
			@sql
	END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	


GO



SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO