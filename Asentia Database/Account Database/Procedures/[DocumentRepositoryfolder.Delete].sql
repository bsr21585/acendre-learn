-- =====================================================================
-- PROCEDURE: [DocumentRepositoryFolder.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DocumentRepositoryFolder.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DocumentRepositoryFolder.Delete]
GO

/*

Deletes document repository folder(s)

*/

CREATE PROCEDURE [DocumentRepositoryFolder.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,
	
	@DocumentRepositoryfolders	IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @DocumentRepositoryfolders) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'DocumentRepositoryFolderDelete_NoRecordsSelected'
		RETURN 1
		END
			
	/*
	
	validate that all document repository folders exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @DocumentRepositoryfolders DRIDRI
		LEFT JOIN tblDocumentRepositoryfolder DRI ON DRI.idDocumentRepositoryfolder = DRIDRI.id
		WHERE DRI.idSite IS NULL
		OR DRI.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DocRepositoryFolderDelete_UsersDoNotExistInSite'
		RETURN 1 
		END
		
	/*
	
	DO NOT DELETE the records. Just mark them as deleted.
	
	*/
	
	UPDATE tblDocumentRepositoryfolder SET
		isDeleted = 1,
		dtDeleted = GETUTCDATE()
	WHERE idDocumentRepositoryfolder IN (
		SELECT id
		FROM @DocumentRepositoryfolders
	)
	
	/*
	
	Update documentRepositoryItem table by setting idDocumentRepositoryFolder = NULL
	so all the documentes item of deleted folder(s) will assigned to root.
	
	*/
	update tblDocumentRepositoryItem SET
	       idDocumentRepositoryFolder = NULL
	WHERE idDocumentRepositoryfolder IN (
		SELECT id
		FROM @DocumentRepositoryfolders
	)


	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO