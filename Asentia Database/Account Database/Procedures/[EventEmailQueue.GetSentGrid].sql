SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventEmailQueue.GetSentGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventEmailQueue.GetSentGrid]
GO

/*

Gets a listing of event email notifications log for sent Grid.

*/

CREATE PROCEDURE [EventEmailQueue.GetSentGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			 
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblEventEmailQueue E 
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite)
				)		
				AND E.dtSent IS NOT NULL AND E.statusDescription IS NULL
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						E.idEventEmailQueue,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'fullName' THEN E.recipientFullName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'notificationName' THEN (CASE WHEN EL.name IS NOT NULL THEN EL.name ELSE EN.name END) END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtSent' THEN E.dtSent END) END DESC,

							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'fullName' THEN E.recipientFullName END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'notificationName' THEN (CASE WHEN EL.name IS NOT NULL THEN EL.name ELSE EN.name END) END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtSent' THEN E.dtSent END) END
						)
						AS [row_number]
					FROM tblEventEmailQueue E
					LEFT JOIN tblEventEmailNotification EN ON EN.idEventEmailNotification = E.idEventEmailNotification			
					LEFT JOIN tblEventEmailNotificationLanguage EL ON EL.idEventEmailNotification = EN.idEventEmailNotification AND EL.idLanguage = @idCallerLanguage
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite)
						)
						AND E.dtSent IS NOT NULL AND E.statusDescription IS NULL
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idEventEmailQueue, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				E.idEventEmailQueue, 
				E.recipientFullName AS fullName,
				E.recipientEmail AS email,
				E.recipientLogin AS username,
				CASE WHEN EL.name IS NOT NULL THEN EL.name ELSE EN.name END AS notificationName, 
				E.dtSent,
				CASE WHEN EN.isDeleted IS NOT NULL THEN EN.isDeleted ELSE 0 END AS isDeleted,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblEventEmailQueue E ON E.idEventEmailQueue = SelectedKeys.idEventEmailQueue
			LEFT JOIN tblEventEmailNotification EN ON EN.idEventEmailNotification = E.idEventEmailNotification			
			LEFT JOIN tblEventEmailNotificationLanguage EL ON EL.idEventEmailNotification = EN.idEventEmailNotification AND EL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblEventEmailQueue E 
				INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = E.idRecipient
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite)
					)
					AND E.dtSent IS NOT NULL AND E.statusDescription IS NULL
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							E.idEventEmailQueue,
							ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'fullName' THEN E.recipientFullName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'notificationName' THEN (CASE WHEN EL.name IS NOT NULL THEN EL.name ELSE EN.name END) END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtSent' THEN E.dtSent END) END DESC,

							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'fullName' THEN E.recipientFullName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'notificationName' THEN (CASE WHEN EL.name IS NOT NULL THEN EL.name ELSE EN.name END) END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtSent' THEN E.dtSent END) END
							)
							AS [row_number]
						FROM tblEventEmailQueue E
						INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = E.idRecipient
						LEFT JOIN tblEventEmailNotification EN ON EN.idEventEmailNotification = E.idEventEmailNotification			
						LEFT JOIN tblEventEmailNotificationLanguage EL ON EL.idEventEmailNotification = EN.idEventEmailNotification AND EL.idLanguage = @idCallerLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite
							)
							AND E.dtSent IS NOT NULL AND E.statusDescription IS NULL
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idEventEmailQueue, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					E.idEventEmailQueue, 
					E.recipientFullName AS fullName,
					E.recipientEmail AS email,
					E.recipientLogin AS username,
					CASE WHEN EL.name IS NOT NULL THEN EL.name ELSE EN.name END AS notificationName, 
					E.dtSent,
					CASE WHEN EN.isDeleted IS NOT NULL THEN EN.isDeleted ELSE 0 END AS isDeleted,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblEventEmailQueue E ON E.idEventEmailQueue = SelectedKeys.idEventEmailQueue
				LEFT JOIN tblEventEmailNotification EN ON EN.idEventEmailNotification = E.idEventEmailNotification
				LEFT JOIN tblEventEmailNotificationLanguage EL ON EL.idEventEmailNotification = EN.idEventEmailNotification AND EL.idLanguage = @idCallerLanguage				
				ORDER BY SelectedKeys.[row_number]

			END

		SET @Return_Code = 0
		SET @Error_Description_Code = ''
			
	END
