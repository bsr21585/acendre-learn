-- =====================================================================
-- PROCEDURE: [LearningPath.GetCourses]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.GetCourses]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.GetCourses]
GO

/*

Returns a recordset of course ids and titles that belong to a learning path.

*/

CREATE PROCEDURE [LearningPath.GetCourses]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idLearningPath			INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		SELECT
			DISTINCT
			LPCL.[order],
			C.idCourse, 
			CASE WHEN C.coursecode IS NOT NULL THEN
				C.coursecode + ' ' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			ELSE
				CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			END AS title
		FROM tblCourse C
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblLearningPathToCourseLink LPCL ON LPCL.idCourse = C.idCourse
		WHERE C.idSite = @idCallerSite
		AND LPCL.idLearningPath = @idLearningPath
		ORDER BY LPCL.[order]

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			LPCL.[order],
			C.idCourse,
			CASE WHEN C.coursecode IS NOT NULL THEN
				C.coursecode + ' ' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			ELSE
				CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END 
			END AS title
		FROM tblCourseLanguage CL
		INNER JOIN CONTAINSTABLE(tblCourseLanguage, *, @searchParam) K ON K.[key] = CL.idCourseLanguage AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCourse C ON C.idCourse = CL.idCourse
		LEFT JOIN tblLearningPathToCourseLink LPCL ON LPCL.idCourse = C.idCourse
		WHERE C.idSite = @idCallerSite
		AND LPCL.idLearningPath = @idLearningPath
		ORDER BY LPCL.[order]

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO