-- =====================================================================
-- PROCEDURE: [Role.GetGroups]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Role.GetGroups]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Role.GetGroups]
GO

/*

Returns a recordset of group ids and names that are linked to a role.

*/

CREATE PROCEDURE [Role.GetGroups]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idRole					INT,
	@excludeAutoJoined		BIT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString
		
	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		IF @excludeAutoJoined = 1

			BEGIN

			SELECT
				DISTINCT
				G.idGroup, 
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
				GRL.idRuleSet
			FROM tblGroup G
			LEFT JOIN tblGroupToRoleLink GRL ON GRL.idGroup = G.idGroup
			LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
			WHERE G.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblGroupToRoleLink GRL
						WHERE GRL.idRole = @idRole
						AND GRL.idGroup = G.idGroup
						AND GRL.idRuleSet IS NULL)
			ORDER BY name

			END

		ELSE

			BEGIN

			SELECT
				DISTINCT
				G.idGroup, 
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
				GRL.idRuleSet
			FROM tblGroup G
			LEFT JOIN tblGroupToRoleLink GRL ON GRL.idGroup = G.idGroup
			LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
			WHERE G.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblGroupToRoleLink GRL
						WHERE GRL.idRole = @idRole
						AND GRL.idGroup = G.idGroup)
			ORDER BY name

			END

		END

	ELSE

		BEGIN

		IF @excludeAutoJoined = 1

			BEGIN

			SELECT
				DISTINCT
				G.idGroup, 
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
				GRL.idRuleSet
			FROM tblGroupLanguage GL
			INNER JOIN CONTAINSTABLE(tblGroupLanguage, *, @searchParam) K ON K.[key] = GL.idGroupLanguage AND GL.idLanguage = @idCallerLanguage
			LEFT JOIN tblGroup G ON G.idGroup = GL.idGroup
			LEFT JOIN tblGroupToRoleLink GRL ON GRL.idGroup = G.idGroup
			WHERE G.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblGroupToRoleLink GRL
						WHERE GRL.idRole = @idRole
						AND GRL.idGroup = G.idGroup
						AND GRL.idRuleSet IS NULL)
			ORDER BY name

			END

		ELSE

			BEGIN

			SELECT
				DISTINCT
				G.idGroup, 
				CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
				GRL.idRuleSet
			FROM tblGroupLanguage GL
			INNER JOIN CONTAINSTABLE(tblGroupLanguage, *, @searchParam) K ON K.[key] = GL.idGroupLanguage AND GL.idLanguage = @idCallerLanguage
			LEFT JOIN tblGroup G ON G.idGroup = GL.idGroup
			LEFT JOIN tblGroupToRoleLink GRL ON GRL.idGroup = G.idGroup
			WHERE G.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblGroupToRoleLink GRL
						WHERE GRL.idRole = @idRole
						AND GRL.idGroup = G.idGroup)
			ORDER BY name

			END

		END
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	