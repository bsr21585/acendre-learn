-- =====================================================================
-- PROCEDURE: [Data-TinCan.GetStatements]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCan.GetStatements]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCan.GetStatements]
GO

/*

Returns statements.

*/

CREATE PROCEDURE [Data-TinCan.GetStatements]
(
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idEndpoint				INT,
	@verbId					NVARCHAR(100),
	@activityId				NVARCHAR(100),
	@registration			NVARCHAR(50),
	@start					INT,
	@limit					INT,
	@since					DATETIME,
	@until					DATETIME,
	@orderAsc				BIT,
	@relatedActivities		BIT,
	@mbox					NVARCHAR(100),
	@mboxSha1Sum			NVARCHAR(100),
	@openId					NVARCHAR(100),
	@account				NVARCHAR(100),
	@relatedAgents			BIT,
	@moreResult				BIT						OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @end INT
	
	SET @end = @start + @limit - 1;
	
	/*
	
	Check wether more results available
	
	*/
	
	IF (SELECT	 COUNT(1)
		FROM	 [tblData-TinCan] t
		LEFT JOIN tblTinCanVerb TCV ON t.verbId = TCV.idVerb
		WHERE	 t.idSite = @idCallerSite
		AND		 t.idEndpoint = @idEndpoint
		AND		 t.isStatementVoided = 0
		AND		 (TCV.URI = @verbId OR @verbId IS NULL)
		AND		 (t.registration = @registration OR @registration IS NULL)
		AND		 (t.dtStored > @since OR @since IS NULL)
		AND		 (t.dtStored <= @until OR @until IS NULL)
		AND		 (((t.mboxObject = @mbox OR @mbox IS NULL)
						AND (t.mboxSha1SumObject = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
						AND (t.openIdObject = @openId OR @openId IS NULL)
						AND (t.accountObject = @account OR @account IS NULL)
					) --Main statement Object
					OR ((@relatedAgents = 1)
						AND (((t.mboxActor = @mbox OR @mbox IS NULL)
								AND (t.mboxSha1SumActor = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
								AND (t.openIdActor = @openId OR @openId IS NULL)
								AND (t.accountActor = @account OR @account IS NULL)
							) --Main statement Actor
							OR ((t.mboxAuthority = @mbox OR @mbox IS NULL)
								AND (t.mboxSha1SumAuthority = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
								AND (t.openIdAuthority = @openId OR @openId IS NULL)
								AND (t.accountAuthority = @account OR @account IS NULL)
							) --Main statement Authority
							OR ((t.mboxTeam = @mbox OR @mbox IS NULL)
								AND (t.mboxSha1SumTeam = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
								AND (t.openIdTeam = @openId OR @openId IS NULL)
								AND (t.accountTeam = @account OR @account IS NULL)
							) --Main statement Team
							OR ((t.mboxInstructor = @mbox OR @mbox IS NULL)
								AND (t.mboxSha1SumInstructor = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
								AND (t.openIdInstructor = @openId OR @openId IS NULL)
								AND (t.accountInstructor = @account OR @account IS NULL)
							) --Main statement Instructor
							OR t.[idData-TinCan] IN (
								SELECT tt.[idData-TinCanParent]
								FROM [tblData-TinCan] tt
								WHERE tt.[idData-TinCanParent] IS NOT NULL
								AND (((tt.mboxObject = @mbox OR @mbox IS NULL)
										AND (tt.mboxSha1SumObject = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
										AND (tt.openIdObject = @openId OR @openId IS NULL)
										AND (tt.accountObject = @account OR @account IS NULL)
									) --Sub-statement Object
									OR ((tt.mboxActor = @mbox OR @mbox IS NULL)
										AND (tt.mboxSha1SumActor = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
										AND (tt.openIdActor = @openId OR @openId IS NULL)
										AND (tt.accountActor = @account OR @account IS NULL)
									) --Sub-statement Actor
									OR ((tt.mboxTeam = @mbox OR @mbox IS NULL)
										AND (tt.mboxSha1SumTeam = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
										AND (tt.openIdTeam = @openId OR @openId IS NULL)
										AND (tt.accountTeam = @account OR @account IS NULL)
									) --Sub-statement Team
									OR ((tt.mboxInstructor = @mbox OR @mbox IS NULL)
										AND (tt.mboxSha1SumInstructor = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
										AND (tt.openIdInstructor = @openId OR @openId IS NULL)
										AND (tt.accountInstructor = @account OR @account IS NULL)
									) --Sub-statement Instructor
									--No check for authority - sub-statements don't have authority
								)
							)
						)
					)
				 )
		AND		 ((t.activityId = @activityId OR @activityId IS NULL)
					OR (@relatedActivities = 1
						AND ((t.[idData-TinCan] IN (
								SELECT tt.[idData-TinCanParent]
								FROM [tblData-TinCan] tt
								WHERE tt.[idData-TinCanParent] IS NOT NULL
								AND tt.activityId = @activityId
								OR tt.[idData-TinCan] IN (
									SELECT context.[idData-TinCan]
									FROM [tblData-TinCanContextActivities] context
									WHERE context.activityId = @activityId)
								)
							)
							OR (t.[idData-TinCan] IN (
								SELECT context.[idData-TinCan]
								FROM [tblData-TinCanContextActivities] context
								WHERE context.activityId = @activityId)
							)
						)
					)
				 )
		) > @end
		BEGIN
		SET @moreResult = 1;
		END
	ELSE
		BEGIN
		SET @moreResult = 0;
		END
	
	/*
	
	Get the data
	
	*/
	
	SELECT statements.[statement] FROM (
		SELECT	 t.[statement], ROW_NUMBER() OVER (ORDER BY
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 0 THEN dtStored END DESC, --Descending order is the default
					CASE WHEN @orderAsc = 1 THEN dtStored END ASC) RowNumber
		FROM	 [tblData-TinCan] t
		LEFT JOIN tblTinCanVerb TCV ON t.verbId = TCV.idVerb
		WHERE	 t.idSite = @idCallerSite
		AND		 t.idEndpoint = @idEndpoint
		AND		 t.[idData-TinCanParent] IS NULL --Get only parent statements
		AND		 t.isStatementVoided = 0
		AND		 (TCV.URI = @verbId OR @verbId IS NULL)
		AND		 (t.registration = @registration OR @registration IS NULL)
		AND		 (t.dtStored > @since OR @since IS NULL) --Only statements since the specified timestamp (exclusive) are returned.
		AND		 (t.dtStored <= @until OR @until IS NULL)
		AND		 (((t.mboxObject = @mbox OR @mbox IS NULL)
						AND (t.mboxSha1SumObject = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
						AND (t.openIdObject = @openId OR @openId IS NULL)
						AND (t.accountObject = @account OR @account IS NULL)
					) --Main statement Object
					OR ((@relatedAgents = 1)
						AND (((t.mboxActor = @mbox OR @mbox IS NULL)
								AND (t.mboxSha1SumActor = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
								AND (t.openIdActor = @openId OR @openId IS NULL)
								AND (t.accountActor = @account OR @account IS NULL)
							) --Main statement Actor
							OR ((t.mboxAuthority = @mbox OR @mbox IS NULL)
								AND (t.mboxSha1SumAuthority = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
								AND (t.openIdAuthority = @openId OR @openId IS NULL)
								AND (t.accountAuthority = @account OR @account IS NULL)
							) --Main statement Authority
							OR ((t.mboxTeam = @mbox OR @mbox IS NULL)
								AND (t.mboxSha1SumTeam = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
								AND (t.openIdTeam = @openId OR @openId IS NULL)
								AND (t.accountTeam = @account OR @account IS NULL)
							) --Main statement Team
							OR ((t.mboxInstructor = @mbox OR @mbox IS NULL)
								AND (t.mboxSha1SumInstructor = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
								AND (t.openIdInstructor = @openId OR @openId IS NULL)
								AND (t.accountInstructor = @account OR @account IS NULL)
							) --Main statement Instructor
							OR t.[idData-TinCan] IN (
								SELECT tt.[idData-TinCanParent]
								FROM [tblData-TinCan] tt
								WHERE tt.[idData-TinCanParent] IS NOT NULL
								AND (((tt.mboxObject = @mbox OR @mbox IS NULL)
										AND (tt.mboxSha1SumObject = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
										AND (tt.openIdObject = @openId OR @openId IS NULL)
										AND (tt.accountObject = @account OR @account IS NULL)
									) --Sub-statement Object
									OR ((tt.mboxActor = @mbox OR @mbox IS NULL)
										AND (tt.mboxSha1SumActor = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
										AND (tt.openIdActor = @openId OR @openId IS NULL)
										AND (tt.accountActor = @account OR @account IS NULL)
									) --Sub-statement Actor
									OR ((tt.mboxTeam = @mbox OR @mbox IS NULL)
										AND (tt.mboxSha1SumTeam = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
										AND (tt.openIdTeam = @openId OR @openId IS NULL)
										AND (tt.accountTeam = @account OR @account IS NULL)
									) --Sub-statement Team
									OR ((tt.mboxInstructor = @mbox OR @mbox IS NULL)
										AND (tt.mboxSha1SumInstructor = @mboxSha1Sum OR @mboxSha1Sum IS NULL)
										AND (tt.openIdInstructor = @openId OR @openId IS NULL)
										AND (tt.accountInstructor = @account OR @account IS NULL)
									) --Sub-statement Instructor
									--No check for authority - sub-statements don't have authority
								)
							)
						)
					)
				 )
		AND		 ((t.activityId = @activityId OR @activityId IS NULL)
					OR (@relatedActivities = 1
						AND ((t.[idData-TinCan] IN (
								SELECT tt.[idData-TinCanParent]
								FROM [tblData-TinCan] tt
								WHERE tt.[idData-TinCanParent] IS NOT NULL
								AND tt.activityId = @activityId
								OR tt.[idData-TinCan] IN (
									SELECT context.[idData-TinCan]
									FROM [tblData-TinCanContextActivities] context
									WHERE context.activityId = @activityId)
								)
							)
							OR (t.[idData-TinCan] IN (
								SELECT context.[idData-TinCan]
								FROM [tblData-TinCanContextActivities] context
								WHERE context.activityId = @activityId)
							)
						)
					)
				 )
		) AS statements
	WHERE statements.RowNumber BETWEEN @start AND @end
	ORDER BY RowNumber
	
	SELECT @Return_Code = 0 --Success
	SELECT @Error_Description_Code = NULL
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO