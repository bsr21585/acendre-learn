-- =====================================================================
-- PROCEDURE: [Enrollment.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.Details]
GO

/*

Return all the properties for a given enrollment id.

*/

CREATE PROCEDURE [Enrollment.Details]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idEnrollment						INT				OUTPUT,
	@idSite								INT				OUTPUT,
	@idCourse							INT				OUTPUT,
	@idUser								INT				OUTPUT,
	@idGroupEnrollment					INT				OUTPUT,
	@idRuleSetEnrollment				INT				OUTPUT,
	@idLearningPathEnrollment			INT				OUTPUT,
	@idTimezone							INT				OUTPUT,
	@isLockedByPrerequisites			BIT				OUTPUT,
	@code								NVARCHAR(255)	OUTPUT,
	@revcode							NVARCHAR(32)	OUTPUT,
	@title								NVARCHAR(255)	OUTPUT,
	@dtStart							DATETIME		OUTPUT,
	@dtDue								DATETIME		OUTPUT,
	@dtExpiresFromStart					DATETIME		OUTPUT,
	@dtExpiresFromFirstLaunch			DATETIME		OUTPUT,
	@dtFirstLaunch						DATETIME		OUTPUT,
	@dtCompleted						DATETIME		OUTPUT,
	@dtCreated							DATETIME		OUTPUT,
	@dtLastSynchronized					DATETIME		OUTPUT,
	@dueInterval						INT				OUTPUT,
	@dueTimeframe						NVARCHAR(4)		OUTPUT,
	@expiresFromStartInterval			INT				OUTPUT,
	@expiresFromStartTimeframe			NVARCHAR(4)		OUTPUT,
	@expiresFromFirstLaunchInterval		INT				OUTPUT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4)		OUTPUT,
	@idActivityImport					INT				OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblEnrollment
		WHERE idEnrollment = @idEnrollment
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data
	
	*/		
	
	SELECT 
		@idEnrollment						= E.idEnrollment,
		@idSite								= E.idSite,
		@idCourse							= E.idCourse,
		@idUser								= E.idUser,
		@idGroupEnrollment					= E.idGroupEnrollment,
		@idRuleSetEnrollment				= E.idRuleSetEnrollment,
		@idLearningPathEnrollment			= E.idLearningPathEnrollment,
		@idTimezone							= E.idTimezone,
		@isLockedByPrerequisites			= E.isLockedByPrerequisites,
		@code								= E.code,
		@revcode							= E.revcode,
		@title								= E.title,
		@dtStart							= E.dtStart,
		@dtDue								= E.dtDue,
		@dtExpiresFromStart					= E.dtExpiresFromStart,
		@dtExpiresFromFirstLaunch			= E.dtExpiresFromFirstLaunch,
		@dtFirstLaunch						= E.dtFirstLaunch,
		@dtCompleted						= E.dtCompleted,
		@dtCreated							= E.dtCreated,
		@dtLastSynchronized					= E.dtLastSynchronized,
		@dueInterval						= E.dueInterval,
		@dueTimeframe						= E.dueTimeframe,
		@expiresFromStartInterval			= E.expiresFromStartInterval,
		@expiresFromStartTimeframe			= E.expiresFromStartTimeframe,
		@expiresFromFirstLaunchInterval		= E.expiresFromFirstLaunchInterval,
		@expiresFromFirstLaunchTimeframe	= E.expiresFromFirstLaunchTimeframe,
		@idActivityImport					= E.idActivityImport
	 FROM tblEnrollment E
	 WHERE E.idEnrollment = @idEnrollment
		
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO