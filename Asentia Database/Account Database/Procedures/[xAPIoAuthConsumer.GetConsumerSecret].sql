-- =====================================================================
-- PROCEDURE: [xAPIoAuthConsumer.GetConsumerSecret]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[xAPIoAuthConsumer.GetConsumerSecret]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [xAPIoAuthConsumer.GetConsumerSecret]
GO

/*

Authenticates credentials for login to the system.

*/
CREATE PROCEDURE [xAPIoAuthConsumer.GetConsumerSecret]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,
	
	@oAuthSecret			nvarchar(200)	OUTPUT,
	@oAuthKey				NVARCHAR(200)
)
AS
	BEGIN
	SET NOCOUNT ON
		
		SELECT @oAuthSecret = oAuthConsumer.oAuthSecret
		FROM tblxAPIoAuthConsumer oAuthConsumer
		WHERE oAuthConsumer.oAuthKey = @oAuthKey
		AND oAuthConsumer.idSite = @idCallerSite
		
		IF (@oAuthSecret IS null)
			BEGIN
				SET @Return_Code = 1
				SET @Error_Description_Code = 'xAPIoAuthConsumerGetConsumerSecret_KeyNotFound'
			END
			
		ELSE
			BEGIN
				SET @Return_Code = 0 --(0 is 'success') 
			END
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO