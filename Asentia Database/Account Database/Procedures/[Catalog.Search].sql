-- =====================================================================
-- PROCEDURE: [Catalog.Search]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.Search]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.Search]
GO

/*

Searches the entire catalog and returns a recordset of objects.

*/

CREATE PROCEDURE [Catalog.Search]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@objectType				NVARCHAR(50)
)
AS
	BEGIN
		SET NOCOUNT ON
		
		--IF @searchParam = '' OR @searchParam = '*'
			--BEGIN
			--SET @Return_Code = 1
			--SET @Error_Description_Code = 'CatalogSearch_MustHaveSearchParameter'
			--RETURN 1
			--END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		-- if this is a catalog search, get the ids of catalogs the user 
		-- cannot access so that they can be excluded from results

		CREATE TABLE #InaccessibleCatalogs (idCatalog INT)

		IF (@objectType = 'catalog' AND @idCaller <> 1)
				BEGIN

				INSERT INTO #InaccessibleCatalogs (
					idCatalog
				)
				SELECT
					C.idCatalog
				FROM tblCatalog C
				WHERE C.idSite = @idCallerSite
				AND C.isPrivate = 1
				AND C.idCatalog NOT IN (
										SELECT CGL.idCatalog
										FROM tblCatalogAccessToGroupLink CGL
										WHERE CGL.idGroup IN (SELECT UGL.idGroup
															  FROM tblUserToGroupLink UGL
															  WHERE UGL.idUser = @idCaller)
									   )

				END
			
		-- if the caller's language is the same as the site's language,
		-- search and display on the base table
		IF (@idCallerLanguage = @idSiteLanguage)

			BEGIN

			-- catalog
			IF (@objectType = 'catalog')
				BEGIN

				SELECT 
					C.idCatalog AS idObject,
					'catalog' AS objectType,
					NULL AS objectCode,
					C.title AS objectTitle,
					C.shortDescription AS objectDescription,
					NULL AS containedWithinObjectId,
					NULL AS containedWithinObjectType,
					NULL AS containedWithinObjectTitle
				FROM tblCatalog C
				INNER JOIN CONTAINSTABLE(tblCatalog, *, @searchParam) K ON K.[key] = C.idCatalog
				WHERE C.idSite = @idCallerSite
				AND C.idCatalog NOT IN (SELECT idCatalog FROM #InaccessibleCatalogs)
				ORDER BY C.title

				END

			-- course
			IF (@objectType = 'course')
				BEGIN

				SELECT DISTINCT
					MAIN.idObject,
					MAIN.objectType,
					MAIN.objectCode,
					MAIN.objectTitle,
					MAIN.objectDescription,
					MAIN.containedWithinObjectId,
					MAIN.containedWithinObjectType,
					MAIN.containedWithinObjectTitle
				FROM (						
						SELECT 
							C.idCourse AS idObject,
							'course' AS objectType,
							C.coursecode AS objectCode,
							C.title AS objectTitle,
							C.shortDescription AS objectDescription,
							NULL AS containedWithinObjectId,
							NULL AS containedWithinObjectType,
							NULL AS containedWithinObjectTitle
						FROM tblCourse C
						INNER JOIN CONTAINSTABLE(tblCourse, *, @searchParam) K ON K.[key] = C.idCourse
						WHERE C.idSite = @idCallerSite
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND C.isPublished = 1
						
						UNION SELECT 
							C.idCourse AS idObject,
							'course' AS objectType,
							C.coursecode AS objectCode,
							C.title AS objectTitle,
							C.shortDescription AS objectDescription,
							NULL AS containedWithinObjectId,
							NULL AS containedWithinObjectType,
							NULL AS containedWithinObjectTitle							
						FROM tblStandupTrainingInstance STI
						INNER JOIN CONTAINSTABLE(tblStandupTrainingInstance, *, @searchParam) K ON K.[key] = STI.idStandupTrainingInstance						
						INNER JOIN tblLessonToContentLink LCL ON LCL.idObject = STI.idStandUpTraining AND LCL.idContentType = 2
						INNER JOIN tblLesson L ON L.idLesson = LCL.idLesson
						INNER JOIN tblCourse C ON C.idCourse = L.idCourse
						WHERE STI.idSite = @idCallerSite
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND C.isPublished = 1
				) MAIN
				ORDER BY MAIN.objectTitle	

				END
			
			-- lesson
			IF (@objectType = 'lesson')
				BEGIN

				SELECT DISTINCT
					MAIN.idObject,
					MAIN.objectType,
					MAIN.objectCode,
					MAIN.objectTitle,
					MAIN.objectDescription,
					MAIN.containedWithinObjectId,
					MAIN.containedWithinObjectType,
					MAIN.containedWithinObjectTitle
				FROM (
						SELECT 
							L.idLesson AS idObject,
							'lesson' AS objectType,
							NULL AS objectCode,
							L.title AS objectTitle,
							L.shortDescription AS objectDescription,
							L.idCourse AS containedWithinObjectId,
							'course' AS containedWithinObjectType,
							C.title AS containedWithinObjectTitle
						FROM tblLesson L
						INNER JOIN CONTAINSTABLE(tblLesson, *, @searchParam) K ON K.[key] = L.idLesson
						LEFT JOIN tblCourse C ON C.idCourse = L.idCourse
						WHERE L.idSite = @idCallerSite
						AND (L.isDeleted IS NULL OR L.isDeleted = 0)
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND C.isPublished = 1

						UNION SELECT 
							L.idLesson AS idObject,
							'lesson' AS objectType,
							NULL AS objectCode,
							L.title AS objectTitle,
							L.shortDescription AS objectDescription,
							L.idCourse AS containedWithinObjectId,
							'course' AS containedWithinObjectType,
							C.title AS containedWithinObjectTitle
						FROM tblStandupTrainingInstance STI
						INNER JOIN CONTAINSTABLE(tblStandupTrainingInstance, *, @searchParam) K ON K.[key] = STI.idStandupTrainingInstance
						LEFT JOIN tblLessonToContentLink LCL ON LCL.idObject = STI.idStandUpTraining AND LCL.idContentType = 2
						LEFT JOIN tblLesson L ON L.idLesson = LCL.idLesson
						LEFT JOIN tblCourse C ON C.idCourse = L.idCourse
						WHERE STI.idSite = @idCallerSite
						AND (L.isDeleted IS NULL OR L.isDeleted = 0)
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND C.isPublished = 1
				) MAIN
				ORDER BY MAIN.objectTitle

				END

			-- learning path
			IF (@objectType = 'learningpath')
				BEGIN

				SELECT 
					LP.idLearningPath AS idObject,
					'learningpath' AS objectType,
					NULL AS objectCode,
					LP.name AS objectTitle,
					LP.shortDescription AS objectDescription,
					NULL AS containedWithinObjectId,
					NULL AS containedWithinObjectType,
					NULL AS containedWithinObjectTitle
				FROM tblLearningPath LP
				INNER JOIN CONTAINSTABLE(tblLearningPath, *, @searchParam) K ON K.[key] = LP.idLearningPath
				WHERE LP.idSite = @idCallerSite
				AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
				AND LP.isPublished = 1
				ORDER BY LP.name

				END

			-- community
			IF (@objectType = 'community')
				BEGIN

				SELECT 
					G.idGroup AS idObject,
					'community' AS objectType,
					NULL AS objectCode,
					G.name AS objectTitle,
					G.shortDescription AS objectDescription,
					NULL AS containedWithinObjectId,
					NULL AS containedWithinObjectType,
					NULL AS containedWithinObjectTitle
				FROM tblGroup G
				INNER JOIN CONTAINSTABLE(tblGroup, *, @searchParam) K ON K.[key] = G.idGroup
				WHERE G.idSite = @idCallerSite
				AND G.isSelfJoinAllowed = 1
				ORDER BY G.name

				END

			-- ilt
			IF (@objectType = 'ilt')
				BEGIN

				SELECT 
					ST.idStandupTraining AS idObject,
					'ilt' AS objectType,
					NULL AS objectCode,
					ST.title AS objectTitle,
					ST.[description] AS objectDescription,
					NULL AS containedWithinObjectId,
					NULL AS containedWithinObjectType,
					NULL AS containedWithinObjectTitle
				FROM tblStandupTraining ST
				INNER JOIN CONTAINSTABLE(tblStandupTraining, *, @searchParam) K ON K.[key] = ST.idStandupTraining
				WHERE ST.idSite = @idCallerSite
				AND ST.isStandaloneEnroll = 1
				ORDER BY ST.title

				END

			-- document
			IF (@objectType = 'document' AND @idCaller <> 0)
				BEGIN

				SELECT
					DRI.idDocumentRepositoryItem AS idObject,
					'document' AS objectType,
					NULL AS objectCode,
					DRI.label AS objectTitle,
					DRI.[filename] AS objectDescription,
					DRI.idObject AS containedWithinObjectId,
					CASE WHEN DRI.idDocumentRepositoryObjectType = 1 THEN 'group'
						WHEN DRI.idDocumentRepositoryObjectType = 2 THEN 'course'
						WHEN DRI.idDocumentRepositoryObjectType = 3 THEN 'learningpath' END AS containedWithinObjectType,
					CASE WHEN DRI.idDocumentRepositoryObjectType = 1 THEN G.name
						WHEN DRI.idDocumentRepositoryObjectType = 2 THEN C.title
						WHEN DRI.idDocumentRepositoryObjectType = 3 THEN LP.name END AS containedWithinObjectTitle
				FROM tblDocumentRepositoryItem DRI
				INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItem, *, @searchParam) K ON K.[key] = DRI.idDocumentRepositoryItem
				LEFT JOIN tblGroup G ON G.idGroup = DRI.idObject
				LEFT JOIN tblCourse C ON C.idCourse = DRI.idObject
				LEFT JOIN tblLearningPath LP ON LP.idLearningPath = DRI.idObject
				WHERE DRI.idSite = @idCallerSite
				ORDER BY DRI.label
				END

			END
			
		-- if the caller's language is not the same as the site's
		-- default language, we will need to search and display on
		-- the language table, and join to the base table for the
		-- rest of the properties
		ELSE

			BEGIN

			-- catalog
			IF (@objectType = 'catalog')
				BEGIN

				SELECT 
					C.idCatalog AS idObject,
					'catalog' AS objectType,
					NULL AS objectCode,
					CL.title AS objectTitle,
					CL.shortDescription AS objectDescription,
					NULL AS containedWithinObjectId,
					NULL AS containedWithinObjectType,
					NULL AS containedWithinObjectTitle
				FROM tblCatalogLanguage CL
				LEFT JOIN tblCatalog C ON C.idCatalog = CL.idCatalog
				INNER JOIN CONTAINSTABLE(tblCatalogLanguage, *, @searchParam) K ON K.[key] = CL.idCatalogLanguage
				WHERE C.idSite = @idCallerSite
				AND CL.idLanguage = @idCallerLanguage
				AND C.idCatalog NOT IN (SELECT idCatalog FROM #InaccessibleCatalogs)
				ORDER BY CL.title

				END

			-- course
			IF (@objectType = 'course')
				BEGIN

				SELECT DISTINCT
					MAIN.idObject,
					MAIN.objectType,
					MAIN.objectCode,
					MAIN.objectTitle,
					MAIN.objectDescription,
					MAIN.containedWithinObjectId,
					MAIN.containedWithinObjectType,
					MAIN.containedWithinObjectTitle
				FROM (						
						SELECT 
							C.idCourse AS idObject,
							'course' AS objectType,
							C.coursecode AS objectCode,
							CL.title AS objectTitle,
							CL.shortDescription AS objectDescription,
							NULL AS containedWithinObjectId,
							NULL AS containedWithinObjectType,
							NULL AS containedWithinObjectTitle
						FROM tblCourseLanguage CL
						LEFT JOIN tblCourse C ON C.idCourse = CL.idCourse
						INNER JOIN CONTAINSTABLE(tblCourseLanguage, *, @searchParam) K ON K.[key] = CL.idCourseLanguage
						WHERE C.idSite = @idCallerSite
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND C.isPublished = 1
						AND CL.idLanguage = @idCallerLanguage
						
						UNION SELECT 
							C.idCourse AS idObject,
							'course' AS objectType,
							C.coursecode AS objectCode,
							CL.title AS objectTitle,
							CL.shortDescription AS objectDescription,
							NULL AS containedWithinObjectId,
							NULL AS containedWithinObjectType,
							NULL AS containedWithinObjectTitle							
						FROM tblStandupTrainingInstance STI
						INNER JOIN CONTAINSTABLE(tblStandupTrainingInstance, *, @searchParam) K ON K.[key] = STI.idStandupTrainingInstance						
						INNER JOIN tblLessonToContentLink LCL ON LCL.idObject = STI.idStandUpTraining AND LCL.idContentType = 2
						INNER JOIN tblLesson L ON L.idLesson = LCL.idLesson
						INNER JOIN tblCourse C ON C.idCourse = L.idCourse
						INNER JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
						WHERE STI.idSite = @idCallerSite
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND C.isPublished = 1
				) MAIN
				ORDER BY MAIN.objectTitle	

				END

			-- lesson
			IF (@objectType = 'lesson')
				BEGIN

				SELECT DISTINCT
					MAIN.idObject,
					MAIN.objectType,
					MAIN.objectCode,
					MAIN.objectTitle,
					MAIN.objectDescription,
					MAIN.containedWithinObjectId,
					MAIN.containedWithinObjectType,
					MAIN.containedWithinObjectTitle
				FROM (
						SELECT 
							L.idLesson AS idObject,
							'lesson' AS objectType,
							NULL AS objectCode,
							LL.title AS objectTitle,
							LL.shortDescription AS objectDescription,
							L.idCourse AS containedWithinObjectId,
							'course' AS containedWithinObjectType,
							CL.title AS containedWithinObjectTitle
						FROM tblLessonLanguage LL
						LEFT JOIN tblLesson L ON L.idLesson = LL.idLesson
						INNER JOIN CONTAINSTABLE(tblLessonLanguage, *, @searchParam) K ON K.[key] = LL.idLessonLanguage
						LEFT JOIN tblCourse C ON C.idCourse = L.idCourse
						LEFT JOIN tblCourseLanguage CL ON CL.idCourse = L.idCourse AND CL.idLanguage = @idCallerLanguage
						WHERE L.idSite = @idCallerSite
						AND (L.isDeleted IS NULL OR L.isDeleted = 0)
						AND LL.idLanguage = @idCallerLanguage
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND C.isPublished = 1

						UNION SELECT 
							L.idLesson AS idObject,
							'lesson' AS objectType,
							NULL AS objectCode,
							LL.title AS objectTitle,
							LL.shortDescription AS objectDescription,
							L.idCourse AS containedWithinObjectId,
							'course' AS containedWithinObjectType,
							CL.title AS containedWithinObjectTitle
						FROM tblStandupTrainingInstance STI
						INNER JOIN CONTAINSTABLE(tblStandupTrainingInstance, *, @searchParam) K ON K.[key] = STI.idStandupTrainingInstance
						LEFT JOIN tblLessonToContentLink LCL ON LCL.idObject = STI.idStandUpTraining AND LCL.idContentType = 2
						LEFT JOIN tblLesson L ON L.idLesson = LCL.idLesson
						LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
						LEFT JOIN tblCourse C ON C.idCourse = L.idCourse
						LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
						WHERE STI.idSite = @idCallerSite
						AND (L.isDeleted IS NULL OR L.isDeleted = 0)
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						AND C.isPublished = 1
				) MAIN
				ORDER BY MAIN.objectTitle

				END

			-- learning path
			IF (@objectType = 'learningpath')
				BEGIN

				SELECT 
					LP.idLearningPath AS idObject,
					'learningpath' AS objectType,
					NULL AS objectCode,
					LPL.name AS objectTitle,
					LPL.shortDescription AS objectDescription,
					NULL AS containedWithinObjectId,
					NULL AS containedWithinObjectType,
					NULL AS containedWithinObjectTitle
				FROM tblLearningPathLanguage LPL
				LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPL.idLearningPath
				INNER JOIN CONTAINSTABLE(tblLearningPathLanguage, *, @searchParam) K ON K.[key] = LPL.idLearningPathLanguage
				WHERE LP.idSite = @idCallerSite
				AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
				AND LP.isPublished = 1
				AND LPL.idLanguage = @idCallerLanguage
				ORDER BY LPL.name

				END

			-- community
			IF (@objectType = 'community')
				BEGIN

				SELECT 
					G.idGroup AS idObject,
					'community' AS objectType,
					NULL AS objectCode,
					GL.name AS objectTitle,
					GL.shortDescription AS objectDescription,
					NULL AS containedWithinObjectId,
					NULL AS containedWithinObjectType,
					NULL AS containedWithinObjectTitle
				FROM tblGroupLanguage GL
				LEFT JOIN tblGroup G ON G.idGroup = GL.idGroup
				INNER JOIN CONTAINSTABLE(tblGroupLanguage, *, @searchParam) K ON K.[key] = GL.idGroupLanguage
				WHERE G.idSite = @idCallerSite
				AND G.isSelfJoinAllowed = 1
				AND GL.idLanguage = @idCallerLanguage
				ORDER BY GL.name

				END

			-- ilt
			IF (@objectType = 'ilt')
				BEGIN

				SELECT 
					ST.idStandupTraining AS idObject,
					'ilt' AS objectType,
					NULL AS objectCode,
					STL.title AS objectTitle,
					STL.[description] AS objectDescription,
					NULL AS containedWithinObjectId,
					NULL AS containedWithinObjectType,
					NULL AS containedWithinObjectTitle
				FROM tblStandupTrainingLanguage STL
				LEFT JOIN tblStandupTraining ST ON ST.idStandupTraining = STL.idStandupTraining
				INNER JOIN CONTAINSTABLE(tblStandupTrainingLanguage, *, @searchParam) K ON K.[key] = STL.idStandupTrainingLanguage
				WHERE ST.idSite = @idCallerSite
				AND ST.isStandaloneEnroll = 1
				AND STL.idLanguage = @idCallerLanguage
				ORDER BY STL.title

				END

			-- document
			IF (@objectType = 'document' AND @idCaller <> 0)
				BEGIN

				SELECT
					DRI.idDocumentRepositoryItem AS idObject,
					'document' AS objectType,
					NULL AS objectCode,
					DRI.[filename] AS objectTitle,
					DRIL.label AS objectDescription,
					DRI.idObject AS containedWithinObjectId,
					CASE WHEN DRI.idDocumentRepositoryObjectType = 1 THEN 'group'
						WHEN DRI.idDocumentRepositoryObjectType = 2 THEN 'course'
						WHEN DRI.idDocumentRepositoryObjectType = 3 THEN 'learningpath' END AS containedWithinObjectType,
					CASE WHEN DRI.idDocumentRepositoryObjectType = 1 THEN GL.name
						WHEN DRI.idDocumentRepositoryObjectType = 2 THEN CL.title
						WHEN DRI.idDocumentRepositoryObjectType = 3 THEN LPL.name END AS containedWithinObjectTitle
				FROM tblDocumentRepositoryItemLanguage DRIL
				LEFT JOIN tblDocumentRepositoryItem DRI ON DRI.idDocumentRepositoryItem = DRIL.idDocumentRepositoryItem
				INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItemLanguage, *, @searchParam) K ON K.[key] = DRIL.idDocumentRepositoryItemLanguage
				LEFT JOIN tblGroupLanguage GL ON GL.idGroup = DRI.idObject
				LEFT JOIN tblCourseLanguage CL ON CL.idCourse = DRI.idObject
				LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = DRI.idObject
				WHERE DRI.idSite = @idCallerSite
				ORDER BY DRI.label
				END

			END	
			
		-- drop temp tables
		DROP TABLE #InaccessibleCatalogs	

		SET @Return_Code = 0
		SET @Error_Description_Code = ''
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO