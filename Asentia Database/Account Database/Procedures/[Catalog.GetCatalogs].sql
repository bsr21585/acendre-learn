-- =====================================================================
-- PROCEDURE: [Catalog.GetCatalogs]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.GetCatalogs]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.GetCatalogs]
GO

/*

Returns a recordset of catalogs parented by a specified catalog, or belonging to the 
catalog root if @idParentCatalog is null.

*/
CREATE PROCEDURE [Catalog.GetCatalogs]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@idParentCatalog			INT,
	@showPrivateCatalogs		BIT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/*

	do not check caller site membership or caller permissions as
	this can be called by a non-logged-in user

	*/


	/*

	validate that the specified language exists, use the site default if not

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	IF @idCallerLanguage IS NULL
		BEGIN
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		END

	/*

	get the base catalogs we will be pulling and put them into a temporary table

	*/

	CREATE TABLE #Catalogs (
		idCatalog	INT,
		idSite		INT,
		idParent	INT,
		title		NVARCHAR(255),
		avatar		NVARCHAR(255),
		isPrivate	BIT,
		isClosed	BIT,
		costType	INT,
		cost		FLOAT,
		[order]		INT
	)

	INSERT INTO #Catalogs (
		idCatalog,
		idSite,
		idParent,
		title,
		avatar,
		isPrivate,
		isClosed,
		costType,
		cost,
		[order]
	)
	SELECT
		CA.idCatalog,
		CA.idSite,
		CA.idParent,
		CASE WHEN CAL.title IS NOT NULL THEN CAL.title ELSE CA.title END AS title,
		CA.avatar,
		CA.isPrivate,
		CA.isClosed,
		CA.costType,
		CA.cost,
		CA.[order]
	FROM tblCatalog CA
	LEFT JOIN tblCatalogLanguage CAL ON CAL.idCatalog = CA.idCatalog AND CAL.idLanguage = @idCallerLanguage
	WHERE CA.idSite = @idCallerSite
	AND (
			(@idParentCatalog IS NULL AND CA.idParent IS NULL)
			OR
			(@idParentCatalog IS NOT NULL AND CA.idParent = @idParentCatalog)
		)			

	/*

	get the catalog (child) counts for each catalog we will be pulling
	and put it into a temporary table

	*/
	
	CREATE TABLE #CatalogCatalogCounts (
		idCatalog INT,
		numCatalogs INT
	)

	INSERT INTO #CatalogCatalogCounts (
		idCatalog,
		numCatalogs
	)
	SELECT
		CA.idCatalog,
		(SELECT COUNT(1) FROM tblCatalog WHERE idParent = CA.idCatalog)
	FROM #Catalogs CA
	

	/*

	get the course counts for each catalog we will be pulling
	and put it into a temporary table
	
	*/
	
	CREATE TABLE #CatalogCourseCounts (
		idCatalog INT,
		numCourses INT
	)

	INSERT INTO #CatalogCourseCounts (
		idCatalog,
		numCourses
	)
	SELECT
		CA.idCatalog,
		(
			SELECT COUNT(1) 
			FROM tblCourseToCatalogLink CCL 
			LEFT JOIN tblCourse CO ON CO.idCourse = CCL.idCourse
			WHERE CCL.idCatalog = CA.idCatalog
			AND (CO.isDeleted IS NULL OR CO.isDeleted = 0)
			AND CO.isPublished = 1

		)
	FROM #Catalogs CA

	
	/*

	put the data together and return it

	*/
	
	CREATE TABLE #CatalogsToReturn (
		idCatalog		INT,
		idSite			INT,
		idParent		INT,
		title			NVARCHAR(255),
		avatar			NVARCHAR(255),
		numCatalogs		INT,
		numCourses		INT,
		isPrivate		BIT,
		isAccessible	BIT,
		isClosed		BIT,
		calculatedCost	FLOAT,
		[order]			INT
	)

	INSERT INTO #CatalogsToReturn (
		idCatalog,
		idSite,
		idParent,
		title,
		avatar,
		numCatalogs,
		numCourses,
		isPrivate,
		isAccessible,
		isClosed,
		calculatedCost,
		[order]
	)
	SELECT
		CA.idCatalog,
		CA.idSite,
		CA.idParent,
		CA.title,
		CA.avatar,
		CACAC.numCatalogs,
		CACOC.numCourses,
		CA.isPrivate,
		-- see how performance is with the below sub-query(s), we may want to change it
		-- but at the moment it seems fine
		CASE WHEN @idCaller = 1 OR CA.isPrivate = 0 OR CA.isPrivate IS NULL THEN
			CONVERT(BIT, 1)
		WHEN @idCaller = 0 THEN
			CONVERT(BIT, 0)
		ELSE
			CASE WHEN (
						SELECT COUNT(1) FROM tblUserToGroupLink UGL
						WHERE UGL.idUser = @idCaller
						AND UGL.idGroup IN (
										    SELECT CAGL.idGroup
											FROM tblCatalogAccessToGroupLink CAGL
											WHERE CAGL.idCatalog = CA.idCatalog
										   )
					  ) > 0 THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END
		END AS isAccessible,
		CA.isClosed,
		-- again, see how performance is with the below sub-query(s), we may want to change it
		-- but at the moment it seems fine
		CASE WHEN CA.costType = 1 THEN NULL -- FREE
			 WHEN CA.costType = 2 AND CA.cost IS NOT NULL THEN CAST(CA.cost AS DECIMAL(18,2)) -- FIXED COST
			 WHEN CA.costType = 3 THEN CAST((SELECT CONVERT(FLOAT, SUM(cost)) FROM tblCourse C WHERE (C.isDeleted IS NULL OR C.isDeleted = 0) AND C.isPublished = 1 AND (C.isClosed IS NULL OR C.isClosed = 0) AND C.idCourse IN (SELECT idCourse FROM tblCourseToCatalogLink WHERE idCatalog = CA.idCatalog)) AS DECIMAL(18,2)) -- SUM OF ALL COURSES
			 WHEN CA.costType = 4 THEN CAST(((SELECT CONVERT(FLOAT, SUM(cost)) FROM tblCourse C WHERE (C.isDeleted IS NULL OR C.isDeleted = 0) AND C.isPublished = 1 AND (C.isClosed IS NULL OR C.isClosed = 0) AND C.idCourse IN (SELECT idCourse FROM tblCourseToCatalogLink WHERE idCatalog = CA.idCatalog))) * (CA.cost / 100) AS DECIMAL(18,2)) -- PCT OFF SUM OF ALL COURSES
		ELSE NULL END AS calculatedCost,
		CA.[order]
	FROM #Catalogs CA	
	LEFT JOIN #CatalogCatalogCounts CACAC ON CACAC.idCatalog = CA.idCatalog
	LEFT JOIN #CatalogCourseCounts CACOC ON CACOC.idCatalog = CA.idCatalog

	/*

	cull the list of catalogs based on whether or not we're showing private catalogs
	and whether the user has access to those private catalogs

	*/

	IF (@showPrivateCatalogs = 0)
		BEGIN
		DELETE FROM #CatalogsToReturn WHERE isPrivate = 1 AND isAccessible = 0
		END
	
	/*

	return the data

	*/

	SELECT * FROM #CatalogsToReturn ORDER BY [order], title

	/*

	drop the temporary tables

	*/

	DROP TABLE #Catalogs
	DROP TABLE #CatalogCatalogCounts
	DROP TABLE #CatalogCourseCounts
	DROP TABLE #CatalogsToReturn


	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO