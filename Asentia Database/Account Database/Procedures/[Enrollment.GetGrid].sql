-- =====================================================================
-- PROCEDURE: [Enrollment.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.GetGrid]
GO

/*

Gets a listing of enrollments for a specified user. 

*/

CREATE PROCEDURE [Enrollment.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,
	
	@idUser					INT
)
AS
	BEGIN
		SET NOCOUNT ON

		CREATE TABLE #Enrollments (
			idEnrollment			INT,
			idCourse				INT,
			idUser					INT,
			code					NVARCHAR(255),
			title					NVARCHAR(255),
			idRuleSetEnrollment		INT,
			idGroupEnrollment		INT,
			dtStart					DATETIME,
			dtDue					DATETIME,
			dtExpires				DATETIME,
			dtCompleted				DATETIME,
			dtFirstLaunch			DATETIME,
			isLockedByPrerequisites	BIT
		)

		INSERT INTO #Enrollments (
			idEnrollment,
			idCourse,
			idUser,
			code,
			title,
			idRuleSetEnrollment,
			idGroupEnrollment,
			dtStart,
			dtDue,
			dtExpires,
			dtCompleted,
			dtFirstLaunch,
			isLockedByPrerequisites
		)
		SELECT
			E.idEnrollment,
			E.idCourse,
			E.idUser,
			E.code,
			E.title,
			E.idRuleSetEnrollment,
			E.idGroupEnrollment,
			E.dtStart,
			E.dtDue,
			CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
				NULL
			ELSE
				CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NOT NULL THEN
					E.dtExpiresFromFirstLaunch
				ELSE
					CASE WHEN E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
						E.dtExpiresFromStart
					ELSE
						CASE WHEN E.dtExpiresFromStart >= E.dtExpiresFromFirstLaunch THEN
							E.dtExpiresFromFirstLaunch
						ELSE
							E.dtExpiresFromStart
						END
					END
				END
			END AS [dtExpires],
			E.dtCompleted,
			E.dtFirstLaunch,
			E.isLockedByPrerequisites
		FROM tblEnrollment E
		WHERE
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite)
			)
		AND E.idUser = @idUser
		AND E.idActivityImport IS NULL -- DO NOT INCLUDE ACTIVITY IMPORT ENROLLMENT RECORDS
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #Enrollments E

			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						E.idEnrollment,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN dtStart END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtStart', 'dtExpires', 'dtDue') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtStart END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtDue END) END DESC,
							-- FORTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtExpires END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN dtStart END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue') THEN title END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtStart', 'dtExpires', 'dtExpires') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtStart END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtDue END) END,
							-- FORTH ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtExpires END) END
						)
						AS [row_number]
					FROM #Enrollments E			
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idEnrollment, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				E.idEnrollment,
				E.idCourse,
				E.code,
				E.title,
				CASE WHEN E.idRuleSetEnrollment IS NOT NULL THEN
					CASE WHEN RSE.recurInterval > 0 THEN
						2
					ELSE
						1
					END
				ELSE
					CASE WHEN E.idGroupEnrollment IS NOT NULL THEN
						3
					ELSE
						4
					END
				END AS enrollmentType,
				E.dtStart,
				E.dtDue,
				E.dtExpires,
				CASE WHEN E.dtCompleted IS NOT NULL THEN
					2
				ELSE
					CASE WHEN E.dtStart > GETUTCDATE() THEN
						5
					ELSE
						CASE WHEN (E.dtExpires IS NOT NULL AND E.dtExpires <= GETUTCDATE()) THEN
							4
						ELSE
							CASE WHEN (E.dtDue IS NOT NULL AND E.dtDue <= GETUTCDATE()) THEN
								3
							ELSE
								1
							END
						END
					END
				END AS enrollmentStatus,
				CASE WHEN E.dtFirstLaunch IS NOT NULL AND (C.isDeleted IS NULL OR C.isDeleted = 0) THEN
					convert(bit, 1)
				ELSE
					convert(bit, 0)
				END AS isResetOn,
				CASE WHEN E.isLockedByPrerequisites = 1 THEN
					convert(bit, [dbo].[EvaluateEnrollmentPrerequisiteLock](E.idEnrollment))
				ELSE
					convert(bit, 0)
				END AS togglePrerequisites,
				convert(bit, 1) AS isActivityOn,
				CASE WHEN E.dtCompleted IS NOT NULL THEN
					convert(bit, 0)
				ELSE
					convert(bit, 1) 
				END AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #Enrollments E ON E.idEnrollment = SelectedKeys.idEnrollment
			LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
			LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = E.idRuleSetEnrollment 
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #Enrollments E
			INNER JOIN CONTAINSTABLE(tblEnrollment, *, @searchParam) K ON K.[key] = E.idEnrollment
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						E.idEnrollment,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN dtStart END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtStart', 'dtExpires', 'dtDue') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtStart END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtDue END) END DESC,
							-- FORTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtExpires END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN dtStart END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue') THEN title END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtStart', 'dtExpires', 'dtExpires') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtStart END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtDue END) END,
							-- FORTH ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtStart', 'dtExpires', 'dtDue') THEN dtExpires END) END
						)
						AS [row_number]
					FROM #Enrollments E
					INNER JOIN CONTAINSTABLE(tblEnrollment, *, @searchParam) K ON K.[key] = E.idEnrollment					
					
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idEnrollment, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				E.idEnrollment,
				E.idCourse,
				E.code,
				E.title,
				CASE WHEN E.idRuleSetEnrollment IS NOT NULL THEN
					CASE WHEN RSE.recurInterval > 0 THEN
						2
					ELSE
						1
					END
				ELSE
					CASE WHEN E.idGroupEnrollment IS NOT NULL THEN
						3
					ELSE
						4
					END
				END AS enrollmentType,
				E.dtStart,
				E.dtDue,
				E.dtExpires,
				CASE WHEN E.dtCompleted IS NOT NULL THEN
					2
				ELSE
					CASE WHEN E.dtStart > GETUTCDATE() THEN
						5
					ELSE
						CASE WHEN (E.dtExpires IS NOT NULL AND E.dtExpires <= GETUTCDATE()) THEN
							4
						ELSE
							CASE WHEN (E.dtDue IS NOT NULL AND E.dtDue <= GETUTCDATE()) THEN
								3
							ELSE
								1
							END
						END
					END
				END AS enrollmentStatus,
				CASE WHEN E.dtFirstLaunch IS NOT NULL AND (C.isDeleted IS NULL OR C.isDeleted = 0) THEN
					convert(bit, 1)
				ELSE
					convert(bit, 0)
				END AS isResetOn,
				CASE WHEN E.isLockedByPrerequisites = 1 THEN
					convert(bit, [dbo].[EvaluateEnrollmentPrerequisiteLock](E.idEnrollment))
				ELSE
					convert(bit, 0)
				END AS togglePrerequisites,
				convert(bit, 1) AS isActivityOn,
				CASE WHEN E.dtCompleted IS NOT NULL THEN
					convert(bit, 0)
				ELSE
					convert(bit, 1) 
				END AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #Enrollments E ON E.idEnrollment = SelectedKeys.idEnrollment
			LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
			LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = E.idRuleSetEnrollment 
			ORDER BY SelectedKeys.[row_number]
			
			END

		-- DROP THE TEMPORARY TABLE
		DROP TABLE #Enrollments
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO