-- =====================================================================
-- PROCEDURE: [Role.RemovePermissions]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Role.RemovePermissions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Role.RemovePermissions]
GO

/*

Add or updates the optional language fields for the object 

*/

CREATE PROCEDURE [Role.RemovePermissions]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idRole					INT, 
	@idPermissions			NVARCHAR(MAX)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	DECLARE @Permissions TABLE (idPermission INT)
	INSERT INTO @Permissions (idPermission)
	SELECT V.s
	FROM [dbo].virtualIDTable (@idPermissions, ',') V
		
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF @idCaller = 1 OR @idCaller = 0
		SET @idCaller = @idCaller -- dont validate admin login
	ELSE IF @idCaller IS NULL
	OR @idCallerSite IS NULL
	OR @idCallerSite = 0
	OR @idCaller = 0
	OR (SELECT idSite FROM tblUser WHERE idUser = @idCaller) IS NULL 
	OR (SELECT idSite FROM tblUser WHERE idUser = @idCaller) <> @idCallerSite
	
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		RETURN 1
		END
		
	/*
	
	Validate caller permission
	
	*/
	
	IF @idCaller <> 1
	
		BEGIN
		
		IF (
			SELECT COUNT(1)
			FROM tblUserToPermissionLink UPL 
			WHERE UPL.idUser = @idCaller
			AND UPL.idPermission = @idPermission
			AND (UPL.idScope = @idRole OR UPL.idScope IS NULL) -- global permission or scoped to this object
			) = 0 
			
			BEGIN
			SELECT @Return_Code = 3
			RETURN 1
			END
		
				
		END
		
	/*
	
	validate that the specified caller language exists
	
	*/
	
	-- not used.
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblRole
		WHERE idRole = @idRole
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
		SELECT @Return_Code = 1
		RETURN 1 
		END
		
	IF (
		SELECT COUNT(1)
		FROM @Permissions PP
		LEFT JOIN tblPermission P ON P.idPermission = PP.idPermission
		WHERE P.idPermission IS NULL
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 1
		RETURN 1 
		END
		
	-- delete the permissions
	
	DELETE FROM tblRoleToPermissionLink
	WHERE idRole = @idRole
	AND idPermission IN (
		SELECT idPermission
		FROM @Permissions
	)
		
	SELECT @Return_Code = 0 --(0 is 'success')
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO