-- =====================================================================
-- PROCEDURE: [StandupTraining.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.Save]
GO

/*

Adds new standup training or updates existing standup training.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [StandupTraining.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idStandupTraining		INT				OUTPUT,
	@title					NVARCHAR(255),
	@avatar					NVARCHAR(255),
	@cost					FLOAT,
	@description			NVARCHAR(MAX),
	@objectives				NVARCHAR(MAX),
	@isStandaloneEnroll		BIT,
	@isRestrictedEnroll		BIT,
	@isRestrictedDrop		BIT,
	@searchTags				NVARCHAR(512),
	@shortcode				NVARCHAR(10)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED


	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			SET @Error_Description_Code = 'CourseSave_SpecifiedLanguageNotDefault'
			RETURN 1 
		END

	*/	

	/*
	
	validate uniqueness for shortcode, if specified

	*/	

	IF (
		SELECT COUNT(1)
		FROM tblStandupTraining
		WHERE idSite = @idCallerSite
		AND shortcode = @shortcode
		AND @shortcode IS NOT NULL
		AND @shortcode <> ''
		AND (isDeleted = 0 OR isDeleted IS NULL)
		AND (
			@idStandupTraining IS NULL
			OR @idStandupTraining <> @idStandupTraining
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'StandupTrainingSave_ShortcodeNotUnique'
		RETURN 1 
		END

	/*

	check XSS vulnerabilities

	*/

	-- description

	IF (@description LIKE '%<script%')
	BEGIN
	SELECT @Return_Code = 4
	SET @Error_Description_Code = 'StandupTrainingSave_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@description LIKE '%<object%')
	BEGIN
	SELECT @Return_Code = 4
	SET @Error_Description_Code = 'StandupTrainingSave_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@description LIKE '%<frame%')
	BEGIN
	SELECT @Return_Code = 4
	SET @Error_Description_Code = 'StandupTrainingSave_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@description LIKE '%<iframe%')
	BEGIN
	SELECT @Return_Code = 4
	SET @Error_Description_Code = 'StandupTrainingSave_DescTagNotAllowed_Iframe'
	RETURN 1 
	END

    -- objectives

	IF (@objectives LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'StandupTrainingSave_ObjTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@objectives LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'StandupTrainingSave_ObjTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@objectives LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'StandupTrainingSave_ObjTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@objectives LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'StandupTrainingSave_ObjTagNotAllowed_Iframe'
	RETURN 1 
	END	
		
	/*
	
	save the data
	
	*/
	
	IF (@idStandupTraining = 0 OR @idStandupTraining IS NULL)
		
		BEGIN
		
		-- insert the new standup training
		
		INSERT INTO tblStandupTraining (
			idSite, 
			title,
			[description],
			objectives,
			isStandaloneEnroll,
			isRestrictedEnroll,
			isRestrictedDrop,
			searchTags,
			avatar,
			cost,
			shortcode,
			dtCreated,
			dtModified
		)			
		VALUES (
			@idCallerSite, 
			@title,
			@description,
			@objectives,
			@isStandaloneEnroll,
			@isRestrictedEnroll,
			@isRestrictedDrop,
			@searchTags,
			@avatar,
			@cost,
			@shortcode,
			GETUTCDATE(),
			GETUTCDATE()
		)
		
		-- get the new standup training's id
		
		SELECT @idStandupTraining = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the standup training id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblStandUpTraining WHERE idStandUpTraining = @idStandupTraining AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idStandupTraining = @idStandupTraining
			SET @Return_Code = 1
			SET @Error_Description_Code = 'StandupTrainingSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing standup training's properties
		
		UPDATE tblStandUpTraining SET
			title = @title,
			[description] = @description,
			objectives = @objectives,
			isStandaloneEnroll = @isStandaloneEnroll,
			isRestrictedEnroll = @isRestrictedEnroll,
			isRestrictedDrop = @isRestrictedDrop,
			searchTags = @searchTags,
			avatar = @avatar,
			cost = @cost,
			shortcode = @shortcode,
			dtModified = GETUTCDATE()
		WHERE idStandUpTraining = @idStandupTraining
		AND idSite = @idCallerSite
		
		-- get the standup training's id 

		SELECT @idStandupTraining = @idStandupTraining
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblStandupTrainingLanguage STL WHERE STL.idStandUpTraining = @idStandupTraining AND STL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblStandupTrainingLanguage SET
			title = @title,
			[description] = @description,
			objectives = @objectives,
			searchTags = @searchTags
		WHERE idStandUpTraining = @idStandupTraining
		AND idLanguage = @idDefaultLanguage
		AND idSite = @idCallerSite

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblStandupTrainingLanguage (
			idSite,
			idStandUpTraining,
			idLanguage,
			title,
			[description],
			objectives,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idStandupTraining,
			@idDefaultLanguage,
			@title,
			@description,
			@objectives,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblStandupTrainingLanguage STL
			WHERE STL.idStandUpTraining = @idStandupTraining
			AND STL.idLanguage = @idDefaultLanguage
			AND STL.idSite = @idCallerSite
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO