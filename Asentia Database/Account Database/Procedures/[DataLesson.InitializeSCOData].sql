-- =====================================================================
-- PROCEDURE: [DataLesson.InitializeSCOData]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DataLesson.InitializeSCOData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.InitializeSCOData]
GO

/*

Creates a record in tblData-SCO for the lesson data if it doesn't already exist.

*/

CREATE PROCEDURE [DataLesson.InitializeSCOData]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idDataLesson			INT,
	@idDataSco				INT				OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that the lesson data exists

	*/

	IF (
		SELECT COUNT(1)
		FROM [tblData-Lesson]
		WHERE [idData-Lesson] = @idDataLesson
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataLessonInitializeSCOData_NoRecordFound'
		RETURN 1
	    END

	/*

	insert a sco data record if one does not already exist for the lesson data
	if one already exists, this just exits quietly

	*/
	
	DECLARE @idTimezone INT
	SELECT @idTimezone = idTimezone FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson

	IF (SELECT COUNT(1) FROM [tblData-SCO] WHERE [idData-Lesson] = @idDataLesson) = 0

		BEGIN
		INSERT INTO [tblData-SCO] (
			idSite,
			[idData-Lesson],
			idTimezone,
			completionStatus,
			successStatus
		)
		VALUES (
			@idCallerSite,
			@idDataLesson,
			@idTimezone,
			0,
			0
		)

		SET @idDataSco = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
		SET  @idDataSco = (SELECT TOP(1) [idData-SCO] from [tblData-SCO] where [idData-Lesson] = @idDataLesson)
		END
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

