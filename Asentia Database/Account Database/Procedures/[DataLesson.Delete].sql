-- =====================================================================
-- PROCEDURE: [DataLesson.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataLesson.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.Delete]
GO

/*

Deletes lesson data

*/

CREATE PROCEDURE [DataLesson.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@LessonDatas			IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @LessonDatas) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'DataLessonDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all lesson data exists within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @LessonDatas LL
		LEFT JOIN [tblData-Lesson] DL ON DL.[idData-Lesson] = LL.id 
		WHERE DL.idSite IS NULL
		OR DL.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataLessonDelete_NoRecordFound'	
		RETURN 1 
		END
		
	/*
	
	Validate if any of the lesson data has already been completed.
	
	*/

	IF (
		SELECT COUNT(1)
		FROM [tblData-Lesson] DL
		WHERE DL.dtCompleted IS NOT NULL 
		AND DL.[idData-Lesson] IN (
			SELECT LL.id
			FROM @LessonDatas LL
			)
		) > 0

		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = 'DataLessonDelete_LessonDataCompleted'
		RETURN 1 
		END
		
	/*
	
	delete the lesson data and all dependent referenced table records
	
	*/
	
	-- delete SCORM interactions
	DELETE FROM [tblData-SCOInt] 
	WHERE [idData-SCO] IN (SELECT [idData-SCO] 
						   FROM [tblData-SCO] 
						   WHERE [idData-Lesson] IN (SELECT id FROM @LessonDatas)
						  )
	
	-- delete SCORM objectives
	DELETE FROM [tblData-SCOObj] 
	WHERE [idData-SCO] IN (SELECT [idData-SCO] 
						   FROM [tblData-SCO] 
						   WHERE [idData-Lesson] IN (SELECT id FROM @LessonDatas)
						   )

	-- delete SCORM lesson data
	DELETE FROM [tblData-SCO] 
	WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
						      FROM [tblData-Lesson] 
							  WHERE [idData-Lesson] IN (SELECT id FROM @LessonDatas)
							 )

	-- delete lesson data
	DELETE FROM [tblData-Lesson] 
	WHERE [idData-Lesson] IN (SELECT id FROM @LessonDatas)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO