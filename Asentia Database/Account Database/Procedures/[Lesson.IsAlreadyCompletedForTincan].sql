-- =====================================================================
-- PROCEDURE: [Lesson.IsAlreadyCompletedForTincan]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Lesson.IsAlreadyCompletedForTincan]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Lesson.IsAlreadyCompletedForTincan]
GO

/*

Adds Tin Can statement(s) context activities.

*/

CREATE PROCEDURE [Lesson.IsAlreadyCompletedForTincan]
(
	@Return_Code			INT								OUTPUT,
	@Error_Description_Code	NVARCHAR(50)					OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idDataLesson		    INT,
	@completed              BIT								OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*
	Checking if lesson is already completedthere there will be no further recording of statements in databse (In case of inter package launch only)
	*/

	SELECT @completed = 0
	 
	 IF EXISTS (SELECT 1 FROM [tblData-Lesson] 
		   WHERE [idData-Lesson] = @idDataLesson 
		   AND dtCompleted IS NOT NULL)
		   BEGIN
		   SELECT @completed = 1 
		   END
	
	

	SELECT @Return_Code = 0 --Success
	SELECT @Error_Description_Code = NULL
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO