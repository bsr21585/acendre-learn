-- =====================================================================
-- PROCEDURE: [CertificationModuleRequirement.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificationModuleRequirement.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModuleRequirement.Save]
GO

/*

Adds new certification module requirement or updates existing certification module requirement.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [CertificationModuleRequirement.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idCertificationModuleRequirement					INT				OUTPUT,	
	@idCertificationModuleRequirementSet				INT,
	@label												NVARCHAR(255),
	@shortDescription									NVARCHAR(512),
	@requirementType									INT,
	@courseCompletionIsAny								BIT,
	@forceCourseCompletionInOrder						BIT,
	@numberCreditsRequired								FLOAT,
	@documentUploadFileType								INT,
	@documentationApprovalRequired						BIT,
	@allowSupervisorsToApproveDocumentation				BIT,
	@allowExpertsToApproveDocumentation					BIT,
	@courseCreditEligibleCoursesIsAll					BIT	
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED


	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'CertificationModuleRequirementSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/
		
	/*
	
	save the data
	
	*/
	
	IF (@idCertificationModuleRequirement = 0 OR @idCertificationModuleRequirement IS NULL)
		
		BEGIN
		
		-- insert the new certification module requirement
		
		INSERT INTO tblCertificationModuleRequirement (
			idCertificationModuleRequirementSet,
			idSite,
			label,
			shortDescription,
			requirementType,
			courseCompletionIsAny,
			forceCourseCompletionInOrder,
			numberCreditsRequired,
			documentUploadFileType,
			documentationApprovalRequired,
			allowSupervisorsToApproveDocumentation,
			allowExpertsToApproveDocumentation,
			courseCreditEligibleCoursesIsAll
		)			
		VALUES (
			@idCertificationModuleRequirementSet,
			@idCallerSite,
			@label,
			@shortDescription,
			@requirementType,
			@courseCompletionIsAny,
			@forceCourseCompletionInOrder,
			@numberCreditsRequired,
			@documentUploadFileType,
			@documentationApprovalRequired,
			@allowSupervisorsToApproveDocumentation,
			@allowExpertsToApproveDocumentation,
			@courseCreditEligibleCoursesIsAll
		)
		
		-- get the new certification module requirement's id
		
		SET @idCertificationModuleRequirement = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the certification module requirement id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblCertificationModuleRequirement WHERE idCertificationModuleRequirement = @idCertificationModuleRequirement AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idCertificationModuleRequirement = @idCertificationModuleRequirement
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertificationModuleRequirementSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing certification module requirement's properties
		
		UPDATE tblCertificationModuleRequirement SET			
			label = @label,
			shortDescription = @shortDescription,
			requirementType = @requirementType,
			courseCompletionIsAny = @courseCompletionIsAny,
			forceCourseCompletionInOrder = @forceCourseCompletionInOrder,
			numberCreditsRequired = @numberCreditsRequired,
			documentUploadFileType = @documentUploadFileType,
			documentationApprovalRequired = @documentationApprovalRequired,
			allowSupervisorsToApproveDocumentation = @allowSupervisorsToApproveDocumentation,
			allowExpertsToApproveDocumentation = @allowExpertsToApproveDocumentation,
			courseCreditEligibleCoursesIsAll = @courseCreditEligibleCoursesIsAll
		WHERE idCertificationModuleRequirement = @idCertificationModuleRequirement

		-- get the certification module requirement's id 

		SET @idCertificationModuleRequirement = @idCertificationModuleRequirement
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblCertificationModuleRequirementLanguage CMRL WHERE CMRL.idCertificationModuleRequirement = @idCertificationModuleRequirement AND CMRL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblCertificationModuleRequirementLanguage SET
			label = @label,
			shortDescription = @shortDescription
		WHERE idCertificationModuleRequirement = @idCertificationModuleRequirement
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCertificationModuleRequirementLanguage (
			idSite,
			idCertificationModuleRequirement,
			idLanguage,
			label,
			shortDescription
		)
		SELECT
			@idCallerSite,
			@idCertificationModuleRequirement,
			@idDefaultLanguage,
			@label,
			@shortDescription
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCertificationModuleRequirementLanguage CMRL
			WHERE CMRL.idCertificationModuleRequirement = @idCertificationModuleRequirement
			AND CMRL.idLanguage = @idDefaultLanguage
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO