-- =====================================================================
-- PROCEDURE: [Catalog.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.Save]
GO

/*

Adds new catalog or updates existing catalog.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [Catalog.Save]
(
	@Return_Code			INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)		OUTPUT,
	@idCallerSite			INT					= 0,	-- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT					= 0,	-- will fail if not specified
	
	@idCatalog				INT					OUTPUT, 
	@idParent				INT, 
	@title					NVARCHAR(255), 
	@order					INT,
	@shortDescription		NVARCHAR(512), 
	@longDescription		NVARCHAR(MAX), 
	@isPrivate				BIT, 
	@isClosed				BIT,						--self-enrollable/purchasable
	@cost					FLOAT,
	@costType				INT,
	@searchTags				NVARCHAR(512),
	@avatar					NVARCHAR(255),
	@shortcode				NVARCHAR(10)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED	-- took reference from Group.Save

	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1)
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			RETURN 1 
		END
	*/	
	
			 
	/*
	
	validate uniqueness within same parent object
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCatalog
		WHERE idSite = @idCallerSite
		AND title = @title
		AND (
			@idCatalog IS NULL
			OR @idCatalog <> idCatalog
			)
		AND ( -- within same parent (or root)
			(@idParent IS NULL AND idParent IS NULL)
			OR (@idParent = idParent)
			)
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'CatalogSave_CatalogNotUnique'		
		RETURN 1 
		END
		
	/*
	
	validate that catalog is not its own parent
	
	*/
	
	IF (@idParent = @idCatalog) 
		BEGIN
		SELECT @Return_Code = 4
		SET @Error_Description_Code = 'CatalogSave_FieldConstraintError'
		RETURN 1 
		END


	/*
	
	validate uniqueness within language
	
	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF( 
		SELECT COUNT(1)
		FROM tblCatalog C
		LEFT JOIN tblCatalogLanguage CL ON CL.idCatalog = C.idCatalog AND CL.idLanguage = @idDefaultLanguage -- same language
		WHERE C.idSite = @idCallerSite -- same site
		AND C.idCatalog <> @idCatalog -- not the same group
		AND CL.title = @title -- validate parameter: title
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'CatalogSave_FieldNotUnique'
		RETURN 1
		END
		
	/*
	
	validate uniqueness for shortcode, if specified

	*/	

	IF (
		SELECT COUNT(1)
		FROM tblCatalog
		WHERE idSite = @idCallerSite
		AND shortcode = @shortcode
		AND @shortcode IS NOT NULL
		AND @shortcode <> ''
		AND (
			idCatalog IS NULL
			OR idCatalog <> idCatalog
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'CatalogSave_ShortcodeNotUnique'
		RETURN 1 
		END	
				
	/*
	
	save the data
	
	*/
	
	IF (@idCatalog = 0 OR @idCatalog is null)
		
		BEGIN
		
		-- insert the new catalog
		
		INSERT INTO tblCatalog (
			idSite, 
			idParent, 
			title, 
			[order],
			shortDescription, 
			longDescription, 
			isPrivate,
			isClosed,
			dtCreated,
			dtModified,
			cost,
			costType,
			searchTags,
			avatar,
			shortcode
		)			
		VALUES (
			@idCallerSite, 
			@idParent, 
			@title,
			@order,
			@shortDescription,
			@longDescription,
			@isPrivate,
			@isClosed,
			GETUTCDATE(),
			GETUTCDATE(),
			@cost,
			@costType,
			@searchTags,
			@avatar,
			@shortcode
		)
		
		-- get the new catalog's id and return successfully
		
		SELECT @idCatalog = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the catalog id exists
		
		IF (SELECT COUNT(1) FROM tblCatalog WHERE idCatalog = @idCatalog) < 1
			
			BEGIN
				SELECT @idCatalog = @idCatalog
				SELECT @Return_Code = 1
				SET @Error_Description_Code = 'CatalogSave_NoRecordFound'
				RETURN 1
			END
			
		-- update existing catalog's properties
		
		UPDATE tblCatalog SET
			idParent				= @idParent, 
			title					= @title, 
			shortDescription		= @shortDescription, 
			longDescription			= @longDescription, 
			isPrivate				= @isPrivate,
			isClosed				= @isClosed,
			dtModified				= GETUTCDATE(),
			cost					= @cost,
			costType				= @costType,
			searchTags				= @searchTags,
			avatar					= @avatar,
			shortcode				= @shortcode
		WHERE idCatalog	= @idCatalog
		
		-- get the id and return successfully
		
		SELECT @idCatalog = @idCatalog
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblCatalogLanguage CL WHERE CL.idCatalog = @idCatalog AND CL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN
			
		UPDATE tblCatalogLanguage SET
			title				= @title,
			shortDescription	= @shortDescription,
			longDescription		= @longDescription,
			searchTags			= @searchTags
		WHERE idCatalog = @idCatalog
		AND idLanguage = @idDefaultLanguage
	
		END
		
	ELSE
	
		BEGIN
	
		INSERT INTO tblCatalogLanguage (
			idSite, -- this field added by chetu on 6th Sep 2014 as procedure was throwing exception
			idCatalog,
			idLanguage,
			title,
			shortDescription,
			longDescription,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idCatalog,
			@idDefaultLanguage,
			@title,
			@shortDescription,
			@longDescription,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCatalogLanguage CL
			WHERE CL.idCatalog = @idCatalog
			AND CL.idLanguage = @idDefaultLanguage
		)
		
		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		
