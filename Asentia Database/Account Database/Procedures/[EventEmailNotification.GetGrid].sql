SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventEmailNotification.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventEmailNotification.GetGrid]
GO

/*

Gets a listing of event email notifications.

*/

CREATE PROCEDURE [EventEmailNotification.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,
	@eventType				INT				= NULL,
	@idObject				INT				= NULL
)
AS
	BEGIN
		SET NOCOUNT ON
		
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblEventEmailNotification E 
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite)
				)	
				AND	
				(
					( @idObject IS NULL AND E.idObject IS NULL
					)
					OR  
					( @idObject IS NOT NULL AND @eventType = 2 AND LEFT(CAST(E.idEventType AS varchar(3)),1) IN (2,3) AND E.idObject = @idObject
					)
					OR  
					( @idObject IS NOT NULL AND LEFT(CAST(E.idEventType AS varchar(3)),1) = @eventType AND E.idObject = @idObject
					)
				)
				AND (E.isDeleted IS NULL OR E.isDeleted = 0)
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						E.idEventEmailNotification,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN E.name END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN E.name END
						)
						AS [row_number]
					FROM tblEventEmailNotification E
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite)
						)
						AND	
						(
							( @idObject IS NULL AND E.idObject IS NULL
							)
							OR  
							( @idObject IS NOT NULL AND @eventType = 2 AND LEFT(CAST(E.idEventType AS varchar(3)),1) IN (2,3) AND E.idObject = @idObject
							)
							OR  
							( @idObject IS NOT NULL AND LEFT(CAST(E.idEventType AS varchar(3)),1) = @eventType AND E.idObject = @idObject
							)
						)
						AND (E.isDeleted IS NULL OR E.isDeleted = 0)
				),	
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idEventEmailNotification, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				E.idEventEmailNotification, 
				CASE WHEN EL.name IS NOT NULL THEN EL.name ELSE E.name END AS name, 
				E.idEventType AS eventType,
				E.idObject AS idObject,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblEventEmailNotification E ON E.idEventEmailNotification = SelectedKeys.idEventEmailNotification
			LEFT JOIN tblEventEmailNotificationLanguage EL ON EL.idEventEmailNotification = E.idEventEmailNotification AND EL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblEventEmailNotification E 
				INNER JOIN CONTAINSTABLE(tblEventEmailNotification, *, @searchParam) K ON K.[key] = E.idEventEmailNotification
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite)
					)
					AND	
					(
						( @idObject IS NULL AND E.idObject IS NULL
						)
						OR  
						( @idObject IS NOT NULL AND @eventType = 2 AND LEFT(CAST(E.idEventType AS varchar(3)),1) IN (2,3) AND E.idObject = @idObject
						)
						OR  
						( @idObject IS NOT NULL AND LEFT(CAST(E.idEventType AS varchar(3)),1) = @eventType AND E.idObject = @idObject
						)
					)
					AND (E.isDeleted IS NULL OR E.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							E.idEventEmailNotification,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN E.name END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN E.name END
							)
							AS [row_number]
						FROM tblEventEmailNotification E
						INNER JOIN CONTAINSTABLE(tblEventEmailNotification, *, @searchParam) K ON K.[key] = E.idEventEmailNotification
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite
							)
							AND	
							(
								( @idObject IS NULL AND E.idObject IS NULL
								)
								OR  
								( @idObject IS NOT NULL AND @eventType = 2 AND LEFT(CAST(E.idEventType AS varchar(3)),1) IN (2,3) AND E.idObject = @idObject
								)
								OR  
								( @idObject IS NOT NULL AND LEFT(CAST(E.idEventType AS varchar(3)),1) = @eventType AND E.idObject = @idObject
								)
							)
							AND (E.isDeleted IS NULL OR E.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idEventEmailNotification, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					E.idEventEmailNotification, 
					CASE WHEN EL.name IS NOT NULL THEN EL.name ELSE E.name END AS name, 
					E.idEventType AS eventType,
					E.idObject AS idObject,
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblEventEmailNotification E ON E.idEventEmailNotification = SelectedKeys.idEventEmailNotification
				LEFT JOIN tblEventEmailNotificationLanguage EL ON EL.idEventEmailNotification = E.idEventEmailNotification AND EL.idLanguage = @idCallerLanguage
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblEventEmailNotificationLanguage EL
				LEFT JOIN tblEventEmailNotification E ON E.idEventEmailNotification = EL.idEventEmailNotification
				INNER JOIN CONTAINSTABLE(tblGroupLanguage, *, @searchParam) K ON K.[key] = EL.idEventEmailNotificationLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite)
					)
					AND EL.idLanguage = @idCallerLanguage
					AND	
					(
						( @idObject IS NULL AND E.idObject IS NULL
						)
						OR  
						( @idObject IS NOT NULL AND @eventType = 2 AND LEFT(CAST(E.idEventType AS varchar(3)),1) IN (2,3) AND E.idObject = @idObject
						)
						OR  
						( @idObject IS NOT NULL AND LEFT(CAST(E.idEventType AS varchar(3)),1) = @eventType AND E.idObject = @idObject
						)
					)
					AND (E.isDeleted IS NULL OR E.isDeleted = 0)
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							EL.idEventEmailNotificationLanguage,
							E.idEventEmailNotification,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN EL.name END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN EL.name END
							)
							AS [row_number]
						FROM tblEventEmailNotificationLanguage EL
						LEFT JOIN tblEventEmailNotification  E ON E.idEventEmailNotification = EL.idEventEmailNotification
						INNER JOIN CONTAINSTABLE(tblEventEmailNotificationLanguage, *, @searchParam) K ON K.[key] = idEventEmailNotificationLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND E.idSite = @idCallerSite
							)
							AND EL.idLanguage = @idCallerLanguage
							AND	
							(
								( @idObject IS NULL AND E.idObject IS NULL
								)
								OR  
								( @idObject IS NOT NULL AND @eventType = 2 AND LEFT(CAST(E.idEventType AS varchar(3)),1) IN (2,3) AND E.idObject = @idObject
								)
								OR  
								( @idObject IS NOT NULL AND LEFT(CAST(E.idEventType AS varchar(3)),1) = @eventType AND E.idObject = @idObject
								)
							)
							AND (E.isDeleted IS NULL OR E.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idEventEmailNotification, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					E.idEventEmailNotification,
					CASE WHEN EL.name IS NOT NULL THEN EL.name ELSE E.name END AS name, 
					E.idEventType AS eventType,
					E.idObject AS idObject,
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblEventEmailNotification E ON E.idEventEmailNotification = SelectedKeys.idEventEmailNotification
				LEFT JOIN tblEventEmailNotificationLanguage EL ON EL.idEventEmailNotification = SelectedKeys.idEventEmailNotification AND EL.idLanguage = @idCallerLanguage
				ORDER BY SelectedKeys.[row_number]

				END
			
			END
			
	END
