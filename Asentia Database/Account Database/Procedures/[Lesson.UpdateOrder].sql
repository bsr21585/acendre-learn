-- =====================================================================
-- PROCEDURE: [Lesson.UpdateOrder]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Lesson.UpdateOrder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Lesson.UpdateOrder]
GO
	
/*

Procedure to update the order of lessons.

*/

CREATE PROCEDURE [Lesson.UpdateOrder]
(	
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT						= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT						= 0,
	
	@LessonOrdering			IDTableWithOrdering		READONLY
)
AS

	BEGIN
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	
	/*

	make sure there are objects in the table parameter

	*/
	
	IF ((SELECT COUNT(1) FROM @LessonOrdering) = 0  )
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'LessonUpdateOrder_NoRecordsToUpdate'
		RETURN 1
		END		
	
	/*
	
	update lesson order
	
	*/		

	UPDATE tblLesson SET
		[order] = LO.[order],
		dtModified = GETUTCDATE()
	FROM @LessonOrdering LO
	WHERE LO.id = tblLesson.idLesson

	/*

	update the course last modified date

	*/

	UPDATE tblCourse SET 
		dtModified = GETUTCDATE()
	WHERE idCourse IN (SELECT DISTINCT idCourse FROM tblLesson WHERE idLesson IN (SELECT id FROM @LessonOrdering))

	/*

	return

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO