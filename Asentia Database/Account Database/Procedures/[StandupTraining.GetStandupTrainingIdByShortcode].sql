-- =====================================================================
-- PROCEDURE: [StandupTraining.GetStandupTrainingIdByShortcode]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.GetStandupTrainingIdByShortcode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.GetStandupTrainingIdByShortcode]
GO

/*

Gets an instructor led training id based on the shortcode

*/

CREATE PROCEDURE [StandupTraining.GetStandupTrainingIdByShortcode]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@shortcode					NVARCHAR(10),
	@idStandupTraining			INT				OUTPUT
AS
	BEGIN	
	SET NOCOUNT ON

    SELECT
		@idStandupTraining = ST.idStandupTraining
	FROM tblStandupTraining ST
	WHERE ST.shortcode = @shortcode
	AND ST.idSite = @idCallerSite

	IF (@idStandupTraining IS NULL)
		BEGIN
		SET @idStandupTraining = 0
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		
