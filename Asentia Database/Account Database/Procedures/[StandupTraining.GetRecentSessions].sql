-- =====================================================================
-- PROCEDURE: [StandupTraining.GetRecentSessions]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.GetRecentSessions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.GetRecentSessions]
GO

/*

Gets a listing of the 5 most recent past Standup Training Instances that belong to a Standup Training Module.

*/

CREATE PROCEDURE [StandupTraining.GetRecentSessions]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
		
	@idStandupTraining			INT	
)
AS
	BEGIN
		SET NOCOUNT ON
		
		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString		

		/*

		put the standup training instances into a temp table with their start dates

		*/

		CREATE TABLE #StandupTrainingInstances (
			idStandupTrainingInstance	INT,
			title						NVARCHAR(255),
			seats						INT,
			seatsFilled					INT,
			waitingSeats				INT,
			waitingSeatsFilled			INT,
			[type]						INT,
			dtStart						DATETIME,
			idTimezone					INT
		)

		INSERT INTO #StandupTrainingInstances (
			idStandupTrainingInstance,
			title,
			seats,
			seatsFilled,
			waitingSeats,
			waitingSeatsFilled,
			[type],
			dtStart,
			idTimezone
		)
		SELECT
			STI.idStandupTrainingInstance,
			STI.title,
			STI.seats,
			(SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = STI.idStandUpTrainingInstance AND isWaitingList = 0),
			STI.waitingSeats,
			(SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = STI.idStandUpTrainingInstance AND isWaitingList = 1),
			STI.[type],
			(SELECT MIN(dtStart) FROM tblStandUpTrainingInstanceMeetingTime WHERE idStandUpTrainingInstance = STI.idStandUpTrainingInstance),
			(SELECT DISTINCT idTimezone FROM tblStandUpTrainingInstanceMeetingTime WHERE idStandUpTrainingInstance = STI.idStandUpTrainingInstance)
		FROM tblStandUpTrainingInstance STI
		WHERE 
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND STI.idSite = @idCallerSite)
			)
		AND STI.idStandUpTraining = @idStandupTraining
		AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)

		/*

		get the data

		*/

		SELECT TOP 5
			STI.idStandupTrainingInstance,
			CASE WHEN STIL.title IS NOT NULL THEN STIL.title ELSE STI.title END AS title,
			STI.[type],
			STI.seats,
			STI.seatsFilled,
			STI.waitingSeats,
			STI.waitingSeatsFilled,
			STI.dtStart,
			STI.idTimezone,
			CONVERT(BIT, 1) AS isModifyOn -- needs to be calculated
		FROM #StandupTrainingInstances STI		
		LEFT JOIN tblStandupTrainingInstanceLanguage STIL ON STIL.idStandupTrainingInstance = STI.idStandUpTrainingInstance AND STIL.idLanguage = @idCallerLanguage
		WHERE STI.dtStart <= GETUTCDATE()
		ORDER BY STI.dtStart DESC
		
		-- drop the temp table
		DROP TABLE #StandupTrainingInstances

		/*

		return

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO