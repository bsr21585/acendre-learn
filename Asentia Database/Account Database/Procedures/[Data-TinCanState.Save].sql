-- =====================================================================
-- PROCEDURE: [Data-TinCanState.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCanState.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCanState.Save]
GO

/*

Adds a new tin can state or updates an existing tin can state.

*/

CREATE PROCEDURE [Data-TinCanState.Save]
(
	@Return_Code			INT								OUTPUT,
	@Error_Description_Code	NVARCHAR(50)					OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idEndpoint				INT,
	@idData_TinCanState		INT								OUTPUT,
	@stateId				NVARCHAR(100),
	@agent					NVARCHAR(MAX),
	@activityId				NVARCHAR(100),
	@registration			NVARCHAR(50),
	@contentType			NVARCHAR(50),
	@docContents			VARBINARY(MAX),
	@dtUpdated				DATETIME
)
AS
	BEGIN
	SET NOCOUNT ON

	IF (@idEndpoint = 0)
     SET @idEndpoint	=	NUll

	-- Get id if record exists
	SELECT @idData_TinCanState = [idData-TinCanState]
	FROM [tblData-TinCanState]
	WHERE idSite = @idCallerSite
	AND stateId = @stateId
	AND activityId = @activityId
	AND agent = @agent
	AND (registration = @registration OR (@registration IS NULL AND registration IS NULL))
	
	-- Update record if already exists otherwise insert a new record.
	IF (@idData_TinCanState IS NULL OR @idData_TinCanState = 0)
		BEGIN
		INSERT INTO [tblData-TinCanState] (
			idSite,
			idEndpoint,
			stateId,
			agent,
			activityId,
			registration,
			contentType,
			docContents,
			dtUpdated
		)
		VALUES (
			@idCallerSite,
			@idEndpoint,
			@stateId,
			@agent,
			@activityId,
			@registration,
			@contentType,
			@docContents,
			@dtUpdated
		)
		
		-- get the new tin can state id and return successfully
		SELECT @idData_TinCanState = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
		
		/*
		
		validate caller permission to the specified record
		
		*/
		
		--IF(
		--	SELECT COUNT(1)
		--	FROM [tblData-TinCanState]
		--	WHERE idSite = @idCallerSite
		--	AND idEndpoint = @idEndpoint
		--	AND stateId = @stateId
		--	AND activityId = @activityId
		--	AND agent = @agent
		--	AND (registration = @registration OR (@registration IS NULL AND registration IS NULL))
		--	) = 0
		--	BEGIN
		--	SELECT @Return_Code = 3
		--	SET @Error_Description_Code = 'TinCanStateSave_PermissionError'
		--	RETURN 1
		--	END
		
		UPDATE [tblData-TinCanState] SET
			docContents = @docContents,
			contentType = @contentType,
			dtUpdated = @dtUpdated
		WHERE [idData-TinCanState] = @idData_TinCanState
		END
	
	SELECT @Return_Code = 0 --Success
	SELECT @Error_Description_Code = NULL
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO