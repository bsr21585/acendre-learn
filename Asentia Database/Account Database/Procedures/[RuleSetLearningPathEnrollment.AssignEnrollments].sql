-- =====================================================================
-- PROCEDURE: [RuleSetLearningPathEnrollment.AssignEnrollments]

/*

RuleSet Learning Path Enrollment Inheritence Rules

1. User has no enrollment(s) of the learning path, manually-assigned or otherwise.
	- Grant RuleSet-inherited Enrollment

2. User has a manually-assigned (direct) enrollment that has been completed.
	- Do nothing.

3. User has a manually-assigned (direct) enrollment that is currently active.
	- Do nothing.

4. User has a RuleSet-inherited enrollment that is the same as the RuleSet Enrollment they are matched to.
	- Do nothing.

5. User has a RuleSet-inherited enrollment that is different than the RuleSet Enrollment they are matched to.

	If new is one-time and old is one-time:
		
		If old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment
		
		If old RuleSet Enrollment is completed:
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from completed enrollment, i.e. make it appear manually-assigned.

IMPORTANT NOTE: 

SYNCHRONIZATION OF LEARNING PATH DATA TO ENROLLMENT, ASSIGNMENT OF COURSE ENROLLMENTS TO LEARNING PATH ENROLLMENT AND SYNCHRONIZATION OF 
LESSON DATA TO COURSE ENROLLMENTS WILL ALL TAKE PLACE IN OTHER PROCEDURES. THIS PROCEDURE ONLY HANDLES THE
ASSIGNMENT OF RULESET-INHERITED ENROLLMENTS.

*/

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.AssignEnrollments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetLearningPathEnrollment.AssignEnrollments]
GO

/*

Assigns learning path RuleSet Enrollment enrollments to users based on rule matches and 
RuleSet Enrollment priorities using the rules outlined above.

Notes:

@LearningPaths	- IDTable of Learning Path ids to assign RuleSet Enrollments for.
				  Required to be passed, but not required to be populated.
				  If not populated, this will be executed on ALL Learning Paths
				  that have RuleSet Enrollments.

@Filters		- IDTable of User or Group ids (see @filterBy param) to
				  assign RuleSet Enrollments for.
				  Required to be passed, but not required to be populated.
				  If not populated, this will be executed on ALL Users or
				  Groups (see @filterBy param).

@filterBy		- Defines the object(s), User or Group, that we are executing
				  this for. If "Group," we are just getting the users that are
				  members of the specified Group(s) to execute this on. There
				  is no direct RuleSet Enrollment to Group inheritance, just
				  rules that can say "if user is a member of ...". Essentially,
				  the "Group" filter only gets used when we are calling this
				  after adding or removing User(s) from Group(s).

The parameters described above are all passed through to the [System.GetRuleSetLearningPathMatches]
procedure, which then returns a recordset of prioritized user-to-ruleset enrollment matches
filtered down to the LearningPath(s) and User(s) resulting from applying the filters above.

*/

CREATE PROCEDURE [RuleSetLearningPathEnrollment.AssignEnrollments]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,

	@LearningPaths				IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/*

	Execute [System.GetRuleSetLearningPathMatches] and dump the results into a temp table.

	*/

	CREATE TABLE #RuleSetLearningPathEnrollmentMatches
	(
		idSite							INT, 
		idRuleSetLearningPathEnrollment INT, 
		[priority]						INT, 
		idRuleSet						INT, 
		idLearningPath					INT, 
		idUser							INT
	)

	INSERT INTO #RuleSetLearningPathEnrollmentMatches
	EXEC [System.GetRuleSetLearningPathMatches] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @LearningPaths, @Filters, @filterBy

	/*

	Get all enrollments for all users and learning paths returned by the [System.GetRuleSetLearningPathMatches] execution
	and place them in a temp table. We do this to create a working environment for applying the RuleSet Enrollment
	inheritance rules so we are not constantly reading from (and therefore locking) the Learning Path Enrollment table.

	*/

	DECLARE @MatchedLearningPaths IDTable
	INSERT INTO @MatchedLearningPaths (id) SELECT DISTINCT idLearningPath FROM #RuleSetLearningPathEnrollmentMatches

	DECLARE @MatchedUsers IDTable
	INSERT INTO @MatchedUsers (id) SELECT DISTINCT idUser FROM #RuleSetLearningPathEnrollmentMatches

	/* 
	
	Replica of tblLearningPathEnrollment minus idTimezone, name, and all intervals.
	We may weed out more fields later as we discover they are not needed. 

	*/

	CREATE TABLE #CurrentLearningPathEnrollments
	(
		idLearningPathEnrollment				INT,
		idSite									INT,
		idLearningPath							INT,
		idUser									INT,
		idRuleSetLearningPathEnrollment			INT,
		dtStart									DATETIME,
		dtDue									DATETIME,
		dtCompleted								DATETIME,
		dtCreated								DATETIME
	)

	INSERT INTO #CurrentLearningPathEnrollments
	(
		idLearningPathEnrollment,
		idSite,
		idLearningPath,
		idUser,
		idRuleSetLearningPathEnrollment,
		dtStart,
		dtDue,
		dtCompleted,
		dtCreated
		)
	SELECT
		E.idLearningPathEnrollment,
		E.idSite,
		E.idLearningPath,
		E.idUser,
		E.idRuleSetLearningPathEnrollment,
        E.dtStart,
		E.dtDue,
		E.dtCompleted,
		E.dtCreated
	FROM tblLearningPathEnrollment E
	WHERE E.idLearningPath IN (SELECT id FROM @MatchedLearningPaths)
	AND E.idUser IN (SELECT id FROM @MatchedUsers)
	
	/*

	Get UTC NOW.

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	Eliminate RuleSet Learning Path Enrollment matches where the user already has a manually-assigned (direct) enrollment of 
	the learning path that is either currently active or has been completed. This is the enforcement of Rules #2 and #3, where 
	the action is "do nothing," which means no RuleSet Enrollment will be granted.

	*/

	DELETE FROM #RuleSetLearningPathEnrollmentMatches
	WHERE idUser IN (
					 SELECT idUser FROM
				     #CurrentLearningPathEnrollments CE
					 WHERE CE.idUser = #RuleSetLearningPathEnrollmentMatches.idUser
					 AND CE.idLearningPath = #RuleSetLearningPathEnrollmentMatches.idLearningPath
					 AND CE.idRuleSetLearningPathEnrollment IS NULL
					)
	AND idLearningPath IN (
						   SELECT idLearningPath FROM
						   #CurrentLearningPathEnrollments CE
					       WHERE CE.idUser = #RuleSetLearningPathEnrollmentMatches.idUser
					       AND CE.idLearningPath = #RuleSetLearningPathEnrollmentMatches.idLearningPath
					       AND CE.idRuleSetLearningPathEnrollment IS NULL
					      )

	/*

	Eliminate RuleSet Learning Path Enrollment matches where the user already has the same RuleSet enrollment of the
	learning path. This is the enforcement of Rule #4, where the action is "do nothing," which means no RuleSet 
	Enrollment will be granted.

	*/
	
	DELETE FROM #RuleSetLearningPathEnrollmentMatches
	WHERE idUser IN (
					 SELECT idUser FROM
				     #CurrentLearningPathEnrollments CE
					 WHERE CE.idUser = #RuleSetLearningPathEnrollmentMatches.idUser
					 AND CE.idLearningPath = #RuleSetLearningPathEnrollmentMatches.idLearningPath
					 AND CE.idRuleSetLearningPathEnrollment IS NOT NULL
					)
	AND idLearningPath IN (
				           SELECT idLearningPath FROM
				           #CurrentLearningPathEnrollments CE
					       WHERE CE.idUser = #RuleSetLearningPathEnrollmentMatches.idUser
					       AND CE.idLearningPath = #RuleSetLearningPathEnrollmentMatches.idLearningPath
					       AND CE.idRuleSetLearningPathEnrollment IS NOT NULL
					      )
	AND idRuleSetLearningPathEnrollment IN (
							                SELECT idRuleSetLearningPathEnrollment FROM
											#CurrentLearningPathEnrollments CE
											WHERE CE.idUser = #RuleSetLearningPathEnrollmentMatches.idUser
											AND CE.idLearningPath = #RuleSetLearningPathEnrollmentMatches.idLearningPath
											AND CE.idRuleSetLearningPathEnrollment = #RuleSetLearningPathEnrollmentMatches.idRuleSetLearningPathEnrollment
										   )

	/*

	Create temporary table for NEW INSERTS.

	*/

	CREATE TABLE #NewInserts
	(
		idSite							INT,
		idRuleSetLearningPathEnrollment	INT,
		idLearningPath					INT,
		idUser							INT
	)

	/*

	IMPORTANT NOTE: The INSERTS below are followed by DELETES from the RuleSetLearningPathEnrollmentMatches
	table, which appear to be redundant and at first glance look like they could just be one 
	statement at the end. But it is necessary to do a DELETE after each INSERT as a safeguard
	against potential duplicate enrollments being created later on.

	*/

	/*

	Put the RuleSet enrollment matches where the user has no enrollments of the LearningPath into the 
	NewInserts temporary table since we KNOW we have to grant these. Then, remove them from the
	RuleSetLearningPathEnrollmentMatches working table. This is the enforcement of Rule #1.

	*/

	-- INSERT
	INSERT INTO #NewInserts
	(
		idSite,
		idRuleSetLearningPathEnrollment,
		idLearningPath,
		idUser
	)
	SELECT
		RSEM.idSite,
		RSEM.idRuleSetLearningPathEnrollment,
		RSEM.idLearningPath,
		RSEM.idUser
	FROM #RuleSetLearningPathEnrollmentMatches RSEM
	WHERE NOT EXISTS (
					  SELECT 1 FROM #CurrentLearningPathEnrollments CE
					  WHERE CE.idUser = RSEM.idUser
					  AND CE.idLearningPath = RSEM.idLearningPath
					 )

	-- DELETE
	DELETE FROM #RuleSetLearningPathEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #NewInserts NI
				  WHERE NI.idSite = #RuleSetLearningPathEnrollmentMatches.idSite
				  AND NI.idRuleSetLearningPathEnrollment = #RuleSetLearningPathEnrollmentMatches.idRuleSetLearningPathEnrollment
				  AND NI.idLearningPath = #RuleSetLearningPathEnrollmentMatches.idLearningPath
				  AND NI.idUser = #RuleSetLearningPathEnrollmentMatches.idUser
				 )

	/*

	"COMPLEX" ENFORCEMENT OF RULE #5 WHERE WE REPLACE THE INHERITANCE OF ONE RULESET ENROLLMENT 
	WITH THE INHERITANCE OF ANOTHER RULESET ENROLLMENT

	RIGHT NOW, LEARNING PATH ENROLLMENTS ARE ONLY ONE-TIME AND DO NOT EXPIRE

	*/

	/*

		New is one-time and old is one-time.
		
		If old RuleSet Enrollment is active:
			- Grant new RuleSet-inherited Enrollment.
			- Link lesson data for currently active enrollment to the new RuleSet-inherited Enrollment.
			- Delete the old RuleSet-inherited Enrollment
		
		If old RuleSet Enrollment is completed:
			- Remove RuleSet Enrollment link of old RuleSet-inherited Enrollment from completed enrollment, i.e. make it appear manually-assigned.

	*/

	/*

	Create temporary table to hold the information for RuleSet Enrollments to be inherited
	where both the new and the old are one-time RuleSet Enrollments.

	*/

	CREATE TABLE #OneTimeToOneTime
	(
		idSite								INT,
		idLearningPath						INT,
		idUser								INT,
		idRuleSetLearningPathEnrollmentNew	INT, -- RuleSet Learning Path Enrollment Id of the new RuleSet Enrollment being inherited
		idRuleSetLearningPathEnrollmentOld	INT, -- RuleSet Learning Path Enrollment Id that is currently inherited
		idLearningPathEnrollmentCurrent		INT, -- The latest Enrollment occurring from the currently inherited RuleSet Enrollment (latest is defined by the latest dtStart)
		removeOldRSELinks					BIT, -- Remove the link to the old RuleSet Enrollment from previous Enrollments?
		updateCurrentEnrollment				BIT  -- Do we update the current Enrollment with the data from the new RuleSet Enrollment?
												 -------------------------------------------------------------------------------------
												 -- If no, we just create a new Enrollment for the RuleSet Enrollment. Note that when
												 -- we say "Grant new..." followed by "Link lesson data..." we're just going to update
												 -- the current Enrollment with the new RuleSet Enrollment data. It is much less complicated
												 -- if we do it that way.
	)

	-- INSERT
	INSERT INTO #OneTimeToOneTime
	(
		idSite,
		idLearningPath,
		idUser,
		idRuleSetLearningPathEnrollmentNew,
		idRuleSetLearningPathEnrollmentOld,
		idLearningPathEnrollmentCurrent,
		removeOldRSELinks,
		updateCurrentEnrollment
	)
	SELECT
		RSEM.idSite,
		RSEM.idLearningPath,
		RSEM.idUser,
		RSEM.idRuleSetLearningPathEnrollment,
		CE.idRuleSetLearningPathEnrollment,
		CE.idLearningPathEnrollment,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			1
		ELSE
			0
		END,
		CASE WHEN CE.dtCompleted IS NOT NULL THEN
			0
		ELSE
			1
		END
	FROM #RuleSetLearningPathEnrollmentMatches RSEM
	LEFT JOIN #CurrentLearningPathEnrollments CE ON CE.idSite = RSEM.idSite 
		AND CE.idUser = RSEM.idUser 
		AND CE.idLearningPath = RSEM.idLearningPath
		AND CE.idRuleSetLearningPathEnrollment <> RSEM.idRuleSetLearningPathEnrollment 
		AND CE.idRuleSetLearningPathEnrollment IS NOT NULL
		AND NOT EXISTS (
						SELECT 1 FROM #CurrentLearningPathEnrollments CE2 
						WHERE CE2.idUser = CE.idUser 
						AND CE2.idLearningPath = CE.idLearningPath
						AND CE2.idRuleSetLearningPathEnrollment = CE.idRuleSetLearningPathEnrollment
						AND CE2.dtStart > CE.dtStart
					   )
	LEFT JOIN tblRuleSetLearningPathEnrollment RSEN ON RSEN.idRuleSetLearningPathEnrollment = RSEM.idRuleSetLearningPathEnrollment
	LEFT JOIN tblRuleSetLearningPathEnrollment RSEC ON RSEC.idRuleSetLearningPathEnrollment = CE.idRuleSetLearningPathEnrollment

	-- DELETE
	DELETE FROM #RuleSetLearningPathEnrollmentMatches
	WHERE EXISTS (
				  SELECT 1 FROM #OneTimeToOneTime OTTOT
				  WHERE OTTOT.idSite = #RuleSetLearningPathEnrollmentMatches.idSite
				  AND OTTOT.idRuleSetLearningPathEnrollmentNew = #RuleSetLearningPathEnrollmentMatches.idRuleSetLearningPathEnrollment
				  AND OTTOT.idLearningPath = #RuleSetLearningPathEnrollmentMatches.idLearningPath
				  AND OTTOT.idUser = #RuleSetLearningPathEnrollmentMatches.idUser
				 )

	/*

	Do work for One-Time to One-Time RuleSet Enrollment shift.

	*/

	-- UPDATE RuleSet Enrollment link for inherited RuleSet Enrollments where the current Enrollment is still active.
	UPDATE tblLearningPathEnrollment SET
		idRuleSetLearningPathEnrollment = OTTOT.idRuleSetLearningPathEnrollmentNew,
		idTimezone = RSE.idTimezone,
		dtStart = CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
					dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
				  ELSE
					@utcNow
				  END,
		dtDue = CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
					CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
						dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
					ELSE
						dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
					END
				ELSE
					NULL
				END,
		dtExpiresFromStart = CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
						dbo.IDateAdd(RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
				ELSE
						NULL
				END,
		dtCreated = @utcNow,
		dueInterval = RSE.dueInterval,
		dueTimeframe = RSE.dueTimeframe,
		expiresFromStartInterval = RSE.expiresFromFirstLaunchInterval,
		expiresFromStartTimeframe = RSE.expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = RSE.expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = RSE.expiresFromFirstLaunchTimeframe
	FROM #OneTimeToOneTime OTTOT
	LEFT JOIN tblRuleSetLearningPathEnrollment RSE ON RSE.idRuleSetLearningPathEnrollment = OTTOT.idRuleSetLearningPathEnrollmentNew
	WHERE OTTOT.idLearningPathEnrollmentCurrent = idLearningPathEnrollment
	AND OTTOT.updateCurrentEnrollment = 1

	-- REMOVE old RuleSet Enrollment link for inherited Enrollments that have been completed.
	UPDATE tblLearningPathEnrollment SET
		idRuleSetLearningPathEnrollment = NULL
	FROM #OneTimeToOneTime OTTOT
	WHERE OTTOT.idUser = tblLearningPathEnrollment.idUser
	AND OTTOT.idLearningPath = tblLearningPathEnrollment.idLearningPath
	AND OTTOT.idRuleSetLearningPathEnrollmentOld = tblLearningPathEnrollment.idRuleSetLearningPathEnrollment
	AND OTTOT.idLearningPathEnrollmentCurrent <> tblLearningPathEnrollment.idLearningPathEnrollment
	AND OTTOT.removeOldRSELinks = 1
	AND tblLearningPathEnrollment.dtCompleted IS NOT NULL

	/*

	Do work for brand new INSERTS.

	*/

	INSERT INTO tblLearningPathEnrollment
	(
		idSite,
		idLearningPath,
		idUser,
		idRuleSetLearningPathEnrollment,
		idTimezone,
		title,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT DISTINCT
		NI.idSite,
		NI.idLearningPath,
		NI.idUser,
		NI.idRuleSetLearningPathEnrollment,
		RSE.idTimezone,
		LP.name,
		CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
			dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow)
		ELSE
			@utcNow
		END,
		CASE WHEN RSE.dueInterval IS NOT NULL AND RSE.dueInterval > 0 THEN
			CASE WHEN RSE.delayInterval IS NOT NULL AND RSE.delayInterval > 0 THEN
				dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, dbo.[IDateAdd](RSE.delayTimeframe, RSE.delayInterval, @utcNow))
			ELSE
				dbo.[IDateAdd](RSE.dueTimeframe, RSE.dueInterval, @utcNow)
			END
		ELSE
			NULL
		END,
		CASE WHEN RSE.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval > 0 THEN
				dbo.IDateAdd(RSE.expiresFromStartTimeframe, RSE.expiresFromStartInterval, RSE.dtStart)
		ELSE
				NULL
		END,
		@utcNow,
		RSE.dueInterval,
		RSE.dueTimeframe,
		RSE.expiresFromStartInterval,
		RSE.expiresFromStartTimeframe,
		RSE.expiresFromFirstLaunchInterval,
		RSE.expiresFromFirstLaunchTimeframe
	FROM #NewInserts NI
	LEFT JOIN tblRuleSetLearningPathEnrollment RSE ON RSE.idRuleSetLearningPathEnrollment = NI.idRuleSetLearningPathEnrollment
	LEFT JOIN tblLearningPath LP ON LP.idLearningPath = NI.idLearningPath

	/*

	For new learning path enrollment inserts, also insert course enrollments for the learning path courses where
	the user does not already have one that is active or has been completed in the last year.

	*/

	CREATE TABLE #NewlyInsertedLearningPathEnrollments (
		idLearningPathEnrollment		INT,
		idSite							INT,
		idUser							INT,
		idLearningPath					INT,
		idTimezone						INT,
		dtCreated						DATETIME,
		dtStart							DATETIME,
		dtDue							DATETIME,
		dueInterval						INT,
		dueTimeframe					NVARCHAR(4),
		dtExpiresFromStart				DATETIME,
		expiresFromStartInterval		INT,
		expiresFromStartTimeframe		NVARCHAR(4),
		expiresFromFirstLaunchInterval	INT,
		expiresFromFirstLaunchTimeframe	NVARCHAR(4)
	)

	INSERT INTO #NewlyInsertedLearningPathEnrollments (
		idLearningPathEnrollment,
		idSite,
		idUser,
		idLearningPath,
		idTimezone,
		dtCreated,
		dtStart,
		dtDue,
		dueInterval,
		dueTimeframe,
		dtExpiresFromStart,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT
		LPE.idLearningPathEnrollment,
		LPE.idSite,
		LPE.idUser,
		LPE.idLearningPath,
		LPE.idTimezone,
		LPE.dtCreated,
		LPE.dtStart,
		LPE.dtDue,
		LPE.dueInterval,
		LPE.dueTimeframe,
		LPE.dtExpiresFromStart,
		LPE.expiresFromStartInterval,
		LPE.expiresFromStartTimeframe,
		LPE.expiresFromFirstLaunchInterval,
		LPE.expiresFromFirstLaunchTimeframe
	FROM #NewInserts NI 
	LEFT JOIN tblLearningPathEnrollment LPE ON LPE.idLearningPath = NI.idLearningPath AND LPE.idUser = NI.idUser
	WHERE LPE.dtCreated = @utcNow

	INSERT INTO tblEnrollment (
		idSite,
		idCourse,
		revcode,
		idUser,
		idLearningPathEnrollment,
		isLockedByPrerequisites,
		idTimezone,
		dtCreated,
		dtStart,
		dtDue,
		dueInterval,
		dueTimeframe,
		dtExpiresFromStart,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe,
		title,
		code,
		credits
	)
	SELECT DISTINCT
		NILPE.idSite,
		LPCL.idCourse,
		C.revcode,
		NILPE.idUser,
		NILPE.idLearningPathEnrollment,
		1,
		NILPE.idTimezone,
		NILPE.dtCreated,
		NILPE.dtStart,
		NILPE.dtDue,
		NILPE.dueInterval,
		NILPE.dueTimeframe,
		NILPE.dtExpiresFromStart,
		NILPE.expiresFromStartInterval,
		NILPE.expiresFromStartTimeframe,
		NILPE.expiresFromFirstLaunchInterval,
		NILPE.expiresFromFirstLaunchTimeframe,
		C.title,
		C.coursecode,
		C.credits
	FROM #NewlyInsertedLearningPathEnrollments NILPE
	LEFT JOIN tblLearningPathToCourseLink LPCL ON LPCL.idLearningPath = NILPE.idLearningPath
	LEFT JOIN tblCourse C ON C.idCourse = LPCL.idCourse
	WHERE NOT EXISTS (SELECT 1 FROM tblEnrollment E
					  WHERE E.idCourse = LPCL.idCourse
					  AND E.idUser = NILPE.idUser
					  AND 
					  (
							-- incompete/active
							(
							E.dtCompleted IS NULL
							AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
							AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
							)
						OR
							-- completed and within a year prior to the learning path enrollment's start
							(
							E.dtCompleted IS NOT NULL
							AND E.dtCompleted >= DATEADD(yyyy, -1, NILPE.dtStart)
							)
					 )
					 )
	/*

	do the event log entries for the learning path enrollments

	*/

	DECLARE @learningPathEnrollmentEventLogItems EventLogItemObjects

	INSERT INTO @learningPathEnrollmentEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		NILPE.idSite,
		NILPE.idLearningPath,
		NILPE.idLearningPathEnrollment, 
		NILPE.idUser
	FROM #NewlyInsertedLearningPathEnrollments NILPE

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 501, @utcNow, @learningPathEnrollmentEventLogItems

	/*

	do the event log entries for course enrollments

	*/

	DECLARE @enrollmentEventLogItems EventLogItemObjects

	INSERT INTO @enrollmentEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		NILPE.idSite,
		E.idCourse,
		E.idEnrollment, 
		NILPE.idUser
	FROM #NewlyInsertedLearningPathEnrollments NILPE
	LEFT JOIN tblEnrollment E ON E.idLearningPathEnrollment = NILPE.idLearningPathEnrollment AND E.idUser = NILPE.idUser
	WHERE E.dtCreated = @utcNow

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 201, @utcNow, @enrollmentEventLogItems
				   
	/* DROP THE TEMPORARY TABLES */
	DROP TABLE #CurrentLearningPathEnrollments
	DROP TABLE #RuleSetLearningPathEnrollmentMatches
	DROP TABLE #NewInserts
	DROP TABLE #OneTimeToOneTime
	DROP TABLE #NewlyInsertedLearningPathEnrollments

	/* FINAL STEP: REVOKE RULESET LEARNING PATH ENROLLMENTS */
	EXEC [RuleSetLearningPathEnrollment.RevokeEnrollments] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @LearningPaths, @Filters, @filterBy

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO