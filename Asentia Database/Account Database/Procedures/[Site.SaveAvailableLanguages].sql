-- =====================================================================
-- PROCEDURE: [Site.SaveAvailableLanguages]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Site.SaveAvailableLanguages]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.SaveAvailableLanguages]
GO

/*
Saves available language for a site.
*/
CREATE PROCEDURE [dbo].[Site.SaveAvailableLanguages]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idSite					INT,
	@languages				IDTable			READONLY

)
AS
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the site exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSite
		WHERE idSite = @idSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'SiteSaveAvailableLanguages_SiteNotFound'
		RETURN 1
		END

	/*
	
	validate that all the languages exist
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @languages LL
		LEFT JOIN tblLanguage L ON L.idLanguage = LL.id
		WHERE L.idLanguage IS NULL -- language does not exist
		) > 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'SiteSaveAvailableLanguages_LanguagesNotFound'
		RETURN 1 
		END
	
	/* Delete available languages */
	DELETE FROM tblSiteAvailableLanguage WHERE idSite = @idSite

	/* Insert available languages */
	INSERT INTO tblSiteAvailableLanguage
		(idSite, 
		idLanguage)
	SELECT
		@idSite,
		id
	FROM @languages LL
	
	SELECT @Return_Code = 0
		
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO