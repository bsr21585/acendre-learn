-- =====================================================================
-- PROCEDURE: [DataLesson.OverrideLessonCompletion]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DataLesson.OverrideLessonCompletion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.OverrideLessonCompletion]
GO

/*

Marks completion timestamp for a lesson data record as an "administrator override."
Used in the event a learner is not committed to any lesson content type and an administrator
wants to make the lesson complete for the learner.

*/

CREATE PROCEDURE [DataLesson.OverrideLessonCompletion]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT				= 0,
	
	@idEnrollment					INT,
	@idDataLesson					INT,
	@completionStatus				INT,
	@successStatus					INT,
	@scoreScaled					FLOAT,
	@timestamp						DATETIME
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that the lesson data exists and is not already completed

	*/

	IF (
		SELECT COUNT(1)
		FROM [tblData-Lesson]
		WHERE [idData-Lesson] = @idDataLesson
		AND idSite = @idCallerSite
		AND dtCompleted IS NULL
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DLOLessonCompletion_NoLessonDataRecordFound'
		RETURN 1
	    END

	/*

	if completed (really just a formality since the intention is to mark completion, but we validate data anyway), 
	mark lesson data as completed using the @timestamp parameter

	*/

	-- if timestamp is null, make it now
	IF @timestamp IS NULL
		BEGIN
		SET @timestamp = GETUTCDATE()
		END
	
	IF (@completionStatus = 2)
		BEGIN

		/* 
		
		delete any tblData-SCO, tblData-SCOInt, and tblData-SCOObj records for this lesson data id
		as we are going to use tblData-SCO to record completion, success, and score for this override

		*/

		-- delete SCORM interactions
		DELETE FROM [tblData-SCOInt] 
		WHERE [idData-SCO] IN (SELECT [idData-SCO] 
							   FROM [tblData-SCO] 
							   WHERE [idData-Lesson] = @idDataLesson
							  )
	
		-- delete SCORM objectives
		DELETE FROM [tblData-SCOObj] 
		WHERE [idData-SCO] IN (SELECT [idData-SCO] 
							   FROM [tblData-SCO] 
							   WHERE [idData-Lesson] = @idDataLesson
							   )

		-- delete SCORM lesson data
		DELETE FROM [tblData-SCO] 
		WHERE [idData-Lesson] = @idDataLesson

		/* insert a new record in tblData-SCO to store completion, success, and score */

		INSERT INTO [tblData-SCO] (
			idSite,
			[idData-Lesson],
			idTimezone,
			completionStatus,
			successStatus,
			scoreScaled,
			[timestamp]
		)
		SELECT
			@idCallerSite,
			@idDataLesson,
			DL.idTimezone,
			@completionStatus,
			@successStatus,
			CONVERT(DECIMAL(10,2), @scoreScaled),
			@timestamp
		FROM [tblData-Lesson] DL
		WHERE DL.[idData-Lesson] = @idDataLesson
		
		/* update the tblData-Lesson record */
		
		UPDATE [tblData-Lesson] SET 
			dtCompleted = @timestamp,
			contentTypeCommittedTo = 0 -- special content type committed to for administrator override
		WHERE [idData-Lesson] = @idDataLesson
		AND idSite = @idCallerSite 

		/* do event log entry for lesson completed */

		DECLARE @eventLogItem EventLogItemObjects
		DECLARE @idLesson INT
		DECLARE @idUser INT

		SELECT
			@idLesson = LD.idLesson,
			@idUser = E.idUser
		FROM [tblData-Lesson] LD
		LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
		WHERE LD.[idData-Lesson] = @idDataLesson

		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idLesson, @idDataLesson, @idUser

		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 303, @timestamp, @eventLogItem

		END

	/*

	check for course completion

	*/

	EXEC [Enrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idEnrollment

	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

