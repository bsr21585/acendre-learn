-- =====================================================================
-- PROCEDURE: [User.SetDefaultLanguage]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.SetDefaultLanguage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.SetDefaultLanguage]
GO

/*

Sets the user's default language.

*/
CREATE PROCEDURE [User.SetDefaultLanguage]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	caller must be an actual user
	
	*/
	
	IF @idCaller <= 1
		BEGIN
		SET @Return_Code = 3 -- permission error
		SET @Error_Description_Code = 'UserSetDefaultLanguage_CallerPermissionError'
		RETURN 1
		END
		
	/*
	
	set user's language
	
	*/

	DECLARE @count INT
		
	SELECT @count = COUNT(1)
	FROM tblUser U
	WHERE U.idUser = @idCaller
	
	IF (@count) = 0 
		BEGIN
		SET @Return_Code = 1 -- not found
		SET @Error_Description_Code = 'UserSetDefaultLanguage_UsernameNotFound'
		END
		
	-- get the language id
	DECLARE @idLanguage INT
	SELECT @idLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString
	
	-- block the trigger from executing in tblUser UPDATE
	DECLARE @CONTEXT VARBINARY(128)

	SET @CONTEXT = CONVERT(VARBINARY(128), 'BLOCK TRIGGER')
	SET CONTEXT_INFO @CONTEXT

	-- set the language
	UPDATE tblUser SET
		idLanguage = @idLanguage
	WHERE idUser = @idCaller

	-- unblock the trigger
	SET @CONTEXT = CONVERT(VARBINARY(128), '')
	SET CONTEXT_INFO @CONTEXT
		
	SET @Return_Code = 0 --(0 is 'success')
	SET @Error_Description_Code = ''
	RETURN 1
	
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO