-- =====================================================================
-- PROCEDURE: [Report.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Report.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Report.SaveLang]
GO

/*

Saves report "language specific" properties for specific language
in report language table.

*/

CREATE PROCEDURE [Report.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idReport				INT,
	@languageString			NVARCHAR(10),
	@title					NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'ReportSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
		
	/*
	
	validate that the group exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblReport
		WHERE idReport = @idReport
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'ReportSaveLang_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	validate uniqueness within language
	
	*/
	
	IF( 
		SELECT COUNT(1)
		FROM tblReport R
		LEFT JOIN tblReportLanguage RL ON RL.idReport= R.idReport AND RL.idLanguage = @idLanguage -- same language
		WHERE R.idSite = @idCallerSite -- same site
		AND R.idReport <> @idReport -- not the same report
		AND RL.title = @title -- validate parameter: name
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'ReportSaveLang_FieldNotUnique'
		RETURN 1
		END
			
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblReportLanguage RL WHERE RL.idReport = @idReport AND RL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblReportLanguage SET
			title				= @title
		WHERE idReport = @idReport
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblReportLanguage (
			idSite,
			idReport,
			idLanguage,
			title
		)
		SELECT
			@idCallerSite,
			@idReport,
			@idLanguage,
			@title
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblReportLanguage RL
			WHERE RL.idReport = @idReport
			AND RL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table
	note: uniqueness should already be validated

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

			UPDATE tblReport SET
				title			 = @title
			WHERE idReport = @idReport

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO