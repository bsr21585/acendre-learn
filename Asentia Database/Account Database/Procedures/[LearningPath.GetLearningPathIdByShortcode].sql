-- =====================================================================
-- PROCEDURE: [LearningPath.GetLearningPathIdByShortcode]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.GetLearningPathIdByShortcode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.GetLearningPathIdByShortcode]
GO

/*

Gets a learning path id based on the shortcode

*/
CREATE PROCEDURE [LearningPath.GetLearningPathIdByShortcode]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@shortcode					NVARCHAR(10),
	@idLearningPath				INT				OUTPUT
AS

	BEGIN	
	SET NOCOUNT ON

    SELECT
		@idLearningPath = LP.idLearningPath
	FROM tblLearningPath LP
	WHERE LP.shortcode = @shortcode
	ANd LP.idSite = @idCallerSite

	IF (@idLearningPath IS NULL)
		BEGIN
		SET @idLearningPath = 0
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		
