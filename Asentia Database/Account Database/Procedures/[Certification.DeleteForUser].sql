-- =====================================================================
-- PROCEDURE: [Certification.DeleteForUser]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Certification.DeleteForUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) 
	DROP PROCEDURE [Certification.DeleteForUser]
GO

/*

Deletes certification to user link(s)

*/

CREATE PROCEDURE [Certification.DeleteForUser]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@CertificationToUserLinks	IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @CertificationToUserLinks) = 0 
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CertificationDeleteForUser_NoRecordFound'
		RETURN 1
		END

	/*

	validate that all certifications user links exist within the site

	*/

	IF (
		SELECT COUNT(1) 
		FROM @CertificationToUserLinks CC
		LEFT JOIN tblCertificationToUserLink CUL ON CUL.idCertificationToUserLink = CC.id
		WHERE CUL.idSite IS NULL
		OR CUL.idSite <> @idCallerSite
	   ) > 0 
	   
	   BEGIN
	   SET @Return_Code = 1
	   SET @Error_Description_Code = 'CertificationDeleteForUser_NoRecordFound'
	   RETURN 1
	   END

	/*

	do the event log entries for revocations first

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	DECLARE @eventLogRevokedItems EventLogItemObjects

	INSERT INTO @eventLogRevokedItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		CUL.idSite,
		CUL.idCertification,
		CUL.idCertificationToUserLink, 
		CUL.idUser
	FROM @CertificationToUserLinks CULCUL
	LEFT JOIN tblCertificationToUserLink CUL ON CUL.idCertificationToUserLink = CULCUL.id

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 603, @utcNow, @eventLogRevokedItems

	/*
		
	DELETE IN ORDER OF DEPENDENCIES

	*/	

	-- DELETE FROM [tblData-CertificationModuleRequirement]
	DELETE FROM [tblData-CertificationModuleRequirement] WHERE idCertificationToUserLink IN (SELECT id FROM @CertificationToUserLinks)

	-- DELETE FROM [tblCertificationToUserLink]
	DELETE FROM [tblCertificationToUserLink] WHERE idCertificationToUserLink IN (SELECT id FROM @CertificationToUserLinks)

	/*

	RETURN

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO