-- =====================================================================
-- PROCEDURE: [Dataset.LoginActivity]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Dataset.LoginActivity]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Dataset.LoginActivity]
GO

/*

Login Activity Dataset

*/

CREATE PROCEDURE [Dataset.LoginActivity]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@frequency                  INT,
	@dateFilter				    NVARCHAR(255),
	@groupByClause				NVARCHAR(768),
	@orderClause				NVARCHAR(768),
    @whereClause                NVARCHAR(MAX),
	@activity                   INT             = 0
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	

	/*

	get the id of the caller's language

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage =
		CASE WHEN idLanguage IS NULL THEN 
			57
		ELSE 
			idLanguage 
		END
		FROM tblLanguage WHERE code = @callerLangString



	/*

		build sql query

	*/
		DECLARE @sql NVARCHAR(MAX)

	  SET @sql = ' SELECT COUNT(idUser)  AS Count ,''Courses''' + ' AS LegendName '

	IF(@frequency > 0 AND @frequency <> 5 ) --group by frequency (day,hour,month,week) 
	     BEGIN 
		 SET @groupByClause = REPLACE(@groupByClause,'XXX','U.dtLastLogin')
		 SET  @sql =@sql +', AC.value '
		 END
	ELSE IF(@frequency > 0 AND @frequency = 5 )--group by frequency (year)
	     BEGIN 
		 SET @groupByClause = REPLACE(@groupByClause,'XXX','U.dtLastLogin')
		 SET  @sql =@sql + ' , '+ @groupByClause
		 END

	IF @dateFilter IS NOT NULL AND @dateFilter <> ''
		BEGIN
		SET @dateFilter = REPLACE(@dateFilter,'XXX','U.dtLastLogin')
		END

		IF(@frequency > 0 AND @frequency <> 5)
		BEGIN
		SET @sql = @sql	+ ' AS Frequency FROM tblanalyticConstants AC '
								+ ' LEFT JOIN  tbluser U  ON AC.value = '+ @groupByClause+ ' '
								+ ' AND U.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '
	                            + ' AND U.dtLastLogin IS NOT NULL AND ' + @dateFilter + ' '
								+ ' LEFT JOIN tblTimezone TZ ON TZ.idTimezone = U.idTimezone '
								+ ' LEFT JOIN tblTimezoneLanguage TZL ON TZL.idTimezone = TZ.idTimezone AND TZL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
						        + ' WHERE (U.isDeleted IS NULL OR U.isDeleted = 0) '
								 + 'AND AC.idconstantType = '+ CONVERT(NVARCHAR, @frequency) +' '
		END
		ELSE IF(@frequency > 0 AND @frequency = 5)
		BEGIN
		SET @sql = @sql	        + ' AS Frequency  '
								+ ' FROM  tbluser U  '
								+ ' LEFT JOIN tblTimezone TZ ON TZ.idTimezone = U.idTimezone '
								+ ' LEFT JOIN tblTimezoneLanguage TZL ON TZL.idTimezone = TZ.idTimezone AND TZL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
						        + ' WHERE (U.isDeleted IS NULL OR U.isDeleted = 0) '
								+ ' AND U.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '
	                            + ' AND U.dtLastLogin IS NOT NULL AND ' + @dateFilter + ' '
						


		END


	 IF @whereClause IS NOT NULL AND @whereClause <> ''
		BEGIN
		IF(@frequency > 0 AND @frequency = 2)--when selected frequency is day
			BEGIN
			SET @whereClause = ' AND AC.value '+@whereClause
			END
		ELSE 
			BEGIN
			SET @whereClause = ' AND DATEPART(DW , dtLastLogin) '+@whereClause
			END

		SET @sql = @sql + @whereClause
		END

        /*

		build group by clause

	*/

	 IF @groupByClause IS NOT NULL AND @groupByClause <> ''
		BEGIN
			IF(@frequency > 0 AND @frequency <> 5 ) --group by frequency (day,hour,month,week)
				BEGIN 
				SET @groupByClause = ' AC.value '
				END
			ELSE IF(@frequency > 0 AND @frequency = 5 )
			    BEGIN
				SET @groupByClause = REPLACE(@groupByClause,'XXX','U.dtLastLogin')
				END
		    
		SET @sql = @sql+ ' GROUP BY ' + @groupByClause
		END


		-- /*

		--build order by clause

	 --    */
		-- DECLARE @orderByClause NVARCHAR(50)
		--IF(@frequency > 0 AND @frequency <> 5 ) 
		--BEGIN 
		--SET @orderByClause = ' ORDER BY AC.idConstant ASC '
		--END
	

		-- SET @sql = @sql+ ' ' + @orderByClause


	/*

	execute the sql statement and log its execution

	*/

	DECLARE @dtExecutionBegin DATETIME
	DECLARE @dtExecutionEnd DATETIME
	DECLARE @executionDurationMS INT

	SET @dtExecutionBegin = GETUTCDATE()

	EXEC sp_executesql @sql

	SET @dtExecutionEnd = GETUTCDATE()

	SET @executionDurationMS = DATEDIFF(ms, @dtExecutionBegin, @dtExecutionEnd)

	INSERT INTO tblReportProcessorLog
	(
		idSite,
		idCaller,
		datasetProcedure,
		dtExecutionBegin,
		dtExecutionEnd,
		executionDurationSeconds,
		sqlQuery
	)
	SELECT
		@idCallerSite,
		@idCaller,
		'[Dataset.LoginActivity]',
		@dtExecutionBegin,
		@dtExecutionEnd,
		CAST((CAST(@executionDurationMS AS DECIMAL)/1000) AS DECIMAL(9,2)),
		@sql

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO