-- =====================================================================
-- PROCEDURE: [StandupTraining.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.GetGrid]
GO

/*

Gets a listing of Standup Training.

*/

CREATE PROCEDURE [StandupTraining.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblStandupTraining ST
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND ST.idSite = @idCallerSite)
				)
			AND (ST.isDeleted IS NULL OR ST.isDeleted = 0)

			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						ST.idStandupTraining,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN ST.title END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN ST.title END) END
						)
						AS [row_number]
					FROM tblStandupTraining ST
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND ST.idSite = @idCallerSite)
						)
					AND (ST.isDeleted IS NULL OR ST.isDeleted = 0)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idStandupTraining, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				ST.idStandupTraining,
				CASE WHEN STL.title IS NOT NULL THEN STL.title ELSE ST.title END AS title,
				ST.avatar,
				(SELECT COUNT(1) FROM tblStandUpTrainingInstance TI WHERE TI.idStandUpTraining = ST.idStandUpTraining AND (TI.isDeleted = 0 OR TI.isDeleted IS NULL)) AS sessionCount,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblStandupTraining ST ON ST.idStandupTraining = SelectedKeys.idStandUpTraining
			LEFT JOIN tblStandupTrainingLanguage STL ON STL.idStandupTraining = ST.idStandUpTraining AND STL.idLanguage = @idCallerLanguage
			WHERE (ST.isDeleted IS NULL OR ST.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblStandupTraining ST
				INNER JOIN CONTAINSTABLE(tblStandupTraining, *, @searchParam) K ON K.[key] = ST.idStandUpTraining
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND ST.idSite = @idCallerSite)
					)
				AND (ST.isDeleted IS NULL OR ST.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							ST.idStandupTraining,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN ST.title END) END DESC,
							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN ST.title END) END
							)
							AS [row_number]
						FROM tblStandupTraining ST
						INNER JOIN CONTAINSTABLE(tblStandupTraining, *, @searchParam) K ON K.[key] = ST.idStandupTraining
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND ST.idSite = @idCallerSite
							)
						AND (ST.isDeleted IS NULL OR ST.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idStandupTraining, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					ST.idStandupTraining, 
					ST.title,
					ST.avatar,
					(SELECT COUNT(1) FROM tblStandUpTrainingInstance TI WHERE TI.idStandUpTraining = ST.idStandUpTraining AND (TI.isDeleted = 0 OR TI.isDeleted IS NULL)) AS sessionCount,
					CONVERT(BIT, 1) AS isModifyOn,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblStandUpTraining ST ON ST.idStandupTraining = SelectedKeys.idStandUpTraining
				WHERE (ST.isDeleted IS NULL OR ST.isDeleted = 0)
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblStandupTrainingLanguage STL
				LEFT JOIN tblStandupTraining ST ON ST.idStandUpTraining = STL.idStandUpTraining
				INNER JOIN CONTAINSTABLE(tblStandupTrainingLanguage, *, @searchParam) K ON K.[key] = STL.idStandupTrainingLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND ST.idSite = @idCallerSite)
					)
				AND STL.idLanguage = @idCallerLanguage
				AND (ST.isDeleted IS NULL OR ST.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							STL.idStandupTrainingLanguage,
							ST.idStandupTraining,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STL.title END) END DESC,
							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN STL.title END) END
							)
							AS [row_number]
						FROM tblStandupTrainingLanguage STL
						LEFT JOIN tblStandupTraining ST ON ST.idStanduptraining = STL.idStandUpTraining
						INNER JOIN CONTAINSTABLE(tblStandupTrainingLanguage, *, @searchParam) K ON K.[key] = STL.idStandupTrainingLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND ST.idSite = @idCallerSite
							)
						AND STL.idLanguage = @idCallerLanguage
						AND (ST.isDeleted IS NULL OR ST.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idStandupTraining, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					ST.idStandupTraining, 
					STL.title,
					ST.avatar,
					(SELECT COUNT(1) FROM tblStandUpTrainingInstance TI WHERE TI.idStandUpTraining = ST.idStandUpTraining AND (TI.isDeleted = 0 OR TI.isDeleted IS NULL)) AS sessionCount,
					CONVERT(BIT, 1) AS isModifyOn,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblStandupTrainingLanguage STL ON STL.idStandupTraining = SelectedKeys.idStandupTraining AND STL.idLanguage = @idCallerLanguage
				LEFT JOIN tblStandupTraining ST ON ST.idStandupTraining = STL.idStandupTraining
				ORDER BY SelectedKeys.[row_number]

				END
			
			END
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO