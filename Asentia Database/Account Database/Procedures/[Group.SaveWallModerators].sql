-- =====================================================================
-- PROCEDURE: [Group.SaveWallModerators]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.SaveWallModerators]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.SaveWallModerators]
GO


/*

Saves moderators for group wall.

*/

CREATE PROCEDURE [Group.SaveWallModerators]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	--   default if not specified
	@callerLangString		NVARCHAR (10),   
	@idCaller				INT				= 0, 

	@idGroup				INT,
	@Moderators				IDTable			READONLY
)
AS

	BEGIN
	
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that all moderators exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Moderators MO
		LEFT JOIN tblUser U ON U.idUser = MO.id
		WHERE U.idSite IS NULL
		OR U.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupSaveWallModerators_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	validate that the group belongs to the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblGroup G
		WHERE G.idGroup = @idGroup
		AND (G.idSite IS NULL OR G.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupSaveWallModerators_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	validate the group wall moderation is on
	
	*/
	
	IF (SELECT COUNT(1) FROM tblGroup WHERE idGroup = @idGroup AND (isFeedModerated IS NULL OR isFeedModerated = 0)) > 0 			
		BEGIN
		SELECT @Return_Code = 3
		SET @Error_Description_Code = 'GroupSaveWallModerators_ModerationOff'
		RETURN 1
		END

	/*
	
	remove all moderators for the group wall from the group feed moderator table
	(if there are any in the table variable, otherwise it means there are none)
	
	*/
	
	DELETE FROM tblGroupFeedModerator
	WHERE idGroup = @idGroup
	
	/*
	
	insert the moderators for the group wall into the group feed moderator table
	
	*/

	IF (SELECT COUNT(1) FROM @Moderators) > 0
		BEGIN

		INSERT INTO tblGroupFeedModerator (
			idSite,
			idGroup,
			idModerator
		)
		SELECT 
			@idCallerSite,
			@idGroup,
			MO.id
		FROM @Moderators MO
		WHERE NOT EXISTS (SELECT 1
						  FROM tblGroupFeedModerator GFM
						  WHERE GFM.idGroup = @idGroup
						  AND GFM.idModerator = MO.id)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO