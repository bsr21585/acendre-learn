-- =====================================================================
-- PROCEDURE: [ContentPackage.GetUtilizationInformation]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ContentPackage.GetUtilizationInformation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ContentPackage.GetUtilizationInformation]
GO

/*

Returns a recordset of lessons that are utilizing the specified content package.

*/
CREATE PROCEDURE [ContentPackage.GetUtilizationInformation]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idContentPackage		INT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	get the data

	*/

	SELECT
		LCL.idObject AS idContentPackage,
		L.idLesson,
		CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE L.title END AS lessonTitle,
		C.idCourse,
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS courseTitle
	FROM tblLessonToContentLink LCL
	LEFT JOIN tblLesson L ON L.idLesson = LCL.idLesson
	LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
	LEFT JOIN tblCourse C ON C.idCourse = L.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	WHERE LCL.idSite = @idCallerSite
	AND (LCL.idContentType = 1 OR LCL.idContentType = 4) -- content package or ojt
	AND LCL.idObject = @idContentPackage
	AND (L.isDeleted IS NULL OR L.isDeleted = 0)
	AND (C.isDeleted IS NULL OR C.isDeleted = 0)
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO