-- =====================================================================
-- PROCEDURE: [Course.GetRuleSetEnrollments]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.GetRuleSetEnrollments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.GetRuleSetEnrollments]
GO

/*

Returns a recordset of ruleset enrollment ids and labels that belong to a course.

*/

CREATE PROCEDURE [Course.GetRuleSetEnrollments]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCourse				INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		SELECT
			DISTINCT
			RSE.idRuleSetEnrollment,
			CASE WHEN RSEL.label IS NOT NULL THEN RSEL.label ELSE RSE.label END AS label,
			RSE.[priority]
		FROM tblRuleSetEnrollment RSE
		LEFT JOIN tblRuleSetEnrollmentLanguage RSEL ON RSEL.idRuleSetEnrollment = RSE.idRuleSetEnrollment AND RSEL.idLanguage = @idCallerLanguage
		WHERE RSE.idSite = @idCallerSite
		AND RSE.idCourse = @idCourse
		ORDER BY [priority]

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			RSE.idRuleSetEnrollment,
			CASE WHEN RSEL.label IS NOT NULL THEN RSEL.label ELSE RSE.label END AS label,
			RSE.[priority]
		FROM tblRuleSetEnrollmentLanguage RSEL
		INNER JOIN CONTAINSTABLE(tblRuleSetEnrollmentLanguage, *, @searchParam) K ON K.[key] = RSEL.idRuleSetEnrollmentLanguage AND RSEL.idLanguage = @idCallerLanguage
		LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
		WHERE RSE.idSite = @idCallerSite
		AND RSE.idCourse = @idCourse
		ORDER BY [priority]

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO