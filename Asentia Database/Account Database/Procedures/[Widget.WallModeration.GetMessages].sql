-- =====================================================================
-- PROCEDURE: [Widget.WallModeration.GetMessages]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Widget.WallModeration.GetMessages]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.WallModeration.GetMessages]
GO

/*

Gets wall feed messages from course(s) and group(s) that need to be moderated.

*/

CREATE PROCEDURE [Widget.WallModeration.GetMessages]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, -- default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0, -- will fail if not specified
	
	@pageSize						INT				= 10,
	@dtQuery						DATETIME,
	@getMessagesNewerThanDtQuery	BIT				= 0, -- default if not specified
	@dtLastRecord					DATETIME		OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get caller's language id

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	/* if dtQuery is not specified use utc now */

	IF (@dtQuery IS NULL)
		BEGIN
		SET @dtQuery = GETUTCDATE()
		END

	/* declare the temporary table */

	CREATE TABLE #FeedMessages (
		idFeedMessage		INT,
		idParentFeedMessage	INT,
		feedType			NVARCHAR(10),
		feedObjectId		INT,
		feedObjectName		NVARCHAR(255),
		feedObjectAvatar	NVARCHAR(255),
		idAuthor			INT,
		[message]			NVARCHAR(MAX),
		[timestamp]			DATETIME,
		authorName			NVARCHAR(255),
		avatar				NVARCHAR(255),
		gender				NVARCHAR(1)
	)
		
	/* get the group feed data */

	INSERT INTO #FeedMessages (
		idFeedMessage,
		idParentFeedMessage,
		feedType,
		feedObjectId,
		feedObjectName,
		feedObjectAvatar,
		idAuthor,
		[message],
		[timestamp],
		authorName,
		avatar,
		gender
	)
	SELECT TOP (@pageSize)
		GFM.idGroupFeedMessage AS idFeedMessage,
		GFM.idParentGroupFeedMessage AS idParentFeedMessage,
		'group' AS feedType,
		GFM.idGroup AS feedObjectId,
		CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS feedObjectName,
		G.avatar AS feedObjectAvatar,
		GFM.idAuthor,
		GFM.[message],
		GFM.[timestamp],
		CASE WHEN GFM.idAuthor > 1 THEN A.firstName + ' ' + A.lastName ELSE '##administrator##' END AS authorName,
		A.avatar,
		A.gender
	FROM tblGroupFeedMessage GFM
	LEFT JOIN tblGroup G ON G.idGroup = GFM.idGroup
	LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
	LEFT JOIN tblUser A ON A.idUser = GFM.idAuthor
	WHERE GFM.idSite = @idCallerSite
	AND G.isFeedActive = 1
	AND (
			(@idCaller = 1) -- admin can moderate anything
			OR
			(
			@idCaller > 1 AND GFM.idGroup IN (SELECT DISTINCT GFMO.idGroup -- group(s) the caller is a moderator of
											  FROM tblGroupFeedModerator GFMO
											  WHERE GFMO.idModerator = @idCaller)
			)
		)
	AND	(GFM.isApproved IS NULL OR GFM.isApproved = 0)
	AND (
			(@getMessagesNewerThanDtQuery = 0 AND GFM.[timestamp] < @dtQuery)
			OR
			(@getMessagesNewerThanDtQuery = 1 AND GFM.[timestamp] > @dtQuery)
		)
	ORDER BY 
		CASE WHEN @getMessagesNewerThanDtQuery = 0 THEN [timestamp] ELSE '' END DESC,
		CASE WHEN @getMessagesNewerThanDtQuery = 1 THEN [timestamp] ELSE '' END ASC

	/* get the course feed data */

	INSERT INTO #FeedMessages (
		idFeedMessage,
		idParentFeedMessage,
		feedType,
		feedObjectId,
		feedObjectName,
		feedObjectAvatar,
		idAuthor,
		[message],
		[timestamp],
		authorName,
		avatar,
		gender
	)
	SELECT TOP (@pageSize)
		CFM.idCourseFeedMessage AS idFeedMessage,
		CFM.idParentCourseFeedMessage AS idParentFeedMessage,
		'course' AS feedType,
		CFM.idCourse AS feedObjectId,
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS feedObjectName,
		C.avatar AS feedObjectAvatar,
		CFM.idAuthor,
		CFM.[message],
		CFM.[timestamp],
		CASE WHEN CFM.idAuthor > 1 THEN A.firstName + ' ' + A.lastName ELSE '##administrator##' END AS authorName,
		A.avatar,
		A.gender
	FROM tblCourseFeedMessage CFM
	LEFT JOIN tblCourse C ON C.idCourse = CFM.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	LEFT JOIN tblUser A ON A.idUser = CFM.idAuthor
	WHERE CFM.idSite = @idCallerSite
	AND C.isFeedActive = 1 -- feed is active
	AND (
			(@idCaller = 1) -- admin can moderate anything
			OR
			(
			@idCaller > 1 AND CFM.idCourse IN (SELECT DISTINCT CFMO.idCourse -- course(s) the caller is a moderator of
											   FROM tblCourseFeedModerator CFMO
											   WHERE CFMO.idModerator = @idCaller)
			)
		)
	AND	(CFM.isApproved IS NULL OR CFM.isApproved = 0)
	AND (
			(@getMessagesNewerThanDtQuery = 0 AND CFM.[timestamp] < @dtQuery)
			OR
			(@getMessagesNewerThanDtQuery = 1 AND CFM.[timestamp] > @dtQuery)
		)
	ORDER BY 
		CASE WHEN @getMessagesNewerThanDtQuery = 0 THEN [timestamp] ELSE '' END DESC,
		CASE WHEN @getMessagesNewerThanDtQuery = 1 THEN [timestamp] ELSE '' END ASC

	/* 
	
	get the timestamp of the last record, use min when getting records older than dtQuery
	and max when getting records newer than dtQuery

	*/

	IF (SELECT COUNT(1) FROM #FeedMessages) > 0

		BEGIN

		IF (@getMessagesNewerThanDtQuery = 0)
			BEGIN
			SELECT @dtLastRecord = MIN([timestamp]) FROM #FeedMessages
			END
		ELSE
			BEGIN
			SELECT @dtLastRecord = MAX([timestamp]) FROM #FeedMessages
			END

		END

	ELSE

		BEGIN
		SET @dtLastRecord = @dtQuery
		END

	/* return the data */

	IF (@getMessagesNewerThanDtQuery = 0)
		BEGIN

		SELECT 
			* 
		FROM #FeedMessages
		ORDER BY [timestamp] DESC

		END

	ELSE
		BEGIN

		SELECT 
			* 
		FROM #FeedMessages
		ORDER BY [timestamp] ASC

		END

	/* drop the temp table */

	DROP TABLE #FeedMessages

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO