-- =====================================================================
-- PROCEDURE: [RuleSetLearningPathEnrollment.Replicate]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.Replicate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetLearningPathEnrollment.Replicate]
GO

/*

Replicates a ruleset learning path enrollment to other learning path(s).

*/

CREATE PROCEDURE [dbo].[RuleSetLearningPathEnrollment.Replicate]
(
	@Return_Code											INT				OUTPUT,
	@Error_Description_Code									NVARCHAR(50)	OUTPUT,
	@idCallerSite											INT				= 0, --default if not specified
	@callerLangString										NVARCHAR(10),
	@idCaller												INT				= 0,

	@idRuleSetLearningPathEnrollmentToReplicate				INT,
	@idLearningPathsToReplicateTo							IDTABLE			READONLY,
	@idLearningPathsToUnSync								IDTABLE			READONLY

)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idRuleSetLearningPathEnrollmentReplicated				INT				= 0
	DECLARE @idLearningPath											INT
	DECLARE @idLearningPathUnsync									INT

	DECLARE learningPathReplicateCursor								CURSOR 
	FOR 
	SELECT id FROM @idLearningPathsToReplicateTo

	DECLARE learningPathUnsyncCursor								CURSOR
	FOR 
	SELECT id FROM @idLearningPathsToUnSync	
	
	/*

	cursor through learning paths to unsync ruleset enrollments

	*/

	IF EXISTS (SELECT 1 FROM @idLearningPathsToUnSync)
	BEGIN

		OPEN learningPathUnsyncCursor
		FETCH NEXT FROM learningPathUnsyncCursor INTO @idLearningPathUnsync
		WHILE @@FETCH_STATUS = 0  
		BEGIN

		UPDATE tblRuleSetLearningPathEnrollment 
		SET idParentRuleSetLearningPathEnrollment = NULL
		WHERE idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollmentToReplicate
		AND idLearningPath = @idLearningPathUnsync

		FETCH NEXT FROM learningPathUnsyncCursor INTO @idLearningPathUnsync

	END

		CLOSE learningPathUnsyncCursor
		DEALLOCATE learningPathUnsyncCursor

	END


	/*
	
	cursor through learning paths to replicate ruleset enrollment to
	
	*/

	IF EXISTS (SELECT 1 FROM @idLearningPathsToReplicateTo)
	BEGIN

		OPEN learningPathReplicateCursor
		FETCH NEXT FROM learningPathReplicateCursor INTO @idLearningPath
		WHILE @@FETCH_STATUS = 0  
		BEGIN

		IF (SELECT COUNT(1) FROM tblRuleSetLearningPathEnrollment WHERE idLearningPath = @idLearningPath AND idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollmentToReplicate  AND idSite = @idCallerSite) < 1
			BEGIN

			/*

			replicate ruleset enrollment	

			*/

			INSERT INTO tblRuleSetLearningPathEnrollment
			SELECT 
				idSite, 
				@idLearningPath, 
				[priority], 
				idTimezone,  
				dtStart, 
				dtEnd,
				delayInterval, 
				delayTimeframe, 
				dueInterval, 
				dueTimeframe, 
				label,
				expiresFromStartInterval, 
				expiresFromStartTimeframe, 
				expiresFromFirstLaunchInterval, 
				expiresFromFirstLaunchTimeframe,
				@idRuleSetLearningPathEnrollmentToReplicate
			FROM tblRuleSetLearningPathEnrollment RSLPE
			WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollmentToReplicate
			SET @idRuleSetLearningPathEnrollmentReplicated = SCOPE_IDENTITY()
		
			/*

			replicate ruleset enrollment language	

			*/

			INSERT INTO tblRuleSetLearningPathEnrollmentLanguage 
			SELECT 
				idSite, 
				@idRuleSetLearningPathEnrollmentReplicated, 
				idLanguage, 
				label
			FROM tblRuleSetLearningPathEnrollmentLanguage
			WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollmentToReplicate

			/*

			replicate rulesets	

			*/

			SELECT 
				idRuleSet AS [idSourceRuleSet], 
				idSite, 
				isAny, 
				label, 
				CAST(NULL AS INT) AS [idDestinationRuleSet]
			INTO #RuleSetsToReplicate
			FROM tblRuleSet
			WHERE idRuleSet IN (SELECT idRuleSet FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollmentToReplicate)

			INSERT INTO tblRuleSet
			SELECT 
				idSite, 
				isAny,
				CONCAT(label, ' ##', idSourceRuleSet, '##')
			FROM #RuleSetsToReplicate

			UPDATE #RuleSetsToReplicate 
			SET idDestinationRuleSet = RS.idRuleSet
			FROM tblRuleSet RS
			WHERE RS.label = #RuleSetsToReplicate.label + ' ##' + convert(nvarchar, #RuleSetsToReplicate.idSourceRuleSet) + '##'
		
			/*

			replicate ruleset languages	

			*/

			SELECT 
				idRuleSetLanguage AS [idSourceRuleSetLanguage], 
				idSite, 
				idRuleSet, 
				idLanguage, 
				label
			INTO #RuleSetLanguagesToReplicate
			FROM tblRuleSetLanguage
			WHERE idRuleSet IN (SELECT idSourceRuleSet FROM #RuleSetsToReplicate)

			INSERT INTO tblRuleSetLanguage
			SELECT 
				idSite, 
				idRuleSet, 
				idLanguage, 
				CONCAT(label, ' ##', idRuleSet, '##')
			FROM #RuleSetLanguagesToReplicate

			UPDATE tblRuleSetLanguage
			SET idRuleSet = RSTR.idDestinationRuleSet
			FROM #RuleSetsToReplicate RSTR
			LEFT JOIN tblRuleSetLanguage RSL on RSL.idRuleSet = RSTR.idSourceRuleSet
			WHERE RSL.label = RSTR.label + ' ##' + convert(nvarchar, RSTR.idSourceRuleSet) + '##'

			/*

			replicate ruleset to ruleset enrollment link

			*/

			INSERT INTO tblRuleSetToRuleSetLearningPathEnrollmentLink
			SELECT 
				idSite, 
				idDestinationRuleSet, 
				@idRuleSetLearningPathEnrollmentReplicated
			FROM #RuleSetsToReplicate 
		
			/*

			replicate rules

			*/

			SELECT 
				idRule, 
				idSite, 
				idRuleSet, 
				userField, 
				operator, 
				textValue, 
				dateValue, 
				numValue, 
				bitValue
			INTO #RulesToReplicate 
			FROM tblRule
			WHERE idRuleSet IN (SELECT idSourceRuleSet FROM #RuleSetsToReplicate)	
		
			INSERT INTO tblRule
			SELECT 
				RTR.idSite, 
				idDestinationRuleSet, 
				userField, 
				operator, 
				textValue, 
				dateValue, 
				numValue, 
				bitValue
			FROM #RulesToReplicate RTR
			LEFT JOIN #RuleSetsToReplicate RSTR ON RSTR.idSourceRuleSet = RTR.idRuleSet
		
			/*

			clean up tblRuleSet and tblRuleSetLanguage labels

			*/

			UPDATE tblRuleSet 
			SET label = RSTR.label
			FROM #RuleSetsToReplicate RSTR
			WHERE idRuleSet = RSTR.idDestinationRuleSet

			UPDATE tblRuleSetLanguage
			SET label = RSTR.label
			FROM #RuleSetsToReplicate RSTR
			WHERE idRuleSet = RSTR.idDestinationRuleSet

			/*

			drop temp tables

			*/

			DROP TABLE #RuleSetsToReplicate
			DROP TABLE #RulesToReplicate
			DROP TABLE #RuleSetLanguagesToReplicate

			END

			FETCH NEXT FROM learningPathReplicateCursor INTO @idLearningPath

		END

		CLOSE learningPathReplicateCursor
		DEALLOCATE learningPathReplicateCursor

	END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO