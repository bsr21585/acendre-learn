-- =====================================================================
-- PROCEDURE: [DocumentRepositoryItem.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DocumentRepositoryItem.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DocumentRepositoryItem.GetGrid]
GO

/*

Gets a listing of Document Repository Items that belong to an object.

*/

CREATE PROCEDURE [DocumentRepositoryItem.GetGrid]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idDocumentRepositoryObjectType		INT,
	@idObject							INT,
	@isUserView							BIT				= 0, --only necessary for profile files
	@searchParam						NVARCHAR(4000),
	@pageNum							INT, 
	@pageSize							INT, 
	@orderColumn						NVARCHAR(255),
	@orderAsc							BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		

		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END


		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString
		
		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblDocumentRepositoryItem DRI
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND DRI.idSite = @idCallerSite)
				)
				AND DRI.idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
				AND DRI.idObject = @idObject
				AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)
				AND (
					(@isUserView = 0) 
					OR 
					(@idDocumentRepositoryObjectType = 4 AND @isUserView = 1 AND (DRI.isVisibleToUser = 1 OR DRI.isVisibleToUser IS NULL)))
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						DRI.idDocumentRepositoryItem,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'label' THEN DRI.label END)	END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN DRF.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'kb' THEN DRI.[kb] END)	END DESC,
						
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'label' THEN DRI.label END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN DRF.name END) END ,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'kb' THEN DRI.[kb] END)	END
						)
						AS [row_number]
					FROM tblDocumentRepositoryItem DRI
					LEFT JOIN  tblDocumentRepositoryFolder DRF ON DRI.idDocumentRepositoryFolder = DRF.idDocumentRepositoryFolder
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND DRI.idSite = @idCallerSite)
						)
						AND DRI.idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
						AND DRI.idObject = @idObject
						AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)
						AND (
							(@isUserView = 0) 
							OR 
							(@idDocumentRepositoryObjectType = 4 AND @isUserView = 1 AND (DRI.isVisibleToUser = 1 OR DRI.isVisibleToUser IS NULL)))
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDocumentRepositoryItem, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				DRI.idDocumentRepositoryItem,
				DRI.[fileName],
				CASE WHEN DRIL.label IS NOT NULL THEN DRIL.label ELSE DRI.label END AS label, 
				DRF.name,
				--DRI.kb,
				CASE WHEN DRI.kb IS NOT NULL THEN
					CASE WHEN DRI.kb > 1024 THEN
						CASE WHEN DRI.kb > 1048576 THEN
							CONVERT(NVARCHAR, ROUND(CAST(1.0*DRI.kb/(1048576) AS DECIMAL(6,2)), 2, 1)) + ' GB'
						ELSE
							CONVERT(NVARCHAR, ROUND(CAST(1.0*DRI.kb/(1024) AS DECIMAL(6,2)), 2, 1)) + ' MB'
						END
					ELSE
						CONVERT(NVARCHAR, ROUND(DRI.kb, 2, 1)) + ' KB'
					END
				ELSE
					NULL
				END AS kb,
				DRI.isPrivate,
				CONVERT(bit, 1) AS isModifyOn, -- needs to be calculated
				DRI.isVisibleToUser,
				DRI.isUploadedByUser,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblDocumentRepositoryItem DRI ON DRI.idDocumentRepositoryItem = SelectedKeys.idDocumentRepositoryItem
			LEFT JOIN tblDocumentRepositoryItemLanguage DRIL ON DRIL.idDocumentRepositoryItem = DRI.idDocumentRepositoryItem AND DRIL.idLanguage = @idCallerLanguage
			LEFT JOIN tblDocumentRepositoryFolder DRF ON DRI.idDocumentRepositoryFolder = DRF.idDocumentRepositoryFolder
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

			SELECT COUNT(1) AS row_count 
			FROM tblDocumentRepositoryItemLanguage DRIL
			LEFT JOIN tblDocumentRepositoryItem DRI ON DRI.idDocumentRepositoryItem = DRIL.idDocumentRepositoryItem
			INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItemLanguage, *, @searchParam) K ON K.[key] = DRIL.idDocumentRepositoryItemLanguage
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND DRI.idSite = @idCallerSite)
				)
				AND DRI.idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
				AND DRI.idObject = @idObject
				AND DRIL.idLanguage = @idCallerLanguage
				AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)
				AND (
					(@isUserView = 0) 
					OR 
					(@idDocumentRepositoryObjectType = 4 AND @isUserView = 1 AND (DRI.isVisibleToUser = 1 OR DRI.isVisibleToUser IS NULL)))
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						DRI.idDocumentRepositoryItem,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'label' THEN DRI.label END)	END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN DRF.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'kb' THEN DRI.[kb] END)	END DESC,
						
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'label' THEN DRI.label END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN DRF.name END) END ,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'kb' THEN DRI.[kb] END)	END
						)
						AS [row_number]
					FROM tblDocumentRepositoryItemLanguage DRIL
					LEFT JOIN tblDocumentRepositoryItem DRI ON DRI.idDocumentRepositoryItem = DRIL.idDocumentRepositoryItem
					INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItemLanguage, *, @searchParam) K ON K.[key] = DRIL.idDocumentRepositoryItemLanguage
					LEFT JOIN  tblDocumentRepositoryFolder DRF ON DRI.idDocumentRepositoryFolder = DRF.idDocumentRepositoryFolder
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND DRI.idSite = @idCallerSite)
						)
						AND DRI.idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
						AND DRI.idObject = @idObject
						AND DRIL.idLanguage = @idCallerLanguage
						AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)
						AND (
							(@isUserView = 0) 
							OR 
							(@idDocumentRepositoryObjectType = 4 AND @isUserView = 1 AND (DRI.isVisibleToUser = 1 OR DRI.isVisibleToUser IS NULL)))
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDocumentRepositoryItem, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				DRI.idDocumentRepositoryItem,
				DRI.[fileName],
				CASE WHEN DRIL.label IS NOT NULL THEN DRIL.label ELSE DRI.label END AS label, 
				DRF.name,
				--DRI.kb,
				CASE WHEN DRI.kb IS NOT NULL THEN
					CASE WHEN DRI.kb > 1024 THEN
						CASE WHEN DRI.kb > 1048576 THEN
							CONVERT(NVARCHAR, ROUND(CAST(1.0*DRI.kb/(1048576) AS DECIMAL(6,2)), 2, 1)) + ' GB'
						ELSE
							CONVERT(NVARCHAR, ROUND(CAST(1.0*DRI.kb/(1024) AS DECIMAL(6,2)), 2, 1)) + ' MB'
						END
					ELSE
						CONVERT(NVARCHAR, ROUND(DRI.kb, 2, 1)) + ' KB'
					END
				ELSE
					NULL
				END AS kb,
				DRI.isPrivate,
				CONVERT(bit, 1) AS isModifyOn, -- needs to be calculated
				DRI.isVisibleToUser,
				DRI.isUploadedByUser,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblDocumentRepositoryItemLanguage DRIL ON DRIL.idDocumentRepositoryItem = SelectedKeys.idDocumentRepositoryItem AND DRIL.idLanguage = @idCallerLanguage
			LEFT JOIN tblDocumentRepositoryItem DRI ON DRI.idDocumentRepositoryItem = DRIL.idDocumentRepositoryItem
			LEFT JOIN tblDocumentRepositoryFolder DRF ON DRI.idDocumentRepositoryFolder = DRF.idDocumentRepositoryFolder
			ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
		ELSE
			
			BEGIN

			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblDocumentRepositoryItem DRI
			INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItem, *, @searchParam) K ON K.[key] = DRI.idDocumentRepositoryItem
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND DRI.idSite = @idCallerSite)
				)
				AND DRI.idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
				AND DRI.idObject = @idObject
				AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)
				AND (
					(@isUserView = 0) 
					OR 
					(@idDocumentRepositoryObjectType = 4 AND @isUserView = 1 AND (DRI.isVisibleToUser = 1 OR DRI.isVisibleToUser IS NULL)))
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						DRI.idDocumentRepositoryItem,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'label' THEN DRI.label END)	END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN DRF.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'kb' THEN DRI.[kb] END)	END DESC,
						
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'label' THEN DRI.label END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN DRF.name END) END ,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE	WHEN @orderColumn IS NULL OR @orderColumn = 'kb' THEN DRI.[kb] END)	END
						)
						AS [row_number]
					FROM tblDocumentRepositoryItem DRI
					INNER JOIN CONTAINSTABLE(tblDocumentRepositoryItem, *, @searchParam) K ON K.[key] = DRI.idDocumentRepositoryItem
					LEFT JOIN  tblDocumentRepositoryFolder DRF ON DRI.idDocumentRepositoryFolder = DRF.idDocumentRepositoryFolder
					WHERE 
						(
						@idCallerSite IS NULL
						OR 
						@idCallerSite IS NOT NULL AND DRI.idSite = @idCallerSite
						)
						AND DRI.idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
						AND DRI.idObject = @idObject
						AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)
						AND (
							(@isUserView = 0) 
							OR 
							(@idDocumentRepositoryObjectType = 4 AND @isUserView = 1 AND (DRI.isVisibleToUser = 1 OR DRI.isVisibleToUser IS NULL)))
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDocumentRepositoryItem, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				DRI.idDocumentRepositoryItem,
				DRI.[fileName],
				CASE WHEN DRIL.label IS NOT NULL THEN DRIL.label ELSE DRI.label END AS label, 
				DRF.name,
				--DRI.kb,
				CASE WHEN DRI.kb IS NOT NULL THEN
					CASE WHEN DRI.kb > 1024 THEN
						CASE WHEN DRI.kb > 1048576 THEN
							CONVERT(NVARCHAR, ROUND(CAST(1.0*DRI.kb/(1048576) AS DECIMAL(6,2)), 2, 1)) + ' GB'
						ELSE
							CONVERT(NVARCHAR, ROUND(CAST(1.0*DRI.kb/(1024) AS DECIMAL(6,2)), 2, 1)) + ' MB'
						END
					ELSE
						CONVERT(NVARCHAR, ROUND(DRI.kb, 2, 1)) + ' KB'
					END
				ELSE
					NULL
				END AS kb,
				DRI.isPrivate,
				CONVERT(bit, 1) AS isModifyOn, -- needs to be calculated
				DRI.isVisibleToUser,
				DRI.isUploadedByUser,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblDocumentRepositoryItemLanguage DRIL ON DRIL.idDocumentRepositoryItem = SelectedKeys.idDocumentRepositoryItem AND DRIL.idLanguage = @idCallerLanguage
			LEFT JOIN tblDocumentRepositoryItem DRI ON DRI.idDocumentRepositoryItem = DRIL.idDocumentRepositoryItem
			LEFT JOIN  tblDocumentRepositoryFolder DRF ON DRI.idDocumentRepositoryFolder = DRF.idDocumentRepositoryFolder
			ORDER BY SelectedKeys.[row_number]
			
			END
			
	END

END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO