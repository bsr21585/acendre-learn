-- =====================================================================
-- PROCEDURE: [Catalog.UpdateOrder]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.UpdateOrder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.UpdateOrder]
GO
	
/*

Updates Order of catalogs and courses

*/

CREATE PROCEDURE [dbo].[Catalog.UpdateOrder]
(
	@Return_Code				INT							OUTPUT,
	@Error_Description_Code		INT OUTPUT,
	@idCallerSite				INT							= 0,	--   default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT, 
	@CatalogsWithOrdering		IDTableWithOrdering			READONLY,
	@CoursesWithOrdering		IDTableCourseWithOrdering	READONLY
)
AS

	BEGIN
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	
	/*
	make sure there are objects in the table parameter
	*/
	
	IF ((SELECT COUNT(1) FROM @CatalogsWithOrdering) = 0 AND (SELECT COUNT(1) FROM @CoursesWithOrdering) = 0 )
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CatalogUpdateOrder_NoRecordsToUpdate'
		RETURN 1
		END
		
	
	/*
	
	updating catalog order
	
	*/
	
	UPDATE tblCatalog SET
	tblCatalog.[order] = CO.[order]
	FROM
    tblCatalog C, @CatalogsWithOrdering CO
    WHERE C.idCatalog = CO.id

	/*
	
	Updating Course order for unassigned courses
	
	*/
	
	;WITH CourseTable AS
	(
		SELECT C.[order] AS OrderToUpdate, CO.[order] AS SourceOrder
		FROM tblCourse C
		Inner join @CoursesWithOrdering CO ON CO.idCourse = C.idCourse
		WHERE CO.idCatalog IS NULL OR CO.idCatalog = 0
	)
	
	UPDATE CourseTable 
	SET OrderToUpdate = SourceOrder
	
	/*
	
	Updating Course order for assigned courses
	
	*/
	
	;WITH CourseCatalogTable AS
	(
		SELECT CCL.[order] AS OrderToUpdate, CO.[order] AS SourceOrder
		FROM tblCourseToCatalogLink CCL
		Inner join @CoursesWithOrdering CO ON CO.idCourse = CCL.idCourse AND CO.idCatalog = CCL.idCatalog
		WHERE CO.idCatalog IS NOT NULL AND CO.idCatalog <> 0
	)
	
	UPDATE CourseCatalogTable 
	SET OrderToUpdate = SourceOrder	


	SELECT @Return_Code = 0
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
