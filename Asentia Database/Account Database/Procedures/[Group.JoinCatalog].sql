-- =====================================================================
-- PROCEDURE: [Group.JoinCatalog]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.JoinCatalog]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.JoinCatalog]
GO

CREATE PROCEDURE [Group.JoinCatalog]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	--   default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, 
		
	@idGroup				INT,
	@Catalogs				IDTable			READONLY
)
AS

	BEGIN
	
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*

	validate group exists
	
	*/

	IF (
		SELECT COUNT(1)
		FROM tblGroup
		WHERE idGroup = @idGroup
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'GroupJoinCatalogs_DetailsNotFound'
		RETURN 1
		END
		
	/*
	
	validate that the objects exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Catalogs UU
		LEFT JOIN tblCatalog C ON C.idCatalog = UU.id
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'GroupJoinCatalogs_UsersNotInSameSite'
		RETURN 1
		END
	
	/*
	
	delete old records and insert new ones
	
	*/

	IF(SELECT COUNT(1) FROM tblCatalogAccessToGroupLink WHERE idGroup = @idGroup) > 0
	BEGIN
		DELETE FROM tblCatalogAccessToGroupLink WHERE idGroup = @idGroup
	END
	
	INSERT INTO tblCatalogAccessToGroupLink (
		idSite,
		idGroup,
		idCatalog,
		created
	)
	SELECT 
		@idCallerSite,
		@idGroup, 
		CC.id,
		GETUTCDATE()
	FROM @Catalogs CC	

	SELECT @Return_Code = 0
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

	
