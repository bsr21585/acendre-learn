-- =====================================================================
-- PROCEDURE: [Report.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Report.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Report.Delete]
GO

/*

Deletes reports

*/

CREATE PROCEDURE [Report.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@Reports				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT
			COUNT(1)
		FROM @Reports)
	= 0 BEGIN
	SET @Return_Code = 1 -- nothing to do
	SET @Error_Description_Code = 'ReportDelete_NoRecordFound'
	RETURN 1
	END
			
	/*
	
	validate that all Reports exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Reports RR
		LEFT JOIN tblReport R ON R.idReport = RR.id
		WHERE R.idSite IS NULL
		OR R.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		RETURN 1 
		END
		
	/*
	
	Mark the Report as deleted. DO NOT remove the record. Do NOT delete language or other sub records either.
	
	*/

	DELETE 
	FROM tblReportFile
	WHERE idReport IN (
		SELECT id
		FROM @Reports
	)	

	DELETE 
	FROM tblReportToReportShortcutsWidgetLink
	WHERE idReport IN (
		SELECT id
		FROM @Reports
	)	

	DELETE 
	FROM tblReportSubscription
	WHERE idReport IN (
		SELECT id
		FROM @Reports
	)	

	UPDATE tblReport SET
		isDeleted = 1,
		dtDeleted = GETUTCDATE()
	WHERE idReport IN (
		SELECT id
		FROM @Reports
	)	

	SELECT @Return_Code = 0
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO