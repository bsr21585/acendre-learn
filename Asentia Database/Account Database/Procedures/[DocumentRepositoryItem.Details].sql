-- =====================================================================
-- PROCEDURE: [DocumentRepositoryItem.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DocumentRepositoryItem.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DocumentRepositoryItem.Details]
GO

/*
Returns all the properties for a given id.
*/
CREATE PROCEDURE [DocumentRepositoryItem.Details]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR (10),
	@idCaller							INT				= 0,
	
	@idDocumentRepositoryItem			INT				OUTPUT,
	@idSite								INT				OUTPUT,
	@idDocumentRepositoryObjectType		INT				OUTPUT,
	@idObject							INT				OUTPUT,
	@idDocumentRepositoryFolder			INT				OUTPUT,
	@idOwner							INT				OUTPUT,
	@fileName							NVARCHAR(255)	OUTPUT, 
	@label								NVARCHAR(255)	OUTPUT, 
	@kb									INT				OUTPUT, 
	@searchTags							NVARCHAR(512)	OUTPUT,
	@isPrivate							BIT				OUTPUT,
	@idLanguage							INT				OUTPUT,
	@languageString						NVARCHAR(10)	OUTPUT,
	@isAllLanguages						BIT				OUTPUT,
	@dtCreated							DATETIME		OUTPUT,
	@isDeleted							BIT				OUTPUT,
	@dtDeleted							DATETIME		OUTPUT,
	@isVisibleToUser					BIT				OUTPUT,
	@isUploadedbyUser					BIT				OUTPUT	
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDocumentRepositoryItem
		WHERE idDocumentRepositoryItem = @idDocumentRepositoryItem
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DocumentRepositoryItemDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idDocumentRepositoryItem			= DRI.idDocumentRepositoryItem,
		@idSite								= DRI.idSite,
		@idDocumentRepositoryObjectType		= DRI.idDocumentRepositoryObjectType,
		@idObject							= DRI.idObject,
		@idDocumentRepositoryFolder			= DRI.idDocumentRepositoryFolder,
		@idOwner							= DRI.idOwner,
		@fileName							= DRI.[fileName],
		@label								= DRI.label,
		@kb									= DRI.kb,
		@searchTags							= DRI.searchTags,
		@isPrivate							= DRI.isPrivate,
		@idLanguage							= DRI.idLanguage,
		@languageString						= L.code,
		@isAllLanguages						= DRI.isAllLanguages,
		@dtCreated							= DRI.dtCreated,
		@isDeleted							= DRI.isDeleted,
		@dtDeleted							= DRI.dtDeleted,
		@isVisibleToUser					= DRI.isVisibleToUser,
		@isUploadedbyUser					= DRI.isUploadedByUser
	FROM tblDocumentRepositoryItem DRI
	LEFT JOIN tblLanguage L ON L.idLanguage = DRI.idLanguage
	WHERE DRI.idDocumentRepositoryItem = @idDocumentRepositoryItem
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DocumentRepositoryItemDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO