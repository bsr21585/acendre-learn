-- =====================================================================
-- PROCEDURE: [Course.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.GetGrid]
GO

/*

Gets a listing of Courses.

*/

CREATE PROCEDURE [Course.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblCourse C
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
				)
			AND (C.isDeleted IS NULL OR C.isDeleted = 0)

			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						C.idCourse,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN C.title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'coursecode' THEN C.coursecode END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END DESC,

						
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN C.title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'coursecode' THEN C.coursecode END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END
							
						
						)
						AS [row_number]
					FROM tblCourse C
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
						)
					AND (C.isDeleted IS NULL OR C.isDeleted = 0)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idCourse, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				C.idCourse,
				C.coursecode,
				CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title, 
				C.credits,
				C.avatar,
				CASE WHEN C.isPublished IS NULL THEN CONVERT(bit, 0) ELSE C.isPublished END AS isPublished,
				CASE WHEN C.isClosed IS NULL THEN CONVERT(bit, 0) ELSE C.isClosed END AS isClosed,
				CONVERT(bit, 1) AS isModifyOn, -- needs to be calculated
				CASE WHEN (SELECT COUNT(1) FROM tblCertificationToCourseCreditLink CCCL WHERE CCCL.idCourse = C.idCourse) > 0 THEN
					CONVERT(bit, 1)
				ELSE
					CONVERT(bit, 0)
				END AS isAttachedToACertification,
				CASE WHEN C.disallowRating IS NULL THEN CONVERT(bit, 0) ELSE C.disallowRating END AS disallowRating,
				(SELECT ISNULL(ROUND(AVG(CAST(rating AS FLOAT)), 1), 0) FROM tblCourseRating CR WHERE CR.idCourse = C.idCourse) AS rating,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblCourse C ON C.idCourse = SelectedKeys.idCourse
			LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
			WHERE (C.isDeleted IS NULL OR C.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblCourse C
				INNER JOIN CONTAINSTABLE(tblCourse, *, @searchParam) K ON K.[key] = C.idCourse
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
					)
				AND (C.isDeleted IS NULL OR C.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							C.idCourse,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN C.title END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'coursecode' THEN C.coursecode END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END DESC,

							
							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN C.title END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'coursecode' THEN C.coursecode END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END

							
							)
							AS [row_number]
						FROM tblCourse C
						INNER JOIN CONTAINSTABLE(tblCourse, *, @searchParam) K ON K.[key] = C.idCourse
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite
							)
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idCourse, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					C.idCourse, 
					C.coursecode,
					C.title,
					C.credits,
					C.avatar,
					CASE WHEN C.isPublished IS NULL THEN CONVERT(bit, 0) ELSE C.isPublished END AS isPublished,
					CASE WHEN C.isClosed IS NULL THEN CONVERT(bit, 0) ELSE C.isClosed END AS isClosed,
					CONVERT(bit, 1) AS isModifyOn,
					CASE WHEN (SELECT COUNT(1) FROM tblCertificationToCourseCreditLink CCCL WHERE CCCL.idCourse = C.idCourse) > 0 THEN
						CONVERT(bit, 1)
					ELSE
						CONVERT(bit, 0)
					END AS isAttachedToACertification,
					CASE WHEN C.disallowRating IS NULL THEN CONVERT(bit, 0) ELSE C.disallowRating END AS disallowRating,
					(SELECT ISNULL(ROUND(AVG(CAST(rating AS FLOAT)), 1), 0) FROM tblCourseRating CR WHERE CR.idCourse = C.idCourse) AS rating,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblCourse C ON C.idCourse = SelectedKeys.idCourse
				WHERE (C.isDeleted IS NULL OR C.isDeleted = 0)
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblCourseLanguage CL
				LEFT JOIN tblCourse C ON C.idCourse = CL.idCourse
				INNER JOIN CONTAINSTABLE(tblCourseLanguage, *, @searchParam) K ON K.[key] = CL.idCourseLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
					)
				AND CL.idLanguage = @idCallerLanguage
				AND (C.isDeleted IS NULL OR C.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							CL.idCourseLanguage,
							C.idCourse,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN CL.title END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'coursecode' THEN C.coursecode END) END DESC,
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END DESC,

							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'title' THEN CL.title END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'coursecode' THEN C.coursecode END) END,
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'credits' THEN C.credits END) END

							
							)
							AS [row_number]
						FROM tblCourseLanguage CL
						LEFT JOIN tblCourse C ON C.idCourse = CL.idCourse
						INNER JOIN CONTAINSTABLE(tblCourseLanguage, *, @searchParam) K ON K.[key] = CL.idCourseLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite
							)
						AND CL.idLanguage = @idCallerLanguage
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idCourse, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					C.idCourse, 
					C.coursecode,
					CL.title,
					C.credits,
					C.avatar,
					CASE WHEN C.isPublished IS NULL THEN CONVERT(bit, 0) ELSE C.isPublished END AS isPublished,
					CASE WHEN C.isClosed IS NULL THEN CONVERT(bit, 0) ELSE C.isClosed END AS isClosed,
					CONVERT(bit, 1) AS isModifyOn,
					CASE WHEN (SELECT COUNT(1) FROM tblCertificationToCourseCreditLink CCCL WHERE CCCL.idCourse = C.idCourse) > 0 THEN
						CONVERT(bit, 1)
					ELSE
						CONVERT(bit, 0)
					END AS isAttachedToACertification,
					CASE WHEN C.disallowRating IS NULL THEN CONVERT(bit, 0) ELSE C.disallowRating END AS disallowRating,
					(SELECT ISNULL(ROUND(AVG(CAST(rating AS FLOAT)), 1), 0) FROM tblCourseRating CR WHERE CR.idCourse = C.idCourse) AS rating,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblCourseLanguage CL ON CL.idCourse = SelectedKeys.idCourse AND CL.idLanguage = @idCallerLanguage
				LEFT JOIN tblCourse C ON C.idCourse = CL.idCourse
				ORDER BY SelectedKeys.[row_number]

				END
			
			END
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO