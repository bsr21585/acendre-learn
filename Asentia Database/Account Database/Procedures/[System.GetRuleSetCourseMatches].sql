-- =====================================================================
-- PROCEDURE: [System.GetRuleSetCourseMatches]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetRuleSetCourseMatches]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetRuleSetCourseMatches]
GO

/*

Gets RuleSet matches for Course RuleSet Enrollments.

*/
CREATE PROCEDURE [System.GetRuleSetCourseMatches]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR (50)	OUTPUT,
	@idCallerSite					INT				= 0,	-- default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT,

	@Courses						IDTable			READONLY,
	@Filters						IDTable			READONLY,
	@filterBy						NVARCHAR(10),
	@executeForRevokeProecdure		BIT				= 0		-- When true, this gets all matches regardless of priority or lifespan.
															-- The results are then used to revoke inherited ruleset enrollments where 
															-- the user no longer matches ANY ruleset enrollment for the course.
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON		

	/* SET FILTER TO 'user' IF IT IS ANYTHING OTHER THAN 'group' */
	IF @filterBy IS NULL OR LOWER(@filterBy) <> LOWER('group')
		SET @filterBy = 'user'

	/* CHECK COURSE AND FILTER LISTS FOR RECORDS */
	DECLARE @CoursesRecordCount INT
	DECLARE @FiltersRecordCount INT
	SELECT @CoursesRecordCount = COUNT(1) FROM @Courses
	SELECT @FiltersRecordCount = COUNT(1) FROM @Filters

	/* GET THE CURRENT UTC DATE */
	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/* CREATE TEMPORARY TABLE TO STORE USER-TO-RULESETENROLLMENT JOINS RESULTING FROM RULES EXECUTION */
	CREATE TABLE #UserToRuleSetEnrollmentJoins 
	(
		idRuleSetEnrollment		INT,
		[priority]				INT,
		idRuleSet INT, 
		idCourse INT, 
		idUser INT
	)

	/* CREATE TEMPORARY TABLE FOR RULESET ENROLLMENT AUTO-JOIN RULES */
	CREATE TABLE #RuleSetEnrollmentRules
	(
		idSite					INT,
		idRuleSet				INT,
		idRuleSetEnrollment		INT,
		idCourse				INT,
		[priority]				INT,
		isAny					BIT,
		field					NVARCHAR(25),
		operator				NVARCHAR(25),
		value					NVARCHAR(255)
	)

	/* PUT RULESET ENROLLMENT AUTO-JOIN RULESETS IN TEMP TABLE */
	IF (@executeForRevokeProecdure = 0)
		BEGIN

		INSERT INTO #RuleSetEnrollmentRules
		(
			idSite,
			idRuleSet,
			idRuleSetEnrollment,
			idCourse,
			[priority],
			isAny,
			field,
			operator,
			value
		)
		SELECT
			RS.idSite,
			RS.idRuleSet,
			RSRSEL.idRuleSetEnrollment,
			RSE.idCourse,
			RSE.[priority],
			RS.isAny,
			R.userField,
			R.operator,
			CASE WHEN R.textValue IS NOT NULL THEN 
				REPLACE(R.textValue, '''', '''''')
			ELSE
				CASE WHEN R.dateValue IS NOT NULL THEN
					CONVERT(NVARCHAR(255), R.dateValue)
				ELSE
					CASE WHEN R.numValue IS NOT NULL THEN
						CONVERT(NVARCHAR(255), R.numValue)
					ELSE
						CASE WHEN R.bitValue IS NOT NULL THEN
							CONVERT(NVARCHAR(255), R.bitValue)
						ELSE
							NULL
						END
					END
				END
			END
		FROM tblRuleSet RS
		LEFT JOIN tblRule R ON R.idRuleSet = RS.idRuleSet		
		LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSRSEL ON RSRSEL.idRuleSet = RS.idRuleSet
		LEFT JOIN tblRuleSetEnrollment RSE ON RSRSEL.idRuleSetEnrollment = RSE.idRuleSetEnrollment
		LEFT JOIN tblCourse C ON C.idCourse = RSE.idCourse
		WHERE RSRSEL.idRuleSetToRuleSetEnrollmentLink IS NOT NULL
		AND (
				(@CoursesRecordCount = 0)
				OR 
				(@CoursesRecordCount > 0 AND EXISTS (SELECT 1 FROM @Courses CP WHERE CP.id = RSE.idCourse))
			)
		-- NO RULES SHOULD BE EVALUATED OR MATCHED FOR RULESET ENROLLMENTS THAT ARE NOT WITHIN LIFESPAN
		AND (RSE.dtStart <= @utcNow)
		AND (RSE.dtEnd IS NULL OR RSE.dtEnd > @utcNow)
		-- NO RULES SHOULD BE EVALUATED OR MATCHED FOR COURSES THAT HAVE BEEN DELETED
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)

		END

	ELSE
		BEGIN

		INSERT INTO #RuleSetEnrollmentRules
		(
			idSite,
			idRuleSet,
			idRuleSetEnrollment,
			idCourse,
			[priority],
			isAny,
			field,
			operator,
			value
		)
		SELECT
			RS.idSite,
			RS.idRuleSet,
			RSRSEL.idRuleSetEnrollment,
			RSE.idCourse,
			RSE.[priority],
			RS.isAny,
			R.userField,
			R.operator,
			CASE WHEN R.textValue IS NOT NULL THEN 
				REPLACE(R.textValue, '''', '''''')
			ELSE
				CASE WHEN R.dateValue IS NOT NULL THEN
					CONVERT(NVARCHAR(255), R.dateValue)
				ELSE
					CASE WHEN R.numValue IS NOT NULL THEN
						CONVERT(NVARCHAR(255), R.numValue)
					ELSE
						CASE WHEN R.bitValue IS NOT NULL THEN
							CONVERT(NVARCHAR(255), R.bitValue)
						ELSE
							NULL
						END
					END
				END
			END
		FROM tblRuleSet RS
		LEFT JOIN tblRule R ON R.idRuleSet = RS.idRuleSet		
		LEFT JOIN tblRuleSetToRuleSetEnrollmentLink RSRSEL ON RSRSEL.idRuleSet = RS.idRuleSet
		LEFT JOIN tblRuleSetEnrollment RSE ON RSRSEL.idRuleSetEnrollment = RSE.idRuleSetEnrollment
		LEFT JOIN tblCourse C ON C.idCourse = RSE.idCourse
		WHERE RSRSEL.idRuleSetToRuleSetEnrollmentLink IS NOT NULL
		AND (
				(@CoursesRecordCount = 0)
				OR 
				(@CoursesRecordCount > 0 AND EXISTS (SELECT 1 FROM @Courses CP WHERE CP.id = RSE.idCourse))
			)
		-- NO RULES SHOULD BE EVALUATED OR MATCHED FOR COURSES THAT HAVE BEEN DELETED
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)

		END

	/* DECLARE VARIABLES FOR BUILDING RULE EXECUTION QUERIES */
	DECLARE @ruleSetEnrollmentsWithRuleSets TABLE (id INT, idSite INT)
	DECLARE @ruleSetsForRuleSetEnrollment IDTable
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @idRuleSetEnrollment INT
	DECLARE @idCourse INT
	DECLARE @priority INT
	DECLARE @idSite INT
	DECLARE @idRuleSet INT
	DECLARE @isAny BIT
	DECLARE @field NVARCHAR(25)
	DECLARE @operator NVARCHAR(25)
	DECLARE @value NVARCHAR(255)
	DECLARE @ruleCounter INT

	/* GET IDS OF RULESET ENROLLMENTS WITH RULESETS */
	INSERT INTO @ruleSetEnrollmentsWithRuleSets ( 
		id,
		idSite
	)
	SELECT DISTINCT
		idRuleSetEnrollment,
		idSite
	FROM #RuleSetEnrollmentRules

	/* CURSOR FOR RULESET ENROLLMENTS WITH RULESETS */
	DECLARE ruleSetEnrollmentsCursor CURSOR FOR
	SELECT DISTINCT
		RS.id,
		RSER.idCourse,
		RSER.[priority],
		RS.idSite
	FROM @ruleSetEnrollmentsWithRuleSets RS
	LEFT JOIN #RuleSetEnrollmentRules RSER ON RSER.idRuleSetEnrollment = id

	OPEN ruleSetEnrollmentsCursor

	FETCH NEXT FROM ruleSetEnrollmentsCursor INTO @idRuleSetEnrollment, @idCourse, @priority, @idSite

	/* LOOP THROUGH RULESET ENROLLMENT RULESETS */
	WHILE (@@FETCH_STATUS = 0)

		BEGIN

		/* GET IDS OF RULESETS FOR THE RULESET ENROLLMENT */
		INSERT INTO @ruleSetsForRuleSetEnrollment
		( id )
		SELECT DISTINCT
			idRuleSet
		FROM #RuleSetEnrollmentRules
		WHERE idRuleSetEnrollment = @idRuleSetEnrollment

		/* CURSOR FOR RULESETS FOR THE RULESET ENROLLMENT */
		DECLARE ruleSetEnrollmentRuleSetsCursor CURSOR FOR
		SELECT DISTINCT
			id,
			RSER.isAny
		FROM @ruleSetsForRuleSetEnrollment
		LEFT JOIN #RuleSetEnrollmentRules RSER ON RSER.idRuleSet = id

		OPEN ruleSetEnrollmentRuleSetsCursor

		FETCH NEXT FROM ruleSetEnrollmentRuleSetsCursor INTO @idRuleSet, @isAny
	
		/* LOOP THROUGH RULESETS FOR THE RULESET ENROLLMENT */
		WHILE (@@FETCH_STATUS = 0)

			BEGIN

			/* START QUERY SQL STRING FOR RULESET */
			SET @sql = 'INSERT INTO #UserToRuleSetEnrollmentJoins (idRuleSetEnrollment, [priority], idRuleSet, idCourse, idUser) SELECT ' + CONVERT(NVARCHAR, @idRuleSetEnrollment) + ', ' + CONVERT(NVARCHAR, @priority) + ', ' + CONVERT(NVARCHAR, @idRuleSet) + ', ' + CONVERT(NVARCHAR, @idCourse) + ', idUser ' + 'FROM tblUser U WHERE ('
			
			/* RESET THE RULE COUNTER */
			SET @ruleCounter = 0

			/* CURSOR FOR RULESET RULES */
			DECLARE ruleSetRulesCursor CURSOR FOR
			SELECT
				field,
				operator,
				value
			FROM #RuleSetEnrollmentRules
			WHERE idRuleSet = @idRuleSet

			OPEN ruleSetRulesCursor
		
			FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
	
			/* LOOP THROUGH RULESET RULES */
			WHILE (@@FETCH_STATUS = 0)

				BEGIN

				/* IF THIS IS NOT THE FIRST RULE, EXAMINE isAny LOGIC TO DETERMINE AND/OR OPERATOR */
				IF @ruleCounter > 0
					BEGIN
						IF @isAny = 1
							BEGIN
							SET @sql = @sql + ' OR '
							END
						ELSE
							BEGIN
							SET @sql = @sql + ' AND '
							END
					END

				/* 
				
				BUILD WHERE CLAUSE BASED ON OPERATOR - DIFFERENT LOGIC FOR GROUP 
				GROUP EVALUATES ON THE "name" IN THE BASE TABLE, NOT EVERY LANGUAGE JUST THE DEFAULT

				*/

				IF @field = 'group'
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					END

				ELSE
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' = ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' <> ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) < ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) < ' + '''' + @value + '''' + ')'
						END

					END
			
				/* INCREMENT RULE COUNTER */
				SET @ruleCounter = @ruleCounter + 1

				FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
				END

			/* CLOSE AND DEALLOCATE CURSOR FOR RULESET RULES */
			CLOSE ruleSetRulesCursor
			DEALLOCATE ruleSetRulesCursor

			/* APEND ANDs FOR THE SITE AND USER DELETED STATUS */
			SET @sql = @sql + ') AND (U.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)))'
			
			/* EXECUTE THE SQL TO INSERT RECORDS IN THE #UserToRuleSetEnrollmentJoins TABLE */
			EXECUTE sp_executesql @sql
		
			FETCH NEXT FROM ruleSetEnrollmentRuleSetsCursor INTO @idRuleSet, @isAny
			END

		/* CLOSE AND DEALLOCATE CURSOR FOR RULESETS FOR THE RULESET ENROLLMENT */
		DELETE FROM @ruleSetsForRuleSetEnrollment
		CLOSE ruleSetEnrollmentRuleSetsCursor
		DEALLOCATE ruleSetEnrollmentRuleSetsCursor

		FETCH NEXT FROM ruleSetEnrollmentsCursor INTO @idRuleSetEnrollment, @idCourse, @priority, @idSite
		END

	/* CLOSE AND DEALLOCATE CURSOR FOR RULESET ENROLLMENTS WITH RULESETS */
	CLOSE ruleSetEnrollmentsCursor
	DEALLOCATE ruleSetEnrollmentsCursor
				  
	/* RETURN RECORDS */
	IF (@executeForRevokeProecdure = 0)
		BEGIN		

		SELECT DISTINCT
			C.idSite,
			URSEJ.idRuleSetEnrollment,
			URSEJ.[priority],
			URSEJ.idRuleSet,
			URSEJ.idCourse,
			URSEJ.idUser
		FROM #UserToRuleSetEnrollmentJoins URSEJ
		LEFT JOIN tblCourse C ON C.idCourse = URSEJ.idCourse
		LEFT JOIN tblUserToGroupLink UGL ON UGL.idUser = URSEJ.idUser
		WHERE (  
				  (
					  @filterBy = 'user' AND
					  (
						(@FiltersRecordCount = 0)
						OR 
						(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = URSEJ.idUser))
					  )
				  )
				  OR
				  (
					  @filterBy = 'group' AND
					  (
						(@FiltersRecordCount = 0)
						OR 
						(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = UGL.idGroup))
					  )
				  )
			  )
		AND EXISTS (
			SELECT MIN(URSEJ2.[priority])
			FROM #UserToRuleSetEnrollmentJoins URSEJ2
			GROUP BY URSEJ2.idUser, URSEJ2.idCourse
			HAVING MIN(URSEJ2.[priority]) = URSEJ.[priority] -- having the 'highest' (lowest number) priority
				AND URSEJ2.idUser = URSEJ.idUser
				AND URSEJ2.idCourse = URSEJ.idCourse		
		)

		END

	ELSE
		BEGIN

		SELECT DISTINCT
			C.idSite,
			URSEJ.idRuleSetEnrollment,
			URSEJ.[priority],
			URSEJ.idRuleSet,
			URSEJ.idCourse,
			URSEJ.idUser
		FROM #UserToRuleSetEnrollmentJoins URSEJ
		LEFT JOIN tblCourse C ON C.idCourse = URSEJ.idCourse
		LEFT JOIN tblUserToGroupLink UGL ON UGL.idUser = URSEJ.idUser
		WHERE (  
				  (
					  @filterBy = 'user' AND
					  (
						(@FiltersRecordCount = 0)
						OR 
						(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = URSEJ.idUser))
					  )
				  )
				  OR
				  (
					  @filterBy = 'group' AND
					  (
						(@FiltersRecordCount = 0)
						OR 
						(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = UGL.idGroup))
					  )
				  )
			  )

		END

	/* DROP TEMPORARY TABLE(S) */
	DROP TABLE #RuleSetEnrollmentRules
	DROP TABLE #UserToRuleSetEnrollmentJoins

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO