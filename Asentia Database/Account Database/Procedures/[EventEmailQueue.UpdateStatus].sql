SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventEmailQueue.UpdateStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventEmailQueue.UpdateStatus]
GO

/*

Updates an event email queue item's status as either sent or failed.

*/

CREATE PROCEDURE [EventEmailQueue.UpdateStatus]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@idEventEmailQueue		INT,
	@isFailed				BIT,
	@statusDescription		NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	do not validate that the caller is a member of the site as this can be called from the job processor
	
	*/

	/*
	
	do not validate caller permission as this can be called from the job processor
	
	*/

			
	/*
	
	validate that the queue item exists
	
	*/
	
	IF (SELECT COUNT(1) FROM tblEventEmailQueue WHERE idEventEmailQueue = @idEventEmailQueue) = 0
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EventEmailQueueUpdateStatus_RecordNotFound'
		RETURN 1 
		END
		
	/* 
	
	if the queue item failed, update the status description
	otherwise, update the sent date

	*/

	IF (@isFailed = 1)
		BEGIN
		UPDATE tblEventEmailQueue SET statusDescription = @statusDescription WHERE idEventEmailQueue = @idEventEmailQueue
		END
	ELSE
		BEGIN
		UPDATE tblEventEmailQueue SET dtSent = GETUTCDATE(), statusDescription = NULL WHERE idEventEmailQueue = @idEventEmailQueue
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO