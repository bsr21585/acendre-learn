-- =====================================================================
-- PROCEDURE: [Activity.Import]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Activity.Import]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Activity.Import]
GO

-- =============================================
-- Author:		Daniel Ko
-- Modified date: July 22nd 2016
-- Description:	This Stored Procedure is for the purpose of Importing the Activity Sheet in Batch
-- =============================================
CREATE PROCEDURE [dbo].[Activity.Import]
	-- Add the parameters for the stored procedure here	
    @Return_Code				INT				OUTPUT ,
    @Error_Description_Code		NVARCHAR(50)	OUTPUT ,
    @idCallerSite				INT				= 0,
    @callerLangString			NVARCHAR(10) ,
    @idCaller					INT				= 0,
    
	@idActivityImport			INT ,
    @ActivityImportTable		ActivityImport	READONLY
AS 
     BEGIN
    
        SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
    SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

		/*

		initialize
		get import record, validate site, get timezone information

		*/

		DECLARE @idSite INT
		DECLARE @idUser INT			-- the importing user
		DECLARE @idUserImported INT -- the user the import is for (null = multiple users)
		DECLARE @idTimeZone INT
		DECLARE @gmtOffset INT
		DECLARE @tzUseDST BIT

		SELECT 
			@idSite = idSite,
			@idUser = idUser,
			@idUserImported = idUserImported
		FROM
			tblActivityImport
		WHERE
			idActivityImport = @idActivityImport 

		-- grab the importing user's timezone; if admin, grab the site timezone
		IF @idUser = 1
			BEGIN	
			SELECT @idTimezone = idTimezone 
			FROM tblSite
			WHERE idSite = @idSite
			END
	
		IF @idUser > 1
			BEGIN	
			SELECT @idTimezone = idTimezone
			FROM tblUser
			WHERE idUser = @idUser
			AND idSite = @idSite
			END
	
		-- if user doesnt have a timezone, get the site's
		IF @idTimeZone IS NULL
			BEGIN	
			SELECT @idTimezone = idTimezone 
			FROM tblSite
			WHERE idSite = @idSite
			END
				
		-- if all else fails, bye bye
		IF @idTimezone is null
			BEGIN	
			-- remove the record of the activity import
			-- another procedure will clean up raw data
			DELETE FROM tblActivityImport 
			WHERE idActivityImport = @idActivityImport
	
			SET @Return_Code = 1
			
			SET @Error_Description_Code = 'ActivityImport_SiteTimezoneNotFound'
			RETURN 1
	
			END
	
		-- get the gmt offset from the timezone
		SELECT @gmtOffset = gmtOffset
		FROM tblTimezone
		WHERE idTimezone = @idTimezone

		-- get whether or not the timezone uses dst
		SELECT @tzUseDST = blnUseDaylightSavings
		FROM tblTimeZone
		WHERE idTimezone = @idTimezone
					
	/*
	
	validate that the specified language is the default language for the site
	
	*/
	
        DECLARE @idCallerLanguage INT
        SELECT  @idCallerLanguage = idLanguage
        FROM    tblLanguage
        WHERE   code = @callerLangString
	
        IF ( SELECT COUNT(1)
             FROM   tblSite S
             WHERE  idSite = @idCallerSite
                    AND idLanguage = @idCallerLanguage
                    AND @idCallerLanguage IS NOT NULL
           ) <> 1 
            BEGIN 
                SELECT  @Return_Code = 5
                SET @Error_Description_Code = 'ActivityImport_SpecifiedLanguageNotDefault'
                RETURN 1 
            END			 
		
	/*	
	save the data	
	*/   
    
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.  
       INSERT INTO dbo.tblEnrollment (
			idSite,
			idCourse,
			revcode,
			idUser,
			isLockedByPrerequisites,
			idTimezone,
			dtCreated,
			dtStart,
			dtCompleted,
			title,
			idActivityImport,
			credits
       )
		SELECT DISTINCT  
			@idCallerSite , -- idSite - int
			C.idCourse , -- idCourse - int
			C.revcode , -- revcode - nvarchar(32)
			U.idUser , -- idUser - int
			0 , -- isLockedByPrerequisites - bit
			@idTimezone AS idTimezone, 
			GETDATE(),
			CASE WHEN dbo.isDateDST(AI.dtCompleted) = 1 and @tzUseDST = 1 THEN DATEADD(DAY, -1, DATEADD(HOUR, (@gmtOffset * -1) - 1, AI.dtCompleted)) ELSE DATEADD(DAY, -1, DATEADD(HOUR, @gmtOffset * -1, AI.dtCompleted)) END AS dtStart,
			CASE WHEN dbo.isDateDST(AI.dtCompleted) = 1 and @tzUseDST = 1 THEN DATEADD(HOUR, (@gmtOffset * -1) - 1, AI.dtCompleted) ELSE DATEADD(HOUR, @gmtOffset * -1, AI.dtCompleted) END AS dtCompleted,
			C.title AS title,
			@idActivityImport AS idActivityImport,
			AI.credits AS credits
		FROM @ActivityImportTable AI
		LEFT JOIN tblCourse C ON C.coursecode = AI.courseCode AND C.idSite = @idCallerSite
		LEFT JOIN tblUser U ON U.username = AI.username AND U.idSite = @idCallerSite
		WHERE LEN(AI.courseCode)>0 
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)  

    -- CHECK IF ANY COUSE CAN QUALIFIED TO BE UNLOCKED
		UPDATE tblEnrollment 
		SET isLockedByPrerequisites = 0
		WHERE idEnrollment IN
		(
			SELECT E.idEnrollment 
			FROM tblEnrollment E
			LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
			LEFT JOIN tblUser U ON U.idUser = E.idUser
			LEFT JOIN @ActivityImportTable AI ON AI.username = U.username
			WHERE E.isLockedByPrerequisites = 1
			AND E.idCourse IN
			(
				SELECT CP1.idCourse
				FROM tblCourseToPrerequisiteLink CP1
				LEFT JOIN tblEnrollment E1 ON E1.idCourse = CP1.idCourse
				LEFT JOIN tblCourse C1 ON C1.idCourse = CP1.idPrerequisite
				WHERE E1.idUser = E.idUser AND C1.coursecode = AI.courseCode
				AND E1.dtCompleted IS NULL 
			)
			AND C.isPrerequisiteAny = 1

			UNION

			SELECT E.idEnrollment 
			FROM tblEnrollment E
			LEFT JOIN tblCourse C ON E.idCourse = C.idCourse
			LEFT JOIN tblCourseToPrerequisiteLink CP ON CP.idPrerequisite = E.idCourse
			LEFT JOIN tblUser U ON U.idUser = E.idUser
			LEFT JOIN @ActivityImportTable AI ON AI.username = U.username
			WHERE E.isLockedByPrerequisites = 1
			AND E.idCourse IN
			(
				SELECT CP1.idCourse
				FROM tblCourseToPrerequisiteLink CP1
				LEFT JOIN tblCourse C1 ON C1.idCourse = CP1.idPrerequisite
				WHERE C1.coursecode = AI.courseCode
			)
			AND C.isPrerequisiteAny = 0
			AND NOT EXISTS (
				SELECT 1
				FROM tblCourseToPrerequisiteLink CP2
				LEFT JOIN tblEnrollment E2 ON E2.idCourse = CP2.idPrerequisite AND E2.idUser = E.idUser
				WHERE CP2.idCourse = E.idCourse AND E2.dtCompleted IS NULL
			) 
		)

   
    -- INSERT WHEN ONLY COURSE NAME BUT NO COURSE CODE
	      INSERT INTO dbo.tblEnrollment (
			idSite,
			idUser,
			isLockedByPrerequisites,
			idTimezone,
			dtCreated,
			dtStart,
			dtCompleted,
			title,
			idActivityImport,
			credits
       )
		SELECT DISTINCT   
			@idCallerSite , -- idSite - int
			U.idUser , -- idUser - int
			0 , -- isLockedByPrerequisites - bit
			@idTimezone AS idTimezone, 
			GETDATE(),
			CASE WHEN dbo.isDateDST(AI.dtCompleted) = 1 and @tzUseDST = 1 THEN DATEADD(DAY, -1, DATEADD(HOUR, (@gmtOffset * -1) - 1, AI.dtCompleted)) ELSE DATEADD(DAY, -1, DATEADD(HOUR, @gmtOffset * -1, AI.dtCompleted)) END AS dtStart,
			CASE WHEN dbo.isDateDST(AI.dtCompleted) = 1 and @tzUseDST = 1 THEN DATEADD(HOUR, (@gmtOffset * -1) - 1, AI.dtCompleted) ELSE DATEADD(HOUR, @gmtOffset * -1, AI.dtCompleted) END AS dtCompleted,
			AI.courseName AS title,
			@idActivityImport AS idActivityImport,
			AI.credits AS credits
		FROM @ActivityImportTable AI
		LEFT JOIN tblUser U ON U.username = AI.username AND U.idSite = @idCallerSite
		WHERE LEN(AI.courseCode)=0 OR AI.courseCode IS NULL

	-- INSERT statements for tbl-Data-Lesson with course code
		INSERT INTO dbo.[tblData-Lesson] ( 
				idSite,
				idEnrollment,
                title,
                revcode,
                [order],
                dtCompleted,
                idTimezone
       )
		SELECT 
			@idCallerSite , -- idSite - int
			ER.idEnrollment, 
            AI.lessonName AS title,
            C.revcode AS revcode, -- revcode - nvarchar(32)
            1 , -- order - int
			CASE WHEN dbo.isDateDST(AI.dtCompleted) = 1 and @tzUseDST = 1 THEN DATEADD(HOUR, (@gmtOffset * -1) - 1, AI.dtCompleted) ELSE DATEADD(HOUR, @gmtOffset * -1, AI.dtCompleted) END AS dtCompleted,
			@idTimezone AS idTimezone
         FROM @ActivityImportTable AI
		 LEFT JOIN tblCourse C ON C.coursecode = AI.courseCode AND C.idSite = @idCallerSite
		 LEFT JOIN tblUser U ON U.username = AI.username  AND U.idSite = @idCallerSite
		 LEFT JOIN tblEnrollment ER ON ER.idUser = U.idUser AND ER.idCourse = C.idCourse AND ER.idActivityImport = @idActivityImport
		 WHERE LEN(AI.lessonName)>0 AND LEN(AI.courseCode)>0   
		 AND (C.isDeleted IS NULL OR C.isDeleted = 0)

	-- INSERT statements for tbl-Data-Lesson without course code
		INSERT INTO dbo.[tblData-Lesson] ( 
				idSite,
				idEnrollment,
                title,
                [order],
                dtCompleted,
                idTimezone
       )
		SELECT 
			@idCallerSite , -- idSite - int
			ER.idEnrollment, 
            AI.lessonName AS title, 
            1 , -- order - int
			CASE WHEN dbo.isDateDST(AI.dtCompleted) = 1 and @tzUseDST = 1 THEN DATEADD(HOUR, (@gmtOffset * -1) - 1, AI.dtCompleted) ELSE DATEADD(HOUR, @gmtOffset * -1, AI.dtCompleted) END AS dtCompleted,
			@idTimezone AS idTimezone
         FROM @ActivityImportTable AI
		 LEFT JOIN tblUser U ON U.username = AI.username AND U.idSite = @idCallerSite
		 LEFT JOIN tblEnrollment ER ON ER.idUser = U.idUser AND ER.title = AI.courseName AND ER.idActivityImport = @idActivityImport
		 WHERE LEN(AI.lessonName)>0 AND (LEN(AI.courseCode)=0 OR AI.courseCode IS NULL)

	---- INSERT statements for tbl-Data-SCO with course code
        INSERT INTO dbo.[tblData-SCO] ( 
				idSite,
				[idData-Lesson],
                completionStatus ,
                successStatus ,
                scoreScaled ,
                [timestamp] ,
                idTimezone 
        )
		 SELECT 
			@idCallerSite AS idSite,
            DL.[idData-Lesson] AS [idData-Lesson], 
            CS.idSCORMVocabulary AS completionStatus,
			SS.idSCORMVocabulary AS successStatus,
			AI.scoreScaled AS scoreScaled,
			CASE WHEN dbo.isDateDST(AI.[timestamp]) = 1 AND @tzUseDST = 1 THEN DATEADD(HOUR, (@gmtOffset * -1) - 1, AI.[timestamp]) ELSE dateadd(hour, @gmtOffset * -1, AI.[timestamp]) END AS [timestamp],
			@idTimezone AS idTimezone
         FROM @ActivityImportTable AI
		 LEFT JOIN tblUser U ON U.username = AI.username AND U.idSite = @idCallerSite
		 LEFT JOIN tblEnrollment ER ON ER.idUser = U.idUser AND ER.idActivityImport = @idActivityImport AND ER.idSite = @idCallerSite
		 LEFT JOIN [tblData-Lesson] DL ON DL.idEnrollment = ER.idEnrollment AND DL.title = AI.lessonName AND DL.idSite = @idCallerSite
		 LEFT JOIN tblSCORMVocabulary CS ON CS.value = AI.completionStatus
		 LEFT JOIN tblSCORMVocabulary SS ON SS.value = AI.successStatus     
		 WHERE LEN(AI.lessonName)>0 AND DL.[idData-Lesson] IS NOT NULL    
                                       
		SELECT  @Return_Code = 0
		SET @Error_Description_Code = 'ActivityImport_SuccessfullyComplete'
		RETURN 1                                   

    END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

