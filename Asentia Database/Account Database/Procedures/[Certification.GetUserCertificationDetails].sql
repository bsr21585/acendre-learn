-- =====================================================================
-- PROCEDURE: [Certification.GetUserCertificationDetails]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.GetUserCertificationDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.GetUserCertificationDetails]
GO

/*

Gets base details of a user's certification such as expiration dates and certification track.

*/

CREATE PROCEDURE [Certification.GetUserCertificationDetails]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0,

	@idCertificationToUserLink		INT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	evaluate certification completion

	*/

	DECLARE @idUser INT

	SELECT
		@idUser = CUL.idUser
	FROM tblCertificationToUserLink CUL
	WHERE CUL.idCertificationToUserLink = @idCertificationToUserLink

	EXEC [Certification.EvaluateCompletion] NULL, NULL, @idCallerSite, @callerLangString, 1, @idCertificationToUserLink, @idUser

	/*

	get the data 

	*/

	SELECT
		CUL.idUser,
		CUL.initialAwardDate,
		CUL.certificationTrack,
		CUL.lastExpirationDate,
		CUL.currentExpirationDate,
		CUL.certificationId
	FROM tblCertificationToUserLink CUL
	WHERE CUL.idSite = @idCallerSite
	AND CUL.idCertificationToUserLink = @idCertificationToUserLink
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	