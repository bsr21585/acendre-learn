-- =====================================================================
-- PROCEDURE: [StandUpTrainingInstanceToUserLink.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandUpTrainingInstanceToUserLink.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandUpTrainingInstanceToUserLink.Details]
GO

/*

Return all the linked properties for a given standup training instance id and user id.

*/
CREATE PROCEDURE [StandUpTrainingInstanceToUserLink.Details]
(
	@Return_Code							INT				OUTPUT,
	@Error_Description_Code					NVARCHAR(50)	OUTPUT,
	@idCallerSite							INT				= 0, --default if not specified
	@callerLangString						NVARCHAR(10),
	@idCaller								INT				= 0,
	
	@idStandupTrainingInstanceToUserLink	INT				OUTPUT,
	@idUser									INT				OUTPUT,
	@idStandupTrainingInstance				INT				OUTPUT,
	@completionStatus						INT				OUTPUT,
	@score									INT				OUTPUT, 
	@isWaitingList							BIT				OUTPUT,
	@registrantKey							BIGINT			OUTPUT,	
	@registrantEmail						NVARCHAR(255)	OUTPUT,
	@joinUrl								NVARCHAR(1024)	OUTPUT,
	@waitlistOrder							INT				OUTPUT	
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that an object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblStandupTrainingInstanceToUserLink
		WHERE idStandUpTrainingInstance = @idStandupTrainingInstance
		AND idUser = @idUser
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'STIULDetails_NoRecordFound'
		RETURN 1
	    END
		
	/*
	
	get the data 
	
	*/
		
	SELECT TOP 1 -- unnecessary, but lets be safe just in case somehow a user made it in twice
		@idStandupTrainingInstanceToUserLink = STIUL.idStandupTrainingInstanceToUserLink,
		@idUser								 = STIUL.idUser,
		@idStandupTrainingInstance			 = STIUL.idStandupTrainingInstance,
		@completionStatus					 = STIUL.completionStatus,
		@score								 = STIUL.score,
		@isWaitingList						 = STIUL.isWaitingList,
		@registrantKey						 = STIUL.registrantKey,
		@registrantEmail					 = STIUL.registrantEmail,
		@joinUrl							 = STIUL.joinUrl,
		@waitlistOrder						 = STIUL.waitlistOrder
	FROM tblStandupTrainingInstanceToUserLink STIUL
	WHERE STIUL.idStandUpTrainingInstance = @idStandupTrainingInstance
	AND STIUL.idUser = @idUser
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'STIULDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO