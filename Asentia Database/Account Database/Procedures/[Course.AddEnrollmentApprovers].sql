-- =====================================================================
-- PROCEDURE: [Course.AddEnrollmentApprovers]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.AddEnrollmentApprovers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.AddEnrollmentApprovers]
GO

/*

Adds approvers to the course

*/

CREATE PROCEDURE [Course.AddEnrollmentApprovers]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idCourse				INT,
	@Approvers				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the course exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourse
		WHERE idSite = @idCallerSite
		AND @idCourse = idCourse
		) <> 1
		BEGIN
		SELECT @Return_Code = 2
		RETURN 1 
		END
		
	/*
	
	validate that all the approvers exist in the same site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Approvers A
		LEFT JOIN tblUser U ON U.idUser = A.id
		WHERE U.idUser IS NULL -- user does not exist at all
		OR U.idSite <> @idCallerSite -- user does not exist within the same site
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		RETURN 1 
		END
	
	/*
	
	insert the approvers where they do not already exist
	
	*/
	
	INSERT INTO tblCourseEnrollmentApprover (idCourse, idUser)
	SELECT @idCourse, A.id
	FROM @Approvers A
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblCourseEnrollmentApprover CEA
		WHERE CEA.idCourse = @idCourse
		AND CEA.idUser = A.id
	)
	
	SELECT @Return_Code = 0
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO