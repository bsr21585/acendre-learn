-- =====================================================================
-- PROCEDURE: [DataSCOInt.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataSCOInt.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataSCOInt.Save]
GO


/*

Returns all the properties for a lesson data given id.

*/

CREATE PROCEDURE [DataSCOInt.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
		
	@idDataLesson			INT,
	@scoIdentifier			NVARCHAR(255),
	@ScoInteraction			ScoInteractionsTable READONLY
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	IF (SELECT COUNT(1) FROM @ScoInteraction) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		RETURN 1
		END


	DECLARE @idDataSCO int
	SET @idDataSCO = (SELECT [idData-SCO] FROM [tblData-Sco] WHERE [idData-Lesson] = @idDataLesson);

	
	/*
	remove the already existing interactions records from the tblData-SCOInt
	*/	

	IF((SELECT COUNT(1) FROM [tblData-SCOInt] DSI WHERE DSI.[idData-SCO] = @idDataSCO AND DSI.scoIdentifier = @scoIdentifier AND DSI.idSite = @idCallerSite) > 0)
	BEGIN
		/*
		delete the interactions records in the tblData-SCOInt
		*/		

		DELETE [tblData-SCOInt] 
		WHERE  [idData-SCO] = @idDataSCO
				AND scoIdentifier = @scoIdentifier 
				AND idSite = @idCallerSite
	END
		
	/*
	insert the interactions records in the tblData-SCOInt
	*/		

	INSERT INTO [tblData-SCOInt] 
	(
		idsite,
		[idData-SCO],
		identifier,
		timestamp,
		result,
		latency,
		scoIdentifier
	)
	SELECT 
		@idCallerSite,
		@idDataSCO,		
		id,
		GETUTCDATE(),
		(SELECT idSCORMVocabulary FROM tblSCORMVocabulary WHERE value = SI.result),
		latency,
		@scoIdentifier
	FROM @ScoInteraction SI

	SET @Return_Code = 0
	SET @Error_Description_Code = ''


	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO