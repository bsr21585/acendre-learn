-- =====================================================================
-- PROCEDURE: [Catalog.GetCommunities]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.GetCommunities]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.GetCommunities]
GO
	
/*

Returns a recordset of user joinable communities (groups).

*/

CREATE PROCEDURE [Catalog.GetCommunities]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	-- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	/*

	do not check caller site membership or caller permissions as
	this can be called by a non-logged-in user

	*/


	/*

	validate that the specified language exists, use the site default if not

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	IF @idCallerLanguage IS NULL
		BEGIN
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		END

			
	/*
	
	get the data and return it
	
	*/

	SELECT
		G.idGroup,
		G.idSite,
		CASE WHEN GL.name IS NOT NULL THEN GL.name ELSE G.name END AS name,
		G.avatar,
		CASE WHEN G.isFeedActive IS NOT NULL THEN G.isFeedActive ELSE 0 END AS isFeedActive,
		CASE WHEN (
					SELECT COUNT(1) 
					FROM tblUserToGroupLink UGL
					WHERE UGL.idGroup = G.idGroup
					AND UGL.idUser = @idCaller 
				  ) = 0 THEN CONVERT(BIT, 0) 
		ELSE CONVERT(BIT, 1) END AS isCallerAlreadyAMember
	FROM tblGroup G
	LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idCallerLanguage
	WHERE G.idSite = @idCallerSite 
	AND G.isSelfJoinAllowed = 1
	ORDER BY name
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO	