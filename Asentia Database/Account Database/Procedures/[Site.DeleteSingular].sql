
-- =====================================================================
-- PROCEDURE: [Site.DeleteSingular]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Site.DeleteSingular]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.DeleteSingular]
GO

CREATE PROCEDURE [Site.DeleteSingular]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	@idSite				INT
)
AS
	
	BEGIN	   
	SET NOCOUNT ON
	
	-- create table and insert idSite value into it and pass it to Site.Delete procedure 
	DECLARE @Sites as IDTable
	INSERT INTO @Sites (id) VALUES (@idSite)

	-- execute Site.Delete procedure
	EXEC [Site.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @Sites
	
	-- return
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1
	
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO