-- =====================================================================
-- PROCEDURE: [ContentPackage.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ContentPackage.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ContentPackage.Delete]
GO

/*

Deletes Content Package(s)

*/
CREATE PROCEDURE [ContentPackage.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@ContentPackages		IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @ContentPackages) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'ContentPackageDelete_NoRecordsSelected'
		RETURN 1
		END
	
	/*
	
	validate that all content packages exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @ContentPackages CPCP
		LEFT JOIN tblContentPackage CP ON CP.idContentPackage = CPCP.id 
		WHERE CP.idSite IS NULL
		OR CP.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ContentPackageDelete_DetailsNotFound'
		RETURN 1 
		END

	/*

	REMOVE the links to content packages from the quizzes/surveys table
	and set the isDraft flag for them
	this returns the quizzes/surveys to an unpublished draft state

	*/

	UPDATE tblQuizSurvey SET 
		idContentPackage = NULL,
		isDraft = 1
	WHERE idSite = @idCallerSite
	AND idContentPackage IN (
		SELECT CPCP.id
		FROM @ContentPackages CPCP
	)

	/*
	
	DELETE the content packages
	
	*/
	
	DELETE FROM tblContentPackage
	WHERE idContentPackage IN (
		SELECT CPCP.id
		FROM @ContentPackages CPCP
	)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO