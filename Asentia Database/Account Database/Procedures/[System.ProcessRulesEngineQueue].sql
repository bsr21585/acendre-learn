-- =====================================================================
-- PROCEDURE: [System.ProcessRulesEngineQueue]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ProcessRulesEngineQueue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ProcessRulesEngineQueue]
GO

/*

Processes the queue for the rules engine. This is the main queue for execution of
group, role, certification, course enrollment, and learning path enrollment rules.

*/
CREATE PROCEDURE [System.ProcessRulesEngineQueue]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* GET UTC TIMESTAMP */
	DECLARE @dtExecuted DATETIME
	SET @dtExecuted = GETUTCDATE()

	/*

	Put the current queue items into a temporary table for processing.

	*/

	DECLARE @Queue TABLE (idQueue INT, idSite INT, triggerObject NVARCHAR(50), triggerObjectId INT)

	INSERT INTO @Queue (
		idQueue,
		idSite,
		triggerObject,
		triggerObjectId
	)
	SELECT
		RSEQ.idRuleSetEngineQueue,
		RSEQ.idSite,
		RSEQ.triggerObject,
		RSEQ.triggerObjectId
	FROM tblRuleSetEngineQueue RSEQ
	WHERE (RSEQ.isProcessing IS NULL OR RSEQ.isProcessing = 0)
	AND RSEQ.dtProcessed IS NULL
	AND RSEQ.dtQueued <= GETUTCDATE()
	ORDER BY RSEQ.dtQueued ASC

	/*

	Set the queue items we grabbed as processing so they aren't picked up by another instance of this procedure.

	*/

	UPDATE tblRuleSetEngineQueue SET
		isProcessing = 1
	WHERE idRuleSetEngineQueue IN (SELECT idQueue FROM @Queue)

	/*

	Process the queue, only if there are items in it.

	*/

	IF (SELECT COUNT(1) FROM @Queue) > 0
		BEGIN

		/* IDTables FOR OBJECTS */
		DECLARE @Users			IDTable
		DECLARE @Groups			IDTable
		DECLARE @Roles			IDTable
		DECLARE @Certifications IDTable
		DECLARE @Courses		IDTable
		DECLARE @LearningPaths	IDTable
		DECLARE @EMPTYIDTABLE	IDTable

		/* POPULATE THE OBJECT IDTables */
		INSERT INTO @Users (id) SELECT DISTINCT Q.triggerObjectId FROM @Queue Q WHERE Q.triggerObject = 'user'
		INSERT INTO @Groups (id) SELECT DISTINCT Q.triggerObjectId FROM @Queue Q WHERE Q.triggerObject = 'group'
		INSERT INTO @Roles (id) SELECT DISTINCT Q.triggerObjectId FROM @Queue Q WHERE Q.triggerObject = 'role'
		INSERT INTO @Certifications (id) SELECT DISTINCT Q.triggerObjectId FROM @Queue Q WHERE Q.triggerObject = 'certification'
		INSERT INTO @Courses (id) SELECT DISTINCT RSE.idCourse FROM @Queue Q LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = Q.triggerObjectId WHERE Q.triggerObject = 'rulesetenrollment' AND RSE.idCourse IS NOT NULL
		INSERT INTO @Courses (id) SELECT DISTINCT Q.triggerObjectId FROM @Queue Q WHERE Q.triggerObject = 'course' AND NOT EXISTS (SELECT 1 FROM @Courses C WHERE C.id = Q.triggerObjectId)
		INSERT INTO @LearningPaths (id) SELECT DISTINCT RSE.idLearningPath FROM @Queue Q LEFT JOIN tblRuleSetLearningPathEnrollment RSE ON RSE.idRuleSetLearningPathEnrollment = Q.triggerObjectId WHERE Q.triggerObject = 'rulesetlearningpathenrollment' AND RSE.idLearningPath IS NOT NULL		
		INSERT INTO @LearningPaths (id) SELECT DISTINCT Q.triggerObjectId FROM @Queue Q WHERE Q.triggerObject = 'learningpath' AND NOT EXISTS (SELECT 1 FROM @LearningPaths LP WHERE LP.id = Q.triggerObjectId)

		/*

		EXECUTE RULES FOR TRIGGER OBJECT 'user'.
		This executes rules on ALL Groups, Certifications, Roles, RuleSetEnrollments, and RuleSetLearningPathEnrollments filtered to users.

		*/

		IF (SELECT COUNT(1) FROM @Users) > 0
			BEGIN

			EXEC [System.ExecuteGroupAutoJoinRules]							@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @Users, 'user'
			EXEC [System.ExecuteRoleAutoJoinRules]							@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @Users, 'user'
			EXEC [System.ExecuteCertificationAutoJoinRules]					@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @Users, 'user'
			EXEC [System.ExecuteRuleSetEnrollmentAssignment]				@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @Users, 'user'
			EXEC [System.ExecuteRuleSetLearningPathEnrollmentAssignment]	@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @Users, 'user'

			END

		/*

		EXECUTE RULES FOR TRIGGER OBJECT 'group'.
		This executes rules on THE Group(s) for ALL users, and ALL Certifications, Roles, RuleSetEnrollments, and RuleSetLearningPathEnrollments filtered to users that are a member of the group(s).

		*/

		IF (SELECT COUNT(1) FROM @Groups) > 0
			BEGIN

			EXEC [System.ExecuteGroupAutoJoinRules]							@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @Groups, @EMPTYIDTABLE, ''

			EXEC [System.ExecuteRoleAutoJoinRules]							@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @Groups, 'group'
			EXEC [System.ExecuteCertificationAutoJoinRules]					@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @Groups, 'group'
			EXEC [System.ExecuteRuleSetEnrollmentAssignment]				@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @Groups, 'group'
			EXEC [System.ExecuteRuleSetLearningPathEnrollmentAssignment]	@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @Groups, 'group'

			END

		/*

		EXECUTE RULES FOR TRIGGER OBJECT 'role'.
		This executes rules on THE Role(s) for ALL users.

		*/

		IF (SELECT COUNT(1) FROM @Roles) > 0
			BEGIN

			EXEC [System.ExecuteRoleAutoJoinRules]							@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @Roles, @EMPTYIDTABLE, ''

			END

		/*

		EXECUTE RULES FOR TRIGGER OBJECT 'certification'.
		This executes rules on THE Certification(s) for ALL users.

		*/

		IF (SELECT COUNT(1) FROM @Certifications) > 0
			BEGIN

			EXEC [System.ExecuteCertificationAutoJoinRules]					@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @Certifications, @EMPTYIDTABLE, ''

			END

		/*

		EXECUTE RULES FOR TRIGGER OBJECT 'rulesetenrollment'.
		This executes rules on THE Course(s) for RuleSetEnrollments for ALL users.

		*/

		IF (SELECT COUNT(1) FROM @Courses) > 0
			BEGIN

			EXEC [System.ExecuteRuleSetEnrollmentAssignment]				@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @Courses, @EMPTYIDTABLE, ''

			END

		/*

		EXECUTE RULES FOR TRIGGER OBJECT 'rulesetlearningpathenrollment'.
		This executes rules on THE Learning Path(s) for RuleSetLearningPathEnrollments for ALL users.

		*/

		IF (SELECT COUNT(1) FROM @LearningPaths) > 0
			BEGIN

			EXEC [System.ExecuteRuleSetLearningPathEnrollmentAssignment]	@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @LearningPaths, @EMPTYIDTABLE, ''

			END

		END

	/*

	Set the queue items we grabbed as processed.

	*/

	UPDATE tblRuleSetEngineQueue SET
		isProcessing = 0,
		dtProcessed = @dtExecuted
	WHERE idRuleSetEngineQueue IN (SELECT idQueue FROM @Queue)

	/*

	Clean up processed queue items that were processed over 3 days ago, this keeps the table small.

	*/

	DELETE FROM tblRuleSetEngineQueue WHERE dtProcessed IS NOT NULL AND dtProcessed < DATEADD(d, -3, @dtExecuted)

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO