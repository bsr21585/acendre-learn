-- =====================================================================
-- PROCEDURE: [ReportFile.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportFile.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportFile.GetGrid]
GO

/*

Gets a listing of report files

*/

CREATE PROCEDURE [ReportFile.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@idCaller				INT				= 0,
	@idReport				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	begin getting the grid data

	*/
	
	-- return the rowcount
			
	SELECT COUNT(1) AS row_count 
	FROM tblReportFile R
	WHERE 
		(
		(@idCallerSite IS NULL) 
		OR 
		(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
		)
		AND R.idReport = @idReport

	;WITH 
		Keys AS (
			SELECT TOP (@pageNum * @pageSize) 
				R.idReportFile,
				ROW_NUMBER() OVER (ORDER BY
					-- ORDER DESC
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 0 THEN R.dtCreated END DESC,
							
					-- ORDER ASC
					CASE WHEN @orderAsc = 1 THEN dtCreated END
				)
				AS [row_number]
			FROM tblReportFile R
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
				)
			AND R.idReport = @idReport
			AND (
				(@idCaller = 1)
				OR
				(@idCaller > 1 AND R.idUser = @idCaller)
		)
			ORDER BY R.dtCreated DESC
		), 
			
		SelectedKeys AS (
			SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
				idReportFile, 
				[row_number]
			FROM Keys
			WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
		)
	SELECT 
		R.idReportFile,
		R.[filename],
		R.dtCreated,
		ISNULL(R.htmlKb, 0) + ISNULL(R.csvKb, 0) + ISNULL(R.pdfKb, 0) AS size,
		CASE WHEN R.htmlKb IS NULL THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS htmlOn,
		CASE WHEN R.csvKb IS NULL THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS csvOn,
		CASE WHEN R.pdfKb IS NULL THEN CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS pdfOn
	FROM SelectedKeys
	JOIN tblReportFile R ON R.idReportFile = SelectedKeys.idReportFile
	ORDER BY R.dtCreated DESC

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END			
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO