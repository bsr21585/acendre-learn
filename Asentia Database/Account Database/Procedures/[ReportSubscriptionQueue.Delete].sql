-- =====================================================================
-- PROCEDURE: [ReportSubscriptionQueue.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportSubscriptionQueue.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportSubscriptionQueue.Delete]
GO

/*

Deletes report subscription queue item(s).

*/

CREATE PROCEDURE [ReportSubscriptionQueue.Delete]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT,
	
	@ReportSubscriptionQueueItems	IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @ReportSubscriptionQueueItems) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		RETURN 1
		END
			
	/*
	
	validate that all report subscription queue item(s) exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @ReportSubscriptionQueueItems EE
		LEFT JOIN tblReportSubscriptionQueue RSQ ON EE.id = RSQ.idReportSubscriptionQueue
		WHERE RSQ.idSite IS NULL
		OR RSQ.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		RETURN 1 
		END
	
		
	/*

	Delete the report subscription queue item(s).

	*/
	
	DELETE FROM tblReportSubscriptionQueue
	WHERE idReportSubscriptionQueue IN (
		SELECT id
		FROM @ReportSubscriptionQueueItems
	)
	
	/*
	
	DO DELETE the all the other user-linked/based information
	
	*/
	
	--XXX - fill this in as it becomes evident
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO