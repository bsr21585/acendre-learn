-- =====================================================================
-- PROCEDURE: [Site.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Site.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.SaveLang]
GO

/*

Add or updates the optional language fields for a site

*/

CREATE PROCEDURE [Site.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idSite					INT, 
	@title					NVARCHAR (255), 
	@company				NVARCHAR (255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
	
		BEGIN 
			SELECT @Return_Code = 1
			RETURN 1 
		END
			 
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSite
		WHERE idSite = @idSite
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 1
			RETURN 1 
		END
		
	/*
	
	validate uniqueness within language (and within parent)
	-validate only the same fields that the .save procedure validates
	
	*/
	
	IF( 
		SELECT COUNT(1)
		FROM tblSite G
		LEFT JOIN tblSiteLanguage SL ON SL.idSite = G.idSite AND SL.idLanguage = @idCallerLanguage -- same language
		WHERE G.idSite = @idCallerSite -- same site
		AND G.idSite <> @idSite -- not the same entry
		AND SL.title = @title -- validate parameter: title
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		RETURN 1
		END
		
	-- update the multi-language
	
	UPDATE tblSiteLanguage SET
		title = @title,
		company = @company
	WHERE idSite = @idSite
	AND idLanguage = @idCallerLanguage
	
	-- insert multi-language
	
	INSERT INTO tblSiteLanguage (
		idSite,
		idLanguage,
		title,
		company
	)
	SELECT
		@idSite,
		@idCallerLanguage,
		@title,
		@company
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblSiteLanguage CL
		WHERE CL.idSite = @idSite
		AND CL.idLanguage = @idCallerLanguage
	)
	
	SELECT @Return_Code = 0 --(0 is 'success')
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO