-- =====================================================================
-- PROCEDURE: [CertificationModule.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificationModule.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModule.Details]
GO

/*

Return all the properties for a given certification module id.

*/

CREATE PROCEDURE [CertificationModule.Details]
(
	@Return_Code									INT				OUTPUT,
	@Error_Description_Code							NVARCHAR(50)	OUTPUT,
	@idCallerSite									INT				= 0, --default if not specified
	@callerLangString								NVARCHAR(10),
	@idCaller										INT				= 0,
	
	@idCertificationModule							INT				OUTPUT,
	@idCertification								INT				OUTPUT,
	@idSite											INT				OUTPUT, 	
	@title											NVARCHAR(255)	OUTPUT,
	@shortDescription								NVARCHAR(512)	OUTPUT, 
	@isAnyRequirementSet							BIT				OUTPUT, 
	@dtCreated										DATETIME		OUTPUT,
	@dtModified										DATETIME		OUTPUT,
	@isDeleted										BIT				OUTPUT,
	@dtDeleted										DATETIME		OUTPUT,
	@isInitialRequirement							BIT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificationModule
		WHERE idCertificationModule = @idCertificationModule
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleDetails_NoRecordFound'
		RETURN 1
	    END

	/*
	
	get the data 
	
	*/
		
	SELECT
		@idCertificationModule				= CM.idCertificationModule,
		@idCertification					= CM.idCertification,
		@idSite								= CM.idSite,		
		@title								= CM.title,
		@shortDescription					= CM.shortDescription,
		@isAnyRequirementSet				= CM.isAnyRequirementSet,
		@dtCreated							= CM.dtCreated,
		@dtModified							= CM.dtModified,
		@isDeleted							= CM.isDeleted,
		@dtDeleted							= CM.dtDeleted,
		@isInitialRequirement				= CM.isInitialRequirement		
	FROM tblCertificationModule CM
	WHERE CM.idCertificationModule = @idCertificationModule
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO