-- =====================================================================
-- PROCEDURE: [LearningPath.IdsForAPI]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.IdsForAPI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.IdsForAPI]
GO

/*

Returns a recordset of learningpath ids to populate a list with no exclusions for a specific object in API.

*/

CREATE PROCEDURE [LearningPath.IdsForAPI]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString


	/*

	get the site's default language id

	*/

	DECLARE @idSiteLanguage INT
	SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite


	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	-- if the caller's language is the same as the site's language, search and display on the base table
	IF (@idCallerLanguage = @idSiteLanguage OR @callerLangString = '')
		BEGIN
			SELECT DISTINCT 
				LP.idLearningPath
			FROM tblLearningPath LP
			INNER JOIN CONTAINSTABLE(tblLearningPath, *, @searchParam) K ON K.[key] = LP.idLearningPath
			WHERE LP.idSite = @idCallerSite
		END
    ELSE 
		BEGIN
			SELECT DISTINCT 
				LP.idLearningPath
			FROM tblLearningPathLanguage LPL
			INNER JOIN CONTAINSTABLE(tblLearningPathLanguage, *, @searchParam) K ON K.[key] = LPL.idLearningPathLanguage AND LPL.idLanguage = @idCallerLanguage
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPL.idLearningPath
			WHERE LP.idSite = @idCallerSite
		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO