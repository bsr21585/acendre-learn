-- =====================================================================
-- PROCEDURE: [TransactionResponse.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionResponse.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionResponse.Save]
GO

/*

Adds new TransactionResponse or updates existing TransactionResponse.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [TransactionResponse.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@id				        INT				OUTPUT,
	@idUser                 INT,            
	@invoiceNumber			NVARCHAR(25),
	@cardType               NVARCHAR(35),
	@cardNumber		    	NVARCHAR(25),
	@idTransaction			NVARCHAR(30),
	@amount			        FLOAT ,
	@responseCode           NVARCHAR (255),
	@responseMessage        NVARCHAR (MAX),
	@dtTransaction		    DATETIME,
	@isApproved             BIT,
	@isValidated            BIT
	)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
		
	/*
	
	save the data
	
	*/
	
	IF (@id = 0 OR @id IS NULL)
		
		BEGIN
		
		-- insert the new TransactionResponse
		
		INSERT INTO tblTransactionResponse (
			idSite, 
	      	idUser,
			invoiceNumber,
			cardType,
			cardNumber,
			idTransaction,
			amount,
			responseCode,
			responseMessage,
			isApproved,
			isValidated,
			dtTransaction			
		)			
		VALUES (
			@idCallerSite,
			@idUser,
			@invoiceNumber,
			@cardType,
			@cardNumber,
			@idTransaction,
		    @amount,
			@responseCode, 
			@responseMessage,
			@isApproved,
			@isValidated,
			@dtTransaction
			)
		
		-- get the new TransactionResponse's id and return successfully
		
		SELECT @id = SCOPE_IDENTITY()
		
		END
	
	SELECT @Error_Description_Code = ''
	SELECT @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO