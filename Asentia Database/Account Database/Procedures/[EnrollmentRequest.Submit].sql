-- =====================================================================
-- PROCEDURE: [EnrollmentRequest.Submit]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EnrollmentRequest.Submit]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EnrollmentRequest.Submit]
GO

/*

Submits a course enrollment request.

*/

CREATE PROCEDURE [EnrollmentRequest.Submit]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idEnrollmentRequest		INT				OUTPUT,
	@idCourse					INT,
	@idUser						INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	declare utc now

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()
		
	/*
	
	submit the request
	
	*/
	
	INSERT INTO tblEnrollmentRequest (
		idSite,
		idUser,
		idCourse,
		[timestamp]
	)
	SELECT
		@idCallerSite,
		@idUser,
		@idCourse,
		@utcNow
	--FROM tblEnrollmentRequest
	--WHERE NOT EXISTS (SELECT 1
					  --FROM tblEnrollmentRequest ER
					  --WHERE ER.idSite = @idCallerSite
					  --AND ER.idUser = @idUser
					  --AND ER.idCourse = @idCourse
					  --AND ER.dtApproved IS NULL
					  --AND ER.dtDenied IS NULL
					  --AND ER.idApprover IS NULL)
			
	SELECT @idEnrollmentRequest = SCOPE_IDENTITY()

	/*

	do the event log entry

	*/
	
	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (idSite, idObject, idObjectRelated, idObjectUser) VALUES (@idCallerSite, @idCourse, @idEnrollmentRequest, @idUser)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 209, @utcNow, @eventLogItems
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END		

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO