-- =====================================================================
-- PROCEDURE: [Data-TinCanState.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCanState.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCanState.Delete]
GO

/*

Deletes existing Tin Can State(s).

*/

CREATE PROCEDURE [Data-TinCanState.Delete]
(
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idEndpoint				INT,
	@stateId				NVARCHAR(100),
	@agent					NVARCHAR(MAX),
	@activityId				NVARCHAR(100),
	@registration			NVARCHAR(50)
)
AS
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that record(s) exist(s)
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-TinCanState]
		WHERE idSite = @idCallerSite
		AND activityId = @activityId
		AND agent = @agent
		AND (stateId = @stateId OR @stateId IS NULL)
		AND (registration = @registration OR ((@registration IS NULL AND registration IS NULL) OR (@registration IS NULL AND @stateId IS NULL)))
		) = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataTinCanStateDelete_DetailsNotFound'
		RETURN 1
		END
	
	/*
	
	validate caller permission to the specified record(s)
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-TinCanState]
		WHERE idSite = @idCallerSite
		AND activityId = @activityId
		AND agent = @agent
		AND (stateId = @stateId OR @stateId IS NULL)
		AND (registration = @registration OR ((@registration IS NULL AND registration IS NULL) OR (@registration IS NULL AND @stateId IS NULL)))
		AND idEndpoint = @idEndpoint
		) = 0
		BEGIN
		SELECT @Return_Code = 3
		SET @Error_Description_Code = 'DataTinCanStateDelete_PermissionError'
		RETURN 1
		END
		
	DELETE FROM [tblData-TinCanState]
	WHERE idSite = @idCallerSite
	AND idEndpoint = @idEndpoint
	AND activityId = @activityId
	AND agent = @agent
	AND (stateId = @stateId OR @stateId IS NULL)
	AND (registration = @registration OR ((@registration IS NULL AND registration IS NULL) OR (@registration IS NULL AND @stateId IS NULL)))
	
	SELECT @Return_Code = 0 --Success
	SELECT @Error_Description_Code = NULL
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO