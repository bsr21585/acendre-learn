-- =====================================================================
-- PROCEDURE: [LearningPath.GetRuleSetEnrollments]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.GetRuleSetEnrollments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.GetRuleSetEnrollments]
GO

/*

Returns a recordset of ruleset enrollment ids and labels that belong to a learning path.

*/

CREATE PROCEDURE [LearningPath.GetRuleSetEnrollments]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idLearningPath			INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		SELECT
			DISTINCT
			RSLPE.idRuleSetLearningPathEnrollment,
			CASE WHEN RSLPEL.label IS NOT NULL THEN RSLPEL.label ELSE RSLPE.label END AS label,
			RSLPE.[priority]
		FROM tblRuleSetLearningPathEnrollment RSLPE
		LEFT JOIN tblRuleSetLearningPathEnrollmentLanguage RSLPEL ON RSLPEL.idRuleSetLearningPathEnrollment = RSLPE.idRuleSetLearningPathEnrollment AND RSLPEL.idLanguage = @idCallerLanguage
		WHERE RSLPE.idSite = @idCallerSite
		AND RSLPE.idLearningPath = @idLearningPath
		ORDER BY [priority]

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			RSLPE.idRuleSetLearningPathEnrollment,
			CASE WHEN RSLPEL.label IS NOT NULL THEN RSLPEL.label ELSE RSLPE.label END AS label,
			RSLPE.[priority]
		FROM tblRuleSetLearningPathEnrollmentLanguage RSLPEL
		INNER JOIN CONTAINSTABLE(tblRuleSetLearningPathEnrollmentLanguage, *, @searchParam) K ON K.[key] = RSLPEL.idRuleSetLearningPathEnrollmentLanguage AND RSLPEL.idLanguage = @idCallerLanguage
		LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = RSLPEL.idRuleSetLearningPathEnrollment
		WHERE RSLPE.idSite = @idCallerSite
		AND RSLPE.idLearningPath = @idLearningPath
		ORDER BY [priority]

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO