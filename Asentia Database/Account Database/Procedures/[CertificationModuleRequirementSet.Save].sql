-- =====================================================================
-- PROCEDURE: [CertificationModuleRequirementSet.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificationModuleRequirementSet.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModuleRequirementSet.Save]
GO

/*

Adds new certification module requirement set or updates existing certification module requirement set.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [CertificationModuleRequirementSet.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idCertificationModuleRequirementSet				INT				OUTPUT,	
	@idCertificationModule								INT,
	@isAny												BIT,
	@label												NVARCHAR(255)	
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED


	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'CertificationModuleRequirementSetSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/
		
	/*
	
	save the data
	
	*/
	
	IF (@idCertificationModuleRequirementSet = 0 OR @idCertificationModuleRequirementSet IS NULL)
		
		BEGIN
		
		-- insert the new certification module requirement set
		
		INSERT INTO tblCertificationModuleRequirementSet (
			idCertificationModule,
			idSite,
			isAny,
			label
		)			
		VALUES (
			@idCertificationModule,
			@idCallerSite,
			@isAny,
			@label
		)
		
		-- get the new certification module requirement set's id
		
		SET @idCertificationModuleRequirementSet = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the certification module requirement set id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblCertificationModuleRequirementSet WHERE idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertificationModuleRequirementSetSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing certification module requirement set's properties
		
		UPDATE tblCertificationModuleRequirementSet SET
			isAny = @isAny,			
			label = @label						
		WHERE idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet

		-- get the certification module requirement set's id 

		SET @idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblCertificationModuleRequirementSetLanguage CMRSL WHERE CMRSL.idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet AND CMRSL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblCertificationModuleRequirementSetLanguage SET
			label = @label
		WHERE idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCertificationModuleRequirementSetLanguage (
			idSite,
			idCertificationModuleRequirementSet,
			idLanguage,
			label
		)
		SELECT
			@idCallerSite,
			@idCertificationModuleRequirementSet,
			@idDefaultLanguage,
			@label
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCertificationModuleRequirementSetLanguage CMRSL
			WHERE CMRSL.idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet
			AND CMRSL.idLanguage = @idDefaultLanguage
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO