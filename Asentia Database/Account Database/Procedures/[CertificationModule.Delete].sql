-- =====================================================================
-- PROCEDURE: [CertificationModule.Delete]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CertificationModule.Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) 
	DROP PROCEDURE [CertificationModule.Delete]
GO

/*

Deletes certification module

*/

CREATE PROCEDURE [CertificationModule.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@CertificationModules	IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @CertificationModules) = 0 
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CertificationModuleDelete_NoRecordFound'
		RETURN 1
		END

	/*

	validate that all certification modules exist within the site

	*/

	IF (
		SELECT COUNT(1) 
		FROM @CertificationModules CMCM
		LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = CMCM.id
		WHERE CM.idSite IS NULL
		OR CM.idSite <> @idCallerSite
	   ) > 0 
	   
	   BEGIN
	   SET @Return_Code = 1
	   SET @Error_Description_Code = 'CertificationModuleDelete_NoRecordFound'
	   RETURN 1
	   END

	/*

	ORIGINALLY WE WERE GOING TO SOFT DELETE THESE, HOWEVER IT MAKES MORE SENSE TO COMPLETELY DELETE THEM 
	AS THERE IS NO POINT IN TRACKING OR REPORTING ON A CERTIFICATION THAT THE ADMIN CHOOSES TO DELETE.
	THIS MAY CHANGE IN THE FUTURE
	
	DELETE IN ORDER OF DEPENDENCIES

	*/

	-- DELETE FROM [tblData-CertificationModuleRequirement]
	DELETE FROM [tblData-CertificationModuleRequirement] WHERE idCertificationModuleRequirement IN (SELECT idCertificationModuleRequirement 
																									FROM tblCertificationModuleRequirement 
																									WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
																																				  FROM tblCertificationModuleRequirementSet 
																																				  WHERE idCertificationModule IN (SELECT id FROM @CertificationModules)))

	-- DELETE FROM [tblCertificationModuleRequirementToCourseLink]
	DELETE FROM [tblCertificationModuleRequirementToCourseLink] 
	WHERE idCertificationModuleRequirement IN (SELECT idCertificationModuleRequirement 
											   FROM tblCertificationModuleRequirement
											   WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
																							 FROM tblCertificationModuleRequirementSet
																							 WHERE idCertificationModule IN (SELECT idCertificationModule
																															 FROM tblCertificationModule
																															 WHERE idCertificationModule IN (SELECT id FROM @CertificationModules))))

	-- DELETE FROM [tblCertificationModuleRequirementLanguage]
	DELETE FROM [tblCertificationModuleRequirementLanguage]
	WHERE idCertificationModuleRequirement IN (SELECT idCertificationModuleRequirement 
											   FROM tblCertificationModuleRequirement
											   WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
																							 FROM tblCertificationModuleRequirementSet
																							 WHERE idCertificationModule IN (SELECT idCertificationModule
																															 FROM tblCertificationModule
																															 WHERE idCertificationModule IN (SELECT id FROM @CertificationModules))))

	-- DELETE FROM [tblCertificationModuleRequirement]
	DELETE FROM [tblCertificationModuleRequirement]
	WHERE idCertificationModuleRequirement IN (SELECT idCertificationModuleRequirement 
											   FROM tblCertificationModuleRequirement
											   WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
																							 FROM tblCertificationModuleRequirementSet
																							 WHERE idCertificationModule IN (SELECT idCertificationModule
																															 FROM tblCertificationModule
																															 WHERE idCertificationModule IN (SELECT id FROM @CertificationModules))))

	-- DELETE FROM [tblCertificationModuleRequirementSetLanguage]
	DELETE FROM [tblCertificationModuleRequirementSetLanguage]
	WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
												  FROM tblCertificationModuleRequirementSet
												  WHERE idCertificationModule IN (SELECT idCertificationModule
																				  FROM tblCertificationModule
																				  WHERE idCertificationModule IN (SELECT id FROM @CertificationModules)))

	-- DELETE FROM [tblCertificationModuleRequirementSet]
	DELETE FROM [tblCertificationModuleRequirementSet]
	WHERE idCertificationModuleRequirementSet IN (SELECT idCertificationModuleRequirementSet
												  FROM tblCertificationModuleRequirementSet
												  WHERE idCertificationModule IN (SELECT idCertificationModule
																				  FROM tblCertificationModule
																				  WHERE idCertificationModule IN (SELECT id FROM @CertificationModules)))

	-- DELETE FROM [tblCertificationModuleLanguage]
	DELETE FROM [tblCertificationModuleLanguage]
	WHERE idCertificationModule IN (SELECT idCertificationModule
								    FROM tblCertificationModule
								    WHERE idCertificationModule IN (SELECT id FROM @CertificationModules))

	-- DELETE FROM [tblCertificationModule]
	DELETE FROM [tblCertificationModule]
	WHERE idCertificationModule IN (SELECT idCertificationModule
								    FROM tblCertificationModule
								    WHERE idCertificationModule IN (SELECT id FROM @CertificationModules))

	/*

	RETURN

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO