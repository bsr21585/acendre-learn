-- =====================================================================
-- PROCEDURE: [Calendar.GetEnrollmentDetails]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Calendar.GetEnrollmentDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Calendar.GetEnrollmentDetails]
GO

/*

Gets the details of a user's enrollment for the calendar's enrollment event's modal.

*/

CREATE PROCEDURE [Calendar.GetEnrollmentDetails]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified

	@idEnrollment						INT				= 0 -- will fail if not specified
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	get the user's language
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	/* 
	
	get the data

	*/

	SELECT		
		E.idEnrollment AS [id],
		E.dtStart,
		E.idTimezone,
		CASE WHEN E.idEnrollment IS NULL THEN
			NULL
		ELSE
			CASE WHEN E.dtCompleted IS NOT NULL THEN
				'completed'
			ELSE
				CASE WHEN (E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromStart <= GETUTCDATE()) OR (E.dtExpiresFromFirstLaunch IS NOT NULL AND E.dtExpiresFromFirstLaunch <= GETUTCDATE()) then
					'expired'
				ELSE
					CASE WHEN E.dtDue IS NOT NULL AND E.dtDue <= GETUTCDATE() THEN
						'overdue'
					ELSE
						CASE WHEN E.dtStart > GETUTCDATE() THEN
							'future'
						ELSE
							'enrolled'
						END
					END
				END
			END
		END AS [status],
		CASE WHEN CL.title IS NOT NULL THEN 
			CL.title 
		ELSE 
			CASE WHEN C.title IS NOT NULL THEN
				C.title 
			ELSE
				E.title
			END
		END AS [eventTitle],
		E.dtDue,
		CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
			E.dtDue
		ELSE
			CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NOT NULL THEN
			E.dtExpiresFromFirstLaunch
			ELSE
				CASE WHEN E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
					E.dtExpiresFromStart
				ELSE
					CASE WHEN E.dtExpiresFromStart >= E.dtExpiresFromFirstLaunch THEN
						E.dtExpiresFromFirstLaunch
					ELSE
						E.dtExpiresFromStart
					END
				END
			 END
		END AS [expirationDate]
	FROM tblEnrollment E
	LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	WHERE E.idEnrollment = @idEnrollment
	AND E.idUser = @idCaller

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
