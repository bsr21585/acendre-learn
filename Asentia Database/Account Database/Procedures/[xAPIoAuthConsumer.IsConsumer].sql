-- =====================================================================
-- PROCEDURE: [xAPIoAuthConsumer.IsConsumer]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[xAPIoAuthConsumer.IsConsumer]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [xAPIoAuthConsumer.IsConsumer]
GO

/*

Authenticates credentials for login to the system.

*/
CREATE PROCEDURE [dbo].[xAPIoAuthConsumer.IsConsumer]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	
	@idEndpoint				INT				OUTPUT,
	@oAuthKey				NVARCHAR(200)
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @count INT
	SET @count = 0
	
	SELECT @count = COUNT(1)
	FROM tblxAPIoAuthConsumer oAuthConsumer
	WHERE oAuthConsumer.oAuthKey = @oAuthKey
	AND oAuthConsumer.idSite = @idCallerSite
	
	IF (@count) = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'xAPIoAuthConsumerIsConsumer_KeyNotFound'
		END
	
	IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'xAPIoAuthConsumerIsConsumer_DuplicateKey'
		RETURN 1
		END
	
	IF (@count) = 1
		BEGIN
		-- get endpoint id
		SELECT @idEndpoint = oAuthConsumer.idxAPIoAuthConsumer
		FROM tblxAPIoAuthConsumer oAuthConsumer
		WHERE oAuthConsumer.oAuthKey = @oAuthKey
		AND oAuthConsumer.idSite = @idCallerSite
	
		SET @Return_Code = 0 --(0 is 'success')
		END
	
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
