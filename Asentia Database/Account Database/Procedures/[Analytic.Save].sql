-- =====================================================================
-- PROCEDURE: [Analytic.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Analytic.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Analytic.Save]
GO

/*

Adds new Analytic or updates existing Analytic.

*/

CREATE PROCEDURE [Analytic.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idAnalytic				INT				OUTPUT, 
	@idSite					INT, 
	@idUser					INT,
	@idDataset				INT,
	@title					NVARCHAR(255),
	@idChartType			INT,
	@frequency              INT             = NULL ,
	@excludeDays             NVARCHAR(255)   = NULL  ,
	@isPublic				BIT,
    @dtStart				DATETIME		= NULL,
	@dtEnd			       	DATETIME		= NULL,
	@activity               INT             = 0
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			RETURN 1 
		END
			 
		
		
	
	/*
	
	save the data
	
	*/
	
	IF (@idAnalytic = 0 OR @idAnalytic is null OR
	( @idCaller <> @idUser AND
		(SELECT COUNT(1)
		FROM tblAnalytic
		WHERE idAnalytic = @idAnalytic
		AND idSite = @idCallerSite
		AND isDeleted <> 1
		) = 0
		)
	)		
		BEGIN
		
		-- insert the new Analytic
		
		INSERT INTO tblAnalytic (
			idSite, 
			idUser,
			idDataset,
			idChartType,
			title,
			excludeDays,
			isPublic,
			frequency,
			dtStart,
			dtEnd,
			dtCreated,
			dtModified,
			activity
		)			
		VALUES (
			@idSite, 
			@idCaller,
			@idDataset,
			@idChartType,
			@title,
			@excludeDays,
			@isPublic,
			@frequency,
			@dtStart,
			@dtEnd,
			GETUTCDATE(),
			GETUTCDATE(),
			@activity
		)
		
		-- get the new Analytic's id and return successfully
		
		SELECT @idAnalytic = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the Analytic id exists and it's not a deleted Analytic
		
		IF (SELECT COUNT(1) FROM tblAnalytic WHERE idAnalytic = @idAnalytic AND (isDeleted IS NULL OR isDeleted = 0)) < 1
			
			BEGIN
				SELECT @Return_Code = 1
				SET @Error_Description_Code = 'AnalyticSave_NoRecordFound'
				RETURN 1
			END
			
		-- update existing Analytic's properties
		
		UPDATE tblAnalytic SET
			title = @title,
			frequency = @frequency,
			isPublic = @isPublic,
			idChartType = @idChartType,
			excludeDays = @excludeDays,
			dtStart = @dtStart,
			dtEnd = @dtEnd,
			dtModified = GETUTCDATE(),
			activity = @activity
		WHERE idAnalytic = @idAnalytic
		
		-- get the id and return successfully
		
		SELECT @idAnalytic = @idAnalytic
				
		END
		


   -- update the multi-language
	
		UPDATE tblAnalyticLanguage SET
			title = @title,
			dtModified = GETUTCDATE()
		WHERE idAnalytic = @idAnalytic
		AND idLanguage = @idCallerLanguage

	
	-- insert multi-language

	INSERT INTO tblAnalyticLanguage (
		idSite,
		idAnalytic,
		idLanguage,
		title,
		dtCreated,
		dtModified
	)
	SELECT
		@idSite,
		@idAnalytic,
		@idCallerLanguage,
		@title,
		GETUTCDATE(),
		GETUTCDATE()
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblAnalyticLanguage CL
		WHERE CL.idAnalytic = @idAnalytic
		AND CL.idLanguage = @idCallerLanguage
	)
	
	SELECT @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO