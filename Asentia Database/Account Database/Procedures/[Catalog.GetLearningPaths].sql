-- =====================================================================
-- PROCEDURE: [Catalog.GetLearningPaths]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.GetLearningPaths]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.GetLearningPaths]
GO
	
/*

Returns a recordset of learning paths parented by a specific catalog, or belonging to the
catalog root if @idParentCatalog is null.

For now, learning paths are not attached to catalogs, but eventually they may be, so we will
leave catalog parameters in place and just make @idParentCatalog always null.

*/

CREATE PROCEDURE [Catalog.GetLearningPaths]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	-- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@idParentCatalog			INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	SET @idParentCatalog = NULL -- always set this to null, for now
	
	/*

	do not check caller site membership or caller permissions as
	this can be called by a non-logged-in user

	*/


	/*

	validate that the specified language exists, use the site default if not

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	IF @idCallerLanguage IS NULL
		BEGIN
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		END

			
	/*
	
	get the data and return it
	
	*/

	IF @idParentCatalog IS NOT NULL -- learning paths belonging to a specified catalog - will never execute, for now
		BEGIN

		SELECT
			LP.idLearningPath,
			LP.idSite,
			CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE LP.name END AS name,
			LP.avatar,
			LP.isClosed,
			CASE WHEN LP.cost IS NOT NULL THEN CAST(LP.cost AS DECIMAL(18,2)) ELSE NULL END AS cost,
			(SELECT COUNT(DISTINCT idCourse) FROM tblLearningPathToCourseLink LPCL WHERE LPCL.idLearningPath = LP.idLearningPath) AS numCourses,
			CASE WHEN (
					   SELECT COUNT(1) 
					   FROM tblLearningPathEnrollment LPE 
					   WHERE LPE.idLearningPath = LP.idLearningPath
					   AND LPE.idUser = @idCaller 
					   AND LPE.idRuleSetLearningPathEnrollment IS NULL 
					   AND LPE.dtCompleted IS NULL
					  ) = 0 THEN CONVERT(BIT, 0) 
			ELSE CONVERT(BIT, 1) END AS isCallerEnrolled, -- note, this checks for self/admin enrolled learning path enrollments where they are not completed
			NULL --LPCL.[order]
		FROM tblLearningPath LP
		LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = @idCallerLanguage
		--LEFT JOIN tblLearningPathToCatalogLink LPCL ON LPCL.idLearningPath = LP.idLearningPath
		WHERE LP.idSite = @idCallerSite 
		AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
		AND LP.isPublished = 1
		--AND LPCL.idLearningPathCatalog = @idParentCatalog
		ORDER BY name --LPCL.[order]

		END

	ELSE -- learning paths not belonging to any catalog - this is the only functionality, for now
		BEGIN

		SELECT
			LP.idLearningPath,
			LP.idSite,
			CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE LP.name END AS name,
			LP.avatar,
			LP.isClosed,
			CASE WHEN LP.cost IS NOT NULL THEN CAST(LP.cost AS DECIMAL(18,2)) ELSE NULL END AS cost,
			(SELECT COUNT(DISTINCT idCourse) FROM tblLearningPathToCourseLink LPCL WHERE LPCL.idLearningPath = LP.idLearningPath) AS numCourses,
			CASE WHEN (
					   SELECT COUNT(1) 
					   FROM tblLearningPathEnrollment LPE 
					   WHERE LPE.idLearningPath = LP.idLearningPath
					   AND LPE.idUser = @idCaller 
					   AND LPE.idRuleSetLearningPathEnrollment IS NULL 
					   AND LPE.dtCompleted IS NULL
					  ) = 0 THEN CONVERT(BIT, 0) 
			ELSE CONVERT(BIT, 1) END AS isCallerEnrolled, -- note, this checks for self/admin enrolled learning path enrollments where they are not completed
			NULL --LP.[order]
		FROM tblLearningPath LP
		LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = @idCallerLanguage
		WHERE LP.idSite = @idCallerSite 
		AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
		AND LP.isPublished = 1
		ORDER BY name --LP.[order]

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO	