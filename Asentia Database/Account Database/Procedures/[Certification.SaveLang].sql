-- =====================================================================
-- PROCEDURE: [Certification.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.SaveLang]
GO

/*

Saves certification "language specific" properties for specific language
in certification language table.

*/

CREATE PROCEDURE [Certification.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCertification		INT, 
	@languageString			NVARCHAR(10),
	@title					NVARCHAR(255), 
	@shortDescription		NVARCHAR(512), 
	@searchTags				NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
			 
	/*
	
	validate that the certification exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertification
		WHERE idCertification = @idCertification
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationSaveLang_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblCertificationLanguage CL WHERE CL.idCertification = @idCertification AND CL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblCertificationLanguage SET
			title = @title,
			shortDescription = @shortDescription,
			searchTags = @searchTags
		WHERE idCertification = @idCertification
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCertificationLanguage (
			idSite,
			idCertification,
			idLanguage,
			title,
			shortDescription,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idCertification,
			@idLanguage,
			@title,
			@shortDescription,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCertificationLanguage CL
			WHERE CL.idCertification = @idCertification
			AND CL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblCertification SET
			title = @title,
			shortDescription = @shortDescription,
			searchTags = @searchTags
		WHERE idCertification = @idCertification

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO