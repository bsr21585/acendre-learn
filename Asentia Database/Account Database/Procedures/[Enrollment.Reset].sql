-- =====================================================================
-- PROCEDURE: [Enrollment.Reset]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.Reset]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.Reset]
GO

/*

Resets an enrollment

*/

CREATE PROCEDURE [Enrollment.Reset]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idEnrollment			INT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that enrollment exists
	
	*/

	IF (SELECT COUNT(1) 
	    FROM tblEnrollment 
		WHERE idEnrollment = @idEnrollment
		AND idSite = @idCallerSite
		) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'EnrollmentReset_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	Reset the enrollment by resetting the completion status of the enrollment and all lesson data,
	and removing and linked content-specific data, i.e. SCO, Task, etc.
	
	*/
	
	-- reset enrollment record
	UPDATE tblEnrollment SET dtCompleted = NULL, dtFirstLaunch = NULL, dtExpiresFromFirstLaunch = NULL WHERE idEnrollment = @idEnrollment

	-- reset all lesson data records for the enrollment
	UPDATE [tblData-Lesson] SET 
		dtCompleted = NULL,
		contentTypeCommittedTo = NULL, 
		dtCommittedToContentType = NULL,
		resetForContentChange = NULL,
		preventPostCompletionLaunchForContentChange = NULL
	WHERE idEnrollment = @idEnrollment

	-- delete any SCO interactions and objectives, then delete SCO data
	-- note that this takes care of both content package and ojt data

	-- delete SCORM interaction data
	DELETE FROM [tblData-SCOInt] 
	WHERE [idData-SCO] IN (SELECT [idData-SCO] 
						   FROM [tblData-SCO] 
						   WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
													 FROM [tblData-Lesson] 
													 WHERE idEnrollment = @idEnrollment)
						  )

	-- delete SCORM objective data
	DELETE FROM [tblData-SCOObj] 
	WHERE [idData-SCO] IN (SELECT [idData-SCO] 
						   FROM [tblData-SCO] 
						   WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
													 FROM [tblData-Lesson] 
													 WHERE idEnrollment = @idEnrollment)
						  )

	-- delete SCO lesson data
	DELETE FROM [tblData-SCO] WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
														FROM [tblData-Lesson] 
														WHERE idEnrollment = @idEnrollment)

	-- delete any "internally-launched" TinCan lesson data
	DELETE FROM [tblData-TinCanContextActivities] 
	WHERE [idData-TinCan] IN (SELECT [idData-TinCan] 
							  FROM [tblData-TinCan] 
							  WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
														FROM [tblData-Lesson] 
														WHERE idEnrollment = @idEnrollment)
							 )

	DELETE FROM [tblData-TinCan] WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
														   FROM [tblData-Lesson] 
														   WHERE idEnrollment = @idEnrollment)

	-- delete task data
	DELETE FROM [tblData-HomeworkAssignment] WHERE [idData-Lesson] IN (SELECT [idData-Lesson] 
																	   FROM [tblData-Lesson] 
																	   WHERE idEnrollment = @idEnrollment)

	/*

	evaluate certificate completion - note, we need to do this here because the Enrollment.EvaluateCompletion procedure
	will not do it due to the fact that we have reset the enrollment

	*/

	DECLARE @idEnrollmentUser INT
	DECLARE @idEnrollmentCourse INT
	DECLARE @idEnrollmentTimezone INT

	SELECT 
		@idEnrollmentUser = idUser,
		@idEnrollmentCourse = idCourse,
		@idEnrollmentTimezone = idTimezone
	FROM tblEnrollment
	WHERE idEnrollment = @idEnrollment

	EXEC [Certificate.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollmentUser, @idEnrollmentCourse, 'course', 'revoked', @idEnrollmentTimezone

	/*

	evaluate enrollment completion - we know the enrollment has been reset here, but call this to do a proper evaluation
	so that learning path enrollment completions can be revoked

	*/

	EXEC [Enrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO