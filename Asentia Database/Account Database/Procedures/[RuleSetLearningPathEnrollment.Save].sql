-- =====================================================================
-- PROCEDURE: [RuleSetLearningPathEnrollment.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetLearningPathEnrollment.Save]
GO

/*

Adds a new ruleset learning path enrollment or updates an existing one.

*/

CREATE PROCEDURE [RuleSetLearningPathEnrollment.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idRuleSetLearningPathEnrollment		INT				OUTPUT,
	@idLearningPath							INT,
	@idTimezone								INT,
	@label									NVARCHAR(255),
	@dtStart								DATETIME,
	@dtEnd									DATETIME,
	@delayInterval							INT,
	@delayTimeframe							NVARCHAR(4),
	@dueInterval							INT,
	@dueTimeframe							NVARCHAR(4),
	@expiresFromStartInterval				INT,
	@expiresFromStartTimeframe				NVARCHAR(4),
	@expiresFromFirstLaunchInterval			INT,
	@expiresFromFirstLaunchTimeframe		NVARCHAR(4),
	@idParentRuleSetLearningPathEnrollment	INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'RuleSetEnrollmentSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/
			 
	/*
	
	validate uniqueness within same parent object
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblRuleSetLearningPathEnrollment
		WHERE idSite = @idCallerSite
		AND idLearningPath = @idLearningPath
		AND label = @label
		AND (
			@idRuleSetLearningPathEnrollment IS NULL
			OR @idRuleSetLearningPathEnrollment <> idRuleSetLearningPathEnrollment
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'RSLPEnrollmentSave_EnrollmentNotUnique' 
		RETURN 1
		END

	/*
	
	validate uniqueness within language, and same parent object
	
	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF( 
		SELECT COUNT(1)
		FROM tblRuleSetLearningPathEnrollment RSLP
		LEFT JOIN tblRuleSetLearningPathEnrollmentLanguage RSLPL ON RSLPL.idRuleSetLearningPathEnrollment = RSLP.idRuleSetLearningPathEnrollment AND RSLPL.idLanguage = @idDefaultLanguage -- same language
		WHERE RSLP.idSite = @idCallerSite -- same site
		AND RSLP.idRuleSetLearningPathEnrollment <> @idRuleSetLearningPathEnrollment -- not the same ruleset enrollment
		AND RSLP.idLearningPath = @idLearningPath
		AND RSLPL.label = @label -- validate parameter: label
		) > 0 
		
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetLearningPathEnrollmentSave_FieldNotUnique'
		RETURN 1
		END
		
	/*
	
	save the data
	
	*/
	
	IF (@idRuleSetLearningPathEnrollment = 0 OR @idRuleSetLearningPathEnrollment IS NULL)
		
		BEGIN
		
		-- get the "priority" number for the new ruleset enrollment

		DECLARE @priority INT

		SELECT 
			@priority = CASE WHEN MAX([priority]) IS NULL THEN 1 ELSE MAX([priority]) + 1 END
		FROM tblRuleSetLearningPathEnrollment
		WHERE idLearningPath = @idLearningPath

		-- insert the new ruleset enrollment
		
		INSERT INTO tblRuleSetLearningPathEnrollment (
			idSite,
			idLearningPath,
			idTimezone,
			[priority],
			label,
			dtStart,
			dtEnd,
			delayInterval,
			delayTimeframe,
			dueInterval,
			dueTimeframe,
			expiresFromStartInterval,
			expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe,
			idParentRuleSetLearningPathEnrollment
		)
		VALUES (
			@idCallerSite,
			@idLearningPath,
			@idTimezone,
			@priority,
			@label,
			@dtStart,
			@dtEnd,
			@delayInterval,
			@delayTimeframe,
			@dueInterval,
			@dueTimeframe,
			@expiresFromStartInterval,
			@expiresFromStartTimeframe,
			@expiresFromFirstLaunchInterval,
			@expiresFromFirstLaunchTimeframe,
			NULL
		)
		
		-- get the new id
		
		SET @idRuleSetLearningPathEnrollment = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the ruleset learning path enrollment exists
		IF (SELECT COUNT(1) FROM tblRuleSetLearningPathEnrollment WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
			SET @Return_Code = 1
			SET @Error_Description_Code = 'RuleSetLearningPathEnrollmentSave_NoRecordFound'
			RETURN 1
			
			END
			
		-- update existing record
		
		UPDATE tblRuleSetLearningPathEnrollment SET
			idTimezone = @idTimezone,
			label = @label,
			dtStart = @dtStart,
			dtEnd = @dtEnd,
			delayInterval = @delayInterval,
			delayTimeframe = @delayTimeframe,
			dueInterval = @dueInterval,
			dueTimeframe = @dueTimeframe,
			expiresFromStartInterval = @expiresFromStartInterval,
			expiresFromStartTimeframe = @expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval = @expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe = @expiresFromFirstLaunchTimeframe,
			idParentRuleSetLearningPathEnrollment = NULL
		WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
		
		-- get the ruleset learning path enrollment's id
		
		SET @idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/
	
	IF (SELECT COUNT(1) FROM tblRuleSetLearningPathEnrollmentLanguage RSLPL WHERE RSLPL.idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment AND RSLPL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblRuleSetLearningPathEnrollmentLanguage SET
			label = @label
		WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblRuleSetLearningPathEnrollmentLanguage (
			idSite,
			idRuleSetLearningPathEnrollment,
			idLanguage,
			label
		)
		SELECT
			@idCallerSite,
			@idRuleSetLearningPathEnrollment,
			@idDefaultLanguage,
			@label
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblRuleSetLearningPathEnrollmentLanguage RSLPL
			WHERE RSLPL.idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
			AND RSLPL.idLanguage = @idDefaultLanguage
		)

		END

	/*

	sync child ruleset learning path enrollments

	*/

	DECLARE @idChildRuleSetLearningPathEnrollment		INT
	DECLARE @idChildLearningPath						INT

	SELECT 
		idRuleSetLearningPathEnrollment
	INTO #SyncedRuleSetLearningPathEnrollmentsToUpdate
	FROM tblRuleSetLearningPathEnrollment
	WHERE idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment	      
								  
	IF EXISTS (SELECT 1 FROM #SyncedRuleSetLearningPathEnrollmentsToUpdate)	
	BEGIN
		DECLARE idChildRuleSetLearningPathEnrollmentCursor							CURSOR LOCAL
		FOR 
		SELECT idRuleSetLearningPathEnrollment FROM #SyncedRuleSetLearningPathEnrollmentsToUpdate	

		OPEN idChildRuleSetLearningPathEnrollmentCursor
		FETCH NEXT FROM idChildRuleSetLearningPathEnrollmentCursor INTO @idChildRuleSetLearningPathEnrollment
		WHILE @@FETCH_STATUS = 0  
		BEGIN
		
		SET @idChildLearningPath = (SELECT idLearningPath FROM tblRuleSetLearningPathEnrollment WHERE idRuleSetLearningPathEnrollment = @idChildRuleSetLearningPathEnrollment)

		EXEC [RuleSetLearningPathEnrollment.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idChildRuleSetLearningPathEnrollment, 
												  @idChildLearningPath, @idTimezone, @label, @dtStart, @dtEnd, @delayInterval, @delayTimeframe, @dueInterval, @dueTimeframe,
												  @expiresFromStartInterval, @expiresFromStartTimeframe, @expiresFromFirstLaunchInterval, @expiresFromFirstLaunchTimeframe, 
								                  @idRuleSetLearningPathEnrollment

		-- sync child ruleset enrollment back to parent ruleset enrollment (calling RuleSet.Save unsyncs the ruleset enrollment)

		UPDATE tblRuleSetLearningPathEnrollment
		SET idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment
		WHERE idRuleSetLearningPathEnrollment = @idChildRuleSetLearningPathEnrollment

		FETCH NEXT FROM idChildRuleSetLearningPathEnrollmentCursor INTO @idChildRuleSetLearningPathEnrollment

		END

	CLOSE idChildRuleSetLearningPathEnrollmentCursor
	DEALLOCATE idChildRuleSetLearningPathEnrollmentCursor

	END
	
	UPDATE tblRuleSetLearningPathEnrollment
	SET idParentRuleSetLearningPathEnrollment = NULL
	WHERE idRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO