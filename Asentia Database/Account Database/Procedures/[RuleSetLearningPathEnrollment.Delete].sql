-- =====================================================================
-- PROCEDURE: [RuleSetLearningPathEnrollment.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetLearningPathEnrollment.Delete]
GO

/*

Deletes ruleset enrollment(s).

*/

CREATE PROCEDURE [RuleSetLearningPathEnrollment.Delete]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idLearningPath						INT,
	@RuleSetLearningPathEnrollments		IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @RuleSetLearningPathEnrollments) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'RuleSetLearningPathEnrollmentDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all rule set enrollments exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @RuleSetLearningPathEnrollments RR
		LEFT JOIN tblRuleSetLearningPathEnrollment R ON RR.id = R.idRuleSetLearningPathEnrollment
		WHERE R.idSite IS NULL
		OR R.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetLearningPathEnrollmentDelete_NoRecordFound'
		RETURN 1 
		END
	
	/*
	
	DELETE child record(s) from the language table.
	
	*/
	
	DELETE FROM tblRuleSetLearningPathEnrollmentLanguage 
	WHERE idRuleSetLearningPathEnrollment IN (SELECT id FROM @RuleSetLearningPathEnrollments)

	/*

	GET ruleset ids to remove rules, rulesets, and ruleset links

	*/

	DECLARE @RuleSets IDTable

	INSERT INTO @RuleSets (
		id
	)
	SELECT
		idRuleSet
	FROM tblRuleSetToRuleSetLearningPathEnrollmentLink
	WHERE idRuleSetLearningPathEnrollment IN (SELECT id FROM @RuleSetLearningPathEnrollments)

	/*

	DELETE ruleset to ruleset learning path enrollment links.
	
	*/
	
	DELETE FROM tblRuleSetToRuleSetLearningPathEnrollmentLink
	WHERE idRuleSetLearningPathEnrollment IN (SELECT id FROM @RuleSetLearningPathEnrollments)

	/*

	DELETE the rules 

	*/

	DELETE FROM tblRule 
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the ruleset language records

	*/

	DELETE FROM tblRuleSetLanguage
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the rulesets

	*/

	DELETE FROM tblRuleSet
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE open ruleset engine queue items

	*/

	DELETE FROM tblRuleSetEngineQueue
	WHERE triggerObjectId IN (SELECT id FROM @RuleSetLearningPathEnrollments) 
	AND triggerObject = 'rulesetlearningpathenrollment'
	AND isProcessing IS NULL
	AND dtProcessed IS NULL

	/*

	DELETE learning path enrollments inherited by the ruleset by using the LearningPathEnrollment.Delete procedure
	Only delete incomplete ones, leave the completed enrollments for transcript purposes.

	*/

	DECLARE @LearningPathEnrollmentsToDelete IDTable

	INSERT INTO @LearningPathEnrollmentsToDelete (
		id
	)
	SELECT
		LPE.idLearningPathEnrollment
	FROM tblLearningPathEnrollment LPE
	WHERE LPE.idRuleSetLearningPathEnrollment IN (SELECT id FROM @RuleSetLearningPathEnrollments)
	AND LPE.dtCompleted IS NULL

	EXEC [LearningPathEnrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @LearningPathEnrollmentsToDelete, 0

	/*

	REMOVE ruleset learning path enrollment link for completed enrollments

	*/

	UPDATE tblLearningPathEnrollment SET
		idRuleSetLearningPathEnrollment = NULL
	WHERE idRuleSetLearningPathEnrollment IN (SELECT id FROM @RuleSetLearningPathEnrollments)
	AND dtCompleted IS NOT NULL

	/*
	
	DELETE record(s) from ruleset learning path enrollment table.
	
	*/
	
	DELETE FROM tblRuleSetLearningPathEnrollment
	WHERE idRuleSetLearningPathEnrollment IN (SELECT id FROM @RuleSetLearningPathEnrollments)

	/*

	UPDATE the ruleset learning path enrollment priorities for the remaining, undeleted ruleset learning path enrollments belonging to the learning path

	*/

	DECLARE @RuleSetLearningPathEnrollmentOrdering TABLE ([priority] INT IDENTITY(1,1), idRuleSetLearningPathEnrollment INT)

	INSERT INTO @RuleSetLearningPathEnrollmentOrdering (
		idRuleSetLearningPathEnrollment
	)
	SELECT
		RSE.idRuleSetLearningPathEnrollment
	FROM tblRuleSetLearningPathEnrollment RSE
	WHERE RSE.idLearningPath = @idLearningPath
	ORDER BY [priority]
	
	UPDATE tblRuleSetLearningPathEnrollment SET
		[priority] = RSEO.[priority]
	FROM @RuleSetLearningPathEnrollmentOrdering RSEO
	WHERE RSEO.idRuleSetLearningPathEnrollment = tblRuleSetLearningPathEnrollment.idRuleSetLearningPathEnrollment
	
	/*

	delete synced ruleset learning path enrollments

	*/

	DECLARE @idRuleSetLearningPathEnrollment						INT

	DECLARE idRuleSetLearningPathEnrollmentCursor					CURSOR LOCAL
	FOR 
	SELECT id FROM @RuleSetLearningPathEnrollments

	OPEN idRuleSetLearningPathEnrollmentCursor
	FETCH NEXT FROM idRuleSetLearningPathEnrollmentCursor INTO @idRuleSetLearningPathEnrollment
	WHILE @@FETCH_STATUS = 0  
	BEGIN

		SELECT 
			idLearningPath
		INTO #idLearningPathsToDeleteFrom
		FROM tblRuleSetLearningPathEnrollment
		WHERE idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment

		DECLARE idLearningPathCursor							CURSOR LOCAL
		FOR 
		SELECT idLearningPath FROM #idLearningPathsToDeleteFrom

		DECLARE @idSyncedLearningPath							INT

		OPEN idLearningPathCursor
		FETCH NEXT FROM idLearningPathCursor INTO @idSyncedLearningPath
		WHILE @@FETCH_STATUS = 0  
		BEGIN

			DECLARE @ChildRuleSetLearningPathEnrollments IDTable

			INSERT INTO @ChildRuleSetLearningPathEnrollments (
				id
			)
			SELECT
				idRuleSetLearningPathEnrollment
			FROM tblRuleSetLearningPathEnrollment
			WHERE idParentRuleSetLearningPathEnrollment IN (SELECT id FROM @RuleSetLearningPathEnrollments)
			AND idLearningPath = @idSyncedLearningPath

			EXEC [RuleSetLearningPathEnrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idSyncedLearningPath, @ChildRuleSetLearningPathEnrollments

			FETCH NEXT FROM idLearningPathCursor INTO @idSyncedLearningPath

		END
		
		CLOSE idLearningPathCursor
		DEALLOCATE idLearningPathCursor

		FETCH NEXT FROM idRuleSetLearningPathEnrollmentCursor INTO @idRuleSetLearningPathEnrollment

	END

	CLOSE idRuleSetLearningPathEnrollmentCursor
	DEALLOCATE idRuleSetLearningPathEnrollmentCursor

	DROP TABLE #idLearningPathsToDeleteFrom


	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO