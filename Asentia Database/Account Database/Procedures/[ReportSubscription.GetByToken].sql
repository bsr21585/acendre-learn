SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportSubscription.GetByToken]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportSubscription.GetByToken]
GO

/*

Gets a report subscription's information by its token.

*/

CREATE PROCEDURE [ReportSubscription.GetByToken]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	
	@token						UNIQUEIDENTIFIER
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	do not validate that the caller is a member of the site as this gets called from an unsubscribe page that the user is not logged in for
	
	*/

	/*
	
	do not validate caller permission as this gets called from an unsubscribe page that the user is not logged in for
	
	*/

			
	/*
	
	validate that the item exists
	
	*/
	
	IF (SELECT COUNT(1) FROM tblReportSubscription RS WHERE RS.idUser = @idCaller AND RS.token = @token) = 0
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ReportSubscriptionGetByToken_RecordNotFound'
		RETURN 1 
		END
		
	/* 
	
	get the caller's default language 
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblUser WHERE idUser = @idCaller

	/*
	
	get the data

	*/

	SELECT TOP 1
		RS.idReportSubscription,
		RS.idUser,
		RS.token,
		CASE WHEN RL.title IS NOT NULL THEN RL.title ELSE R.title END AS title
	FROM tblReportSubscription RS
	LEFT JOIN tblReport R ON R.idReport = RS.idReport
	LEFT JOIN tblReportLanguage RL ON RL.idReport = R.idReport AND RL.idLanguage = @idCallerLanguage
	WHERE RS.idUser = @idCaller
	AND RS.token = @token

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO