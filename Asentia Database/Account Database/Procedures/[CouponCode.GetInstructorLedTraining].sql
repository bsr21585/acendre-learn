-- =====================================================================
-- PROCEDURE: [CouponCode.GetInstructorLedTraining]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CouponCode.GetInstructorLedTraining]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CouponCode.GetInstructorLedTraining]
GO

/*

Returns a recordset of instructor led training ids and titles that belong to a coupon code.

*/

CREATE PROCEDURE [CouponCode.GetInstructorLedTraining]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCouponCode			INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL

		BEGIN

		SELECT
			DISTINCT
			S.idStandupTraining, 
			CASE WHEN SL.title IS NOT NULL THEN SL.title ELSE S.title END AS title
		FROM tblStandUpTraining S
		LEFT JOIN tblStandUpTrainingLanguage SL ON SL.idStandUpTraining = S.idStandUpTraining AND SL.idLanguage = @idCallerLanguage				
		WHERE S.idSite = @idCallerSite
		AND (S.isDeleted = 0 OR S.isDeleted IS NULL)
		AND EXISTS (SELECT 1 FROM tblCouponCodeToStandupTrainingLink CCSL
					WHERE CCSL.idCouponCode = @idCouponCode
					AND CCSL.idStandupTraining = S.idStandUpTraining)
		ORDER BY title

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			S.idStandupTraining, 
			CASE WHEN SL.title IS NOT NULL THEN SL.title ELSE S.title END AS title
		FROM tblStandUpTrainingLanguage SL
		INNER JOIN CONTAINSTABLE(tblStandUpTrainingLanguage, *, @searchParam) K ON K.[key] = SL.idStandUpTrainingLanguage AND SL.idLanguage = @idCallerLanguage
		LEFT JOIN tblStandUpTraining S ON S.idStandUpTraining = SL.idStandUpTraining
		WHERE S.idSite = @idCallerSite
		AND (S.isDeleted = 0 OR S.isDeleted IS NULL)
		AND EXISTS (SELECT 1 FROM tblCouponCodeToStandupTrainingLink CCSL
					WHERE CCSL.idCouponCode = @idCouponCode
					AND CCSL.idStandupTraining = S.idStandUpTraining)
		ORDER BY title

		END
	
	SELECT @Error_Description_Code = ''
	SELECT @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	