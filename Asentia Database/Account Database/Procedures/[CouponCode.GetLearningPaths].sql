-- =====================================================================
-- PROCEDURE: [CouponCode.GetLearningPaths]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CouponCode.GetLearningPaths]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CouponCode.GetLearningPaths]
GO

/*

Returns a recordset of learning path ids and titles that belong to a coupon code.

*/

CREATE PROCEDURE [CouponCode.GetLearningPaths]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCouponCode			INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL

		BEGIN

		SELECT
			DISTINCT
			L.idLearningPath, 
			CASE WHEN LL.name IS NOT NULL THEN LL.name ELSE L.name END AS title
		FROM tblLearningPath L
		LEFT JOIN tblLearningPathLanguage LL ON LL.idLearningPath = L.idLearningPath AND LL.idLanguage = @idCallerLanguage				
		WHERE L.idSite = @idCallerSite
		AND (L.isDeleted = 0 OR L.isDeleted IS NULL)
		AND EXISTS (SELECT 1 FROM tblCouponCodeToLearningPathLink CCLL
					WHERE CCLL.idCouponCode = @idCouponCode
					AND CCLL.idLearningPath = L.idLearningPath)
		ORDER BY title

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			L.idLearningPath, 
			CASE WHEN LL.name IS NOT NULL THEN LL.name ELSE L.name END AS title
		FROM tblLearningPathLanguage LL
		INNER JOIN CONTAINSTABLE(tblLearningPathLanguage, *, @searchParam) K ON K.[key] = LL.idLearningPathLanguage AND LL.idLanguage = @idCallerLanguage
		LEFT JOIN tblLearningPath L ON L.idLearningPath = LL.idLearningPath
		WHERE L.idSite = @idCallerSite
		AND (L.isDeleted = 0 OR L.isDeleted IS NULL)
		AND EXISTS (SELECT 1 FROM tblCouponCodeToLearningPathLink CCLL
					WHERE CCLL.idCouponCode = @idCouponCode
					AND CCLL.idLearningPath = L.idLearningPath)
		ORDER BY title

		END
	
	SELECT @Error_Description_Code = ''
	SELECT @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	