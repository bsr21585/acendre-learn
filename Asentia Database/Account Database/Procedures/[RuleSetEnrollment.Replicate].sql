-- =====================================================================
-- PROCEDURE: [RuleSetEnrollment.Replicate]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.Replicate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetEnrollment.Replicate]
GO

/*

Replicates a ruleset enrollment to other course(s).

*/

CREATE PROCEDURE [RuleSetEnrollment.Replicate]
(
	@Return_Code										INT				OUTPUT,
	@Error_Description_Code								NVARCHAR(50)	OUTPUT,
	@idCallerSite										INT				= 0, --default if not specified
	@callerLangString									NVARCHAR(10),
	@idCaller											INT				= 0,

	@idRuleSetEnrollmentToReplicate						INT,
	@idCoursesToReplicateTo								IDTABLE			READONLY,
	@idCoursesToUnSync									IDTABLE			READONLY

)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idRuleSetEnrollmentReplicated				INT				= 0
	DECLARE @idCourse									INT
	DECLARE @idCourseUnsync								INT

	DECLARE courseReplicateCursor						CURSOR 
	FOR 
	SELECT id FROM @idCoursesToReplicateTo

	DECLARE courseUnsyncCursor							CURSOR
	FOR 
	SELECT id FROM @idCoursesToUnSync	
	
	/*

	cursor through courses to unsync ruleset enrollments

	*/

	IF EXISTS (SELECT 1 FROM @idCoursesToUnSync)
	BEGIN

		OPEN courseUnsyncCursor
		FETCH NEXT FROM courseUnsyncCursor INTO @idCourseUnsync
		WHILE @@FETCH_STATUS = 0  
		BEGIN

		UPDATE tblRuleSetEnrollment 
		SET idParentRuleSetEnrollment = NULL
		WHERE idParentRuleSetEnrollment = @idRuleSetEnrollmentToReplicate
		AND idCourse = @idCourseUnsync

		FETCH NEXT FROM courseUnsyncCursor INTO @idCourseUnsync

	END

		CLOSE courseUnsyncCursor
		DEALLOCATE courseUnsyncCursor

	END


	/*
	
	cursor through courses to replicate ruleset enrollment to
	
	*/

	IF EXISTS (SELECT 1 FROM @idCoursesToReplicateTo)
	BEGIN

		OPEN courseReplicateCursor
		FETCH NEXT FROM courseReplicateCursor INTO @idCourse
		WHILE @@FETCH_STATUS = 0  
		BEGIN

		IF (SELECT COUNT(1) FROM tblRuleSetEnrollment WHERE idCourse = @idCourse AND idParentRuleSetEnrollment = @idRuleSetEnrollmentToReplicate  AND idSite = @idCallerSite) < 1
			BEGIN

			/*

			replicate ruleset enrollment	

			*/

			INSERT INTO tblRuleSetEnrollment
			(idSite, idCourse, isLockedByPrerequisites, priority, idTimezone, isFixedDate, dtStart, dtEnd, delayInterval, delayTimeframe, dueInterval, dueTimeframe, recurInterval, recurTimeframe, expiresFromStartInterval, expiresFromStartTimeframe, expiresFromFirstLaunchInterval, expiresFromFirstLaunchTimeframe, label, forceReassignment, dtCreated, idParentRuleSetEnrollment)
			SELECT 
				idSite, 
				@idCourse, 
				isLockedByPrerequisites, 
				[priority], 
				idTimezone, 
				isFixedDate, 
				dtStart, 
				dtEnd,
				delayInterval, 
				delayTimeframe, 
				dueInterval, 
				dueTimeframe, 
				recurInterval, 
				recurTimeframe, 
				expiresFromStartInterval, 
				expiresFromFirstLaunchTimeframe,
				expiresFromFirstLaunchInterval, 
				expiresFromStartTimeframe, 
				label, 
				forceReassignment, 
				GETUTCDATE(), 
				@idRuleSetEnrollmentToReplicate
			FROM tblRuleSetEnrollment RSE
			WHERE idRuleSetEnrollment = @idRuleSetEnrollmentToReplicate
			SET @idRuleSetEnrollmentReplicated = SCOPE_IDENTITY()
		
			/*

			replicate ruleset enrollment language	

			*/

			INSERT INTO tblRuleSetEnrollmentLanguage 
			SELECT 
				idSite, 
				@idRuleSetEnrollmentReplicated, 
				idLanguage, 
				label
			FROM tblRuleSetEnrollmentLanguage
			WHERE idRuleSetEnrollment = @idRuleSetEnrollmentToReplicate

			/*

			replicate rulesets	

			*/

			SELECT 
				idRuleSet AS [idSourceRuleSet], 
				idSite, 
				isAny, 
				label, 
				CAST(NULL AS INT) AS [idDestinationRuleSet]
			INTO #RuleSetsToReplicate
			FROM tblRuleSet
			WHERE idRuleSet IN (SELECT idRuleSet FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSetEnrollment = @idRuleSetEnrollmentToReplicate)

			INSERT INTO tblRuleSet
			SELECT 
				idSite, 
				isAny,
				CONCAT(label, ' ##', idSourceRuleSet, '##')
			FROM #RuleSetsToReplicate

			UPDATE #RuleSetsToReplicate 
			SET idDestinationRuleSet = RS.idRuleSet
			FROM tblRuleSet RS
			WHERE RS.label = #RuleSetsToReplicate.label + ' ##' + convert(nvarchar, #RuleSetsToReplicate.idSourceRuleSet) + '##'
		
			/*

			replicate ruleset languages	

			*/

			SELECT 
				idRuleSetLanguage AS [idSourceRuleSetLanguage], 
				idSite, 
				idRuleSet, 
				idLanguage, 
				label
			INTO #RuleSetLanguagesToReplicate
			FROM tblRuleSetLanguage
			WHERE idRuleSet IN (SELECT idSourceRuleSet FROM #RuleSetsToReplicate)

			INSERT INTO tblRuleSetLanguage
			SELECT 
				idSite, 
				idRuleSet, 
				idLanguage, 
				CONCAT(label, ' ##', idRuleSet, '##')
			FROM #RuleSetLanguagesToReplicate

			UPDATE tblRuleSetLanguage
			SET idRuleSet = RSTR.idDestinationRuleSet
			FROM #RuleSetsToReplicate RSTR
			LEFT JOIN tblRuleSetLanguage RSL on RSL.idRuleSet = RSTR.idSourceRuleSet
			WHERE RSL.label = RSTR.label + ' ##' + convert(nvarchar, RSTR.idSourceRuleSet) + '##'

			/*

			replicate ruleset to ruleset enrollment link

			*/

			INSERT INTO tblRuleSetToRuleSetEnrollmentLink
			SELECT 
				idSite, 
				idDestinationRuleSet, 
				@idRuleSetEnrollmentReplicated
			FROM #RuleSetsToReplicate 
		
			/*

			replicate rules

			*/

			SELECT 
				idRule, 
				idSite, 
				idRuleSet, 
				userField, 
				operator, 
				textValue, 
				dateValue, 
				numValue, 
				bitValue
			INTO #RulesToReplicate 
			FROM tblRule
			WHERE idRuleSet IN (SELECT idSourceRuleSet FROM #RuleSetsToReplicate)	
		
			INSERT INTO tblRule
			SELECT 
				RTR.idSite, 
				idDestinationRuleSet, 
				userField, 
				operator, 
				textValue, 
				dateValue, 
				numValue, 
				bitValue
			FROM #RulesToReplicate RTR
			LEFT JOIN #RuleSetsToReplicate RSTR ON RSTR.idSourceRuleSet = RTR.idRuleSet
		
			/*

			clean up tblRuleSet and tblRuleSetLanguage labels

			*/

			UPDATE tblRuleSet 
			SET label = RSTR.label
			FROM #RuleSetsToReplicate RSTR
			WHERE idRuleSet = RSTR.idDestinationRuleSet

			UPDATE tblRuleSetLanguage
			SET label = RSTR.label
			FROM #RuleSetsToReplicate RSTR
			WHERE idRuleSet = RSTR.idDestinationRuleSet

			/*

			drop temp tables

			*/

			DROP TABLE #RuleSetsToReplicate
			DROP TABLE #RulesToReplicate
			DROP TABLE #RuleSetLanguagesToReplicate

			END

			FETCH NEXT FROM courseReplicateCursor INTO @idCourse

		END

		CLOSE courseReplicateCursor
		DEALLOCATE courseReplicateCursor

	END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

