-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.GetSeatsAvailability]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.GetSeatsAvailability]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.GetSeatsAvailability]
GO

/*

Returns a recordset of resource ids and names that belong to a standup training instance.

*/

CREATE PROCEDURE [StandupTrainingInstance.GetSeatsAvailability]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@idStandupTrainingInstance	INT,
	@isWaitingList				BIT,
	@availableSeats				INT             OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	IF(@isWaitingList = 0)
	BEGIN
	   SELECT @availableSeats = (
	    STI.seats -             --total seats
	   (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink  STITUL 
	    WHERE STITUL.idStandupTrainingInstance = @idStandupTrainingInstance 
		AND STITUL.isWaitingList = 0)  --used seats
	   )
	   FROM tblStandUpTrainingInstance STI
	   WHERE STI.idStandUpTrainingInstance = @idStandupTrainingInstance
	   AND STI.idSite = @idCallerSite
	END
	ELSE
	BEGIN
		SELECT @availableSeats = (
			STI.waitingSeats -             --total seats
		   (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink  STITUL 
		    WHERE STITUL.idStandupTrainingInstance = @idStandupTrainingInstance 
			AND STITUL.isWaitingList = 1)  --used seats
		   )
		   FROM tblStandUpTrainingInstance STI
		   WHERE STI.idStandUpTrainingInstance = @idStandupTrainingInstance
		   AND STI.idSite =  @idCallerSite
	END   

	SET @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	