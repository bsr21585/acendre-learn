-- =====================================================================
-- PROCEDURE: [LearningPath.GetCourseSampleScreens]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.GetCourseSampleScreens]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.GetCourseSampleScreens]
GO

/*

Returns a recordset of course sample screens for courses belonging to the specified learning path.

*/

CREATE PROCEDURE [LearningPath.GetCourseSampleScreens]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idLearningPath			INT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	get the data
	
	*/

	DECLARE @courseIds TABLE (idCourse INT)

	INSERT INTO @courseIds (
		idCourse
	)
	SELECT 
		LPCL.idCourse
	FROM tblLearningPathToCourseLink LPCL
	LEFT JOIN tblCourse C ON C.idCourse = LPCL.idCourse
	WHERE (C.isDeleted IS NULL OR C.isDeleted = 0)
	AND LPCL.idLearningPath = @idLearningPath
	ORDER BY LPCL.[order]
		
	SELECT
		idCourse,
		[filename]
	FROM tblCourseToScreenshotLink CS
	WHERE CS.idSite = @idCallerSite
	AND CS.idCourse IN (SELECT idCourse FROM @courseIds)
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO