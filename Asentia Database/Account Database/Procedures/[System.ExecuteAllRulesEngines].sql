-- =====================================================================
-- PROCEDURE: [System.ExecuteAllRulesEngines]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ExecuteAllRulesEngines]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ExecuteAllRulesEngines]
GO

/*

Processes the entire system's rules engine for all group, role, certification, course enrollment, 
and learning path enrollment rules. This should only be executed manually, and on an as needed basis.
It is meant as a backup in case rules engine queue processing misses something. 

*/
CREATE PROCEDURE [System.ExecuteAllRulesEngines]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* DECLARE EMPTY FILTER TABLE */
	DECLARE @EMPTYIDTABLE IDTable
	
	/* EXECUTE ALL OF THE RULES ENGINES */
	EXEC [System.ExecuteGroupAutoJoinRules]							@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @EMPTYIDTABLE, ''
	EXEC [System.ExecuteRoleAutoJoinRules]							@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @EMPTYIDTABLE, ''
	EXEC [System.ExecuteCertificationAutoJoinRules]					@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @EMPTYIDTABLE, ''
	EXEC [System.ExecuteRuleSetEnrollmentAssignment]				@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @EMPTYIDTABLE, ''
	EXEC [System.ExecuteRuleSetLearningPathEnrollmentAssignment]	@Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EMPTYIDTABLE, @EMPTYIDTABLE, ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO