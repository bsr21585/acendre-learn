-- =====================================================================
-- PROCEDURE: [Data-TinCanProfile.GetProfiles]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCanProfile.GetProfiles]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCanProfile.GetProfiles]
GO

/*

Returns profiles.

*/

CREATE PROCEDURE [Data-TinCanProfile.GetProfiles]
(
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idEndpoint				INT,
	@activityId				NVARCHAR(100),
	@agent					NVARCHAR(MAX),
	@since					DATETIME
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @end INT
	
	/*
	
	Get the data
	
	*/
	
	SELECT	DISTINCT profileId
	FROM	[tblData-TinCanProfile]
	WHERE	idSite = @idCallerSite
	AND		idEndpoint = @idEndpoint
	AND		(@activityId IS NULL OR activityId = @activityId)
	AND		(@agent IS NULL OR agent = @agent)
	AND		(@since IS NULL OR dtUpdated > @since) --Only ids of profiles stored since the specified timestamp (exclusive) are returned.

	SELECT @Return_Code = 0 --Success
	SELECT @Error_Description_Code = NULL
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO