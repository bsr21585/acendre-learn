-- =====================================================================
-- PROCEDURE: [System.GenerateTokenForSSO]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF exists (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GenerateTokenForSSO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GenerateTokenForSSO]
GO

/*

Generates an SSO token for a user. Used in SSO processes for both the API and "jump" from Customer Manager.

*/

CREATE PROCEDURE [System.GenerateTokenForSSO]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idUser					INT,
	@token					NVARCHAR(40)	OUTPUT	   
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	get the site's hostname from the site table and validate that it exists
	
	*/

	DECLARE @hostname NVARCHAR(255)
		
	SELECT 
		@hostname = hostname
	FROM tblSite
	WHERE idSite = @idCallerSite

	IF (@hostname IS NULL OR @hostname = '')
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'SystemGenerateTokenForSSO_SiteDoesNotExist'
		RETURN 1
		END
	
	/*
	
	generate the token using the site's id, hostname, and the current utc timestamp

	*/

	DECLARE @tokenBase NVARCHAR(512)
	SET @tokenBase = CONVERT(NVARCHAR, @idCallerSite) + @hostname + CONVERT(NVARCHAR, GETUTCDATE(), 21)
	SET @token = dbo.GetHashedString('SHA1', @tokenBase)

	/*

	validate that we have a token

	*/

	IF (@token IS NULL OR @token = '')
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'SystemGenerateTokenForSSO_TokenNotGenerated'
		RETURN 1
		END

	/*

	insert the token

	*/

	INSERT INTO tblSSOToken (
		idSite,
		idUser,
		token,
		dtExpires
	)
	SELECT
		@idCallerSite,
		@idUser,
		@token,
		DATEADD(n, 1, GETUTCDATE()) -- make the token expire in 1 minute, that should be plenty of time

	/*

	return

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''			

	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO