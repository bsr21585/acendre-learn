-- =====================================================================
-- PROCEDURE: [CertificateImport.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificateImport.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificateImport.Delete]
GO

CREATE PROCEDURE [CertificateImport.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@CertificatesImport		IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @CertificatesImport) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CertificateImportDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all certificates exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @CertificatesImport CCI
		LEFT JOIN tblCertificateImport CI ON CI.idCertificateImport = CCI.id
		WHERE CI.idSite IS NULL
		OR CI.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificateImportDelete_NoRecordFound'
		RETURN 1 
		END
	
	/*

	Delete the Data from [tblCertificateRecord]

	*/

	DELETE	FROM tblCertificateRecord 
		WHERE idCertificateImport IN (
			SELECT id
			FROM @CertificatesImport
		)

	/*
	
	Delete the certificate(s) imported record .
	
	*/
	
	Delete tblCertificateImport 
	WHERE idCertificateImport IN (
		SELECT id
		FROM @CertificatesImport
	)
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO