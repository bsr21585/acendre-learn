-- =====================================================================
-- PROCEDURE: [Course.Delete]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Course.Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) 
	DROP PROCEDURE [Course.Delete]
GO

/*

Deletes courses

*/

CREATE PROCEDURE [Course.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@Courses				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @Courses) = 0 
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CourseDelete_NoRecordFound'
		RETURN 1
		END

	/*

	validate that all courses exist within the site

	*/

	IF (
		SELECT COUNT(1) 
		FROM @Courses CC
		LEFT JOIN tblCourse C ON C.idCourse = CC.id
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
	   ) > 0 
	   
	   BEGIN
	   SET @Return_Code = 1
	   SET @Error_Description_Code = 'CourseDelete_NoRecordFound'
	   RETURN 1
	   END

	/*

	SOFT DELETE the course(s), just mark the course(s) as deleted. DO NOT remove the record!

	*/

	UPDATE tblCourse SET isDeleted = 1, dtDeleted = GETUTCDATE() WHERE idCourse IN (SELECT id FROM @Courses)

	/*

	SOFT DELETE all lessons related to course(s) which we are deleting, were they are not already deleted

	*/

	UPDATE tblLesson SET isDeleted = 1, dtDeleted = GETUTCDATE() WHERE idCourse IN (SELECT id FROM @Courses) AND (isDeleted IS NULL OR isDeleted = 0)	

	/*

	DELETE all the entries in [tblLessonToContentLink] related to course(s) which we are deleting

	*/

	DELETE FROM tblLessonToContentLink WHERE idLesson IN (SELECT idLesson FROM tblLesson WHERE idCourse IN (SELECT id FROM @Courses))

	/*

	DELETE all the entries in [tblCourseToPrerequisiteLink] related to course(s) which we are deleting

	*/

	DELETE FROM tblCourseToPrerequisiteLink WHERE idCourse IN (SELECT id FROM @Courses)

	/*

	DELETE all the entries in [tblCourseExpert] related to course(s) which we are deleting

	*/

	DELETE FROM tblCourseExpert WHERE idCourse IN (SELECT id FROM @Courses)

	/*

	DELETE all the entries in [tblCourseFeedMessage] related to course(s) which we are deleting

	*/
	
	DELETE FROM tblCourseFeedMessage WHERE idCourse IN (SELECT id FROM @Courses)

	/*

	DELETE all the entries in [tblCourseFeedModerator] related to course(s) which we are deleting

	*/

	DELETE FROM tblCourseFeedModerator WHERE idCourse IN (SELECT id FROM @Courses)

	/*

	DELETE all the entries in [tblUserToCourseFeedSubscription] related to course(s) which we are deleting

	*/

	DELETE FROM tblUserToCourseFeedSubscription WHERE idCourse IN (SELECT id FROM @Courses)

	/*

	DELETE all the entries in [tblCourseRating] related to course(s) which we are deleting

	*/

	DELETE FROM tblCourseRating WHERE idCourse IN (SELECT id FROM @Courses)

	/*

	DELETE all the entries in [tblDocumentRepositoryItemLanguage] related to course(s) which we are deleting

	*/

	DELETE FROM tblDocumentRepositoryItemLanguage WHERE idDocumentRepositoryItem IN (SELECT idDocumentRepositoryItem FROM tblDocumentRepositoryItem WHERE idObject IN (SELECT id FROM @Courses) AND idDocumentRepositoryObjectType = 2)  -- 2 is for course

	/*

	DELETE all the entries in [tblDocumentRepositoryItem] related to course(s) which we are deleting

	*/

	DELETE FROM tblDocumentRepositoryItem WHERE idObject IN (SELECT id FROM @Courses) AND idDocumentRepositoryObjectType = 2  -- 2 is for course

	/*

	DELETE all the entries in [tblDocumentRepositoryFolder] related to course(s) which we are deleting

	*/

	DELETE FROM tblDocumentRepositoryFolder WHERE idObject IN (SELECT id FROM @Courses) AND idDocumentRepositoryObjectType = 2 -- 2 is for course

	/*

	SOFT DELETE all the entries in [tblEventEmailNotification] related to course(s) which we are deleting, where they are not already deleted

	*/

	UPDATE tblEventEmailNotification SET isDeleted = 1 WHERE idObject IN (SELECT id FROM @Courses) AND (isDeleted IS NULL OR isDeleted = 0)
	AND idEventType >= 200 AND idEventType < 400 -- 200 - 300 is course, 300 - 400 is lesson	

	/*

	SOFT DELETE all the entries in [tblCertificate] related to course(s) which we are deleting, where they are not already deleted

	*/

	UPDATE tblCertificate SET dtDeleted = GETUTCDATE(), isDeleted = 1 WHERE idObject IN (SELECT id FROM @Courses) AND objectType = 1 AND (isDeleted IS NULL OR isDeleted = 0) -- objectType 1 is for course

	/*

	DELETE all the entries in [tblCourseToCatalogLink] related to course(s) which we are deleting

	*/

	DELETE FROM tblCourseToCatalogLink WHERE idCourse IN (SELECT id FROM @Courses CC)

	/*

	DELETE all the entries in [tblCouponCodeToCourseLink] related to course(s) which we are deleting

	*/

	DELETE FROM tblCouponCodeToCourseLink WHERE idCourse IN (SELECT id FROM @Courses)	

	/*

	GET the ruleset enrollment ids to remove

	*/

	DECLARE @RuleSetEnrollments IDTable

	INSERT INTO @RuleSetEnrollments (
		id
	)
	SELECT
		idRuleSetEnrollment
	FROM tblRuleSetEnrollment
	WHERE idCourse IN (SELECT id FROM @Courses)

	/*

	GET ruleset ids to remove rules, rulesets, and ruleset links

	*/

	DECLARE @RuleSets IDTable

	INSERT INTO @RuleSets (
		id
	)
	SELECT
		idRuleSet
	FROM tblRuleSetToRuleSetEnrollmentLink
	WHERE idRuleSetEnrollment IN (SELECT id FROM @RuleSetEnrollments)

	/*

	DELETE ruleset to ruleset enrollment links.
	
	*/
	
	DELETE FROM tblRuleSetToRuleSetEnrollmentLink
	WHERE idRuleSetEnrollment IN (SELECT id FROM @RuleSetEnrollments)

	/*

	DELETE the rules 

	*/

	DELETE FROM tblRule 
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the ruleset language records

	*/

	DELETE FROM tblRuleSetLanguage
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the rulesets

	*/

	DELETE FROM tblRuleSet
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE open ruleset engine queue items

	*/

	DELETE FROM tblRuleSetEngineQueue
	WHERE triggerObjectId IN (SELECT id FROM @RuleSetEnrollments) 
	AND triggerObject = 'rulesetenrollment'
	AND isProcessing IS NULL
	AND dtProcessed IS NULL

	/*

	DELETE enrollments for the course(s) by using the Enrollment.Delete Procedure
	Only delete incomplete ones, leave the completed enrollments for transcript purposes.

	*/

	DECLARE @EnrollmentsToDelete IDTable

	INSERT INTO @EnrollmentsToDelete (
		id
	)
	SELECT
		E.idEnrollment
	FROM tblEnrollment E
	WHERE E.idCourse IN (SELECT id FROM @Courses)
	AND E.dtCompleted IS NULL
	AND E.idActivityImport IS NULL -- exclude imported records

	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EnrollmentsToDelete, 0

	/*

	SET idRuleSetEnrollment, idGroupEnrollment, idLearningPathEnrollment to NULL for all COMPLETED enrollments of the course(s) which we are deleting

	*/

	UPDATE tblEnrollment SET idRuleSetEnrollment = NULL, idGroupEnrollment = NULL, idLearningPathEnrollment = NULL WHERE idCourse IN (SELECT id FROM @Courses) AND dtCompleted IS NOT NULL

	/*

	DELETE all the entries in [tblRuleSetEnrollmentLanguage] related to course(s) which we are deleting

	*/

	DELETE FROM tblRuleSetEnrollmentLanguage
	WHERE idRuleSetEnrollment IN (SELECT idRuleSetEnrollment
								  FROM tblRuleSetEnrollment
								  WHERE idCourse IN (SELECT id FROM @Courses))

	/*

	DELETE all the entries in [tblRuleSetEnrollment] related to course(s) which we are deleting

	*/

	DELETE FROM tblRuleSetEnrollment WHERE idCourse IN (SELECT id FROM @Courses)

	/*
	
	DELETE the group enrollments by using the group enrollment delete procedure.
	
	*/
	
	DECLARE @GroupEnrollmentsToDelete IDTable

	INSERT INTO @GroupEnrollmentsToDelete (
		id
	)
	SELECT
		idGroupEnrollment
	FROM tblGroupEnrollment GE
	WHERE GE.idCourse IN (SELECT id FROM @Courses)

	EXEC [GroupEnrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, 1, @GroupEnrollmentsToDelete, 0

	/*

	DELETE all the entries in [tblLearningPathToCourseLink] related to course(s) which we are deleting

	*/

	DELETE FROM tblLearningPathToCourseLink WHERE idCourse IN (SELECT id FROM @Courses)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO