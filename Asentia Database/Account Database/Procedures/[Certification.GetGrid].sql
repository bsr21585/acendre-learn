-- =====================================================================
-- PROCEDURE: [Certification.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.GetGrid]
GO

/*

Gets a listing of Certifications.

*/

CREATE PROCEDURE [Certification.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblCertification C
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
				)
			AND (C.isDeleted IS NULL OR C.isDeleted = 0)

			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						C.idCertification,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN C.title END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN C.title END
						)
						AS [row_number]
					FROM tblCertification C
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
						)
					AND (C.isDeleted IS NULL OR C.isDeleted = 0)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idCertification, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				C.idCertification,
				CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title, 
				CASE WHEN C.isPublished IS NULL THEN CONVERT(bit, 0) ELSE C.isPublished END AS isPublished,
				CASE WHEN C.isClosed IS NULL THEN CONVERT(bit, 0) ELSE C.isClosed END AS isClosed,
				CONVERT(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblCertification C ON C.idCertification = SelectedKeys.idCertification
			LEFT JOIN tblCertificationLanguage CL ON CL.idCertification = C.idCertification AND CL.idLanguage = @idCallerLanguage
			WHERE (C.isDeleted IS NULL OR C.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblCertification C
				INNER JOIN CONTAINSTABLE(tblCertification, *, @searchParam) K ON K.[key] = C.idCertification
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
					)
				AND (C.isDeleted IS NULL OR C.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							C.idCertification,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN C.title END DESC,
							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN C.title END
							)
							AS [row_number]
						FROM tblCertification C
						INNER JOIN CONTAINSTABLE(tblCertification, *, @searchParam) K ON K.[key] = C.idCertification
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite
							)
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idCertification, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					C.idCertification, 
					C.title,
					CASE WHEN C.isPublished IS NULL THEN CONVERT(bit, 0) ELSE C.isPublished END AS isPublished,
					CASE WHEN C.isClosed IS NULL THEN CONVERT(bit, 0) ELSE C.isClosed END AS isClosed,
					CONVERT(bit, 1) AS isModifyOn,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblCertification C ON C.idCertification = SelectedKeys.idCertification
				WHERE (C.isDeleted IS NULL OR C.isDeleted = 0)
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblCertificationLanguage CL
				LEFT JOIN tblCertification C ON C.idCertification = CL.idCertification
				INNER JOIN CONTAINSTABLE(tblCertificationLanguage, *, @searchParam) K ON K.[key] = CL.idCertificationLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
					)
				AND CL.idLanguage = @idCallerLanguage
				AND (C.isDeleted IS NULL OR C.isDeleted = 0)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							CL.idCertificationLanguage,
							C.idCertification,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN CL.title END DESC,
							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN CL.title END
							)
							AS [row_number]
						FROM tblCertificationLanguage CL
						LEFT JOIN tblCertification C ON C.idCertification = CL.idCertification
						INNER JOIN CONTAINSTABLE(tblCertificationLanguage, *, @searchParam) K ON K.[key] = CL.idCertificationLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite
							)
						AND CL.idLanguage = @idCallerLanguage
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idCertification, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					C.idCertification, 
					CL.title,
					CASE WHEN C.isPublished IS NULL THEN CONVERT(bit, 0) ELSE C.isPublished END AS isPublished,
					CASE WHEN C.isClosed IS NULL THEN CONVERT(bit, 0) ELSE C.isClosed END AS isClosed,
					CONVERT(bit, 1) AS isModifyOn,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblCertificationLanguage CL ON CL.idCertification = SelectedKeys.idCertification AND CL.idLanguage = @idCallerLanguage
				LEFT JOIN tblCertification C ON C.idCertification = CL.idCertification
				ORDER BY SelectedKeys.[row_number]

				END
			
			END
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO