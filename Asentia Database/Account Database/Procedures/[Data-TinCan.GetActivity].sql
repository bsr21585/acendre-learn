-- =====================================================================
-- PROCEDURE: [Data-TinCan.GetActivity]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCan.GetActivity]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCan.GetActivity]
GO

/*

Returns activity for a given id.

*/

CREATE PROCEDURE [Data-TinCan.GetActivity]
(
	@Return_Code							INT						OUTPUT,
	@Error_Description_Code					NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idEndpoint				INT,
	@activityId								NVARCHAR(100),
	@object									NVARCHAR(max)			OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the activity exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-TinCan]
		WHERE idSite = @idCallerSite
		AND activityId = @activityId
		) = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataTinCanGetActivity_ActivityNotFound'
		RETURN 1
		END
	
	/*
	
	validate caller permission to the specified activity
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-TinCan]
		WHERE idSite = @idCallerSite
		AND activityId = @activityId
		AND idEndpoint = @idEndpoint
		) = 0
		BEGIN
		SELECT @Return_Code = 3
		SET @Error_Description_Code = 'DataTinCanGetActivity_PermissionError'
		RETURN 1
		END
	
	/*
	
	get the data 
	
	*/
	
	SELECT	TOP 1 @object = [object]
	FROM	[tblData-TinCan]
	WHERE	idSite = @idCallerSite
	AND		activityId = @activityId
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataTinCanGetActivity_ActivityNotFound'
		END
	ELSE
		BEGIN
		SELECT @Return_Code = 0 --Success
		SELECT @Error_Description_Code = NULL
		END
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO