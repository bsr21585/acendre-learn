-- =====================================================================
-- PROCEDURE: [QuizSurvey.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[QuizSurvey.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [QuizSurvey.Details]
GO

/*

Return all the properties for a given quiz/survey id.

*/
CREATE PROCEDURE [QuizSurvey.Details]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	
	@idQuizSurvey				INT				OUTPUT,
	@idSite						INT				OUTPUT,
	@idAuthor					INT				OUTPUT,
	@type						INT				OUTPUT,
	@identifier					NVARCHAR(255)	OUTPUT,
	@guid						NVARCHAR(36)	OUTPUT,
	@data						NVARCHAR(MAX)	OUTPUT,
	@isDraft					BIT				OUTPUT,
	@idContentPackage			INT				OUTPUT,
    @dtCreated					DATETIME		OUTPUT,
    @dtModified					DATETIME		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblQuizSurvey
		WHERE idQuizSurvey = @idQuizSurvey
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'QuizSurveyDetails_NoRecordFound'
		RETURN 1
		END	
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idQuizSurvey		= QS.idQuizSurvey,
		@idSite				= QS.idSite,
		@idAuthor			= QS.idAuthor,
		@type				= QS.[type],
		@identifier			= QS.identifier,
		@guid				= QS.[guid],
		@data				= QS.data,
		@isDraft			= QS.isDraft,
		@idContentPackage	= QS.idContentPackage,
		@dtCreated			= QS.dtCreated,
		@dtModified			= QS.dtModified
	FROM tblQuizSurvey QS
	WHERE QS.idQuizSurvey = @idQuizSurvey
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'QuizSurveyDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO