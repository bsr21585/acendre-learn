-- =====================================================================
-- PROCEDURE: [Dataset.UniqueUserLogins]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Dataset.UniqueUserLogins]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Dataset.UniqueUserLogins]
GO

CREATE PROCEDURE [dbo].[Dataset.UniqueUserLogins]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@fields						NVARCHAR(MAX),
	@whereClause				NVARCHAR(MAX),
	@orderByClause				NVARCHAR(768),
	@maxRecords					INT,
	@logReportExecution			BIT
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*

	declare and assign local variables to prevent "parameter sniffing"

	*/

	DECLARE @fieldsLOC					NVARCHAR(MAX)
	DECLARE @whereClauseLOC				NVARCHAR(MAX)
	DECLARE @orderByClauseLOC			NVARCHAR(768)
	DECLARE @maxRecordsLOC				INT
	DECLARE @logReportExecutionLOC		BIT

	SET @fieldsLOC				= @fields
	SET @whereClauseLOC			= @whereClause
	SET @orderByClauseLOC		= @orderByClause
	SET @maxRecordsLOC			= @maxRecords
	SET @logReportExecutionLOC	= @logReportExecution

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
		if no fields defined, select * (all)

	*/

	IF @fieldsLOC IS NULL OR @fieldsLOC = ''
		BEGIN
		SET @fieldsLOC = '*'
		END

	/*

	get the id of the caller's language

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage =
		CASE WHEN idLanguage IS NULL THEN 
			57
		ELSE 
			idLanguage 
		END
		FROM tblLanguage WHERE code = @callerLangString

	/*

	get the group scope so we can filter on what the calling user is allowed to see
	note that NULL scope means the caller can see anything, and idCaller 1 automatically has null scope and permission to anything
	
	*/

	DECLARE @idDatasetPermission INT
	SET @idDatasetPermission = 401

	DECLARE @groupScope NVARCHAR(MAX)
	SET @groupScope = NULL

	IF (@idCaller > 1)
		BEGIN 

		CREATE TABLE #CallerRoles (idRole INT)
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT URL.idRole FROM tblUserToRoleLink URL WHERE URL.idUser = @idCaller -- directly assigned roles
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT GRL.idRole FROM tblGroupToRoleLink GRL -- group inherited roles
									      WHERE GRL.idGroup IN (SELECT DISTINCT UGL.idGroup FROM tblUserToGroupLink UGL WHERE UGL.idUser = @idCaller)
										  AND NOT EXISTS (SELECT 1 FROM #CallerRoles CR WHERE CR.idRole = GRL.idRole)
		
		-- if the caller does not have any permissions to this dataset with NULL scope, get the defined scopes
		IF (
		    SELECT COUNT(1) FROM tblRoleToPermissionLink RPL 
		    WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles)
			AND RPL.idPermission = @idDatasetPermission
			AND RPL.scope IS NULL
		   ) = 0
			BEGIN

			-- get all scope items into the group scope variable			
			DECLARE @scopeItems NVARCHAR(MAX)
			SET @groupScope = ''

			DECLARE scopeListCursor CURSOR LOCAL FOR
				SELECT scope FROM tblRoleToPermissionLink RPL WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles) AND RPL.idPermission = @idDatasetPermission AND RPL.scope IS NOT NULL

			OPEN scopeListCursor

			FETCH NEXT FROM scopeListCursor INTO @scopeItems

			WHILE @@FETCH_STATUS = 0
				BEGIN

				IF (@scopeItems <> '')
					BEGIN
					IF (@groupScope <> '')
						BEGIN
						SET @groupScope = @groupScope + ',' + @scopeItems
						END
					ELSE
						BEGIN
						SET @groupScope = @scopeItems
						END
						
					END

				FETCH NEXT FROM scopeListCursor INTO @scopeItems

				END

			-- kill the cursor
			CLOSE scopeListCursor
			DEALLOCATE scopeListCursor

			END

		END

	/*

		build sql query

	*/

   DECLARE @sql NVARCHAR(MAX)

   SET @sql = ' SELECT ' +
				' [_idSite], ' +
				' [Year], ' +
				' [Month], ' +
				' COUNT(1) AS [Number of Unique Logins] ' +
			  ' FROM ' +
				' (SELECT DISTINCT ' +
					CONVERT(NVARCHAR, @idCallerSite) + ' AS _idSite, ' +
					' UL.idUser, ' +
					' S.idSite AS [idSite], ' +
					' V.s AS [Year], ' +
					' CASE ' +
						' WHEN Z.s = ''01'' THEN ''January'' ' +
						' WHEN Z.s = ''02'' THEN ''February'' ' +
						' WHEN Z.s = ''03'' THEN ''March'' ' +
						' WHEN Z.s = ''04'' THEN ''April'' ' +
						' WHEN Z.s = ''05'' THEN ''May'' ' +
						' WHEN Z.s = ''06'' THEN ''June'' ' +
						' WHEN Z.s = ''07'' THEN ''July'' ' +
						' WHEN Z.s = ''08'' THEN ''August'' ' +
						' WHEN Z.s = ''09'' THEN ''September'' ' +
						' WHEN Z.s = ''10'' THEN ''October'' ' +
						' WHEN Z.s = ''11'' THEN ''November'' ' +
						' WHEN Z.s = ''12'' THEN ''December'' ' +
					' END AS [Month] ' +
				' FROM tblUserLog UL ' +
				' LEFT JOIN dbo.virtualTable(''2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025,2026,2027,2028,2029,2030'', '','') V ON 1 = 1 ' +
				' LEFT JOIN dbo.virtualTable(''01,02,03,04,05,06,07,08,09,10,11,12'', '','') Z ON 1 = 1 ' +
				' LEFT JOIN tblSite S ON S.idSite = UL.idSite ' +
				' WHERE S.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + 
				' AND UL.action = ''login'' ' +
				' AND YEAR(UL.timestamp) = V.s ' +
				' AND MONTH(UL.timestamp) = Z.s ' +
				' AND V.s <= YEAR(GETUTCDATE()) ' +
				' AND Z.s <= MONTH(GETUTCDATE())) ' +
			' AS Users ' +
			' GROUP BY [Year], [Month], [_idSite] ' 
		
	/*

	execute the sql statement and log its execution

	*/

	DECLARE @dtExecutionBegin DATETIME
	DECLARE @dtExecutionEnd DATETIME
	DECLARE @executionDurationMS INT

	SET @dtExecutionBegin = GETUTCDATE()

	EXEC sp_executesql @sql

	SET @dtExecutionEnd = GETUTCDATE()

	SET @executionDurationMS = DATEDIFF(ms, @dtExecutionBegin, @dtExecutionEnd)

	IF (@logReportExecutionLOC = 1)
	BEGIN
		INSERT INTO tblReportProcessorLog
		(
			idSite,
			idCaller,
			datasetProcedure,
			dtExecutionBegin,
			dtExecutionEnd,
			executionDurationSeconds,
			sqlQuery
		)
		SELECT
			@idCallerSite,
			@idCaller,
			'[Dataset.UniqueUserLogins]',
			@dtExecutionBegin,
			@dtExecutionEnd,
			CAST((CAST(@executionDurationMS AS DECIMAL)/1000) AS DECIMAL(9,2)),
			@sql
	END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END