-- =====================================================================
-- PROCEDURE: [Dataset.UserCertificationTranscripts]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Dataset.UserCertificationTranscripts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Dataset.UserCertificationTranscripts]
GO

/*

User Certification Transcripts Dataset

*/

CREATE PROCEDURE [Dataset.UserCertificationTranscripts]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@fields						NVARCHAR(MAX),
	@whereClause				NVARCHAR(MAX),
	@orderByClause				NVARCHAR(768),
	@maxRecords					INT,
	@logReportExecution			BIT
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*

	declare and assign local variables to prevent "parameter sniffing"

	*/

	DECLARE @fieldsLOC					NVARCHAR(MAX)
	DECLARE @whereClauseLOC				NVARCHAR(MAX)
	DECLARE @orderByClauseLOC			NVARCHAR(768)
	DECLARE @maxRecordsLOC				INT
	DECLARE @logReportExecutionLOC		BIT

	SET @fieldsLOC				= @fields
	SET @whereClauseLOC			= @whereClause
	SET @orderByClauseLOC		= @orderByClause
	SET @maxRecordsLOC			= @maxRecords
	SET @logReportExecutionLOC	= @logReportExecution

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
		if no fields defined, select * (all)

	*/

	IF @fieldsLOC IS NULL OR @fieldsLOC = ''
		BEGIN
		SET @fieldsLOC = '*'
		END

	/*

	get the id of the caller's language

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage =
		CASE WHEN idLanguage IS NULL THEN 
			57
		ELSE 
			idLanguage 
		END
		FROM tblLanguage WHERE code = @callerLangString

	/*

	get the group scope so we can filter on what the calling user is allowed to see
	note that NULL scope means the caller can see anything, and idCaller 1 automatically has null scope and permission to anything
	
	*/

	DECLARE @idDatasetPermission INT
	SET @idDatasetPermission = 407

	DECLARE @groupScope NVARCHAR(MAX)
	SET @groupScope = NULL

	IF (@idCaller > 1)
		BEGIN 

		CREATE TABLE #CallerRoles (idRole INT)
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT URL.idRole FROM tblUserToRoleLink URL WHERE URL.idUser = @idCaller -- directly assigned roles
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT GRL.idRole FROM tblGroupToRoleLink GRL -- group inherited roles
									      WHERE GRL.idGroup IN (SELECT DISTINCT UGL.idGroup FROM tblUserToGroupLink UGL WHERE UGL.idUser = @idCaller)
										  AND NOT EXISTS (SELECT 1 FROM #CallerRoles CR WHERE CR.idRole = GRL.idRole)
		
		-- if the caller does not have any permissions to this dataset with NULL scope, get the defined scopes
		IF (
		    SELECT COUNT(1) FROM tblRoleToPermissionLink RPL 
		    WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles)
			AND RPL.idPermission = @idDatasetPermission
			AND RPL.scope IS NULL
		   ) = 0
			BEGIN

			-- get all scope items into the group scope variable			
			DECLARE @scopeItems NVARCHAR(MAX)
			SET @groupScope = ''

			DECLARE scopeListCursor CURSOR LOCAL FOR
				SELECT scope FROM tblRoleToPermissionLink RPL WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles) AND RPL.idPermission = @idDatasetPermission AND RPL.scope IS NOT NULL

			OPEN scopeListCursor

			FETCH NEXT FROM scopeListCursor INTO @scopeItems

			WHILE @@FETCH_STATUS = 0
				BEGIN

				IF (@scopeItems <> '')
					BEGIN
					IF (@groupScope <> '')
						BEGIN
						SET @groupScope = @groupScope + ',' + @scopeItems
						END
					ELSE
						BEGIN
						SET @groupScope = @scopeItems
						END
						
					END

				FETCH NEXT FROM scopeListCursor INTO @scopeItems

				END

			-- kill the cursor
			CLOSE scopeListCursor
			DEALLOCATE scopeListCursor

			END

		END

	/*

		build sql query

	*/

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCont NVARCHAR(MAX)

	SET @sql = 'SELECT DISTINCT '
				+ CASE WHEN (@maxRecordsLOC IS NOT NULL AND @maxRecordsLOC > 0) THEN + ' TOP ' + convert(nvarchar, @maxRecordsLOC) + ' ' ELSE '' END
    SET @sql = @sql +  @fieldsLOC + ',_idUser'

	-- if one of the requested fields is "Uploaded Task File" get _idDataCertificationModuleRequirement, it's needed to link to the file
	IF (CHARINDEX('[Uploaded Task File]', @fieldsLOC) > 0)
		BEGIN
		SET @sql = @sql + ',CASE WHEN [Uploaded Task File] IS NOT NULL THEN _idDataCertificationModuleRequirement ELSE NULL END AS _idDataCertificationModuleRequirement'		
		END

    SET @sql = @sql +  ' FROM ('
				+ 'SELECT DISTINCT '
				+ '	U.idSite AS _idSite,  '
				+ ' U.idUser AS _idUser,  '
				+ '	U.lastname + '', '' + U.firstName + (CASE WHEN U.middlename IS NOT NULL THEN '' '' + U.middlename ELSE '''' END) AS [Full Name],  '
				+ '	U.lastname AS [##lastName##],  '
				+ '	U.middleName AS [##middleName##], '
				+ '	U.firstname AS [##firstName##],  '
				+ '	U.email AS [##email##],  '
				+ ' U.username AS [##username##],  '
				+ ' U.jobTitle AS [##jobTitle##],  '
				+ '	U.jobClass AS [##jobClass##],  '
				+ '	U.company AS [##company##],  '
				+ '	REPLACE(REPLACE(U.address, CHAR(10), '' ''), CHAR(13), '' '') AS [##address##], '
				+ '	U.city AS [##city##],  '
				+ '	U.province AS [##province##],  '
				+ '	U.postalcode AS [##postalcode##],  '
				+ '	U.country AS [##country##], '
				+ '	U.phonePrimary AS [##phonePrimary##], ' 
				+ '	U.phoneWork AS [##phoneWork##],  '
				+ '	U.phoneHome AS [##phoneHome##],  '
				+ '	U.phoneFax AS [##phoneFax##],  '
				+ '	U.phoneMobile AS [##phoneMobile##], '
				+ '	U.phonePager AS [##phonePager##],  '
				+ '	U.phoneOther AS [##phoneOther##],  '
				+ '	U.division AS [##division##],  '
				+ '	U.department AS [##department##],  '
				+ '	U.region AS [##region##],  '
				+ '	U.employeeID AS [##employeeID##], '
				+ '	U.dtHire AS [##dtHire##], '
				+ '	U.dtTerm AS [##dtTerm##], '
				+ '	U.gender AS [##gender##], '
				+ '	U.race AS [##race##], '
				+ '	U.dtDOB AS [##dtDOB##], '
				+ ' CONVERT(BIT, U.isActive) AS [##isActive##],	'
				+ ' CAST(U.isActive AS INT) AS [_sb##isactive##],  '
				+ ' U.dtCreated AS [##dtCreated##], '
				+ ' U.dtExpires AS [##dtExpires##], '
				+ ' U.dtLastLogin AS [##dtLastLogin##], '
				+ '	L.idLanguage AS [##idLanguage##], '
				+ '	L.code AS [_sb##languageCode##], '
				+ '	U.idTimezone AS [##idTimezone##], '
				
				-- if "Group" fields are in the selected fields or filters, select tblUserToGroupLink fields
				IF (CHARINDEX('[Group]', @fieldsLOC) > 0 OR CHARINDEX('[Group]', @whereClauseLOC) > 0)
					BEGIN
					SET @sql = @sql + '	CASE WHEN GL.[name] IS NOT NULL THEN GL.[name] ELSE G.[name] END AS [Group], '
					+ '	UGL.idGroup as _sbGroup, ' -- hidden field from reports; used to filter only users that the report-runner is permitted to see
					END

				SET @sql = @sql + '	SUPER.displayName AS [Supervisor], '
				+ '	SUPER.idUser AS _idSupervisor, '
				+ '	U.field00 AS [##field00##], '
				+ '	U.field01 AS [##field01##], '
				+ '	U.field02 AS [##field02##], '
				+ '	U.field03 AS [##field03##], '
				+ '	U.field04 AS [##field04##], '
				+ '	U.field05 AS [##field05##], '
				+ '	U.field06 AS [##field06##], '
				+ '	U.field07 AS [##field07##], '
				+ '	U.field08 AS [##field08##], '
				+ '	U.field09 AS [##field09##], '
				+ '	U.field10 AS [##field10##], '
				+ '	U.field11 AS [##field11##], '
				+ '	U.field12 AS [##field12##], '
				+ '	U.field13 AS [##field13##], '
				+ '	U.field14 AS [##field14##], '
				+ '	U.field15 AS [##field15##], '
				+ '	U.field16 AS [##field16##], '
				+ '	U.field17 AS [##field17##], '
				+ '	U.field18 AS [##field18##], '
				+ '	U.field19 AS [##field19##], '
				+ '	CASE WHEN CL.title IS NOT NULL THEN '
				+ '		CL.title '
				+ '	ELSE '
				+ '		C.title '
				+ '	END AS [Certification], '
				+ ' CONVERT(NVARCHAR, CUL.certificationTrack) AS [Current Track], '
				+ ' CASE WHEN CUL.initialAwardDate IS NOT NULL AND CUL.currentExpirationDate IS NOT NULL THEN '
				+ ' 	CASE WHEN GETUTCDATE() < CUL.currentExpirationDate THEN '
				+ ' 		''current'' '
				+ ' 	ELSE '
				+ ' 		''expired'' '
				+ ' 	END '
				+ ' ELSE '
				+ ' 	''incomplete'' '
				+ ' END AS [Certification Status], '
				+ ' CUL.initialAwardDate AS [Initial Award Date], '
				+ ' CUL.currentExpirationDate AS [Current Expiration Date], '
				+ ' CUL.lastExpirationDate AS [Last Expiration Date], '
				+ '	CASE WHEN CML.title IS NOT NULL THEN '
				+ '		CML.title '
				+ '	ELSE '
				+ '		CM.title '
				+ '	END AS [Unit], '
				-- UNIT STATUS
				+ '	CASE WHEN CMRSL.label IS NOT NULL THEN '
				+ '		CMRSL.label '
				+ '	ELSE '
				+ '		CMRS.label '
				+ '	END AS [Segment], '
				+ '	CASE WHEN CMRL.label IS NOT NULL THEN '
				+ '		CMRL.label '
				+ '	ELSE '
				+ '		CMR.label '
				-- SEGMENT STATUS
				+ '	END AS [Requirement], '
				-- REQUIREMENT STATUS
				--+ '	CASE WHEN CMR.requirementType = 0 THEN '
				--+ '		CASE WHEN CMR.courseCompletionIsAny = 1 THEN '
				--+ '			CASE WHEN (SELECT COUNT(1) '
				--+ '					   FROM (SELECT DISTINCT idCourse '
				--+ '							 FROM tblEnrollment E '
				--+ '							 WHERE E.idUser = U.idUser '
				--+ '							 AND E.dtCompleted IS NOT NULL '
				--+ '							 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN '
				--+ '					  ) >= 1 THEN 1 ELSE 0 END '
				--+ '		ELSE '
				--+ '			CASE WHEN (SELECT COUNT(1) '
				--+ '					   FROM (SELECT DISTINCT idCourse '
				--+ '					   FROM tblEnrollment E '
				--+ '					   WHERE E.idUser = U.idUser '
				--+ '					   AND E.dtCompleted IS NOT NULL '
				--+ '					   AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN '
				--+ '					  ) >= (SELECT COUNT(1) '
				--+ '							FROM tblCertificationModuleRequirementToCourseLink '
				--+ '							WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement '
				--+ '						   ) THEN 1 ELSE 0 END '
				--+ '		END '
				--+ '	WHEN CMR.requirementType = 1 THEN '
				--+ '		CASE WHEN CMR.courseCreditEligibleCoursesIsAll = 1 THEN '
				--+ '			CASE WHEN (SELECT SUM(CCCL.credits) '
				--+ '					   FROM (SELECT DISTINCT idCourse  '
				--+ '							 FROM tblEnrollment E  '
				--+ '							 WHERE E.idUser = U.idUser  '
				--+ '							 AND E.dtCompleted IS NOT NULL  '
				--+ '							 AND E.idCourse IN (SELECT idCourse FROM tblCertificationToCourseCreditLink WHERE idCertification = CUL.idCertification)) E_MAIN '
				--+ '					   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse '
				--+ '					  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END '
				--+ '		ELSE '
				--+ '			CASE WHEN (SELECT SUM(CCCL.credits) '
				--+ '					   FROM (SELECT DISTINCT idCourse  '
				--+ '							 FROM tblEnrollment E  '
				--+ '							 WHERE E.idUser = U.idUser  '
				--+ '							 AND E.dtCompleted IS NOT NULL  '
				--+ '							 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN '
				--+ '					   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse '
				--+ '					  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END '
				--+ '		END '
				--+ '	WHEN CMR.requirementType = 2 THEN '
				--+ '		CASE WHEN DCMR.dtCompleted IS NOT NULL THEN 1 ELSE 0 END '
				--+ '	END AS [Requirement Status], '
				+ ' DCMR.[idData-CertificationModuleRequirement] AS _idDataCertificationModuleRequirement,  '			
				+ ' DCMR.dtSubmitted AS [Task Submitted Date], '
				+ ' DCMR.dtApproved AS [Task Approved Date], '
				+ ' CASE WHEN DCMR.idApprover IS NULL THEN '
				+ '		NULL '
				+ '	ELSE '
				+ '		APPROVER.displayName '
				+ ' END AS [Task Approver], '
				+ ' DCMR.dtCompleted AS [Task Completed Date], '
				+ ' DCMR.completionDocumentationFilePath AS [Uploaded Task File] '				
				+ 'FROM tblUser U '

				-- if "Group" fields are in the selected fields or filters, or there is group scope, join to group tables
				IF (CHARINDEX('[Group]', @fieldsLOC) > 0 OR CHARINDEX('[Group]', @whereClauseLOC) > 0 OR (CHARINDEX('_idUser =', @whereClauseLOC) <= 0 AND @groupScope IS NOT NULL AND @groupScope <> ''))				
					BEGIN
					SET @sql = @sql + 'LEFT JOIN tblUserToGroupLink UGL on UGL.idUser = U.idUser '
					+ 'LEFT JOIN tblGroup G on UGL.idGroup = G.idGroup '
					+ 'LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
					END
				
				SET @sql = @sql + 'LEFT JOIN tblLanguage L on L.idLanguage = U.idLanguage '				
				+ 'LEFT JOIN tblUserToSupervisorLink USL ON USL.idUser = U.idUser '
                + 'LEFT JOIN tblUser SUPER ON USL.idSupervisor = SUPER.idUser '
				+ 'LEFT JOIN tblCertificationToUserLink CUL ON CUL.idUser = U.idUser '
				+ 'LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification '
				+ 'LEFT JOIN tblCertificationLanguage CL ON CL.idCertification = C.idCertification AND CL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
				+ 'LEFT JOIN tblCertificationModule CM ON CM.idCertification = C.idCertification '
				+ 'LEFT JOIN tblCertificationModuleLanguage CML ON CML.idCertificationModule = CM.idCertificationModule AND CML.idLanguage = '  + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
				+ 'LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModule = CM.idCertificationModule '
				+ 'LEFT JOIN tblCertificationModuleRequirementSetLanguage CMRSL ON CMRSL.idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet AND CMRSL.idLanguage = '  + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
				+ 'LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet ' 
				+ 'LEFT JOIN tblCertificationModuleRequirementLanguage CMRL ON CMRL.idCertificationModuleRequirement = CMR.idCertificationModuleRequirement AND CMRL.idLanguage = '  + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
				+ 'LEFT JOIN [tblData-CertificationModuleRequirement] DCMR ON DCMR.idCertificationToUserLink = CUL.idCertificationToUserLink AND DCMR.idCertificationModuleRequirement = CMR.idCertificationModuleRequirement AND CMR.requirementType = 2 '
				+ 'LEFT JOIN tblUser APPROVER ON APPROVER.idUser = DCMR.idApprover '
				+ 'WHERE ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) '
				+ ' AND U.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '
				
				IF (CHARINDEX('_idUser =', @whereClauseLOC) <= 0 AND @groupScope IS NOT NULL AND @groupScope <> '')
					BEGIN
					SET @sql = @sql + 'AND UGL.idgroup IN (' + @groupScope + ') '
					END
							
				SET @sql = @sql + ') MAIN'    

	IF @whereClauseLOC IS NOT NULL AND @whereClauseLOC <> ''
		BEGIN
		SET @sql = @sql + ' WHERE ' + @whereClauseLOC
		END

	IF @orderByClauseLOC IS NOT NULL AND @orderByClauseLOC <> ''
		BEGIN
		
		-- for security reasons, we will always pass @orderByClauseLOC with a trailing comma, remove that comma
		-- this will prevent another sql statement from being attached to this query
		SET @orderByClauseLOC = LEFT(@orderByClauseLOC, LEN(@orderByClauseLOC) - 1)

		SET @sql = @sql + ' ORDER BY ' + @orderByClauseLOC

		END

	/*

	execute the sql statement and log its execution

	*/

	DECLARE @dtExecutionBegin DATETIME
	DECLARE @dtExecutionEnd DATETIME
	DECLARE @executionDurationMS INT

	SET @dtExecutionBegin = GETUTCDATE()

	EXEC sp_executesql @sql

	SET @dtExecutionEnd = GETUTCDATE()

	SET @executionDurationMS = DATEDIFF(ms, @dtExecutionBegin, @dtExecutionEnd)

	IF (@logReportExecutionLOC = 1)
	BEGIN
		INSERT INTO tblReportProcessorLog
		(
			idSite,
			idCaller,
			datasetProcedure,
			dtExecutionBegin,
			dtExecutionEnd,
			executionDurationSeconds,
			sqlQuery
		)
		SELECT
			@idCallerSite,
			@idCaller,
			'[Dataset.UserCertificationTranscripts]',
			@dtExecutionBegin,
			@dtExecutionEnd,
			CAST((CAST(@executionDurationMS AS DECIMAL)/1000) AS DECIMAL(9,2)),
			@sql	
	END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	


GO
	
