-- =====================================================================
-- PROCEDURE: [Catalog.RemoveCourses]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.RemoveCourses]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.RemoveCourses]
GO

/*

removes courses from a catalog

*/
	
CREATE PROCEDURE [Catalog.RemoveCourses]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--   default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT, 
	
	@idCatalog					INT,
	@Courses					IDTable			READONLY
)
AS

	BEGIN
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	make sure there are objects in the table parameter
	
	*/
	
	IF (SELECT COUNT(1) FROM @Courses) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CatalogRemoveCourses_NoCoursesToRemove'
		RETURN 1
		END

	/*
	
	make sure the catalog exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCatalog
		WHERE idCatalog = @idCatalog
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN
		SELECT @Return_Code = 1 -- 'details not found'
		SET @Error_Description_Code = 'CatalogRemoveCourses_CatalogNotExists'
		RETURN 1
		END
	
	/*
	
	remove the courses from the catalog
	
	*/
	
	DELETE FROM tblCourseToCatalogLink
	WHERE idCatalog = @idCatalog
	AND EXISTS (
		SELECT 1
		FROM @Courses CC
		WHERE tblCourseToCatalogLink.idCourse = CC.id
	)
	SET @Return_Code = 0
	
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO