SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportSubscription.DeleteByToken]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportSubscription.DeleteByToken]
GO

/*

Deletes a report subscription by its token.
Used when a user unsubscribes from a report via the link in the email.

*/

CREATE PROCEDURE [ReportSubscription.DeleteByToken]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	
	@token						UNIQUEIDENTIFIER
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	do not validate that the caller is a member of the site as this gets called from an unsubscribe page that the user is not logged in for
	
	*/

	/*
	
	do not validate caller permission as this gets called from an unsubscribe page that the user is not logged in for
	
	*/

			
	/*
	
	validate that the item exists
	
	*/
	
	IF (SELECT COUNT(1) FROM tblReportSubscription RS WHERE RS.idUser = @idCaller AND RS.token = @token) = 0
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'ReportSubscriptionDeleteByToken_RecordNotFound'
		RETURN 1 
		END

	/*
	
	delete the report subscription

	*/

	DELETE FROM tblReportSubscription WHERE idUser = @idCaller AND token = @token

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO