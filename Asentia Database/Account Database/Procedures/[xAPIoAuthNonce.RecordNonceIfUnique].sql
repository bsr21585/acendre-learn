-- =====================================================================
-- PROCEDURE: [xAPIoAuthNonce.RecordNonceIfUnique]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[xAPIoAuthNonce.RecordNonceIfUnique]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [xAPIoAuthNonce.RecordNonceIfUnique]
GO

/*

Procedure to save xAPIoAuthNonce data. 

*/

CREATE PROCEDURE [xAPIoAuthNonce.RecordNonceIfUnique]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,
		
	@oAuthKey				NVARCHAR(200),
	@nonce					NVARCHAR(200)
)
AS
	BEGIN
	SET NOCOUNT ON
	DECLARE @idConsumer INT
	
	DECLARE @count INT
	SET @count = 0
	
		SELECT @idConsumer = Consumer.idxAPIoAuthConsumer
		FROM tblxAPIoAuthConsumer Consumer
		WHERE Consumer.oAuthKey = @oAuthKey AND
		Consumer.idSite = @idCallerSite
		
		if (@idConsumer) > 0
		BEGIN
			SELECT @count = COUNT(1) 
			FROM tblxAPIoAuthNonce Nonce
			WHERE @idConsumer = Nonce.idxAPIoAuthConsumer
			AND Nonce.nonce = @nonce
		END
		
		ELSE
		BEGIN
			SET @Return_Code = 1
			SET @Error_Description_Code = 'xAPIoAuthNonceRecordNonceIfUnique_KeyNotFound'
			RETURN
		END
						
		IF (@count) > 0
		BEGIN
			SET @Return_Code = 2 -- a nonce exists (very BAD)
			SET @Error_Description_Code = 'xAPIoAuthNonceRecordNonceIfUnique_DuplicateNonce'
		RETURN
		END
		
		IF (@count) = 0 -- no such nounce exists - store it
		BEGIN
			SET @Return_Code = 0 --(0 is 'success')
			
			INSERT INTO tblxAPIoAuthNonce (idxAPIoAuthConsumer, nonce)
			VALUES (@idConsumer, @nonce)
		RETURN
		END
		
		RETURN
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO