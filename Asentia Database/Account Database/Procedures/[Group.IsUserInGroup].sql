-- =====================================================================
-- PROCEDURE: [Group.IsUserInGroup]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.IsUserInGroup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.IsUserInGroup]
GO

/*
Return all the properties for a given id.
*/
CREATE PROCEDURE [Group.IsUserInGroup]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	
	@idUser						INT,
	@idGroup					INT, 
	@isUserInGroup				BIT = 0		OUTPUT,
	@selfJoined					BIT = 0		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUserToGroupLink
		WHERE idGroup = @idGroup 
		AND   idUser = @idUser		
		AND idSite = @idCallerSite			
		) = 0 
		BEGIN	
		SET @selfJoined		= 0	
		SET @Return_Code	= 0
		SET @isUserInGroup	= 0
		END
	ELSE
		BEGIN
		SET @selfJoined		= (SELECT CASE WHEN COUNT(1) = 0 THEN 0 ELSE 1 END FROM tblUserToGroupLink WHERE selfJoined = 1 AND idGroup = @idGroup AND idUser = @idUser AND idSite = @idCallerSite)
		SET @Return_Code	= 0		
		SET @isUserInGroup	= 1				
		END
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO