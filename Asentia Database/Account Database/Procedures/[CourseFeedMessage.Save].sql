-- =====================================================================
-- PROCEDURE: [CourseFeedMessage.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CourseFeedMessage.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CourseFeedMessage.Save]
GO

/*

Adds new course feed message or updates existing course feed message. 

*/

CREATE PROCEDURE [CourseFeedMessage.Save]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idCourseFeedMessage			INT				OUTPUT,
	@idCourse					INT,
	@idAuthor					INT,
	@idParentCourseFeedMessage	INT,
	@message					NVARCHAR(MAX),
	@isApproved					BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/* 
	
	save the data 
	
	*/

	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	IF (@idCourseFeedMessage = 0 OR @idParentCourseFeedMessage IS NULL)

		BEGIN

		-- insert the course feed message

		INSERT INTO tblCourseFeedMessage (
			idSite,
			idCourse,
			idLanguage,
			idAuthor,
			idParentCourseFeedMessage,
			[message],
			[timestamp],
			isApproved
		)
		VALUES (
			@idCallerSite,
			@idCourse,
			@idLanguage,
			@idCaller,
			@idParentCourseFeedMessage,
			@message,
			GETUTCDATE(),
			@isApproved
		)

		-- get the new course feed message's id and return successfully

		SELECT @idCourseFeedMessage = SCOPE_IDENTITY()

		END

	ELSE

		BEGIN

		-- check that the course feed message id exists
		IF (SELECT COUNT(1) FROM tblCourseFeedMessage WHERE idCourseFeedMessage = @idCourseFeedMessage AND idSite = @idCallerSite) < 1
			BEGIN

			SET @idCourseFeedMessage = @idCourseFeedMessage
			SET @Return_Code = 1 --(1 is 'details not found')
			SET @Error_Description_Code = 'CourseFeedMessageSave_NoRecordFound'
			RETURN 1
			
			END

		-- update the existing course feed message's properties

		UPDATE tblCourseFeedMessage SET
			idSite = @idCallerSite,
			idCourse = @idCourse,
			idLanguage = @idLanguage,
			idAuthor = @idCaller,
			idParentCourseFeedMessage = @idParentCourseFeedMessage,
			[message] = @message,
			[timestamp] = GETUTCDATE(),
			isApproved = @isApproved
		WHERE idCourseFeedMessage = @idCourseFeedMessage

		-- get the course feed message's id 
		SELECT @idCourseFeedMessage = @idCourseFeedMessage

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO