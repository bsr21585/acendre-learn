-- =====================================================================
-- PROCEDURE: [DataLesson.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataLesson.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.Details]
GO


/*

Returns all the properties for a lesson data given id.

*/

CREATE PROCEDURE [DataLesson.Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idDataLesson			INT				OUTPUT,
	@idSite					INT				OUTPUT,
	@idEnrollment			INT				OUTPUT,
	@idLesson				INT				OUTPUT,
	@idTimezone				INT				OUTPUT,
	@title					NVARCHAR(255)	OUTPUT,
	@revcode				NVARCHAR(32)	OUTPUT,
	@order					INT				OUTPUT,
	@dtCompleted			DATETIME		OUTPUT,
	@contentTypeCommittedTo	INT				OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-Lesson]
		WHERE [idData-Lesson] = @idDataLesson
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataLessonDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idDataLesson			= DL.[idData-Lesson],
		@idSite					= DL.idSite,
		@idEnrollment			= DL.idEnrollment,
		@idLesson				= DL.idLesson,
		@idTimezone				= DL.idTimezone,
		@title					= DL.title,
		@revcode				= DL.revcode,
		@order					= DL.[order],
		@dtCompleted			= DL.dtCompleted,
		@contentTypeCommittedTo	= DL.contentTypeCommittedTo
	FROM [tblData-Lesson] DL
	WHERE DL.[idData-Lesson] = @idDataLesson
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataLessonDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO