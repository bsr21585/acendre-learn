-- =====================================================================
-- PROCEDURE: [[OBJECT].Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[[OBJECT].Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [[OBJECT].Details]
GO

/*

Return all the properties for a given [OBJECT] id.

*/

CREATE PROCEDURE [[OBJECT].Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@id[OBJECT]				INT				OUTPUT, 
	-- LIST OBJECT SPECIFIC PARAMS BELOW AND REMOVE THIS COMMENT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
		
	/*
	
	validate caller permission to all specified objects
	
	*/
	
	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tbl[OBJECT]
		WHERE id[OBJECT] = @id[OBJECT]
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0) -- REMOVE THIS WHERE THE OBJECT'S TABLE DOES NOT HAVE A DELETE FLAG
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = '[OBJECT]Details_NoRecordFound'
		RETURN 1
		END
	
	/*
	
	get the data 
	
	*/
	
	SELECT
		-- FIELDS - REMOVE THIS COMMENT
	FROM tbl[OBJECT] _
	WHERE _.id[OBJECT] = @id[OBJECT]
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = '[OBJECT]Details_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO