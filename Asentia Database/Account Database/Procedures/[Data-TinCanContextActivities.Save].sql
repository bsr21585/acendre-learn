-- =====================================================================
-- PROCEDURE: [Data-TinCanContextActivities.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Data-TinCanContextActivities.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Data-TinCanContextActivities.Save]
GO

/*

Adds Tin Can statement(s) context activities.

*/

CREATE PROCEDURE [Data-TinCanContextActivities.Save]
(
	@Return_Code			INT								OUTPUT,
	@Error_Description_Code	NVARCHAR(50)					OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@activities				TinCanContextActivitiesTable	READONLY
)
AS
	BEGIN
	SET NOCOUNT ON

	INSERT INTO [tblData-TinCanContextActivities] (
		[idData-TinCan],
		idSite,
		activityId,
		activityType,
		objectType
	)
	SELECT  (SELECT t.[idData-TinCan]
			FROM [tblData-TinCan] t
			WHERE t.idSite = @idCallerSite
			AND t.statementId = a.statementId),
			@idCallerSite,
			activityId,
			activityType,
			objectType
	FROM	@activities a
	
	SELECT @Return_Code = 0 --Success
	SELECT @Error_Description_Code = NULL
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO