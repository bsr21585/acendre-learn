-- =====================================================================
-- PROCEDURE: [CouponCode.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CouponCode.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CouponCode.Save]
GO

/*

Adds new coupon code or updates existing coupon code.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [CouponCode.Save]
(
	@Return_Code               	INT                        OUTPUT,
	@Error_Description_Code    	NVARCHAR(50)			   OUTPUT,
	@idCallerSite              	INT                        = 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller                   INT                        = 0, -- will fail if not specified

	@idCouponCode              	INT                        OUTPUT,
	@usesAllowed               	INT,
	@discountType              	INT,
	@discount                 	FLOAT,
	@code                     	NVARCHAR(10), 
	@isDeleted                	BIT,
	@comments                 	NVARCHAR(MAX),
	@forCatalog               	INT,
	@forCourse                	INT, 	
	@forLearningPath          	INT,
	@forStandupTraining			INT,
	@dtStart                  	DATETIME,
	@isSingleUsePerUser			BIT, 
	@dtEnd                      DATETIME
   
)
AS   
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
   
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		               
	/*
   
	validate uniqueness for coupon code, if specified
   
	*/
   
	IF (
			SELECT COUNT(1)
			FROM tblCouponCode
			WHERE idSite = @idCallerSite
			AND code = LTRIM(LTRIM(@code))
			AND (isDeleted IS NULL OR isDeleted = 0)
			AND (
					@idCouponCode IS NULL
					OR @idCouponCode <> idCouponCode
				)
			) > 0
			BEGIN
			SET @Return_Code = 2
			SET @Error_Description_Code = 'CouponCodeSave_FieldNotUnique'
			RETURN 1 
			END    		      
		                
	/*
   
	save the data
   
	*/
   
	IF (@idCouponCode = 0 OR @idCouponCode IS NULL)
          
		BEGIN
          
		-- insert the new coupon code
          
		INSERT INTO tblCouponCode (
			idSite,
			usesAllowed,
			discountType,
			discount,
			code,
			isDeleted,
			comments,
			forCourse,
			forCatalog,
			forLearningPath,
			forStandupTraining,
			dtStart,
			isSingleUsePerUser,
			dtEnd			
		)                    
		VALUES (
			@idCallerSite, 
			@usesAllowed,
			@discountType,
			@discount,
			@code,
			@isDeleted,
			@comments,
			@forCourse,
			@forCatalog,
			@forLearningPath,
			@forStandupTraining,
			@dtStart,
			@isSingleUsePerUser,
			@dtEnd
		)
          
		-- get the new coupon code's id return successfully
          
		SELECT @idCouponCode = SCOPE_IDENTITY()
          
		END
          
	ELSE
          
		BEGIN

		-- check that the coupon code exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblCouponCode WHERE idCouponCode = @idCouponCode AND idSite = @idCallerSite AND (isDeleted IS NULL OR isDeleted = 0)) < 1
				BEGIN                 
				SET @idCouponCode = @idCouponCode
				SET @Return_Code = 1
				SET @Error_Description_Code = 'CouponCodeSave_FieldNotUnique'
				RETURN 1
				END

		-- remove coupon code object links when the object's tie to coupon code is not "listed below"

		IF (@forCourse <> 3 AND (SELECT COUNT(1) from tblCouponCodeToCourseLink where idCouponCode = @idCouponCode) > 0)
			BEGIN 
			DELETE FROM tblCouponCodeToCourseLink WHERE idCouponCode = @idCouponCode
			END

		IF (@forCatalog <> 3 AND (SELECT COUNT(1) from tblCouponCodeToCatalogLink where idCouponCode = @idCouponCode) > 0)
			BEGIN 
			DELETE FROM tblCouponCodeToCatalogLink WHERE idCouponCode = @idCouponCode
			END

		IF (@forLearningPath <> 3 AND (SELECT COUNT(1) from tblCouponCodeToLearningPathLink where idCouponCode = @idCouponCode) > 0)
			BEGIN 
			DELETE FROM tblCouponCodeToLearningPathLink WHERE idCouponCode = @idCouponCode
			END

		IF (@forStandupTraining <> 3 AND (SELECT COUNT(1) from tblCouponCodeToStandupTrainingLink where idCouponCode = @idCouponCode) > 0)
			BEGIN 
			DELETE FROM tblCouponCodeToStandupTrainingLink WHERE idCouponCode = @idCouponCode
			END

		-- update existing coupon code's properties
          
		UPDATE tblCouponCode SET
				usesAllowed = @usesAllowed,
				discountType = @discountType,
				discount = @discount,
				code = @code,
				isDeleted = @isDeleted,
				comments = @comments,
				forCourse = @forCourse,
				forCatalog = @forCatalog,
				forLearningPath = @forLearningPath,
				forStandupTraining = @forStandupTraining,
				dtStart = @dtStart,
				isSingleUsePerUser = @isSingleUsePerUser,
				dtEnd = @dtEnd                 
		WHERE idCouponCode = @idCouponCode
		AND idSite = @idCallerSite
          
		-- get the couponcode's id 
		SELECT @idCouponCode = @idCouponCode
                       
	END
          
	SELECT @Error_Description_Code = ''
	SELECT @Return_Code = 0
          
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO