-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.SaveInstructors]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.SaveInstructors]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.SaveInstructors]
GO

/*

Saves instructors for a standup training instance

*/

CREATE PROCEDURE [StandupTrainingInstance.SaveInstructors]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT,
	@Instructors				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that all instructors exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Instructors II
		LEFT JOIN tblUser U ON U.idUser = II.id
		WHERE U.idSite IS NULL
		OR U.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'STISaveInstructors_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	validate that the standup training instance belongs to the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblStandupTrainingInstance STI
		WHERE STI.idStandupTrainingInstance = @idStandupTrainingInstance
		AND (STI.idSite IS NULL OR STI.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'STISaveInstructors_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	get exisitng instuctors for this session
	
	*/
		
	DECLARE @existingInstructors TABLE (idInstructor INT)

	INSERT INTO @existingInstructors (
		idInstructor
	)
	SELECT 
		idInstructor
	FROM tblStandupTrainingInstanceToInstructorLink STII
	WHERE STII.idStandupTrainingInstance = @idStandupTrainingInstance

	/*
	
	remove instructors where they do not exist in the Instructors table variable
	
	*/
	
	DELETE FROM tblStandupTrainingInstanceToInstructorLink WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND idInstructor NOT IN (SELECT id FROM @Instructors)
	
	/*

	insert the instructors into the linking table where they do not already exist

	*/

	INSERT INTO tblStandupTrainingInstanceToInstructorLink (
		idSite,
		idStandupTrainingInstance,
		idInstructor
	)
	SELECT
		@idCallerSite,
		@idStandupTrainingInstance,
		II.id
	FROM @Instructors II
	WHERE NOT EXISTS (SELECT 1 
					  FROM tblStandupTrainingInstanceToInstructorLink
					  WHERE idStandupTrainingInstance = @idStandupTrainingInstance
					  AND idInstructor = II.id)

	/*

	do instructor event log events

	*/

	DECLARE @idStandUpTraining INT
	DECLARE @utcNow DATETIME

	SELECT @idStandUpTraining = idStandUpTraining FROM tblStandUpTrainingInstance WHERE idStandUpTrainingInstance = @idStandupTrainingInstance
	SET @utcNow = GETUTCDATE()

	-- instructor removed events
	DECLARE @instructorRemovedEventLogItems EventLogItemObjects
	
	INSERT INTO @instructorRemovedEventLogItems (
		idSite, 
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite, 
		@idStandUpTraining, 
		@idStandupTrainingInstance,
		EI.idInstructor
	FROM @existingInstructors EI
	WHERE EI.idInstructor NOT IN (SELECT id 
								  FROM @Instructors)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 403, @utcNow, @instructorRemovedEventLogItems

	-- instructor added events
	DECLARE @instructorAddedChangedEventLogItems EventLogItemObjects

	INSERT INTO @instructorAddedChangedEventLogItems (
		idSite, 
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite, 
		@idStandupTraining, 
		@idStandupTrainingInstance,
		I.id
	FROM @Instructors I
	WHERE I.id NOT IN (SELECT idInstructor
					   FROM @existingInstructors)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 402, @utcNow, @instructorAddedChangedEventLogItems

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO