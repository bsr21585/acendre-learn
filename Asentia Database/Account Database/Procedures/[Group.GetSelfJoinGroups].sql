-- =====================================================================
-- PROCEDURE: [Group.GetSelfJoinGroups]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Group.GetSelfJoinGroups]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.GetSelfJoinGroups]
GO

/*

Return all the properties for a given group id.

*/
CREATE PROCEDURE [Group.GetSelfJoinGroups]
(
	@Return_Code			INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)		OUTPUT,
	@idCallerSite			INT					= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT	
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	get the data
	
	*/
	
	SELECT	G.idGroup,
			G.name,
			G.idSite,			
			G.avatar,
			G.shortDescription,
			G.longDescription
	FROM tblGroup G	
	WHERE	G.isSelfJoinAllowed	IS NOT NULL
	AND		G.isSelfJoinAllowed	= 1
	AND		G.idSite		= @idCallerSite

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO