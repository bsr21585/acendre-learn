-- =====================================================================
-- PROCEDURE: [CertificationModuleRequirement.SaveRequirementCourses]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CertificationModuleRequirement.SaveRequirementCourses]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) 
	DROP PROCEDURE [CertificationModuleRequirement.SaveRequirementCourses]
GO

/*

Saves course information for course requirements.

*/

CREATE PROCEDURE [CertificationModuleRequirement.SaveRequirementCourses]
(
	@Return_Code						INT						OUTPUT,
	@Error_Description_Code				NVARCHAR(50)			OUTPUT,
	@idCallerSite						INT						= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT						= 0,
	
	@idCertificationModuleRequirement	INT,
	@CertificationRequirementCourses	IDTableWithOrdering		READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END	

	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificationModuleRequirement
		WHERE idCertificationModuleRequirement = @idCertificationModuleRequirement
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CMRSaveRequirementCourses_NoRecordFound'
		RETURN 1
	    END

	/* 

	delete requirement courses that arent in the requirement courses param

	*/

	DELETE FROM tblCertificationModuleRequirementToCourseLink 
	WHERE idCertificationModuleRequirement = @idCertificationModuleRequirement
	AND NOT EXISTS (SELECT 1 FROM @CertificationRequirementCourses CRC
					WHERE CRC.id = tblCertificationModuleRequirementToCourseLink.idCourse)

	/*

	update existing attached courses

	*/

	UPDATE tblCertificationModuleRequirementToCourseLink SET
		[order] = CRC.[order]
	FROM @CertificationRequirementCourses CRC
	WHERE tblCertificationModuleRequirementToCourseLink.idCertificationModuleRequirement = @idCertificationModuleRequirement
	AND CRC.id = tblCertificationModuleRequirementToCourseLink.idCourse
	
	/*

	insert new attached courses

	*/

	INSERT INTO tblCertificationModuleRequirementToCourseLink (
		idSite,
		idCertificationModuleRequirement,
		idCourse,
		[order]
	)
	SELECT
		@idCallerSite,
		@idCertificationModuleRequirement,
		CRC.id,
		CrC.[order]
	FROM @CertificationRequirementCourses CRC
	WHERE NOT EXISTS (SELECT 1 FROM tblCertificationModuleRequirementToCourseLink CMRCL
					  WHERE CMRCL.idCertificationModuleRequirement = @idCertificationModuleRequirement
					  AND CMRCL.idCourse = CRC.id)

	/*

	RETURN

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO