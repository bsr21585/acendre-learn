-- =====================================================================
-- PROCEDURE: [StandupTraining.GetFutureSessions]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.GetFutureSessions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.GetFutureSessions]
GO

CREATE PROCEDURE [dbo].[StandupTraining.GetFutureSessions]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idStandupTraining		INT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	for finding the record which are of future

	*/
		 DECLARE @temp TABLE(
                	idStandUpTrainingInstance INT,
					dtStaart DATETIME
							)
							INSERT INTO @temp SELECT idStandUpTrainingInstance,MIN(dtStart)
							FROM tblStandUpTrainingInstanceMeetingTime GROUP BY idStandUpTrainingInstance
	                      
	
       SELECT
			DISTINCT
			S.idStandUpTrainingInstance AS idSession,
			T.dtStaart,
			(S.locationDescription+' ' + S.city) AS Location,
			CASE WHEN SL.title IS NOT NULL THEN SL.title ELSE S.title END AS title,
			CASE WHEN (S.seats - (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink where idStandUpTrainingInstance = S.idStandUpTrainingInstance)) > 0 THEN 1 ELSE 0 END AS isSeatAvailable,
			CONVERT(NVARCHAR(50),(S.seats -  (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink where idStandUpTrainingInstance = S.idStandUpTrainingInstance)))+'/'+ 
			CONVERT(NVARCHAR(50),S.seats) AS [Status]
		FROM tblStandUpTrainingInstance S
		LEFT JOIN tblStandUpTraining ST ON ST.idStandUpTraining = S.idStandUpTraining
		LEFT JOIN tblStandUpTrainingInstanceLanguage SL ON SL.idStandUpTrainingInstance = S.idStandUpTrainingInstance AND SL.idLanguage = @idCallerLanguage
		LEFT JOIN @temp T ON T.idStandUpTrainingInstance = S.idStandUpTrainingInstance 
		WHERE S.idSite = @idCallerSite
		AND (S.isDeleted IS NULL OR S.isDeleted = 0)
		AND S.idStandUpTraining = @idStandupTraining
		AND S.seats - (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink where idStandUpTrainingInstance = S.idStandUpTrainingInstance) > 0
		AND ST.isStandaloneEnroll = 1
		AND T.dtStaart > GETUTCDATE()
		ORDER BY T.dtStaart ASC
	

		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
