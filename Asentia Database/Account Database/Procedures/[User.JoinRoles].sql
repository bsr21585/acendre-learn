-- =====================================================================
-- PROCEDURE: [User.JoinRoles]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[User.JoinRoles]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.JoinRoles]
GO

/*

Joins a user to one or more roles.

*/

CREATE PROCEDURE [User.JoinRoles]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idUser					INT,
	@Roles					IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	make sure there are objects in the table parameter
	
	*/
	
	IF (SELECT COUNT(1) FROM @Roles) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'UserJoinRoles_NoRolesFound'
		RETURN 1
		END
		
	/*
	
	validate that the user exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUser
		WHERE idSite = @idCallerSite
		AND @idUser = idUser
		) <> 1
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'UserJoinRoles_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	validate that all the roles exist in the same site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Roles RR
		LEFT JOIN tblRole R ON R.idRole = RR.id
		WHERE R.idRole IS NULL -- role does not exist at all
		OR (R.idSite <> @idCallerSite AND R.idSite <> 1) -- role does not exist within the same site and is not built-in
		) > 0
		BEGIN
		SELECT @Return_Code = 3
		SET @Error_Description_Code = 'UserJoinRoles_PermissionError'
		RETURN 1 
		END
	
	/*
	
	attach the user to roles where they do not already exist
	
	*/
	
	INSERT INTO tblUserToRoleLink (
		idSite,
		idUser, 
		idRole,
		created
		)
	SELECT 
		@idCallerSite,
		@idUser, 
		R.id,
		GETUTCDATE()
	FROM @Roles R
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblUserToRoleLink URL
		WHERE URL.idUser = @idUser
		AND URL.idRole = R.id
		AND URL.idRuleSet IS NULL -- manual join
	)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

