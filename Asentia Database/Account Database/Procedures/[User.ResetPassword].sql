-- =====================================================================
-- PROCEDURE: [User.ResetPassword]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.ResetPassword]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.ResetPassword]
GO

/*

Resets the user's password.

*/
CREATE PROCEDURE [User.ResetPassword]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@userFirstName			NVARCHAR(255)	OUTPUT,
	@userLastName			NVARCHAR(255)	OUTPUT,
	@userEmail				NVARCHAR(255)	OUTPUT,
	@userCulture			NVARCHAR(20)	OUTPUT,
	
	@username				NVARCHAR(512),
	@idSite					INT,
	@newPassword			NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @count INT
	SET @count = 0
	
	DECLARE @idUser INT
	
	/*
	
	administrator account cannot be reset
	
	*/
	
	IF @username = 'admin' OR @username = 'administrator'
		BEGIN
		SET @Return_Code = 4 -- failed login
		SET @Error_Description_Code = 'UserResetPassword_CannotResetAdmin'
		RETURN 1
		END
		
	/*
	
	reset user's password
	
	*/
		
	SELECT @count = COUNT(1)
	FROM tblUser U
	WHERE U.username = @username
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
	AND U.idSite = @idSite
	
	IF (@count) = 0 
		BEGIN
		SET @Return_Code = 1 -- not found
		SET @Error_Description_Code = 'UserResetPassword_UsernameNotFound'
		END
		
	IF (@count) > 1
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'UserResetPassword_DuplicateAccount'
		RETURN 1
		END
		
	IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idUser = U.idUser,
			@userFirstName = U.firstName,
			@userLastName = U.lastName,
			@userEmail = U.email
			-- TODO: userCulture
		FROM tblUser U
		WHERE U.username = @username
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
		AND U.idSite = @idSite
		
		IF (@userEmail IS NULL)
			BEGIN
			SET @Return_Code = 1 -- no email address
			SET @Error_Description_Code = 'UserResetPassword_NoEmailAddress'
			RETURN 1	
			END
	
		-- block the trigger from executing in tblUser UPDATE
		DECLARE @CONTEXT VARBINARY(128)

		SET @CONTEXT = CONVERT(VARBINARY(128), 'BLOCK TRIGGER')
		SET CONTEXT_INFO @CONTEXT

		-- reset the password
		UPDATE tblUser SET
			[password] = dbo.GetHashedString('SHA1', @newPassword)
		WHERE idUser = @idUser
		AND idSite = @idSite

		-- unblock the trigger
		SET @CONTEXT = CONVERT(VARBINARY(128), '')
		SET CONTEXT_INFO @CONTEXT
		
		SET @Return_Code = 0 --(0 is 'success')
		SET @Error_Description_Code = ''
		RETURN 1
		END
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO