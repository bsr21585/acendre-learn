-- =====================================================================
-- PROCEDURE: [InboxMessage.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[InboxMessage.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [InboxMessage.Details]
GO

/*
Return all the properties for a given id.
*/
CREATE PROCEDURE [InboxMessage.Details]
(
	@Return_Code				INT							OUTPUT,
	@Error_Description_Code		NVARCHAR(50)				OUTPUT,
	@idCallerSite				INT				= 0, 
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,
	
	@idInboxMessage				INT,
	@type						NVARCHAR(5)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	validate that the message exists 
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblInboxMessage
		WHERE idInboxMessage = @idInboxMessage
		) = 0 

		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'InboxMessageDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
	SELECT
		IM.idInboxMessage,
		IM.idRecipient, 
		IM.idSender, 
		IM.[subject],
		IM.[message],
		IM.[dtCreated],
		case when IM.idSender = 1 then 'Administrator' +' ('+ + 'admin' +')' else U.displayName + ' (' + U.username + ')' end AS [from],
		case when IM.idRecipient = 1 then 'Administrator' +' ('+ + 'admin' +')' else R.displayName + ' (' + R.username + ')' end AS [to],
		IM.[idParentInboxMessage]
	FROM tblInboxMessage IM
	INNER JOIN tblUser U ON U.idUser = IM.idSender
	INNER JOIN tblUser R ON R.idUser = IM.idRecipient
	WHERE IM.idInboxMessage = @idInboxMessage 
		  AND
		  (
			  ( --if its Sent of Draft Message Check 
				(@type='Draft' OR @type='Sent') AND (
								(
									IM.idRecipient = @idCaller
									AND
									IM.isRecipientDeleted IS NULL
								)
								OR
								(
									IM.idSender = @idCaller
									AND
									IM.isSenderDeleted IS NULL
								)
							)
				AND ((IM.isDraft = 1 AND @type = 'Draft') OR((IM.isDraft = 0 OR IM.isDraft IS NULL) AND @type = 'Sent'))
				AND (((IM.isSent = 0 OR IM.isSent IS NULL) AND @type='Draft') OR (IM.isSent = 1 AND @type = 'Sent'))
			)
			OR
			(
				@type='Inbox' AND isSent = 1 AND idRecipient = @idCaller
			)
		)
		AND IM.idSite = @idCallerSite
	
	IF(@type='inbox')
	BEGIN
		UPDATE tblInboxMessage SET
			  isRead = 1,
			  dtRead = GETUTCDATE()
		WHERE (idInboxMessage = @idInboxMessage OR idParentInboxMessage =  idInboxMessage OR idParentInboxMessage = @idInboxMessage) 
		AND isSent = 1
		AND (isRead = 0 OR isRead IS NULL)
		AND idRecipient = @idCaller
		AND idSite = @idCallerSite
	END

	IF @@ROWCOUNT = 0
		SELECT @Return_Code = 1,@Error_Description_Code = 'InboxMessageDetails_NoRecordFound'
	ELSE
		SELECT @Return_Code = 0	
	END