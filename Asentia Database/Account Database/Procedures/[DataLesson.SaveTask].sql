-- =====================================================================
-- PROCEDURE: [DataLesson.SaveTask]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DataLesson.SaveTask]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataLesson.SaveTask]
GO

/*

Saves task information for a learner's task.
Used when a learner uploads their task.

*/

CREATE PROCEDURE [DataLesson.SaveTask]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT				= 0,
	
	@idDataLesson					INT,
	@uploadedTaskFilename			NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that the lesson data exists and is not completed

	*/

	IF (
		SELECT COUNT(1)
		FROM [tblData-Lesson]
		WHERE [idData-Lesson] = @idDataLesson
		AND idSite = @idCallerSite
		AND dtCompleted IS NULL
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataLessonSaveTask_NoRecordFound'
		RETURN 1
	    END

	/*

	update the task data if it exists, otherwise insert it

	*/

	IF (SELECT COUNT(1) FROM [tblData-HomeworkAssignment] WHERE [idData-Lesson] = @idDataLesson) > 0

		BEGIN
		UPDATE [tblData-HomeworkAssignment] SET
			uploadedAssignmentFilename = @uploadedTaskFilename,
			dtUploaded = GETUTCDATE(),
			completionStatus = null, -- also reset completionStatus, successStatus, score, timestamp, and proctoringUser
			successStatus = null,
			score = null,
			[timestamp] = null,
			proctoringUser = null
		WHERE [idData-Lesson] = @idDataLesson
		END

	ELSE

		BEGIN
		INSERT INTO [tblData-HomeworkAssignment] (
			idSite,
			[idData-Lesson],
			uploadedAssignmentFilename,
			dtUploaded
		)
		VALUES (
			@idCallerSite,
			@idDataLesson,
			@uploadedTaskFilename,
			GETUTCDATE()
		)
		END
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

