-- =====================================================================
-- PROCEDURE: [GroupEnrollment.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GroupEnrollment.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GroupEnrollment.GetGrid]
GO

/*

Gets a listing of group enrollments for a specified group. 

*/

CREATE PROCEDURE [GroupEnrollment.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,
	
	@idGroup				INT
)
AS
	BEGIN
		SET NOCOUNT ON

		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
		
		/*

		begin getting the grid data

		*/

			IF @searchParam IS NULL
	
			BEGIN

				-- return the rowcount
	
				SELECT COUNT(1) AS row_count 
		FROM tblGroupEnrollment GE
		WHERE
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND GE.idSite = @idCallerSite)
			)
		AND GE.idGroup = @idGroup
		
				;WITH 
			Keys AS (
				SELECT TOP (@pageNum * @pageSize) 
					GE.idGroupEnrollment,
					ROW_NUMBER() OVER (ORDER BY 
						-- FIRST ORDER DESC
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN GE.dtStart END) END DESC,
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn <> 'dtStart' THEN C.title END) END DESC,
						-- SECOND ORDER DESC
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN C.title END) END DESC,
						
						-- FIRST ORDER ASC
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN GE.dtStart END) END,
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn <> 'dtStart' THEN C.title END) END,
						-- SECOND ORDER ASC
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN C.title END) END
					)
					AS [row_number]
				FROM tblGroupEnrollment GE 
				LEFT JOIN tblCourse C ON GE.idCourse = C.idCourse
				WHERE
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND GE.idSite = @idCallerSite)
					)
				AND GE.idGroup = @idGroup
			), 
			
			SelectedKeys AS (
				SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
					idGroupEnrollment, 
					[row_number]
				FROM Keys
				WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
			)
		SELECT
			GE.idGroupEnrollment,
			GE.idCourse,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title,
			GE.dtStart,
			GE.isLockedByPrerequisites AS togglePrerequisites,
			convert(bit, 1) AS isModifyOn,
			SelectedKeys.[row_number]
		FROM SelectedKeys
		JOIN tblGroupEnrollment GE ON GE.idGroupEnrollment = SelectedKeys.idGroupEnrollment
		LEFT JOIN tblCourse C ON C.idCourse = GE.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		WHERE
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND GE.idSite = @idCallerSite)
			)
		AND GE.idGroup = @idGroup		
		ORDER BY SelectedKeys.[row_number]

		     END
		ELSE
		     BEGIN

			   -- return the rowcount
	
				SELECT COUNT(1) AS row_count 
		FROM tblGroupEnrollment GE
		LEFT JOIN tblCourse C ON GE.idCourse = C.idCourse
		INNER JOIN CONTAINSTABLE(tblCourse, *, @searchParam) K ON K.[key] = C.idCourse
		WHERE
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND GE.idSite = @idCallerSite)
			)
		AND GE.idGroup = @idGroup
		
				;WITH 
			Keys AS (
				SELECT TOP (@pageNum * @pageSize) 
					GE.idGroupEnrollment,
					ROW_NUMBER() OVER (ORDER BY 
						-- FIRST ORDER DESC
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN GE.dtStart END) END DESC,
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn <> 'dtStart' THEN C.title END) END DESC,
						-- SECOND ORDER DESC
						CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtStart' THEN C.title END) END DESC,
						
						-- FIRST ORDER ASC
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN GE.dtStart END) END,
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn <> 'dtStart' THEN C.title END) END,
						-- SECOND ORDER ASC
						CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtStart' THEN C.title END) END
					)
					AS [row_number]
				FROM tblGroupEnrollment GE 
				LEFT JOIN tblCourse C ON GE.idCourse = C.idCourse
				INNER JOIN CONTAINSTABLE(tblCourse, *, @searchParam) K ON K.[key] = C.idCourse
				WHERE
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND GE.idSite = @idCallerSite)
					)
				AND GE.idGroup = @idGroup
			), 
			
			SelectedKeys AS (
				SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
					idGroupEnrollment, 
					[row_number]
				FROM Keys
				WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
			)
		SELECT
			GE.idGroupEnrollment,
			GE.idCourse,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title,
			GE.dtStart,
			GE.isLockedByPrerequisites AS togglePrerequisites,
			convert(bit, 1) AS isModifyOn,
			SelectedKeys.[row_number]
		FROM SelectedKeys
		JOIN tblGroupEnrollment GE ON GE.idGroupEnrollment = SelectedKeys.idGroupEnrollment
		LEFT JOIN tblCourse C ON C.idCourse = GE.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		WHERE
			(
			(@idCallerSite IS NULL) 
			OR 
			(@idCallerSite IS NOT NULL AND GE.idSite = @idCallerSite)
			)
		AND GE.idGroup = @idGroup		
		ORDER BY SelectedKeys.[row_number]


			 END
		
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO