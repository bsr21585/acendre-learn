-- =====================================================================
-- PROCEDURE: [DataSCO.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataSCO.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataSCO.Details]
GO


/*

Returns all the properties for a lesson data given id.

*/

CREATE PROCEDURE [DataSCO.Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idDataSCO				INT				OUTPUT,
	@idSite					INT				OUTPUT,
	@idDataLesson			INT				OUTPUT,
	@idTimezone				INT				OUTPUT,
	@manifestIdentifier		NVARCHAR(255)	OUTPUT,
	@completionStatus		INT				OUTPUT,
	@successStatus			INT				OUTPUT,
	@scoreScaled			FLOAT			OUTPUT,
	@totalTime				FLOAT			OUTPUT,
	@timestamp				DATETIME		OUTPUT,
	@actualAttemptCount		INT				OUTPUT,
	@effectiveAttemptCount	INT				OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM [tblData-SCO]
		WHERE [idData-SCO] = @idDataSCO
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataSCODetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idDataSCO				=DS.[idData-SCO]		,
		@idSite					=DS.idSite,
		@idDataLesson			=DS.[idData-Lesson],
		@idTimezone				=DS.idTimezone,
		@manifestIdentifier		=DS.manifestIdentifier,
		@completionStatus		=DS.completionStatus,
		@successStatus			=DS.successStatus,			
		@scoreScaled			=DS.scoreScaled,
		@totalTime				=DS.totalTime,
		@timestamp				=DS.[timestamp],
		@actualAttemptCount		=DS.actualAttemptCount,
		@effectiveAttemptCount	=DS.effectiveAttemptCount
	FROM [tblData-SCO] DS
	WHERE DS.[idData-SCO] = @idDataSCO
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataSCODetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO