-- =====================================================================
-- PROCEDURE: [RuleSetEnrollment.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetEnrollment.GetGrid]
GO

/*

Gets a listing of ruleset enrollments.

*/

CREATE PROCEDURE [RuleSetEnrollment.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,

	@idCourse				INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END
		
		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
		
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblRuleSetEnrollment RSE
			WHERE 
				(
					(@idCallerSite IS NULL)
					OR 
					(@idCallerSite IS NOT NULL AND RSE.idSite = @idCallerSite)
				)
			AND RSE.idCourse = @idCourse

			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						RSE.idRuleSetEnrollment,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN RSE.[priority] END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RSE.[priority] END
						)
						AS [row_number]
					FROM tblRuleSetEnrollment RSE
					WHERE 
						(
							(@idCallerSite IS NULL)
							OR 
							(@idCallerSite IS NOT NULL AND RSE.idSite = @idCallerSite)
						)
					AND RSE.idCourse = @idCourse
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idRuleSetEnrollment, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				RSE.idRuleSetEnrollment,
				CASE WHEN RSEL.label IS NOT NULL THEN RSEL.label ELSE RSE.label END AS label,
				RSE.[priority],
				CASE WHEN RSE.isFixedDate = 1 THEN
					CASE WHEN RSE.recurInterval IS NOT NULL AND RSE.recurInterval > 0 
						THEN 3
					ELSE 1 END
				ELSE
					CASE WHEN RSE.recurInterval IS NOT NULL AND RSE.recurInterval > 0
						THEN 4
					ELSE 2 END
				END AS enrollmentType,				
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = SelectedKeys.idRuleSetEnrollment
			LEFT JOIN tblRuleSetEnrollmentLanguage RSEL ON RSEL.idRuleSetEnrollment = RSE.idRuleSetEnrollment AND RSEL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
			
			END
		
		ELSE
		
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblRuleSetEnrollment RSE
				INNER JOIN CONTAINSTABLE(tblRuleSetEnrollment, *, @searchParam) K ON K.[key] = RSE.idRuleSetEnrollment
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND RSE.idSite = @idCallerSite)
					)
				AND RSE.idCourse = @idCourse
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							RSE.idRuleSetEnrollment,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN RSE.[priority] END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RSE.[priority] END
							)
							AS [row_number]
						FROM tblRuleSetEnrollment RSE
						INNER JOIN CONTAINSTABLE(tblRuleSetEnrollment, *, @searchParam) K ON K.[key] = RSE.idRuleSetEnrollment
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND RSE.idSite = @idCallerSite
							)
						AND RSE.idCourse = @idCourse
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idRuleSetEnrollment, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					RSE.idRuleSetEnrollment,
					RSE.label AS label,
					RSE.[priority],
					CASE WHEN RSE.isFixedDate = 1 THEN
						CASE WHEN RSE.recurInterval IS NOT NULL AND RSE.recurInterval > 0 
							THEN 3
						ELSE 1 END
					ELSE
						CASE WHEN RSE.recurInterval IS NOT NULL AND RSE.recurInterval > 0
							THEN 4
						ELSE 2 END
					END AS enrollmentType,					
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = SelectedKeys.idRuleSetEnrollment
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblRuleSetEnrollmentLanguage RSEL
				LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
				INNER JOIN CONTAINSTABLE(tblRuleSetEnrollmentLanguage, *, @searchParam) K ON K.[key] = RSEL.idRuleSetEnrollmentLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND RSE.idSite = @idCallerSite)
					)
				AND RSEL.idLanguage = @idCallerLanguage
				AND RSE.idCourse = @idCourse
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							RSEL.idRuleSetEnrollmentLanguage,
							RSE.idRuleSetEnrollment,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN RSE.[priority] END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RSE.[priority] END
							)
							AS [row_number]
						FROM tblRuleSetEnrollmentLanguage RSEL
						LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
						INNER JOIN CONTAINSTABLE(tblRuleSetEnrollmentLanguage, *, @searchParam) K ON K.[key] = RSEL.idRuleSetEnrollmentLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND RSE.idSite = @idCallerSite
							)
						AND RSEL.idLanguage = @idCallerLanguage
						AND RSE.idCourse = @idCourse
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idRuleSetEnrollment, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					RSE.idRuleSetEnrollment, 
					RSEL.label AS label,
					RSE.[priority],
					CASE WHEN RSE.isFixedDate = 1 THEN
						CASE WHEN RSE.recurInterval IS NOT NULL AND RSE.recurInterval > 0 
							THEN 3
						ELSE 1 END
					ELSE
						CASE WHEN RSE.recurInterval IS NOT NULL AND RSE.recurInterval > 0
							THEN 4
						ELSE 2 END
					END AS enrollmentType,
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblRuleSetEnrollmentLanguage RSEL ON RSEL.idRuleSetEnrollment = SelectedKeys.idRuleSetEnrollment AND RSEL.idLanguage = @idCallerLanguage
				LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSEL.idRuleSetEnrollment
				ORDER BY SelectedKeys.[row_number]

				END
			
			END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO