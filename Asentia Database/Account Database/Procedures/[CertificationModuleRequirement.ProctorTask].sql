-- =====================================================================
-- PROCEDURE: [CertificationModuleRequirement.ProctorTask]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[CertificationModuleRequirement.ProctorTask]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModuleRequirement.ProctorTask]
GO

/*

Updates completion, success, and score for a learner's task.
Used when a proctor grades a learner's task.

*/

CREATE PROCEDURE [CertificationModuleRequirement.ProctorTask]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0,
	
	@idCertificationToUserLink				INT,
	@idDataCertificationModuleRequirement	INT,
	@approve								BIT	
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that the certification requirement data exists and is not completed

	*/

	IF (
		SELECT COUNT(1)
		FROM [tblData-CertificationModuleRequirement]
		WHERE [idData-CertificationModuleRequirement] = @idDataCertificationModuleRequirement
		AND idSite = @idCallerSite
		AND dtCompleted IS NULL
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataCMRPT_NoCRDataRecordFound'
		RETURN 1
	    END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	update the task data

	*/

	UPDATE [tblData-CertificationModuleRequirement] SET
		dtCompleted = CASE WHEN @approve = 1 THEN @utcNow ELSE NULL END,
		isApproved = @approve,
		dtApproved = CASE WHEN @approve = 1 THEN @utcNow ELSE NULL END,
		idApprover = @idCaller		
	WHERE [idData-CertificationModuleRequirement] = @idDataCertificationModuleRequirement	

	/*

	check for certification completion

	*/

	DECLARE @idUser INT
	
	SELECT 
		@idUser = idUser 
	FROM [tblData-CertificationModuleRequirement] DCMR
	LEFT JOIN tblCertificationToUserLink CUL ON CUL.idCertificationToUserLink = DCMR.idCertificationToUserLink
	WHERE DCMR.[idData-CertificationModuleRequirement] = @idDataCertificationModuleRequirement

	EXEC [Certification.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idCertificationToUserLink, @idUser

	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

