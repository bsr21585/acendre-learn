-- =====================================================================
-- PROCEDURE: [QuizSurvey.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[QuizSurvey.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [QuizSurvey.Delete]
GO

/*

Deletes Quiz(zes)/Survey(s)

*/
CREATE PROCEDURE [QuizSurvey.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@QuizzesSurveys			IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @QuizzesSurveys) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'QuizSurveyDelete_NoRecordsSelected'
		RETURN 1
		END
	
	/*
	
	validate that all quizzes/surveys exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @QuizzesSurveys QSQS
		LEFT JOIN tblQuizSurvey QS ON QS.idQuizSurvey = QSQS.id 
		WHERE QS.idSite IS NULL
		OR QS.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'QuizSurveyDelete_DetailsNotFound'
		RETURN 1 
		END

	/*

	REMOVE the links to quizzes/surveys from content package table
	this effectively makes the package that was generated from the quiz/survey
	just a standard scorm package

	*/

	UPDATE tblContentPackage SET 
		idQuizSurvey = NULL 
	WHERE idSite = @idCallerSite
	AND idQuizSurvey IN (
		SELECT QSQS.id
		FROM @QuizzesSurveys QSQS
	)

	/*
	
	DELETE the quizzes/surveys
	
	*/
	
	DELETE FROM tblQuizSurvey
	WHERE idQuizSurvey IN (
		SELECT QSQS.id
		FROM @QuizzesSurveys QSQS
	)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO