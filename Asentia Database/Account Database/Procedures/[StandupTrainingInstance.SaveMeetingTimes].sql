-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.SaveMeetingTimes]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.SaveMeetingTimes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.SaveMeetingTimes]
GO

/*

Saves meeting times for a standup training instance

*/

CREATE PROCEDURE [StandupTrainingInstance.SaveMeetingTimes]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT,
	@MeetingTimes				DateRange		READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate that the standup training instance belongs to the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblStandupTrainingInstance STI
		WHERE STI.idStandupTrainingInstance = @idStandupTrainingInstance
		AND (STI.idSite IS NULL OR STI.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'STISaveMeetingTimes_DetailsNotFound'
		RETURN 1 
		END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	determine if we're going to write a time changed event

	*/

	DECLARE @writeTimeChangeEvent BIT
	SET @writeTimeChangeEvent = 0

	DECLARE @ExistingMeetingTimes DateRange
	DECLARE @MeetingTimesDifferences DateRange

	-- get the existing meeting times
	INSERT INTO @ExistingMeetingTimes (
		dtStart,
		dtEnd,
		idTimezone
	)
	SELECT
		dtStart,
		dtEnd,
		idTimezone
	FROM tblStandUpTrainingInstanceMeetingTime
	WHERE idStandupTrainingInstance = @idStandupTrainingInstance

	-- get the differences
	INSERT INTO @MeetingTimesDifferences SELECT * FROM @MeetingTimes EXCEPT SELECT * FROM @ExistingMeetingTimes
	INSERT INTO @MeetingTimesDifferences SELECT * FROM @ExistingMeetingTimes EXCEPT SELECT * FROM @MeetingTimes
	
	-- if there are differences, set the flag
	IF (SELECT COUNT(1) FROM @MeetingTimesDifferences) > 0 AND (SELECT COUNT(1) FROM @ExistingMeetingTimes) > 0
		BEGIN
		SET @writeTimeChangeEvent = 1
		END
		
	/*
	
	remove all meeting times for this standup training instance from the meeting times table
	(if there are any in the table variable, otherwise it means there are none)
	
	*/
	
	DELETE FROM tblStandUpTrainingInstanceMeetingTime
	WHERE idStandupTrainingInstance = @idStandupTrainingInstance
	AND idSite = @idCallerSite
	
	/*

	insert the standup training instance meeting times for ths standup training instance into the meeting times table

	*/

	IF (SELECT COUNT(1) FROM @MeetingTimes) > 0
		BEGIN

		INSERT INTO tblStandUpTrainingInstanceMeetingTime (
			idStandUpTrainingInstance,
			idSite,
			dtStart,
			dtEnd,
			[minutes],
			idTimezone
		)
		SELECT
			@idStandupTrainingInstance,
			@idCallerSite,
			MT.dtStart,
			MT.dtEnd,
			DATEDIFF(minute, MT.dtStart, MT.dtEnd),
			MT.idTimezone
		FROM @MeetingTimes MT

		END

	/*

	write time change event log entry if the flag is set

	*/

	IF (@writeTimeChangeEvent = 1)
		BEGIN

		DECLARE @eventLogItem EventLogItemObjects

		INSERT INTO @eventLogItem (
			idSite, 
			idObject, 
			idObjectRelated, 
			idObjectUser
		) 
		SELECT 
			@idCallerSite, 
			STI.idStandUpTraining, 
			@idStandupTrainingInstance, 
			NULL
		FROM tblStandUpTrainingInstance STI
		WHERE STI.idStandUpTrainingInstance = @idStandupTrainingInstance

		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 406, @utcNow, @eventLogItem

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO