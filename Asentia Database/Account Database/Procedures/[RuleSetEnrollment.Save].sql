-- =====================================================================
-- PROCEDURE: [RuleSetEnrollment.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetEnrollment.Save]
GO

/*

Adds a new ruleset enrollment or updates an existing one.

*/

CREATE PROCEDURE [RuleSetEnrollment.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idRuleSetEnrollment				INT				OUTPUT,
	@idCourse							INT,
	@idTimezone							INT,
	@label								NVARCHAR(255),
	@isLockedByPrerequisites			BIT,
	@isFixedDate						INT,
	@dtStart							DATETIME,
	@dtEnd								DATETIME,
	@delayInterval						INT,
	@delayTimeframe						NVARCHAR(4),
	@dueInterval						INT,
	@dueTimeframe						NVARCHAR(4),
	@recurInterval						INT,
	@recurTimeframe						NVARCHAR(4),
	@expiresFromStartInterval			INT,
	@expiresFromStartTimeframe			NVARCHAR(4),
	@expiresFromFirstLaunchInterval		INT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4),
	@forceReassignment					BIT,
	@idParentRuleSetEnrollment			INT				
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'RuleSetEnrollmentSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/
			 
	/*
	
	validate uniqueness within same parent object
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblRuleSetEnrollment
		WHERE idSite = @idCallerSite
		AND idCourse = @idCourse
		AND label = @label
		AND (
			@idRuleSetEnrollment IS NULL
			OR @idRuleSetEnrollment <> idRuleSetEnrollment
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetEnrollmentSave_RuleSetEnrollmentNotUnique'
		RETURN 1
		END

	/*
	
	validate uniqueness within language, and same parent object
	
	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF( 
		SELECT COUNT(1)
		FROM tblRuleSetEnrollment RSE
		LEFT JOIN tblRuleSetEnrollmentLanguage RSEL ON RSEL.idRuleSetEnrollment = RSE.idRuleSetEnrollment AND RSEL.idLanguage = @idDefaultLanguage -- same language
		WHERE RSE.idSite = @idCallerSite -- same site
		AND RSE.idRuleSetEnrollment <> @idRuleSetEnrollment -- not the same ruleset enrollment
		AND RSE.idCourse = @idCourse
		AND RSEL.label = @label -- validate parameter: label
		) > 0 
		
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetEnrollmentSave_FieldNotUnique'
		RETURN 1
		END
		
	/*
	
	save the data
	
	*/
	
	IF (@idRuleSetEnrollment = 0 OR @idRuleSetEnrollment IS NULL)
		
		BEGIN
		
		-- get the "priority" number for the new ruleset enrollment

		DECLARE @priority INT

		SELECT 
			@priority = CASE WHEN MAX([priority]) IS NULL THEN 1 ELSE MAX([priority]) + 1 END
		FROM tblRuleSetEnrollment
		WHERE idCourse = @idCourse

		-- insert the new ruleset enrollment
		
		INSERT INTO tblRuleSetEnrollment (
			idSite,
			idCourse,
			idTimezone,
			[priority],
			label,
			isLockedByPrerequisites,
			isFixedDate,
			dtStart,
			dtEnd,
			dtCreated,
			delayInterval,
			delayTimeframe,
			dueInterval,
			dueTimeframe,
			recurInterval,
			recurTimeframe,
			expiresFromStartInterval,
			expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe,
			forceReassignment,
			idParentRuleSetEnrollment
		)
		VALUES (
			@idCallerSite,
			@idCourse,
			@idTimezone,
			@priority,
			@label,
			@isLockedByPrerequisites,
			@isFixedDate,
			@dtStart,
			@dtEnd,
			GETUTCDATE(),
			@delayInterval,
			@delayTimeframe,
			@dueInterval,
			@dueTimeframe,
			@recurInterval,
			@recurTimeframe,
			@expiresFromStartInterval,
			@expiresFromStartTimeframe,
			@expiresFromFirstLaunchInterval,
			@expiresFromFirstLaunchTimeframe,
			@forceReassignment,
			NULL
		)
		
		-- get the new id
		
		SET @idRuleSetEnrollment = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the ruleset enrollment exists
		IF (SELECT COUNT(1) FROM tblRuleSetEnrollment WHERE idRuleSetEnrollment = @idRuleSetEnrollment AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idRuleSetEnrollment = @idRuleSetEnrollment
			SET @Return_Code = 1
			SET @Error_Description_Code = 'RuleSetEnrollmentSave_NoRecordFound'
			RETURN 1
			
			END

		-- if there is a change to the start date, delete the open queued item for that ruleset enrollment 
		DELETE FROM tblRuleSetEngineQueue 
		WHERE triggerObjectId = @idRuleSetEnrollment AND triggerObject = 'rulesetenrollment' AND isProcessing IS NULL AND dtProcessed IS NULL
		AND @dtStart <> (SELECT dtStart FROM tblRuleSetEnrollment WHERE idRuleSetEnrollment = @idRuleSetEnrollment)
			
		-- update existing record		
		UPDATE tblRuleSetEnrollment SET
			idTimezone = @idTimezone,
			label = @label,
			isLockedByPrerequisites = @isLockedByPrerequisites,
			dtStart = @dtStart,
			dtEnd = @dtEnd,
			delayInterval = @delayInterval,
			delayTimeframe = @delayTimeframe,
			dueInterval = @dueInterval,
			dueTimeframe = @dueTimeframe,
			recurInterval = @recurInterval,
			recurTimeframe = @recurTimeframe,
			expiresFromStartInterval = @expiresFromStartInterval,
			expiresFromStartTimeframe = @expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval = @expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe = @expiresFromFirstLaunchTimeframe,
			forceReassignment = @forceReassignment,
			idParentRuleSetEnrollment = NULL
		WHERE idRuleSetEnrollment = @idRuleSetEnrollment
		
		-- get the ruleset enrollment's id
		
		SET @idRuleSetEnrollment = @idRuleSetEnrollment
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/
	
	IF (SELECT COUNT(1) FROM tblRuleSetEnrollmentLanguage RSEL WHERE RSEL.idRuleSetEnrollment = @idRuleSetEnrollment AND RSEL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblRuleSetEnrollmentLanguage SET
			label = @label
		WHERE idRuleSetEnrollment = @idRuleSetEnrollment
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblRuleSetEnrollmentLanguage (
			idSite,
			idRuleSetEnrollment,
			idLanguage,
			label
		)
		SELECT
			@idCallerSite,
			@idRuleSetEnrollment,
			@idDefaultLanguage,
			@label
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblRuleSetEnrollmentLanguage RSEL
			WHERE RSEL.idRuleSetEnrollment = @idRuleSetEnrollment
			AND RSEL.idLanguage = @idDefaultLanguage
		)

		END

	/*

	sync child ruleset enrollments

	*/

	DECLARE @idChildRuleSetEnrollment		INT
	DECLARE @idChildCourse					INT

	SELECT 
		idRuleSetEnrollment
	INTO #SyncedRuleSetEnrollmentsToUpdate
	FROM tblRuleSetEnrollment
	WHERE idParentRuleSetEnrollment = @idRuleSetEnrollment	      
								  
	IF EXISTS (SELECT 1 FROM #SyncedRuleSetEnrollmentsToUpdate)	
	BEGIN
		DECLARE idChildRuleSetEnrollmentCursor							CURSOR LOCAL
		FOR 
		SELECT idRuleSetEnrollment FROM #SyncedRuleSetEnrollmentsToUpdate	

		OPEN idChildRuleSetEnrollmentCursor
		FETCH NEXT FROM idChildRuleSetEnrollmentCursor INTO @idChildRuleSetEnrollment
		WHILE @@FETCH_STATUS = 0  
		BEGIN
		
		SET @idChildCourse = (SELECT idCourse FROM tblRuleSetEnrollment WHERE idRuleSetEnrollment = @idChildRuleSetEnrollment)

		EXEC [RuleSetEnrollment.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idChildRuleSetEnrollment, @idChildCourse, @idTimezone, 
								  @label, @isLockedByPrerequisites, @isFixedDate, @dtStart, @dtEnd, @delayInterval, @delayTimeframe, @dueInterval, @dueTimeframe, @recurInterval, 
								  @recurTimeframe, @expiresFromStartInterval, @expiresFromStartTimeframe, @expiresFromFirstLaunchInterval, @expiresFromFirstLaunchTimeframe, 
								  @forceReassignment, @idRuleSetEnrollment
		
		-- sync child ruleset enrollment back to parent ruleset enrollment (calling RuleSet.Save unsyncs the ruleset enrollment)

		UPDATE tblRuleSetEnrollment
		SET idParentRuleSetEnrollment = @idRuleSetEnrollment
		WHERE idRuleSetEnrollment = @idChildRuleSetEnrollment

		FETCH NEXT FROM idChildRuleSetEnrollmentCursor INTO @idChildRuleSetEnrollment

		END

	CLOSE idChildRuleSetEnrollmentCursor
	DEALLOCATE idChildRuleSetEnrollmentCursor

	END
	
	UPDATE tblRuleSetEnrollment
	SET idParentRuleSetEnrollment = NULL
	WHERE idRuleSetEnrollment = @idRuleSetEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO