-- =====================================================================
-- PROCEDURE: [Calendar.GetItemsForUser]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Calendar.GetItemsForUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Calendar.GetItemsForUser]
GO

/*

Returns a listing of items for a user (enrollment events, ILT sessions, etc) that occur within 
the same month as a specified date.

*/

CREATE PROCEDURE [Calendar.GetItemsForUser]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified

	@calendarDate			DATETIME			
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
			
	/*
	
	validate caller permission
	
	*/
	
	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	get the caller's language
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	
	/*
	
	get the month timespan, plus and minus 1 day, based off of the specified date
	this is to grab only the events that occur within the same month as the specified date
	
	*/

	DECLARE @startDate DATETIME	
	SET @startDate = DATEADD(DAY, -(DAY(@calendarDate)), @calendarDate)
	SET @startDate = DATEADD(DAY, DATEDIFF(DAY, '19000101', @startDate), '19000101')
	
	DECLARE @endDate DATETIME
	SET @endDate = DATEADD(DAY, -(DAY(DATEADD(mm, 1, @calendarDate)) - 1), DATEADD(mm, 1, @calendarDate))
	SET @endDate = DATEADD(DAY, DATEDIFF(DAY, '19000101', @endDate), '23:59:59')

	/* 
	
	get the calendar items

	*/

	-- SELECT FOR ENROLLMENT DUE
	SELECT		
		E.idEnrollment AS [id],
		'enrollmentDue' AS [event],
		'ENROLLMENT DUE: ' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS [eventTitle],
		E.dtDue AS [dtEvent]		
	FROM tblEnrollment E
	LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	WHERE E.idUser = @idCaller
	AND E.idCourse > 0 
	AND E.idCourse IS NOT NULL
	AND E.idActivityImport IS NULL
	AND E.dtCompleted IS NULL
	AND E.dtDue IS NOT NULL
	AND E.dtDue >= @startDate 
	AND E.dtDue <= @endDate
	-- UNION SELECT FOR ENROLLMENT EXPIRES
	UNION SELECT
		E.idEnrollment AS [id],
		'enrollmentExpires' AS [event],
		'ENROLLMENT EXPIRES: ' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS [eventTitle],
		CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NOT NULL THEN
			E.dtExpiresFromFirstLaunch
		ELSE
			CASE WHEN E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
				E.dtExpiresFromStart
			ELSE
				CASE WHEN E.dtExpiresFromStart >= E.dtExpiresFromFirstLaunch THEN
					E.dtExpiresFromFirstLaunch
				ELSE
					E.dtExpiresFromStart
				END
			END
		END AS [dtEvent]
	FROM tblEnrollment E
	LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	WHERE E.idUser = @idCaller
	AND E.idCourse > 0 
	AND E.idCourse IS NOT NULL
	AND E.idActivityImport IS NULL
	AND E.dtCompleted IS NULL
	AND (
			(E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromStart >= @startDate AND E.dtExpiresFromStart <= @endDate)
			OR
			(E.dtExpiresFromFirstLaunch IS NOT NULL AND E.dtExpiresFromFirstLaunch >= @startDate AND E.dtExpiresFromFirstLaunch <= @endDate)
		)
	-- UNION SELECT FOR ILT SESSION
	UNION SELECT
		STIUL.idStandupTrainingInstance AS [id],
		'session' AS [event],
		CASE WHEN STL.title IS NOT NULL THEN STL.title ELSE ST.title END
		+ ' - ' +
		CASE WHEN STIL.title IS NOT NULL THEN STIL.title ELSE STI.title END
		AS [eventTitle],
		STIMT.dtStart AS [dtEvent]
	FROM tblStandupTrainingInstanceToUserLink STIUL
	LEFT JOIN tblStandUpTrainingInstanceMeetingTime STIMT ON STIMT.idStandupTrainingInstance = STIUL.idStandupTrainingInstance
	LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandupTrainingInstance = STIMT.idStandupTrainingInstance
	LEFT JOIN tblStandUpTraining ST ON ST.idStandUpTraining = STI.idStandupTraining
	LEFT JOIN tblStandupTrainingLanguage STL ON STL.idStandUpTraining = ST.idStandUpTraining AND STL.idLanguage = @idCallerLanguage
	LEFT JOIN tblStandupTrainingInstanceLanguage STIL ON STIL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance AND STIL.idLanguage = @idCallerLanguage
	WHERE STIUL.idUser = @idCaller		
	AND STIMT.dtStart >= @startDate 
	AND STIMT.dtStart <= @endDate
	AND (STI.isDeleted IS NULL OR STI.isDeleted = 0)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
