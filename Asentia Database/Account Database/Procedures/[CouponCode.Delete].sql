-- =====================================================================
-- PROCEDURE: [CouponCode.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CouponCode.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CouponCode.Delete]
GO

CREATE PROCEDURE [CouponCode.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@idCaller				INT				= 0,
	
	@CouponCodes			IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF (SELECT COUNT(1) FROM @CouponCodes) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CouponCodeDelete_NoRecordsSelected'
		RETURN 1
		END

	/*
	
	validate that all coupon codess exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @CouponCodes CC
		LEFT JOIN tblCouponCode C ON C.idCouponCode = CC.id
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'CouponCodeDelete_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	Mark the Coupon as deleted. DO NOT remove the record.
	
	*/
	
	UPDATE tblCouponCode SET
		isDeleted = 1
	WHERE idCouponCode IN (
		SELECT id
		FROM @CouponCodes
	)
	
	SELECT @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO