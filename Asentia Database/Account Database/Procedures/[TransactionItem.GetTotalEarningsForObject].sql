-- =====================================================================
-- PROCEDURE: [TransactionItem.GetTotalEarningsForObject]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[TransactionItem.GetTotalEarningsForObject]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.GetTotalEarningsForObject]
GO

/*

Gets total e-commerce earnings for a object (catalog, course, learning path, ilt).

*/

CREATE PROCEDURE [TransactionItem.GetTotalEarningsForObject]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idObject				INT,
	@itemType				INT,
	@totalEarnings			FLOAT			OUTPUT	
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	get the total earnings for the object

	*/

	SELECT @totalEarnings = SUM(TI.paid) FROM tblTransactionItem TI WHERE TI.itemId = @idObject AND TI.itemType = @itemType AND TI.confirmed = 1 AND TI.paid > 0

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO