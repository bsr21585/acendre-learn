-- =====================================================================
-- PROCEDURE: [CertificationModule.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificationModule.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModule.SaveLang]
GO

/*

Saves certification module "language specific" properties for specific language
in certification module language table.

*/

CREATE PROCEDURE [CertificationModule.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCertificationModule	INT, 
	@languageString			NVARCHAR(10),
	@title					NVARCHAR(255), 
	@shortDescription		NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
			 
	/*
	
	validate that the certification module exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificationModule
		WHERE idCertificationModule = @idCertificationModule
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleSaveLang_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblCertificationModuleLanguage CML WHERE CML.idCertificationModule = @idCertificationModule AND CML.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblCertificationModuleLanguage SET
			title = @title,
			shortDescription = @shortDescription
		WHERE idCertificationModule = @idCertificationModule
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCertificationModuleLanguage (
			idSite,
			idCertificationModule,
			idLanguage,
			title,
			shortDescription
		)
		SELECT
			@idCallerSite,
			@idCertificationModule,
			@idLanguage,
			@title,
			@shortDescription
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCertificationModuleLanguage CML
			WHERE CML.idCertificationModule = @idCertificationModule
			AND CML.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblCertificationModule SET
			title = @title,
			shortDescription = @shortDescription
		WHERE idCertificationModule = @idCertificationModule

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO