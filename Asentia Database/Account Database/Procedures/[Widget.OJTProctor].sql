-- =====================================================================
-- PROCEDURE: [Widget.OJTProctor]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Widget.OJTProctor]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.OJTProctor]
GO

/*

Gets a listing of lesson data for ojt proctor widget.

*/

CREATE PROCEDURE [Widget.OJTProctor]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	-- NOTE THAT WE ARE NOT USING THE SEARCH, PAGINATION, OR SORT HERE BUT
	-- SINCE THESE ARE COMMON PARAMETERS FOR GRIDS, WE NEED TO KEEP THEM
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	determine if caller has "global" permission to proctor, i.e. is the caller an admin

	if the caller does have permission, we'll skip figuring out what he can proctor and
	just list everything that needs proctoring
	
	*/

	DECLARE @callerHasGlobalPermission BIT
	SET @callerHasGlobalPermission = [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL)

	-- THIS IS TEMPORARY
	IF @idCaller > 1
		BEGIN
		SET @callerHasGlobalPermission = 0
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	if the user does not have "global" permission then get the users the caller is a supervisor 
	of and the courses the caller is an expert for

	*/

	CREATE TABLE #AuthorizedUsers (idUser INT)
	CREATE TABLE #AuthorizedCourses (idCourse INT)

	IF (@callerHasGlobalPermission = 0)
		
		BEGIN
		
		INSERT INTO #AuthorizedUsers (idUser)
		SELECT DISTINCT idUser FROM tblUserToSupervisorLink WHERE idSupervisor = @idCaller

		INSERT INTO #AuthorizedCourses (idCourse)
		SELECT DISTINCT idCourse FROM tblCourseExpert WHERE idUser = @idCaller

		END

	/*

	begin getting the grid data

	*/

	CREATE TABLE #LessonDataForProctor (
		idEnrollment				INT,
		idUser						INT,
		idCourse					INT,
		idLesson					INT,
		[idData-Lesson]				INT,
		[idData-SCO]				INT,
		idContentPackage			INT,
		courseName					NVARCHAR(255),
		lessonName					NVARCHAR(255),
		userDisplayName				NVARCHAR(255),
		idContentPackageType		INT,
		contentPackageName			NVARCHAR(255),
		contentPackagePath			NVARCHAR(1024)
	)

	-- get the grid data

	INSERT INTO #LessonDataForProctor (
		idEnrollment,
		idUser,
		idCourse,
		idLesson,
		[idData-Lesson],
		[idData-SCO],
		idContentPackage,
		courseName,
		lessonName,
		userDisplayName,
		idContentPackageType,
		contentPackageName,
		contentPackagePath
	)
	SELECT
		E.idEnrollment,
		U.idUser,
		C.idCourse,
		L.idLesson,
		LD.[idData-Lesson],
		DSCO.[idData-SCO],
		CP.idContentPackage,
		CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE E.title END,
		CASE WHEN LL.title IS NOT NULL THEN LL.title ELSE LD.title END,
		U.displayName,
		CP.idContentPackageType,
		CP.name,
		CP.[path]
	FROM [tblData-Lesson] LD
	LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
	LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson
	LEFT JOIN tblLessonLanguage LL ON LL.idLesson = L.idLesson AND LL.idLanguage = @idCallerLanguage
	LEFT JOIN tblLessonToContentLink LCLOJT ON LCLOJT.idLesson = L.idLesson AND LCLOJT.idContentType = 4
	LEFT JOIN tblContentPackage CP ON CP.idContentPackage = LCLOJT.idObject
	LEFT JOIN tblUser U ON U.idUser = E.idUser
	LEFT JOIN [tblData-SCO] DSCO ON DSCO.[idData-Lesson] = LD.[idData-Lesson]
	WHERE LD.idSite = @idCallerSite
	AND LD.contentTypeCommittedTo = 4														-- committed to ojt
	AND LD.dtCompleted IS NULL																-- lesson data is not complete
	AND (DSCO.proctoringUser IS NULL OR DSCO.proctoringUser = @idCaller)					-- not yet proctored or proctoring user is the calling user
	AND E.dtCompleted IS NULL																-- enrollment is not complete
	AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)				-- enrollment is not expired
	AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))	-- users must not be deleted or unapproved
	AND U.isActive = 1																		-- users must be active
	AND (	-- caller is an admin, or caller is not an admin and IS an expert or supervisor for respective objects
			@callerHasGlobalPermission = 1
			OR
			(
				@callerHasGlobalPermission = 0
				AND
				C.idCourse IN (SELECT idCourse FROM #AuthorizedCourses)
			)
			OR
			(
				@callerHasGlobalPermission = 0
				AND
				U.idUser IN (SELECT idUser FROM #AuthorizedUsers)
			)
		)

	-- return the rowcount and grid data

	SELECT COUNT(1) AS row_count
	FROM #LessonDataForProctor

	;WITH
		Keys AS (
			SELECT TOP (@pageNum * @pageSize)
				LDFP.[idData-Lesson],
				ROW_NUMBER() OVER (ORDER BY LDFP.userDisplayName)
				AS [row_number]
			FROM #LessonDataForProctor LDFP
		),

		SelectedKeys AS (
			SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
				[idData-Lesson], 
				[row_number]
			FROM Keys
			WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
		)
	SELECT
		LDFP.idEnrollment,
		LDFP.idUser,
		LDFP.idCourse,
		LDFP.idLesson,
		LDFP.[idData-Lesson],
		LDFP.[idData-SCO],
		LDFP.idContentPackage,
		LDFP.courseName,
		LDFP.lessonName,
		LDFP.userDisplayName,
		LDFP.idContentPackageType,
		LDFP.contentPackageName,
		LDFP.contentPackagePath,
		SelectedKeys.[row_number]
	FROM SelectedKeys
	JOIN #LessonDataForProctor LDFP ON LDFP.[idData-Lesson] = SelectedKeys.[idData-Lesson]
	ORDER BY SelectedKeys.[row_number]

	-- drop temporary tables
	DROP TABLE #AuthorizedCourses
	DROP TABLE #AuthorizedUsers
	DROP TABLE #LessonDataForProctor

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO