-- =====================================================================
-- PROCEDURE: [System.GetRuleSetRoleMatches]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetRuleSetRoleMatches]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetRuleSetRoleMatches]
GO

/*

Gets RuleSet matches for Roles.

*/
CREATE PROCEDURE [System.GetRuleSetRoleMatches]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,

	@Roles						IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* SET FILTER TO 'user' IF IT IS ANYTHING OTHER THAN 'group' */
	IF @filterBy IS NULL OR LOWER(@filterBy) <> LOWER('group')
		SET @filterBy = 'user'

	/* CHECK GROUP AND FILTER LISTS FOR RECORDS */
	DECLARE @RolesRecordCount INT
	DECLARE @FiltersRecordCount INT
	SELECT @RolesRecordCount = COUNT(1) FROM @Roles
	SELECT @FiltersRecordCount = COUNT(1) FROM @Filters

	/* 
	
		CREATE TEMPORARY TABLE TO STORE USER-TO-ROLE JOINS
		RESULTING FROM RULES EXECUTION
	*/
	CREATE TABLE #UserToRoleJoins
	(
		idRuleSet	INT,
		idRole		INT,
		idUser		INT
	)

	/* CREATE TEMPORARY TABLE FOR ROLE AUTO-JOIN RULES */
	CREATE TABLE #RoleRules
	(
		idSite		INT,
		idRuleSet	INT,
		idRole		INT,
		isAny		BIT,
		field		NVARCHAR(25),
		operator	NVARCHAR(25),
		value		NVARCHAR(255)
	)

	/* PUT ROLE AUTO-JOIN RULESETS IN TEMP TABLE */
	INSERT INTO #RoleRules
	(
		idSite,
		idRuleSet,
		idRole,
		isAny,
		field,
		operator,
		value
	)
	SELECT
		RS.idSite,
		RS.idRuleSet,
		RSRL.idRole,
		RS.isAny,
		R.userField,
		R.operator,
		CASE WHEN R.textValue IS NOT NULL THEN 
			REPLACE(R.textValue, '''', '''''')
		ELSE
			CASE WHEN R.dateValue IS NOT NULL THEN
				CONVERT(NVARCHAR(255), R.dateValue)
			ELSE
				CASE WHEN R.numValue IS NOT NULL THEN
					CONVERT(NVARCHAR(255), R.numValue)
				ELSE
					CASE WHEN R.bitValue IS NOT NULL THEN
						CONVERT(NVARCHAR(255), R.bitValue)
					ELSE
						NULL
					END
				END
			END
		END
	FROM tblRuleSet RS
	LEFT JOIN tblRule R ON R.idRuleSet = RS.idRuleSet	
	LEFT JOIN tblRuleSetToRoleLink RSRL ON RSRL.idRuleSet = RS.idRuleSet	
	WHERE RSRL.idRuleSetToRoleLink IS NOT NULL	
	AND (
			(@RolesRecordCount = 0)
			OR 
			(@RolesRecordCount > 0 AND EXISTS (SELECT 1 FROM @Roles RP WHERE RP.id = RSRL.idRole))
		)

	/* DECLARE VARIABLES FOR BUILDING RULE EXECUTION QUERIES */
	DECLARE @rolesWithRuleSets TABLE (id INT, idSite INT)
	DECLARE @ruleSetsForRole IDTable
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @idRole INT
	DECLARE @idSite INT
	DECLARE @idRuleSet INT
	DECLARE @isAny BIT
	DECLARE @field NVARCHAR(25)
	DECLARE @operator NVARCHAR(25)
	DECLARE @value NVARCHAR(255)
	DECLARE @ruleCounter INT

	/* GET IDS OF ROLES WITH RULESETS */
	INSERT INTO @rolesWithRuleSets ( 
		id,
		idSite
	)
	SELECT DISTINCT
		idRole,
		idSite
	FROM #RoleRules

	/* CURSOR FOR ROLES WITH RULESETS */
	DECLARE rolesCursor CURSOR FOR
	SELECT
		id,
		idSite
	FROM @rolesWithRuleSets

	OPEN rolesCursor

	FETCH NEXT FROM rolesCursor INTO @idRole, @idSite

	/* LOOP THROUGH ROLES RULESETS */
	WHILE (@@FETCH_STATUS = 0)

		BEGIN

		/* GET IDS OF RULESETS FOR THE ROLE */
		INSERT INTO @ruleSetsForRole
		( id )
		SELECT DISTINCT
			idRuleSet
		FROM #RoleRules
		WHERE idRole = @idRole

		/* CURSOR FOR RULESETS FOR THE ROLE */
		DECLARE roleRuleSetsCursor CURSOR FOR
		SELECT DISTINCT
			id,
			RR.isAny
		FROM @ruleSetsForRole
		LEFT JOIN #RoleRules RR ON RR.idRuleSet = id

		OPEN roleRuleSetsCursor

		FETCH NEXT FROM roleRuleSetsCursor INTO @idRuleSet, @isAny
	
		/* LOOP THROUGH RULESETS FOR THE ROLE */
		WHILE (@@FETCH_STATUS = 0)

			BEGIN

			/* START QUERY SQL STRING FOR RULESET */
			SET @sql = 'INSERT INTO #UserToRoleJoins (idRuleSet, idRole, idUser) SELECT ' + CONVERT(NVARCHAR, @idRuleSet) + ', ' + CONVERT(NVARCHAR, @idRole) + ', idUser ' + 'FROM tblUser U WHERE ('
			
			/* RESET THE RULE COUNTER */
			SET @ruleCounter = 0

			/* CURSOR FOR RULESET RULES */
			DECLARE ruleSetRulesCursor CURSOR FOR
			SELECT
				field,
				operator,
				value
			FROM #RoleRules
			WHERE idRuleSet = @idRuleSet

			OPEN ruleSetRulesCursor
		
			FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
	
			/* LOOP THROUGH RULESET RULES */
			WHILE (@@FETCH_STATUS = 0)

				BEGIN

				/* IF THIS IS NOT THE FIRST RULE, EXAMINE isAny LOGIC TO DETERMINE AND/OR OPERATOR */
				IF @ruleCounter > 0
					BEGIN
						IF @isAny = 1
							BEGIN
							SET @sql = @sql + ' OR '
							END
						ELSE
							BEGIN
							SET @sql = @sql + ' AND '
							END
					END

				/* 
				
				BUILD WHERE CLAUSE BASED ON OPERATOR - DIFFERENT LOGIC FOR GROUP 
				GROUP EVALUATES ON THE "name" IN THE BASE TABLE, NOT EVERY LANGUAGE JUST THE DEFAULT

				*/

				IF @field = 'group'
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					END

				ELSE
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' = ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' <> ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) < ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) < ' + '''' + @value + '''' + ')'
						END

					END
			
				/* INCREMENT RULE COUNTER */
				SET @ruleCounter = @ruleCounter + 1

				FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
				END

			/* CLOSE AND DEALLOCATE CURSOR FOR RULESET RULES */
			CLOSE ruleSetRulesCursor
			DEALLOCATE ruleSetRulesCursor

			/* APEND ANDs FOR THE SITE AND USER DELETED STATUS */
			SET @sql = @sql + ') AND (U.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)))'

			/* EXECUTE THE SQL TO INSERT RECORDS IN THE #UserToRoleJoins TABLE */
			EXECUTE sp_executesql @sql
		
			FETCH NEXT FROM roleRuleSetsCursor INTO @idRuleSet, @isAny
			END

		/* CLOSE AND DEALLOCATE CURSOR FOR RULESETS FOR THE ROLE */
		DELETE FROM @ruleSetsForRole
		CLOSE roleRuleSetsCursor
		DEALLOCATE roleRuleSetsCursor

		FETCH NEXT FROM rolesCursor INTO @idRole, @idSite
		END

	/* CLOSE AND DEALLOCATE CURSOR FOR ROLES WITH RULESETS */
	CLOSE rolesCursor
	DEALLOCATE rolesCursor
				  
	/* RETURN RECORDS */
	SELECT DISTINCT
		R.idSite,
		URJ.idRuleSet,
		URJ.idRole,
		URJ.idUser
	FROM #UserToRoleJoins URJ
	LEFT JOIN tblRole R ON R.idRole = URJ.idRole
	LEFT JOIN tblUserToGroupLink UGL ON UGL.idUser = URJ.idUser
	WHERE (  
			  (
				  @filterBy = 'user' AND
				  (
					(@FiltersRecordCount = 0)
					OR 
					(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = URJ.idUser))
				  )
			  )
			  OR
			  (
				  @filterBy = 'group' AND
				  (
					(@FiltersRecordCount = 0)
					OR 
					(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = UGL.idGroup))
				  )
			  )
		  )

	/* DROP TEMPORARY TABLES */
	DROP TABLE #RoleRules
	DROP TABLE #UserToRoleJoins

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO