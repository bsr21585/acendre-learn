-- =====================================================================
-- PROCEDURE: [TransactionItem.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.Save]
GO

/*

Adds a new transaction item or updates an existing transaction item.

*/

CREATE PROCEDURE [TransactionItem.Save]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idTransactionItem			INT				OUTPUT, 
	@idParentTransactionItem	INT, 	
	@idPurchase					INT,
	@idEnrollment				INT,
	@idUser						INT,
	@userName					NVARCHAR(255),
	@idAssigner					INT, 
	@assignerName				NVARCHAR(255),
	@itemId						INT,
	@itemName					NVARCHAR(255),
	@itemType					INT,
	@description				NVARCHAR(512),
	@cost						FLOAT,
	@paid						FLOAT,
	@idCouponCode				INT,
	@couponCode					NVARCHAR(10),
	@dtAdded					DATETIME,
	@confirmed					BIT,
	@idIltSession				INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END		
			 
	/*
	
	save the data
	
	*/
	
	IF (@idTransactionItem = 0 OR @idTransactionItem IS NULL)
		
		BEGIN
		
		-- insert the new transaction item
		
		INSERT INTO tblTransactionItem (			
			idParentTransactionItem,
			idPurchase,
			idEnrollment,
			idUser,
			userName,
			idAssigner,
			assignerName,
			itemId,
			itemName,
			itemType,
			[description],
			cost,
			paid,
			idCouponCode,
			couponCode,
			dtAdded,
			confirmed,
			idSite,
			idIltSession
		)			
		VALUES (
			@idParentTransactionItem, 	
			@idPurchase,
			@idEnrollment,
			@idUser,
			@userName,
			@idAssigner, 
			@assignerName,
			@itemId,
			@itemName,
			@itemType,
			@description,
			@cost,
			@paid,
			@idCouponCode,
			@couponCode,
			GETUTCDATE(),
			@confirmed,
			@idCallerSite,
			@idIltSession
		)
		
		-- get the new transaction item's id and return successfully
		
		SELECT @idTransactionItem = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the transaction item id exists
		
		IF (SELECT COUNT(1) FROM tblTransactionItem WHERE idTransactionItem = @idTransactionItem AND idSite = @idCallerSite) < 1			
			BEGIN
			SET @Return_Code = 1
			SET @Error_Description_Code = 'TransactionItemSave_NoRecordFound'
			RETURN 1
			END
			
		-- update existing transaction item's properties
		
		UPDATE tblTransactionItem SET			
			idParentTransactionItem		= @idParentTransactionItem, 	
			idPurchase					= @idPurchase,
			idEnrollment				= @idEnrollment,
			idUser						= @idUser,
			userName					= @userName,
			idAssigner					= @idAssigner, 
			assignerName				= @assignerName,
			itemId						= @itemId,
			itemName					= @itemName,
			itemType					= @itemType,
			[description]				= @description,
			cost						= @cost,
			paid						= @paid,
			idCouponCode				= @idCouponCode,
			couponCode					= @couponCode,
			dtAdded						= @dtAdded,
			confirmed					= @confirmed,
			idSite						= @idCallerSite,
			idIltSession				= @idIltSession
		WHERE idTransactionItem = @idTransactionItem
		
		-- get the transaction item's id
		
		SELECT @idTransactionItem = @idTransactionItem
				
		END
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO