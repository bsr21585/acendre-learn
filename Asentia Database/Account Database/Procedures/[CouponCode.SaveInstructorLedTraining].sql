-- =====================================================================
-- PROCEDURE: [[CouponCode.SaveInstructorLedTraining]]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CouponCode.SaveInstructorLedTraining]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CouponCode.SaveInstructorLedTraining]
GO

/*

Saves the instructor led training for coupon code.

*/

CREATE PROCEDURE [CouponCode.SaveInstructorLedTraining]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCouponCode			INT				OUTPUT,
	@forStandupTraining		INT, 
	@idStandupTrainingList	IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END			 
	
	/*

	drop the existing list

	*/

	DELETE FROM tblCouponCodeToStandupTrainingLink WHERE idCouponCode = @idCouponCode


	/*

	relink the list

	*/

	IF @forStandupTraining = 3 -- 3 is listing of individual objects

	BEGIN
	
	IF (SELECT COUNT(1) FROM @idStandupTrainingList) > 0 
		BEGIN
		
		INSERT INTO tblCouponCodeToStandupTrainingLink (
			idCouponCode, 
			idStandupTraining
		)
		SELECT 
			@idCouponCode, 
			id
		FROM @idStandupTrainingList L
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCouponCodeToStandupTrainingLink
			WHERE idCouponCode = @idCouponCode
			AND idStandupTraining = L.id
		)
			
		END
		
	END	

	SET @Return_Code = 0
	SET @Error_Description_Code = ''	
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO