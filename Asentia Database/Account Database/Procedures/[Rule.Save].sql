-- =====================================================================
-- PROCEDURE: [Rule.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Rule.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Rule.Save]
GO

CREATE PROCEDURE [Rule.Save]
(
	@Return_Code			INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)		OUTPUT,
	@idCallerSite			INT					= 0,
	@callerLangString		NVARCHAR (10),
	@idCaller				INT					= 0,
	
	@idRuleSet				INT,
	@rulesTable				RuleTable			READONLY
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED	-- took reference from Group.Save
	
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			RETURN 1 
		END
	
	*/


	/*

	get data for syncing child rulesets' rules

	*/

	DECLARE @idParentRuleSetEnrollment					INT
	DECLARE @idParentRuleSetLearningPathEnrollment		INT
	DECLARE @idChildRuleSet								INT

	SET @idParentRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idRuleSet)
	SET @idParentRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = @idRuleSet)
	
	-- get child rulesets

	SELECT 
		RS.idRuleSet
	INTO #SyncedCourseRuleSetsToUpdate 
	FROM tblRuleSetToRuleSetEnrollmentLink RSTRSEL
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSTRSEL.idRuleSetEnrollment 
	LEFT JOIN tblRuleSet RS on RS.idRuleSet = RSTRSEL.idRuleSet
	WHERE idParentRuleSetEnrollment = @idParentRuleSetEnrollment
	AND RS.label = (SELECT label FROM tblRuleSet WHERE idRuleSet = @idRuleSet)

	SELECT 
		RS.idRuleSet
	INTO #SyncedLearningPathRuleSetsToUpdate 
	FROM tblRuleSetToRuleSetLearningPathEnrollmentLink RSTRSLPEL
	LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = RSTRSLPEL.idRuleSetLearningPathEnrollment 
	LEFT JOIN tblRuleSet RS on RS.idRuleSet = RSTRSLPEL.idRuleSet
	WHERE idParentRuleSetLearningPathEnrollment = @idParentRuleSetLearningPathEnrollment
	AND RS.label = (SELECT label FROM tblRuleSet WHERE idRuleSet = @idRuleSet)
	
	-- DELETE Rules

	DELETE 
	FROM tblRule
	WHERE (idRuleSet = @idRuleSet) 
	OR (idRuleSet IN (SELECT idRuleSet FROM #SyncedCourseRuleSetsToUpdate))
	OR (idRuleSet IN (SELECT idRuleSet FROM #SyncedLearningPathRuleSetsToUpdate))
	AND NOT EXISTS (
		SELECT 1
		FROM @rulesTable rt
		WHERE rt.idRule = tblRule.idRule
		)

	-- UPDATE EXISTING Rules

	UPDATE dbo.tblRule SET
		 idSite = rt.idSite,
		 userField = rt.userField,
		 operator  = rt.operator,
		 textValue = rt.textValue,
		 dateValue = rt.dateValue,
		 numValue = rt.numValue,
		 bitValue = rt.bitValue
	FROM dbo.tblRule r, @rulesTable rt
	WHERE r.idRule = rt.idRule
	

-- NEW RULES Insertion

	INSERT INTO tblRule ( 
		idSite ,
		idRuleSet ,
		userField ,
		operator ,
		textValue ,
		dateValue ,
		numValue ,
		bitValue
		)
	SELECT 
		idSite, 
		@idRuleSet,
		userField ,
		operator ,
		textValue ,
		dateValue,
		numValue ,
		bitValue
	FROM @rulesTable rt
	WHERE NOT EXISTS (
		SELECT 1
		FROM dbo.tblRule 
		WHERE tblRule.idRule = rt.idRule
	)

	-- sync child rulesets' rules

	IF EXISTS (SELECT 1 FROM #SyncedCourseRuleSetsToUpdate)				-- courses  
	BEGIN

		DECLARE idChildRuleSetCursor					CURSOR LOCAL
		FOR 
		SELECT idRuleSet FROM #SyncedCourseRuleSetsToUpdate

		OPEN idChildRuleSetCursor
		FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet
		WHILE @@FETCH_STATUS = 0  
		BEGIN
		
		INSERT INTO tblRule ( 
			idSite ,
			idRuleSet ,
			userField ,
			operator ,
			textValue ,
			dateValue ,
			numValue ,
			bitValue
			)
		SELECT 
			idSite, 
			@idChildRuleSet,
			userField ,
			operator ,
			textValue ,
			dateValue,
			numValue ,
			bitValue
		FROM @rulesTable rt
		WHERE NOT EXISTS (
			SELECT 1
			FROM dbo.tblRule 
			WHERE tblRule.idRule = rt.idRule
		)
		
		FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet

		END

	CLOSE idChildRuleSetCursor
	DEALLOCATE idChildRuleSetCursor
	DROP TABLE #SyncedCourseRuleSetsToUpdate

	END

	ELSE IF EXISTS (SELECT 1 FROM #SyncedLearningPathRuleSetsToUpdate)	-- learning paths
	BEGIN

		DECLARE idChildRuleSetCursor					CURSOR LOCAL
		FOR 
		SELECT idRuleSet FROM #SyncedLearningPathRuleSetsToUpdate

		OPEN idChildRuleSetCursor
		FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet
		WHILE @@FETCH_STATUS = 0  
		BEGIN
		
		INSERT INTO tblRule ( 
			idSite ,
			idRuleSet ,
			userField ,
			operator ,
			textValue ,
			dateValue ,
			numValue ,
			bitValue
			)
		SELECT 
			idSite, 
			@idChildRuleSet,
			userField ,
			operator ,
			textValue ,
			dateValue,
			numValue ,
			bitValue
		FROM @rulesTable rt
		WHERE NOT EXISTS (
			SELECT 1
			FROM dbo.tblRule 
			WHERE tblRule.idRule = rt.idRule
		)
		
		FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet

		END

	CLOSE idChildRuleSetCursor
	DEALLOCATE idChildRuleSetCursor
	DROP TABLE #SyncedLearningPathRuleSetsToUpdate

	END

	SELECT @Return_Code = 0
	END			

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO