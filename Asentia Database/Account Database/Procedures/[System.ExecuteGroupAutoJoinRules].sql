-- =====================================================================
-- PROCEDURE: [System.ExecuteGroupAutoJoinRules]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ExecuteGroupAutoJoinRules]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ExecuteGroupAutoJoinRules]
GO

/*

Executes the Auto-Join Rules for Groups.

*/
CREATE PROCEDURE [System.ExecuteGroupAutoJoinRules]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,
	@Groups						IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* GET UTC TIMESTAMP */
	DECLARE @dtExecuted DATETIME
	SET @dtExecuted = GETUTCDATE()

	/* 
		
	Create temporary table to store user-to-group joins resulting from rules execution.
	
	*/

	CREATE TABLE #UserToGroupMatches
	(
		idSite		INT,
		idRuleSet	INT,
		idGroup		INT,
		idUser		INT		
	)

	/*

	Get the ruleset to group matches, and place them in the temporary table.

	*/

	INSERT INTO #UserToGroupMatches
	EXEC [System.GetRuleSetGroupMatches] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @Groups, @Filters, @filterBy
	
	/*

	Join users that need to be joined, remove users that need to be removed.

	*/
				  
	/* ADD RECORDS TO USER TO GROUP LINK */

	INSERT INTO tblUserToGroupLink
	(
		idSite,
		idUser,
		idGroup,
		idRuleSet,
		created
	)
	SELECT
		G.idSite,
		UGM.idUser,
		UGM.idGroup,
		UGM.idRuleSet,
		@dtExecuted
	FROM #UserToGroupMatches UGM
	LEFT JOIN tblGroup G ON G.idGroup = UGM.idGroup
	WHERE NOT EXISTS (SELECT 1 FROM tblUserToGroupLink UGL
					  WHERE UGL.idRuleSet IS NOT NULL
					  AND UGL.idUser = UGM.idUser
					  AND UGL.idGroup = UGM.idGroup
					  AND UGL.idRuleSet = UGM.idRuleSet)

	/* REMOVE RECORDS FROM USER TO GROUP LINK */
	DELETE FROM tblUserToGroupLink
	WHERE idUserToGroupLink IN (
								SELECT
									UGL.idUserToGroupLink
								FROM tblUserToGroupLink UGL
								WHERE NOT EXISTS (SELECT 1 FROM #UserToGroupMatches UGM
												  WHERE UGM.idUser = UGL.idUser
												  AND UGM.idGroup = UGL.idGroup
												  AND UGM.idRuleSet = UGL.idRuleSet)
								AND UGL.idRuleSet IS NOT NULL
								AND (
										((SELECT COUNT(1) FROM @Filters) = 0)										
										OR
										(@filterBy = 'user' AND UGL.idUser IN (SELECT DISTINCT id FROM @Filters))
									)
								AND (
										((SELECT COUNT(1) FROM @Groups) = 0)										
										OR
										(UGL.idGroup IN (SELECT DISTINCT id FROM @Groups))
									)
							   )

	/* DROP TEMPORARY TABLES */
	DROP TABLE #UserToGroupMatches

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO