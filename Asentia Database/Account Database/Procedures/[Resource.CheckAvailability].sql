-- =====================================================================
-- PROCEDURE: [Resource.CheckAvailability]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Resource.CheckAvailability]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Resource.CheckAvailability]
GO

/*

check is resource is available at selected time intervals
*/

CREATE PROCEDURE [Resource.CheckAvailability]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idObject                   INT,
	@idResource					INT,
	@MeetingTimes				DateRange		READONLY,
	@isAvailable                BIT             OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	select * from @MeetingTimes
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	--set default value
	SET @isAvailable = 1

	/*
	
	validate that the resource belongs to the site
	
	*/
	
	IF ((
		SELECT COUNT(1)
        FROM tblResourceToObjectLink ROL
		INNER JOIN tblStandUpTrainingInstance STI ON ROL.idObject = STI.idStandUpTrainingInstance
        CROSS JOIN @MeetingTimes DR
        WHERE 
        (idResource           =         @idResource   
		OR idResource = (SELECT idParentResource FROM tblResource WHERE idResource = @idResource)
		OR idResource IN (SELECT idResource FROM tblResource WHERE idParentResource = @idResource))
        AND idObject != @idObject  
        AND (  (DR.dtStart <= ROL.dtStart AND DR.dtEnd >= ROL.dtEnd)
                    OR(DR.dtStart BETWEEN ROL.dtStart AND  ROL.dtEnd)
                    OR(DR.dtEnd   BETWEEN ROL.dtStart AND  ROL.dtEnd)
            )      
		AND ROL.idSite = @idCallerSite 
		) > 0 
		OR 
		(	--Checck Outside Use and Maintenance Schedule Conflict
		SELECT COUNT(1)
        FROM tblResourceToObjectLink ROL
        CROSS JOIN @MeetingTimes DR
        WHERE 
        (idResource           =         @idResource   
		OR idResource = (SELECT idParentResource FROM tblResource WHERE idResource = @idResource)
		OR idResource IN (SELECT idResource FROM tblResource WHERE idParentResource = @idResource))
        AND idObject = 0 
        AND (  (DR.dtStart <= ROL.dtStart AND DR.dtEnd >= ROL.dtEnd)
                    OR(DR.dtStart BETWEEN ROL.dtStart AND  ROL.dtEnd)
                    OR(DR.dtEnd   BETWEEN ROL.dtStart AND  ROL.dtEnd)
            )      
		AND ROL.idSite = @idCallerSite 
		) > 0 		
		)

		
		BEGIN 
		select 'came in'
		SET @isAvailable = 0
		END
		
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO