-- =====================================================================
-- PROCEDURE: [Course.IdsForAPI]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.IdsForAPI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.IdsForAPI]
GO

/*

Returns a recordset of course ids to populate a list with no exclusions for a specific object in API.

*/

CREATE PROCEDURE [Course.IdsForAPI]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@dataType				NVARCHAR(20),
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString


	/*

	get the site's default language id

	*/

	DECLARE @idSiteLanguage INT
	SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite


	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @dataType = 'code'
			
		BEGIN

		SELECT DISTINCT
			C.idCourse
		FROM tblCourse C
		WHERE C.idSite = @idCallerSite
		AND C.coursecode = @searchParam
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)

		END
			
	ELSE

		BEGIN

		-- if the caller's language is the same as the site's language,
		-- search and display on the base table
		IF (@idCallerLanguage = @idSiteLanguage OR @callerLangString = '')
			BEGIN
				SELECT DISTINCT C.idCourse
				FROM tblCourse C
				INNER JOIN CONTAINSTABLE(tblCourse, *, @searchParam) K ON K.[key] = C.idCourse
				WHERE C.idSite = @idCallerSite
				AND (C.isDeleted IS NULL OR C.isDeleted = 0)
			END
        ELSE 
			BEGIN
				SELECT DISTINCT C.idCourse
				FROM tblCourseLanguage CL
				INNER JOIN CONTAINSTABLE(tblCourseLanguage, *, @searchParam) K ON K.[key] = CL.idCourseLanguage AND CL.idLanguage = @idCallerLanguage
				LEFT JOIN tblCourse C ON C.idCourse = CL.idCourse
				WHERE C.idSite = @idCallerSite
				AND (C.isDeleted IS NULL OR C.isDeleted = 0)
			END
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO