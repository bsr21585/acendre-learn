-- =====================================================================
-- PROCEDURE: [Group.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.SaveLang]
GO

/*

Saves group "language specific" properties for specific language
in group language table.

*/

CREATE PROCEDURE [Group.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idGroup				INT,
	@languageString			NVARCHAR(10),
	@shortDescription		NVARCHAR(512),
	@longDescription		NVARCHAR(MAX),
	@name					NVARCHAR(255),
	@searchTags				NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
		
	/*
	
	validate that the group exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblGroup
		WHERE idGroup = @idGroup
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupSaveLang_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	validate uniqueness within language
	
	*/
	
	IF( 
		SELECT COUNT(1)
		FROM tblGroup G
		LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idLanguage -- same language
		WHERE G.idSite = @idCallerSite -- same site
		AND G.idGroup <> @idGroup -- not the same group
		AND GL.name = @name -- validate parameter: name
		) > 0 
		
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'GroupSaveLang_FieldNotUnique'
		RETURN 1
		END
			
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblGroupLanguage GL WHERE GL.idGroup = @idGroup AND GL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblGroupLanguage SET
			name				= @name,
			shortDescription	= @shortDescription,
			longDescription  	= @longDescription,
			searchTags			= @searchTags
		WHERE idGroup = @idGroup
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblGroupLanguage (
			idSite,
			idGroup,
			idLanguage,
			name,
			shortDescription,
			longDescription,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idGroup,
			@idLanguage,
			@name,
			@shortDescription,
			@longDescription,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblGroupLanguage GL
			WHERE GL.idGroup = @idGroup
			AND GL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table
	note: uniqueness should already be validated

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

			UPDATE tblGroup SET
				name			 = @name,
				shortDescription = @shortDescription,
				longDescription  = @longDescription,
				searchTags		 = @searchTags
			WHERE idGroup = @idGroup

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO