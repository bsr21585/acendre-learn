-- =====================================================================
-- PROCEDURE: [Dataset.CatalogCourseInformation]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Dataset.CatalogCourseInformation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Dataset.CatalogCourseInformation]
GO

/*

Catalog Course Information Dataset

*/

CREATE PROCEDURE [Dataset.CatalogCourseInformation]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,

	@fields						NVARCHAR(MAX),
	@whereClause				NVARCHAR(MAX),
	@orderByClause				NVARCHAR(768),
	@maxRecords					INT,
	@logReportExecution			BIT
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*

	declare and assign local variables to prevent "parameter sniffing"

	*/

	DECLARE @fieldsLOC					NVARCHAR(MAX)
	DECLARE @whereClauseLOC				NVARCHAR(MAX)
	DECLARE @orderByClauseLOC			NVARCHAR(768)
	DECLARE @maxRecordsLOC				INT
	DECLARE @logReportExecutionLOC		BIT

	SET @fieldsLOC				= @fields
	SET @whereClauseLOC			= @whereClause
	SET @orderByClauseLOC		= @orderByClause
	SET @maxRecordsLOC			= @maxRecords
	SET @logReportExecutionLOC	= @logReportExecution

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
		if no fields defined, select * (all)

	*/

	IF @fieldsLOC IS NULL OR @fieldsLOC = ''
		BEGIN
		SET @fieldsLOC = '*'
		END

	/*

	get the id of the caller's language

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage =
		CASE WHEN idLanguage IS NULL THEN 
			57
		ELSE 
			idLanguage 
		END
		FROM tblLanguage WHERE code = @callerLangString

	/*

	get the group scope so we can filter on what the calling user is allowed to see
	note that NULL scope means the caller can see anything, and idCaller 1 automatically has null scope and permission to anything
	
	*/

	DECLARE @idDatasetPermission INT
	SET @idDatasetPermission = 403

	DECLARE @groupScope NVARCHAR(MAX)
	SET @groupScope = NULL

	IF (@idCaller > 1)
		BEGIN 

		CREATE TABLE #CallerRoles (idRole INT)
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT URL.idRole FROM tblUserToRoleLink URL WHERE URL.idUser = @idCaller -- directly assigned roles
		INSERT INTO #CallerRoles (idRole) SELECT DISTINCT GRL.idRole FROM tblGroupToRoleLink GRL -- group inherited roles
									      WHERE GRL.idGroup IN (SELECT DISTINCT UGL.idGroup FROM tblUserToGroupLink UGL WHERE UGL.idUser = @idCaller)
										  AND NOT EXISTS (SELECT 1 FROM #CallerRoles CR WHERE CR.idRole = GRL.idRole)
		
		-- if the caller does not have any permissions to this dataset with NULL scope, get the defined scopes
		IF (
		    SELECT COUNT(1) FROM tblRoleToPermissionLink RPL 
		    WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles)
			AND RPL.idPermission = @idDatasetPermission
			AND RPL.scope IS NULL
		   ) = 0
			BEGIN

			-- get all scope items into the group scope variable			
			DECLARE @scopeItems NVARCHAR(MAX)
			SET @groupScope = ''

			DECLARE scopeListCursor CURSOR LOCAL FOR
				SELECT scope FROM tblRoleToPermissionLink RPL WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles) AND RPL.idPermission = @idDatasetPermission AND RPL.scope IS NOT NULL

			OPEN scopeListCursor

			FETCH NEXT FROM scopeListCursor INTO @scopeItems

			WHILE @@FETCH_STATUS = 0
				BEGIN

				IF (@scopeItems <> '')
					BEGIN
					IF (@groupScope <> '')
						BEGIN
						SET @groupScope = @groupScope + ',' + @scopeItems
						END
					ELSE
						BEGIN
						SET @groupScope = @scopeItems
						END
						
					END

				FETCH NEXT FROM scopeListCursor INTO @scopeItems

				END

			-- kill the cursor
			CLOSE scopeListCursor
			DEALLOCATE scopeListCursor

			END

		END

	/*

		build sql query

	*/

	DECLARE @sql NVARCHAR(MAX)

	SET @sql = 'SELECT DISTINCT '
				+ CASE WHEN (@maxRecordsLOC IS NOT NULL AND @maxRecordsLOC > 0) THEN + ' TOP ' + convert(nvarchar, @maxRecordsLOC) + ' ' ELSE '' END
    SET @sql = @sql +  @fieldsLOC
    SET @sql = @sql +  ' FROM ('
				+ '	SELECT DISTINCT '
				+ '		CA.title as [Catalog Name], '
				+ '		CASE WHEN CA.costType = 1 THEN 0 '
				+ '		     WHEN CA.costType = 2 THEN CA.cost '
				+ '			 WHEN CA.costType = 3 THEN (SELECT SUM(CAO.cost) FROM tblCourse CAO '
				+ '									LEFT JOIN tblCourseToCatalogLink COCL ON COCL.idCourse = CAO.idCourse '
				+ '									WHERE COCL.idCatalog = CA.idCatalog ) '
				+ '			 WHEN CA.costType = 4 THEN (SELECT SUM(CAO.cost)* ((100 - CA.cost)/100) FROM tblCourse CAO  '
				+ '									LEFT JOIN tblCourseToCatalogLink COCL ON COCL.idCourse = CAO.idCourse  '
				+ '									WHERE COCL.idCatalog = CA.idCatalog ) '
				+ '		     ELSE NULL END '
				+ '		AS [Catalog Cost],  '
				+ '		CO.idSite as _idSite, '
				+ '		case when CL.title is not null then '
				+ '			CL.title  '
				+ '		else '
			    + '			CO.[title]  '
				+ '		end as [Course Name], '
				+ '		CO.title + ''_'' + case when L.[order] < 10 then ''0'' + cast(L.[order] as varchar(10)) else cast(L.[order] as varchar(10)) end as [_order_Course], '
				+ '		CO.coursecode as [Course Code],	'
				+ '		CO.cost as [Course Cost], '
				+ '		RATING.[Course Rating] as [Course Rating],	'
				+ '		RATING.[Votes Cast] as [Votes Cast],		'
				+ '		CO.credits as [Course Credits], '
				+ '		CO.isPublished as [Course Published],	'
				+ '		CO.isClosed as [Course Closed],		'
				+ '		CO.isLocked as [Course Locked],		'
				+ '		case when LLan.title is not null then '
				+ '				LLan.title '
				+ '		else '
				+ '				L.title '
				+ '		end as [Lesson Name], '
				+ '		case when (SELECT 1 FROM tblLessonToContentLink LCL WHERE L.idLesson = LCL.idLesson and LCL.idContentType = 1) IS NULL then ''0'' else ''1'' end +'
				+ '		case when (SELECT 1 FROM tblLessonToContentLink LCL WHERE L.idLesson = LCL.idLesson and LCL.idContentType = 2) IS NULL then ''0'' else ''2'' end +'
				+ '		case when (SELECT 1 FROM tblLessonToContentLink LCL WHERE L.idLesson = LCL.idLesson and LCL.idContentType = 3) IS NULL then ''0'' else ''3'' end +'
				+ '		case when (SELECT 1 FROM tblLessonToContentLink LCL WHERE L.idLesson = LCL.idLesson and LCL.idContentType = 4) IS NULL then ''0'' else ''4'' end AS [Content Type_s], '
				+ '		CP.name as [Content Package], '
				+ '		OCP.name as [OJT Content Package], '
IF (CHARINDEX('[Resource Name]', @fieldsLOC) > 0 or CHARINDEX('[Resource Name]', @whereClauseLOC) > 0)
	BEGIN
	SET @sql = @sql + ' LCLHWA.assignmentResourcePath as [Resource Name], '
	END
IF (CHARINDEX('[ILT', @fieldsLOC) > 0 or CHARINDEX('[Session', @fieldsLOC) > 0 or CHARINDEX('[ILT', @whereClauseLOC) > 0 or CHARINDEX('[Session', @whereClauseLOC) > 0)
	BEGIN
	SET @sql = @sql + ' case when SUTL.title is not null then '
				+ '			SUTL.title  '
				+ '		else '
			    + '			SUT.title  '
				+ '		end as [ILT Module], '
				+ '     case when SUTIL.title is not null then '
				+ '			SUTIL.title  '
				+ '		else '
			    + '			SUTI.title  '
				+ '		end as [Session Name], '
				+ '     SUTIM.dtStart as [Session Date/Time], '
				+ '		SUTI.seats as [Session Seats], '
				+ '		SUTI.waitingSeats as [Session Waiting Seats], '				
				+ '		ISNULL(SUTI.seats, 0) - (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink WHERE isWaitingList = 0 AND idStandupTrainingInstance = SUTI.idStandupTrainingInstance) AS [Session Seats Available], '
				+ '		INS.displayName as [Session Instructor], '
				+ '		INS.email as [Session Instructor Email], '
				+ '		SUTI.city as [Session City], '
				+ '		SUTI.province as [Session Province], '
				+ '		SU.lastname + '', '' + SU.firstName + (case when SU.middlename is not null then '' '' + SU.middlename else '''' end) as [Session Student Name], '
				+ '		SU.email as [Session Student Email], '
				+ '		SU.username as [Session Student Username], '
	END

	SET @sql = @sql + '	case when CFL.name is not null then '
				+ '			CFL.name '
				+ '		else '
				+ '			CF.name '
				+ '		end as [Certificate Name], '
				+ '		CF.issuingOrganization as [Issuing Organization], '
				+ '		CF.credits as [Certificate Credits], '
				+ '		CF.credits as [Certificate Code], '
				+ '		CF.isActive as [Certificate Status] '
				+ '	FROM tblCourse CO '
				+ ' LEFT JOIN tblCourseLanguage CL on CL.idCourse = CO.idCourse and CL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
				+ '	LEFT JOIN tblCourseToCatalogLink COCL ON COCL.idCourse = CO.idCourse '
				+ '	LEFT JOIN tblCatalog CA ON CA.idCatalog = COCL.idCatalog '
				+ ' LEFT JOIN tblCertificate CF ON CF.idObject = CO.idCourse AND (CO.isDeleted IS NULL OR CO.isDeleted = 0) AND CF.objectType = 1 AND (CF.isDeleted IS NULL OR CF.isDeleted = 0) '
				+ ' LEFT JOIN tblCertificateLanguage CFL ON CFL.idCertificate = CF.idCertificate AND CFL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '

	SET @sql = @sql + '	LEFT JOIN tblLesson L ON L.idCourse = CO.idCourse AND (L.isDeleted IS NULL OR L.isDeleted = 0) AND L.title is not null '
				+ '	LEFT JOIN tblLessonLanguage LLan ON LLan.idLesson = L.idLesson '
				+ '	LEFT JOIN tblLessonToContentLink LCP ON LCP.idLesson = L.idLesson AND LCP.idContentType = 1 '
				+ '	LEFT JOIN tblContentPackage CP ON CP.idContentPackage = LCP.idObject'
				+ '	LEFT JOIN tblLessonToContentLink OLCP ON OLCP.idLesson = L.idLesson AND OLCP.idContentType = 4 '
				+ '	LEFT JOIN tblContentPackage OCP ON OCP.idContentPackage = OLCP.idObject'
IF (CHARINDEX('[Resource Name]', @fieldsLOC) > 0 or CHARINDEX('[Resource Name]', @whereClauseLOC) > 0)
	BEGIN
	SET @sql = @sql + '	LEFT JOIN tblLessonToContentLink LCLHWA ON LCLHWA.idLesson = L.idLesson AND LCLHWA.idContentType = 3 '
	END

IF (CHARINDEX('[ILT', @fieldsLOC) > 0 or CHARINDEX('[Session', @fieldsLOC) > 0 or CHARINDEX('[ILT', @whereClauseLOC) > 0 or CHARINDEX('[Session', @whereClauseLOC) > 0)
	BEGIN
	SET @sql = @sql + '	LEFT JOIN tblLessonToContentLink LCLST ON LCLST.idLesson = L.idLesson AND LCLST.idContentType = 2 '
				+ '	LEFT JOIN tblStandUpTraining SUT ON SUT.idStandUpTraining = LCLST.idObject AND (SUT.isDeleted IS NULL OR SUT.isDeleted = 0) '
				+ '	LEFT JOIN tblStandUpTrainingLanguage SUTL ON SUTL.idStandUpTraining = SUT.idStandUpTraining and SUTL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
				+ '	LEFT JOIN tblStandUpTrainingInstance SUTI ON SUTI.idStandUpTraining = SUT.idStandUpTraining AND (SUTI.isDeleted IS NULL OR SUTI.isDeleted = 0) '
				+ '	LEFT JOIN tblStandUpTrainingInstanceLanguage SUTIL ON SUTIL.idStandUpTrainingInstance = SUTI.idStandUpTrainingInstance and SUTIL.idLanguage = ' + CONVERT(NVARCHAR, @idCallerLanguage) + ' '
				+ '	LEFT JOIN tblStandupTrainingInstanceToUserLink SULINK ON SULINK.idStandUpTrainingInstance = SUTI.idStandUpTrainingInstance '
				+ '	LEFT JOIN tblUser SU ON SU.idUser = SULINK.idUser AND (SU.isDeleted IS NULL OR SU.isDeleted = 0) '
				+ '	LEFT JOIN tblStandupTrainingInstanceToInstructorLink STII ON STII.idStandUpTrainingInstance = SUTI.idStandUpTrainingInstance '
				+ '	LEFT JOIN tblUser INS ON INS.idUser = STII.idInstructor AND (INS.isDeleted IS NULL OR INS.isDeleted = 0) '
				+ ' LEFT JOIN tblUserToGroupLink UGL on UGL.idUser = SU.idUser '
				+ '	LEFT JOIN tblStandUpTrainingInstanceMeetingTime SUTIM ON SUTIM.idStandUpTrainingInstance = SUTI.idStandUpTrainingInstance '				
	END

	SET @sql = @sql + '	LEFT JOIN (     '
				+ '		SELECT		'
				+ '			idCourse,	'
				+ '			ROUND(AVG(CAST(rating as FLOAT)), 1) as [Course Rating],		'
				+ '			COUNT(rating) as [Votes Cast]		'
				+ '		FROM tblCourseRating		'
				+ '		WHERE rating is not null	'
				+ '		GROUP BY idCourse		'
				+ '		) RATING ON RATING.idCourse = CO.idCourse		'

	SET @sql = @sql + ' WHERE (CO.isDeleted IS NULL OR CO.isDeleted = 0) '
					+ ' AND CO.idSite = ' + CONVERT(NVARCHAR, @idCallerSite) + ' '
				
	IF (CHARINDEX('[Session', @fieldsLOC) > 0 AND @groupScope IS NOT NULL AND @groupScope <> '')
		BEGIN
		SET @sql = @sql + 'AND UGL.idgroup IN (' + @groupScope + ') '
		END
							
	SET @sql = @sql + ') MAIN'   

	IF @whereClauseLOC IS NOT NULL AND @whereClauseLOC <> ''
		BEGIN
		SET @sql = @sql + ' WHERE ' + @whereClauseLOC
		END

	IF @orderByClauseLOC IS NOT NULL AND @orderByClauseLOC <> ''
		BEGIN
		
		-- for security reasons, we will always pass @orderByClauseLOC with a trailing comma, remove that comma
		-- this will prevent another sql statement from being attached to this query
		SET @orderByClauseLOC = LEFT(@orderByClauseLOC, LEN(@orderByClauseLOC) - 1)

		SET @sql = @sql + ' ORDER BY ' + @orderByClauseLOC
		END

	/*

	execute the sql statement and log its execution

	*/

	DECLARE @dtExecutionBegin DATETIME
	DECLARE @dtExecutionEnd DATETIME
	DECLARE @executionDurationMS INT

	SET @dtExecutionBegin = GETUTCDATE()

	EXEC sp_executesql @sql

	SET @dtExecutionEnd = GETUTCDATE()

	SET @executionDurationMS = DATEDIFF(ms, @dtExecutionBegin, @dtExecutionEnd)

	IF (@logReportExecutionLOC = 1)
	BEGIN
		INSERT INTO tblReportProcessorLog
		(
			idSite,
			idCaller,
			datasetProcedure,
			dtExecutionBegin,
			dtExecutionEnd,
			executionDurationSeconds,
			sqlQuery
		)
		SELECT
			@idCallerSite,
			@idCaller,
			'[Dataset.CatalogCourseInformation]',
			@dtExecutionBegin,
			@dtExecutionEnd,
			CAST((CAST(@executionDurationMS AS DECIMAL)/1000) AS DECIMAL(9,2)),
			@sql
	END
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
