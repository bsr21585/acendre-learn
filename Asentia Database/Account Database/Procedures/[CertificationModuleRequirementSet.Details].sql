-- =====================================================================
-- PROCEDURE: [CertificationModuleRequirementSet.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificationModuleRequirementSet.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModuleRequirementSet.Details]
GO

/*

Return all the properties for a given certification module requirement set id.

*/

CREATE PROCEDURE [CertificationModuleRequirementSet.Details]
(
	@Return_Code									INT				OUTPUT,
	@Error_Description_Code							NVARCHAR(50)	OUTPUT,
	@idCallerSite									INT				= 0, --default if not specified
	@callerLangString								NVARCHAR(10),
	@idCaller										INT				= 0,
	
	@idCertificationModuleRequirementSet			INT				OUTPUT,
	@idCertificationModule							INT				OUTPUT,
	@idSite											INT				OUTPUT,
	@isAny											BIT				OUTPUT,
	@label											NVARCHAR(255)	OUTPUT	
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificationModuleRequirementSet
		WHERE idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleRequirementSetDetails_NoRecordFound'
		RETURN 1
	    END

	/*
	
	get the data 
	
	*/
		
	SELECT
		@idCertificationModuleRequirementSet	= CMRS.idCertificationModuleRequirementSet,
		@idCertificationModule					= CMRS.idCertificationModule,
		@idSite									= CMRS.idSite,
		@isAny									= CMRS.isAny,
		@label									= CMRS.label
	FROM tblCertificationModuleRequirementSet CMRS
	WHERE CMRS.idCertificationModuleRequirementSet = @idCertificationModuleRequirementSet
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleRequirementSetDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO