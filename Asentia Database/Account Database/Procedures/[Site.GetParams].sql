-- =====================================================================
-- PROCEDURE: [Site.GetParams]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Site.GetParams]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.GetParams]
GO

/*
Return all the parameters for a given site id.
*/
CREATE PROCEDURE [Site.GetParams]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idSite					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSite
		WHERE idSite = @idSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'SiteGetParams_DetailsNotFound'
		RETURN 1
		END
	
	SELECT 
		CASE WHEN SP.[idSiteParam] IS NOT NULL THEN SP.[param] ELSE DSP.[param] END AS [param], 
		CASE WHEN SP.[idSiteParam] IS NOT NULL THEN SP.[value] ELSE DSP.[value] END AS [value]
	FROM tblSiteParam DSP
	LEFT JOIN tblSiteParam SP ON SP.[param] = DSP.[param] AND SP.idSite = @idSite
	WHERE DSP.idSite = 1
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO