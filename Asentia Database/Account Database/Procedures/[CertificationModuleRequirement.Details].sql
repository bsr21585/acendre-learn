-- =====================================================================
-- PROCEDURE: [CertificationModuleRequirement.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificationModuleRequirement.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModuleRequirement.Details]
GO

/*

Return all the properties for a given certification module requirement id.

*/

CREATE PROCEDURE [CertificationModuleRequirement.Details]
(
	@Return_Code									INT				OUTPUT,
	@Error_Description_Code							NVARCHAR(50)	OUTPUT,
	@idCallerSite									INT				= 0, --default if not specified
	@callerLangString								NVARCHAR(10),
	@idCaller										INT				= 0,
	
	@idCertificationModuleRequirement				INT				OUTPUT,
	@idCertificationModuleRequirementSet			INT				OUTPUT,
	@idSite											INT				OUTPUT, 	
	@label											NVARCHAR(255)	OUTPUT,
	@shortDescription								NVARCHAR(512)	OUTPUT,
	@requirementType								INT				OUTPUT,
	@courseCompletionIsAny							BIT				OUTPUT, 
	@forceCourseCompletionInOrder					BIT				OUTPUT,
	@numberCreditsRequired							FLOAT			OUTPUT,
	@documentUploadFileType							INT				OUTPUT,
	@documentationApprovalRequired					BIT				OUTPUT,
	@allowSupervisorsToApproveDocumentation			BIT				OUTPUT,
	@allowExpertsToApproveDocumentation				BIT				OUTPUT,
	@courseCreditEligibleCoursesIsAll				BIT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificationModuleRequirement
		WHERE idCertificationModuleRequirement = @idCertificationModuleRequirement
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleRequirementDetails_NoRecordFound'
		RETURN 1
	    END

	/*
	
	get the data 
	
	*/
		
	SELECT
		@idCertificationModuleRequirement		= CMR.idCertificationModuleRequirement,
		@idCertificationModuleRequirementSet	= CMR.idCertificationModuleRequirementSet,
		@idSite									= CMR.idSite,		
		@label									= CMR.label,
		@shortDescription						= CMR.shortDescription,
		@requirementType						= CMR.requirementType,
		@courseCompletionIsAny					= CMR.courseCompletionIsAny,
		@forceCourseCompletionInOrder			= CMR.forceCourseCompletionInOrder,
		@numberCreditsRequired					= CMR.numberCreditsRequired,
		@documentUploadFileType					= CMR.documentUploadFileType,
		@documentationApprovalRequired			= CMR.documentationApprovalRequired,
		@allowSupervisorsToApproveDocumentation	= CMR.allowSupervisorsToApproveDocumentation,
		@allowExpertsToApproveDocumentation		= CMR.allowExpertsToApproveDocumentation,
		@courseCreditEligibleCoursesIsAll		= CMR.courseCreditEligibleCoursesIsAll			
	FROM tblCertificationModuleRequirement CMR
	WHERE CMR.idCertificationModuleRequirement = @idCertificationModuleRequirement
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationModuleRequirementDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO