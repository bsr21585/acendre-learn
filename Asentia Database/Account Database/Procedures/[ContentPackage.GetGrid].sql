-- =====================================================================
-- PROCEDURE: [ContentPackage.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ContentPackage.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ContentPackage.GetGrid]
GO

/*

Gets a listing of Content Packages.

*/

CREATE PROCEDURE [ContentPackage.GetGrid]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@searchParam				NVARCHAR(4000),
	@pageNum					INT, 
	@pageSize					INT, 
	@orderColumn				NVARCHAR(255),
	@orderAsc					BIT,
	@contentPackageOriginType	NVARCHAR(10)	= NULL
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblContentPackage CP
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND CP.idSite = @idCallerSite)
				)
				AND
				(
				(@contentPackageOriginType IS NULL) 
				OR 
				(@contentPackageOriginType = 'scorm' AND CP.idContentPackageType = 1 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL)
				OR
				(@contentPackageOriginType = 'xapi' AND CP.idContentPackageType = 2 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL)
				OR
				(@contentPackageOriginType = 'quizsurvey' AND CP.idContentPackageType = 1 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NOT NULL)
				OR
				(@contentPackageOriginType = 'video' AND CP.idContentPackageType = 1 AND CP.idMediaType = 1 AND CP.idQuizSurvey IS NULL)
				OR
				(@contentPackageOriginType = 'powerpoint' AND CP.idContentPackageType = 1 AND CP.idMediaType = 2 AND CP.idQuizSurvey IS NULL)
				OR
				(@contentPackageOriginType = 'pdf' AND CP.idContentPackageType = 1 AND CP.idMediaType = 3 AND CP.idQuizSurvey IS NULL)
				)
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						CP.idContentPackage,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN CP.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'size' THEN CP.kb END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'contentPackageType' THEN CP.idContentPackageType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtModified' THEN CP.dtModified END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN CP.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'size' THEN CP.kb END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'contentPackageType' THEN CP.idContentPackageType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtModified' THEN CP.dtModified END) END
						)
						AS [row_number]
					FROM tblContentPackage CP
					WHERE
						(
						(@idCallerSite IS NULL)
						OR 
						(@idCallerSite IS NOT NULL AND CP.idSite = @idCallerSite)
						)
						AND
						(
						(@contentPackageOriginType IS NULL) 
						OR 
						(@contentPackageOriginType = 'scorm' AND CP.idContentPackageType = 1 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL)
						OR
						(@contentPackageOriginType = 'xapi' AND CP.idContentPackageType = 2 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL)
						OR
						(@contentPackageOriginType = 'quizsurvey' AND CP.idContentPackageType = 1 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NOT NULL)
						OR
						(@contentPackageOriginType = 'video' AND CP.idContentPackageType = 1 AND CP.idMediaType = 1 AND CP.idQuizSurvey IS NULL)
						OR
						(@contentPackageOriginType = 'powerpoint' AND CP.idContentPackageType = 1 AND CP.idMediaType = 2 AND CP.idQuizSurvey IS NULL)
						OR
						(@contentPackageOriginType = 'pdf' AND CP.idContentPackageType = 1 AND CP.idMediaType = 3 AND CP.idQuizSurvey IS NULL)
						)
				),
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idContentPackage,
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				CP.idContentPackage,
				CPT.name AS contentPackageType,
				CP.name,
				CASE WHEN CP.kb IS NOT NULL THEN
					CASE WHEN CP.kb > 1024 THEN
						CASE WHEN CP.kb > 1048576 THEN
							CONVERT(NVARCHAR, ROUND(CAST(1.0*CP.kb/(1048576) AS DECIMAL(6,2)), 2, 1)) + ' GB'
						ELSE
							CONVERT(NVARCHAR, ROUND(CAST(1.0*CP.kb/(1024) AS DECIMAL(6,2)), 2, 1)) + ' MB'
						END
					ELSE
						CONVERT(NVARCHAR, ROUND(CP.kb, 2, 1)) + ' KB'
					END
				ELSE
					NULL
				END AS size,
				CP.dtModified,
				CASE WHEN (
						   SELECT COUNT(1) FROM tblLessonToContentLink LCL 
						   LEFT JOIN tblLesson L ON L.idLesson = LCL.idLesson
						   LEFT JOIN tblCourse C ON C.idCourse = L.idCourse
						   WHERE LCL.idSite = @idCallerSite
						   AND (LCL.idContentType = 1 OR LCL.idContentType = 4)
						   AND LCL.idObject = CP.idContentPackage 
						   AND (L.isDeleted IS NULL OR L.isDeleted = 0)
						   AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						  ) > 0 THEN
					CONVERT(BIT, 1)
				ELSE
					CONVERT(BIT, 0)
				END AS isPackageInUse,
				CP.isMediaUpload,
				CP.idMediaType,
				CP.isProcessed,
				CP.idQuizSurvey,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblContentPackage CP ON CP.idContentPackage = SelectedKeys.idContentPackage
			LEFT JOIN tblContentPackageType CPT ON CPT.idContentPackageType = CP.idContentPackageType
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblContentPackage CP
			INNER JOIN CONTAINSTABLE(tblContentPackage, *, @searchParam) K ON K.[key] = CP.idContentPackage
			WHERE
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND CP.idSite = @idCallerSite)
				)
				AND
				(
				(@contentPackageOriginType IS NULL) 
				OR 
				(@contentPackageOriginType = 'scorm' AND CP.idContentPackageType = 1 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL)
				OR
				(@contentPackageOriginType = 'xapi' AND CP.idContentPackageType = 2 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL)
				OR
				(@contentPackageOriginType = 'quizsurvey' AND CP.idContentPackageType = 1 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NOT NULL)
				OR
				(@contentPackageOriginType = 'video' AND CP.idContentPackageType = 1 AND CP.idMediaType = 1 AND CP.idQuizSurvey IS NULL)
				OR
				(@contentPackageOriginType = 'powerpoint' AND CP.idContentPackageType = 1 AND CP.idMediaType = 2 AND CP.idQuizSurvey IS NULL)
				OR
				(@contentPackageOriginType = 'pdf' AND CP.idContentPackageType = 1 AND CP.idMediaType = 3 AND CP.idQuizSurvey IS NULL)
				)
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						CP.idContentPackage,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN CP.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'size' THEN CP.kb END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'contentPackageType' THEN CP.idContentPackageType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtModified' THEN CP.dtModified END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN CP.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'size' THEN CP.kb END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'contentPackageType' THEN CP.idContentPackageType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtModified' THEN CP.dtModified END) END
						)
						AS [row_number]
					FROM tblContentPackage CP
					INNER JOIN CONTAINSTABLE(tblContentPackage, *, @searchParam) K ON K.[key] = CP.idContentPackage
					WHERE 
						(
						@idCallerSite IS NULL
						OR 
						@idCallerSite IS NOT NULL AND CP.idSite = @idCallerSite
						)
						AND
						(
						(@contentPackageOriginType IS NULL) 
						OR 
						(@contentPackageOriginType = 'scorm' AND CP.idContentPackageType = 1 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL)
						OR
						(@contentPackageOriginType = 'xapi' AND CP.idContentPackageType = 2 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL)
						OR
						(@contentPackageOriginType = 'quizsurvey' AND CP.idContentPackageType = 1 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NOT NULL)
						OR
						(@contentPackageOriginType = 'video' AND CP.idContentPackageType = 1 AND CP.idMediaType = 1 AND CP.idQuizSurvey IS NULL)
						OR
						(@contentPackageOriginType = 'powerpoint' AND CP.idContentPackageType = 1 AND CP.idMediaType = 2 AND CP.idQuizSurvey IS NULL)
						OR
						(@contentPackageOriginType = 'pdf' AND CP.idContentPackageType = 1 AND CP.idMediaType = 3 AND CP.idQuizSurvey IS NULL)
						)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idContentPackage, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				CP.idContentPackage,
				CPT.name AS contentPackageType,
				CP.name,
				CASE WHEN CP.kb IS NOT NULL THEN
					CASE WHEN CP.kb > 1024 THEN
						CASE WHEN CP.kb > 1048576 THEN
							CONVERT(NVARCHAR, ROUND(CAST(1.0*CP.kb/(1048576) AS DECIMAL(6,2)), 2, 1)) + ' GB'
						ELSE
							CONVERT(NVARCHAR, ROUND(CAST(1.0*CP.kb/(1024) AS DECIMAL(6,2)), 2, 1)) + ' MB'
						END
					ELSE
						CONVERT(NVARCHAR, ROUND(CP.kb, 2, 1)) + ' KB'
					END
				ELSE
					NULL
				END AS size,
				CP.dtModified,  
				CASE WHEN (
						   SELECT COUNT(1) FROM tblLessonToContentLink LCL 
						   LEFT JOIN tblLesson L ON L.idLesson = LCL.idLesson
						   LEFT JOIN tblCourse C ON C.idCourse = L.idCourse
						   WHERE LCL.idSite = @idCallerSite
						   AND (LCL.idContentType = 1 OR LCL.idContentType = 4)
						   AND LCL.idObject = CP.idContentPackage 
						   AND (L.isDeleted IS NULL OR L.isDeleted = 0)
						   AND (C.isDeleted IS NULL OR C.isDeleted = 0)
						  ) > 0 THEN
					CONVERT(BIT, 1)
				ELSE
					CONVERT(BIT, 0)
				END AS isPackageInUse,
				CP.isMediaUpload,
				CP.idMediaType,
				CP.isProcessed,
				CP.idQuizSurvey,
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblContentPackage CP ON CP.idContentPackage = SelectedKeys.idContentPackage
			LEFT JOIN tblContentPackageType CPT ON CPT.idContentPackageType = CP.idContentPackageType
			ORDER BY SelectedKeys.[row_number]
			
			END
			
	END