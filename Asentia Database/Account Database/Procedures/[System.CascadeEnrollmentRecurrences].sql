-- =====================================================================
-- PROCEDURE: [System.CascadeEnrollmentRecurrences]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CascadeEnrollmentRecurrences]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CascadeEnrollmentRecurrences]
GO

/*

Cascades enrollment recurrences to users. 

*/

CREATE PROCEDURE [System.CascadeEnrollmentRecurrences]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON

	/*

	declare and set utcNow

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	create temporary tables to work with

	*/

	CREATE TABLE #RecurringRuleSetEnrollments (
		idRuleSetEnrollment				INT,
		idSite							INT,
		idCourse						INT,
		isLockedByPrerequisites			BIT,
		idTimezone						INT,
		dtStart							DATETIME,
		dtEnd							DATETIME,
		dueInterval						INT,
		dueTimeframe					NVARCHAR(4),
		recurInterval					INT,
		recurTimeframe					NVARCHAR(4),
		expiresFromStartInterval		INT,
		expiresFromStartTimeframe		NVARCHAR(4),
		expiresFromFirstLaunchInterval	INT,
		expiresFromFirstLaunchTimeframe	NVARCHAR(4)
	)

	CREATE TABLE #RecurrencesToCascade (
		idEnrollment			INT,
		idRuleSetEnrollment		INT,
		idCourse				INT,
		idUser					INT,
		lastDtStart				DATETIME,
		nextDtStart				DATETIME
	)

	/*

	get all of the recurring ruleset enrollments that are still within their lifespan

	*/

	INSERT INTO #RecurringRuleSetEnrollments (
		idRuleSetEnrollment,
		idSite,
		idCourse,
		isLockedByPrerequisites,
		idTimezone,
		dtStart,
		dtEnd,
		dueInterval,
		dueTimeframe,
		recurInterval,
		recurTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT
		idRuleSetEnrollment,
		idSite,
		idCourse,
		isLockedByPrerequisites,
		idTimezone,
		dtStart,
		dtEnd,
		dueInterval,
		dueTimeframe,
		recurInterval,
		recurTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	FROM tblRuleSetEnrollment
	WHERE recurInterval IS NOT NULL
	AND recurTimeframe IS NOT NULL
	AND dtStart <= @utcNow
	AND (dtEnd IS NULL OR dtEnd > @utcNow)

	/*

	get the list of enrollments that are due to have their next recurrence cascaded

	*/

	INSERT INTO #RecurrencesToCascade (
		idEnrollment,
		idRuleSetEnrollment,
		idCourse,
		idUser,
		lastDtStart,
		nextDtStart
	)
	SELECT
		E.idEnrollment,
		E.idRuleSetEnrollment,
		E.idCourse,
		E.idUser,
		E.dtStart AS lastDtStart,
		dbo.IDateAdd(RRSE.recurTimeframe, RRSE.recurInterval, E.dtStart) AS nextDtStart
	FROM (
			SELECT
				idEnrollment,
				idRulesetEnrollment,
				idCourse,
				idUser,
				dtStart,
				ROW_NUMBER() OVER(PARTITION BY idUser, idCourse, idRuleSetEnrollment ORDER BY dtStart DESC) AS ordinal -- this helps us determine the latest recurrence by dtStart
			FROM tblEnrollment
			WHERE idRuleSetEnrollment IN (SELECT idRuleSetEnrollment FROM #RecurringRuleSetEnrollments)
		 ) AS E
	LEFT JOIN #RecurringRuleSetEnrollments RRSE ON RRSE.idRuleSetEnrollment = E.idRuleSetEnrollment
	LEFT JOIN tblUser U ON U.idUser = E.idUser
	WHERE ordinal = 1																	-- this selects the last recurrence by dtStart
	AND dbo.IDateAdd(RRSE.recurTimeframe, RRSE.recurInterval, E.dtStart) <= @utcNow		-- where the next start date is not in the future
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- only non-deleted and non-pending users
	-- AND (U.dtExpires IS NULL OR U.dtExpires > @utcNow)								-- commented to allow cascade to expired users, they may be active again in the future so keep up with the cascading
	-- AND U.isActive = 1																-- commented to allow cascade to inactive users, they may be active again in the future so keep up with the cascading
	ORDER BY nextDtStart, E.idRuleSetEnrollment, E.idUser

	/*

	cascade the recurrences

	*/

	INSERT INTO tblEnrollment (
		idSite,
		idCourse,
		revcode,
		idUser,
		idRuleSetEnrollment,
		isLockedByPrerequisites,
		idTimezone,
		dtCreated,
		dtStart,
		dtDue,
		dueInterval,
		dueTimeframe,
		title,
		code,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		dtExpiresFromStart
	)
	SELECT
		RRSE.idSite,
		RRSE.idCourse,
		C.revcode,
		RTC.idUser,
		RRSE.idRuleSetEnrollment,
		RRSE.isLockedByPrerequisites,
		RRSE.idTimezone,
		@utcNow,
		RTC.nextDtStart,
		dbo.IDateAdd(RRSE.dueTimeframe, RRSE.dueInterval, RTC.nextDtStart),
		RRSE.dueInterval,
		RRSE.dueTimeframe,
		C.title,
		C.coursecode,
		RRSE.expiresFromFirstLaunchInterval,
		RRSE.expiresFromFirstLaunchTimeframe,
		RRSE.expiresFromStartInterval,
		RRSE.expiresFromStartTimeframe,
		dbo.IDateAdd(RRSE.expiresFromStartTimeframe, RRSE.expiresFromStartInterval, RTC.nextDtStart)
	FROM #RecurrencesToCascade RTC
	LEFT JOIN #RecurringRuleSetEnrollments RRSE ON RRSE.idRuleSetEnrollment = RTC.idRuleSetEnrollment
	LEFT JOIN tblCourse C ON C.idCourse = RRSE.idCourse
	WHERE NOT EXISTS ( -- do this to prevent duplicates of the same recurrence
		SELECT 1 FROM tblEnrollment E
		WHERE RRSE.idCourse = E.idCourse						-- same course
		AND RTC.idUser = E.idUser								-- same user
		AND RRSE.idRuleSetEnrollment = E.idRuleSetEnrollment	-- same ruleset enrollment
		AND RTC.nextDtStart = E.dtStart							-- same start date
	)

	/*

	do the event log entries

	*/

	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		E.idSite,
		RTC.idCourse,
		E.idEnrollment, 
		RTC.idUser
	FROM #RecurrencesToCascade RTC
	LEFT JOIN tblEnrollment E ON E.idCourse = RTC.idCourse AND E.idUser = RTC.idUser AND E.idRuleSetEnrollment = RTC.idRuleSetEnrollment AND E.dtStart = RTC.nextDtStart
	WHERE E.dtCreated = @utcNow

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 201, @utcNow, @eventLogItems

	/*

	drop the temporary tables

	*/

	DROP TABLE #RecurringRuleSetEnrollments
	DROP TABLE #RecurrencesToCascade

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO