-- =====================================================================
-- PROCEDURE: [Course.IdsAndNamesNotMembersOfAnyCatalog]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.IdsAndNamesNotMembersOfAnyCatalog]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.IdsAndNamesNotMembersOfAnyCatalog]
GO

/*

Returns a recordset of course ids and titles that are not members of any catalog.

*/

CREATE PROCEDURE [Course.IdsAndNamesNotMembersOfAnyCatalog]
(
	@Return_Code				INT					OUTPUT,
	@Error_Description_Code		NVARCHAR(50)		OUTPUT,
	@idCallerSite				INT					= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT					= 0,

	@searchParam				NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString
	
	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT
			C.idCourse, 
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title,
			C.avatar
		FROM tblCourse C
		JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		WHERE C.idSite = @idCallerSite
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)
		AND C.isPublished = 1
		AND C.idCourse NOT IN (SELECT DISTINCT CTCL.idCourse
							   FROM tblCourseToCatalogLink CTCL
							   WHERE CTCL.idSite = @idCallerSite)
		ORDER BY [order]

		END
			
	ELSE

		BEGIN

		SELECT
			C.idCourse, 
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title,
			C.avatar
		FROM tblCourseLanguage CL
		INNER JOIN CONTAINSTABLE(tblCourseLanguage, *, @searchParam) K ON K.[key] = CL.idCourseLanguage AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCourse C ON C.idCourse = CL.idCourse
		WHERE C.idSite = @idCallerSite
		AND (C.isDeleted IS NULL OR C.isDeleted = 0)
		AND C.isPublished = 1
		AND C.idCourse NOT IN (SELECT DISTINCT CTCL.idCourse
							   FROM tblCourseToCatalogLink CTCL
							   WHERE CTCL.idSite = @idCallerSite)
		ORDER BY [order]

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO	