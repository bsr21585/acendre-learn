-- =====================================================================
-- PROCEDURE: [TransactionItem.ClearAllUnconfirmedForPurchase]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.ClearAllUnconfirmedForPurchase]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.ClearAllUnconfirmedForPurchase]
GO

/*

Clears all unconfirmed transaction items for a purchase.

Used when the processor is PayPal and Buy Now is clicked for an item. This is because PayPal 
does not have a cart per se. So we need to ensure only one item is linked to a purchase and nothing
stale is left behind. This is really a safeguard for if someone doesn't follow through with a PayPal 
purchase, and tries to purchase something else prior to us cleaning up the PayPal active carts. It's
necessary since we have no control over what happens when they are taken to the PayPal payment page.

*/

CREATE PROCEDURE [TransactionItem.ClearAllUnconfirmedForPurchase]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idPurchase				INT
)
AS	

	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END		
			 
	/*
	
	delete all unconfirmed transaction items for the purchase
	this should effectively be everything for that purchase, since we are only passing "active carts"
	
	*/

	DELETE FROM tblTransactionItem
	WHERE idPurchase = @idPurchase
	AND (confirmed IS NULL OR confirmed = 0)	
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO