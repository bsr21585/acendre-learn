-- =====================================================================
-- PROCEDURE: [User.IdsAndNamesForEnrollmentApproversSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.IdsAndNamesForEnrollmentApproversSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.IdsAndNamesForEnrollmentApproversSelectList]
GO

/*

Returns a recordset of user ids and names to polulate
"Approvers" select list for course.

*/

CREATE PROCEDURE [User.IdsAndNamesForEnrollmentApproversSelectList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCourse				INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT TOP 1000
			U.idUser, 
			U.displayName + ' (' + U.username + ')' AS displayName
		FROM tblUser U
		WHERE U.idSite = @idCallerSite
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
		AND NOT EXISTS (SELECT 1 FROM tblCourseEnrollmentApprover CEA
						WHERE CEA.idCourse = @idCourse
						AND CEA.idUser = U.idUser)
		ORDER BY displayName

		END

	ELSE

		BEGIN

		SELECT DISTINCT TOP 1000
			U.idUser, 
			U.displayName + ' (' + U.username + ')' AS displayName
		FROM tblUser U
		INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = U.idUser
		WHERE U.idSite = @idCallerSite
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
		AND NOT EXISTS (SELECT 1 FROM tblCourseEnrollmentApprover CEA
						WHERE CEA.idCourse = @idCourse
						AND CEA.idUser = U.idUser)
		ORDER BY displayName

		END
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	