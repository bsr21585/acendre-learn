-- =====================================================================
-- PROCEDURE: [DocumentRepositoryItem.GetObjectDocumentsInFolder]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DocumentRepositoryItem.GetObjectDocumentsInFolder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DocumentRepositoryItem.GetObjectDocumentsInFolder]
GO

/*
Return all the  documents for a particulare object
*/
CREATE PROCEDURE [dbo].[DocumentRepositoryItem.GetObjectDocumentsInFolder]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (255)	OUTPUT,
	@idCaller					INT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR (10),

	@idObject					INT,
	@idObjectType				INT,
	@idDocumentRepositoryFolder	INT = NULL
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	

	/*
	
	validate that the specified language exists, use site default if not
	
	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	IF @idCallerLanguage IS NULL
	SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		DRI.idDocumentRepositoryItem,
		DRI.[filename],
		DRI.idDocumentRepositoryFolder,
		CASE WHEN DRIL.label IS NOT NULL THEN DRIL.label ELSE DRI.label END AS [label]
	FROM tblDocumentRepositoryItem DRI
	LEFT JOIN tblDocumentRepositoryItemLanguage DRIL ON DRIL.idDocumentRepositoryItem = DRI.idDocumentRepositoryItem AND DRIL.idLanguage = @idCallerLanguage
	LEFT JOIN tblDocumentRepositoryFolder DRF ON DRF.idDocumentRepositoryFolder = DRI.idDocumentRepositoryFolder
	WHERE	DRI.idObject	= @idObject
	AND		DRI.idDocumentRepositoryObjectType	= @idObjectType
	AND		((@idDocumentRepositoryFolder IS NULL AND DRI.idDocumentRepositoryFolder IS NULL) OR DRI.idDocumentRepositoryFolder = @idDocumentRepositoryFolder)
	AND		(DRI.isDeleted IS NULL OR DRI.isDeleted = 0)
	AND		((DRI.isAllLanguages = 1 OR DRI.idLanguage IS NULL) OR DRI.idLanguage = @idCallerLanguage)
		
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO