-- =====================================================================
-- PROCEDURE: [StandupTraining.SaveAvatar]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.SaveAvatar]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.SaveAvatar]
GO

/*

Saves a standup training's avatar path to the database.

*/

CREATE PROCEDURE [StandupTraining.SaveAvatar]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idStandupTraining		INT, 
	@avatar					NVARCHAR (255)
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	update the standup training's avatar path

	*/
		
	-- check that the standup training id exists
	IF (SELECT COUNT(1) FROM tblStandUpTraining WHERE idStandUpTraining = @idStandupTraining AND idSite = @idCallerSite) < 1
		BEGIN                               
			
		SET @idStandupTraining = @idStandupTraining
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'StandupTrainingSaveAvatar_NoRecordFound'
		RETURN 1
			
		END
			
		
	-- update existing course's properties
		
	UPDATE tblStandUpTraining SET
		avatar = @avatar
	WHERE idStandUpTraining = @idStandupTraining

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END			

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO