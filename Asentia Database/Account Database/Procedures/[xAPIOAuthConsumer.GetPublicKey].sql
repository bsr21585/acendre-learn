-- =====================================================================
-- PROCEDURE: [xAPIOAuthConsumer.GetPublicKey]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[xAPIOAuthConsumer.GetPublicKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [xAPIOAuthConsumer.GetPublicKey]
GO

/*

Authenticates credentials for login to the system.

*/
CREATE PROCEDURE [xAPIOAuthConsumer.GetPublicKey]
(	
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,
	
	@PublicKey				nvarchar(MAX)	OUTPUT,
	@oAuthKey				NVARCHAR(200)
)
AS
	BEGIN
	SET NOCOUNT ON
			
		SELECT @PublicKey = oAuthConsumer.oAuthSecret
		FROM tblxAPIOAuthConsumer oAuthConsumer
		WHERE oAuthConsumer.oAuthKey = @oAuthKey
		AND oAuthConsumer.idSite = @idCallerSite
		
		IF (@PublicKey IS null)
			BEGIN
				SET @Return_Code = 1
				SET @Error_Description_Code = 'xAPIOAuthConsumerGetPublicKey_PublicKeyNotFound'
			END
			
		ELSE
			BEGIN
				SET @Return_Code = 0 --(0 is 'success') 
			END		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
