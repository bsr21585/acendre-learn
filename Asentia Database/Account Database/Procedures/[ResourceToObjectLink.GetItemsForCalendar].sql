-- =====================================================================
-- PROCEDURE: [ResourceToObjectLink.GetItemsForCalendar]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ResourceToObjectLink.GetItemsForCalendar]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ResourceToObjectLink.GetItemsForCalendar]
GO

/*

Returns a listing of items coming up on the calendar.

*/

CREATE PROCEDURE [ResourceToObjectLink.GetItemsForCalendar]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idResource             INT
)
AS
	
	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

		
	/*
	
	get the user's language
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	/* 
	
	get the calendar items

	*/

	SELECT
		id as idResourceToObject,
		'resourceBooked' AS [event],
		RO.idObject,
		RO.objectType,
		RO.dtStart,
		RO.dtEnd,
		RO.isOutsideUse,
		RO.isMaintenance
	FROM tblResourceToObjectLink RO
	LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandUpTrainingInstance = RO.idObject
	LEFT JOIN tblResource R ON R.idResource = RO.idResource AND (R.isDeleted IS NULL OR R.isDeleted = 0)
	LEFT JOIN tblResourceLanguage RL ON RL.idResource = R.idResource AND RL.idLanguage = @idCallerLanguage
	WHERE (RO.idResource = @idResource 
	OR RO.idResource=(select idParentResource from tblResource where idResource=@idResource AND idSite = @idCallerSite	))
	-- UNION SELECT OTHER OBJECTS

	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
