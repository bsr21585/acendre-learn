
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventEmailNotification.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventEmailNotification.Details]
GO

/*
Return all the properties for a given id.
*/
CREATE PROCEDURE [EventEmailNotification.Details]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	
	@idEventEmailNotification	INT				OUTPUT,
	@idSite						INT				OUTPUT,
	@name						NVARCHAR(255)	OUTPUT,
	@idEventType				INT				OUTPUT,
	@idEventTypeRecipient		INT				OUTPUT,
	@idObject					INT				OUTPUT,
	@from						NVARCHAR(255)	OUTPUT,
	@copyTo						NVARCHAR(255)	OUTPUT,
	@specificEmailAddress		NVARCHAR(255)	OUTPUT,
	@isActive					BIT				OUTPUT,
	@dtActivation				DATETIME		OUTPUT,
	@interval					INT				OUTPUT,
	@timeFrame					NVARCHAR(4)		OUTPUT,
	@priority					INT				OUTPUT,
	@isHTMLBased				BIT				OUTPUT,
	@attachmentType				INT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblEventEmailNotification
		WHERE idEventEmailNotification = @idEventEmailNotification
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EventEmailNotificationDetails_NoRecordFound'
		RETURN 1
		END
	
	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	/*
	
	get the data 
	
	*/
	
	SELECT
		@idEventEmailNotification	= E.idEventEmailNotification,
		@idSite						= E.idSite,
		@name						= CASE WHEN EL.[name] IS NULL OR EL.[name] = '' THEN E.[name] ELSE EL.[name] END,
		@idEventType				= E.idEventType,
		@idEventTypeRecipient		= E.idEventTypeRecipient,
		@idObject					= E.idObject,
		@from						= E.[from],
		@copyTo						= E.copyTo,
		@specificEmailAddress		= E.specificEmailAddress,
		@isActive					= E.isActive,
		@dtActivation				= E.dtActivation,
		@interval					= E.interval,
		@timeFrame					= E.timeFrame,
		@priority					= E.priority,
		@isHTMLBased				= E.isHTMLBased,
		@attachmentType				= E.attachmentType
	FROM tblEventEmailNotification E
	LEFT JOIN tblEventEmailNotificationLanguage EL ON E.idEventEmailNotification = EL.idEventEmailNotification AND EL.idLanguage = @idCallerLanguage
	WHERE E.idEventEmailNotification = @idEventEmailNotification
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EventEmailNotificationDetails_NoRecordFound'
		END
	ELSE
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO