-- =====================================================================
-- PROCEDURE: [ExceptionLog.GetGrid]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[ExceptionLog.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ExceptionLog.GetGrid]
GO

/*
Gets a listing from the exception log.
*/
CREATE PROCEDURE [ExceptionLog.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idSite					INT, 
	@searchParam			NVARCHAR(255),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblExceptionLog
			WHERE 
				(
				(@idSite IS NULL) 
				OR 
				(@idSite IS NOT NULL AND idSite = @idSite)
				)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idExceptionLog,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END
						)
						AS [row_number]
					FROM tblExceptionLog
					WHERE 
						(
						(@idSite IS NULL) 
						OR 
						(@idSite IS NOT NULL AND idSite = @idSite)
						)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idExceptionLog, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				EXL.idExceptionLog, 
				CONVERT(VARCHAR(12), EXL.[timestamp], 107) AS [timestamp],
				LTRIM(right(CONVERT(VARCHAR(25), EXL.[timestamp], 100), 7)) AS [time],		
				CASE WHEN EXL.idSite IS NULL THEN '[N/A]' ELSE S.hostname END AS [site],
				CASE WHEN EXL.idUser IS NULL THEN '[N/A]' ELSE U.displayName + '(' + U.username + ')' END AS [user],
				EXL.page,
				EXL.exceptionType,
				EXL.exceptionMessage,
				EXL.exceptionStackTrace,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblExceptionLog EXL ON EXL.idExceptionLog = SelectedKeys.idExceptionLog
			LEFT JOIN tblSite S ON S.idSite = EXL.idSite
			LEFT JOIN tblUser U ON U.idUser = EXL.idUser
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblExceptionLog
			INNER JOIN CONTAINSTABLE(tblExceptionLog, *, @searchParam) K ON K.[key] = tblExceptionLog.idExceptionLog
			WHERE 
				(
				(@idSite IS NULL) 
				OR 
				(@idSite IS NOT NULL AND idSite = @idSite)
				)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idExceptionLog,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END
						)
						AS [row_number]
					FROM tblExceptionLog
					INNER JOIN CONTAINSTABLE(tblExceptionLog, *, @searchParam) K ON K.[key] = tblExceptionLog.idExceptionLog
					WHERE 
						(
						@idSite IS NULL
						OR 
						@idSite IS NOT NULL AND idSite = @idSite
						)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idExceptionLog, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				EXL.idExceptionLog, 
				EXL.[timestamp],					
				CASE WHEN EXL.idSite IS NULL THEN '[N/A]' ELSE S.hostname END AS [site],
				CASE WHEN EXL.idUser IS NULL THEN '[N/A]' ELSE U.displayName + '(' + U.username + ')' END AS [user],
				EXL.page,
				EXL.exceptionType,
				EXL.exceptionMessage,
				EXL.exceptionStackTrace,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblExceptionLog EXL ON EXL.idExceptionLog = SelectedKeys.idExceptionLog
			LEFT JOIN tblSite S ON S.idSite = EXL.idSite
			LEFT JOIN tblUser U ON U.idUser = EXL.idUser
			ORDER BY SelectedKeys.[row_number]
			
			END
	END		
			
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO