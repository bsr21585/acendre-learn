-- =====================================================================
-- PROCEDURE: [Enrollment.SetLessonDataCompletionForStandupTraining]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.SetLessonDataCompletionForStandupTraining]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.SetLessonDataCompletionForStandupTraining]
GO

/*

Looks for standup training instances a learner has taken prior to being enrolled in a course
that can be counted as credit toward completion of the enrollment, and marks the appropriate
lesson data record(s) as completed for them.

This looks back 30 days prior to the enrollment start and applies any standup training instances
that a user has completed within that time period as credit for lesson data where that standup 
training module is a requirement.

Note that if a user enrolls in a standup training while currently enrolled in a course the 
standup training applies to, the manage roster functionality for the standup training instance
takes care of completing lesson data. This just fills in the gap where the standup training
instance was completed prior to the user having the enrollment.

*/

CREATE PROCEDURE [Enrollment.SetLessonDataCompletionForStandupTraining]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idEnrollment			INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/* NO PERMISSIONS CHECKS - WE CANNOT RESTRICT THIS BASED ON PERMISSIONS SINCE IT CAN BE CALLED AS A RESULT OF LEARNER ACTIONS */

	/*
	
	validate that the enrollment exists within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblEnrollment E
		WHERE E.idEnrollment = @idEnrollment  
		AND (E.idSite IS NULL OR E.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'EnrollmentSetLessonDataCompletionFST_NoRecordFound'
		RETURN 1 
		END

	/*

	declare and set utcNow

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	create a temporary table to hold the lesson data records we need to update

	*/

	CREATE TABLE #LessonDataToUpdate (
		[idData-Lesson]		INT,
		dtCompleted			DATETIME
	)

	/*

	get lesson data records we need to update completion for

	*/

	INSERT INTO #LessonDataToUpdate (
		[idData-Lesson],
		dtCompleted
	)
	SELECT
		LD.[idData-Lesson],
		STIUL.dtCompleted
	FROM tblEnrollment E
	LEFT JOIN [tblData-Lesson] LD ON LD.idEnrollment = E.idEnrollment
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson
	INNER JOIN tblLessonToContentLink LCL ON LCL.idLesson = L.idLesson AND LCL.idContentType = 2 -- standup training
	LEFT JOIN tblStandUpTraining ST ON ST.idStandUpTraining = LCL.idObject
	LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandUpTraining = ST.idStandUpTraining
	INNER JOIN tblStandupTrainingInstanceToUserLink STIUL ON STIUL.idUser = E.idUser AND STIUL.idStandupTrainingInstance = STI.idStandUpTrainingInstance
	WHERE E.idEnrollment = @idEnrollment
	AND LD.dtCompleted IS NULL
	AND LD.contentTypeCommittedTo IS NULL
	AND STIUL.completionStatus = 2 -- completed
	AND STIUL.dtCompleted >= DATEADD(d, -30, E.dtStart) -- completion of standup training instance within 30 days of enrollment start
	AND E.dtStart <= @utcNow							-- enrollment has started
	-- enrollment has not expired
	AND (E.dtExpiresFromStart > @utcNow OR E.dtExpiresFromStart IS NULL)
	AND (E.dtExpiresFromFirstLaunch > @utcNow OR E.dtExpiresFromFirstLaunch IS NULL)

	/*

	update the lesson data

	*/

	UPDATE [tblData-Lesson] SET
		dtCompleted = LDTU.dtCompleted,
		contentTypeCommittedTo = 2, -- standup training
		dtCommittedToContentType = @utcNow
	FROM #LessonDataToUpdate LDTU
	WHERE LDTU.[idData-Lesson] = [tblData-Lesson].[idData-Lesson]

	/*

	do event log entries for lesson data completions

	*/

	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		LD.idLesson,
		LDTU.[idData-Lesson],
		E.idUser
	FROM #LessonDataToUpdate LDTU
	LEFT JOIN [tblData-Lesson] LD ON LD.[idData-Lesson] = LDTU.[idData-Lesson]
	LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 303, @utcNow, @eventLogItems

	-- drop the temporary table
	DROP TABLE #LessonDataToUpdate

	/*

	check for enrollment completion

	*/

	EXEC [Enrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO