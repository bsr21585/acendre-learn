-- =====================================================================
-- PROCEDURE: [Site.GetLeaderboard]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Site.GetLeaderboard]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.GetLeaderboard]
GO

/*

Gets leaderboard data for site.

*/

CREATE PROCEDURE [Site.GetLeaderboard]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idUser					INT,			-- populated when this is called from learner dashboard widget
											-- we cannot just use @idCaller, because that would result in records
											-- for the calling user being added to the leaderboard when called from
											-- administrator interface
	@relativeTo				NVARCHAR(50)	= NULL,	-- populated when this is called from learner dashboard widget
													-- used in conjunction with @idUser as a filter to allow the leaderboard
													-- to show data for other users that have something in common with the learner
													-- i.e. same department, or job title, etc.
	@numRecords				INT,
	@dtStart				DATETIME,
	@leaderboardType		NVARCHAR(20)
)
AS
	BEGIN
		SET NOCOUNT ON
		
		DECLARE @idUserLocal INT
		SET @idUserLocal = @idUser

		/*

		if number of records to return is not set, set it to a default of 25

		*/

		IF @numRecords IS NULL

			BEGIN
			SET @numRecords = 25
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the groups that the caller has user manager permission over
		if caller is 1 or has null scope for user manager, they see global statistics

		*/

		DECLARE @idPermission INT
		DECLARE @hasGlobalPermission BIT
		DECLARE @groupScope NVARCHAR(MAX)

		SET @idPermission = 105 -- 105 is user manager, 107 is group manager if we wanted to use that permission	
		SET @hasGlobalPermission = 1
		SET @groupScope = NULL	

		IF (@idCaller > 1 AND @idUserLocal IS NULL)
			BEGIN 

			CREATE TABLE #CallerRoles (idRole INT)
			INSERT INTO #CallerRoles (idRole) SELECT DISTINCT URL.idRole FROM tblUserToRoleLink URL WHERE URL.idUser = @idCaller -- directly assigned roles
			INSERT INTO #CallerRoles (idRole) SELECT DISTINCT GRL.idRole FROM tblGroupToRoleLink GRL -- group inherited roles
											  WHERE GRL.idGroup IN (SELECT DISTINCT UGL.idGroup FROM tblUserToGroupLink UGL WHERE UGL.idUser = @idCaller)
											  AND NOT EXISTS (SELECT 1 FROM #CallerRoles CR WHERE CR.idRole = GRL.idRole)
		
			-- if the caller does not have permission with NULL scope, get the defined scopes
			IF (
				SELECT COUNT(1) FROM tblRoleToPermissionLink RPL 
				WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles)
				AND RPL.idPermission = @idPermission
				AND RPL.scope IS NULL
			   ) = 0
				BEGIN

				SET @hasGlobalPermission = 0

				-- get all scope items into the group scope variable			
				DECLARE @scopeItems NVARCHAR(MAX)
				SET @groupScope = ''

				DECLARE scopeListCursor CURSOR LOCAL FOR
					SELECT scope FROM tblRoleToPermissionLink RPL WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles) AND RPL.idPermission = @idPermission AND RPL.scope IS NOT NULL

				OPEN scopeListCursor

				FETCH NEXT FROM scopeListCursor INTO @scopeItems

				WHILE @@FETCH_STATUS = 0
					BEGIN

					IF (@scopeItems <> '')
						BEGIN
						IF (@groupScope <> '')
							BEGIN
							SET @groupScope = @groupScope + ',' + @scopeItems
							END
						ELSE
							BEGIN
							SET @groupScope = @scopeItems
							END
						
						END

					FETCH NEXT FROM scopeListCursor INTO @scopeItems

					END

				-- kill the cursor
				CLOSE scopeListCursor
				DEALLOCATE scopeListCursor

				END

			END

		/*

		create temporary tables

		*/

		CREATE TABLE #RawData (
			idUser		INT,
			name		NVARCHAR(512),
			gender		NVARCHAR(1),
			total		FLOAT(2)
		)

		CREATE TABLE #Leaderboard (
			id			INT				IDENTITY(1,1),
			position	INT,
			idUser		INT,
			name		NVARCHAR(512),
			gender		NVARCHAR(1),
			total		FLOAT(2)
		)		

		/*

		certificates count leaderboard

		*/

		IF @leaderboardType = 'certificate_count'

			BEGIN

			INSERT INTO #RawData (
				idUser,
				name,
				gender,
				total
			)
			SELECT
				idUser,
				name,
				gender,
				CASE WHEN numCerts IS NULL THEN 0 ELSE numCerts END AS numCerts
			FROM (
				SELECT DISTINCT
					U.idUser,
					U.firstname + ' ' + U.lastname AS name,
					U.gender,
					numCerts = COUNT(1) OVER (PARTITION BY CR.idUser)
				FROM tblCertificateRecord CR
				LEFT JOIN tblUser U ON U.idUser = CR.idUser
				WHERE CR.idSite = @idCallerSite
				AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
				AND U.isActive = 1
				AND (U.excludeFromLeaderboards = 0 OR U.excludeFromLeaderboards IS NULL)
				AND (
					U.dtExpires IS NULL
					OR U.dtExpires > GETUTCDATE()
					)
				AND CR.[timestamp] IS NOT NULL
				AND (
					CR.[timestamp] >= @dtStart
					OR @dtStart IS NULL
					)
				AND (
					@hasGlobalPermission = 1
					OR
					(
						@hasGlobalPermission = 0
						AND
						U.idUser IN (SELECT DISTINCT UGL.idUser FROM tblUserToGroupLink UGL WHERE UGL.idGroup IN (SELECT s FROM dbo.DelimitedStringToTable(@groupScope, ',')))
					)
				)
			) MAIN

			END

		/*

		certificates credits leaderboard

		*/

		IF @leaderboardType = 'certificate_credits'

			BEGIN

			INSERT INTO #RawData (
				idUser,
				name,
				gender,
				total
			)
			SELECT
				idUser,
				name,
				gender,
				CASE WHEN numCredits IS NULL THEN 0 ELSE numCredits END AS numCredits
			FROM (
				SELECT DISTINCT
					U.idUser,
					U.firstname + ' ' + U.lastname AS name,
					U.gender,
					numCredits = SUM(credits) OVER (PARTITION BY CR.idUser)
				FROM tblCertificateRecord CR
				LEFT JOIN tblUser U ON U.idUser = CR.idUser
				WHERE CR.idSite = @idCallerSite
				AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
				AND U.isActive = 1
				AND (U.excludeFromLeaderboards = 0 OR U.excludeFromLeaderboards IS NULL)
				AND (
					U.dtExpires IS NULL
					OR U.dtExpires > GETUTCDATE()
					)
				AND CR.[timestamp] IS NOT NULL
				AND (
					CR.[timestamp] >= @dtStart
					OR @dtStart IS NULL
					)
				AND (
					@hasGlobalPermission = 1
					OR
					(
						@hasGlobalPermission = 0
						AND
						U.idUser IN (SELECT DISTINCT UGL.idUser FROM tblUserToGroupLink UGL WHERE UGL.idGroup IN (SELECT s FROM dbo.DelimitedStringToTable(@groupScope, ',')))
					)
				)
			) MAIN

			END

		/*

		enrollments count leaderboard

		*/

		IF @leaderboardType = 'enrollment_count'

			BEGIN

			INSERT INTO #RawData (
				idUser,
				name,
				gender,
				total
			)
			SELECT
				idUser,
				name,
				gender,
				CASE WHEN numCourses IS NULL THEN 0 ELSE numCourses END AS numCourses
			FROM (
				SELECT DISTINCT
					U.idUser,
					U.firstname + ' ' + U.lastname AS name,
					U.gender,
					numCourses = COUNT(1) OVER (PARTITION BY E.idUser)
				FROM tblEnrollment E
				LEFT JOIN tblUser U ON U.idUser = E.idUser
				WHERE E.idSite = @idCallerSite
				AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
				AND U.isActive = 1
				AND (U.excludeFromLeaderboards = 0 OR U.excludeFromLeaderboards IS NULL)
				AND (
					U.dtExpires IS NULL
					OR U.dtExpires > GETUTCDATE()
					)
				AND E.dtCompleted IS NOT NULL				
				AND (
					E.dtCompleted >= @dtStart
					OR @dtStart IS NULL
					)
				AND E.idActivityImport IS NULL
				AND (
					@hasGlobalPermission = 1
					OR
					(
						@hasGlobalPermission = 0
						AND
						U.idUser IN (SELECT DISTINCT UGL.idUser FROM tblUserToGroupLink UGL WHERE UGL.idGroup IN (SELECT s FROM dbo.DelimitedStringToTable(@groupScope, ',')))
					)
				)
			) MAIN

			END

		/*

		enrollments credits leaderboard

		*/

		IF @leaderboardType = 'enrollment_credits'

			BEGIN

			INSERT INTO #RawData (
				idUser,
				name,
				gender,
				total
			)
			SELECT
				idUser,
				name,
				gender,
				CASE WHEN numCredits IS NULL THEN 0 ELSE numCredits END AS numCredits
			FROM (
				SELECT DISTINCT
					U.idUser,
					U.firstname + ' ' + U.lastname AS name,
					U.gender,
					numCredits = SUM(E.credits) OVER (PARTITION BY E.idUser) -- add credits column to enrollment table
				FROM tblEnrollment E
				LEFT JOIN tblUser U ON U.idUser = E.idUser
				WHERE E.idSite = @idCallerSite
				AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
				AND U.isActive = 1
				AND (U.excludeFromLeaderboards = 0 OR U.excludeFromLeaderboards IS NULL)
				AND (
					U.dtExpires IS NULL
					OR U.dtExpires > GETUTCDATE()
					)
				AND E.dtCompleted IS NOT NULL
				AND (
					E.dtCompleted >= @dtStart
					OR @dtStart IS NULL
					)
				AND E.idActivityImport IS NULL
				AND (
					@hasGlobalPermission = 1
					OR
					(
						@hasGlobalPermission = 0
						AND
						U.idUser IN (SELECT DISTINCT UGL.idUser FROM tblUserToGroupLink UGL WHERE UGL.idGroup IN (SELECT s FROM dbo.DelimitedStringToTable(@groupScope, ',')))
					)
				)
			) MAIN

			END

		/*

		dynamic sql for use in "relative to" filtering, only gets used if this is a learner dashboard and a "relative to" filter is used

		*/				

		IF (@idUserLocal IS NOT NULL AND @idUserLocal > 1 AND @relativeTo IS NOT NULL AND @relativeTo <> '_global_')
			BEGIN

			DECLARE @relativeToSQL NVARCHAR(MAX)

			SET @relativeToSQL = 'DELETE FROM #RawData'
							   + ' WHERE idUser <> ' + CONVERT(NVARCHAR, @idUserLocal)
							   + ' AND idUser IN (SELECT idUser' 
							   + '                FROM tblUser'
							   + '                WHERE idSite = ' + CONVERT(NVARCHAR, @idCallerSite)
							   + '                AND (' + @relativeTo + ' <> (SELECT ' + @relativeTo + ' FROM tblUser WHERE idUser = ' + CONVERT(NVARCHAR, @idUserLocal) + ')'
							   + '                OR ' + @relativeTo + ' IS NULL))'

			EXEC sp_executesql @relativeToSQL

			END

		/*

		insert the top X records into the leaderboard table

		*/

		INSERT INTO #Leaderboard (
			position,
			idUser,
			name,
			gender,
			total
		)
		SELECT TOP (@numRecords)
			(SELECT COUNT(1) FROM #RawData RD WHERE RD.total > RD2.total) + 1 AS position,
			RD2.idUser,
			RD2.name, 
			RD2.gender,
			RD2.total
		FROM #RawData RD2
		ORDER BY RD2.total DESC

		/*

		if the user matches the score of the last position, but have not been included yet, include them now

		*/

		IF @idUserLocal > 1

			IF (SELECT total FROM #RawData WHERE idUser = @idUserLocal) = (SELECT MIN(total) FROM #Leaderboard)
			   AND NOT EXISTS (SELECT 1 FROM #Leaderboard WHERE idUser = @idUserLocal) -- user is not already in the table

				BEGIN

				DELETE FROM #Leaderboard WHERE id = (SELECT MAX(id) FROM #Leaderboard)

				INSERT INTO #Leaderboard (
					position, 
					idUser, 
					name, 
					gender, 
					total
				)
				SELECT
					(SELECT COUNT(1) FROM #RawData RD WHERE RD.total > RD2.total) + 1 AS position,
					RD2.idUser, 
					RD2.name,
					RD2.gender,  
					RD2.total
				FROM #RawData RD2
				WHERE idUser = @idUserLocal

				END
	
		/*

		add + X others note if they match the min score

		*/

		IF (
			SELECT COUNT(1)
			FROM #RawData
			WHERE idUser NOT IN (SELECT idUser FROM #Leaderboard)
			AND total = (SELECT MIN(total) FROM #Leaderboard)
			) > 0

			BEGIN

			INSERT INTO #Leaderboard (
				position, 
				idUser, 
				name, 
				gender, 
				total
			)
			SELECT 
				(SELECT MAX(position) FROM #Leaderboard) AS position, --999999999,
				0,
				'+ ' + CONVERT(NVARCHAR(7), COUNT(1)) + ' [others]',
				NULL,
				(SELECT MIN(total) FROM #Leaderboard)
			FROM #RawData 
			WHERE idUser NOT IN (
				SELECT idUser 
				FROM #Leaderboard
			) 
			AND total = (SELECT MIN(total) FROM #Leaderboard)

			END

		/*

		insert the user if not already inserted

		*/

		IF @idUserLocal > 1

			BEGIN

			IF (SELECT COUNT(1) FROM #Leaderboard WHERE idUser = @idUserLocal) = 0

				BEGIN

				INSERT INTO #Leaderboard (
					position, 
					idUser, 
					name, 
					gender, 
					total
				)
				SELECT
					(SELECT COUNT(1) FROM #RawData RD WHERE RD.total > RD2.total) + 1 AS position,
					RD2.idUser,
					RD2.name,
					RD2.gender,
					RD2.total
				FROM #RawData RD2
				WHERE idUser = @idUserLocal

				END

			END

		/*

		if the user still isnt in the leaderboard table, they have 0, insert that

		*/

		IF @idUserLocal > 1

			BEGIN

			IF (SELECT COUNT(1) FROM #Leaderboard WHERE idUser = @idUserLocal) = 0

				BEGIN

				INSERT INTO #Leaderboard (
					position, 
					idUser, 
					name, 
					gender, 
					total
				)
				SELECT
					(SELECT COUNT(1) FROM #RawData) + 1,
					U.idUser,
					U.firstname + ' ' + U.lastname,
					U.gender,
					0
				FROM tblUser U
				WHERE U.idUser = @idUserLocal

				END

			END

		/*

		select the records from the leaderboard temp table

		*/

		SELECT
			id, 
			position, 
			idUser, 
			gender, 
			name, 
			total
		FROM #Leaderboard

		/* 
		
		drop the temporary tables

		*/

		DROP TABLE #RawData
		DROP TABLE #Leaderboard

		/*

		return

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		
	END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO