-- =====================================================================
-- PROCEDURE: [SiteToDomainAliasLink.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[SiteToDomainAliasLink.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SiteToDomainAliasLink.Save]
GO

/*

Save site to domain alis link

*/

CREATE PROCEDURE [SiteToDomainAliasLink.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idSite					INT,	
	@domain					NVARCHAR (255)		
)
AS

	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site (commented this section of validation for now, as per the suggestion from Joe)
	
	*/
	

	--IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
	--	BEGIN
	--	SET @Return_Code = 3 -- sort of (caller not member of specified site).
	--	SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
	--	RETURN 1
	--	END

	--/*
	
	--validate caller permission
	
	--*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	validate uniqueness for domain alias, if specified
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblSiteToDomainAliasLink STDAL
		WHERE  STDAL.domain = @domain
		AND		STDAL.idSite != @idSite
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'SiteToDomainAliasLinkSave_DomainAliasNotUnique'		
		RETURN 1 
		END


	/*
	
    Save site to domain alis link
	
	*/

		 INSERT INTO tblSiteToDomainAliasLink (
				idSite,
				domain
		   )   
		   SELECT
				@idSite,
				@domain
		  WHERE NOT EXISTS (
			SELECT 1
			FROM tblSiteToDomainAliasLink STDAL
					WHERE  STDAL.domain = @domain
					AND		STDAL.idSite = @idSite)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO