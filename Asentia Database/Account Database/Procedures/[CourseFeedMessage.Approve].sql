-- =====================================================================
-- PROCEDURE: [CourseFeedMessage.Approve]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CourseFeedMessage.Approve]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CourseFeedMessage.Approve]
GO

/*

approves a course feed message

*/

CREATE PROCEDURE [CourseFeedMessage.Approve]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified

	@idCourseFeedMessage	INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the Course feed message exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourseFeedMessage
		WHERE @idCourseFeedMessage = idCourseFeedMessage
		) <> 1
		BEGIN
		SELECT @Return_Code = 2
		RETURN 1 
		END		
	
	/*
	
	approve the message
	
	*/
	
	UPDATE tblCourseFeedMessage SET
		dtApproved = GETUTCDATE(),
		idApprover = @idCaller,
		isApproved = 1
	WHERE idCourseFeedMessage = @idCourseFeedMessage
	AND (isApproved IS NULL OR isApproved = 0)
	
	SELECT @Return_Code = 0
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO