-- =====================================================================
-- PROCEDURE: [Certificate.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certificate.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certificate.Delete]
GO

CREATE PROCEDURE [Certificate.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@Certificates			IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Certificates) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CertificateDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all certificates exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Certificates CC
		LEFT JOIN tblCertificate C ON C.idCertificate = CC.id
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificateDelete_NoRecordFound'
		RETURN 1 
		END
	
	/*
	
	Mark the certificate(s) as deleted. DO NOT remove the record. Do NOT delete language or other sub records either.
	
	*/
	
	UPDATE tblCertificate SET
		isActive = 0,
		activationDate = NULL,
		isDeleted = 1,
		dtDeleted = GETUTCDATE()
	WHERE idCertificate IN (
		SELECT id
		FROM @Certificates
	)
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO