-- =====================================================================
-- PROCEDURE: [Certification.GetRequirementStatusesForUser]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Certification.GetRequirementStatusesForUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.GetRequirementStatusesForUser]
GO

/*

Gets a recordset of requirement statuses for a user certification.

*/

CREATE PROCEDURE [Certification.GetRequirementStatusesForUser]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,	

	@idUser						INT,
	@idCertificationToUserLink	INT,
	@isInitialRequirement		BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		/*

		GET THE CERTIFICATION ID

		*/

		DECLARE @idCertification INT
		SELECT @idCertification = idCertification FROM tblCertificationToUserLink WHERE idCertificationToUserLink = @idCertificationToUserLink			

		/*

		CREATE TEMP TABLES TO TRACK IF REQUIREMENTS ARE MET

		*/

		CREATE TABLE #RequirementsStatus (
			idCertificationModuleRequirement	INT,
			idCertificationModuleRequirementSet INT,
			isRequirementMet					BIT,
			numberCreditsEarned					FLOAT,
			idsOfCoursesCompleted				NVARCHAR(MAX)
		)

		CREATE TABLE #RequirementSetsStatus (
			idCertificationModuleRequirementSet INT,
			idCertificationModule				INT,
			isRequirementMet					BIT
		)

		CREATE TABLE #ModulesStatus (
			idCertificationModule	INT,
			idCertification			INT,
			isRequirementMet		BIT
		)

		CREATE TABLE #CertificationStatus (
			idCertification		INT,
			isRequirementMet	BIT
		)

		/*

		EVALUATE REQUIRMENTS

		*/

		IF (@isInitialRequirement = 1) -- INITIAL REQUIREMENTS
			BEGIN

			/* REQUIREMENTS STATUS */

			INSERT INTO #RequirementsStatus (
				idCertificationModuleRequirement,
				idCertificationModuleRequirementSet,
				isRequirementMet,
				numberCreditsEarned,
				idsOfCoursesCompleted
			)
			SELECT DISTINCT
				CMR.idCertificationModuleRequirement,
				CMR.idCertificationModuleRequirementSet,
				CASE WHEN CMR.requirementType = 0 THEN
						CASE WHEN CMR.courseCompletionIsAny = 1 THEN
							CASE WHEN (SELECT COUNT(1)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									  ) >= 1 THEN 1 ELSE 0 END
						ELSE
							CASE WHEN (SELECT COUNT(1)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									  ) >= (SELECT COUNT(1) 
											FROM tblCertificationModuleRequirementToCourseLink
											WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement
											) THEN 1 ELSE 0 END
						END
					 WHEN CMR.requirementType = 1 THEN
						CASE WHEN CMR.courseCreditEligibleCoursesIsAll = 1 THEN				
							CASE WHEN (SELECT SUM(CCCL.credits)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationToCourseCreditLink WHERE idCertification = CUL.idCertification)) E_MAIN
									   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse AND CCCL.idCertification = CUL.idCertification
									  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END
						ELSE
							CASE WHEN (SELECT SUM(CCCL.credits)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse AND CCCL.idCertification = CUL.idCertification
									  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END
						END
					 WHEN CMR.requirementType = 2 THEN
						CASE WHEN DCMR.dtCompleted IS NOT NULL THEN 1 ELSE 0 END
				END AS [isRequirementMet],
				CASE WHEN CMR.requirementType = 0 THEN NULL
					 WHEN CMR.requirementType = 1 THEN
						CASE WHEN CMR.courseCreditEligibleCoursesIsAll = 1 THEN				
							(SELECT SUM(CCCL.credits)
							 FROM (SELECT DISTINCT idCourse 
								   FROM tblEnrollment E 
								   WHERE E.idUser = @idUser 
								   AND E.dtCompleted IS NOT NULL 
								   AND E.idCourse IN (SELECT idCourse FROM tblCertificationToCourseCreditLink WHERE idCertification = CUL.idCertification)) E_MAIN
							 LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse AND CCCL.idCertification = CUL.idCertification)
						ELSE
							(SELECT SUM(CCCL.credits)
							 FROM (SELECT DISTINCT idCourse 
								   FROM tblEnrollment E 
								   WHERE E.idUser = @idUser 
								   AND E.dtCompleted IS NOT NULL 
								   AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
							 LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse AND CCCL.idCertification = CUL.idCertification)
						END
					 WHEN CMR.requirementType = 2 THEN NULL
				END AS [numberCreditsEarned],
				CASE WHEN CMR.requirementType = 0 OR CMR.requirementType = 1 THEN
					CASE WHEN CMR.courseCreditEligibleCoursesIsAll = 1 THEN	
						STUFF((
								SELECT ',' + CONVERT(NVARCHAR, E_MAIN.idCourse)
								FROM (SELECT DISTINCT idCourse
									  FROM tblEnrollment E
									  WHERE E.idUser = @idUser
									  AND E.dtCompleted IS NOT NULL
									  AND E.idCourse IN (SELECT idCourse FROM tblCertificationToCourseCreditLink WHERE idCertification = CUL.idCertification))	E_MAIN																											
								FOR XML PATH('')
						), 1, 1, '')
					ELSE
						STUFF((
								SELECT ',' + CONVERT(NVARCHAR, E_MAIN.idCourse)
								FROM (SELECT DISTINCT idCourse
									  FROM tblEnrollment E
									  WHERE E.idUser = @idUser
									  AND E.dtCompleted IS NOT NULL
									  AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement))	E_MAIN																											
								FOR XML PATH('')
						), 1, 1, '')
					END
				ELSE
					NULL
				END AS [idsOfCoursesCompleted]
			FROM tblCertificationToUserLink CUL
			LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
			LEFT JOIN tblCertificationModule CM ON CM.idCertification = C.idCertification
			LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModule = CM.idCertificationModule
			LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet
			LEFT JOIN [tblData-CertificationModuleRequirement] DCMR ON DCMR.idCertificationToUserLink = CUL.idCertificationToUserLink AND DCMR.idCertificationModuleRequirement = CMR.idCertificationModuleRequirement
			WHERE CUL.idUser = @idUser
			AND CUL.idCertification = @idCertification
			AND CM.isInitialRequirement = @isInitialRequirement

			/* REQUIREMENT SETS STATUS */

			INSERT INTO #RequirementSetsStatus (
				idCertificationModuleRequirementSet,
				idCertificationModule,
				isRequirementMet
			)
			SELECT DISTINCT
				RS.idCertificationModuleRequirementSet,
				CMRS.idCertificationModule,
				CASE WHEN CMRS.isAny = 1 THEN
					CASE WHEN (SELECT COUNT(1) FROM #RequirementsStatus WHERE isRequirementMet = 1 AND idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) >= 1 THEN 1 ELSE 0 END
				ELSE
					CASE WHEN (SELECT COUNT(1) FROM #RequirementsStatus WHERE isRequirementMet = 1 AND idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) = (SELECT COUNT(1) FROM #RequirementsStatus WHERE idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) THEN 1 ELSE 0 END
				END
			FROM #RequirementsStatus RS
			LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModuleRequirementSet = RS.idCertificationModuleRequirementSet

			/* MODULES STATUS */

			INSERT INTO #ModulesStatus (
				idCertificationModule,
				idCertification,
				isRequirementMet
			)
			SELECT DISTINCT
				RSS.idCertificationModule,
				CM.idCertification,
				CASE WHEN CM.isAnyRequirementSet = 1 THEN
					CASE WHEN (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE isRequirementMet = 1 AND idCertificationModule = CM.idCertificationModule) >= 1 THEN 1 ELSE 0 END
				ELSE
					CASE WHEN (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE isRequirementMet = 1 AND idCertificationModule = CM.idCertificationModule) = (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE idCertificationModule = CM.idCertificationModule) THEN 1 ELSE 0 END
				END
			FROM #RequirementSetsStatus RSS
			LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = RSS.idCertificationModule

			/* CERTIFICATION STATUS */

			INSERT INTO #CertificationStatus (
				idCertification,
				isRequirementMet
			)
			SELECT DISTINCT
				CUL.idCertification,
				CASE WHEN CUL.initialAwardDate IS NOT NULL THEN
					1
				ELSE
					CASE WHEN C.isAnyModule = 1 THEN
						CASE WHEN (SELECT COUNT(1) FROM #ModulesStatus WHERE isRequirementMet = 1) >= 1 THEN 1 ELSE 0 END
					ELSE
						CASE WHEN (SELECT COUNT(1) FROM #ModulesStatus WHERE isRequirementMet = 1) = (SELECT COUNT(1) FROM #ModulesStatus) THEN 1 ELSE 0 END
					END
				END
			FROM tblCertificationToUserLink CUL
			LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
			WHERE CUL.idCertification = @idCertification
			AND CUL.idUser = @idUser

			END

		ELSE -- RENEWAL

			BEGIN

			/* REQUIREMENTS STATUS */

			INSERT INTO #RequirementsStatus (
				idCertificationModuleRequirement,
				idCertificationModuleRequirementSet,
				isRequirementMet,
				numberCreditsEarned,
				idsOfCoursesCompleted
			)
			SELECT DISTINCT
				CMR.idCertificationModuleRequirement,
				CMR.idCertificationModuleRequirementSet,
				CASE WHEN CMR.requirementType = 0 THEN
						CASE WHEN CMR.courseCompletionIsAny = 1 THEN
							CASE WHEN (SELECT COUNT(1)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND CUL.lastExpirationDate IS NOT NULL
											 AND CUL.currentExpirationDate IS NOT NULL
											 AND E.dtCompleted >= CUL.lastExpirationDate									 
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									  ) >= 1 THEN 1 ELSE 0 END
						ELSE
							CASE WHEN (SELECT COUNT(1)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND CUL.lastExpirationDate IS NOT NULL
											 AND CUL.currentExpirationDate IS NOT NULL
											 AND E.dtCompleted >= CUL.lastExpirationDate
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									  ) >= (SELECT COUNT(1) 
											FROM tblCertificationModuleRequirementToCourseLink
											WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement
											) THEN 1 ELSE 0 END
						END
					 WHEN CMR.requirementType = 1 THEN
						CASE WHEN CMR.courseCreditEligibleCoursesIsAll = 1 THEN				
							CASE WHEN (SELECT SUM(CCCL.credits)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND CUL.lastExpirationDate IS NOT NULL
											 AND CUL.currentExpirationDate IS NOT NULL
											 AND E.dtCompleted >= CUL.lastExpirationDate
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationToCourseCreditLink WHERE idCertification = CUL.idCertification)) E_MAIN
									   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse AND CCCL.idCertification = CUL.idCertification
									  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END
						ELSE
							CASE WHEN (SELECT SUM(CCCL.credits)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND CUL.lastExpirationDate IS NOT NULL
											 AND CUL.currentExpirationDate IS NOT NULL
											 AND E.dtCompleted >= CUL.lastExpirationDate
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse AND CCCL.idCertification = CUL.idCertification
									  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END
						END
					 WHEN CMR.requirementType = 2 THEN
						CASE WHEN DCMR.dtCompleted IS NOT NULL 
								  AND CUL.lastExpirationDate IS NOT NULL
								  AND CUL.currentExpirationDate IS NOT NULL
								  AND DCMR.dtCompleted >= CUL.lastExpirationDate THEN 1 ELSE 0 END
				END AS [isRequirementMet],
				CASE WHEN CMR.requirementType = 0 THEN NULL
					 WHEN CMR.requirementType = 1 THEN
						CASE WHEN CMR.courseCreditEligibleCoursesIsAll = 1 THEN				
							(SELECT SUM(CCCL.credits)
							 FROM (SELECT DISTINCT idCourse 
								   FROM tblEnrollment E 
								   WHERE E.idUser = @idUser 
								   AND E.dtCompleted IS NOT NULL 
								   AND CUL.lastExpirationDate IS NOT NULL
								   AND CUL.currentExpirationDate IS NOT NULL
								   AND E.dtCompleted >= CUL.lastExpirationDate
								   AND E.idCourse IN (SELECT idCourse FROM tblCertificationToCourseCreditLink WHERE idCertification = CUL.idCertification)) E_MAIN
							 LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse AND CCCL.idCertification = CUL.idCertification)
						ELSE
							(SELECT SUM(CCCL.credits)
							 FROM (SELECT DISTINCT idCourse 
								   FROM tblEnrollment E 
								   WHERE E.idUser = @idUser 
								   AND E.dtCompleted IS NOT NULL 
								   AND CUL.lastExpirationDate IS NOT NULL
								   AND CUL.currentExpirationDate IS NOT NULL
								   AND E.dtCompleted >= CUL.lastExpirationDate
								  AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
							 LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse AND CCCL.idCertification = CUL.idCertification)
						END
					 WHEN CMR.requirementType = 2 THEN NULL
				END AS [numberCreditsEarned],
				CASE WHEN CMR.requirementType = 0 OR CMR.requirementType = 1 THEN
					CASE WHEN CMR.courseCreditEligibleCoursesIsAll = 1 THEN	
						STUFF((
								SELECT ',' + CONVERT(NVARCHAR, E_MAIN.idCourse)
								FROM (SELECT DISTINCT idCourse
									  FROM tblEnrollment E
									  WHERE E.idUser = @idUser
									  AND E.dtCompleted IS NOT NULL
									  AND CUL.lastExpirationDate IS NOT NULL
									  AND CUL.currentExpirationDate IS NOT NULL
									  AND E.dtCompleted >= CUL.lastExpirationDate
									  AND E.idCourse IN (SELECT idCourse FROM tblCertificationToCourseCreditLink WHERE idCertification = CUL.idCertification))	E_MAIN																											
								FOR XML PATH('')
						), 1, 1, '')
					ELSE
						STUFF((
								SELECT ',' + CONVERT(NVARCHAR, E_MAIN.idCourse)
								FROM (SELECT DISTINCT idCourse
									  FROM tblEnrollment E
									  WHERE E.idUser = @idUser
									  AND E.dtCompleted IS NOT NULL
									  AND CUL.lastExpirationDate IS NOT NULL
									  AND CUL.currentExpirationDate IS NOT NULL
									  AND E.dtCompleted >= CUL.lastExpirationDate
									  AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement))	E_MAIN																											
								FOR XML PATH('')
						), 1, 1, '')
					END
				ELSE
					NULL
				END AS [idsOfCoursesCompleted]
			FROM tblCertificationToUserLink CUL
			LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
			LEFT JOIN tblCertificationModule CM ON CM.idCertification = C.idCertification
			LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModule = CM.idCertificationModule
			LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet
			LEFT JOIN [tblData-CertificationModuleRequirement] DCMR ON DCMR.idCertificationToUserLink = CUL.idCertificationToUserLink AND DCMR.idCertificationModuleRequirement = CMR.idCertificationModuleRequirement
			WHERE CUL.idUser = @idUser
			AND CUL.idCertification = @idCertification
			AND CM.isInitialRequirement = @isInitialRequirement

			/* REQUIREMENT SETS STATUS */

			INSERT INTO #RequirementSetsStatus (
				idCertificationModuleRequirementSet,
				idCertificationModule,
				isRequirementMet
			)
			SELECT DISTINCT
				RS.idCertificationModuleRequirementSet,
				CMRS.idCertificationModule,
				CASE WHEN CMRS.isAny = 1 THEN
					CASE WHEN (SELECT COUNT(1) FROM #RequirementsStatus WHERE isRequirementMet = 1 AND idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) >= 1 THEN 1 ELSE 0 END
				ELSE
					CASE WHEN (SELECT COUNT(1) FROM #RequirementsStatus WHERE isRequirementMet = 1 AND idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) = (SELECT COUNT(1) FROM #RequirementsStatus WHERE idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) THEN 1 ELSE 0 END
				END
			FROM #RequirementsStatus RS
			LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModuleRequirementSet = RS.idCertificationModuleRequirementSet

			/* MODULES STATUS */

			INSERT INTO #ModulesStatus (
				idCertificationModule,
				idCertification,
				isRequirementMet
			)
			SELECT DISTINCT
				RSS.idCertificationModule,
				CM.idCertification,
				CASE WHEN CM.isAnyRequirementSet = 1 THEN
					CASE WHEN (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE isRequirementMet = 1 AND idCertificationModule = CM.idCertificationModule) >= 1 THEN 1 ELSE 0 END
				ELSE
					CASE WHEN (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE isRequirementMet = 1 AND idCertificationModule = CM.idCertificationModule) = (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE idCertificationModule = CM.idCertificationModule) THEN 1 ELSE 0 END
				END
			FROM #RequirementSetsStatus RSS
			LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = RSS.idCertificationModule

			/* CERTIFICATION STATUS */

			INSERT INTO #CertificationStatus (
				idCertification,
				isRequirementMet
			)
			SELECT DISTINCT
				CUL.idCertification,
				CASE WHEN CUL.lastExpirationDate IS NOT NULL AND CUL.currentExpirationDate IS NOT NULL AND GETUTCDATE() >= CUL.lastExpirationDate AND GETUTCDATE() <= CUL.currentExpirationDate THEN
					1
				ELSE
					CASE WHEN C.isAnyModule = 1 THEN
						CASE WHEN (SELECT COUNT(1) FROM #ModulesStatus WHERE isRequirementMet = 1) >= 1 THEN 1 ELSE 0 END
					ELSE
						CASE WHEN (SELECT COUNT(1) FROM #ModulesStatus WHERE isRequirementMet = 1) = (SELECT COUNT(1) FROM #ModulesStatus) THEN 1 ELSE 0 END
					END
				END
			FROM tblCertificationToUserLink CUL
			LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
			WHERE CUL.idCertification = @idCertification
			AND CUL.idUser = @idUser

			END

		/*

		SELECT THE RECORDSETS

		*/

		CREATE TABLE #CertificationRequirementStatuses (
			objectType								NVARCHAR(255),
			idObject								INT,
			idObjectRelated							INT,
			isRequirementMet						BIT,
			requirementType							INT,
			courseCompletionIsAny					BIT,
			numberCreditsRequired					FLOAT,
			numberCreditsEarned						FLOAT,
			courseCreditEligibleCourseIsAll			BIT,
			documentationApprovalRequired			BIT,
			allowSupervisorsToApproveDocumentation	BIT,
			completionDocumentationFilePath			NVARCHAR(255),
			dtSubmitted								DATETIME,
			dtApproved								DATETIME,
			idDataCertificationModuleRequirement	INT,
			idsOfCoursesCompleted					NVARCHAR(MAX)
		)

		-- certification
		INSERT INTO #CertificationRequirementStatuses (
			objectType,
			idObject,
			idObjectRelated,
			isRequirementMet,
			requirementType,
			courseCompletionIsAny,
			numberCreditsRequired,
			numberCreditsEarned,
			courseCreditEligibleCourseIsAll,
			documentationApprovalRequired,
			allowSupervisorsToApproveDocumentation,
			completionDocumentationFilePath,
			dtSubmitted,
			dtApproved,
			idDataCertificationModuleRequirement,
			idsOfCoursesCompleted
		)
		SELECT
			'certification',
			idCertification,
			NULL,
			isRequirementMet,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL
		FROM #CertificationStatus

		-- modules
		INSERT INTO #CertificationRequirementStatuses (
			objectType,
			idObject,
			idObjectRelated,
			isRequirementMet,
			requirementType,
			courseCompletionIsAny,
			numberCreditsRequired,
			numberCreditsEarned,
			courseCreditEligibleCourseIsAll,
			documentationApprovalRequired,
			allowSupervisorsToApproveDocumentation,
			completionDocumentationFilePath,
			dtSubmitted,
			dtApproved,
			idDataCertificationModuleRequirement,
			idsOfCoursesCompleted
		)
		SELECT
			'module',
			idCertificationModule,
			idCertification,
			isRequirementMet,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL
		FROM #ModulesStatus

		-- requirement sets
		INSERT INTO #CertificationRequirementStatuses (
			objectType,
			idObject,
			idObjectRelated,
			isRequirementMet,
			requirementType,
			courseCompletionIsAny,
			numberCreditsRequired,
			numberCreditsEarned,
			courseCreditEligibleCourseIsAll,
			documentationApprovalRequired,
			allowSupervisorsToApproveDocumentation,
			completionDocumentationFilePath,
			dtSubmitted,
			dtApproved,
			idDataCertificationModuleRequirement,
			idsOfCoursesCompleted
		)
		SELECT
			'requirementset',
			idCertificationModuleRequirementSet,
			idCertificationModule,
			isRequirementMet,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL
		FROM #RequirementSetsStatus
		
		-- requirements
		INSERT INTO #CertificationRequirementStatuses (
			objectType,
			idObject,
			idObjectRelated,
			isRequirementMet,
			requirementType,
			courseCompletionIsAny,
			numberCreditsRequired,
			numberCreditsEarned,
			courseCreditEligibleCourseIsAll,
			documentationApprovalRequired,
			allowSupervisorsToApproveDocumentation,
			completionDocumentationFilePath,
			dtSubmitted,
			dtApproved,
			idDataCertificationModuleRequirement,
			idsOfCoursesCompleted
		)
		SELECT
			'requirement',
			RS.idCertificationModuleRequirement,
			RS.idCertificationModuleRequirementSet,
			RS.isRequirementMet,
			CMR.requirementType,
			CMR.courseCompletionIsAny,
			CMR.numberCreditsRequired,
			RS.numberCreditsEarned,
			CMR.courseCreditEligibleCoursesIsAll,
			CMR.documentationApprovalRequired,
			CMR.allowSupervisorsToApproveDocumentation,
			DCMR.completionDocumentationFilePath,
			DCMR.dtSubmitted,
			DCMR.dtApproved,
			DCMR.[idData-CertificationModuleRequirement],
			RS.idsOfCoursesCompleted
		FROM #RequirementsStatus RS
		LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirement = RS.idCertificationModuleRequirement
		LEFT JOIN [tblData-CertificationModuleRequirement] DCMR ON DCMR.idCertificationModuleRequirement = CMR.idCertificationModuleRequirement AND CMR.requirementType = 2 AND DCMR.idCertificationToUserLink = @idCertificationToUserLink
		
		-- return the recordset
		SELECT
			objectType,
			idObject,
			idObjectRelated,
			isRequirementMet,
			requirementType,
			courseCompletionIsAny,
			numberCreditsRequired,
			numberCreditsEarned,
			courseCreditEligibleCourseIsAll,
			documentationApprovalRequired,
			allowSupervisorsToApproveDocumentation,
			completionDocumentationFilePath,
			dtSubmitted,
			dtApproved,
			idDataCertificationModuleRequirement,
			idsOfCoursesCompleted
		FROM #CertificationRequirementStatuses

		/*

		DROP THE TEMP TABLES

		*/

		DROP TABLE #CertificationRequirementStatuses
		DROP TABLE #CertificationStatus
		DROP TABLE #ModulesStatus
		DROP TABLE #RequirementSetsStatus
		DROP TABLE #RequirementsStatus

		/*

		return

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO