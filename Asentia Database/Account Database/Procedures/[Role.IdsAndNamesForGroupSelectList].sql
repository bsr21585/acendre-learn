-- =====================================================================
-- PROCEDURE: [Role.IdsAndNamesForGroupSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Role.IdsAndNamesForGroupSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Role.IdsAndNamesForGroupSelectList]
GO

/*

Returns a recordset of role ids and names to populate
"attach role" select list for group.

*/

CREATE PROCEDURE [Role.IdsAndNamesForGroupSelectList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idGroup				INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT
			R.idRole, 
			CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name
		FROM tblRole R
		LEFT JOIN tblRoleLanguage RL ON RL.idRole = R.idRole AND RL.idLanguage = @idCallerLanguage
		WHERE R.idSite = @idCallerSite -- do not allow roles belonging to site id 1 to be viewed, we are not allowing system roles to be assigned to groups
		AND NOT EXISTS (SELECT 1 FROM tblGroupToRoleLink GRL
						WHERE GRL.idGroup = @idGroup
						AND GRL.idRole = R.idRole)
		ORDER BY name

		END

	ELSE

		BEGIN

		SELECT DISTINCT
			R.idRole, 
			CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name
		FROM tblRole R
		INNER JOIN CONTAINSTABLE(tblRole, *, @searchParam) K ON K.[key] = R.idRole 
		LEFT JOIN tblRoleLanguage Rl ON R.idRole = RL.idRole AND RL.idLanguage = @idCallerLanguage
		WHERE R.idSite = @idCallerSite -- do not allow roles belonging to site id 1 to be viewed, we are not allowing system roles to be assigned to groups
		AND NOT EXISTS (SELECT 1 FROM tblGroupToRoleLink GRL
						WHERE GRL.idGroup = @idGroup
						AND GRL.idRole = R.idRole)

		ORDER BY name

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	