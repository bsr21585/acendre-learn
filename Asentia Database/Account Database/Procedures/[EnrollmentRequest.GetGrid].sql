-- =====================================================================
-- PROCEDURE: [EnrollmentRequest.GetGrid]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[EnrollmentRequest.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EnrollmentRequest.GetGrid]
GO

/*

Gets a listing of enrollment requests that the CALLER is valid to approve/deny.

*/
CREATE PROCEDURE [EnrollmentRequest.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000), -- NO searching used in this proc
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
	
	)
AS

SET NOCOUNT ON

	-- return the rowcount with ordering
	
	SELECT COUNT(1) AS row_count 
	FROM tblEnrollmentRequest ER
	WHERE ER.idCourse IN (
		SELECT idCourse
		FROM tblCourseEnrollmentApprover
		WHERE idUser = @idCaller
	) OR ER.idUser IN (	
		SELECT idUser 
		FROM tblUsertoGroupLink UGL
		WHERE idGroup IN (
			SELECT idGroup
			FROM tblGroupEnrollmentApprover
			WHERE idUser = @idCaller
		)
	)
	AND dtDenied IS NULL -- not denied yet
	AND dtApproved IS NULL -- not approved yet
	;WITH
		Keys AS (
			SELECT TOP (@pageNum * @pageSize) 
				idEnrollmentRequest,
				ROW_NUMBER() OVER (ORDER BY
					-- FIRST ORDER DESC
					CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'name' THEN fullName END) END DESC,
					CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'course' THEN C.title END) END DESC,
					CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('name', 'course') THEN timestamp END) END DESC,
					-- SECOND ORDER DESC
					CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn IN ('name', 'course') THEN timestamp END) END DESC,
					CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('name', 'course') THEN fullName END) END DESC,
					
					-- FIRST ORDER ASC
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'name' THEN fullName END) END,
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'course' THEN C.title END) END,
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('name', 'course') THEN fullName END) END,
					-- SECOND ORDER DESC
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('name', 'course') THEN timestamp END) END,
					CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('name', 'course') THEN fullName END) END DESC
				)
				AS [row_number]
			FROM tblEnrollmentRequest ER
			LEFT JOIN ( -- user subquery with names formatted to 'last, first middle'
				SELECT 
					idUser, 
					UU.lastname + ', ' + UU.firstName + CASE WHEN UU.middleName IS NOT NULL AND UU.middleName <> '' THEN ' ' + UU.middleName END AS fullName
				FROM tblUser UU
				WHERE UU.idSite = @idCallerSite
			) U ON U.idUser = ER.idUser
			LEFT JOIN tblCourse C ON C.idCourse = ER.idcourse
			WHERE ER.idCourse IN (
				SELECT idCourse
				FROM tblCourseEnrollmentApprover
				WHERE idUser = @idCaller
			) OR ER.idUser IN (	
				SELECT idUser 
				FROM tblUsertoGroupLink UGL
				WHERE idGroup IN (
					SELECT idGroup
					FROM tblGroupEnrollmentApprover
					WHERE idUser = @idCaller
				)
			)
			AND dtDenied IS NULL -- not denied yet
			AND dtApproved IS NULL -- not approved yet
		),
		SelectedKeys AS (
			SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
				idEnrollmentRequest, 
				[row_number]
			FROM Keys
			WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
		)
		
	-- return the data
	
	SELECT 
		ER.idEnrollmentRequest, 
		ER.idUser, 
		ER.idCourse, 
		U.lastname + ', ' + U.firstName + CASE WHEN U.middleName IS NOT NULL AND U.middleName <> '' THEN ' ' + U.middleName END AS name,
		U.email, 
		C.title,
		C.courseCode, 
		ER.[timestamp],
		SelectedKeys.[row_number]
	FROM SelectedKeys
	LEFT JOIN tblEnrollmentRequest ER ON ER.idEnrollmentRequest = SelectedKeys.idEnrollmentRequest
	LEFT JOIN tblUser U ON U.idUser = ER.idUser
	LEFT JOIN tblCourse C ON C.idCourse = ER.idCourse
	ORDER BY SelectedKeys.[row_number]
	
				
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO