-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.SaveResources]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.SaveResources]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.SaveResources]
GO

/*

Saves resources for a standup training instance

*/

CREATE PROCEDURE [StandupTrainingInstance.SaveResources]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT,
	@Resources					IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that all resources exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Resources RR
		LEFT JOIN tblResource R ON R.idResource = RR.id
		WHERE R.idSite IS NULL
		OR R.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'STISaveResources_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	validate that the standup training instance belongs to the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblStandupTrainingInstance STI
		WHERE STI.idStandupTrainingInstance = @idStandupTrainingInstance
		AND (STI.idSite IS NULL OR STI.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'STISaveResources_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	remove all resource links for this standup training instance from the resource to object link table
	(if there are any in the table variable, otherwise it means there are none)
	
	*/
	
	DELETE FROM tblResourceToObjectLink
	WHERE objectType = 'standuptraining'
	AND idObject = @idStandupTrainingInstance
	AND idSite =  @idCallerSite
	
	/*

	insert the standup training instance resources for ths standup training instance into the resource to object link table

	*/

	IF (SELECT COUNT(1) FROM @Resources) > 0
		BEGIN

		DECLARE @StandupTrainingInstanceMeetingTime TABLE (dtStart DATETIME, dtEnd DATETIME)

		INSERT INTO @StandupTrainingInstanceMeetingTime (
			dtStart,
			dtEnd
		)
		SELECT
			STIMT.dtStart,
			STIMT.dtEnd
		FROM tblStandUpTrainingInstanceMeetingTime STIMT
		WHERE STIMT.idStandupTrainingInstance = @idStandupTrainingInstance

		INSERT INTO tblResourceToObjectLink (
			idSite,
			idResource,
			idObject,
			objectType,
			dtStart,
			dtEnd,
			isOutsideUse,
			isMaintenance
		)
		SELECT
			@idCallerSite,
			RR.id,
			@idStandupTrainingInstance,
			'standuptraining',
			STIMT.dtStart,
			STIMT.dtEnd,
			0,
			0
		FROM @Resources RR, @StandupTrainingInstanceMeetingTime STIMT
		WHERE NOT EXISTS (SELECT 1 
						  FROM tblResourceToObjectLink
						  WHERE idObject = @idStandupTrainingInstance
						  AND objectType = 'standuptraining'
						  AND dtStart = STIMT.dtStart
						  AND dtEnd = STIMT.dtEnd
						  AND idResource = RR.id
						  AND idSite =	@idCallerSite)

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO