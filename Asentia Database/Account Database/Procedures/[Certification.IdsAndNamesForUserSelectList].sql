-- =====================================================================
-- PROCEDURE: [Certification.IdsAndNamesForUserSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.IdsAndNamesForUserSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.IdsAndNamesForUserSelectList]
GO

/*

Returns a recordset of certification ids and names to populate
"attach certification" select list for user.

*/
CREATE PROCEDURE [Certification.IdsAndNamesForUserSelectList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idUser					INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT
			C.idCertification, 
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblCertification C
		LEFT JOIN tblCertificationLanguage CL ON CL.idCertification = C.idCertification AND CL.idLanguage = @idCallerLanguage
		WHERE C.idSite = @idCallerSite
		AND NOT EXISTS (SELECT 1 FROM tblCertificationToUserLink CUL
						WHERE CUL.idUser = @idUser
						AND CUL.idCertification = C.idCertification)
		ORDER BY title

		END

	ELSE

		BEGIN

		SELECT DISTINCT
			C.idCertification, 
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblCertification C
		INNER JOIN CONTAINSTABLE(tblCertification, *, @searchParam) K ON K.[key] = C.idCertification
		LEFT JOIN tblCertificationLanguage CL ON CL.idCertification = C.idCertification AND CL.idLanguage = @idCallerLanguage
		WHERE C.idSite = @idCallerSite
		AND NOT EXISTS (SELECT 1 FROM tblCertificationToUserLink CUL
						WHERE CUL.idUser = @idUser
						AND CUL.idCertification = C.idCertification)
		ORDER BY title

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	