-- =====================================================================
-- PROCEDURE: [RuleSetEnrollment.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetEnrollment.SaveLang]
GO

/*

Saves ruleset enrollment "language specific" properties for specific language
in ruleset enrollment language table.

*/

CREATE PROCEDURE [RuleSetEnrollment.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idRuleSetEnrollment	INT, 
	@languageString			NVARCHAR(10),
	@label					NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetEnrollmentSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
			 
	/*
	
	validate that the ruleset enrollment exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblRuleSetEnrollment
		WHERE idRuleSetEnrollment = @idRuleSetEnrollment
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetEnrollmentSaveLang_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblRuleSetEnrollmentLanguage RSEL WHERE RSEL.idRuleSetEnrollment = @idRuleSetEnrollment AND RSEL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblRuleSetEnrollmentLanguage SET
			label = @label
		WHERE idRuleSetEnrollment = @idRuleSetEnrollment
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblRuleSetEnrollmentLanguage (
			idSite,
			idRuleSetEnrollment,
			idLanguage,
			label
		)
		SELECT
			@idCallerSite,
			@idRuleSetEnrollment,
			@idLanguage,
			@label
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblRuleSetEnrollmentLanguage RSEL
			WHERE RSEL.idRuleSetEnrollment = @idRuleSetEnrollment
			AND RSEL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblRuleSetEnrollment SET
			label = @label
		WHERE idRuleSetEnrollment = @idRuleSetEnrollment

		END

	/*

	sync child ruleset enrollment languages

	*/

	DECLARE @idChildRuleSetEnrollment		INT
	DECLARE @idChildCourse					INT

	SELECT 
		idRuleSetEnrollment
	INTO #SyncedRuleSetEnrollmentsToUpdate
	FROM tblRuleSetEnrollment
	WHERE idParentRuleSetEnrollment = @idRuleSetEnrollment	      
								  
	IF EXISTS (SELECT 1 FROM #SyncedRuleSetEnrollmentsToUpdate)	
	BEGIN
		DECLARE idChildRuleSetEnrollmentCursor							CURSOR LOCAL
		FOR 
		SELECT idRuleSetEnrollment FROM #SyncedRuleSetEnrollmentsToUpdate	

		OPEN idChildRuleSetEnrollmentCursor
		FETCH NEXT FROM idChildRuleSetEnrollmentCursor INTO @idChildRuleSetEnrollment
		WHILE @@FETCH_STATUS = 0  
		BEGIN
		
		SET @idChildCourse = (SELECT idCourse FROM tblRuleSetEnrollment WHERE idRuleSetEnrollment = @idChildRuleSetEnrollment)

		EXEC [RuleSetEnrollment.SaveLang] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idChildRuleSetEnrollment, @languageString, @label
		
		-- sync child ruleset enrollment back to parent ruleset enrollment (calling RuleSet.Save unsyncs the ruleset enrollment)

		UPDATE tblRuleSetEnrollment
		SET idParentRuleSetEnrollment = @idRuleSetEnrollment
		WHERE idRuleSetEnrollment = @idChildRuleSetEnrollment

		FETCH NEXT FROM idChildRuleSetEnrollmentCursor INTO @idChildRuleSetEnrollment

		END

	CLOSE idChildRuleSetEnrollmentCursor
	DEALLOCATE idChildRuleSetEnrollmentCursor

	END
	
	UPDATE tblRuleSetEnrollment
	SET idParentRuleSetEnrollment = NULL
	WHERE idRuleSetEnrollment = @idRuleSetEnrollment

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO