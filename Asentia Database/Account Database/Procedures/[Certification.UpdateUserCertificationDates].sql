-- =====================================================================
-- PROCEDURE: [Certification.UpdateUserCertificationDates]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.UpdateUserCertificationDates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.UpdateUserCertificationDates]
GO

/*

Updates user certification initial award date

*/

CREATE PROCEDURE [Certification.UpdateUserCertificationDates]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0,	-- default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0,

	@idCertificationToUserLink		INT,
	@dateFlag						INT,					-- 1 = Initial Award, 2 = Current Expiration, 3 = Last Expiration	
	@dateToUpdate					DATETIME		= NULL	-- this is the date based on the dateFlag
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	DECLARE @idCertification INT
	DECLARE @expiresInterval INT
	DECLARE @expiresTimeframe NVARCHAR(4)
	DECLARE @oldCertificationTrack INT	

	SELECT
		@idCertification = idCertification,
		@oldCertificationTrack = certificationTrack
	FROM [tblCertificationToUserLink]
	WHERE idCertificationToUserLink = @idCertificationToUserLink

	-- Initial Award Date Update
	IF @dateFlag = 1
	BEGIN
		IF @dateToUpdate IS NULL
		BEGIN
			-- Initial Award Date is null
			UPDATE [tblCertificationToUserlink] SET
				initialAwardDate = NULL
			WHERE idCertificationToUserLink = @idCertificationToUserLink
		END
		ELSE
		BEGIN
			-- Initial Award Date is not null
			UPDATE [tblCertificationToUserLink] SET
				initialAwardDate = @dateToUpdate,
				certificationTrack = 1
			WHERE idCertificationToUserLink = @idCertificationToUserLink
		END
	END
	ELSE IF @dateFlag = 2 -- Current Expiration Date Update
	BEGIN
		IF @dateToUpdate IS NULL
		BEGIN
			UPDATE [tblCertificationToUserlink] SET
				currentExpirationDate = NULL
			WHERE idCertificationToUserLink = @idCertificationToUserLink
		END
		ELSE
		BEGIN
			UPDATE [tblCertificationToUserLink] SET
				currentExpirationDate = @dateToUpdate
			WHERE idCertificationToUserLink = @idCertificationToUserLink
		END

	END
	ELSE -- Last Expiration Date Update
	BEGIN
		IF @dateToUpdate IS NULL
		BEGIN
			UPDATE [tblCertificationToUserlink] SET
				lastExpirationDate = NULL
			WHERE idCertificationToUserLink = @idCertificationToUserLink
		END
		ELSE
		BEGIN
			UPDATE [tblCertificationToUserLink] SET
				lastExpirationDate = @dateToUpdate
			WHERE idCertificationToUserLink = @idCertificationToUserLink
		END
	END
	
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	