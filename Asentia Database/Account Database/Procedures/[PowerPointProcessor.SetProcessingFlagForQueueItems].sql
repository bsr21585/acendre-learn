-- =====================================================================
-- PROCEDURE: [PowerPointProcessor.SetProcessingFlagForQueueItems]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[PowerPointProcessor.SetProcessingFlagForQueueItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [PowerPointProcessor.SetProcessingFlagForQueueItems]
GO

/*

Sets the processing flag for queue items that have been picked up by the processor.
This is so they don't get picked up by another processor thread.

*/

CREATE PROCEDURE [PowerPointProcessor.SetProcessingFlagForQueueItems]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@QueueItems				IDTable			READONLY
)
AS

	BEGIN
	SET NOCOUNT ON

	/*

	set the processing flag for the queue items

	*/

	UPDATE tblContentPackage SET
		isProcessing = 1
	WHERE EXISTS (
		SELECT 1
		FROM @QueueItems QI
		WHERE QI.id = tblContentPackage.idContentPackage
		)
	AND isMediaUpload = 1
	AND idMediaType = 2  -- PowerPoint media type
	AND isProcessed = 0  -- not yet processed
	AND isProcessing = 0 -- is not currently processing

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO