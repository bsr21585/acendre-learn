-- =====================================================================
-- PROCEDURE: [Resource.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Resource.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Resource.SaveLang]
GO

/*

Saves resource "language specific" properties for specific language
in resource language table.

*/

CREATE PROCEDURE [Resource.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idResource				INT, 
	@languageString			NVARCHAR(10),
	@name					NVARCHAR(255), 
	@description		NVARCHAR(MAX)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'ResourceSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
			 
	/*
	
	validate that the Resource exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblResource
		WHERE idResource = @idResource
		AND idSite = @idCallerSite
		) <> 1
		BEGIN 
			SELECT @Return_Code = 1
			SET @Error_Description_Code = 'ResourceSaveLang_DetailsNotFound'
			RETURN 1 
		END

	/*

	check XSS vulnerabilities

	*/

	-- Description

	IF (@description LIKE '%<script%')
	BEGIN
	SELECT @Return_Code = 2
	SET @Error_Description_Code = 'ResourceSaveLang_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@description LIKE '%<object%')
	BEGIN
	SELECT @Return_Code = 2
	SET @Error_Description_Code = 'ResourceSaveLang_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@description LIKE '%<frame%')
	BEGIN
	SELECT @Return_Code = 2
	SET @Error_Description_Code = 'ResourceSaveLang_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@description LIKE '%<iframe%')
	BEGIN
	SELECT @Return_Code = 2
	SET @Error_Description_Code = 'ResourceSaveLang_DescTagNotAllowed_Iframe'
	RETURN 1 
	END

		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblResourceLanguage RL WHERE RL.idResource = @idResource AND RL.idLanguage = @idLanguage) > 0
	
		BEGIN 

		UPDATE tblResourceLanguage SET
			name			= @name,
			[description]	= @description,
			idLanguage		= @idLanguage
		WHERE idResource	= @idResource
		AND idLanguage		= @idLanguage
		AND idSite			= @idCallerSite	
		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblResourceLanguage(
			idSite,
			idResource,
			idLanguage,
			name,
			[description]
		)
		SELECT
			@idCallerSite,
			@idResource,
			@idLanguage,
			@name,
			@description
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblResourceLanguage RL
			WHERE RL.idResource = @idResource
			AND RL.idLanguage = @idLanguage
			AND idSite			= @idCallerSite	
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblResource SET
			name = @name,
			[description] = @description
		WHERE idResource= @idResource
		AND idSite			= @idCallerSite	

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO