-- =====================================================================
-- PROCEDURE: [Certificate.Details]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certificate.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certificate.Details]
GO

/*

Return all the properties for a given certificate id.

*/

CREATE PROCEDURE [Certificate.Details]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT,
	
	@idCertificate						INT				OUTPUT,
	@idSite								INT				OUTPUT,
	@name								NVARCHAR(255)	OUTPUT,
	@issuingOrganization				NVARCHAR(255)	OUTPUT,
	@description						NVARCHAR(MAX)	OUTPUT,
	@credits							FLOAT			OUTPUT,
	@code								NVARCHAR(25)	OUTPUT,
	@filename							NVARCHAR(255)	OUTPUT,
	@isActive							BIT				OUTPUT,
	@activationDate						DATETIME		OUTPUT,
	@expiresInterval					INT				OUTPUT,
	@expiresTimeframe					NVARCHAR(4)		OUTPUT,
	@objectType							INT				OUTPUT,
	@idObject							INT				OUTPUT,
	@isDeleted							BIT				OUTPUT,
	@dtDeleted							DATETIME		OUTPUT,
	@reissueBasedOnMostRecentCompletion	BIT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificate
		WHERE idCertificate = @idCertificate
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificateDetails_NoRecordFound'
		RETURN 1
	    END
	
	/*
	
	get the data 
	
	*/
	
	SELECT
		@idCertificate						= C.idCertificate,
		@idSite								= C.idSite,
		@name								= C.name,
		@issuingOrganization				= C.issuingOrganization,
		@description						= C.[description],
		@credits							= C.credits,
		@code								= C.code,
		@filename							= C.[filename],
		@isActive							= C.isActive,
		@activationDate						= C.activationDate,
		@expiresInterval					= C.expiresInterval,
		@expiresTimeframe					= C.expiresTimeframe,
		@objectType							= C.objectType,
		@idObject							= C.idObject,
		@isDeleted							= C.isDeleted,
		@dtDeleted							= C.dtDeleted,
		@reissueBasedOnMostRecentCompletion	= C.reissueBasedOnMostRecentCompletion
	FROM tblCertificate C
	WHERE C.idCertificate = @idCertificate
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificateDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO