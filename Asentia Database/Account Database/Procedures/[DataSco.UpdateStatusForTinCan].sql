-- =====================================================================
-- PROCEDURE: [DataSCO.UpdateStatusForTinCan]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataSCO.UpdateStatusForTinCan]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataSCO.UpdateStatusForTinCan]
GO

/*

Updates SCO data for internally launched TinCan packages.

*/

CREATE PROCEDURE [DataSCO.UpdateStatusForTinCan]
(
	@Return_Code			INT								OUTPUT,
	@Error_Description_Code	NVARCHAR(50)					OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@packageType			NVARCHAR(10),
	@idDataLesson           INT,
	@activityId             NVARCHAR(300)
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
		
	validate sco data exists
		
	*/
		
	IF (SELECT COUNT(1)
		FROM [tblData-SCO]
		WHERE idSite = @idCallerSite
		AND [idData-Lesson] = @idDataLesson
		) = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataSCOUpdateStatusForTinCan_NoRecordFound'
		RETURN 1
		END

    /*

	exit if the lesson data is already completed
	
	*/

	IF (SELECT COUNT(1) FROM [tblData-Lesson] 
		WHERE [idData-Lesson] = @idDataLesson 
		AND dtCompleted IS NOT NULL
		) = 1
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		RETURN 1
		END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*
	
	find success status and completion status, and get the score saved in tblData-TinCan table while running the package

	*/

	DECLARE @completionStatus	INT
	DECLARE @successStatus		INT

	DECLARE @result TABLE (verbId INT, scoreScaled FLOAT)

	INSERT INTO @result 
	SELECT TOP (1) 
		verbId,
		scorescaled AS scorescaled
	FROM [tblData-TinCan]
	WHERE scoreScaled IS NOT NULL 
	AND [idData-Lesson] = @idDataLesson 
	AND activityId = @activityId
	ORDER BY dtStored DESC
		
	DECLARE @verbId			INT
	DECLARE @scoreScaled	FLOAT

	SELECT 
		@scoreScaled = scoreScaled,
		@verbId = verbId  
	FROM @result


	IF (@verbId = 15) -- passed
		BEGIN
		SET @completionStatus = 2 -- completed
		SET @successStatus = 4  -- passed
	    END
	ELSE IF (@verbId = 9) -- failed
		BEGIN
		SET @completionStatus = 3 -- incomplete
		SET @successStatus = 5 -- failed
		END 
	ELSE IF (@verbId = 6) -- completed
		BEGIN
		SET @completionStatus = 2 -- completed
		SET @successStatus = 0 -- unknown
	    END 
    ELSE
	    BEGIN
		SET @completionStatus = 3 -- incomplete
		SET @successStatus = 0 -- unknown
		END

	/*

	update status and score in tblData-SCO

	*/

	UPDATE [tblData-SCO] SET
		completionStatus = @completionStatus,
		successStatus	 = @successStatus,
		scoreScaled		 = @scoreScaled,
		[timestamp]		 = @utcNow
	WHERE [idData-Lesson] = @idDataLesson
		
	/*
		
	update lesson data record
		
	*/

	IF (@completionStatus = 2) -- completed
		BEGIN
		
		UPDATE [tblData-Lesson] SET dtCompleted = GETUTCDATE() WHERE [idData-Lesson] = @idDataLesson

		-- If launcher is a learner then set contentTypeCommittedTo flag on  
		IF (@packageType = 'learner')
			BEGIN
			UPDATE [tblData-Lesson] SET contentTypeCommittedTo = 1 
			WHERE [idData-Lesson] = @idDataLesson
			AND idSite = @idCallerSite
			END
		-- OJT launch, update proctoring user
		ELSE
			BEGIN
				UPDATE [tblData-SCO] SET 
					proctoringUser = @idCaller 
				WHERE idSite = @idCallerSite 
				AND [idData-Lesson] = @idDataLesson
			END
		
		END

	/*

	do event log entry for completed, passed, or failed

	*/

	DECLARE @eventLogItem EventLogItemObjects
	DECLARE @idLesson INT
	DECLARE @idUser INT
	DECLARE @idEventType INT

	SELECT
		@idLesson = LD.idLesson,
		@idUser = E.idUser
	FROM [tblData-Lesson] LD
	LEFT JOIN tblEnrollment E ON E.idEnrollment = LD.idEnrollment
	WHERE LD.[idData-Lesson] = @idDataLesson

	INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idLesson, @idDataLesson, @idUser

	IF (@completionStatus = 2 AND @successStatus <> 5 AND @successStatus <> 4)
		BEGIN
		SET @idEventType = 303 -- completed
		END

	IF (@successStatus = 4)
		BEGIN
		SET @idEventType = 301 -- passed
		END

	IF (@successStatus = 5)
		BEGIN
		SET @idEventType = 302 -- failed
		END

	IF @idEventType IS NOT NULL
		BEGIN
		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, @idEventType, @utcNow, @eventLogItem
		END
	/*

	check for course completion

	*/

	DECLARE @idEnrollment INT
	SELECT @idEnrollment = idEnrollment FROM [tblData-Lesson] WHERE [idData-Lesson] = @idDataLesson

	EXEC [Enrollment.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @idEnrollment

	/*
	
	update first launch date and expiration from first launch date for enrollment

	*/

	-- first launch
	UPDATE tblEnrollment SET
		dtFirstLaunch = GETUTCDATE() 		   
	WHERE idEnrollment = @idEnrollment
	AND dtFirstLaunch IS NULL

	-- expiration from first launch
	UPDATE tblEnrollment SET
		dtExpiresFromFirstLaunch = CASE WHEN dtExpiresFromFirstLaunch IS NULL AND expiresFromFirstLaunchInterval IS NOT NULL AND expiresFromFirstLaunchTimeframe IS NOT NULL THEN 
										CASE WHEN expiresFromFirstLaunchTimeframe = 'd' THEN
											DATEADD(d, expiresFromFirstLaunchInterval, dtFirstLaunch)
										WHEN expiresFromFirstLaunchTimeframe = 'ww' THEN
											DATEADD(ww, expiresFromFirstLaunchInterval, dtFirstLaunch)
										WHEN expiresFromFirstLaunchTimeframe = 'm' THEN
											DATEADD(m, expiresFromFirstLaunchInterval, dtFirstLaunch)
										WHEN expiresFromFirstLaunchTimeframe = 'yyyy' THEN
											DATEADD(yyyy, expiresFromFirstLaunchInterval, dtFirstLaunch)
										ELSE
											dtExpiresFromFirstLaunch
										END
								   ELSE
										dtExpiresFromFirstLaunch
								   END
	FROM tblEnrollment E
	WHERE E.idEnrollment = @idEnrollment
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO