-- =====================================================================
-- PROCEDURE: [Purchase.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Purchase.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Purchase.Details]
GO

/*

Return all the properties for a given purchase id.

*/
CREATE PROCEDURE [Purchase.Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@idPurchase				INT				OUTPUT,
	@idUser					INT				OUTPUT, 
	@orderNumber			NVARCHAR(12)	OUTPUT,
	@timeStamp				DATETIME		OUTPUT,
	@ccnum					NVARCHAR(4)		OUTPUT,
	@currency				NVARCHAR(4)		OUTPUT,
	@idSite					INT				OUTPUT,
	@dtPending				DATETIME		OUTPUT, 
	@failed					BIT				OUTPUT,
	@transactionId			NVARCHAR(255)	OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblPurchase
		WHERE idPurchase = @idPurchase
		AND idSite = @idCallerSite		
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'PurchaseDetails_NoRecordFound' 
		RETURN 1
		END	
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idPurchase		= P.idPurchase,
		@idUser			= P.idUser,
		@orderNumber	= P.orderNumber,
		@timeStamp		= P.[timeStamp],
		@ccnum			= P.ccnum,
		@currency		= P.currency,
		@idSite			= P.idSite,
		@dtPending		= P.dtPending,
		@failed			= P.failed,
		@transactionId	= P.transactionId
	FROM tblPurchase P	
	WHERE P.idPurchase = @idPurchase
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'PurchaseDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO