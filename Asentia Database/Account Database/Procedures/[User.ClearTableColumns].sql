-- =====================================================================
-- PROCEDURE: [User.ClearTableColumns]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.ClearTableColumns]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.ClearTableColumns]
GO

/*

Clear Table Columns for None-Date Data

*/
CREATE PROCEDURE [User.ClearTableColumns]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
		
	@columns				NVARCHAR (512)
)
AS
	BEGIN
	SET NOCOUNT ON
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
		
	update the columns data with all date ready data, erase all not date formate data. 

	*/


	DECLARE @columnName NVARCHAR(100)
	DECLARE @sqlText nvarchar(1000); 
	DECLARE @getid CURSOR

	SET @getid = CURSOR FOR
	SELECT COLUMN_NAME
		FROM INFORMATION_SCHEMA.COLUMNS C
		WHERE TABLE_NAME = 'tblUser' AND COLUMN_NAME IN 
			(SELECT uf.s
			 FROM DelimitedStringToTable( @columns,',') UF)

	OPEN @getid
	FETCH NEXT
	FROM @getid INTO @columnName 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sqlText = N'UPDATE tblUser SET ' + @columnName +' = null WHERE idSite = ' + CONVERT(varchar(10), @idCallerSite)
		Exec (@sqlText)

		FETCH NEXT
		FROM @getid INTO @columnName 
	END

	CLOSE @getid
	DEALLOCATE @getid

	SELECT @Return_Code = 0
 				
	END			

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
