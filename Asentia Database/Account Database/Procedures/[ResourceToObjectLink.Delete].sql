-- =====================================================================
-- PROCEDURE: [ResourceToObjectLink.Delete]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ResourceToObjectLink.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ResourceToObjectLink.Delete]
GO

/*

Deletes a Resource

*/

CREATE PROCEDURE [ResourceToObjectLink.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (50)	OUTPUT, 
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	@ResourceToObjectLinkIds	IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @ResourceToObjectLinkIds) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'ResourceToObjectLinkDelete_NoRecordsSelected'
		RETURN 1
		END
			
	/*
	
	validate that all Resources exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @ResourceToObjectLinkIds CC
		LEFT JOIN tblResourceToObjectLink C ON C.id = CC.id
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'ResourceToObjectLinkDelete_NoRecordsSelected'			
		RETURN 1 
		END
			
	/*
	
	Delete the object links
	
	*/
	
	DELETE FROM tblResourceToObjectLink
	WHERE id IN (
		SELECT id
		FROM @ResourceToObjectLinkIds
	)
	
	SELECT @Return_Code = 0
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO