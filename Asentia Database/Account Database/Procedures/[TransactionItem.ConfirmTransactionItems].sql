-- =====================================================================
-- PROCEDURE: [TransactionItem.ConfirmTransactionItems]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.ConfirmTransactionItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.ConfirmTransactionItems]
GO

/*

Confirms transaction item(s).

*/

CREATE PROCEDURE [TransactionItem.ConfirmTransactionItems]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,
	
	@TransactionItems			IDTable			READONLY,
	@disassociateFromPurchase	BIT
)
AS	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*

	validate that there are records to confirm

	*/

	IF (SELECT COUNT(1) FROM @TransactionItems) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'TIConfirmTransactionItems_NoRecordsSelected'
		RETURN 1
		END

	/*
	
	validate that all transaction items exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @TransactionItems TI
		LEFT JOIN tblTransactionItem T ON T.idTransactionItem = TI.id
		WHERE T.idSite IS NULL
		OR T.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TIConfirmTransactionItems_NoRecordsFound'
		RETURN 1 
		END	
		
	/*

	mark the transaction items as confirmed

	*/

	UPDATE tblTransactionItem SET
		confirmed = 1,
		idPurchase = CASE WHEN @disassociateFromPurchase = 1 THEN NULL ELSE idPurchase END
	WHERE idTransactionItem IN (
		SELECT id
		FROM @TransactionItems
	)
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
