-- =====================================================================
-- PROCEDURE: [Resource.IdsAndNamesForResourceOwnersSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Resource.IdsAndNamesForResourceOwnersSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Resource.IdsAndNamesForResourceOwnersSelectList]
GO

/*

Return all the parent Resources for caller site

*/
CREATE PROCEDURE [Resource.IdsAndNamesForResourceOwnersSelectList]
(
	@Return_Code				INT					OUTPUT,
	@Error_Description_Code		NVARCHAR (255)		OUTPUT,
	@idCaller					INT,
	@idCallerSite				INT			= 0,	 --default if not specified
	@callerLangString			NVARCHAR (10),
	@idResource					INT,
	@searchParam				NVARCHAR(4000)
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	/*
	
	get the data 
	
	*/
	IF @searchParam = '' OR @searchParam = '*' --@idParentResource
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
			
			BEGIN	
			
			SELECT DISTINCT
				R.idResource,
				R.name
			FROM tblResource R
			WHERE R.idSite = @idCallerSite
				AND (isDeleted IS NULL OR isDeleted = 0)
				AND R.idResource <> (SELECT R.idResource FROM tblResource R
								WHERE R.idResource = @idResource
								)
				AND	R.idParentResource IS NULL
			ORDER BY 2
	END

		ELSE

			BEGIN

			SELECT DISTINCT
				R.idResource,
				R.name
			FROM tblResource R
				INNER JOIN CONTAINSTABLE(tblResource, *, @searchParam) K ON K.[key] = R.idResource
			WHERE R.idSite = @idCallerSite
				AND (isDeleted IS NULL OR isDeleted = 0)
			ORDER BY 2

			END
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO