-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.RemoveUser]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[StandupTrainingInstance.RemoveUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.RemoveUser]
GO

/*

Removes a user from a standup training instance.

*/

CREATE PROCEDURE [StandupTrainingInstance.RemoveUser]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT,
	@idUser						INT,
	@idUserToPromote			INT				OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
			
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get some parameters for the instance and the user link that we'll need

	*/

	DECLARE @totalSeats INT
	DECLARE @seatsRemaining INT -- get the value of this post-delete
	DECLARE @isWaitingList BIT

	SELECT @totalSeats = seats FROM tblStandUpTrainingInstance WHERE idStandUpTrainingInstance = @idStandupTrainingInstance AND idSite = @idCallerSite
	SELECT @isWaitingList = isWaitingList FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND idUser = @idUser

	/*

	log the removal so that event email can be fired

	*/

	DECLARE @idEventType INT

	IF @isWaitingList = 1
		BEGIN
		SET @idEventType = 408 -- learner removed from waitlist
		END
	ELSE
		BEGIN
		SET @idEventType = 405 -- learner removed from roster
		END
	
	DECLARE @learnerRemovedEventLogItems EventLogItemObjects
	DECLARE @utcNow DATETIME

	SET @utcNow = GETUTCDATE()

	INSERT INTO @learnerRemovedEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		STI.idStandUpTraining, 
		@idStandupTrainingInstance,
		@idUser	
	FROM tblStandUpTrainingInstance STI
	WHERE STI.idStandUpTrainingInstance = @idStandupTrainingInstance

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, @idEventType, @utcNow, @learnerRemovedEventLogItems

	/*

	remove the user from the instance

	*/

	DELETE FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND idUser = @idUser

	/*

	if this was a waitlist removal, re-order the waitlist

	*/

	IF @isWaitingList = 1
		BEGIN

		UPDATE tblStandupTrainingInstanceToUserLink SET
			waitlistOrder = MAIN.rowNumber
		FROM (SELECT
			  ROW_NUMBER() OVER(ORDER BY waitlistOrder) AS rowNumber,
			  S.*
			  FROM tblStandupTrainingInstanceToUserLink S
			  WHERE S.idStandupTrainingInstance = @idStandupTrainingInstance
			  AND S.isWaitingList = 1) AS MAIN
		WHERE tblStandupTrainingInstanceToUserLink.idStandupTrainingInstanceToUserLink = MAIN.idStandupTrainingInstanceToUserLink

		END

	/*

	if this was not a waitlist removal, find the next user to promote

	*/

	SET @idUserToPromote = NULL

	IF @isWaitingList <> 1
		BEGIN

		SELECT @seatsRemaining = @totalSeats - COUNT(1) FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND isWaitingList <> 1

		IF @seatsRemaining > 0
			BEGIN
			SET @idUserToPromote = (SELECT TOP 1 idUser FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND isWaitingList = 1 ORDER BY waitlistOrder)
			END

		END
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

