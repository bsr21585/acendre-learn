-- =====================================================================
-- PROCEDURE: [TransactionItem.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.Details]
GO

/*

Return all the properties for a given transaction item id.

*/
CREATE PROCEDURE [TransactionItem.Details]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	
	@idTransactionItem			INT				OUTPUT,
	@idParentTransactionItem	INT				OUTPUT, 
	@idPurchase					INT				OUTPUT,
	@idEnrollment				INT				OUTPUT,
	@idUser						INT				OUTPUT,
	@userName					NVARCHAR(255)	OUTPUT,
	@idAssigner					INT				OUTPUT,
	@assignerName				NVARCHAR(255)	OUTPUT, 
	@itemId						INT				OUTPUT, 
	@itemName					NVARCHAR(255)	OUTPUT,
	@itemType					INT				OUTPUT,
	@description				NVARCHAR(512)	OUTPUT,
	@cost						FLOAT			OUTPUT,
	@paid						FLOAT			OUTPUT,
	@idCouponCode				INT				OUTPUT,
	@couponCode					NVARCHAR(10)	OUTPUT,
	@dtAdded					DATETIME		OUTPUT,
	@confirmed					BIT				OUTPUT,
	@idSite						INT				OUTPUT,
	@idIltSession				INT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblTransactionItem
		WHERE idTransactionItem = @idTransactionItem
		AND idSite = @idCallerSite		
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idTransactionItem			= T.idTransactionItem,
		@idParentTransactionItem	= T.idParentTransactionItem,
		@idPurchase					= T.idPurchase,
		@idEnrollment				= T.idEnrollment,
		@idUser						= T.idUser,
		@userName					= T.userName,
		@idAssigner					= T.idAssigner,
		@assignerName				= T.assignerName,
		@itemId						= T.itemId,
		@itemName					= T.itemName,
		@itemType					= T.itemType,
		@description				= T.[description],
		@cost						= T.cost,
		@paid						= T.paid,
		@idCouponCode				= T.idCouponCode,
		@couponCode					= T.couponCode,
		@dtAdded					= T.dtAdded,
		@confirmed					= T.confirmed,
		@idSite						= T.idSite,
		@idIltSession				= T.idIltSession
	FROM tblTransactionItem T	
	WHERE T.idTransactionItem = @idTransactionItem
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'TransactionItemDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO