-- =====================================================================
-- PROCEDURE: [User.ApproveRejectRegistration]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.ApproveRejectRegistration]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.ApproveRejectRegistration]
GO

/*

Approves or rejects a user's pending registration.

*/

CREATE PROCEDURE [User.ApproveRejectRegistration]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idUser					INT,
	@isRegistrationApproved	BIT,			--1 for approved, 0 for rejected
	@idApprover				INT				= NULL,
	@rejectionComments		NVARCHAR(MAX)	= NULL	
)
AS

	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	declare utc now

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	update the user with approval or rejection info

	*/

	UPDATE tblUser SET 
		isRegistrationApproved = @isRegistrationApproved,
		dtApproved = CASE WHEN @isRegistrationApproved = 1 THEN @utcNow ELSE NULL END,
		dtDenied = CASE WHEN @isRegistrationApproved = 0 THEN @utcNow ELSE NULL END,
		idApprover = @idApprover,
		rejectionComments = @rejectionComments		
	WHERE idUser = @idUser
	AND idSite = @idCallerSite

	/*

	do the event log entry

	*/
	
	DECLARE @eventLogItems EventLogItemObjects
	DECLARE @idEvent INT

	IF @isRegistrationApproved = 1
		SET @idEvent = 104 /* registration approved */
	ELSE
		SET @idEvent = 105 /* registration rejected */	

	INSERT INTO @eventLogItems (idSite, idObject, idObjectRelated, idObjectUser) VALUES (@idCallerSite, @idUser, @idUser, @idUser)

	EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, @idEvent, @utcNow, @eventLogItems			
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO