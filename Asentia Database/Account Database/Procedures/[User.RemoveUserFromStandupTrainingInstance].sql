-- =====================================================================
-- PROCEDURE: [User.RemoveUserFromStandupTrainingInstance]
-- =====================================================================

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.RemoveUserFromStandupTrainingInstance]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.RemoveUserFromStandupTrainingInstance]
GO

/*

Gets a grid of groups a user belongs to.

*/


CREATE PROCEDURE [User.RemoveUserFromStandupTrainingInstance] 
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
		
	@idUser					INT,
	@idSession				INT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	make sure there are objects in the table parameter
	
	*/
	
	IF (SELECT COUNT(1) FROM tblStandUpTrainingInstance where idStandUpTrainingInstance = @idSession) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'UserRemoveUserFromSTI_NoSTIFound'
		RETURN 1
		END
		
	/*
	
	validate that the user exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUser
		WHERE idSite = @idCallerSite
		AND @idUser = idUser
		) <> 1
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'UserRemoveUserFromSTI_NoUserFound'
		RETURN 1 
		END
		
	
	/*
	
	remove user from standup training instance
	
	*/
	
	DELETE FROM tblStandupTrainingInstanceToUserLink
	WHERE idUser = @idUser AND 
		  idStandupTrainingInstance = @idSession
		  
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO