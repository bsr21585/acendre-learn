-- =====================================================================
-- PROCEDURE: [RuleSetLearningPathEnrollment.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetLearningPathEnrollment.GetGrid]
GO

/*

Gets a listing of ruleset learning path enrollments.

*/

CREATE PROCEDURE [RuleSetLearningPathEnrollment.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,

	@idLearningPath			INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END
		
		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
		
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblRuleSetLearningPathEnrollment RSLPE
			WHERE 
				(
					(@idCallerSite IS NULL)
					OR 
					(@idCallerSite IS NOT NULL AND RSLPE.idSite = @idCallerSite)
				)
			AND RSLPE.idLearningPath = @idLearningPath

			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						RSLPE.idRuleSetLearningPathEnrollment,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN RSLPE.[priority] END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RSLPE.[priority] END
						)
						AS [row_number]
					FROM tblRuleSetLearningPathEnrollment RSLPE
					WHERE 
						(
							(@idCallerSite IS NULL)
							OR 
							(@idCallerSite IS NOT NULL AND RSLPE.idSite = @idCallerSite)
						)
					AND RSLPE.idLearningPath = @idLearningPath
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idRuleSetLearningPathEnrollment, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				RSLPE.idRuleSetLearningPathEnrollment,				
				CASE WHEN RSLPEL.label IS NOT NULL THEN RSLPEL.label ELSE RSLPE.label END AS label,
				RSLPE.[priority],
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = SelectedKeys.idRuleSetLearningPathEnrollment
			LEFT JOIN tblRuleSetLearningPathEnrollmentLanguage RSLPEL ON RSLPEL.idRuleSetLearningPathEnrollment = RSLPE.idRuleSetLearningPathEnrollment AND RSLPEL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
			
			END
		
		ELSE
		
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblRuleSetLearningPathEnrollment RSLPE
				INNER JOIN CONTAINSTABLE(tblRuleSetLearningPathEnrollment, *, @searchParam) K ON K.[key] = RSLPE.idRuleSetLearningPathEnrollment
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND RSLPE.idSite = @idCallerSite)
					)
				AND RSLPE.idLearningPath = @idLearningPath
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							RSLPE.idRuleSetLearningPathEnrollment,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN RSLPE.[priority] END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RSLPE.[priority] END
							)
							AS [row_number]
						FROM tblRuleSetLearningPathEnrollment RSLPE
						INNER JOIN CONTAINSTABLE(tblRuleSetLearningPathEnrollment, *, @searchParam) K ON K.[key] = RSLPE.idRuleSetLearningPathEnrollment
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND RSLPE.idSite = @idCallerSite
							)
						AND RSLPE.idLearningPath = @idLearningPath
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idRuleSetLearningPathEnrollment, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					RSLPE.idRuleSetLearningPathEnrollment,
					RSLPE.label AS label,
					RSLPE.[priority],
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = SelectedKeys.idRuleSetLearningPathEnrollment
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblRuleSetLearningPathEnrollmentLanguage RSLPEL
				LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = RSLPEL.idRuleSetLearningPathEnrollment
				INNER JOIN CONTAINSTABLE(tblRuleSetLearningPathEnrollmentLanguage, *, @searchParam) K ON K.[key] = RSLPEL.idRuleSetLearningPathEnrollmentLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND RSLPE.idSite = @idCallerSite)
					)
				AND RSLPEL.idLanguage = @idCallerLanguage
				AND RSLPE.idLearningPath = @idLearningPath
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							RSLPEL.idRuleSetLearningPathEnrollmentLanguage,
							RSLPE.idRuleSetLearningPathEnrollment,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN RSLPE.[priority] END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RSLPE.[priority] END
							)
							AS [row_number]
						FROM tblRuleSetLearningPathEnrollmentLanguage RSLPEL
						LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = RSLPEL.idRuleSetLearningPathEnrollment
						INNER JOIN CONTAINSTABLE(tblRuleSetLearningPathEnrollmentLanguage, *, @searchParam) K ON K.[key] = RSLPEL.idRuleSetLearningPathEnrollmentLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND RSLPE.idSite = @idCallerSite
							)
						AND RSLPEL.idLanguage = @idCallerLanguage
						AND RSLPE.idLearningPath = @idLearningPath
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idRuleSetLearningPathEnrollment, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					RSLPE.idRuleSetLearningPathEnrollment, 
					RSLPEL.label AS label,
					RSLPE.[priority],				
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblRuleSetLearningPathEnrollmentLanguage RSLPEL ON RSLPEL.idRuleSetLearningPathEnrollment = SelectedKeys.idRuleSetLearningPathEnrollment AND RSLPEL.idLanguage = @idCallerLanguage
				LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = RSLPEL.idRuleSetLearningPathEnrollment
				ORDER BY SelectedKeys.[row_number]

				END
			
			END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO