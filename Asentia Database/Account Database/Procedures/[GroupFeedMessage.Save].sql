-- =====================================================================
-- PROCEDURE: [GroupFeedMessage.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GroupFeedMessage.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GroupFeedMessage.Save]
GO

/*

Adds new group feed message or updates existing group feed message. 

*/

CREATE PROCEDURE [GroupFeedMessage.Save]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	
	@idGroupFeedMessage			INT				OUTPUT,
	@idGroup					INT,
	@idAuthor					INT,
	@idParentGroupFeedMessage	INT,
	@message					NVARCHAR(MAX),
	@isApproved					BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/* 
	
	save the data 
	
	*/

	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	IF (@idGroupFeedMessage = 0 OR @idParentGroupFeedMessage IS NULL)

		BEGIN

		-- insert the group feed message

		INSERT INTO tblGroupFeedMessage (
			idSite,
			idGroup,
			idLanguage,
			idAuthor,
			idParentGroupFeedMessage,
			[message],
			[timestamp],
			isApproved
		)
		VALUES (
			@idCallerSite,
			@idGroup,
			@idLanguage,
			@idCaller,
			@idParentGroupFeedMessage,
			@message,
			GETUTCDATE(),
			@isApproved
		)

		-- get the new group feed message's id and return successfully

		SELECT @idGroupFeedMessage = SCOPE_IDENTITY()

		END

	ELSE

		BEGIN

		-- check that the group feed message id exists
		IF (SELECT COUNT(1) FROM tblGroupFeedMessage WHERE idGroupFeedMessage = @idGroupFeedMessage AND idSite = @idCallerSite) < 1
			BEGIN

			SET @idGroupFeedMessage = @idGroupFeedMessage
			SET @Return_Code = 1 --(1 is 'details not found')
			SET @Error_Description_Code = 'GroupFeedMessageSave_NoRecordFound'
			RETURN 1
			
			END

		-- update the existing group feed message's properties

		UPDATE tblGroupFeedMessage SET
			idSite = @idCallerSite,
			idGroup = @idGroup,
			idLanguage = @idLanguage,
			idAuthor = @idCaller,
			idParentGroupFeedMessage = @idParentGroupFeedMessage,
			[message] = @message,
			[timestamp] = GETUTCDATE(),
			isApproved = @isApproved
		WHERE idGroupFeedMessage = @idGroupFeedMessage

		-- get the group feed message's id 
		SELECT @idGroupFeedMessage = @idGroupFeedMessage

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO