-- =====================================================================
-- PROCEDURE: [Group.JoinUsers]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.JoinUsers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.JoinUsers]
GO

CREATE PROCEDURE [Group.JoinUsers]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	--   default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, 
		
	@idGroup				INT,
	@Users					IDTable			READONLY,
	@isSelfJoined			BIT				= 0
)
AS

	BEGIN
	
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	make sure there are objects in the table parameter
	
	*/
	
	IF (SELECT COUNT(1) FROM @Users) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'GroupJoinUsers_NoUsersFound'
		RETURN 1
		END
		
	/*

	validate group exists
	
	*/

	IF (
		SELECT COUNT(1)
		FROM tblGroup
		WHERE idGroup = @idGroup
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupJoinUsers_DetailsNotFound'
		RETURN 1
		END
		
	/*
	
	validate that the objects exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Users UU
		LEFT JOIN tblUser U ON U.idUser = UU.id
		WHERE U.idSite IS NULL
		OR U.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupJoinUsers_UsersNotInSameSite'
		RETURN 1
		END
	
	/*
	
	attach the group to users where they do not already exist
	
	*/

	INSERT INTO tblUserToGroupLink (
		idSite,
		idGroup,
		idUser,
		created,
		selfJoined
	)
	SELECT 
		@idCallerSite,
		@idGroup, 
		UU.id,
		GETUTCDATE(),
		CASE WHEN @isSelfJoined = 1 THEN 1 ELSE 0 END
	FROM @Users UU
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblUserToGroupLink UGL
		WHERE UGL.idGroup = @idGroup
		AND UGL.idUser = UU.id
		AND idRuleSet IS NULL -- manual join
	)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

	
