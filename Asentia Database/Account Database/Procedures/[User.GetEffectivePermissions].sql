-- =====================================================================
-- PROCEDURE: [User.GetEffectivePermissions]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[User.GetEffectivePermissions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.GetEffectivePermissions]
GO

/*

Returns effective permissions for a user.

*/

CREATE PROCEDURE [User.GetEffectivePermissions]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@dtLastPermissionCheck	DATETIME		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END


	
	/*

	get all groups the user is a member of

	*/

	CREATE TABLE #UserGroups (
		idGroup INT
	)

	INSERT INTO #UserGroups (
		idGroup
	)
	SELECT DISTINCT
		UGL.idGroup
	FROM tblUserToGroupLink UGL
	WHERE UGL.idUser = @idCaller

	/*

	get all the roles the user is a member of directly and by group

	*/

	CREATE TABLE #Roles (
		idRole INT
	)

	/* direct membership */

	INSERT INTO #Roles (
		idRole
	)
	SELECT DISTINCT
		URL.idRole
	FROM tblUserToRoleLink URL
	WHERE URL.idUser = @idCaller

	/* membership from group */

	INSERT INTO #Roles (
		idRole
	)
	SELECT
		GRL.idRole
	FROM tblGroupToRoleLink GRL
	WHERE GRL.idGroup IN (SELECT idGroup FROM #UserGroups)

	/*

	get permissions from roles

	*/

	CREATE TABLE #RolePermissions (
		idPermission INT,
		scope NVARCHAR(MAX)
	)

	INSERT INTO #RolePermissions (
		idPermission,
		scope
	)
	SELECT
		RPL.idPermission,
		RPL.scope
	FROM tblRoleToPermissionLink RPL
	WHERE RPL.idRole IN (SELECT DISTINCT idRole FROM #Roles)


	/*

	return effective permissions
	
	*/
	
	SELECT DISTINCT
		idPermission,
		scope
	FROM #RolePermissions
	
	/*
	
	kill temporary tables
	
	*/
	
	DROP TABLE #UserGroups
	DROP TABLE #Roles
	DROP TABLE #RolePermissions

	-- block the trigger from executing in tblUser UPDATE
	DECLARE @CONTEXT VARBINARY(128)

	SET @CONTEXT = CONVERT(VARBINARY(128), 'BLOCK TRIGGER')
	SET CONTEXT_INFO @CONTEXT

	-- set/update last permission check timestamp
	SET @dtLastPermissionCheck = GETUTCDATE()
	UPDATE tblUser SET dtLastPermissionCheck = @dtLastPermissionCheck WHERE idUser = @idCaller

	-- unblock the trigger
	SET @CONTEXT = CONVERT(VARBINARY(128), '')
	SET CONTEXT_INFO @CONTEXT
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO