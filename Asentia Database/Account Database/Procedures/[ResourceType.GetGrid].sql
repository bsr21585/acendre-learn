-- =====================================================================
-- PROCEDURE: [ResourceType.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ResourceType.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ResourceType.GetGrid]
GO

/*

Gets a listing of Coupon codes.

*/

Create PROCEDURE [dbo].[ResourceType.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@idCaller				INT				= 0,
	@callerLangString		NVARCHAR (10),
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		/*

		begin getting the grid data

		*/
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblResourceType RT
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND RT.idSite = @idCallerSite)
				)
			AND RT.isDeleted = 0
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						RT.idResourceType,	
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'resourceType' THEN RT.ResourceType END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'resourceType' THEN RT.ResourceType END) END
						)
						AS [row_number]
					FROM tblResourceType RT
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND RT.idSite = @idCallerSite)
						)
					AND RT.isDeleted = 0
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idResourceType, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				RT.idResourceType,
				RT.idSite,	
				CAST(1 AS BIT) AS isModifyOn,
				(CASE WHEN RT.ResourceType IS NULL OR RT.ResourceType = '' THEN '' ELSE RT.ResourceType END) AS ResourceType
			FROM SelectedKeys 
			INNER JOIN tblResourceType RT ON RT.idResourceType = SelectedKeys.idResourceType
		    WHERE idSite = @idCallerSite
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- search and display on the base table
			

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblResourceType RT
				INNER JOIN CONTAINSTABLE(tblResource, *, @searchParam) K ON K.[key] = RT.idResourceType
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND RT.idSite = @idCallerSite)
					)
				AND RT.isDeleted = 0
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							RT.idResourceType,
							ROW_NUMBER() OVER (ORDER BY
								-- ORDER DESC
								CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'resourceType' THEN RT.ResourceType END) END DESC,
							
								-- ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'resourceType' THEN RT.ResourceType END) END
							)
							AS [row_number]
						FROM tblResourceType RT
						INNER JOIN CONTAINSTABLE(tblResourceType, *, @searchParam) K ON K.[key] = RT.idResourceType
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND RT.idSite = @idCallerSite
							)
						AND RT.isDeleted = 0
						), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idResourceType, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
					
					SELECT 
				RT.idResourceType,
				CAST(1 AS BIT) as isModifyOn,
				(CASE WHEN RT.ResourceType IS NULL OR RT.ResourceType = '' THEN '' ELSE RT.ResourceType END) AS ResourceType
			FROM SelectedKeys
			INNER JOIN tblResourceType RT ON RT.idResourceType = SelectedKeys.idResourceType
		    AND RT.idSite = @idCallerSite
			ORDER BY SelectedKeys.[row_number]
				END

	END			
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
