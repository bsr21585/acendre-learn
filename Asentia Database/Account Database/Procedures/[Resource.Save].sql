-- =====================================================================
-- PROCEDURE: [Resource.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Resource.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Resource.Save]
GO

/*

Adds new Resource or updates existing Resource.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [Resource.Save]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, -- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0, -- will fail if not specified
	@idResource					INT				OUTPUT,
	@idOwner					INT			,
	@idParentResource			INT			,
	@name						NVARCHAR(255),
	@idResourceType				INT,
	@description				NVARCHAR(MAX),
	@identifier					NVARCHAR(50) ,
	@isMovable					BIT,
	@isAvailable				BIT,
	@capacity					NVARCHAR(255),
	@room						NVARCHAR(255),
	@building					NVARCHAR(255),
	@city						NVARCHAR(255),
	@province					NVARCHAR(255),
	@country					NVARCHAR(255),
	@ownerWithinSystem			BIT			,
	@ownerName					NVARCHAR(255),
	@ownerEmail					NVARCHAR(255),
	@ownerContactInfo			NVARCHAR(512)
	)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END	
					 
	/*
	
	validate uniqueness for Resource, if specified
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblResource
		WHERE idSite = @idCallerSite
		AND name = @name
		AND ISNULL(isDeleted,0) = 0 
		AND (
			@idResource IS NULL
			OR @idResource <> idResource
			)
		AND ( -- of same resource type 
			@idResourceType IS NULL 
			OR (@idResourceType = idResourceType)
			)
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'ResourceSave_ResourceNotUnique'		
		RETURN 1 
		END

	
	/*
	
	validate uniqueness within language
	
	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF( 
		SELECT COUNT(1)
		FROM tblResource R
		LEFT JOIN tblResourceLanguage RL ON RL.idResource = R.idResource AND RL.idLanguage = @idDefaultLanguage -- same language
		WHERE R.idSite = @idCallerSite -- same site
		AND R.idResource <> @idResource -- not the same Resource
		AND RL.name = @name -- validate parameter: name
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'ResourceSave_FieldNotUnique'
		RETURN 1
		END
		
	
		
	/*
	
	save the data
	
	*/
	
	IF (@idResource = 0 OR @idResource IS NULL)
		
		BEGIN
		
		-- insert the new Resource
		INSERT INTO tblResource (
			idSite, 
			name,
			idParentResource,
			idOwner,
			idResourceType,
			description,
			identifier,
			isMoveable,
			isAvailable,
			capacity,
			locationRoom,
			locationBuilding,
			locationCity,
			locationProvince,
			locationCountry,
			ownerWithinSystem,
			ownerName,
			ownerEmail,
			ownerContactInformation,
			isDeleted
		)
		VALUES (
			@idCallerSite, 
			@name,
			CASE WHEN @idParentResource = 0 THEN null ELSE @idParentResource END,
			CASE WHEN @ownerWithinSystem = 0 THEN null ELSE @idOwner END,
			@idResourceType,
			@description,
			@identifier ,
			@isMovable	,
			@isAvailable,
			@capacity	,
			@room	,
			@building ,
			@city,
			@province,
			@country ,
			@ownerWithinSystem,
			CASE WHEN @ownerWithinSystem = 1 THEN NULL ELSE @ownerName END,
			CASE WHEN @ownerWithinSystem = 1 THEN NULL ELSE @ownerEmail END,
			CASE WHEN @ownerWithinSystem = 1 THEN NULL ELSE @ownerContactInfo END,
			0
			)
		
		-- get the new Resource's id and return successfully
		
		SELECT @idResource = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the Resource id exists 
		IF (SELECT COUNT(1) FROM tblResource WHERE idResource = @idResource AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idResource = @idResource
			SELECT @Return_Code = 1
			SET @Error_Description_Code = 'ResourceSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing Resource's properties
		
		UPDATE tblResource SET
			name						= @name,
			idOwner						= CASE WHEN @ownerWithinSystem = 0 THEN  NULL ELSE @idOwner END,
			idParentResource			= CASE WHEN @idParentResource = 0 THEN null ELSE @idParentResource END,
			idResourceType				= @idResourceType,
		   description					= @description,
			identifier					= @identifier,
			isMoveable					= @isMovable,
			isAvailable					= @isAvailable,
			capacity					= @capacity,
			locationRoom				= @room,
			locationBuilding			= @building,
			locationCity				= @city,
			locationProvince			= @province,
			locationCountry				= @country,
			ownerWithinSystem			= @ownerWithinSystem,
			ownerName					= CASE WHEN @ownerWithinSystem = 1 THEN NULL ELSE @ownerName END,
			ownerEmail					= CASE WHEN @ownerWithinSystem = 1 THEN NULL ELSE @ownerEmail END,
			ownerContactInformation		= CASE WHEN @ownerWithinSystem = 1 THEN NULL ELSE @ownerContactInfo END	
		WHERE idResource	= @idResource
		AND idSite			= @idCallerSite	
		
		-- get the Resource's id 
		SELECT @idResource = @idResource
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id

	IF (SELECT COUNT(1) FROM tblResourceLanguage RL WHERE RL.idResource = @idResource AND RL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblResourceLanguage SET
			name = @name,
			[description] = @description
		WHERE idResource = @idResource
		AND idLanguage = @idDefaultLanguage
		AND idSite			= @idCallerSite	

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblResourceLanguage (
			idSite,
			idResource,
			idLanguage,
			name,
			description
		)
		SELECT
			@idCallerSite,
			@idResource,
			@idDefaultLanguage,
			@name,
			@description
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblResourceLanguage RL
			WHERE RL.idResource = @idResource
			AND RL.idLanguage = @idDefaultLanguage
			AND Rl.idSite = @idCallerSite	
		)

		END

	SELECT @Error_Description_Code = ''
	SELECT @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO