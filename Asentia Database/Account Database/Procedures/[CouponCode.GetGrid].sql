-- =====================================================================
-- PROCEDURE: [CouponCode.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CouponCode.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CouponCode.GetGrid]
GO

/*

Gets a listing of Coupon codes.

*/

CREATE PROCEDURE [CouponCode.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblCouponCode C
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
				)
				AND (C.isDeleted IS NULL OR C.isDeleted = 0)			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						C.idCouponCode,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN C.idCouponCode END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN C.idCouponCode END
						)
						AS [row_number]
					FROM tblCouponCode C
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
						)
						AND (C.isDeleted IS NULL OR C.isDeleted = 0)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idCouponCode, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				C.idCouponCode,
				C.code,
				C.discountType,
				C.discount AS discountValue,
				C.dtStart, 
				C.dtEnd, 
				C.usesAllowed,
				(SELECT
						(C.usesAllowed-
						(SELECT COUNT(*) AS Used FROM tblTransactionItem WHERE idCouponCode =C.idCouponCode and confirmed=1)-
						(SELECT COUNT(*) AS Pending FROM tblTransactionItem WHERE idCouponCode =C.idCouponCode and confirmed=0)))
							as usesRemaining,
						(SELECT
						((SELECT COUNT(*) AS Pending FROM tblTransactionItem WHERE idCouponCode =C.idCouponCode and confirmed=0))) AS usesPending,
						
				CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblCouponCode C ON C.idCouponCode = SelectedKeys.idCouponCode
			Where (C.isDeleted IS NULL OR C.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- search and display on the base table
			

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblCouponCode C
				INNER JOIN CONTAINSTABLE(tblCouponCode, *, @searchParam) K ON K.[key] = C.idCouponCode
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite)
					)
					AND (C.isDeleted IS NULL OR C.isDeleted = 0)
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							C.idCouponCode,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN C.Code END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN C.Code END
							)
							AS [row_number]
						FROM tblCouponCode C
						INNER JOIN CONTAINSTABLE(tblCouponCode, *, @searchParam) K ON K.[key] = C.idCouponCode
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND C.idSite = @idCallerSite
							)
							AND (C.isDeleted IS NULL OR C.isDeleted = 0)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idCouponCode, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
					
				SELECT 
						C.idCouponCode,
						C.code,
						C.discountType,
						C.discount AS discountValue,
						C.dtStart, 
						C.dtEnd, 
						C.usesAllowed,
						(SELECT
						(C.usesAllowed-
						(SELECT COUNT(1) AS Used FROM tblTransactionItem WHERE idCouponCode =C.idCouponCode and confirmed=1)-
						(SELECT COUNT(1) AS Pending FROM tblTransactionItem WHERE idCouponCode =C.idCouponCode and confirmed=0)))
							as usesRemaining,
						(SELECT
						((SELECT COUNT(1) AS Pending FROM tblTransactionItem WHERE idCouponCode =C.idCouponCode and confirmed=0))) AS usesPending,
 
					CONVERT(BIT, 1) AS isModifyOn, -- needs to be calculated
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblCouponCode C ON C.idCouponCode = SelectedKeys.idCouponCode
				where (C.isDeleted IS NULL OR C.isDeleted = 0)
				ORDER BY SelectedKeys.[row_number]

				END

	END			
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO