-- =====================================================================
-- PROCEDURE: [ReportSubscriptionQueue.Retry]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportSubscriptionQueue.Retry]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportSubscriptionQueue.Retry]
GO

/*

Retry report subscription queue item(s).

*/

CREATE PROCEDURE [ReportSubscriptionQueue.Retry]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT,
	
	@ReportSubscriptionQueueItems	IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @ReportSubscriptionQueueItems) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		RETURN 1
		END
			
	/*
	
	validate that all report subscription queue item(s) exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @ReportSubscriptionQueueItems EE
		LEFT JOIN tblReportSubscriptionQueue E ON EE.id = E.idReportSubscriptionQueue
		WHERE E.idSite IS NULL
		OR E.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		RETURN 1 
		END
	
		
	/*

	Retry the report subscription queue item(s).

	*/
	
	UPDATE tblReportSubscriptionQueue SET
		dtSent = NULL,
		statusDescription = NULL
	WHERE idReportSubscriptionQueue IN (
		SELECT id
		FROM @ReportSubscriptionQueueItems
	)
	

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO