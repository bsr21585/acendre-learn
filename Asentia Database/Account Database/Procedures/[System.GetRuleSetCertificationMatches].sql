-- =====================================================================
-- PROCEDURE: [System.GetRuleSetCertificationMatches]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetRuleSetCertificationMatches]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetRuleSetCertificationMatches]
GO

/*

Gets RuleSet matches for Certifications.

*/
CREATE PROCEDURE [System.GetRuleSetCertificationMatches]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,

	@Certifications				IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* SET FILTER TO 'user' IF IT IS ANYTHING OTHER THAN 'group' */
	IF @filterBy IS NULL OR LOWER(@filterBy) <> LOWER('group')
		SET @filterBy = 'user'

	/* CHECK CERTIFICATION AND FILTER LISTS FOR RECORDS */
	DECLARE @CertificationsRecordCount INT
	DECLARE @FiltersRecordCount INT
	SELECT @CertificationsRecordCount = COUNT(1) FROM @Certifications
	SELECT @FiltersRecordCount = COUNT(1) FROM @Filters

	/* 
	
		CREATE TEMPORARY TABLE TO STORE USER-TO-CERTIFICATION JOINS
		RESULTING FROM RULES EXECUTION
	*/
	CREATE TABLE #UserToCertificationJoins
	(
		idRuleSet		INT,
		idCertification	INT,
		idUser			INT
	)

	/* CREATE TEMPORARY TABLE FOR CERTIFICATION AUTO-JOIN RULES */
	CREATE TABLE #CertificationRules
	(
		idSite			INT,
		idRuleSet		INT,
		idCertification	INT,
		isAny			BIT,
		field			NVARCHAR(25),
		operator		NVARCHAR(25),
		value			NVARCHAR(255)
	)

	/* PUT CERTIFICATION AUTO-JOIN RULESETS IN TEMP TABLE */
	INSERT INTO #CertificationRules
	(
		idSite,
		idRuleSet,
		idCertification,
		isAny,
		field,
		operator,
		value
	)
	SELECT
		RS.idSite,
		RS.idRuleSet,
		RSCL.idCertification,
		RS.isAny,
		R.userField,
		R.operator,
		CASE WHEN R.textValue IS NOT NULL THEN 
			REPLACE(R.textValue, '''', '''''')
		ELSE
			CASE WHEN R.dateValue IS NOT NULL THEN
				CONVERT(NVARCHAR(255), R.dateValue)
			ELSE
				CASE WHEN R.numValue IS NOT NULL THEN
					CONVERT(NVARCHAR(255), R.numValue)
				ELSE
					CASE WHEN R.bitValue IS NOT NULL THEN
						CONVERT(NVARCHAR(255), R.bitValue)
					ELSE
						NULL
					END
				END
			END
		END
	FROM tblRuleSet RS
	LEFT JOIN tblRule R ON R.idRuleSet = RS.idRuleSet
	LEFT JOIN tblRuleSetToCertificationLink RSCL ON RSCL.idRuleSet = RS.idRuleSet	
	WHERE RSCL.idRuleSetToCertificationLink IS NOT NULL	
	AND (
			(@CertificationsRecordCount = 0)
			OR 
			(@CertificationsRecordCount > 0 AND EXISTS (SELECT 1 FROM @Certifications CP WHERE CP.id = RSCL.idCertification))
		)

	/* DECLARE VARIABLES FOR BUILDING RULE EXECUTION QUERIES */
	DECLARE @certificationsWithRuleSets TABLE (id INT, idSite INT)
	DECLARE @ruleSetsForCertification IDTable
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @idCertification INT
	DECLARE @idSite INT
	DECLARE @idRuleSet INT
	DECLARE @isAny BIT
	DECLARE @field NVARCHAR(25)
	DECLARE @operator NVARCHAR(25)
	DECLARE @value NVARCHAR(255)
	DECLARE @ruleCounter INT

	/* GET IDS OF CERTIFICATIONS WITH RULESETS */
	INSERT INTO @certificationsWithRuleSets ( 
		id, 
		idSite 
	)
	SELECT DISTINCT
		idCertification,
		idSite
	FROM #CertificationRules

	/* CURSOR FOR CERTIFICATIONS WITH RULESETS */
	DECLARE certificationsCursor CURSOR FOR
	SELECT
		id, 
		idSite
	FROM @certificationsWithRuleSets

	OPEN certificationsCursor

	FETCH NEXT FROM certificationsCursor INTO @idCertification, @idSite

	/* LOOP THROUGH CERTIFICATIONS RULESETS */
	WHILE (@@FETCH_STATUS = 0)

		BEGIN

		/* GET IDS OF RULESETS FOR THE CERTIFICATION */
		INSERT INTO @ruleSetsForCertification
		( id )
		SELECT DISTINCT
			idRuleSet
		FROM #CertificationRules
		WHERE idCertification = @idCertification

		/* CURSOR FOR RULESETS FOR THE CERTIFICATION */
		DECLARE certificationRuleSetsCursor CURSOR FOR
		SELECT DISTINCT
			id,
			CR.isAny
		FROM @ruleSetsForCertification
		LEFT JOIN #CertificationRules CR ON CR.idRuleSet = id

		OPEN certificationRuleSetsCursor

		FETCH NEXT FROM certificationRuleSetsCursor INTO @idRuleSet, @isAny
	
		/* LOOP THROUGH RULESETS FOR THE CERTIFICATION */
		WHILE (@@FETCH_STATUS = 0)

			BEGIN

			/* START QUERY SQL STRING FOR RULESET */			
			SET @sql = 'INSERT INTO #UserToCertificationJoins (idRuleSet, idCertification, idUser) SELECT ' + CONVERT(NVARCHAR, @idRuleSet) + ', ' + CONVERT(NVARCHAR, @idCertification) + ', idUser ' + 'FROM tblUser U WHERE ('
			
			/* RESET THE RULE COUNTER */
			SET @ruleCounter = 0

			/* CURSOR FOR RULESET RULES */
			DECLARE ruleSetRulesCursor CURSOR FOR
			SELECT
				field,
				operator,
				value
			FROM #CertificationRules
			WHERE idRuleSet = @idRuleSet

			OPEN ruleSetRulesCursor
		
			FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
	
			/* LOOP THROUGH RULESET RULES */
			WHILE (@@FETCH_STATUS = 0)

				BEGIN

				/* IF THIS IS NOT THE FIRST RULE, EXAMINE isAny LOGIC TO DETERMINE AND/OR OPERATOR */
				IF @ruleCounter > 0
					BEGIN
						IF @isAny = 1
							BEGIN
							SET @sql = @sql + ' OR '
							END
						ELSE
							BEGIN
							SET @sql = @sql + ' AND '
							END
					END

				/* 
				
				BUILD WHERE CLAUSE BASED ON OPERATOR - DIFFERENT LOGIC FOR GROUP 
				GROUP EVALUATES ON THE "name" IN THE BASE TABLE, NOT EVERY LANGUAGE JUST THE DEFAULT

				*/

				IF @field = 'group'
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					END

				ELSE
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' = ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' <> ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) < ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) < ' + '''' + @value + '''' + ')'
						END

					END
			
				/* INCREMENT RULE COUNTER */
				SET @ruleCounter = @ruleCounter + 1

				FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
				END

			/* CLOSE AND DEALLOCATE CURSOR FOR RULESET RULES */
			CLOSE ruleSetRulesCursor
			DEALLOCATE ruleSetRulesCursor

			/* APEND ANDs FOR THE SITE AND USER DELETED STATUS */
			SET @sql = @sql + ') AND (U.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)))'

			/* EXECUTE THE SQL TO INSERT RECORDS IN THE #UserToGroupJoins TABLE */
			EXECUTE sp_executesql @sql
		
			FETCH NEXT FROM certificationRuleSetsCursor INTO @idRuleSet, @isAny
			END

		/* CLOSE AND DEALLOCATE CURSOR FOR RULESETS FOR THE CERTIFICATION */
		DELETE FROM @ruleSetsForCertification
		CLOSE certificationRuleSetsCursor
		DEALLOCATE certificationRuleSetsCursor

		FETCH NEXT FROM certificationsCursor INTO @idCertification, @idSite
		END

	/* CLOSE AND DEALLOCATE CURSOR FOR CERTIFICATIONS WITH RULESETS */
	CLOSE certificationsCursor
	DEALLOCATE certificationsCursor
				  
	/* RETURN RECORDS */
	SELECT DISTINCT
		C.idSite,
		UCJ.idRuleSet,
		UCJ.idCertification,
		UCJ.idUser
	FROM #UserToCertificationJoins UCJ
	LEFT JOIN tblCertification C ON C.idCertification = UCJ.idCertification
	LEFT JOIN tblUserToGroupLink UGL ON UGL.idUser = UCJ.idUser	
	WHERE (  
			  (
				  @filterBy = 'user' AND
				  (
					(@FiltersRecordCount = 0)
					OR 
					(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = UCJ.idUser))
				  )
			  )
			  OR
			  (
				  @filterBy = 'group' AND
				  (
					(@FiltersRecordCount = 0)
					OR 
					(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = UGL.idGroup))
				  )
			  )
		  )
	
	/* DROP TEMPORARY TABLES */
	DROP TABLE #CertificationRules
	DROP TABLE #UserToCertificationJoins

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO