-- =====================================================================
-- PROCEDURE: [DocumentRepositoryFolder.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DocumentRepositoryFolder.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DocumentRepositoryFolder.GetGrid]
GO

/*

Gets a listing of Document Repository Folders that belong to an object.

*/

CREATE PROCEDURE [DocumentRepositoryFolder.GetGrid]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR (10),
	@idCaller							INT				= 0,
	
	@idDocumentRepositoryObjectType		INT,
	@idObject							INT,
	@searchParam						NVARCHAR(4000)  = NULL, --default if not specified,
	@pageNum							INT, 
	@pageSize							INT, 
	@orderColumn						NVARCHAR(255),
	@orderAsc							BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblDocumentRepositoryFolder DRF
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND DRF.idSite = @idCallerSite)
				)
				AND DRF.idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
				AND DRF.idObject = @idObject
				AND (DRF.isDeleted IS NULL OR DRF.isDeleted = 0)
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						DRF.idDocumentRepositoryFolder,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN DRF.[name] END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN DRF.[name] END) END
						)
						AS [row_number]
					FROM tblDocumentRepositoryFolder DRF
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND DRF.idSite = @idCallerSite)
						)
						AND DRF.idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
						AND DRF.idObject = @idObject
						AND (DRF.isDeleted IS NULL OR DRF.isDeleted = 0)
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDocumentRepositoryFolder, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				DRF.idDocumentRepositoryFolder,
				DRF.name,
				CONVERT(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblDocumentRepositoryFolder DRF ON DRF.idDocumentRepositoryFolder = SelectedKeys.idDocumentRepositoryFolder
			ORDER BY SelectedKeys.[row_number]
						
			END
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO