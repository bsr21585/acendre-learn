-- =====================================================================
-- PROCEDURE: [User.OptOutEmailNotifications]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.OptOutEmailNotifications]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.OptOutEmailNotifications]
GO

/*

Out out of Email Notifications Setting

*/
CREATE PROCEDURE [User.OptOutEmailNotifications]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
		
	@idUser					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*

	ensure that the caller exists

	*/
	/*
	IF (SELECT COUNT(1) FROM tblUser WHERE idUser = @idCaller) = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'UserOptOutEmailNotifications_NoUserFound'
		RETURN 1
		END
	*/

	-- block the trigger from executing in tblUser UPDATE
	DECLARE @CONTEXT VARBINARY(128)

	SET @CONTEXT = CONVERT(VARBINARY(128), 'BLOCK TRIGGER')
	SET CONTEXT_INFO @CONTEXT

	-- change the user's EmailNotifications flag
	UPDATE tblUser SET
		[optOutOfEmailNotifications] = 1
	WHERE idUser = @idUser

	-- unblock the trigger
	SET @CONTEXT = CONVERT(VARBINARY(128), '')
	SET CONTEXT_INFO @CONTEXT
		
	-- return successfully
	SET @Return_Code = 0 --(0 is 'success')
	SET @Error_Description_Code = ''
				
	END			

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
