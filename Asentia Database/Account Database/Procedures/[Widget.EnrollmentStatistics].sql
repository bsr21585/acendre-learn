-- =====================================================================
-- PROCEDURE: [Widget.EnrollmentStatistics]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Widget.EnrollmentStatistics]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.EnrollmentStatistics]
GO

/*

Gets gets data for the Enrollment Statistics Widget.

*/

CREATE PROCEDURE [Widget.EnrollmentStatistics]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
AS
	BEGIN
		SET NOCOUNT ON

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the groups that the caller has user manager permission over
		if caller is 1 or has null scope for user manager, they see global statistics

		*/

		DECLARE @idPermission INT
		DECLARE @hasGlobalPermission BIT
		DECLARE @groupScope NVARCHAR(MAX)

		SET @idPermission = 105 -- 105 is user manager, 107 is group manager if we wanted to use that permission	
		SET @hasGlobalPermission = 1
		SET @groupScope = NULL	

		IF (@idCaller > 1)
			BEGIN 

			CREATE TABLE #CallerRoles (idRole INT)
			INSERT INTO #CallerRoles (idRole) SELECT DISTINCT URL.idRole FROM tblUserToRoleLink URL WHERE URL.idUser = @idCaller -- directly assigned roles
			INSERT INTO #CallerRoles (idRole) SELECT DISTINCT GRL.idRole FROM tblGroupToRoleLink GRL -- group inherited roles
											  WHERE GRL.idGroup IN (SELECT DISTINCT UGL.idGroup FROM tblUserToGroupLink UGL WHERE UGL.idUser = @idCaller)
											  AND NOT EXISTS (SELECT 1 FROM #CallerRoles CR WHERE CR.idRole = GRL.idRole)
		
			-- if the caller does not have permission with NULL scope, get the defined scopes
			IF (
				SELECT COUNT(1) FROM tblRoleToPermissionLink RPL 
				WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles)
				AND RPL.idPermission = @idPermission
				AND RPL.scope IS NULL
			   ) = 0
				BEGIN

				SET @hasGlobalPermission = 0

				-- get all scope items into the group scope variable			
				DECLARE @scopeItems NVARCHAR(MAX)
				SET @groupScope = ''

				DECLARE scopeListCursor CURSOR LOCAL FOR
					SELECT scope FROM tblRoleToPermissionLink RPL WHERE RPL.idRole IN (SELECT idRole FROM #CallerRoles) AND RPL.idPermission = @idPermission AND RPL.scope IS NOT NULL

				OPEN scopeListCursor

				FETCH NEXT FROM scopeListCursor INTO @scopeItems

				WHILE @@FETCH_STATUS = 0
					BEGIN

					IF (@scopeItems <> '')
						BEGIN
						IF (@groupScope <> '')
							BEGIN
							SET @groupScope = @groupScope + ',' + @scopeItems
							END
						ELSE
							BEGIN
							SET @groupScope = @scopeItems
							END
						
						END

					FETCH NEXT FROM scopeListCursor INTO @scopeItems

					END

				-- kill the cursor
				CLOSE scopeListCursor
				DEALLOCATE scopeListCursor

				END

			END

		/*

		put all course enrollments into a temporary table, we'll work from that table

		*/

		CREATE TABLE #Enrollments (
			idEnrollment			INT,
			idCourse				INT,
			dtStart					DATETIME,
			dtDue					DATETIME,
			dtExpires				DATETIME,
			dtCompleted				DATETIME
		)

		INSERT INTO #Enrollments (
			idEnrollment,
			idCourse,
			dtStart,
			dtDue,
			dtExpires,
			dtCompleted
		)
		SELECT
			E.idEnrollment,
			E.idCourse,
			E.dtStart,
			E.dtDue,
			CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
				NULL
			ELSE
				CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NOT NULL THEN
					E.dtExpiresFromFirstLaunch
				ELSE
					CASE WHEN E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
						E.dtExpiresFromStart
					ELSE
						CASE WHEN E.dtExpiresFromStart >= E.dtExpiresFromFirstLaunch THEN
							E.dtExpiresFromFirstLaunch
						ELSE
							E.dtExpiresFromStart
						END
					END
				END
			END AS [dtExpires],
			E.dtCompleted
		FROM tblEnrollment E
		LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
		LEFT JOIN tblUser U ON U.idUser = E.idUser
		WHERE E.idSite = @idCallerSite
		AND E.idCourse > 0
		AND (C.isDeleted = 0 OR C.isDeleted IS NULL)
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- enrollments for deleted and unapproved users should not be reflected
		AND U.isActive = 1 -- only active users should be included
		AND E.dtStart <= GETUTCDATE()
		AND E.idActivityImport IS NULL
		AND (
				@hasGlobalPermission = 1
				OR
				(
					@hasGlobalPermission = 0
					AND
					U.idUser IN (SELECT DISTINCT UGL.idUser FROM tblUserToGroupLink UGL WHERE UGL.idGroup IN (SELECT s FROM dbo.DelimitedStringToTable(@groupScope, ',')))
				)
			)

		/*

		put all learning path enrollments into a temporary table, we'll work from that table

		*/

		CREATE TABLE #LearningPathEnrollments (
			idLearningPathEnrollment	INT,
			idLearningPath				INT,
			dtStart						DATETIME,
			dtDue						DATETIME,
			dtExpires					DATETIME,
			dtCompleted					DATETIME
		)

		INSERT INTO #LearningPathEnrollments (
			idLearningPathEnrollment,
			idLearningPath,
			dtStart,
			dtDue,
			dtExpires,
			dtCompleted
		)
		SELECT
			LPE.idLearningPathEnrollment,
			LPE.idLearningPath,
			LPE.dtStart,
			LPE.dtDue,
			CASE WHEN LPE.dtExpiresFromStart IS NULL AND LPE.dtExpiresFromFirstLaunch IS NULL THEN
				NULL
			ELSE
				CASE WHEN LPE.dtExpiresFromStart IS NULL AND LPE.dtExpiresFromFirstLaunch IS NOT NULL THEN
					LPE.dtExpiresFromFirstLaunch
				ELSE
					CASE WHEN LPE.dtExpiresFromStart IS NOT NULL AND LPE.dtExpiresFromFirstLaunch IS NULL THEN
						LPE.dtExpiresFromStart
					ELSE
						CASE WHEN LPE.dtExpiresFromStart >= LPE.dtExpiresFromFirstLaunch THEN
							LPE.dtExpiresFromFirstLaunch
						ELSE
							LPE.dtExpiresFromStart
						END
					END
				END
			END AS [dtExpires],
			LPE.dtCompleted
		FROM tblLearningPathEnrollment LPE
		LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
		LEFT JOIN tblUser U ON U.idUser = LPE.idUser
		WHERE LPE.idSite = @idCallerSite
		AND LPE.idLearningPath > 0
		AND (LP.isDeleted = 0 OR LP.isDeleted IS NULL)
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- enrollments for deleted and unapproved users should not be reflected
		AND U.isActive = 1 -- only active users should be included
		AND LPE.dtStart <= GETUTCDATE()


		/*

		COURSE ENROLLMENT STATISTICS BY STATUS

		*/

		SELECT
			(SELECT COUNT(1) FROM #Enrollments E WHERE (E.dtDue > GETUTCDATE() OR E.dtDue IS NULL) AND (E.dtExpires > GETUTCDATE() OR E.dtExpires IS NULL) AND E.dtCompleted IS NULL) AS [##Enrolled##],
			(SELECT COUNT(1) FROM #Enrollments E WHERE E.dtCompleted IS NOT NULL) AS [##Completed##],
			(SELECT COUNT(1) FROM #Enrollments E WHERE E.dtDue <= GETUTCDATE() AND (E.dtExpires > GETUTCDATE() OR E.dtExpires IS NULL) AND E.dtCompleted IS NULL) AS [##Overdue##],
			(SELECT COUNT(1) FROM #Enrollments E WHERE E.dtExpires IS NOT NULL AND E.dtExpires <= GETUTCDATE() AND E.dtCompleted IS NULL) AS [##Expired##],
			(SELECT COUNT(1) FROM #Enrollments E) AS _Total_

		/*

		TOP 5 COURSES BY NUMBER OF ENROLLMENTS

		*/

		CREATE TABLE #Top5CoursesByEnrollments (
			position				INT		IDENTITY(1,1),
			idCourse				INT,
			total					INT
		)

		INSERT INTO #Top5CoursesByEnrollments (
			idCourse,
			total
		)
		SELECT TOP 5
			E.idCourse,
			COUNT(1) AS total
		FROM #Enrollments E
		GROUP BY E.idCourse
		ORDER BY total DESC

		SELECT
			T5.idCourse,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS _Title_,
			T5.total AS _Total_
		FROM #Top5CoursesByEnrollments T5
		LEFT JOIN tblCourse C ON C.idCourse = T5.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		ORDER BY T5.position


		/*

		TOP 5 COURSES BY NUMBER OF COMPLETIONS

		*/

		CREATE TABLE #Top5CoursesByCompletions (
			position				INT		IDENTITY(1,1),
			idCourse				INT,
			total					INT
		)

		INSERT INTO #Top5CoursesByCompletions (
			idCourse,
			total
		)
		SELECT TOP 5
			E.idCourse,
			COUNT(1) AS total
		FROM #Enrollments E
		WHERE E.dtCompleted IS NOT NULL
		GROUP BY E.idCourse
		ORDER BY total DESC

		SELECT
			T5.idCourse,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS _Title_,
			T5.total AS _Total_
		FROM #Top5CoursesByCompletions T5
		LEFT JOIN tblCourse C ON C.idCourse = T5.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		ORDER BY T5.position

		/*

		LEARNING PATH ENROLLMENT STATISTICS BY STATUS

		*/

		SELECT
			(SELECT COUNT(1) FROM #LearningPathEnrollments LPE WHERE (LPE.dtDue > GETUTCDATE() OR LPE.dtDue IS NULL) AND (LPE.dtExpires > GETUTCDATE() OR LPE.dtExpires IS NULL) AND LPE.dtCompleted IS NULL) AS [##Enrolled##],
			(SELECT COUNT(1) FROM #LearningPathEnrollments LPE WHERE LPE.dtCompleted IS NOT NULL) AS [##Completed##],
			(SELECT COUNT(1) FROM #LearningPathEnrollments LPE WHERE LPE.dtDue <= GETUTCDATE() AND (LPE.dtExpires > GETUTCDATE() OR LPE.dtExpires IS NULL) AND LPE.dtCompleted IS NULL) AS [##Overdue##],
			(SELECT COUNT(1) FROM #LearningPathEnrollments LPE WHERE LPE.dtExpires IS NOT NULL AND LPE.dtExpires <= GETUTCDATE() AND LPE.dtCompleted IS NULL) AS [##Expired##],
			(SELECT COUNT(1) FROM #LearningPathEnrollments LPE) AS _Total_
		
		/*

		TOP 5 LEARNING PATHS BY NUMBER OF ENROLLMENTS

		*/

		CREATE TABLE #Top5LearningPathsByEnrollments (
			position				INT		IDENTITY(1,1),
			idLearningPath			INT,
			total					INT
		)

		INSERT INTO #Top5LearningPathsByEnrollments (
			idLearningPath,
			total
		)
		SELECT TOP 5
			LPE.idLearningPath,
			COUNT(1) AS total
		FROM #LearningPathEnrollments LPE
		GROUP BY LPE.idLearningPath
		ORDER BY total DESC

		SELECT
			T5.idLearningPath,
			CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE LP.name END AS _Title_,
			T5.total AS _Total_
		FROM #Top5LearningPathsByEnrollments T5
		LEFT JOIN tblLearningPath LP ON LP.idLearningPath = T5.idLearningPath
		LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = @idCallerLanguage
		ORDER BY T5.position


		/*

		TOP 5 LEARNING PATHS BY NUMBER OF COMPLETIONS

		*/

		CREATE TABLE #Top5LearningPathsByCompletions (
			position				INT		IDENTITY(1,1),
			idLearningPath			INT,
			total					INT
		)

		INSERT INTO #Top5LearningPathsByCompletions (
			idLearningPath,
			total
		)
		SELECT TOP 5
			LPE.idLearningPath,
			COUNT(1) AS total
		FROM #LearningPathEnrollments LPE
		WHERE LPE.dtCompleted IS NOT NULL
		GROUP BY LPE.idLearningPath
		ORDER BY total DESC

		SELECT
			T5.idLearningPath,
			CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE LP.name END AS _Title_,
			T5.total AS _Total_
		FROM #Top5LearningPathsByCompletions T5
		LEFT JOIN tblLearningPath LP ON LP.idLearningPath = T5.idLearningPath
		LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = @idCallerLanguage
		ORDER BY T5.position

		-- DROP THE TEMPORARY TABLES
		DROP TABLE #Enrollments
		DROP TABLE #Top5CoursesByEnrollments
		DROP TABLE #Top5CoursesByCompletions
		DROP TABLE #LearningPathEnrollments
		DROP TABLE #Top5LearningPathsByEnrollments
		DROP TABLE #Top5LearningPathsByCompletions

		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO