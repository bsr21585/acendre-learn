-- =====================================================================
-- PROCEDURE: [Catalog.GetInstructorLedTrainingModules]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.GetInstructorLedTrainingModules]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.GetInstructorLedTrainingModules]
GO
	
/*

Returns a recordset of standalone enrollable instructor-led training modules.

*/

CREATE PROCEDURE [Catalog.GetInstructorLedTrainingModules]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	-- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	/*

	do not check caller site membership or caller permissions as
	this can be called by a non-logged-in user

	*/


	/*

	validate that the specified language exists, use the site default if not

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	IF @idCallerLanguage IS NULL
		BEGIN
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		END

			
	/*
	
	get the data and return it
	
	*/

	SELECT
		SUT.idStandUpTraining,
		SUT.idSite,
		CASE WHEN SUTL.title IS NOT NULL THEN SUTL.title ELSE SUT.title END AS title,
		SUT.avatar,
		CASE WHEN SUT.cost IS NOT NULL THEN CAST(SUT.cost AS DECIMAL(18,2)) ELSE NULL END AS cost,
		CASE WHEN (
					SELECT COUNT(1) 
					FROM tblStandupTrainingInstanceToUserLink STIUL
					WHERE STIUL.idStandupTrainingInstance IN (SELECT DISTINCT STI.idStandupTrainingInstance
															  FROM tblStandUpTrainingInstance STI
															  WHERE STI.idStandUpTraining = SUT.idStandUpTraining)
					AND STIUL.idUser = @idCaller
				  ) = 0 THEN CONVERT(BIT, 0) 
		ELSE CONVERT(BIT, 1) END AS isOrWasCallerEnrolledInASession
	FROM tblStandUpTraining SUT
	LEFT JOIN tblStandUpTrainingLanguage SUTL ON SUTL.idStandUpTraining = SUT.idStandUpTraining AND SUTL.idLanguage = @idCallerLanguage
	WHERE SUT.idSite = @idCallerSite 
	AND (SUT.isDeleted IS NULL OR SUT.isDeleted = 0)
	AND SUT.isStandaloneEnroll = 1
	ORDER BY title
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO	