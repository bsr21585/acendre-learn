-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.GetMeetingTimes]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[StandupTrainingInstance.GetMeetingTimes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.GetMeetingTimes]
GO

/*

Return all meeting times for a given standup training instance id.

*/

CREATE PROCEDURE [StandupTrainingInstance.GetMeetingTimes]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblStandupTrainingInstance STI
		WHERE idStandUpTrainingInstance = @idStandupTrainingInstance
		AND STI.idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'STIGetMeetingTimes_DetailsNotFound'
		RETURN 1
		END
	
	SELECT 
		STIMT.dtStart,
		STIMT.dtEnd,
		STIMT.[minutes],
		STIMT.idTimezone,
		CONVERT(BIT,(CASE WHEN STIMT.dtStart < GETUTCDATE() THEN 1 ELSE 0 END)) AS isExistingDate
	FROM tblStandupTrainingInstanceMeetingTime STIMT
	WHERE STIMT.idStandUpTrainingInstance = @idStandupTrainingInstance
	AND STIMT.idSite = @idCallerSite
	ORDER BY STIMT.dtStart ASC 
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO