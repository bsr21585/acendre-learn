-- =====================================================================
-- PROCEDURE: [LearningPathEnrollment.EvaluateCompletion]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPathEnrollment.EvaluateCompletion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPathEnrollment.EvaluateCompletion]
GO

/*

Evaluates completion status of learning path enrollments based on completion of its courses,
and marks the enrollments accordingly.

Note, this should not be called from procedural code, only from other procedures that
affect course enrollment completion.

Since this procedure is to only be called when affecting a course enrollment's completion,
we use @idCourse to get all learning paths the course belongs to, and we evaluate those.

*/

CREATE PROCEDURE [LearningPathEnrollment.EvaluateCompletion]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idUser					INT,
	@idCourse				INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/* NO PERMISSIONS CHECKS - WE CANNOT RESTRICT THIS BASED ON PERMISSIONS SINCE IT CAN BE CALLED AS A RESULT OF LEARNER ACTIONS */

	/*
	
	validate that the course exists within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourse C
		WHERE C.idCourse = @idCourse
		AND (C.idSite IS NULL OR C.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LearningPathEnrollmentEC_NoCourseRecordFound'
		RETURN 1 
		END

	/*

	get all learning paths the course belongs to

	*/

	CREATE TABLE #LearningPaths (
		idLearningPath	INT
	)

	INSERT INTO #LearningPaths (
		idLearningPath
	)
	SELECT DISTINCT
		idLearningPath
	FROM tblLearningPathToCourseLink
	WHERE idCourse = @idCourse

	/*

	get all of the learning path enrollments for the user where
	they are in out list of learning paths above

	*/

	CREATE TABLE #LearningPathEnrollments (
		idLearningPathEnrollment	INT,
		idLearningPath				INT,
		idUser						INT,
		dtStart						DATETIME,
		dtCompleted					DATETIME,
		idTimezone					INT
	)

	INSERT INTO #LearningPathEnrollments (
		idLearningPathEnrollment,
		idLearningPath,
		idUser,
		dtStart,
		dtCompleted,
		idTimezone
	)
	SELECT 
		LPE.idLearningPathEnrollment,
		LPE.idLearningPath,
		LPE.idUser,
		LPE.dtStart,
		LPE.dtCompleted,
		LPE.idTimezone
	FROM tblLearningPathEnrollment LPE
	WHERE LPE.idUser = @idUser
	AND LPE.idLearningPath IN (SELECT idLearningPath FROM #LearningPaths)


	/*
	
	declare variables we're going to be working with
	
	*/
			
	DECLARE @isEnrollmentCompleted					BIT
	DECLARE @enrollmentMeetsCompletionCriteria		BIT
	DECLARE @idLearningPathEnrollment				INT
	DECLARE @idLearningPath							INT
	DECLARE @learningPathEnrollmentStartDate		DATETIME
	DECLARE @learningPathEnrollmentCompletionDate	DATETIME
	DECLARE @idLearningPathEnrollmentTimezone		INT

	SET @isEnrollmentCompleted = 0
	SET @enrollmentMeetsCompletionCriteria = 0

	/*

	cursor through all of the user's learning path enrollments and evaluate completion for each

	*/

	DECLARE learningPathEnrollmentCursor CURSOR LOCAL FOR
		SELECT idLearningPathEnrollment, idLearningPath, dtStart, dtCompleted, idTimezone FROM #LearningPathEnrollments

	OPEN learningPathEnrollmentCursor

	FETCH NEXT FROM learningPathEnrollmentCursor INTO 
		@idLearningPathEnrollment, @idLearningPath, @learningPathEnrollmentStartDate, @learningPathEnrollmentCompletionDate, @idLearningPathEnrollmentTimezone

	WHILE @@FETCH_STATUS = 0
		BEGIN

		/*

		reinitialize completion and completion criteria variables

		*/

		SET @isEnrollmentCompleted = 0
		SET @enrollmentMeetsCompletionCriteria = 0

		/* 

		get the learning path enrollment's current completion status 

		*/

		IF @learningPathEnrollmentCompletionDate IS NOT NULL
			BEGIN
			SET @isEnrollmentCompleted = 1
			END

		/* 

		evaluate the completion status of the learning path enrollment by checking to ensure all courses belonging 
		to the learning path have a completion date that is at least a year prior to the learning path's start date

		*/

		CREATE TABLE #CourseEnrollments (
			[idEnrollment]	INT, -- doubtful we will need this, but lets get it just in case
			[idCourse]		INT,
			[dtCompleted]	DATETIME
		)

		INSERT INTO #CourseEnrollments (
			idEnrollment,
			idCourse,
			dtCompleted
		)
		SELECT
			idEnrollment,
			idCourse,
			dtCompleted
		FROM tblEnrollment
		WHERE idCourse IN (SELECT idCourse FROM tblLearningPathToCourseLink WHERE idLearningPath = @idLearningPath)
		AND idUser = @idUser

		IF (
			SELECT COUNT(DISTINCT idCourse) 
			FROM #CourseEnrollments 
			WHERE dtCompleted IS NOT NULL
			AND dtCompleted >= DATEADD(yyyy, -1, @learningPathEnrollmentStartDate)
			) 
			= 
			(
			 SELECT COUNT(DISTINCT idCourse) 
			 FROM tblLearningPathToCourseLink 
			 WHERE idLearningPath = @idLearningPath
			)
			BEGIN
			SET @enrollmentMeetsCompletionCriteria = 1
			END

		/*

		if the enrollment meets completion criteria and is not already completed, complete it,
		then award any certificates that need to be awarded

		*/

		IF @enrollmentMeetsCompletionCriteria = 1 AND @isEnrollmentCompleted = 0
			BEGIN

			DECLARE @utcNow DATETIME
			SET @utcNow = GETUTCDATE()

			-- update the learning path enrollment record
			UPDATE tblLearningPathEnrollment SET dtCompleted = GETUTCDATE() WHERE idLearningPathEnrollment = @idLearningPathEnrollment
			
			-- write event log entry for completion
			DECLARE @eventLogItem EventLogItemObjects
			INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, idLearningPath, idLearningPathEnrollment, idUser FROM tblLearningPathEnrollment WHERE idLearningPathEnrollment = @idLearningPathEnrollment
			EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 505, @utcNow, @eventLogItem

			-- evaluate certificates
			EXEC [Certificate.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idUser, @idLearningPath, 'learningpath', 'completed', @idLearningPathEnrollmentTimezone
			
			END

		/*

		if the enrollment is already completed but does not meet completion criteria, 
		remove completion, this would happen in the case of a lesson data or course reset, 
		then revoke any certificates that need to be revoked

		*/

		IF @isEnrollmentCompleted = 1 AND @enrollmentMeetsCompletionCriteria = 0
			BEGIN
			UPDATE tblLearningPathEnrollment SET dtCompleted = NULL WHERE idLearningPathEnrollment = @idLearningPathEnrollment
			EXEC [Certificate.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idUser, @idLearningPath, 'learningpath', 'revoked', @idLearningPathEnrollmentTimezone
			END

		-- drop the course enrollments temporary table 
		-- so it can be recreated in the next loop
		DROP TABLE #CourseEnrollments

		FETCH NEXT FROM learningPathEnrollmentCursor INTO 
			@idLearningPathEnrollment, @idLearningPath, @learningPathEnrollmentStartDate, @learningPathEnrollmentCompletionDate, @idLearningPathEnrollmentTimezone

		END

	-- kill the cursor
	CLOSE learningPathEnrollmentCursor
	DEALLOCATE learningPathEnrollmentCursor

	-- drop the other temporary tables
	DROP TABLE #LearningPaths
	DROP TABLE #LearningPathEnrollments

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO