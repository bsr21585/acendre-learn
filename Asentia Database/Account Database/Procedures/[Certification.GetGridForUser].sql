-- =====================================================================
-- PROCEDURE: [Certification.GetGridForUser]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Certification.GetGridForUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.GetGridForUser]
GO

/*

Gets a listing of certifications for a given user. 

*/

CREATE PROCEDURE [Certification.GetGridForUser]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,

	@idUser					INT
)
AS
	BEGIN
		SET NOCOUNT ON	

		CREATE TABLE #Certifications (
			idCertificationToUserLink				INT,
			idCertification							INT,
			idUser									INT,			
			title									NVARCHAR(255),
			[status]								NVARCHAR(20),
			initialAwardDate						DATETIME,
			lastExpirationDate						DATETIME,
			currentExpirationDate					DATETIME
		)

		/*

		get the language id from the caller language string
	
		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		insert into the temp table

		*/

		INSERT INTO #Certifications (
			idCertificationToUserLink,
			idCertification,
			idUser,			
			title,
			[status],
			initialAwardDate,
			lastExpirationDate,
			currentExpirationDate			
		)
		SELECT
			CUL.idCertificationToUserLink,
			CUL.idCertification,
			CUL.idUser,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title,			
			CASE WHEN CUL.initialAwardDate IS NOT NULL AND CUL.currentExpirationDate IS NOT NULL THEN
				CASE WHEN GETUTCDATE() < CUL.currentExpirationDate THEN
					'1' -- current
				ELSE
					'2' -- expired
				END
			ELSE
				'0' -- incomplete
			END AS [status],
			CUL.initialAwardDate,
			CUL.lastExpirationDate,
			CUL.currentExpirationDate
		FROM tblCertificationToUserLink CUL
		LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
		LEFT JOIN tblCertificationLanguage CL ON CL.idCertification = C.idCertification AND CL.idLanguage = @idCallerLanguage
		WHERE CUL.idUser = @idUser		
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END
		
		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #Certifications C
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						C.idCertificationToUserLink,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'initialAwardDate' THEN initialAwardDate END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'lastExpirationDate' THEN lastExpirationDate END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'currentExpirationDate' THEN currentExpirationDate END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'status' THEN [status] END) END DESC,							
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN [status] END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN currentExpirationDate END) END DESC,							
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'initialAwardDate' THEN initialAwardDate END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'lastExpirationDate' THEN lastExpirationDate END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'currentExpirationDate' THEN currentExpirationDate END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'status' THEN [status] END) END,							
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN title END) END,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN [status] END) END,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN currentExpirationDate END) END
						)
						AS [row_number]
					FROM #Certifications C
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idCertificationToUserLink, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				C.idCertificationToUserLink,
				C.idCertification,
				C.idUser,			
				C.title,
				C.[status],
				C.initialAwardDate,
				C.lastExpirationDate,
				C.currentExpirationDate,
				CONVERT(BIT, 1) AS [isResetOn],
				CONVERT(BIT, 1) AS [isActivityOn],
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #Certifications C ON C.idCertificationToUserLink = SelectedKeys.idCertificationToUserLink
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #Certifications C
			INNER JOIN CONTAINSTABLE(tblCertification, *, @searchParam) K ON K.[key] = C.idCertification
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						C.idCertificationToUserLink,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'initialAwardDate' THEN initialAwardDate END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'lastExpirationDate' THEN lastExpirationDate END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'currentExpirationDate' THEN currentExpirationDate END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'status' THEN [status] END) END DESC,							
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN [status] END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN currentExpirationDate END) END DESC,							
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'initialAwardDate' THEN initialAwardDate END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'lastExpirationDate' THEN lastExpirationDate END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'currentExpirationDate' THEN currentExpirationDate END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'status' THEN [status] END) END,							
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN title END) END,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN [status] END) END,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('initialAwardDate', 'lastExpirationDate', 'currentExpirationDate', 'status') THEN currentExpirationDate END) END
						)
						AS [row_number]
					FROM #Certifications C
					INNER JOIN CONTAINSTABLE(tblCertification, *, @searchParam) K ON K.[key] = C.idCertification
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idCertificationToUserLink, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				C.idCertificationToUserLink,
				C.idCertification,
				C.idUser,			
				C.title,
				C.[status],
				C.initialAwardDate,
				C.lastExpirationDate,
				C.currentExpirationDate,
				CONVERT(BIT, 1) AS [isResetOn],
				CONVERT(BIT, 1) AS [isActivityOn],				
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #Certifications C ON C.idCertificationToUserLink = SelectedKeys.idCertificationToUserLink			
			ORDER BY SelectedKeys.[row_number]
			
			END

		-- DROP THE TEMPORARY TABLE
		DROP TABLE #Certifications
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO