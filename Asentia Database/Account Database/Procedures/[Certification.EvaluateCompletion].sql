-- =====================================================================
-- PROCEDURE: [Certification.EvaluateCompletion]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.EvaluateCompletion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.EvaluateCompletion]
GO

/*

Evaluates completion status of a single certification, or all certifications for a user.

Note, this should not be called from procedural code, only from other procedures that
certification data.

*/

CREATE PROCEDURE [Certification.EvaluateCompletion]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idCertificationToUserLink	INT,
	@idUser						INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*

	declare utcNow

	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/* NO PERMISSIONS CHECKS - WE CANNOT RESTRICT THIS BASED ON PERMISSIONS SINCE IT CAN BE CALLED AS A RESULT OF LEARNER ACTIONS */

	/*
	
	if a single certification was supplied, validate that the record exists within the site
	
	*/
	
	IF (@idCertificationToUserLink IS NOT NULL)
		BEGIN

		IF (
			SELECT COUNT(1)
			FROM tblCertificationToUserLink CUL
			WHERE CUL.idCertificationToUserLink = @idCertificationToUserLink  
			AND (CUL.idSite IS NULL OR CUL.idSite <> @idCallerSite)
			) > 0
		
			BEGIN 
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertificationEvaluateCompletion_NoRecordFound'
			RETURN 1 
			END

		END

	/*

	get the user's timezone

	*/

	DECLARE @idUserTimezone INT

	SELECT 
		@idUserTimezone = CASE WHEN U.idTimezone IS NOT NULL THEN U.idTimezone ELSE S.idTimezone END 
	FROM tblUser U 
	LEFT JOIN tblSite S ON S.idSite = U.idSite 
	WHERE U.idUser = @idUser

	/*

	put certification user links in a table to cursor through, even if it's just 1

	*/

	DECLARE @certificationToUserLinks TABLE (idCertificationToUserLink INT)

	INSERT INTO @certificationToUserLinks (
		idCertificationToUserLink
	)
	SELECT
		CUL.idCertificationToUserLink
	FROM tblCertificationToUserLink CUL
	WHERE CUL.idUser = @idUser
	AND (
			(CUL.certificationTrack = 0 AND CUL.initialAwardDate IS NULL)
			OR
			(
				CUL.certificationTrack = 1
				AND
				CUL.lastExpirationDate IS NOT NULL
				AND
				@utcNow >= lastExpirationDate
				AND
				CUL.currentExpirationDate IS NOT NULL
			)
		)
	AND (
			(@idCertificationToUserLink IS NULL)
			OR
			(@idCertificationToUserLink IS NOT NULL AND CUL.idCertificationToUserLink = @idCertificationToUserLink)
		)
				 
	/*
	
	declare variables we're going to be working with
	
	*/

	DECLARE @idCertification INT
	DECLARE @certificationTrack INT
	DECLARE @isInitialRequirement BIT
	DECLARE @eventLogItem EventLogItemObjects
	DECLARE userCertificationsCursor CURSOR FOR
		SELECT DISTINCT
			CULCUL.idCertificationToUserLink,
			CUL.idCertification,
			CUL.certificationTrack
		FROM @certificationToUserLinks CULCUL
		LEFT JOIN tblCertificationToUserLink CUL ON CUL.idCertificationToUserLink = CULCUL.idCertificationToUserLink

	/*

	CREATE TEMP TABLES TO TRACK IF REQUIREMENTS ARE MET

	*/

	CREATE TABLE #RequirementsStatus (
		idCertificationModuleRequirement	INT,
		idCertificationModuleRequirementSet INT,
		isRequirementMet					BIT
	)

	CREATE TABLE #RequirementSetsStatus (
		idCertificationModuleRequirementSet INT,
		idCertificationModule				INT,
		isRequirementMet					BIT
	)

	CREATE TABLE #ModulesStatus (
		idCertificationModule	INT,
		idCertification			INT,
		isRequirementMet		BIT
	)

	CREATE TABLE #CertificationStatus (
		idCertification		INT,
		isRequirementMet	BIT
	)

	/*

	BEGIN CURSORING THROUGH THE CERTIFICATION AND EVALUATING FOR COMPLETION

	*/

	OPEN userCertificationsCursor

	FETCH NEXT FROM userCertificationsCursor INTO @idCertificationToUserLink, @idCertification, @certificationTrack

	WHILE (@@FETCH_STATUS = 0)

		BEGIN

		-- clear the temp tables
		DELETE FROM #CertificationStatus
		DELETE FROM #ModulesStatus
		DELETE FROM #RequirementSetsStatus
		DELETE FROM #RequirementsStatus
		DELETE FROM @eventLogItem
		
		-- set whether or not we're looking at initial requirements
		IF (@certificationTrack = 0)
			BEGIN
			SET @isInitialRequirement = 1
			END
		ELSE
			BEGIN
			SET @isInitialRequirement = 0
			END
		
		/*

		EVALUATE THE REQUIRMENTS

		*/

		IF (@isInitialRequirement = 1) -- INITIAL REQUIREMENTS
			BEGIN

			/* REQUIREMENTS STATUS */

			INSERT INTO #RequirementsStatus (
				idCertificationModuleRequirement,
				idCertificationModuleRequirementSet,
				isRequirementMet
			)
			SELECT DISTINCT
				CMR.idCertificationModuleRequirement,
				CMR.idCertificationModuleRequirementSet,
				CASE WHEN CMR.requirementType = 0 THEN
						CASE WHEN CMR.courseCompletionIsAny = 1 THEN
							CASE WHEN (SELECT COUNT(1)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									  ) >= 1 THEN 1 ELSE 0 END
						ELSE
							CASE WHEN (SELECT COUNT(1)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									  ) >= (SELECT COUNT(1) 
											FROM tblCertificationModuleRequirementToCourseLink
											WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement
											) THEN 1 ELSE 0 END
						END
					 WHEN CMR.requirementType = 1 THEN
						CASE WHEN CMR.courseCreditEligibleCoursesIsAll = 1 THEN				
							CASE WHEN (SELECT SUM(CCCL.credits)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationToCourseCreditLink WHERE idCertification = CUL.idCertification)) E_MAIN
									   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse
									  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END
						ELSE
							CASE WHEN (SELECT SUM(CCCL.credits)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse
									  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END
						END
					 WHEN CMR.requirementType = 2 THEN
						CASE WHEN DCMR.dtCompleted IS NOT NULL THEN 1 ELSE 0 END
				END AS [isRequirementMet]
			FROM tblCertificationToUserLink CUL
			LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
			LEFT JOIN tblCertificationModule CM ON CM.idCertification = C.idCertification
			LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModule = CM.idCertificationModule
			LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet
			LEFT JOIN [tblData-CertificationModuleRequirement] DCMR ON DCMR.idCertificationToUserLink = CUL.idCertificationToUserLink AND DCMR.idCertificationModuleRequirement = CMR.idCertificationModuleRequirement
			WHERE CUL.idUser = @idUser
			AND CUL.idCertification = @idCertification
			AND CM.isInitialRequirement = @isInitialRequirement

			/* REQUIREMENT SETS STATUS */

			INSERT INTO #RequirementSetsStatus (
				idCertificationModuleRequirementSet,
				idCertificationModule,
				isRequirementMet
			)
			SELECT DISTINCT
				RS.idCertificationModuleRequirementSet,
				CMRS.idCertificationModule,
				CASE WHEN CMRS.isAny = 1 THEN
					CASE WHEN (SELECT COUNT(1) FROM #RequirementsStatus WHERE isRequirementMet = 1 AND idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) >= 1 THEN 1 ELSE 0 END
				ELSE
					CASE WHEN (SELECT COUNT(1) FROM #RequirementsStatus WHERE isRequirementMet = 1 AND idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) = (SELECT COUNT(1) FROM #RequirementsStatus WHERE idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) THEN 1 ELSE 0 END
				END
			FROM #RequirementsStatus RS
			LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModuleRequirementSet = RS.idCertificationModuleRequirementSet

			/* MODULES STATUS */

			INSERT INTO #ModulesStatus (
				idCertificationModule,
				idCertification,
				isRequirementMet
			)
			SELECT DISTINCT
				RSS.idCertificationModule,
				CM.idCertification,
				CASE WHEN CM.isAnyRequirementSet = 1 THEN
					CASE WHEN (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE isRequirementMet = 1 AND idCertificationModule = CM.idCertificationModule) >= 1 THEN 1 ELSE 0 END
				ELSE
					CASE WHEN (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE isRequirementMet = 1 AND idCertificationModule = CM.idCertificationModule) = (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE idCertificationModule = CM.idCertificationModule) THEN 1 ELSE 0 END
				END
			FROM #RequirementSetsStatus RSS
			LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = RSS.idCertificationModule

			/* CERTIFICATION STATUS */

			INSERT INTO #CertificationStatus (
				idCertification,
				isRequirementMet
			)
			SELECT DISTINCT
				CUL.idCertification,
				CASE WHEN CUL.initialAwardDate IS NOT NULL THEN
					1
				ELSE
					CASE WHEN C.isAnyModule = 1 THEN
						CASE WHEN (SELECT COUNT(1) FROM #ModulesStatus WHERE isRequirementMet = 1) >= 1 THEN 1 ELSE 0 END
					ELSE
						CASE WHEN (SELECT COUNT(1) FROM #ModulesStatus WHERE isRequirementMet = 1) = (SELECT COUNT(1) FROM #ModulesStatus) THEN 1 ELSE 0 END
					END
				END
			FROM tblCertificationToUserLink CUL
			LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
			WHERE CUL.idCertification = @idCertification
			AND CUL.idUser = @idUser

			/* DO THE INITIAL AWARD IF THE REQUIREMENTS WERE MET */

			IF (SELECT DISTINCT isRequirementMet FROM #CertificationStatus WHERE idCertification = @idCertification) = 1
				BEGIN

				UPDATE tblCertificationToUserLink SET
					initialAwardDate = @utcNow,
					certificationTrack = 1,
					currentExpirationDate = CASE WHEN C.initialAwardExpiresTimeframe  = 'd'
												THEN DATEADD(day, C.initialAwardExpiresInterval, @utcNow)
											WHEN C.initialAwardExpiresTimeframe  = 'm'
												THEN DATEADD(month, C.initialAwardExpiresInterval, @utcNow)
											WHEN C.initialAwardExpiresTimeframe  = 'yyyy'
												THEN DATEADD(year, C.initialAwardExpiresInterval, @utcNow)
											WHEN C.initialAwardExpiresTimeframe  = 'ww'
												THEN DATEADD(week, C.initialAwardExpiresInterval, @utcNow)
											ELSE 
												NULL
											END,					
					currentExpirationInterval = C.initialAwardExpiresInterval,
					currentExpirationTimeframe = C.initialAwardExpiresTimeframe,
					lastExpirationDate = @utcNow
				FROM tblCertificationToUserLink CUL
				LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
				WHERE CUL.idCertificationToUserLink = @idCertificationToUserLink

				-- write event log entry for initial award				
				INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idCertification, @idCertificationToUserLink, @idUser
				EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 604, @utcNow, @eventLogItem

				-- evaluate certificate completion for certification
				EXEC [Certificate.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idUser, @idCertification, 'certification', 'completed', @idUserTimezone	

				END

			END

		ELSE -- RENEWAL

			BEGIN

			/* REQUIREMENTS STATUS */

			INSERT INTO #RequirementsStatus (
				idCertificationModuleRequirement,
				idCertificationModuleRequirementSet,
				isRequirementMet
			)
			SELECT DISTINCT
				CMR.idCertificationModuleRequirement,
				CMR.idCertificationModuleRequirementSet,
				CASE WHEN CMR.requirementType = 0 THEN
						CASE WHEN CMR.courseCompletionIsAny = 1 THEN
							CASE WHEN (SELECT COUNT(1)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND CUL.lastExpirationDate IS NOT NULL
											 AND CUL.currentExpirationDate IS NOT NULL
											 AND E.dtCompleted >= CUL.lastExpirationDate									 
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									  ) >= 1 THEN 1 ELSE 0 END
						ELSE
							CASE WHEN (SELECT COUNT(1)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND CUL.lastExpirationDate IS NOT NULL
											 AND CUL.currentExpirationDate IS NOT NULL
											 AND E.dtCompleted >= CUL.lastExpirationDate
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									  ) >= (SELECT COUNT(1) 
											FROM tblCertificationModuleRequirementToCourseLink
											WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement
											) THEN 1 ELSE 0 END
						END
					 WHEN CMR.requirementType = 1 THEN
						CASE WHEN CMR.courseCreditEligibleCoursesIsAll = 1 THEN				
							CASE WHEN (SELECT SUM(CCCL.credits)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND CUL.lastExpirationDate IS NOT NULL
											 AND CUL.currentExpirationDate IS NOT NULL
											 AND E.dtCompleted >= CUL.lastExpirationDate
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationToCourseCreditLink WHERE idCertification = CUL.idCertification)) E_MAIN
									   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse
									  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END
						ELSE
							CASE WHEN (SELECT SUM(CCCL.credits)
									   FROM (SELECT DISTINCT idCourse 
											 FROM tblEnrollment E 
											 WHERE E.idUser = @idUser 
											 AND E.dtCompleted IS NOT NULL 
											 AND CUL.lastExpirationDate IS NOT NULL
											 AND CUL.currentExpirationDate IS NOT NULL
											 AND E.dtCompleted >= CUL.lastExpirationDate
											 AND E.idCourse IN (SELECT idCourse FROM tblCertificationModuleRequirementToCourseLink WHERE idCertificationModuleRequirement = CMR.idCertificationModuleRequirement)) E_MAIN
									   LEFT JOIN tblCertificationToCourseCreditLink CCCL ON CCCL.idCourse = E_MAIN.idCourse
									  ) >= CMR.numberCreditsRequired THEN 1 ELSE 0 END
						END
					 WHEN CMR.requirementType = 2 THEN
						CASE WHEN DCMR.dtCompleted IS NOT NULL 
								  AND CUL.lastExpirationDate IS NOT NULL
								  AND CUL.currentExpirationDate IS NOT NULL
								  AND DCMR.dtCompleted >= CUL.lastExpirationDate THEN 1 ELSE 0 END
				END AS [isRequirementMet]
			FROM tblCertificationToUserLink CUL
			LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
			LEFT JOIN tblCertificationModule CM ON CM.idCertification = C.idCertification
			LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModule = CM.idCertificationModule
			LEFT JOIN tblCertificationModuleRequirement CMR ON CMR.idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet
			LEFT JOIN [tblData-CertificationModuleRequirement] DCMR ON DCMR.idCertificationToUserLink = CUL.idCertificationToUserLink AND DCMR.idCertificationModuleRequirement = CMR.idCertificationModuleRequirement
			WHERE CUL.idUser = @idUser
			AND CUL.idCertification = @idCertification
			AND CM.isInitialRequirement = @isInitialRequirement

			/* REQUIREMENT SETS STATUS */

			INSERT INTO #RequirementSetsStatus (
				idCertificationModuleRequirementSet,
				idCertificationModule,
				isRequirementMet
			)
			SELECT DISTINCT
				RS.idCertificationModuleRequirementSet,
				CMRS.idCertificationModule,
				CASE WHEN CMRS.isAny = 1 THEN
					CASE WHEN (SELECT COUNT(1) FROM #RequirementsStatus WHERE isRequirementMet = 1 AND idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) >= 1 THEN 1 ELSE 0 END
				ELSE
					CASE WHEN (SELECT COUNT(1) FROM #RequirementsStatus WHERE isRequirementMet = 1 AND idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) = (SELECT COUNT(1) FROM #RequirementsStatus WHERE idCertificationModuleRequirementSet = CMRS.idCertificationModuleRequirementSet) THEN 1 ELSE 0 END
				END
			FROM #RequirementsStatus RS
			LEFT JOIN tblCertificationModuleRequirementSet CMRS ON CMRS.idCertificationModuleRequirementSet = RS.idCertificationModuleRequirementSet

			/* MODULES STATUS */

			INSERT INTO #ModulesStatus (
				idCertificationModule,
				idCertification,
				isRequirementMet
			)
			SELECT DISTINCT
				RSS.idCertificationModule,
				CM.idCertification,
				CASE WHEN CM.isAnyRequirementSet = 1 THEN
					CASE WHEN (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE isRequirementMet = 1 AND idCertificationModule = CM.idCertificationModule) >= 1 THEN 1 ELSE 0 END
				ELSE
					CASE WHEN (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE isRequirementMet = 1 AND idCertificationModule = CM.idCertificationModule) = (SELECT COUNT(1) FROM #RequirementSetsStatus WHERE idCertificationModule = CM.idCertificationModule) THEN 1 ELSE 0 END
				END
			FROM #RequirementSetsStatus RSS
			LEFT JOIN tblCertificationModule CM ON CM.idCertificationModule = RSS.idCertificationModule

			/* CERTIFICATION STATUS */

			INSERT INTO #CertificationStatus (
				idCertification,
				isRequirementMet
			)
			SELECT DISTINCT
				CUL.idCertification,
				CASE WHEN CUL.lastExpirationDate IS NOT NULL AND CUL.currentExpirationDate IS NOT NULL AND @utcNow >= CUL.lastExpirationDate AND @utcNow <= CUL.currentExpirationDate THEN
					1
				ELSE
					CASE WHEN C.isAnyModule = 1 THEN
						CASE WHEN (SELECT COUNT(1) FROM #ModulesStatus WHERE isRequirementMet = 1) >= 1 THEN 1 ELSE 0 END
					ELSE
						CASE WHEN (SELECT COUNT(1) FROM #ModulesStatus WHERE isRequirementMet = 1) = (SELECT COUNT(1) FROM #ModulesStatus) THEN 1 ELSE 0 END
					END
				END
			FROM tblCertificationToUserLink CUL
			LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
			WHERE CUL.idCertification = @idCertification
			AND CUL.idUser = @idUser

			/* DO THE RENEWAL IF THE REQUIREMENTS WERE MET */

			IF (SELECT DISTINCT isRequirementMet FROM #CertificationStatus WHERE idCertification = @idCertification) = 1
				BEGIN

				UPDATE tblCertificationToUserLink SET
					certificationTrack = 1,
					lastExpirationDate = CUL.currentExpirationDate,					
					currentExpirationDate = CASE WHEN C.renewalExpiresTimeframe  = 'd'
												THEN DATEADD(day, C.renewalExpiresInterval, CUL.currentExpirationDate)
											WHEN C.renewalExpiresTimeframe  = 'm'
												THEN DATEADD(month, C.renewalExpiresInterval, CUL.currentExpirationDate)
											WHEN C.renewalExpiresTimeframe  = 'yyyy'
												THEN DATEADD(year, C.renewalExpiresInterval, CUL.currentExpirationDate)
											WHEN C.renewalExpiresTimeframe  = 'ww'
												THEN DATEADD(week, C.renewalExpiresInterval, CUL.currentExpirationDate)
											ELSE 
												NULL
											END,					
					currentExpirationInterval = C.renewalExpiresInterval,
					currentExpirationTimeframe = C.renewalExpiresTimeframe
				FROM tblCertificationToUserLink CUL
				LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
				WHERE CUL.idCertificationToUserLink = @idCertificationToUserLink

				-- write event log entry for renewal
				INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idCertification, @idCertificationToUserLink, @idUser
				EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 605, @utcNow, @eventLogItem

				-- evaluate certificate completion for certification
				EXEC [Certificate.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idUser, @idCertification, 'certification', 'completed', @idUserTimezone	

				END

			END		

		-- get next cursor item
		FETCH NEXT FROM userCertificationsCursor INTO @idCertificationToUserLink, @idCertification, @certificationTrack

		END

	CLOSE userCertificationsCursor
	DEALLOCATE userCertificationsCursor

	/*

	DROP THE TEMP TABLES

	*/

	DROP TABLE #CertificationStatus
	DROP TABLE #ModulesStatus
	DROP TABLE #RequirementSetsStatus
	DROP TABLE #RequirementsStatus	

	/*

	return

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO