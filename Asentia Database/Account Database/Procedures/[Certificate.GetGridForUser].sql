-- =====================================================================
-- PROCEDURE: [Certificate.GetGridForUser]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certificate.GetGridForUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certificate.GetGridForUser]
GO

/*

Gets a listing of Certificates awarded to a user.

*/

CREATE PROCEDURE [Certificate.GetGridForUser]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idUser					INT,
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblCertificateRecord CR
			INNER JOIN tblCertificate C ON C.idCertificate = CR.idCertificate
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND CR.idSite = @idCallerSite)
				)
			AND CR.idUser = @idUser
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						CR.idCertificateRecord,
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN C.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'timestamp' THEN CR.[timestamp] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'credits' THEN C.credits END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'dtExpires' THEN CR.expires END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN C.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'timestamp' THEN CR.[timestamp] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'credits' THEN C.credits END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'dtExpires' THEN CR.expires END) END
						)
						AS [row_number]
					FROM tblCertificateRecord CR
					INNER JOIN tblCertificate C ON C.idCertificate = CR.idCertificate
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND CR.idSite = @idCallerSite)
						)
					AND CR.idUser = @idUser
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idCertificateRecord,
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				CR.idCertificateRecord,
				CR.[timestamp],
				C.idCertificate,
				CASE WHEN CL.name IS NOT NULL THEN CL.name ELSE C.name END AS name,
				C.issuingOrganization,
				C.credits,
				CR.expires AS dtExpires, 
				CONVERT(BIT, 1) AS isViewOn,
				CONVERT(BIT, 1) AS isPrintOn,
				CONVERT(BIT, 1) AS isPDFExportOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			INNER JOIN tblCertificateRecord CR ON CR.idCertificateRecord = SelectedKeys.idCertificateRecord
			INNER JOIN tblCertificate C ON C.idCertificate = CR.idCertificate
			LEFT JOIN tblCertificateLanguage CL ON CL.idCertificate = C.idCertificate AND CL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblCertificateRecord CR
				INNER JOIN tblCertificate C ON C.idCertificate = CR.idCertificate
				INNER JOIN CONTAINSTABLE(tblCertificate, *, @searchParam) K ON K.[key] = C.idCertificate
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND CR.idSite = @idCallerSite)
					)
				AND CR.idUser = @idUser			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							CR.idCertificateRecord,
							ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN C.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'timestamp' THEN CR.[timestamp] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'credits' THEN C.credits END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'dtExpires' THEN CR.expires END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN C.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'timestamp' THEN CR.[timestamp] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'credits' THEN C.credits END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'dtExpires' THEN CR.expires END) END
							)
							AS [row_number]
						FROM tblCertificateRecord CR
						INNER JOIN tblCertificate C ON C.idCertificate = CR.idCertificate
						INNER JOIN CONTAINSTABLE(tblCertificate, *, @searchParam) K ON K.[key] = C.idCertificate
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND CR.idSite = @idCallerSite
							)
						AND CR.idUser = @idUser
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idCertificateRecord, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					CR.idCertificateRecord,
					CR.[timestamp],
					C.idCertificate,
					C.name,
					C.issuingOrganization,
					C.credits, 
					CR.expires AS dtExpires,
					CONVERT(BIT, 1) AS isViewOn,
					CONVERT(BIT, 1) AS isPrintOn,
					CONVERT(BIT, 1) AS isPDFExportOn,				
					SelectedKeys.[row_number]
				FROM SelectedKeys
				INNER JOIN tblCertificateRecord CR ON CR.idCertificateRecord = SelectedKeys.idCertificateRecord
				INNER JOIN tblCertificate C ON C.idCertificate = CR.idCertificate
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblCertificateLanguage CL
				LEFT JOIN tblCertificate C ON C.idCertificate = CL.idCertificate
				INNER JOIN tblCertificateRecord CR ON CR.idCertificate = C.idCertificate				
				INNER JOIN CONTAINSTABLE(tblCertificateLanguage, *, @searchParam) K ON K.[key] = CL.idCertificateLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND CR.idSite = @idCallerSite)
					)
				AND CL.idLanguage = @idCallerLanguage
				AND CR.idUser = @idUser			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							CL.idCertificateLanguage,
							C.idCertificate,
							CR.idCertificateRecord,
							ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN CL.name END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'timestamp' THEN CR.[timestamp] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'credits' THEN C.credits END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'dtExpires' THEN CR.expires END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'name' THEN CL.name END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'timestamp' THEN CR.[timestamp] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'credits' THEN C.credits END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'dtExpires' THEN CR.expires END) END
							)
							AS [row_number]
						FROM tblCertificateLanguage CL
						LEFT JOIN tblCertificate C ON C.idCertificate = CL.idCertificate
						INNER JOIN tblCertificateRecord CR ON CR.idCertificate = C.idCertificate
						INNER JOIN CONTAINSTABLE(tblCertificateLanguage, *, @searchParam) K ON K.[key] = CL.idCertificateLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND CR.idSite = @idCallerSite
							)
						AND CL.idLanguage = @idCallerLanguage
						AND CR.idUser = @idUser						
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idCertificateRecord,
							idCertificate,
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					CR.idCertificateRecord,
					CR.[timestamp],
					C.idCertificate,
					C.name,
					C.issuingOrganization,
					C.credits, 
					CR.expires AS dtExpires,
					CONVERT(BIT, 1) AS isViewOn,
					CONVERT(BIT, 1) AS isPrintOn,
					CONVERT(BIT, 1) AS isPDFExportOn,				
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblCertificateLanguage CL ON CL.idCertificate = SelectedKeys.idCertificate AND CL.idLanguage = @idCallerLanguage
				LEFT JOIN tblCertificate C ON C.idCertificate = CL.idCertificate
				INNER JOIN tblCertificateRecord CR ON CR.idCertificate = C.idCertificate
				ORDER BY SelectedKeys.[row_number]

				END
			
			END
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO