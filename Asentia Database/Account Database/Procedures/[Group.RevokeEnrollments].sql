-- =====================================================================
-- PROCEDURE: [Group.RevokeEnrollments]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.RevokeEnrollments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.RevokeEnrollments]
GO

/*

Revokes course Group Enrollment enrollments where the enrollment is incomplete
and the user is no longer in the group. 

This procedure handles COMPLETE REVOCATION of enrollments inherited from a
Group Enrollment, as well as lateral moves from one Group Enrollment to another.
This procedure is "hooked into" (called directly from) the AssignEnrollments procedure 
in order to guarantee that revocations and lateral moves are done AFTER the assignments 
are done. 

IN ORDER TO COMPLETELY ELIMINATE THE POSSIBILITY OF INHERITED ENROLLMENTS BEING
MISTAKENLY REMOVED, THIS PROCEDURE SHOULD ONLY BE CALLED FROM "AssignEnrollments,"
IT SHOULD NEVER BE CALLED DIRECTLY BY A JOB OR FROM PROCEDURAL CODE.

Notes:

@Groups  -  IDTable of Group ids to revoke Enrollments for.
		    Required to be passed, but not required to be populated.
		    If not populated, this will be executed on ALL Groups
		    that have Group Enrollments.

Note that this will take care of revocations where user(s) are no longer members of 
groups containing enrollments. Revocations where the Group Enrollment has been deleted
take place in the GroupEnrollment.Delete procedure.

*/

CREATE PROCEDURE [Group.RevokeEnrollments]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,

	@Groups						IDTable			READONLY
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* CHECK GROUP LIST FOR RECORDS */
	DECLARE @GroupsRecordCount INT
	SELECT @GroupsRecordCount = COUNT(1) FROM @Groups

	/*

	Get all of the group enrollment matched users for the group(s) and dump the results into a temp table.

	*/

	DECLARE @GroupEnrollmentMatches TABLE (
		idSite INT,
		idGroupEnrollment INT,
		idCourse INT,
		idUser INT
	)

	INSERT INTO @GroupEnrollmentMatches
	(
		idSite,
		idGroupEnrollment,
		idCourse,
		idUser
	)
	SELECT
		GE.idSite,
		GE.idGroupEnrollment,
		GE.idCourse,
		UGL.idUser
	FROM tblGroupEnrollment GE
	LEFT JOIN tblCourse C ON C.idCourse = GE.idCourse
	LEFT JOIN tblUserToGroupLink UGL ON UGL.idGroup = GE.idGroup
	WHERE (
			(@GroupsRecordCount = 0)
			OR
			(@GroupsRecordCount > 0 AND EXISTS (SELECT 1 FROM @Groups GG WHERE GG.id = GE.idGroup))
		  )
	AND (C.isDeleted IS NULL OR C.isDeleted = 0)
	AND UGL.idUser IS NOT NULL

	/*

	Get all enrollments that are group inherited where they are not in the matches above.
	This should net us the enrollments that need to be revoked, or have the group enrollment
	link removed where the enrollment is already completed.

	*/

	DECLARE @MatchedGroupEnrollments IDTable
	INSERT INTO @MatchedGroupEnrollments (id) SELECT DISTINCT idGroupEnrollment FROM @GroupEnrollmentMatches

	/*

	If this is for "all groups" we need to make sure all group enrollments are in the @MatchedGroupEnrollments table.
	This is for "edge cases" where there are no more users in a group containing a group enrollment as the above query
	will not return those group enrollment ids.

	*/

	IF (@GroupsRecordCount = 0)
		BEGIN
		INSERT INTO @MatchedGroupEnrollments 
			(id) 
		SELECT DISTINCT idGroupEnrollment 
		FROM tblGroupEnrollment 
		WHERE idGroupEnrollment NOT IN (SELECT id FROM @MatchedGroupEnrollments)
		END

	/* 
	
	Replica of tblEnrollment minus idTimezone, code, revcode, title, all intervals, and idActivityImport. 
	We may weed out more fields later as we discover they are not needed. 

	*/

	CREATE TABLE #CurrentEnrollments
	(
		idEnrollment				INT,
		idSite						INT,
		idCourse					INT,
		idUser						INT,
		idGroupEnrollment			INT,
		idRuleSetEnrollment			INT,
		isLockedByPrerequisites		BIT,
		dtStart						DATETIME,
		dtDue						DATETIME,
		dtExpiresFromStart			DATETIME,
		dtExpiresFromFirstLaunch	DATETIME,
		dtFirstLaunch				DATETIME,
		dtCompleted					DATETIME,
		dtCreated					DATETIME,
		dtLastSynchronized			DATETIME
	)

	INSERT INTO #CurrentEnrollments
	(
		idEnrollment,
		idSite,
		idCourse,
		idUser,
		idGroupEnrollment,
		idRuleSetEnrollment,
		isLockedByPrerequisites,
		dtStart,
		dtDue,
		dtExpiresFromStart,
		dtExpiresFromFirstLaunch,
		dtFirstLaunch,
		dtCompleted,
		dtCreated,
		dtLastSynchronized
	)
	SELECT
		E.idEnrollment,
		E.idSite,
		E.idCourse,
		E.idUser,
		E.idGroupEnrollment,
		E.idRuleSetEnrollment,
		E.isLockedByPrerequisites,
		E.dtStart,
		E.dtDue,
		E.dtExpiresFromStart,
		E.dtExpiresFromFirstLaunch,
		E.dtFirstLaunch,
		E.dtCompleted,
		E.dtCreated,
		E.dtLastSynchronized
	FROM tblEnrollment E	
	WHERE E.idGroupEnrollment IN (SELECT id FROM @MatchedGroupEnrollments)
	AND E.idUser NOT IN (SELECT DISTINCT idUser FROM @GroupEnrollmentMatches)
	AND E.idActivityImport IS NULL -- WE SHOULD WEED OUT ENROLLMENTS CREATED FROM AN ACTIVITY IMPORT, FOR NOW	

	/*

	REMOVE the link to the Group Enrollment for completed enrollments where the user is no longer
	matched to the Group Enrollment they have inherited the enrollment for.

	*/

	UPDATE tblEnrollment SET		
		idGroupEnrollment = NULL
	FROM #CurrentEnrollments CE
	WHERE CE.idEnrollment = tblEnrollment.idEnrollment
	AND CE.dtCompleted IS NOT NULL

	/*

	DELETE incomplete Enrollments where the user is no longer matched to the Group Enrollment they
	have inherited the enrollment for.

	*/

	DECLARE @EnrollmentsToDelete IDTable

	INSERT INTO @EnrollmentsToDelete (
		id
	)
	SELECT
		CE.idEnrollment
	FROM #CurrentEnrollments CE
	WHERE CE.dtCompleted IS NULL

	-- do the delete using the Enrollment.Delete procedure
	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EnrollmentsToDelete
				   
	/* DROP THE TEMPORARY TABLES */
	DROP TABLE #CurrentEnrollments	

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO