-- =====================================================================
-- PROCEDURE: [Analytic.SaveCourses]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Analytic.SaveCourses]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Analytic.SaveCourses]
GO

/*

Saves courses for learning path.

*/

CREATE PROCEDURE [Analytic.SaveCourses]
(
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT						= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT						= 0,
	
	@idAnalytic				INT,
	@Courses				IDTableWithOrdering		READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that all courses exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Courses CC
		LEFT JOIN tblCourse C ON C.idCourse = CC.id
		WHERE C.idSite IS NULL 
		OR C.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'AnalyticSaveCourses_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	validate that the analytic belongs to the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAnalytic LP
		WHERE LP.idAnalytic = @idAnalytic
		AND (idSite IS NULL OR idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'AnalyticSaveCourses_DetailsNotFound'
		RETURN 1 
		END

	/*
	
	remove all courses for this analytic from the linking table
	
	*/
	
	DELETE FROM tblAnalyticToCourseLink
	WHERE idAnalytic = @idAnalytic
	
	/*

	insert the courses for this analytic into the linking table

	*/
	
	INSERT INTO tblAnalyticToCourseLink (
		idAnalytic, 
		idSite,
		idCourse, 
		[order]
	)
	SELECT 
		@idAnalytic,
		@idCallerSite, 
		id, 
		[order]
	FROM @Courses C
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblAnalyticToCourseLink
		WHERE idAnalytic = @idAnalytic
		AND idCourse = C.id
	)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO