-- =====================================================================
-- PROCEDURE: [[OBJECT].Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[[OBJECT].Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [[OBJECT].Delete]
GO

/*

Deletes [OBJECT]s

*/

CREATE PROCEDURE [[OBJECT].Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	@[OBJECT]s				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
		
	/*
	
	validate caller permission to all specified objects
	
	*/
	
	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @[OBJECT]s) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = '[OBJECT]Delete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all [OBJECT]s exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @[OBJECT]s __
		LEFT JOIN tbl[OBJECT] _ ON _.id[OBJECT] = __.id
		WHERE _.idSite IS NULL
		OR _.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = '[OBJECT]Delete_NoRecordFound'
		RETURN 1 
		END
		
	/*
	
	DELETE THE OBJECT OR MARK OBJECT AS DELETED, DEPENDING ON WHETHER OR NOT THE OBJECT'S TABLE 
	CONTAINS DELETE FLAGS, AND DELETE ANY RELATED/LINKED OBJECTS.

	REMOVE THIS COMMENT BLOCK AND REPLACE WITH COMMENT BLOCKS FOR THE ACTIONS SPECIFIC TO THE OBJECT
	
	*/
	
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO