-- =====================================================================
-- PROCEDURE: [GridAnalytic.ContentPackages]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[GridAnalytic.ContentPackages]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GridAnalytic.ContentPackages]
GO

/*

Gets grid analytic data for Content Packages grid page.

*/

CREATE PROCEDURE [GridAnalytic.ContentPackages]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
AS
	BEGIN
		SET NOCOUNT ON

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		create a temp table for base content packages data, and populate it
		we will run the analytics off of that table so as not to tie up the real table

		*/

		CREATE TABLE #ContentPackagesBase (
			idContentPackage INT,
			idSite INT,
			isSCORM BIT,
			isXAPI BIT,
			isQuizSurvey BIT,
			isVideo BIT,
			isPowerPoint BIT,			
			isPDF BIT,
			dtCreated DATETIME
		)

		INSERT INTO #ContentPackagesBase (
			idContentPackage,
			idSite,
			isSCORM,
			isXAPI,
			isQuizSurvey,
			isVideo,
			isPowerPoint,
			isPDF,
			dtCreated
		)
		SELECT
			CP.idContentPackage,
			CP.idSite,
			CASE WHEN CP.idContentPackageType = 1 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			CASE WHEN CP.idContentPackageType = 2 AND CP.idMediaType IS NULL AND CP.idQuizSurvey IS NULL THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			CASE WHEN CP.idQuizSurvey IS NOT NULL AND CP.idMediaType IS NULL THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			CASE WHEN CP.idMediaType = 1 THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			CASE WHEN CP.idMediaType = 2 THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			CASE WHEN CP.idMediaType = 3 THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			CP.dtCreated
		FROM tblContentPackage CP
		WHERE CP.idSite = @idCallerSite
		AND (CP.isProcessing = 0 OR CP.isProcessing IS NULL) -- nothing that is currently processing
		/*

		run the analytics

		*/

		SELECT
			(SELECT COUNT(1) FROM #ContentPackagesBase) AS total,
			(SELECT COUNT(1) FROM #ContentPackagesBase WHERE isSCORM = 1) AS scorm,
			(SELECT COUNT(1) FROM #ContentPackagesBase WHERE isXAPI = 1) AS xapi,
			(SELECT COUNT(1) FROM #ContentPackagesBase WHERE isQuizSurvey = 1) AS quizSurvey,
			(SELECT COUNT(1) FROM #ContentPackagesBase WHERE isVideo = 1) AS video,
			(SELECT COUNT(1) FROM #ContentPackagesBase WHERE isPowerPoint = 1) AS powerPoint,
			(SELECT COUNT(1) FROM #ContentPackagesBase WHERE isPDF = 1) AS pdf,
			(SELECT COUNT(1) FROM #ContentPackagesBase WHERE dtCreated >= DATEADD(WEEK, DATEDIFF(WEEK, '1905-01-01', GETUTCDATE()), '1905-01-01')) AS createdThisWeek,
			(SELECT COUNT(1) FROM #ContentPackagesBase WHERE dtCreated >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETUTCDATE()), 0)) AS createdThisMonth,
			(SELECT COUNT(1) FROM #ContentPackagesBase WHERE dtCreated >= DATEADD(YEAR, DATEDIFF(YEAR, 0, GETUTCDATE()), 0)) AS createdThisYear

		-- DROP THE TEMPORARY TABLES
		DROP TABLE #ContentPackagesBase

		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO