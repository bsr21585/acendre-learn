-- =====================================================================
-- PROCEDURE: [ContentPackage.Save]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ContentPackage.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ContentPackage.Save]
GO

/*

Adds new content package or updates existing content package.

*/
CREATE PROCEDURE [ContentPackage.Save]
(
    @Return_Code					INT				OUTPUT,
    @Error_Description_Code			NVARCHAR(50)	OUTPUT,
    @idCallerSite					INT				= 0,
    @callerLangString				NVARCHAR(10),
    @idCaller						INT				= 0,
    
    @idContentPackage				INT				OUTPUT,
    @name							NVARCHAR(255),
    @path							NVARCHAR(1024),
    @kb								INT,
	@idContentPackageType			INT,
    @idSCORMPackageType				INT,
    @manifest						NVARCHAR(MAX),
	@isMediaUpload					BIT,
	@idMediaType					INT,
	@contentTitle					NVARCHAR(255),
	@originalMediaFilename			NVARCHAR(255),
	@isVideoMedia3rdParty			BIT,
	@videoMediaEmbedCode			NVARCHAR(MAX),
	@enableAutoplay					BIT,
	@allowRewind					BIT,
	@allowFastForward				BIT,
	@allowNavigation				BIT,
	@allowResume					BIT,
	@minProgressForCompletion		FLOAT,
	@isProcessing					BIT,
	@isProcessed					BIT,
	@dtProcessed					DATETIME,
	@idQuizSurvey					INT,
	@hasManifestComplianceErrors	BIT,
	@manifestComplianceErrors		NVARCHAR(MAX),
	@openSesameGUID					NVARCHAR(36)

)
AS 
    BEGIN
    SET NOCOUNT ON

    DECLARE @idPermission INT -- define the permission required to perform this function
    SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	save the data
	
	*/
	
    IF (@idContentPackage = 0 OR @idContentPackage IS NULL) 
            
		BEGIN
		
		-- insert the new object
		
        INSERT  INTO dbo.tblContentPackage ( 
			idSite,
            name,
            [path],
            kb,
            idContentPackageType,
			idSCORMPackageType,
            manifest,
            dtCreated,
            dtModified,
			isMediaUpload,
			idMediaType,
			contentTitle,
			originalMediaFilename,
			isVideoMedia3rdParty,
			videoMediaEmbedCode,
			enableAutoplay,
			allowRewind,
			allowFastForward,
			allowNavigation,
			allowResume,
			minProgressForCompletion,
			isProcessing,
			isProcessed,
			dtProcessed,
			idQuizSurvey,
			hasManifestComplianceErrors,
			manifestComplianceErrors,
			openSesameGUID
        )
        VALUES ( 
			@idCallerSite,
            @name,
            @path,
            @kb,
            @idContentPackageType,
			@idSCORMPackageType,
            @manifest,
            GETUTCDATE(),
            GETUTCDATE(),
			@isMediaUpload,
			@idMediaType,
			@contentTitle,
			@originalMediaFilename,
			@isVideoMedia3rdParty,
			@videoMediaEmbedCode,
			@enableAutoplay,
			@allowRewind,
			@allowFastForward,
			@allowNavigation,
			@allowResume,
			@minProgressForCompletion,
			@isProcessing,
			@isProcessed,
			@dtProcessed,
			@idQuizSurvey,
			@hasManifestComplianceErrors,
			@manifestComplianceErrors,
			@openSesameGUID
        )
		
		-- get the new object id and return successfully
		
        SELECT @idContentPackage = SCOPE_IDENTITY()
    
		END
		
	ELSE

		BEGIN
		
		-- check that the group id exists
		IF (SELECT COUNT(1) FROM tblContentPackage WHERE idContentPackage = @idContentPackage AND idSite = @idCallerSite) < 1
			BEGIN                               
			
			SET @idContentPackage = @idContentPackage
			SET @Return_Code = 1 --(1 is 'details not found')
			SET @Error_Description_Code = 'ContentPackageSave_NoRecordFound'
			RETURN 1
			
			END
			
		-- update existing content package's properties
		
		UPDATE tblContentPackage SET
			name = @name,
			[path] = @path,
			kb = @kb,
			idContentPackageType = @idContentPackageType,
			idSCORMPackageType = @idSCORMPackageType,
            manifest = @manifest,
            dtModified = GETUTCDATE(),
			contentTitle = @contentTitle,
			enableAutoplay = @enableAutoplay,
			allowRewind = @allowRewind,
			allowFastForward = @allowFastForward,
			allowNavigation = @allowNavigation,
			allowResume = @allowResume,
			minProgressForCompletion = @minProgressForCompletion,
			idQuizSurvey = @idQuizSurvey,
			hasManifestComplianceErrors = @hasManifestComplianceErrors,
			manifestComplianceErrors = @manifestComplianceErrors,
			openSesameGUID = @openSesameGUID
		WHERE idContentPackage = @idContentPackage
		
		-- get the content package's id 
		SELECT @idContentPackage = @idContentPackage

		END
		
	SET @Error_Description_Code = ''
	SET @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO