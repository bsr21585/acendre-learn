-- =====================================================================
-- PROCEDURE: [LearningPath.EnrollmentStatistics]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[LearningPath.EnrollmentStatistics]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.EnrollmentStatistics]
GO

/*

Gets enrollment statistics for a learning path.

*/

CREATE PROCEDURE [LearningPath.EnrollmentStatistics]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idLearningPath			INT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	put all learning path enrollments into a temporary table, we'll work from that table

	*/

	CREATE TABLE #LearningPathEnrollments (
		idLearningPathEnrollment	INT,
		idLearningPath				INT,
		dtStart						DATETIME,
		dtDue						DATETIME,
		dtExpires					DATETIME,
		dtCompleted					DATETIME
	)

	INSERT INTO #LearningPathEnrollments (
		idLearningPathEnrollment,
		idLearningPath,
		dtStart,
		dtDue,
		dtExpires,
		dtCompleted
	)
	SELECT
		LPE.idLearningPathEnrollment,
		LPE.idLearningPath,
		LPE.dtStart,
		LPE.dtDue,
		CASE WHEN LPE.dtExpiresFromStart IS NULL AND LPE.dtExpiresFromFirstLaunch IS NULL THEN
			NULL
		ELSE
			CASE WHEN LPE.dtExpiresFromStart IS NULL AND LPE.dtExpiresFromFirstLaunch IS NOT NULL THEN
				LPE.dtExpiresFromFirstLaunch
			ELSE
				CASE WHEN LPE.dtExpiresFromStart IS NOT NULL AND LPE.dtExpiresFromFirstLaunch IS NULL THEN
					LPE.dtExpiresFromStart
				ELSE
					CASE WHEN LPE.dtExpiresFromStart >= LPE.dtExpiresFromFirstLaunch THEN
						LPE.dtExpiresFromFirstLaunch
					ELSE
						LPE.dtExpiresFromStart
					END
				END
			END
		END AS [dtExpires],
		LPE.dtCompleted
	FROM tblLearningPathEnrollment LPE
	LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
	LEFT JOIN tblUser U ON U.idUser = LPE.idUser
	WHERE LPE.idSite = @idCallerSite
	AND LPE.idLearningPath = @idLearningPath
	AND (LP.isDeleted = 0 OR LP.isDeleted IS NULL)
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- enrollments for deleted and unapproved users should not be reflected
	AND U.isActive = 1 -- only active users should be included
	AND LPE.dtStart <= GETUTCDATE()	
		
	/*

	LEARNING PATH ENROLLMENT STATISTICS BY STATUS

	*/

	SELECT
		(SELECT COUNT(1) FROM #LearningPathEnrollments LPE WHERE (LPE.dtDue > GETUTCDATE() OR LPE.dtDue IS NULL) AND (LPE.dtExpires > GETUTCDATE() OR LPE.dtExpires IS NULL) AND LPE.dtCompleted IS NULL) AS [##Enrolled##],
		(SELECT COUNT(1) FROM #LearningPathEnrollments LPE WHERE LPE.dtCompleted IS NOT NULL) AS [##Completed##],
		(SELECT COUNT(1) FROM #LearningPathEnrollments LPE WHERE LPE.dtDue <= GETUTCDATE() AND (LPE.dtExpires > GETUTCDATE() OR LPE.dtExpires IS NULL) AND LPE.dtCompleted IS NULL) AS [##Overdue##],
		(SELECT COUNT(1) FROM #LearningPathEnrollments LPE WHERE LPE.dtExpires IS NOT NULL AND LPE.dtExpires <= GETUTCDATE() AND LPE.dtCompleted IS NULL) AS [##Expired##],
		(SELECT COUNT(1) FROM #LearningPathEnrollments LPE) AS _Total_
		
	-- DROP THE TEMPORARY TABLES
	DROP TABLE #LearningPathEnrollments		

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO