-- =====================================================================
-- PROCEDURE: [Role.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Role.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Role.GetGrid]
GO

/*

Gets a listing of Roles.

*/

CREATE PROCEDURE [Role.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		get the site's default language id

		*/

		DECLARE @idSiteLanguage INT
		SELECT @idSiteLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblRole R
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
				OR
				(R.idSite = 1)
				)			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						R.idRole,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN R.name END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN R.name END
						)
						AS [row_number]
					FROM tblRole R
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
						OR
						(R.idSite = 1)
						)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idRole, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				R.idRole,
				R.idSite,
				CASE WHEN RL.name IS NOT NULL THEN RL.name ELSE R.name END AS name,
				CASE WHEN R.idSite = 1 THEN 'view' ELSE 'modify' END AS viewModify,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblRole R ON R.idRole = SelectedKeys.idRole
			LEFT JOIN tblRoleLanguage RL ON RL.idRole = R.idRole AND RL.idLanguage = @idCallerLanguage
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- if the caller's language is the same as the site's language,
			-- search and display on the base table
			IF (@idCallerLanguage = @idSiteLanguage)

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblRole R
				INNER JOIN CONTAINSTABLE(tblRole, *, @searchParam) K ON K.[key] = R.idRole
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
					OR
					(R.idSite = 1)
					)
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							R.idRole,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN R.name END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN R.name END
							)
							AS [row_number]
						FROM tblRole R
						INNER JOIN CONTAINSTABLE(tblRole, *, @searchParam) K ON K.[key] = R.idRole
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite
							OR
							(R.idSite = 1)
							)
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idRole, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					R.idRole,
					R.idSite,
					R.name AS name,
					CASE WHEN R.idSite = 1 THEN 'view' ELSE 'modify' END AS viewModify,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblRole R ON R.idRole = SelectedKeys.idRole
				ORDER BY SelectedKeys.[row_number]

				END
			
			-- if the caller's language is not the same as the site's
			-- default language, we will need to search and display on
			-- the language table, and join to the base table for the
			-- rest of the properties
			ELSE

				BEGIN

				-- return the rowcount
			
				SELECT COUNT(1) AS row_count 
				FROM tblRoleLanguage RL
				LEFT JOIN tblRole R ON R.idRole = RL.idRole
				INNER JOIN CONTAINSTABLE(tblRoleLanguage, *, @searchParam) K ON K.[key] = RL.idRoleLanguage
				WHERE 
					(
					(@idCallerSite IS NULL) 
					OR 
					(@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite)
					OR
					(R.idSite = 1)
					)
					AND RL.idLanguage = @idCallerLanguage
			
				;WITH 
					Keys AS (
						SELECT TOP (@pageNum * @pageSize) 
							RL.idRoleLanguage,
							R.idRole,
							ROW_NUMBER() OVER (ORDER BY
								-- FIRST ORDER DESC
								CASE WHEN @orderAsc = 0 THEN RL.name END DESC,
							
								-- FIRST ORDER ASC
								CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RL.name END
							)
							AS [row_number]
						FROM tblRoleLanguage RL
						LEFT JOIN tblRole R ON R.idRole = RL.idRole
						INNER JOIN CONTAINSTABLE(tblRoleLanguage, *, @searchParam) K ON K.[key] = RL.idRoleLanguage
						WHERE 
							(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND R.idSite = @idCallerSite
							OR
							(R.idSite = 1)
							)
							AND RL.idLanguage = @idCallerLanguage
					), 
				
					SelectedKeys AS (
						SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
							idRole, 
							[row_number]
						FROM Keys
						WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					)
				SELECT 
					R.idRole,
					R.idSite,
					RL.name,
					CASE WHEN R.idSite = 1 THEN 'view' ELSE 'modify' END AS viewModify,
					SelectedKeys.[row_number]
				FROM SelectedKeys
				JOIN tblRoleLanguage RL ON RL.idRole = SelectedKeys.idRole AND RL.idLanguage = @idCallerLanguage
				LEFT JOIN tblRole R ON R.idRole = RL.idRole
				ORDER BY SelectedKeys.[row_number]

				END
			
			END

	END					

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO