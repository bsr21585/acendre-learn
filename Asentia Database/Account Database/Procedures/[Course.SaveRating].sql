-- =====================================================================
-- PROCEDURE: [Course.SaveRating]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.SaveRating]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.SaveRating]
GO

/*

Saves a user's rating for a course.

*/

CREATE PROCEDURE [Course.SaveRating]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCourse				INT,
	@rating					INT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate that the course belongs to the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourse C
		WHERE C.idCourse = @idCourse
		AND (C.idSite IS NULL OR C.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseSaveRating_DetailsNotFound'
		RETURN 1 
		END
	
	/*

	insert/update the user's rating for the course

	*/

	IF (SELECT COUNT(1) FROM tblCourseRating WHERE idCourse = @idCourse AND idUser = @idCaller) = 0
		BEGIN

		INSERT INTO tblCourseRating (
			idSite,
			idCourse,
			idUser,
			rating,
			[timestamp]
		)
		VALUES (
			@idCallerSite,
			@idCourse,
			@idCaller,
			@rating,
			GETUTCDATE()
		)

		END
	ELSE
		BEGIN

		UPDATE tblCourseRating SET
			rating = @rating,
			[timestamp] = GETUTCDATE()
		WHERE idCourse = @idCourse
		AND idUser = @idCaller

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO