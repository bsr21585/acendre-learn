-- =====================================================================
-- PROCEDURE: [Group.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.Delete]
GO

CREATE PROCEDURE [Group.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@Groups					IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF (SELECT COUNT(1) FROM @Groups) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'GroupDelete_NoRecordsSelected'
		RETURN 1
		END

	/*
	
	validate that all groups exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Groups GG
		LEFT JOIN tblGroup G ON G.idGroup = GG.id
		WHERE G.idSite IS NULL
		OR G.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'GroupDelete_DetailsNotFound'
		RETURN 1 
		END

	/*

	Delete the group feed moderators and messages

	*/

	DELETE FROM tblGroupFeedModerator
	WHERE EXISTS (
		SELECT 1
		FROM @Groups GG
		WHERE tblGroupFeedModerator.idGroup = GG.id
	)

	DELETE FROM tblGroupFeedMessage
	WHERE EXISTS (
		SELECT 1
		FROM @Groups GG
		WHERE tblGroupFeedMessage.idGroup = GG.id
	)

	/*

	GET ruleset ids to remove rules, rulesets, and ruleset links

	*/

	DECLARE @RuleSets IDTable

	INSERT INTO @RuleSets (
		id
	)
	SELECT
		idRuleSet
	FROM tblRuleSetToGroupLink
	WHERE idGroup IN (SELECT id FROM @Groups)

	/*

	DELETE ruleset to ruleset group links.
	
	*/
	
	DELETE FROM tblRuleSetToGroupLink
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the rules 

	*/

	DELETE FROM tblRule 
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the ruleset language records

	*/

	DELETE FROM tblRuleSetLanguage
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE the rulesets

	*/

	DELETE FROM tblRuleSet
	WHERE idRuleSet IN (SELECT id FROM @RuleSets)

	/*

	DELETE open ruleset engine queue items

	*/

	DELETE FROM tblRuleSetEngineQueue
	WHERE triggerObjectId IN (SELECT id FROM @Groups) 
	AND triggerObject = 'group'
	AND isProcessing IS NULL
	AND dtProcessed IS NULL

	/*
	
	DELETE the role to group links
	
	*/
	
	DELETE FROM tblGroupToRoleLink
	WHERE EXISTS (
		SELECT 1
		FROM @Groups GG
		WHERE tblGroupToRoleLink.idGroup = GG.id
	)
	
	/*
	
	DELETE the user to group links
	
	*/
	
	DELETE FROM tblUserToGroupLink
	WHERE EXISTS (
		SELECT 1
		FROM @Groups GG
		WHERE tblUserToGroupLink.idGroup = GG.id
	)

	/*
	
	DELETE the catalog access to group links
	
	*/
	
	DELETE FROM tblCatalogAccessToGroupLink
	WHERE EXISTS (
		SELECT 1
		FROM @Groups GG
		WHERE tblCatalogAccessToGroupLink.idGroup = GG.id
	)

	/*
	
	DELETE the group enrollments by using the group enrollment delete procedure.
	
	*/
	
	DECLARE @GroupEnrollmentsToDelete IDTable

	INSERT INTO @GroupEnrollmentsToDelete (
		id
	)
	SELECT
		idGroupEnrollment
	FROM tblGroupEnrollment GE
	WHERE GE.idGroup IN (SELECT id FROM @Groups)

	EXEC [GroupEnrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, 1, @GroupEnrollmentsToDelete, 0

	/*

	DELETE all the entries in [tblDocumentRepositoryItemLanguage] related to group(s) which we are deleting

	*/

	DELETE FROM tblDocumentRepositoryItemLanguage WHERE idDocumentRepositoryItem IN (SELECT idDocumentRepositoryItem FROM tblDocumentRepositoryItem WHERE idObject IN (SELECT id FROM @Groups) AND idDocumentRepositoryObjectType = 1)  -- 1 is for group

	/*

	DELETE all the entries in [tblDocumentRepositoryItem] related to group(s) which we are deleting

	*/

	DELETE FROM tblDocumentRepositoryItem WHERE idObject IN (SELECT id FROM @Groups) AND idDocumentRepositoryObjectType = 1  -- 1 is for group

	/*

	DELETE all the entries in [tblDocumentRepositoryFolder] related to group(s) which we are deleting

	*/

	DELETE FROM tblDocumentRepositoryFolder WHERE idObject IN (SELECT id FROM @Groups) AND idDocumentRepositoryObjectType = 1 -- 1 is for group

	/*

	DELETE the group language properties

	*/

	DELETE FROM tblGroupLanguage WHERE idGroup IN (SELECT id FROM @Groups)
	
	/*
	
	DELETE the groups
	
	*/
	
	DELETE FROM tblGroup WHERE idGroup IN (SELECT id FROM @Groups)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO