-- =====================================================================
-- PROCEDURE: [Role.SavePermissions]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Role.SavePermissions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Role.SavePermissions]
GO

CREATE PROCEDURE [Role.SavePermissions]
(
	@Return_Code			INT					OUTPUT,
	@Error_Description_Code NVARCHAR(50)		OUTPUT,
	@idCallerSite			INT					= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT					= 0,

	@idRole					INT,
	@Permissions			IDTableWithScope	READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Permissions) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'RoleSavePermissions_NoPermissionsToSave'
		RETURN 1
		END

	/*

	ensure that all scoped group ids belong to the site

	*/

	-- get all scope items into a temporary table
	DECLARE @scopeList TABLE (idScope INT)
	DECLARE @scopeItems NVARCHAR(MAX)
	DECLARE @scopeItem NVARCHAR(10)
	DECLARE @strPos INT

	DECLARE checkScopeListCursor CURSOR LOCAL FOR
		SELECT scope FROM @Permissions WHERE scope IS NOT NULL

	OPEN checkScopeListCursor

	FETCH NEXT FROM checkScopeListCursor INTO @scopeItems

	WHILE @@FETCH_STATUS = 0
		BEGIN

		WHILE CHARINDEX(',', @scopeItems) > 0
			BEGIN

			SELECT @strPos = CHARINDEX(',', @scopeItems)
			SELECT @scopeItem = SUBSTRING(@scopeItems, 1, @strPos - 1)
			INSERT INTO @scopeList SELECT CONVERT(INT, @scopeItem)
			SELECT @scopeItems = SUBSTRING(@scopeItems, @strPos + 1, LEN(@scopeItems) - @strPos)

			END

		INSERT INTO @scopeList SELECT CONVERT(INT, @scopeItems)

		FETCH NEXT FROM checkScopeListCursor INTO @scopeItems

		END

	-- kill the cursor
	CLOSE checkScopeListCursor
	DEALLOCATE checkScopeListCursor

	-- make sure all scoped groups belong to the site
	IF (
		SELECT COUNT(1) 
		FROM @scopeList SL
		LEFT JOIN tblGroup G ON G.idGroup = SL.idScope
		WHERE G.idSite IS NULL
		OR G.idSite <> @idCallerSite
		) > 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'RoleSavePermissions_ScopedGroupNotInPortal'
		RETURN 1
		END
		
	/*

	DELETE role to permission links for permissions not in the table variable

	*/

	DELETE FROM tblRoleToPermissionLink
	WHERE idRole = @idRole
	AND NOT EXISTS (SELECT 1 FROM
					@Permissions PP
					WHERE PP.id = idPermission)

	/*
	
	UPDATE scope for existing role to permission links
	
	*/
	
	UPDATE tblRoleToPermissionLink SET
		scope = PP.scope
	FROM @Permissions PP
	WHERE idRole = @idRole
	AND idPermission = PP.id
	
	/*
	
	INSERT new role to permission links
	
	*/

	INSERT INTO tblRoleToPermissionLink (
		idSite,
		idRole,
		idPermission,
		scope
	)
	SELECT
		@idCallerSite,
		@idRole,
		PP.id,
		PP.scope
	FROM @Permissions PP
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblRoleToPermissionLink RTPL
		WHERE RTPL.idRole = @idRole
		AND RTPL.idPermission = PP.id
	)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	
