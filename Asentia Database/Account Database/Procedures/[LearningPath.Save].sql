-- =====================================================================
-- PROCEDURE: [LearningPath.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.Save]
GO

/*

Adds new learning path or updates existing learning path.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [LearningPath.Save]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, -- default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0, -- will fail if not specified
	
	@idLearningPath					INT				OUTPUT,
	@name							NVARCHAR(255),
	@shortDescription				NVARCHAR(512),
	@longDescription				NVARCHAR(MAX),
	@cost							FLOAT, 
	@isPublished					BIT,
	@isClosed						BIT,
	@selfEnrollmentIsOneTimeOnly	BIT,
	@forceCompletionInOrder			BIT,
	@searchTags						NVARCHAR(512),
	@shortcode						NVARCHAR(10)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'LearningPathSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/

	/*
	
	validate uniqueness for shortcode, if specified

	*/	

	IF (
		SELECT COUNT(1)
		FROM tblLearningPath
		WHERE idSite = @idCallerSite
		AND shortcode = @shortcode
		AND @shortcode IS NOT NULL
		AND @shortcode <> ''
		AND (isDeleted = 0 OR isDeleted IS NULL)
		AND (
			@idLearningPath IS NULL
			OR @idLearningPath <> idLearningPath
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'LearningPathSave_ShortcodeNotUnique'
		RETURN 1 
		END	
		
	/*

	check XSS vulnerabilities

	*/

	-- longDescription

	IF (@longDescription LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LearningPathSave_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LearningPathSave_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LearningPathSave_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'LearningPathSave_DescTagNotAllowed_Iframe'
	RETURN 1 
	END	

	/*
	
	save the data
	
	*/
	
	IF (@idLearningPath = 0 OR @idLearningPath IS NULL)
		
		BEGIN
		
		-- insert the new learning path
		
		INSERT INTO tblLearningPath (
			idSite, 
			name,
			shortDescription,
			longDescription,
			cost, 
			isPublished,
			isClosed,
			selfEnrollmentIsOneTimeOnly,
			forceCompletionInOrder,
			dtCreated,
			dtModified,
			searchTags,
			shortcode
		)			
		VALUES (
			@idCallerSite, 
			@name,
			@shortDescription,
			@longDescription,
			@cost, 
			@isPublished,
			@isClosed,
			@selfEnrollmentIsOneTimeOnly,
			@forceCompletionInOrder,
			GETUTCDATE(),
			GETUTCDATE(),
			@searchTags,
			@shortcode
		)
		
		-- get the new learning path's id
		
		SET @idLearningPath = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the learning path id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblLearningPath WHERE idLearningPath = @idLearningPath AND idSite = @idCallerSite AND dtDeleted IS NULL) < 1	
			BEGIN

			SET @idLearningPath = @idLearningPath
			SET @Return_Code = 1
			SET @Error_Description_Code = 'LearningPathSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing learning path's properties
		
		UPDATE tblLearningPath SET
			name							= @name,
			shortDescription				= @shortDescription,
			longDescription					= @longDescription,
			cost							= @cost, 
			isPublished						= @isPublished,
			isClosed						= @isClosed,
			selfEnrollmentIsOneTimeOnly		= @selfEnrollmentIsOneTimeOnly,
			forceCompletionInOrder			= @forceCompletionInOrder,
			dtModified						= GETUTCDATE(),
			searchTags						= @searchTags,
			shortcode						= @shortcode
		WHERE idLearningPath = @idLearningPath
		
		-- get the learning path's id
		
		SET @idLearningPath = @idLearningPath
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblLearningPathLanguage LPL WHERE LPL.idLearningPath = @idLearningPath AND LPL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblLearningPathLanguage SET
			name = @name,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			searchTags = @searchTags
		WHERE idLearningPath = @idLearningPath
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblLearningPathLanguage (
			idSite,
			idLearningPath,
			idLanguage,
			name,
			shortDescription,
			longDescription,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idLearningPath,
			@idDefaultLanguage,
			@name,
			@shortDescription,
			@longDescription,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblLearningPathLanguage LPL
			WHERE LPL.idLearningPath = @idLearningPath
			AND LPL.idLanguage = @idDefaultLanguage
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO