-- =====================================================================
-- PROCEDURE: [InboxMessage.SentItemsGetGrid]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[InboxMessage.SentItemsGetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [InboxMessage.SentItemsGetGrid]
GO

CREATE PROCEDURE [InboxMessage.SentItemsGetGrid]
(
	@Return_Code			INT						OUTPUT,
	@Error_Description_Code	NVARCHAR(50)			OUTPUT,
	@idCallerSite			INT				= 0, 
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141
		
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblInboxMessage
			WHERE 
				(
				(@idCaller IS NULL) 
				OR 
				(@idCaller IS NOT NULL AND idSender = @idCaller)
				)
				AND
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
				)
				AND (isSenderDeleted IS NULL OR isSenderDeleted = 0)
				AND (isDraft IS NULL OR isDraft=0)
				AND isSent = 1
				
			    	
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idInboxMessage,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'to' THEN displayName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'subject' THEN [subject] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN IM.dtCreated END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('to', 'subject') THEN IM.dtCreated END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN [subject] END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'to' THEN [subject] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'subject' THEN displayName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN displayName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn = 'to' THEN displayName END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn = 'subject' THEN [subject] END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN IM.dtCreated END) END,
							-- SECOND ORDER ASC
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn IN ('to', 'subject') THEN IM.dtCreated END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN [subject] END) END,
							-- THIRD ORDER ASC
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn = 'to' THEN [subject] END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn = 'subject' THEN displayName END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN displayName END) END
						)
						AS [row_number]
					FROM tblInboxMessage IM
					LEFT JOIN tblUser U ON U.idUser = IM.idRecipient
					WHERE
						(
						(@idCaller IS NULL) 
						OR
						(@idCaller IS NOT NULL AND idSender = @idCaller)
						)
						AND
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND IM.idSite = @idCallerSite)
						)
						AND (isSenderDeleted IS NULL OR isSenderDeleted = 0)
						AND (isDraft IS NULL OR isDraft=0)
						AND isSent = 1
				), 
				
				SelectedKeys AS (
					SELECT 
						idInboxMessage, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				IM.idInboxMessage,
				CASE WHEN IM.idRecipient = 1 THEN 'Administrator' ELSE U.displayName END AS [to],
				CASE WHEN idParentInboxMessage is not null THEN 'RE: ' + IM.[subject]  ELSE IM.[subject]  END AS [subject],
				IM.[dtCreated],
				IM.dtSent,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblInboxMessage IM ON IM.idInboxMessage = SelectedKeys.idInboxMessage 
            LEFT JOIN tblUser U ON U.idUser = IM.idRecipient		
            WHERE (IM.isSenderDeleted IS NULL OR IM.isSenderDeleted = 0)
			AND (IM.isDraft IS NULL OR IM.isDraft=0)
			ORDER BY SelectedKeys.[row_number]
			
			END
		
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblInboxMessage
			INNER JOIN CONTAINSTABLE(tblInboxMessage, *, @searchParam) K ON K.[key] = tblInboxMessage.idInboxMessage
			WHERE 
				(
				(@idCaller IS NULL) 
				OR
				(@idCaller IS NOT NULL AND idSender = @idCaller)
				)
				AND
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
				)
				AND (isSenderDeleted IS NULL OR isSenderDeleted = 0)
				AND (isDraft IS NULL OR isDraft=0)
				AND isSent = 1
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idInboxMessage,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'to' THEN displayName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'subject' THEN [subject] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN IM.dtCreated END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('to', 'subject') THEN IM.dtCreated END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN [subject] END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'to' THEN [subject] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'subject' THEN displayName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN displayName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn = 'to' THEN displayName END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn = 'subject' THEN [subject] END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN IM.dtCreated END) END,
							-- SECOND ORDER ASC
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn IN ('to', 'subject') THEN IM.dtCreated END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN [subject] END) END,
							-- THIRD ORDER ASC
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn = 'to' THEN [subject] END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn = 'subject' THEN displayName END) END,
							CASE WHEN (@orderAsc IS NULL OR @orderAsc = 1) THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('to', 'subject') THEN displayName END) END
						)
						AS [row_number]
					FROM tblInboxMessage IM
					LEFT JOIN tblUser U ON U.idUser = IM.idRecipient
					INNER JOIN CONTAINSTABLE(tblInboxMessage, *, @searchParam) K ON K.[key] = IM.idInboxMessage
					WHERE 
						(
						(@idCaller IS NULL) 
						OR
						(@idCaller IS NOT NULL AND idSender = @idCaller)
						)
						AND
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND IM.idSite = @idCallerSite)
						)
						AND (isSenderDeleted IS NULL OR isSenderDeleted = 0)
						AND (isDraft IS NULL OR isDraft=0)
						AND isSent = 1
						
				), 
				
				SelectedKeys AS (
					SELECT 
						idInboxMessage, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
					
				)
			SELECT 
				IM.idInboxMessage,
				CASE WHEN IM.idRecipient = 1 THEN 'Administrator' ELSE U.displayName END AS [to],
				CASE WHEN idParentInboxMessage is not null THEN 'RE: ' + IM.[subject]  ELSE IM.[subject]  END AS [subject],
				IM.[dtCreated],
				IM.dtSent,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblInboxMessage IM ON IM.idInboxMessage = SelectedKeys.idInboxMessage 
			LEFT JOIN tblUser U ON U.idUser = IM.idRecipient 
			WHERE (IM.isSenderDeleted IS NULL OR IM.isSenderDeleted = 0)
			AND (IM.isDraft IS NULL OR IM.isDraft=0)
			AND IM.isSent = 1
			ORDER BY SelectedKeys.[row_number]
			
			END
	END		