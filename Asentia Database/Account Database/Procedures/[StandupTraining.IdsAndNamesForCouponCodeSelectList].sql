-- =====================================================================
-- PROCEDURE: [StandupTraining.IdsAndNamesForCouponCodeSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.IdsAndNamesForCouponCodeSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.IdsAndNamesForCouponCodeSelectList]
GO

/*

Returns a recordset of instructor led training ids and titles to populate the
course select list for a coupon code.

*/

CREATE PROCEDURE [StandupTraining.IdsAndNamesForCouponCodeSelectList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCouponCode			INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT 
			S.idStandupTraining, 
			CASE WHEN SL.title IS NOT NULL THEN SL.title ELSE S.title END AS title
		FROM tblStandupTraining S
		LEFT JOIN tblStandupTrainingLanguage SL ON SL.idStandupTraining = S.idStandupTraining AND SL.idLanguage = @idCallerLanguage
		WHERE S.idSite = @idCallerSite
		AND (S.isDeleted IS NULL OR S.isDeleted = 0)
		AND (S.isStandaloneEnroll = 1) -- must be standalone enroll
		AND NOT EXISTS (SELECT 1 FROM tblCouponCodeToStandupTrainingLink CCTSL
						WHERE CCTSL.idCouponCode = @idCouponCode
						AND CCTSL.idStandupTraining = S.idStandupTraining)
		ORDER BY title

		END
			
	ELSE

		BEGIN

		SELECT DISTINCT
			S.idStandupTraining, 
			CASE WHEN SL.title IS NOT NULL THEN SL.title ELSE S.title END AS title
		FROM tblStandupTrainingLanguage SL
		INNER JOIN CONTAINSTABLE(tblStandupTrainingLanguage, *, @searchParam) K ON K.[key] = SL.idStandUpTrainingLanguage AND SL.idLanguage = @idCallerLanguage
		LEFT JOIN tblStandUpTraining S ON S.idStandUpTraining = SL.idStandUpTraining
		WHERE S.idSite = @idCallerSite
		AND (S.isDeleted IS NULL OR S.isDeleted = 0)
		AND (S.isStandaloneEnroll = 1) -- must be standalone enroll
		AND NOT EXISTS (SELECT 1 FROM tblCouponCodeToStandupTrainingLink CCTSL
						WHERE CCTSL.idCouponCode = @idCouponCode
						AND CCTSL.idStandupTraining = S.idStandupTraining)
		ORDER BY title

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	