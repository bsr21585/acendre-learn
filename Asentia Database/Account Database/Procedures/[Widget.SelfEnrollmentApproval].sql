-- =====================================================================
-- PROCEDURE: [Widget.SelfEnrollmentApproval]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Widget.SelfEnrollmentApproval]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Widget.SelfEnrollmentApproval]
GO

/*

Gets a listing of self enrollments that need to be approved.

*/

CREATE PROCEDURE [Widget.SelfEnrollmentApproval]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT,
	
	-- NOTE THAT WE ARE NOT USING THE SEARCH, PAGINATION, OR SORT HERE BUT
	-- SINCE THESE ARE COMMON PARAMETERS FOR GRIDS, WE NEED TO KEEP THEM
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	begin getting the grid data

	*/

	CREATE TABLE #SelfEnrollmentApprovals (
		idEnrollmentRequest		INT,
		displayName				NVARCHAR(768),
		course					NVARCHAR(255),
	)

	-- get the grid data

	DECLARE @globalPermissions INT

	-- GLOBAL SELF ENROLLMENT PERMISSION

	SELECT 
		@globalPermissions = COUNT(RPL.idRoleToPermissionLink)
	FROM tblRoleToPermissionLink RPL 
	LEFT JOIN tblUserToRoleLink URL ON RPL.idRole = URL.idRole
	WHERE RPL.idPermission = 310
	AND (URL.idUser = @idCaller OR @idCaller = 1)	

	IF @globalPermissions > 0

		BEGIN
		
		INSERT INTO #SelfEnrollmentApprovals (
			idEnrollmentRequest,
			displayName,
			course
		)
		SELECT
			ER.idEnrollmentRequest,
			U.displayName,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblEnrollmentRequest ER
		LEFT JOIN tblUser U ON ER.idUser = U.idUser
		LEFT JOIN tblCourse C ON ER.idCourse = C.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		WHERE ER.idSite = @idCallerSite
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
		AND (U.isRegistrationApproved = 1 OR U.isRegistrationApproved IS NULL)
		AND ER.dtApproved IS NULL 
		AND ER.dtDenied IS NULL

		END

	ELSE

		BEGIN

		-- APPROVERS APPROVE
		
		INSERT INTO #SelfEnrollmentApprovals (
			idEnrollmentRequest,
			displayName,
			course
		)
		SELECT
			ER.idEnrollmentRequest,
			U.displayName,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblEnrollmentRequest ER
		LEFT JOIN tblUser U ON ER.idUser = U.idUser
		LEFT JOIN tblCourse C ON ER.idCourse = C.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCourseEnrollmentApprover CEA ON CEA.idCourse = ER.idCourse
		WHERE ER.idSite = @idCallerSite
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
		AND (U.isRegistrationApproved = 1 OR U.isRegistrationApproved IS NULL)
		AND ER.dtApproved IS NULL 
		AND ER.dtDenied IS NULL 
		AND CEA.idUser = @idCaller
				
		UNION
				
		-- SUPERVISORS APPROVE
	
		SELECT
			ER.idEnrollmentRequest,
			U.displayName,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblEnrollmentRequest ER
		LEFT JOIN tblUser U ON ER.idUser = U.idUser
		LEFT JOIN tblCourse C ON ER.idCourse = C.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblUserToSupervisorLink USL ON USL.idUser = ER.idUser
		WHERE ER.idSite = @idCallerSite
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
		AND (U.isRegistrationApproved = 1 OR U.isRegistrationApproved IS NULL)
		AND ER.dtApproved IS NULL 
		AND ER.dtDenied IS NULL 
		AND C.isApprovalAllowedByUserSupervisor = 1 
		AND USL.idSupervisor = @idCaller

		UNION

		-- EXPERTS APPROVE
	
		SELECT
			ER.idEnrollmentRequest,
			U.displayName,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblEnrollmentRequest ER
		LEFT JOIN tblUser U ON ER.idUser = U.idUser
		LEFT JOIN tblCourse C ON ER.idCourse = C.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCourseExpert CE ON CE.idCourse = ER.idCourse
		WHERE ER.idSite = @idCallerSite
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
		AND (U.isRegistrationApproved = 1 OR U.isRegistrationApproved IS NULL)
		AND ER.dtApproved IS NULL 
		AND ER.dtDenied IS NULL 
		AND C.isApprovalAllowedByCourseExperts = 1 
		AND CE.idUser = @idCaller
		
		END

	-- return the rowcount and grid data

	SELECT COUNT(1) AS row_count
	FROM #SelfEnrollmentApprovals

	;WITH
		Keys AS (
			SELECT TOP (@pageNum * @pageSize)
				SEA.idEnrollmentRequest,
				ROW_NUMBER() OVER (ORDER BY SEA.displayName DESC)
				AS [row_number]
			FROM #SelfEnrollmentApprovals SEA
		),

		SelectedKeys AS (
			SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
				idEnrollmentRequest, 
				[row_number]
			FROM Keys
			WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
		)
	SELECT
		SEA.idEnrollmentRequest,
		SEA.displayName,
		SEA.course
	FROM SelectedKeys 
	LEFT JOIN #SelfEnrollmentApprovals SEA ON SEA.idEnrollmentRequest = SelectedKeys.idEnrollmentRequest
	ORDER BY SelectedKeys.[row_number]

	-- drop temporary tables
	DROP TABLE #SelfEnrollmentApprovals

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO