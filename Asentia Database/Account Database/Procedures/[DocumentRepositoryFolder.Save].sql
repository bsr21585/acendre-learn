-- =====================================================================
-- PROCEDURE: [DocumentRepositoryFolder.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DocumentRepositoryFolder.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DocumentRepositoryFolder.Save]
GO

/*

Adds a new document repository folder or updates an existing one.

*/

CREATE PROCEDURE [DocumentRepositoryFolder.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR (10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idDocumentRepositoryFolder			INT				OUTPUT,
	@idDocumentRepositoryObjectType		INT,
	@idObject							INT,
	@folderName							NVARCHAR(255)
	
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate uniqueness within same parent object
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDocumentRepositoryFolder
		WHERE (isDeleted IS NULL OR isDeleted = 0) -- ignore deleted records
		AND idSite = @idCallerSite
		AND idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
		AND idObject = @idObject
		AND [Name] = @folderName
		AND (
			@idDocumentRepositoryFolder IS NULL
			OR @idDocumentRepositoryFolder <> idDocumentRepositoryFolder
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'DocumentRepositoryFolderSave_FieldNotUnique'
		RETURN 1 
		END

	
	/*
	
	save the data
	
	*/
	
	IF (@idDocumentRepositoryFolder = 0 OR @idDocumentRepositoryFolder IS NULL)
		
		BEGIN
		
		-- insert the new document repository folder
		
		INSERT INTO tblDocumentRepositoryFolder (
			idSite,
			idDocumentRepositoryObjectType,
			idObject,
			[Name]
		)
		VALUES (
			@idCallerSite,
			@idDocumentRepositoryObjectType,
			@idObject,
			@folderName		
		)
		
		-- get the new id

		SELECT @idDocumentRepositoryFolder = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the document repository folder id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblDocumentRepositoryFolder WHERE idDocumentRepositoryFolder = @idDocumentRepositoryFolder AND idSite = @idCallerSite AND (isDeleted IS NULL OR isDeleted = 0)) < 1	
			BEGIN

			SET @idDocumentRepositoryFolder = @idDocumentRepositoryFolder
			SET @Return_Code = 1
			SET @Error_Description_Code = 'DocumentRepositoryFolderSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing document repository folder's properties
		
		UPDATE tblDocumentRepositoryFolder SET
			name = @folderName
		WHERE idDocumentRepositoryFolder = @idDocumentRepositoryFolder
		
		-- get the document repository folder's id

		SELECT @idDocumentRepositoryFolder = @idDocumentRepositoryFolder
				
		END

	SET @Error_Description_Code = ''
	SET @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
