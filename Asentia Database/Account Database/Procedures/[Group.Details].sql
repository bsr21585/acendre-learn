-- =====================================================================
-- PROCEDURE: [Group.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Group.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.Details]
GO

/*

Return all the properties for a given group id.

*/
CREATE PROCEDURE [Group.Details]
(
	@Return_Code			INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)		OUTPUT,
	@idCallerSite			INT					= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT					= 0,
	
	@idGroup				INT					OUTPUT, 
	@idSite					INT					OUTPUT, 
	@name					NVARCHAR(255)		OUTPUT,
	@primaryGroupToken		NVARCHAR(255)		OUTPUT,		
	@objectGUID				NVARCHAR(36)		OUTPUT,	
	@distinguishedName		NVARCHAR(255)		OUTPUT,
	@avatar					NVARCHAR(255)		OUTPUT,
	@shortDescription		NVARCHAR(512)		OUTPUT,
	@longDescription		NVARCHAR(max)		OUTPUT,
	@isSelfJoinAllowed		BIT					OUTPUT,
	@isFeedActive           BIT                 OUTPUT,
	@isFeedModerated        BIT                 OUTPUT,
	@membershipIsPublicized	BIT                 OUTPUT,
	@isCallingUserAMember	BIT					OUTPUT,
	@searchTags				NVARCHAR(512)		OUTPUT,
	@numberOfMembers		INT					OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblGroup
		WHERE idGroup = @idGroup
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupDetails_NoRecordFound'
		RETURN 1
		END
	
	/*
	
	get the data
	
	*/
	
	SELECT
		@idGroup				= G.idGroup,
		@idSite					= G.idSite,
		@name					= G.name,
		@primaryGroupToken		= G.primaryGroupToken,
		@objectGUID				= G.objectGUID,	
		@distinguishedName		= G.distinguishedName,
		@avatar					= G.avatar,
		@shortDescription		= G.shortDescription,
		@longDescription		= G.longDescription,
		@isSelfJoinAllowed		= G.isSelfJoinAllowed,
		@isFeedActive			= G.IsFeedActive,
		@isFeedModerated		= G.IsFeedModerated,
		@membershipIsPublicized	= G.membershipIsPublicized,
		@searchTags				= G.searchTags
	FROM tblGroup G
	WHERE G.idGroup = @idGroup
	AND G.idSite = @idCallerSite

	-- determine if the calling user is a member of the group
	IF (
		SELECT COUNT(1) 
		FROM tblUserToGroupLink UGL
		WHERE UGL.idGroup = @idGroup
		AND UGL.idUser = @idCaller) > 0
		BEGIN
		SET @isCallingUserAMember = 1
		END
	ELSE
		BEGIN
		SET @isCallingUserAMember = 0
		END

	-- get the number of group members
	SELECT @numberOfMembers = COUNT(DISTINCT idUser) FROM tblUserToGroupLink WHERE idGroup = @idGroup

	-- return
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GroupDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO