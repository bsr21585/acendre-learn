-- =====================================================================
-- PROCEDURE: [User.MergeUserAccounts]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.MergeUserAccounts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.MergeUserAccounts]
GO

/*

Merges a source user account into a destination user account

*/
CREATE PROCEDURE [User.MergeUserAccounts] 
	
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idUserSource			INT,
	@idUserDestination		INT
AS

	BEGIN	
	SET NOCOUNT ON

	/*

	re-link all objects linked to source user to destination user where duplicates will not be created

	note the following exclusions:

	- source user profile information is NOT copied to destination user
	- source user roles are NOT linked to destination user
	- anything inherited by ruleset will NOT be copied to the destination user
	- additional tables not copied:
		- tblEventEmailQueue
		- tblEventLog
		- tblExceptionLog
		- tblReportSubscriptionQueue
		- tblSSOToken

	*/

	-- ACTIVITY IMPORTS
	UPDATE tblActivityImport SET idUser = @idUserDestination WHERE idUser = @idUserSource
	UPDATE tblActivityImport SET idUserImported = @idUserDestination WHERE idUserImported = @idUserSource

	-- CERTIFICATE IMPORTS
	UPDATE tblCertificateImport SET idUser = @idUserDestination WHERE idUser = @idUserSource
	UPDATE tblCertificateImport SET idUserImported = @idUserDestination WHERE idUserImported = @idUserSource

	-- CERTIFICATE RECORDS
	UPDATE tblCertificateRecord SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idCertificate NOT IN (
		SELECT idCertificate FROM tblCertificateRecord WHERE idUser = @idUserDestination)

	UPDATE tblCertificateRecord SET idAwardedBy = @idUserDestination WHERE idAwardedBy = @idUserSource

	-- CERTIFICATION TO USER LINKS
	UPDATE tblCertificationToUserLink SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idCertification NOT IN (
		SELECT idCertification FROM tblCertificationToUserLink WHERE idUser = @idUserDestination)

	-- COURSE ENROLLMENT APPROVERS
	UPDATE tblCourseApprover SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idCourse NOT IN (
		SELECT idCourse FROM tblCourseApprover WHERE idUser = @idUserDestination) -- TABLE PENDING DELETION?

	UPDATE tblCourseEnrollmentApprover SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idCourse NOT IN (
		SELECT idCourse FROM tblCourseEnrollmentApprover WHERE idUser = @idUserDestination)

	-- GROUP ENROLLMENT APPROVERS
	UPDATE tblGroupEnrollmentApprover SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idGroup NOT IN (
		SELECT idGroup FROM tblGroupEnrollmentApprover WHERE idUser = @idUserDestination) -- TABLE PENDING DELETION?

	-- LEARNING PATH ENROLLMENT APPROVERS
	UPDATE tblLearningPathApprover SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idLearningPath NOT IN (
		SELECT idLearningPath FROM tblLearningPathApprover WHERE idUser = @idUserDestination) -- TABLE PENDING DELETION?

	UPDATE tblLearningPathEnrollmentApprover SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idLearningPath NOT IN (
		SELECT idLearningPath FROM tblLearningPathEnrollmentApprover WHERE idUser = @idUserDestination) -- TABLE PENDING DELETION?

	-- COURSE EXPERTS
	UPDATE tblCourseExpert SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idCourse NOT IN (
		SELECT idCourse FROM tblCourseExpert WHERE idUser = @idUserDestination)

	-- COURSE RATINGS
	UPDATE tblCourseRating SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idCourse NOT IN (
		SELECT idCourse FROM tblCourseRating WHERE idUser = @idUserDestination)

	-- COURSE FEED MESSAGES
	UPDATE tblCourseFeedMessage SET idAuthor = @idUserDestination WHERE idAuthor = @idUserSource

	UPDATE tblCourseFeedMessage SET idApprover = @idUserDestination WHERE idApprover = @idUserSource

	-- COURSE FEED MODERATORS
	UPDATE tblCourseFeedModerator SET idModerator = @idUserDestination WHERE idModerator = @idUserSource AND idCourse NOT IN (
		SELECT idCourse FROM tblCourseFeedModerator WHERE idModerator = @idUserDestination)

	-- GROUP FEED MESSAGES
	UPDATE tblGroupFeedMessage SET idAuthor = @idUserDestination WHERE idAuthor = @idUserSource

	UPDATE tblGroupFeedMessage SET idApprover = @idUserDestination WHERE idApprover = @idUserSource

	-- GROUP FEED MODERATORS
	UPDATE tblGroupFeedModerator SET idModerator = @idUserDestination WHERE idModerator = @idUserSource AND idGroup NOT IN (
		SELECT idGroup FROM tblGroupFeedModerator WHERE idModerator = @idUserDestination)

	-- LEARNING PATH FEED MESSAGES
	UPDATE tblLearningPathFeedMessage SET idAuthor = @idUserDestination WHERE idAuthor = @idUserSource -- TABLE PENDING DELETION?

	UPDATE tblLearningPathFeedMessage SET idApprover = @idUserDestination WHERE idApprover = @idUserSource

	-- LEARNING PATH FEED MODERATORS
	UPDATE tblLearningPathFeedModerator SET idModerator = @idUserDestination WHERE idModerator = @idUserSource AND idLearningPathFeed NOT IN (
		SELECT idLearningPathFeed FROM tblLearningPathFeedModerator WHERE idModerator = @idUserDestination) -- TABLE PENDING DELETION?

	-- CERTIFICATION MODULE REQUIREMENT DATA
	UPDATE [tblData-CertificationModuleRequirement] SET idApprover = @idUserDestination WHERE idApprover = @idUserSource

	-- HOMEWORK ASSIGNMENT DATA
	UPDATE [tblData-HomeworkAssignment] SET proctoringUser = @idUserDestination WHERE proctoringUser = @idUserSource

	-- SCO (OJT) DATA
	UPDATE [tblData-SCO] SET proctoringUser = @idUserDestination WHERE proctoringUser = @idUserSource

	-- ENROLLMENT REQUESTS
	UPDATE tblEnrollmentRequest SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idCourse NOT IN (
		SELECT idCourse FROM tblEnrollmentRequest WHERE idUser = @idUserDestination)

	UPDATE tblEnrollmentRequest SET idApprover = @idUserDestination WHERE idApprover = @idUserSource

	-- REPORTS
	UPDATE tblReport SET idUser = @idUserDestination WHERE idUser = @idUserSource

	-- REPORT FILES
	UPDATE tblReportFile SET idUser = @idUserDestination WHERE idUser = @idUserSource

	-- REPORT SUBSCRIPTIONS
	UPDATE tblReportSubscription SET idUser = @idUserDestination WHERE idUser = @idUserSource

	-- REPORT SUBSCRIPTION WIDGET LINKS
	UPDATE tblReportToReportShortcutsWidgetLink SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idReport NOT IN (
		SELECT idReport FROM tblReportToReportShortcutsWidgetLink WHERE idUser = @idUserDestination)	

	-- INBOX MESSAGES
	UPDATE tblInboxMessage SET idRecipient = @idUserDestination WHERE idRecipient = @idUserSource

	UPDATE tblInboxMessage SET idSender = @idUserDestination WHERE idSender = @idUserSource

	-- QUIZZES AND SURVEYS
	UPDATE tblQuizSurvey SET idAuthor = @idUserDestination WHERE idAuthor = @idUserSource

	-- USER TO COURSE FEED SUBSCRIPTIONS
	UPDATE tblUserToCourseFeedSubscription SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idCourse NOT IN (
		SELECT idCourse FROM tblUserToCourseFeedSubscription WHERE idUser = @idUserDestination)

	-- USER TO LEARNING PATH LINKS
	UPDATE tblUserToLearningPathLink SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idLearningPath NOT IN (
		SELECT idLearningPath FROm tblUserToLearningPathLink WHERE idUser = @idUserDestination) -- TABLE PENDING DELETION?

	-- USER TO RULESET ENROLLMENT LINKS
	UPDATE tblUserToRuleSetEnrollmentLink SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idCourse NOT IN (
		SELECT idCourse FROm tblUserToRuleSetEnrollmentLink WHERE idUser = @idUserDestination) -- TABLE PENDING DELETION?
		
	-- WEB MEETING ORGANIZERS
	UPDATE tblWebMeetingOrganizer SET idUser = @idUserDestination WHERE idUser = @idUserSource

	-- USER SUPERVISOR LINKS
	UPDATE tblUserToSupervisorLink SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idSupervisor <> @idUserDestination

	UPDATE tblUserToSupervisorLink SET idSupervisor = @idUserDestination WHERE idSupervisor = @idUserSource AND idUser <> @idUserDestination

	-- ILT TO INSTRUCTOR LINKS
	UPDATE tblStandupTrainingInstanceToInstructorLink SET idInstructor = @idUserDestination WHERE idInstructor = @idUserSource AND idStandupTrainingInstance NOT IN (
		SELECT idStandupTrainingInstance FROM tblStandupTrainingInstanceToInstructorLink WHERE idInstructor = @idUserDestination)

	-- USER APPROVALS
	UPDATE tblUser SET idApprover = @idUserDestination WHERE idApprover = @idUserSource
	
	-- USER TO GROUP LINKS
	UPDATE tblUserToGroupLink SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idRuleSet IS NULL AND idGroup NOT IN (
		SELECT idGroup FROM tblUserToGroupLink WHERE idUser = @idUserDestination)

	-- ILT TO USER LINKS
	UPDATE tblStandupTrainingInstanceToUserLink SET idUser = @idUserDestination WHERE idUser = @idUserSource AND idStandupTrainingInstance NOT IN (
		SELECT idStandupTrainingInstance FROM tblStandupTrainingInstanceToUserLink WHERE idUser = @idUserDestination)

	-- LEARNING PATH ENROLLMENTS

	-- completed learning path enrollments detached from rulesets
	UPDATE tblLearningPathEnrollment SET
		idUser = @idUserDestination,
		idRuleSetLearningPathEnrollment = NULL
	WHERE idUser = @idUserSource
	AND dtCompleted IS NOT NULL

	-- all active learning path enrollment records, detached from any ruleset enrollment inheritance, and
	-- where there is not already an active enrollment of the same learning path for the destination account
	UPDATE LPE SET
		LPE.idUser = @idUserDestination,
		LPE.idRuleSetLearningPathEnrollment = NULL
	FROM tblLearningPathEnrollment LPE
	WHERE LPE.dtCompleted IS NULL
	AND LPE.idUser = @idUserSource
	AND LPE.idLearningPath NOT IN (SELECT idLearningPath
								   FROM tblLearningPathEnrollment
								   WHERE idUser = @idUserDestination
								   AND dtCompleted IS NULL)

	-- COURSE ENROLLMENTS

	-- completed course enrollments detached from rulesets, groups, and learning path enrollments
	UPDATE tblEnrollment SET
		idUser = @idUserDestination,
		idGroupEnrollment = NULL,
		idRuleSetEnrollment = NULL,
		idLearningPathEnrollment = NULL
	WHERE idUser = @idUserSource
	AND dtCompleted IS NOT NULL

	-- all active enrollment records, detached from any ruleset or group enrollment inheritance, 
	-- and where there is not already an active enrollment of the same course for the destination account
	UPDATE E SET
		E.idUser = @idUserDestination,
		E.idRuleSetEnrollment = NULL,
		E.idGroupEnrollment = NULL,
		E.idLearningPathEnrollment = NULL
	FROM tblEnrollment E
	WHERE E.dtCompleted IS NULL 
	AND E.idUser = @idUserSource
	AND E.idCourse NOT IN (SELECT idCourse
						   FROM tblEnrollment
						   WHERE idUser = @idUserDestination
						   AND dtCompleted IS NULL						   
						   AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
						   AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL))

	-- PURCHASES
	UPDATE tblPurchase SET idUser = @idUserDestination WHERE idUser = @idUserSource

	-- TRANSACTION ITEMS
	UPDATE tblTransactionItem SET idUser = @idUserDestination WHERE idUser = @idUserSource

	UPDATE tblTransactionItem SET idAssigner = @idUserDestination WHERE idUser = @idUserSource

	-- TRANSACTION RESPONSES
	UPDATE tblTransactionResponse SET idUser = @idUserDestination WHERE idUser = @idUserSource

	-- USER PROFILE FILES
	UPDATE tblDocumentRepositoryItem SET idObject = @idUserDestination WHERE idObject = @idUserSource AND idDocumentRepositoryObjectType = 4

	/*

	hard delete the source user

	*/

	DECLARE @Users IDTable

	INSERT INTO @Users (id) VALUES (@idUserSource)

	EXEC [User.HardDelete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @Users

	/*

	return

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	