-- =====================================================================
-- PROCEDURE: [LearningPathEnrollment.Delete]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPathEnrollment.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPathEnrollment.Delete]
GO

/*

Deletes learning path enrollments

*/

CREATE PROCEDURE [LearningPathEnrollment.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@LearningPathEnrollments	IDTable			READONLY,
	@writeToEventLog			BIT				= 1
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @LearningPathEnrollments) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'LearningPathEnrollmentDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	DO NOT validate that all enrollments exist within the site as
	this may be called indirectly from automated "job" procedures
	
	*/
		
	/*
	
	Validate if any of the learning path enrollments have already been completed.
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLearningPathEnrollment LPE
		WHERE LPE.dtCompleted IS NOT NULL 
		AND LPE.idLearningPathEnrollment IN (
			SELECT LPELPE.id 
			FROM @LearningPathEnrollments LPELPE
			)
		) > 0

		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = 'LearningPathEnrollmentDelete_EnrollmentCompleted'
		RETURN 1 
		END
	
	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*
	
	delete the enrollments and all dependent referenced table records
	
	*/

	-- remove the learning path enrollment link from completed course enrollments directly inherited
	-- by the learning path enrollment
	UPDATE tblEnrollment SET
		idLearningPathEnrollment = NULL
	WHERE idLearningPathEnrollment IN (SELECT id FROM @LearningPathEnrollments)
	AND dtCompleted IS NOT NULL

	-- remove the learning path enrollment link from non-completed course enrollments where the course
	-- is part of another active learning path enrollment	
	DECLARE @LearningPathEnrollmentUsers IDTable

	INSERT INTO @LearningPathEnrollmentUsers (id) SELECT DISTINCT idUser FROM tblLearningPathEnrollment WHERE idLearningPathEnrollment IN (SELECT id FROM @LearningPathEnrollments)

	UPDATE tblEnrollment SET
		idLearningPathEnrollment = NULL
	WHERE idEnrollment IN (SELECT E.idEnrollment
						   FROM tblEnrollment E
						   WHERE E.idLearningPathEnrollment IN (SELECT id FROM @LearningPathEnrollments)
						   AND E.idCourse IN (SELECT DISTINCT idCourse
											  FROM tblLearningPathEnrollment LPE
											  LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
											  LEFT JOIN tblLearningPathToCourseLink LPCL ON LPCL.idLearningPath = LP.idLearningPath
											  WHERE LPE.idUser IN (SELECT id FROM @LearningPathEnrollmentUsers)
											  AND LPE.idLearningPathEnrollment NOT IN (SELECT id FROM @LearningPathEnrollments)
											  AND (LPE.dtExpiresFromStart > GETUTCDATE() OR LPE.dtExpiresFromStart IS NULL)
											  AND (LPE.dtExpiresFromFirstLaunch > GETUTCDATE() OR LPE.dtExpiresFromFirstLaunch IS NULL))
						   AND E.idLearningPathEnrollment IS NOT NULL
						   AND E.dtCompleted IS NULL)	

	-- delete incomplete course enrollments directly inherited by the learning path enrollment
	DECLARE @CourseEnrollmentsToDelete IDTable

	INSERT INTO @CourseEnrollmentsToDelete (
		id
	)
	SELECT
		E.idEnrollment
	FROM tblEnrollment E
	WHERE E.idLearningPathEnrollment IN (SELECT id FROM @LearningPathEnrollments)
	AND E.dtCompleted IS NULL

	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @CourseEnrollmentsToDelete, @writeToEventLog

	/*

	log the event log entries prior to actually deleting the learning path enrollments so that we can log the information

	*/

	IF (@writeToEventLog = 1)
		BEGIN
		
		DECLARE @eventLogItems EventLogItemObjects

		INSERT INTO @eventLogItems (
			idSite,
			idObject, 
			idObjectRelated, 
			idObjectUser
		) 
		SELECT 
			LPE.idSite,
			LPE.idLearningPath,
			LPELPE.id, 
			LPE.idUser
		FROM @LearningPathEnrollments LPELPE
		LEFT JOIN tblLearningPathEnrollment LPE ON LPE.idLearningPathEnrollment = LPELPE.id

		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 503, @utcNow, @eventLogItems

		END

	-- delete learning path enrollments
	DELETE FROM tblLearningPathEnrollment
	WHERE idLearningPathEnrollment IN (SELECT id FROM @LearningPathEnrollments)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO