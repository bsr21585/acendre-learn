-- =====================================================================
-- PROCEDURE: [Calendar.GetItemsForResource]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Calendar.GetItemsForResource]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Calendar.GetItemsForResource]
GO

/*

Returns a listing of items coming up on the calendar.

*/

CREATE PROCEDURE [Calendar.GetItemsForResource]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idResource             INT
)
AS
	
	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate caller permission
	
	*/
	
	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	get the user's language
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	/* 
	
	get the calendar items

	*/

	SELECT
		'resourceBooked' AS [event],
		RO.dtStart,RO.dtEnd,
		RO.isOutsideUse,RO.isMaintenance
	FROM tblResourceToObjectLink RO
	LEFT JOIN tblResource R ON R.idResource = RO.idResource
	LEFT JOIN tblResourceLanguage RL ON RL.idResource = R.idResource AND RL.idLanguage = @idCallerLanguage
	WHERE RO.idResource = @idResource 
	OR RO.idResource=(select idParentResource from tblResource where idResource=@idResource)
	-- UNION SELECT OTHER OBJECTS

	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
