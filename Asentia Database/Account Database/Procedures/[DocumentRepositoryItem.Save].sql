-- =====================================================================
-- PROCEDURE: [DocumentRepositoryItem.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DocumentRepositoryItem.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DocumentRepositoryItem.Save]
GO

/*

Adds a new document repository item or updates an existing one.

*/

CREATE PROCEDURE [DocumentRepositoryItem.Save]
(
		@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR (10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idDocumentRepositoryItem			INT				OUTPUT,
	@idDocumentRepositoryObjectType		INT,
	@idObject							INT,
	@idDocumentRepositoryFolder			INT,
	@idOwner							INT,
	@label								NVARCHAR(255),
	@fileName							NVARCHAR(255),
	@kb									INT,
	@searchTags							NVARCHAR(512),
	@isPrivate							BIT,
	@languageString						NVARCHAR(10),
	@isAllLanguages						BIT,
	@isVisibleToUser					BIT				= NULL,		-- only necessary for profile files
	@isUploadedByUser					BIT				= NULL		-- only necessary for profile files
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate uniqueness within same parent object
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDocumentRepositoryItem
		WHERE (isDeleted IS NULL OR isDeleted = 0) -- ignore deleted records
		AND idSite = @idCallerSite
		AND idDocumentRepositoryObjectType = @idDocumentRepositoryObjectType
		AND idObject = @idObject
		AND label = @label
		AND (
			@idDocumentRepositoryItem IS NULL
			OR @idDocumentRepositoryItem <> idDocumentRepositoryItem
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'DocumentRepositoryItemSave_FieldNotUnique'
		RETURN 1 
		END

	/*

	get the language id from the language string parameter

	*/

	DECLARE @idLanguage INT
	SELECT @idLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @languageString

	/*
	
	save the data
	
	*/
	
	IF (@idDocumentRepositoryItem = 0 OR @idDocumentRepositoryItem IS NULL)
		
		BEGIN
		
		-- insert the new document repository item
		
		INSERT INTO tblDocumentRepositoryItem (
			idSite,
			idDocumentRepositoryObjectType,
			idObject,
			idDocumentRepositoryFolder,
			idOwner,
			label,
			[fileName],
			kb,
			searchTags,
			isPrivate,
			idLanguage,
			isAllLanguages,
			isVisibleToUser,
			isUploadedByUser,
			dtCreated
		)
		VALUES (
			@idCallerSite,
			@idDocumentRepositoryObjectType,
			@idObject,
			@idDocumentRepositoryFolder,
			@idOwner,
			@label,
			@fileName,
			@kb,
			@searchTags,
			@isPrivate,
			@idLanguage,
			@isAllLanguages,
			@isVisibleToUser,
			@isUploadedByUser,
			GETUTCDATE()
		)
		
		-- get the new id

		SELECT @idDocumentRepositoryItem = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the document repository item id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblDocumentRepositoryItem WHERE idDocumentRepositoryItem = @idDocumentRepositoryItem AND idSite = @idCallerSite AND (isDeleted IS NULL OR isDeleted = 0)) < 1	
			BEGIN

			SET @idDocumentRepositoryItem = @idDocumentRepositoryItem
			SET @Return_Code = 1
			SET @Error_Description_Code = 'DocumentRepositoryItemSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing document repository item's properties
		
		UPDATE tblDocumentRepositoryItem SET
			label	=	@label,
			[fileName] = @filename,
			idDocumentRepositoryFolder = @idDocumentRepositoryFolder,
			searchTags = @searchTags,
			isPrivate = @isPrivate,
			idLanguage = @idLanguage,
			isAllLanguages = @isAllLanguages,
			isVisibleToUser = @isVisibleToUser,
			isUploadedByUser = @isUploadedByUser
		WHERE idDocumentRepositoryItem = @idDocumentRepositoryItem
		
		-- get the document repository item's id

		SELECT @idDocumentRepositoryItem = @idDocumentRepositoryItem
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblDocumentRepositoryItemLanguage DRIL WHERE DRIL.idDocumentRepositoryItem = @idDocumentRepositoryItem AND DRIL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblDocumentRepositoryItemLanguage SET
			label = @label,
			searchTags = @searchTags
		WHERE idDocumentRepositoryItem = @idDocumentRepositoryItem
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblDocumentRepositoryItemLanguage (
			idSite,
			idDocumentRepositoryItem,
			idLanguage,
			label,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idDocumentRepositoryItem,
			@idDefaultLanguage,
			@label,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblDocumentRepositoryItemLanguage DRIL
			WHERE DRIL.idDocumentRepositoryItem = @idDocumentRepositoryItem
			AND DRIL.idLanguage = @idDefaultLanguage
		)

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
