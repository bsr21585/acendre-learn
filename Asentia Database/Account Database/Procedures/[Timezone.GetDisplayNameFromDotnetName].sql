-- =====================================================================
-- PROCEDURE: [Timezone.GetDisplayNameFromDotnetName]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Timezone.GetDisplayNameFromDotnetName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Timezone.GetDisplayNameFromDotnetName]
GO

/*

Gets the timezone display name from dotnet name

*/

CREATE PROCEDURE [Timezone.GetDisplayNameFromDotnetName]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@dotNetName				NVARCHAR(255),
	@displayName			NVARCHAR(255)   OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON

	/*
	 Gets the timezone display name from dotnet name

	*/

	SELECT  DISTINCT
	       @displayName = displayName FROM tblTimezone
		   WHERE dotNetName = @dotNetName
		
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	