-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.Details]
GO

/*

Return all the properties for a given standup training instance id.

*/
CREATE PROCEDURE [StandupTrainingInstance.Details]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT				OUTPUT,
	@idStandupTraining			INT				OUTPUT,
	@idSite						INT				OUTPUT, 
	@title						NVARCHAR(255)	OUTPUT,
	@description				NVARCHAR(MAX)	OUTPUT,
	@seats						INT				OUTPUT,
	@seatsRemaining				INT				OUTPUT,
	@waitingSeats				INT				OUTPUT,
	@waitingSeatsRemaining		INT				OUTPUT,
	@type						INT				OUTPUT,
	@isClosed					BIT			    OUTPUT,
	@urlRegistration			NVARCHAR(255)	OUTPUT,
	@urlAttend					NVARCHAR(255)	OUTPUT,
	@city						NVARCHAR(255)	OUTPUT,
	@province					NVARCHAR(255)	OUTPUT,
	@postalcode					NVARCHAR(25)	OUTPUT,
	@locationDescription		NVARCHAR(MAX)	OUTPUT,
	@isDeleted					BIT				OUTPUT,
	@dtDeleted					DATETIME		OUTPUT,
	@integratedObjectKey		BIGINT			OUTPUT,
	@hostUrl					NVARCHAR(1024)	OUTPUT,
	@genericJoinUrl				NVARCHAR(1024)	OUTPUT,
	@meetingPassword			NVARCHAR(255)	OUTPUT,
	@idWebMeetingOrganizer		INT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblStandupTrainingInstance
		WHERE idStandUpTrainingInstance = @idStandupTrainingInstance
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'StandupTrainingInstanceDetails_NoRecordFound'
		RETURN 1
	    END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idStandupTrainingInstance	= STI.idStandupTrainingInstance,
		@idStandupTraining			= STI.idStandupTraining,
		@idSite						= STI.idSite,
		@title						= STI.title,
		@description				= STI.[description],
		@seats						= STI.seats,
		@seatsRemaining				= STI.seats - (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND isWaitingList = 0),
		@waitingSeats				= STI.waitingSeats,
		@waitingSeatsRemaining		= STI.waitingSeats - (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND isWaitingList = 1),
		@type						= STI.[type],
		@isClosed					= STI.isClosed,
		@urlRegistration			= STI.urlRegistration,
		@urlAttend					= STI.urlAttend,
		@city						= STI.city,
		@province					= STI.province,
		@postalcode					= STI.postalcode,
		@locationDescription		= STI.locationDescription,
		@isDeleted					= STI.isDeleted,
		@dtDeleted					= STI.dtDeleted,
		@integratedObjectKey		= STI.integratedObjectKey,
		@hostUrl					= STI.hostUrl,
		@genericJoinUrl				= STI.genericJoinUrl,
		@meetingPassword			= STI.meetingPassword,
		@idWebMeetingOrganizer		= STI.idWebMeetingOrganizer
	FROM tblStandupTrainingInstance STI
	WHERE STI.idStandUpTrainingInstance = @idStandupTrainingInstance
	AND STI.idSite = @idCallerSite
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'StandupTrainingInstanceDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO