-- =====================================================================
-- PROCEDURE: [User.IdsForAPI]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.IdsForAPI]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.IdsForAPI]
GO

/*

Returns a recordset of user ids to populate a list with no exclusions for a specific object in API.

*/
CREATE PROCEDURE [User.IdsForAPI]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@dataType				NVARCHAR(20),
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	


	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @dataType = 'username'
			
		BEGIN

		SELECT DISTINCT U.idUser
		FROM tblUser U
		WHERE U.idSite = @idCallerSite
		AND U.username = @searchParam
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))

		END

	ELSE IF @dataType = 'email'
			
		BEGIN

		SELECT DISTINCT U.idUser
		FROM tblUser U
		WHERE U.idSite = @idCallerSite
		AND U.email = @searchParam
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))

		END
				
	ELSE

		BEGIN

		SELECT DISTINCT U.idUser
		FROM tblUser U
		INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = U.idUser
		WHERE U.idSite = @idCallerSite
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))

		END


	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO