-- =====================================================================
-- PROCEDURE: [GridAnalytic.LearningPaths]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[GridAnalytic.LearningPaths]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GridAnalytic.LearningPaths]
GO

/*

Gets grid analytic data for LearningPaths grid page.

*/

CREATE PROCEDURE [GridAnalytic.LearningPaths]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
AS
	BEGIN
		SET NOCOUNT ON

		/*

		get the language id from the caller language string

		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		create a temp table for base learning path data, and populate it
		we will run the analytics off of that table so as not to tie up the real table

		*/

		CREATE TABLE #LearningPathsBase (
			idLearningPath INT,
			idSite INT,
			isPublished BIT,
			isClosed BIT,
			dtCreated DATETIME
		)

		INSERT INTO #LearningPathsBase (
			idLearningPath,
			idSite,
			isPublished,
			isClosed,
			dtCreated
		)
		SELECT
			LP.idLearningPath,
			LP.idSite,
			CASE WHEN LP.isPublished = 1 THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			CASE WHEN LP.isClosed = 1 THEN
				CONVERT(BIT, 1)
			ELSE
				CONVERT(BIT, 0)
			END,
			LP.dtCreated
		FROM tblLearningPath LP
		WHERE LP.idSite = @idCallerSite
		AND (LP.isDeleted = 0 OR LP.isDeleted IS NULL) -- non-deleted courses

		/*

		run the analytics

		*/

		SELECT
			(SELECT COUNT(1) FROM #LearningPathsBase) AS total,
			(SELECT COUNT(1) FROM #LearningPathsBase WHERE isPublished = 1) AS published,
			(SELECT COUNT(1) FROM #LearningPathsBase WHERE isPublished = 0) AS nonPublished,
			(SELECT COUNT(1) FROM #LearningPathsBase WHERE isClosed = 1) AS closed,
			(SELECT COUNT(1) FROM #LearningPathsBase WHERE dtCreated >= DATEADD(WEEK, DATEDIFF(WEEK, '1905-01-01', GETUTCDATE()), '1905-01-01')) AS createdThisWeek,
			(SELECT COUNT(1) FROM #LearningPathsBase WHERE dtCreated >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETUTCDATE()), 0)) AS createdThisMonth,
			(SELECT COUNT(1) FROM #LearningPathsBase WHERE dtCreated >= DATEADD(YEAR, DATEDIFF(YEAR, 0, GETUTCDATE()), 0)) AS createdThisYear

		-- DROP THE TEMPORARY TABLES
		DROP TABLE #LearningPathsBase

		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO