-- =====================================================================
-- PROCEDURE: [CouponCode.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CouponCode.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CouponCode.Details]
GO

/*

Return all the properties for a given coupon code id.

*/
CREATE PROCEDURE [CouponCode.Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idCouponCode			INT				OUTPUT,
	@idSite					INT				OUTPUT, 
	@usesAllowed			INT				OUTPUT,
	@discountType			INT				OUTPUT,
	@discount				FLOAT			OUTPUT,
	@code					NVARCHAR(10)	OUTPUT,
	@isDeleted				BIT				OUTPUT,	
	@comments				NVARCHAR(MAX)   OUTPUT,
	@forCatalog				INT				OUTPUT, 
	@forCourse				INT				OUTPUT,	
	@forLearningPath		INT				OUTPUT, 
	@forStandupTraining		INT				OUTPUT, 
	@dtStart				DATETIME		OUTPUT,
	@isSingleUsePerUser		BIT				OUTPUT,
	@dtEnd					DATETIME		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/

	IF (
		SELECT COUNT(1)
		FROM tblCouponCode
		WHERE idCouponCode = @idCouponCode
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CouponCodeDetails_NoRecordFound'
		RETURN 1
	    END	
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idCouponCode		= C.idCouponCode,
		@idSite				= C.idSite,			
		@usesAllowed		= C.usesAllowed,
		@discountType		= C.discountType,
		@discount			= C.discount,
		@code				= C.code,
		@isDeleted			= C.isDeleted,
		@comments			= C.comments,
		@forCatalog			= CASE WHEN C.forCatalog IS NULL THEN 1 ELSE C.forCatalog END,
		@forCourse			= CASE WHEN C.forCourse IS NULL THEN 1 ELSE C.forCourse END,
		@forLearningPath	= CASE WHEN C.forLearningPath IS NULL THEN 1 ELSE C.forLearningPath END,
		@forStandupTraining	= CASE WHEN C.forStandupTraining IS NULL THEN 1 ELSE C.forStandupTraining END,
		@dtStart			= C.dtStart,
		@isSingleUsePerUser	= C.isSingleUsePerUser,
		@dtEnd				= C.dtEnd
	FROM tblCouponCode C
	WHERE C.idCouponCode = @idCouponCode
	AND C.idSite = @idCallerSite
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CouponCodeDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO