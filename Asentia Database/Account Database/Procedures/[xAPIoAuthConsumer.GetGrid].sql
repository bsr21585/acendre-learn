-- =====================================================================
-- PROCEDURE: [xAPIoAuthConsumer.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[xAPIoAuthConsumer.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [xAPIoAuthConsumer.GetGrid]
GO

/*

Gets a listing of tokens from tblxAPIoAuthConsumer.

*/

CREATE PROCEDURE [xAPIoAuthConsumer.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblxAPIoAuthConsumer AAC
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
				)
			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idxAPIoAuthConsumer,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'Label' THEN AAC.label END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'isActive' THEN AAC.isActive END) END DESC,
							
							-- FIRST ORDER ASC							
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'Label' THEN AAC.label END) END,							
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'isActive' THEN AAC.isActive END) END
						)
						AS [row_number]
					FROM tblxAPIoAuthConsumer AAC
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
						)
					
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idxAPIoAuthConsumer, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				AT.idxAPIoAuthConsumer,
				AT.label,
				AT.[oAuthKey],
				AT.[oAuthSecret], 
				AT.isActive, 
				Convert(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblxAPIoAuthConsumer AT ON AT.idxAPIoAuthConsumer = SelectedKeys.idxAPIoAuthConsumer
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblxAPIoAuthConsumer
			INNER JOIN CONTAINSTABLE(tblxAPIoAuthConsumer, *, @searchParam) K ON K.[key] = tblxAPIoAuthConsumer.idxAPIoAuthConsumer
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
				)
			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idxAPIoAuthConsumer,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'Label' THEN AAC.label END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'isActive' THEN AAC.isActive END) END DESC,
							
							-- FIRST ORDER ASC							
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'Label' THEN AAC.label END) END,							
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'isActive' THEN AAC.isActive END) END
						)
						AS [row_number]
					FROM tblxAPIoAuthConsumer AAC
					INNER JOIN CONTAINSTABLE(tblxAPIoAuthConsumer, *, @searchParam) K ON K.[key] = AAC.idxAPIoAuthConsumer
					WHERE 
						(
						@idCallerSite IS NULL
						OR 
						@idCallerSite IS NOT NULL AND idSite = @idCallerSite
						)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idxAPIoAuthConsumer, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				AT.idxAPIoAuthConsumer,
				AT.label,
				AT.[oAuthKey],
				AT.[oAuthSecret],  
				AT.[isActive], 
				Convert(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblxAPIoAuthConsumer AT ON AT.idxAPIoAuthConsumer = SelectedKeys.idxAPIoAuthConsumer
			ORDER BY SelectedKeys.[row_number]
			
			END
			
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO