-- =====================================================================
-- PROCEDURE: [GroupEnrollment.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GroupEnrollment.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GroupEnrollment.Delete]
GO

/*

Deletes group enrollments

*/

CREATE PROCEDURE [GroupEnrollment.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@cascadeDeleteToUsers		BIT,			-- when true, we remove all in-progress enrollments that are inherited to users
												-- when false, we re-link in-progress inherited enrollments so that they appear manually-assigned
	@GroupEnrollments			IDTable			READONLY,
	@writeToEventLog			BIT				= 1
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @GroupEnrollments) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'GroupEnrollmentDelete_NoRecordFound'
		RETURN 1
		END
			
	/*
	
	validate that all group enrollments exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @GroupEnrollments GEGE
		LEFT JOIN tblGroupEnrollment GE ON GE.idGroupEnrollment = GEGE.id 
		WHERE GE.idSite IS NULL
		OR GE.idSite <> @idCallerSite
		) > 0
		
		BEGIN 	
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'GroupEnrollmentDelete_NoRecordFound'
		RETURN 1 
		END
	
	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/*

	End and unlink all inherited enrollments that are completed

	*/

	UPDATE tblEnrollment SET
		dtExpiresFromStart = @utcNow,
		idGroupEnrollment = NULL
	WHERE idGroupEnrollment IN (
		SELECT id 
		FROM @GroupEnrollments
	)
	AND dtCompleted IS NOT NULL
	
	/*

	Delete all inherited enrollments that are not completed, if @cascadeDeleteToUsers is true

	*/

	IF (@cascadeDeleteToUsers = 1)

		BEGIN
	
		DECLARE @EnrollmentsToDelete IDTable

		INSERT INTO @EnrollmentsToDelete (
			id
		)
		SELECT
			idEnrollment
		FROM tblEnrollment E
		WHERE E.idGroupEnrollment IN (SELECT id FROM @GroupEnrollments)
		AND E.dtCompleted IS NULL

		-- do the delete using the Enrollment.Delete procedure
		EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EnrollmentsToDelete, @writeToEventLog

		END			
		
	ELSE

		BEGIN

		UPDATE tblEnrollment SET
			idGroupEnrollment = NULL
		WHERE idGroupEnrollment IN (
			SELECT id
			FROM @GroupEnrollments
		)
		AND dtCompleted IS NULL

		END

	/*
	
	Delete Group Enrollments
	
	*/
	
	DELETE FROM tblGroupEnrollment
	WHERE idGroupEnrollment IN (
		SELECT id 
		FROM @GroupEnrollments
	)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO