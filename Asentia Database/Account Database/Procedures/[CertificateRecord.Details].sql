-- =====================================================================
-- PROCEDURE: [CertificateRecord.Details]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificateRecord.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificateRecord.Details]
GO

/*

Return all the properties for a given certificate id.

*/

CREATE PROCEDURE [CertificateRecord.Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCertificateRecord	INT				OUTPUT,
	@idSite					INT				OUTPUT,
	@idCertificate			INT				OUTPUT,
	@idUser					INT				OUTPUT,
	@idAwardedBy			INT				OUTPUT,
	@timestamp				DATETIME		OUTPUT,
	@expires				DATETIME		OUTPUT,
	@code					NVARCHAR(25)	OUTPUT,
	@idTimeZone				INT				OUTPUT,
	@credits				NVARCHAR(25)	OUTPUT,
	@awardData				NVARCHAR(MAX)	OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificateRecord
		WHERE idCertificateRecord = @idCertificateRecord
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificateRecordDetails_NoRecordFound'
		RETURN 1
	    END
		
	SELECT
		@idCertificateRecord	= CR.idCertificateRecord,
		@idSite					= CR.idSite,
		@idCertificate			= CR.idCertificate,
		@idUser					= CR.idUser,
		@idAwardedBy			= CR.idAwardedBy,
		@timestamp				= CR.[timestamp],
		@expires				= CR.expires,
		@code					= CR.code,
		@idTimeZone				= CR.idTimezone,
		@credits				= CR.credits,
		@awardData				= CR.awardData

	FROM tblCertificateRecord CR
	WHERE CR.idCertificateRecord = @idCertificateRecord
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificateRecordDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO