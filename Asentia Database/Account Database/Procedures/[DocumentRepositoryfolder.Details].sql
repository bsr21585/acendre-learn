-- =====================================================================
-- PROCEDURE: [DocumentRepositoryFolder.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DocumentRepositoryFolder.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DocumentRepositoryFolder.Details]
GO

/*
Returns all the properties for a given id.
*/
CREATE PROCEDURE [DocumentRepositoryFolder.Details]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR (10),
	@idCaller							INT				= 0,
	
	@idDocumentRepositoryFolder			INT				OUTPUT,
	@idSite								INT				OUTPUT,
	@idObjectType						INT				OUTPUT,
	@idObject							INT				OUTPUT,
	@folderName							NVARCHAR(255)	OUTPUT, 
    @isDeleted							BIT				OUTPUT,
	@dtDeleted							DATETIME		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDocumentRepositoryFolder
		WHERE idDocumentRepositoryfolder = @idDocumentRepositoryfolder
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DocumentRepositoryFolderDetails_NoRecordFound'
		RETURN 1
		END
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		@idDocumentRepositoryFolder			= DRI.idDocumentRepositoryfolder,
		@idSite								= DRI.idSite,
		@idObjectType						= DRI.idDocumentRepositoryObjectType,
		@idObject							= DRI.idObject,
		@idDocumentRepositoryFolder			= DRI.idDocumentRepositoryFolder,
	    @folderName							= DRI.[name],
		@isDeleted							= DRI.isDeleted,
		@dtDeleted							= DRI.dtDeleted
	FROM tblDocumentRepositoryfolder DRI
	WHERE DRI.idDocumentRepositoryfolder = @idDocumentRepositoryfolder
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DocumentRepositoryFolderDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO