-- =====================================================================
-- PROCEDURE: [Purchase.ClearAllUnconfirmed]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Purchase.ClearAllUnconfirmed]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Purchase.ClearAllUnconfirmed]
GO

/*

Clears all unconfirmed purchases (active carts) for a site. 

*/

CREATE PROCEDURE [Purchase.ClearAllUnconfirmed]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idSite					INT
)
AS	

	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END		
			 
	/*
	
	delete all transaction items for unconfirmed purchases 
	
	*/

	DELETE FROM tblTransactionItem
	WHERE idSite = @idSite
	AND idPurchase IN (SELECT idPurchase FROM tblPurchase
					   WHERE idSite = @idSite
					   AND [timestamp] IS NULL
					   AND (failed = 0 OR failed IS NULL))

	/*

	delete all unconfirmed purchases

	*/

	DELETE FROM tblPurchase
	WHERE idSite = @idSite
	AND [timestamp] IS NULL
	AND (failed = 0 OR failed IS NULL)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO