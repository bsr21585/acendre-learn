-- =====================================================================
-- PROCEDURE: [Catalog.GetCourses]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.GetCourses]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.GetCourses]
GO
	
/*

Returns a recordset of courses parented by a specific catalog, or belonging to the
catalog root if @idParentCatalog is null.

*/

CREATE PROCEDURE [Catalog.GetCourses]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	-- default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@idParentCatalog			INT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	/*

	do not check caller site membership or caller permissions as
	this can be called by a non-logged-in user

	*/


	/*

	validate that the specified language exists, use the site default if not

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

	IF @idCallerLanguage IS NULL
		BEGIN
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		END

			
	/*
	
	get the data and return it
	
	*/

	IF @idParentCatalog IS NOT NULL -- courses belonging to a specified catalog
		BEGIN

		SELECT
			CO.idCourse,
			CO.idSite,
			CASE WHEN COL.title IS NOT NULL THEN COL.title ELSE CO.title END AS title,
			CO.avatar,
			CO.coursecode,
			CO.credits,
			CASE WHEN CO.isClosed IS NOT NULL THEN CO.isClosed ELSE 0 END AS isClosed,
			CASE WHEN CO.cost IS NOT NULL THEN CAST(CO.cost AS DECIMAL(18,2)) ELSE NULL END AS cost,
			CASE WHEN (
					   SELECT COUNT(1) 
					   FROM tblEnrollment E 
					   WHERE E.idCourse = CO.idCourse 
					   AND E.idUser = @idCaller 
					   AND (
							 (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
							 AND
							 (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
						   )
					   AND E.dtCompleted IS NULL
					  ) = 0 THEN CONVERT(BIT, 0) 
			ELSE CONVERT(BIT, 1) END AS isCallerEnrolled, -- note, this checks for any course enrollments where they are not completed and have not expired
			CCL.[order]
		FROM tblCourse CO
		LEFT JOIN tblCourseLanguage COL ON COL.idCourse = CO.idCourse AND COL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCourseToCatalogLink CCL ON CCL.idCourse = CO.idCourse
		WHERE CO.idSite = @idCallerSite 
		AND (CO.isDeleted IS NULL OR CO.isDeleted = 0)
		AND CO.isPublished = 1
		AND CCL.idCatalog = @idParentCatalog
		ORDER BY CCL.[order], title

		END

	ELSE -- courses not belonging to any catalog
		BEGIN

		SELECT
			CO.idCourse,
			CO.idSite,
			CASE WHEN COL.title IS NOT NULL THEN COL.title ELSE CO.title END AS title,
			CO.avatar,
			CO.coursecode,
			CO.credits,
			CO.isClosed,
			CASE WHEN CO.cost IS NOT NULL THEN CAST(CO.cost AS DECIMAL(18,2)) ELSE NULL END AS cost,
			CASE WHEN (
					   SELECT COUNT(1) 
					   FROM tblEnrollment E 
					   WHERE E.idCourse = CO.idCourse 
					   AND E.idUser = @idCaller 
					   AND E.idGroupEnrollment IS NULL 
					   AND E.idRuleSetEnrollment IS NULL 
					   AND (
							 (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
							 AND
							 (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
						   )
					   AND E.dtCompleted IS NULL
					  ) = 0 THEN CONVERT(BIT, 0) 
			ELSE CONVERT(BIT, 1) END AS isCallerEnrolled, -- note, this checks for self/admin enrolled course enrollments where they are not completed and have not expired
			CO.[order]
		FROM tblCourse CO
		LEFT JOIN tblCourseLanguage COL ON COL.idCourse = CO.idCourse AND COL.idLanguage = @idCallerLanguage
		WHERE CO.idSite = @idCallerSite 
		AND (CO.isDeleted IS NULL OR CO.isDeleted = 0)
		AND CO.isPublished = 1
		AND CO.idCourse NOT IN (SELECT DISTINCT CCL.idCourse FROM tblCourseToCatalogLink CCL WHERE CCL.idSite = @idCallerSite)
		ORDER BY CO.[order], title

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO	