-- =====================================================================
-- PROCEDURE: [CouponCode.GetCatalogs]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CouponCode.GetCatalogs]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CouponCode.GetCatalogs]
GO

/*

Returns a recordset of catalog ids and titles that belong to a coupon code.

*/

CREATE PROCEDURE [CouponCode.GetCatalogs]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCouponCode			INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL

		BEGIN

		SELECT
			DISTINCT
			C.idCatalog, 
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblCatalog C
		LEFT JOIN tblCatalogLanguage CL ON CL.idCatalog = C.idCatalog AND CL.idLanguage = @idCallerLanguage				
		WHERE C.idSite = @idCallerSite
		AND EXISTS (SELECT 1 FROM tblCouponCodeToCatalogLink CCCL
					WHERE CCCL.idCouponCode = @idCouponCode
					AND CCCL.idCatalog = C.idCatalog)
		ORDER BY title

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			C.idCatalog,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title
		FROM tblCatalogLanguage CL
		INNER JOIN CONTAINSTABLE(tblCatalogLanguage, *, @searchParam) K ON K.[key] = CL.idCatalogLanguage AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCatalog C ON C.idCatalog = CL.idCatalog
		WHERE C.idSite = @idCallerSite
		AND EXISTS (SELECT 1 FROM tblCouponCodeToCatalogLink CCCL
					WHERE CCCL.idCouponCode = @idCouponCode
					AND CCCL.idCatalog = C.idCatalog)
		ORDER BY title

		END
	
	SELECT @Error_Description_Code = ''
	SELECT @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	