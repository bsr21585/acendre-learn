-- =====================================================================
-- PROCEDURE: [Catalog.Delete]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.Delete]
GO

/*

Deletes a catalog

*/

CREATE PROCEDURE [dbo].[Catalog.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (50)	OUTPUT, --added by chetu on 7th Sep 2014 as code was expecting this
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	@Catalogs					IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Catalogs) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CatalogDelete_NoRecordsToDelete'
		RETURN 1
		END
			
	/*
	
	validate that all catalogs exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Catalogs CC
		LEFT JOIN tblCatalog C ON C.idCatalog = CC.id
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'CatalogDelete_CatalogsOfDifferentSite'			
		RETURN 1 
		END
		
	-- declare and find the sub catalogs so that we can recurse
	
	DECLARE @SubCatalogs IDTable
	
	INSERT INTO @SubCatalogs (id)
		SELECT idCatalog 
		FROM tblCatalog 
		WHERE idSite = @idCallerSite 
		AND idParent IS NOT NULL
		AND idParent IN (SELECT id FROM @Catalogs)
		
	/*
	
	if we have subcatalogs, recurse the delete
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @SubCatalogs
		) > 0 
		BEGIN
		
		EXEC	[Catalog.Delete]
				@Return_Code			= @Return_Code				OUTPUT,
				@Error_Description_Code = @Error_Description_Code	OUTPUT, --added by chetu on 7th Sep 2014 as code was expecting this
				@idCallerSite			= @idCallerSite,
				@callerLangString		= @callerLangString,
				@idCaller				= @idCaller,
				@Catalogs				= @SubCatalogs
		
		IF (@Return_Code <> 0)
		
			BEGIN
			-- bubble up the return code and exit
			RETURN 1
			END
			
			
		END

	/*
	
	Delete the catalog access to group links
	
	*/
	
	DELETE FROM tblCatalogAccessToGroupLink
	WHERE idCatalog IN (
		SELECT id
		FROM @Catalogs
	)
		
	/*
	
	Delete the course links
	
	*/
	
	DELETE FROM tblCourseToCatalogLink
	WHERE idCatalog IN (
		SELECT id
		FROM @Catalogs
	)
	
	/*
	
	Delete the language records 
	
	*/
	
	DELETE FROM tblCatalogLanguage
	WHERE idCatalog IN (
		SELECT id
		FROM @Catalogs
	)
	
	/*
	
	Delete the catalog itself
	
	*/
	
	DELETE FROM tblCatalog
	WHERE idCatalog IN (
		SELECT id
		FROM @Catalogs
	)
	
	SELECT @Return_Code = 0
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		
