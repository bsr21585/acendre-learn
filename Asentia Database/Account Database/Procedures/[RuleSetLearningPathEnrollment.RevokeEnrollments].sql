-- =====================================================================
-- PROCEDURE: [RuleSetLearningPathEnrollment.RevokeEnrollments]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.RevokeEnrollments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetLearningPathEnrollment.RevokeEnrollments]
GO

/*

Revokes learning path RuleSet Enrollment enrollments where the enrollment is incomplete
and the user no longer matches any RuleSet Enrollments of the Learning Path. 

This procedure only handles COMPLETE REVOCATION of enrollments inherited from a
RuleSet Enrollment. Lateral moves from one RuleSet Enrollment to another are 
handled in the AssignEnrollments procedure. Therefore, this procedure is "hooked
into" (called directly from) the AssignEnrollments procedure in order to guarantee
that the revocations are done AFTER the assignments are done. 

IN ORDER TO COMPLETELY ELIMINATE THE POSSIBILITY OF INHERITED ENROLLMENTS BEING
MISTAKENLY REMOVED, THIS PROCEDURE SHOULD ONLY BE CALLED FROM "AssignEnrollments,"
IT SHOULD NEVER BE CALLED DIRECTLY BY A JOB OR FROM PROCEDURAL CODE.

Notes:

@LearningPaths	- IDTable of Learning Path ids to assign RuleSet Enrollments for.
				  Required to be passed, but not required to be populated.
				  If not populated, this will be executed on ALL Learning Paths
				  that have RuleSet Enrollments.

@Filters		- IDTable of User or Group ids (see @filterBy param) to
				  assign RuleSet Enrollments for.
				  Required to be passed, but not required to be populated.
				  If not populated, this will be executed on ALL Users or
				  Groups (see @filterBy param).

@filterBy		- Defines the object(s), User or Group, that we are executing
				  this for. If "Group," we are just getting the users that are
				  members of the specified Group(s) to execute this on. There
				  is no direct RuleSet Enrollment to Group inheritance, just
				  rules that can say "if user is a member of ...". Essentially,
				  the "Group" filter only gets used when we are calling this
				  after adding or removing User(s) from Group(s).

The parameters described above are all passed through to the [System.GetRuleSetLearningPathMatches]
procedure, which then returns a recordset of user-to-ruleset enrollment matches filtered 
down to the Learning Path(s) and User(s) resulting from applying the filters above.

*/

CREATE PROCEDURE [RuleSetLearningPathEnrollment.RevokeEnrollments]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,

	@LearningPaths				IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/*

	Execute [System.GetRuleSetLearningPathMatches] and dump the results into a temp table.

	*/

	CREATE TABLE #RuleSetLearningPathEnrollmentMatches
	(
		idSite							INT,
		idRuleSetLearningPathEnrollment INT,
		[priority]						INT,
		idRuleSet						INT,
		idLearningPath					INT,
		idUser							INT
	)

	INSERT INTO #RuleSetLearningPathEnrollmentMatches
	EXEC [System.GetRuleSetLearningPathMatches] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @LearningPaths, @Filters, @filterBy, 1

	/*

	DELETE incomplete Learning Path Enrollments where the user is no longer matched to the RuleSet Learning 
	Path Enrollment they have inherited the enrollment for.

	*/

	DECLARE @LearningPathEnrollmentsToDelete IDTable

	INSERT INTO @LearningPathEnrollmentsToDelete (
		id
	)
	SELECT
		idLearningPathEnrollment
	FROM tblLearningPathEnrollment LPE
	WHERE NOT EXISTS (SELECT 1 FROM #RuleSetLearningPathEnrollmentMatches RSM
					  WHERE LPE.idUser = RSM.idUser
					  AND LPE.idRuleSetLearningPathEnrollment = RSM.idRuleSetLearningPathEnrollment
					  AND LPE.idLearningPath = RSM.idLearningPath)
	--AND LPE.idLearningPath IN (SELECT DISTINCT idLearningPath FROM #RuleSetLearningPathEnrollmentMatches RSM)
	AND (
			((SELECT COUNT(1) FROM @LearningPaths) = 0)
			OR
			(LPE.idLearningPath IN (SELECT DISTINCT id FROM @LearningPaths))
		)
	AND (
			((SELECT COUNT(1) FROM @Filters) = 0)
			OR
			(@filterBy = 'group' AND LPE.idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT id FROM @Filters)))
			OR
			(@filterBy = 'user' AND LPE.idUser IN (SELECT DISTINCT id FROM @Filters))
		)
	AND LPE.idRuleSetLearningPathEnrollment IS NOT NULL
	AND LPE.dtCompleted IS NULL

	EXEC [LearningPathEnrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @LearningPathEnrollmentsToDelete
	
	/*

	REMOVE the link to the RuleSet Learning Path Enrollment for completed enrollments where the user is no longer
	matched to the RuleSet Learning Path Enrollment they have inherited the enrollment for.

	*/

	UPDATE tblLearningPathEnrollment SET
		idRuleSetLearningPathEnrollment = NULL
	WHERE NOT EXISTS (SELECT 1 FROM #RuleSetLearningPathEnrollmentMatches RSM
					  WHERE tblLearningPathEnrollment.idUser = RSM.idUser
					  AND tblLearningPathEnrollment.idRuleSetLearningPathEnrollment = RSM.idRuleSetLearningPathEnrollment
					  AND tblLearningPathEnrollment.idLearningPath = RSM.idLearningPath)
	AND tblLearningPathEnrollment.idLearningPath IN (SELECT DISTINCT RSM.idLearningPath FROM #RuleSetLearningPathEnrollmentMatches RSM)
	AND (
			((SELECT COUNT(1) FROM @Filters) = 0)
			OR
			(@filterBy = 'group' AND tblLearningPathEnrollment.idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT id FROM @Filters)))
			OR
			(@filterBy = 'user' AND tblLearningPathEnrollment.idUser IN (SELECT DISTINCT id FROM @Filters))
		)
	AND tblLearningPathEnrollment.idRuleSetLearningPathEnrollment IS NOT NULL
	AND tblLearningPathEnrollment.dtCompleted IS NOT NULL
				   
	/* DROP THE TEMPORARY TABLE */
	DROP TABLE #RuleSetLearningPathEnrollmentMatches

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO