-- =====================================================================
-- PROCEDURE: [Role.JoinGroups]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Role.JoinGroups]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Role.JoinGroups]
GO

CREATE PROCEDURE [Role.JoinGroups]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	--   default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, 
		
	@idRole					INT,
	@Groups					IDTable			READONLY
)
AS

	BEGIN
	
	SET NOCOUNT ON
   
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	make sure there are objects in the table parameter
	
	*/
	
	IF (SELECT COUNT(1) FROM @Groups) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'RoleJoinGroups_NoGroupsFound'
		RETURN 1
		END
		
	/*

	validate role exists
	
	*/

	IF (
		SELECT COUNT(1)
		FROM tblRole
		WHERE idRole = @idRole
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN
		SELECT @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'RoleJoinGroups_DetailsNotFound'
		RETURN 1
		END
		
	/*
	
	validate that the objects exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Groups GG
		LEFT JOIN tblGroup G ON G.idGroup = GG.id
		WHERE G.idSite IS NULL
		OR G.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'RoleJoinGroups_GroupsNotInSameSite'
		RETURN 1
		END
	
	/*

	attach the role to groups where they do not already exist 

	*/

	INSERT INTO tblGroupToRoleLink (
		idSite,
		idRole,
		idGroup,
		created
	)
	SELECT 
		@idCallerSite,
		@idRole, 
		GG.id,
		GETUTCDATE()
	FROM @Groups GG
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblGroupToRoleLink GRL
		WHERE GRL.idRole = @idRole
		AND GRL.idGroup = GG.id
		AND idRuleSet IS NULL -- manual join
	)

	SELECT @Return_Code = 0
	
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
