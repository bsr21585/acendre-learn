-- =====================================================================
-- PROCEDURE: [QuizSurvey.Save]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[QuizSurvey.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [QuizSurvey.Save]
GO

/*

Adds new quiz/survey or updates existing quiz/survey.

*/
CREATE PROCEDURE [QuizSurvey.Save]
(
    @Return_Code				INT				OUTPUT,
    @Error_Description_Code		NVARCHAR(50)	OUTPUT,
    @idCallerSite				INT				= 0,
    @callerLangString			NVARCHAR(10),
    @idCaller					INT				= 0,
    
    @idQuizSurvey				INT				OUTPUT,
	@idAuthor					INT,
	@type						INT,
	@identifier					NVARCHAR(255),
	@guid						NVARCHAR(36),
	@data						NVARCHAR(MAX),
	@isDraft					BIT,
	@idContentPackage			INT
)
AS 
    BEGIN
    SET NOCOUNT ON

    DECLARE @idPermission INT -- define the permission required to perform this function
    SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	ensure that an identifier was specified

	*/

	IF @identifier IS NULL OR @identifier = ''
		BEGIN                               
		SET @Return_Code = 4
		SET @Error_Description_Code = 'QuizSurveySave_IdentifierMustBeSpecified'
		RETURN 1
		END

		
	/*
	
	save the data
	
	*/
	
    IF (@idQuizSurvey = 0 OR @idQuizSurvey IS NULL) 
            
		BEGIN
		
		-- insert the new object
		
        INSERT INTO tblQuizSurvey ( 
			idSite,
			idAuthor,
			[type],
			identifier,
			[guid],
			data,
			isDraft,
            dtCreated,
            dtModified
			-- idContentPackage does not get inserted as the content package will not be created prior to the quiz/survey
        )
        VALUES ( 
			@idCallerSite,
			@idAuthor,
			@type,
            @identifier,
			@guid,
			@data,
			@isDraft,
            GETUTCDATE(),
            GETUTCDATE()
        )
		
		-- get the new object id and return successfully
		
        SELECT @idQuizSurvey = SCOPE_IDENTITY()
    
		END
		
	ELSE

		BEGIN
		
		-- check that the quiz/survey id exists
		IF (SELECT COUNT(1) FROM tblQuizSurvey WHERE idQuizSurvey = @idQuizSurvey AND idSite = @idCallerSite) < 1
			BEGIN                               
			
			SET @idQuizSurvey = @idQuizSurvey
			SET @Return_Code = 1 --(1 is 'details not found')
			SET @Error_Description_Code = 'QuizSurveySave_NoRecordFound'
			RETURN 1
			
			END
			
		-- update existing quiz/survey's properties
		
		UPDATE tblQuizSurvey SET
			[type] = @type,
			identifier = @identifier,
			data = @data,
			isDraft = @isDraft,
			idContentPackage = @idContentPackage,
            dtModified = GETUTCDATE()
		WHERE idQuizSurvey = @idQuizSurvey
		
		-- get the quiz/survey's id 
		SELECT @idQuizSurvey = @idQuizSurvey

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO