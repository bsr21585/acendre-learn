-- =====================================================================
-- PROCEDURE: [LearningPath.IdsAndNamesForCouponCodeSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[LearningPath.IdsAndNamesForCouponCodeSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPath.IdsAndNamesForCouponCodeSelectList]
GO

/*

Returns a recordset of course ids and titles to populate the
course select list for a coupon code.

*/

CREATE PROCEDURE [LearningPath.IdsAndNamesForCouponCodeSelectList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCouponCode			INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141 
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT 
			LP.idLearningPath, 
			CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE LP.name END AS title
		FROM tblLearningPath LP
		LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = @idCallerLanguage
		WHERE LP.idSite = @idCallerSite
		AND (LP.isDeleted = 0 OR LP.isDeleted IS NULL)
		AND NOT EXISTS (SELECT 1 FROM tblCouponCodeToLearningPathLink CCLL
						WHERE CCLL.idCouponCode = @idCouponCode
						AND CCLL.idLearningPath = LP.idLearningPath)
		ORDER BY title

		END
			
	ELSE

		BEGIN

		SELECT DISTINCT
			LP.idLearningPath, 
			CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE LP.name END AS title
		FROM tblLearningPathLanguage LPL
		INNER JOIN CONTAINSTABLE(tblLearningPathLanguage, *, @searchParam) K ON K.[key] = LPL.idLearningPathLanguage AND LPL.idLanguage = @idCallerLanguage
		LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPL.idLearningPath
		WHERE LP.idSite = @idCallerSite
		AND (LP.isDeleted = 0 OR LP.isDeleted IS NULL)
		AND NOT EXISTS (SELECT 1 FROM tblCouponCodeToLearningPathLink CCLL
						WHERE CCLL.idCouponCode = @idCouponCode
						AND CCLL.idLearningPath = LP.idLearningPath)
		ORDER BY title

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	