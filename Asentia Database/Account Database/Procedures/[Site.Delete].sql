-- =====================================================================
-- PROCEDURE: [Site.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Site.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Site.Delete]
GO

CREATE PROCEDURE [Site.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@Sites					IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site - This might not be necessary since it's the site(s) we're deleting.
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF (SELECT COUNT(1) FROM @Sites) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'SiteDelete_NoRecordsSelected'
		RETURN 1
		END

	/*

	declare and populate a table of the hostname/domain links to the site(s), we'll need to return
	this back to customer manager code so those links can be removed from the account-level links

	*/

	DECLARE @SiteHostnameDomainLinks TABLE (idSite INT, domain NVARCHAR(255), hostname NVARCHAR(255))

	INSERT INTO @SiteHostnameDomainLinks (
		idSite,
		domain, 
		hostname
	) 
	SELECT 
		SDAL.idSite,
		SDAL.domain,		 
		S.hostname 
	FROM tblSiteToDomainAliasLink SDAL
	LEFT JOIN tblSite S ON S.idSite = SDAL.idSite
	WHERE SDAL.idSite IN (SELECT id FROM @Sites)	

	/*
	
	DELETE all objects dependent on the site(s) in order of dependencies

	*/	

	-- COUPON CODE TO ILT LINKS
	DELETE FROM tblCouponCodeToStandupTrainingLink WHERE idCouponCode IN (
		SELECT idCouponCode FROM tblCouponCode WHERE idSite IN (SELECT id FROM @Sites)
	)

	-- COUPON CODE TO CATALOG LINKS
	DELETE FROM tblCouponCodeToCatalogLink WHERE idCouponCode IN (
		SELECT idCouponCode FROM tblCouponCode WHERE idSite IN (SELECT id FROM @Sites)
	)

	-- COUPON CODE TO COURSE LINKS
	DELETE FROM tblCouponCodeToCourseLink WHERE idCouponCode IN (
		SELECT idCouponCode FROM tblCouponCode WHERE idSite IN (SELECT id FROM @Sites)
	)

	-- COUPON CODE TO LEARNING PATH LINKS
	DELETE FROM tblCouponCodeToLearningPathLink WHERE idCouponCode IN (
		SELECT idCouponCode FROM tblCouponCode WHERE idSite IN (SELECT id FROM @Sites)
	)

	-- COUPON CODE USE
	DELETE FROM tblCouponCodeUse WHERE idCouponCode IN (
		SELECT idCouponCode FROM tblCouponCode WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION?
	)

	-- GROUP ENROLLMENT APPROVERS
	DELETE FROM tblGroupEnrollmentApprover WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION?	

	-- SSO TOKENS
	DELETE FROM tblSSOToken WHERE idSite IN (SELECT id FROM @Sites)

	-- INBOX MESSAGES
	DELETE FROM tblInboxMessage WHERE idSite IN (SELECT id FROM @Sites)

	-- ILT SESSION TO USER LINKS
	DELETE FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance IN (
		SELECT idStandupTrainingInstance FROM tblStandUpTrainingInstance WHERE idSite IN (SELECT id FROM @Sites)
	)

	-- ILT SESSION TO INSTRUCTOR LINKS
	DELETE FROM tblStandupTrainingInstanceToInstructorLink WHERE idSite IN (SELECT id FROM @Sites)

	-- ILT SESSION MEETING TIMES
	DELETE FROM tblStandUpTrainingInstanceMeetingTime WHERE idSite IN (SELECT id FROM @Sites)

	-- ILT SESSIONS
	DELETE FROM tblStandUpTrainingInstanceLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblStandUpTrainingInstance WHERE idSite IN (SELECT id FROM @Sites)

	-- ILTS
	DELETE FROM tblStandUpTrainingLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblStandUpTraining WHERE idSite IN (SELECT id FROM @Sites)

	-- WEB MEETING ORGANIZERS
	DELETE FROM tblWebMeetingOrganizer WHERE idSite IN (SELECT id FROM @Sites)

	-- RESOURCE TO OBJECT LINKS
	DELETE FROM tblResourceToObjectLink WHERE idSite IN (SELECT id FROM @Sites)

	-- RESOURCES
	DELETE FROM tblResourceLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblResource WHERE idSite IN (SELECT id FROM @Sites)

	-- REPORT PROCESSOR LOG ENTRIES
	DELETE FROM tblReportProcessorLog WHERE idSite IN (SELECT id FROM @Sites)

	-- REPORT SUBSCRIPTION QUEUE
	DELETE FROM tblReportSubscriptionQueue WHERE idSite IN (SELECT id FROM @Sites)

	-- REPORT SUBSCRIPTIONS
	DELETE FROM tblReportSubscription WHERE idSite IN (SELECT id FROM @Sites)

	-- REPORT TO REPORT SHORTCUTS
	DELETE FROM tblReportToReportShortcutsWidgetLink WHERE idSite IN (SELECT id FROM @Sites)

	-- REPORT FILES
	DELETE FROM tblReportFile WHERE idSite IN (SELECT id FROM @Sites)

	-- REPORTS
	DELETE FROM tblReportLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblReport WHERE idSite IN (SELECT id FROM @Sites)

	-- ANALYTICS
	DELETE FROM tblAnalyticToCourseLink WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION
	DELETE FROM tblAnalyticLanguage WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION
	DELETE FROM tblAnalytic WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION
	DELETE FROM tblAnalyticDatasetLanguage WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION
	DELETE FROM tblAnalyticDataset WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION

	-- DATASETS
	DELETE FROM tblDatasetLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblDataset WHERE idSite IN (SELECT id FROM @Sites)

	-- DOCUMENT REPOSITORY ITEMS
	DELETE FROM tblDocumentRepositoryItemLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblDocumentRepositoryItem WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblDocumentRepositoryFolder WHERE idSite IN (SELECT id FROM @Sites)
	
	-- CERTIFICATION TO USER LINKS
	DELETE FROM [tblData-CertificationModuleRequirement] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificationToUserLink WHERE idSite IN (SELECT id FROM @Sites)

	-- COURSE LESSON DATA AND ENROLLMENTS
	DELETE FROM [tblData-SCOInt] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM [tblData-SCOObj] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM [tblData-TinCanContextActivities] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM [tblData-SCO] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM [tblData-TinCan] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM [tblData-HomeworkAssignment] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM [tblData-Lesson] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM [tblData-TinCanProfile] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM [tblData-TinCanState] WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblEnrollment WHERE idSite IN (SELECT id FROM @Sites)

	-- ENROLLMENT REQUESTS
	DELETE FROM tblEnrollmentRequest WHERE idSite IN (SELECT id FROM @Sites)	

	-- ACTIVITY IMPORTS
	DELETE FROM tblActivityImport WHERE idSite IN (SELECT id FROM @Sites)

	-- XAPI NONCES AND KEYS
	DELETE FROM tblxAPIoAuthNonce WHERE idxAPIoAuthConsumer IN (
		SELECT idxAPIoAuthConsumer FROM tblxAPIoAuthConsumer WHERE idSite IN (SELECT id FROM @Sites)
	)

	DELETE FROM tblxAPIoAuthConsumer WHERE idSite IN (SELECT id FROM @Sites)

	-- CATALOG ACCESS TO GROUP LINKS
	DELETE FROM tblCatalogAccessToGroupLink WHERE idSite IN (SELECT id FROM @Sites)

	-- CATALOG TO COURSE LINKS
	DELETE FROM tblCourseToCatalogLink WHERE idSite IN (SELECT id FROM @Sites)

	-- CATALOGS
	DELETE FROM tblCatalogLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCatalog WHERE idSite IN (SELECT id FROM @Sites)	
	
	-- CERTIFICATE RECORDS
	DELETE FROM tblCertificateRecord WHERE idSite IN (SELECT id FROM @Sites)

	-- CERTIFICATE IMPORTS
	DELETE FROM tblCertificateImport WHERE idSite IN (SELECT id FROM @Sites)

	-- CERTIFICATES
	DELETE FROM tblCertificateLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificate WHERE idSite IN (SELECT id FROM @Sites)

	-- TRANSACTIONS AND PURCHASES
	DELETE FROM tblTransactionResponse WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblTransactionItem WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblPurchase WHERE idSite IN (SELECT id FROM @Sites)

	-- COUPON CODES
	DELETE FROM tblCouponCode WHERE idSite IN (SELECT id FROM @Sites)

	-- RULESET LINKS AND RULESETS
	DELETE FROM tblRuleSetEngineQueue WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRuleSetToCertificationLink WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRuleSetToGroupLink WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRuleSetToRoleLink WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRuleSetToRuleSetEnrollmentLink WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRule WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRuleSetLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRuleSet WHERE idSite IN (SELECT id FROM @Sites)

	-- USER TO ROLE LINKS
	DELETE FROM tblUserToRoleLink WHERE idSite IN (SELECT id FROM @Sites)

	-- GROUP TO ROLE LINKS
	DELETE FROM tblGroupToRoleLink WHERE idSite IN (SELECT id FROM @Sites)

	-- ROLE TO PERMISSION LINKS
	DELETE FROM tblRoleToPermissionLink WHERE idSite IN (SELECT id FROM @Sites)

	-- ROLES
	DELETE FROM tblRoleLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRole WHERE idSite IN (SELECT id FROM @Sites)

	-- RESOURCE TYPES
	DELETE FROM tblResourceTypeLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblResourceType WHERE idSite IN (SELECT id FROM @Sites)

	-- CERTIFICATIONS
	DELETE FROM tblCertificationModuleRequirementToCourseLink WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificationModuleRequirementLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificationModuleRequirement WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificationModuleRequirementSetLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificationModuleRequirementSet WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificationToCourseCreditLink WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificationModuleLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificationModule WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertificationLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCertification WHERE idSite IN (SELECT id FROM @Sites)

	-- LEARNING PATH ENROLLMENT APPROVERS
	DELETE FROM tblLearningPathApprover WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION
	DELETE FROM tblLearningPathEnrollmentApprover WHERE idSite IN (SELECT id FROM @Sites)

	-- LEARNING PATH ENROLLMENTS
	DELETE FROM tblLearningPathEnrollment WHERE idSite IN (SELECT id FROM @Sites)

	-- LEARNING PATH FEED MESSAGES
	DELETE FROM tblLearningPathFeedMessage WHERE idSite IN (SELECT id FROM @Sites)

	-- LEARNING PATH FEED MODERATORS
	DELETE FROM tblLearningPathFeedModerator WHERE idSite IN (SELECT id FROM @Sites)

	-- LEARNING PATH FEED
	DELETE FROM tblLearningPathFeed WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION

	-- LEARNING PATH TO COURSE LINKS
	DELETE FROM tblLearningPathToCourseLink WHERE idSite IN (SELECT id FROM @Sites)

	-- RULESET LEARNING PATH ENROLLMENTS	
	DELETE FROM tblRuleSetLearningPathEnrollmentLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRuleSetLearningPathEnrollment WHERE idSite IN (SELECT id FROM @Sites)

	-- USER TO LEARNING PATH LINKS
	DELETE FROM tblUserToLearningPathLink WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION

	-- LEARNING PATHS
	DELETE FROM tblLearningPathLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblLearningPath WHERE idSite IN (SELECT id FROM @Sites)

	-- GROUP ENROLLMENTS
	DELETE FROM tblGroupEnrollment WHERE idSite IN (SELECT id FROM @Sites)

	-- GROUP FEED MESSAGES
	DELETE FROM tblGroupFeedMessage WHERE idSite IN (SELECT id FROM @Sites)

	-- GROUP FEED MODERATORS 
	DELETE FROM tblGroupFeedModerator WHERE idSite IN (SELECT id FROM @Sites)

	-- USER TO GROUP LINKS
	DELETE FROM tblUserToGroupLink WHERE idSite IN (SELECT id FROM @Sites)

	-- GROUPS
	DELETE FROM tblGroupLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblGroup WHERE idSite IN (SELECT id FROM @Sites)

	-- USER TO RULESET ENROLLMENT LINKS
	DELETE FROM tblUserToRuleSetEnrollmentLink WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION

	-- RULESET ENROLLMENTS
	DELETE FROM tblRuleSetEnrollmentLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblRuleSetEnrollment WHERE idSite IN (SELECT id FROM @Sites)

	-- LESSON TO CONTENT LINKS
	DELETE FROM tblLessonToContentLink WHERE idSite IN (SELECT id FROM @Sites)

	-- LESSONS
	DELETE FROM tblLessonLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblLesson WHERE idSite IN (SELECT id FROM @Sites)

	-- COURSE ENROLLMENT APPROVERS
	DELETE FROM tblCourseApprover WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION
	DELETE FROM tblCourseEnrollmentApprover WHERE idSite IN (SELECT id FROM @Sites)

	-- USER TO COURSE FEED SUBSCRIPTIONS
	DELETE FROM tblUserToCourseFeedSubscription WHERE idSite IN (SELECT id FROM @Sites) -- TABLE PENDING DELETION

	-- COURSE EXPERTS
	DELETE FROM tblCourseExpert WHERE idSite IN (SELECT id FROM @Sites)

	-- COURSE RATINGS
	DELETE FROM tblCourseRating WHERE idSite IN (SELECT id FROM @Sites)

	-- COURSE SCREENSHOTS
	DELETE FROM tblCourseToScreenshotLink WHERE idSite IN (SELECT id FROM @Sites)

	-- COURSE PREREQUISITES
	DELETE FROM tblCourseToPrerequisiteLink WHERE idSite IN (SELECT id FROM @Sites)

	-- COURSE FEED MESSAGES
	DELETE FROM tblCourseFeedMessage WHERE idSite IN (SELECT id FROM @Sites)

	-- COURSE FEED MODERATORS
	DELETE FROM tblCourseFeedModerator WHERE idSite IN (SELECT id FROM @Sites)

	-- COURSES
	DELETE FROM tblCourseLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblCourse WHERE idSite IN (SELECT id FROM @Sites)

	-- CONTENT PACKAGES, AND QUIZZES & SURVEYS
	-- WE DO THEM TOGETHER BECAUSE OF CIRCULAR REFERENCES
	UPDATE tblContentPackage SET idQuizSurvey = NULL WHERE idSite IN (SELECT id FROM @Sites)
	UPDATE tblQuizSurvey SET idContentPackage = NULL WHERE idSite IN (SELECT id FROM @Sites)

	DELETE FROM tblContentPackage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblQuizSurvey WHERE idSite IN (SELECT id FROM @Sites)

	-- EXCEPTION LOGS
	DELETE FROM tblExceptionLog WHERE idSite IN (SELECT id FROM @Sites)

	-- EVENT EMAIL QUEUES
	DELETE FROM tblEventEmailQueue WHERE idSite IN (SELECT id FROM @Sites)

	-- EVENT EMAIL NOTIFICATIONS
	DELETE FROM tblEventEmailNotificationLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblEventEmailNotification WHERE idSite IN (SELECT id FROM @Sites)

	-- EVENT LOGS
	DELETE FROM tblEventLog WHERE idSite IN (SELECT id FROM @Sites)

	-- USER TO SUPERVISOR LINKS
	DELETE FROM tblUserToSupervisorLink WHERE idSite IN (SELECT id FROM @Sites)

	-- USERS
	DELETE FROM tblUser WHERE idSite IN (SELECT id FROM @Sites)

	-- LICENSE AGREEMENT TO SITE LINKS
	DELETE FROM tblLicenseAgreementToSiteLink WHERE idSite IN (SELECT id FROM @Sites)

	-- SITE AVAILABLE LANGUAGES
	DELETE FROM tblSiteAvailableLanguage WHERE idSite IN (SELECT id FROM @Sites)

	-- SITE IP RESTRICTIONS
	DELETE FROM tblSiteIPRestriction WHERE idSite IN (SELECT id FROM @Sites)

	-- SITE TO DOMAIN ALIAS LINKS
	DELETE FROM tblSiteToDomainAliasLink WHERE idSite IN (SELECT id FROM @Sites)

	-- SITE PARAMS
	DELETE FROM tblSiteParam WHERE idSite IN (SELECT id FROM @Sites)

	-- SITES
	DELETE FROM tblSiteLanguage WHERE idSite IN (SELECT id FROM @Sites)
	DELETE FROM tblSite WHERE idSite IN (SELECT id FROM @Sites)

	/*

	do one final cleanup of tblRuleSetEngineQueue to account for items queued by triggers

	*/

	DELETE FROM tblRuleSetEngineQueue WHERE idSite IN (SELECT id FROM @Sites)

	/*

	return the hostname/domain links of the site(s)

	*/

	SELECT * FROM @SiteHostnameDomainLinks		

	SET @Return_Code = 0
	SET @Error_Description_Code = ''	
	
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO