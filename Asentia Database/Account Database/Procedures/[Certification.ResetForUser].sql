-- =====================================================================
-- PROCEDURE: [Certification.ResetForUser]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Certification.ResetForUser]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) 
	DROP PROCEDURE [Certification.ResetForUser]
GO

/*

Resets a certification for a user.

*/

CREATE PROCEDURE [Certification.ResetForUser]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idCertificationToUserLink	INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*
	
	validate that certification to user link exists
	
	*/

	IF (SELECT COUNT(1) 
	    FROM tblCertificationToUserLink 
		WHERE idCertificationToUserLink = @idCertificationToUserLink
		AND idSite = @idCallerSite
		) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CertificationResetForUser_NoRecordFound'
		RETURN 1
		END	

	/*
		
	delete the certification requirement data

	*/	

	-- DELETE FROM [tblData-CertificationModuleRequirement]
	DELETE FROM [tblData-CertificationModuleRequirement] WHERE idCertificationToUserLink = @idCertificationToUserLink

	/*

	reset the certification to user link by clearing out all data

	*/

	UPDATE tblCertificationToUserLink SET
		initialAwardDate = null,
		certificationTrack = 0,
		currentExpirationDate = null,
		currentExpirationInterval = null,
		currentExpirationTimeframe = null,
		certificationId = null,
		lastExpirationDate = null
	WHERE idCertificationToUserLink = @idCertificationToUserLink

	/*

	do evaluation for certificate revocations

	*/

	DECLARE @idUser				INT
	DECLARE @idCertification	INT
	DECLARE @idUserTimezone		INT

	SELECT
		@idUser = CUL.idUser,
		@idCertification = CUL.idCertification,
		@idUserTimezone = CASE WHEN U.idTimezone IS NOT NULL THEN U.idTimezone ELSE S.idTimezone END
	FROM tblCertificationToUserLink CUL
	LEFT JOIN tblUser U ON U.idUser = CUL.idUser
	LEFT JOIN tblSite S ON S.idSite = U.idSite
	WHERE CUL.idCertificationToUserLink = @idCertificationToUserLink

	EXEC [Certificate.EvaluateCompletion] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idUser, @idCertification, 'certification', 'revoked', @idUserTimezone

	/*

	RETURN

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO