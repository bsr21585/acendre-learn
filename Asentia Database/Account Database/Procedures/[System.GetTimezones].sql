-- =====================================================================
-- PROCEDURE: [System.GetTimezones]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetTimezones]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetTimezones]
GO

/*

Returns a recordset of timezone ids and names. 

*/

CREATE PROCEDURE [System.GetTimezones]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0
)
AS

	BEGIN
	SET NOCOUNT ON
			
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString
		
	SELECT
		DISTINCT
		T.idTimezone, 
		CASE WHEN TL.displayName IS NOT NULL THEN TL.displayName ELSE T.displayName END AS displayName,
		T.dotNetName AS dotNetName
	FROM tblTimezone T
	LEFT JOIN tblTimezoneLanguage TL ON TL.idTimezone = T.idTimezone AND TL.idLanguage = @idCallerLanguage
	ORDER BY T.idTimezone
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	