-- =====================================================================
-- PROCEDURE: [User.LessonStatistics]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[User.LessonStatistics]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.LessonStatistics]
GO

/*

Gets lesson statistics across all lessons for a user.

*/

CREATE PROCEDURE [User.LessonStatistics]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idUser					INT,
	@modulesPassed			INT				OUTPUT,
	@modulesFailed			INT				OUTPUT,
	@averageScore			INT				OUTPUT,	
	@totalTrainingTime		NVARCHAR(8)		OUTPUT,
	@averageTimePerLesson	NVARCHAR(8)		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	get module data for all enrollments belonging to the user

	*/

	CREATE TABLE #UserEnrollmentLessonData (
		completionStatus	INT,
		successStatus		INT,
		scoreScaled			FLOAT,
		totalTime			FLOAT
	)

	INSERT INTO #UserEnrollmentLessonData (
		completionStatus,
		successStatus,
		scoreScaled,
		totalTime
	)
	SELECT
		SCO.completionStatus,
		SCO.successStatus,
		SCO.scoreScaled,
		SCO.totalTime
	FROM tblEnrollment E
	LEFT JOIN [tblData-Lesson] LD ON LD.idEnrollment = E.idEnrollment
	LEFT JOIN tblLesson L ON L.idLesson = LD.idLesson
	LEFT JOIN [tblData-SCO] SCO ON SCO.[idData-Lesson] = LD.[idData-Lesson]
	WHERE E.idUser = @idUser
	AND (L.isDeleted = 0 OR L.isDeleted IS NULL)
	AND SCO.completionStatus IS NOT NULL
	AND SCO.successStatus IS NOT NULL

	/*

	get the total number of modules passed

	*/
	
	SELECT @modulesPassed = COUNT(1) FROM #UserEnrollmentLessonData LD WHERE LD.successStatus = 4

	/*

	get the total number of modules failed

	*/
	
	SELECT @modulesFailed = COUNT(1) FROM #UserEnrollmentLessonData LD WHERE LD.successStatus = 5
	
	/*

	get the average score of all modules

	*/

	SELECT @averageScore = CONVERT(INT, ROUND((AVG(LD.scoreScaled) * 100), 0)) FROM #UserEnrollmentLessonData LD WHERE LD.scoreScaled IS NOT NULL
	
	/*

	get the total training time across all modules

	*/

	SELECT @totalTrainingTime = CONVERT(NVARCHAR(8), DATEADD(SECOND, SUM(LD.totalTime), 0), 108) FROM #UserEnrollmentLessonData LD WHERE LD.totalTime IS NOT NULL
			
	/*

	get the average total time over all lessons

	*/

	SELECT @averageTimePerLesson = CONVERT(NVARCHAR(8), DATEADD(SECOND, AVG(LD.totalTime), 0), 108) FROM #UserEnrollmentLessonData LD WHERE LD.totalTime IS NOT NULL
		
	-- DROP THE TEMPORARY TABLES
	DROP TABLE #UserEnrollmentLessonData		

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO