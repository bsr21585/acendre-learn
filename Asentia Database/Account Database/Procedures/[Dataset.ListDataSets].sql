-- =====================================================================
-- PROCEDURE: [DataSet.ListDataSets]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DataSet.ListDataSets]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DataSet.ListDataSets]
GO

CREATE PROCEDURE [DataSet.ListDataSets]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDataset

		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataSetListDataSets_NoRecordFound'
		RETURN 1
		END

    /*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'DataSetListDataSets_LanguageDoesNotExist'
		RETURN 1 
		END
	
	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	/*
	
	get the data 
	
	*/
		
	SELECT
		DS.idSite,
		DS.idDataset,
		CASE WHEN DSL.name IS NULL OR DSL.name = '' THEN DS.name  ELSE DSL.name END AS name,
		DSL.[description]
	FROM tblDataset DS
	LEFT JOIN tblDatasetLanguage DSL ON DSL.idDataset = DS.idDataset AND DSL.idLanguage = @idLanguage
	WHERE DS.idSite = 1 OR DS.idSite = @idCallerSite
	ORDER BY DS.[order]

	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'DataSetListDataSets_NoRecordFound'
		END
	ELSE
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO