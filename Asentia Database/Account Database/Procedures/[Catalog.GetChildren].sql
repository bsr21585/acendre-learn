-- =====================================================================
-- PROCEDURE: [Catalog.GetChildren]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.GetChildren]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.GetChildren]
GO

/*
Return all the children catalog for a particulare parent catalog id
*/
CREATE PROCEDURE [dbo].[Catalog.GetChildren]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (255)	OUTPUT,
	@idCaller					INT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR (10),
	@idParentCatalog			INT,
	@languageString				NVARCHAR (10),
	@showPrivateCatalog         BIT             = 0
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	/*
	
	get the data 
	
	*/
		
	;WITH CatalogTable AS
	(
		SELECT
			C.idCatalog,
			CASE WHEN CL.title IS NULL OR CL.title = '' THEN C.title ELSE CL.title END AS title,
			C.idParent,
			C.[order],
			COUNT(cChildren.idCatalog) as catalogCount,
			C.isPrivate,
			CASE WHEN (
						SELECT COUNT(1) FROM tblUserToGroupLink UL 
						WHERE UL.idUser = @idCaller 
						AND UL.idGroup IN (SELECT CL.idGroup
										   FROM tblCatalogAccessToGroupLink CL 
										   WHERE CL.idCatalog = C.idCatalog)
					  ) > 0 OR @idCaller = 1 OR C.isPrivate = 0 THEN 'True' ELSE 'False' END AS isAccessible
		FROM tblCatalog C
		LEFT JOIN tblCatalog cChildren ON cChildren.idParent = c.idCatalog
		LEFT JOIN tblCatalogLanguage CL ON CL.idCatalog = C.idCatalog AND CL.idLanguage = @idCallerLanguage
		LEFT JOIN tblCatalogAccessToGroupLink CGL ON C.idCatalog = CGL.idCatalog
		WHERE C.idParent = @idParentCatalog
		AND C.idSite = @idCallerSite
		AND (
				(
					(SELECT COUNT(1) FROM tblUserToGroupLink UL 
					 WHERE UL.idUser = @idCaller 
					 AND UL.idGroup IN (SELECT CL.idGroup
									    FROM tblCatalogAccessToGroupLink CL 
									    WHERE CL.idCatalog = C.idCatalog)) > 0
					OR @idCaller = 1 OR C.isPrivate = 0
				)
				OR
				(@showPrivateCatalog = 1 AND C.isPrivate = 1)
			)
		GROUP BY C.idCatalog, CL.title, C.title, C.idParent, C.[order], C.isPrivate, CGL.idGroup
	)
	
	SELECT
		CT.idCatalog,
		CT.title,
		CT.idParent,
		CT.[order],
		CT.catalogCount,
		COUNT(CCL.idCourse) AS courseCount,
		CT.isPrivate,
		CT.isAccessible
	FROM CatalogTable CT
	LEFT JOIN tblCourseToCatalogLink CCL ON CCL.idCatalog = CT.idCatalog
	GROUP BY CT.idCatalog, CT.title, CT.idParent, CT.[order], CT.catalogCount, CT.isPrivate, CT.isAccessible
	ORDER BY CT.[order], CT.title
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO