-- =====================================================================
-- PROCEDURE: [Certification.EnrollmentStatistics]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Certification.EnrollmentStatistics]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.EnrollmentStatistics]
GO

/*

Gets enrollment statistics for a certification.

*/

CREATE PROCEDURE [Certification.EnrollmentStatistics]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idCertification		INT
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	put all certification enrollments into a temporary table, we'll work from that table

	*/

	CREATE TABLE #CertificationEnrollments (
		idCertificationToUserLink	INT,
		idCertification				INT,
		initialAwardDate			DATETIME,
		currentExpirationDate		DATETIME,
		lastExpirationDate			DATETIME		
	)

	INSERT INTO #CertificationEnrollments (
		idCertificationToUserLink,
		idCertification,
		initialAwardDate,
		currentExpirationDate,
		lastExpirationDate
	)
	SELECT
		CUL.idCertificationToUserLink,
		CUL.idCertification,
		CUL.initialAwardDate,
		CUL.currentExpirationDate,
		CUL.lastExpirationDate
	FROM tblCertificationToUserLink CUL
	LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
	LEFT JOIN tblUser U ON U.idUser = CUL.idUser
	WHERE CUL.idSite = @idCallerSite
	AND CUL.idCertification = @idCertification
	AND (C.isDeleted = 0 OR C.isDeleted IS NULL)
	AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)) -- enrollments for deleted and unapproved users should not be reflected
	AND U.isActive = 1 -- only active users should be included	
		
	/*

	CERTIFICATION ENROLLMENT STATISTICS BY STATUS

	*/

	SELECT
		(SELECT COUNT(1) FROM #CertificationEnrollments CE WHERE CE.initialAwardDate IS NULL AND CE.currentExpirationDate IS NULL) AS [##NotEarned##],
		(SELECT COUNT(1) FROM #CertificationEnrollments CE WHERE CE.initialAwardDate IS NOT NULL AND CE.currentExpirationDate IS NOT NULL AND CE.currentExpirationDate > GETUTCDATE()) AS [##Active##],
		(SELECT COUNT(1) FROM #CertificationEnrollments CE WHERE CE.initialAwardDate IS NOT NULL AND CE.currentExpirationDate IS NOT NULL AND CE.currentExpirationDate >= GETUTCDATE()) AS [##Expired##],
		(SELECT COUNT(1) FROM #CertificationEnrollments CE) AS _Total_
		
	-- DROP THE TEMPORARY TABLES
	DROP TABLE #CertificationEnrollments		

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO