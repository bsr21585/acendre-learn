-- =====================================================================
-- PROCEDURE: [Role.GetUsers]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Role.GetUsers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Role.GetUsers]
GO

/*

Returns a recordset of user ids and names that are members of a role.

*/

CREATE PROCEDURE [Role.GetUsers]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idRole					INT,
	@excludeAutoJoined		BIT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		IF @excludeAutoJoined = 1

			BEGIN

			SELECT
				DISTINCT
				U.idUser, 
				U.displayName + ' (' + U.username + ')' AS displayName,
				(SELECT COUNT(1) FROM tblUserToRoleLink RULES_URL WHERE RULES_URL.idRole = @idRole AND RULES_URL.idUser = U.idUser AND RULES_URL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblUser U
			LEFT JOIN tblUserToRoleLink URL ON URL.idUser = U.idUser
			WHERE U.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblUserToRoleLink URL
						WHERE URL.idRole = @idRole
						AND URL.idUser = U.idUser
						AND URL.idRuleSet IS NULL)
			ORDER BY displayName

			END

		ELSE

			BEGIN

			SELECT
				DISTINCT
				U.idUser, 
				U.displayName + ' (' + U.username + ')' AS displayName,
				(SELECT COUNT(1) FROM tblUserToRoleLink RULES_URL WHERE RULES_URL.idRole = @idRole AND RULES_URL.idUser = U.idUser AND RULES_URL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblUser U
			LEFT JOIN tblUserToRoleLink URL ON URL.idUser = U.idUser
			WHERE U.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblUserToRoleLink URL
						WHERE URL.idRole = @idRole
						AND URL.idUser = U.idUser)
			ORDER BY displayName

			END

		END

	ELSE

		BEGIN

		IF @excludeAutoJoined = 1

			BEGIN

			SELECT
				DISTINCT
				U.idUser, 
				U.displayName + ' (' + U.username + ')' AS displayName,
				(SELECT COUNT(1) FROM tblUserToRoleLink RULES_URL WHERE RULES_URL.idRole = @idRole AND RULES_URL.idUser = U.idUser AND RULES_URL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblUser U
			INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = U.idUser
			LEFT JOIN tblUserToRoleLink URL ON URL.idUser = U.idUser
			WHERE U.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblUserToRoleLink URL
						WHERE URL.idRole = @idRole
						AND URL.idUser = U.idUser
						AND URL.idRuleSet IS NULL)
			ORDER BY displayName

			END

		ELSE

			BEGIN

			SELECT
				DISTINCT
				U.idUser, 
				U.displayName + ' (' + U.username + ')' AS displayName,
				(SELECT COUNT(1) FROM tblUserToRoleLink RULES_URL WHERE RULES_URL.idRole = @idRole AND RULES_URL.idUser = U.idUser AND RULES_URL.idRuleSet IS NOT NULL) AS numRulesJoinedBy
			FROM tblUser U
			INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = U.idUser
			LEFT JOIN tblUserToRoleLink URL ON URL.idUser = U.idUser
			WHERE U.idSite = @idCallerSite
			AND EXISTS (SELECT 1 FROM tblUserToRoleLink URL
						WHERE URL.idRole = @idRole
						AND URL.idUser = U.idUser)
			ORDER BY displayName

			END

		END
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	