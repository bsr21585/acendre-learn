-- =====================================================================
-- PROCEDURE: [InboxMessage.Send]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[InboxMessage.Send]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [InboxMessage.Send]
GO

/*

Marks the message as 'sent'

*/

CREATE PROCEDURE [InboxMessage.Send]
(
	@Return_Code				INT						OUTPUT,
	@Error_Description_Code		NVARCHAR(50)			OUTPUT,
	@idCallerSite				INT				= 0, 
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0, 
	
	@idInboxMessage				INT						OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the caller is the same person as the sender.
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblInboxMessage
		WHERE idInboxMessage = @idInboxMessage
		AND idSender = @idCaller
		) = 0
		
		BEGIN
		SELECT @Return_Code = 3
		SET @Error_Description_Code = 'InboxMessage_PermissionError'
		RETURN 1
		END
	
	/*
	
	send the message
	
	*/
		
	UPDATE tblInboxMessage SET 
		dtSent = GETUTCDATE(),  --dtSent must be updated 
		isDraft = 0,
		isSent = 1
	WHERE idInboxMessage = @idInboxMessage
		
	END
		
