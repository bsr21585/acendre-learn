-- =====================================================================
-- PROCEDURE: [EventLog.GetGrid]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[EventLog.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventLog.GetGrid]
GO

/*

Gets a listing of all event log entries for a site.
This returns only event log entries explicitly written to the event log,
it does not include "dynamic events" such as enrollment expiration, etc.

*/
CREATE PROCEDURE [EventLog.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idSite					INT, 
	@searchParam			NVARCHAR(255),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		CREATE TABLE #EventLog (
			idEventLog				INT,
			idSite					INT,
			idEventType				INT,
			idTargetUser			INT,
			objectName				NVARCHAR(1500),
			targetUser				NVARCHAR(1500),
			executingUser			NVARCHAR(1500),
			dtAction				DATETIME
		)

		-- User Events
		INSERT INTO #EventLog (
			idEventLog,
			idSite,
			idEventType,
			idTargetUser,
			objectName,
			targetUser,
			executingUser,
			dtAction
		)
		  SELECT 
				E.idEventLog,				
				E.idSite,
				E.idEventType,
				E.idObjectUser,
				CASE WHEN E.idObject IS NULL THEN '[N/A]' ELSE O.displayName + ' (' + O.username + ')' END AS objectName,
				CASE WHEN E.idObjectUser IS NULL THEN '[N/A]' ELSE U.displayName + ' (' + U.username + ')' END AS targetUser,
				CASE WHEN E.idExecutingUser IS NULL THEN '[N/A]' WHEN E.idExecutingUser = 1 THEN 'Administrator' ELSE EXU.displayName + ' (' + EXU.username + ')' END AS executingUser,
				E.[timestamp] AS dtAction
			FROM tblEventLog E
			LEFT JOIN tblUser O ON O.idUser = E.idObject
			LEFT JOIN tblUser U ON U.idUser = E.idObjectUser
			LEFT JOIN tblUser EXU ON EXU.idUser = E.idExecutingUser
			WHERE E.idEventType > 100 AND E.idEventType < 200
			AND (
					@idSite IS NULL -- all sites
					OR
					@idSite = E.idSite
				)

		-- Course Events
		INSERT INTO #EventLog (
			idEventLog,
			idSite,
			idEventType,
			idTargetUser,
			objectName,
			targetUser,
			executingUser,
			dtAction
		)
		  SELECT 
				E.idEventLog,				
				E.idSite,
				E.idEventType,
				E.idObjectUser,
				CASE WHEN E.idObject IS NULL THEN '[N/A]' ELSE O.title END AS objectName,
				CASE WHEN E.idObjectUser IS NULL THEN '[N/A]' ELSE U.displayName + ' (' + U.username + ')' END AS targetUser,
				CASE WHEN E.idExecutingUser IS NULL THEN '[N/A]' WHEN E.idExecutingUser = 1 THEN 'Administrator' ELSE EXU.displayName + ' (' + EXU.username + ')' END AS executingUser,
				E.[timestamp] AS dtAction
			FROM tblEventLog E
			LEFT JOIN tblCourse O ON O.idCourse = E.idObject
			LEFT JOIN tblUser U ON U.idUser = E.idObjectUser
			LEFT JOIN tblUser EXU ON EXU.idUser = E.idExecutingUser
			WHERE E.idEventType > 200 AND E.idEventType < 300
			AND (
					@idSite IS NULL -- all sites
					OR
					@idSite = E.idSite
				)

		-- Lesson Events
		INSERT INTO #EventLog (
			idEventLog,
			idSite,
			idEventType,
			idTargetUser,
			objectName,
			targetUser,
			executingUser,
			dtAction
		)
		  SELECT 
				E.idEventLog,				
				E.idSite,
				E.idEventType,
				E.idObjectUser,
				CASE WHEN E.idObject IS NULL THEN '[N/A]' ELSE O.title END AS objectName,
				CASE WHEN E.idObjectUser IS NULL THEN '[N/A]' ELSE U.displayName + ' (' + U.username + ')' END AS targetUser,
				CASE WHEN E.idExecutingUser IS NULL THEN '[N/A]' WHEN E.idExecutingUser = 1 THEN 'Administrator' ELSE EXU.displayName + ' (' + EXU.username + ')' END AS executingUser,
				E.[timestamp] AS dtAction
			FROM tblEventLog E
			LEFT JOIN tblLesson O ON O.idLesson = E.idObject
			LEFT JOIN tblUser U ON U.idUser = E.idObjectUser
			LEFT JOIN tblUser EXU ON EXU.idUser = E.idExecutingUser
			WHERE E.idEventType > 300 AND E.idEventType < 400
			AND (
					@idSite IS NULL -- all sites
					OR
					@idSite = E.idSite
				)

		-- Session Events
		INSERT INTO #EventLog (
			idEventLog,
			idSite,
			idEventType,
			idTargetUser,
			objectName,
			targetUser,
			executingUser,
			dtAction
		)
		  SELECT 
				E.idEventLog,				
				E.idSite,
				E.idEventType,
				E.idObjectUser,
				CASE WHEN E.idObjectRelated IS NULL THEN '[N/A]' ELSE O.title END AS objectName,
				CASE WHEN E.idObjectUser IS NULL THEN '[N/A]' ELSE U.displayName + ' (' + U.username + ')' END AS targetUser,
				CASE WHEN E.idExecutingUser IS NULL THEN '[N/A]' WHEN E.idExecutingUser = 1 THEN 'Administrator' ELSE EXU.displayName + ' (' + EXU.username + ')' END AS executingUser,
				E.[timestamp] AS dtAction
			FROM tblEventLog E
			LEFT JOIN tblStandupTrainingInstance O ON O.idStandUpTrainingInstance = E.idObjectRelated
			LEFT JOIN tblUser U ON U.idUser = E.idObjectUser
			LEFT JOIN tblUser EXU ON EXU.idUser = E.idExecutingUser
			WHERE E.idEventType > 400 AND E.idEventType < 500
			AND (
					@idSite IS NULL -- all sites
					OR
					@idSite = E.idSite
				)

		-- Learning Path Events
		INSERT INTO #EventLog (
			idEventLog,
			idSite,
			idEventType,
			idTargetUser,
			objectName,
			targetUser,
			executingUser,
			dtAction
		)
		  SELECT 
				E.idEventLog,				
				E.idSite,
				E.idEventType,
				E.idObjectUser,
				CASE WHEN E.idObject IS NULL THEN '[N/A]' ELSE O.name END AS objectName,
				CASE WHEN E.idObjectUser IS NULL THEN '[N/A]' ELSE U.displayName + ' (' + U.username + ')' END AS targetUser,
				CASE WHEN E.idExecutingUser IS NULL THEN '[N/A]' WHEN E.idExecutingUser = 1 THEN 'Administrator' ELSE EXU.displayName + ' (' + EXU.username + ')' END AS executingUser,
				E.[timestamp] AS dtAction
			FROM tblEventLog E
			LEFT JOIN tblLearningPath O ON O.idLearningPath = E.idObject
			LEFT JOIN tblUser U ON U.idUser = E.idObjectUser
			LEFT JOIN tblUser EXU ON EXU.idUser = E.idExecutingUser
			WHERE E.idEventType > 500 AND E.idEventType < 600
			AND (
					@idSite IS NULL -- all sites
					OR
					@idSite = E.idSite
				)

		-- Certification Events
		INSERT INTO #EventLog (
			idEventLog,
			idSite,
			idEventType,
			idTargetUser,
			objectName,
			targetUser,
			executingUser,
			dtAction
		)
		  SELECT 
				E.idEventLog,				
				E.idSite,
				E.idEventType,
				E.idObjectUser,
				CASE WHEN E.idObject IS NULL THEN '[N/A]' ELSE C.title END AS objectName,
				CASE WHEN E.idObjectUser IS NULL THEN '[N/A]' ELSE U.displayName + ' (' + U.username + ')' END AS targetUser,
				CASE WHEN E.idExecutingUser IS NULL THEN '[N/A]' WHEN E.idExecutingUser = 1 THEN 'Administrator' ELSE EXU.displayName + ' (' + EXU.username + ')' END AS executingUser,
				E.[timestamp] AS dtAction
			FROM tblEventLog E
			LEFT JOIN tblCertification C ON C.idCertification = E.idObject
			LEFT JOIN tblUser U ON U.idUser = E.idObjectUser
			LEFT JOIN tblUser EXU ON EXU.idUser = E.idExecutingUser
			WHERE E.idEventType > 600 AND E.idEventType < 700
			AND (
					@idSite IS NULL -- all sites
					OR
					@idSite = E.idSite
				)

		-- Certificate Events
		INSERT INTO #EventLog (
			idEventLog,
			idSite,
			idEventType,
			idTargetUser,
			objectName,
			targetUser,
			executingUser,
			dtAction
		)
		  SELECT 
				E.idEventLog,				
				E.idSite,
				E.idEventType,
				E.idObjectUser,
				CASE WHEN E.idObject IS NULL THEN '[N/A]' ELSE O.name END AS objectName,
				CASE WHEN E.idObjectUser IS NULL THEN '[N/A]' ELSE U.displayName + ' (' + U.username + ')' END AS targetUser,
				CASE WHEN E.idExecutingUser IS NULL THEN '[N/A]' WHEN E.idExecutingUser = 1 THEN 'Administrator' ELSE EXU.displayName + ' (' + EXU.username + ')' END AS executingUser,
				E.[timestamp] AS dtAction
			FROM tblEventLog E
			LEFT JOIN tblCertificate O ON O.idCertificate = E.idObject
			LEFT JOIN tblUser U ON U.idUser = E.idObjectUser
			LEFT JOIN tblUser EXU ON EXU.idUser = E.idExecutingUser
			WHERE E.idEventType > 700 AND E.idEventType < 800
			AND (
					@idSite IS NULL -- all sites
					OR
					@idSite = E.idSite
				)
		
		IF @searchParam = '' OR @searchParam IS NULL
			BEGIN
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #EventLog EVL
			LEFT JOIN tblEventType ET ON ET.idEventType = EVL.idEventType
			WHERE (@searchParam IS NULL)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idEventLog,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'idEventType' THEN EVL.idEventType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'objectName' THEN objectName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'targetUser' THEN targetUser END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'executingUser' THEN executingUser END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtAction' THEN dtAction END) END DESC,

							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'idEventType' THEN EVL.idEventType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'objectName' THEN objectName END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'targetUser' THEN targetUser END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'executingUser' THEN executingUser END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtAction' THEN dtAction END) END
						)
						AS [row_number]
					FROM #EventLog EVL
					LEFT JOIN tblEventType ET ON ET.idEventType = EVL.idEventType
					WHERE (@searchParam IS NULL)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idEventLog, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
				SELECT 
				EVL.idEventLog, 
				EVL.idSite,
				EVL.idEventType,
				ET.name AS eventType,
				EVL.objectName,
				EVL.targetUser,
				EVL.executingUser,
				EVL.dtAction,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #EventLog EVL ON EVL.idEventLog = SelectedKeys.idEventLog
			LEFT JOIN tblEventType ET ON ET.idEventType = EVL.idEventType
			ORDER BY SelectedKeys.[row_number]
			END
		ELSE
			BEGIN
				
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #EventLog EVL
			INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = EVL.idTargetUser
			LEFT JOIN tblEventType ET ON ET.idEventType = EVL.idEventType
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idEventLog,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'idEventType' THEN EVL.idEventType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'objectName' THEN objectName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'targetUser' THEN targetUser END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'executingUser' THEN executingUser END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtAction' THEN dtAction END) END DESC,

							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'idEventType' THEN EVL.idEventType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'objectName' THEN objectName END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'targetUser' THEN targetUser END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'executingUser' THEN executingUser END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtAction' THEN dtAction END) END
						)
						AS [row_number]
					FROM #EventLog EVL
					INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = EVL.idTargetUser
					LEFT JOIN tblEventType ET ON ET.idEventType = EVL.idEventType
				), 
				
			SelectedKeys AS (
				SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
					idEventLog, 
					[row_number]
				FROM Keys
				WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
			)
		SELECT 
			EVL.idEventLog, 
			EVL.idSite,
			EVL.idEventType,
			ET.name AS eventType,
			EVL.objectName,
			EVL.targetUser,
			EVL.executingUser,
			EVL.dtAction,
			SelectedKeys.[row_number]
		FROM SelectedKeys
		JOIN #EventLog EVL ON EVL.idEventLog = SelectedKeys.idEventLog
		LEFT JOIN tblEventType ET ON ET.idEventType = EVL.idEventType
		ORDER BY SelectedKeys.[row_number]
		END
			
	END		
			
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO