-- =====================================================================
-- PROCEDURE: [Session.GetTimezoneInformation]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Session.GetTimezoneInformation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Session.GetTimezoneInformation]
GO

/*

Gets timezone information to store in session for the user.

*/

CREATE PROCEDURE [Session.GetTimezoneInformation]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@timezoneDotNetName		NVARCHAR(255)	OUTPUT,
	@timezoneGMTOffset		FLOAT			OUTPUT, 
	@timezoneDisplayName	NVARCHAR(255)	OUTPUT,
	@utcNow					DATETIME		OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	make sure that there is a site, die if not
	
	*/
	
	IF @idCallerSite = 0
	
		BEGIN
		SET @Return_Code = 1 -- sort of (no site, so no timezone).
		SET @Error_Description_Code = 'SessionGetTimezoneInformation_DetailsNotFound'
		RETURN 1
		END

	/*

	get the id of the caller's language

	*/

	DECLARE @idLanguage INT
	SELECT @idLanguage =
		CASE WHEN idLanguage IS NULL THEN 
			57
		ELSE 
			idLanguage 
		END
		FROM tblLanguage WHERE code = @callerLangString

	/*

	if this is an actual user, get the user's timezone information

	*/

	IF @idCaller > 1

		BEGIN
		
		SELECT
			@timezoneDotNetName = TZ.dotNetName,
			@timezoneGMTOffset = TZ.gmtOffset,
			@timezoneDisplayName = 
				CASE WHEN TZL.displayName IS NOT NULL THEN 
					TZL.displayName
				ELSE
					TZ.displayName
				END
		FROM tblUser U
		LEFT JOIN tblTimezone TZ ON TZ.idTimezone = U.idTimezone
		LEFT JOIN tblTimezoneLanguage TZL ON TZL.idLanguage = @idLanguage AND TZL.idTimezone = U.idTimezone
		WHERE U.idUser = @idCaller

		END

	/*

	if this is before a user's session is established, or
	the user is the administrator, or we could not get timezone
	information for the user, get the timezone information from
	the site

	*/

	IF (@idCaller IS NULL OR @idCaller < 2) OR (@timezoneDotNetName IS NULL OR @timezoneGMTOffset IS NULL OR @timezoneDisplayName IS NULL)

		BEGIN

		SELECT
			@timezoneDotNetName = TZ.dotNetName,
			@timezoneGMTOffset = TZ.gmtOffset,
			@timezoneDisplayName = 
				CASE WHEN TZL.displayName IS NOT NULL THEN 
					TZL.displayName
				ELSE
					TZ.displayName
				END
		FROM tblSite S
		LEFT JOIN tblTimezone TZ ON TZ.idTimezone = S.idTimezone
		LEFT JOIN tblTimezoneLanguage TZL ON TZL.idLanguage = @idLanguage AND TZL.idTimezone = S.idTimezone
		WHERE S.idSite = @idCallerSite

		END

	/*

	if all else fails, use default timezone of EST

	*/

	IF (@timezoneDotNetName IS NULL OR @timezoneGMTOffset IS NULL OR @timezoneDisplayName IS NULL)

		BEGIN

		SELECT
			@timezoneDotNetName = TZ.dotNetName,
			@timezoneGMTOffset = TZ.gmtOffset,
			@timezoneDisplayName = 
				CASE WHEN TZL.displayName IS NOT NULL THEN 
					TZL.displayName
				ELSE
					TZ.displayName
				END
		FROM tblTimezone TZ
		LEFT JOIN tblTimezoneLanguage TZL ON TZL.idLanguage = @idLanguage AND TZL.idTimezone = TZ.idTimezone
		WHERE TZ.idTimezone = 15

		END

	/*

	get utc now

	*/

	SELECT @utcNow = GETUTCDATE()
			
	SELECT @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
