-- =====================================================================
-- PROCEDURE: [Certification.GetGridForUserDashboard]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Certification.GetGridForUserDashboard]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.GetGridForUserDashboard]
GO

/*

Gets a listing of user certifications for display on user's dashboard. 

*/

CREATE PROCEDURE [Certification.GetGridForUserDashboard]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT	
)
AS
	BEGIN
		SET NOCOUNT ON	

		CREATE TABLE #Certifications (
			idCertificationToUserLink				INT,
			idCertification							INT,
			idUser									INT,			
			title									NVARCHAR(255),
			[status]								NVARCHAR(20),
			currentExpirationDate					DATETIME
		)

		/*

		get the language id from the caller language string
	
		*/

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		insert into the temp table

		*/

		INSERT INTO #Certifications (
			idCertificationToUserLink,
			idCertification,
			idUser,			
			title,
			[status],
			currentExpirationDate			
		)
		SELECT
			CUL.idCertificationToUserLink,
			CUL.idCertification,
			CUL.idUser,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END AS title,			
			CASE WHEN CUL.initialAwardDate IS NOT NULL AND CUL.currentExpirationDate IS NOT NULL THEN
				CASE WHEN GETUTCDATE() < CUL.currentExpirationDate THEN
					'1'
				ELSE
					'2'
				END
			ELSE
				'0'
			END AS [status],
			currentExpirationDate
		FROM tblCertificationToUserLink CUL
		LEFT JOIN tblCertification C ON C.idCertification = CUL.idCertification
		LEFT JOIN tblCertificationLanguage CL ON CL.idCertification = C.idCertification AND CL.idLanguage = @idCallerLanguage
		WHERE CUL.idUser = @idCaller		
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END
		
		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #Certifications C
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						C.idCertificationToUserLink,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'currentExpirationDate' THEN currentExpirationDate END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'status' THEN [status] END) END DESC,							
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('currentExpirationDate', 'status') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN [status] END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN currentExpirationDate END) END DESC,							
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'currentExpirationDate' THEN currentExpirationDate END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'status' THEN [status] END) END,							
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN title END) END,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('currentExpirationDate', 'status') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN [status] END) END,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN currentExpirationDate END) END
						)
						AS [row_number]
					FROM #Certifications C
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idCertificationToUserLink, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				C.idCertificationToUserLink,
				C.idCertification,
				C.idUser,			
				C.title,
				C.[status],
				C.currentExpirationDate,
				CONVERT(BIT, 1) AS [view],				
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #Certifications C ON C.idCertificationToUserLink = SelectedKeys.idCertificationToUserLink
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #Certifications C
			INNER JOIN CONTAINSTABLE(tblCertification, *, @searchParam) K ON K.[key] = C.idCertification
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						C.idCertificationToUserLink,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'currentExpirationDate' THEN currentExpirationDate END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'status' THEN [status] END) END DESC,							
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('currentExpirationDate', 'status') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN [status] END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN currentExpirationDate END) END DESC,							
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'currentExpirationDate' THEN currentExpirationDate END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'status' THEN [status] END) END,							
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN title END) END,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('currentExpirationDate', 'status') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN [status] END) END,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('currentExpirationDate', 'status') THEN currentExpirationDate END) END
						)
						AS [row_number]
					FROM #Certifications C
					INNER JOIN CONTAINSTABLE(tblCertification, *, @searchParam) K ON K.[key] = C.idCertification
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idCertificationToUserLink, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				C.idCertificationToUserLink,
				C.idCertification,
				C.idUser,			
				C.title,
				C.[status],
				C.currentExpirationDate,
				CONVERT(BIT, 1) AS [view],				
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #Certifications C ON C.idCertificationToUserLink = SelectedKeys.idCertificationToUserLink			
			ORDER BY SelectedKeys.[row_number]
			
			END

		-- DROP THE TEMPORARY TABLE
		DROP TABLE #Certifications
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO