-- =====================================================================
-- PROCEDURE: [Lesson.SaveContent]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Lesson.SaveContent]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Lesson.SaveContent]
GO

/*

Saves lesson to content linkage.

*/

CREATE PROCEDURE [Lesson.SaveContent]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idLesson							INT,
	@idObject							INT,
	@idContentType						INT,
	@idTaskDocumentType					INT,
	@taskResourcePath					NVARCHAR(255),
	@allowSupervisorsAsProctor			BIT,
	@allowCourseExpertsAsProctor		BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the lesson exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLesson
		WHERE idLesson = @idLesson
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LessonSaveContent_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	update/insert the content in the content table - we do updates and inserts one at a time based on content type 
	now because right now we only one of each content type per lesson; if/when we allow more than one of each, we will 
	end up wiping out existing content links, then saving all content links as new at once

	*/

	IF (SELECT COUNT(1) FROM tblLessonToContentLink LCL WHERE LCL.idLesson = @idLesson AND LCL.idContentType = @idContentType) > 0
	
		BEGIN

		UPDATE tblLessonToContentLink SET
			idObject = @idObject,
			idAssignmentDocumentType = @idTaskDocumentType,
			assignmentResourcePath = @taskResourcePath,
			allowSupervisorsAsProctor = @allowSupervisorsAsProctor,
			allowCourseExpertsAsProctor = @allowCourseExpertsAsProctor
		WHERE idLesson = @idLesson
		AND idContentType = @idContentType

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblLessonToContentLink (
			idSite,
			idLesson,
			idObject,
			idContentType,
			idAssignmentDocumentType,
			assignmentResourcePath,
			allowSupervisorsAsProctor,
			allowCourseExpertsAsProctor
		)
		SELECT
			@idCallerSite,
			@idLesson,
			@idObject,
			@idContentType,
			@idTaskDocumentType,
			@taskResourcePath,
			@allowSupervisorsAsProctor,
			@allowCourseExpertsAsProctor
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblLessonToContentLink LCL
			WHERE LCL.idLesson = @idLesson
			AND LCL.idContentType = @idContentType
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO