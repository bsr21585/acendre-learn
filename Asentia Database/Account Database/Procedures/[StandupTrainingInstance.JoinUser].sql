-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.JoinUser]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[StandupTrainingInstance.JoinUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.JoinUser]
GO

/*

Joins a user to a standup training instance.

*/

CREATE PROCEDURE [StandupTrainingInstance.JoinUser]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT,
	@idUser						INT,
	@isWaitingList				BIT,
	@promotedFromWaitlist		BIT,
	@registrantKey				BIGINT,
	@registrantEmail			NVARCHAR(255),
	@joinUrl					NVARCHAR(1024)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the user exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblUser
		WHERE idSite = @idCallerSite
		AND idUser = @idUser
		) <> 1
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'StandupTrainingInstanceJoinUser_UserNotFound'
		RETURN 1 
		END

	/*
	
	validate that the standup training module exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblStandUpTraining
		WHERE idSite = @idCallerSite
		AND idStandUpTraining = (SELECT idStandupTraining FROM tblStandUpTrainingInstance WHERE idStandUpTrainingInstance = @idStandupTrainingInstance)
		) <> 1
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'STIJoinUser_StandupTrainingModuleNotFound'
		RETURN 1 
		END

	/*
	
	validate that the standup training instance exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblStandUpTrainingInstance
		WHERE idSite = @idCallerSite
		AND idStandUpTrainingInstance = @idStandupTrainingInstance
		) <> 1
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'StandupTrainingInstanceJoinUser_NoRecordFound'
		RETURN 1 
		END

	/*

	validate that the user is not already joined to the instance

	*/

	IF (
		SELECT COUNT(1)
		FROM tblStandUpTrainingInstanceToUserLink
		WHERE idUser = @idUser
		AND idStandUpTrainingInstance = @idStandupTrainingInstance
		AND (isWaitingList IS NULL OR isWaitingList = 0)
		) <> 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'STIJoinUser_UserAlreadyJoinedToInstance'
		RETURN 1 
		END

	/*

	join the user to the instance, promote (update) or add (insert)

	*/

	IF @promotedFromWaitlist = 1

		BEGIN

		-- update the record
		UPDATE tblStandupTrainingInstanceToUserLink SET
			isWaitingList = @isWaitingList,
			registrantKey = @registrantKey,
			registrantEmail = @registrantEmail,
			joinUrl = @joinUrl,
			waitlistOrder = NULL
		WHERE idStandupTrainingInstance = @idStandupTrainingInstance
		AND idUser = @idUser
		AND isWaitingList = 1

		-- re-order the waiting list		
		UPDATE tblStandupTrainingInstanceToUserLink SET
			waitlistOrder = MAIN.rowNumber
		FROM (SELECT
			  ROW_NUMBER() OVER(ORDER BY waitlistOrder) AS rowNumber,
			  S.*
			  FROM tblStandupTrainingInstanceToUserLink S
			  WHERE S.idStandupTrainingInstance = @idStandupTrainingInstance
			  AND S.isWaitingList = 1) AS MAIN
		WHERE tblStandupTrainingInstanceToUserLink.idStandupTrainingInstanceToUserLink = MAIN.idStandupTrainingInstanceToUserLink

		END

	ELSE
		BEGIN
		
		-- get waitlist ordinal for new record added to waitlist
		DECLARE @waitlistOrder INT
		SET @waitlistOrder = NULL

		IF @isWaitingList = 1
			BEGIN
			SELECT @waitlistOrder = CASE WHEN MAX(waitlistOrder) IS NOT NULL THEN MAX(waitlistOrder) + 1 ELSE 1 END 
			FROM tblStandupTrainingInstanceToUserLink WHERE idStandupTrainingInstance = @idStandupTrainingInstance AND isWaitingList = 1
			END

		-- insert the new record
		INSERT INTO tblStandupTrainingInstanceToUserLink (
			idUser,
			idStandupTrainingInstance,
			isWaitingList,
			registrantKey,
			registrantEmail,
			joinUrl,
			waitlistOrder
		)
		VALUES (
			@idUser,
			@idStandupTrainingInstance,
			@isWaitingList,
			@registrantKey,
			@registrantEmail,
			@joinUrl,
			@waitlistOrder
		)

		END

	/*

	log session joined event for user joined

	*/

	DECLARE @learnerAddedEventLogItems EventLogItemObjects
	DECLARE @utcNow DATETIME

	SET @utcNow = GETUTCDATE()

	INSERT INTO @learnerAddedEventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idCallerSite,
		STI.idStandUpTraining,
		@idStandupTrainingInstance,
		@idUser	
	FROM tblStandUpTrainingInstance STI
	WHERE STI.idStandUpTrainingInstance = @idStandupTrainingInstance

	IF @promotedFromWaitlist = 1
		BEGIN
			EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 409, @utcNow, @learnerAddedEventLogItems	
		END
	ELSE IF @isWaitingList = 1
		BEGIN
			EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 407, @utcNow, @learnerAddedEventLogItems	
		END
	ELSE
		BEGIN
			EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 404, @utcNow, @learnerAddedEventLogItems	
		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

