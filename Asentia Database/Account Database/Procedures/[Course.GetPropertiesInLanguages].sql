-- =====================================================================
-- PROCEDURE: [Course.GetPropertiesInLanguages]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Course.GetPropertiesInLanguages]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.GetPropertiesInLanguages]
GO

/*

Return all language specific properties for a given course id.

*/

CREATE PROCEDURE [Course.GetPropertiesInLanguages]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCourse				INT
)
AS
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourse C
		WHERE idCourse = @idCourse
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseGetPropertiesInLanguages_DetailsNotFound'
		RETURN 1
		END
	
	SELECT 
		L.code as [langString],
		CL.title,
		CL.shortDescription,
		CL.longDescription,
		CL.objectives,
		CL.searchTags
	FROM tblCourseLanguage CL
	LEFT JOIN tblLanguage L ON L.idLanguage = CL.idLanguage
	WHERE CL.idCourse = @idCourse
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO