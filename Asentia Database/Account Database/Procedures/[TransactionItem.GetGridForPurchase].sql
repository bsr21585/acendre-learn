-- =====================================================================
-- PROCEDURE: [TransactionItem.GetGridForPurchase]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionItem.GetGridForPurchase]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionItem.GetGridForPurchase]
GO

/*

Gets a grid of all transaction items for a purchase id.
Used for My Cart and Purchase Receipts. 

*/

CREATE PROCEDURE [TransactionItem.GetGridForPurchase]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0,	-- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
				
	-- NOTE THAT WE ARE NOT USING THE SEARCH, PAGINATION, OR SORT HERE BUT
	-- SINCE THESE ARE COMMON PARAMETERS FOR GRIDS, WE NEED TO KEEP THEM
	@searchParam			NVARCHAR(4000),
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,

	@idPurchase				INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141	
		
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	/*

	begin getting the grid data

	*/

	SELECT COUNT(1) AS row_count
	FROM [tblTransactionItem] TI
	WHERE
		(
		(@idCallerSite IS NULL)
		OR 
		(@idCallerSite IS NOT NULL AND TI.idSite = @idCallerSite)
		)
	AND TI.idPurchase = @idPurchase
			
	SELECT
		TI.idTransactionItem,
		TI.idSite,
		TI.idPurchase,		
		TI.idUser,		
		TI.itemId,
		CASE 
			WHEN TI.itemType = 1 THEN
				CASE WHEN COL.title IS NOT NULL THEN COL.title ELSE TI.itemName END
			WHEN TI.itemType = 2 THEN
				CASE WHEN CAL.title IS NOT NULL THEN CAL.title ELSE TI.itemName END
			WHEN TI.itemType = 3 THEN
				CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE TI.itemName END
			WHEN TI.itemType = 4 THEN
				CASE WHEN STL.title IS NOT NULL THEN STL.title ELSE TI.itemName END
		ELSE TI.itemName END AS itemName,
		TI.itemType,
		CASE 
			WHEN TI.itemType = 1 THEN
				CASE WHEN COL.shortDescription IS NOT NULL THEN COL.title ELSE TI.[description] END
			WHEN TI.itemType = 2 THEN
				CASE WHEN CAL.shortDescription IS NOT NULL THEN CAL.title ELSE TI.[description] END
			WHEN TI.itemType = 3 THEN
				CASE WHEN LPL.shortDescription IS NOT NULL THEN LPL.name ELSE TI.[description] END
			WHEN TI.itemType = 4 THEN
				CASE WHEN STL.[description] IS NOT NULL THEN STL.title ELSE TI.[description] END
		ELSE TI.[description] END AS [description],
		TI.cost,
		TI.paid,
		TI.idCouponCode,
		TI.couponCode,
		CASE WHEN TI.idCouponCode IS NULL THEN 0 ELSE 1 END AS isCouponCodeApplied,
		CASE 
			WHEN TI.itemType = 2 THEN [dbo].GetCourseListForCatalogAsCommaSeperated(TI.itemID, @idCallerSite, @idCallerLanguage)
			WHEN TI.itemType = 3 THEN [dbo].GetCourseListForLearningPathAsCommaSeperated(TI.itemId, @idCallerSite, @idCallerLanguage)
		ELSE '' END AS includedCourses,
		ROW_NUMBER() OVER (ORDER BY TI.idTransactionItem) AS [row_number]
	FROM tblTransactionItem TI
	LEFT JOIN tblCourseLanguage COL ON COL.idCourse = TI.itemId AND COL.idLanguage = @idCallerLanguage AND TI.itemType = 1	
	LEFT JOIN tblCatalogLanguage CAL ON CAL.idCatalog = TI.itemId AND CAL.idLanguage = @idCallerLanguage AND TI.itemType = 2	
	LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = TI.itemId AND LPL.idLanguage = @idCallerLanguage AND TI.itemType = 3
	LEFT JOIN tblStandupTrainingInstance STI ON STI.idStandUpTrainingInstance = TI.itemId AND TI.itemType = 4
	LEFT JOIN tblStandUpTrainingLanguage STL ON STL.idStandUpTraining = STI.idStandUpTraining AND STL.idLanguage = @idCallerLanguage
	WHERE
		(
		(@idCallerSite IS NULL)
		OR
		(@idCallerSite IS NOT NULL AND TI.idSite = @idCallerSite)
		)
	AND TI.idPurchase = @idPurchase
	ORDER BY TI.idTransactionItem			
	
	SET @Return_Code = 0
	SET	@Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
