-- =====================================================================
-- PROCEDURE: [SCORMPackage.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[SCORMPackage.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SCORMPackage.Save]
GO

/*

Return all the properties for a given course id.

*/

CREATE PROCEDURE [SCORMPackage.Save]
(
    @Return_Code			INT				OUTPUT ,
    @Error_Description_Code NVARCHAR(50)	OUTPUT ,
    @idCallerSite			INT				= 0,
    @callerLangString		NVARCHAR(10),
    @idCaller				INT				= 0,
    
    @idSCORMPackage			INT OUTPUT ,
    @name					NVARCHAR(255),
    @path					NVARCHAR(MAX),
    @kb						INT,
    @idSCORMPackageType		INT,
    @imsManifest			NVARCHAR(MAX),
    @dtCreated				DATETIME,
    @dtModified				DATETIME
)
AS 
    BEGIN
        SET NOCOUNT ON

    DECLARE @idPermission INT -- define the permission required to perform this function
    SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	
	*/
	
        DECLARE @idCallerLanguage INT
        SELECT  @idCallerLanguage = idLanguage
        FROM    tblLanguage
        WHERE   code = @callerLangString
	
        IF ( SELECT COUNT(1)
             FROM   tblSite S
             WHERE  idSite = @idCallerSite
                    AND idLanguage = @idCallerLanguage
                    AND @idCallerLanguage IS NOT NULL
           ) <> 1 
            BEGIN 
                SELECT  @Return_Code = 5
                SET @Error_Description_Code = 'SCORMPackageSave_LanguageNotDefault' 
                RETURN 1 
            END
			 
	/*
	
	validate uniqueness
	
	*/
	
        --IF ( SELECT COUNT(1)
        --     FROM   tblSCORMPackage
        --     WHERE  idSite = @idCallerSite
        --            AND name = @name
        --            AND ( @idSCORMPackage IS NULL
        --                  OR @idSCORMPackage <> idSCORMPackage
        --                )
        --   ) > 0 
        --    BEGIN
        --        SELECT  @Return_Code = 2
        --        SET @Error_Description_Code = 'Field_NotUnique'
        --        RETURN 1 
        --    END
		
	/*
	
	save the data
	
	*/
	
        IF ( @idSCORMPackage = 0
             OR @idSCORMPackage IS NULL
           ) 
            BEGIN
		
		-- insert the new object
		
                INSERT  INTO dbo.tblSCORMPackage
                        ( [idSite] ,
                          [name] ,
                          [path] ,
                          [kb] ,
                          [idSCORMPackageType] ,
                          [imsManifest] ,
                          [dtCreated] ,
                          [dtModified]
                        )
                VALUES  ( @idCallerSite , -- idSite - int
                          @name , -- name - nvarchar(255)
                          @path , -- path - nvarchar(max)
                          @kb , -- kb - int
                          @idSCORMPackageType , -- idSCORMPackageType - int
                          @imsManifest , -- imsManifest - nvarchar(max)
                          @dtCreated , -- dtCreated - datetime
                          @dtModified  -- dtModified - datetime
                        )
		
		-- get the new object id and return successfully
		
                SELECT  @idSCORMPackage = SCOPE_IDENTITY()
                SET @Error_Description_Code = 'SCORMPackageSave_Inserted'  
				SELECT  @Return_Code = 0
            END
		
        ELSE 
            BEGIN
			
		-- update existing object's properties
		
                UPDATE  tblSCORMPackage
                SET     [idSite] = @idCallerSite , -- idSite - int
                        [name] = @name , -- name - nvarchar(255)
                        [path] = @path , -- path - nvarchar(max)
                        [kb] = @kb , -- kb - int
                        [idSCORMPackageType] = @idSCORMPackageType , -- idSCORMPackageType - int
                        [imsManifest] = @imsManifest , -- imsManifest - nvarchar(max)
                        [dtCreated] = @dtCreated , -- dtCreated - datetime
                        [dtModified] = @dtModified  -- dtModified - datetime
                WHERE   idSCORMPackage = @idSCORMPackage 
		
		-- get the id and return successfully
		
                SELECT  @idSCORMPackage = @idSCORMPackage
                SET @Error_Description_Code = 'SCORMPackageSave_Updated' 
				SELECT  @Return_Code = 0
            END
    END
		

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

