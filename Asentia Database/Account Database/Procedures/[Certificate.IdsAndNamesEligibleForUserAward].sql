-- =====================================================================
-- PROCEDURE: [Certificate.IdsAndNamesEligibleForUserAward]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certificate.IdsAndNamesEligibleForUserAward]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certificate.IdsAndNamesEligibleForUserAward]
GO

/*

Returns a recordset of ids and names that have not been awarded to
a specified user or have been previously awarded by are expired.

*/

CREATE PROCEDURE [Certificate.IdsAndNamesEligibleForUserAward]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idUser					INT
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	SELECT DISTINCT
		C.idCertificate, 
		CASE WHEN CL.name IS NOT NULL THEN CL.name ELSE C.name END AS name
	FROM tblCertificate C
	LEFT JOIN tblCertificateLanguage CL ON CL.idCertificate = C.idCertificate AND CL.idLanguage = @idCallerLanguage
	WHERE C.idSite = @idCallerSite
	AND (C.isDeleted IS NULL OR C.isDeleted = 0)
	AND C.objectType IS NOT NULL -- no template certificates
	AND C.idObject IS NOT NULL -- no template certificates
	AND (C.idCertificate NOT IN (SELECT 
									idCertificate 
								 FROM tblCertificateRecord 
								 WHERE idUser = @idUser
								 AND GETUTCDATE() < expires
								)						
		)
	ORDER BY name							
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO