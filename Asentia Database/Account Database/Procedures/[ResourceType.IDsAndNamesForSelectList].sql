-- =====================================================================
-- PROCEDURE: [ResourceType.IDsAndNamesForSelectList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ResourceType.IDsAndNamesForSelectList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ResourceType.IDsAndNamesForSelectList]
GO

/*

Returns a recordset of group ids and names that a user is a member of.

*/

CREATE PROCEDURE [ResourceType.IDsAndNamesForSelectList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	
		SELECT
			RT.idResourceType ,
			RT.ResourceType
		FROM 
			tblResourceType RT
		WHERE RT.isDeleted = 0
		AND idSite	= @idCallerSite	
		ORDER BY RT.ResourceType
		SET @Return_Code = 0

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	