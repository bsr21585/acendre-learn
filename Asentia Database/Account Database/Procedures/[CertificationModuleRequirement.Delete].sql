-- =====================================================================
-- PROCEDURE: [CertificationModuleRequirement.Delete]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[CertificationModuleRequirement.Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1) 
	DROP PROCEDURE [CertificationModuleRequirement.Delete]
GO

/*

Deletes certification module requirement

*/

CREATE PROCEDURE [CertificationModuleRequirement.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@CertificationModuleRequirements		IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	validate that there are records to delete

	*/

	IF (SELECT COUNT(1) FROM @CertificationModuleRequirements) = 0 
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'CertificationModuleRequirementDelete_NoRecordFound'
		RETURN 1
		END

	/*

	validate that all certification module requirements exist within the site

	*/

	IF (
		SELECT COUNT(1) 
		FROM @CertificationModuleRequirements CMCM
		LEFT JOIN tblCertificationModuleRequirement CM ON CM.idCertificationModuleRequirement = CMCM.id
		WHERE CM.idSite IS NULL
		OR CM.idSite <> @idCallerSite
	   ) > 0 
	   
	   BEGIN
	   SET @Return_Code = 1
	   SET @Error_Description_Code = 'CertificationModuleRequirementDelete_NoRecordFound'
	   RETURN 1
	   END

	/*
	
	DELETE IN ORDER OF DEPENDENCIES

	*/

	-- DELETE FROM [tblData-CertificationModuleRequirement]
	DELETE FROM [tblData-CertificationModuleRequirement] WHERE idCertificationModuleRequirement IN (SELECT id FROM @CertificationModuleRequirements)

	-- DELETE FROM [tblCertificationModuleRequirementToCourseLink]
	DELETE FROM [tblCertificationModuleRequirementToCourseLink] 
	WHERE idCertificationModuleRequirement IN (SELECT id FROM @CertificationModuleRequirements)

	-- DELETE FROM [tblCertificationModuleRequirementLanguage]
	DELETE FROM [tblCertificationModuleRequirementLanguage]
	WHERE idCertificationModuleRequirement IN (SELECT id FROM @CertificationModuleRequirements)

	-- DELETE FROM [tblCertificationModuleRequirement]
	DELETE FROM [tblCertificationModuleRequirement]
	WHERE idCertificationModuleRequirement IN (SELECT id FROM @CertificationModuleRequirements)

	/*

	RETURN

	*/

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO