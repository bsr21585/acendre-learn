-- =====================================================================
-- PROCEDURE: [System.SynchronizeUserBatchFromSQLJob]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.SynchronizeUserBatchFromSQLJob]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.SynchronizeUserBatchFromSQLJob]
GO

/*

This procedure is used for synchronizing a user batch from a SQL agent job.

We call this from the SQL job when we have a client FTP uploaded user batch file that needs
to be synchronized. This should be used for user batches only, no SQL/AD synchronization.

*/
CREATE PROCEDURE [System.SynchronizeUserBatchFromSQLJob]
(
	-- This will not ever be called from procedural code, so skip the standard params.

	@idSite						INT,
	@dataFile					NVARCHAR(512),
	@appendMode					BIT				-- 0 = the users in the file are the complete user base, users not in file are disabled
												-- 1 = append the users
)
AS
	BEGIN
	SET NOCOUNT ON

	/*

	VALIDATE THE SITE ID

	*/

	IF (
		SELECT COUNT(1)
		FROM tblSite
		WHERE idSite = @idSite
		AND @idSite IS NOT NULL
		) <> 1
		BEGIN
		RAISERROR('Site not specified.', 16, 1)
		RETURN 0
		END

	/* GET UTC TIMESTAMP */
	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	/* 

	CREATE A TEMPORARY TABLE TO HOLD THE USER BATCH RAW DATA
		
	*/

	CREATE TABLE #UserBatchRawData
	(
		[firstName]				NVARCHAR(255)	NOT NULL,
		[middleName]			NVARCHAR(255)	NULL,
		[lastName]				NVARCHAR(255)	NOT NULL,
		[email]					NVARCHAR(255)	NULL,
		[username]				NVARCHAR(512)	NOT NULL,
		[password]				NVARCHAR(512)	NULL,
		[mustChangePassword]	BIT				NULL,
		[localTimezone]			NVARCHAR(50)	NULL,
		[languageCode]			NVARCHAR(10)	NULL,
		[expires]				DATETIME		NULL,
		[status]				BIT				NULL,
		[company]				NVARCHAR(255)	NULL,
		[streetAddress]			NVARCHAR(512)	NULL,
		[city]					NVARCHAR(255)	NULL,
		[province]				NVARCHAR(255)	NULL,
		[postalCode]			NVARCHAR(25)	NULL,
		[country]				NVARCHAR(50)	NULL,
		[phonePrimary]			NVARCHAR(25)	NULL,
		[phoneHome]				NVARCHAR(25)	NULL,
		[phoneWork]				NVARCHAR(25)	NULL,
		[phoneFax]				NVARCHAR(25)	NULL,
		[phoneMobile]			NVARCHAR(25)	NULL,
		[phonePager]			NVARCHAR(25)	NULL,
		[phoneOther]			NVARCHAR(25)	NULL,
		[employeeId]			NVARCHAR(255)	NULL,
		[jobTitle]				NVARCHAR(255)	NULL,
		[jobClass]				NVARCHAR(255)	NULL,
		[division]				NVARCHAR(255)	NULL,
		[region]				NVARCHAR(255)	NULL,
		[department]			NVARCHAR(255)	NULL,
		[hireDate]				DATETIME		NULL,
		[teminalDate]			DATETIME		NULL,
		[gender]				NVARCHAR(1)		NULL,
		[race]					NVARCHAR(64)	NULL,
		[dob]					DATETIME		NULL,
		[field00]				NVARCHAR(255)	NULL,
		[field01]				NVARCHAR(255)	NULL,
		[field02]				NVARCHAR(255)	NULL,
		[field03]				NVARCHAR(255)	NULL,
		[field04]				NVARCHAR(255)	NULL,
		[field05]				NVARCHAR(255)	NULL,
		[field06]				NVARCHAR(255)	NULL,
		[field07]				NVARCHAR(255)	NULL,
		[field08]				NVARCHAR(255)	NULL,
		[field09]				NVARCHAR(255)	NULL,
		[field10]				NVARCHAR(255)	NULL,
		[field11]				NVARCHAR(255)	NULL,
		[field12]				NVARCHAR(255)	NULL,
		[field13]				NVARCHAR(255)	NULL,
		[field14]				NVARCHAR(255)	NULL,
		[field15]				NVARCHAR(255)	NULL,
		[field16]				NVARCHAR(255)	NULL,
		[field17]				NVARCHAR(255)	NULL,
		[field18]				NVARCHAR(255)	NULL,
		[field19]				NVARCHAR(255)	NULL,
		[supervisors]			NVARCHAR(MAX)	NULL,
		[end]					NVARCHAR(20)	NOT NULL
	)

	/*

	CREATE A TEMPORARY TABLE TO HOLD THE USER BATCH DATA FOR PROCESSING

	NOTE: THIS IS DONE IN ADDITION TO THE TABLE ABOVE BECAUSE WE NEED SOME
	EXTRA COLUMNS USED FOR PROCESSING AND THE TABLE ABOVE CANNOT HAVE THOSE
	COLUMNS BECAUSE IT NEEDS TO BE IDENTICAL IN STRUCTURE TO THE IMPORT FILE
	SO THAT WE CAN AVOID HAVING TO USE A FORMAT FILE FOR THE BULK INSERT

	*/

	CREATE TABLE #UserBatchForProcessing
	(
		[row]					INT				IDENTITY(1, 1)	NOT NULL,
		[idSite]				INT								NOT NULL,
		[firstName]				NVARCHAR(255)					NOT NULL,
		[middleName]			NVARCHAR(255)					NULL,
		[lastName]				NVARCHAR(255)					NOT NULL,
		[email]					NVARCHAR(255)					NULL,
		[username]				NVARCHAR(512)					NOT NULL,
		[password]				NVARCHAR(512)					NULL,
		[mustChangePassword]	BIT								NULL,
		[localTimezone]			NVARCHAR(50)					NULL,
		[languageCode]			NVARCHAR(10)					NULL,
		[expires]				DATETIME						NULL,
		[status]				BIT								NULL,
		[company]				NVARCHAR(255)					NULL,
		[streetAddress]			NVARCHAR(512)					NULL,
		[city]					NVARCHAR(255)					NULL,
		[province]				NVARCHAR(255)					NULL,
		[postalCode]			NVARCHAR(25)					NULL,
		[country]				NVARCHAR(50)					NULL,
		[phonePrimary]			NVARCHAR(25)					NULL,
		[phoneHome]				NVARCHAR(25)					NULL,
		[phoneWork]				NVARCHAR(25)					NULL,
		[phoneFax]				NVARCHAR(25)					NULL,
		[phoneMobile]			NVARCHAR(25)					NULL,
		[phonePager]			NVARCHAR(25)					NULL,
		[phoneOther]			NVARCHAR(25)					NULL,
		[employeeId]			NVARCHAR(255)					NULL,
		[jobTitle]				NVARCHAR(255)					NULL,
		[jobClass]				NVARCHAR(255)					NULL,
		[division]				NVARCHAR(255)					NULL,
		[region]				NVARCHAR(255)					NULL,
		[department]			NVARCHAR(255)					NULL,
		[hireDate]				DATETIME						NULL,
		[teminalDate]			DATETIME						NULL,
		[gender]				NVARCHAR(1)						NULL,
		[race]					NVARCHAR(64)					NULL,
		[dob]					DATETIME						NULL,
		[field00]				NVARCHAR(255)					NULL,
		[field01]				NVARCHAR(255)					NULL,
		[field02]				NVARCHAR(255)					NULL,
		[field03]				NVARCHAR(255)					NULL,
		[field04]				NVARCHAR(255)					NULL,
		[field05]				NVARCHAR(255)					NULL,
		[field06]				NVARCHAR(255)					NULL,
		[field07]				NVARCHAR(255)					NULL,
		[field08]				NVARCHAR(255)					NULL,
		[field09]				NVARCHAR(255)					NULL,
		[field10]				NVARCHAR(255)					NULL,
		[field11]				NVARCHAR(255)					NULL,
		[field12]				NVARCHAR(255)					NULL,
		[field13]				NVARCHAR(255)					NULL,
		[field14]				NVARCHAR(255)					NULL,
		[field15]				NVARCHAR(255)					NULL,
		[field16]				NVARCHAR(255)					NULL,
		[field17]				NVARCHAR(255)					NULL,
		[field18]				NVARCHAR(255)					NULL,
		[field19]				NVARCHAR(255)					NULL,
		[supervisors]			NVARCHAR(MAX)					NULL,
		[newFlag]				BIT								NULL,
		[modFlag]				BIT								NULL
	)

	/*

	PUT THE DATA FROM THE BATCH FILE INTO THE TEMP TABLE

	*/

	DECLARE @SQL NVARCHAR(MAX)

	SET @SQL = N'
	BULK INSERT #UserBatchRawData
	FROM ''' + @dataFile + N'''
	WITH (
		CODEPAGE = 1252,
		FIELDTERMINATOR = ''\t''
		)'
	
	EXECUTE sp_executesql @SQL

	/*

	MOVE THE DATA FROM THE RAW DATA TEMP TABLE TO THE PROCESSING TEMP TABLE

	*/

	INSERT INTO #UserBatchForProcessing (
		[idSite],
		[firstName],
		[middleName],
		[lastName],
		[email],
		[username],
		[password],
		[mustChangePassword],
		[localTimezone],
		[languageCode],
		[expires],
		[status],
		[company],
		[streetAddress],
		[city],
		[province],
		[postalCode],
		[country],
		[phonePrimary],
		[phoneHome],
		[phoneWork],
		[phoneFax],
		[phoneMobile],
		[phonePager],
		[phoneOther],
		[employeeId],
		[jobTitle],
		[jobClass],
		[division],
		[region],
		[department],
		[hireDate],
		[teminalDate],
		[gender],
		[race],
		[dob],
		[field00],
		[field01],
		[field02],
		[field03],
		[field04],
		[field05],
		[field06],
		[field07],
		[field08],
		[field09],
		[field10],
		[field11],
		[field12],
		[field13],
		[field14],
		[field15],
		[field16],
		[field17],
		[field18],
		[field19],
		[supervisors]
	)
	SELECT
		@idSite,
		[firstName],
		[middleName],
		[lastName],
		[email],
		[username],
		[password],
		[mustChangePassword],
		[localTimezone],
		[languageCode],
		[expires],
		[status],
		[company],
		[streetAddress],
		[city],
		[province],
		[postalCode],
		[country],
		[phonePrimary],
		[phoneHome],
		[phoneWork],
		[phoneFax],
		[phoneMobile],
		[phonePager],
		[phoneOther],
		[employeeId],
		[jobTitle],
		[jobClass],
		[division],
		[region],
		[department],
		[hireDate],
		[teminalDate],
		[gender],
		[race],
		[dob],
		[field00],
		[field01],
		[field02],
		[field03],
		[field04],
		[field05],
		[field06],
		[field07],
		[field08],
		[field09],
		[field10],
		[field11],
		[field12],
		[field13],
		[field14],
		[field15],
		[field16],
		[field17],
		[field18],
		[field19],
		[supervisors]
	FROM #UserBatchRawData

	/*

	VALIDATE THAT ALL RECORDS HAVE FIRST NAME, LAST NAME, AND USERNAME

	*/

	IF (
		SELECT COUNT(1)
		FROM #UserBatchForProcessing
		WHERE (firstName IS NULL OR firstName = '')
		OR (lastName IS NULL OR lastName = '')
		OR (username IS NULL OR username = '')
		) > 0
		BEGIN
		RAISERROR('Required data is missing for Column A (default label: First Name), Column C (default label: Last Name), and/or Column E (default label: Username).', 16, 1)
		RETURN 0
		END

	/*

	VALIDATE NO DUPLICATE USERNAMES WITHIN THE IMPORT DATA

	*/

	IF (
		SELECT COUNT(total)
		FROM
			(
			SELECT SUM(1) AS total
			FROM #UserBatchForProcessing
			WHERE username IS NOT NULL
			AND username <> ''
			GROUP BY username
			) MAIN
		WHERE total > 1
		) > 0
		BEGIN
		RAISERROR('Duplicate usernames exist in data import file.', 16, 1)
		RETURN 0
		END

	/*

	FLAG NEW RECORDS

	*/

	UPDATE #UserBatchForProcessing SET
		newFlag = 1
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblUser U
		WHERE U.idSite = #UserBatchForProcessing.idSite
		AND U.username = #UserBatchForProcessing.username
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
	)
	AND newflag IS NULL

	/*

	VALIDATE THAT ALL NEW RECORDS HAVE A PASSWORD

	*/

	IF (
		SELECT COUNT(1)
		FROM #UserBatchForProcessing
		WHERE ([password] IS NULL OR [password] = '')
		AND newFlag = 1
		) > 0
		BEGIN
		RAISERROR('Required data is missing in Column F (default label: Password) for new user records.', 16, 1)
		RETURN 0
		END

	/*

	FLAG UPDATES THAT ACTUALLY CHANGE DATA (OTHERS WILL BE NON-FLAGGED AND THEREFORE SKIPPED DURING UPDATE)

	*/

	UPDATE #UserBatchForProcessing SET
		modFlag = 1
	WHERE EXISTS (
		SELECT 1
		FROM tblUser U
		LEFT JOIN tblTimezone TZ ON TZ.idTimezone = U.idTimezone
		LEFT JOIN tblLanguage L ON L.idLanguage = U.idLanguage
		WHERE U.idSite = #UserBatchForProcessing.idSite
		AND U.username = #UserBatchForProcessing.username
		AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
		AND (
			U.firstName <> #UserBatchForProcessing.firstName
			OR ISNULL(U.middleName, '') <> ISNULL(#UserBatchForProcessing.middleName, '')
			OR U.lastName <> #UserBatchForProcessing.lastName
			OR ISNULL(U.email, '') <> ISNULL(#UserBatchForProcessing.email, '')
			OR (TZ.dotNetName <> #UserBatchForProcessing.localTimezone AND #UserBatchForProcessing.localTimezone IS NOT NULL)
			OR (L.code <> #UserBatchForProcessing.languageCode AND #UserBatchForProcessing.languageCode IS NOT NULL)
			OR ISNULL(U.dtExpires, '1975-01-01 01:23:45.678') <> ISNULL(#UserBatchForProcessing.expires, '1975-01-01 01:23:45.678')
			OR (U.isActive <> #UserBatchForProcessing.[status] AND #UserBatchForProcessing.[status] IS NOT NULL)
			OR ISNULL(U.company, '') <> ISNULL(#UserBatchForProcessing.company, '')
			OR ISNULL(U.[address], '') <> ISNULL(#UserBatchForProcessing.streetAddress, '')
			OR ISNULL(U.city, '') <> ISNULL(#UserBatchForProcessing.city, '')
			OR ISNULL(U.province, '') <> ISNULL(#UserBatchForProcessing.province, '')
			OR ISNULL(U.postalcode, '') <> ISNULL(#UserBatchForProcessing.postalCode, '')
			OR ISNULL(U.country, '') <> ISNULL(#UserBatchForProcessing.country, '')
			OR ISNULL(U.phonePrimary, '') <> ISNULL(#UserBatchForProcessing.phonePrimary, '')
			OR ISNULL(U.phoneHome, '') <> ISNULL(#UserBatchForProcessing.phoneHome, '')
			OR ISNULL(U.phoneWork, '') <> ISNULL(#UserBatchForProcessing.phoneWork, '')
			OR ISNULL(U.phoneFax, '') <> ISNULL(#UserBatchForProcessing.phoneFax, '')
			OR ISNULL(U.phoneMobile, '') <> ISNULL(#UserBatchForProcessing.phoneMobile, '')
			OR ISNULL(U.phonePager, '') <> ISNULL(#UserBatchForProcessing.phonePager, '')
			OR ISNULL(U.phoneOther, '') <> ISNULL(#UserBatchForProcessing.phoneOther, '')
			OR ISNULL(U.employeeID, '') <> ISNULL(#UserBatchForProcessing.employeeId, '')
			OR ISNULL(U.jobTitle, '') <> ISNULL(#UserBatchForProcessing.jobTitle, '')
			OR ISNULL(U.jobClass, '') <> ISNULL(#UserBatchForProcessing.jobClass, '')
			OR ISNULL(U.division, '') <> ISNULL(#UserBatchForProcessing.division, '')
			OR ISNULL(U.region, '') <> ISNULL(#UserBatchForProcessing.region, '')
			OR ISNULL(U.department, '') <> ISNULL(#UserBatchForProcessing.department, '')
			OR ISNULL(U.dtHire, '1975-01-01 01:23:45.678') <> ISNULL(#UserBatchForProcessing.hireDate, '1975-01-01 01:23:45.678')
			OR ISNULL(U.dtTerm, '1975-01-01 01:23:45.678') <> ISNULL(#UserBatchForProcessing.teminalDate, '1975-01-01 01:23:45.678')
			OR ISNULL(U.gender, '') <> ISNULL(#UserBatchForProcessing.gender, '')
			OR ISNULL(U.race, '') <> ISNULL(#UserBatchForProcessing.race, '')
			OR ISNULL(U.dtDOB, '1975-01-01 01:23:45.678') <> ISNULL(#UserBatchForProcessing.dob, '1975-01-01 01:23:45.678')
			OR ISNULL(U.field00, '') <> ISNULL(#UserBatchForProcessing.field00, '')
			OR ISNULL(U.field01, '') <> ISNULL(#UserBatchForProcessing.field01, '')
			OR ISNULL(U.field02, '') <> ISNULL(#UserBatchForProcessing.field02, '')
			OR ISNULL(U.field03, '') <> ISNULL(#UserBatchForProcessing.field03, '')
			OR ISNULL(U.field04, '') <> ISNULL(#UserBatchForProcessing.field04, '')
			OR ISNULL(U.field05, '') <> ISNULL(#UserBatchForProcessing.field05, '')
			OR ISNULL(U.field06, '') <> ISNULL(#UserBatchForProcessing.field06, '')
			OR ISNULL(U.field07, '') <> ISNULL(#UserBatchForProcessing.field07, '')
			OR ISNULL(U.field08, '') <> ISNULL(#UserBatchForProcessing.field08, '')
			OR ISNULL(U.field09, '') <> ISNULL(#UserBatchForProcessing.field09, '')
			OR ISNULL(U.field10, '') <> ISNULL(#UserBatchForProcessing.field10, '')
			OR ISNULL(U.field11, '') <> ISNULL(#UserBatchForProcessing.field11, '')
			OR ISNULL(U.field12, '') <> ISNULL(#UserBatchForProcessing.field12, '')
			OR ISNULL(U.field13, '') <> ISNULL(#UserBatchForProcessing.field13, '')
			OR ISNULL(U.field14, '') <> ISNULL(#UserBatchForProcessing.field14, '')
			OR ISNULL(U.field15, '') <> ISNULL(#UserBatchForProcessing.field15, '')
			OR ISNULL(U.field16, '') <> ISNULL(#UserBatchForProcessing.field16, '')
			OR ISNULL(U.field17, '') <> ISNULL(#UserBatchForProcessing.field17, '')
			OR ISNULL(U.field18, '') <> ISNULL(#UserBatchForProcessing.field18, '')
			OR ISNULL(U.field19, '') <> ISNULL(#UserBatchForProcessing.field19, '')
		)
	)
	AND newFlag IS NULL
	AND modFlag IS NULL

	/*

	GET THE SITE'S DEFAULT LANGUAGE AND TIMEZONE

	*/

	DECLARE @idDefaultTimezone	INT
	DECLARE @idDefaultLanguage	INT

	SELECT 
		@idDefaultTimezone = CASE WHEN idTimezone IS NOT NULL THEN idTimezone ELSE 17 END,
		@idDefaultLanguage = CASE WHEN idLanguage IS NOT NULL THEN idLanguage ELSE 57 END
	FROM tblSite
	WHERE idSite = @idSite

	/*

	UPDATES

	*/

	UPDATE tblUser SET
		firstName = UB.firstName,
		middleName = UB.middleName,
		lastName = UB.lastName,
		displayName = UB.lastName + ', ' + UB.firstName + CASE WHEN UB.middleName IS NOT NULL AND UB.middleName <> '' THEN ' ' + UB.middleName ELSE '' END,
		email = UB.email,
		idTimezone = CASE WHEN UB.localTimezone IS NOT NULL AND U.idTimezone <> TZ.idTimezone THEN TZ.idTimezone ELSE U.idTimezone END,
		idLanguage = CASE WHEN UB.languageCode IS NOT NULL AND U.idLanguage <> L.idLanguage THEN L.idLanguage ELSE U.idLanguage END,
		isActive = CASE WHEN UB.[status] IS NOT NULL AND U.isActive <> UB.[status] THEN UB.[status] ELSE U.isActive END,
		dtExpires = UB.[expires],
		dtModified = @utcNow,
		company = UB.company,
		[address] = UB.streetAddress,
		city = UB.city,
		province = UB.province,
		postalcode = UB.postalCode,
		country = UB.country,
		phonePrimary = UB.phonePrimary,
		phoneHome = UB.phoneHome,
		phoneWork = UB.phoneWork,
		phoneFax = UB.phoneFax,
		phoneMobile = UB.phoneMobile,
		phonePager = UB.phonePager,
		phoneOther = UB.phoneOther,
		employeeID = UB.employeeId,
		jobTitle = UB.jobTitle,
		jobClass = UB.jobClass,
		division = UB.division,
		region = UB.region,
		department = UB.department,
		dtHire = UB.hireDate,
		dtTerm = UB.teminalDate,
		gender = UB.gender,
		race = UB.race,
		dtDOB = UB.dob,
		field00 = UB.field00,
		field01 = UB.field01,
		field02 = UB.field02,
		field03 = UB.field03,
		field04 = UB.field04,
		field05 = UB.field05,
		field06 = UB.field06,
		field07 = UB.field07,
		field08 = UB.field08,
		field09 = UB.field09,
		field10 = UB.field10,
		field11 = UB.field11,
		field12 = UB.field12,
		field13 = UB.field13,
		field14 = UB.field14,
		field15 = UB.field15,
		field16 = UB.field16,
		field17 = UB.field17,
		field18 = UB.field18,
		field19 = UB.field19
	FROM #UserBatchForProcessing UB
	LEFT JOIN tblUser U ON U.idSite = @idSite AND U.username = UB.username
	LEFT JOIN tblTimezone TZ ON TZ.dotNetName = UB.localTimezone
	LEFT JOIN tblLanguage L ON L.code = UB.languageCode
	WHERE UB.idSite = @idSite
	AND U.idUser IS NOT NULL -- the user record was found
	AND (U.isDeleted = 0 OR U.isDeleted IS NULL)
	AND UB.modFlag = 1

	/*

	INSERTS

	*/
	
	INSERT INTO tblUser (
		firstName,
		middleName,
		lastName,
		displayName,
		email,
		username,
		[password],
		idSite,
		isDeleted,
		idTimezone,
		isActive,
		mustchangePassword,
		dtCreated,
		dtExpires,
		idLanguage,
		dtModified,
		employeeID,
		company,
		[address],
		city,
		province,
		postalcode,
		country,
		phonePrimary,
		phoneWork,
		phoneFax,
		phoneHome,
		phoneMobile,
		phonePager,
		phoneOther,
		department,
		division,
		region,
		jobTitle,
		jobClass,
		gender,
		race,
		dtDOB,
		dtHire,
		dtTerm,
		field00,
		field01,
		field02,
		field03,
		field04,
		field05,
		field06,
		field07,
		field08,
		field09,
		field10,
		field11,
		field12,
		field13,
		field14,
		field15,
		field16,
		field17,
		field18,
		field19
	)
	SELECT
		UB.firstName,
		UB.middleName,
		UB.lastName,
		UB.lastName + ', ' + UB.firstName + CASE WHEN UB.middleName IS NOT NULL AND UB.middleName <> '' THEN ' ' + UB.middleName ELSE '' END,
		UB.email,
		UB.username,
		dbo.GetHashedString('SHA1', UB.[password]),
		@idSite,
		0,
		CASE WHEN TZ.idTimezone IS NOT NULL THEN TZ.idTimezone ELSE @idDefaultTimezone END,
		CASE WHEN UB.[status] IS NOT NULL THEN UB.[status] ELSE 1 END,							-- defaults to an active user
		CASE WHEN UB.mustChangePassword IS NOT NULL THEN UB.mustChangePassword ELSE 0 END,		-- defaults to user does not have to change password
		@utcNow,
		UB.expires,
		CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE @idDefaultLanguage END,
		@utcNow,
		UB.employeeId,
		UB.company,
		UB.streetAddress,
		UB.city,
		UB.province,
		UB.postalcode,
		UB.country,
		UB.phonePrimary,
		UB.phoneWork,
		UB.phoneFax,
		UB.phoneHome,
		UB.phoneMobile,
		UB.phonePager,
		UB.phoneOther,
		UB.department,
		UB.division,
		UB.region,
		UB.jobTitle,
		UB.jobClass,
		UB.gender,
		UB.race,
		UB.dob,
		UB.hireDate,
		UB.teminalDate,
		UB.field00,
		UB.field01,
		UB.field02,
		UB.field03,
		UB.field04,
		UB.field05,
		UB.field06,
		UB.field07,
		UB.field08,
		UB.field09,
		UB.field10,
		UB.field11,
		UB.field12,
		UB.field13,
		UB.field14,
		UB.field15,
		UB.field16,
		UB.field17,
		UB.field18,
		UB.field19
	FROM #UserBatchForProcessing UB
	LEFT JOIN tblTimezone TZ ON TZ.dotNetName = UB.localTimezone
	LEFT JOIN tblLanguage L ON L.code = UB.languageCode
	WHERE UB.idSite = @idSite
	AND UB.newFlag = 1

	/*

	IF APPEND MODE IS OFF, DISABLE USERS NOT INCLUDED IN THE BATCH

	*/
	
	IF @appendMode = 0
		BEGIN

		UPDATE tblUser SET
			isActive = 0
		WHERE idSite = @idSite
		AND (isDeleted = 0 OR isDeleted IS NULL)
		AND NOT EXISTS (
						SELECT 1
						FROM #UserBatchForProcessing
						WHERE tblUser.username = #UserBatchForProcessing.username
					   )

		END
	
	/*

	ADD EVENT LOG ENTRIES FOR NEWLY CREATED USERS

	*/
	
	DECLARE @eventLogItems EventLogItemObjects

	INSERT INTO @eventLogItems (
		idSite,
		idObject, 
		idObjectRelated, 
		idObjectUser
	) 
	SELECT 
		@idSite,
		U.idUser,
		U.idUser,
		U.idUser
	FROM tblUser U
	WHERE U.idSite = @idSite
	AND U.dtCreated = @utcNow
	AND EXISTS (
				SELECT 1 
				FROM #UserBatchForProcessing UB
				WHERE UB.username = U.username
				AND UB.newFlag = 1
			   )

	EXEC [EventLog.Add] NULL, NULL, @idSite, 'en-US', 1, NULL, 101, @utcNow, @eventLogItems

	/*

	USER SUPERVISORS - ATTACH SUPERVISORS WHO AREN'T ALREADY ATTACHED, AND DETACH SUPERVISORS
	WHO ARE NO LONGER IN THE SUPERVISORS FIELD

	*/

	CREATE TABLE #UserSupervisors (
		username	NVARCHAR(512),
		supervisor	NVARCHAR(512)
	)

	-- get the supervisors for each user by cusoring through the data and parsing out
	-- the comma separated supervisors field into its own temporary table

	DECLARE @username NVARCHAR(512)
	DECLARE @supervisor NVARCHAR(512)

	DECLARE userSupervisorsCursor CURSOR FOR
		SELECT DISTINCT
			US.username,
			US.supervisors
		FROM #UserBatchForProcessing US

	OPEN userSupervisorsCursor

	FETCH NEXT FROM userSupervisorsCursor INTO @username, @supervisor

	WHILE (@@FETCH_STATUS = 0)

		BEGIN

		INSERT INTO #UserSupervisors (
			username,
			supervisor
		)
		SELECT 
			@username,
			s 
		FROM [dbo].[DelimitedStringToTable](@supervisor, ',')

		FETCH NEXT FROM userSupervisorsCursor INTO @username, @supervisor

		END

	CLOSE userSupervisorsCursor
	DEALLOCATE userSupervisorsCursor

	-- insert supervisors where they do not already exist
	INSERT INTO tblUserToSupervisorLink (
		idSite,
		idUser,
		idSupervisor
	)
	SELECT  
		@idSite,
		U.idUser,
		S.idUser AS idSupervisor
	FROM #UserSupervisors US
	LEFT JOIN tblUser U ON U.idSite = @idSite AND U.username = US.username
	LEFT JOIN tblUser S ON S.idSite = @idSite AND S.username = US.supervisor
	WHERE U.idUser IS NOT NULL -- the user record was found
	AND S.idUser IS NOT NULL   -- the supervisor record was found
	AND (U.isDeleted = 0 OR U.isDeleted IS NULL) -- user cannot be a deleted user
	AND (S.isDeleted = 0 OR S.isDeleted IS NULL) -- supervisor cannot be a deleted user
	AND NOT EXISTS (SELECT 1
					FROM tblUserToSupervisorLink USL
					WHERE USL.idSite = @idSite
					AND USL.idUser = U.idUser
					AND USL.idSupervisor = S.idUser) -- the supervisor to user link doesn't already exist

	-- delete supervisors where they are not in the #UserSupervisors temp table
	-- note, this is only done for the users who were in the batch, if the user isn't
	-- in the batch, their superivsor records will stay intact
	DELETE FROM tblUserToSupervisorLink 
	WHERE idSite = @idSite
	AND NOT EXISTS (SELECT 1
					FROM #UserSupervisors US
					LEFT JOIN tblUser U ON U.username = US.username
					LEFT JOIN tblUser S ON S.username = US.supervisor
					WHERE U.idSite = @idSite
					AND S.idSite = @idSite
					AND U.idUser IS NOT NULL
					AND S.idUser IS NOT NULL
					AND tblUserToSupervisorLink.idUser = U.idUser
					AND tblUserToSupervisorLink.idSupervisor = S.idUser)

	-- delete supervisors where there are no supervisors for the user
	DELETE FROM tblUserToSupervisorLink
	WHERE idSite = @idSite
	AND idUser IN (
					SELECT U.idUser
					FROM #UserBatchForProcessing UBP
					LEFT JOIN tblUser U ON U.employeeID = UBP.employeeId
					WHERE (UBP.supervisors = '' OR UBP.supervisors IS NULL)
					AND U.idUser IS NOT NULL
				  )

	/* DROP TEMPORARY TABLES */
	DROP TABLE #UserBatchRawData
	DROP TABLE #UserBatchForProcessing
	DROP TABLE #UserSupervisors

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO