-- =====================================================================
-- PROCEDURE: [GroupEnrollment.SaveForMultipleCourses]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[GroupEnrollment.SaveForMultipleCourses]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [GroupEnrollment.SaveForMultipleCourses]
GO

/*

Adds new group enrollments for multiple courses.

*/

CREATE PROCEDURE [GroupEnrollment.SaveForMultipleCourses]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@Courses							IDTable			READONLY,
	@idGroup							INT,
	@idTimezone							INT,
	@isLockedByPrerequisites			BIT,
	@dtStart							DATETIME,
	@dueInterval						INT,
	@dueTimeframe						NVARCHAR(4),
	@expiresFromStartInterval			INT,
	@expiresFromStartTimeframe			NVARCHAR(4),
	@expiresFromFirstLaunchInterval		INT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4)
)
AS

	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF (SELECT COUNT(1) FROM @Courses) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'GEnrollmentSaveForMultipleCourses_NoRecordFound' 
		RETURN 1
		END

	/*
	
	validate that all courses exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Courses CC
		LEFT JOIN tblCourse C ON C.idCourse = CC.id 
		WHERE C.idSite IS NULL
		OR C.idSite <> @idCallerSite
		) > 0
		
		BEGIN 	
		SET @Return_Code = 1
		SET @Error_Description_Code = 'GEnrollmentSaveForMultipleCourses_NoRecordFound'
		RETURN 1 
		END

	/*

	check for duplicate course to group (only one allowed for a given group/course)

	*/

	IF (
		SELECT COUNT(1) 
		FROM tblGroupEnrollment 
		WHERE idCourse IN (SELECT id FROM @Courses)	-- same course(s)
		AND idGroup = @idGroup						-- same group
		) <> 0

		BEGIN	
		SET @Return_Code = 2
		SET @Error_Description_Code = 'GEnrollmentSaveForMultipleCourses_DuplicateRecord' 
		RETURN 1
		END
	
	/*

	save the data

	*/

	-- insert the new group enrollments

	INSERT INTO tblGroupEnrollment (
		idSite,
		idCourse,
		idGroup,
		idTimezone,
		isLockedByPrerequisites,
		dtStart,
		dtCreated,
		dueInterval,
		dueTimeframe,
		expiresFromStartInterval,
		expiresFromStartTimeframe,
		expiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe
	)
	SELECT
		@idCallerSite,
		CC.id,
		@idGroup,
		@idTimezone,
		@isLockedByPrerequisites,
		@dtStart,
		GETUTCDATE(),
		@dueInterval,
		@dueTimeframe,
		@expiresFromStartInterval,
		@expiresFromStartTimeframe,
		@expiresFromFirstLaunchInterval,
		@expiresFromFirstLaunchTimeframe
	FROM @Courses CC
	LEFT JOIN tblCourse C ON C.idCourse = CC.id

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO