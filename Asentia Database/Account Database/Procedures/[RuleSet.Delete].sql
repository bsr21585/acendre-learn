-- =====================================================================
-- PROCEDURE: [RuleSet.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSet.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSet.Delete]
GO

/*

Deletes rule set(s).

*/

CREATE PROCEDURE [RuleSet.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@RuleSets				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @RuleSets) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'RuleSetDelete_NoRecordsSelected'
		RETURN 1
		END
			
	/*
	
	validate that all rule sets exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @RuleSets RR
		LEFT JOIN tblRuleSet R ON RR.id = R.idRuleSet
		WHERE R.idSite IS NULL
		OR R.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetDelete_DetailsNotFound'
		RETURN 1 
		END


	/*

	get data for deleting synced child rulesets

	*/

	DECLARE @idParentRuleSetEnrollment					INT
	DECLARE @idParentRuleSetLearningPathEnrollment		INT
	DECLARE @idChildRuleSet								INT

	SET @idParentRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = (SELECT TOP 1 id FROM @RuleSets))
	SET @idParentRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = (SELECT TOP 1 id FROM @RuleSets))
	
	UPDATE tblRuleSetEnrollment
	SET idParentRuleSetEnrollment = NULL
	WHERE idRuleSetEnrollment = @idParentRuleSetEnrollment

	-- get child rulesets

	SELECT 
		RS.idRuleSet
	INTO #SyncedCourseRuleSetsToUpdate 
	FROM tblRuleSetToRuleSetEnrollmentLink RSTRSEL
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSTRSEL.idRuleSetEnrollment 
	LEFT JOIN tblRuleSet RS on RS.idRuleSet = RSTRSEL.idRuleSet
	WHERE idParentRuleSetEnrollment = @idParentRuleSetEnrollment
	AND RS.label = (SELECT label FROM tblRuleSet WHERE idRuleSet IN (SELECT id FROM @RuleSets))

	SELECT 
		RS.idRuleSet
	INTO #SyncedLearningPathRuleSetsToUpdate 
	FROM tblRuleSetToRuleSetLearningPathEnrollmentLink RSTRSLPEL
	LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = RSTRSLPEL.idRuleSetLearningPathEnrollment 
	LEFT JOIN tblRuleSet RS on RS.idRuleSet = RSTRSLPEL.idRuleSet
	WHERE idParentRuleSetLearningPathEnrollment = @idParentRuleSetLearningPathEnrollment
	AND RS.label = (SELECT label FROM tblRuleSet WHERE idRuleSet IN (SELECT id FROM @RuleSets))
		
	/*
	
	Delete child record(s) from the link table.

	*/

	DELETE FROM tblRule
	WHERE idRuleSet IN (
		SELECT id
		FROM @RuleSets
	)
	OR idRuleSet IN (SELECT idRuleSet FROM #SyncedCourseRuleSetsToUpdate)
	OR idRuleSet IN (SELECT idRuleSet FROM #SyncedLearningPathRuleSetsToUpdate)


	DELETE FROM tblRuleSetToGroupLink
	WHERE idRuleSet IN (
		SELECT id
		FROM @RuleSets
	)
	
	DELETE FROM tblRuleSetToRoleLink
	WHERE idRuleSet IN (
		SELECT id
		FROM @RuleSets
	)
	
	DELETE FROM tblRuleSetToRuleSetEnrollmentLink
	WHERE idRuleSet IN (
		SELECT id
		FROM @RuleSets
	)
	OR idRuleSet IN (SELECT idRuleSet FROM #SyncedCourseRuleSetsToUpdate)

	DELETE FROM tblRuleSetToRuleSetLearningPathEnrollmentLink
	WHERE idRuleSet IN (
		SELECT id
		FROM @RuleSets
	)
	OR idRuleSet IN (SELECT idRuleSet FROM #SyncedLearningPathRuleSetsToUpdate)

	DELETE FROM tblRuleSetToCertificationLink
	WHERE idRuleSet IN (
		SELECT id
		FROM @RuleSets
	)

	/*

	Do NOT delete user linkage to objects where the ruleset is the cause of the linkage.
	Auto-Join rules procedures should take care of that.

	*/

	/*

	Delete child record(s) from the language table.

	*/

	DELETE FROM tblRuleSetLanguage
	WHERE idRuleSet IN (
		SELECT id
		FROM @RuleSets
	)
	OR idRuleSet IN (SELECT idRuleSet FROM #SyncedCourseRuleSetsToUpdate)
	OR idRuleSet IN (SELECT idRuleSet FROM #SyncedLearningPathRuleSetsToUpdate)
	
	/*

	Delete record(s) from rule set table.

	*/

	DELETE FROM tblRuleSet
	WHERE idRuleSet IN (
		SELECT id
		FROM @RuleSets
	)
	OR idRuleSet IN (SELECT idRuleSet FROM #SyncedCourseRuleSetsToUpdate)
	OR idRuleSet IN (SELECT idRuleSet FROM #SyncedLearningPathRuleSetsToUpdate)
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO