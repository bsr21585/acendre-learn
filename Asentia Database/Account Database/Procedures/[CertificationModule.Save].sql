-- =====================================================================
-- PROCEDURE: [CertificationModule.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[CertificationModule.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [CertificationModule.Save]
GO

/*

Adds new certification module or updates existing certification module.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [CertificationModule.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idCertificationModule				INT				OUTPUT,	
	@idCertification					INT,
	@title								NVARCHAR(255),
	@shortDescription					NVARCHAR(512),
	@isAnyRequirementSet				BIT,	
	@isInitialRequirement				BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED


	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'CertificationModuleSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/
		
	/*
	
	save the data
	
	*/
	
	IF (@idCertificationModule = 0 OR @idCertificationModule IS NULL)
		
		BEGIN
		
		-- insert the new certification module
		
		INSERT INTO tblCertificationModule (
			idCertification,
			idSite,
			title,
			shortDescription,
			isAnyRequirementSet,
			dtCreated,
			dtModified,
			isDeleted,
			dtDeleted,
			isInitialRequirement
		)			
		VALUES (
			@idCertification,
			@idCallerSite,
			@title,
			@shortDescription,
			@isAnyRequirementSet,
			GETUTCDATE(),
			GETUTCDATE(),
			0,
			NULL,
			@isInitialRequirement
		)
		
		-- get the new certification module's id
		
		SET @idCertificationModule = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the certification module id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblCertificationModule WHERE idCertificationModule = @idCertificationModule AND idSite = @idCallerSite AND (isDeleted IS NULL OR isDeleted = 0)) < 1
			BEGIN
			
			SET @idCertificationModule = @idCertificationModule
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertificationModuleSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing certification module's properties
		
		UPDATE tblCertificationModule SET			
			title = @title,
			shortDescription = @shortDescription,			
			dtModified = GETUTCDATE(),
			isAnyRequirementSet = @isAnyRequirementSet,
			isInitialRequirement = @isInitialRequirement			
		WHERE idCertificationModule = @idCertificationModule

		-- get the certification module's id 

		SET @idCertificationModule = @idCertificationModule
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblCertificationModuleLanguage CML WHERE CML.idCertificationModule = @idCertificationModule AND CML.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblCertificationModuleLanguage SET
			title = @title,
			shortDescription = @shortDescription
		WHERE idCertificationModule = @idCertificationModule
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCertificationModuleLanguage (
			idSite,
			idCertificationModule,
			idLanguage,
			title,
			shortDescription
		)
		SELECT
			@idCallerSite,
			@idCertificationModule,
			@idDefaultLanguage,
			@title,
			@shortDescription
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCertificationModuleLanguage CML
			WHERE CML.idCertificationModule = @idCertificationModule
			AND CML.idLanguage = @idDefaultLanguage
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO