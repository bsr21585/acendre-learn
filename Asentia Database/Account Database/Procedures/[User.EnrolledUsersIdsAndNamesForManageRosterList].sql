-- =====================================================================
-- PROCEDURE: [User.EnrolledUsersIdsAndNamesForManageRosterList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.EnrolledUsersIdsAndNamesForManageRosterList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.EnrolledUsersIdsAndNamesForManageRosterList]
GO

/*

Returns a recordset of user ids and names to polulate
"attach user" select list for standup training enrolled list, for users
that are enrolled in a course with this ILT Attached

*/

CREATE PROCEDURE [User.EnrolledUsersIdsAndNamesForManageRosterList]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0,

	@idStandupTraining				INT,
	@idStandupTrainingInstance		INT,
	@searchParam					NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT TOP 1000
			U.idUser, 
			U.displayName + ' (' + U.username + ')' AS displayName,
			U.email AS registrantEmail
		FROM tblUser U
		LEFT JOIN tblEnrollment E ON E.idUser = U.idUser
		LEFT JOIN tblLesson L ON L.idCourse = E.idCourse
		LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = L.idLesson
		WHERE U.idSite = @idCallerSite
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
		AND LCL.idObject = @idStandupTraining
		AND LCL.idContentType = 2
		AND NOT EXISTS (SELECT 1 FROM tblStandUpTrainingInstanceToUserLink STUL
						WHERE STUL.idStandupTrainingInstance = @idStandupTrainingInstance
						AND STUL.idUser = U.idUser)
		ORDER BY displayName

		END

	ELSE

		BEGIN

		SELECT DISTINCT TOP 1000
			U.idUser, 
			U.displayName + ' (' + U.username + ')' AS displayName,
			U.email AS registrantEmail
		FROM tblUser U
		INNER JOIN CONTAINSTABLE(tblUser, *, @searchParam) K ON K.[key] = U.idUser
		LEFT JOIN tblEnrollment E ON E.idUser = U.idUser
		LEFT JOIN tblLesson L ON L.idCourse = E.idCourse
		LEFT JOIN tblLessonToContentLink LCL ON LCL.idLesson = L.idLesson
		WHERE U.idSite = @idCallerSite
		AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1))
		AND LCL.idObject = @idStandupTraining
		AND LCL.idContentType = 2
		ORDER BY displayName

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	