-- =====================================================================
-- PROCEDURE: [Lesson.SetContentChangeLessonDataFlags]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Lesson.SetContentChangeLessonDataFlags]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Lesson.SetContentChangeLessonDataFlags]
GO

/*

Sets flags for lesson data to reset or prevent content launch when a lesson's content has changed.

*/

CREATE PROCEDURE [Lesson.SetContentChangeLessonDataFlags]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, --default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0,
	
	@idLesson							INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the lesson exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblLesson
		WHERE idLesson = @idLesson
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'LessonSetContentChangeLessonDataF_DetailsNotFound'
		RETURN 1 
		END
		
	/*

	set resetForContentChange flag for all incomplete lesson data records for the lesson

	*/

	UPDATE [tblData-Lesson] SET
		resetForContentChange = 1
	WHERE idLesson = @idLesson
	AND dtCompleted IS NULL

	/*

	set preventPostCompletionLaunchForContent flag for all completed lesson data records for the lesson

	*/

	UPDATE [tblData-Lesson] SET
		preventPostCompletionLaunchForContentChange = 1
	WHERE idLesson = @idLesson
	AND dtCompleted IS NOT NULL

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO