-- =====================================================================
-- PROCEDURE: [LearningPathEnrollment.GetGridForUserDashboard]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[LearningPathEnrollment.GetGridForUserDashboard]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [LearningPathEnrollment.GetGridForUserDashboard]
GO

/*

Gets a listing of user learning path enrollments for display on user's dashboard. 

*/

CREATE PROCEDURE [LearningPathEnrollment.GetGridForUserDashboard]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,
	
	@enrollmentStatus		NVARCHAR(20)
)
AS
	BEGIN
		SET NOCOUNT ON

		CREATE TABLE #LearningPathEnrollments (
			idLearningPathEnrollment			INT,
			idLearningPath						INT,
			idUser								INT,
			title								NVARCHAR(255),
			idRuleSetLearningPathEnrollment		INT,
			dtStart								DATETIME,
			dtDue								DATETIME,
			dtExpires							DATETIME,
			dtCompleted							DATETIME,
			dtFirstLaunch						DATETIME,
			numCourses							INT,
			numCoursesCompleted					INT
		)

		INSERT INTO #LearningPathEnrollments (
			idLearningPathEnrollment,
			idLearningPath,
			idUser,
			title,
			idRuleSetLearningPathEnrollment,
			dtStart,
			dtDue,
			dtExpires,
			dtCompleted,
			dtFirstLaunch,
			numCourses,
			numCoursesCompleted
		)
		SELECT
			LPE.idLearningPathEnrollment,
			LPE.idLearningPath,
			LPE.idUser,
			LPE.title,
			LPE.idRuleSetLearningPathEnrollment,
			LPE.dtStart,
			LPE.dtDue,
			CASE WHEN LPE.dtExpiresFromStart IS NULL AND LPE.dtExpiresFromFirstLaunch IS NULL THEN
				NULL
			ELSE
				CASE WHEN LPE.dtExpiresFromStart IS NULL AND LPE.dtExpiresFromFirstLaunch IS NOT NULL THEN
					LPE.dtExpiresFromFirstLaunch
				ELSE
					CASE WHEN LPE.dtExpiresFromStart IS NOT NULL AND LPE.dtExpiresFromFirstLaunch IS NULL THEN
						LPE.dtExpiresFromStart
					ELSE
						CASE WHEN LPE.dtExpiresFromStart >= LPE.dtExpiresFromFirstLaunch THEN
							LPE.dtExpiresFromFirstLaunch
						ELSE
							LPE.dtExpiresFromStart
						END
					END
				END
			END AS [dtExpires],
			LPE.dtCompleted,
			LPE.dtFirstLaunch,
			(SELECT COUNT(1) FROM tblLearningPathToCourseLink LPCL WHERE LPCL.idLearningPath = LPE.idLearningPath) AS numCourses,
			(
			 SELECT COUNT(DISTINCT idCourse) 
			 FROM tblEnrollment 
			 WHERE idUser = LPE.idUser 
			 AND dtCompleted IS NOT NULL 
			 AND dtCompleted >= DATEADD(yyyy, -1, LPE.dtStart) 
			 AND idCourse IN (SELECT idCourse FROM tblLearningPathToCourseLink WHERE idLearningPath = LPE.idLearningPath)					  
			) AS numCoursesCompleted
		FROM tblLearningPathEnrollment LPE
		WHERE LPE.idUser = @idCaller
		AND 
		(
				(
				@enrollmentStatus = 'enrolled'
				AND (LPE.dtDue > GETUTCDATE() OR LPE.dtDue IS NULL)
				AND (LPE.dtExpiresFromStart > GETUTCDATE() OR LPE.dtExpiresFromStart IS NULL)
				AND (LPE.dtExpiresFromFirstLaunch > GETUTCDATE() OR LPE.dtExpiresFromFirstLaunch IS NULL)
				AND LPE.dtCompleted IS NULL
				)
			OR
				(
				@enrollmentStatus = 'overdue'
				AND LPE.dtDue <= GETUTCDATE()
				AND (LPE.dtExpiresFromStart > GETUTCDATE() OR LPE.dtExpiresFromStart IS NULL)
				AND (LPE.dtExpiresFromFirstLaunch > GETUTCDATE() OR LPE.dtExpiresFromFirstLaunch IS NULL)
				AND LPE.dtCompleted IS NULL
				)
			OR
				(
				@enrollmentStatus = 'completed'
				AND LPE.dtCompleted IS NOT NULL
				)
			OR
				(
				@enrollmentStatus = 'expired'
				AND (
						(LPE.dtExpiresFromStart IS NOT NULL AND LPE.dtExpiresFromStart <= GETUTCDATE())
						OR
						(LPE.dtExpiresFromFirstLaunch IS NOT NULL AND LPE.dtExpiresFromFirstLaunch <= GETUTCDATE())
					)
				AND LPE.dtCompleted IS NULL
				)
		)
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END
		
		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #LearningPathEnrollments LPE
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						LPE.idLearningPathEnrollment,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END DESC,							
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCompleted' THEN dtCompleted END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtDue END) END DESC,

							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtCompleted' THEN dtCompleted END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtExpires', 'dtExpires', 'dtCompleted') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtExpires', 'dtCompleted') THEN dtDue END) END
						)
						AS [row_number]
					FROM #LearningPathEnrollments LPE
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idLearningPathEnrollment,
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				LPE.idLearningPathEnrollment,	
				LPE.idLearningPath,
				LPE.title,
				LPE.dtDue AS [dtDue],
				LPE.dtCompleted AS [dtCompleted],
				LPE.dtExpires AS [dtExpires],
				LPE.dtFirstLaunch AS [dtFirstLaunch],
				CASE WHEN (@enrollmentStatus = 'expired') OR (@enrollmentStatus = 'completed' AND (LPE.dtExpires IS NOT NULL AND LPE.dtExpires <= GETUTCDATE())) THEN CONVERT(BIT, 0) -- 2 is expired
				ELSE
					CONVERT(BIT, 1) -- 1 is OK to view
				END AS [view],
				LPE.numCourses,
				LPE.numCoursesCompleted,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #LearningPathEnrollments LPE ON LPE.idLearningPathEnrollment = SelectedKeys.idLearningPathEnrollment
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #LearningPathEnrollments LPE
			INNER JOIN CONTAINSTABLE(tblEnrollment, *, @searchParam) K ON K.[key] = E.idEnrollment
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						LPE.idLearningPathEnrollment,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END DESC,							
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCompleted' THEN dtCompleted END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtDue END) END DESC,

							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtCompleted' THEN dtCompleted END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtExpires', 'dtExpires', 'dtCompleted') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtExpires', 'dtCompleted') THEN dtDue END) END
						)
						AS [row_number]
					FROM #LearningPathEnrollments LPE
					INNER JOIN CONTAINSTABLE(tblLearningPathEnrollment, *, @searchParam) K ON K.[key] = LPE.idLearningPathEnrollment
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idLearningPathEnrollment, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				LPE.idLearningPathEnrollment,	
				LPE.idLearningPath,
				LPE.title,				
				LPE.dtDue AS [dtDue],
				LPE.dtCompleted AS [dtCompleted],
				LPE.dtExpires AS [dtExpires],
				LPE.dtFirstLaunch AS [dtFirstLaunch],
				CASE WHEN (@enrollmentStatus = 'expired') OR (@enrollmentStatus = 'completed' AND (LPE.dtExpires IS NOT NULL AND LPE.dtExpires <= GETUTCDATE())) THEN CONVERT(BIT, 0) -- 2 is expired
				ELSE
					CONVERT(BIT, 1) -- 1 is OK to view
				END AS [view],
				LPE.numCourses,
				LPE.numCoursesCompleted,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #LearningPathEnrollments LPE ON LPE.idLearningPathEnrollment = SelectedKeys.idLearningPathEnrollment
			LEFT JOIN tblLearningPath LP ON LP.idLearningPath = LPE.idLearningPath
			ORDER BY SelectedKeys.[row_number]
			
			END

		-- DROP THE TEMPORARY TABLE
		DROP TABLE #LearningPathEnrollments
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO