-- =====================================================================
-- PROCEDURE: [EventEmailQueue.GetCurrentItems]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventEmailQueue.GetCurrentItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventEmailQueue.GetCurrentItems]
GO

/*

Gets the current (not yet sent) items in the email queue.

*/

CREATE PROCEDURE [EventEmailQueue.GetCurrentItems]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idSite					INT,
	@numItems				INT
)
AS

	BEGIN
	SET NOCOUNT ON

	IF (@numItems IS NULL)
		BEGIN
		SET @numItems = 1000 -- default is 1000
		END	

	SELECT TOP (@numItems)
		EEQ.idEventEmailQueue,
		EEQ.idSite,
		S.hostname,
		EEQ.idEventLog,
		EEQ.idEventType,
		EEQ.idEventEmailNotification,
		EEQ.idEventTypeRecipient,
		EEQ.idObject,
		EEQ.idObjectRelated,
		EEQ.objectType,
		EEQ.idObjectUser,
		EEQ.objectUserFullName,
		EEQ.objectUserFirstName,
		EEQ.objectUserLogin,
		EEQ.objectUserEmail,
		EEQ.idRecipient,
		EEQ.recipientLangString,
		EEQ.recipientFullName,
		EEQ.recipientFirstName,
		EEQ.recipientLogin,
		EEQ.recipientEmail,
		EEQ.recipientTimezone,
		EEQ.recipientTimezoneDotNetName,
		EEQ.[from],
		EEQ.copyTo,
		CASE WHEN SP1.value IS NOT NULL AND SP1.value <> '' THEN SP1.value ELSE NULL END AS systemFromAddressOverride,
		CASE WHEN SP2.value IS NOT NULL AND SP2.value <> '' THEN SP2.value ELSE NULL END AS systemSmtpServerNameOverride,
		CASE WHEN SP3.value IS NOT NULL AND SP3.value <> '' THEN SP3.value ELSE NULL END AS systemSmtpServerPortOverride,
		CASE WHEN SP4.value IS NOT NULL AND SP4.value <> '' THEN SP4.value ELSE NULL END AS systemSmtpServerUseSslOverride,
		CASE WHEN SP5.value IS NOT NULL AND SP5.value <> '' THEN SP5.value ELSE NULL END AS systemSmtpServerUsernameOverride,
		CASE WHEN SP6.value IS NOT NULL AND SP6.value <> '' THEN SP6.value ELSE NULL END AS systemSmtpServerPasswordOverride,
		EEQ.[priority],
		EEQ.isHTMLBased,
		EEQ.dtEvent,
		EEQ.dtAction,
		EEQ.dtActivation,
		EEQ.dtSent,
		EEQ.statusDescription,
		EEQ.attachmentType,
		CASE WHEN EEQ.objectType = 'user' THEN 
			'<object type="user">'
			+ '<objectName><![CDATA[' + EEQ.objectUserFullName + ']]></objectName>'
			+ '<rejectionComments><![CDATA[' + CASE WHEN U.rejectionComments IS NOT NULL THEN U.rejectionComments ELSE '' END + ']]></rejectionComments>'
			+ '</object>'
		WHEN EEQ.objectType = 'courseenrollment' THEN
			'<object type="courseenrollment">'
			+ '<objectName><![CDATA[' + CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END + ']]></objectName>'
			+ '<enrollmentDescription><![CDATA[' + CASE WHEN CL.shortDescription IS NOT NULL THEN CL.shortDescription ELSE C.shortDescription END + ']]></enrollmentDescription>'
			+ '<enrollmentEstCompletionTime><![CDATA[' + CASE WHEN C.[minutes] IS NOT NULL THEN CONVERT(NVARCHAR, C.[minutes]) ELSE '' END + ']]></enrollmentEstCompletionTime>'
			+ '<enrollmentDueDate><![CDATA[' + CASE WHEN E.dtDue IS NOT NULL THEN CONVERT(NVARCHAR, E.dtDue, 21) ELSE '' END + ']]></enrollmentDueDate>'
			+ '<enrollmentExpireDate><![CDATA[' + CASE WHEN E.dtExpiresFromStart IS NOT NULL THEN CONVERT(NVARCHAR, E.dtExpiresFromStart, 21) ELSE CASE WHEN E.dtExpiresFromFirstLaunch IS NOT NULL THEN CONVERT(NVARCHAR, E.dtExpiresFromFirstLaunch, 21) ELSE '' END END + ']]></enrollmentExpireDate>'
			+ '</object>'
		WHEN EEQ.objectType = 'courseenrollmentrequest' THEN
			'<object type="courseenrollmentrequest">'
			+ '<objectName><![CDATA[' + CASE WHEN ERCL.title IS NOT NULL THEN ERCL.title ELSE ERC.title END + ']]></objectName>'
			+ '<enrollmentDescription><![CDATA[' + CASE WHEN ERCL.shortDescription IS NOT NULL THEN ERCL.shortDescription ELSE ERC.shortDescription END + ']]></enrollmentDescription>'
			+ '<enrollmentEstCompletionTime><![CDATA[' + CASE WHEN ERC.[minutes] IS NOT NULL THEN CONVERT(NVARCHAR, ERC.[minutes]) ELSE '' END + ']]></enrollmentEstCompletionTime>'			
			+ '<rejectionComments><![CDATA[' + CASE WHEN ER.rejectionComments IS NOT NULL THEN ER.rejectionComments ELSE '' END + ']]></rejectionComments>'
			+ '</object>'
		WHEN EEQ.objectType = 'lesson' THEN 
			'<object type="lesson">'
			+ '<objectName><![CDATA[' + CASE WHEN LNL.title IS NOT NULL THEN LNL.title ELSE LN.title END + ']]></objectName>'
			+ '</object>'
		WHEN EEQ.objectType = 'standuptraininginstance' THEN 
			'<object type="standuptraininginstance">'
			+ '<objectName><![CDATA[' + CASE WHEN STL.title IS NOT NULL THEN STL.title ELSE ST.title END + ' - ' + CASE WHEN STIL.title IS NOT NULL THEN STIL.title ELSE STI.title END + ']]></objectName>'
			+ '<sessionDateTime><![CDATA[' + (SELECT CASE WHEN MIN(STIMT.dtStart) IS NOT NULL THEN CONVERT(NVARCHAR, MIN(STIMT.dtStart), 21) ELSE '' END FROM tblStandupTrainingInstanceMeetingTime STIMT WHERE STIMT.idStandupTrainingInstance = STI.idStandupTrainingInstance) + ']]></sessionDateTime>'
			+ '<sessionTimezoneDotNetName><![CDATA[' + (SELECT CASE WHEN TZ.dotNetName IS NOT NULL THEN TZ.dotNetName ELSE '' END FROM tblTimezone TZ WHERE TZ.idTimezone = (SELECT TOP 1 STIMT.idTimezone FROM tblStandupTrainingInstanceMeetingTime STIMT WHERE STIMT.idStandupTrainingInstance = STI.idStandupTrainingInstance)) + ']]></sessionTimezoneDotNetName>'
			+ '<sessionTimezoneFriendlyName><![CDATA[' + (SELECT CASE WHEN TZ.displayName IS NOT NULL THEN TZ.displayName ELSE '' END FROM tblTimezone TZ WHERE TZ.idTimezone = (SELECT TOP 1 STIMT.idTimezone FROM tblStandupTrainingInstanceMeetingTime STIMT WHERE STIMT.idStandupTrainingInstance = STI.idStandupTrainingInstance)) + ']]></sessionTimezoneFriendlyName>'
			+ '<sessionLocation><![CDATA[' + CASE WHEN STI.[type] = 0 THEN CASE WHEN STI.urlAttend IS NOT NULL THEN STI.urlAttend ELSE '' END ELSE CASE WHEN STI.city IS NOT NULL THEN STI.city ELSE '' END + CASE WHEN STI.province IS NOT NULL THEN ', ' + STI.province ELSE '' END END + ']]></sessionLocation>'
			+ '<sessionLocationDescription><![CDATA[' + CASE WHEN STI.locationDescription IS NOT NULL THEN STI.locationDescription ELSE '' END + ']]></sessionLocationDescription>'
			+ '<sessionInstructorFullName><![CDATA[' +  ISNULL(STUFF((SELECT ', ' + INS.firstName + ' ' + INS.lastname FROM tblStandupTrainingInstanceToInstructorLink STII LEFT JOIN tblUser INS ON INS.idUser = STII.idInstructor AND STII.idStandupTrainingInstance = STI.idStandUpTrainingInstance FOR XML PATH('')), 1, 1, ''), '') + ']]></sessionInstructorFullName>'
			+ '<sessionIsRestrictedDrop><![CDATA[' + CASE WHEN ST.isRestrictedDrop = 1 THEN 'true' ELSE 'false' END + ']]></sessionIsRestrictedDrop>'
			+ '<sessionDateTimes>' + ISNULL(STUFF((SELECT ' <meetingTime><dtStart>' + CONVERT(NVARCHAR, STIMT.dtStart, 21)  + '</dtStart><dtEnd>' + CONVERT(NVARCHAR, STIMT.dtEnd, 21)  + '</dtEnd></meetingTime>' FROM tblStandupTrainingInstanceMeetingTime STIMT WHERE STIMT.idStandupTrainingInstance = STI.idStandupTrainingInstance FOR XML PATH('')), 1, 1, ''), '') + '</sessionDateTimes>'
			+ '</object>'
		WHEN EEQ.objectType = 'learningpathenrollment' THEN 
			'<object type="learningpathenrollment">'
			+ '<objectName><![CDATA[' + CASE WHEN LPL.name IS NOT NULL THEN LPL.name ELSE LP.name END + ']]></objectName>'
			+ '<enrollmentDescription><![CDATA[' + CASE WHEN LPL.shortDescription IS NOT NULL THEN LPL.shortDescription ELSE LP.shortDescription END + ']]></enrollmentDescription>'
			+ '<enrollmentDueDate><![CDATA[' + CASE WHEN LPE.dtDue IS NOT NULL THEN CONVERT(NVARCHAR, LPE.dtDue, 21) ELSE '' END + ']]></enrollmentDueDate>'
			+ '<enrollmentExpireDate><![CDATA[' + CASE WHEN LPE.dtExpiresFromStart IS NOT NULL THEN CONVERT(NVARCHAR, LPE.dtExpiresFromStart, 21) ELSE CASE WHEN LPE.dtExpiresFromFirstLaunch IS NOT NULL THEN CONVERT(NVARCHAR, LPE.dtExpiresFromFirstLaunch, 21) ELSE '' END END + ']]></enrollmentExpireDate>'
			+ '</object>'
		WHEN EEQ.objectType = 'certification' THEN
			'<object type="certification">'
			+ '<objectName><![CDATA[' + CASE WHEN CRL.title IS NOT NULL THEN CRL.title ELSE CR.title END + ']]></objectName>'
			+ '<certificationDescription><![CDATA[' + CASE WHEN CRL.shortDescription IS NOT NULL THEN CRL.shortDescription ELSE CR.shortDescription END + ']]></certificationDescription>'			
			+ '</object>'
		WHEN EEQ.objectType = 'certificate' THEN 
			'<object type="certificate">'
			+ '<objectName><![CDATA[' + CASE WHEN CEL.name IS NOT NULL THEN CEL.name ELSE CE.name END + ']]></objectName>'
			+ '</object>'
		WHEN EEQ.objectType = 'coursediscussionmessage' THEN
			'<object type="coursediscussionmessage">'
			+ '<objectName><![CDATA[' + CASE WHEN MMCL.title IS NOT NULL THEN MMCL.title ELSE MMC.title END + ']]></objectName>'
			+ '<discussionMessage><![CDATA[' + CFM.[message] + ']]></discussionMessage>'
			+ '</object>'
		WHEN EEQ.objectType = 'groupdiscussionmessage' THEN
			'<object type="groupdiscussionmessage">'
			+ '<objectName><![CDATA[' + CASE WHEN MMGL.name IS NOT NULL THEN MMGL.name ELSE MMG.name END + ']]></objectName>'
			+ '<discussionMessage><![CDATA[' + GFM.[message] + ']]></discussionMessage>'
			+ '</object>'
		ELSE
			'<object type="">'
			+ '<objectName><![CDATA[]]></objectName>'
			+ '</object>'
		END AS [objectInformation]
	FROM tblEventEmailQueue EEQ
	LEFT JOIN tblUser U ON U.idUser = EEQ.idObjectUser AND EEQ.objectType = 'user'
	LEFT JOIN tblSite S ON S.idSite = EEQ.idSite
	LEFT JOIN tblSiteParam SP1 ON SP1.idSite = 1 AND SP1.[param] = 'System.EmailFromAddressOverride'
	LEFT JOIN tblSiteParam SP2 ON SP2.idSite = 1 AND SP2.[param] = 'System.EmailSmtpServerNameOverride'
	LEFT JOIN tblSiteParam SP3 ON SP3.idSite = 1 AND SP3.[param] = 'System.EmailSmtpServerPortOverride'
	LEFT JOIN tblSiteParam SP4 ON SP4.idSite = 1 AND SP4.[param] = 'System.EmailSmtpServerUseSslOverride'
	LEFT JOIN tblSiteParam SP5 ON SP5.idSite = 1 AND SP5.[param] = 'System.EmailSmtpServerUsernameOverride'
	LEFT JOIN tblSiteParam SP6 ON SP6.idSite = 1 AND SP6.[param] = 'System.EmailSmtpServerPasswordOverride'
	LEFT JOIN tblLanguage L ON L.code = EEQ.recipientLangString
	LEFT JOIN tblCourse C ON C.idCourse = EEQ.idObject AND EEQ.objectType = 'courseenrollment'
	LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'courseenrollment'
	LEFT JOIN tblEnrollment E ON E.idEnrollment = EEQ.idObjectRelated AND EEQ.objectType = 'courseenrollment'
	LEFT JOIN tblCourse ERC ON ERC.idCourse = EEQ.idObject AND EEQ.objectType = 'courseenrollmentrequest'
	LEFT JOIN tblCourseLanguage ERCL ON ERCL.idCourse = ERC.idCourse AND ERCL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'courseenrollmentrequest'
	LEFT JOIN tblEnrollmentRequest ER ON ER.idEnrollmentRequest = EEQ.idObjectRelated AND EEQ.objectType = 'courseenrollmentrequest'	
	LEFT JOIN tblLesson LN ON LN.idLesson = EEQ.idObject AND EEQ.objectType = 'lesson'
	LEFT JOIN tblLessonLanguage LNL ON LNL.idLesson = LN.idLesson AND LNL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'lesson'
	LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandUpTrainingInstance = EEQ.idObjectRelated AND EEQ.objectType = 'standuptraininginstance'
	LEFT JOIN tblStandUpTrainingInstanceLanguage STIL ON STIL.idStandUpTrainingInstance = STI.idStandUpTrainingInstance AND STIL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'standuptraininginstance'
	LEFT JOIN tblStandUpTraining ST ON ST.idStandUpTraining = STI.idStandUpTraining AND EEQ.objectType = 'standuptraininginstance'
	LEFT JOIN tblStandUpTrainingLanguage STL ON STL.idStandUpTraining = ST.idStandUpTraining AND STL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'standuptraininginstance'
	LEFT JOIN tblLearningPath LP ON LP.idLearningPath = EEQ.idObject AND EEQ.objectType = 'learningpathenrollment'
	LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'learningpathenrollment'
	LEFT JOIN tblLearningPathEnrollment LPE ON LPE.idLearningPathEnrollment = EEQ.idObjectRelated AND EEQ.objectType = 'learningpathenrollment'
	LEFT JOIN tblCertification CR ON CR.idCertification = EEQ.idObject AND EEQ.objectType = 'certification'
	LEFT JOIN tblCertificationLanguage CRL ON CRL.idCertification = CR.idCertification AND CRL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'certification'
	LEFT JOIN tblCertificationToUserLink CUL ON CUL.idCertificationToUserLink = EEQ.idObjectRelated AND EEQ.objectType = 'certification'
	LEFT JOIN tblCertificate CE ON CE.idCertificate = EEQ.idObject AND EEQ.objectType = 'certificate'
	LEFT JOIN tblCertificateLanguage CEL ON CEL.idCertificate = CE.idCertificate AND CEL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'certificate'	
	LEFT JOIN tblCourseFeedMessage CFM ON CFM.idCourseFeedMessage = EEQ.idObject AND EEQ.objectType = 'coursediscussionmessage'
	LEFT JOIN tblCourse MMC ON MMC.idCourse = EEQ.idObjectRelated AND EEQ.objectType = 'coursediscussionmessage'
	LEFT JOIN tblCourseLanguage MMCL ON MMCL.idCourse = MMC.idCourse AND MMCL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'coursediscussionmessage'
	LEFT JOIN tblGroupFeedMessage GFM ON GFM.idGroupFeedMessage = EEQ.idObject AND EEQ.objectType = 'groupdiscussionmessage'
	LEFT JOIN tblGroup MMG ON MMG.idGroup = EEQ.idObjectRelated AND EEQ.objectType = 'groupdiscussionmessage'
	LEFT JOIN tblGroupLanguage MMGL ON MMGL.idGroup = MMG.idGroup AND MMGL.idLanguage = CASE WHEN L.idLanguage IS NOT NULL THEN L.idLanguage ELSE 57 END AND EEQ.objectType = 'groupdiscussionmessage'
	WHERE EEQ.dtAction <= GETUTCDATE()
	AND EEQ.dtAction >= EEQ.dtActivation
	AND EEQ.dtSent IS NULL
	AND EEQ.statusDescription IS NULL
	AND (
			@idSite IS NULL	-- all sites
			OR
			EEQ.idSite = @idSite -- just a specified site
		)
	ORDER BY EEQ.dtAction ASC

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO