-- =====================================================================
-- PROCEDURE: [ReportFile.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ReportFile.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ReportFile.Save]
GO

/*

Save Report File Name and Date

*/

CREATE PROCEDURE [ReportFile.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idReport				INT,
	@filename				NVARCHAR(255),
	@htmlKb					INT, 
	@csvKb					INT,
	@pdfKb					INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	save the data
	
	*/	
		
		BEGIN
		
		-- insert the new Report File Data
		
		INSERT INTO tblReportFile(
			idSite,
			idReport,
			idUser,
			[filename],
			dtCreated,
			htmlKb,
			csvKb,
			pdfKb
		)
		VALUES (
			@idCallerSite,
			@idReport,
			@idCaller,
			@filename,
			GETUTCDATE(),
			@htmlKb,
			@csvKb,
			@pdfKb
		)
		
		
		END		

	SELECT @Return_Code = 0
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO