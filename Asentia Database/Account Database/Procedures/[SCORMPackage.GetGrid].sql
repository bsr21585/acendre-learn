-- =====================================================================
-- PROCEDURE: [SCORMPackage.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[SCORMPackage.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [SCORMPackage.GetGrid]
GO

/*

Gets a listing of Package.

*/

CREATE PROCEDURE [dbo].[SCORMPackage.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblSCORMPackage
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
				)
			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idSCORMPackage,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN name END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN name END
						)
						AS [row_number]
					FROM tblSCORMPackage
					WHERE 
						(
						(@idCallerSite IS NULL) 
						OR 
						(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
						)
					
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idSCORMPackage, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				P.idSCORMPackage,
				P.name,
				P.kb,
				P.dtModified, 
				Convert(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblSCORMPackage P ON P.idSCORMPackage = SelectedKeys.idSCORMPackage
			ORDER BY SelectedKeys.[row_number]
						
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblSCORMPackage
			INNER JOIN CONTAINSTABLE(tblSCORMPackage, *, @searchParam) K ON K.[key] = tblSCORMPackage.idSCORMPackage
			WHERE 
				(
				(@idCallerSite IS NULL) 
				OR 
				(@idCallerSite IS NOT NULL AND idSite = @idCallerSite)
				)
			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idSCORMPackage,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN name END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN name END
						)
						AS [row_number]
					FROM tblSCORMPackage
					INNER JOIN CONTAINSTABLE(tblSCORMPackage, *, @searchParam) K ON K.[key] = tblSCORMPackage.idSCORMPackage
					WHERE 
						(
						@idCallerSite IS NULL
						OR 
						@idCallerSite IS NOT NULL AND idSite = @idCallerSite
						)
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idSCORMPackage, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				P.idSCORMPackage,
				P.name,
				P.kb,
				P.dtModified,  
				Convert(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblSCORMPackage P ON P.idSCORMPackage = SelectedKeys.idSCORMPackage			
			ORDER BY SelectedKeys.[row_number]
			
			END
			
	END

