-- =====================================================================
-- PROCEDURE: [RuleSetLearningPathEnrollment.IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetLearningPathEnrollment.IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetLearningPathEnrollment.IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList]
GO

/*

Returns a recordset of learning path ids and learning path names to populate
"replicate to other learning paths" checkbox list for a ruleset learning path enrollment.

*/

CREATE PROCEDURE [dbo].[RuleSetLearningPathEnrollment.IdsAndLearningPathNamesForRuleSetLearningPathEnrollmentReplicationList]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idRuleSetLearningPathEnrollment	INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END

	IF @searchParam IS NULL
			
		BEGIN

		SELECT DISTINCT
			(SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetLearningPathEnrollment WHERE idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment and idLearningPath = LP.idLearningPath) AS [idRuleSetLearningPathEnrollment],
			LP.idLearningPath, 
			LP.name,
			(SELECT idParentRuleSetLearningPathEnrollment FROM tblRuleSetLearningPathEnrollment WHERE idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment and idLearningPath = LP.idLearningPath) AS [idParentRuleSetLearningPathEnrollment]
		FROM tblLearningPath LP
		LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = @idCallerLanguage 
		WHERE LP.idSite = @idCallerSite 
		AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
		ORDER BY LP.name
		END
			
	ELSE

		BEGIN

		SELECT DISTINCT
			(SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetLearningPathEnrollment WHERE idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment and idLearningPath = LP.idLearningPath) AS [idRuleSetLearningPathEnrollment],
			LP.idLearningPath, 
			LP.name,
			(SELECT idParentRuleSetLearningPathEnrollment FROM tblRuleSetLearningPathEnrollment WHERE idParentRuleSetLearningPathEnrollment = @idRuleSetLearningPathEnrollment and idLearningPath = LP.idLearningPath) AS [idParentRuleSetLearningPathEnrollment]
		FROM tblLearningPath LP
		LEFT JOIN tblLearningPathLanguage LPL ON LPL.idLearningPath = LP.idLearningPath AND LPL.idLanguage = @idCallerLanguage 
		INNER JOIN CONTAINSTABLE(tblLearningPathLanguage, *, @searchParam) K ON K.[key] = LPL.idLearningPathLanguage AND LPL.idLanguage = @idCallerLanguage
		WHERE LP.idSite = @idCallerSite 
		AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
		ORDER BY LP.name
		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO