-- =====================================================================
-- PROCEDURE: [Certificate.EvaluateCompletion]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certificate.EvaluateCompletion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certificate.EvaluateCompletion]
GO

/*

Awards or revokes user certificates for a course or learning path based on the completion action of
the user's respective course or learning path enrollment.

Note, this should not be called from procedural code, only from other procedures that
affect completion status of courses or learning paths, namely [Enrollment.EvaluateCompletion],
[LearningPathEnrollment.EvaluateCompletion], and [Certification.EvaluateCompletion].

*/

CREATE PROCEDURE [Certificate.EvaluateCompletion]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idUser					INT,
	@idObject				INT,
	@objectType				NVARCHAR(20),
	@completionAction		NVARCHAR(10),
	@enrollmentTimezone		INT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/* NO PERMISSIONS CHECKS - WE CANNOT RESTRICT THIS BASED ON PERMISSIONS SINCE IT CAN BE CALLED AS A RESULT OF LEARNER ACTIONS */

	/*
	
	declare variables we're going to be working with
	
	*/

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()

	-- temp table for the object's certificates
	CREATE TABLE #Certificates (
		idCertificate	INT,
		isActive		BIT,
		activationDate	DATETIME
	)

	DECLARE @idCertificateRecord	INT -- variable for the returned certificate record id
	DECLARE @idCertificate			INT

	/*

	course certificates

	*/

	IF @objectType = 'course'
		BEGIN

		/*
	
		validate that the object exists within the site
	
		*/
	
		IF (
			SELECT COUNT(1)
			FROM tblCourse C
			WHERE C.idCourse = @idObject  
			AND (C.idSite <> @idCallerSite)
			--AND (C.isDeleted IS NULL OR C.isDeleted = 0)
			) > 0
		
			BEGIN 
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertificateEvaluateCompletion_NoCourseRecordFound'
			RETURN 1 
			END

		/* 
	
		get all certificates associated with the course id

		*/

		INSERT INTO #Certificates (
			idCertificate,
			isActive,
			activationDate
		)
		SELECT
			C.idCertificate,
			C.isActive,
			C.activationDate
		FROM tblCertificate C
		WHERE C.idSite = @idCallerSite
		AND C.idobject = @idObject
		AND C.objectType = 1 -- course

		/*

		if the action is completed, award certificates for the course to the user

		*/

		IF @completionAction = 'completed'
			BEGIN

			DECLARE certificateAwardCursor CURSOR LOCAL FOR
				SELECT idCertificate FROM #Certificates WHERE isActive = 1 AND activationDate <= @utcNow -- get only the currently active certificates

			OPEN certificateAwardCursor

			FETCH NEXT FROM certificateAwardCursor INTO @idCertificate

			WHILE @@FETCH_STATUS = 0
				BEGIN

				-- if there is no current (unexpired) certificate award, award it
				IF (SELECT COUNT(1) FROM tblCertificateRecord CR WHERE CR.idCertificate = @idCertificate AND CR.idUser = @idUser AND (CR.expires IS NULL OR CR.expires > @utcNow)) = 0
					BEGIN					
					EXEC [CertificateRecord.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idCertificateRecord, @idCertificate, @idUser, @utcNow, @enrollmentTimezone, 1
					END
				ELSE
					BEGIN

					-- check if the "reissue unexpired" flag is set on the certificate; if it is, check for an current unexpired certificate award and update it
					IF (SELECT COUNT(1) FROM tblCertificate C WHERE C.idCertificate = @idCertificate AND reissueBasedOnMostRecentCompletion = 1) = 1
						BEGIN

						-- get the certificate record to update
						SELECT TOP 1 @idCertificateRecord = CR.idCertificateRecord FROM tblCertificateRecord CR WHERE CR.idCertificate = @idCertificate AND CR.idUser = @idUser AND (CR.expires IS NULL OR CR.expires > @utcNow)

						-- update it, if one exists
						IF (@idCertificateRecord IS NOT NULL)
							BEGIN							
							EXEC [CertificateRecord.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idCertificateRecord, @idCertificate, @idUser, @utcNow, @enrollmentTimezone, 1
							SET @idCertificateRecord = NULL -- return @idCertificateRecord back to null so we can continue
							END

						END

					END

				FETCH NEXT FROM certificateAwardCursor INTO @idCertificate

				END

			-- kill the cursor
			CLOSE certificateAwardCursor
			DEALLOCATE certificateAwardCursor

			END

		/*

		if the action is revoked, revoke currently awarded certificates for the course from the user 
		as long as they have no other completed enrollments since the most recently expired certificate award
		also, leave expired certificate records and certificate import certificate records alone

		*/

		IF @completionAction = 'revoked'
			BEGIN

			DECLARE certificateRevokeCursor CURSOR LOCAL FOR
				SELECT idCertificate FROM #Certificates -- get all the certificates

			OPEN certificateRevokeCursor

			FETCH NEXT FROM certificateRevokeCursor INTO @idCertificate

			WHILE @@FETCH_STATUS = 0
				BEGIN

				DELETE
				FROM tblCertificateRecord
				WHERE idUser = @idUser
				AND idCertificate = @idCertificate
				AND idCertificateImport IS NULL				-- do not revoke imported certificate records
				AND (expires IS NULL OR expires > @utcNow)	-- do not revoke any expired certificate records
				-- there are no other completions of the course this certificate belongs to since the last certificate's expiration date
				-- or the certificate has no expiration and there are absolutely no other completions of the course this certificate belongs to
				AND (
						(SELECT COUNT(1) 
						FROM tblEnrollment 
						WHERE idUser = @idUser 
						AND idCourse = @idObject 
						AND dtCompleted IS NOT NULL 
						AND dtCompleted >= (SELECT MAX(CR2.expires)
											FROM tblCertificateRecord CR2
											WHERE CR2.idUser = @idUser
											AND CR2.idCertificate = @idCertificate
											AND CR2.idCertificateImport IS NULL					-- do not look at imported certificate records
											AND CR2.expires IS NOT NULL 
											AND CR2.expires <= @utcNow	-- look at only expired certificate records
											)
						) = 0
						OR
						(
							expires IS NULL
							AND (SELECT COUNT(1)
							FROM tblEnrollment 
							WHERE idUser = @idUser
							AND idCourse = @idObject
							AND dtCompleted IS NOT NULL
							) = 0
						)
					 )
								
				FETCH NEXT FROM certificateRevokeCursor INTO @idCertificate

				END

			-- kill the cursor
			CLOSE certificateRevokeCursor
			DEALLOCATE certificateRevokeCursor

			END

		/* 
		
		drop the temporary table 

		*/

		DROP TABLE #Certificates

		END

	/*

	learning path certificates

	*/

	IF @objectType = 'learningpath'
		BEGIN

		/*
	
		validate that the object exists within the site
	
		*/
	
		IF (
			SELECT COUNT(1)
			FROM tblLearningPath LP
			WHERE LP.idLearningPath = @idObject  
			AND (LP.idSite <> @idCallerSite)
			--AND (LP.isDeleted IS NULL OR LP.isDeleted = 0)
			) > 0
		
			BEGIN 
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertEvaluateCompletion_NoLearningPathRecordFound'
			RETURN 1 
			END

		/* 
	
		get all certificates associated with the learning path id

		*/

		INSERT INTO #Certificates (
			idCertificate,
			isActive,
			activationDate
		)
		SELECT
			C.idCertificate,
			C.isActive,
			C.activationDate
		FROM tblCertificate C
		WHERE C.idSite = @idCallerSite
		AND C.idobject = @idObject
		AND C.objectType = 2 -- learning path

		/*

		if the action is completed, award certificates for the learning path to the user

		*/

		IF @completionAction = 'completed'
			BEGIN

			DECLARE certificateAwardCursor CURSOR LOCAL FOR
				SELECT idCertificate FROM #Certificates WHERE isActive = 1 AND activationDate <= @utcNow -- get only the currently active certificates

			OPEN certificateAwardCursor

			FETCH NEXT FROM certificateAwardCursor INTO @idCertificate

			WHILE @@FETCH_STATUS = 0
				BEGIN

				IF (SELECT COUNT(1) FROM tblCertificateRecord CR WHERE CR.idCertificate = @idCertificate AND CR.idUser = @idUser AND (CR.expires IS NULL OR CR.expires > @utcNow)) = 0
					BEGIN
					EXEC [CertificateRecord.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idCertificateRecord, @idCertificate, @idUser, @utcNow, @enrollmentTimezone, 1
					END
				ELSE
					BEGIN

					-- check if the "reissue unexpired" flag is set on the certificate; if it is, check for an current unexpired certificate award and update it
					IF (SELECT COUNT(1) FROM tblCertificate C WHERE C.idCertificate = @idCertificate AND reissueBasedOnMostRecentCompletion = 1) = 1
						BEGIN

						-- get the certificate record to update
						SELECT TOP 1 @idCertificateRecord = CR.idCertificateRecord FROM tblCertificateRecord CR WHERE CR.idCertificate = @idCertificate AND CR.idUser = @idUser AND (CR.expires IS NULL OR CR.expires > @utcNow)

						-- update it, if one exists
						IF (@idCertificateRecord IS NOT NULL)
							BEGIN
							EXEC [CertificateRecord.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idCertificateRecord, @idCertificate, @idUser, @utcNow, @enrollmentTimezone, 1
							SET @idCertificateRecord = NULL -- return @idCertificateRecord back to null so we can continue
							END

						END

					END

				FETCH NEXT FROM certificateAwardCursor INTO @idCertificate

				END

			-- kill the cursor
			CLOSE certificateAwardCursor
			DEALLOCATE certificateAwardCursor

			END

		/*

		if the action is revoked, revoke currently awarded certificates for the learning path from the user 
		as long as they have no other completed enrollments since the most recently expired certificate award
		also, leave expired certificate records and certificate import certificate records alone

		*/

		IF @completionAction = 'revoked'
			BEGIN

			DECLARE certificateRevokeCursor CURSOR LOCAL FOR
				SELECT idCertificate FROM #Certificates -- get all the certificates

			OPEN certificateRevokeCursor

			FETCH NEXT FROM certificateRevokeCursor INTO @idCertificate

			WHILE @@FETCH_STATUS = 0
				BEGIN

				DELETE
				FROM tblCertificateRecord
				WHERE idUser = @idUser
				AND idCertificate = @idCertificate
				AND idCertificateImport IS NULL				-- do not revoke imported certificate records
				AND (expires IS NULL OR expires > @utcNow)	-- do not revoke any expired certificate records				
				-- there are no other completions of the course this certificate belongs to since the last certificate's expiration date
				-- or the certificate has no expiration and there are absolutely no other completions of the course this certificate belongs to
				AND (
						(SELECT COUNT(1) 
						FROM tblLearningPathEnrollment 
						WHERE idUser = @idUser 
						AND idLearningPath = @idObject 
						AND dtCompleted IS NOT NULL 
						AND dtCompleted >= (SELECT MAX(CR2.expires)
											FROM tblCertificateRecord CR2
											WHERE CR2.idUser = @idUser
											AND CR2.idCertificate = @idCertificate
											AND CR2.idCertificateImport IS NULL					-- do not look at imported certificate records
											AND CR2.expires IS NOT NULL 
											AND CR2.expires <= @utcNow	-- look at only expired certificate records
											)
						) = 0
						OR
						(
							expires IS NULL
							AND (SELECT COUNT(1)
							FROM tblEnrollment 
							WHERE idUser = @idUser
							AND idCourse = @idObject
							AND dtCompleted IS NOT NULL
							) = 0
						)
					 )

				FETCH NEXT FROM certificateRevokeCursor INTO @idCertificate

				END

			-- kill the cursor
			CLOSE certificateRevokeCursor
			DEALLOCATE certificateRevokeCursor

			END
		/* 
		
		drop the temporary table 

		*/

		DROP TABLE #Certificates

		END

	/*

	certification certificates

	*/

	IF @objectType = 'certification'
		BEGIN

		/*
	
		validate that the object exists within the site
	
		*/
	
		IF (
			SELECT COUNT(1)
			FROM tblCertification C
			WHERE C.idCertification = @idObject  
			AND (C.idSite <> @idCallerSite)			
			--AND (C.isDeleted IS NULL OR C.isDeleted = 0)
			) > 0
		
			BEGIN 
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CertificateEvaluateCompletion_NoCertRecordFound'
			RETURN 1 
			END		

		/* 
	
		get all certificates associated with the certification id

		*/

		INSERT INTO #Certificates (
			idCertificate,
			isActive,
			activationDate
		)
		SELECT
			C.idCertificate,
			C.isActive,
			C.activationDate
		FROM tblCertificate C
		WHERE C.idSite = @idCallerSite
		AND C.idObject = @idObject
		AND C.objectType = 3 -- certification

		/*

		if the action is completed, award certificates for the certification to the user

		*/

		IF @completionAction = 'completed'
			BEGIN

			DECLARE certificateAwardCursor CURSOR LOCAL FOR
				SELECT idCertificate FROM #Certificates WHERE isActive = 1 AND activationDate <= @utcNow -- get only the currently active certificates

			OPEN certificateAwardCursor

			FETCH NEXT FROM certificateAwardCursor INTO @idCertificate

			WHILE @@FETCH_STATUS = 0
				BEGIN

				-- if there is no current (unexpired) certificate award, award it
				IF (SELECT COUNT(1) FROM tblCertificateRecord CR WHERE CR.idCertificate = @idCertificate AND CR.idUser = @idUser AND (CR.expires IS NULL OR CR.expires > @utcNow)) = 0
					BEGIN					
					EXEC [CertificateRecord.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idCertificateRecord, @idCertificate, @idUser, @utcNow, @enrollmentTimezone, 1
					END
				ELSE
					BEGIN

					-- check if the "reissue unexpired" flag is set on the certificate; if it is, check for an current unexpired certificate award and update it
					IF (SELECT COUNT(1) FROM tblCertificate C WHERE C.idCertificate = @idCertificate AND reissueBasedOnMostRecentCompletion = 1) = 1
						BEGIN

						-- get the certificate record to update
						SELECT TOP 1 @idCertificateRecord = CR.idCertificateRecord FROM tblCertificateRecord CR WHERE CR.idCertificate = @idCertificate AND CR.idUser = @idUser AND (CR.expires IS NULL OR CR.expires > @utcNow)

						-- update it, if one exists
						IF (@idCertificateRecord IS NOT NULL)
							BEGIN							
							EXEC [CertificateRecord.Save] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, 1, @idCertificateRecord, @idCertificate, @idUser, @utcNow, @enrollmentTimezone, 1
							SET @idCertificateRecord = NULL -- return @idCertificateRecord back to null so we can continue
							END

						END

					END

				FETCH NEXT FROM certificateAwardCursor INTO @idCertificate

				END

			-- kill the cursor
			CLOSE certificateAwardCursor
			DEALLOCATE certificateAwardCursor

			END

		/*

		if the action is revoked, revoke currently awarded certificates for the course from the user 
		as long as they have no other completed enrollments since the most recently expired certificate award
		also, leave expired certificate records and certificate import certificate records alone

		*/

		IF @completionAction = 'revoked'
			BEGIN

			DECLARE certificateRevokeCursor CURSOR LOCAL FOR
				SELECT idCertificate FROM #Certificates -- get all the certificates

			OPEN certificateRevokeCursor

			FETCH NEXT FROM certificateRevokeCursor INTO @idCertificate

			WHILE @@FETCH_STATUS = 0
				BEGIN

				DELETE
				FROM tblCertificateRecord
				WHERE idUser = @idUser
				AND idCertificate = @idCertificate
				AND idCertificateImport IS NULL				-- do not revoke imported certificate records
				AND (expires IS NULL OR expires > @utcNow)	-- do not revoke any expired certificate records
				-- there are no other completions of the certification this certificate belongs to since the last certificate's expiration date
				-- or the certificate has no expiration and there are absolutely no other completions of the course this certificate belongs to
				AND (
						(SELECT COUNT(1) 
						FROM tblCertificationToUserLink 
						WHERE idUser = @idUser 
						AND idCertification = @idObject 
						AND initialAwardDate IS NOT NULL 
						AND initialAwardDate >= (SELECT MAX(CR2.expires)
												 FROM tblCertificateRecord CR2
												 WHERE CR2.idUser = @idUser
												 AND CR2.idCertificate = @idCertificate
												 AND CR2.idCertificateImport IS NULL		-- do not look at imported certificate records
												 AND CR2.expires IS NOT NULL 
												 AND CR2.expires <= @utcNow					-- look at only expired certificate records
												)
						) = 0
						OR
						(
							expires IS NULL
							AND (SELECT COUNT(1)
							FROM tblCertificationToUserLink
							WHERE idUser = @idUser
							AND idCertification = @idObject
							AND initialAwardDate IS NOT NULL
							) = 0
						)
					 )
								
				FETCH NEXT FROM certificateRevokeCursor INTO @idCertificate

				END

			-- kill the cursor
			CLOSE certificateRevokeCursor
			DEALLOCATE certificateRevokeCursor

			END

		-- drop the temporary table 
		DROP TABLE #Certificates

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END
	
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO