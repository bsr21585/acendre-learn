-- =====================================================================
-- PROCEDURE: [Catalog.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.SaveLang]
GO

/*

Add or updates the optional language fields for a catalog

*/

CREATE PROCEDURE [Catalog.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCatalog				INT,
	@title					NVARCHAR(255),
	@shortDescription		NVARCHAR(512),
	@longDescription		NVARCHAR(MAX),
	@searchTags				NVARCHAR(512),
	@languageString			NVARCHAR(10)	
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idCallerLanguage IS NULL
	
		BEGIN 
			SELECT @Return_Code = 1
			SET @Error_Description_Code = 'CatalogSaveLang_LanguageDoesNotExist'
			RETURN 1 
		END
			 
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCatalog
		WHERE idCatalog = @idCatalog
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 1
			SET @Error_Description_Code = 'CatalogSaveLang_DetailsNotFound'
			RETURN 1 
		END
		
	/*
	
	validate uniqueness within language (and within parent)
	-validate only the same fields that the .save procedure validates
	
	*/
	
	DECLARE @idParent INT
	SELECT @idParent = idParent FROM tblCatalog WHERE idCatalog = @idCatalog
	
	IF( 
		SELECT COUNT(1)
		FROM tblCatalog G
		LEFT JOIN tblCatalogLanguage GL ON GL.idCatalog = G.idCatalog AND GL.idLanguage = @idCallerLanguage -- same language
		WHERE G.idSite = @idCallerSite -- same site
		AND G.idCatalog <> @idCatalog -- not the same entry
		AND G.idParent IS NULL AND @idParent IS NULL -- within the same parent
		AND GL.title = @title -- validate parameter: title
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'CatalogSaveLang_CatalogNotUnique'	
		RETURN 1
		END
		
	-- update the multi-language
	
	UPDATE tblCatalogLanguage SET
		title = @title,
		shortDescription = @shortDescription,
		longDescription = @longDescription,
		searchTags = @searchTags
	WHERE idCatalog = @idCatalog
	AND idLanguage = @idCallerLanguage
	
	-- insert multi-language
	
	INSERT INTO tblCatalogLanguage (
		idSite,
		idCatalog,
		idLanguage,
		title,
		shortDescription,
		longDescription,
		searchTags
	)
	SELECT
		@idCallerSite,
		@idCatalog,
		@idCallerLanguage,
		@title,
		@shortDescription,
		@longDescription,
		@searchTags
	WHERE NOT EXISTS (
		SELECT 1
		FROM tblCatalogLanguage CL
		WHERE CL.idCatalog = @idCatalog
		AND CL.idLanguage = @idCallerLanguage
	)
	
	SET @Return_Code = 0 --(0 is 'success')
	SET @Error_Description_Code = ''
		
	END

		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO