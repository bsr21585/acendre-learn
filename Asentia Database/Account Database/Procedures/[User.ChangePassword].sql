-- =====================================================================
-- PROCEDURE: [User.ChangePassword]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.ChangePassword]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.ChangePassword]
GO

/*

Changes the calling user's password.

*/
CREATE PROCEDURE [User.ChangePassword]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@password				NVARCHAR (512)
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	ensure that the caller is not the administrator
	
	*/

	IF (@idCaller = 1)
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = 'UserChangePassword_InvalidUserAccount'
		RETURN 1
		END

	/*

	ensure that the caller exists

	*/

	IF (SELECT COUNT(1) FROM tblUser WHERE idUser = @idCaller) = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'UserChangePassword_NoUserFound'
		RETURN 1
		END

	/*

	hash the new password and compare to the current one, error if they are the same

	*/
	
	DECLARE @hashedNewPassword NVARCHAR(512)
	DECLARE @hashedCurrentPassword NVARCHAR(512)

	SET @hashedNewPassword = dbo.GetHashedString('SHA1', @password)
	SELECT @hashedCurrentPassword = [password] FROM tblUser WHERE idUser = @idCaller

	IF (@hashedCurrentPassword = @hashedNewPassword)
		BEGIN
		SET @Return_Code = 4
		SET @Error_Description_Code = 'UserChangePassword_OldPasswordSameAsNew'
		RETURN 1
		END
	
	-- block the trigger from executing in tblUser UPDATE
	DECLARE @CONTEXT VARBINARY(128)

	SET @CONTEXT = CONVERT(VARBINARY(128), 'BLOCK TRIGGER')
	SET CONTEXT_INFO @CONTEXT

	-- update the user's password and must change password flag
	UPDATE tblUser SET
		[password] = @hashedNewPassword,
		mustchangePassword = 0
	WHERE idUser = @idCaller

	-- unblock the trigger
	SET @CONTEXT = CONVERT(VARBINARY(128), '')
	SET CONTEXT_INFO @CONTEXT
		
	-- return successfully
	SET @Return_Code = 0 --(0 is 'success')
	SET @Error_Description_Code = ''
				
	END			

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
