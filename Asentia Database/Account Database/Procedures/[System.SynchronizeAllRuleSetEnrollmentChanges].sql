-- =====================================================================
-- PROCEDURE: [System.SynchronizeAllRuleSetEnrollmentChanges]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.SynchronizeAllRuleSetEnrollmentChanges]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.SynchronizeAllRuleSetEnrollmentChanges]
GO

/*

Synchronizes ruleset enrollment (learning path and course) changes to their user-inherited enrollments. 
This will update due and expires intervals for inherited enrollments that have not yet been completed where 
parameters of the ruleset enrollment they were inherited by have changed.

*/

CREATE PROCEDURE [System.SynchronizeAllRuleSetEnrollmentChanges]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0
)
WITH RECOMPILE
AS
	
	BEGIN
	SET NOCOUNT ON

	/*

	SYNCHRONIZE CHANGES FOR RULESET INHERITED LEARNING PATH ENROLLMENTS

	*/

	-- create a temporary table to hold the inherited learning path enrollments we need to synchronize changes for
	CREATE TABLE #InheritedRulesetLearningPathEnrollmentUpdates (
		idLearningPathEnrollment					INT,	
		newDueInterval								INT,
		newDueTimeframe								NVARCHAR(4),	
		newExpiresFromStartInterval					INT,
		newExpiresFromStartTimeframe				NVARCHAR(4),	
		newExpiresFromFirstLaunchInterval			INT,
		newExpiresFromFirstLaunchTimeframe			NVARCHAR(4)
	)

	-- get the inherited learning path enrollments where there are changes to the ruleset learning path enrollment
	-- and the inherited enrollment has not been completed
	INSERT INTO #InheritedRulesetLearningPathEnrollmentUpdates (
		idLearningPathEnrollment,		
		newDueInterval,
		newDueTimeframe,
		newExpiresFromStartInterval,
		newExpiresFromStartTimeframe,	
		newExpiresFromFirstLaunchInterval,
		newExpiresFromFirstLaunchTimeframe
	)
	SELECT
		LPE.idLearningPathEnrollment,		
		RSLPE.dueInterval,	
		RSLPE.dueTimeframe,
		RSLPE.expiresFromStartInterval,
		RSLPE.expiresFromStartTimeframe,	
		RSLPE.expiresFromFirstLaunchInterval,
		RSLPE.expiresFromFirstLaunchTimeframe
	FROM tblLearningPathEnrollment LPE
	LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = LPE.idRuleSetLearningPathEnrollment
	WHERE LPE.idRuleSetLearningPathEnrollment IS NOT NULL
	AND RSLPE.idRuleSetLearningPathEnrollment IS NOT NULL
	AND LPE.idLearningPath = RSLPE.idLearningPath
	AND LPE.dtCompleted IS NULL
	AND (
			-- DUE
			(
				(RSLPE.dueInterval IS NULL AND LPE.dueInterval IS NOT NULL)
				OR
				(RSLPE.dueInterval IS NOT NULL AND LPE.dueInterval IS NULL)
				OR
				(RSLPE.dueInterval IS NOT NULL AND LPE.dueInterval IS NOT NULL AND RSLPE.dueInterval <> LPE.dueInterval)
			)
			OR
			(
				(RSLPE.dueTimeframe IS NULL AND LPE.dueTimeframe IS NOT NULL)
				OR
				(RSLPE.dueTimeframe IS NOT NULL AND LPE.dueTimeframe IS NULL)
				OR
				(RSLPE.dueTimeframe IS NOT NULL AND LPE.dueTimeframe IS NOT NULL AND RSLPE.dueTimeframe <> LPE.dueTimeframe)
			)
			OR
			-- EXPIRES FROM START
			(
				(RSLPE.expiresFromStartInterval IS NULL AND LPE.expiresFromStartInterval IS NOT NULL)
				OR
				(RSLPE.expiresFromStartInterval IS NOT NULL AND LPE.expiresFromStartInterval IS NULL)
				OR
				(RSLPE.expiresFromStartInterval IS NOT NULL AND LPE.expiresFromStartInterval IS NOT NULL AND RSLPE.expiresFromStartInterval <> LPE.expiresFromStartInterval)
			)
			OR
			(
				(RSLPE.expiresFromStartTimeframe IS NULL AND LPE.expiresFromStartTimeframe IS NOT NULL)
				OR
				(RSLPE.expiresFromStartTimeframe IS NOT NULL AND LPE.expiresFromStartTimeframe IS NULL)
				OR
				(RSLPE.expiresFromStartTimeframe IS NOT NULL AND LPE.expiresFromStartTimeframe IS NOT NULL AND RSLPE.expiresFromStartTimeframe <> LPE.expiresFromStartTimeframe)
			)
			OR
			-- EXPIRES FROM FIRST LAUNCH
			(
				(RSLPE.expiresFromFirstLaunchInterval IS NULL AND LPE.expiresFromFirstLaunchInterval IS NOT NULL)
				OR
				(RSLPE.expiresFromFirstLaunchInterval IS NOT NULL AND LPE.expiresFromFirstLaunchInterval IS NULL)
				OR
				(RSLPE.expiresFromFirstLaunchInterval IS NOT NULL AND LPE.expiresFromFirstLaunchInterval IS NOT NULL AND RSLPE.expiresFromFirstLaunchInterval <> LPE.expiresFromFirstLaunchInterval)
			)
			OR
			(
				(RSLPE.expiresFromFirstLaunchTimeframe IS NULL AND LPE.expiresFromFirstLaunchTimeframe IS NOT NULL)
				OR
				(RSLPE.expiresFromFirstLaunchTimeframe IS NOT NULL AND LPE.expiresFromFirstLaunchTimeframe IS NULL)
				OR
				(RSLPE.expiresFromFirstLaunchTimeframe IS NOT NULL AND LPE.expiresFromFirstLaunchTimeframe IS NOT NULL AND RSLPE.expiresFromFirstLaunchTimeframe <> LPE.expiresFromFirstLaunchTimeframe)
			)
		)

	-- synchronize the inherited learning path enrollments
	UPDATE tblLearningPathEnrollment SET
		dtDue = CASE WHEN IRLPEU.newDueInterval IS NULL THEN NULL ELSE dbo.IDateAdd(IRLPEU.newDueTimeframe, IRLPEU.newDueInterval, tblLearningPathEnrollment.dtStart) END,
		dtExpiresFromStart = CASE WHEN IRLPEU.newExpiresFromStartInterval IS NULL THEN NULL ELSE dbo.IDateAdd(IRLPEU.newExpiresFromStartTimeframe, IRLPEU.newExpiresFromStartInterval, tblLearningPathEnrollment.dtStart) END,
		dtExpiresFromFirstLaunch = CASE WHEN IRLPEU.newExpiresFromFirstLaunchInterval IS NOT NULL AND tblLearningPathEnrollment.dtFirstLaunch IS NOT NULL THEN dbo.IDateAdd(IRLPEU.newExpiresFromFirstLaunchTimeframe, IRLPEU.newExpiresFromFirstLaunchInterval, tblLearningPathEnrollment.dtFirstLaunch) ELSE NULL END,
		dueInterval = IRLPEU.newDueInterval,
		dueTimeframe = IRLPEU.newDueTimeframe,
		expiresFromStartInterval = IRLPEU.newExpiresFromStartInterval,
		expiresFromStartTimeframe = IRLPEU.newExpiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = IRLPEU.newExpiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = IRLPEU.newExpiresFromFirstLaunchTimeframe   
	FROM #InheritedRulesetLearningPathEnrollmentUpdates IRLPEU
	WHERE IRLPEU.idLearningPathEnrollment = tblLearningPathEnrollment.idLearningPathEnrollment

	-- synchronize the course enrollments that were part of the inherited learning path enrollment
	UPDATE tblEnrollment SET
		dtDue = CASE WHEN IRLPEU.newDueInterval IS NULL THEN NULL ELSE dbo.IDateAdd(IRLPEU.newDueTimeframe, IRLPEU.newDueInterval, tblEnrollment.dtStart) END,
		dtExpiresFromStart = CASE WHEN IRLPEU.newExpiresFromStartInterval IS NULL THEN NULL ELSE dbo.IDateAdd(IRLPEU.newExpiresFromStartTimeframe, IRLPEU.newExpiresFromStartInterval, tblEnrollment.dtStart) END,
		dtExpiresFromFirstLaunch = CASE WHEN IRLPEU.newExpiresFromFirstLaunchInterval IS NOT NULL AND tblEnrollment.dtFirstLaunch IS NOT NULL THEN dbo.IDateAdd(IRLPEU.newExpiresFromFirstLaunchTimeframe, IRLPEU.newExpiresFromFirstLaunchInterval, tblEnrollment.dtFirstLaunch) ELSE NULL END,
		dueInterval = IRLPEU.newDueInterval,
		dueTimeframe = IRLPEU.newDueTimeframe,
		expiresFromStartInterval = IRLPEU.newExpiresFromStartInterval,
		expiresFromStartTimeframe = IRLPEU.newExpiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = IRLPEU.newExpiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = IRLPEU.newExpiresFromFirstLaunchTimeframe   
	FROM #InheritedRulesetLearningPathEnrollmentUpdates IRLPEU
	WHERE IRLPEU.idLearningPathEnrollment = tblEnrollment.idLearningPathEnrollment


	/*

	SYNCHRONIZE CHANGES FOR RULESET INHERITED COURSE ENROLLMENTS

	*/

	-- create a temporary table to hold the inherited course enrollments we need to synchronize changes for
	CREATE TABLE #InheritedRulesetEnrollmentUpdates (
		idEnrollment								INT,	
		newDueInterval								INT,
		newDueTimeframe								NVARCHAR(4),	
		newExpiresFromStartInterval					INT,
		newExpiresFromStartTimeframe				NVARCHAR(4),	
		newExpiresFromFirstLaunchInterval			INT,
		newExpiresFromFirstLaunchTimeframe			NVARCHAR(4)
	)

	-- get the inherited course enrollments where there are changes to the ruleset course enrollment
	-- and the inherited enrollment has not been completed
	INSERT INTO #InheritedRulesetEnrollmentUpdates (
		idEnrollment,		
		newDueInterval,
		newDueTimeframe,
		newExpiresFromStartInterval,
		newExpiresFromStartTimeframe,	
		newExpiresFromFirstLaunchInterval,
		newExpiresFromFirstLaunchTimeframe
	)
	SELECT
		E.idEnrollment,		
		RSE.dueInterval,	
		RSE.dueTimeframe,
		RSE.expiresFromStartInterval,
		RSE.expiresFromStartTimeframe,	
		RSE.expiresFromFirstLaunchInterval,
		RSE.expiresFromFirstLaunchTimeframe
	FROM tblEnrollment E
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = E.idRuleSetEnrollment
	WHERE E.idRuleSetEnrollment IS NOT NULL
	AND RSE.idRuleSetEnrollment IS NOT NULL
	AND E.idCourse = RSE.idCourse
	AND E.dtCompleted IS NULL
	AND E.idActivityImport IS NULL
	AND (
			-- DUE
			(
				(RSE.dueInterval IS NULL AND E.dueInterval IS NOT NULL)
				OR
				(RSE.dueInterval IS NOT NULL AND E.dueInterval IS NULL)
				OR
				(RSE.dueInterval IS NOT NULL AND E.dueInterval IS NOT NULL AND RSE.dueInterval <> E.dueInterval)
			)
			OR
			(
				(RSE.dueTimeframe IS NULL AND E.dueTimeframe IS NOT NULL)
				OR
				(RSE.dueTimeframe IS NOT NULL AND E.dueTimeframe IS NULL)
				OR
				(RSE.dueTimeframe IS NOT NULL AND E.dueTimeframe IS NOT NULL AND RSE.dueTimeframe <> E.dueTimeframe)
			)
			OR
			-- EXPIRES FROM START
			(
				(RSE.expiresFromStartInterval IS NULL AND E.expiresFromStartInterval IS NOT NULL)
				OR
				(RSE.expiresFromStartInterval IS NOT NULL AND E.expiresFromStartInterval IS NULL)
				OR
				(RSE.expiresFromStartInterval IS NOT NULL AND E.expiresFromStartInterval IS NOT NULL AND RSE.expiresFromStartInterval <> E.expiresFromStartInterval)
			)
			OR
			(
				(RSE.expiresFromStartTimeframe IS NULL AND E.expiresFromStartTimeframe IS NOT NULL)
				OR
				(RSE.expiresFromStartTimeframe IS NOT NULL AND E.expiresFromStartTimeframe IS NULL)
				OR
				(RSE.expiresFromStartTimeframe IS NOT NULL AND E.expiresFromStartTimeframe IS NOT NULL AND RSE.expiresFromStartTimeframe <> E.expiresFromStartTimeframe)
			)
			OR
			-- EXPIRES FROM FIRST LAUNCH
			(
				(RSE.expiresFromFirstLaunchInterval IS NULL AND E.expiresFromFirstLaunchInterval IS NOT NULL)
				OR
				(RSE.expiresFromFirstLaunchInterval IS NOT NULL AND E.expiresFromFirstLaunchInterval IS NULL)
				OR
				(RSE.expiresFromFirstLaunchInterval IS NOT NULL AND E.expiresFromFirstLaunchInterval IS NOT NULL AND RSE.expiresFromFirstLaunchInterval <> E.expiresFromFirstLaunchInterval)
			)
			OR
			(
				(RSE.expiresFromFirstLaunchTimeframe IS NULL AND E.expiresFromFirstLaunchTimeframe IS NOT NULL)
				OR
				(RSE.expiresFromFirstLaunchTimeframe IS NOT NULL AND E.expiresFromFirstLaunchTimeframe IS NULL)
				OR
				(RSE.expiresFromFirstLaunchTimeframe IS NOT NULL AND E.expiresFromFirstLaunchTimeframe IS NOT NULL AND RSE.expiresFromFirstLaunchTimeframe <> E.expiresFromFirstLaunchTimeframe)
			)
		)

	-- synchronize the inherited course enrollments
	UPDATE tblEnrollment SET
		dtDue = CASE WHEN IREU.newDueInterval IS NULL THEN NULL ELSE dbo.IDateAdd(IREU.newDueTimeframe, IREU.newDueInterval, tblEnrollment.dtStart) END,
		dtExpiresFromStart = CASE WHEN IREU.newExpiresFromStartInterval IS NULL THEN NULL ELSE dbo.IDateAdd(IREU.newExpiresFromStartTimeframe, IREU.newExpiresFromStartInterval, tblEnrollment.dtStart) END,
		dtExpiresFromFirstLaunch = CASE WHEN IREU.newExpiresFromFirstLaunchInterval IS NOT NULL AND tblEnrollment.dtFirstLaunch IS NOT NULL THEN dbo.IDateAdd(IREU.newExpiresFromFirstLaunchTimeframe, IREU.newExpiresFromFirstLaunchInterval, tblEnrollment.dtFirstLaunch) ELSE NULL END,
		dueInterval = IREU.newDueInterval,
		dueTimeframe = IREU.newDueTimeframe,
		expiresFromStartInterval = IREU.newExpiresFromStartInterval,
		expiresFromStartTimeframe = IREU.newExpiresFromStartTimeframe,
		expiresFromFirstLaunchInterval = IREU.newExpiresFromFirstLaunchInterval,
		expiresFromFirstLaunchTimeframe = IREU.newExpiresFromFirstLaunchTimeframe   
	FROM #InheritedRulesetEnrollmentUpdates IREU
	WHERE IREU.idEnrollment = tblEnrollment.idEnrollment


	/*

	DROP THE TEMPORARY TABLES

	*/

	DROP TABLE #InheritedRulesetLearningPathEnrollmentUpdates
	DROP TABLE #InheritedRulesetEnrollmentUpdates

	/* RETURN */
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO