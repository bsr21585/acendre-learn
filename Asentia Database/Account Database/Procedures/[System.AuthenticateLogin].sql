-- =====================================================================
-- PROCEDURE: [System.AuthenticateLogin]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF exists (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.AuthenticateLogin]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.AuthenticateLogin]
GO

/*

Authenticates credentials for login to the system.

*/
CREATE PROCEDURE [System.AuthenticateLogin]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerSite					INT				= 0, --default if not specified
	@callerLangString				NVARCHAR(10),
	@idCaller						INT				= 0,
	
	@idSite							INT				OUTPUT,
	@idSiteUser						INT				OUTPUT,
	@userFirstName					NVARCHAR(255)	OUTPUT,
	@userLastName					NVARCHAR(255)	OUTPUT,
	@userAvatar						NVARCHAR(255)	OUTPUT,
	@userGender						NVARCHAR(1)		OUTPUT,
	@userCulture					NVARCHAR(20)	OUTPUT,
	@isUserACourseExpert			BIT				OUTPUT,
	@isUserACourseApprover			BIT				OUTPUT,
	@isUserASupervisor				BIT				OUTPUT,
	@isUserAWallModerator			BIT				OUTPUT,
	@isUserAnILTInstructor			BIT				OUTPUT,
	@userMustChangePassword			BIT				OUTPUT,
	@mustExecuteLicenseAgreement	BIT				OUTPUT,
	@lastActiveSessionId			NVARCHAR(80)	OUTPUT,
	@dtUserAgreementAgreed			DATETIME		OUTPUT,
	
	@username						NVARCHAR(512),
	@password						NVARCHAR(512),
	@loginPriority					NVARCHAR(10),
	@simultaniousLogin				BIT,
	@dtSessionExpires	 			DATETIME,
	@sessionId						NVARCHAR(80),
	@clientIPAddress				NVARCHAR(15),
	@lockoutMinutes					INT,
	@lockoutAttempts				INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @count INT
	SET @count = 0
	DECLARE @failCount INT
	SET @failCount = 0
	DECLARE @idUser INT
	DECLARE @lockout BIT
	SET @lockout = 0
	    
	/* 
	
	attempt to authenticate the administrator user, this
	is the only account that should be named "administrator"
	
	*/
	
	IF @username = 'admin' OR @username = 'administrator'
		BEGIN
				
		SELECT @count = COUNT(1)
		FROM tblSite S
		WHERE (S.[password] = dbo.GetHashedString('SHA1', @password) OR S.[password] = LOWER(CONVERT(NVARCHAR(512), HashBytes('MD5', CONVERT(VARCHAR, @password)), 2))) -- the check against MD5 hash is for users conveted from Inquisiq
		AND S.idSite = @idSite
		
		IF (@count) = 0 
		BEGIN
		SET @Return_Code = 3 -- failed login
		SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'
		RETURN 1
		END
	
		SELECT TOP 1
			@idSite = S.idSite,
			@idSiteUser = 1,
			@userFirstName = 'ADMINISTRATOR',
			@userLastName = '',
			@userAvatar = NULL,
			@userGender = NULL,
			@userCulture = L.code,
			@isUserACourseExpert = 1,
			@isUserACourseApprover = 1,
			@isUserASupervisor = 1,
			@isUserAWallModerator = 1,
			@isUserAnILTInstructor = 1,
			@userMustChangePassword = 0,
			@mustExecuteLicenseAgreement = CASE WHEN (SELECT COUNT(1) FROM tblSite WHERE idSite = S.idSite AND S.dtLicenseAgreementExecuted IS NULL) > 0 THEN 1 ELSE 0 END,
			@dtUserAgreementAgreed = GETUTCDATE()
		FROM tblSite S
		LEFT JOIN tblLanguage L ON L.idLanguage = S.idLanguage
		WHERE (S.[password] = dbo.GetHashedString('SHA1', @password) OR S.[password] = LOWER(CONVERT(NVARCHAR(512), HashBytes('MD5', CONVERT(VARCHAR, @password)), 2))) -- the check against MD5 hash is for users conveted from Inquisiq
		AND S.idSite = @idSite
			
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		
		END
		
	ELSE
		BEGIN
		
		/*
		
		attempt to authenticate as a normal user
		
		*/
		
		/*
		
		Check if user account has been locked out
		
		*/
				
		IF (@lockoutAttempts) > 0 
			BEGIN
				SELECT @failCount = ISNULL(UL.failCount, 0),
				@idUser = U.idUser,
				@lockout = CASE WHEN UL.dtLockoutUntil > GETDATE() THEN 1
								ELSE 0
								END
				FROM tblUser U
				LEFT JOIN tblUserLockout UL ON UL.idUser = U.idUser
				WHERE U.username = @username

				IF (@lockout) = 1 
				BEGIN
					SET @Return_Code = 3 -- user account lockout
					SET @Error_Description_Code = 'UserAccountIsLockedDueToTooManyFailedLoginAttempts'
					RETURN 1
				END
			END
		ELSE
			BEGIN
				DELETE FROM tblUserLockout 
				WHERE idSite = @idSite
			END

		SELECT @count = COUNT(1)
		FROM tblUser U
		WHERE U.username = @username
		AND (U.[password] = dbo.GetHashedString('SHA1', @password) OR U.[password] = LOWER(CONVERT(NVARCHAR(512), HashBytes('MD5', CONVERT(VARCHAR, @password)), 2))) -- the check against MD5 hash is for users conveted from Inquisiq
		AND U.idSite = @idSite
		AND U.isDeleted = 0
		
			IF (@count) = 0 
			BEGIN

			/*
		
			if Lockout setting is on
		
			*/
			 IF (@lockoutAttempts) > 0 
				 BEGIN
					IF (@lockoutAttempts > 0 AND @failCount = 0)
					BEGIN
						INSERT INTO tblUserLockout
						(idSite, idUser, failCount)
							SELECT @idSite, idUser, 1
								FROM tblUser U
								WHERE U.username = @username
						SET @Return_Code = 3 -- failed login
						SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'
					END

					IF ((@lockoutAttempts > @failCount +1) AND @failCount >= 1 )
					BEGIN
						UPDATE tblUserLockout
						SET failCount = @failCount +1
						WHERE idUser = @idUser	
						SET @Return_Code = 3 -- failed login
						SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'
					END

					IF ( @lockoutAttempts > 0 AND @lockoutAttempts <= @failCount + 1 )
					BEGIN
						UPDATE tblUserLockout
						SET failCount = @failCount +1,
							dtLockoutUntil = DATEADD(minute, @lockoutMinutes, GETDATE())
						WHERE idUser = @idUser	

						SET @Return_Code = 3 -- user account lockout
						SET @Error_Description_Code = 'UserAccountIsLockedDueToTooManyFailedLoginAttempts'
						RETURN 1
					END
				  END
			  ELSE
				  BEGIN
						SET @Return_Code = 3 -- failed login
						SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'	
				  END						
			END

		IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'SystemAuthenticateLogin_DuplicateAccount'
		RETURN 1
		END
		
		IF (@count) = 1
		BEGIN

			/*
			
			check for disabled account
			
			*/
			IF (SELECT U.isActive 
				FROM tblUser U 
				WHERE U.username = @username 
				AND U.idSite = @idSite
				AND U.isDeleted = 0) = 0
			BEGIN
			SET @Return_Code = 3 -- caller permission error
			SET @Error_Description_Code = 'SystemAuthenticateLogin_DisabledAccount'
			RETURN 1
			END
			
			/*

			check for expired account

			*/
			IF (SELECT U.dtExpires 
				FROM tblUser U 
				WHERE U.username = @username 
				AND U.idSite = @idSite
				AND U.isDeleted = 0) < GETUTCDATE()
			BEGIN
			SET @Return_Code = 3 -- caller permission error
			SET @Error_Description_Code = 'SystemAuthenticateLogin_ExpiredAccount'
			RETURN 1
			END

			/*

			check if account is awaiting approval

			*/
			IF (SELECT U.isRegistrationApproved 
				FROM tblUser U 
				WHERE U.username = @username 
				AND U.idSite = @idSite
				AND U.isDeleted = 0) = 0
			BEGIN
			SET @Return_Code = 3 -- caller permission error
			SET @Error_Description_Code = 'SystemAuthenticateLogin_AccountAwaitingApproval'
			RETURN 1
			END

			/*

			check if account is prevented from logging in via login form

			*/
			IF (SELECT U.disableLoginFromLoginForm 
				FROM tblUser U 
				WHERE U.username = @username 
				AND U.idSite = @idSite
				AND U.isDeleted = 0) = 1
			BEGIN
			SET @Return_Code = 3 -- caller permission error
			SET @Error_Description_Code = 'SystemAuthenticateLogin_LoginDisabledFromLoginForm'
			RETURN 1
			END

			/*

			check if user is prevented from logging in from their current IP address

			*/
			IF (SELECT ISNULL(U.exemptFromIPLoginRestriction, 0)
				FROM tblUser U
				WHERE U.username = @username 
				AND U.idSite = @idSite
				AND U.isDeleted = 0) <> 1
				BEGIN

				DECLARE @ipRestrictionSetting NVARCHAR(10)		

				SELECT 		
					@ipRestrictionSetting = CASE WHEN SP.[idSiteParam] IS NOT NULL THEN SP.[value] ELSE DSP.[value] END
				FROM tblSiteParam DSP
				LEFT JOIN tblSiteParam SP ON SP.[param] = DSP.[param] AND SP.idSite = @idSite
				WHERE DSP.idSite = 1 AND DSP.[param] = 'System.IPAddressRestriction'

				IF (
					(@ipRestrictionSetting = 'login')
					AND 
					(SELECT COUNT(1) FROM tblSiteIPRestriction WHERE ip = @clientIPAddress AND idSite = @idSite) = 0
				   )
				BEGIN
				SET @Return_Code = 3 -- caller permission error
				SET @Error_Description_Code = 'SystemAuthenticateLogin_LoginDisabledFromLocation'
				RETURN 1
				END

				END
			
			/*

			validate simultanious login for normal user 

			*/
			IF(@simultaniousLogin = 0)
				BEGIN				
				IF(@loginPriority = 'first') 
					BEGIN
					IF((SELECT dtSessionExpires FROM tblUser 
						WHERE username = @username) > GETUTCDATE()
						)
						BEGIN
						SET @Return_Code = 3 -- caller permission error
						SET @Error_Description_Code = 'SystemAuthenticateLogin_UserAlreadyLoggedIn'
						RETURN 1
						END	
					END
				END
		
			/*

			populate user's session information and proceed with login

			*/
			SELECT TOP 1
				@idSite							= U.idSite,
				@idSiteUser						= U.idUser,
				@userFirstName					= U.firstName,
				@userLastName					= U.lastName,
				@userAvatar						= U.avatar,
				@userGender						= U.gender,
				@userCulture					= L.code,
				@isUserACourseExpert			= CASE WHEN (SELECT COUNT(1) FROM tblCourseExpert WHERE idUser = U.idUser) > 0 THEN 1 ELSE 0 END,
				@isUserACourseApprover			= CASE WHEN (SELECT COUNT(1) FROM tblCourseEnrollmentApprover WHERE idUser = U.idUser) > 0 THEN 1 ELSE 0 END,
				@isUserASupervisor				= CASE WHEN (SELECT COUNT(1) FROM tblUserToSupervisorLink WHERE idSupervisor = U.idUser) > 0 THEN 1 ELSE 0 END,
				@isUserAWallModerator			= CASE WHEN 
													(SELECT COUNT(1) FROM tblCourseFeedModerator WHERE idModerator = U.idUser) > 0 
													OR (SELECT COUNT(1) FROM tblGroupFeedModerator WHERE idModerator = U.idUser) > 0 
												  THEN 1 ELSE 0 END,
				@isUserAnILTInstructor			= CASE WHEN (SELECT COUNT(1) FROM tblStandupTrainingInstanceToInstructorLink WHERE idInstructor = U.idUser) > 0 THEN 1 ELSE 0 END,				
				@userMustChangePassword 		= CASE WHEN U.mustchangePassword = 1 THEN 1 ELSE 0 END,
				@lastActiveSessionId			= U.activeSessionId,
				@mustExecuteLicenseAgreement 	= 0,
				@dtUserAgreementAgreed  		= U.dtUserAgreementAgreed
			FROM tblUser U
			LEFT JOIN tblLanguage L ON L.idLanguage = U.idLanguage
			WHERE U.username = @username
			AND (U.[password] = dbo.GetHashedString('SHA1', @password) OR U.[password] = LOWER(CONVERT(NVARCHAR(512), HashBytes('MD5', CONVERT(VARCHAR, @password)), 2))) -- the check against MD5 hash is for users conveted from Inquisiq
			AND U.idSite = @idSite
			AND U.isDeleted = 0

			-- block the trigger from executing in tblUser UPDATE
			DECLARE @CONTEXT VARBINARY(128)

			SET @CONTEXT = CONVERT(VARBINARY(128), 'BLOCK TRIGGER')
			SET CONTEXT_INFO @CONTEXT

			-- update user last login date and clean up lockout record
			UPDATE tblUser SET 
				dtLastLogin			=	GETUTCDATE(),
				dtSessionExpires	=	@dtSessionExpires,
				activeSessionId		=	@sessionId
			WHERE idUser = @idSiteUser

			INSERT INTO tblUserLog
			VALUES (@idSiteUser, @idSite, 'login', GETUTCDATE())

			DELETE FROM tblUserLockout 
			WHERE idUser = @idSiteUser

			-- unblock the trigger
			SET @CONTEXT = CONVERT(VARBINARY(128), '')
			SET CONTEXT_INFO @CONTEXT
			
			SELECT @Return_Code = 0 --(0 is 'success')
			RETURN 1
		END
		
		END
		
		RETURN 1
		
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO