-- =====================================================================
-- PROCEDURE: [System.GetRuleSetGroupMatches]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetRuleSetGroupMatches]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetRuleSetGroupMatches]
GO

/*

Gets RuleSet matches for Groups.

*/
CREATE PROCEDURE [System.GetRuleSetGroupMatches]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,

	@Groups						IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* SET FILTER TO 'user' - IT CAN ONLY BE USER */
	SET @filterBy = 'user'

	/* CHECK GROUP AND FILTER LISTS FOR RECORDS */
	DECLARE @GroupsRecordCount INT
	DECLARE @FiltersRecordCount INT
	SELECT @GroupsRecordCount = COUNT(1) FROM @Groups
	SELECT @FiltersRecordCount = COUNT(1) FROM @Filters

	/* 
	
		CREATE TEMPORARY TABLE TO STORE USER-TO-GROUP JOINS
		RESULTING FROM RULES EXECUTION
	*/
	CREATE TABLE #UserToGroupJoins
	(
		idRuleSet	INT,
		idGroup		INT,
		idUser		INT
	)

	/* CREATE TEMPORARY TABLE FOR GROUP AUTO-JOIN RULES */
	CREATE TABLE #GroupRules
	(
		idSite		INT,
		idRuleSet	INT,
		idGroup		INT,
		isAny		BIT,
		field		NVARCHAR(25),
		operator	NVARCHAR(25),
		value		NVARCHAR(255)
	)

	/* PUT GROUP AUTO-JOIN RULESETS IN TEMP TABLE */
	INSERT INTO #GroupRules
	(
		idSite,
		idRuleSet,
		idGroup,
		isAny,
		field,
		operator,
		value
	)
	SELECT
		RS.idSite,
		RS.idRuleSet,
		RSGL.idGroup,
		RS.isAny,
		R.userField,
		R.operator,
		CASE WHEN R.textValue IS NOT NULL THEN 
			REPLACE(R.textValue, '''', '''''')
		ELSE
			CASE WHEN R.dateValue IS NOT NULL THEN
				CONVERT(NVARCHAR(255), R.dateValue)
			ELSE
				CASE WHEN R.numValue IS NOT NULL THEN
					CONVERT(NVARCHAR(255), R.numValue)
				ELSE
					CASE WHEN R.bitValue IS NOT NULL THEN
						CONVERT(NVARCHAR(255), R.bitValue)
					ELSE
						NULL
					END
				END
			END
		END
	FROM tblRuleSet RS
	LEFT JOIN tblRule R ON R.idRuleSet = RS.idRuleSet
	LEFT JOIN tblRuleSetToGroupLink RSGL ON RSGL.idRuleSet = RS.idRuleSet	
	WHERE RSGL.idRuleSetToGroupLink IS NOT NULL	
	AND (
			(@GroupsRecordCount = 0)
			OR 
			(@GroupsRecordCount > 0 AND EXISTS (SELECT 1 FROM @Groups GP WHERE GP.id = RSGL.idGroup))
		)

	/* DECLARE VARIABLES FOR BUILDING RULE EXECUTION QUERIES */
	DECLARE @groupsWithRuleSets TABLE (id INT, idSite INT)
	DECLARE @ruleSetsForGroup IDTable
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @idGroup INT
	DECLARE @idSite INT
	DECLARE @idRuleSet INT
	DECLARE @isAny BIT
	DECLARE @field NVARCHAR(25)
	DECLARE @operator NVARCHAR(25)
	DECLARE @value NVARCHAR(255)
	DECLARE @ruleCounter INT

	/* GET IDS OF GROUPS WITH RULESETS */
	INSERT INTO @groupsWithRuleSets ( 
		id,
		idSite
	)
	SELECT DISTINCT
		idGroup,
		idSite
	FROM #GroupRules

	/* CURSOR FOR GROUPS WITH RULESETS */
	DECLARE groupsCursor CURSOR FOR
	SELECT
		id,
		idSite
	FROM @groupsWithRuleSets

	OPEN groupsCursor

	FETCH NEXT FROM groupsCursor INTO @idGroup, @idSite

	/* LOOP THROUGH GROUPS RULESETS */
	WHILE (@@FETCH_STATUS = 0)

		BEGIN

		/* GET IDS OF RULESETS FOR THE GROUP */
		INSERT INTO @ruleSetsForGroup
		( id )
		SELECT DISTINCT
			idRuleSet
		FROM #GroupRules
		WHERE idGroup = @idGroup

		/* CURSOR FOR RULESETS FOR THE GROUP */
		DECLARE groupRuleSetsCursor CURSOR FOR
		SELECT DISTINCT
			id,
			GR.isAny
		FROM @ruleSetsForGroup
		LEFT JOIN #GroupRules GR ON GR.idRuleSet = id

		OPEN groupRuleSetsCursor

		FETCH NEXT FROM groupRuleSetsCursor INTO @idRuleSet, @isAny
	
		/* LOOP THROUGH RULESETS FOR THE GROUP */
		WHILE (@@FETCH_STATUS = 0)

			BEGIN

			/* START QUERY SQL STRING FOR RULESET */
			SET @sql = 'INSERT INTO #UserToGroupJoins (idRuleSet, idGroup, idUser) SELECT ' + CONVERT(NVARCHAR, @idRuleSet) + ', ' + CONVERT(NVARCHAR, @idGroup) + ', idUser ' + 'FROM tblUser U WHERE ('
			
			/* RESET THE RULE COUNTER */
			SET @ruleCounter = 0

			/* CURSOR FOR RULESET RULES */
			DECLARE ruleSetRulesCursor CURSOR FOR
			SELECT
				field,
				operator,
				value
			FROM #GroupRules
			WHERE idRuleSet = @idRuleSet

			OPEN ruleSetRulesCursor
		
			FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
	
			/* LOOP THROUGH RULESET RULES */
			WHILE (@@FETCH_STATUS = 0)

				BEGIN

				/* IF THIS IS NOT THE FIRST RULE, EXAMINE isAny LOGIC TO DETERMINE AND/OR OPERATOR */
				IF @ruleCounter > 0
					BEGIN
						IF @isAny = 1
							BEGIN
							SET @sql = @sql + ' OR '
							END
						ELSE
							BEGIN
							SET @sql = @sql + ' AND '
							END
					END

				/* 
				
				BUILD WHERE CLAUSE BASED ON OPERATOR - DIFFERENT LOGIC FOR GROUP 
				GROUP EVALUATES ON THE "name" IN THE BASE TABLE, NOT EVERY LANGUAGE JUST THE DEFAULT

				YES, YOU CAN BE JOINED TO A GROUP AS A RESULT OF BEING A MEMBER OF ANOTHER GROUP!

				*/

				IF @field = 'group'
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''' + @value + '%''' + ')))'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '''' + ')))'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name LIKE ' + '''%' + @value + '%''' + ')))'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(idUser NOT IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT idGroup FROM tblGroup WHERE tblGroup.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND name = ' + '''' + @value + '''' + ')))'
						END

					END

				ELSE
					BEGIN

					IF @operator = 'sw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'nsw'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''' + @value + '%''' + ')'
						END

					IF @operator = 'ew'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'new'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '''' + ')'
						END

					IF @operator = 'c'
						BEGIN
						SET @sql = @sql + '(' + @field + ' LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'nc'
						BEGIN
						SET @sql = @sql + '(' + @field + ' NOT LIKE ' + '''%' + @value + '%''' + ')'
						END

					IF @operator = 'eq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' = ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'neq'
						BEGIN
						SET @sql = @sql + '(' + @field + ' <> ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltn'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS INT) < ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'gtd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) > ' + '''' + @value + '''' + ')'
						END

					IF @operator = 'ltd'
						BEGIN
						SET @sql = @sql + '(TRY_CAST(' + @field + ' AS DATETIME) < ' + '''' + @value + '''' + ')'
						END

					END
			
				/* INCREMENT RULE COUNTER */
				SET @ruleCounter = @ruleCounter + 1

				FETCH NEXT FROM ruleSetRulesCursor INTO @field, @operator, @value
				END

			/* CLOSE AND DEALLOCATE CURSOR FOR RULESET RULES */
			CLOSE ruleSetRulesCursor
			DEALLOCATE ruleSetRulesCursor

			/* APEND ANDs FOR THE SITE AND USER DELETED STATUS */
			SET @sql = @sql + ') AND (U.idSite = ' + CONVERT(NVARCHAR, @idSite) + ' AND ((U.isDeleted IS NULL OR U.isDeleted = 0) AND (U.isRegistrationApproved IS NULL OR U.isRegistrationApproved = 1)))'

			/* EXECUTE THE SQL TO INSERT RECORDS IN THE #UserToGroupJoins TABLE */
			EXECUTE sp_executesql @sql
		
			FETCH NEXT FROM groupRuleSetsCursor INTO @idRuleSet, @isAny
			END

		/* CLOSE AND DEALLOCATE CURSOR FOR RULESETS FOR THE GROUP */
		DELETE FROM @ruleSetsForGroup
		CLOSE groupRuleSetsCursor
		DEALLOCATE groupRuleSetsCursor

		FETCH NEXT FROM groupsCursor INTO @idGroup, @idSite
		END

	/* CLOSE AND DEALLOCATE CURSOR FOR GROUPS WITH RULESETS */
	CLOSE groupsCursor
	DEALLOCATE groupsCursor
				  
	/* RETURN RECORDS */
	SELECT DISTINCT
		G.idSite,
		UGJ.idRuleSet,
		UGJ.idGroup,
		UGJ.idUser
	FROM #UserToGroupJoins UGJ
	LEFT JOIN tblGroup G ON G.idGroup = UGJ.idGroup
	WHERE (  
			  @filterBy = 'user' AND
			  (
				(@FiltersRecordCount = 0)
				OR 
				(@FiltersRecordCount > 0 AND EXISTS (SELECT 1 FROM @Filters FP WHERE FP.id = UGJ.idUser))
			  )
		  )
	
	/* DROP TEMPORARY TABLES */
	DROP TABLE #GroupRules
	DROP TABLE #UserToGroupJoins

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	RETURN 1

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO