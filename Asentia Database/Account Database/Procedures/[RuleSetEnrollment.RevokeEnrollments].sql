-- =====================================================================
-- PROCEDURE: [RuleSetEnrollment.RevokeEnrollments]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSetEnrollment.RevokeEnrollments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSetEnrollment.RevokeEnrollments]
GO

/*

Revokes course RuleSet Enrollment enrollments where the enrollment is incomplete
and the user no longer matches any RuleSet Enrollments of the Course. 

This procedure only handles COMPLETE REVOCATION of enrollments inherited from a
RuleSet Enrollment. Lateral moves from one RuleSet Enrollment to another are 
handled in the AssignEnrollments procedure. Therefore, this procedure is "hooked
into" (called directly from) the AssignEnrollments procedure in order to guarantee
that the revocations are done AFTER the assignments are done. 

IN ORDER TO COMPLETELY ELIMINATE THE POSSIBILITY OF INHERITED ENROLLMENTS BEING
MISTAKENLY REMOVED, THIS PROCEDURE SHOULD ONLY BE CALLED FROM "AssignEnrollments,"
IT SHOULD NEVER BE CALLED DIRECTLY BY A JOB OR FROM PROCEDURAL CODE.

Notes:

@Courses  - IDTable of Course ids to assign RuleSet Enrollments for.
		    Required to be passed, but not required to be populated.
		    If not populated, this will be executed on ALL Courses
		    that have RuleSet Enrollments.

@Filters  - IDTable of User or Group ids (see @filterBy param) to
		    assign RuleSet Enrollments for.
		    Required to be passed, but not required to be populated.
		    If not populated, this will be executed on ALL Users or
		    Groups (see @filterBy param).

@filterBy - Defines the object(s), User or Group, that we are executing
			this for. If "Group," we are just getting the users that are
			members of the specified Group(s) to execute this on. There
			is no direct RuleSet Enrollment to Group inheritance, just
			rules that can say "if user is a member of ...". Essentially,
			the "Group" filter only gets used when we are calling this
			after adding or removing User(s) from Group(s).

The parameters described above are all passed through to the [System.GetRuleSetCourseMatches]
procedure, which then returns a recordset of user-to-ruleset enrollment matches filtered 
down to the Course(s) and User(s) resulting from applying the filters above.

*/

CREATE PROCEDURE [RuleSetEnrollment.RevokeEnrollments]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,

	@Courses					IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/*

	Execute [System.GetRuleSetCourseMatches] and dump the results into a temp table.

	*/

	CREATE TABLE #RuleSetEnrollmentMatches
	(
		idSite INT, 
		idRuleSetEnrollment INT, 
		[priority] INT, 
		idRuleSet INT, 
		idCourse INT, 
		idUser INT
	)

	INSERT INTO #RuleSetEnrollmentMatches
	EXEC [System.GetRuleSetCourseMatches] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @Courses, @Filters, @filterBy, 1

	/*

	DELETE incomplete Enrollments where the user is no longer matched to the RuleSet Enrollment they
	have inherited the enrollment for.

	*/

	DECLARE @EnrollmentsToDelete IDTable

	INSERT INTO @EnrollmentsToDelete (
		id
	)
	SELECT
		idEnrollment
	FROM tblEnrollment E
	WHERE NOT EXISTS (SELECT 1 FROM #RuleSetEnrollmentMatches RSM
					  WHERE E.idUser = RSM.idUser
					  AND E.idRuleSetEnrollment = RSM.idRuleSetEnrollment
					  AND E.idCourse = RSM.idCourse)
	--AND E.idCourse IN (SELECT DISTINCT idCourse FROM #RuleSetEnrollmentMatches RSM)
	AND (
			((SELECT COUNT(1) FROM @Courses) = 0)
			OR
			(E.idCourse IN (SELECT DISTINCT id FROM @Courses))
		)
	AND (
			((SELECT COUNT(1) FROM @Filters) = 0)
			OR
			(@filterBy = 'group' AND E.idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT id FROM @Filters)))
			OR
			(@filterBy = 'user' AND E.idUser IN (SELECT DISTINCT id FROM @Filters))
		)
	AND E.idRuleSetEnrollment IS NOT NULL
	AND E.dtCompleted IS NULL

	-- do the delete using the Enrollment.Delete procedure
	EXEC [Enrollment.Delete] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, @EnrollmentsToDelete

	/*

	REMOVE the link to the RuleSet Enrollment for completed enrollments where the user is no longer
	matched to the RuleSet Enrollment they have inherited the enrollment for.

	*/

	UPDATE tblEnrollment SET
		idRuleSetEnrollment = NULL
	WHERE NOT EXISTS (SELECT 1 FROM #RuleSetEnrollmentMatches RSM
					  WHERE tblEnrollment.idUser = RSM.idUser
					  AND tblEnrollment.idRuleSetEnrollment = RSM.idRuleSetEnrollment
					  AND tblEnrollment.idCourse = RSM.idCourse)
	AND tblEnrollment.idCourse IN (SELECT DISTINCT idCourse FROM #RuleSetEnrollmentMatches RSM)
	AND (
			((SELECT COUNT(1) FROM @Filters) = 0)
			OR
			(@filterBy = 'group' AND tblEnrollment.idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT id FROM @Filters)))
			OR
			(@filterBy = 'user' AND tblEnrollment.idUser IN (SELECT DISTINCT id FROM @Filters))
		)
	AND tblEnrollment.idRuleSetEnrollment IS NOT NULL
	AND tblEnrollment.dtCompleted IS NOT NULL
				   
	/* DROP THE TEMPORARY TABLES */
	DROP TABLE #RuleSetEnrollmentMatches

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO