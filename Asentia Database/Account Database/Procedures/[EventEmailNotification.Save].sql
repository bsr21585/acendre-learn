-- =====================================================================
-- PROCEDURE: [EventEmailNotification.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[EventEmailNotification.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [EventEmailNotification.Save]
GO

CREATE PROCEDURE [EventEmailNotification.Save]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT				= 0,
	
	@idEventEmailNotification	INT				OUTPUT,
	@idSite						INT,
	@name						NVARCHAR(255),
	@idEventType				INT,
	@idEventTypeRecipient		INT,
	@idObject					INT,
	@from						NVARCHAR(255),
	@copyTo						NVARCHAR(255),
	@specificEmailAddress		NVARCHAR(255),
	@isActive					BIT,
	@interval					INT,
	@timeFrame					NVARCHAR(4),
	@priority					INT,
	@isHTMLBased				BIT,
	@attachmentType				INT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	Validate the recipient selection to the event type
	
	*/
	
	IF (
		SELECT COUNT(1) 
		FROM tblEventTypeToEventTypeRecipientLink
		WHERE idEventType = @idEventType
		AND idEventTypeRecipient = @idEventTypeRecipient
		) = 0 
		BEGIN
		SET @idEventEmailNotification = @idEventEmailNotification
		SET @Return_Code = 4
		SET @Error_Description_Code = 'EENotificationSave_InvalidRecipientForEvent'
		RETURN 1
		END
		
	/*
	
	DISSALLOW when existing record found with:
		- same site
		- same event
		- same object (or no object / NULL)
		- same interval/timeframe
		- same recipient
	
	*/
	
	IF (
		SELECT COUNT(1) 
		FROM tblEventEmailNotification EEN
		WHERE idSite = @idSite
		AND idEventType = @idEventType
		AND interval = @interval
		AND timeframe = @timeframe
		AND idEventTypeRecipient = @idEventTypeRecipient
		AND (
			(idObject IS NULL AND @idObject IS NULL) -- no object (all events)
			OR 
			idObject = @idObject -- or same object 
			)
		AND (
			(@idEventEmailNotification = 0 OR @idEventEmailNotification IS NULL)
			OR 
			idEventEmailNotification <> @idEventEmailNotification
			)
		AND (isDeleted = 0 OR isDeleted IS NULL)
		) <> 0
		BEGIN
		SET @idEventEmailNotification = @idEventEmailNotification
		SET @Return_Code = 7 
		SET @Error_Description_Code = 'EventEmailNotificationSave_ObjectNotUnique'
		RETURN 1
		END
	
	/*

	save the data

	*/

	IF (@idEventEmailNotification = 0 OR @idEventEmailNotification IS NULL)

		BEGIN
		
		-- insert the new email notification
		
		INSERT INTO [tblEventEmailNotification] (
			idSite,
			[name],
			idEventType,
			idEventTypeRecipient,
			isDeleted,
			idObject,
			[from],
			copyTo,
			specificEmailAddress,
			isActive,
			dtActivation,
			interval,
			timeFrame,
			[priority],
			isHTMLBased,
			attachmentType
		)
		VALUES (
			@idSite,
			@name,
			@idEventType,
			@idEventTypeRecipient,
			0,
			@idObject,
			@from,
			@copyTo,
			@specificEmailAddress,
			@isActive,
			CASE WHEN @isActive = 1 THEN GETUTCDATE() ELSE NULL END,
			@interval,
			@timeFrame,
			@priority,
			@isHTMLBased,
			@attachmentType			
		)
		
		-- get the new email notification's id
		
		SET @idEventEmailNotification = SCOPE_IDENTITY()
		
		END
		
	ELSE
	
		BEGIN
		
		-- check that the email notification exists
		IF (SELECT COUNT(1) FROM tblEventEmailNotification WHERE idEventEmailNotification = @idEventEmailNotification AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idEventEmailNotification = @idEventEmailNotification
			SET @Return_Code = 1 --(details not found)
			SET @Error_Description_Code = 'EventEmailNotificationSave_NoRecordFound'
			RETURN 1
			
			END
			
		-- update existing email notification's properties

		UPDATE tblEventEmailNotification SET
			name = @name,
			idEventType = @idEventType,
			idEventTypeRecipient = @idEventTypeRecipient,
			idObject = @idObject,
			[from] = @from,
			copyTo = @copyTo,
			specificEmailAddress = @specificEmailAddress,
			isActive = @isActive,
			dtActivation = CASE WHEN @isActive = 1 THEN GETUTCDATE() ELSE NULL END,
			interval = @interval,
			timeframe = @timeframe,
			[priority] = @priority,
			isHTMLBased = @isHTMLBased,
			attachmentType = @attachmentType
		WHERE idEventEmailNotification = @idEventEmailNotification

		-- get the email notification's id
		
		SET @idEventEmailNotification = @idEventEmailNotification
		
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF (SELECT COUNT(1) FROM tblEventEmailNotificationLanguage EENL WHERE EENL.idEventEmailNotification = @idEventEmailNotification AND EENL.idLanguage = @idDefaultLanguage) > 0 

		BEGIN

		UPDATE tblEventEmailNotificationLanguage SET
			[name] = @name
		WHERE idEventEmailNotification = @idEventEmailNotification
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
		
		BEGIN
	
		INSERT INTO tblEventEmailNotificationLanguage (
			idSite,
			idEventEmailNotification,
			idLanguage,
			[name]
		)
		SELECT
			@idSite,
			@idEventEmailNotification,
			@idDefaultLanguage,
			@name
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblEventEmailNotificationLanguage EENL
			WHERE EENL.idEventEmailNotification = @idEventEmailNotification
			AND EENL.idLanguage = @idDefaultLanguage
		)

		END
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO