-- =====================================================================
-- PROCEDURE: [RuleSet.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSet.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSet.GetGrid]
GO

/*

Gets a listing of rule sets.

*/

CREATE PROCEDURE [RuleSet.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@idRuleSetEnrollment	INT,
	@pageNum				INT,
	@pageSize				INT,
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END
			
		IF @searchParam IS NULL
		
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblRuleSet RS
			INNER JOIN tblRuleSetToRuleSetEnrollmentLink RSL ON RS.idRuleSet = RSL.idRuleSet
			WHERE 
				(
					@idCallerSite IS NULL
					OR 
					@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite
				)
			AND RSL.idRuleSetEnrollment = @idRuleSetEnrollment
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						RS.idRuleSet,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN RS.label END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RS.label END
						)
						AS [row_number]
					FROM tblRuleSet RS
					INNER JOIN tblRuleSetToRuleSetEnrollmentLink RSL ON RS.idRuleSet = RSL.idRuleSet
					WHERE 
						(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite
						)
					AND RSL.idRuleSetEnrollment = @idRuleSetEnrollment
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idRuleSet, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				R.idRuleSet,
				R.isAny,
				R.label,
				Convert(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblRuleSet R ON R.idRuleSet = SelectedKeys.idRuleSet
			ORDER BY SelectedKeys.[row_number]
			
			END
		
		ELSE
		
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblRuleSet RS
			INNER JOIN tblRuleSetToRuleSetEnrollmentLink RSL ON RS.idRuleSet = RSL.idRuleSet
			INNER JOIN CONTAINSTABLE(tblRuleSet, *, @searchParam) K ON K.[key] = RS.idRuleSet
			WHERE 
				(
					@idCallerSite IS NULL
					OR 
					@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite
				)
			AND RSL.idRuleSetEnrollment = @idRuleSetEnrollment
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						RS.idRuleSet,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN RS.label END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN RS.label END
						)
						AS [row_number]
					FROM tblRuleSet RS
					INNER JOIN tblRuleSetToRuleSetEnrollmentLink RSL ON RS.idRuleSet = RSL.idRuleSet
					INNER JOIN CONTAINSTABLE(tblRuleSet, *, @searchParam) K ON K.[key] = RS.idRuleSet
					WHERE
						(
							@idCallerSite IS NULL
							OR 
							@idCallerSite IS NOT NULL AND RS.idSite = @idCallerSite
						)
					AND RSL.idRuleSetEnrollment = @idRuleSetEnrollment
				),
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idRuleSet, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				R.idRuleSet,
				R.isAny,
				R.label,
				Convert(bit, 1) AS isModifyOn, -- needs to be calculated
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblRuleSet R ON R.idRuleSet = SelectedKeys.idRuleSet
			ORDER BY SelectedKeys.[row_number]
			
			END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO