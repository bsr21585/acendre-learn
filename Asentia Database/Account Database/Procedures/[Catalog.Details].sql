-- =====================================================================
-- PROCEDURE: [Catalog.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Catalog.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Catalog.Details]
GO

/*
Return all the properties for a given catalog id.
*/
CREATE PROCEDURE [Catalog.Details]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,
	
	@idCatalog					INT,
	@idSite						INT				OUTPUT, 
	@title						NVARCHAR(255)	OUTPUT,
	@idParent					INT				OUTPUT,
	@order						INT				OUTPUT,
	@shortDescription			NVARCHAR(512)	OUTPUT,
	@longDescription			NVARCHAR(MAX)	OUTPUT,
	@isPrivate					BIT				OUTPUT,
	@cost						FLOAT			OUTPUT,
	@calculatedCost				FLOAT			OUTPUT,
	@isClosed					BIT				OUTPUT,
	@dtCreated					DATETIME		OUTPUT,
	@dtModified					DATETIME		OUTPUT,
	@costType					INT				OUTPUT,
	@searchTags					NVARCHAR(512)	OUTPUT,
	@numberEnrollableCourses	INT				OUTPUT,
	@avatar						NVARCHAR(255)	OUTPUT,
	@shortcode					NVARCHAR(10)	OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCatalog
		WHERE idCatalog = @idCatalog
		AND idSite = @idCallerSite
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CatalogDetails_NoRecordFound'
		RETURN 1
		END
	
	/*
	
	validate that the specified language exists, use site default if not
	
	*/
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF @idCallerLanguage IS NULL
		SELECT @idCallerLanguage = idLanguage FROM tblSite WHERE idSite = @idCallerSite
		
	/*
	
	get the data 
	
	*/

	DECLARE @totalAllCourse DECIMAL(18,2)

	SELECT 
		@totalAllCourse = ISNULL(SUM(C.cost),0) 
	FROM tblCourseToCatalogLink CCL
	INNER JOIN tblCourse C ON C.idCourse = CCL.idCourse
	WHERE CCL.idCatalog = @idCatalog
	AND CCL.idSite = @idCallerSite
	AND (C.isDeleted IS NULL OR C.isDeleted = 0)
	AND C.isPublished = 1
		
	SELECT		
		@idCatalog			= C.idCatalog,
		@idSite				= C.idSite,
		@title				= CASE WHEN CL.title IS NULL OR CL.title = '' THEN C.title ELSE CL.title END,
		@idParent			= C.idParent,
		@order				= C.[order],
		@shortDescription	= CASE WHEN CL.shortDescription IS NULL THEN C.shortDescription ELSE CL.shortDescription END,
		@longDescription	= CASE WHEN CL.longDescription IS NULL THEN C.longDescription ELSE CL.longDescription END,
		@isPrivate			= C.isPrivate,
		@cost				= CASE ISNULL(C.costType, 1) 
								WHEN 1 THEN 0
								WHEN 2 THEN ISNULL(C.cost, 0)
								WHEN 3 THEN @totalAllCourse
								WHEN 4 THEN ISNULL(C.cost, 0) END,
		@calculatedCost		= CASE WHEN C.costType = 1 THEN 0 -- FREE
								WHEN C.costType = 2 AND C.cost IS NOT NULL THEN CAST(C.cost AS DECIMAL(18,2)) -- FIXED COST
								WHEN C.costType = 3 THEN CAST((SELECT CONVERT(FLOAT, SUM(cost)) FROM tblCourse CO WHERE (CO.isDeleted IS NULL OR CO.isDeleted = 0) AND CO.isPublished = 1 AND (CO.isClosed IS NULL OR CO.isClosed = 0) AND CO.idCourse IN (SELECT idCourse FROM tblCourseToCatalogLink WHERE idCatalog = C.idCatalog)) AS DECIMAL(18,2)) -- SUM OF ALL COURSES
								WHEN C.costType = 4 THEN CAST(((SELECT CONVERT(FLOAT, SUM(cost)) FROM tblCourse CO WHERE (CO.isDeleted IS NULL OR CO.isDeleted = 0) AND CO.isPublished = 1 AND (CO.isClosed IS NULL OR CO.isClosed = 0) AND CO.idCourse IN (SELECT idCourse FROM tblCourseToCatalogLink WHERE idCatalog = C.idCatalog))) * (C.cost / 100) AS DECIMAL(18,2)) -- PCT OFF SUM OF ALL COURSES
							  ELSE 0 END,
		@isClosed			= C.isClosed,
		@dtCreated			= C.dtCreated,
		@dtModified			= C.dtModified,
		@costType			= CASE WHEN C.costType IS NULL THEN 1 ELSE C.costType END,
		@searchTags			= C.searchTags,
		@avatar				= C.avatar,
		@shortcode			= C.shortcode
	FROM tblCatalog C
	LEFT JOIN tblCatalogLanguage CL ON CL.idCatalog = C.idCatalog AND CL.idLanguage = @idCallerLanguage
	WHERE C.idCatalog = @idCatalog

	-- get the number of enrollable courses in the catalog
	SELECT 
		@numberEnrollableCourses = COUNT(1)
	FROM tblCourse C
	WHERE C.idSite = @idCallerSite
	AND (C.isDeleted IS NULL OR C.isDeleted = 0)
	AND (C.isClosed IS NULL OR C.isClosed = 0)
	AND C.isPublished = 1
	AND C.idCourse IN (SELECT idCourse FROM tblCourseToCatalogLink WHERE idCatalog = @idCatalog)
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CatalogDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO