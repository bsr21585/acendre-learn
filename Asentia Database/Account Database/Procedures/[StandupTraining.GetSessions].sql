-- =====================================================================
-- PROCEDURE: [StandupTraining.GetSessions]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTraining.GetSessions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTraining.GetSessions]
GO

/*

Returns a recordset of session ids and titles that belong to a standup training.

*/

CREATE PROCEDURE [StandupTraining.GetSessions]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@idStandupTraining		INT,
	@searchParam			NVARCHAR(4000)
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	/*

	get the language id from the caller language string

	*/

	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = L.idLanguage FROM tblLanguage L WHERE L.code = @callerLangString

		/*

		for finding the record which are neither befor 3 months nor after 3 moths 

		*/
		 DECLARE @temp TABLE(
                	idStandUpTrainingInstance INT,
					dtStaart DATETIME
							)
							INSERT INTO @temp SELECT idStandUpTrainingInstance,MIN(dtStart)
							FROM tblStandUpTrainingInstanceMeetingTime GROUP BY idStandUpTrainingInstance
		DECLARE @dateBefore3Months DATETIME
		DECLARE @dateAfter3Months DATETIME
		SELECT @dateBefore3Months = DATEADD(month, -3, GETUTCDATE())
		SELECT @dateAfter3Months = DATEADD(month, +3, GETUTCDATE())

	IF @searchParam = '' OR @searchParam = '*'
		BEGIN
		SET @searchParam = null
		END
		
	IF @searchParam IS NULL
			
		BEGIN

		 SELECT
			DISTINCT
			S.idStandUpTrainingInstance AS idSession,
			T.dtStaart,
			(S.locationDescription+' ' + S.city) AS Location,
			CASE WHEN SL.title IS NOT NULL THEN SL.title ELSE S.title END AS title,
			CASE WHEN (S.seats - (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink where idStandUpTrainingInstance = S.idStandUpTrainingInstance)) > 0 THEN 1 ELSE 0 END AS isSeatAvailabe,
			CASE WHEN T.dtStaart BETWEEN @dateBefore3Months AND GETUTCDATE() then 0 ELSE 1 END AS CssClass ,                        --0 for red text and 1 for green text
			CONVERT(NVARCHAR(50),(S.seats -  (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink where idStandUpTrainingInstance = S.idStandUpTrainingInstance)))+'/'+ 
			CONVERT(NVARCHAR(50),S.seats) AS [Status]
		FROM tblStandUpTrainingInstance S
		LEFT JOIN tblStandUpTrainingInstanceLanguage SL ON SL.idStandUpTrainingInstance = S.idStandUpTrainingInstance AND SL.idLanguage = @idCallerLanguage
		LEFT JOIN @temp T ON T.idStandUpTrainingInstance = S.idStandUpTrainingInstance 
		WHERE S.idSite = @idCallerSite
		AND (S.isDeleted IS NULL OR S.isDeleted = 0)
		AND S.idStandUpTraining = @idStandupTraining
		AND t.dtStaart BETWEEN @dateBefore3Months AND @dateAfter3Months		
		ORDER BY T.dtStaart ASC

		END

	ELSE

		BEGIN

		SELECT
			DISTINCT
			S.idStandUpTrainingInstance  AS idSession,
			T.dtStaart,
			(S.locationDescription+' ' + S.city) AS Location,
			CASE WHEN SL.title IS NOT NULL THEN SL.title ELSE S.title END AS title,
			CASE WHEN (S.seats - (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink where idStandUpTrainingInstance = S.idStandUpTrainingInstance)) > 0 THEN 1 ELSE 0 END AS isSeatAvailabe,
			CASE WHEN T.dtStaart BETWEEN @dateBefore3Months AND GETUTCDATE() then 0 ELSE 1 END AS CssClass,   --0 for red text and 1 for green text
			CONVERT(NVARCHAR(50),(S.seats -  (SELECT COUNT(1) FROM tblStandupTrainingInstanceToUserLink where idStandUpTrainingInstance = S.idStandUpTrainingInstance)))+'/'+ 
			CONVERT(NVARCHAR(50),S.seats) AS [Status]
		FROM tblStandUpTrainingInstanceLanguage SL
		INNER JOIN CONTAINSTABLE(tblStandUpTrainingInstanceLanguage, *, @searchParam) K ON K.[key] = SL.idLanguage AND SL.idLanguage = @idCallerLanguage
		LEFT JOIN tblStandUpTrainingInstance S ON S.idStandUpTrainingInstance =  SL.idStandUpTrainingInstance
		LEFT JOIN @temp T ON T.idStandUpTrainingInstance = S.idStandUpTrainingInstance 
		WHERE S.idSite = @idCallerSite
		AND (S.isDeleted IS NULL OR S.isDeleted = 0)
		AND S.idStandUpTraining = @idStandupTraining
		AND t.dtStaart BETWEEN @dateBefore3Months AND @dateAfter3Months
		ORDER BY T.dtStaart ASC
		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO