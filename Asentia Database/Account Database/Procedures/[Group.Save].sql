-- =====================================================================
-- PROCEDURE: [Group.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Group.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Group.Save]
GO

/*

Adds new group or updates existing group.
Note: This saves "language specific" properties in the site's default language only.

*/
CREATE PROCEDURE [Group.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idGroup				INT				OUTPUT,
	@name					NVARCHAR(255),	-- must be unique
	@primaryGroupToken		NVARCHAR(255),		
	@objectGUID				UNIQUEIDENTIFIER,	
	@distinguishedName		NVARCHAR(255),
	@avatar					NVARCHAR(255),
	@shortDescription		NVARCHAR(512),
	@longDescription		NVARCHAR(MAX),
	@isSelfJoinAllowed		BIT,
	@isFeedActive           BIT,
	@isFeedModerated        BIT,	
	@membershipIsPublicized	BIT,
	@searchTags				NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED
	
	
	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
			SELECT @Return_Code = 5
			SET @Error_Description_Code = 'GroupSave_SpecifiedLanguageNotDefault'
			RETURN 1 
		END
	
	*/
			 
	/*
	
	validate uniqueness
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblGroup
		WHERE idSite = @idCallerSite
		AND name = @name
		AND (
			@idGroup IS NULL
			OR @idGroup <> idGroup
			)
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'GroupSave_FieldNotUnique'
		RETURN 1 
		END

	/*
	
	validate uniqueness within language
	
	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite
	
	IF( 
		SELECT COUNT(1)
		FROM tblGroup G
		LEFT JOIN tblGroupLanguage GL ON GL.idGroup = G.idGroup AND GL.idLanguage = @idDefaultLanguage -- same language
		WHERE G.idSite = @idCallerSite -- same site
		AND G.idGroup <> @idGroup -- not the same group
		AND GL.name = @name -- validate parameter: name
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'GroupSave_FieldNotUnique'
		RETURN 1
		END
		
	/*
	
	save the data
	
	*/
	
	IF (@idGroup = 0 OR @idGroup IS NULL)
		
		BEGIN
		
		-- insert the new group
		
		INSERT INTO tblGroup (
			idSite, 
			name,
			primaryGroupToken,
			objectGUID,
			distinguishedName,
			avatar ,
			shortDescription,
			longDescription,
			isSelfJoinAllowed,
			isFeedActive,
			isFeedModerated,
			membershipIsPublicized,
			searchTags
		)			
		VALUES (
			@idCallerSite, 
			@name,
			@primaryGroupToken,
			@objectGUID,
			@distinguishedName,
			@avatar ,
			@shortDescription,
			@longDescription,
			@isSelfJoinAllowed,
			@isFeedActive,
			@isFeedModerated,
			@membershipIsPublicized,
			@searchTags
		)
		
		-- get the new group's id and return successfully
		
		SELECT @idGroup = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the group id exists
		IF (SELECT COUNT(1) FROM tblGroup WHERE idGroup = @idGroup AND idSite = @idCallerSite) < 1
			BEGIN                               
			
			SET @idGroup = @idGroup
			SET @Return_Code = 1 --(1 is 'details not found')
			SET @Error_Description_Code = 'GroupSave_NoRecordFound'
			RETURN 1
			
			END
			
		-- update existing group's properties
		
		UPDATE tblGroup SET
			name = @name,
			primaryGroupToken = @primaryGroupToken,
			objectGUID = @objectGUID,
			distinguishedName = @distinguishedName,
			avatar = @avatar,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			isSelfJoinAllowed = @isSelfJoinAllowed ,			
			isFeedActive = @isFeedActive,
			isFeedModerated = @isFeedModerated,
			membershipIsPublicized = @membershipIsPublicized,
			searchTags = @searchTags
		WHERE idGroup = @idGroup 

			 /*
	
		Delete all Moderators if isModerated is 0 
		Delete all Moderators and Messages if isActive is 0 
	
		*/       
	    
	        IF (@isFeedActive=1 and  @isFeedModerated=0 )
	          BEGIN
                 --Delete all moderator of that groupfeed
	              DELETE from tblGroupFeedModerator 
	              WHERE idGroup	=	@idGroup
	              AND   idSite		=	@idCallerSite
	             
	           END 
	         	 ELSE
	              IF (@isFeedActive  =   0 and  @isFeedModerated   =  0 )
	  	    	BEGIN
	              --Delete all moderator of that groupfeed
	               --first delete the comments of feed
					DELETE from tblGroupFeedMessage
					WHERE idGroup   	=	@idGroup  
					AND idSite			=	@idCallerSite
					AND ISNULl(idParentGroupFeedMessage,0)=1

					--then delete the parent feed
					 DELETE from tblGroupFeedMessage 
					WHERE idGroup   	=	@idGroup  
					AND idSite			=	@idCallerSite
	              END

		
		-- get the group's id 
		SELECT @idGroup = @idGroup

		END

	/*
	
	update/insert the default language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblGroupLanguage GL WHERE GL.idGroup = @idGroup AND GL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblGroupLanguage SET
			name = @name,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			searchTags = @searchTags
		WHERE idGroup = @idGroup
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblGroupLanguage (
			idSite,
			idGroup,
			idLanguage,
			name,
			shortDescription,
			longDescription,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idGroup,
			@idDefaultLanguage,
			@name,
			@shortDescription,
			@longDescription,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblGroupLanguage GL
			WHERE GL.idGroup = @idGroup
			AND GL.idLanguage = @idDefaultLanguage
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
