-- =====================================================================
-- PROCEDURE: [Course.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.Save]
GO

/*

Adds new course or updates existing course.
Note: This saves "language specific" properties in the site's default language only.

*/

CREATE PROCEDURE [Course.Save]
(
	@Return_Code									INT				OUTPUT,
	@Error_Description_Code							NVARCHAR(50)	OUTPUT,
	@idCallerSite									INT				= 0, -- default if not specified
	@callerLangString								NVARCHAR(10),
	@idCaller										INT				= 0, -- will fail if not specified
	
	@idCourse										INT				OUTPUT,
	@title											NVARCHAR(255),
	@coursecode										NVARCHAR(255),	-- must be unique if specified
	@avatar											NVARCHAR(255),
	@cost											FLOAT,	
	@credits										FLOAT,
	@minutes										INT,
	@shortDescription								NVARCHAR(512), 
	@longDescription								NVARCHAR(MAX),
	@objectives										NVARCHAR(MAX),
	@socialMedia									NVARCHAR(MAX), 
	@isPublished									BIT,
	@isClosed										BIT,
	@isLocked										BIT,
	@isPrerequisiteAny								BIT,
	@isFeedActive									BIT,
	@isFeedModerated								BIT,
	@isFeedOpenSubscription							BIT,
	@disallowRating									BIT,
	@forceLessonCompletionInOrder					BIT,
	@requireFirstLessonToBeCompletedBeforeOthers	BIT,
	@lockLastLessonUntilOthersCompleted				BIT,
	@selfEnrollmentIsOneTimeOnly					BIT,
	@selfEnrollmentDueInterval						INT,
	@selfEnrollmentDueTimeframe						NVARCHAR(4),
	@selfEnrollmentExpiresFromStartInterval			INT,
	@selfEnrollmentExpiresFromStartTimeframe		NVARCHAR(4),
	@selfEnrollmentExpiresFromFirstLaunchInterval	INT,
	@selfEnrollmentExpiresFromFirstLaunchTimeframe	NVARCHAR(4),
	@searchTags										NVARCHAR(512),
	@isSelfEnrollmentApprovalRequired				BIT,
	@isApprovalAllowedByCourseExperts				BIT,
	@isApprovalAllowedByUserSupervisor				BIT,
	@shortcode										NVARCHAR(10)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language is the default language for the site
	MAY NOT BE NEEDED


	
	DECLARE @idCallerLanguage INT
	SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString
	
	IF (
		SELECT COUNT(1) 
		FROM tblSite S
		WHERE idSite = @idCallerSite
		AND idLanguage = @idCallerLanguage
		AND @idCallerLanguage IS NOT NULL
		) <> 1
		
		BEGIN 
		SET @Return_Code = 5
		SET @Error_Description_Code = 'CourseSave_SpecifiedLanguageNotDefault'
		RETURN 1 
		END

	*/
			 
	/*
	
	validate uniqueness for course code, if specified
	
	*/

	IF (
		SELECT COUNT(1)
		FROM tblCourse
		WHERE idSite = @idCallerSite
		AND coursecode = @coursecode
		AND @coursecode IS NOT NULL
		AND @coursecode <> ''
		AND (isDeleted = 0 OR isDeleted IS NULL)
		AND (
			@idCourse IS NULL
			OR @idCourse <> idCourse
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'CourseSave_FieldNotUnique'
		RETURN 1 
		END

	/*
	
	validate uniqueness for shortcode, if specified

	*/	

	IF (
		SELECT COUNT(1)
		FROM tblCourse
		WHERE idSite = @idCallerSite
		AND shortcode = @shortcode
		AND @shortcode IS NOT NULL
		AND @shortcode <> ''
		AND (isDeleted = 0 OR isDeleted IS NULL)
		AND (
			@idCourse IS NULL
			OR @idCourse <> idCourse
			)
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'CourseSave_ShortcodeNotUnique'
		RETURN 1 
		END

	/*

	check XSS vulnerabilities

	*/

	-- longDescription

	IF (@longDescription LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSave_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSave_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSave_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@longDescription LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSave_DescTagNotAllowed_Iframe'
	RETURN 1 
	END

	-- objectives

	IF (@objectives LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSave_ObjTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@objectives LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSave_ObjTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@objectives LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSave_ObjTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@objectives LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CourseSave_ObjTagNotAllowed_Iframe'
	RETURN 1 
	END

	/*

	if the course is locked, it must also be closed

	*/

	IF (@isLocked = 1)
	BEGIN
	SET @isClosed = 1
	END

	/*

	generate random revision code
	revision code is changed each time a course is saved

	*/

	DECLARE @revcode NVARCHAR(32)
	DECLARE @randomIsUnique BIT
	DECLARE @i INT

	SET @i = 0
	SET @randomIsUnique = 1

	WHILE @randomIsUnique > 0 AND @i < 100

		BEGIN

		EXEC [System.GenerateRandomString] 32, @revcode OUTPUT

		SET @randomIsUnique = (SELECT COUNT(1) FROM tblCourse WHERE revcode = @revcode AND idSite = @idCallerSite)
		SET @i = @i + 1

		END

	IF @i >= 100

		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'CourseSave_RevCodeNotUnique'
		RETURN 1
		END	
		
	/*
	
	save the data
	
	*/
	
	IF (@idCourse = 0 OR @idCourse IS NULL)
		
		BEGIN

		-- get the "order" number for the new course, "order" for the course table is the ordering in the root catalog

		DECLARE @order INT

		SELECT 
			@order = CASE WHEN MAX([order]) IS NULL THEN 1 ELSE MAX([order]) + 1 END
		FROM tblCourse
		WHERE idSite = @idCallerSite
		
		-- insert the new course
		
		INSERT INTO tblCourse (
			idSite, 
			title,
			coursecode,
			revcode,
			avatar,
			cost,	
			credits,
			[minutes],
			shortDescription, 
			longDescription,
			objectives,
			socialMedia, 
			isPublished,
			isClosed,
			isLocked,
			isPrerequisiteAny,
			isFeedActive,
			isFeedModerated,
			isFeedOpenSubscription,
			dtCreated,
			dtModified,
			isDeleted,
			dtDeleted,
			[order],
			disallowRating,
			forceLessonCompletionInOrder,
			requireFirstLessonToBeCompletedBeforeOthers,
			lockLastLessonUntilOthersCompleted,
			selfEnrollmentIsOneTimeOnly,
			selfEnrollmentDueInterval,
			selfEnrollmentDueTimeframe,
			selfEnrollmentExpiresFromStartInterval,
			selfEnrollmentExpiresFromStartTimeframe,
			selfEnrollmentExpiresFromFirstLaunchInterval,
			selfEnrollmentExpiresFromFirstLaunchTimeframe,
			searchTags,
			isSelfEnrollmentApprovalRequired,
			isApprovalAllowedByCourseExperts,
			isApprovalAllowedByUserSupervisor,
			shortcode
		)			
		VALUES (
			@idCallerSite, 
			@title,
			@coursecode,
			@revcode,
			@avatar,
			@cost,	
			@credits,
			@minutes,
			@shortDescription, 
			@longDescription,
			@objectives,
			@socialMedia, 
			@isPublished,
			@isClosed,
			@isLocked,
			@isPrerequisiteAny,
			@isFeedActive,
			@isFeedModerated,
			@isFeedOpenSubscription,
			GETUTCDATE(),
			GETUTCDATE(),
			0,
			NULL,
			NULL,
			@disallowRating,
			@forceLessonCompletionInOrder,
			@requireFirstLessonToBeCompletedBeforeOthers,
			@lockLastLessonUntilOthersCompleted,
			@selfEnrollmentIsOneTimeOnly,
			@selfEnrollmentDueInterval,
			@selfEnrollmentDueTimeframe,
			@selfEnrollmentExpiresFromStartInterval,
			@selfEnrollmentExpiresFromStartTimeframe,
			@selfEnrollmentExpiresFromFirstLaunchInterval,
			@selfEnrollmentExpiresFromFirstLaunchTimeframe,
			@searchTags,
			@isSelfEnrollmentApprovalRequired,
			@isApprovalAllowedByCourseExperts,
			@isApprovalAllowedByUserSupervisor,
			@shortcode
		)
		
		-- get the new course's id
		
		SET @idCourse = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the course id exists and it's not deleted
		IF (SELECT COUNT(1) FROM tblCourse WHERE idCourse = @idCourse AND idSite = @idCallerSite AND (isDeleted IS NULL OR isDeleted = 0)) < 1
			BEGIN
			
			SET @idCourse = @idCourse
			SET @Return_Code = 1
			SET @Error_Description_Code = 'CourseSave_NoRecordFound'
			RETURN 1

			END
			
		-- update existing course's properties
		
		UPDATE tblCourse SET
			title = @title,
			coursecode = @coursecode,
			revcode = @revcode,
			avatar = @avatar,
			cost = @cost,
			credits = @credits,
			[minutes] = @minutes,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			objectives = @objectives,
			socialMedia = @socialMedia, 
			isPublished = @isPublished,
			isClosed = @isClosed,
			isLocked = @isLocked,
			isPrerequisiteAny = @isPrerequisiteAny,
			isFeedActive = @isFeedActive,
			isFeedModerated = @isFeedModerated,
			isFeedOpenSubscription = @isFeedOpenSubscription,
			dtModified = GETUTCDATE(),
			disallowRating = @disallowRating,
			forceLessonCompletionInOrder = @forceLessonCompletionInOrder,
			requireFirstLessonToBeCompletedBeforeOthers = @requireFirstLessonToBeCompletedBeforeOthers,
			lockLastLessonUntilOthersCompleted = @lockLastLessonUntilOthersCompleted,
			selfEnrollmentIsOneTimeOnly = @selfEnrollmentIsOneTimeOnly,
			selfEnrollmentDueInterval = @selfEnrollmentDueInterval,
			selfEnrollmentDueTimeframe = @selfEnrollmentDueTimeframe,
			selfEnrollmentExpiresFromStartInterval = @selfEnrollmentExpiresFromStartInterval,
			selfEnrollmentExpiresFromStartTimeframe = @selfEnrollmentExpiresFromStartTimeframe,
			selfEnrollmentExpiresFromFirstLaunchInterval = @selfEnrollmentExpiresFromFirstLaunchInterval,
			selfEnrollmentExpiresFromFirstLaunchTimeframe = @selfEnrollmentExpiresFromFirstLaunchTimeframe,
			searchTags = @searchTags,
			isSelfEnrollmentApprovalRequired = @isSelfEnrollmentApprovalRequired,
			isApprovalAllowedByCourseExperts = @isApprovalAllowedByCourseExperts,
			isApprovalAllowedByUserSupervisor = @isApprovalAllowedByUserSupervisor,
			shortcode = @shortcode
		WHERE idCourse = @idCourse

		/*	
		
		delete all moderators if isModerated = 0 
		delete all moderators and messages if isFeedActive = 0 
	
		*/    

		IF (@isFeedModerated = 0)
			BEGIN
	        DELETE FROM tblCourseFeedModerator WHERE idCourse = @idCourse AND idSite = @idCallerSite	       
			END 	  

	    IF (@isFeedActive = 0)	       
			BEGIN
			DELETE FROM tblCourseFeedModerator WHERE idCourse = @idCourse AND idSite = @idCallerSite	       	       
	        DELETE FROM tblCourseFeedMessage WHERE idCourse = @idCourse AND idSite = @idCallerSite AND ISNULL(idParentCourseFeedMessage, 0) = 1
			DELETE FROM tblCourseFeedMessage WHERE idCourse = @idCourse AND idSite = @idCallerSite
			END

		-- get the course's id 

		SET @idCourse = @idCourse
				
		END
		
	/*
	
	update/insert the default language in the language table

	*/

	-- get the site's default language id
	DECLARE @idDefaultLanguage INT
	SELECT @idDefaultLanguage = S.idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite

	IF (SELECT COUNT(1) FROM tblCourseLanguage CL WHERE CL.idCourse = @idCourse AND CL.idLanguage = @idDefaultLanguage) > 0
	
		BEGIN

		UPDATE tblCourseLanguage SET
			title = @title,
			shortDescription = @shortDescription,
			longDescription = @longDescription,
			objectives = @objectives,
			searchTags = @searchTags
		WHERE idCourse = @idCourse
		AND idLanguage = @idDefaultLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCourseLanguage (
			idSite,
			idCourse,
			idLanguage,
			title,
			shortDescription,
			longDescription,
			objectives,
			searchTags
		)
		SELECT
			@idCallerSite,
			@idCourse,
			@idDefaultLanguage,
			@title,
			@shortDescription,
			@longDescription,
			@objectives,
			@searchTags
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCourseLanguage CL
			WHERE CL.idCourse = @idCourse
			AND CL.idLanguage = @idDefaultLanguage
		)

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO