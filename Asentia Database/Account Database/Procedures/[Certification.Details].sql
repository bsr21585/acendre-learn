-- =====================================================================
-- PROCEDURE: [Certification.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certification.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certification.Details]
GO

/*

Return all the properties for a given certification id.

*/

CREATE PROCEDURE [Certification.Details]
(
	@Return_Code									INT				OUTPUT,
	@Error_Description_Code							NVARCHAR(50)	OUTPUT,
	@idCallerSite									INT				= 0, --default if not specified
	@callerLangString								NVARCHAR(10),
	@idCaller										INT				= 0,
	
	@idCertification								INT				OUTPUT,
	@idSite											INT				OUTPUT, 	
	@title											NVARCHAR(255)	OUTPUT,
	@shortDescription								NVARCHAR(512)	OUTPUT, 
	@searchTags										NVARCHAR(512)	OUTPUT, 
	@dtCreated										DATETIME		OUTPUT,
	@dtModified										DATETIME		OUTPUT,
	@isDeleted										BIT				OUTPUT,
	@dtDeleted										DATETIME		OUTPUT,
	@initialAwardExpiresInterval					INT				OUTPUT,
	@initialAwardExpiresTimeframe					NVARCHAR(4)		OUTPUT,
	@renewalExpiresInterval							INT				OUTPUT,
	@renewalExpiresTimeframe						NVARCHAR(4)		OUTPUT,
	@accreditingOrganization						NVARCHAR(255)	OUTPUT,	
	@certificationContactName						NVARCHAR(255)	OUTPUT,
	@certificationContactEmail						NVARCHAR(255)	OUTPUT,
	@certificationContactPhoneNumber				NVARCHAR(20)	OUTPUT,
	@isPublished									BIT				OUTPUT,
	@isClosed										BIT				OUTPUT,
	@isAnyModule									BIT				OUTPUT,
	@isRenewalAnyModule								BIT				OUTPUT,
	@isCallingUserEnrolled							BIT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	/*
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END
	*/

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertification
		WHERE idCertification = @idCertification
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) = 0 
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationDetails_NoRecordFound'
		RETURN 1
	    END

	/*
	
	get the data 
	
	*/
		
	SELECT
		@idCertification					= C.idCertification,
		@idSite								= C.idSite,		
		@title								= C.title,
		@shortDescription					= C.shortDescription,
		@searchTags							= C.searchTags,
		@dtCreated							= C.dtCreated,
		@dtModified							= C.dtModified,
		@isDeleted							= C.isDeleted,
		@dtDeleted							= C.dtDeleted,
		@initialAwardExpiresInterval		= C.initialAwardExpiresInterval,
		@initialAwardExpiresTimeframe		= C.initialAwardExpiresTimeframe,
		@renewalExpiresInterval				= C.renewalExpiresInterval,
		@renewalExpiresTimeframe			= C.renewalExpiresTimeframe,
		@accreditingOrganization			= C.accreditingOrganization,		
		@certificationContactName			= C.certificationContactName,
		@certificationContactEmail			= C.certificationContactEmail,
		@certificationContactPhoneNumber	= C.certificationContactPhoneNumber,
		@isPublished						= C.isPublished,
		@isClosed							= C.isClosed,
		@isAnyModule						= CASE WHEN C.isAnyModule IS NOT NULL THEN C.isAnyModule ELSE 0 END,
		@isRenewalAnyModule					= CASE WHEN C.isRenewalAnyModule IS NOT NULL THEN C.isRenewalAnyModule ELSE 0 END
	FROM tblCertification C
	WHERE C.idCertification = @idCertification

	-- determine if the calling user is currently enrolled in the certification
	IF (
		SELECT COUNT(1) 
		FROM tblCertificationToUserLink CUL
		WHERE CUL.idCertification = @idCertification
		AND CUL.idUser = @idCaller) > 0
		BEGIN
		SET @isCallingUserEnrolled = 1
		END
	ELSE
		BEGIN
		SET @isCallingUserEnrolled = 0
		END
	
	IF @@ROWCOUNT = 0
		BEGIN
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificationDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO