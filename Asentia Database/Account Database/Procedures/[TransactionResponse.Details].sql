-- =====================================================================
-- PROCEDURE: [TransactionResponse.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[TransactionResponse.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [TransactionResponse.Details]
GO

/*

Return all the properties for a given Resource id.

*/
CREATE PROCEDURE [TransactionResponse.Details]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@id				        INT			   OUTPUT,
	@invoiceNumber			NVARCHAR(25)   OUTPUT,
	@cardType               NVARCHAR(35)   OUTPUT,
	@cardNumber		    	NVARCHAR(25)   OUTPUT,
	@idTransaction			NVARCHAR(30)   OUTPUT,
	@amount			        FLOAT          OUTPUT,
	@responseCode           NVARCHAR (255) OUTPUT,
	@responseMessage        NVARCHAR (MAX) OUTPUT,
	@dtTransaction		    DATETIME       OUTPUT,
	@isApproved             BIT            OUTPUT,
	@isValidated            BIT            OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	Validate user
	
	*/
	 IF (@idCaller <> 1)
	    BEGIN
	    declare @count INT
	    SELECT  @count=  COUNT(1) FROM tblTransactionResponse
	        WHERE id		=	@id
	        AND   idUser	=	@idCaller
	     if(@count = 0)
	        BEGIN
			SET @Return_Code = 1 -- sort of (caller not member of specified site).
			SET @Error_Description_Code = 'TransactionResponseDetails_NoRecordFound'
			RETURN 1
			END
	    END
			
	/*
	get the data 
	
	*/
		
	SELECT 
	     @invoiceNumber  =  invoiceNumber,
	     @cardType       =  cardType,
	     @cardNumber     =  cardNumber,
	     @idTransaction  =  idTransaction,
	     @amount         =  amount,
	     @responseCode   =  responseCode,
	     @responseMessage=  responseMessage,
	     @isApproved     =  isApproved,
	     @isValidated    =  isValidated,
	     @dtTransaction  =  dtTransaction
	 FROM tblTransactionResponse
	 where id = @id 
	 AND idSite = @idCallerSite
	
	IF @@ROWCOUNT = 0
		BEGIN
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'TransactionResponseDetails_NoRecordFound'
		END
	ELSE
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO