-- =====================================================================
-- PROCEDURE: [PowerPointProcessor.UpdateProcessedQueueItem]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[PowerPointProcessor.UpdateProcessedQueueItem]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [PowerPointProcessor.UpdateProcessedQueueItem]
GO

/*

Updates a processed queue item to fill in information and unset the processing flag
once processing of item is finished.

*/

CREATE PROCEDURE [PowerPointProcessor.UpdateProcessedQueueItem]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,

	@idContentPackage		INT,
	@kb						INT,
	@manifest				NVARCHAR(MAX)
)
AS

	BEGIN
	SET NOCOUNT ON

	/*

	update the content package record with the kb and manifest values,
	unset the processing flag, and mark as processed

	*/

	UPDATE tblContentPackage SET
		kb = @kb,
		dtModified = GETUTCDATE(),
		manifest = @manifest,
		isProcessing = 0,
		isProcessed = 1,
		dtProcessed = GETUTCDATE()
	WHERE idContentPackage = @idContentPackage

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO