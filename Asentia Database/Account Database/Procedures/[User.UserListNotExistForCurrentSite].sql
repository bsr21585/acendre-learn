-- =====================================================================
-- PROCEDURE: [User.UserListNotExistForCurrentSite]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[User.UserListNotExistForCurrentSite]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [User.UserListNotExistForCurrentSite]
GO

/*

Returns a recordset of user ids, usernames, and display names 

*/

CREATE PROCEDURE [User.UserListNotExistForCurrentSite]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@UserNameFile			UserNameTable	READONLY
)
AS

	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	SELECT DISTINCT	
		username
	FROM @UserNameFile F
	WHERE NOT EXISTS (SELECT 1 
					  FROM tblUser U
					  WHERE U.idSite = @idCallerSite
					  AND ((U.isDeleted IS NULL OR U.isDeleted = 0)) -- DO NOT weed out pending users
					  AND U.username = F.username)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	