-- =====================================================================
-- PROCEDURE: [Purchase.GetGrid]
-- =====================================================================

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Purchase.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Purchase.GetGrid]
GO

/*

Gets a listing of purchases for a user, if @idUser is specified, or for all users if @idUser is null.

*/
CREATE PROCEDURE [Purchase.GetGrid] 
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
		
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,

	@idUser					INT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		-- NO SEARCHING FOR NOW

		--IF @searchParam = '' OR @searchParam = '*'
			--BEGIN
			SET @searchParam = NULL
			--END		

		/*

		begin getting the grid data

		*/		

		IF @searchParam IS NULL

			BEGIN

			-- create a temporary table for purchases and put the data into that

			CREATE TABLE #Purchases (
				idPurchase		INT				NOT NULL,
				orderNumber		NVARCHAR(12)	NOT NULL,
				dtOrder			DATETIME		NOT NULL,
				amount			FLOAT			NOT NULL,
				viewReceipt		BIT				NOT NULL
			)

			INSERT INTO #Purchases (
				idPurchase,
				orderNumber,
				dtOrder,
				amount,
				viewReceipt
			)
			SELECT
				P.idPurchase,
				P.orderNumber,
				P.[timeStamp],
				(SELECT SUM(paid) FROM tblTransactionItem TI WHERE TI.idPurchase = P.idPurchase AND TI.paid IS NOT NULL),
				1
			FROM tblPurchase P
			WHERE (
				   (@idUser IS NULL)
				   OR
				   (@idUser IS NOT NULL AND P.idUser = @idUser)
				  )
			AND	(
				 (@idCallerSite IS NULL) 
				 OR 
				 (@idCallerSite IS NOT NULL AND P.idSite = @idCallerSite)
				)
			AND P.[timestamp] IS NOT NULL

			-- return the rowcount

			SELECT COUNT(1) AS row_count 
			FROM #Purchases P			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize)
						P.idPurchase, 
						ROW_NUMBER() OVER (ORDER BY
							-- ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'dtOrder' THEN P.dtOrder END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'amount' THEN P.amount END) END DESC,
							
							-- ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn = 'dtOrder' THEN P.dtOrder END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'amount' THEN P.amount END) END
						)
						AS [row_number]
					FROM #Purchases P												
				), 

				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idPurchase, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)

			SELECT
				P.idPurchase,
				P.orderNumber,
				P.dtOrder,
				P.amount,
				P.viewReceipt,
				SelectedKeys.[row_number]					
			FROM SelectedKeys
			JOIN #Purchases P ON P.idPurchase = SelectedKeys.idPurchase				
			ORDER BY SelectedKeys.[row_number]

			END

	-- drop the temporary table

	DROP TABLE #Purchases

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
