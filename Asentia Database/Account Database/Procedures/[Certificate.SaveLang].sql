-- =====================================================================
-- PROCEDURE: [Certificate.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Certificate.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Certificate.SaveLang]
GO

/*

Saves certificate "language specific" properties for specific language
in certificate language table.

*/

CREATE PROCEDURE [Certificate.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idCertificate			INT,
	@languageString			NVARCHAR(10),
	@name					NVARCHAR(255),
	@description			NVARCHAR(MAX),
	@filename				NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificateSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
		
	/*
	
	validate that the certificate exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCertificate
		WHERE idCertificate = @idCertificate
		AND idSite = @idCallerSite
		AND (isDeleted IS NULL OR isDeleted = 0)
		) <> 1
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CertificateSaveLang_DetailsNotFound'
		RETURN 1 
		END

	/*

	check XSS vulnerabilities

	*/

	-- description

	IF (@description LIKE '%<script%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CertificateSaveLang_DescTagNotAllowed_Script'
	RETURN 1 
	END

	IF (@description LIKE '%<object%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CertificateSaveLang_DescTagNotAllowed_Object'
	RETURN 1 
	END

	IF (@description LIKE '%<frame%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CertificateSaveLang_DescTagNotAllowed_Frame'
	RETURN 1 
	END

	IF (@description LIKE '%<iframe%')
	BEGIN
	SET @Return_Code = 4
	SET @Error_Description_Code = 'CertificateSaveLang_DescTagNotAllowed_Iframe'
	RETURN 1 
	END
		
	/*
	
	validate uniqueness within language
	
	*/
	
	IF( 
		SELECT COUNT(1)
		FROM tblCertificate C
		LEFT JOIN tblCertificateLanguage CL ON CL.idCertificate = C.idCertificate AND CL.idLanguage = @idLanguage -- same language
		WHERE C.idSite = @idCallerSite -- same site
		AND C.idCertificate <> @idCertificate -- not the same Certificate
		AND CL.name = @name -- validate parameter: name
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'CertificateSaveLang_FieldNotUnique'
		RETURN 1
		END
		
	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblCertificateLanguage CL WHERE CL.idCertificate = @idCertificate AND CL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblCertificateLanguage SET
			name = @name,
			[description] = @description,
			[filename] = @filename
		WHERE idCertificate = @idCertificate
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblCertificateLanguage (
			idSite,
			idCertificate,
			idLanguage,
			name,
			[description],
			[filename]
		)
		SELECT
			@idCallerSite,
			@idCertificate,
			@idLanguage,
			@name,
			@description,
			@filename
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblCertificateLanguage CL
			WHERE CL.idCertificate = @idCertificate
			AND CL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblCertificate SET
			name = @name,
			[description] = @description,
			[filename] = @filename
		WHERE idCertificate = @idCertificate

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO