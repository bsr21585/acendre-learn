-- =====================================================================
-- PROCEDURE: [Analytic.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Analytic.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Analytic.Delete]
GO

/*

Deletes analytics

*/

CREATE PROCEDURE [Analytic.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@Analytics				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	IF (SELECT COUNT(1) FROM @Analytics) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		RETURN 1
		END
			
	/*
	
	validate that all Analytics exist within the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM @Analytics AA
		LEFT JOIN tblAnalytic A ON A.idAnalytic = AA.id
		WHERE A.idSite IS NULL
		OR A.idSite <> @idCallerSite
		) > 0
		
		BEGIN 
		SELECT @Return_Code = 1
		RETURN 1 
		END
		
	/*
	
	Mark the Analytic as deleted. DO NOT remove the record. Do NOT delete language or other sub records either.
	
	*/
	
	UPDATE tblAnalytic SET
		isDeleted = 1,
		dtDeleted = GETUTCDATE()
	WHERE idAnalytic IN (
		SELECT id
		FROM @Analytics
	)	

	SELECT @Return_Code = 0
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO