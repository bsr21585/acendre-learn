-- =====================================================================
-- PROCEDURE: [Enrollment.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Enrollment.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.Save]
GO

/*

Adds new enrollment or updates existing enrollment.

*/

CREATE PROCEDURE [Enrollment.Save]
(
	@Return_Code						INT				OUTPUT,
	@Error_Description_Code				NVARCHAR(50)	OUTPUT,
	@idCallerSite						INT				= 0, -- default if not specified
	@callerLangString					NVARCHAR(10),
	@idCaller							INT				= 0, -- will fail if not specified
	
	@idEnrollment						INT				OUTPUT,
	@idCourse							INT,
	@idUser								INT,
	@idRuleSetEnrollment				INT,
	@idGroupEnrollment					INT,
	@idLearningPathEnrollment			INT,
	@idTimezone							INT,
	@isLockedByPrerequisites			BIT,
	@dtStart							DATETIME,
	@dueInterval						INT,
	@dueTimeframe						NVARCHAR(4),
	@expiresFromStartInterval			INT,
	@expiresFromStartTimeframe			NVARCHAR(4),
	@expiresFromFirstLaunchInterval		INT,
	@expiresFromFirstLaunchTimeframe	NVARCHAR(4)
)
AS

	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END

	DECLARE @utcNow DATETIME
	SET @utcNow = GETUTCDATE()
	
	/*

	save the data

	*/

	IF (@idEnrollment = 0 OR @idEnrollment IS NULL)

		BEGIN

		-- insert the new enrollment

		INSERT INTO tblEnrollment (
			idSite,
			idCourse,
			idUser,
			idLearningPathEnrollment,
			idTimezone,
			isLockedByPrerequisites,
			code,
			revcode,
			title,
			dtStart,
			dtDue,
			dtExpiresFromStart,
			dtCreated,
			dueInterval,
			dueTimeframe,
			expiresFromStartInterval,
			expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe
		)
		SELECT
			@idCallerSite,
			@idCourse,
			@idUser,
			@idLearningPathEnrollment,
			@idTimezone,
			@isLockedByPrerequisites,
			C.coursecode,
			C.revcode,
			C.title,
			@dtStart,
			CASE WHEN @dueInterval > 0 THEN
				dbo.IDateAdd(@dueTimeframe, @dueInterval, @dtStart)
			ELSE
				NULL
			END,
			CASE WHEN @expiresFromStartInterval > 0 THEN
				dbo.IDateAdd(@expiresFromStartTimeframe, @expiresFromStartInterval, @dtStart)
			ELSE
				NULL
			END,
			@utcNow,
			@dueInterval,
			@dueTimeframe,
			@expiresFromStartInterval,
			@expiresFromStartTimeframe,
			@expiresFromFirstLaunchInterval,
			@expiresFromFirstLaunchTimeframe
		FROM tblCourse C
		WHERE C.idCourse = @idCourse

		-- get the new enrollment's id

		SET @idEnrollment = SCOPE_IDENTITY()

		/* do the event log entry */

		DECLARE @eventLogItem EventLogItemObjects

		INSERT INTO @eventLogItem (idSite, idObject, idObjectRelated, idObjectUser) SELECT @idCallerSite, @idCourse, @idEnrollment, @idUser

		EXEC [EventLog.Add] @Return_Code, @Error_Description_Code, @idCallerSite, @callerLangString, @idCaller, NULL, 201, @utcNow, @eventLogItem

		/* synchronize lesson data for the new enrollment - POSSIBLY MOVE THIS CALL TO PROCEDURAL CODE */

		EXEC [Enrollment.SynchronizeLessonData] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @idEnrollment

		END

	ELSE

		BEGIN

		-- check that the enrollment id exists
		IF (SELECT COUNT(1) FROM tblEnrollment WHERE idEnrollment = @idEnrollment AND idSite = @idCallerSite) < 1
			BEGIN
			
			SET @idEnrollment = @idEnrollment
			SET @Return_Code = 1
			SET @Error_Description_Code = 'EnrollmentSave_NoRecordFound'
			RETURN 1

			END

		-- update existing enrollment's properties

		UPDATE tblEnrollment SET
			idRuleSetEnrollment					= @idRuleSetEnrollment, -- can be updated to detach a one-off change from its inheritance
			idGroupEnrollment					= @idGroupEnrollment, -- can be updated to detach a one-off change from its inheritance
			idTimezone							= @idTimezone,
			isLockedByPrerequisites				= @isLockedByPrerequisites,
			dtStart								= @dtStart,
			dtDue								= CASE WHEN @dueInterval > 0 THEN
													dbo.IDateAdd(@dueTimeframe, @dueInterval, @dtStart)
												  ELSE
													NULL
												  END,
			dtExpiresFromStart					= CASE WHEN @expiresFromStartInterval > 0 THEN
													dbo.IDateAdd(@expiresFromStartTimeframe, @expiresFromStartInterval, @dtStart)
												  ELSE
													NULL
												  END,
			dueInterval							= @dueInterval,
			dueTimeframe						= @dueTimeframe,
			expiresFromStartInterval			= @expiresFromStartInterval,
			expiresFromStartTimeframe			= @expiresFromStartTimeframe,
			expiresFromFirstLaunchInterval		= @expiresFromFirstLaunchInterval,
			expiresFromFirstLaunchTimeframe		= @expiresFromFirstLaunchTimeframe
		WHERE idEnrollment = @idEnrollment

		-- get the enrollment's id

		SET @idEnrollment = @idEnrollment

		/* synchronize lesson data for the updated enrollment - POSSIBLY MOVE THIS CALL TO PROCEDURAL CODE */

		EXEC [Enrollment.SynchronizeLessonData] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @idEnrollment

		END

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		