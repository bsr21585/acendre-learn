-- =====================================================================
-- PROCEDURE: [StandupTrainingInstance.CheckOrganizerAvailability]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[StandupTrainingInstance.CheckOrganizerAvailability]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [StandupTrainingInstance.CheckOrganizerAvailability]
GO

/*

Check whether or not a GTM, GTT, GTW, or WebEx organizer is availabile to 
schedule a session for specified times. Essentially, this checks for scheduling 
conflicts against other sessions.

*/

CREATE PROCEDURE [StandupTrainingInstance.CheckOrganizerAvailability]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0, --default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,
	
	@idStandupTrainingInstance	INT,
	@idWebMeetingOrganizer		INT,
	@meetingType				INT,
	@MeetingTimes				DateRange		READONLY,
	@isAvailable				BIT             OUTPUT
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT  -- define the permission required to perform this function
	SET @idPermission = 141

	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
	
	/*
	
	set default value

	*/

	SET @isAvailable = 1

	/*

	if this is a new organizer (0), it has no conflicts, return true

	*/

	IF (@idWebMeetingOrganizer = 0)
		BEGIN
		SET @Return_Code = 0
		SET @Error_Description_Code = ''
		RETURN
		END

	/*

	check organizer availability

	*/

	CREATE TABLE #tblOrganizerToObjectLink (
		idStandupTrainingInstance	INT,
		idWebMeetingOrganizer		INT,
		dtStart						DATETIME,
		dtEnd						DATETIME
	)

	INSERT INTO #tblOrganizerToObjectLink (
		idStandupTrainingInstance,
		idWebMeetingOrganizer,
		dtStart,
		dtEnd
	)
	SELECT
		STI.idStandUpTrainingInstance,
		STI.idWebMeetingOrganizer,
		STIMT.dtStart,
		STIMT.dtEnd
	FROM tblStandUpTrainingInstance STI
	INNER JOIN tblStandUpTrainingInstanceMeetingTime STIMT ON STI.idStandUpTrainingInstance = STIMT.idStandUpTrainingInstance
	WHERE (STI.isDeleted IS NULL OR STI.isDeleted = 0)
	AND STI.[type] = @meetingType

	IF (
		SELECT COUNT(1)
        FROM #tblOrganizerToObjectLink OOL
        CROSS JOIN @MeetingTimes DR WHERE (OOL.idWebMeetingOrganizer = @idWebMeetingOrganizer OR (OOL.idWebMeetingOrganizer IS NULL AND @idWebMeetingOrganizer IS NULL)) -- note that NULL organizer is "default organizer"
									AND OOL.idStandupTrainingInstance != @idStandupTrainingInstance 
									AND (  
											(DR.dtStart <= OOL.dtStart AND DR.dtEnd >= OOL.dtEnd)
											OR (DR.dtStart BETWEEN OOL.dtStart AND OOL.dtEnd)
											OR (DR.dtEnd   BETWEEN OOL.dtStart AND OOL.dtEnd)
									)      
		) > 0
		
		BEGIN 
		SET @isAvailable = 0
		END		
		
	DROP TABLE #tblOrganizerToObjectLink

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO