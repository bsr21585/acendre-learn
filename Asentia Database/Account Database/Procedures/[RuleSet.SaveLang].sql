-- =====================================================================
-- PROCEDURE: [RuleSet.SaveLang]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[RuleSet.SaveLang]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [RuleSet.SaveLang]
GO

/*

Saves rule set "language specific" properties for specific language
in rule set language table.

*/

CREATE PROCEDURE [RuleSet.SaveLang]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idRuleSet				INT,
	@languageString			NVARCHAR(10),
	@label					NVARCHAR(255)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
		
	/*
	
	validate that the specified language exists
	
	*/
	
	DECLARE @idLanguage INT
	SELECT @idLanguage = idLanguage FROM tblLanguage WHERE code = @languageString
	
	IF @idLanguage IS NULL
	
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetSaveLang_LanguageDoesNotExist'
		RETURN 1 
		END
		
	/*
	
	validate that the rule set exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblRuleSet
		WHERE idRuleSet = @idRuleSet
		AND idSite = @idCallerSite
		) <> 1
		
		BEGIN 
		SELECT @Return_Code = 1
		SET @Error_Description_Code = 'RuleSetSaveLang_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	validate uniqueness within language
	
	*/
	
	IF( 
		SELECT COUNT(1)
		FROM tblRuleSet RS
		LEFT JOIN tblRuleSetLanguage RSL ON RSL.idRuleSet = RS.idRuleSet AND RSL.idLanguage = @idLanguage -- same language
		WHERE RS.idSite = @idCallerSite -- same site
		AND RS.idRuleSet <> @idRuleSet -- not the same rule set
		AND RSL.label = @label -- validate parameter: label
		) > 0 
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'RuleSetSave_RuleSetNotUnique'
		RETURN 1
		END

	/*

	get data for syncing child rulesets' languages

	*/

	DECLARE @idParentRuleSetEnrollment					INT
	DECLARE @idParentRuleSetLearningPathEnrollment		INT

	DECLARE @idChildRuleSetEnrollment					INT
	DECLARE @idChildRuleSetLearningPathEnrollment		INT

	DECLARE @idChildRuleSet								INT

	SET @idParentRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idRuleSet)
	SET @idParentRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = @idRuleSet)
	
	-- get child rulesets

	SELECT 
		RS.idRuleSet
	INTO #SyncedCourseRuleSetsToUpdate 
	FROM tblRuleSetToRuleSetEnrollmentLink RSTRSEL
	LEFT JOIN tblRuleSetEnrollment RSE ON RSE.idRuleSetEnrollment = RSTRSEL.idRuleSetEnrollment 
	LEFT JOIN tblRuleSet RS on RS.idRuleSet = RSTRSEL.idRuleSet
	WHERE idParentRuleSetEnrollment = @idParentRuleSetEnrollment
	AND RS.label = (SELECT label FROM tblRuleSet WHERE idRuleSet = @idRuleSet)

	SELECT 
		RS.idRuleSet
	INTO #SyncedLearningPathRuleSetsToUpdate 
	FROM tblRuleSetToRuleSetLearningPathEnrollmentLink RSTRSLPEL
	LEFT JOIN tblRuleSetLearningPathEnrollment RSLPE ON RSLPE.idRuleSetLearningPathEnrollment = RSTRSLPEL.idRuleSetLearningPathEnrollment 
	LEFT JOIN tblRuleSet RS on RS.idRuleSet = RSTRSLPEL.idRuleSet
	WHERE idParentRuleSetLearningPathEnrollment = @idParentRuleSetLearningPathEnrollment
	AND RS.label = (SELECT label FROM tblRuleSet WHERE idRuleSet = @idRuleSet)

	/*
	
	update/insert the language in the language table

	*/

	IF (SELECT COUNT(1) FROM tblRuleSetLanguage RSL WHERE RSL.idRuleSet = @idRuleSet AND RSL.idLanguage = @idLanguage) > 0
	
		BEGIN

		UPDATE tblRuleSetLanguage SET
			label = @label
		WHERE idRuleSet = @idRuleSet
		AND idLanguage = @idLanguage

		END

	ELSE
	
		BEGIN
	
		INSERT INTO tblRuleSetLanguage (
			idSite,
			idRuleSet,
			idLanguage,
			label
		)
		SELECT
			@idCallerSite,
			@idRuleSet,
			@idLanguage,
			@label
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblRuleSetLanguage RSL
			WHERE RSL.idRuleSet = @idRuleSet
			AND RSL.idLanguage = @idLanguage
		)

		END

	/*

	if the language we're saving in is also the site's default language, save it in the base table
	note: uniqueness should already be validated

	*/

	IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

		UPDATE tblRuleSet SET
			label = @label
		WHERE idRuleSet = @idRuleSet

		END

	-- sync child rulesets' languages

	IF EXISTS (SELECT 1 FROM #SyncedCourseRuleSetsToUpdate)			-- courses
	BEGIN

		DECLARE idChildRuleSetCursor					CURSOR LOCAL
		FOR 
		SELECT idRuleSet FROM #SyncedCourseRuleSetsToUpdate

		OPEN idChildRuleSetCursor
		FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet
		WHILE @@FETCH_STATUS = 0  
		BEGIN
		
		IF (SELECT COUNT(1) FROM tblRuleSetLanguage RSL WHERE RSL.idRuleSet = @idChildRuleSet AND RSL.idLanguage = @idLanguage) > 0
	
			BEGIN

				UPDATE tblRuleSetLanguage SET
					label = @label
				WHERE idRuleSet = @idChildRuleSet
				AND idLanguage = @idLanguage

			END

		ELSE
	
			BEGIN
	
				INSERT INTO tblRuleSetLanguage (
					idSite,
					idRuleSet,
					idLanguage,
					label
				)
				SELECT
					@idCallerSite,
					@idChildRuleSet,
					@idLanguage,
					@label
				WHERE NOT EXISTS (
					SELECT 1
					FROM tblRuleSetLanguage RSL
					WHERE RSL.idRuleSet = @idChildRuleSet
					AND RSL.idLanguage = @idLanguage
				)

			END

		IF (SELECT idLanguage FROM tblSite S WHERE S.idSite = @idCallerSite) = @idLanguage

		BEGIN

			UPDATE tblRuleSet SET
				label = @label
			WHERE idRuleSet = @idChildRuleSet

		END

		SET @idChildRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idChildRuleSet)

		UPDATE tblRuleSetEnrollment
		SET idParentRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idRuleSet)
		WHERE idRuleSetEnrollment = @idChildRuleSetEnrollment
		
		FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet

		END

	CLOSE idChildRuleSetCursor
	DEALLOCATE idChildRuleSetCursor
	DROP TABLE #SyncedCourseRuleSetsToUpdate

	END

	ELSE IF EXISTS (SELECT 1 FROM #SyncedLearningPathRuleSetsToUpdate)		-- learning paths
	BEGIN

		DECLARE idChildRuleSetCursor					CURSOR LOCAL
		FOR 
		SELECT idRuleSet FROM #SyncedLearningPathRuleSetsToUpdate

		OPEN idChildRuleSetCursor
		FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet
		WHILE @@FETCH_STATUS = 0  
		BEGIN
		
		IF (SELECT COUNT(1) FROM tblRuleSetLanguage RSL WHERE RSL.idRuleSet = @idChildRuleSet AND RSL.idLanguage = @idLanguage) > 0
	
			BEGIN

				UPDATE tblRuleSetLanguage SET
					label = @label
				WHERE idRuleSet = @idChildRuleSet
				AND idLanguage = @idLanguage

			END

		ELSE
	
			BEGIN
	
				INSERT INTO tblRuleSetLanguage (
					idSite,
					idRuleSet,
					idLanguage,
					label
				)
				SELECT
					@idCallerSite,
					@idChildRuleSet,
					@idLanguage,
					@label
				WHERE NOT EXISTS (
					SELECT 1
					FROM tblRuleSetLanguage RSL
					WHERE RSL.idRuleSet = @idChildRuleSet
					AND RSL.idLanguage = @idLanguage
				)

			END

		BEGIN

			UPDATE tblRuleSet SET
				label = @label
			WHERE idRuleSet = @idChildRuleSet

		END

		SET @idChildRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = @idChildRuleSet)

		UPDATE tblRuleSetLearningPathEnrollment
		SET idParentRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = @idRuleSet)
		WHERE idRuleSetLearningPathEnrollment = @idChildRuleSetLearningPathEnrollment
		
		FETCH NEXT FROM idChildRuleSetCursor INTO @idChildRuleSet

		END

	CLOSE idChildRuleSetCursor
	DEALLOCATE idChildRuleSetCursor
	DROP TABLE #SyncedCourseRuleSetsToUpdate

	END

	UPDATE tblRuleSetEnrollment
	SET idParentRuleSetEnrollment = NULL
	WHERE idRuleSetEnrollment = (SELECT idRuleSetEnrollment FROM tblRuleSetToRuleSetEnrollmentLink WHERE idRuleSet = @idRuleSet)

	UPDATE tblRuleSetLearningPathEnrollment
	SET idParentRuleSetLearningPathEnrollment = NULL
	WHERE idRuleSetLearningPathEnrollment = (SELECT idRuleSetLearningPathEnrollment FROM tblRuleSetToRuleSetLearningPathEnrollmentLink WHERE idRuleSet = @idRuleSet)

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO