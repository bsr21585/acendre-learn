-- =====================================================================
-- PROCEDURE: [System.ExecuteRoleAutoJoinRules]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ExecuteRoleAutoJoinRules]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ExecuteRoleAutoJoinRules]
GO

/*

Executes the Auto-Join Rules for Roles.

*/
CREATE PROCEDURE [System.ExecuteRoleAutoJoinRules]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT,
	@Roles						IDTable			READONLY,
	@Filters					IDTable			READONLY,
	@filterBy					NVARCHAR(10)
)
WITH RECOMPILE
AS
	BEGIN
	SET NOCOUNT ON

	/* GET UTC TIMESTAMP */
	DECLARE @dtExecuted DATETIME
	SET @dtExecuted = GETUTCDATE()

	/* 
		
	Create temporary table to store user-to-role joins resulting from rules execution.
	
	*/

	CREATE TABLE #UserToRoleMatches
	(
		idSite		INT,
		idRuleSet	INT,
		idRole		INT,
		idUser		INT
	)

	/*

	Get the ruleset to role matches, and place them in the temporary table.

	*/

	INSERT INTO #UserToRoleMatches
	EXEC [System.GetRuleSetRoleMatches] NULL, NULL, @idCallerSite, @callerLangString, @idCaller, @Roles, @Filters, @filterBy
	
	/*

	Join users that need to be joined, remove users that need to be removed.

	*/
				  
	/* ADD RECORDS TO USER TO ROLE LINK */
	INSERT INTO tblUserToRoleLink
	(
		idSite,
		idUser,
		idRole,
		idRuleSet,
		created
	)
	SELECT
		R.idSite,
		URM.idUser,
		URM.idRole,
		URM.idRuleSet,
		@dtExecuted
	FROM #UserToRoleMatches URM
	LEFT JOIN tblRole R ON R.idRole = URM.idRole
	WHERE NOT EXISTS (SELECT 1 FROM tblUserToRoleLink URL
					  WHERE URL.idRuleSet IS NOT NULL
					  AND URL.idUser = URM.idUser
					  AND URL.idRole = URM.idRole
					  AND URL.idRuleSet = URM.idRuleSet)

	/* REMOVE RECORDS FROM USER TO ROLE LINK */
	DELETE FROM tblUserToRoleLink
	WHERE idUserToRoleLink IN  (
							    SELECT
									URL.idUserToRoleLink
								FROM tblUserToRoleLink URL
								WHERE NOT EXISTS (SELECT 1 FROM #UserToRoleMatches URM
												  WHERE URM.idUser = URL.idUser
												  AND URM.idRole = URL.idRole
												  AND URM.idRuleSet = URL.idRuleSet)
								AND URL.idRuleSet IS NOT NULL
								AND (
										((SELECT COUNT(1) FROM @Filters) = 0)
										OR
										(@filterBy = 'group' AND URL.idUser IN (SELECT DISTINCT idUser FROM tblUserToGroupLink WHERE idGroup IN (SELECT DISTINCT id FROM @Filters)))
										OR
										(@filterBy = 'user' AND URL.idUser IN (SELECT DISTINCT id FROM @Filters))
									)
								AND (
										((SELECT COUNT(1) FROM @Roles) = 0)										
										OR
										(URL.idRole IN (SELECT DISTINCT id FROM @Roles))
									)
							   )

	/* DROP TEMPORARY TABLES */
	DROP TABLE #UserToRoleMatches

	END

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO