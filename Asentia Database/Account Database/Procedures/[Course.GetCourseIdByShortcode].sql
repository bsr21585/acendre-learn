-- =====================================================================
-- PROCEDURE: [Course.GetCourseIdByShortcode]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.GetCourseIdByShortcode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.GetCourseIdByShortcode]
GO

/*

Gets a course id based on the shortcode.

*/
CREATE PROCEDURE [Course.GetCourseIdByShortcode]
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerSite				INT				= 0,	--default if not specified
	@callerLangString			NVARCHAR(10),
	@idCaller					INT				= 0,

	@shortcode					NVARCHAR(10),
	@idCourse					INT				OUTPUT
AS

	BEGIN	
	SET NOCOUNT ON

    SELECT
		@idCourse = C.idCourse
	FROM tblCourse C
	WHERE C.shortcode = @shortcode
	AND C.idSite = @idCallerSite

	IF (@idCourse IS NULL)
		BEGIN
		SET @idCourse = 0
		END
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO		
