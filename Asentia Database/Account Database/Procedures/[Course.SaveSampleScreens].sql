-- =====================================================================
-- PROCEDURE: [Course.SaveSampleScreens]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Course.SaveSampleScreens]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Course.SaveSampleScreens]
GO

/*

Saves sample screens for courses

*/

CREATE PROCEDURE [Course.SaveSampleScreens]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@idCourse				INT,
	@fileNames				FileNameTable   READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @idPermission INT -- define the permission required to perform this function
	SET @idPermission = 141
	
	/*
	
	validate that the caller is a member of the site
	
	*/
	
	IF [dbo].[ValidateCallerAsMemberOfSite](@idCaller, @idCallerSite) = 0
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = '_Global_UserNotAMemberOfSite'
		RETURN 1
		END

	/*
	
	validate caller permission
	
	*/

	IF [dbo].[ValidateCallerPermission](@idCaller, @idPermission, NULL) = 0
		BEGIN
		SET @Return_Code = 3
		SET @Error_Description_Code = '_Global_YouDoNotHavePermission'
		RETURN 1
		END
			
	/*
	
	
	validate that the course belongs to the site
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblCourse C
		WHERE C.idCourse = @idCourse
		AND (C.idSite IS NULL OR C.idSite <> @idCallerSite)
		) > 0
		
		BEGIN 
		SET @Return_Code = 1
		SET @Error_Description_Code = 'CourseSaveSampleScreens_DetailsNotFound'
		RETURN 1 
		END
		
	/*
	
	remove sample screen links for this course where they do not exist in the table param

	*/
	
	DELETE FROM tblCourseToScreenshotLink
	WHERE idCourse = @idCourse 
	AND idSite = @idCallerSite
	AND NOT EXISTS (SELECT 1 FROM @fileNames FNFN
					WHERE FNFN.[filename] = tblCourseToScreenshotLink.[filename])
	
	/*

	insert the course sample screens for ths course into the course screenshot table where they do not already exist
	(if there are any in the table variable, otherwise it means there are none)

	*/

	IF (SELECT COUNT(1) FROM @fileNames) > 0
		BEGIN

		INSERT INTO tblCourseToScreenshotLink (
			idSite,
			idCourse,
			[filename]
		)
		SELECT
			@idCallerSite,
			@idCourse,
			FN.[filename]
		FROM @fileNames FN
		WHERE NOT EXISTS (SELECT 1 FROM tblCourseToScreenshotLink CSSL
						  WHERE CSSL.[filename] = FN.[filename])

		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
	
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO