-- =====================================================================
-- PROCEDURE: [Enrollment.GetGridForUserDashboard]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Enrollment.GetGridForUserDashboard]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Enrollment.GetGridForUserDashboard]
GO

/*

Gets a listing of user enrollments for display on user's dashboard. 

*/

CREATE PROCEDURE [Enrollment.GetGridForUserDashboard]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR(10),
	@idCaller				INT				= 0,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,
	
	@enrollmentStatus		NVARCHAR(20)
)
AS
	BEGIN
		SET NOCOUNT ON

		CREATE TABLE #Enrollments (
			idEnrollment							INT,
			idCourse								INT,
			idUser									INT,
			code									NVARCHAR(255),
			title									NVARCHAR(255),
			idRuleSetEnrollment						INT,
			idGroupEnrollment						INT,
			dtStart									DATETIME,
			dtDue									DATETIME,
			dtExpires								DATETIME,
			dtCompleted								DATETIME,
			dtFirstLaunch							DATETIME,
			isCourseLocked							BIT,
			isLockedByPrerequisites					BIT,
			isLockedByLearningPathOrderEnforcement	BIT,
			numLessons								INT,
			numLessonsCompleted						INT,
			hasUnscheduledILT						BIT							
		)

		DECLARE @idCallerLanguage INT
		SELECT @idCallerLanguage = idLanguage FROM tblLanguage WHERE code = @callerLangString

		INSERT INTO #Enrollments (
			idEnrollment,
			idCourse,
			idUser,
			code,
			title,
			idRuleSetEnrollment,
			idGroupEnrollment,
			dtStart,
			dtDue,
			dtExpires,
			dtCompleted,
			dtFirstLaunch,
			isCourseLocked,
			isLockedByPrerequisites,
			isLockedByLearningPathOrderEnforcement,
			numLessons,
			numLessonsCompleted,
			hasUnscheduledILT
		)
		SELECT
			E.idEnrollment,
			E.idCourse,
			E.idUser,
			E.code,
			CASE WHEN CL.title IS NOT NULL THEN CL.title ELSE C.title END,
			E.idRuleSetEnrollment,
			E.idGroupEnrollment,
			E.dtStart,
			E.dtDue,
			CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
				NULL
			ELSE
				CASE WHEN E.dtExpiresFromStart IS NULL AND E.dtExpiresFromFirstLaunch IS NOT NULL THEN
					E.dtExpiresFromFirstLaunch
				ELSE
					CASE WHEN E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromFirstLaunch IS NULL THEN
						E.dtExpiresFromStart
					ELSE
						CASE WHEN E.dtExpiresFromStart >= E.dtExpiresFromFirstLaunch THEN
							E.dtExpiresFromFirstLaunch
						ELSE
							E.dtExpiresFromStart
						END
					END
				END
			END AS [dtExpires],
			E.dtCompleted,
			E.dtFirstLaunch,
			CASE WHEN C.isLocked = 1 THEN 1 ELSE 0 END AS isCourseLocked,
			CASE WHEN E.isLockedByPrerequisites = 1 THEN
				convert(bit, [dbo].[EvaluateEnrollmentPrerequisiteLock](E.idEnrollment))
			ELSE
				convert(bit, 0)
			END AS isLockedByPrerequisites,
			CASE WHEN E.idLearningPathEnrollment IS NOT NULL THEN
				convert(bit, [dbo].[EvaluateLearningPathEnrollmentOrderingLock](E.idEnrollment))
			ELSE
				convert(bit, 0)
			END AS isLockedByLearningPathOrderEnforcement,
			(SELECT COUNT(1) FROM [tblData-Lesson] DL WHERE DL.idEnrollment = E.idEnrollment) AS numLessons,
			-- we calculate number of lessons completed this way so that we can display a 100% progress bar for
			-- completions that have occured via administrator override where no lesson data was actually completed by the learner
			CASE WHEN @enrollmentStatus = 'completed' THEN
				(SELECT COUNT(1) FROM [tblData-Lesson] DL WHERE DL.idEnrollment = E.idEnrollment)
			ELSE
				(SELECT COUNT(1) FROM [tblData-Lesson] DL WHERE DL.idEnrollment = E.idEnrollment AND DL.dtCompleted IS NOT NULL)
			END AS numLessonsCompleted,
			CASE WHEN @enrollmentStatus = 'completed' OR @enrollmentStatus = 'expired' THEN 
				CONVERT(BIT, 0)
			ELSE
				CASE WHEN (SELECT COUNT(1) FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson IN (SELECT idLesson FROM tblLesson WHERE idCourse = E.idCourse)) = 0 THEN
					CONVERT(BIT, 0)
				ELSE
					CASE WHEN 
						(
						 SELECT COUNT(1) 
						 FROM tblStandupTrainingInstanceToUserLink STIUL 
						 LEFT JOIN tblStandUpTrainingInstance STI ON STI.idStandUpTrainingInstance = STIUL.idStandupTrainingInstance
						 WHERE STIUL.idUser = E.idUser
						 AND STI.idStandUpTraining IN (SELECT LCL.idObject FROM tblLessonToContentLink LCL WHERE LCL.idContentType = 2 AND LCL.idLesson IN (SELECT idLesson FROM tblLesson WHERE idCourse = E.idCourse))
						) 
						<
						(
						 SELECT COUNT(1) 
						 FROM tblLessonToContentLink LCL 
						 WHERE LCL.idContentType = 2 
						 AND LCL.idLesson IN (SELECT idLesson FROM tblLesson WHERE idCourse = E.idCourse)
						)
					THEN
						CONVERT(BIT, 1)
					ELSE
						CONVERT(BIT, 0)
					END
				END
			END AS hasUnscheduledILT
		FROM tblEnrollment E
		LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
		LEFT JOIN tblCourseLanguage CL ON CL.idCourse = C.idCourse AND CL.idLanguage = @idCallerLanguage
		WHERE E.idUser = @idCaller
		AND 
		(
				(
				@enrollmentStatus = 'enrolled'
				AND (E.dtStart <= GETUTCDATE())
				AND (E.dtDue > GETUTCDATE() OR E.dtDue IS NULL)
				AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
				AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
				AND E.dtCompleted IS NULL
				)
			OR
				(
				@enrollmentStatus = 'overdue'
				AND (E.dtStart <= GETUTCDATE())
				AND E.dtDue <= GETUTCDATE()
				AND (E.dtExpiresFromStart > GETUTCDATE() OR E.dtExpiresFromStart IS NULL)
				AND (E.dtExpiresFromFirstLaunch > GETUTCDATE() OR E.dtExpiresFromFirstLaunch IS NULL)
				AND E.dtCompleted IS NULL
				)
			OR
				(
				@enrollmentStatus = 'completed'
				AND (E.dtStart <= GETUTCDATE())
				AND E.dtCompleted IS NOT NULL
				)
			OR
				(
				@enrollmentStatus = 'expired'
				AND (E.dtStart <= GETUTCDATE())
				AND (
						(E.dtExpiresFromStart IS NOT NULL AND E.dtExpiresFromStart <= GETUTCDATE())
						OR
						(E.dtExpiresFromFirstLaunch IS NOT NULL AND E.dtExpiresFromFirstLaunch <= GETUTCDATE())
					)
				AND E.dtCompleted IS NULL
				)
		)
		AND E.idActivityImport IS NULL -- DO NOT INCLUDE ACTIVITY IMPORT ENROLLMENT RECORDS
		AND C.isDeleted = 0 -- DO NOT INCLUDE DELETED ENROLLMENTS
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END
		
		/*

		begin getting the grid data

		*/

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #Enrollments E
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						E.idEnrollment,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCompleted' THEN dtCompleted END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtDue END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtCompleted END) END DESC,
							-- FORTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtExpires END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtCompleted' THEN dtCompleted END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtExpires', 'dtExpires', 'dtCompleted') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtExpires', 'dtCompleted') THEN dtDue END) END,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtCompleted END) END,
							-- FORTH ORDER DESC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtExpires END) END
						)
						AS [row_number]
					FROM #Enrollments E
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idEnrollment, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT
				E.idEnrollment,
				E.idCourse,
				E.code,
				E.title,
				E.dtDue AS [dtDue],
				E.dtCompleted AS [dtCompleted],
				E.dtExpires AS [dtExpires],
				E.dtFirstLaunch AS [dtFirstLaunch],
				CASE WHEN (C.isFeedActive = 1 AND (C.isDeleted IS NULL OR C.isDeleted = 0)) THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS [wall],
				CASE WHEN C.isDeleted = 1 THEN  CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS [details],
				CASE 
					WHEN (E.isCourseLocked = 1) THEN CONVERT(INT, 5) -- 5 is locked because course is locked
					WHEN (E.isLockedByLearningPathOrderEnforcement = 1) THEN CONVERT(INT, 4) -- 4 is locked by learning path ordering
					WHEN (E.isLockedByPrerequisites = 1) THEN CONVERT(INT, 3) -- 3 is locked by prerequisites
					WHEN (@enrollmentStatus = 'expired') OR (@enrollmentStatus = 'completed' AND (E.dtExpires IS NOT NULL AND E.dtExpires <= GETUTCDATE())) THEN CONVERT(INT, 2) -- 2 is expired
				ELSE
					CONVERT(INT, 1) -- 1 is OK to launch
				END AS [launch],
				E.numLessons,
				E.numLessonsCompleted,
				E.hasUnscheduledILT,
				E.isCourseLocked,
				C.avatar,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #Enrollments E ON E.idEnrollment = SelectedKeys.idEnrollment
			LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #Enrollments E
			INNER JOIN CONTAINSTABLE(tblEnrollment, *, @searchParam) K ON K.[key] = E.idEnrollment
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						E.idEnrollment,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'dtCompleted' THEN dtCompleted END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtDue END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtCompleted END) END DESC,
							-- FORTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtExpires END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtExpires' THEN dtExpires END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtDue' THEN dtDue END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'dtCompleted' THEN dtCompleted END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN title END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('dtExpires', 'dtExpires', 'dtCompleted') THEN title END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtExpires', 'dtCompleted') THEN dtDue END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtCompleted END) END,
							-- FORTH ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('dtExpires', 'dtDue', 'dtCompleted') THEN dtExpires END) END
						)
						AS [row_number]
					FROM #Enrollments E
					INNER JOIN CONTAINSTABLE(tblEnrollment, *, @searchParam) K ON K.[key] = E.idEnrollment
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idEnrollment, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				E.idEnrollment,
				E.idCourse,
				E.code,
				E.title,
				E.dtDue AS [dtDue],
				E.dtCompleted AS [dtCompleted],
				E.dtExpires AS [dtExpires],
				E.dtFirstLaunch AS [dtFirstLaunch],
				CASE WHEN (C.isFeedActive = 1 AND (C.isDeleted IS NULL OR C.isDeleted = 0)) THEN CONVERT(BIT, 1) ELSE CONVERT(BIT, 0) END AS [wall],
				CASE WHEN C.isDeleted = 1 THEN  CONVERT(BIT, 0) ELSE CONVERT(BIT, 1) END AS [details],
				CASE 
					WHEN (E.isCourseLocked = 1) THEN CONVERT(INT, 5) -- 5 is locked because course is locked
					WHEN (E.isLockedByLearningPathOrderEnforcement = 1) THEN CONVERT(INT, 4) -- 4 is locked by learning path ordering
					WHEN (E.isLockedByPrerequisites = 1) THEN CONVERT(INT, 3) -- 3 is locked by prerequisites
					WHEN (@enrollmentStatus = 'expired') OR (@enrollmentStatus = 'completed' AND (E.dtExpires IS NOT NULL AND E.dtExpires <= GETUTCDATE())) THEN CONVERT(INT, 2) -- 2 is expired
				ELSE
					CONVERT(INT, 1) -- 1 is OK to launch
				END AS [launch],
				E.numLessons,
				E.numLessonsCompleted,
				E.hasUnscheduledILT,
				E.isCourseLocked,
				C.avatar,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #Enrollments E ON E.idEnrollment = SelectedKeys.idEnrollment
			LEFT JOIN tblCourse C ON C.idCourse = E.idCourse
			ORDER BY SelectedKeys.[row_number]
			
			END

		-- DROP THE TEMPORARY TABLE
		DROP TABLE #Enrollments
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO