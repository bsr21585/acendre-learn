/* 

NON-CLUSTERED INDEXES 

*/

-- [tblCertificationToUserLink]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblCertificationToUserLink]') AND name = N'NCI_CertificationToUserLink_User')
	DROP INDEX [NCI_CertificationToUserLink_User] ON [tblCertificationToUserLink] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_CertificationToUserLink_User] ON [tblCertificationToUserLink]
( 
	[idUser] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblContentPackage]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblContentPackage]') AND name = N'NCI_ContentPackage_Site')
	DROP INDEX [NCI_ContentPackage_Site] ON [tblContentPackage] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_ContentPackage_Site] ON [tblContentPackage] 
(
	[idSite] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblCourse]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblCourse]') AND name = N'NCI_Course_Site')
	DROP INDEX [NCI_Course_Site] ON [tblCourse] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_Course_Site] ON [tblCourse] 
(
	[idSite] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblData-CertificationModuleRequirement]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblData-CertificationModuleRequirement]') AND name = N'NCI_DataCertificationModuleRequirement_CertificationUserLink')
	DROP INDEX [NCI_DataCertificationModuleRequirement_CertificationUserLink] ON [tblData-CertificationModuleRequirement] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_DataCertificationModuleRequirement_CertificationUserLink] ON [tblData-CertificationModuleRequirement]
( 
	[idCertificationToUserLink] ASC	
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblData-HomeworkAssignment]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblData-HomeworkAssignment]') AND name = N'NCI_DataHomeworkAssignment_DataLesson')
	DROP INDEX [NCI_DataHomeworkAssignment_DataLesson] ON [tblData-HomeworkAssignment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_DataHomeworkAssignment_DataLesson] ON [tblData-HomeworkAssignment]
( 
	[idData-Lesson] ASC	
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblData-Lesson]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblData-Lesson]') AND name = N'NCI_DataLesson_Enrollment')
	DROP INDEX [NCI_DataLesson_Enrollment] ON [tblData-Lesson] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_DataLesson_Enrollment] ON [tblData-Lesson]
( 
	[idEnrollment] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblData-Lesson]') AND name = N'NCI_DataLesson_EnrollmentOrderLesson')
	DROP INDEX [NCI_DataLesson_EnrollmentOrderLesson] ON [tblData-Lesson] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_DataLesson_EnrollmentOrderLesson] ON [tblData-Lesson]
( 
	[idEnrollment] ASC,
	[order] ASC,
	[idLesson] ASC	
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblData-SCO]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblData-SCO]') AND name = N'NCI_DataSCO_DataLesson')
	DROP INDEX [NCI_DataSCO_DataLesson] ON [tblData-SCO] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_DataSCO_DataLesson] ON [tblData-SCO]
( 
	[idData-Lesson] ASC	
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblData-SCOInt]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblData-SCOInt]') AND name = N'NCI_DataSCOInt_DataSCO')
	DROP INDEX [NCI_DataSCOInt_DataSCO] ON [tblData-SCOInt] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_DataSCOInt_DataSCO] ON [tblData-SCOInt]
( 
	[idData-SCO] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblData-SCOObj]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblData-SCOObj]') AND name = N'NCI_DataSCOObj_DataSCO')
	DROP INDEX [NCI_DataSCOObj_DataSCO] ON [tblData-SCOObj] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_DataSCOObj_DataSCO] ON [tblData-SCOObj]
( 
	[idData-SCO] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblEnrollment]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblEnrollment]') AND name = N'NCI_Enrollment_User')
	DROP INDEX [NCI_Enrollment_User] ON [tblEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_Enrollment_User] ON [tblEnrollment]
( 
	[idUser] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblEnrollment]') AND name = N'NCI_Enrollment_GroupEnrollment')
	DROP INDEX [NCI_Enrollment_GroupEnrollment] ON [tblEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_Enrollment_GroupEnrollment] ON [tblEnrollment]
( 
	[idGroupEnrollment] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblEnrollment]') AND name = N'NCI_Enrollment_RuleSetEnrollment')
	DROP INDEX [NCI_Enrollment_RuleSetEnrollment] ON [tblEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_Enrollment_RuleSetEnrollment] ON [tblEnrollment]
( 
	[idRuleSetEnrollment] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblEnrollment]') AND name = N'NCI_Enrollment_LearningPathEnrollment')
	DROP INDEX [NCI_Enrollment_LearningPathEnrollment] ON [tblEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_Enrollment_LearningPathEnrollment] ON [tblEnrollment]
( 
	[idLearningPathEnrollment] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblEnrollment]') AND name = N'NCI_Enrollment_UserEnrollmentCompletedCourse')
	DROP INDEX [NCI_Enrollment_UserEnrollmentCompletedCourse] ON [tblEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_Enrollment_UserEnrollmentCompletedCourse] ON [tblEnrollment]
( 
	[idUser] ASC,
	[idEnrollment] ASC,
	[dtCompleted] ASC,
	[idCourse] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblGroup]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblGroup]') AND name = N'NCI_Group_Site')
	DROP INDEX [NCI_Group_Site] ON [tblGroup] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_Group_Site] ON [tblGroup]
( 
	[idSite] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblGroup]') AND name = N'NCI_Group_SiteName')
	DROP INDEX [NCI_Group_SiteName] ON [tblGroup] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_Group_SiteName] ON [tblGroup]
( 
	[name] ASC,
	[idSite] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblGroupEnrollment]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblGroupEnrollment]') AND name = N'NCI_GroupEnrollment_Group')
	DROP INDEX [NCI_GroupEnrollment_Group] ON [tblGroupEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_GroupEnrollment_Group] ON [tblGroupEnrollment]
( 
	[idGroup] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblLearningPathEnrollment]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblLearningPathEnrollment]') AND name = N'NCI_LearningPathEnrollment_User')
	DROP INDEX [NCI_LearningPathEnrollment_User] ON [tblLearningPathEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_LearningPathEnrollment_User] ON [tblLearningPathEnrollment]
( 
	[idUser] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblLearningPathEnrollment]') AND name = N'NCI_LearningPathEnrollment_RuleSetEnrollment')
	DROP INDEX [NCI_LearningPathEnrollment_RuleSetEnrollment] ON [tblLearningPathEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_LearningPathEnrollment_RuleSetEnrollment] ON [tblLearningPathEnrollment]
( 
	[idRuleSetLearningPathEnrollment] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblRuleSetEnrollment]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetEnrollment]') AND name = N'NCI_RuleSetEnrollment_Site')
	DROP INDEX [NCI_RuleSetEnrollment_Site] ON [tblRuleSetEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_RuleSetEnrollment_Site] ON [tblRuleSetEnrollment]
( 
	[idSite] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetEnrollment]') AND name = N'NCI_RuleSetEnrollment_Course')
	DROP INDEX [NCI_RuleSetEnrollment_Course] ON [tblRuleSetEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_RuleSetEnrollment_Course] ON [tblRuleSetEnrollment]
( 
	[idCourse] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetEnrollment]') AND name = N'NCI_RuleSetEnrollment_RulseSetEnrollmentRecurInterval')
	DROP INDEX [NCI_RuleSetEnrollment_RulseSetEnrollmentRecurInterval] ON [tblRuleSetEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_RuleSetEnrollment_RulseSetEnrollmentRecurInterval] ON [tblRuleSetEnrollment]
( 
	[idRuleSetEnrollment] ASC,
	[recurInterval] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetEnrollment]') AND name = N'NCI_RuleSetEnrollment_StartEndCourseRuleSetEnrollment')
	DROP INDEX [NCI_RuleSetEnrollment_StartEndCourseRuleSetEnrollment] ON [tblRuleSetEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_RuleSetEnrollment_StartEndCourseRuleSetEnrollment] ON [tblRuleSetEnrollment]
( 
	[dtStart] ASC,
	[dtEnd] ASC,
	[idCourse] ASC,
	[idRuleSetEnrollment] ASC	
) INCLUDE ([priority]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblRuleSetLearningPathEnrollment]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetLearningPathEnrollment]') AND name = N'NCI_RuleSetLearningPathEnrollment_LearningPath')
	DROP INDEX [NCI_RuleSetLearningPathEnrollment_LearningPath] ON [tblRuleSetLearningPathEnrollment] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_RuleSetLearningPathEnrollment_LearningPath] ON [tblRuleSetLearningPathEnrollment]
( 
	[idLearningPath] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblStandupTrainingInstanceToUserLink]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblStandupTrainingInstanceToUserLink]') AND name = N'NCI_StandupTrainingInstanceToUserLink_User')
	DROP INDEX [NCI_StandupTrainingInstanceToUserLink_User] ON [tblStandupTrainingInstanceToUserLink] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [NCI_StandupTrainingInstanceToUserLink_User] ON [tblStandupTrainingInstanceToUserLink]
( 
	[idUser] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblTransactionItem]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblTransactionItem]') AND name = N'NCI_TransactionItem_UserSite')
	DROP INDEX [NCI_TransactionItem_UserSite] ON [tblTransactionItem] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_TransactionItem_UserSite] ON [tblTransactionItem] 
(
	[idUser] ASC,
	[idSite] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblUser]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Site')
	DROP INDEX [NCI_User_Site] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Site] ON [tblUser] 
(
	[idSite] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_SiteUserDeletedApproved')
	DROP INDEX [NCI_User_SiteUserDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_SiteUserDeletedApproved] ON [tblUser] 
(
	[idSite] ASC,
	[idUser] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_DeletedApprovedSite')
	DROP INDEX [NCI_User_DeletedApprovedSite] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_DeletedApprovedSite] ON [tblUser] 
(	
	[isDeleted] ASC,
	[isRegistrationApproved] ASC,
	[idSite] ASC
) INCLUDE ([idUser], [username]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_UsernameSiteDeletedApproved')
	DROP INDEX [NCI_User_UsernameSiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_UsernameSiteDeletedApproved] ON [tblUser] 
(	
	[username] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_EmailSiteDeletedApproved')
	DROP INDEX [NCI_User_EmailSiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_EmailSiteDeletedApproved] ON [tblUser] 
(	
	[email] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_CompanySiteDeletedApproved')
	DROP INDEX [NCI_User_CompanySiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_CompanySiteDeletedApproved] ON [tblUser] 
(	
	[company] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_ProvinceSiteDeletedApproved')
	DROP INDEX [NCI_User_ProvinceSiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_ProvinceSiteDeletedApproved] ON [tblUser] 
(	
	[province] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_RegionSiteDeletedApproved')
	DROP INDEX [NCI_User_RegionSiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_RegionSiteDeletedApproved] ON [tblUser] 
(	
	[region] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_DepartmentSiteDeletedApproved')
	DROP INDEX [NCI_User_DepartmentSiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_DepartmentSiteDeletedApproved] ON [tblUser] 
(	
	[department] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_JobTitleSiteDeletedApproved')
	DROP INDEX [NCI_User_JobTitleSiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_JobTitleSiteDeletedApproved] ON [tblUser] 
(	
	[jobTitle] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_JobClassSiteDeletedApproved')
	DROP INDEX [NCI_User_JobClassSiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_JobClassSiteDeletedApproved] ON [tblUser] 
(	
	[jobClass] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field00SiteDeletedApproved')
	DROP INDEX [NCI_User_Field00SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field00SiteDeletedApproved] ON [tblUser] 
(	
	[field00] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field01SiteDeletedApproved')
	DROP INDEX [NCI_User_Field01SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field01SiteDeletedApproved] ON [tblUser] 
(	
	[field01] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field02SiteDeletedApproved')
	DROP INDEX [NCI_User_Field02SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field02SiteDeletedApproved] ON [tblUser] 
(	
	[field02] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field03SiteDeletedApproved')
	DROP INDEX [NCI_User_Field03SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field03SiteDeletedApproved] ON [tblUser] 
(	
	[field03] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field04SiteDeletedApproved')
	DROP INDEX [NCI_User_Field04SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field04SiteDeletedApproved] ON [tblUser] 
(	
	[field04] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field05SiteDeletedApproved')
	DROP INDEX [NCI_User_Field05SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field05SiteDeletedApproved] ON [tblUser] 
(	
	[field05] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field06SiteDeletedApproved')
	DROP INDEX [NCI_User_Field06SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field06SiteDeletedApproved] ON [tblUser] 
(	
	[field06] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field07SiteDeletedApproved')
	DROP INDEX [NCI_User_Field07SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field07SiteDeletedApproved] ON [tblUser] 
(	
	[field07] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field08SiteDeletedApproved')
	DROP INDEX [NCI_User_Field08SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field08SiteDeletedApproved] ON [tblUser] 
(	
	[field08] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUser]') AND name = N'NCI_User_Field09SiteDeletedApproved')
	DROP INDEX [NCI_User_Field09SiteDeletedApproved] ON [tblUser] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_User_Field09SiteDeletedApproved] ON [tblUser] 
(	
	[field09] ASC,
	[idSite] ASC,
	[isDeleted] ASC,
	[isRegistrationApproved] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblUserToGroupLink]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUserToGroupLink]') AND name = N'NCI_UserToGroupLink_UserGroup')
	DROP INDEX [NCI_UserToGroupLink_UserGroup] ON [tblUserToGroupLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_UserToGroupLink_UserGroup] ON [tblUserToGroupLink] 
(
	[idUser] ASC, 
	[idGroup] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUserToGroupLink]') AND name = N'NCI_UserToGroupLink_GroupUser')
	DROP INDEX [NCI_UserToGroupLink_GroupUser] ON [tblUserToGroupLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_UserToGroupLink_GroupUser] ON [tblUserToGroupLink] 
(
	[idGroup] ASC,
	[idUser] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUserToGroupLink]') AND name = N'NCI_UserToGroupLink_User')
	DROP INDEX [NCI_UserToGroupLink_User] ON [tblUserToGroupLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_UserToGroupLink_User] ON [tblUserToGroupLink] 
(
	[idUser] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblUserToGroupLink]') AND name = N'NCI_UserToGroupLink_Group')
	DROP INDEX [NCI_UserToGroupLink_Group] ON [tblUserToGroupLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_UserToGroupLink_Group] ON [tblUserToGroupLink] 
(
	[idGroup] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblRuleSet]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSet]') AND name = N'NCI_RuleSet_Site')
	DROP INDEX [NCI_RuleSet_Site] ON [tblRuleSet] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSet_Site] ON [tblRuleSet] 
(
	[idSite] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblRule]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRule]') AND name = N'NCI_Rule_RuleSet')
	DROP INDEX [NCI_Rule_RuleSet] ON [tblRule] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_Rule_RuleSet] ON [tblRule] 
(
	[idRuleSet] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblRuleSetToGroupLink]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToGroupLink]') AND name = N'NCI_RuleSetToGroupLink_RuleSet')
	DROP INDEX [NCI_RuleSetToGroupLink_RuleSet] ON [tblRuleSetToGroupLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToGroupLink_RuleSet] ON [tblRuleSetToGroupLink] 
(
	[idRuleSet] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToGroupLink]') AND name = N'NCI_RuleSetToGroupLink_Group')
	DROP INDEX [NCI_RuleSetToGroupLink_Group] ON [tblRuleSetToGroupLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToGroupLink_Group] ON [tblRuleSetToGroupLink] 
(
	[idGroup] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblRuleSetToRoleLink]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToRoleLink]') AND name = N'NCI_RuleSetToRoleLink_RuleSet')
	DROP INDEX [NCI_RuleSetToRoleLink_RuleSet] ON [tblRuleSetToRoleLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToRoleLink_RuleSet] ON [tblRuleSetToRoleLink] 
(
	[idRuleSet] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToRoleLink]') AND name = N'NCI_RuleSetToRoleLink_Role')
	DROP INDEX [NCI_RuleSetToRoleLink_Role] ON [tblRuleSetToRoleLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToRoleLink_Role] ON [tblRuleSetToRoleLink] 
(
	[idRole] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblRuleSetToRuleSetEnrollmentLink]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToRuleSetEnrollmentLink]') AND name = N'NCI_RuleSetToRuleSetEnrollmentLink_RuleSet')
	DROP INDEX [NCI_RuleSetToRuleSetEnrollmentLink_RuleSet] ON [tblRuleSetToRuleSetEnrollmentLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToRuleSetEnrollmentLink_RuleSet] ON [tblRuleSetToRuleSetEnrollmentLink] 
(
	[idRuleSet] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToRuleSetEnrollmentLink]') AND name = N'NCI_RuleSetToRuleSetEnrollmentLink_RuleSetEnrollment')
	DROP INDEX [NCI_RuleSetToRuleSetEnrollmentLink_RuleSetEnrollment] ON [tblRuleSetToRuleSetEnrollmentLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToRuleSetEnrollmentLink_RuleSetEnrollment] ON [tblRuleSetToRuleSetEnrollmentLink] 
(
	[idRuleSetEnrollment] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblRuleSetToRuleSetLearningPathEnrollmentLink]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToRuleSetLearningPathEnrollmentLink]') AND name = N'NCI_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSet')
	DROP INDEX [NCI_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSet] ON [tblRuleSetToRuleSetLearningPathEnrollmentLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSet] ON [tblRuleSetToRuleSetLearningPathEnrollmentLink] 
(
	[idRuleSet] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToRuleSetLearningPathEnrollmentLink]') AND name = N'NCI_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSetLearningPathEnrollment')
	DROP INDEX [NCI_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSetLearningPathEnrollment] ON [tblRuleSetToRuleSetLearningPathEnrollmentLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToRuleSetLearningPathEnrollmentLink_RuleSetLearningPathEnrollment] ON [tblRuleSetToRuleSetLearningPathEnrollmentLink] 
(
	[idRuleSetLearningPathEnrollment] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

-- [tblRuleSetToCertificationLink]

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToCertificationLink]') AND name = N'NCI_RuleSetToCertificationLink_RuleSet')
	DROP INDEX [NCI_RuleSetToCertificationLink_RuleSet] ON [tblRuleSetToCertificationLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToCertificationLink_RuleSet] ON [tblRuleSetToCertificationLink] 
(
	[idRuleSet] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[tblRuleSetToCertificationLink]') AND name = N'NCI_RuleSetToCertificationLink_Certification')
	DROP INDEX [NCI_RuleSetToCertificationLink_Certification] ON [tblRuleSetToCertificationLink] WITH ( ONLINE = OFF )
GO	

CREATE NONCLUSTERED INDEX [NCI_RuleSetToCertificationLink_Certification] ON [tblRuleSetToCertificationLink] 
(
	[idCertification] ASC
) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO