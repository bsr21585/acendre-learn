﻿IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDatabaseServer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDatabaseServer] (
	[idDatabaseServer] [INT] IDENTITY(1,1) NOT NULL,
	[serverName] [NVARCHAR](255) NOT NULL, 
	[networkName] [NVARCHAR](255) NOT NULL,
	[username] [NVARCHAR](512) NULL,
	[password] [NVARCHAR](512) NULL,
	[isTrusted] [BIT] NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_idDatabaseServer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatabaseServer ADD CONSTRAINT [PK_idDatabaseServer] PRIMARY KEY CLUSTERED (idDatabaseServer ASC)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDatabaseServer]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDatabaseServer]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblDatabaseServer (
	serverName,
	networkName
)
KEY INDEX PK_idDatabaseServer
ON [Asentia-CustomerManager] -- insert index to this catalog

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccount]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccount] (
	[idAccount] [int] IDENTITY (2, 1) NOT NULL,
	[idDatabaseServer] [int] NULL, 
	[company] [nvarchar] (255) NOT NULL,
	[contactFirstName] [nvarchar] (255) NOT NULL,
	[contactLastName] [nvarchar] (255) NOT NULL,
	[contactDisplayName] [nvarchar] (512) NOT NULL,
	[contactEmail] [nvarchar] (255) NULL,
	[username] [nvarchar] (512) NOT NULL,
	[password] [nvarchar] (512) NULL,
	[databaseName] [nvarchar] (255) NULL,
	[dtCreated] [datetime] NOT NULL,
	[isActive] [bit] NOT NULL,
	[isDeleted] [bit] NOT NULL,
	[isJobProcessing] [bit] NULL,
	[dtLastJobProcessStart] [datetime] NULL,
	[dtLastJobProcessEnd] [datetime] NULL,
	[isPowerPointJobProcessing] [bit] NULL,
	[dtLastPowerPointJobProcessStart] [datetime] NULL,
	[dtLastPowerPointJobProcessEnd] [datetime] NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccount ADD CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED (idAccount ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Account_DatabaseServer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccount ADD CONSTRAINT [FK_Account_DatabaseServer] FOREIGN KEY (idDatabaseServer) REFERENCES [tblDatabaseServer] (idDatabaseServer)
			
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAccount]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAccount]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblAccount (
	company,
	contactDisplayName, 
	contactEmail
)
KEY INDEX PK_Account
ON [Asentia-CustomerManager] -- insert index to this catalog

/**
INSERT TEMPLATE ACCOUNT
**/
SET IDENTITY_INSERT tblAccount ON

UPDATE tblAccount SET
	company = '__SYSTEM__',
	contactFirstName = '__SYSTEM__',
	contactLastName = '__SYSTEM__',
	contactDisplayName = '__SYSTEM__',
	contactEmail = '__SYSTEM__',
	username = '__SYSTEM__',
	[password] = '__SYSTEM__',
	isActive = 1,
	isDeleted = 0
WHERE idAccount = 1

INSERT INTO tblAccount (
	idAccount,
	company,
	contactFirstName,
	contactLastName,
	contactDisplayName,
	contactEmail,
	username,
	[password],
	dtCreated,
	isActive,
	isDeleted
) 
SELECT
	1,
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	GETUTCDATE(),
	1,
	0
WHERE NOT EXISTS (
	SELECT 1
	FROM tblAccount
	WHERE idAccount = 1
)

SET IDENTITY_INSERT tblAccount OFF
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountContract]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountContract] (
	[idAccountContract]					INT			IDENTITY(1,1)	NOT NULL,
	[idAccount]							INT							NOT NULL,
	[allowedUsers]						INT							NULL,		-- NULL means "unlimited"
	[disabledUserRatio]					FLOAT						NULL,		-- NULL means "no disabled user ratio"
	[disabledUserThresholdInterval]		INT							NULL,		-- NULL means "no disabled user ratio"
	[disabledUserThresholdTimeframe]	NVARCHAR(4)					NULL,	    -- NULL means "no disabled user ratio"
	[diskSpaceLimitKB]					INT							NULL,		-- NULL means "unlimited"
	[contractNotes]						NVARCHAR(MAX)				NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountContract]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountContract ADD CONSTRAINT [PK_AccountContract] PRIMARY KEY CLUSTERED (idAccountContract ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountContract_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountContract ADD CONSTRAINT [FK_AccountContract_Account] FOREIGN KEY (idAccount) REFERENCES [tblAccount] (idAccount)
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountContractToPortalLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountContractToPortalLink] (
	[idAccountContractToPortalLink]	INT			IDENTITY(1,1)	NOT NULL,
	[idAccountContract]				INT							NOT NULL,
	[idPortal]						INT							NULL,		-- NULL means "all portals"
	[portalName]					NVARCHAR(255)				NULL		-- NULL means "all portals"
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountContractToPortalLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountContractToPortalLink ADD CONSTRAINT [PK_AccountContractToPortalLink] PRIMARY KEY CLUSTERED (idAccountContractToPortalLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountContractToPortalLink_AccountContract]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountContractToPortalLink ADD CONSTRAINT [FK_AccountContractToPortalLink_AccountContract] FOREIGN KEY (idAccountContract) REFERENCES [tblAccountContract] (idAccountContract)	
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountParam]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountParam] (
	[idAccountParam] [INT] IDENTITY(1,1) NOT NULL,
	[idAccount] [INT] NOT NULL, 
	[param] [NVARCHAR](255) NOT NULL,
	[value] [NVARCHAR](255) NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountParam]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountParam ADD CONSTRAINT [PK_AccountParam] PRIMARY KEY CLUSTERED (idAccountParam ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountParam_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountParam ADD CONSTRAINT [FK_AccountParam_Account] FOREIGN KEY (idAccount) REFERENCES [tblAccount] (idAccount)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountToDomainAliasLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountToDomainAliasLink] (
	[idAccountToDomainAliasLink] [int] IDENTITY (1, 1) NOT NULL,
	[idAccount] [int] NOT NULL,
	[hostname] [nvarchar] (255) NULL,
	[domain] [nvarchar] (255) NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountToDomainAliasLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountToDomainAliasLink ADD CONSTRAINT [PK_AccountToDomainAliasLink] PRIMARY KEY CLUSTERED (idAccountToDomainAliasLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountToDomainAliasLink_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountToDomainAliasLink ADD CONSTRAINT [FK_AccountToDomainAliasLink_Account] FOREIGN KEY (idAccount) REFERENCES [tblAccount] (idAccount)	
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountUser] (
	[idAccountUser] [int] IDENTITY (2, 1) NOT NULL,
	[idAccount] [int] NOT NULL,
	[firstName] [nvarchar] (255) NOT NULL,
	[lastName] [nvarchar] (255) NOT NULL,
	[displayName] [nvarchar] (512) NOT NULL,
	[email] [nvarchar] (255) NULL,
	[username] [nvarchar] (512) NOT NULL,
	[password] [nvarchar] (512) NOT NULL,
	[idRole] [int] NOT NULL,
	[isActive] [bit] NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountUser ADD CONSTRAINT [PK_AccountUser] PRIMARY KEY CLUSTERED (idAccountUser ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountUser_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountUser ADD CONSTRAINT [FK_AccountUser_Account] FOREIGN KEY (idAccount) REFERENCES [tblAccount] (idAccount)	
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAccountUser]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAccountUser]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblAccountUser (
	displayName,
	email,
	username
)
KEY INDEX PK_AccountUser
ON [Asentia-CustomerManager] -- insert index to this catalog

/**
INSERT MASTER ADMINISTRATOR ACCOUNT USER
**/
SET IDENTITY_INSERT tblAccountUser ON

UPDATE tblAccountUser SET
	idAccount = 1,
	firstName = 'ICS Learning Group',
	lastName = 'Administrator',
	displayName = 'Administrator, ICS Learning Group',
	email = 'tech@icslearninggroup.com',
	username = 'administrator',
	idRole = 1,
	isActive = 1
WHERE idAccountUser = 1

INSERT INTO tblAccountUser (
	idAccountUser,
	idAccount,
	firstName,
	lastName,
	displayName,
	email,
	username,
	[password],
	idRole,
	isActive
) 
SELECT
	1,
	1,
	'ICS Learning Group',
	'Administrator',
	'Administrator, ICS Learning Group',
	'tech@icslearninggroup.com',
	'administrator',
	dbo.GetHashedString('SHA1', '#fr4nt1c!'),
	idRole = 1,
	isActive = 1
WHERE NOT EXISTS (
	SELECT 1
	FROM tblAccountUser
	WHERE idAccountUser = 1
)

SET IDENTITY_INSERT tblAccountUser OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRole]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRole] (
	[idRole] [int] IDENTITY (1, 1) NOT NULL, 
	[roleName] [nvarchar] (255) NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRole ADD CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED (idRole ASC)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRole]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRole]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRole (
	roleName
)
KEY INDEX PK_Role
ON [Asentia-CustomerManager] -- insert index to this catalog

/** 
INSERT ROLES 
**/
SET IDENTITY_INSERT tblRole ON

UPDATE tblRole SET roleName = 'System User' WHERE idRole = 1
UPDATE tblRole SET roleName = 'Account User' WHERE idRole = 2

INSERT INTO tblRole 
(
	idRole, 
	roleName
) 
SELECT
	1,
	'System User'
WHERE NOT EXISTS (
	SELECT 1
	FROM tblRole
	WHERE idRole = 1
)

INSERT INTO tblRole 
(
	idRole, 
	roleName
) 
SELECT
	2,
	'Account User'
WHERE NOT EXISTS (
	SELECT 1
	FROM tblRole
	WHERE idRole = 2
)

SET IDENTITY_INSERT tblRole OFF
	
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLanguage] (
	[idLanguage]		INT				IDENTITY (1, 1)		NOT NULL, 
	[code]				NVARCHAR(20)						NOT NULL,
	[name]				NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLanguage ADD CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED (idLanguage ASC)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblLanguage] ON

INSERT INTO [tblLanguage] (
	[idLanguage], 
	[code],
	[name]
)
SELECT
	idLanguage,
	code,
	name
FROM (
	SELECT 0 AS idLanguage,'af-ZA' AS code,'Afrikaans (South Africa)' AS name
	UNION SELECT 1 AS idLanguage,'am-ET' AS code,'Amharic (Ethiopia)' AS name
	UNION SELECT 2 AS idLanguage,'ar-AE' AS code,'Arabic (U.A.E.)' AS name
	UNION SELECT 3 AS idLanguage,'ar-BH' AS code,'Arabic (Bahrain)' AS name
	UNION SELECT 4 AS idLanguage,'ar-DZ' AS code,'Arabic (Algeria)' AS name
	UNION SELECT 5 AS idLanguage,'ar-EG' AS code,'Arabic (Egypt)' AS name
	UNION SELECT 6 AS idLanguage,'ar-IQ' AS code,'Arabic (Iraq)' AS name
	UNION SELECT 7 AS idLanguage,'ar-JO' AS code,'Arabic (Jordan)' AS name
	UNION SELECT 8 AS idLanguage,'ar-KW' AS code,'Arabic (Kuwait)' AS name
	UNION SELECT 9 AS idLanguage,'ar-LB' AS code,'Arabic (Lebanon)' AS name
	UNION SELECT 10 AS idLanguage,'ar-LY' AS code,'Arabic (Libya)' AS name
	UNION SELECT 11 AS idLanguage,'ar-MA' AS code,'Arabic (Morocco)' AS name
	UNION SELECT 12 AS idLanguage,'ar-OM' AS code,'Arabic (Oman)' AS name
	UNION SELECT 13 AS idLanguage,'ar-QA' AS code,'Arabic (Qatar)' AS name
	UNION SELECT 14 AS idLanguage,'ar-SA' AS code,'Arabic (Saudi Arabia)' AS name
	UNION SELECT 15 AS idLanguage,'ar-SY' AS code,'Arabic (Syria)' AS name
	UNION SELECT 16 AS idLanguage,'ar-TN' AS code,'Arabic (Tunisia)' AS name
	UNION SELECT 17 AS idLanguage,'ar-YE' AS code,'Arabic (Yemen)' AS name
	UNION SELECT 18 AS idLanguage,'arn-CL' AS code,'Mapudungun (Chile)' AS name
	UNION SELECT 19 AS idLanguage,'as-IN' AS code,'Assamese (India)' AS name
	UNION SELECT 20 AS idLanguage,'az-Cyrl-AZ' AS code,'Azeri (Cyrillic, Azerbaijan)' AS name
	UNION SELECT 21 AS idLanguage,'az-Latn-AZ' AS code,'Azeri (Latin, Azerbaijan)' AS name
	UNION SELECT 22 AS idLanguage,'ba-RU' AS code,'Bashkir (Russia)' AS name
	UNION SELECT 23 AS idLanguage,'be-BY' AS code,'Belarusian (Belarus)' AS name
	UNION SELECT 24 AS idLanguage,'bg-BG' AS code,'Bulgarian (Bulgaria)' AS name
	UNION SELECT 25 AS idLanguage,'bn-BD' AS code,'Bengali (Bangladesh)' AS name
	UNION SELECT 26 AS idLanguage,'bn-IN' AS code,'Bengali (India)' AS name
	UNION SELECT 27 AS idLanguage,'bo-CN' AS code,'Tibetan (PRC)' AS name
	UNION SELECT 28 AS idLanguage,'br-FR' AS code,'Breton (France)' AS name
	UNION SELECT 29 AS idLanguage,'bs-Cyrl-BA' AS code,'Bosnian (Cyrillic, Bosnia and Herzegovina)' AS name
	UNION SELECT 30 AS idLanguage,'bs-Latn-BA' AS code,'Bosnian (Latin, Bosnia and Herzegovina)' AS name
	UNION SELECT 31 AS idLanguage,'ca-ES' AS code,'Catalan (Catalan)' AS name
	UNION SELECT 32 AS idLanguage,'co-FR' AS code,'Corsican (France)' AS name
	UNION SELECT 33 AS idLanguage,'cs-CZ' AS code,'Czech (Czech Republic)' AS name
	UNION SELECT 34 AS idLanguage,'cy-GB' AS code,'Welsh (United Kingdom)' AS name
	UNION SELECT 35 AS idLanguage,'da-DK' AS code,'Danish (Denmark)' AS name
	UNION SELECT 36 AS idLanguage,'de-AT' AS code,'German (Austria)' AS name
	UNION SELECT 37 AS idLanguage,'de-CH' AS code,'German (Switzerland)' AS name
	UNION SELECT 38 AS idLanguage,'de-DE' AS code,'German (Germany)' AS name
	UNION SELECT 39 AS idLanguage,'de-LI' AS code,'German (Liechtenstein)' AS name
	UNION SELECT 40 AS idLanguage,'de-LU' AS code,'German (Luxembourg)' AS name
	UNION SELECT 41 AS idLanguage,'dsb-DE' AS code,'Lower Sorbian (Germany)' AS name
	UNION SELECT 42 AS idLanguage,'dv-MV' AS code,'Divehi (Maldives)' AS name
	UNION SELECT 43 AS idLanguage,'el-GR' AS code,'Greek (Greece)' AS name
	UNION SELECT 44 AS idLanguage,'en-029' AS code,'English (Caribbean)' AS name
	UNION SELECT 45 AS idLanguage,'en-AU' AS code,'English (Australia)' AS name
	UNION SELECT 46 AS idLanguage,'en-BZ' AS code,'English (Belize)' AS name
	UNION SELECT 47 AS idLanguage,'en-CA' AS code,'English (Canada)' AS name
	UNION SELECT 48 AS idLanguage,'en-GB' AS code,'English (United Kingdom)' AS name
	UNION SELECT 49 AS idLanguage,'en-IE' AS code,'English (Ireland)' AS name
	UNION SELECT 50 AS idLanguage,'en-IN' AS code,'English (India)' AS name
	UNION SELECT 51 AS idLanguage,'en-JM' AS code,'English (Jamaica)' AS name
	UNION SELECT 52 AS idLanguage,'en-MY' AS code,'English (Malaysia)' AS name
	UNION SELECT 53 AS idLanguage,'en-NZ' AS code,'English (New Zealand)' AS name
	UNION SELECT 54 AS idLanguage,'en-PH' AS code,'English (Republic of the Philippines)' AS name
	UNION SELECT 55 AS idLanguage,'en-SG' AS code,'English (Singapore)' AS name
	UNION SELECT 56 AS idLanguage,'en-TT' AS code,'English (Trinidad and Tobago)' AS name
	UNION SELECT 57 AS idLanguage,'en-US' AS code,'English (United States)' AS name
	UNION SELECT 58 AS idLanguage,'en-ZA' AS code,'English (South Africa)' AS name
	UNION SELECT 59 AS idLanguage,'en-ZW' AS code,'English (Zimbabwe)' AS name
	UNION SELECT 60 AS idLanguage,'es-AR' AS code,'Spanish (Argentina)' AS name
	UNION SELECT 61 AS idLanguage,'es-BO' AS code,'Spanish (Bolivia)' AS name
	UNION SELECT 62 AS idLanguage,'es-CL' AS code,'Spanish (Chile)' AS name
	UNION SELECT 63 AS idLanguage,'es-CO' AS code,'Spanish (Colombia)' AS name
	UNION SELECT 64 AS idLanguage,'es-CR' AS code,'Spanish (Costa Rica)' AS name
	UNION SELECT 65 AS idLanguage,'es-DO' AS code,'Spanish (Dominican Republic)' AS name
	UNION SELECT 66 AS idLanguage,'es-EC' AS code,'Spanish (Ecuador)' AS name
	UNION SELECT 67 AS idLanguage,'es-ES' AS code,'Spanish (Spain, International Sort)' AS name
	UNION SELECT 68 AS idLanguage,'es-GT' AS code,'Spanish (Guatemala)' AS name
	UNION SELECT 69 AS idLanguage,'es-HN' AS code,'Spanish (Honduras)' AS name
	UNION SELECT 70 AS idLanguage,'es-MX' AS code,'Spanish (Mexico)' AS name
	UNION SELECT 71 AS idLanguage,'es-NI' AS code,'Spanish (Nicaragua)' AS name
	UNION SELECT 72 AS idLanguage,'es-PA' AS code,'Spanish (Panama)' AS name
	UNION SELECT 73 AS idLanguage,'es-PE' AS code,'Spanish (Peru)' AS name
	UNION SELECT 74 AS idLanguage,'es-PR' AS code,'Spanish (Puerto Rico)' AS name
	UNION SELECT 75 AS idLanguage,'es-PY' AS code,'Spanish (Paraguay)' AS name
	UNION SELECT 76 AS idLanguage,'es-SV' AS code,'Spanish (El Salvador)' AS name
	UNION SELECT 77 AS idLanguage,'es-US' AS code,'Spanish (United States)' AS name
	UNION SELECT 78 AS idLanguage,'es-UY' AS code,'Spanish (Uruguay)' AS name
	UNION SELECT 79 AS idLanguage,'es-VE' AS code,'Spanish (Bolivarian Republic of Venezuela)' AS name
	UNION SELECT 80 AS idLanguage,'et-EE' AS code,'Estonian (Estonia)' AS name
	UNION SELECT 81 AS idLanguage,'eu-ES' AS code,'Basque (Basque)' AS name
	UNION SELECT 82 AS idLanguage,'fa-IR' AS code,'Persian' AS name
	UNION SELECT 83 AS idLanguage,'fi-FI' AS code,'Finnish (Finland)' AS name
	UNION SELECT 84 AS idLanguage,'fil-PH' AS code,'Filipino (Philippines)' AS name
	UNION SELECT 85 AS idLanguage,'fo-FO' AS code,'Faroese (Faroe Islands)' AS name
	UNION SELECT 86 AS idLanguage,'fr-BE' AS code,'French (Belgium)' AS name
	UNION SELECT 87 AS idLanguage,'fr-CA' AS code,'French (Canada)' AS name
	UNION SELECT 88 AS idLanguage,'fr-CH' AS code,'French (Switzerland)' AS name
	UNION SELECT 89 AS idLanguage,'fr-FR' AS code,'French (France)' AS name
	UNION SELECT 90 AS idLanguage,'fr-LU' AS code,'French (Luxembourg)' AS name
	UNION SELECT 91 AS idLanguage,'fr-MC' AS code,'French (Monaco)' AS name
	UNION SELECT 92 AS idLanguage,'fy-NL' AS code,'Frisian (Netherlands)' AS name
	UNION SELECT 93 AS idLanguage,'ga-IE' AS code,'Irish (Ireland)' AS name
	UNION SELECT 94 AS idLanguage,'gd-GB' AS code,'Scottish Gaelic (United Kingdom)' AS name
	UNION SELECT 95 AS idLanguage,'gl-ES' AS code,'Galician (Galician)' AS name
	UNION SELECT 96 AS idLanguage,'gsw-FR' AS code,'Alsatian (France)' AS name
	UNION SELECT 97 AS idLanguage,'gu-IN' AS code,'Gujarati (India)' AS name
	UNION SELECT 98 AS idLanguage,'ha-Latn-NG' AS code,'Hausa (Latin, Nigeria)' AS name
	UNION SELECT 99 AS idLanguage,'he-IL' AS code,'Hebrew (Israel)' AS name
	UNION SELECT 100 AS idLanguage,'hi-IN' AS code,'Hindi (India)' AS name
	UNION SELECT 101 AS idLanguage,'hr-BA' AS code,'Croatian (Latin, Bosnia and Herzegovina)' AS name
	UNION SELECT 102 AS idLanguage,'hr-HR' AS code,'Croatian (Croatia)' AS name
	UNION SELECT 103 AS idLanguage,'hsb-DE' AS code,'Upper Sorbian (Germany)' AS name
	UNION SELECT 104 AS idLanguage,'hu-HU' AS code,'Hungarian (Hungary)' AS name
	UNION SELECT 105 AS idLanguage,'hy-AM' AS code,'Armenian (Armenia)' AS name
	UNION SELECT 106 AS idLanguage,'id-ID' AS code,'Indonesian (Indonesia)' AS name
	UNION SELECT 107 AS idLanguage,'ig-NG' AS code,'Igbo (Nigeria)' AS name
	UNION SELECT 108 AS idLanguage,'ii-CN' AS code,'Yi (PRC)' AS name
	UNION SELECT 109 AS idLanguage,'is-IS' AS code,'Icelandic (Iceland)' AS name
	UNION SELECT 110 AS idLanguage,'it-CH' AS code,'Italian (Switzerland)' AS name
	UNION SELECT 111 AS idLanguage,'it-IT' AS code,'Italian (Italy)' AS name
	UNION SELECT 112 AS idLanguage,'iu-Cans-CA' AS code,'Inuktitut (Syllabics, Canada)' AS name
	UNION SELECT 113 AS idLanguage,'iu-Latn-CA' AS code,'Inuktitut (Latin, Canada)' AS name
	UNION SELECT 114 AS idLanguage,'ja-JP' AS code,'Japanese (Japan)' AS name
	UNION SELECT 115 AS idLanguage,'ka-GE' AS code,'Georgian (Georgia)' AS name
	UNION SELECT 116 AS idLanguage,'kk-KZ' AS code,'Kazakh (Kazakhstan)' AS name
	UNION SELECT 117 AS idLanguage,'kl-GL' AS code,'Greenlandic (Greenland)' AS name
	UNION SELECT 118 AS idLanguage,'km-KH' AS code,'Khmer (Cambodia)' AS name
	UNION SELECT 119 AS idLanguage,'kn-IN' AS code,'Kannada (India)' AS name
	UNION SELECT 120 AS idLanguage,'ko-KR' AS code,'Korean (Korea)' AS name
	UNION SELECT 121 AS idLanguage,'kok-IN' AS code,'Konkani (India)' AS name
	UNION SELECT 122 AS idLanguage,'ky-KG' AS code,'Kyrgyz (Kyrgyzstan)' AS name
	UNION SELECT 123 AS idLanguage,'lb-LU' AS code,'Luxembourgish (Luxembourg)' AS name
	UNION SELECT 124 AS idLanguage,'lo-LA' AS code,'Lao (Lao P.D.R.)' AS name
	UNION SELECT 125 AS idLanguage,'lt-LT' AS code,'Lithuanian (Lithuania)' AS name
	UNION SELECT 126 AS idLanguage,'lv-LV' AS code,'Latvian (Latvia)' AS name
	UNION SELECT 127 AS idLanguage,'mi-NZ' AS code,'Maori (New Zealand)' AS name
	UNION SELECT 128 AS idLanguage,'mk-MK' AS code,'Macedonian (Former Yugoslav Republic of Macedonia)' AS name
	UNION SELECT 129 AS idLanguage,'ml-IN' AS code,'Malayalam (India)' AS name
	UNION SELECT 130 AS idLanguage,'mn-MN' AS code,'Mongolian (Cyrillic, Mongolia)' AS name
	UNION SELECT 131 AS idLanguage,'mn-Mong-CN' AS code,'Mongolian (Traditional Mongolian, PRC)' AS name
	UNION SELECT 132 AS idLanguage,'moh-CA' AS code,'Mohawk (Mohawk)' AS name
	UNION SELECT 133 AS idLanguage,'mr-IN' AS code,'Marathi (India)' AS name
	UNION SELECT 134 AS idLanguage,'ms-BN' AS code,'Malay (Brunei Darussalam)' AS name
	UNION SELECT 135 AS idLanguage,'ms-MY' AS code,'Malay (Malaysia)' AS name
	UNION SELECT 136 AS idLanguage,'mt-MT' AS code,'Maltese (Malta)' AS name
	UNION SELECT 137 AS idLanguage,'nb-NO' AS code,'Norwegian, Bokm�l (Norway)' AS name
	UNION SELECT 138 AS idLanguage,'ne-NP' AS code,'Nepali (Nepal)' AS name
	UNION SELECT 139 AS idLanguage,'nl-BE' AS code,'Dutch (Belgium)' AS name
	UNION SELECT 140 AS idLanguage,'nl-NL' AS code,'Dutch (Netherlands)' AS name
	UNION SELECT 141 AS idLanguage,'nn-NO' AS code,'Norwegian, Nynorsk (Norway)' AS name
	UNION SELECT 142 AS idLanguage,'nso-ZA' AS code,'Sesotho sa Leboa (South Africa)' AS name
	UNION SELECT 143 AS idLanguage,'oc-FR' AS code,'Occitan (France)' AS name
	UNION SELECT 144 AS idLanguage,'or-IN' AS code,'Oriya (India)' AS name
	UNION SELECT 145 AS idLanguage,'pa-IN' AS code,'Punjabi (India)' AS name
	UNION SELECT 146 AS idLanguage,'pl-PL' AS code,'Polish (Poland)' AS name
	UNION SELECT 147 AS idLanguage,'prs-AF' AS code,'Dari (Afghanistan)' AS name
	UNION SELECT 148 AS idLanguage,'ps-AF' AS code,'Pashto (Afghanistan)' AS name
	UNION SELECT 149 AS idLanguage,'pt-BR' AS code,'Portuguese (Brazil)' AS name
	UNION SELECT 150 AS idLanguage,'pt-PT' AS code,'Portuguese (Portugal)' AS name
	UNION SELECT 151 AS idLanguage,'qut-GT' AS code,'K''iche (Guatemala)' AS name
	UNION SELECT 152 AS idLanguage,'quz-BO' AS code,'Quechua (Bolivia)' AS name
	UNION SELECT 153 AS idLanguage,'quz-EC' AS code,'Quechua (Ecuador)' AS name
	UNION SELECT 154 AS idLanguage,'quz-PE' AS code,'Quechua (Peru)' AS name
	UNION SELECT 155 AS idLanguage,'rm-CH' AS code,'Romansh (Switzerland)' AS name
	UNION SELECT 156 AS idLanguage,'ro-RO' AS code,'Romanian (Romania)' AS name
	UNION SELECT 157 AS idLanguage,'ru-RU' AS code,'Russian (Russia)' AS name
	UNION SELECT 158 AS idLanguage,'rw-RW' AS code,'Kinyarwanda (Rwanda)' AS name
	UNION SELECT 159 AS idLanguage,'sa-IN' AS code,'Sanskrit (India)' AS name
	UNION SELECT 160 AS idLanguage,'sah-RU' AS code,'Yakut (Russia)' AS name
	UNION SELECT 161 AS idLanguage,'se-FI' AS code,'Sami, Northern (Finland)' AS name
	UNION SELECT 162 AS idLanguage,'se-NO' AS code,'Sami, Northern (Norway)' AS name
	UNION SELECT 163 AS idLanguage,'se-SE' AS code,'Sami, Northern (Sweden)' AS name
	UNION SELECT 164 AS idLanguage,'si-LK' AS code,'Sinhala (Sri Lanka)' AS name
	UNION SELECT 165 AS idLanguage,'sk-SK' AS code,'Slovak (Slovakia)' AS name
	UNION SELECT 166 AS idLanguage,'sl-SI' AS code,'Slovenian (Slovenia)' AS name
	UNION SELECT 167 AS idLanguage,'sma-NO' AS code,'Sami, Southern (Norway)' AS name
	UNION SELECT 168 AS idLanguage,'sma-SE' AS code,'Sami, Southern (Sweden)' AS name
	UNION SELECT 169 AS idLanguage,'smj-NO' AS code,'Sami, Lule (Norway)' AS name
	UNION SELECT 170 AS idLanguage,'smj-SE' AS code,'Sami, Lule (Sweden)' AS name
	UNION SELECT 171 AS idLanguage,'smn-FI' AS code,'Sami, Inari (Finland)' AS name
	UNION SELECT 172 AS idLanguage,'sms-FI' AS code,'Sami, Skolt (Finland)' AS name
	UNION SELECT 173 AS idLanguage,'sq-AL' AS code,'Albanian (Albania)' AS name
	UNION SELECT 174 AS idLanguage,'sr-Cyrl-BA' AS code,'Serbian (Cyrillic, Bosnia and Herzegovina)' AS name
	UNION SELECT 175 AS idLanguage,'sr-Cyrl-CS' AS code,'Serbian (Cyrillic, Serbia and Montenegro (Former))' AS name
	UNION SELECT 176 AS idLanguage,'sr-Cyrl-ME' AS code,'Serbian (Cyrillic, Montenegro)' AS name
	UNION SELECT 177 AS idLanguage,'sr-Cyrl-RS' AS code,'Serbian (Cyrillic, Serbia)' AS name
	UNION SELECT 178 AS idLanguage,'sr-Latn-BA' AS code,'Serbian (Latin, Bosnia and Herzegovina)' AS name
	UNION SELECT 179 AS idLanguage,'sr-Latn-CS' AS code,'Serbian (Latin, Serbia and Montenegro (Former))' AS name
	UNION SELECT 180 AS idLanguage,'sr-Latn-ME' AS code,'Serbian (Latin, Montenegro)' AS name
	UNION SELECT 181 AS idLanguage,'sr-Latn-RS' AS code,'Serbian (Latin, Serbia)' AS name
	UNION SELECT 182 AS idLanguage,'sv-FI' AS code,'Swedish (Finland)' AS name
	UNION SELECT 183 AS idLanguage,'sv-SE' AS code,'Swedish (Sweden)' AS name
	UNION SELECT 184 AS idLanguage,'sw-KE' AS code,'Kiswahili (Kenya)' AS name
	UNION SELECT 185 AS idLanguage,'syr-SY' AS code,'Syriac (Syria)' AS name
	UNION SELECT 186 AS idLanguage,'ta-IN' AS code,'Tamil (India)' AS name
	UNION SELECT 187 AS idLanguage,'te-IN' AS code,'Telugu (India)' AS name
	UNION SELECT 188 AS idLanguage,'tg-Cyrl-TJ' AS code,'Tajik (Cyrillic, Tajikistan)' AS name
	UNION SELECT 189 AS idLanguage,'th-TH' AS code,'Thai (Thailand)' AS name
	UNION SELECT 190 AS idLanguage,'tk-TM' AS code,'Turkmen (Turkmenistan)' AS name
	UNION SELECT 191 AS idLanguage,'tn-ZA' AS code,'Setswana (South Africa)' AS name
	UNION SELECT 192 AS idLanguage,'tr-TR' AS code,'Turkish (Turkey)' AS name
	UNION SELECT 193 AS idLanguage,'tt-RU' AS code,'Tatar (Russia)' AS name
	UNION SELECT 194 AS idLanguage,'tzm-Latn-DZ' AS code,'Tamazight (Latin, Algeria)' AS name
	UNION SELECT 195 AS idLanguage,'ug-CN' AS code,'Uyghur (PRC)' AS name
	UNION SELECT 196 AS idLanguage,'uk-UA' AS code,'Ukrainian (Ukraine)' AS name
	UNION SELECT 197 AS idLanguage,'ur-PK' AS code,'Urdu (Islamic Republic of Pakistan)' AS name
	UNION SELECT 198 AS idLanguage,'uz-Cyrl-UZ' AS code,'Uzbek (Cyrillic, Uzbekistan)' AS name
	UNION SELECT 199 AS idLanguage,'uz-Latn-UZ' AS code,'Uzbek (Latin, Uzbekistan)' AS name
	UNION SELECT 200 AS idLanguage,'vi-VN' AS code,'Vietnamese (Vietnam)' AS name
	UNION SELECT 201 AS idLanguage,'wo-SN' AS code,'Wolof (Senegal)' AS name
	UNION SELECT 202 AS idLanguage,'xh-ZA' AS code,'isiXhosa (South Africa)' AS name
	UNION SELECT 203 AS idLanguage,'yo-NG' AS code,'Yoruba (Nigeria)' AS name
	UNION SELECT 204 AS idLanguage,'zh-CN' AS code,'Chinese (Simplified, PRC)' AS name
	UNION SELECT 205 AS idLanguage,'zh-HK' AS code,'Chinese (Traditional, Hong Kong S.A.R.)' AS name
	UNION SELECT 206 AS idLanguage,'zh-MO' AS code,'Chinese (Traditional, Macao S.A.R.)' AS name
	UNION SELECT 207 AS idLanguage,'zh-SG' AS code,'Chinese (Simplified, Singapore)' AS name
	UNION SELECT 208 AS idLanguage,'zh-TW' AS code,'Chinese (Traditional, Taiwan)' AS name
	UNION SELECT 209 AS idLanguage,'zu-ZA' AS code,'isiZulu (South Africa)' AS name

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblLanguage L WHERE L.idLanguage = MAIN.idLanguage)

SET IDENTITY_INSERT [tblLanguage] OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTimezone]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTimezone] (
	[idTimezone]					INT				IDENTITY(1,1)	NOT NULL,
	[dotNetName]					NVARCHAR(255)	UNIQUE			NOT NULL,
	[displayName]					NVARCHAR(255)					NOT NULL,
	[gmtOffset]						FLOAT							NOT NULL,
	[blnUseDaylightSavings]			BIT								NOT NULL,
	[order]							INT								NULL,
	[isEnabled]						BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezone ADD CONSTRAINT [PK_Timezone] PRIMARY KEY CLUSTERED (idTimezone ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT tblTimezone ON

INSERT INTO tblTimezone (
	idTimezone, 
	dotNetName, 
	displayName, 
	gmtOffset, 
	blnUseDaylightSavings, 
	[order], 
	isEnabled
)
SELECT
	idTimezone, 
	dotNetName, 
	displayName, 
	gmtOffset, 
	blnUseDaylightSavings, 
	[order], 
	isEnabled
FROM (
	SELECT 1 AS idTimezone, 'Dateline Standard Time' AS dotNetName, '(GMT-12:00) International Date Line West' AS displayName, -12 AS gmtOffset, 0 AS blnUseDaylightSavings, 1 AS [order], 1 AS isEnabled
	UNION SELECT 2 AS idTimezone, 'UTC-11' AS dotNetName, '(GMT-11:00) Coordinated Universal Time-11' AS displayName, -11 AS gmtOffset, 0 AS blnUseDaylightSavings, 2 AS [order], 1 AS isEnabled
	UNION SELECT 3 AS idTimezone, 'Hawaiian Standard Time' AS dotNetName, '(GMT-10:00) Hawaii' AS displayName, -10 AS gmtOffset, 0 AS blnUseDaylightSavings, 3 AS [order], 1 AS isEnabled
	UNION SELECT 4 AS idTimezone, 'Alaskan Standard Time' AS dotNetName, '(GMT-09:00) Alaska' AS displayName, -9 AS gmtOffset, 1 AS blnUseDaylightSavings, 4 AS [order], 1 AS isEnabled
	UNION SELECT 5 AS idTimezone, 'Pacific Standard Time (Mexico)' AS dotNetName, '(GMT-08:00) Baja California' AS displayName, -8 AS gmtOffset, 1 AS blnUseDaylightSavings, 5 AS [order], 1 AS isEnabled
	UNION SELECT 6 AS idTimezone, 'Pacific Standard Time' AS dotNetName, '(GMT-08:00) Pacific Time (US & Canada)' AS displayName, -8 AS gmtOffset, 1 AS blnUseDaylightSavings, 6 AS [order], 1 AS isEnabled
	UNION SELECT 7 AS idTimezone, 'US Mountain Standard Time' AS dotNetName, '(GMT-07:00) Arizona' AS displayName, -7 AS gmtOffset, 0 AS blnUseDaylightSavings, 7 AS [order], 1 AS isEnabled
	UNION SELECT 8 AS idTimezone, 'Mountain Standard Time (Mexico)' AS dotNetName, '(GMT-07:00) Chihuahua, La Paz, Mazatlan' AS displayName, -7 AS gmtOffset, 1 AS blnUseDaylightSavings, 8 AS [order], 1 AS isEnabled
	UNION SELECT 9 AS idTimezone, 'Mountain Standard Time' AS dotNetName, '(GMT-07:00) Mountain Time (US & Canada)' AS displayName, -7 AS gmtOffset, 1 AS blnUseDaylightSavings, 9 AS [order], 1 AS isEnabled
	UNION SELECT 10 AS idTimezone, 'Central America Standard Time' AS dotNetName, '(GMT-06:00) Central America' AS displayName, -6 AS gmtOffset, 0 AS blnUseDaylightSavings, 10 AS [order], 1 AS isEnabled
	UNION SELECT 11 AS idTimezone, 'Central Standard Time' AS dotNetName, '(GMT-06:00) Central Time (US & Canada)' AS displayName, -6 AS gmtOffset, 1 AS blnUseDaylightSavings, 11 AS [order], 1 AS isEnabled
	UNION SELECT 12 AS idTimezone, 'Central Standard Time (Mexico)' AS dotNetName, '(GMT-06:00) Guadalajara, Mexico City, Monterrey' AS displayName, -6 AS gmtOffset, 1 AS blnUseDaylightSavings, 12 AS [order], 1 AS isEnabled
	UNION SELECT 13 AS idTimezone, 'Canada Central Standard Time' AS dotNetName, '(GMT-06:00) Saskatchewan' AS displayName, -6 AS gmtOffset, 0 AS blnUseDaylightSavings, 13 AS [order], 1 AS isEnabled
	UNION SELECT 14 AS idTimezone, 'SA Pacific Standard Time' AS dotNetName, '(GMT-05:00) Bogota, Lima, Quito' AS displayName, -5 AS gmtOffset, 0 AS blnUseDaylightSavings, 14 AS [order], 1 AS isEnabled
	UNION SELECT 15 AS idTimezone, 'Eastern Standard Time' AS dotNetName, '(GMT-05:00) Eastern Time (US & Canada)' AS displayName, -5 AS gmtOffset, 1 AS blnUseDaylightSavings, 15 AS [order], 1 AS isEnabled
	UNION SELECT 16 AS idTimezone, 'US Eastern Standard Time' AS dotNetName, '(GMT-05:00) Indiana (East)' AS displayName, -5 AS gmtOffset, 1 AS blnUseDaylightSavings, 16 AS [order], 1 AS isEnabled
	UNION SELECT 17 AS idTimezone, 'Venezuela Standard Time' AS dotNetName, '(GMT-04:30) Caracas' AS displayName, -4.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 17 AS [order], 1 AS isEnabled
	UNION SELECT 18 AS idTimezone, 'Paraguay Standard Time' AS dotNetName, '(GMT-04:00) Asuncion' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 18 AS [order], 1 AS isEnabled
	UNION SELECT 19 AS idTimezone, 'Atlantic Standard Time' AS dotNetName, '(GMT-04:00) Atlantic Time (Canada)' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 19 AS [order], 1 AS isEnabled
	UNION SELECT 20 AS idTimezone, 'Central Brazilian Standard Time' AS dotNetName, '(GMT-04:00) Cuiaba' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 20 AS [order], 1 AS isEnabled
	UNION SELECT 21 AS idTimezone, 'SA Western Standard Time' AS dotNetName, '(GMT-04:00) Georgetown, La Paz, Manaus, San Juan' AS displayName, -4 AS gmtOffset, 0 AS blnUseDaylightSavings, 21 AS [order], 1 AS isEnabled
	UNION SELECT 22 AS idTimezone, 'Pacific SA Standard Time' AS dotNetName, '(GMT-04:00) Santiago' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 22 AS [order], 1 AS isEnabled
	UNION SELECT 23 AS idTimezone, 'Newfoundland Standard Time' AS dotNetName, '(GMT-03:30) Newfoundland' AS displayName, -3.5 AS gmtOffset, 1 AS blnUseDaylightSavings, 23 AS [order], 1 AS isEnabled
	UNION SELECT 24 AS idTimezone, 'E. South America Standard Time' AS dotNetName, '(GMT-03:00) Brasilia' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 24 AS [order], 1 AS isEnabled
	UNION SELECT 25 AS idTimezone, 'Argentina Standard Time' AS dotNetName, '(GMT-03:00) Buenos Aires' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 25 AS [order], 1 AS isEnabled
	UNION SELECT 26 AS idTimezone, 'SA Eastern Standard Time' AS dotNetName, '(GMT-03:00) Cayenne, Fortaleza' AS displayName, -3 AS gmtOffset, 0 AS blnUseDaylightSavings, 26 AS [order], 1 AS isEnabled
	UNION SELECT 27 AS idTimezone, 'Greenland Standard Time' AS dotNetName, '(GMT-03:00) Greenland' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 27 AS [order], 1 AS isEnabled
	UNION SELECT 28 AS idTimezone, 'Montevideo Standard Time' AS dotNetName, '(GMT-03:00) Montevideo' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 28 AS [order], 1 AS isEnabled
	UNION SELECT 29 AS idTimezone, 'Bahia Standard Time' AS dotNetName, '(GMT-03:00) Salvador' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 29 AS [order], 1 AS isEnabled
	UNION SELECT 30 AS idTimezone, 'UTC-02' AS dotNetName, '(GMT-02:00) Coordinated Universal Time-02' AS displayName, -2 AS gmtOffset, 0 AS blnUseDaylightSavings, 30 AS [order], 1 AS isEnabled
	UNION SELECT 31 AS idTimezone, 'Mid-Atlantic Standard Time' AS dotNetName, '(GMT-02:00) Mid-Atlantic' AS displayName, -2 AS gmtOffset, 1 AS blnUseDaylightSavings, 31 AS [order], 1 AS isEnabled
	UNION SELECT 32 AS idTimezone, 'Azores Standard Time' AS dotNetName, '(GMT-01:00) Azores' AS displayName, -1 AS gmtOffset, 1 AS blnUseDaylightSavings, 32 AS [order], 1 AS isEnabled
	UNION SELECT 33 AS idTimezone, 'Cape Verde Standard Time' AS dotNetName, '(GMT-01:00) Cape Verde Is.' AS displayName, -1 AS gmtOffset, 0 AS blnUseDaylightSavings, 33 AS [order], 1 AS isEnabled
	UNION SELECT 34 AS idTimezone, 'Morocco Standard Time' AS dotNetName, '(GMT) Casablanca' AS displayName, 0 AS gmtOffset, 1 AS blnUseDaylightSavings, 34 AS [order], 1 AS isEnabled
	UNION SELECT 35 AS idTimezone, 'UTC' AS dotNetName, '(GMT) Coordinated Universal Time' AS displayName, 0 AS gmtOffset, 0 AS blnUseDaylightSavings, 35 AS [order], 1 AS isEnabled
	UNION SELECT 36 AS idTimezone, 'GMT Standard Time' AS dotNetName, '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London' AS displayName, 0 AS gmtOffset, 1 AS blnUseDaylightSavings, 36 AS [order], 1 AS isEnabled
	UNION SELECT 37 AS idTimezone, 'Greenwich Standard Time' AS dotNetName, '(GMT) Monrovia, Reykjavik' AS displayName, 0 AS gmtOffset, 0 AS blnUseDaylightSavings, 37 AS [order], 1 AS isEnabled
	UNION SELECT 38 AS idTimezone, 'W. Europe Standard Time' AS dotNetName, '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 38 AS [order], 1 AS isEnabled
	UNION SELECT 39 AS idTimezone, 'Central Europe Standard Time' AS dotNetName, '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 39 AS [order], 1 AS isEnabled
	UNION SELECT 40 AS idTimezone, 'Romance Standard Time' AS dotNetName, '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 40 AS [order], 1 AS isEnabled
	UNION SELECT 41 AS idTimezone, 'Central European Standard Time' AS dotNetName, '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 41 AS [order], 1 AS isEnabled
	UNION SELECT 42 AS idTimezone, 'Libya Standard Time' AS dotNetName, '(GMT+01:00) Tripoli' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 42 AS [order], 1 AS isEnabled
	UNION SELECT 43 AS idTimezone, 'W. Central Africa Standard Time' AS dotNetName, '(GMT+01:00) West Central Africa' AS displayName, 1 AS gmtOffset, 0 AS blnUseDaylightSavings, 43 AS [order], 1 AS isEnabled
	UNION SELECT 44 AS idTimezone, 'Namibia Standard Time' AS dotNetName, '(GMT+01:00) Windhoek' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 44 AS [order], 1 AS isEnabled
	UNION SELECT 45 AS idTimezone, 'GTB Standard Time' AS dotNetName, '(GMT+02:00) Athens, Bucharest' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 45 AS [order], 1 AS isEnabled
	UNION SELECT 46 AS idTimezone, 'Middle East Standard Time' AS dotNetName, '(GMT+02:00) Beirut' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 46 AS [order], 1 AS isEnabled
	UNION SELECT 47 AS idTimezone, 'Egypt Standard Time' AS dotNetName, '(GMT+02:00) Cairo' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 47 AS [order], 1 AS isEnabled
	UNION SELECT 48 AS idTimezone, 'Syria Standard Time' AS dotNetName, '(GMT+02:00) Damascus' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 48 AS [order], 1 AS isEnabled
	UNION SELECT 49 AS idTimezone, 'E. Europe Standard Time' AS dotNetName, '(GMT+02:00) E. Europe' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 49 AS [order], 1 AS isEnabled
	UNION SELECT 50 AS idTimezone, 'South Africa Standard Time' AS dotNetName, '(GMT+02:00) Harare, Pretoria' AS displayName, 2 AS gmtOffset, 0 AS blnUseDaylightSavings, 50 AS [order], 1 AS isEnabled
	UNION SELECT 51 AS idTimezone, 'FLE Standard Time' AS dotNetName, '(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 51 AS [order], 1 AS isEnabled
	UNION SELECT 52 AS idTimezone, 'Turkey Standard Time' AS dotNetName, '(GMT+02:00) Istanbul' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 52 AS [order], 1 AS isEnabled
	UNION SELECT 53 AS idTimezone, 'Israel Standard Time' AS dotNetName, '(GMT+02:00) Jerusalem' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 53 AS [order], 1 AS isEnabled
	UNION SELECT 54 AS idTimezone, 'Jordan Standard Time' AS dotNetName, '(GMT+03:00) Amman' AS displayName, 3 AS gmtOffset, 1 AS blnUseDaylightSavings, 54 AS [order], 1 AS isEnabled
	UNION SELECT 55 AS idTimezone, 'Arabic Standard Time' AS dotNetName, '(GMT+03:00) Baghdad' AS displayName, 3 AS gmtOffset, 1 AS blnUseDaylightSavings, 55 AS [order], 1 AS isEnabled
	UNION SELECT 56 AS idTimezone, 'Kaliningrad Standard Time' AS dotNetName, '(GMT+03:00) Kaliningrad, Minsk' AS displayName, 3 AS gmtOffset, 1 AS blnUseDaylightSavings, 56 AS [order], 1 AS isEnabled
	UNION SELECT 57 AS idTimezone, 'Arab Standard Time' AS dotNetName, '(GMT+03:00) Kuwait, Riyadh' AS displayName, 3 AS gmtOffset, 0 AS blnUseDaylightSavings, 57 AS [order], 1 AS isEnabled
	UNION SELECT 58 AS idTimezone, 'E. Africa Standard Time' AS dotNetName, '(GMT+03:00) Nairobi' AS displayName, 3 AS gmtOffset, 0 AS blnUseDaylightSavings, 58 AS [order], 1 AS isEnabled
	UNION SELECT 59 AS idTimezone, 'Iran Standard Time' AS dotNetName, '(GMT+03:30) Tehran' AS displayName, 3.5 AS gmtOffset, 1 AS blnUseDaylightSavings, 59 AS [order], 1 AS isEnabled
	UNION SELECT 60 AS idTimezone, 'Arabian Standard Time' AS dotNetName, '(GMT+04:00) Abu Dhabi, Muscat' AS displayName, 4 AS gmtOffset, 0 AS blnUseDaylightSavings, 60 AS [order], 1 AS isEnabled
	UNION SELECT 61 AS idTimezone, 'Azerbaijan Standard Time' AS dotNetName, '(GMT+04:00) Baku' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 61 AS [order], 1 AS isEnabled
	UNION SELECT 62 AS idTimezone, 'Russian Standard Time' AS dotNetName, '(GMT+04:00) Moscow, St. Petersburg, Volgograd' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 62 AS [order], 1 AS isEnabled
	UNION SELECT 63 AS idTimezone, 'Mauritius Standard Time' AS dotNetName, '(GMT+04:00) Port Louis' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 63 AS [order], 1 AS isEnabled
	UNION SELECT 64 AS idTimezone, 'Georgian Standard Time' AS dotNetName, '(GMT+04:00) Tbilisi' AS displayName, 4 AS gmtOffset, 0 AS blnUseDaylightSavings, 64 AS [order], 1 AS isEnabled
	UNION SELECT 65 AS idTimezone, 'Caucasus Standard Time' AS dotNetName, '(GMT+04:00) Yerevan' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 65 AS [order], 1 AS isEnabled
	UNION SELECT 66 AS idTimezone, 'Afghanistan Standard Time' AS dotNetName, '(GMT+04:30) Kabul' AS displayName, 4.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 66 AS [order], 1 AS isEnabled
	UNION SELECT 67 AS idTimezone, 'West Asia Standard Time' AS dotNetName, '(GMT+05:00) Ashgabat, Tashkent' AS displayName, 5 AS gmtOffset, 0 AS blnUseDaylightSavings, 67 AS [order], 1 AS isEnabled
	UNION SELECT 68 AS idTimezone, 'Pakistan Standard Time' AS dotNetName, '(GMT+05:00) Islamabad, Karachi' AS displayName, 5 AS gmtOffset, 1 AS blnUseDaylightSavings, 68 AS [order], 1 AS isEnabled
	UNION SELECT 69 AS idTimezone, 'India Standard Time' AS dotNetName, '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi' AS displayName, 5.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 69 AS [order], 1 AS isEnabled
	UNION SELECT 70 AS idTimezone, 'Sri Lanka Standard Time' AS dotNetName, '(GMT+05:30) Sri Jayawardenepura' AS displayName, 5.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 70 AS [order], 1 AS isEnabled
	UNION SELECT 71 AS idTimezone, 'Nepal Standard Time' AS dotNetName, '(GMT+05:45) Kathmandu' AS displayName, 5.75 AS gmtOffset, 0 AS blnUseDaylightSavings, 71 AS [order], 1 AS isEnabled
	UNION SELECT 72 AS idTimezone, 'Central Asia Standard Time' AS dotNetName, '(GMT+06:00) Astana' AS displayName, 6 AS gmtOffset, 0 AS blnUseDaylightSavings, 72 AS [order], 1 AS isEnabled
	UNION SELECT 73 AS idTimezone, 'Bangladesh Standard Time' AS dotNetName, '(GMT+06:00) Dhaka' AS displayName, 6 AS gmtOffset, 1 AS blnUseDaylightSavings, 73 AS [order], 1 AS isEnabled
	UNION SELECT 74 AS idTimezone, 'Ekaterinburg Standard Time' AS dotNetName, '(GMT+06:00) Ekaterinburg' AS displayName, 6 AS gmtOffset, 1 AS blnUseDaylightSavings, 74 AS [order], 1 AS isEnabled
	UNION SELECT 75 AS idTimezone, 'Myanmar Standard Time' AS dotNetName, '(GMT+06:30) Yangon (Rangoon)' AS displayName, 6.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 75 AS [order], 1 AS isEnabled
	UNION SELECT 76 AS idTimezone, 'SE Asia Standard Time' AS dotNetName, '(GMT+07:00) Bangkok, Hanoi, Jakarta' AS displayName, 7 AS gmtOffset, 0 AS blnUseDaylightSavings, 76 AS [order], 1 AS isEnabled
	UNION SELECT 77 AS idTimezone, 'N. Central Asia Standard Time' AS dotNetName, '(GMT+07:00) Novosibirsk' AS displayName, 7 AS gmtOffset, 1 AS blnUseDaylightSavings, 77 AS [order], 1 AS isEnabled
	UNION SELECT 78 AS idTimezone, 'China Standard Time' AS dotNetName, '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 78 AS [order], 1 AS isEnabled
	UNION SELECT 79 AS idTimezone, 'North Asia Standard Time' AS dotNetName, '(GMT+08:00) Krasnoyarsk' AS displayName, 8 AS gmtOffset, 1 AS blnUseDaylightSavings, 79 AS [order], 1 AS isEnabled
	UNION SELECT 80 AS idTimezone, 'Singapore Standard Time' AS dotNetName, '(GMT+08:00) Kuala Lumpur, Singapore' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 80 AS [order], 1 AS isEnabled
	UNION SELECT 81 AS idTimezone, 'W. Australia Standard Time' AS dotNetName, '(GMT+08:00) Perth' AS displayName, 8 AS gmtOffset, 1 AS blnUseDaylightSavings, 81 AS [order], 1 AS isEnabled
	UNION SELECT 82 AS idTimezone, 'Taipei Standard Time' AS dotNetName, '(GMT+08:00) Taipei' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 82 AS [order], 1 AS isEnabled
	UNION SELECT 83 AS idTimezone, 'Ulaanbaatar Standard Time' AS dotNetName, '(GMT+08:00) Ulaanbaatar' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 83 AS [order], 1 AS isEnabled
	UNION SELECT 84 AS idTimezone, 'North Asia East Standard Time' AS dotNetName, '(GMT+09:00) Irkutsk' AS displayName, 9 AS gmtOffset, 1 AS blnUseDaylightSavings, 84 AS [order], 1 AS isEnabled
	UNION SELECT 85 AS idTimezone, 'Tokyo Standard Time' AS dotNetName, '(GMT+09:00) Osaka, Sapporo, Tokyo' AS displayName, 9 AS gmtOffset, 0 AS blnUseDaylightSavings, 85 AS [order], 1 AS isEnabled
	UNION SELECT 86 AS idTimezone, 'Korea Standard Time' AS dotNetName, '(GMT+09:00) Seoul' AS displayName, 9 AS gmtOffset, 0 AS blnUseDaylightSavings, 86 AS [order], 1 AS isEnabled
	UNION SELECT 87 AS idTimezone, 'Cen. Australia Standard Time' AS dotNetName, '(GMT+09:30) Adelaide' AS displayName, 9.5 AS gmtOffset, 1 AS blnUseDaylightSavings, 87 AS [order], 1 AS isEnabled
	UNION SELECT 88 AS idTimezone, 'AUS Central Standard Time' AS dotNetName, '(GMT+09:30) Darwin' AS displayName, 9.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 88 AS [order], 1 AS isEnabled
	UNION SELECT 89 AS idTimezone, 'E. Australia Standard Time' AS dotNetName, '(GMT+10:00) Brisbane' AS displayName, 10 AS gmtOffset, 0 AS blnUseDaylightSavings, 89 AS [order], 1 AS isEnabled
	UNION SELECT 90 AS idTimezone, 'AUS Eastern Standard Time' AS dotNetName, '(GMT+10:00) Canberra, Melbourne, Sydney' AS displayName, 10 AS gmtOffset, 1 AS blnUseDaylightSavings, 90 AS [order], 1 AS isEnabled
	UNION SELECT 91 AS idTimezone, 'West Pacific Standard Time' AS dotNetName, '(GMT+10:00) Guam, Port Moresby' AS displayName, 10 AS gmtOffset, 0 AS blnUseDaylightSavings, 91 AS [order], 1 AS isEnabled
	UNION SELECT 92 AS idTimezone, 'Tasmania Standard Time' AS dotNetName, '(GMT+10:00) Hobart' AS displayName, 10 AS gmtOffset, 1 AS blnUseDaylightSavings, 92 AS [order], 1 AS isEnabled
	UNION SELECT 93 AS idTimezone, 'Yakutsk Standard Time' AS dotNetName, '(GMT+10:00) Yakutsk' AS displayName, 10 AS gmtOffset, 1 AS blnUseDaylightSavings, 93 AS [order], 1 AS isEnabled
	UNION SELECT 94 AS idTimezone, 'Central Pacific Standard Time' AS dotNetName, '(GMT+11:00) Solomon Is., New Caledonia' AS displayName, 11 AS gmtOffset, 0 AS blnUseDaylightSavings, 94 AS [order], 1 AS isEnabled
	UNION SELECT 95 AS idTimezone, 'Vladivostok Standard Time' AS dotNetName, '(GMT+11:00) Vladivostok' AS displayName, 11 AS gmtOffset, 1 AS blnUseDaylightSavings, 95 AS [order], 1 AS isEnabled
	UNION SELECT 96 AS idTimezone, 'New Zealand Standard Time' AS dotNetName, '(GMT+12:00) Auckland, Wellington' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 96 AS [order], 1 AS isEnabled
	UNION SELECT 97 AS idTimezone, 'UTC+12' AS dotNetName, '(GMT+12:00) Coordinated Universal Time+12' AS displayName, 12 AS gmtOffset, 0 AS blnUseDaylightSavings, 97 AS [order], 1 AS isEnabled
	UNION SELECT 98 AS idTimezone, 'Fiji Standard Time' AS dotNetName, '(GMT+12:00) Fiji' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 98 AS [order], 1 AS isEnabled
	UNION SELECT 99 AS idTimezone, 'Magadan Standard Time' AS dotNetName, '(GMT+12:00) Magadan' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 99 AS [order], 1 AS isEnabled
	UNION SELECT 100 AS idTimezone, 'Kamchatka Standard Time' AS dotNetName, '(GMT+12:00) Petropavlovsk-Kamchatsky - Old' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 100 AS [order], 1 AS isEnabled
	UNION SELECT 101 AS idTimezone, 'Tonga Standard Time' AS dotNetName, '(GMT+13:00) Nuku''alofa' AS displayName, 13 AS gmtOffset, 0 AS blnUseDaylightSavings, 101 AS [order], 1 AS isEnabled
	UNION SELECT 102 AS idTimezone, 'Samoa Standard Time' AS dotNetName, '(GMT+13:00) Samoa' AS displayName, 13 AS gmtOffset, 1 AS blnUseDaylightSavings, 102 AS [order], 1 AS isEnabled

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblTimezone TZ WHERE TZ.idTimezone = MAIN.idTimezone)

SET IDENTITY_INSERT tblTimezone OFF
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTimezoneLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTimezoneLanguage] (
	[idTimezoneLanguage]			INT				IDENTITY(1,1)	NOT NULL,
	[idTimezone]					INT								NULL,
	[idLanguage]					INT								NULL,
	[displayName]					NVARCHAR(255)					NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_TimezoneLanguage]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezoneLanguage ADD CONSTRAINT [PK_TimezoneLanguage] PRIMARY KEY CLUSTERED (idTimezoneLanguage ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TimezoneLanguage_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezoneLanguage ADD CONSTRAINT [FK_TimezoneLanguage_Timezone] FOREIGN KEY (idTimezone) REFERENCES [tblTimezone] (idTimezone)

IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_TimezoneLanguage_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezoneLanguage ADD CONSTRAINT [FK_TimezoneLanguage_Language] FOREIGN KEY (idLanguage) REFERENCES [tblLanguage] (idLanguage)

/**
INSERT VALUES
**/

--ENGLISH
INSERT INTO tblTimezoneLanguage (
	idTimezone,
	idLanguage,
	displayName
)
SELECT
	TZ.idTimezone,
	57,
	TZ.displayName
FROM tblTimezone TZ
WHERE NOT EXISTS (
	SELECT 1
	FROM tblTimezoneLanguage TZL
	WHERE TZL.idTimezone = TZ.idTimezone
	AND TZL.idLanguage = 57
)
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblPortalCloneLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblPortalCloneLog] (
	[idPortalCloneLog]	INT			IDENTITY(1,1)	NOT NULL,
	[token]				NVARCHAR(50)				NULL,
	[timestamp]			DATETIME					NOT NULL,
	[message]			NVARCHAR(MAX)				NOT NULL,
	[isError]			BIT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_PortalCloneLog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblPortalCloneLog ADD CONSTRAINT [PK_PortalCloneLog] PRIMARY KEY CLUSTERED (idPortalCloneLog ASC)
