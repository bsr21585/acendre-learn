IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountParam]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountParam] (
	[idAccountParam] [INT] IDENTITY(1,1) NOT NULL,
	[idAccount] [INT] NOT NULL, 
	[param] [NVARCHAR](255) NOT NULL,
	[value] [NVARCHAR](255) NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountParam]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountParam ADD CONSTRAINT [PK_AccountParam] PRIMARY KEY CLUSTERED (idAccountParam ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountParam_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountParam ADD CONSTRAINT [FK_AccountParam_Account] FOREIGN KEY (idAccount) REFERENCES [tblAccount] (idAccount)
