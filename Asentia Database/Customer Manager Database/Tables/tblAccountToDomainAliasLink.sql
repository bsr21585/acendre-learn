IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountToDomainAliasLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountToDomainAliasLink] (
	[idAccountToDomainAliasLink] [int] IDENTITY (1, 1) NOT NULL,
	[idAccount] [int] NOT NULL,
	[hostname] [nvarchar] (255) NULL,
	[domain] [nvarchar] (255) NULL,
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountToDomainAliasLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountToDomainAliasLink ADD CONSTRAINT [PK_AccountToDomainAliasLink] PRIMARY KEY CLUSTERED (idAccountToDomainAliasLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountToDomainAliasLink_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountToDomainAliasLink ADD CONSTRAINT [FK_AccountToDomainAliasLink_Account] FOREIGN KEY (idAccount) REFERENCES [tblAccount] (idAccount)	
	