IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccount]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccount] (
	[idAccount] [int] IDENTITY (2, 1) NOT NULL,
	[idDatabaseServer] [int] NULL, 
	[company] [nvarchar] (255) NOT NULL,
	[contactFirstName] [nvarchar] (255) NOT NULL,
	[contactLastName] [nvarchar] (255) NOT NULL,
	[contactDisplayName] [nvarchar] (512) NOT NULL,
	[contactEmail] [nvarchar] (255) NULL,
	[username] [nvarchar] (512) NOT NULL,
	[password] [nvarchar] (512) NULL,
	[databaseName] [nvarchar] (255) NULL,
	[dtCreated] [datetime] NOT NULL,
	[isActive] [bit] NOT NULL,
	[isDeleted] [bit] NOT NULL,
	[isJobProcessing] [bit] NULL,
	[dtLastJobProcessStart] [datetime] NULL,
	[dtLastJobProcessEnd] [datetime] NULL,
	[isPowerPointJobProcessing] [bit] NULL,
	[dtLastPowerPointJobProcessStart] [datetime] NULL,
	[dtLastPowerPointJobProcessEnd] [datetime] NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccount ADD CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED (idAccount ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_Account_DatabaseServer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccount ADD CONSTRAINT [FK_Account_DatabaseServer] FOREIGN KEY (idDatabaseServer) REFERENCES [tblDatabaseServer] (idDatabaseServer)
			
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAccount]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAccount]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblAccount (
	company,
	contactDisplayName, 
	contactEmail
)
KEY INDEX PK_Account
ON [Asentia-CustomerManager] -- insert index to this catalog

/**
INSERT TEMPLATE ACCOUNT
**/
SET IDENTITY_INSERT tblAccount ON

UPDATE tblAccount SET
	company = '__SYSTEM__',
	contactFirstName = '__SYSTEM__',
	contactLastName = '__SYSTEM__',
	contactDisplayName = '__SYSTEM__',
	contactEmail = '__SYSTEM__',
	username = '__SYSTEM__',
	[password] = '__SYSTEM__',
	isActive = 1,
	isDeleted = 0
WHERE idAccount = 1

INSERT INTO tblAccount (
	idAccount,
	company,
	contactFirstName,
	contactLastName,
	contactDisplayName,
	contactEmail,
	username,
	[password],
	dtCreated,
	isActive,
	isDeleted
) 
SELECT
	1,
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	'__SYSTEM__',
	GETUTCDATE(),
	1,
	0
WHERE NOT EXISTS (
	SELECT 1
	FROM tblAccount
	WHERE idAccount = 1
)

SET IDENTITY_INSERT tblAccount OFF
	