IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblTimezone]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblTimezone] (
	[idTimezone]					INT				IDENTITY(1,1)	NOT NULL,
	[dotNetName]					NVARCHAR(255)	UNIQUE			NOT NULL,
	[displayName]					NVARCHAR(255)					NOT NULL,
	[gmtOffset]						FLOAT							NOT NULL,
	[blnUseDaylightSavings]			BIT								NOT NULL,
	[order]							INT								NULL,
	[isEnabled]						BIT								NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Timezone]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblTimezone ADD CONSTRAINT [PK_Timezone] PRIMARY KEY CLUSTERED (idTimezone ASC)
	
/**
INSERT VALUES
**/

SET IDENTITY_INSERT tblTimezone ON

INSERT INTO tblTimezone (
	idTimezone, 
	dotNetName, 
	displayName, 
	gmtOffset, 
	blnUseDaylightSavings, 
	[order], 
	isEnabled
)
SELECT
	idTimezone, 
	dotNetName, 
	displayName, 
	gmtOffset, 
	blnUseDaylightSavings, 
	[order], 
	isEnabled
FROM (
	SELECT 1 AS idTimezone, 'Dateline Standard Time' AS dotNetName, '(GMT-12:00) International Date Line West' AS displayName, -12 AS gmtOffset, 0 AS blnUseDaylightSavings, 1 AS [order], 1 AS isEnabled
	UNION SELECT 2 AS idTimezone, 'UTC-11' AS dotNetName, '(GMT-11:00) Coordinated Universal Time-11' AS displayName, -11 AS gmtOffset, 0 AS blnUseDaylightSavings, 2 AS [order], 1 AS isEnabled
	UNION SELECT 3 AS idTimezone, 'Hawaiian Standard Time' AS dotNetName, '(GMT-10:00) Hawaii' AS displayName, -10 AS gmtOffset, 0 AS blnUseDaylightSavings, 3 AS [order], 1 AS isEnabled
	UNION SELECT 4 AS idTimezone, 'Alaskan Standard Time' AS dotNetName, '(GMT-09:00) Alaska' AS displayName, -9 AS gmtOffset, 1 AS blnUseDaylightSavings, 4 AS [order], 1 AS isEnabled
	UNION SELECT 5 AS idTimezone, 'Pacific Standard Time (Mexico)' AS dotNetName, '(GMT-08:00) Baja California' AS displayName, -8 AS gmtOffset, 1 AS blnUseDaylightSavings, 5 AS [order], 1 AS isEnabled
	UNION SELECT 6 AS idTimezone, 'Pacific Standard Time' AS dotNetName, '(GMT-08:00) Pacific Time (US & Canada)' AS displayName, -8 AS gmtOffset, 1 AS blnUseDaylightSavings, 6 AS [order], 1 AS isEnabled
	UNION SELECT 7 AS idTimezone, 'US Mountain Standard Time' AS dotNetName, '(GMT-07:00) Arizona' AS displayName, -7 AS gmtOffset, 0 AS blnUseDaylightSavings, 7 AS [order], 1 AS isEnabled
	UNION SELECT 8 AS idTimezone, 'Mountain Standard Time (Mexico)' AS dotNetName, '(GMT-07:00) Chihuahua, La Paz, Mazatlan' AS displayName, -7 AS gmtOffset, 1 AS blnUseDaylightSavings, 8 AS [order], 1 AS isEnabled
	UNION SELECT 9 AS idTimezone, 'Mountain Standard Time' AS dotNetName, '(GMT-07:00) Mountain Time (US & Canada)' AS displayName, -7 AS gmtOffset, 1 AS blnUseDaylightSavings, 9 AS [order], 1 AS isEnabled
	UNION SELECT 10 AS idTimezone, 'Central America Standard Time' AS dotNetName, '(GMT-06:00) Central America' AS displayName, -6 AS gmtOffset, 0 AS blnUseDaylightSavings, 10 AS [order], 1 AS isEnabled
	UNION SELECT 11 AS idTimezone, 'Central Standard Time' AS dotNetName, '(GMT-06:00) Central Time (US & Canada)' AS displayName, -6 AS gmtOffset, 1 AS blnUseDaylightSavings, 11 AS [order], 1 AS isEnabled
	UNION SELECT 12 AS idTimezone, 'Central Standard Time (Mexico)' AS dotNetName, '(GMT-06:00) Guadalajara, Mexico City, Monterrey' AS displayName, -6 AS gmtOffset, 1 AS blnUseDaylightSavings, 12 AS [order], 1 AS isEnabled
	UNION SELECT 13 AS idTimezone, 'Canada Central Standard Time' AS dotNetName, '(GMT-06:00) Saskatchewan' AS displayName, -6 AS gmtOffset, 0 AS blnUseDaylightSavings, 13 AS [order], 1 AS isEnabled
	UNION SELECT 14 AS idTimezone, 'SA Pacific Standard Time' AS dotNetName, '(GMT-05:00) Bogota, Lima, Quito' AS displayName, -5 AS gmtOffset, 0 AS blnUseDaylightSavings, 14 AS [order], 1 AS isEnabled
	UNION SELECT 15 AS idTimezone, 'Eastern Standard Time' AS dotNetName, '(GMT-05:00) Eastern Time (US & Canada)' AS displayName, -5 AS gmtOffset, 1 AS blnUseDaylightSavings, 15 AS [order], 1 AS isEnabled
	UNION SELECT 16 AS idTimezone, 'US Eastern Standard Time' AS dotNetName, '(GMT-05:00) Indiana (East)' AS displayName, -5 AS gmtOffset, 1 AS blnUseDaylightSavings, 16 AS [order], 1 AS isEnabled
	UNION SELECT 17 AS idTimezone, 'Venezuela Standard Time' AS dotNetName, '(GMT-04:30) Caracas' AS displayName, -4.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 17 AS [order], 1 AS isEnabled
	UNION SELECT 18 AS idTimezone, 'Paraguay Standard Time' AS dotNetName, '(GMT-04:00) Asuncion' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 18 AS [order], 1 AS isEnabled
	UNION SELECT 19 AS idTimezone, 'Atlantic Standard Time' AS dotNetName, '(GMT-04:00) Atlantic Time (Canada)' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 19 AS [order], 1 AS isEnabled
	UNION SELECT 20 AS idTimezone, 'Central Brazilian Standard Time' AS dotNetName, '(GMT-04:00) Cuiaba' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 20 AS [order], 1 AS isEnabled
	UNION SELECT 21 AS idTimezone, 'SA Western Standard Time' AS dotNetName, '(GMT-04:00) Georgetown, La Paz, Manaus, San Juan' AS displayName, -4 AS gmtOffset, 0 AS blnUseDaylightSavings, 21 AS [order], 1 AS isEnabled
	UNION SELECT 22 AS idTimezone, 'Pacific SA Standard Time' AS dotNetName, '(GMT-04:00) Santiago' AS displayName, -4 AS gmtOffset, 1 AS blnUseDaylightSavings, 22 AS [order], 1 AS isEnabled
	UNION SELECT 23 AS idTimezone, 'Newfoundland Standard Time' AS dotNetName, '(GMT-03:30) Newfoundland' AS displayName, -3.5 AS gmtOffset, 1 AS blnUseDaylightSavings, 23 AS [order], 1 AS isEnabled
	UNION SELECT 24 AS idTimezone, 'E. South America Standard Time' AS dotNetName, '(GMT-03:00) Brasilia' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 24 AS [order], 1 AS isEnabled
	UNION SELECT 25 AS idTimezone, 'Argentina Standard Time' AS dotNetName, '(GMT-03:00) Buenos Aires' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 25 AS [order], 1 AS isEnabled
	UNION SELECT 26 AS idTimezone, 'SA Eastern Standard Time' AS dotNetName, '(GMT-03:00) Cayenne, Fortaleza' AS displayName, -3 AS gmtOffset, 0 AS blnUseDaylightSavings, 26 AS [order], 1 AS isEnabled
	UNION SELECT 27 AS idTimezone, 'Greenland Standard Time' AS dotNetName, '(GMT-03:00) Greenland' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 27 AS [order], 1 AS isEnabled
	UNION SELECT 28 AS idTimezone, 'Montevideo Standard Time' AS dotNetName, '(GMT-03:00) Montevideo' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 28 AS [order], 1 AS isEnabled
	UNION SELECT 29 AS idTimezone, 'Bahia Standard Time' AS dotNetName, '(GMT-03:00) Salvador' AS displayName, -3 AS gmtOffset, 1 AS blnUseDaylightSavings, 29 AS [order], 1 AS isEnabled
	UNION SELECT 30 AS idTimezone, 'UTC-02' AS dotNetName, '(GMT-02:00) Coordinated Universal Time-02' AS displayName, -2 AS gmtOffset, 0 AS blnUseDaylightSavings, 30 AS [order], 1 AS isEnabled
	UNION SELECT 31 AS idTimezone, 'Mid-Atlantic Standard Time' AS dotNetName, '(GMT-02:00) Mid-Atlantic' AS displayName, -2 AS gmtOffset, 1 AS blnUseDaylightSavings, 31 AS [order], 1 AS isEnabled
	UNION SELECT 32 AS idTimezone, 'Azores Standard Time' AS dotNetName, '(GMT-01:00) Azores' AS displayName, -1 AS gmtOffset, 1 AS blnUseDaylightSavings, 32 AS [order], 1 AS isEnabled
	UNION SELECT 33 AS idTimezone, 'Cape Verde Standard Time' AS dotNetName, '(GMT-01:00) Cape Verde Is.' AS displayName, -1 AS gmtOffset, 0 AS blnUseDaylightSavings, 33 AS [order], 1 AS isEnabled
	UNION SELECT 34 AS idTimezone, 'Morocco Standard Time' AS dotNetName, '(GMT) Casablanca' AS displayName, 0 AS gmtOffset, 1 AS blnUseDaylightSavings, 34 AS [order], 1 AS isEnabled
	UNION SELECT 35 AS idTimezone, 'UTC' AS dotNetName, '(GMT) Coordinated Universal Time' AS displayName, 0 AS gmtOffset, 0 AS blnUseDaylightSavings, 35 AS [order], 1 AS isEnabled
	UNION SELECT 36 AS idTimezone, 'GMT Standard Time' AS dotNetName, '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London' AS displayName, 0 AS gmtOffset, 1 AS blnUseDaylightSavings, 36 AS [order], 1 AS isEnabled
	UNION SELECT 37 AS idTimezone, 'Greenwich Standard Time' AS dotNetName, '(GMT) Monrovia, Reykjavik' AS displayName, 0 AS gmtOffset, 0 AS blnUseDaylightSavings, 37 AS [order], 1 AS isEnabled
	UNION SELECT 38 AS idTimezone, 'W. Europe Standard Time' AS dotNetName, '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 38 AS [order], 1 AS isEnabled
	UNION SELECT 39 AS idTimezone, 'Central Europe Standard Time' AS dotNetName, '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 39 AS [order], 1 AS isEnabled
	UNION SELECT 40 AS idTimezone, 'Romance Standard Time' AS dotNetName, '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 40 AS [order], 1 AS isEnabled
	UNION SELECT 41 AS idTimezone, 'Central European Standard Time' AS dotNetName, '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 41 AS [order], 1 AS isEnabled
	UNION SELECT 42 AS idTimezone, 'Libya Standard Time' AS dotNetName, '(GMT+01:00) Tripoli' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 42 AS [order], 1 AS isEnabled
	UNION SELECT 43 AS idTimezone, 'W. Central Africa Standard Time' AS dotNetName, '(GMT+01:00) West Central Africa' AS displayName, 1 AS gmtOffset, 0 AS blnUseDaylightSavings, 43 AS [order], 1 AS isEnabled
	UNION SELECT 44 AS idTimezone, 'Namibia Standard Time' AS dotNetName, '(GMT+01:00) Windhoek' AS displayName, 1 AS gmtOffset, 1 AS blnUseDaylightSavings, 44 AS [order], 1 AS isEnabled
	UNION SELECT 45 AS idTimezone, 'GTB Standard Time' AS dotNetName, '(GMT+02:00) Athens, Bucharest' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 45 AS [order], 1 AS isEnabled
	UNION SELECT 46 AS idTimezone, 'Middle East Standard Time' AS dotNetName, '(GMT+02:00) Beirut' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 46 AS [order], 1 AS isEnabled
	UNION SELECT 47 AS idTimezone, 'Egypt Standard Time' AS dotNetName, '(GMT+02:00) Cairo' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 47 AS [order], 1 AS isEnabled
	UNION SELECT 48 AS idTimezone, 'Syria Standard Time' AS dotNetName, '(GMT+02:00) Damascus' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 48 AS [order], 1 AS isEnabled
	UNION SELECT 49 AS idTimezone, 'E. Europe Standard Time' AS dotNetName, '(GMT+02:00) E. Europe' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 49 AS [order], 1 AS isEnabled
	UNION SELECT 50 AS idTimezone, 'South Africa Standard Time' AS dotNetName, '(GMT+02:00) Harare, Pretoria' AS displayName, 2 AS gmtOffset, 0 AS blnUseDaylightSavings, 50 AS [order], 1 AS isEnabled
	UNION SELECT 51 AS idTimezone, 'FLE Standard Time' AS dotNetName, '(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 51 AS [order], 1 AS isEnabled
	UNION SELECT 52 AS idTimezone, 'Turkey Standard Time' AS dotNetName, '(GMT+02:00) Istanbul' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 52 AS [order], 1 AS isEnabled
	UNION SELECT 53 AS idTimezone, 'Israel Standard Time' AS dotNetName, '(GMT+02:00) Jerusalem' AS displayName, 2 AS gmtOffset, 1 AS blnUseDaylightSavings, 53 AS [order], 1 AS isEnabled
	UNION SELECT 54 AS idTimezone, 'Jordan Standard Time' AS dotNetName, '(GMT+03:00) Amman' AS displayName, 3 AS gmtOffset, 1 AS blnUseDaylightSavings, 54 AS [order], 1 AS isEnabled
	UNION SELECT 55 AS idTimezone, 'Arabic Standard Time' AS dotNetName, '(GMT+03:00) Baghdad' AS displayName, 3 AS gmtOffset, 1 AS blnUseDaylightSavings, 55 AS [order], 1 AS isEnabled
	UNION SELECT 56 AS idTimezone, 'Kaliningrad Standard Time' AS dotNetName, '(GMT+03:00) Kaliningrad, Minsk' AS displayName, 3 AS gmtOffset, 1 AS blnUseDaylightSavings, 56 AS [order], 1 AS isEnabled
	UNION SELECT 57 AS idTimezone, 'Arab Standard Time' AS dotNetName, '(GMT+03:00) Kuwait, Riyadh' AS displayName, 3 AS gmtOffset, 0 AS blnUseDaylightSavings, 57 AS [order], 1 AS isEnabled
	UNION SELECT 58 AS idTimezone, 'E. Africa Standard Time' AS dotNetName, '(GMT+03:00) Nairobi' AS displayName, 3 AS gmtOffset, 0 AS blnUseDaylightSavings, 58 AS [order], 1 AS isEnabled
	UNION SELECT 59 AS idTimezone, 'Iran Standard Time' AS dotNetName, '(GMT+03:30) Tehran' AS displayName, 3.5 AS gmtOffset, 1 AS blnUseDaylightSavings, 59 AS [order], 1 AS isEnabled
	UNION SELECT 60 AS idTimezone, 'Arabian Standard Time' AS dotNetName, '(GMT+04:00) Abu Dhabi, Muscat' AS displayName, 4 AS gmtOffset, 0 AS blnUseDaylightSavings, 60 AS [order], 1 AS isEnabled
	UNION SELECT 61 AS idTimezone, 'Azerbaijan Standard Time' AS dotNetName, '(GMT+04:00) Baku' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 61 AS [order], 1 AS isEnabled
	UNION SELECT 62 AS idTimezone, 'Russian Standard Time' AS dotNetName, '(GMT+04:00) Moscow, St. Petersburg, Volgograd' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 62 AS [order], 1 AS isEnabled
	UNION SELECT 63 AS idTimezone, 'Mauritius Standard Time' AS dotNetName, '(GMT+04:00) Port Louis' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 63 AS [order], 1 AS isEnabled
	UNION SELECT 64 AS idTimezone, 'Georgian Standard Time' AS dotNetName, '(GMT+04:00) Tbilisi' AS displayName, 4 AS gmtOffset, 0 AS blnUseDaylightSavings, 64 AS [order], 1 AS isEnabled
	UNION SELECT 65 AS idTimezone, 'Caucasus Standard Time' AS dotNetName, '(GMT+04:00) Yerevan' AS displayName, 4 AS gmtOffset, 1 AS blnUseDaylightSavings, 65 AS [order], 1 AS isEnabled
	UNION SELECT 66 AS idTimezone, 'Afghanistan Standard Time' AS dotNetName, '(GMT+04:30) Kabul' AS displayName, 4.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 66 AS [order], 1 AS isEnabled
	UNION SELECT 67 AS idTimezone, 'West Asia Standard Time' AS dotNetName, '(GMT+05:00) Ashgabat, Tashkent' AS displayName, 5 AS gmtOffset, 0 AS blnUseDaylightSavings, 67 AS [order], 1 AS isEnabled
	UNION SELECT 68 AS idTimezone, 'Pakistan Standard Time' AS dotNetName, '(GMT+05:00) Islamabad, Karachi' AS displayName, 5 AS gmtOffset, 1 AS blnUseDaylightSavings, 68 AS [order], 1 AS isEnabled
	UNION SELECT 69 AS idTimezone, 'India Standard Time' AS dotNetName, '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi' AS displayName, 5.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 69 AS [order], 1 AS isEnabled
	UNION SELECT 70 AS idTimezone, 'Sri Lanka Standard Time' AS dotNetName, '(GMT+05:30) Sri Jayawardenepura' AS displayName, 5.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 70 AS [order], 1 AS isEnabled
	UNION SELECT 71 AS idTimezone, 'Nepal Standard Time' AS dotNetName, '(GMT+05:45) Kathmandu' AS displayName, 5.75 AS gmtOffset, 0 AS blnUseDaylightSavings, 71 AS [order], 1 AS isEnabled
	UNION SELECT 72 AS idTimezone, 'Central Asia Standard Time' AS dotNetName, '(GMT+06:00) Astana' AS displayName, 6 AS gmtOffset, 0 AS blnUseDaylightSavings, 72 AS [order], 1 AS isEnabled
	UNION SELECT 73 AS idTimezone, 'Bangladesh Standard Time' AS dotNetName, '(GMT+06:00) Dhaka' AS displayName, 6 AS gmtOffset, 1 AS blnUseDaylightSavings, 73 AS [order], 1 AS isEnabled
	UNION SELECT 74 AS idTimezone, 'Ekaterinburg Standard Time' AS dotNetName, '(GMT+06:00) Ekaterinburg' AS displayName, 6 AS gmtOffset, 1 AS blnUseDaylightSavings, 74 AS [order], 1 AS isEnabled
	UNION SELECT 75 AS idTimezone, 'Myanmar Standard Time' AS dotNetName, '(GMT+06:30) Yangon (Rangoon)' AS displayName, 6.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 75 AS [order], 1 AS isEnabled
	UNION SELECT 76 AS idTimezone, 'SE Asia Standard Time' AS dotNetName, '(GMT+07:00) Bangkok, Hanoi, Jakarta' AS displayName, 7 AS gmtOffset, 0 AS blnUseDaylightSavings, 76 AS [order], 1 AS isEnabled
	UNION SELECT 77 AS idTimezone, 'N. Central Asia Standard Time' AS dotNetName, '(GMT+07:00) Novosibirsk' AS displayName, 7 AS gmtOffset, 1 AS blnUseDaylightSavings, 77 AS [order], 1 AS isEnabled
	UNION SELECT 78 AS idTimezone, 'China Standard Time' AS dotNetName, '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 78 AS [order], 1 AS isEnabled
	UNION SELECT 79 AS idTimezone, 'North Asia Standard Time' AS dotNetName, '(GMT+08:00) Krasnoyarsk' AS displayName, 8 AS gmtOffset, 1 AS blnUseDaylightSavings, 79 AS [order], 1 AS isEnabled
	UNION SELECT 80 AS idTimezone, 'Singapore Standard Time' AS dotNetName, '(GMT+08:00) Kuala Lumpur, Singapore' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 80 AS [order], 1 AS isEnabled
	UNION SELECT 81 AS idTimezone, 'W. Australia Standard Time' AS dotNetName, '(GMT+08:00) Perth' AS displayName, 8 AS gmtOffset, 1 AS blnUseDaylightSavings, 81 AS [order], 1 AS isEnabled
	UNION SELECT 82 AS idTimezone, 'Taipei Standard Time' AS dotNetName, '(GMT+08:00) Taipei' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 82 AS [order], 1 AS isEnabled
	UNION SELECT 83 AS idTimezone, 'Ulaanbaatar Standard Time' AS dotNetName, '(GMT+08:00) Ulaanbaatar' AS displayName, 8 AS gmtOffset, 0 AS blnUseDaylightSavings, 83 AS [order], 1 AS isEnabled
	UNION SELECT 84 AS idTimezone, 'North Asia East Standard Time' AS dotNetName, '(GMT+09:00) Irkutsk' AS displayName, 9 AS gmtOffset, 1 AS blnUseDaylightSavings, 84 AS [order], 1 AS isEnabled
	UNION SELECT 85 AS idTimezone, 'Tokyo Standard Time' AS dotNetName, '(GMT+09:00) Osaka, Sapporo, Tokyo' AS displayName, 9 AS gmtOffset, 0 AS blnUseDaylightSavings, 85 AS [order], 1 AS isEnabled
	UNION SELECT 86 AS idTimezone, 'Korea Standard Time' AS dotNetName, '(GMT+09:00) Seoul' AS displayName, 9 AS gmtOffset, 0 AS blnUseDaylightSavings, 86 AS [order], 1 AS isEnabled
	UNION SELECT 87 AS idTimezone, 'Cen. Australia Standard Time' AS dotNetName, '(GMT+09:30) Adelaide' AS displayName, 9.5 AS gmtOffset, 1 AS blnUseDaylightSavings, 87 AS [order], 1 AS isEnabled
	UNION SELECT 88 AS idTimezone, 'AUS Central Standard Time' AS dotNetName, '(GMT+09:30) Darwin' AS displayName, 9.5 AS gmtOffset, 0 AS blnUseDaylightSavings, 88 AS [order], 1 AS isEnabled
	UNION SELECT 89 AS idTimezone, 'E. Australia Standard Time' AS dotNetName, '(GMT+10:00) Brisbane' AS displayName, 10 AS gmtOffset, 0 AS blnUseDaylightSavings, 89 AS [order], 1 AS isEnabled
	UNION SELECT 90 AS idTimezone, 'AUS Eastern Standard Time' AS dotNetName, '(GMT+10:00) Canberra, Melbourne, Sydney' AS displayName, 10 AS gmtOffset, 1 AS blnUseDaylightSavings, 90 AS [order], 1 AS isEnabled
	UNION SELECT 91 AS idTimezone, 'West Pacific Standard Time' AS dotNetName, '(GMT+10:00) Guam, Port Moresby' AS displayName, 10 AS gmtOffset, 0 AS blnUseDaylightSavings, 91 AS [order], 1 AS isEnabled
	UNION SELECT 92 AS idTimezone, 'Tasmania Standard Time' AS dotNetName, '(GMT+10:00) Hobart' AS displayName, 10 AS gmtOffset, 1 AS blnUseDaylightSavings, 92 AS [order], 1 AS isEnabled
	UNION SELECT 93 AS idTimezone, 'Yakutsk Standard Time' AS dotNetName, '(GMT+10:00) Yakutsk' AS displayName, 10 AS gmtOffset, 1 AS blnUseDaylightSavings, 93 AS [order], 1 AS isEnabled
	UNION SELECT 94 AS idTimezone, 'Central Pacific Standard Time' AS dotNetName, '(GMT+11:00) Solomon Is., New Caledonia' AS displayName, 11 AS gmtOffset, 0 AS blnUseDaylightSavings, 94 AS [order], 1 AS isEnabled
	UNION SELECT 95 AS idTimezone, 'Vladivostok Standard Time' AS dotNetName, '(GMT+11:00) Vladivostok' AS displayName, 11 AS gmtOffset, 1 AS blnUseDaylightSavings, 95 AS [order], 1 AS isEnabled
	UNION SELECT 96 AS idTimezone, 'New Zealand Standard Time' AS dotNetName, '(GMT+12:00) Auckland, Wellington' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 96 AS [order], 1 AS isEnabled
	UNION SELECT 97 AS idTimezone, 'UTC+12' AS dotNetName, '(GMT+12:00) Coordinated Universal Time+12' AS displayName, 12 AS gmtOffset, 0 AS blnUseDaylightSavings, 97 AS [order], 1 AS isEnabled
	UNION SELECT 98 AS idTimezone, 'Fiji Standard Time' AS dotNetName, '(GMT+12:00) Fiji' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 98 AS [order], 1 AS isEnabled
	UNION SELECT 99 AS idTimezone, 'Magadan Standard Time' AS dotNetName, '(GMT+12:00) Magadan' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 99 AS [order], 1 AS isEnabled
	UNION SELECT 100 AS idTimezone, 'Kamchatka Standard Time' AS dotNetName, '(GMT+12:00) Petropavlovsk-Kamchatsky - Old' AS displayName, 12 AS gmtOffset, 1 AS blnUseDaylightSavings, 100 AS [order], 1 AS isEnabled
	UNION SELECT 101 AS idTimezone, 'Tonga Standard Time' AS dotNetName, '(GMT+13:00) Nuku''alofa' AS displayName, 13 AS gmtOffset, 0 AS blnUseDaylightSavings, 101 AS [order], 1 AS isEnabled
	UNION SELECT 102 AS idTimezone, 'Samoa Standard Time' AS dotNetName, '(GMT+13:00) Samoa' AS displayName, 13 AS gmtOffset, 1 AS blnUseDaylightSavings, 102 AS [order], 1 AS isEnabled

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblTimezone TZ WHERE TZ.idTimezone = MAIN.idTimezone)

SET IDENTITY_INSERT tblTimezone OFF