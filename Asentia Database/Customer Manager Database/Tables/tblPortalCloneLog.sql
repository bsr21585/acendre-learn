IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblPortalCloneLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblPortalCloneLog] (
	[idPortalCloneLog]	INT			IDENTITY(1,1)	NOT NULL,
	[token]				NVARCHAR(50)				NULL,
	[timestamp]			DATETIME					NOT NULL,
	[message]			NVARCHAR(MAX)				NOT NULL,
	[isError]			BIT							NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_PortalCloneLog]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblPortalCloneLog ADD CONSTRAINT [PK_PortalCloneLog] PRIMARY KEY CLUSTERED (idPortalCloneLog ASC)