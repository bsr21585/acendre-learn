IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblRole]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblRole] (
	[idRole] [int] IDENTITY (1, 1) NOT NULL, 
	[roleName] [nvarchar] (255) NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Role]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblRole ADD CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED (idRole ASC)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRole]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblRole]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblRole (
	roleName
)
KEY INDEX PK_Role
ON [Asentia-CustomerManager] -- insert index to this catalog

/** 
INSERT ROLES 
**/
SET IDENTITY_INSERT tblRole ON

UPDATE tblRole SET roleName = 'System User' WHERE idRole = 1
UPDATE tblRole SET roleName = 'Account User' WHERE idRole = 2

INSERT INTO tblRole 
(
	idRole, 
	roleName
) 
SELECT
	1,
	'System User'
WHERE NOT EXISTS (
	SELECT 1
	FROM tblRole
	WHERE idRole = 1
)

INSERT INTO tblRole 
(
	idRole, 
	roleName
) 
SELECT
	2,
	'Account User'
WHERE NOT EXISTS (
	SELECT 1
	FROM tblRole
	WHERE idRole = 2
)

SET IDENTITY_INSERT tblRole OFF
	