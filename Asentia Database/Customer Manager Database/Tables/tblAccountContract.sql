IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountContract]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountContract] (
	[idAccountContract]					INT			IDENTITY(1,1)	NOT NULL,
	[idAccount]							INT							NOT NULL,
	[allowedUsers]						INT							NULL,		-- NULL means "unlimited"
	[disabledUserRatio]					FLOAT						NULL,		-- NULL means "no disabled user ratio"
	[disabledUserThresholdInterval]		INT							NULL,		-- NULL means "no disabled user ratio"
	[disabledUserThresholdTimeframe]	NVARCHAR(4)					NULL,	    -- NULL means "no disabled user ratio"
	[diskSpaceLimitKB]					INT							NULL,		-- NULL means "unlimited"
	[contractNotes]						NVARCHAR(MAX)				NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountContract]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountContract ADD CONSTRAINT [PK_AccountContract] PRIMARY KEY CLUSTERED (idAccountContract ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountContract_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountContract ADD CONSTRAINT [FK_AccountContract_Account] FOREIGN KEY (idAccount) REFERENCES [tblAccount] (idAccount)
	