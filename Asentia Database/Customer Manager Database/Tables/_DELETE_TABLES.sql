IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccount]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblAccount]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountToDomainAliasLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblAccountToDomainAliasLink]
GO

IF EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [tblAccountUser]
GO