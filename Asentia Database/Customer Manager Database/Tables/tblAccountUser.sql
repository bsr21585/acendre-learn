IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountUser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountUser] (
	[idAccountUser] [int] IDENTITY (2, 1) NOT NULL,
	[idAccount] [int] NOT NULL,
	[firstName] [nvarchar] (255) NOT NULL,
	[lastName] [nvarchar] (255) NOT NULL,
	[displayName] [nvarchar] (512) NOT NULL,
	[email] [nvarchar] (255) NULL,
	[username] [nvarchar] (512) NOT NULL,
	[password] [nvarchar] (512) NOT NULL,
	[idRole] [int] NOT NULL,
	[isActive] [bit] NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountUser]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountUser ADD CONSTRAINT [PK_AccountUser] PRIMARY KEY CLUSTERED (idAccountUser ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountUser_Account]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountUser ADD CONSTRAINT [FK_AccountUser_Account] FOREIGN KEY (idAccount) REFERENCES [tblAccount] (idAccount)	
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAccountUser]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblAccountUser]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblAccountUser (
	displayName,
	email,
	username
)
KEY INDEX PK_AccountUser
ON [Asentia-CustomerManager] -- insert index to this catalog

/**
INSERT MASTER ADMINISTRATOR ACCOUNT USER
**/
SET IDENTITY_INSERT tblAccountUser ON

UPDATE tblAccountUser SET
	idAccount = 1,
	firstName = 'ICS Learning Group',
	lastName = 'Administrator',
	displayName = 'Administrator, ICS Learning Group',
	email = 'tech@icslearninggroup.com',
	username = 'administrator',
	idRole = 1,
	isActive = 1
WHERE idAccountUser = 1

INSERT INTO tblAccountUser (
	idAccountUser,
	idAccount,
	firstName,
	lastName,
	displayName,
	email,
	username,
	[password],
	idRole,
	isActive
) 
SELECT
	1,
	1,
	'ICS Learning Group',
	'Administrator',
	'Administrator, ICS Learning Group',
	'tech@icslearninggroup.com',
	'administrator',
	dbo.GetHashedString('SHA1', '#fr4nt1c!'),
	idRole = 1,
	isActive = 1
WHERE NOT EXISTS (
	SELECT 1
	FROM tblAccountUser
	WHERE idAccountUser = 1
)

SET IDENTITY_INSERT tblAccountUser OFF