IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblDatabaseServer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblDatabaseServer] (
	[idDatabaseServer] [INT] IDENTITY(1,1) NOT NULL,
	[serverName] [NVARCHAR](255) NOT NULL, 
	[networkName] [NVARCHAR](255) NOT NULL,
	[username] [NVARCHAR](512) NULL,
	[password] [NVARCHAR](512) NULL,
	[isTrusted] [BIT] NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_idDatabaseServer]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblDatabaseServer ADD CONSTRAINT [PK_idDatabaseServer] PRIMARY KEY CLUSTERED (idDatabaseServer ASC)
	
/**
FULL TEXT INDEX
**/
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDatabaseServer]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
AND NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[tblDatabaseServer]') AND OBJECTPROPERTY(id, N'TableHasActiveFulltextIndex') = 1)
CREATE FULLTEXT INDEX ON tblDatabaseServer (
	serverName,
	networkName
)
KEY INDEX PK_idDatabaseServer
ON [Asentia-CustomerManager] -- insert index to this catalog
