IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblAccountContractToPortalLink]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblAccountContractToPortalLink] (
	[idAccountContractToPortalLink]	INT			IDENTITY(1,1)	NOT NULL,
	[idAccountContract]				INT							NOT NULL,
	[idPortal]						INT							NULL,		-- NULL means "all portals"
	[portalName]					NVARCHAR(255)				NULL		-- NULL means "all portals"
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_AccountContractToPortalLink]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountContractToPortalLink ADD CONSTRAINT [PK_AccountContractToPortalLink] PRIMARY KEY CLUSTERED (idAccountContractToPortalLink ASC)
	
/**
FOREIGN KEYS
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[FK_AccountContractToPortalLink_AccountContract]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblAccountContractToPortalLink ADD CONSTRAINT [FK_AccountContractToPortalLink_AccountContract] FOREIGN KEY (idAccountContract) REFERENCES [tblAccountContract] (idAccountContract)	
	