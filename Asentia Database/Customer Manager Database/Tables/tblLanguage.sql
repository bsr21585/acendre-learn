IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[tblLanguage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [tblLanguage] (
	[idLanguage]		INT				IDENTITY (1, 1)		NOT NULL, 
	[code]				NVARCHAR(20)						NOT NULL,
	[name]				NVARCHAR(255)						NOT NULL
) ON [PRIMARY]

/**
PRIMARY KEY
*/
IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE id = object_id(N'[PK_Language]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
	ALTER TABLE tblLanguage ADD CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED (idLanguage ASC)

/**
INSERT VALUES
**/

SET IDENTITY_INSERT [tblLanguage] ON

INSERT INTO [tblLanguage] (
	[idLanguage], 
	[code],
	[name]
)
SELECT
	idLanguage,
	code,
	name
FROM (
	SELECT 0 AS idLanguage,'af-ZA' AS code,'Afrikaans (South Africa)' AS name
	UNION SELECT 1 AS idLanguage,'am-ET' AS code,'Amharic (Ethiopia)' AS name
	UNION SELECT 2 AS idLanguage,'ar-AE' AS code,'Arabic (U.A.E.)' AS name
	UNION SELECT 3 AS idLanguage,'ar-BH' AS code,'Arabic (Bahrain)' AS name
	UNION SELECT 4 AS idLanguage,'ar-DZ' AS code,'Arabic (Algeria)' AS name
	UNION SELECT 5 AS idLanguage,'ar-EG' AS code,'Arabic (Egypt)' AS name
	UNION SELECT 6 AS idLanguage,'ar-IQ' AS code,'Arabic (Iraq)' AS name
	UNION SELECT 7 AS idLanguage,'ar-JO' AS code,'Arabic (Jordan)' AS name
	UNION SELECT 8 AS idLanguage,'ar-KW' AS code,'Arabic (Kuwait)' AS name
	UNION SELECT 9 AS idLanguage,'ar-LB' AS code,'Arabic (Lebanon)' AS name
	UNION SELECT 10 AS idLanguage,'ar-LY' AS code,'Arabic (Libya)' AS name
	UNION SELECT 11 AS idLanguage,'ar-MA' AS code,'Arabic (Morocco)' AS name
	UNION SELECT 12 AS idLanguage,'ar-OM' AS code,'Arabic (Oman)' AS name
	UNION SELECT 13 AS idLanguage,'ar-QA' AS code,'Arabic (Qatar)' AS name
	UNION SELECT 14 AS idLanguage,'ar-SA' AS code,'Arabic (Saudi Arabia)' AS name
	UNION SELECT 15 AS idLanguage,'ar-SY' AS code,'Arabic (Syria)' AS name
	UNION SELECT 16 AS idLanguage,'ar-TN' AS code,'Arabic (Tunisia)' AS name
	UNION SELECT 17 AS idLanguage,'ar-YE' AS code,'Arabic (Yemen)' AS name
	UNION SELECT 18 AS idLanguage,'arn-CL' AS code,'Mapudungun (Chile)' AS name
	UNION SELECT 19 AS idLanguage,'as-IN' AS code,'Assamese (India)' AS name
	UNION SELECT 20 AS idLanguage,'az-Cyrl-AZ' AS code,'Azeri (Cyrillic, Azerbaijan)' AS name
	UNION SELECT 21 AS idLanguage,'az-Latn-AZ' AS code,'Azeri (Latin, Azerbaijan)' AS name
	UNION SELECT 22 AS idLanguage,'ba-RU' AS code,'Bashkir (Russia)' AS name
	UNION SELECT 23 AS idLanguage,'be-BY' AS code,'Belarusian (Belarus)' AS name
	UNION SELECT 24 AS idLanguage,'bg-BG' AS code,'Bulgarian (Bulgaria)' AS name
	UNION SELECT 25 AS idLanguage,'bn-BD' AS code,'Bengali (Bangladesh)' AS name
	UNION SELECT 26 AS idLanguage,'bn-IN' AS code,'Bengali (India)' AS name
	UNION SELECT 27 AS idLanguage,'bo-CN' AS code,'Tibetan (PRC)' AS name
	UNION SELECT 28 AS idLanguage,'br-FR' AS code,'Breton (France)' AS name
	UNION SELECT 29 AS idLanguage,'bs-Cyrl-BA' AS code,'Bosnian (Cyrillic, Bosnia and Herzegovina)' AS name
	UNION SELECT 30 AS idLanguage,'bs-Latn-BA' AS code,'Bosnian (Latin, Bosnia and Herzegovina)' AS name
	UNION SELECT 31 AS idLanguage,'ca-ES' AS code,'Catalan (Catalan)' AS name
	UNION SELECT 32 AS idLanguage,'co-FR' AS code,'Corsican (France)' AS name
	UNION SELECT 33 AS idLanguage,'cs-CZ' AS code,'Czech (Czech Republic)' AS name
	UNION SELECT 34 AS idLanguage,'cy-GB' AS code,'Welsh (United Kingdom)' AS name
	UNION SELECT 35 AS idLanguage,'da-DK' AS code,'Danish (Denmark)' AS name
	UNION SELECT 36 AS idLanguage,'de-AT' AS code,'German (Austria)' AS name
	UNION SELECT 37 AS idLanguage,'de-CH' AS code,'German (Switzerland)' AS name
	UNION SELECT 38 AS idLanguage,'de-DE' AS code,'German (Germany)' AS name
	UNION SELECT 39 AS idLanguage,'de-LI' AS code,'German (Liechtenstein)' AS name
	UNION SELECT 40 AS idLanguage,'de-LU' AS code,'German (Luxembourg)' AS name
	UNION SELECT 41 AS idLanguage,'dsb-DE' AS code,'Lower Sorbian (Germany)' AS name
	UNION SELECT 42 AS idLanguage,'dv-MV' AS code,'Divehi (Maldives)' AS name
	UNION SELECT 43 AS idLanguage,'el-GR' AS code,'Greek (Greece)' AS name
	UNION SELECT 44 AS idLanguage,'en-029' AS code,'English (Caribbean)' AS name
	UNION SELECT 45 AS idLanguage,'en-AU' AS code,'English (Australia)' AS name
	UNION SELECT 46 AS idLanguage,'en-BZ' AS code,'English (Belize)' AS name
	UNION SELECT 47 AS idLanguage,'en-CA' AS code,'English (Canada)' AS name
	UNION SELECT 48 AS idLanguage,'en-GB' AS code,'English (United Kingdom)' AS name
	UNION SELECT 49 AS idLanguage,'en-IE' AS code,'English (Ireland)' AS name
	UNION SELECT 50 AS idLanguage,'en-IN' AS code,'English (India)' AS name
	UNION SELECT 51 AS idLanguage,'en-JM' AS code,'English (Jamaica)' AS name
	UNION SELECT 52 AS idLanguage,'en-MY' AS code,'English (Malaysia)' AS name
	UNION SELECT 53 AS idLanguage,'en-NZ' AS code,'English (New Zealand)' AS name
	UNION SELECT 54 AS idLanguage,'en-PH' AS code,'English (Republic of the Philippines)' AS name
	UNION SELECT 55 AS idLanguage,'en-SG' AS code,'English (Singapore)' AS name
	UNION SELECT 56 AS idLanguage,'en-TT' AS code,'English (Trinidad and Tobago)' AS name
	UNION SELECT 57 AS idLanguage,'en-US' AS code,'English (United States)' AS name
	UNION SELECT 58 AS idLanguage,'en-ZA' AS code,'English (South Africa)' AS name
	UNION SELECT 59 AS idLanguage,'en-ZW' AS code,'English (Zimbabwe)' AS name
	UNION SELECT 60 AS idLanguage,'es-AR' AS code,'Spanish (Argentina)' AS name
	UNION SELECT 61 AS idLanguage,'es-BO' AS code,'Spanish (Bolivia)' AS name
	UNION SELECT 62 AS idLanguage,'es-CL' AS code,'Spanish (Chile)' AS name
	UNION SELECT 63 AS idLanguage,'es-CO' AS code,'Spanish (Colombia)' AS name
	UNION SELECT 64 AS idLanguage,'es-CR' AS code,'Spanish (Costa Rica)' AS name
	UNION SELECT 65 AS idLanguage,'es-DO' AS code,'Spanish (Dominican Republic)' AS name
	UNION SELECT 66 AS idLanguage,'es-EC' AS code,'Spanish (Ecuador)' AS name
	UNION SELECT 67 AS idLanguage,'es-ES' AS code,'Spanish (Spain, International Sort)' AS name
	UNION SELECT 68 AS idLanguage,'es-GT' AS code,'Spanish (Guatemala)' AS name
	UNION SELECT 69 AS idLanguage,'es-HN' AS code,'Spanish (Honduras)' AS name
	UNION SELECT 70 AS idLanguage,'es-MX' AS code,'Spanish (Mexico)' AS name
	UNION SELECT 71 AS idLanguage,'es-NI' AS code,'Spanish (Nicaragua)' AS name
	UNION SELECT 72 AS idLanguage,'es-PA' AS code,'Spanish (Panama)' AS name
	UNION SELECT 73 AS idLanguage,'es-PE' AS code,'Spanish (Peru)' AS name
	UNION SELECT 74 AS idLanguage,'es-PR' AS code,'Spanish (Puerto Rico)' AS name
	UNION SELECT 75 AS idLanguage,'es-PY' AS code,'Spanish (Paraguay)' AS name
	UNION SELECT 76 AS idLanguage,'es-SV' AS code,'Spanish (El Salvador)' AS name
	UNION SELECT 77 AS idLanguage,'es-US' AS code,'Spanish (United States)' AS name
	UNION SELECT 78 AS idLanguage,'es-UY' AS code,'Spanish (Uruguay)' AS name
	UNION SELECT 79 AS idLanguage,'es-VE' AS code,'Spanish (Bolivarian Republic of Venezuela)' AS name
	UNION SELECT 80 AS idLanguage,'et-EE' AS code,'Estonian (Estonia)' AS name
	UNION SELECT 81 AS idLanguage,'eu-ES' AS code,'Basque (Basque)' AS name
	UNION SELECT 82 AS idLanguage,'fa-IR' AS code,'Persian' AS name
	UNION SELECT 83 AS idLanguage,'fi-FI' AS code,'Finnish (Finland)' AS name
	UNION SELECT 84 AS idLanguage,'fil-PH' AS code,'Filipino (Philippines)' AS name
	UNION SELECT 85 AS idLanguage,'fo-FO' AS code,'Faroese (Faroe Islands)' AS name
	UNION SELECT 86 AS idLanguage,'fr-BE' AS code,'French (Belgium)' AS name
	UNION SELECT 87 AS idLanguage,'fr-CA' AS code,'French (Canada)' AS name
	UNION SELECT 88 AS idLanguage,'fr-CH' AS code,'French (Switzerland)' AS name
	UNION SELECT 89 AS idLanguage,'fr-FR' AS code,'French (France)' AS name
	UNION SELECT 90 AS idLanguage,'fr-LU' AS code,'French (Luxembourg)' AS name
	UNION SELECT 91 AS idLanguage,'fr-MC' AS code,'French (Monaco)' AS name
	UNION SELECT 92 AS idLanguage,'fy-NL' AS code,'Frisian (Netherlands)' AS name
	UNION SELECT 93 AS idLanguage,'ga-IE' AS code,'Irish (Ireland)' AS name
	UNION SELECT 94 AS idLanguage,'gd-GB' AS code,'Scottish Gaelic (United Kingdom)' AS name
	UNION SELECT 95 AS idLanguage,'gl-ES' AS code,'Galician (Galician)' AS name
	UNION SELECT 96 AS idLanguage,'gsw-FR' AS code,'Alsatian (France)' AS name
	UNION SELECT 97 AS idLanguage,'gu-IN' AS code,'Gujarati (India)' AS name
	UNION SELECT 98 AS idLanguage,'ha-Latn-NG' AS code,'Hausa (Latin, Nigeria)' AS name
	UNION SELECT 99 AS idLanguage,'he-IL' AS code,'Hebrew (Israel)' AS name
	UNION SELECT 100 AS idLanguage,'hi-IN' AS code,'Hindi (India)' AS name
	UNION SELECT 101 AS idLanguage,'hr-BA' AS code,'Croatian (Latin, Bosnia and Herzegovina)' AS name
	UNION SELECT 102 AS idLanguage,'hr-HR' AS code,'Croatian (Croatia)' AS name
	UNION SELECT 103 AS idLanguage,'hsb-DE' AS code,'Upper Sorbian (Germany)' AS name
	UNION SELECT 104 AS idLanguage,'hu-HU' AS code,'Hungarian (Hungary)' AS name
	UNION SELECT 105 AS idLanguage,'hy-AM' AS code,'Armenian (Armenia)' AS name
	UNION SELECT 106 AS idLanguage,'id-ID' AS code,'Indonesian (Indonesia)' AS name
	UNION SELECT 107 AS idLanguage,'ig-NG' AS code,'Igbo (Nigeria)' AS name
	UNION SELECT 108 AS idLanguage,'ii-CN' AS code,'Yi (PRC)' AS name
	UNION SELECT 109 AS idLanguage,'is-IS' AS code,'Icelandic (Iceland)' AS name
	UNION SELECT 110 AS idLanguage,'it-CH' AS code,'Italian (Switzerland)' AS name
	UNION SELECT 111 AS idLanguage,'it-IT' AS code,'Italian (Italy)' AS name
	UNION SELECT 112 AS idLanguage,'iu-Cans-CA' AS code,'Inuktitut (Syllabics, Canada)' AS name
	UNION SELECT 113 AS idLanguage,'iu-Latn-CA' AS code,'Inuktitut (Latin, Canada)' AS name
	UNION SELECT 114 AS idLanguage,'ja-JP' AS code,'Japanese (Japan)' AS name
	UNION SELECT 115 AS idLanguage,'ka-GE' AS code,'Georgian (Georgia)' AS name
	UNION SELECT 116 AS idLanguage,'kk-KZ' AS code,'Kazakh (Kazakhstan)' AS name
	UNION SELECT 117 AS idLanguage,'kl-GL' AS code,'Greenlandic (Greenland)' AS name
	UNION SELECT 118 AS idLanguage,'km-KH' AS code,'Khmer (Cambodia)' AS name
	UNION SELECT 119 AS idLanguage,'kn-IN' AS code,'Kannada (India)' AS name
	UNION SELECT 120 AS idLanguage,'ko-KR' AS code,'Korean (Korea)' AS name
	UNION SELECT 121 AS idLanguage,'kok-IN' AS code,'Konkani (India)' AS name
	UNION SELECT 122 AS idLanguage,'ky-KG' AS code,'Kyrgyz (Kyrgyzstan)' AS name
	UNION SELECT 123 AS idLanguage,'lb-LU' AS code,'Luxembourgish (Luxembourg)' AS name
	UNION SELECT 124 AS idLanguage,'lo-LA' AS code,'Lao (Lao P.D.R.)' AS name
	UNION SELECT 125 AS idLanguage,'lt-LT' AS code,'Lithuanian (Lithuania)' AS name
	UNION SELECT 126 AS idLanguage,'lv-LV' AS code,'Latvian (Latvia)' AS name
	UNION SELECT 127 AS idLanguage,'mi-NZ' AS code,'Maori (New Zealand)' AS name
	UNION SELECT 128 AS idLanguage,'mk-MK' AS code,'Macedonian (Former Yugoslav Republic of Macedonia)' AS name
	UNION SELECT 129 AS idLanguage,'ml-IN' AS code,'Malayalam (India)' AS name
	UNION SELECT 130 AS idLanguage,'mn-MN' AS code,'Mongolian (Cyrillic, Mongolia)' AS name
	UNION SELECT 131 AS idLanguage,'mn-Mong-CN' AS code,'Mongolian (Traditional Mongolian, PRC)' AS name
	UNION SELECT 132 AS idLanguage,'moh-CA' AS code,'Mohawk (Mohawk)' AS name
	UNION SELECT 133 AS idLanguage,'mr-IN' AS code,'Marathi (India)' AS name
	UNION SELECT 134 AS idLanguage,'ms-BN' AS code,'Malay (Brunei Darussalam)' AS name
	UNION SELECT 135 AS idLanguage,'ms-MY' AS code,'Malay (Malaysia)' AS name
	UNION SELECT 136 AS idLanguage,'mt-MT' AS code,'Maltese (Malta)' AS name
	UNION SELECT 137 AS idLanguage,'nb-NO' AS code,'Norwegian, Bokm�l (Norway)' AS name
	UNION SELECT 138 AS idLanguage,'ne-NP' AS code,'Nepali (Nepal)' AS name
	UNION SELECT 139 AS idLanguage,'nl-BE' AS code,'Dutch (Belgium)' AS name
	UNION SELECT 140 AS idLanguage,'nl-NL' AS code,'Dutch (Netherlands)' AS name
	UNION SELECT 141 AS idLanguage,'nn-NO' AS code,'Norwegian, Nynorsk (Norway)' AS name
	UNION SELECT 142 AS idLanguage,'nso-ZA' AS code,'Sesotho sa Leboa (South Africa)' AS name
	UNION SELECT 143 AS idLanguage,'oc-FR' AS code,'Occitan (France)' AS name
	UNION SELECT 144 AS idLanguage,'or-IN' AS code,'Oriya (India)' AS name
	UNION SELECT 145 AS idLanguage,'pa-IN' AS code,'Punjabi (India)' AS name
	UNION SELECT 146 AS idLanguage,'pl-PL' AS code,'Polish (Poland)' AS name
	UNION SELECT 147 AS idLanguage,'prs-AF' AS code,'Dari (Afghanistan)' AS name
	UNION SELECT 148 AS idLanguage,'ps-AF' AS code,'Pashto (Afghanistan)' AS name
	UNION SELECT 149 AS idLanguage,'pt-BR' AS code,'Portuguese (Brazil)' AS name
	UNION SELECT 150 AS idLanguage,'pt-PT' AS code,'Portuguese (Portugal)' AS name
	UNION SELECT 151 AS idLanguage,'qut-GT' AS code,'K''iche (Guatemala)' AS name
	UNION SELECT 152 AS idLanguage,'quz-BO' AS code,'Quechua (Bolivia)' AS name
	UNION SELECT 153 AS idLanguage,'quz-EC' AS code,'Quechua (Ecuador)' AS name
	UNION SELECT 154 AS idLanguage,'quz-PE' AS code,'Quechua (Peru)' AS name
	UNION SELECT 155 AS idLanguage,'rm-CH' AS code,'Romansh (Switzerland)' AS name
	UNION SELECT 156 AS idLanguage,'ro-RO' AS code,'Romanian (Romania)' AS name
	UNION SELECT 157 AS idLanguage,'ru-RU' AS code,'Russian (Russia)' AS name
	UNION SELECT 158 AS idLanguage,'rw-RW' AS code,'Kinyarwanda (Rwanda)' AS name
	UNION SELECT 159 AS idLanguage,'sa-IN' AS code,'Sanskrit (India)' AS name
	UNION SELECT 160 AS idLanguage,'sah-RU' AS code,'Yakut (Russia)' AS name
	UNION SELECT 161 AS idLanguage,'se-FI' AS code,'Sami, Northern (Finland)' AS name
	UNION SELECT 162 AS idLanguage,'se-NO' AS code,'Sami, Northern (Norway)' AS name
	UNION SELECT 163 AS idLanguage,'se-SE' AS code,'Sami, Northern (Sweden)' AS name
	UNION SELECT 164 AS idLanguage,'si-LK' AS code,'Sinhala (Sri Lanka)' AS name
	UNION SELECT 165 AS idLanguage,'sk-SK' AS code,'Slovak (Slovakia)' AS name
	UNION SELECT 166 AS idLanguage,'sl-SI' AS code,'Slovenian (Slovenia)' AS name
	UNION SELECT 167 AS idLanguage,'sma-NO' AS code,'Sami, Southern (Norway)' AS name
	UNION SELECT 168 AS idLanguage,'sma-SE' AS code,'Sami, Southern (Sweden)' AS name
	UNION SELECT 169 AS idLanguage,'smj-NO' AS code,'Sami, Lule (Norway)' AS name
	UNION SELECT 170 AS idLanguage,'smj-SE' AS code,'Sami, Lule (Sweden)' AS name
	UNION SELECT 171 AS idLanguage,'smn-FI' AS code,'Sami, Inari (Finland)' AS name
	UNION SELECT 172 AS idLanguage,'sms-FI' AS code,'Sami, Skolt (Finland)' AS name
	UNION SELECT 173 AS idLanguage,'sq-AL' AS code,'Albanian (Albania)' AS name
	UNION SELECT 174 AS idLanguage,'sr-Cyrl-BA' AS code,'Serbian (Cyrillic, Bosnia and Herzegovina)' AS name
	UNION SELECT 175 AS idLanguage,'sr-Cyrl-CS' AS code,'Serbian (Cyrillic, Serbia and Montenegro (Former))' AS name
	UNION SELECT 176 AS idLanguage,'sr-Cyrl-ME' AS code,'Serbian (Cyrillic, Montenegro)' AS name
	UNION SELECT 177 AS idLanguage,'sr-Cyrl-RS' AS code,'Serbian (Cyrillic, Serbia)' AS name
	UNION SELECT 178 AS idLanguage,'sr-Latn-BA' AS code,'Serbian (Latin, Bosnia and Herzegovina)' AS name
	UNION SELECT 179 AS idLanguage,'sr-Latn-CS' AS code,'Serbian (Latin, Serbia and Montenegro (Former))' AS name
	UNION SELECT 180 AS idLanguage,'sr-Latn-ME' AS code,'Serbian (Latin, Montenegro)' AS name
	UNION SELECT 181 AS idLanguage,'sr-Latn-RS' AS code,'Serbian (Latin, Serbia)' AS name
	UNION SELECT 182 AS idLanguage,'sv-FI' AS code,'Swedish (Finland)' AS name
	UNION SELECT 183 AS idLanguage,'sv-SE' AS code,'Swedish (Sweden)' AS name
	UNION SELECT 184 AS idLanguage,'sw-KE' AS code,'Kiswahili (Kenya)' AS name
	UNION SELECT 185 AS idLanguage,'syr-SY' AS code,'Syriac (Syria)' AS name
	UNION SELECT 186 AS idLanguage,'ta-IN' AS code,'Tamil (India)' AS name
	UNION SELECT 187 AS idLanguage,'te-IN' AS code,'Telugu (India)' AS name
	UNION SELECT 188 AS idLanguage,'tg-Cyrl-TJ' AS code,'Tajik (Cyrillic, Tajikistan)' AS name
	UNION SELECT 189 AS idLanguage,'th-TH' AS code,'Thai (Thailand)' AS name
	UNION SELECT 190 AS idLanguage,'tk-TM' AS code,'Turkmen (Turkmenistan)' AS name
	UNION SELECT 191 AS idLanguage,'tn-ZA' AS code,'Setswana (South Africa)' AS name
	UNION SELECT 192 AS idLanguage,'tr-TR' AS code,'Turkish (Turkey)' AS name
	UNION SELECT 193 AS idLanguage,'tt-RU' AS code,'Tatar (Russia)' AS name
	UNION SELECT 194 AS idLanguage,'tzm-Latn-DZ' AS code,'Tamazight (Latin, Algeria)' AS name
	UNION SELECT 195 AS idLanguage,'ug-CN' AS code,'Uyghur (PRC)' AS name
	UNION SELECT 196 AS idLanguage,'uk-UA' AS code,'Ukrainian (Ukraine)' AS name
	UNION SELECT 197 AS idLanguage,'ur-PK' AS code,'Urdu (Islamic Republic of Pakistan)' AS name
	UNION SELECT 198 AS idLanguage,'uz-Cyrl-UZ' AS code,'Uzbek (Cyrillic, Uzbekistan)' AS name
	UNION SELECT 199 AS idLanguage,'uz-Latn-UZ' AS code,'Uzbek (Latin, Uzbekistan)' AS name
	UNION SELECT 200 AS idLanguage,'vi-VN' AS code,'Vietnamese (Vietnam)' AS name
	UNION SELECT 201 AS idLanguage,'wo-SN' AS code,'Wolof (Senegal)' AS name
	UNION SELECT 202 AS idLanguage,'xh-ZA' AS code,'isiXhosa (South Africa)' AS name
	UNION SELECT 203 AS idLanguage,'yo-NG' AS code,'Yoruba (Nigeria)' AS name
	UNION SELECT 204 AS idLanguage,'zh-CN' AS code,'Chinese (Simplified, PRC)' AS name
	UNION SELECT 205 AS idLanguage,'zh-HK' AS code,'Chinese (Traditional, Hong Kong S.A.R.)' AS name
	UNION SELECT 206 AS idLanguage,'zh-MO' AS code,'Chinese (Traditional, Macao S.A.R.)' AS name
	UNION SELECT 207 AS idLanguage,'zh-SG' AS code,'Chinese (Simplified, Singapore)' AS name
	UNION SELECT 208 AS idLanguage,'zh-TW' AS code,'Chinese (Traditional, Taiwan)' AS name
	UNION SELECT 209 AS idLanguage,'zu-ZA' AS code,'isiZulu (South Africa)' AS name

) MAIN
WHERE NOT EXISTS (SELECT 1 FROM tblLanguage L WHERE L.idLanguage = MAIN.idLanguage)

SET IDENTITY_INSERT [tblLanguage] OFF