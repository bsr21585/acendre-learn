IF NOT EXISTS (SELECT * FROM sys.fulltext_catalogs WHERE name = N'Asentia-CustomerManager')
CREATE FULLTEXT CATALOG [Asentia-CustomerManager]
	WITH ACCENT_SENSITIVITY = OFF