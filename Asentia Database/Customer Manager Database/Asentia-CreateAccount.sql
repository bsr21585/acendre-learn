DECLARE @company			NVARCHAR(255)
DECLARE @contactFirstName	NVARCHAR(255)
DECLARE @contactLastName	NVARCHAR(255)
DECLARE @contactEmail		NVARCHAR(255)
DECLARE @username			NVARCHAR(255)
DECLARE @password			NVARCHAR(255)
DECLARE @databaseName		NVARCHAR(255)

SET @company			= ''
SET @contactFirstName	= ''
SET @contactLastName	= ''
SET @contactEmail		= ''
SET @username			= ''
SET @password			= ''
SET @databaseName		= 'Asentia-'

INSERT INTO tblAccount (
	idDatabaseServer,
	company,
	contactFirstName,
	contactLastName,
	contactDisplayName,
	contactEmail,
	username,
	[password],
	databaseName,
	dtCreated,
	isActive,
	isDeleted
)
VALUES (
	1,
	@company,
	@contactFirstName,
	@contactLastName,
	@contactLastName + ', ' + @contactFirstName,
	@contactEmail,
	@username,
	dbo.GetHashedString('SHA1', @password),
	@databaseName,
	GETUTCDATE(),
	1,
	0
)

SELECT * FROM tblAccount