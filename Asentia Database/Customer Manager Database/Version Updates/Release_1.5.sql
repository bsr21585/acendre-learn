/*

ADDITIONS TO [tblAccount]

*/

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblAccount' AND column_name = 'isPowerPointJobProcessing')
	ALTER TABLE [tblAccount] ADD [isPowerPointJobProcessing] BIT NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblAccount' AND column_name = 'dtLastPowerPointJobProcessStart')
	ALTER TABLE [tblAccount] ADD [dtLastPowerPointJobProcessStart] DATETIME NULL
GO

IF NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_name = 'tblAccount' AND column_name = 'dtLastPowerPointJobProcessEnd')
	ALTER TABLE [tblAccount] ADD [dtLastPowerPointJobProcessEnd] DATETIME NULL
GO