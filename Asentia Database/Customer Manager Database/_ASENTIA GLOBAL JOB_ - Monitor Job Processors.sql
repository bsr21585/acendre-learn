USE [msdb]
GO

BEGIN TRANSACTION

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'_ASENTIA GLOBAL JOB_ - Monitor Job Processors', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'ICSLEARNING\jcarpenski', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Check for stale job processor jobs and alert]    Script Date: 3/19/2018 11:37:41 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Check for stale job processor jobs and alert', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'IF (
	SELECT COUNT(1)
	FROM [Asentia-CustomerManager].[dbo].tblAccount A
	WHERE A.idAccount > 1
	AND A.isActive = 1
	AND (A.isDeleted = 0 OR A.isDeleted = NULL)
	AND (
		DATEDIFF(MINUTE, ISNULL(A.dtLastJobProcessEnd,''1975-01-01 01:23:45.678''), GETUTCDATE()) >= 15
		OR 
		DATEDIFF(MINUTE, ISNULL(A.dtLastPowerPointJobProcessEnd,''1975-01-01 01:23:45.678''), GETUTCDATE()) >= 15
		)
	) > 0
	BEGIN

	DECLARE @tableHTML NVARCHAR(MAX)

	SET @tableHTML =
		N''<style>'' +
		N''h1 { font-family: Arial; font-size: 16px; font-weight: bold; }'' +
		N''p { font-family: Arial; font-size: 12px; }'' +
		N''table { font-family: Arial; font-size: 12px; }'' +
		N''table th { font-weight: bold; }'' +
		N''</style>'' +
		N''<H1>Accounts with Stale Job Processor States</H1>'' +
		N''<p>The following accounts have not had their Asentia and/or Asentia PowerPoint Processor jobs processed in over 15 minutes. 
		The account''''s processing flags could be stuck or there could be an issue with either the Asentia Job Procesor or Asentia PowerPoint
		Job Processor services.</p>'' +
		N''<table border="1">'' +
		N''<tr>'' +
		N''<th>idAccount</th>'' +
		N''<th>databaseName</th>'' +
		N''<th>isJobProcessing</th>'' +
		N''<th>dtLastJobProcessStart</th>'' +
		N''<th>dtLastJobProcessEnd</th>'' +
		N''<th>isPowerPointJobProcessing</th>'' +
		N''<th>dtLastPowerPointJobProcessStart</th>'' +
		N''<th>dtLastPowerPointJobProcessEnd</th>'' +
		N''</tr>'' +

		CAST ( ( SELECT td = A.idAccount,					'''',  
                    td = A.databaseName,					'''',  
                    td = A.isJobProcessing,					'''',  
                    td = A.dtLastJobProcessStart,			'''',  
                    td = A.dtLastJobProcessEnd,				'''',  
					td = A.isPowerPointJobProcessing,		'''',  
                    td = A.dtLastPowerPointJobProcessStart, '''', 
                    td = A.dtLastPowerPointJobProcessEnd
              FROM [Asentia-CustomerManager].[dbo].tblAccount A  
              WHERE A.idAccount > 1
			  AND A.isActive = 1
			  AND (A.isDeleted = 0 OR A.isDeleted = NULL)
			  AND (
				   DATEDIFF(MINUTE, ISNULL(A.dtLastJobProcessEnd,''1975-01-01 01:23:45.678''), GETUTCDATE()) >= 15
				   OR 
				   DATEDIFF(MINUTE, ISNULL(A.dtLastPowerPointJobProcessEnd,''1975-01-01 01:23:45.678''), GETUTCDATE()) >= 15
				  )  
              FOR XML PATH(''tr''), TYPE   
		) AS NVARCHAR(MAX) ) + 

		N''</table>''

	EXEC msdb.dbo.sp_send_dbmail
	@profile_name = ''Asentia Alerts'',
	@recipients = ''tech@icslearninggroup.com'',
	@subject = ''Stale Job Processor States for Accounts'',
	@body = @tableHTML,
	@body_format = ''HTML''

	END', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every 15 minutes', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=15, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20160915, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'4f6c2cb5-f931-4a32-a639-3856404c79af'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


