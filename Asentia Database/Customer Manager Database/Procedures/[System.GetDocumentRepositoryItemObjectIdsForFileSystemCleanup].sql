
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetDocumentRepositoryItemObjectIdsForFileSystemCleanup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetDocumentRepositoryItemObjectIdsForFileSystemCleanup]
GO


/*

Get Object IDs of the objects in the database

*/
CREATE PROCEDURE [dbo].[System.GetDocumentRepositoryItemObjectIdsForFileSystemCleanup]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@DBServerName			NVARCHAR(50),
	@DatabaseName			NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	-- Create temp table to hold object IDs
	CREATE TABLE #ObjectIDs
	   (
		  [objectId] INT NOT NULL,
		  [objectName] NVARCHAR(100) NOT NULL,
		  [siteHostname] NVARCHAR(100) NOT NULL
	   )
	  

		-- add object ids to the temp table
				
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		DRI.idObject,'
				+ '		CASE WHEN DRI.idDocumentRepositoryObjectType = 1 THEN ''group'''
				+ '		     WHEN DRI.idDocumentRepositoryObjectType = 2 THEN ''course'''
				+ '			 WHEN DRI.idDocumentRepositoryObjectType = 3 THEN ''learningpath'''
				+ '			 ELSE ''user'' END,'
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].[' + @DatabaseName + '].[dbo].' + '[tblDocumentRepositoryItem] DRI'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = DRI.idSite'
		
		EXEC (@sql)
	  
		
		SELECT objectId, objectName, siteHostname FROM [#ObjectIDs]
		
		
		-- Drop temporary object IDs table
		
		DROP TABLE #ObjectIDs
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemGetDocumentRepositoryItemObjectIdsForFileSystemCleanup_Success'
		
	SET XACT_ABORT OFF

END
GO