SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.PurchaseTransaction]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.PurchaseTransaction]
GO


/*

CLONE PORTAL - PURCHASE / TRANSACTION DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.PurchaseTransaction]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.0: Copy PurchaseTransaction - Initialized', 0)
    
    BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')	 

		-- copy purchases
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblPurchase] '
	   			 + '( '
	   			 + '	idUser, '
	   			 + '	orderNumber, '
	   			 + '	[timeStamp], '
	   			 + '	ccnum, '
	   			 + '	currency, '
	   			 + '	idSite,	'
	   			 + '	dtPending, '
	   			 + '	failed, '
	   			 + '	transactionId '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTU.destinationId, '
	   			 + '	TP.orderNumber, '
	   			 + '	TP.[timeStamp], '
	   			 + '	TP.ccnum, '
	   			 + '	TP.currency, '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TP.dtPending, '
	   			 + '	TP.failed, '
	   			 + '	TP.transactionId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblPurchase] TP '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = TP.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE TP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   								
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.1: Copy PurchaseTransaction - Purchases Inserted', 0)
	   
		-- insert the mapping for source to destination purchase ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SP.idPurchase, '
	   			 + '	DP.idPurchase, '
	   			 + '	''purchase'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblPurchase] DP '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblPurchase] SP '
	   			 + 'ON (SP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SP.orderNumber = DP.orderNumber ) '
	   			 + '	AND (SP.timeStamp = DP.timeStamp OR (SP.timeStamp IS NULL AND DP.timeStamp IS NULL)) '
	   			 + '	AND (SP.dtPending = DP.dtPending OR (SP.dtPending IS NULL AND DP.dtPending IS NULL))) '
	   			 + 'WHERE DP.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.2: Copy PurchaseTransaction - Purchase Mappings Created', 0)

		-- insert records from source site to destination table TransactionItem
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblTransactionItem] '
	   			 + '( '
	   			 + '	idParentTransactionItem, '
	   			 + '	idPurchase, '
	   			 + '	idEnrollment, '
	   			 + '	idUser, '
	   			 + '	userName, '
	   			 + '	idAssigner, '
	   			 + '	assignerName, '
	   			 + '	itemId, '
	   			 + '	itemName, '
	   			 + '	itemType, '
	   			 + '	description, '
	   			 + '	cost, '
	   			 + '	paid, '
	   			 + '	idCouponCode, '
	   			 + '	couponCode, '
	   			 + '	dtAdded, '
	   			 + '	confirmed, '
	   			 + '	idSite, '
				 + '	idIltSession '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	NULL, '     -- this null value of idParentTransactionItem will be updated from temporary TransactionItemIdMappings table below
	   			 + '	TTP.destinationId, '	   			 
	   			 + '	CASE '
	   			 + '	WHEN TI.itemType = 1 THEN '
	   			 + '		TTE.destinationId '
	   			 + '	WHEN TI.itemType = 3 THEN '
	   			 + '		TTLPE.destinationId '
	   			 + '	ELSE '
	   			 + '		NULL '
	   			 + '	END, '
	   			 + '	TTU.destinationId, '
	   			 + '	TI.userName, '
	   			 + '	TTUA.destinationId, '
	   			 + '	TI.assignerName, '	   			 
	   			 + '	CASE '
	   			 + '	WHEN TI.itemType = 1 THEN '
	   			 + '		TTC.destinationId '
	   			 + '	WHEN TI.itemType = 2 THEN '
	   			 + '		TTCA.destinationId '
	   			 + '	WHEN TI.itemType = 3 THEN '
	   			 + '		TTLP.destinationId '
	   			 + '	WHEN TI.itemType = 4 THEN '
	   			 + '		TTST.destinationId '
	   			 + '	WHEN TI.itemType = 5 THEN '
	   			 + '		TTG.destinationId '
	   			 + '	ELSE '
	   			 + '		NULL '
	   			 + '	END, '
				 + '	TI.itemName  + '' ##'' + CAST(TI.idTransactionItem AS NVARCHAR) + ''##'', '
	   			 + '	TI.itemType, '
	   			 + '	TI.description, '
	   			 + '	TI.cost, '
	   			 + '	TI.paid, '
	   			 + '	TTCC.destinationId, '
	   			 + '	TI.couponCode, '
	   			 + '	TI.dtAdded, '
	   			 + '	TI.confirmed, '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTSTI.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblTransactionItem] TI '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTP on (TTP.sourceId = TI.idPurchase AND TTP.object = ''purchase'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TI.itemType = 1 AND TTC.sourceId = TI.itemId AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTE on (TI.itemType = 1 AND TTE.sourceId = TI.idEnrollment AND TTE.object = ''enrollment'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCA on (TI.itemType = 2 AND TTCA.sourceId = TI.itemId AND TTCA.object = ''catalog'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TI.itemType = 3 AND TTLP.sourceId = TI.itemId AND TTLP.object = ''learningPaths'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLPE on (TI.itemType = 3 AND TTLPE.sourceId = TI.idEnrollment AND TTLPE.object = ''learningpathenrollment'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST on (TI.itemType = 4 AND TTST.sourceId = TI.itemId AND TTST.object = ''standuptraining'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TI.itemType = 5 AND TTG.sourceId = TI.itemId AND TTG.object = ''groups'') '	   			 
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = TI.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUA on (TTUA.sourceId = TI.idAssigner AND TTUA.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC on (TTCC.sourceId = TI.idCouponCode AND TTCC.object = ''couponcode'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI on (TTSTI.sourceId = TI.idIltSession AND TTSTI.object = ''session'') '
	   			 + 'WHERE TI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.3: Copy PurchaseTransaction - Transaction Items Inserted', 0)
	   
		-- insert the mapping for source to destination transaction item ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	STI.[idTransactionItem], '
	   			 + '	DTI.[idTransactionItem], '
	   			 + '	''transactionitem'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblTransactionItem] DTI '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblTransactionItem] STI '
	   			 + 'ON (STI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND STI.itemName + '' ##'' + CAST(STI.idTransactionItem AS NVARCHAR) + ''##'' = DTI.itemName) '
	   			 + 'WHERE DTI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.4: Copy PurchaseTransaction - Transaction Item Mappings Created', 0)

		-- update idParentTransactionItem from Temp Table TransactionItem
		SET @sql = 'UPDATE DTI '
	   			 + '	SET idParentTransactionItem = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblTransactionItem] STI '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblTransactionItem] DTI '
	   			 + 'ON (STI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND STI.itemName + '' ##'' + CAST(STI.idTransactionItem AS NVARCHAR) + ''##'' = DTI.itemName) '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = STI.idParentTransactionItem AND TEMP.object = ''transactionitem'') '
	   			 + 'WHERE DTI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND STI.idParentTransactionItem IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.5: Copy PurchaseTransaction - Transaction Item Parent Ids Updated', 0)

		-- clean up the ##idTransactionItem## additions we made to transaction item names, we did that to uniquely distinguish transaction items
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblTransactionItem] SET '
				 + '	itemName = REPLACE(DTI.itemName, '' ##'' + CAST(STI.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblTransactionItem] DTI '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] STI ON STI.destinationID = DTI.idTransactionItem AND STI.object = ''transactionitem'' '
				 + 'WHERE DTI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND STI.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14.6: Copy PurchaseTransaction - Transaction Item Names Cleaned Up', 0)

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_PurchaseTransaction_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 14: Copy PurchaseTransaction - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_PurchaseTransaction_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END