-- =====================================================================
-- PROCEDURE: [AccountToDomainAliasLink.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[AccountToDomainAliasLink.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountToDomainAliasLink.Save]
GO

/*

Insert account to domain alis link

*/

CREATE PROCEDURE [AccountToDomainAliasLink.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@hostName				NVARCHAR (255),
	@idAccount				INT		
		
)
AS

	BEGIN
	SET NOCOUNT ON



	

	/*
	
    Save site to domain alis link
	
	*/
	
		INSERT INTO tblAccountToDomainAliasLink (
			idAccount,
			hostname,
			domain
		)   
		SELECT
			@idAccount,
			@hostname,
			NULL
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblAccountToDomainAliasLink ATDAL
			WHERE ATDAL.idAccount = @idAccount
			AND ATDAL.hostname = @hostName
		)

		
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO