-- =====================================================================
-- PROCEDURE: [Account.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[Account.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.Details]
GO

/*
Return all the properties for a given object id.
*/
CREATE PROCEDURE [Account.Details]
(
	@Return_Code		INT					OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount	INT					= 0, --default if not specified
	@callerLangString	NVARCHAR (10),
	@idCaller			INT,
	
	@idAccount			INT					OUTPUT, 
	@idDatabaseServer	INT					OUTPUT, 
	@company			NVARCHAR(255)		OUTPUT,
	@contactFirstName	NVARCHAR(255)		OUTPUT,
	@contactLastName	NVARCHAR(255)		OUTPUT,
	@contactDisplayName	NVARCHAR(512)		OUTPUT,
	@contactEmail		NVARCHAR(255)		OUTPUT,
	@username			NVARCHAR(512)		OUTPUT,
	@password			NVARCHAR(512)		OUTPUT,
	@isActive			BIT					OUTPUT,
	@isDeleted			BIT					OUTPUT,
	@databaseName		NVARCHAR(512)		OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* validate caller permission */
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller		
	
	IF @idCallerRole <> 1 AND @idCallerAccount <> @idAccount
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
		
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccount
		WHERE idAccount = @idAccount
		) = 0 
		BEGIN
		SET @Return_Code = 1
		RETURN 1
		END
	
	/*
	
	get the data 
	
	*/
	
	SELECT
		@idAccount = A.idAccount, 
		@idDatabaseserver = A.idDatabaseServer, 
		@company = A.company,
		@contactFirstName = A.contactFirstName,
		@contactLastName = A.contactLastName,
		@contactDisplayName = A.contactDisplayName,
		@contactEmail = A.contactEmail,
		@username = A.username,
		@password = A.[password],
		@isActive = A.isActive,
		@isDeleted = A.isDeleted,
		@databaseName = A.databaseName
	FROM tblAccount A
	WHERE A.idAccount = @idAccount
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO