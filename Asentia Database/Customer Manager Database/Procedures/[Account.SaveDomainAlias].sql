-- =====================================================================
-- PROCEDURE: [Account.SaveDomainAlias]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.SaveDomainAlias]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.SaveDomainAlias]
GO

/*

Insert account to domain alis link

*/

CREATE PROCEDURE [Account.SaveDomainAlias]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@domainAlias			NVARCHAR (255),
	@idAccount				INT		
		
)
AS

	BEGIN
	SET NOCOUNT ON
	


	
	/*
	validate uniqueness for domain alias, if specified
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccountToDomainAliasLink ATDAL
		WHERE ( ATDAL.domain = @domainAlias
		OR ATDAL.hostname = @domainAlias)
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'AccountSaveDomainAlias_DomainAliasNotUnique'		 
		RETURN 1 
		END
	

	/*
	
    Save site to domain alis link
	
	*/
	
		INSERT INTO tblAccountToDomainAliasLink (
			idAccount,
			hostname,
			domain
		)   
		SELECT
			@idAccount,
			NULL,
			@domainAlias
		WHERE NOT EXISTS (
			SELECT 1
			FROM tblAccountToDomainAliasLink ATDAL
			WHERE ATDAL.idAccount = @idAccount
			AND ATDAL.domain = @domainAlias
		)
		
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO