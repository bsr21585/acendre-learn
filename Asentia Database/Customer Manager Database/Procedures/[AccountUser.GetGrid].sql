-- =====================================================================
-- PROCEDURE: [AccountUser.GetGrid]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[AccountUser.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.GetGrid]
GO

CREATE PROCEDURE [AccountUser.GetGrid]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(255)	OUTPUT,
	@idCallerAccount			INT,
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,			-- this is the ID of the user making the grid call.
	@idRole                     INT              =0,
	@searchParam				NVARCHAR(4000),
	@pageNum					INT, 
	@pageSize					INT, 
	@orderColumn				NVARCHAR(255),
	@orderAsc					BIT
)
AS
	BEGIN
		SET NOCOUNT ON

		DECLARE   @IDTable AS IDTable
		IF (@idRole=0)
			BEGIN
		    INSERT INTO @IDTable
		    SELECT idRole FROM tblRole 
			END
		ELSE
			BEGIN
			INSERT INTO @IDTable
			SELECT idRole FROM tblRole WHERE idRole=@idRole 
			END
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = NULL
			END

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblAccountUser AU
			
			WHERE 
				(
				((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
				OR
				((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
				OR
				(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
				)
			AND AU.idAccountUser <> 1 -- not the system admin user
			AND AU.idRole IN (SELECT id FROM @IDTable)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						AU.idAccountUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END DESC,
							-- FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END DESC,
							-- FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END DESC,
							-- SIXTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END,
							-- FOURTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END,
							-- FIFTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END,
							-- SIXTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END
						)
						AS [row_number]
					FROM tblAccountUser AU
					JOIN tblRole R ON R.idRole = AU.idRole
					JOIN tblAccount A ON A.idAccount = AU.idAccount
					WHERE 
						(
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
						OR 
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
						OR
						(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
						)
					AND AU.idAccountUser <> 1 -- not the system admin user
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idAccountUser, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				AU.idAccountUser, 
				AU.displayName,
				AU.username, 
				AU.email,
				CASE WHEN A.username = '__SYSTEM__' THEN '' ELSE A.username END AS accountName,
				R.roleName,
				AU.isActive,
				SelectedKeys.[row_number],
				'True' AS isModifyOn
			FROM SelectedKeys
			JOIN tblAccountUser AU ON AU.idAccountUser = SelectedKeys.idAccountUser
			JOIN tblRole R ON R.idRole = AU.idRole
			JOIN tblAccount A ON A.idAccount = AU.idAccount  WHERE  AU.idRole IN (SELECT id FROM @IDTable)
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			SELECT COUNT(1) AS row_count FROM
			(
				SELECT idAccountUser
				FROM tblAccountUser AU
				JOIN tblRole R ON R.idRole = AU.idRole
				INNER JOIN CONTAINSTABLE(tblAccountUser, *, @searchParam) K ON K.[key] = AU.idAccountUser
				WHERE 
					(
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
					OR 
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
					OR
					(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
					)
				AND AU.idAccountUser <> 1 -- not the system admin user
				AND AU.idRole IN (SELECT id FROM @IDTable)
				UNION
				
				SELECT idAccountUser
				FROM tblAccountUser AU
				INNER JOIN CONTAINSTABLE(tblAccount, *, @searchParam) K ON K.[key] = AU.idAccount
				WHERE 
					(
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
					OR 
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
					OR
					(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
					)
				AND AU.idAccountUser <> 1 -- not the system admin user
				
				UNION
			
				SELECT idAccountUser
				FROM tblAccountUser AU
				INNER JOIN CONTAINSTABLE(tblRole, *, @searchParam) K ON K.[key] = AU.idRole
				WHERE 
					(
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
					OR 
					((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
					OR
					(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
					)
				AND AU.idAccountUser <> 1 -- not the system admin user
			) 
			AS rowCountRS
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						AU.idAccountUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END DESC,
							-- FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END DESC,
							-- FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END DESC,
							-- SIXTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END,
							-- FOURTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END,
							-- FIFTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END,
							-- SIXTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END
						)
						AS [row_number]
					FROM tblAccountUser AU
					JOIN tblRole R ON R.idRole = AU.idRole
					JOIN tblAccount A ON A.idAccount = AU.idAccount
					INNER JOIN CONTAINSTABLE(tblAccountUser, *, @searchParam) K ON K.[key] = AU.idAccountUser
					WHERE 
						(
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
						OR 
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
						OR
						(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
						)
					AND AU.idAccountUser <> 1 -- not the system admin user
					
					UNION
					
					SELECT TOP (@pageNum * @pageSize) 
						AU.idAccountUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END DESC,
							-- FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END DESC,
							-- FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END DESC,
							-- SIXTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END,
							-- FOURTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END,
							-- FIFTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END,
							-- SIXTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END
						)
						AS [row_number]
					FROM tblAccountUser AU
					JOIN tblRole R ON R.idRole = AU.idRole
					JOIN tblAccount A ON A.idAccount = AU.idAccount
					INNER JOIN CONTAINSTABLE(tblAccount, *, @searchParam) K ON K.[key] = AU.idAccount
					WHERE 
						(
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
						OR 
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
						OR
						(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
						)
					AND AU.idAccountUser <> 1 -- not the system admin user
					
					UNION
					
					SELECT TOP (@pageNum * @pageSize) 
						AU.idAccountUser,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							-- SECOND ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							-- THIRD ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END DESC,
							-- FOURTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END DESC,
							-- FIFTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END DESC,
							-- SIXTH ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'username' THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'email' THEN AU.email END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'accountName' THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'roleName' THEN R.roleName END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							-- SECOND ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.lastname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							-- THIRD ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email', 'accountName', 'roleName') THEN AU.firstname END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName', 'roleName') THEN A.username END) END,
							-- FOURTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('username', 'email') THEN A.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email') THEN AU.username END) END,
							-- FIFTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IN ('email') THEN AU.username END) END,
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('email') THEN AU.email END) END,
							-- SIXTH ORDER ASC
							CASE WHEN @orderAsc = 1 THEN (CASE WHEN @orderColumn IS NULL OR @orderColumn NOT IN ('username', 'email', 'accountName') THEN R.roleName END) END
						)
						AS [row_number]
					FROM tblAccountUser AU
					JOIN tblRole R ON R.idRole = AU.idRole
					JOIN tblAccount A ON A.idAccount = AU.idAccount
					INNER JOIN CONTAINSTABLE(tblRole, *, @searchParam) K ON K.[key] = AU.idRole
					WHERE 
						(
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller = 1) 
						OR 
						((@idCallerAccount IS NULL OR @idCallerAccount = 1) AND @idCaller > 1 AND AU.idAccount > 1) 
						OR
						(@idCallerAccount > 1 AND @idCaller = 1 AND AU.idAccount = @idCallerAccount)
						)
					AND AU.idAccountUser <> 1 -- not the system admin user
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idAccountUser, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				AU.idAccountUser, 
				AU.displayName,
				AU.username, 
				AU.email,
				CASE WHEN A.username = '__SYSTEM__' THEN '' ELSE A.username END AS accountName,
				R.roleName,
				AU.isActive,
				SelectedKeys.[row_number],
				'True' as isModifyOn
			FROM SelectedKeys
			JOIN tblAccountUser AU ON AU.idAccountUser = SelectedKeys.idAccountUser
			JOIN tblRole R ON R.idRole = AU.idRole
			JOIN tblAccount A ON A.idAccount = AU.idAccount WHERE AU.idRole IN (SELECT id FROM @IDTable)
			ORDER BY SelectedKeys.[row_number]
			
			END
		
	END
GO


