SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.ContentResource]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.ContentResource]
GO


/*

CLONE PORTAL - CONTENT PACKAGE / RESOURCE / QUIZSURVEY DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.ContentResource]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,			-- needed for quiz/survey and resource tables	
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@idAccount				INT				= NULL
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.0: Copy ContentResource - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)
		DECLARE @subSql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		QUIZ/SURVEY

		*/

		-- copy quizes and surveys
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblQuizSurvey] '
				 + '( '
				 + '	idSite, '
				 + '	[type], '
				 + '	identifier, '
				 + '	[guid], '
				 + '	data, '
				 + '	isDraft, '
				 + '	idContentPackage, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	idAuthor '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	QS.[type], '
				 + '	QS.identifier, '
				 + '	QS.[guid], '
				 + '	QS.data, '
				 + '	QS.isDraft, '
				 + '	NULL, '
				 + '	QS.dtCreated, '
				 + '	QS.dtModified, '

		IF(@fullCopyFlag = 1) -- idAuthor is available for full copy only, because tblUser moved only for full copy
		
			BEGIN
				
			SET @subSql = '	TTU.destinationId '
						+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblQuizSurvey] QS '
                		+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = QS.idAuthor AND TTU.object = ''users'') '
						+ 'WHERE QS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
			END

		ELSE
			
			BEGIN -- otherwise 1 is inserted in this field as administrator id
		
			SET @subSql = '	1 '
						+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblQuizSurvey] QS '
                		+ 'WHERE QS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
			END
	
		SET @sql = @sql + @subSql

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.1: Copy ContentResource - Quizzes/Surveys Inserted', 0)

		-- insert the mapping for source to destination quiz/survey ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idQuizSurvey, '
	   			 + '	DST.idQuizSurvey, '
	   			 + '	''quizsurvey'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblQuizSurvey] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblQuizSurvey] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.guid = DST.guid) '
	   			 + '	AND (SRC.dtCreated = DST.dtCreated)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.2: Copy ContentResource - Quiz/Survey Mappings Created', 0)

		-- copy content packages
		DECLARE @idSourceAccount INT

		IF (@sourceDBName = @destinationDBName) 
			BEGIN
			SET @idSourceAccount = @idAccount
			END
		ELSE
			BEGIN
			SET @idSourceAccount = (SELECT DISTINCT TOP 1 idAccount FROM tblAccount where databaseName = @sourceDBName)
			END

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblContentPackage] '
				 + '( '
				 + '	idSite, '
				 + '	name, '
				 + '	[path], '
				 + '	kb, '
				 + '	idContentPackageType, '
				 + '	idSCORMPackageType, '
				 + '	manifest, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isMediaUpload, '
				 + '	idMediaType, '
				 + '	contentTitle, '
				 + '	originalMediaFilename, '
				 + '	isVideoMedia3rdParty, '
				 + '	videoMediaEmbedCode, '
				 + '	enableAutoplay, '
				 + '	allowRewind, '
				 + '	allowFastForward, '
				 + '	allowNavigation, '
				 + '	allowResume, '
				 + '	minProgressForCompletion, '
				 + '	isProcessing, '
				 + '	isProcessed, '
				 + '	dtProcessed, '
				 + '	idQuizSurvey, '
				 + '	hasManifestComplianceErrors, '
				 + '	manifestComplianceErrors, '
				 + '	openSesameGUID '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	CP.name, '
				 + '	REPLACE(CP.[path], ''' + CAST(@idSourceAccount AS NVARCHAR) + '-' + CAST(@idSiteSource AS NVARCHAR) + '-'', ''' + CAST(@idAccount AS NVARCHAR) + '-' + CAST(@idSiteDestination AS NVARCHAR) + '-''), '
				 + '	CP.kb, '
				 + '	CP.idContentPackageType, '
				 + '	CP.idSCORMPackageType, '
				 + '	CP.manifest, '
				 + '	CP.dtCreated, '
				 + '	CP.dtModified, '
				 + '	CP.isMediaUpload, '
				 + '	CP.idMediaType, '
				 + '	CP.contentTitle, '
				 + '	CP.originalMediaFilename, '
				 + '	CP.isVideoMedia3rdParty, '
				 + '	CP.videoMediaEmbedCode, '
				 + '	CP.enableAutoplay, '
				 + '	CP.allowRewind, '
				 + '	CP.allowFastForward, '
				 + '	CP.allowNavigation, '
				 + '	CP.allowResume, '
				 + '	CP.minProgressForCompletion, '
				 + '	CP.isProcessing, '
				 + '	CP.isProcessed, '
				 + '	CP.dtProcessed, '
				 + '	TTQS.destinationId, '
				 + '	CP.hasManifestComplianceErrors, '
				 + '	CP.manifestComplianceErrors, '
				 + '	CP.openSesameGUID '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblContentPackage] CP '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTQS on (TTQS.sourceId = CP.idQuizSurvey AND TTQS.object = ''quizsurvey'') '
				 + 'WHERE CP.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		 
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.3: Copy ContentResource - Content Packages Inserted', 0)
	   
		-- insert the mapping for source to destination content package ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idContentPackage, '
				 + '	DST.idContentPackage, '
				 + '	''contentpackage'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblContentPackage] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblContentPackage] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SRC.name = DST.name) '
				 + '	AND (SRC.dtCreated = DST.dtCreated ) '
				 + '	AND (SRC.dtModified = DST.dtModified )) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.4: Copy ContentResource - Content Package Mappings Created', 0)
	   
		-- there is cross dependency between tblContentPackage and tblQuizSurvey, so we need to update tblQuizSurvey
		-- and set idContentPackage after we have moved tblContentPackage data
		SET @sql = 'UPDATE DSTQS '
				 + '	SET idContentPackage = TEMP.destinationId '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblQuizSurvey] SRCQS '
				 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblQuizSurvey] DSTQS '
				 + 'ON (SRCQS.guid = DSTQS.guid '
				 + '	AND (SRCQS.dtCreated = DSTQS.dtCreated) '
				 + '	AND SRCQS.idSite =' + CAST(@idSiteSource AS NVARCHAR) + ') '
				 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
				 + 'ON (TEMP.sourceId = SRCQS.idContentPackage AND TEMP.object = ''contentpackage'') '
				 + 'WHERE DSTQS.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SRCQS.idContentPackage IS NOT NULL '

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.5: Copy ContentResource - Content Package Quiz/Survey Ids Updated', 0)
		
		-- copy resource types
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceType] '
	   			 + '( '
				 + '	idSite, '
				 + '	resourceType, '
				 + '	isDeleted, '
				 + '	dtDeleted '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	RT.resourceType, '
				 + '	RT.isDeleted, '
				 + '	RT.dtDeleted '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceType] RT '
				 + 'WHERE RT.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
                										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.6: Copy ContentResource - Resource Types Inserted', 0)
	   
		-- insert the mapping for source to destination resource type ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idResourceType, '
				 + '	DST.idResourceType, '
				 + '	''resourcetype'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceType] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceType] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SRC.resourceType = DST.resourceType) '
				 + '	AND (SRC.isDeleted = DST.isDeleted) '
				 + '	AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.7: Copy ContentResource - Resource Types Mapped', 0)
	   			   
		-- copy resource type languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceTypeLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idResourceType, '
	   			 + '	idLanguage, '
	   			 + '	resourceType '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTRT.destinationId, '
	   			 + '	RTL.idLanguage, '
	   			 + '	RTL.resourceType '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceTypeLanguage] RTL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRT ON (TTRT.sourceId = RTL.idResourceType AND TTRT.object = ''resourcetype'') '
	   			 + 'WHERE RTL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)	   						
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.8: Copy ContentResource - Resource Type Languages Inserted', 0)

		-- copy resources
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResource] '
				 + '( '
				 + '	idSite, '
				 + '	name, '
				 + '	idParentResource, '
				 + '	idOwner, '
				 + '	idResourceType, '
				 + '	[description], '
				 + '	identifier, '
				 + '	isMoveable, '
				 + '	isAvailable, '
				 + '	capacity, '
				 + '	locationRoom, '
				 + '	locationBuilding, '
				 + '	locationCity, '
				 + '	locationProvince, '
				 + '	locationCountry, '
				 + '	ownerWithinSystem, '
				 + '	ownerName, '
				 + '	ownerEmail, '
				 + '	ownerContactInformation, '
				 + '	isDeleted, '
				 + '	dtDeleted '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	R.name, '
				 + '	NULL, ' -- idParentResource will be updated from temporary ResourceIdMappings table below

		IF(@fullCopyFlag = 1) -- idOwner is available for full copy only, because tblUser moved only for full copy
		
			BEGIN

			SET @sql = @sql + '	TTU.destinationId, ' -- owner

			END

		ELSE

			BEGIN

			SET @sql = @sql + ' 1, '

			END

		SET @sql = @sql + '	TTRT.destinationId, '
						+ '	R.[description], '
						+ '	R.identifier, '
						+ '	R.isMoveable, '
						+ '	R.isAvailable, '
						+ '	R.capacity, '
						+ '	R.locationRoom, '
						+ '	R.locationBuilding, '
						+ '	R.locationCity, '
						+ '	R.locationProvince, '
						+ '	R.locationCountry, '
						+ '	R.ownerWithinSystem, '
						+ '	R.ownerName, '
						+ '	R.ownerEmail, '
						+ '	R.ownerContactInformation, '
						+ '	R.isDeleted, '
						+ '	R.dtDeleted '
						+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResource] R '
						+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRT on (TTRT.sourceId = R.idResourceType AND TTRT.object = ''resourcetype'') '

		IF(@fullCopyFlag = 1) -- idOwner is available for full copy only, because tblUser moved only for full copy

			BEGIN

			SET @sql = @sql + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = R.idOwner AND TTU.object = ''users'') '

			END

		SET @sql = @sql + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR)		
     										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.9: Copy ContentResource - Resources Inserted', 0)
	   
		-- insert the mapping for source to destination resource ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idResource, '
	   			 + '	DST.idResource, '
	   			 + '	''resource'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResource] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResource] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.name = DST.name) '
	   			 + '	AND (SRC.locationRoom = DST.locationRoom OR (SRC.locationRoom IS NULL AND DST.locationRoom IS NULL)) '
	   			 + '	AND (SRC.locationBuilding = DST.locationBuilding OR (SRC.locationBuilding IS NULL AND DST.locationBuilding IS NULL)) '
	   			 + '	AND (SRC.locationCity = DST.locationCity OR (SRC.locationCity IS NULL AND DST.locationCity IS NULL)) '
	   			 + '	AND (SRC.ownerEmail = DST.ownerEmail OR (SRC.ownerEmail IS NULL AND DST.ownerEmail IS NULL)) '
	   			 + '	AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.10: Copy ContentResource - Resource Mappings Created', 0)
	   
		-- update idParentResource
		SET @sql = 'UPDATE DSTR '
	   			 + '	SET idParentResource = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResource] SRCR '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResource] DSTR '
	   			 + 'ON (SRCR.name = DSTR.name AND (SRCR.ownerEmail = DSTR.ownerEmail OR (SRCR.ownerEmail IS NULL AND DSTR.ownerEmail IS NULL)) '
	   		  	 + '	AND (SRCR.locationRoom = DSTR.locationRoom OR (SRCR.locationRoom IS NULL AND DSTR.locationRoom IS NULL)) '
	   		  	 + '	AND (SRCR.locationBuilding = DSTR.locationBuilding OR (SRCR.locationBuilding IS NULL AND DSTR.locationBuilding IS NULL)) '
	   		  	 + '	AND (SRCR.locationCity = DSTR.locationCity OR (SRCR.locationCity IS NULL AND DSTR.locationCity IS NULL)) '
	   		  	 + '	AND SRCR.idSite =' + CAST(@idSiteSource AS NVARCHAR) + ') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCR.idParentResource AND TEMP.object = ''resource'') '
	   			 + 'WHERE DSTR.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SRCR.idParentResource IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.11: Copy ContentResource - Resource Parent Ids Updated', 0)
			   
		-- copy resource languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idResource, '
	   			 + '	idLanguage, '
	   			 + '	name, '
	   			 + '	[description] '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	RL.idLanguage, '
	   			 + '	RL.name, '
	   			 + '	RL.[description] '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceLanguage] RL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RL.idResource AND TTR.object = ''resource'') '
	   			 + 'WHERE RL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 			
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5.12: Copy ContentResource - Resource Languages Inserted', 0)

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_ContentResource_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 5: Copy ContentResource - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_ContentResource_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF		

END