
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT				OUTPUT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT				OUTPUT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	  
	/*
	  
	create id mappings table or clear out existing id mappings table

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_IDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL,'
			+ '    [object] NVARCHAR(255)'
			+ ')'
			+ ' END '
			+ ' ELSE '
	SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'

	EXEC(@sql)
	  
	/*
	  
	create timezone mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_TimezoneIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '
			+ ' (1, 1), '
			+ ' (2, 102), '
			+ ' (3, 3), '
			+ ' (4, 4), '
			+ ' (5, 6), '
			+ ' (6, 5), '
			+ ' (7, 7), '
			+ ' (8, 8), '
			+ ' (9, 8), '
			+ ' (10, 9), '
			+ ' (11, 10), '
			+ ' (12, 11), '
			+ ' (13, 12), '
			+ ' (14, 12), '
			+ ' (15, 13), '
			+ ' (16, 14), '
			+ ' (17, 15), '
			+ ' (18, 16), '
			+ ' (19, 17), '
			+ ' (20, 19), '
			+ ' (21, 21), '
			+ ' (22, 21), '
			+ ' (23, 22), '
			+ ' (24, 23), '
			+ ' (25, 24), '
			+ ' (26, 25), '
			+ ' (27, 27), '
			+ ' (28, 28), '
			+ ' (29, 31), '
			+ ' (30, 32), '
			+ ' (31, 33), '
			+ ' (32, 34), '
			+ ' (33, 36), '
			+ ' (34, 38), '
			+ ' (35, 39), '
			+ ' (36, 40), '
			+ ' (37, 41), '
			+ ' (38, 43), '
			+ ' (39, 54), '
			+ ' (40, 45), '
			+ ' (41, 46), '
			+ ' (42, 47), '
			+ ' (43, 50), '
			+ ' (44, 51), '
			+ ' (45, 53), '
			+ ' (46, 56), '
			+ ' (47, 44), '
			+ ' (48, 55), '
			+ ' (49, 57), '
			+ ' (50, 62), '
			+ ' (51, 58), '
			+ ' (52, 64), '
			+ ' (53, 59), '
			+ ' (54, 60), '
			+ ' (55, 61), '
			+ ' (56, 65), '
			+ ' (57, 65), '
			+ ' (58, 66), '
			+ ' (59, 74), '
			+ ' (60, 68), '
			+ ' (61, 69), '
			+ ' (62, 70), '
			+ ' (63, 71), '
			+ ' (64, 77), '
			+ ' (65, 72), '
			+ ' (66, 75), '
			+ ' (67, 76), '
			+ ' (68, 79), '
			+ ' (69, 78), '
			+ ' (70, 83), '
			+ ' (71, 80), '
			+ ' (72, 81), '
			+ ' (73, 82), '
			+ ' (74, 85), '
			+ ' (75, 86), '
			+ ' (76, 93), '
			+ ' (77, 87), '
			+ ' (78, 88), '
			+ ' (79, 89), '
			+ ' (80, 90), '
			+ ' (81, 91), '
			+ ' (82, 92), '
			+ ' (83, 95), '
			+ ' (84, 99), '
			+ ' (85, 96), '
			+ ' (86, 98), '
			+ ' (87, 101) '
			+ ' END '		

	EXEC(@sql)

	/*
	  
	create event type mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EventTypeIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '			
			+ ' (1, 702), '		-- Certificate Awarded
			+ ' (2, 701), '		-- Certificate Earned
			+ ' (3, 703), '		-- Certificate Revoked
			+ ' (4, 0), '		-- Purchase Confirmed - NOT IN ASENTIA
			+ ' (5, 206), '		-- Enrollment Start
			+ ' (6, 205), '		-- Enrollment End
			+ ' (7, 203), '		-- Enrollment Revoked
			+ ' (8, 204), '		-- Enrollment Due
			+ ' (9, 301), '		-- Lesson Passed
			+ ' (10, 302), '	-- Lesson Failed
			+ ' (11, 303), '	-- Lesson Completed
			+ ' (14, 101), '	-- User Created
			+ ' (15, 102), '	-- User Deleted
			+ ' (16, 402), '	-- Session Instructor Assigned or Changed
			+ ' (17, 403), '	-- Session Instructor Removed
			+ ' (18, 404), '	-- Learner Joined to Session
			+ ' (19, 405), '	-- Learner Dropped From Session
			+ ' (20, 406), '	-- Session Time or Location Changed
			+ ' (21, 205), '	-- Enrollment Completed
			+ ' (22, 103), '	-- User Account Expires
			+ ' (24, 401), '	-- Session Meets
			+ ' (25, 704) '		-- Certificate Expires
			+ ' END '		

	EXEC(@sql)

	/*
	  
	create event type recipient mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EventTypeRecipientIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '
			+ ' (1, 1), '
			+ ' (3, 2), '
			+ ' (4, 1), '
			+ ' (5, 2), '
			+ ' (6, 1), '
			+ ' (7, 2), '
			+ ' (8, 1), '
			+ ' (9, 3), '
			+ ' (10, 1), '
			+ ' (11, 2), '
			+ ' (12, 1), '
			+ ' (13, 2), '
			+ ' (14, 1), '
			+ ' (15, 2), '
			+ ' (16, 1), '
			+ ' (17, 2), '
			+ ' (18, 1), '
			+ ' (19, 2), '
			+ ' (20, 1), '
			+ ' (21, 2), '
			+ ' (22, 1), '
			+ ' (23, 2), '
			+ ' (24, 1), '
			+ ' (25, 2), '
			+ ' (28, 1), '
			+ ' (29, 3), '
			+ ' (30, 1), '
			+ ' (32, 1), '
			+ ' (33, 5), '
			+ ' (34, 6), '
			+ ' (35, 1), '
			+ ' (36, 5), '
			+ ' (38, 1), '
			+ ' (39, 5), '
			+ ' (40, 2), '
			+ ' (41, 1), '
			+ ' (42, 5), '
			+ ' (43, 2), '
			+ ' (44, 1), '
			+ ' (45, 5), '
			+ ' (46, 6), '
			+ ' (47, 1), '
			+ ' (48, 2), '
			+ ' (49, 8), '
			+ ' (50, 8), '
			+ ' (51, 8), '
			+ ' (52, 8), '
			+ ' (53, 8), '
			+ ' (54, 8), '
			+ ' (55, 8), '
			+ ' (56, 8), '
			+ ' (57, 8), '
			+ ' (58, 8), '
			+ ' (59, 8), '
			+ ' (60, 8), '
			+ ' (61, 8), '
			+ ' (62, 1), '
			+ ' (63, 3), '
			+ ' (64, 1), '
			+ ' (65, 5), '
			+ ' (66, 6), '
			+ ' (67, 1), '
			+ ' (68, 2), '
			+ ' (69, 8) '
			+ ' END '		

	EXEC(@sql)

	/*
	
	validate unique host name in Account database -- this runs in scope of "Customer Manager" so it can be executed here instead of as dynamic SQL
	
	*/
	
	IF (SELECT COUNT(1) FROM tblAccountToDomainAliasLink WHERE hostname = @destinationHostname) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'SystemCopyInquisiqToAsentia_HostnameNotUnique'
		RETURN 1
		END

	/*

	get the id of the source (Inquisiq) site

	*/

	SET @sql = 'SELECT @idSiteSource = idSite FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSite] WHERE hostname = ''' + @siteHostname + ''''

	EXECUTE SP_EXECUTESQL @sql, N'@idSiteSource INT OUTPUT', @idSiteSource OUTPUT
	
    /*

	TRANSACTION BEGIN

	*/

	BEGIN DISTRIBUTED TRANSACTION

	-- =============================================================================
	-- LOGIC START

	/************************************************SITE*************************************************************/
		-- tblSite
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite]'
				+ ' ( '
				+ '		hostname, '
				+ '		password, '
				+ '		isActive, '
				+ '		dtExpires, '
				+ '		title, '
				+ '		company, '
				+ '		contactName, '
				+ '		contactEmail, '
				+ '		userLimit, '
				+ '		kbLimit, '
				+ '		idLanguage, '
				+ '		idTimezone' 
				+ ' ) '
				+ ' SELECT ' 
				+ '		''' + @destinationHostname + ''','							-- hostname
				+ '		S.password,'												-- password (this is an MD5 value)
				+ '		S.isActive,'												-- is active?
				+ '		S.dtExpires,'												-- date expires
				+ '		S.name,'													-- title
				+ '		SC.company,'												-- company
				+ '		SC.firstName + '' '' + SC.lastName,'						-- contact name
				+ '		SC.email,'													-- contact email
				+ '		CASE WHEN S.userLimit > 0 THEN S.userLimit ELSE NULL END,'	-- user limit
				+ '		CASE WHEN S.kbLimit > 0 THEN S.kbLimit ELSE NULL END,'		-- kb limit
				+ '		' + CAST(@idSiteDefaultLang AS NVARCHAR) + ','				-- language ID
				+ '		15'															-- timezone ID				
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSite] S'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblSiteContact] SC ON SC.idSite = S.idSite'
				+ ' WHERE S.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		
		EXEC (@sql)

		-- get the newly inserted site id as @idSiteDestination
		SET @sql = 'SELECT @idSiteDestination = idSite FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite] WHERE hostname = ''' + @destinationHostname + ''''

		EXECUTE SP_EXECUTESQL @sql, N'@idSiteDestination INT OUTPUT', @idSiteDestination OUTPUT
	  
		-- insert idSite of source and destination site tables inside temporary table for mapping	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteSource) + ', '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''site'' ' 				
		
		EXEC(@sql)		   	   

		-- tblSiteLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSiteLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		company'
				+ '	) '
				+ ' SELECT '
				+ '		S.idSite, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ','
				+ '		S.title, '
				+ '		S.company'
				+ ' FROM  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite] S'
				+ ' WHERE S.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

		-- tblSiteToDomainAliasLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSiteToDomainAliasLink]' 
				+ ' ( '
				+ '		idSite,'
				+ '		domain'				
				+ '	) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		''' + @destinationHostname + ''''				
		
		EXEC(@sql)

	/************************************************ USER ******************************************************/
		-- tblUser
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUser]'
				+ ' ( '
				+ '		firstName, '
				+ '		middleName, '
				+ '		lastName, '
				+ '		displayName, '
				+ '		email, '
				+ '		username, '
				+ '		password, '
				+ '		idSite, '
				+ '		isDeleted, '
				+ '		idTimezone, '
				+ '		objectGUID, '
				+ '		activationGUID, '
				+ '		distinguishedName, '
				+ '		isActive, '
				+ '		mustchangePassword, '  
				+ '		dtCreated, '
				+ '		dtExpires, '
				+ '		idLanguage, '
				+ '		dtModified, '
				+ '		employeeID, '
				+ '		company, '
				+ '		address, '
				+ '		city, '
				+ '		province, '
				+ '		postalcode, '
				+ '		country, '
				+ '		phonePrimary, '
				+ '		phoneWork, '
				+ '		phoneFax, '
				+ '		phoneHome, '
				+ '		phoneMobile, '
				+ '		phonePager, '
				+ '		phoneOther, '
				+ '		department, '
				+ '		division, '
				+ '		region, '
				+ '		jobTitle, '
				+ '		jobClass, '
				+ '		gender, '
				+ '		race, '
				+ '		dtDOB, '
				+ '		dtHire, '
				+ '		dtTerm, '
				+ '		field00, '
				+ '		field01, '
				+ '		field02, '
				+ '		field03, '
				+ '		field04, '
				+ '		field05, '
				+ '		field06, '
				+ '		field07, '
				+ '		field08, '
				+ '		field09, '
				+ '		field10, '
				+ '		field11, '
				+ '		field12, '
				+ '		field13, '
				+ '		field14, '
				+ '		field15, '
				+ '		field16, '
				+ '		field17, '
				+ '		field18, '
				+ '		field19, '
				+ '		avatar, '
				+ '		dtLastLogin, '
				+ '		dtDeleted, '
				+ '		dtSessionExpires, '
				+ '		activeSessionId, '
				+ '		dtLastPermissionCheck, '
				+ '		isRegistrationApproved, '
				+ '		dtApproved, '
				+ '		dtDenied, '				
				+ '		idApprover, '
				+ '		rejectionComments '
				+ ' ) '
				+ ' SELECT ' 
				+ '		U.firstName,'									-- first name
				+ '		U.middleName,'									-- middle name
				+ '		U.lastName,'									-- last name
				+ '		U.lastName + '', '' + U.firstName,'				-- display name
				+ '		U.email,'										-- email
				+ '		U.username,'									-- username
				+ '		U.password,'									-- password (this is an MD5 value)
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- site ID
				+ '		0,'												-- is deleted? 
				+ '		ISNULL(TZ.destinationID, 15) AS idTimezone,'	-- timezone id (default to Eastern timezone if the value is NULL)
				+ '		U.objectGUID,'									-- object GUID
				+ '		NULL,'											-- activation GUID
				+ '		U.distinguishedName,'							-- distinguished name
				+ '		U.isActive,'									-- is active?
				+ '		1,'												-- must change password? (since Asentia uses SHA1, we need to force all users to change their password)
				+ '		U.dtCreated,'									-- date created
				+ '		U.dtExpires,'									-- date expires
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ','	-- language id, use Asentia default
				+ '		U.dtModified,'									-- date modified
				+ '		UP.employeeID,'									-- employeed ID
				+ '		UP.company,'									-- company
				+ '		UP.address,'									-- street address
				+ '		UP.city,'										-- city
				+ '		UP.province,'									-- state/province
				+ '		UP.postalcode,'									-- postal code
				+ '		UP.country,'									-- country
				+ '		UP.phonePrimary,'								-- primary phone
				+ '		UP.phoneWork,'									-- work phone
				+ '		UP.phoneFax,'									-- fax
				+ '		UP.phoneHome,'									-- home phone
				+ '		UP.phoneMobile,'								-- mobile phone
				+ '		UP.phonePager,'									-- pager
				+ '		UP.phoneOther,'									-- other phone
				+ '		UP.department,'									-- department
				+ '		UP.division,'									-- division
				+ '		UP.region,'										-- region
				+ '		UP.jobTitle,'									-- job title
				+ '		UP.jobClass,'									-- job class
				+ '		UP.gender,'										-- gender
				+ '		UP.race,'										-- race
				+ '		UP.dtDOB,'										-- date of birth
				+ '		UP.dtHire,'										-- hire date
				+ '		UP.dtTerm,'										-- term date
				+ '		UDUFV.field00,'									-- user defined field00
				+ '		UDUFV.field01,'									-- user defined field01
				+ '		UDUFV.field02,'									-- user defined field02
				+ '		UDUFV.field03,'									-- user defined field03
				+ '		UDUFV.field04,'									-- user defined field04
				+ '		UDUFV.field05,'									-- user defined field05
				+ '		UDUFV.field06,'									-- user defined field06
				+ '		UDUFV.field07,'									-- user defined field07
				+ '		UDUFV.field08,'									-- user defined field08
				+ '		UDUFV.field09,'									-- user defined field09
				+ '		UDUFV.field10,'									-- user defined field10
				+ '		NULL,'											-- user defined field11
				+ '		NULL,'											-- user defined field12
				+ '		NULL,'											-- user defined field13
				+ '		NULL,'											-- user defined field14
				+ '		NULL,'											-- user defined field15
				+ '		NULL,'											-- user defined field16
				+ '		NULL,'											-- user defined field17
				+ '		NULL,'											-- user defined field18
				+ '		NULL,'											-- user defined field19
				+ '		NULL,'											-- avatar
				+ '		NULL,'											-- date last login (set to null because user has never logged into Asentia)
				+ '		NULL,'											-- date deleted
				+ '		NULL,'											-- date session expires
				+ '		NULL,'											-- active session ID
				+ '		NULL,'											-- date last permission check
				+ '		NULL,'											-- is registration approved?
				+ '		NULL,'											-- date approved
				+ '		NULL,'											-- date denied
				+ '		NULL,'											-- approver id
				+ '		NULL'											-- rejection comments
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].' + '[tblUser] U'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserProperties] UP ON UP.idUser = U.idUser'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserDefinedUserFieldValue] UDUFV ON UDUFV.idUser = U.idUser'
				+ ' LEFT JOIN [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = U.idTimezone'
				+ ' WHERE U.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)
	  
		-- insert mapping for idUser 1 (admin)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		1, '
				+ '		1, '
				+ '		''user'''
				+ ' ) ' 

		EXEC(@sql)

		-- insert idUser mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SU.idUser, '
				+ '		DU.idUser,'
				+ '		''user''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUser] DU '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] SU ON SU.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SU.username = DU.username collate database_default AND SU.dtCreated = DU.dtCreated'
				+ ' WHERE DU.idSite IS NOT NULL AND DU.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SU.idUser IS NOT NULL AND DU.idUser IS NOT NULL'
		
		EXEC(@sql)

	/*****************************************************GROUP********************************************************/
		-- tblGroup
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblGroup]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		primaryGroupToken, '
				+ '		objectGUID, '
				+ '		distinguishedName, '
				+ '		avatar, '
				+ '		isSelfJoinAllowed, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isFeedActive, '
				+ '		isFeedModerated, '
				+ '		membershipIsPublicized,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		G.name,'										-- name
				+ '		G.primaryGroupToken,'							-- primary group token
				+ '		G.objectGUID,'									-- object GUID
				+ '		G.distinguishedName,'							-- distinguished name
				+ '		NULL,'											-- avatar
				+ '		0,'												-- is self joined allowed?
				+ '		NULL,'											-- short description
				+ '		NULL,'											-- long description
				+ '		0,'												-- is feed active?
				+ '		0,'												-- is feed moderated?
				+ '		0,'												-- membership is publicized?
				+ '		NULL'											-- search tags
				+ '		FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G'
				+ ' WHERE G.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idGroup mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SG.idGroup, '
				+ '		DG.idGroup,'
				+ '		''group''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblGroup] DG '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] SG ON SG.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SG.name = DG.name collate database_default'
				+ ' WHERE DG.idSite IS NOT NULL AND DG.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SG.idGroup IS NOT NULL AND DG.idGroup IS NOT NULL'
						
		EXEC(@sql)

		-- tblGroupLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroupLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idGroup,'
				+ '		idLanguage,'
				+ '		name,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		G.idGroup, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		G.name, '
				+ '		G.shortDescription, '
				+ '		G.longDescription, '
				+ '		G.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroup] G'
				+ ' WHERE G.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

	/**********************************************COURSE************************************************************/
		-- tblCourse
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse]'
				+ ' ( '
				+ '		idSite, '
				+ '		title, '
				+ '		coursecode, '
				+ '		revcode, '
				+ '		cost, '
				+ '		credits, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isPublished, '
				+ '		isClosed, '
				+ '		isLocked,'
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		minutes,'
				+ '		objectives,'
				+ '		socialMedia,'
				+ '		isPrerequisiteAny,'
				+ '		avatar,'
				+ '		isFeedActive,'
				+ '		isFeedModerated,'
				+ '		isFeedOpenSubscription,'
				+ '		disallowRating,'
				+ '		forceLessonCompletionInOrder,'
				+ '		selfEnrollmentDueInterval,'
				+ '		selfEnrollmentDueTimeframe,'
				+ '		selfEnrollmentExpiresFromStartInterval,'
				+ '		selfEnrollmentExpiresFromStartTimeframe,'
				+ '		selfEnrollmentExpiresFromFirstLaunchInterval,'
				+ '		selfEnrollmentExpiresFromFirstLaunchTimeframe,'
				+ '		searchTags,'
				+ '		requireFirstLessonToBeCompletedBeforeOthers,'
				+ '		lockLastLessonUntilOthersCompleted,'
				+ '		isSelfEnrollmentApprovalRequired,'
				+ '		isApprovalAllowedByCourseExperts,'
				+ '		isApprovalAllowedByUserSupervisor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		C.name,'										-- title
				+ '		C.code,'										-- code
				+ '		NULL,'											-- revcode
				+ '		C.cost,'										-- cost
				+ '		C.credits,'										-- credits
				+ '		C.shortDescription,'							-- short description
				+ '		C.description,'									-- description
				+ '		C.isPublished,'									-- is published?
				+ '		C.isClosed,'									-- is closed?	
				+ '		C.isLocked,'									-- is locked?
				+ '		C.dtCreated,'									-- date created
				+ '		C.dtModified,'									-- date modified
				+ '		0,'												-- is deleted?
				+ '		NULL,'											-- date deleted
				+ '		C.minutes,'										-- minutes
				+ '		C.objectives,'									-- objectives
				+ '		REPLACE(REPLACE(C.socialMedia, ''protocol="http://"'', ''protocol="http://" iconType="default"''), ''protocol="https://"'', ''protocol="https://" iconType="default"''),' -- social media
				+ '		C.prerequisiteAny,'								-- any or all prerequisites?
				+ '		NULL,'											-- avatar
				+ '		0,'												-- is feed active?
				+ '		0,'												-- is feed moderated?
				+ '		0,'												-- is feed open subscription?
				+ '		~C.allowRating,'								-- disallow rating? (negate it because Asentia's field is "disallow")
				+ '		C.blnForceLessonCompletionInOrder,'				-- force lesson completion in order?
				+ '		NULL,'											-- self enrollment due interval
				+ '		NULL,'											-- self enrollment due timeframe
				+ '		C.accessInterval,'								-- self enrollment expires from start interval
				+ '		C.accessTimeFrame,'								-- self enrollment expires from start timeframe
				+ '		NULL,'											-- self enrollment expires from first launch interval
				+ '		NULL,'											-- self enrollment expires from first launch timeframe
				+ '		NULL,'											-- search tags
				+ '		C.blnFirstLessonRequired,'						-- require first lesson to be completed before others
				+ '		C.blnLastLessonLocked,'							-- lock last lesson until others completed
				+ '		NULL,'											-- is self enrollment approval required?
				+ '		NULL,'											-- is approval allowed by course experts?
				+ '		NULL'											-- is approval allowed by user supervisor?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idCourse mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCourse, '
				+ '		DC.idCourse,'
				+ '		''course''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] SC ON SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SC.name = DC.title collate database_default AND SC.dtModified = DC.dtModified'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCourse IS NOT NULL AND DC.idCourse IS NOT NULL'
		
		EXEC(@sql)

		-- tblCourseLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourseLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCourse,'
				+ '		idLanguage,'
				+ '		title, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		objectives, '
				+ '		searchTags '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCourse, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.title, '
				+ '		C.shortDescription, '
				+ '		C.longDescription, '
				+ '		C.objectives, '
				+ '		C.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)

		EXEC(@sql)
	   
		-- tblCourseRating
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseRating]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		rating, '
				+ '		timestamp '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		C.destinationID,'								-- course id
				+ '		U.destinationID,'								-- user id
				+ '		CR.rating,'										-- rating
				+ '		CR.timestamp'									-- timestamp
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseRating] CR'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CR.idCourse AND C.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] U ON U.sourceID = CR.idUser AND U.object = ''user'''
				+ ' WHERE C.destinationID IS NOT NULL AND U.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCourseExpert	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseExpert]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- course id
				+ '		UM.destinationID'								-- user id
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = C.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = C.idExpert AND UM.object = ''user'''
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblDocumentRespositoryItem (tblCourseMaterial in Inquisiq) - file copies are done inside of procedural code
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblDocumentRepositoryItem]'
				+ ' ( '
				+ '		idSite, '
				+ '		idDocumentRepositoryObjectType, '
				+ '		idObject, '
				+ '		idDocumentRepositoryFolder, '
				+ '		idOwner, '
				+ '		filename, '
				+ '		kb, '
				+ '		searchTags, '
				+ '		isPrivate, '
				+ '		idLanguage, '
				+ '		isAllLanguages, '
				+ '		dtCreated, '
				+ '		isDeleted, '
				+ '		dtDeleted, '
				+ '		label, '
				+ '		isVisibleToUser, '
				+ '		isUploadedByUser '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		2,'												-- document repository object type (course = 2)
				+ '		C.destinationID,'								-- object id (course id)
				+ '		NULL,'											-- document repository folder id
				+ '		1,'												-- owner id
				+ '		CM.filename,'									-- filename
				+ '		CM.kb,'											-- kb
				+ '		NULL,'											-- search tags
				+ '		CM.isPrivate,'									-- is private?
				+ '		NULL,'											-- language id
				+ '		1,'												-- is all languages?
				+ '		GETUTCDATE(),'									-- date created
				+ '		0,'												-- is deleted?
				+ '		NULL,'											-- date deleted
				+ '		CM.name,'										-- label
				+ '		NULL,'											-- is visible to user?
				+ '		NULL'											-- is uploaded by user?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseMaterial] CM'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CM.idCourse AND C.object = ''course'''
				+ ' WHERE C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblDocumentRepositoryItemLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblDocumentRepositoryItemLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idDocumentRepositoryItem,'
				+ '		idLanguage,'
				+ '		label, '				
				+ '		searchTags '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		DRI.idDocumentRepositoryItem, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		DRI.label, '				
				+ '		DRI.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblDocumentRepositoryItem] DRI'
				+ ' WHERE DRI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND DRI.idDocumentRepositoryObjectType = 2'

		EXEC(@sql)

	/*****************************************************LESSON**********************************************************************/
		-- tblLesson
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		dtDeleted, '
				+ '		isDeleted, '
				+ '		title, '
				+ '		revcode, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		[order], '
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- course id
				+ '		NULL,'											-- deleted date
				+ '		0,'												-- is deleted?
				+ '		L.name,'										-- title
				+ '		NULL,'											-- revcode
				+ '		L.shortDescription,'							-- short description
				+ '		L.description,'									-- long description
				+ '		L.[order],'										-- lesson order
				+ '		GETUTCDATE(),'									-- date created
				+ '		GETUTCDATE(),'									-- date modified
				+ '		NULL'											-- search tags
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = L.idCourse AND CM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL'
		
		EXEC (@sql)	  

		-- insert idLesson mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SL.idLesson, '
				+ '		DL.idLesson,'
				+ '		''lesson''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLesson] DL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.destinationID = DL.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.idCourse = CM.sourceID AND SL.name = DL.title collate database_default'
				+ ' WHERE DL.idSite IS NOT NULL AND DL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SL.idLesson IS NOT NULL AND DL.idLesson IS NOT NULL'
		
		EXEC(@sql)

		-- tblLessonLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLessonLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idLesson,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		L.idLesson, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		L.title, '
				+ '		L.shortDescription, '
				+ '		L.longDescription, '
				+ '		L.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLesson] L'
				+ ' WHERE L.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

	/*****************************************************INSTRUCTOR LED TRAINING**********************************************************************/
		-- tblStandUpTraining (this needs to be sourced from tblLesson in Inquisiq)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandUpTraining]'
				+ ' ( '
				+ '		idSite, '
				+ '		title, '
				+ '		description, '
				+ '		isStandaloneEnroll, '
				+ '		isRestrictedEnroll, '
				+ '		isRestrictedDrop, '
				+ '		searchTags, '
				+ '		avatar, '
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		cost'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','					-- idSite
				+ '		L.name + '' ##'' + CONVERT(NVARCHAR, L.idLesson) + ''##'','		-- title
				+ '		NULL,'															-- description
				+ '		0,'																-- isStandaloneEnroll
				+ '		0,'																-- isRestrictedEnroll
				+ '		0,'																-- isRestrictedDrop
				+ '		NULL,'															-- searchTags
				+ '		NULL,'															-- avatar
				+ '		0,'																-- isDeleted
				+ '		NULL,'															-- dtDeleted
				+ '		NULL'															-- cost
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = L.idCourse AND CM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL'
				+ ' AND L.idLessonType IN (1, 2)' -- 1 = classroom, 2 = web meeting
						
		EXEC (@sql)

		-- insert idStandupTraining mappings (idStandupTraining maps to Inquisiq idLesson as they are equivalents)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SL.idLesson, '
				+ '		DST.idStandupTraining,'
				+ '		''ilt''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTraining] DST '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.name + '' ##'' + CONVERT(NVARCHAR, SL.idLesson) + ''##'' = DST.title collate database_default'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = SL.idCourse AND C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)				
				+ ' WHERE DST.idSite IS NOT NULL AND DST.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SL.idLesson IS NOT NULL AND DST.idStandupTraining IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLesson## additions we made to standup training titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTraining] SET '
				+ '		title = REPLACE(DST.title, '' ##'' + CONVERT(NVARCHAR, STM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTraining] DST '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STM ON STM.destinationID = DST.idStandupTraining AND STM.object = ''ilt'''
				+ ' WHERE STM.sourceID IS NOT NULL'

		EXEC (@sql)

		--tblStandUpTrainingLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idStandUpTraining,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		description,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		ST.idStandUpTraining, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		ST.title, '
				+ '		ST.description, '
				+ '		ST.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTraining] ST'
				+ ' WHERE ST.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- tblStandUpTrainingInstance
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandUpTrainingInstance]'
				+ ' ( '
				+ '		idStandUpTraining, '
				+ '		idSite, '
				+ '		title, '
				+ '		description, '
				+ '		seats, '
				+ '		waitingSeats, '
				+ '		urlRegistration, '
				+ '		urlAttend, '
				+ '		city, '
				+ '		province,'
				+ '		locationDescription,'
				+ '		idInstructor,'
				+ '		type,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		integratedObjectKey,'
				+ '		hostUrl,'
				+ '		genericJoinUrl,'
				+ '		meetingPassword,'
				+ '		idWebMeetingOrganizer'
				+ ' ) '
				+ ' SELECT ' 
				+ '		STM.destinationID, '																				-- idStandupTraining
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','														-- idSite
				+ '		CONVERT(NVARCHAR, S.[datetime], 101) + '' ('' + CONVERT(NVARCHAR, S.[datetime], 108) + '' UTC)'' + '' ##'' + CONVERT(NVARCHAR, S.idSession) + ''##'','	-- title
				+ '		NULL,'																								-- description
				+ '		S.seats,'																							-- seats
				+ '		S.waitingSeats,'																					-- waitingSeats
				+ '		NULL,'																								-- urlRegistration
				+ '		S.url,'																								-- urlAttend
				+ '		S.city,'																							-- city
				+ '		S.province,'																						-- province
				+ '		S.locationDescription,'																				-- locationDescription
				+ '		UM.destinationID,'																					-- idInstructor
				+ '		CASE WHEN L.idLessonType = 1 THEN 1 ELSE 0 END,'													-- type
				+ '		0,'																									-- is deleted?
				+ '		NULL,'																								-- date deleted
				+ '		NULL,'																								-- integrated object key
				+ '		NULL,'																								-- host url
				+ '		NULL,'																								-- generic join url
				+ '		NULL,'																								-- meeting password
				+ '		NULL'																								-- web meeting organizer id
				+ '	FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSession] S'
				+ '	LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L ON L.idLesson = S.idLesson'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = L.idCourse'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STM ON STM.sourceID = L.idLesson AND STM.object = ''ilt'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = S.idInstructor AND UM.object = ''user'''				
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND STM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idStandupTrainingInstance mappings (idStandupTrainingInstance maps to Inquisiq idSession)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SS.idSession, '
				+ '		DSTI.idStandupTrainingInstance,'
				+ '		''iltsession''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTrainingInstance] DSTI '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSession] SS ON CONVERT(NVARCHAR, SS.[datetime], 101) + '' ('' + CONVERT(NVARCHAR, SS.[datetime], 108) + '' UTC)'' + '' ##'' + CONVERT(NVARCHAR, SS.idSession) + ''##'' = DSTI.title collate database_default'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.idLesson = SS.idLesson'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] SC ON SC.idCourse = SL.idCourse AND SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
				+ ' WHERE DSTI.idSite IS NOT NULL AND DSTI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SS.idSession IS NOT NULL AND DSTI.idStandupTrainingInstance IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idSession## additions we made to standup training instance titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTrainingInstance] SET '
				+ '		title = REPLACE(DSTI.title, '' ##'' + CONVERT(NVARCHAR, STIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTrainingInstance] DSTI '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STIM ON STIM.destinationID = DSTI.idStandupTrainingInstance AND STIM.object = ''iltsession'''
				+ ' WHERE STIM.sourceID IS NOT NULL'

		EXEC (@sql)
	   
		-- tblStandUpTrainingInstanceLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstanceLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idStandUpTrainingInstance,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		description,'
				+ '		locationDescription'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		STI.idStandUpTrainingInstance, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		STI.title, '
				+ '		STI.description, '
				+ '		STI.locationDescription '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstance] STI'
				+ ' WHERE STI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

	/******************************************************CATALOG********************************************************************/	   
		-- tblCatalog
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog]'
				+ ' ( '
				+ '		idSite, '
				+ '		idParent, '
				+ '		title, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isPrivate, '
				+ '		cost, '
				+ '		isClosed, '
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		costType,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		NULL,'											-- parent id (NULL for now)
				+ '		C.name,'										-- title
				+ '		C.shortDescription,'							-- short description
				+ '		C.description,'									-- description
				+ '		C.isPrivate,'									-- is the catalog private
				+ '		CASE WHEN C.cost >= 1 THEN C.cost '				-- cost
				+ '			 WHEN C.cost >= 0.1 AND C.cost <= 0.9 THEN C.cost * 100 '
				+ '		ELSE 0 END, '									
				+ '		C.isClosed,'									-- is catalog closed?
				+ '		GETUTCDATE(),'									-- date created
				+ '		GETUTCDATE(),'									-- date modified
				+ '		CASE WHEN C.cost = 0 THEN 3'					-- cost type
				+ '			 WHEN C.cost >= 1 THEN 2'
				+ '			 WHEN C.cost >= 0.1 AND C.cost <= 0.9 THEN 4'
				+ '		ELSE 1 END,'
				+ '		NULL'											-- search tags
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idCatalog mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCatalog, '
				+ '		DC.idCatalog,'
				+ '		''catalog''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] SC ON SC.idSite = '+ CONVERT(NVARCHAR, @idSiteSource) + ' AND SC.name = DC.title collate database_default'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCatalog IS NOT NULL AND DC.idCatalog IS NOT NULL'
		
		EXEC(@sql)
	   
		-- update idParent for catalogs
		SET @sql = 'UPDATE DESTINATIONCATALOG SET'
				+ '		idParent = CM.destinationID'
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] SOURCECATALOG'
				+ ' LEFT JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DESTINATIONCATALOG ON SOURCECATALOG.name = DESTINATIONCATALOG.title collate database_default AND SOURCECATALOG.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = SOURCECATALOG.idParent AND CM.object = ''catalog'''
				+ ' WHERE DESTINATIONCATALOG.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SOURCECATALOG.idParent IS NOT NULL'
	   
		EXEC(@sql)

		-- tblCatalogLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalogLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCatalog,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCatalog, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.title, '
				+ '		C.shortDescription, '
				+ '		C.longDescription, '
				+ '		C.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalog] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

	/******************************************************CONTENT PACKAGE********************************************************************/	   
		-- tblContentPackage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage]'
				+ ' ( '
				+ '		idSite, '
				+ '		idContentPackageType, '
				+ '		idSCORMPackageType, '
				+ '		name, '
				+ '		path, '
				+ '		kb, '
				+ '		manifest, '
				+ '		dtCreated, '
				+ '		dtModified,'
				+ '		isMediaUpload,'
				+ '		idMediaType,'
				+ '		contentTitle,'
				+ '		originalMediaFilename,'
				+ '		isVideoMedia3rdParty,'
				+ '		videoMediaEmbedCode,'
				+ '		enableAutoplay,'
				+ '		allowRewind,'
				+ '		allowFastForward,'
				+ '		allowNavigation,'
				+ '		allowResume,'
				+ '		minProgressForCompletion,'
				+ '		isProcessing,'
				+ '		isProcessed,'
				+ '		dtProcessed,'
				+ '		idQuizSurvey'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		1,'												-- content package type (1 = SCORM)
				+ '		1,'												-- SCORM package type (1 = content)
				+ '		SP.name,'										-- name
				+ '		STUFF([path], CHARINDEX(''' + CONVERT(NVARCHAR, @idSiteSource) + '-'', [path]), LEN(''' + CONVERT(NVARCHAR, @idSiteSource) + '-''), ''/warehouse/' + CONVERT(NVARCHAR, @idAccount) + '-'' + ''' + CONVERT(NVARCHAR, @idSiteDestination) + '-''),' -- path				
				-- + '		REPLACE(SP.path, ''' + CONVERT(NVARCHAR, @idSiteSource) + '-'', ''/warehouse/' + CONVERT(NVARCHAR, @idAccount) + '-' + CONVERT(NVARCHAR, @idSiteDestination) + '-''),'	-- path
				+ '		SP.kb,'											-- kb
				+ '		SP.imsmanifest,'								-- manifest
				+ '		SP.dateCreated,'								-- created date
				+ '		SP.dateModified,'								-- modified date
				+ '		SP.isMediaUpload,'								-- is media upload?
				+ '		SP.idMediaType,'								-- media type
				+ '		SP.contentTitle,'								-- content title
				+ '		SP.originalMediaFilename,'						-- original media filename
				+ '		SP.isVideoMedia3rdParty,'						-- is video media 3rd party?
				+ '		SP.videoMediaEmbedCode,'						-- video media embed code
				+ '		SP.enableAutoPlay,'								-- enable autoplay
				+ '		SP.allowRewind,'								-- allow rewind
				+ '		SP.allowFastForward,'							-- allow fast forward
				+ '		SP.allowNavigation,'							-- allow navigation
				+ '		SP.allowResume,'								-- allow resume
				+ '		SP.minProgressForCompletion,'					-- minimum progress for completion
				+ '		SP.isProcessing,'								-- is processing?
				+ '		SP.isProcessed,'								-- is processed?
				+ '		SP.dtProcessed,'								-- date processed				
				+ '		NULL'											-- quiz survey id (inserted later, doing so now is a circular reference)
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SP'
				+ ' WHERE SP.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idContentPackage mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SCP.idSCORMPackage, '
				+ '		DCP.idContentPackage,'
				+ '		''contentpackage''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage] DCP '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SCP ON SCP.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SCP.name = DCP.name collate database_default AND SCP.dateCreated = DCP.dtCreated'
				+ ' WHERE DCP.idSite IS NOT NULL AND DCP.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCP.idSCORMPackage IS NOT NULL AND DCP.idContentPackage IS NOT NULL'
		
		EXEC(@sql)		

	/*****************************************************************QUIZ SURVEY*************************************************************/
		-- tblQuizSurvey
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblQuizSurvey]'
				+ ' ( '
				+ '		idSite, '
				+ '		idAuthor, '
				+ '		type, '
				+ '		identifier, '
				+ '		guid, '
				+ '		data, '
				+ '		isDraft, '
				+ '		idContentPackage, '
				+ '		dtCreated,'
				+ '		dtModified'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		ISNULL(UM.destinationID, 1),'													-- idAuthor
				+ '		QS.type,'																		-- type
				+ '		QS.identifier,'																	-- identifier
				+ '		QS.guid,'																		-- guid
				+ '		QS.data,'																		-- data
				+ '		QS.isDraft,'																	-- isDraft
				+ '		CASE WHEN QS.idSCORMPackage IS NOT NULL THEN CPM.destinationID ELSE NULL END,'	-- idContentPackage
				+ '		QS.dtCreated,'																	-- dtCreated
				+ '		QS.dtModified'																	-- dtModfied
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblQuizSurvey] QS'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = QS.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = QS.idSCORMPackage AND CPM.object = ''contentpackage'''
				+ ' WHERE QS.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idQuizSurvey mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SQS.idQuizSurvey, '
				+ '		DQS.idQuizSurvey,'
				+ '		''quizsurvey''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblQuizSurvey] DQS '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblQuizSurvey] SQS ON SQS.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SQS.identifier = DQS.identifier collate database_default AND SQS.guid = DQS.guid collate database_default AND SQS.dtCreated = DQS.dtCreated'
				+ ' WHERE DQS.idSite IS NOT NULL AND DQS.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SQS.idQuizSurvey IS NOT NULL AND DQS.idQuizSurvey IS NOT NULL'
		
		EXEC(@sql)

		-- update idQuizSurvey in tblContentPackage
		--SET @sql = 'UPDATE DCP SET'
				--+ '		idQuizSurvey = QSM.destinationID'
				--+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SCP'
				--+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = SCP.idSCORMPackage AND CPM.object = ''contentpackage'''
				--+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] QSM ON QSM.sourceID = SCP.idQuizSurvey AND QSM.object = ''quizsurvey'''
				--+ ' LEFT JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage] DCP ON SCP.idContentPackage = CPM.destinationID'								
				--+ ' WHERE DCP.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCP.idQuizSurvey IS NOT NULL AND QSM.destinationID IS NOT NULL'
	   
		--EXEC(@sql)

	/*********************************************************CERTIFICATE**************************************************/
		-- tblCertificate
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		issuingOrganization, '
				+ '		description, '
				+ '		credits, '
				+ '		filename, '
				+ '		isActive, '
				+ '		activationDate, '
				+ '		expiresInterval,'
				+ '		expiresTimeframe,'
				+ '		objectType,'
				+ '		idObject,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		code,'
				+ '		reissueBasedOnMostRecentCompletion'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		C.name,'																		-- name
				+ '		C.issuingOrganization,'															-- title
				+ '		C.description,'																	-- description
				+ '		C.credits,'																		-- credits
				+ '		REPLACE(C.filename, ''/_config/' + @siteHostname + '/_images/certs/'', ''''),'	-- filename
				+ '		C.isActive,'																	-- is certificate active?
				+ '		C.activationDate,'																-- activation date
				+ '		C.expiresInterval,'																-- expires interval
				+ '		C.expiresTimespan,'																-- expires timeframe
				+ '		1,'																				-- object type (1 = course)
				+ '		CM.destinationID,'																-- object id (course id)
				+ '		0,'																				-- is deleted?
				+ '		NULL,'																			-- date deleted?
				+ '		C.code,'																		-- code
				+ '		NULL'																			-- reissue based on most recent completion
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] C'
				+ ' LEFT JOIN  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseCertificateLink] CCL ON CCL.idCertificate = C.idCertificate'
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CCL.idCourse AND CM.object = ''course'''
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND CM.destinationID IS NOT NULL'
		
		EXEC (@sql)		

		-- insert idCertificate mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCertificate, '
				+ '		DC.idCertificate,'
				+ '		''certificate''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] SC ON SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND  SC.name = DC.name collate database_default AND REPLACE(SC.filename, ''/_config/' + @siteHostname + '/_images/certs/'', '''') = DC.filename collate database_default AND SC.activationDate = DC.activationDate'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCertificate IS NOT NULL AND DC.idCertificate IS NOT NULL'
		
		EXEC(@sql)

		-- tblCertificateLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificateLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCertificate,'
				+ '		idLanguage,'
				+ '		name,'
				+ '		description'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCertificate, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.name, '
				+ '		C.description '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificate] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)		

		-- create a table, and get certificate instance data mappings so certificate config JSON files can be written
		SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_CertificateLayoutDataMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings]'
			+ '('
			+ '    [destinationID] INT NOT NULL,'
			+ '    [data] NVARCHAR(MAX) NOT NULL'
			+ ')'
			+ ' END '
			+ ' ELSE '
		SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings]'

		EXEC(@sql)
		
		-- get the certificate instance data		
		SET @sql = ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_CertificateLayoutDataMappings] '
				+ ' ( '
				+ '		destinationID,'
				+ '		data'
				+ ' ) '
				+ ' SELECT '
				+ '		DC.idCertificate, '
				+ '		REPLACE(REPLACE(SC.instanceData, ''%20'', '' ''), ''"ImageSource": "'' + SC.[filename] + ''"'', ''"ImageSource": "/_config/' + @destinationHostname + '/certificates/'' + CONVERT(NVARCHAR, DC.idCertificate) + ''/'' + DC.[filename] + ''"'') '
				--+ '		CASE WHEN LEFT(SC.instanceData,14) = ''<instanceData>'' THEN '
				--+ '			  ''{"Certificate" : '''
				--+ '			+ ''{ "Container" : '''
				--+ '			+ ''{ "Width" : "800px", "Height" : "600px", "ImageSource" : "'' + REPLACE(CAST(SC.[instanceData] AS XML).value(''(/instanceData/image)[1]'',''NVARCHAR(MAX)''),''_images/certs'',''certificates/'' + CONVERT(NVARCHAR, DC.idCertificate)) +''" },'''
				--+ '			+ ''"Elements": ['''
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 1 then ''{"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[1]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[1]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[1]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[1]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[1]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[1]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[1]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 2 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[2]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[2]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[2]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[2]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[2]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[2]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[2]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 3 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[3]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[3]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[3]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[3]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[3]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[3]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[3]'',''nvarchar(max)'') + ''"}'' else '''' end'
		--SET @sql2 = '		+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 4 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[4]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[4]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[4]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[4]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[4]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[4]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[4]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 5 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[5]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[5]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[5]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[5]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[5]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[5]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[5]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 6 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[6]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[6]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[6]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[6]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[6]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[6]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[6]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+ CASE when CAST(SC.[instanceData] AS XML).value(''(count(/instanceData/fields/field))'',''nvarchar(max)'') >= 7 then '', {"Identifier": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@columnName)[7]'',''nvarchar(max)'') + ''", "Value" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field)[7]'',''nvarchar(max)'') + ''", "IsStaticLabel" : "false", "Top": "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@y)[7]'',''nvarchar(max)'') + ''", "Left" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@x)[7]'',''nvarchar(max)'') + ''", "Width" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@width)[7]'',''nvarchar(max)'') + ''", "FontSize" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@size)[7]'',''nvarchar(max)'') + ''", "Alignment" : "'' + CAST(SC.[instanceData] AS XML).value(''(/instanceData/fields/field/@align)[7]'',''nvarchar(max)'') + ''"}'' else '''' end'
				--+ '			+'']}}'''    
				--+ '		ELSE SC.instanceData END '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificate] DC '
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.destinationID = DC.idCertificate AND CM.object = ''certificate'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificate] SC ON SC.idCertificate = CM.sourceID'				
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SC.idSite IS NOT NULL AND SC.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
	
		EXEC (@sql)
		
		-- tblCertificateRecord - this may need a clean-up after migration as there may be "duplicate" certificate record entries
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCertificateRecord]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCertificate, '
				+ '		idUser, '
				+ '		idTimezone, '
				+ '		idAwardedBy, '
				+ '		timestamp, '
				+ '		expires, '
				+ '		code, '
				+ '		credits,'
				+ '		awardData,'
				+ '		idCertificateImport'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite
				+ '		CM.destinationID,'															-- certificate id
				+ '		UM.destinationID,'															-- user id
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'			-- timezone
				+ '		1,'																			-- awarded by id
				+ '		CR.timestamp,'																-- timestamp
				+ '		CR.expires,'																-- expires
				+ '		CR.code,'																	-- code
				+ '		CR.credits,'																-- credits
				+ '		NULL,'																		-- award data
				+ '		NULL'																		-- certificate import
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCertificateRecord] CR '				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CR.idCertificate AND CM.object = ''certificate'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = CR.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = CR.idTimezone '
				+ ' WHERE CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)			
	   
	/*************************************************ACTIVITY IMPORT****************************************************/
		-- tblActivityImport
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblActivityImport]'
				+ ' ( '
				+ '		idSite, '
				+ '		idUser, '
				+ '		timestamp, '
				+ '		idUserImported, '
				+ '		importFileName '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		UM.destinationID,'								-- user id
				+ '		AI.timestamp,'									-- timestamp
				+ '		UIM.destinationID,'								-- user imported id
				+ '		AI.importFileName'								-- import filename
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblActivityImport] AI'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = AI.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UIM ON UIM.sourceID = AI.idUserImported AND UIM.object = ''user'''
				+ ' WHERE idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idActivityImport mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SAI.idActivityImport, '
				+ '		DAI.idActivityImport,'
				+ '		''activityimport''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblActivityImport] DAI '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblActivityImport] SAI ON SAI.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SAI.timestamp = DAI.timestamp'
				+ ' WHERE DAI.idSite IS NOT NULL AND DAI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SAI.idActivityImport IS NOT NULL AND DAI.idActivityImport IS NOT NULL'
		
		EXEC(@sql)

	/**************************************************EVENT EMAIL NOTIFICATION******************************************/
		-- tblEventEmailNotification
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		idEventType, '
				+ '		idEventTypeRecipient, '
				+ '		idObject, '
				+ '		[from], '
				+ '		isActive, '
				+ '		dtActivation, '
				+ '		interval,'
				+ '		timeframe,'
				+ '		priority'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','										-- idSite
				+ '		EEN.name + '' ##'' + CONVERT(NVARCHAR, EEN.idEventEmailNotification) + ''##'','		-- name
				+ '		ETM.destinationID,'																	-- idEventType
				+ '		ETRM.destinationID,'																-- idEventTypeRecipient
				+ '		NULL,'																				-- idObject
				+ '		EEN.[from],'																		-- from
				+ '		EEN.isActive,'																		-- is active?
				+ '		CASE WHEN EEN.isActive = 1 THEN GETUTCDATE() ELSE NULL END,'						-- activation date
				+ '		EEN.interval,'																		-- interval
				+ '		EEN.timeframe,'																		-- timeframe
				+ '		EEN.priority'																		-- priority
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] EEN'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings] ETM ON ETM.sourceID = EEN.idEventType'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings] ETRM ON ETRM.sourceID = EEN.idEventTypeRecipient'
				+ ' WHERE EEN.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND EEN.idEventType <> 4'	-- exclude "Purchase Confirmed"
		
		EXEC (@sql)

		-- insert idEventEmailNotification mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEEN.idEventEmailNotification, '
				+ '		DEEN.idEventEmailNotification,'
				+ '		''emailnotification''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] SEEN ON SEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SEEN.name + '' ##'' + CONVERT(NVARCHAR, SEEN.idEventEmailNotification) + ''##'' = DEEN.name collate database_default'
				+ ' WHERE DEEN.idSite IS NOT NULL AND DEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEEN.idEventEmailNotification IS NOT NULL AND DEEN.idEventEmailNotification IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEventEmailNotification## additions we made to email notification titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] SET '
				+ '		name = REPLACE(DEEN.name, '' ##'' + CONVERT(NVARCHAR, EENM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EENM ON EENM.destinationID = DEEN.idEventEmailNotification AND EENM.object = ''emailnotification'''
				+ ' WHERE EENM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblEventEmailNotificationLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotificationLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idEventEmailNotification,'
				+ '		idLanguage,'
				+ '		name'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		EEN.idEventEmailNotification, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		EEN.name '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] EEN'
				+ ' WHERE EEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- create a table, and get email notification data mappings so email notification config XML files can be written		
		SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EmailNotificationDataMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings]'
			+ '('
			+ '    [destinationID] INT NOT NULL,'
			+ '    [data] NVARCHAR(MAX) NOT NULL'
			+ ')'
			+ ' END '
			+ ' ELSE '
		SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings]'

		EXEC(@sql)
		
		-- get the email notification data		
		SET @sql = ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings] '
				+ ' ( '
				+ '		destinationID,'
				+ '		data'
				+ ' ) '
				+ ' SELECT '
				+ '		DEEN.idEventEmailNotification, '
				+ '		  ''<?xml version="1.0" encoding="utf-8"?>'' '
				+ '		+ ''<emailNotification>'' '
				+ '		+ ''	<language code="default">'' '
				+ '		+ ''		<subject><![CDATA['' + SEEN.[subject] + '']]></subject>'' '
				+ '		+ ''		<body><![CDATA['' + SEEN.[bodyTemplate] + '']]></body>'' '
				+ '		+ ''	</language>'' '
				+ '		+ ''	<language code="en-US">'' '
				+ '		+ ''		<subject><![CDATA['' + SEEN.[subject] + '']]></subject>'' '
				+ '		+ ''		<body><![CDATA['' + SEEN.[bodyTemplate] + '']]></body>'' '
				+ '		+ ''	</language>'' '
				+ '		+ ''</emailNotification>'' '				
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EENM ON EENM.destinationID = DEEN.idEventEmailNotification AND EENM.object = ''emailnotification'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] SEEN ON SEEN.idEventEmailNotification = EENM.sourceID'				
				+ ' WHERE DEEN.idSite IS NOT NULL AND DEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SEEN.idSite IS NOT NULL AND SEEN.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
	
		EXEC (@sql)

	/*****************************************************COUPON CODE***********************************************************************/		
		-- tblCouponCode
		SET @sql = ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCode]'
				+ ' ( '
				+ '		idSite, '
				+ '		usesAllowed, '
				+ '		discount, '
				+ '		discountType, '
				+ '		code, '
				+ '		isDeleted, '
				+ '		comments, '
				+ '		forCourse, '
				+ '		forCatalog, '
				+ '		dtStart, '
				+ '		dtEnd, '
				+ '		isSingleUsePerUser, '
				+ '		dtDeleted, '
				+ '		forLearningPath, '
				+ '		forStandupTraining ' 
				+ ' ) '
				+ ' SELECT DISTINCT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) +','						-- idSite
				+ '		CC.usesAllowed,'													-- uses allowed
				+ '		CASE WHEN CC.value < 0 THEN CC.value * -1'							-- discount
				+ '			 WHEN CC.value >= 0.1 AND CC.value <= 0.9 THEN CC.value * 100'
				+ '		ELSE CC.value END,'
				+ '		CASE WHEN CC.value = 0 THEN 1'										-- discount type
				+ '			 WHEN CC.value >= 0.1 AND CC.value <= 0.9 THEN 4'
				+ '			 WHEN CC.value < 0 THEN 3'
				+ '		ELSE 2 END,'
				+ '		CC.code,'															-- code
				+ '		CC.isDeleted,'														-- is deleted?
				+ '		CC.comments,'														-- comments
				+ '		CASE WHEN CC.isCourse = 0 THEN 1 '									-- for course
				+ '			 WHEN CC.isCourse = 1 AND courseLink.idCourse IS NULL THEN 2 '
				+ '		ELSE 3 END,'														
				+ '		CASE WHEN CC.isCatalog = 0 THEN 1 '									-- for catalog
				+ '			 WHEN CC.isCatalog = 1 AND catalogLink.idCatalog IS NULL THEN 2 '
				+ '		ELSE 3 END,'														
				+ '		CC.dtStart,'														-- date start
				+ '		CC.dtEnd,'															-- date end
				+ '		CC.isSingleUsePerUser, '											-- is single use per user?
				+ '		CASE WHEN CC.isDeleted = 1 THEN GETUTCDATE() ELSE NULL END, '		-- date deleted
				+ '		1, '																-- for learning path
				+ '		1 '																	-- for standup training	
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCode] CC'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCourseLink] courseLink ON courseLink.idCouponCode = CC.idCouponCode'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCatalogLink] catalogLink ON catalogLink.idCouponCode = CC.idCouponCode'
				+ ' WHERE CC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idCouponCode mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SCC.idCouponCode, '
				+ '		DCC.idCouponCode,'
				+ '		''couponcode''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCode] DCC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCode] SCC ON SCC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SCC.code = DCC.code AND SCC.usesAllowed = DCC.usesAllowed'
				+ ' WHERE DCC.idSite IS NOT NULL AND DCC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCC.idCouponCode IS NOT NULL AND DCC.idCouponCode IS NOT NULL'
		
		EXEC(@sql)
	   
	/*****************************************************LINKING TABLES****************************************************************/
		-- tblCourseToCatalogLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToCatalogLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idCatalog '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		COM.destinationID,'								-- idCourse
				+ '		CAM.destinationID'								-- idCatalog
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseCatalogLink] CCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] COM ON COM.sourceID = CCL.idCourse AND COM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CAM ON CAM.sourceID = CCL.idCatalog AND CAM.object = ''catalog'''
				+ ' WHERE COM.destinationID IS NOT NULL AND CAM.destinationID IS NOT NULL'
		
		EXEC (@sql)
	   
		-- tblCourseToPrerequisiteLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToPrerequisiteLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idPrerequisite '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- idCourse
				+ '		PM.destinationID'								-- idPrerequisite
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCoursePrerequisiteLink] CPL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CPL.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] PM ON PM.sourceID = CPL.idPrerequisite AND PM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL AND PM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		
		-- tblCouponCodeToCourseLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCodeToCourseLink]'
				+ ' ( '
				+ '		idCouponCode, '
				+ '		idCourse '
				+ ' ) '
				+ ' SELECT ' 
				+ '		CC.destinationID,'	-- idCouponCode
				+ '		C.destinationID'	-- idCourse
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCourseLink] CCCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CC ON CC.sourceID = CCCL.idCouponCode AND CC.object = ''couponcode'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CCCL.idCourse AND C.object = ''course'''
				+ ' WHERE CC.destinationID IS NOT NULL AND C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCouponCodeToCatalogLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCodeToCatalogLink]'
				+ ' ( '
				+ '		idCouponCode, '
				+ '		idCatalog '
				+ ' ) '
				+ ' SELECT ' 
				+ '		CC.destinationID,'	-- idCouponCode
				+ '		C.destinationID'	-- idCatalog
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCatalogLink] CCCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CC ON CC.sourceID = CCCL.idCouponCode AND CC.object = ''couponcode'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CCCL.idCatalog AND C.object = ''catalog'''
				+ ' WHERE CC.destinationID IS NOT NULL AND C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCourseToScreenshotLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToScreenshotLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		filename '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '	-- idSite
				+ '		CM.destinationID, '									-- idCourse
				+ '		SS.s '												-- filename
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = C.idCourse AND CM.object = ''course'''
				+ ' CROSS APPLY (SELECT s FROM [dbo].[DelimitedStringToTable](C.sampleScreens, ''|'') WHERE C.sampleScreens IS NOT NULL AND C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ') SS'
		
		EXEC (@sql)
	   
		-- tblLessonToContentLink - link SCORM lesson types to content packages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLessonToContentLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idLesson, '
				+ '		idObject, '
				+ '		idContentType, '
				+ '		idAssignmentDocumentType, '
				+ '		allowSupervisorsAsProctor, '
				+ '		allowCourseExpertsAsProctor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '	-- idSite
				+ '		LM.destinationID, '									-- idLesson
				+ '		CPM.destinationID, '								-- idObject
				+ '		1, '												-- idContentType (1 = content package)
				+ '		NULL, '												-- idAssignmentDocumentType
				+ '		NULL, '												-- allow supervisors as proctor?
				+ '		NULL '												-- allow course experts as proctor?
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = L.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMResource] SR ON SR.idSCORMResource = L.idSCORMResource'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SP ON SP.idSCORMPackage = SR.idSCORMPackage'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = SP.idSCORMPackage AND CPM.object = ''contentpackage'''
				+ ' WHERE L.idLessonType = 0 AND LM.destinationID IS NOT NULL AND CPM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblLessonToContentLink - link Classroom/Web Meeting lesson types to ILT modules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLessonToContentLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idLesson, '
				+ '		idObject, '
				+ '		idContentType, '
				+ '		idAssignmentDocumentType, '
				+ '		allowSupervisorsAsProctor, '
				+ '		allowCourseExpertsAsProctor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) +', '	-- idSite
				+ '		LM.destinationID, '								-- idLesson
				+ '		ILTM.destinationID, '							-- idObject
				+ '		2, '											-- idContentType (2 = ILT)
				+ '		NULL, '											-- idAssignmentDocumentType
				+ '		NULL, '											-- allow supervisors as proctor?
				+ '		NULL '											-- allow course experts as proctor?
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = L.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] ILTM ON ILTM.sourceID = L.idLesson AND ILTM.object = ''ilt'''
				+ ' WHERE L.idLessonType IN (1, 2) AND LM.destinationID IS NOT NULL AND ILTM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblUserToGroupLink -- make all user to group links seem as if the user were manually placed into the group,
							  -- post conversion, after group rules have run, we will need to clean up the "manual" entries
							  -- left over where the user gets a group entry by rule
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUserToGroupLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idUser, '
				+ '		idGroup, '
				+ '		idRuleSet, '
				+ '		created, '
				+ '		selfJoined '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		UM.destinationID,'								-- idUser
				+ '		GM.destinationID, '								-- idGroup
				+ '		NULL, '											-- idRuleSet
				+ '		GETUTCDATE(), '									-- created date
				+ '		0 '												-- self joined
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUserGroupLink] UGL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = UGL.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GM ON GM.sourceID = UGL.idGroup AND GM.object = ''group'''
				+ ' WHERE UM.destinationID IS NOT NULL AND GM.destinationID IS NOT NULL'
		
		EXEC (@sql)

	/**********************************************ENROLLMENTS (WHERE WE DO NOT HAVE TO ONE-OFF)**********************************************/
		
		/**********ACTIVITY IMPORTED ENROLLMENTS***********/

		-- enrollments (instances only) that belong to an activity import
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite
				+ '		CM.destinationID,'															-- idCourse				
				+ '		UM.destinationID,'															-- idUser
				+ '		NULL,'																		-- idGroupEnrollment
				+ '		NULL,'																		-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'			-- idTimezone
				+ '		0,'																			-- isLockedByPrerequisites
				+ '		EI.code,'																	-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','	-- title
				+ '		EI.dtStart,'																-- dtStart
				+ '		EI.dtDue,'																	-- dtDue
				+ '		EI.dtEnd,'																	-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'															-- dtFirstLaunch
				+ '		EI.dtCompleted,'															-- dtCompleted
				+ '		EI.dtStart,'																-- dtCreated
				+ '		EI.dtLastSynchronized,'														-- dtLastSynchronized
				+ '		NULL,'																		-- dueInterval
				+ '		NULL,'																		-- dueTimeframe
				+ '		NULL,'																		-- expiresFromStartInterval
				+ '		NULL,'																		-- expiresFromStartTimeframe
				+ '		AIM.destinationID,'															-- idActivityImport
				+ '		EI.credits '																-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] AIM ON AIM.sourceID = EI.idActivityImport AND AIM.object = ''activityimport'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NOT NULL AND UM.destinationID IS NOT NULL AND AIM.destinationID IS NOT NULL'
		
		EXEC (@sql)				

		/**********MANUAL SINGLE ENROLLMENTS**********/

		-- manual (self-assigned or admin-assigned) enrollments - recurrence will be taken away 		
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		NULL,'																							-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		/**********BUILD RULESET ENROLLMENTS FROM GROUP ENROLLMENTS **********/

		-- add ruleset enrollments using group enrollments from Inquisiq
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idTimezone, '
				+ '		priority, '
				+ '		label, '				
				+ '		isLockedByPrerequisites, '
				+ '		isFixedDate, '
				+ '		dtStart, '
				+ '		dtEnd, '
				+ '		dtCreated, '				
				+ '		delayInterval, '
				+ '		delayTimeframe, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		recurInterval, '
				+ '		recurTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		expiresFromFirstLaunchInterval, '
				+ '		expiresFromFirstLaunchTimeframe, '
				+ '		forceReassignment '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','																-- idSite
				+ '		CM.destinationID,'																							-- idCourse				
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'											-- idTimezone
				+ '		ROW_NUMBER() OVER(PARTITION BY CM.destinationID ORDER BY CM.destinationID),'								-- priority
				+ '		''START: '' + CONVERT(NVARCHAR, GE.dtStart, 101)'															-- label
				+ '		+ CASE WHEN GE.dueInterval IS NOT NULL THEN '' DUE: '' + CONVERT(NVARCHAR, GE.dueInterval) + GE.dueTimeframe ELSE '''' END'
				+ '		+ CASE WHEN GE.recurInterval IS NOT NULL THEN '' RECUR: '' + CONVERT(NVARCHAR, GE.recurInterval) + GE.recurTimeframe ELSE '''' END'
				+ '		+ CASE WHEN GE.accessInterval IS NOT NULL THEN '' ACCESS: '' + CONVERT(NVARCHAR, GE.accessInterval) + GE.accessTimeframe ELSE '''' END,'
				+ '		CASE WHEN GE.isLockedForPrerequisites IS NOT NULL THEN GE.isLockedForPrerequisites ELSE 0 END,'				-- isLockedByPrerequisites
				+ '		GE.useFixedDates,'																							-- isFixedDate
				+ '		GE.dtStart,'																								-- dtStart
				+ '		GE.dtEnd,'																									-- dtEnd
				+ '		GE.dtStart,'																								-- dtCreated
				+ '		GE.delayInterval,'																							-- delayInterval
				+ '		GE.delayTimeframe,'																							-- delayTimeframe				
				+ '		GE.dueInterval,'																							-- dueInterval
				+ '		GE.dueTimeframe,'																							-- dueTimeframe
				+ '		GE.recurInterval,'																							-- recurInterval
				+ '		GE.recurTimeframe,'																							-- recurTimeframe
				+ '		GE.accessInterval,'																							-- expiresFromStartInterval
				+ '		GE.accessTimeframe,'																						-- expiresFromStartTimeframe
				+ '		GE.idGroupEnrollment,'																						-- expiresFromFirstLaunchInterval (use as placeholder for mapping, put idGroupEnrollment in it)
				+ '		NULL, '																										-- expiresFromFirstLaunchTimeframe
				+ '		0 '																											-- forceReassignment
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = GE.idCourse'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = GE.idCourse AND CM.object = ''course'''						
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = GE.idTimezone '
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND CM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idRulesetEnrollment mappings (idRulesetEnrollment maps to Inquisiq idGroupEnrollment)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SGE.idGroupEnrollment, '
				+ '		DRSE.idRulesetEnrollment,'
				+ '		''rulesetenrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] DRSE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] SGE ON SGE.idGroupEnrollment = DRSE.expiresFromFirstLaunchInterval'				
				+ ' WHERE DRSE.idSite IS NOT NULL AND DRSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SGE.idGroupEnrollment IS NOT NULL AND DRSE.idRulesetEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the idGroupEnrollment additions we made to tblRulesetEnrollment, we did that to allow it to be mapped
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollment] SET '
				+ '		expiresFromFirstLaunchInterval = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] DRSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SGEM ON SGEM.destinationID = DRSE.[idRulesetEnrollment] AND SGEM.object = ''rulesetenrollment'''
				+ ' WHERE SGEM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- create rulesets for the rulesetenrollment to group mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] '
				+ ' ( '
				+ '		idSite,'
				+ '		isAny,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		1,'
				+ '		''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'''
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- create rules for the rulesetenrollment to group mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRule] '
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		userField,'
				+ '		operator,'
				+ '		textValue'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		RS.idRuleset,'
				+ '		''group'','
				+ '		''eq'','
				+ '		G.name'				
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS ON RS.label = ''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'' collate database_default'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL AND RS.idRuleset IS NOT NULL'
		
		EXEC(@sql)

		-- link rulesets to rulesetenrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetToRulesetEnrollmentLink] '
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		idRulesetEnrollment'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		RS.idRuleset,'
				+ '		RSE.idRuleSetEnrollment'
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS ON RS.label = ''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'' collate database_default'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL AND RS.idRuleset IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idGroupEnrollment## additions we made to ruleset labels, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] SET '
				+ '		label = REPLACE(DRS.label, '' ##'' + CONVERT(NVARCHAR, RSEM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRuleset] DRS '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetToRulesetEnrollmentLink] RSEL ON RSEL.idRuleset = DRS.idRuleset'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RSEM ON RSEM.destinationID = RSEL.idRulesetEnrollment AND RSEM.object = ''rulesetenrollment'''
				+ ' WHERE RSEM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblRulesetEnrollmentLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollmentLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idRulesetEnrollment,'
				+ '		idLanguage,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		RSE.idRulesetEnrollment, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		RSE.label '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollmentLanguage] RSE'
				+ ' WHERE RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- tblRulesetLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		idLanguage,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		RS.idRuleset, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		RS.label '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS'
				+ ' WHERE RS.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		/**********RULESET ENROLLMENTS CASCADED**********/

		-- group enrollments from Inquisiq that were cascaded get mapped as cascaded ruleset enrollments in Asentia
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		RSEM.destinationID,'																			-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RSEM ON RSEM.sourceID = E.idGroupEnrollment AND RSEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NOT NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL AND RSEM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		
		/**********LESSON DATA AND SCORM INTERACTIONS ASSOCIATED WITH ENROLLMENTS**********/

		-- insert idEnrollment mappings (idEnrollment maps to Inquisiq idEnrollmentInstance)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEI.idEnrollmentInstance, '
				+ '		DE.idEnrollment,'
				+ '		''enrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] SEI ON SEI.name + '' ##'' + CONVERT(NVARCHAR, SEI.idEnrollmentInstance) + ''##'' = DE.title collate database_default'
				+ ' WHERE DE.idSite IS NOT NULL AND DE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEI.idEnrollmentInstance IS NOT NULL AND DE.idEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEnrollmentInstance## additions we made to enrollment titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] SET '
				+ '		title = REPLACE(DE.title, '' ##'' + CONVERT(NVARCHAR, SEIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SEIM ON SEIM.destinationID = DE.idEnrollment AND SEIM.object = ''enrollment'''
				+ ' WHERE SEIM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idEnrollment, '
				+ '		idLesson, '
				+ '		title, '
				+ '		revcode, '
				+ '		[order], '
				+ '		dtCompleted, '
				+ '		idTimezone, '
				+ '		contentTypeCommittedTo, '
				+ '		dtCommittedToContentType, '
				+ '		resetForContentChange, '
				+ '		preventPostCompletionLaunchForContentChange '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		EM.destinationID,'																		-- idEnrollment				
				+ '		LM.destinationID,'																		-- idLesson
				+ '		LD.lessonName + '' ##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','					-- title
				+ '		NULL,'																					-- revcode
				+ '		LD.[order],'																			-- order
				+ '		CASE WHEN LD.completionStatus = ''completed'' OR LD.successStatus = ''passed'' THEN'	-- dtCompleted
				+ '			LD.[timestamp] '
				+ '		ELSE NULL END,'
				+ '		E.idTimezone,'																			-- idTimezone
				+ '		NULL,'																					-- contentTypeCommittedTo
				+ '		NULL,'																					-- dtCommittedToContentType
				+ '		NULL,'																					-- resetForContentChange
				+ '		NULL'																					-- preventPostCompletionLaunchForContentChange						
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EM ON EM.sourceID = EI.idEnrollmentInstance AND EM.object = ''enrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = LD.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EM.destinationID'				
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EM.destinationID IS NOT NULL AND E.idEnrollment IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-Lesson mappings (idData-Lesson maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDL.[idData-Lesson],'
				+ '		''lessondata''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] SLD ON SLD.lessonName + '' ##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDL.title collate database_default'				
				+ ' WHERE DDL.idSite IS NOT NULL AND DDL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDL.[idData-Lesson] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to lesson data titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] SET '
				+ '		title = REPLACE(DDL.title, '' ##'' + CONVERT(NVARCHAR, SLDM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDL.[idData-Lesson] AND SLDM.object = ''lessondata'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data sco
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-Lesson], '
				+ '		manifestIdentifier, '
				+ '		completionStatus, '
				+ '		successStatus, '
				+ '		scoreScaled, '
				+ '		totalTime, '
				+ '		[timestamp], '
				+ '		idTimezone, '
				+ '		actualAttemptCount, '
				+ '		effectiveAttemptCount, '
				+ '		proctoringUser '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		LDM.destinationID,'																-- [idData-Lesson]
				+ '		''##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','							-- use manifest identifier as a mapping plceholder
				+ '		CASE WHEN CS.idSCORMVocabulary IS NULL THEN 0 ELSE CS.idSCORMVocabulary END,'	-- completionStatus
				+ '		CASE WHEN SS.idSCORMVocabulary IS NULL THEN 0 ELSE SS.idSCORMVocabulary END,'	-- successStatus
				+ '		LD.scoreScaled,'																-- scoreScaled
				+ '		LD.totalTime,'																	-- totalTime
				+ '		LD.[timestamp],'																-- timestamp
				+ '		DL.idTimezone,'																	-- idTimezone
				+ '		LD.actualAttempts,'																-- actualAttemptCount
				+ '		LD.effectiveAttempts,'															-- effectiveAttemptCount
				+ '		NULL'																			-- proctoringUser
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LDM ON LDM.sourceID = LD.idLessonData AND LDM.object = ''lessondata'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] DL ON DL.[idData-Lesson] = LDM.destinationID'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] CS ON CS.value = LD.completionStatus'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] SS ON SS.value = LD.successStatus'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND LDM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-SCO mappings (idData-SCO maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDSCO.[idData-SCO],'
				+ '		''lessondatasco''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] SLD ON ''##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDSCO.manifestIdentifier collate database_default'
				+ ' WHERE DDSCO.idSite IS NOT NULL AND DDSCO.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDSCO.[idData-SCO] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to data sco manifestIdentifier, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO] SET '
				+ '		manifestIdentifier = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDSCO.[idData-Lesson] AND SLDM.object = ''lessondatasco'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- scorm interactions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCOInt]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-SCO], '
				+ '		identifier, '
				+ '		timestamp, '
				+ '		result, '
				+ '		latency, '
				+ '		scoIdentifier '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		DSCOM.destinationID,'																	-- [idData-SCO]
				+ '		SI.identifier,'																			-- identifier
				+ '		SI.timestamp,'																			-- timestamp
				+ '		CASE WHEN SV.idSCORMVocabulary IS NOT NULL THEN SV.idSCORMVocabulary ELSE NULL END,'	-- result
				+ '		SI.latency,'																			-- latency
				+ '		NULL'																					-- scoIdentifier
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblScormInt] SI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD ON LD.idLessonData = SI.idLessonData'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] DSCOM ON DSCOM.sourceID = SI.idLessonData AND DSCOM.object = ''lessondatasco'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSCORMVocabulary] SV ON SV.value = SI.result'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND DSCOM.destinationID IS NOT NULL AND DSCOM.destinationID IN (SELECT [idData-SCO] FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO])'
		
		EXEC (@sql)
		
	/****************************************************INSERT HOSTNAME INTO ACCOUNT DB******************************************************/

		SET @sql = 'INSERT INTO [tblAccountToDomainAliasLink]'
				+ ' ( '
				+ '		idAccount, '
				+ '		hostname '			
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idAccount) + ','	-- idAccount
				+ '		''' + @destinationHostname + ''''		-- hostname
		
		EXEC (@sql)
		
	/*********************************************************COMMIT THE TRANSACTION**********************************************************/

	BEGIN TRY
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentia_Success'

		COMMIT TRANSACTION	   
		--TRANSACTION END

	END TRY

	BEGIN CATCH
				
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentia_Failed'
		
		IF @@TRANCOUNT > 0
			BEGIN
			ROLLBACK TRANSACTION
			END

	END CATCH
		
	SET XACT_ABORT OFF

END
GO