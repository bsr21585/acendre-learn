-- =====================================================================
-- PROCEDURE: [DatabaseServer.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.Details]
GO


CREATE PROCEDURE [DatabaseServer.Details]
(
	@Return_Code					INT				OUTPUT,
	@Error_Description_Code			NVARCHAR(50)	OUTPUT,
	@idCallerAccount				INT				= 0, --default if not specified
	@callerLangString				NVARCHAR (10),
	@idCaller						INT				= 0,
	
	@idDatabaseServer							INT				OUTPUT, 
	@serverName						NVARCHAR(255)	OUTPUT,
	@networkName					NVARCHAR(255)	OUTPUT,
	@username						NVARCHAR(255)	OUTPUT,	
	@password						NVARCHAR(255)	OUTPUT,
	@isTrusted						BIT				OUTPUT
	
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/*
	
	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDatabaseServer
		WHERE idDatabaseServer = @idDatabaseServer
		) = 0 
		BEGIN
		SET @Return_Code = 1
		RETURN 1
		END
		
	
	
	
	SELECT
		@serverName		= S.serverName,	
		@networkName	= S.networkName,		
		@username		= S.username,
		@password		= S.password,
		@isTrusted	    = s.isTrusted
		
	FROM tblDatabaseServer S
	
	WHERE S.idDatabaseServer = @idDatabaseServer
	
	
	SELECT @Return_Code = 0
		
	END
	
