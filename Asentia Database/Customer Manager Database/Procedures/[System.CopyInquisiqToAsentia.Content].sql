
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Content]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Content]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Content]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@useNewContentPath		BIT
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	
	/******************************************************CONTENT PACKAGE********************************************************************/	   
		-- tblContentPackage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage]'
				+ ' ( '
				+ '		idSite, '
				+ '		idContentPackageType, '
				+ '		idSCORMPackageType, '
				+ '		name, '
				+ '		path, '
				+ '		kb, '
				+ '		manifest, '
				+ '		dtCreated, '
				+ '		dtModified,'
				+ '		isMediaUpload,'
				+ '		idMediaType,'
				+ '		contentTitle,'
				+ '		originalMediaFilename,'
				+ '		isVideoMedia3rdParty,'
				+ '		videoMediaEmbedCode,'
				+ '		enableAutoplay,'
				+ '		allowRewind,'
				+ '		allowFastForward,'
				+ '		allowNavigation,'
				+ '		allowResume,'
				+ '		minProgressForCompletion,'
				+ '		isProcessing,'
				+ '		isProcessed,'
				+ '		dtProcessed,'
				+ '		idQuizSurvey'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		1,'												-- content package type (1 = SCORM)
				+ '		1,'												-- SCORM package type (1 = content)
				+ '		SP.name,'										-- name
				+ '		CASE WHEN ' + CONVERT(NVARCHAR, @useNewContentPath) + ' = 1 THEN'
				+ '			''/warehouse/' + CONVERT(NVARCHAR, @idAccount) + '-'' + ''' + CONVERT(NVARCHAR, @idSiteDestination) + '-'' + CONVERT(NVARCHAR, FORMAT(SP.dateCreated, ''yyyy-MM-dd-hh-mm-ss'')) + ''-'' + SP.path' -- new content path
				+ '		ELSE'
				+ '			STUFF([path], CHARINDEX(''' + CONVERT(NVARCHAR, @idSiteSource) + '-'', [path]), LEN(''' + CONVERT(NVARCHAR, @idSiteSource) + '-''), ''/warehouse/' + CONVERT(NVARCHAR, @idAccount) + '-'' + ''' + CONVERT(NVARCHAR, @idSiteDestination) + '-'')' -- old content path
				+ '		END,'				
				+ '		SP.kb,'											-- kb
				+ '		SP.imsmanifest,'								-- manifest
				+ '		SP.dateCreated,'								-- created date
				+ '		SP.dateModified,'								-- modified date
				+ '		SP.isMediaUpload,'								-- is media upload?
				+ '		SP.idMediaType,'								-- media type
				+ '		SP.contentTitle,'								-- content title
				+ '		SP.originalMediaFilename,'						-- original media filename
				+ '		SP.isVideoMedia3rdParty,'						-- is video media 3rd party?
				+ '		SP.videoMediaEmbedCode,'						-- video media embed code
				+ '		SP.enableAutoPlay,'								-- enable autoplay
				+ '		SP.allowRewind,'								-- allow rewind
				+ '		SP.allowFastForward,'							-- allow fast forward
				+ '		SP.allowNavigation,'							-- allow navigation
				+ '		SP.allowResume,'								-- allow resume
				+ '		SP.minProgressForCompletion,'					-- minimum progress for completion
				+ '		SP.isProcessing,'								-- is processing?
				+ '		SP.isProcessed,'								-- is processed?
				+ '		SP.dtProcessed,'								-- date processed				
				+ '		NULL'											-- quiz survey id (inserted later, doing so now is a circular reference)
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SP'
				+ ' WHERE SP.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idContentPackage mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SCP.idSCORMPackage, '
				+ '		DCP.idContentPackage,'
				+ '		''contentpackage''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblContentPackage] DCP '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SCP ON SCP.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SCP.name = DCP.name collate database_default AND SCP.dateCreated = DCP.dtCreated'
				+ ' WHERE DCP.idSite IS NOT NULL AND DCP.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCP.idSCORMPackage IS NOT NULL AND DCP.idContentPackage IS NOT NULL'
		
		EXEC(@sql)		

	/*****************************************************************QUIZ SURVEY*************************************************************/
		-- tblQuizSurvey
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblQuizSurvey]'
				+ ' ( '
				+ '		idSite, '
				+ '		idAuthor, '
				+ '		type, '
				+ '		identifier, '
				+ '		guid, '
				+ '		data, '
				+ '		isDraft, '
				+ '		idContentPackage, '
				+ '		dtCreated,'
				+ '		dtModified'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		ISNULL(UM.destinationID, 1),'													-- idAuthor
				+ '		QS.type,'																		-- type
				+ '		QS.identifier,'																	-- identifier
				+ '		QS.guid,'																		-- guid
				+ '		QS.data,'																		-- data
				+ '		QS.isDraft,'																	-- isDraft
				+ '		CASE WHEN QS.idSCORMPackage IS NOT NULL THEN CPM.destinationID ELSE NULL END,'	-- idContentPackage
				+ '		QS.dtCreated,'																	-- dtCreated
				+ '		QS.dtModified'																	-- dtModfied
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblQuizSurvey] QS'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = QS.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = QS.idSCORMPackage AND CPM.object = ''contentpackage'''
				+ ' WHERE QS.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idQuizSurvey mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SQS.idQuizSurvey, '
				+ '		DQS.idQuizSurvey,'
				+ '		''quizsurvey''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblQuizSurvey] DQS '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblQuizSurvey] SQS ON SQS.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SQS.identifier = DQS.identifier collate database_default AND SQS.guid = DQS.guid collate database_default AND SQS.dtCreated = DQS.dtCreated'
				+ ' WHERE DQS.idSite IS NOT NULL AND DQS.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SQS.idQuizSurvey IS NOT NULL AND DQS.idQuizSurvey IS NOT NULL'
		
		EXEC(@sql)

		
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentia_Success'

		
		
	SET XACT_ABORT OFF

END
GO