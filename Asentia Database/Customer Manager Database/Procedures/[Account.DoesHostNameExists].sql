-- =====================================================================
-- PROCEDURE: [Account.DoesHostNameExists]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.DoesHostNameExists]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.DoesHostNameExists]
GO

/*

Determines if the current session Site alredy has a host name with same name.

*/
CREATE PROCEDURE [Account.DoesHostNameExists]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@hostName				NVARCHAR (255),
	@isHostNameAlredyExists	BIT				OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	/*
	
	Determines if the current session Site alredy has a host name with same name.
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccountToDomainAliasLink ATDAL
		WHERE (ATDAL.hostname = @hostName 
		OR ATDAL.domain = @hostName )
		AND @idCallerAccount <> ATDAL.idAccount
		
		) = 0 
		BEGIN
		SET @isHostNameAlredyExists = 0
		END
	ELSE
		BEGIN
		SET @isHostNameAlredyExists = 1
		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO