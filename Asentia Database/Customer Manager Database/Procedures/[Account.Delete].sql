-- =====================================================================
-- PROCEDURE: [Account.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.Delete]
GO

CREATE PROCEDURE [Account.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@Accounts				IDTable			READONLY
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	IF (SELECT COUNT(1) FROM @Accounts) = 0
		BEGIN
		SET @Return_Code = 1 -- nothing to do
		SET @Error_Description_Code = 'AccountDelete_NoRecordsSelected'
		RETURN 1
		END
		
	IF @idCallerAccount <> 1
		BEGIN
		SET @Return_Code = 3 -- caller permission error
		SET @Error_Description_Code = 'AccountDelete_PermissionError'
		RETURN 1
		END
		
	/*
	
	Mark the account(s) as deleted. DO NOT remove the record.
	
	*/
	
	UPDATE tblAccount SET
		isDeleted = 1
	WHERE idAccount IN (
		SELECT id
		FROM @Accounts
	)
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO