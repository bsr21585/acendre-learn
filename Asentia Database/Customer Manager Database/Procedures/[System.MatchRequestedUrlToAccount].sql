-- =====================================================================
-- PROCEDURE: [System.MatchRequestedUrlToAccount]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF exists (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.MatchRequestedUrlToAccount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.MatchRequestedUrlToAccount]
GO

/*

Matches a url to an account id.

*/
CREATE PROCEDURE [System.MatchRequestedUrlToAccount]
(
	@Return_Code				INT					OUTPUT,
	@Error_Description_Code		NVARCHAR(50)		OUTPUT,

	@idAccount					INT					OUTPUT,
	@url						NVARCHAR(255)
)
AS
	BEGIN
	SET NOCOUNT ON
		SELECT DISTINCT @idAccount = idAccount
		FROM tblAccountToDomainAliasLink
		WHERE @url = hostname
		OR @url = domain
		
		IF (@idAccount is null)
			BEGIN
			SELECT @idAccount = 1
			END
			
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

