SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Group]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Group]
GO


/*

CLONE PORTAL - GROUP DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.Group]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.0: Copy Groups - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	  
		/*

		GROUPS

		*/
                
		-- copy groups
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroup] '
				 + '( '
				 + '	idSite, '
				 + '	name, '
				 + '	primaryGroupToken, '
				 + '	objectGUID, '
				 + '	distinguishedName, '
				 + '	avatar, '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	isSelfJoinAllowed, '
				 + '	isFeedActive, '
				 + '	isFeedModerated, '
				 + '	membershipIsPublicized, '
				 + '	searchTags '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	G.name  + '' ##'' + CAST(G.idGroup AS NVARCHAR) + ''##'', '
				 + '	G.primaryGroupToken, '
				 + '	G.objectGUID, '
				 + '	G.distinguishedName, '
				 + '	G.avatar, '
				 + '	G.shortDescription, '
				 + '	G.longDescription, '
				 + '	G.isSelfJoinAllowed, '
				 + '	G.isFeedActive, '
				 + '	G.isFeedModerated, '
				 + '	G.membershipIsPublicized, '
				 + '	G.searchTags '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroup] G '
				 + 'WHERE G.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
                										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.1: Copy Groups - Groups Inserted', 0)

		-- insert the mapping for source to destination group id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idGroup, '
	   			 + '	DST.idGroup, '
	   			 + '	''groups'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroup] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroup] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.name + '' ##'' + CAST(SRC.idGroup AS NVARCHAR) + ''##'' = DST.name) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
				 		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.2: Copy Groups - Group Mappings Created', 0)

		-- clean up the ##idGroup## additions we made to group titles, we did that to uniquely distinguish groups
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroup] SET '
				 + '	name = REPLACE(DG.name, '' ##'' + CAST(SG.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroup] DG '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SG ON SG.destinationID = DG.idGroup AND SG.object = ''groups'' '
				 + 'WHERE DG.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SG.sourceID IS NOT NULL '

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.3: Copy Groups - Group Titles Cleaned Up', 0)
			   
		-- copy group languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupLanguage] '
	   				+ '( '
	   				+ '	idSite, '
	   				+ '	idGroup, '
	   				+ '	idLanguage, '
	   				+ '	name, '
	   				+ '	shortDescription, '
	   				+ '	longDescription, '
	   				+ '	searchTags '
	   				+ ') '
	   				+ 'SELECT '
	   				+	CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				+ '	TTG.destinationId, '
	   				+ '	GL.idLanguage, '
	   				+ '	GL.name, '
	   				+ '	GL.shortDescription, '
	   				+ '	GL.longDescription, '
	   				+ '	GL.searchTags '
	   				+ 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupLanguage] GL '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GL.idGroup AND TTG.object = ''groups'') '
	   				+ 'WHERE GL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.4: Copy Groups - Group Languages Inserted', 0)	 
		
		-- copy catalog access to group links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalogAccessToGroupLink] '
	     		 + '( '
	     		 + '	idSite, '
	     		 + '	idGroup, '
	     		 + '	idCatalog, '
	     		 + '	created '
	     		 + ') '
	     		 + 'SELECT '
	     		 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	     		 + '	TTG.destinationId, '
	     		 + '	TTC.destinationId, '
	     		 + '	CATGL.created '
	     		 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalogAccessToGroupLink] CATGL '
	     		 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CATGL.idCatalog AND TTC.object = ''catalog'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = CATGL.idGroup AND TTG.object = ''groups'') '
				 + 'WHERE CATGL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.5: Copy Groups - Group To Catalog Access Inserted', 0)

		-- copy group enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupEnrollment] '
	   			 + '( '
				 + '	idSite, '
				 + '	idCourse, '
				 + '	idGroup, '
				 + '	idTimezone, '
				 + '	isLockedByPrerequisites, '
				 + '	dtStart, '
				 + '	dtCreated, '
				 + '	dueInterval, '
				 + '	dueTimeframe, '
				 + '	expiresFromStartInterval, '
				 + '	expiresFromStartTimeframe, '
				 + '	expiresFromFirstLaunchInterval, '
				 + '	expiresFromFirstLaunchTimeframe '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	TTG.destinationId, '
				 + '	GE.idTimezone, '
				 + '	GE.isLockedByPrerequisites, '
				 + '	GE.dtStart, '
				 + '	GE.dtCreated, '
				 + '	GE.dueInterval, '
				 + '	GE.dueTimeframe, '
				 + '	GE.expiresFromStartInterval, '
				 + '	GE.expiresFromStartTimeframe, '
				 + '	GE.expiresFromFirstLaunchInterval, '
				 + '	GE.expiresFromFirstLaunchTimeframe '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupEnrollment] GE '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GE.idGroup AND TTG.object = ''groups'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = GE.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE GE.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.6: Copy Groups - Group Enrollments Inserted', 0)
	   
		-- insert the mapping for source to destination group enrollment id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idGroupEnrollment, '
				 + '	DST.idGroupEnrollment, '
				 + '	''groupenrollment'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupEnrollment] DST '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.destinationId = DST.idGroup AND TTG.object = ''groups'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.destinationId = DST.idCourse AND TTC.object = ''courses'') '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupEnrollment] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SRC.dtStart = DST.dtStart)'
				 + '	AND (SRC.dtCreated = DST.dtCreated)'
				 + '	AND (SRC.idGroup = TTG.sourceId) '
				 + '	AND (SRC.idCourse = TTC.sourceId)) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3.7: Copy Groups - Group Enrollment Mappings Created', 0)

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_Group_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 3: Copy Groups - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_Group_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END