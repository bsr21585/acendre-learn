SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Enrollment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Enrollment]
GO


/*

CLONE PORTAL - ENROLLMENT DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.Enrollment]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.0: Copy Enrollment - Initialized', 0)
    
    BEGIN TRY	
    
		DECLARE @sql	NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		LEARNING PATH ENROLLMENTS

		*/

		-- copy learning path enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathEnrollment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idLearningPath, '
	   			 + '	idUser, '
	   			 + '	idRuleSetLearningPathEnrollment, '
	   			 + '	idTimezone, '
	   			 + '	title, '
	   			 + '	dtStart, '
	   			 + '	dtDue, '
	   			 + '	dtCompleted, '
	   			 + '	dtCreated, '
	   			 + '	dueInterval, '
	   			 + '	dueTimeframe, '
	   			 + '	dtLastSynchronized, '
				 + '	dtExpiresFromStart, '
				 + '	dtExpiresFromFirstLaunch, '
				 + '	dtFirstLaunch, '
				 + '	expiresFromStartInterval, '
				 + '	expiresFromStartTimeframe, '
				 + '	expiresFromFirstLaunchInterval, '
				 + '	expiresFromFirstLaunchTimeframe '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTLP.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	TTRSLPE.destinationId, '
	   			 + '	LPE.idTimezone, '	   			 
				 + '	LPE.title  + '' ##'' + CAST(LPE.idLearningPathEnrollment AS NVARCHAR) + ''##'', '
	   			 + '	LPE.dtStart, '
	   			 + '	LPE.dtDue, '
	   			 + '	LPE.dtCompleted, '
	   			 + '	LPE.dtCreated, '
	   			 + '	LPE.dueInterval, '
	   			 + '	LPE.dueTimeframe, '
	   			 + '	LPE.dtLastSynchronized, '
				 + '	LPE.dtExpiresFromStart, '
				 + '	LPE.dtExpiresFromFirstLaunch, '
				 + '	LPE.dtFirstLaunch, '
				 + '	LPE.expiresFromStartInterval, '
				 + '	LPE.expiresFromStartTimeframe, '
				 + '	LPE.expiresFromFirstLaunchInterval, '
				 + '	LPE.expiresFromFirstLaunchTimeframe '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathEnrollment] LPE '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = LPE.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = LPE.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRSLPE on (TTRSLPE.sourceId = LPE.idRuleSetLearningPathEnrollment AND TTRSLPE.object = ''rulesetlearningpathenrollment'') '
	   			 + 'WHERE LPE.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.1: Copy Enrollment - Learning Path Enrollments Inserted', 0)
	   
		-- insert the mapping for source to destination learning path enrollment ids
		SET @sql = ' INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + ' SELECT '
	   			 + '	SLPE.idLearningPathEnrollment, '
	   			 + '	DLPE.idLearningPathEnrollment, '
	   			 + '	''learningpathenrollment'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathEnrollment] DLPE '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathEnrollment] SLPE '
	   			 + 'ON (SLPE.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SLPE.title + '' ##'' + CAST(SLPE.idLearningPathEnrollment AS NVARCHAR) + ''##'' = DLPE.title) '
	   			 + 'WHERE DLPE.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.2: Copy Enrollment - Learning Path Enrollment Mappings Created', 0)

		-- clean up the ##idLearningPathEnrollment## additions we made to learning path enrollment titles, we did that to uniquely distinguish courses
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLearningPathEnrollment] SET '
				 + '	title = REPLACE(DLPE.title, '' ##'' + CAST(SLPE.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLearningPathEnrollment] DLPE '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SLPE ON SLPE.destinationID = DLPE.idLearningPathEnrollment AND SLPE.object = ''learningpathenrollment'' '
				 + 'WHERE DLPE.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SLPE.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.3: Copy Enrollment - Learning Path Enrollment Titles Cleaned Up', 0)	  

		-- copy learning path enrollment approvers
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathEnrollmentApprover] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idLearningPath, '
	   			 + '	idUser '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTLP.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathEnrollmentApprover] LPEA '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = LPEA.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = LPEA.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE LPEA.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.4: Copy Enrollment - Learning Path Enrollment Approvers Inserted', 0)	  

		/*

		ENROLLMENTS

		*/

		-- copy enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEnrollment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser, '
	   			 + '	idGroupEnrollment, '
	   			 + '	idRuleSetEnrollment, '
	   			 + '	idLearningPathEnrollment, '
	   			 + '	idTimezone, '
	   			 + '	isLockedByPrerequisites, '
	   			 + '	code, '
	   			 + '	revcode, '
	   			 + '	title, '
	   			 + '	dtStart, '
	   			 + '	dtDue, '
	   			 + '	dtExpiresFromStart, '
	   			 + '	dtExpiresFromFirstLaunch, '
	   			 + '	dtFirstLaunch, '
	   			 + '	dtCompleted, '
	   			 + '	dtCreated, '
	   			 + '	dtLastSynchronized, '
	   			 + '	dueInterval, '
	   			 + '	dueTimeframe, '
	   			 + '	expiresFromStartInterval, '
	   			 + '	expiresFromStartTimeframe, '
	   			 + '	expiresFromFirstLaunchInterval, '
	   			 + '	expiresFromFirstLaunchTimeframe, '
	   			 + '	idActivityImport, '
	   			 + '	credits '
	   			 + ') '
	   			 + ' SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	TTGE.destinationId, '
	   			 + '	TTRSE.destinationId, '
	   			 + '	TTLPE.destinationId, '
	   			 + '	E.idTimezone, '
	   			 + '	E.isLockedByPrerequisites, '
	   			 + '	E.code, '
	   			 + '	E.revcode, '
	   			 + '	E.title + '' ##'' + CAST(E.idEnrollment AS NVARCHAR) + ''##'', '
	   			 + '	E.dtStart, '
	   			 + '	E.dtDue, '
	   			 + '	E.dtExpiresFromStart, '
	   			 + '	E.dtExpiresFromFirstLaunch, '
	   			 + '	E.dtFirstLaunch, '
	   			 + '	E.dtCompleted, '
	   			 + '	E.dtCreated, '
	   			 + '	E.dtLastSynchronized, '
	   			 + '	E.dueInterval, '
	   			 + '	E.dueTimeframe, '
	   			 + '	E.expiresFromStartInterval, '
	   			 + '	E.expiresFromStartTimeframe, '
	   			 + '	E.expiresFromFirstLaunchInterval, '
	   			 + '	E.expiresFromFirstLaunchTimeframe, '
	   			 + '	TTAI.destinationId, '
	   			 + '	E.credits '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEnrollment] E '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = E.idCourse AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = E.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTGE on (TTGE.sourceId = E.idGroupEnrollment AND TTGE.object = ''groupenrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRSE on (TTRSE.sourceId = E.idRuleSetEnrollment AND TTRSE.object = ''rulesetenrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLPE on (TTLPE.sourceId = E.idLearningPathEnrollment AND TTLPE.object = ''learningpathenrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTAI on (TTAI.sourceId = E.idActivityImport AND TTAI.object = ''activityimport'') '
	   			 + 'WHERE E.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.5: Copy Enrollment - Enrollments Inserted', 0)
	   		
		-- insert the mapping for source to destination enrollment id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SE.idEnrollment, '
	   			 + '	DE.idEnrollment, '
	   			 + '	''enrollment'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEnrollment] DE '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEnrollment] SE '
	   			 + 'ON (SE.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SE.title + '' ##'' + CAST(SE.idEnrollment AS NVARCHAR) + ''##'' = DE.title)'
	   			 + 'WHERE DE.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.6: Copy Enrollment - Enrollment Mappings Created', 0)

		-- clean up the ##idEnrollment## additions we made to title, we did that to uniquely distinguish enrollments
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] SET '
				 + '	title = REPLACE(DE.title, '' ##'' + CAST(SE.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] DE '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SE ON SE.destinationID = DE.idEnrollment AND SE.object = ''enrollment'' '				 
				 + 'WHERE DE.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SE.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.7: Copy Enrollment - Enrollment Titles Cleaned Up', 0)

		-- copy enrollment requests
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEnrollmentRequest] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser, '
	   			 + '	timestamp, '
	   			 + '	dtApproved, '
	   			 + '	dtDenied, '
	   			 + '	idApprover, '
	   			 + '	rejectionComments '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	ER.timestamp, '
	   			 + '	ER.dtApproved, '
	   			 + '	ER.dtDenied, '
	   			 + '	TTUA.destinationId, '
	   			 + '	ER.rejectionComments '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEnrollmentRequest] ER '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = ER.idCourse AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = ER.idUser AND TTU.object = ''users'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUA on (TTU.sourceId = ER.idApprover AND TTU.object = ''users'') '
	   			 + 'WHERE ER.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.8: Copy Enrollment - Enrollment Requests Inserted', 0)

		/*

		LESSON (MODULE) DATA

		*/

		-- copy lesson (module) data records
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-Lesson] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEnrollment, '
	   			 + '	idLesson, '
	   			 + '	idTimezone, '
	   			 + '	title, '
	   			 + '	revcode, '
	   			 + '	[order], '
	   			 + '	contentTypeCommittedTo, '
	   			 + '	dtCommittedToContentType, '
	   			 + '	dtCompleted, '
	   			 + '	resetForContentChange, '
	   			 + '	preventPostCompletionLaunchForContentChange '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTE.destinationId, '
	   			 + '	TTL.destinationId, '
	   			 + '	DL.idTimezone, '
				 + '	DL.title + '' ##'' + CAST(DL.[idData-Lesson] AS NVARCHAR) + ''##'', '
	   			 + '	DL.revcode, '
	   			 + '	DL.[order], '
	   			 + '	DL.contentTypeCommittedTo, '
	   			 + '	DL.dtCommittedToContentType, '
	   			 + '	DL.dtCompleted, '
	   			 + '	DL.resetForContentChange, '
	   			 + '	DL.preventPostCompletionLaunchForContentChange '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-Lesson] DL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTE on (TTE.sourceId = DL.idEnrollment AND TTE.object = ''enrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTL on (TTL.sourceId = DL.idLesson AND TTL.object = ''lesson'') '
	   			 + 'WHERE DL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.9: Copy Enrollment - Lesson (Module) Data Inserted', 0)
	   		
		-- insert the mapping for source to destination lesson data id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SD.[idData-Lesson], '
	   			 + '	DD.[idData-Lesson], '
	   			 + '	''datalesson'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-Lesson] DD '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-Lesson] SD '
	   			 + 'ON (SD.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SD.title + '' ##'' + CAST(SD.[idData-Lesson] AS NVARCHAR) + ''##'' = DD.title)'
	   			 + 'WHERE DD.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.10: Copy Enrollment - Lesson (Module) Data Mappings Created', 0)	   	   

		-- clean up the ##idData-Lesson## additions we made to title, we did that to uniquely distinguish lesson data
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] SET '
				 + '	title = REPLACE(DLD.title, '' ##'' + CAST(SLD.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] DLD '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SLD ON SLD.destinationID = DLD.[idData-Lesson] AND SLD.object = ''datalesson'' '				 
				 + 'WHERE DLD.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SLD.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11.11: Copy Enrollment - Lesson (Module) Data Titles Cleaned Up', 0)

		/*

		RETURN

		*/	   
			   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_Enrollment_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 11: Copy Enrollment - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_Enrollment_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END