
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetObjectIdsForFileSystemCleanup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetObjectIdsForFileSystemCleanup]
GO


/*

Get Object IDs of the objects in the database

*/
CREATE PROCEDURE [dbo].[System.GetObjectIdsForFileSystemCleanup]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@DBServerName			NVARCHAR(50),
	@DatabaseName			NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	-- Create temp table to hold object IDs
	CREATE TABLE #ObjectIDs
	   (
		  [objectId] INT NOT NULL,
		  [objectName] NVARCHAR(100) NOT NULL,
		  [siteHostname] NVARCHAR(100) NOT NULL
	   )
	  
	/************************************************ CERTIFICATES ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		C.idCertificate,'
				+ '		''certificates'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblCertificate] C'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = C.idSite'
		
		EXEC (@sql)
	  
	  /************************************************ COURSES ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		C.idCourse,'
				+ '		''courses'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblCourse] C'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = C.idSite'
		
		EXEC (@sql)

		/************************************************ EMAIL NOTIFICATIONS ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		EN.idEventEmailNotification,'
				+ '		''emailNotifications'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblEventEmailNotification] EN'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = EN.idSite'
		
		EXEC (@sql)

		/************************************************ GROUPS ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		G.idGroup,'
				+ '		''groups'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblGroup] G'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = G.idSite'
		
		EXEC (@sql)

		/************************************************ LEARNING PATHS ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		LP.idLearningPath,'
				+ '		''learningPaths'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblLearningPath] LP'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = LP.idSite'
		
		EXEC (@sql)

		/************************************************ ILT ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		ILT.idStandUpTraining,'
				+ '		''standuptraining'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblStandUpTraining] ILT'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = ILT.idSite'
		
		EXEC (@sql)

		/************************************************ USERS ******************************************************/
		
		SET @sql = 'INSERT INTO [#ObjectIDs]'
				+ ' ( '
				+ '		objectId, '
				+ '		objectName, '
				+ '		siteHostname '
				+ ' ) '
				+ ' SELECT ' 
				+ '		U.idUser,'
				+ '		''users'','
				+ '		S.hostname'
				+ ' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblUser] U'
				+ ' LEFT JOIN [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].' + '[tblSite] S'
				+ ' ON S.idSite = U.idSite'
		
		EXEC (@sql)
	  
		
		SELECT objectId, objectName, siteHostname FROM [#ObjectIDs]
		
		
		-- Drop temporary object IDs table
		
		DROP TABLE #ObjectIDs
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemGetObjectIdsForFileSystemCleanup_Success'
		
	SET XACT_ABORT OFF

END
GO