SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Data]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Data]
GO


/*

CLONE PORTAL - MODULE/SCO/XAPI DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.Data]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON
    
	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.0: Copy Data - Initialized', 0)

    BEGIN TRY	
    
		DECLARE @sql	NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	  
		-- copy sco data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCO] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[idData-Lesson], '
	   			 + '	idTimezone, '
	   			 + '	manifestIdentifier, '
	   			 + '	completionStatus, '
	   			 + '	successStatus, '
	   			 + '	scoreScaled, '
	   			 + '	totalTime, '
	   			 + '	[timestamp], '
	   			 + '	actualAttemptCount, '
	   			 + '	effectiveAttemptCount, '
	   			 + '	proctoringUser '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTDL.destinationId, '
	   			 + '	DS.idTimezone, '
	   			 + '	DS.manifestIdentifier, '
	   			 + '	DS.completionStatus, '
	   			 + '	DS.successStatus, '
	   			 + '	DS.scoreScaled, '
	   			 + '	DS.totalTime, '
	   			 + '	DS.[timestamp], '
	   			 + '	DS.actualAttemptCount, '
	   			 + '	DS.effectiveAttemptCount, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCO] DS '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on (TTDL.sourceId = DS.[idData-Lesson] AND TTDL.object = ''datalesson'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DS.[proctoringUser] AND TTU.object = ''users'') '
	   			 + 'WHERE DS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDL.destinationId IS NOT NULL '
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.1: Copy Data - Data SCO Records Inserted', 0)
	
		-- insert the mapping for source to destination data sco ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SDS.[idData-SCO], '
	   			 + '	DDS.[idData-SCO], '
	   			 + '	''datasco'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCO] DDS '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on (TTDL.destinationId = DDS.[idData-Lesson] AND TTDL.object = ''datalesson'') '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCO] SDS '
	   			 + 'ON (SDS.idSite =' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SDS.[idData-Lesson] = TTDL.sourceId)) '	   			 
	   			 + 'WHERE DDS.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SDS.[idData-SCO] IS NOT NULL AND DDS.[idData-SCO] IS NOT NULL'
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.2: Copy Data - Data SCO Record Mappings Created', 0)

		-- copy homework assignment data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-HomeworkAssignment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[idData-Lesson], '
	   			 + '	uploadedAssignmentFilename, '
	   			 + '	dtUploaded, '
	   			 + '	completionStatus, '
	   			 + '	successStatus, '
	   			 + '	score, '
	   			 + '	[timestamp], '
	   			 + '	proctoringUser '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTDL.destinationId, '
	   			 + '	DHA.uploadedAssignmentFilename, '
	   			 + '	DHA.dtUploaded, '
	   			 + '	DHA.completionStatus, '
	   			 + '	DHA.successStatus, '
	   			 + '	DHA.score, '
	   			 + '	DHA.[timestamp], '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-HomeworkAssignment] DHA '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on (TTDL.sourceId = DHA.[idData-Lesson] AND TTDL.object = ''datalesson'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DHA.[proctoringUser] AND TTU.object = ''users'') '
	   			 + 'WHERE DHA.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDL.destinationId IS NOT NULL '
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.3: Copy Data - Homework Assignment Data Records Inserted', 0)

		-- copy xapi oauth consumers
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	label, '
	   			 + '	oAuthKey, '
	   			 + '	oAuthSecret, '
	   			 + '	isActive '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	XAPIAC.label, '
	   			 + '	XAPIAC.oAuthKey, '
	   			 + '	XAPIAC.oAuthSecret, '
	   			 + '	XAPIAC.isActive '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] XAPIAC '
	   			 + 'WHERE XAPIAC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   								
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.4: Copy Data - XAPI Oauth Consumers Inserted', 0)

		-- insert the mapping for source to destination xapi oauth consumer ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + ' SELECT '
	   			 + '	SP.idxAPIoAuthConsumer, '
	   			 + '	DP.idxAPIoAuthConsumer, '
	   			 + '	''xapioauthconsumer'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] DP '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] SP '
	   			 + 'ON (SP.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SP.label = DP.label OR (SP.label IS NULL AND DP.label IS NULL)) '
	   			 + '	AND (SP.oAuthKey = DP.oAuthKey OR (SP.oAuthKey IS NULL AND DP.oAuthKey IS NULL)) '
	   			 + '	AND (SP.oAuthSecret = DP.oAuthSecret OR (SP.oAuthSecret IS NULL AND DP.oAuthSecret IS NULL))) '
	   			 + 'WHERE DP.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.5: Copy Data - XAPI Oauth Consumer Mappings Created', 0)

		-- copy xapi data records
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCan] '
	   			 + '( '
	   			 + '	[idData-Lesson], '
	   			 + '	[idData-TinCanParent], '
	   			 + '	idSite, '
	   			 + '	idEndpoint, '
	   			 + '	isInternalAPI, '
	   			 + '	statementId, '
	   			 + '	actor, '
	   			 + '	verbId, '
	   			 + '	verb, '
	   			 + '	activityId, '
	   			 + '	mboxObject, '
	   			 + '	mboxSha1SumObject, '
	   			 + '	openIdObject, '
	   			 + '	accountObject, '
	   			 + '	mboxActor, '
	   			 + '	mboxSha1SumActor, '
	   			 + '	openIdActor, '
	   			 + '	accountActor, '
	   			 + '	mboxAuthority, '
	   			 + '	mboxSha1SumAuthority, '
	   			 + '	openIdAuthority, '
	   			 + '	accountAuthority, '
	   			 + '	mboxTeam, '
	   			 + '	mboxSha1SumTeam, '
	   			 + '	openIdTeam, '
	   			 + '	accountTeam, '
	   			 + '	mboxInstructor, '
	   			 + '	mboxSha1SumInstructor, '
	   			 + '	openIdInstructor, '
	   			 + '	accountInstructor, '
	   			 + '	[object], '
	   			 + '	registration, '
	   			 + '	[statement], '
	   			 + '	isVoidingStatement, '
	   			 + '	isStatementVoided, '
	   			 + '	dtStored, '
	   			 + '	scoreScaled '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTDL.destinationId, '
	   			 + '	NULL, '			     -- this null value of [idData-TinCanParent] will be updated from temporary DataTinCanIdMappings table below 
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTXAAC.destinationId, '
	   			 + '	DTC.isInternalAPI, '
	   			 + '	DTC.statementId, '
	   			 + '	DTC.actor, '
	   			 + '	DTC.verbId, '
	   			 + '	DTC.verb, '
	   			 + '	DTC.activityId, '
	   			 + '	DTC.mboxObject, '
	   			 + '	DTC.mboxSha1SumObject, '
	   			 + '	DTC.openIdObject, '
	   			 + '	DTC.accountObject, '
	   			 + '	DTC.mboxActor, '
	   			 + '	DTC.mboxSha1SumActor, '
	   			 + '	DTC.openIdActor, '
	   			 + '	DTC.accountActor, '
	   			 + '	DTC.mboxAuthority, '
	   			 + '	DTC.mboxSha1SumAuthority, '
	   			 + '	DTC.openIdAuthority, '
	   			 + '	DTC.accountAuthority, '
	   			 + '	DTC.mboxTeam, '
	   			 + '	DTC.mboxSha1SumTeam, '
	   			 + '	DTC.openIdTeam, '
	   			 + '	DTC.accountTeam, '
	   			 + '	DTC.mboxInstructor, '
	   			 + '	DTC.mboxSha1SumInstructor, '
	   			 + '	DTC.openIdInstructor, '
	   			 + '	DTC.accountInstructor, '
	   			 + '	DTC.[object], '
	   			 + '	DTC.registration, '
	   			 + '	DTC.[statement], '
	   			 + '	DTC.isVoidingStatement, '
	   			 + '	DTC.isStatementVoided, '
	   			 + '	DTC.dtStored, '
	   			 + '	DTC.scoreScaled '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCan] DTC '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on TTDL.sourceId = DTC.[idData-Lesson] AND TTDL.object = ''datalesson'' '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTXAAC on TTXAAC.sourceId = DTC.[idEndpoint] AND TTXAAC.object = ''xapioauthconsumer'' '
	   			 + 'WHERE DTC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDL.destinationId IS NOT NULL'
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.6: Copy Data - XAPI Data Inserted', 0)
	   
		-- insert the mapping for source to destination catalog ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SDS.[idData-TinCan], '
	   			 + '	DDS.[idData-TinCan], '
	   			 + '	''datatincan'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCan] DDS '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCan] SDS '
	   			 + 'ON (SDS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SDS.statementId = DDS.statementId OR (SDS.statementId IS NULL AND DDS.statementId IS NULL)) '
	   			 + '	AND (SDS.dtStored = DDS.dtStored)) '
	   			 + 'WHERE DDS.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.7: Copy Data - XAPI Data Mappings Created', 0)
	   
		-- update [idData-TinCanParent] from temp table DataTinCan
		SET @sql = 'UPDATE DDTC '
	   			 + '	SET [idData-TinCanParent] = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCan] SDTC '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCan] DDTC '
	   			 + 'ON (SDTC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SDTC.statementId = DDTC.statementId OR (SDTC.statementId IS NULL AND DDTC.statementId IS NULL)) '
	   			 + '	AND (SDTC.dtStored = DDTC.dtStored OR (SDTC.dtStored IS NULL AND DDTC.dtStored IS NULL))) '
	   			 + 'INNER JOIN  [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SDTC.[idData-TinCanParent] AND TEMP.object = ''datatincan'') '
	   			 + 'WHERE DDTC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SDTC.[idData-TinCanParent] IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.8: Copy Data - XAPI Data Parent Ids Updated', 0)

		-- copy xapi oauth nonces
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblxAPIoAuthNonce] '
	   			 + '( '
	   			 + '	idxAPIoAuthConsumer, '
	   			 + '	nonce '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTXAAC.destinationId, '
	   			 + '	xAPIAN.nonce '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblxAPIoAuthNonce] xAPIAN '
	   			 + 'INNER JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblxAPIoAuthConsumer] xAPIAC  ON  xAPIAC.idxAPIoAuthConsumer= xAPIAN.idxAPIoAuthConsumer '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTXAAC on TTXAAC.sourceId = xAPIAN.[idxAPIoAuthConsumer] AND TTXAAC.object = ''xapioauthconsumer'' '
	   			 + 'WHERE xAPIAC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 		
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.9: Copy Data - XAPI Oauth Nonces Inserted', 0)

		-- copy xapi profiles
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCanProfile] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEndpoint, '
	   			 + '	profileId, '
	   			 + '	activityId, '
	   			 + '	agent, '
	   			 + '	contentType, '
	   			 + '	docContents, '
	   			 + '	dtUpdated '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTXAAC.destinationId, '
	   			 + '	DTCP.profileId, '
	   			 + '	DTCP.activityId, '
	   			 + '	DTCP.agent, '
	   			 + '	DTCP.contentType, '
	   			 + '	DTCP.docContents, '
	   			 + '	DTCP.dtUpdated '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCanProfile] DTCP '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTXAAC on TTXAAC.sourceId = DTCP.[idEndpoint] AND TTXAAC.object = ''xapioauthconsumer'' '
	   			 + 'WHERE DTCP.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.10: Copy Data - XAPI Profiles Inserted', 0)

		-- copy xapi state
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCanState] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEndpoint, '
	   			 + '	stateId, '
	   			 + '	agent, '
	   			 + '	activityId, '
	   			 + '	registration, '
	   			 + '	contentType, '
	   			 + '	docContents, '
	   			 + '	dtUpdated '
	   			 + ') '
	   			 + ' SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTXAAC.destinationId, '
	   			 + '	DTCS.stateId, '
	   			 + '	DTCS.agent, '
	   			 + '	DTCS.activityId, '
	   			 + '	DTCS.registration, '
	   			 + '	DTCS.contentType, '
	   			 + '	DTCS.docContents, '
	   			 + '	DTCS.dtUpdated '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCanState] DTCS '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTXAAC on TTXAAC.sourceId = DTCS.[idEndpoint] AND TTXAAC.object = ''xapioauthconsumer'' '
	   			 + 'WHERE DTCS.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.11: Copy Data - XAPI State Inserted', 0)

		-- copy api context activities
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-TinCanContextActivities] '
	   			 + '( '
	   			 + '	[idData-TinCan], '
	   			 + '	idSite, '
	   			 + '	activityId, '
	   			 + '	activityType, '
	   			 + '	objectType '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTDTC.destinationId, '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	DTCA.activityId, '
	   			 + '	DTCA.activityType, '
	   			 + '	DTCA.objectType '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-TinCanContextActivities] DTCA '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDTC on (TTDTC.sourceId = DTCA.[idData-TinCan] AND TTDTC.object = ''datatincan'') '
	   			 + 'WHERE DTCA.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDTC.destinationId IS NOT NULL'
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.12: Copy Data - XAPI Context Activities Inserted', 0)	

		-- copy scorm interactions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCOInt] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[idData-SCO], '
	   			 + '	[identifier], '
	   			 + '	[timestamp], '
	   			 + '	[result], '
	   			 + '	[latency], '
	   			 + '	[scoIdentifier] '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTDS.destinationId, '	   			 
				 + '	DSI.identifier  + '' ##'' + CAST(DSI.[idData-SCOInt] AS NVARCHAR) + ''##'', '
	   			 + '	DSI.[timestamp], '
	   			 + '	DSI.result, '
	   			 + '	DSI.latency, '
	   			 + '	DSI.scoIdentifier '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCOInt] DSI '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDS on (TTDS.sourceId = DSI.[idData-SCO] AND TTDS.object = ''datasco'') '
	   			 + 'WHERE DSI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDS.destinationId IS NOT NULL'
	   	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.13: Copy Data - SCORM Interactions Inserted', 0)

		-- insert the mapping for source to destination scorm interaction ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.[idData-SCOInt], '
	   			 + '	DST.[idData-SCOInt], '
	   			 + '	''scorminteraction'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCOInt] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCOInt] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.identifier + '' ##'' + CAST(SRC.[idData-SCOInt] AS NVARCHAR) + ''##'' = DST.identifier) '
	   			 + 'WHERE DST.idSite =' + CAST(@idSiteDestination AS NVARCHAR)
				 		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.14: Copy Data - SCORM Interaction Mappings Created', 0)

		-- clean up the ##idData-SCOInt## additions we made to scorm interaction identifiers, we did that to uniquely distinguish scorm interactions
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCOInt] SET '
				 + '	identifier = REPLACE(DSI.identifier, '' ##'' + CAST(SSI.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCOInt] DSI '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SSI ON SSI.destinationID = DSI.[idData-SCOInt] AND SSI.object = ''scorminteraction'' '
				 + 'WHERE DSI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SSI.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.15: Copy Data - SCORM Interaction Identifiers Cleaned Up', 0)

		-- copy scorm objectives
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-SCOObj] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[idData-SCO], '
	   			 + '	[identifier], '
	   			 + '	[scoreScaled], '
	   			 + '	[completionStatus], '
	   			 + '	[successStatus] '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTDS.destinationId, '
	   			 + '	DSO.identifier, '
	   			 + '	DSO.scoreScaled, '
	   			 + '	DSO.completionStatus, '
	   			 + '	DSO.successStatus '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-SCOObj] DSO '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDS on (TTDS.sourceId = DSO.[idData-SCO] AND TTDS.object = ''datasco'') '
	   			 + 'WHERE DSO.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13.16: Copy Data - SCORM Objectives Inserted', 0)

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_Data_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 13: Copy Data - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_Data_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END