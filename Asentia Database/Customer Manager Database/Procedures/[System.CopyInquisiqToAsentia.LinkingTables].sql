
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.LinkingTables]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.LinkingTables]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.LinkingTables]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	/*****************************************************LINKING TABLES****************************************************************/
		-- tblCourseToCatalogLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToCatalogLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idCatalog '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		COM.destinationID,'								-- idCourse
				+ '		CAM.destinationID'								-- idCatalog
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseCatalogLink] CCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] COM ON COM.sourceID = CCL.idCourse AND COM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CAM ON CAM.sourceID = CCL.idCatalog AND CAM.object = ''catalog'''
				+ ' WHERE COM.destinationID IS NOT NULL AND CAM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCatalogAccessToGroupLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalogAccessToGroupLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idGroup, '
				+ '		idCatalog, '
				+ '		created '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		GRM.destinationID,'								-- idGroup
				+ '		CAM.destinationID,'								-- idCatalog
				+ '		GETUTCDATE()'									-- created
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupRight] GR'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GRM ON GRM.sourceID = GR.idGroup AND GRM.object = ''group'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CAM ON CAM.sourceID = GR.idScope AND CAM.object = ''catalog'''
				+ ' WHERE CAM.destinationID IS NOT NULL AND CAM.destinationID IS NOT NULL'
		
		EXEC (@sql)
	   
		-- tblCourseToPrerequisiteLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToPrerequisiteLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idPrerequisite '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- idCourse
				+ '		PM.destinationID'								-- idPrerequisite
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCoursePrerequisiteLink] CPL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = CPL.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] PM ON PM.sourceID = CPL.idPrerequisite AND PM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL AND PM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		
		-- tblCouponCodeToCourseLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCodeToCourseLink]'
				+ ' ( '
				+ '		idCouponCode, '
				+ '		idCourse '
				+ ' ) '
				+ ' SELECT ' 
				+ '		CC.destinationID,'	-- idCouponCode
				+ '		C.destinationID'	-- idCourse
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCourseLink] CCCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CC ON CC.sourceID = CCCL.idCouponCode AND CC.object = ''couponcode'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CCCL.idCourse AND C.object = ''course'''
				+ ' WHERE CC.destinationID IS NOT NULL AND C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCouponCodeToCatalogLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCodeToCatalogLink]'
				+ ' ( '
				+ '		idCouponCode, '
				+ '		idCatalog '
				+ ' ) '
				+ ' SELECT ' 
				+ '		CC.destinationID,'	-- idCouponCode
				+ '		C.destinationID'	-- idCatalog
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCatalogLink] CCCL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CC ON CC.sourceID = CCCL.idCouponCode AND CC.object = ''couponcode'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CCCL.idCatalog AND C.object = ''catalog'''
				+ ' WHERE CC.destinationID IS NOT NULL AND C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCourseToScreenshotLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseToScreenshotLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		filename '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '	-- idSite
				+ '		CM.destinationID, '									-- idCourse
				+ '		SS.s '												-- filename
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = C.idCourse AND CM.object = ''course'''
				+ ' CROSS APPLY (SELECT s FROM [dbo].[DelimitedStringToTable](C.sampleScreens, ''|'') WHERE C.sampleScreens IS NOT NULL AND C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ') SS'
		
		EXEC (@sql)
	   
		-- tblLessonToContentLink - link SCORM lesson types to content packages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLessonToContentLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idLesson, '
				+ '		idObject, '
				+ '		idContentType, '
				+ '		idAssignmentDocumentType, '
				+ '		allowSupervisorsAsProctor, '
				+ '		allowCourseExpertsAsProctor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '	-- idSite
				+ '		LM.destinationID, '									-- idLesson
				+ '		CPM.destinationID, '								-- idObject
				+ '		1, '												-- idContentType (1 = content package)
				+ '		NULL, '												-- idAssignmentDocumentType
				+ '		NULL, '												-- allow supervisors as proctor?
				+ '		NULL '												-- allow course experts as proctor?
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = L.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMResource] SR ON SR.idSCORMResource = L.idSCORMResource'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSCORMPackage] SP ON SP.idSCORMPackage = SR.idSCORMPackage'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CPM ON CPM.sourceID = SP.idSCORMPackage AND CPM.object = ''contentpackage'''
				+ ' WHERE L.idLessonType = 0 AND LM.destinationID IS NOT NULL AND CPM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblLessonToContentLink - link Classroom/Web Meeting lesson types to ILT modules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLessonToContentLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idLesson, '
				+ '		idObject, '
				+ '		idContentType, '
				+ '		idAssignmentDocumentType, '
				+ '		allowSupervisorsAsProctor, '
				+ '		allowCourseExpertsAsProctor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) +', '	-- idSite
				+ '		LM.destinationID, '								-- idLesson
				+ '		ILTM.destinationID, '							-- idObject
				+ '		2, '											-- idContentType (2 = ILT)
				+ '		NULL, '											-- idAssignmentDocumentType
				+ '		NULL, '											-- allow supervisors as proctor?
				+ '		NULL '											-- allow course experts as proctor?
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = L.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] ILTM ON ILTM.sourceID = L.idLesson AND ILTM.object = ''ilt'''
				+ ' WHERE L.idLessonType IN (1, 2) AND LM.destinationID IS NOT NULL AND ILTM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblUserToGroupLink -- make all user to group links seem as if the user were manually placed into the group,
							  -- post conversion, after group rules have run, we will need to clean up the "manual" entries
							  -- left over where the user gets a group entry by rule
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUserToGroupLink]'
				+ ' ( '
				+ '		idSite, '
				+ '		idUser, '
				+ '		idGroup, '
				+ '		idRuleSet, '
				+ '		created, '
				+ '		selfJoined '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		UM.destinationID,'								-- idUser
				+ '		GM.destinationID, '								-- idGroup
				+ '		NULL, '											-- idRuleSet
				+ '		GETUTCDATE(), '									-- created date
				+ '		0 '												-- self joined
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUserGroupLink] UGL'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = UGL.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GM ON GM.sourceID = UGL.idGroup AND GM.object = ''group'''
				+ ' WHERE UM.destinationID IS NOT NULL AND GM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaLinkingTables_Success'

		
	SET XACT_ABORT OFF

END
GO