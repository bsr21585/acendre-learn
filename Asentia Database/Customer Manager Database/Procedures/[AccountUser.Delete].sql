-- =====================================================================
-- PROCEDURE: [AccountUser.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[AccountUser.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.Delete]
GO

CREATE PROCEDURE [AccountUser.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (10)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	
	@AccountUsers				IDTable			READONLY	
)
AS
	
	BEGIN
	
	/* validate caller permission */
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller
	
	DECLARE @isCallerSystemAdmin BIT
	DECLARE @isCallerSystemUser BIT
	DECLARE @isCallerAccountAdmin BIT
	DECLARE @isCallerAccountUser BIT
	
	SELECT @isCallerSystemAdmin =	CASE WHEN @idCallerAccount = 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
								  
	SELECT @isCallerSystemUser =	CASE WHEN @idCallerAccount = 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountAdmin =	CASE WHEN @idCallerAccount > 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountUser =	CASE WHEN @idCallerAccount > 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	IF -- account users cannot delete any users
	   (@isCallerAccountUser = 1)
		BEGIN
		SET @Return_Code = 3 -- sort of (caller not member of specified site).
		SET @Error_Description_Code = 'AccountUserDelete_PermissionError'
		RETURN 1
		END
	
	/*
		check to see if there is anything to do
	*/
	
	IF (SELECT COUNT(1) FROM @AccountUsers) = 0
		BEGIN
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'AccountUserDelete_NoRecordFound'
		RETURN 1
		END
	
	/* 
	
	validate caller permissions to the objects being deleted 
	if there is any objects that the caller doesn't have permission
	to delete, then no deletes will happen
	
	*/
	
	IF @isCallerSystemAdmin <> 1
	
		BEGIN
		
		IF (
			SELECT COUNT(1)
			FROM @AccountUsers VAU
			LEFT JOIN tblAccountUser AU ON VAU.id = AU.idAccountUser
			WHERE (
					-- system users cannot delete other system users
					(@isCallerSystemUser = 1 AND AU.idAccount = 1)
					OR
					-- account administrators cannot delete users outside of their account
					(@isCallerAccountAdmin = 1 AND AU.idAccount <> @idCallerAccount)
				)
			) > 0 
			
			BEGIN
			SET @Return_Code = 3 -- sort of (caller not member of specified site).
			SET @Error_Description_Code = 'AccountUserDelete_PermissionError'
			RETURN 1
			END
				
		END
		
	/*
	
	Delete the account user(s).
	
	*/
	
	DELETE FROM tblAccountUser
	WHERE EXISTS (
		SELECT 1
		FROM @AccountUsers VAU
		WHERE tblAccountUser.idAccountUser = VAU.id
	)
	
	SELECT @Return_Code = 0
	
	SET NOCOUNT ON
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO