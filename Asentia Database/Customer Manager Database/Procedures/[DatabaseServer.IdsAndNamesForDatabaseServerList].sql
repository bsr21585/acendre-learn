-- =====================================================================
-- PROCEDURE: [DatabaseServer.IdsAndNamesForDatabaseServerList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.IdsAndNamesForDatabaseServerList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.IdsAndNamesForDatabaseServerList]
GO

/*
Return all ids and database server name column values.
*/
CREATE PROCEDURE [DatabaseServer.IdsAndNamesForDatabaseServerList]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		INT				OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT
)
AS
	BEGIN
	SET NOCOUNT ON
		
	--DECLARE @idCallerRole INT
	--SELECT @idCallerRole = 0
	--SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller
	
	--DECLARE @isCallerViewingSelf BIT
	--DECLARE @isCallerSystemAdmin BIT
	--DECLARE @isCallerSystemUser BIT
	--DECLARE @isCallerAccountAdmin BIT
	--DECLARE @isCallerAccountUser BIT
	
	--SELECT @isCallerViewingSelf =	CASE WHEN @idAccountUser = @idCaller THEN 1 ELSE 0 END
	
	--SELECT @isCallerSystemAdmin =	CASE WHEN @idCallerAccount = 1 
	--										  AND @idCaller = 1 
	--										  AND @idCallerRole = 1 
	--								THEN 1 
	--								ELSE 0 END
								  
	--SELECT @isCallerSystemUser =	CASE WHEN @idCallerAccount = 1 
	--									      AND @idCaller > 1 
	--									      AND @idCallerRole = 1 
	--								THEN 1 
	--								ELSE 0 END
									
	--SELECT @isCallerAccountAdmin =	CASE WHEN @idCallerAccount > 1 
	--										  AND @idCaller = 1 
	--										  AND @idCallerRole = 2 
	--								THEN 1 
	--								ELSE 0 END
									
	--SELECT @isCallerAccountUser =	CASE WHEN @idCallerAccount > 1 
	--									      AND @idCaller > 1 
	--									      AND @idCallerRole = 2 
	--								THEN 1 
	--								ELSE 0 END
									
	-- if caller is not viewing self, make sure the
	-- caller is not an account user
	
	--IF @isCallerViewingSelf = 0
	--	BEGIN
	--	IF -- account users cannot view any other users
	--	   (@isCallerAccountUser = 1)
	--		BEGIN
	--		SET @Return_Code = 3 -- sort of (caller not member of specified site).
	--		SET @Error_Description_Code = 'AccountUserDetails_PermissionError'
	--		RETURN 1
	--		END
	--	END
		

	--/*
	
	--validate caller permission
	
	--*/
	
	--IF @isCallerSystemAdmin <> 1 OR @isCallerViewingSelf <> 1
	
	--	BEGIN
		
	--	IF (
	--		SELECT COUNT(1)
	--		FROM tblDatabase DB 
	--		WHERE DB.idDatabase = @idDatabase
	--		AND (
	--				-- system users cannot view other system users
	--				(@isCallerSystemUser = 1 AND DB.idDatabase = 1)
	--				OR
	--				-- account administrators cannot view users outside of their account
	--				(@isCallerAccountAdmin = 1 AND AU.idAccount <> @idCallerAccount)
	--			)
	--		) > 0 
			
	--		BEGIN
	--		SET @Return_Code = 3 -- sort of (caller not member of specified site).
	--		SET @Error_Description_Code = 'AccountUserDetails_PermissionError'
	--		RETURN 1
	--		END
				
	--	END
	
	/*
	
	get the data 
	
	*/
	
	SELECT
		DBS.idDatabaseServer,		
		DBS.serverName		
	FROM tblDataBaseServer DBS
	ORDER BY DBS.serverName
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO