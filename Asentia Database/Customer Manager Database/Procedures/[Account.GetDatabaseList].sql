-- =====================================================================
-- PROCEDURE: [Account.GetDatabaseList]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.GetDatabaseList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.GetDatabaseList]
GO


/*
Return all database ids and names.
*/
CREATE PROCEDURE [Account.GetDatabaseList]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@idDatabaseServer			INT,
	@callerLangString			NVARCHAR(10),
	@idCaller					INT
)
AS
	BEGIN
	SET NOCOUNT ON
		
	/*
	
	get the database details
	
	*/
	
	SELECT DISTINCT
		databaseName,
		idAccount
	FROM tblAccount  
	WHERE idDatabaseServer = @idDatabaseServer
	ORDER BY databaseName
	
	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
