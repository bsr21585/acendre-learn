SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.CatalogCourse]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.CatalogCourse]
GO


/*

CLONE PORTAL - CATALOG / COURSE / LESSON / LEARNING PATH DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.CatalogCourse]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.0: Copy CatalogCourse - Initialized', 0)
    
    BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
		
		/*
		
		CATALOGS
		
		*/	

		-- copy catalogs
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalog] '
				 + '( '
				 + '	idSite, '
				 + '	idParent, '
				 + '	title, '
				 + '	[order], '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	isPrivate, '
				 + '	isClosed, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	cost, '
				 + '	costType, '
				 + '	searchTags, '
				 + '	avatar '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	NULL, '     -- null value of idParent will be updated from CatalogIdMappings table below
				 + '	C.title, '
				 + '	C.[order], '
				 + '	C.shortDescription, '
				 + '	C.longDescription, '
				 + '	C.isPrivate, '
				 + '	C.isClosed, '
				 + '	C.dtCreated, '
				 + '	C.dtModified, '
				 + '	C.cost, '
				 + '	C.costType, '
				 + '	C.searchTags, '
				 + '	C.avatar '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalog] C '
				 + 'WHERE C.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.1: Copy CatalogCourse - Catalogs Inserted', 0)
	   
		-- insert the mapping for source to destination catalog ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCatalog, '
				 + '	DST.idCatalog, '
				 + '	''catalog'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalog] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalog] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND (SRC.title = DST.title) '
				 + '	AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL)) '
				 + '	AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL)) '
				 + ') '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.2: Copy CatalogCourse - Catalog Mappings Created', 0)
	   
		-- update catalog parent ids from id mappings table
		SET @sql = 'UPDATE DSTC '
	   			 + '	SET idParent = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalog] SRCC '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalog] DSTC '
	   			 + 'ON (SRCC.title = DSTC.title '
	   			 + '	AND (SRCC.dtCreated = DSTC.dtCreated OR (SRCC.dtCreated IS NULL AND DSTC.dtCreated IS NULL)) '
	   			 + '	AND (SRCC.dtModified = DSTC.dtModified OR (SRCC.dtModified IS NULL AND DSTC.dtModified IS NULL)) '
	   			 + '	AND SRCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)+ ') '
	   			 + 'INNER JOIN  [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCC.idParent AND TEMP.object = ''catalog'') '
	   			 + 'WHERE DSTC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' '
	   			 + 'AND SRCC.idParent IS NOT NULL '
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.3: Copy CatalogCourse - Catalog Parent Ids Updated', 0)	   
	   	   
		-- copy catalog languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCatalogLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCatalog, '
				 + '	idLanguage, '
				 + '	title, '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	searchTags '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	CL.idLanguage, '
				 + '	CL.title, '
				 + '	CL.shortDescription, '
				 + '	CL.longDescription, '
				 + '	CL.searchTags  '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCatalogLanguage] CL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CL.idCatalog AND TTC.object = ''catalog'') '
				 + 'WHERE CL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
								
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.4: Copy CatalogCourse - Catalog Languages Inserted', 0)

		/*

		COURSES

		*/

		-- copy courses
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourse] '
				 + '( '
				 + '	idSite, '
				 + '	title, '
				 + '	coursecode, '
				 + '	revcode, '
				 + '	avatar, '
				 + '	cost, '
				 + '	credits, '
				 + '	[minutes], '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	objectives, '
				 + '	socialMedia, '
				 + '	isPublished, '
				 + '	isClosed, '
				 + '	isLocked, '
				 + '	isPrerequisiteAny, '
				 + '	isFeedActive, '
				 + '	isFeedModerated, '
				 + '	isFeedOpenSubscription, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	[order], '
				 + '	disallowRating, '
				 + '	forceLessonCompletionInOrder, '
				 + '	selfEnrollmentDueInterval, '
				 + '	selfEnrollmentDueTimeframe, '
				 + '	selfEnrollmentExpiresFromStartInterval, '
				 + '	selfEnrollmentExpiresFromStartTimeframe, '
				 + '	selfEnrollmentExpiresFromFirstLaunchInterval, '
				 + '	selfEnrollmentExpiresFromFirstLaunchTimeframe, '
				 + '	searchTags, '
				 + '	requireFirstLessonToBeCompletedBeforeOthers, '
				 + '	lockLastLessonUntilOthersCompleted, '
				 + '	isSelfEnrollmentApprovalRequired, '
				 + '	isApprovalAllowedByCourseExperts, '
				 + '	isApprovalAllowedByUserSupervisor, '
				 + '	shortcode '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	C.title  + '' ##'' + CAST(C.idCourse AS NVARCHAR) + ''##'', '
				 + '	C.coursecode, '
				 + '	C.revcode, '
				 + '	C.avatar, '
				 + '	C.cost, '
				 + '	C.credits, '
				 + '	C.[minutes], '
				 + '	C.shortDescription, '
				 + '	C.longDescription, '
				 + '	C.objectives, '
				 + '	C.socialMedia, '
				 + '	C.isPublished, '
				 + '	C.isClosed, '
				 + '	C.isLocked, '
				 + '	C.isPrerequisiteAny, '
				 + '	C.isFeedActive, '
				 + '	C.isFeedModerated, '
				 + '	C.isFeedOpenSubscription, '
				 + '	C.dtCreated, '
				 + '	C.dtModified, '
				 + '	C.isDeleted, '
				 + '	C.dtDeleted, '
				 + '	C.[order], '
				 + '	C.disallowRating, '
				 + '	C.forceLessonCompletionInOrder, '
				 + '	C.selfEnrollmentDueInterval, '
				 + '	C.selfEnrollmentDueTimeframe, '
				 + '	C.selfEnrollmentExpiresFromStartInterval, '
				 + '	C.selfEnrollmentExpiresFromStartTimeframe, '
				 + '	C.selfEnrollmentExpiresFromFirstLaunchInterval, '
				 + '	C.selfEnrollmentExpiresFromFirstLaunchTimeframe, '
				 + '	C.searchTags, '
				 + '	C.requireFirstLessonToBeCompletedBeforeOthers, '
				 + '	C.lockLastLessonUntilOthersCompleted, '
				 + '	C.isSelfEnrollmentApprovalRequired, '
				 + '	C.isApprovalAllowedByCourseExperts, '
				 + '	C.isApprovalAllowedByUserSupervisor, '
				 + '	C.shortcode '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourse] C '
				 + 'WHERE C.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
							          										
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.5: Copy CatalogCourse - Courses Inserted', 0)
	   
		-- insert the mapping for source to destination course id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idCourse, '
	   			 + '	DST.idCourse, '
	   			 + '	''courses'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourse] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourse] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.title + '' ##'' + CAST(SRC.idCourse AS NVARCHAR) + ''##'' = DST.title) '
	   			 + 'WHERE DST.idSite =' + CAST(@idSiteDestination AS NVARCHAR)
				 		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.6: Copy CatalogCourse - Course Mappings Created', 0)

		-- clean up the ##idCourse## additions we made to course titles, we did that to uniquely distinguish courses
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] SET '
				 + '	title = REPLACE(DC.title, '' ##'' + CAST(SC.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] DC '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SC ON SC.destinationID = DC.idCourse AND SC.object = ''courses'' '
				 + 'WHERE DC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SC.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.7: Copy CatalogCourse - Course Titles Cleaned Up', 0)
						
		-- copy course languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idLanguage, '
	   			 + '	title, '
	   			 + '	shortDescription, '
	   			 + '	longDescription, '
	   			 + '	objectives, '
	   			 + '	searchTags '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	CL.idLanguage, '
	   			 + '	CL.title, '
	   			 + '	CL.shortDescription, '
	   			 + '	CL.longDescription, '
	   			 + '	CL.objectives, '
	   			 + '	CL.searchTags '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseLanguage] CL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CL.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 				
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.8: Copy CatalogCourse - Course Languages Inserted', 0)

		-- copy course to catalog links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseToCatalogLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCatalog, '
	   			 + '	idCourse, '
	   			 + '	[order] '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTCatalog.destinationId, '
	   			 + '	TTCourse.destinationId, '
	   			 + '	CTCL.[order] '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseToCatalogLink] CTCL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCatalog on (TTCatalog.sourceId = CTCL.idCatalog AND TTCatalog.object = ''catalog'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCourse on (TTCourse.sourceId = CTCL.idCourse AND TTCourse.object = ''courses'') '
	   			 + 'WHERE CTCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.9: Copy CatalogCourse - Course To Catalog Links Inserted', 0)

		-- copy course to prerequisite links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseToPrerequisiteLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idPrerequisite '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTCourse.destinationId, '
	   			 + '	TTPrerequisite.destinationId '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseToPrerequisiteLink] CTPL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCourse on (TTCourse.sourceId = CTPL.idCourse AND TTCourse.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTPrerequisite on (TTPrerequisite.sourceId = CTPL.idPrerequisite AND TTPrerequisite.object = ''courses'') '
	   			 + 'WHERE CTPL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.10: Copy CatalogCourse - Course To Prerequisite Links Inserted', 0)

		-- copy course to screenshot links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseToScreenshotLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	[filename] '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTCourse.destinationId, '
	   			 + '	CTSL.[filename] '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseToScreenshotLink] CTSL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCourse on (TTCourse.sourceId = CTSL.idCourse AND TTCourse.object = ''courses'') '
	   			 + 'WHERE CTSL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.11: Copy CatalogCourse - Course To Screenshot Links Inserted', 0)

		/*

		MODULES (LESSONS)

		*/

		-- copy modules (lessons)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLesson] '
				 + '( '
				 + '	idSite, '
				 + '	idCourse, '
				 + '	title, '
				 + '	revcode, '
				 + '	shortDescription, '
				 + '	longDescription, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	[order], '
				 + '	searchTags, '
				 + '	isOptional '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	L.title  + '' ##'' + CAST(L.idLesson AS NVARCHAR) + ''##'', '
				 + '	L.revcode, '
				 + '	L.shortDescription, '
				 + '	L.longDescription, '
				 + '	L.dtCreated, '
				 + '	L.dtModified, '
				 + '	L.isDeleted, '
				 + '	L.dtDeleted, '
				 + '	L.[order], '
				 + '	L.searchTags, '
				 + '	L.isOptional '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLesson] L '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = L.idCourse AND TTC.object = ''courses'') '
				 + 'WHERE L.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.12: Copy CatalogCourse - Modules Inserted', 0)
	   
		-- insert the mapping for source to destination course id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idLesson, '
				 + '	DST.idLesson, '
				 + '	''lesson'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLesson] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLesson] SRC '
				 + 'ON (SRC.idSite =' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.title + '' ##'' + CAST(SRC.idLesson AS NVARCHAR) + ''##'' = DST.title) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
		
	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.13: Copy CatalogCourse - Module Mappings Created', 0)

		-- clean up the ##idLesson## additions we made to lesson titles, we did that to uniquely distinguish lessons
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLesson] SET '
				 + '	title = REPLACE(DL.title, '' ##'' + CAST(SL.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLesson] DL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SL ON SL.destinationID = DL.idLesson AND SL.object = ''lesson'' '
				 + 'WHERE SL.sourceID IS NOT NULL '

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.14: Copy CatalogCourse - Module Titles Cleaned Up', 0)

		-- copy lesson languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLessonLanguage] '
				+ '( '
				+ '	idSite, '
				+ '	idLesson, '
				+ '	idLanguage, '
				+ '	title, '
				+ '	shortDescription, '
				+ '	longDescription, '
				+ '	searchTags '
				+ ') '
				+ 'SELECT '
				+	CAST(@idSiteDestination AS NVARCHAR) + ', '
				+ '	TTL.destinationId, '
				+ '	LL.idLanguage, '
				+ '	LL.title, '
				+ '	LL.shortDescription, '
				+ '	LL.longDescription, '
				+ '	LL.searchTags '
				+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLessonLanguage] LL '
				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTL on (TTL.sourceId = LL.idLesson AND TTL.object = ''lesson'') '
				+ 'WHERE LL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.15: Copy CatalogCourse - Module Languages Inserted', 0)
	                
		/*

		LEARNING PATHS

		*/

		-- copy learning paths
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPath] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	name, '
	   			 + '	shortDescription, '
	   			 + '	longDescription, '
	   			 + '	cost, '
	   			 + '	isPublished, '
	   			 + '	isClosed, '
	   			 + '	forceCompletionInOrder, '
	   			 + '	dtCreated, '
	   			 + '	dtModified, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	avatar, '
	   			 + '	searchTags, '
				 + '	shortcode '
	   			 + ') '
	   			 + 'SELECT '
	   			 +	CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	LP.name  + '' ##'' + CAST(LP.idLearningPath AS NVARCHAR) + ''##'', '
	   			 + '	LP.shortDescription, '
	   			 + '	LP.longDescription, '
	   			 + '	LP.cost, '
	   			 + '	LP.isPublished, '
	   			 + '	LP.isClosed, '
	   			 + '	LP.forceCompletionInOrder, '
	   			 + '	LP.dtCreated, '
	   			 + '	LP.dtModified, '
	   			 + '	LP.isDeleted, '
	   			 + '	LP.dtDeleted, '
	   			 + '	LP.avatar, '
	   			 + '	LP.searchTags, '
				 + '	LP.shortcode '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPath] LP '
	   			 + 'WHERE LP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.16: Copy CatalogCourse - Learning Paths Inserted', 0)

		-- insert the mapping for source to destination learning path id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SLP.idLearningPath, '
	   			 + '	DLP.idLearningPath, '
	   			 + '	''learningPaths'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPath] DLP '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPath] SLP '
	   			 + 'ON (SLP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SLP.name + '' ##'' + CAST(SLP.idLearningPath AS NVARCHAR) + ''##'' = DLP.name) '
	   			 + 'WHERE DLP.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			
		EXEC(@sql)	 

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.17: Copy CatalogCourse - Learning Path Mappings Created', 0)

		-- clean up the ##idLearningPath## additions we made to learning path names, we did that to uniquely distinguish lessons
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLearningPath] SET '
				 + '	name = REPLACE(DLP.name, '' ##'' + CAST(SLP.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLearningPath] DLP '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SLP ON SLP.destinationID = DLP.idLearningPath AND SLP.object = ''learningPaths'' '
				 + 'WHERE SLP.sourceID IS NOT NULL '

		EXEC (@sql)	   

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.18: Copy CatalogCourse - Learning Path Titles Cleaned Up', 0)
			   
		-- copy learning path languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idLearningPath, '
	   			 + '	idLanguage, '
	   			 + '	name, '
	   			 + '	shortDescription, '
	   			 + '	longDescription, '
	   			 + '	searchTags '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTLP.destinationId, '
	   			 + '	LPL.idLanguage, '
	   			 + '	LPL.name, '
	   			 + '	LPL.shortDescription, '
	   			 + '	LPL.longDescription, '
	   			 + '	LPL.searchTags '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathLanguage] LPL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = LPL.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'WHERE LPL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.19: Copy CatalogCourse - Learning Path Languages Inserted', 0)

		-- copy learning path to course links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLearningPathToCourseLink] '
	   			+ '( '
	   			+ '	idSite, '
	   			+ '	idLearningPath, '
	   			+ '	idCourse, '
	   			+ '	[order] '
	   			+ ') '
	   			+ 'SELECT '
	   			+	CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			+ '	TTLP.destinationId, '
	   			+ '	TTC.destinationId, '
	   			+ '	LPCL.[order] '
	   			+ 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLearningPathToCourseLink] LPCL '
	   			+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = LPCL.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = LPCL.idCourse AND TTC.object = ''courses'') '
	   			+ 'WHERE LPCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.20: Copy CatalogCourse - Learning Path Course Links Inserted', 0)

		-- copy rule set learning path enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idLearningPath, '
	   			 + '	idTimezone, '
	   			 + '	priority, '
	   			 + '	label, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	delayInterval, '
	   			 + '	delayTimeframe, '
	   			 + '	dueInterval, '
	   			 + '	dueTimeframe, '
				 + '	expiresFromStartInterval, '
				 + '	expiresFromStartTimeframe, '
				 + '	expiresFromFirstLaunchInterval, '
				 + '	expiresFromFirstLaunchTimeframe '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTLP.destinationId, '
	   			 + '	RSLPE.idTimezone, '
	   			 + '	RSLPE.priority, '
				 + '	RSLPE.label  + '' ##'' + CAST(RSLPE.idRuleSetLearningPathEnrollment AS NVARCHAR) + ''##'', '
	   			 + '	RSLPE.dtStart, '
	   			 + '	RSLPE.dtEnd, '
	   			 + '	RSLPE.delayInterval, '
	   			 + '	RSLPE.delayTimeframe, '
	   			 + '	RSLPE.dueInterval, '
	   			 + '	RSLPE.dueTimeframe, '
				 + '	RSLPE.expiresFromStartInterval, '
				 + '	RSLPE.expiresFromStartTimeframe, '
				 + '	RSLPE.expiresFromFirstLaunchInterval, '
				 + '	RSLPE.expiresFromFirstLaunchTimeframe '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollment] RSLPE '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = RSLPE.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'WHERE RSLPE.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 	
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.21: Copy CatalogCourse - RuleSet Learning Path Enrollments Inserted', 0)

		-- insert the mapping for source to destination rule set learning path enrollment id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SCC.idRuleSetLearningPathEnrollment, '
	   			 + '	DCC.idRuleSetLearningPathEnrollment, '
	   			 + '	''rulesetlearningpathenrollment'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollment] DCC '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollment] SCC '
	   			 + 'ON (SCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SCC.label + '' ##'' + CAST(SCC.idRuleSetLearningPathEnrollment AS NVARCHAR) + ''##'' = DCC.label) '
	   			 + 'WHERE DCC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)		
	   
		EXEC(@sql)	

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.22: Copy CatalogCourse - RuleSet Learning Path Enrollment Mappings Created', 0)

		-- clean up the ##idRuleSetLearningPathEnrollment## additions we made to learning path names, we did that to uniquely distinguish lessons
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleSetLearningPathEnrollment] SET '
				 + '	label = REPLACE(DCC.label, '' ##'' + CAST(SCC.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleSetLearningPathEnrollment] DCC '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SCC ON SCC.destinationID = DCC.idRuleSetLearningPathEnrollment AND SCC.object = ''rulesetlearningpathenrollment'' '
				 + 'WHERE SCC.sourceID IS NOT NULL '

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.23: Copy CatalogCourse - Ruleset Learning Path Enrollment Titles Cleaned Up', 0)
	   
		-- copy ruleset learning path enrollment languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollmentLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSetLearningPathEnrollment, '
	   			 + '	idLanguage, '
	   			 + '	label '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTRSLPE.destinationId, '
	   			 + '	RSLPEL.idLanguage, '
	   			 + '	RSLPEL.label '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetLearningPathEnrollmentLanguage] RSLPEL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRSLPE on (TTRSLPE.sourceId = RSLPEL.idRuleSetLearningPathEnrollment AND TTRSLPE.object = ''rulesetlearningpathenrollment'') '
	   			 + 'WHERE RSLPEL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2.24: Copy CatalogCourse - RuleSet Learning Path Enrollment Languages Inserted', 0)

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_CatalogCourse_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 2: Copy CatalogCourse - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_CatalogCourse_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END