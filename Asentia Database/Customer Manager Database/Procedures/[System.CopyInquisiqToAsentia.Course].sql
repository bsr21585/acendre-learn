
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Course]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Course]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Course]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	/**********************************************COURSE************************************************************/
		-- tblCourse
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse]'
				+ ' ( '
				+ '		idSite, '
				+ '		title, '
				+ '		coursecode, '
				+ '		revcode, '
				+ '		cost, '
				+ '		credits, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isPublished, '
				+ '		isClosed, '
				+ '		isLocked,'
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		minutes,'
				+ '		objectives,'
				+ '		socialMedia,'
				+ '		isPrerequisiteAny,'
				+ '		avatar,'
				+ '		isFeedActive,'
				+ '		isFeedModerated,'
				+ '		isFeedOpenSubscription,'
				+ '		disallowRating,'
				+ '		forceLessonCompletionInOrder,'
				+ '		selfEnrollmentDueInterval,'
				+ '		selfEnrollmentDueTimeframe,'
				+ '		selfEnrollmentExpiresFromStartInterval,'
				+ '		selfEnrollmentExpiresFromStartTimeframe,'
				+ '		selfEnrollmentExpiresFromFirstLaunchInterval,'
				+ '		selfEnrollmentExpiresFromFirstLaunchTimeframe,'
				+ '		searchTags,'
				+ '		requireFirstLessonToBeCompletedBeforeOthers,'
				+ '		lockLastLessonUntilOthersCompleted,'
				+ '		isSelfEnrollmentApprovalRequired,'
				+ '		isApprovalAllowedByCourseExperts,'
				+ '		isApprovalAllowedByUserSupervisor'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite				
				+ '		C.name + '' ##'' + CONVERT(NVARCHAR, C.idCourse) + ''##'','	-- title
				+ '		C.code,'										-- code
				+ '		NULL,'											-- revcode
				+ '		C.cost,'										-- cost
				+ '		C.credits,'										-- credits
				+ '		C.shortDescription,'							-- short description
				+ '		C.description,'									-- description
				+ '		C.isPublished,'									-- is published?
				+ '		C.isClosed,'									-- is closed?	
				+ '		C.isLocked,'									-- is locked?
				+ '		C.dtCreated,'									-- date created
				+ '		C.dtModified,'									-- date modified
				+ '		0,'												-- is deleted?
				+ '		NULL,'											-- date deleted
				+ '		C.minutes,'										-- minutes
				+ '		C.objectives,'									-- objectives
				+ '		REPLACE(REPLACE(C.socialMedia, ''protocol="http://"'', ''protocol="http://" iconType="default"''), ''protocol="https://"'', ''protocol="https://" iconType="default"''),' -- social media
				+ '		C.prerequisiteAny,'								-- any or all prerequisites?
				+ '		NULL,'											-- avatar
				+ '		0,'												-- is feed active?
				+ '		0,'												-- is feed moderated?
				+ '		0,'												-- is feed open subscription?
				+ '		~C.allowRating,'								-- disallow rating? (negate it because Asentia's field is "disallow")
				+ '		C.blnForceLessonCompletionInOrder,'				-- force lesson completion in order?
				+ '		NULL,'											-- self enrollment due interval
				+ '		NULL,'											-- self enrollment due timeframe
				+ '		C.accessInterval,'								-- self enrollment expires from start interval
				+ '		C.accessTimeFrame,'								-- self enrollment expires from start timeframe
				+ '		NULL,'											-- self enrollment expires from first launch interval
				+ '		NULL,'											-- self enrollment expires from first launch timeframe
				+ '		NULL,'											-- search tags
				+ '		C.blnFirstLessonRequired,'						-- require first lesson to be completed before others
				+ '		C.blnLastLessonLocked,'							-- lock last lesson until others completed
				+ '		NULL,'											-- is self enrollment approval required?
				+ '		NULL,'											-- is approval allowed by course experts?
				+ '		NULL'											-- is approval allowed by user supervisor?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idCourse mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCourse, '
				+ '		DC.idCourse,'
				+ '		''course''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] SC ON SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SC.name + '' ##'' + CONVERT(NVARCHAR, SC.idCourse) + ''##'' = DC.title collate database_default AND SC.dtModified = DC.dtModified'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCourse IS NOT NULL AND DC.idCourse IS NOT NULL'
		
		EXEC(@sql)		

		-- clean up the ##idCourse## additions we made to course titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] SET '
				+ '		title = REPLACE(DST.title, '' ##'' + CONVERT(NVARCHAR, SRC.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourse] DST '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SRC ON SRC.destinationID = DST.idCourse AND SRC.object = ''course'''
				+ ' WHERE SRC.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblCourseLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourseLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCourse,'
				+ '		idLanguage,'
				+ '		title, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		objectives, '
				+ '		searchTags '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCourse, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.title, '
				+ '		C.shortDescription, '
				+ '		C.longDescription, '
				+ '		C.objectives, '
				+ '		C.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCourse] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)

		EXEC(@sql)
	   
		-- tblCourseRating
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseRating]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		rating, '
				+ '		timestamp '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		C.destinationID,'								-- course id
				+ '		U.destinationID,'								-- user id
				+ '		CR.rating,'										-- rating
				+ '		CR.timestamp'									-- timestamp
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseRating] CR'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CR.idCourse AND C.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] U ON U.sourceID = CR.idUser AND U.object = ''user'''
				+ ' WHERE C.destinationID IS NOT NULL AND U.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblCourseExpert	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCourseExpert]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- course id
				+ '		UM.destinationID'								-- user id
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = C.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = C.idExpert AND UM.object = ''user'''
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblDocumentRespositoryItem (tblCourseMaterial in Inquisiq) - file copies are done inside of procedural code
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblDocumentRepositoryItem]'
				+ ' ( '
				+ '		idSite, '
				+ '		idDocumentRepositoryObjectType, '
				+ '		idObject, '
				+ '		idDocumentRepositoryFolder, '
				+ '		idOwner, '
				+ '		filename, '
				+ '		kb, '
				+ '		searchTags, '
				+ '		isPrivate, '
				+ '		idLanguage, '
				+ '		isAllLanguages, '
				+ '		dtCreated, '
				+ '		isDeleted, '
				+ '		dtDeleted, '
				+ '		label, '
				+ '		isVisibleToUser, '
				+ '		isUploadedByUser '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		2,'												-- document repository object type (course = 2)
				+ '		C.destinationID,'								-- object id (course id)
				+ '		NULL,'											-- document repository folder id
				+ '		1,'												-- owner id
				+ '		CM.filename,'									-- filename
				+ '		CM.kb,'											-- kb
				+ '		NULL,'											-- search tags
				+ '		CM.isPrivate,'									-- is private?
				+ '		NULL,'											-- language id
				+ '		1,'												-- is all languages?
				+ '		GETUTCDATE(),'									-- date created
				+ '		0,'												-- is deleted?
				+ '		NULL,'											-- date deleted
				+ '		CM.name,'										-- label
				+ '		NULL,'											-- is visible to user?
				+ '		NULL'											-- is uploaded by user?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourseMaterial] CM'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] C ON C.sourceID = CM.idCourse AND C.object = ''course'''
				+ ' WHERE C.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- tblDocumentRepositoryItemLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblDocumentRepositoryItemLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idDocumentRepositoryItem,'
				+ '		idLanguage,'
				+ '		label, '				
				+ '		searchTags '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		DRI.idDocumentRepositoryItem, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		DRI.label, '				
				+ '		DRI.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblDocumentRepositoryItem] DRI'
				+ ' WHERE DRI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND DRI.idDocumentRepositoryObjectType = 2'

		EXEC(@sql)

		/*****************************************************LESSON**********************************************************************/
		-- tblLesson
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		dtDeleted, '
				+ '		isDeleted, '
				+ '		title, '
				+ '		revcode, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		[order], '
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		CM.destinationID,'								-- course id
				+ '		NULL,'											-- deleted date
				+ '		0,'												-- is deleted?
				+ '		L.name,'										-- title
				+ '		NULL,'											-- revcode
				+ '		L.shortDescription,'							-- short description
				+ '		L.description,'									-- long description
				+ '		L.[order],'										-- lesson order
				+ '		GETUTCDATE(),'									-- date created
				+ '		GETUTCDATE(),'									-- date modified
				+ '		NULL'											-- search tags
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = L.idCourse AND CM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL'
		
		EXEC (@sql)	  

		-- insert idLesson mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SL.idLesson, '
				+ '		DL.idLesson,'
				+ '		''lesson''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblLesson] DL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.destinationID = DL.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.idCourse = CM.sourceID AND SL.name = DL.title collate database_default'
				+ ' WHERE DL.idSite IS NOT NULL AND DL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SL.idLesson IS NOT NULL AND DL.idLesson IS NOT NULL'
		
		EXEC(@sql)

		-- tblLessonLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLessonLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idLesson,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		L.idLesson, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		L.title, '
				+ '		L.shortDescription, '
				+ '		L.longDescription, '
				+ '		L.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblLesson] L'
				+ ' WHERE L.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

	/*****************************************************INSTRUCTOR LED TRAINING**********************************************************************/
		-- tblStandUpTraining (this needs to be sourced from tblLesson in Inquisiq)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandUpTraining]'
				+ ' ( '
				+ '		idSite, '
				+ '		title, '
				+ '		description, '
				+ '		isStandaloneEnroll, '
				+ '		isRestrictedEnroll, '
				+ '		isRestrictedDrop, '
				+ '		searchTags, '
				+ '		avatar, '
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		cost'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','					-- idSite
				+ '		L.name + '' ##'' + CONVERT(NVARCHAR, L.idLesson) + ''##'','		-- title
				+ '		NULL,'															-- description
				+ '		0,'																-- isStandaloneEnroll
				+ '		0,'																-- isRestrictedEnroll
				+ '		0,'																-- isRestrictedDrop
				+ '		NULL,'															-- searchTags
				+ '		NULL,'															-- avatar
				+ '		0,'																-- isDeleted
				+ '		NULL,'															-- dtDeleted
				+ '		NULL'															-- cost
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = L.idCourse AND CM.object = ''course'''
				+ ' WHERE CM.destinationID IS NOT NULL'
				+ ' AND L.idLessonType IN (1, 2)' -- 1 = classroom, 2 = web meeting
						
		EXEC (@sql)

		-- insert idStandupTraining mappings (idStandupTraining maps to Inquisiq idLesson as they are equivalents)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SL.idLesson, '
				+ '		DST.idStandupTraining,'
				+ '		''ilt''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTraining] DST '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.name + '' ##'' + CONVERT(NVARCHAR, SL.idLesson) + ''##'' = DST.title collate database_default'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = SL.idCourse AND C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)				
				+ ' WHERE DST.idSite IS NOT NULL AND DST.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SL.idLesson IS NOT NULL AND DST.idStandupTraining IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLesson## additions we made to standup training titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTraining] SET '
				+ '		title = REPLACE(DST.title, '' ##'' + CONVERT(NVARCHAR, STM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTraining] DST '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STM ON STM.destinationID = DST.idStandupTraining AND STM.object = ''ilt'''
				+ ' WHERE STM.sourceID IS NOT NULL'

		EXEC (@sql)

		--tblStandUpTrainingLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idStandUpTraining,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		description,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		ST.idStandUpTraining, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		ST.title, '
				+ '		ST.description, '
				+ '		ST.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTraining] ST'
				+ ' WHERE ST.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- tblStandUpTrainingInstance
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandUpTrainingInstance]'
				+ ' ( '
				+ '		idStandUpTraining, '
				+ '		idSite, '
				+ '		title, '
				+ '		description, '
				+ '		seats, '
				+ '		waitingSeats, '
				+ '		urlRegistration, '
				+ '		urlAttend, '
				+ '		city, '
				+ '		province,'
				+ '		locationDescription,'
				+ '		type,'
				+ '		isDeleted,'
				+ '		dtDeleted,'
				+ '		integratedObjectKey,'
				+ '		hostUrl,'
				+ '		genericJoinUrl,'
				+ '		meetingPassword,'
				+ '		idWebMeetingOrganizer'
				+ ' ) '
				+ ' SELECT ' 
				+ '		STM.destinationID, '																				-- idStandupTraining
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','														-- idSite
				+ '		CONVERT(NVARCHAR, S.[datetime], 101) + '' ('' + CONVERT(NVARCHAR, S.[datetime], 108) + '' UTC)'' + '' ##'' + CONVERT(NVARCHAR, S.idSession) + ''##'','	-- title
				+ '		NULL,'																								-- description
				+ '		S.seats,'																							-- seats
				+ '		S.waitingSeats,'																					-- waitingSeats
				+ '		NULL,'																								-- urlRegistration
				+ '		S.url,'																								-- urlAttend
				+ '		S.city,'																							-- city
				+ '		S.province,'																						-- province
				+ '		S.locationDescription,'																				-- locationDescription
				+ '		CASE WHEN L.idLessonType = 1 THEN 1 ELSE 0 END,'													-- type
				+ '		0,'																									-- is deleted?
				+ '		NULL,'																								-- date deleted
				+ '		NULL,'																								-- integrated object key
				+ '		NULL,'																								-- host url
				+ '		NULL,'																								-- generic join url
				+ '		NULL,'																								-- meeting password
				+ '		NULL'																								-- web meeting organizer id
				+ '	FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSession] S'
				+ '	LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] L ON L.idLesson = S.idLesson'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = L.idCourse'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STM ON STM.sourceID = L.idLesson AND STM.object = ''ilt'''			
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND STM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idStandupTrainingInstance mappings (idStandupTrainingInstance maps to Inquisiq idSession)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SS.idSession, '
				+ '		DSTI.idStandupTrainingInstance,'
				+ '		''iltsession''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTrainingInstance] DSTI '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSession] SS ON CONVERT(NVARCHAR, SS.[datetime], 101) + '' ('' + CONVERT(NVARCHAR, SS.[datetime], 108) + '' UTC)'' + '' ##'' + CONVERT(NVARCHAR, SS.idSession) + ''##'' = DSTI.title collate database_default'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLesson] SL ON SL.idLesson = SS.idLesson'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] SC ON SC.idCourse = SL.idCourse AND SC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
				+ ' WHERE DSTI.idSite IS NOT NULL AND DSTI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SS.idSession IS NOT NULL AND DSTI.idStandupTrainingInstance IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idSession## additions we made to standup training instance titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTrainingInstance] SET '
				+ '		title = REPLACE(DSTI.title, '' ##'' + CONVERT(NVARCHAR, STIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblStandupTrainingInstance] DSTI '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STIM ON STIM.destinationID = DSTI.idStandupTrainingInstance AND STIM.object = ''iltsession'''
				+ ' WHERE STIM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblStandupTrainingInstanceToInstructorLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandupTrainingInstanceToInstructorLink] '
				+ ' ( '
				+ '		idSite,'
				+ '		idStandupTrainingInstance,'
				+ '		idInstructor'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		STI.destinationID, '
				+ '		U.destinationID '
				+ ' FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblSession] S '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STI ON STI.sourceID = S.idSession AND STI.object = ''iltsession'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] U ON U.sourceID = S.idInstructor AND U.object = ''user'''
				+ ' WHERE S.idInstructor IS NOT NULL AND STI.destinationID IS NOT NULL AND U.destinationID IS NOT NULL'

		EXEC (@sql)

		-- tblStandUpTrainingInstanceMeetingTime
		SET @sql = 'INSERT INTO[' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstanceMeetingTime] '
			+ ' ( '
			+ '		idStandUpTrainingInstance,'
			+ '		idSite,'
			+ '		dtStart,'
			+ '		dtEnd,'
			+ '		minutes,'
			+ '		idTimezone'
			+ ' ) '
			+ ' SELECT '
			+ '		STI.destinationID,'
			+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
			+ '		S.datetime,'
			+ '		DATEADD(minute, S.minutes, S.datetime),'
			+ '		S.minutes,'
			+ '		TZ.destinationID'
			+ '	FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblSession] S '
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] STI ON STI.sourceID = S.idSession AND STI.object = ''iltsession'''
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = S.idTimezone '
			+ ' WHERE STI.destinationID IS NOT NULL'

			EXEC (@sql)
	   
		-- tblStandUpTrainingInstanceLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstanceLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idStandUpTrainingInstance,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		description,'
				+ '		locationDescription'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		STI.idStandUpTrainingInstance, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		STI.title, '
				+ '		STI.description, '
				+ '		STI.locationDescription '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstance] STI'
				+ ' WHERE STI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

	/******************************************************CATALOG********************************************************************/	   
		-- tblCatalog
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog]'
				+ ' ( '
				+ '		idSite, '
				+ '		idParent, '
				+ '		title, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isPrivate, '
				+ '		cost, '
				+ '		isClosed, '
				+ '		dtCreated,'
				+ '		dtModified,'
				+ '		costType,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		NULL,'											-- parent id (NULL for now)				
				+ '		C.name + '' ##'' + CONVERT(NVARCHAR, C.idCatalog) + ''##'','	-- title
				+ '		C.shortDescription,'							-- short description
				+ '		C.description,'									-- description
				+ '		C.isPrivate,'									-- is the catalog private
				+ '		CASE WHEN C.cost >= 1 THEN C.cost '				-- cost
				+ '			 WHEN C.cost >= 0.1 AND C.cost <= 0.9 THEN C.cost * 100 '
				+ '		ELSE 0 END, '									
				+ '		C.isClosed,'									-- is catalog closed?
				+ '		GETUTCDATE(),'									-- date created
				+ '		GETUTCDATE(),'									-- date modified
				+ '		CASE WHEN C.cost = 0 THEN 3'					-- cost type
				+ '			 WHEN C.cost >= 1 THEN 2'
				+ '			 WHEN C.cost >= 0.1 AND C.cost <= 0.9 THEN 4'
				+ '		ELSE 1 END,'
				+ '		NULL'											-- search tags
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' '
		
		EXEC (@sql)

		-- insert idCatalog mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SC.idCatalog, '
				+ '		DC.idCatalog,'
				+ '		''catalog''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] SC ON SC.idSite = '+ CONVERT(NVARCHAR, @idSiteSource) + ' AND SC.name + '' ##'' + CONVERT(NVARCHAR, SC.idCatalog) + ''##'' = DC.title collate database_default'
				+ ' WHERE DC.idSite IS NOT NULL AND DC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SC.idCatalog IS NOT NULL AND DC.idCatalog IS NOT NULL'
		
		EXEC(@sql)	

		-- update idParent for catalogs
		SET @sql = 'UPDATE DESTINATIONCATALOG SET'
				+ '		idParent = CM.destinationID'
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCatalog] SOURCECATALOG'
				+ ' LEFT JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DESTINATIONCATALOG ON SOURCECATALOG.name + '' ##'' + CONVERT(NVARCHAR, SOURCECATALOG.idCatalog) + ''##'' = DESTINATIONCATALOG.title collate database_default AND SOURCECATALOG.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = SOURCECATALOG.idParent AND CM.object = ''catalog'''
				+ ' WHERE DESTINATIONCATALOG.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SOURCECATALOG.idParent IS NOT NULL'
	   
		EXEC(@sql)

		-- clean up the ##idCatalog## additions we made to catalog titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalog] SET '
				+ '		title = REPLACE(DST.title, '' ##'' + CONVERT(NVARCHAR, SRC.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCatalog] DST '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SRC ON SRC.destinationID = DST.idCatalog AND SRC.object = ''catalog'''
				+ ' WHERE SRC.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblCatalogLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalogLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idCatalog,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		C.idCatalog, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		C.title, '
				+ '		C.shortDescription, '
				+ '		C.longDescription, '
				+ '		C.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCatalog] C'
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)
		
		
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaCourse_Success'

	SET XACT_ABORT OFF

END
GO