SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Site]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Site]
GO


/*

CLONE PORTAL - SITE DATA

*/
CREATE PROCEDURE [System.ClonePortal.Site]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,
	@idSiteSource			INT,
	@idSiteDestination		INT				OUTPUT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@destinationHostName    NVARCHAR(50),
	@idAccount				INT				= NULL
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.0: Copy Site - Initialized', 0)
    
	BEGIN TRY		
    
		DECLARE @sql				NVARCHAR(MAX)				
		DECLARE @doesHostnameExist	BIT

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		first, disable the SQL Agent Job for the destination database

		*/

		SET @sql = 'IF EXISTS ('
				 + 'SELECT 1 FROM [' + @destinationDBServer + '].[msdb].[dbo].[sysjobs] '
				 + 'WHERE name = ''Asentia Jobs - ' + @destinationDBName + ''') '
				 + 'BEGIN EXEC [msdb].[dbo].sp_update_job @job_name = ''Asentia Jobs - ' + @destinationDBName + ''', @enabled = 0 END'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.1: Copy Site - SQL AGENT JOB DISABLED', 0)

		/*
	  
		create id mappings table or clear out existing id mappings table

		*/
	
		SET @sql = 'IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE name = N''ClonePortal_IDMappings'' AND xtype = ''U'') '
			+ '	BEGIN '
			+ '	CREATE TABLE [dbo].[ClonePortal_IDMappings] '
			+ '	('
			+ '		[sourceID]		INT				NOT NULL, '
			+ '		[destinationID] INT				NOT NULL, '
			+ '		[object]		NVARCHAR(255) '
			+ ')'
			+ '	END '
			+ '	ELSE '
			+ '	BEGIN '
		SET @sql = @sql + '	DELETE FROM [dbo].[ClonePortal_IDMappings] ' 
			+ '	END'
				
		EXEC(@sql)
	
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.2: Copy Site - Mapping Table Created', 0)
	
		/*
	
		validate that the hostname is unique within the destination
	
		*/
		
		SET @sql = 'SET @doesHostnameExist = (SELECT CASE WHEN COUNT(1) > 0 THEN 1 ELSE 0 END FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSite] '
				 + 'WHERE hostname = ' + '''' + @destinationHostName + '''' + ')'
	
		EXECUTE SP_EXECUTESQL @sql, N'@doesHostnameExist INT OUTPUT', @doesHostnameExist OUTPUT
				
		IF (@doesHostnameExist = 1)
		BEGIN
			SET @Return_Code = 2
			SET @Error_Description_Code = 'SystemClonePortal_HostNameNotUnique'			
			RETURN 1
		END
	   
		/*

		FULL COPY

		*/

		IF @fullCopyFlag = 1
			BEGIN

			-- copy the site
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSite] '
					 + '( '
					 + '	hostname, '
					 + '	password, '
					 + '	isActive, '
					 + '	dtExpires, '
					 + '	title, '
					 + '	company, '
					 + '	contactName, '
					 + '	contactEmail, '
					 + '	userLimit, '
					 + '	kbLimit, '
					 + '	idLanguage, '
					 + '	idTimezone, '
					 + '	dtLicenseAgreementExecuted, '
					 + '	dtUserAgreementModified '
					 + ') '
					 + 'SELECT '
					 + '	''' + @destinationHostName + ''', '
					 + '	CP.password, '
					 + '	CP.isActive, '
					 + '	CP.dtExpires, '
					 + '	CP.title, '
					 + '	CP.company, '
					 + '	CP.contactName, '
					 + '	CP.contactEmail, '
					 + '	CP.userLimit, '
					 + '	CP.kbLimit, '
					 + '	CP.idLanguage, '
					 + '	CP.idTimezone, '
					 + '	CP.dtLicenseAgreementExecuted, '
					 + '	CP.dtUserAgreementModified '
					 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSite] CP '
					 + 'WHERE CP.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		
			EXEC (@sql)

			-- get the new site id
			DECLARE @newIdSite INT
			SET @sql = 'SET @newIdSite = (SELECT idSite FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSite] WHERE hostname = ''' + @destinationHostName + ''')'
			EXECUTE SP_EXECUTESQL @sql, N'@newIdSite INT OUTPUT', @newIdSite OUTPUT

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.3: Copy Site - Site Record Inserted', 0)
	   
			-- set new created destination site id 
			SET @idSiteDestination = @newIdSite
	  
			-- insert the mapping for source to destination site id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
					 + '( '
					 + '	sourceId, '
					 + '	destinationId, '
					 + '	[object] '
					 + ') '
					 + 'VALUES '
					 + '( '
					 +		CAST(@idSiteSource AS NVARCHAR) + ', '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
					 + '	''site'' ' 
					 + ') '

			EXEC(@sql)	

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.4: Copy Site - Site Mapping Created', 0)
	   
			-- copy the site language table
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSiteLanguage] '
					 + '( '
					 + '	idSite, '
					 + '	idLanguage, '
					 + '	title, '
					 + '	company '
					 + ') '
					 + 'SELECT '
					 + '	TTS.destinationId, '
					 + '	SL.idLanguage, '
					 + '	SL.title, '
					 + '	SL.company '
					 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteLanguage] SL '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTS ON TTS.sourceId = SL.idSite AND TTS.object = ''site'' '
					 + 'WHERE SL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		
			EXEC(@sql)
	   
			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.5: Copy Site - Site Language Record Inserted', 0)
	   
			-- insert the account to domain alias link for the hostname
			SET @sql = 'INSERT INTO [tblAccountToDomainAliasLink] '
					 + '( '
					 + '	idAccount, '
					 + '	hostname, '
					 + '	domain '
					 + ') '
					 + 'SELECT '
					 +		CAST(@idAccount AS NVARCHAR) + ', '
					 + '	''' + @destinationHostName + ''', '
					 + '	NULL ' 
								
			EXEC(@sql)		   
	   
			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.6: Copy Site - Account to Domain Alias Record Inserted', 0)

			-- insert the site to domain alias link for the hostname
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSiteToDomainAliasLink] '
	   				 + '( '
	   				 + '	idSite, '
	   				 + '	domain '
	   				 + ') '
	   				 + 'SELECT '
	   				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				 + '	''' + @destinationHostName + ''' '
						
			EXEC(@sql)	

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.7: Copy Site - Site To Domain Alias Record Inserted', 0)

			END
		/*

		PARTIAL COPY - THIS IS FOR CREATING A NEW SITE FROM TEMPLATE
		THE SITE IS ALREADY CREATED FROM THAT PAGE, WE ONLY NEED TO MAP IT

		*/

		ELSE
			BEGIN

			-- insert the mapping for source to destination site id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
					 + '( '
					 + '	sourceId, '
					 + '	destinationId, '
					 + '	[object] '
					 + ') '
					 + 'VALUES '
					 + '( '
					 +		CAST(@idSiteSource AS NVARCHAR) + ', '
					 +		CAST(@idSiteDestination AS NVARCHAR) +', '
					 + '	''site'' '
					 + ') '

			EXEC(@sql)	

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.4: Copy Site - Site Mapping Created', 0)

			END

		/*

		ITEMS THAT ARE COPIED FOR BOTH FULL AND PARTIAL COPY

		*/
      
		-- copy site params
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSiteParam] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	[param], '
	   			 + '	value '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	SP.[param], '
	   			 + '	SP.value '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteParam] SP '
	   			 + 'WHERE SP.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
				 + 'AND NOT EXISTS (SELECT 1 FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteParam] SP1 '
				 + '				WHERE SP1.[param] = SP.[param] AND SP1.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ')'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.8: Copy Site - Site Params Inserted', 0)
		
		-- copy site available languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblSiteAvailableLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idLanguage '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	SAL.idLanguage '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteAvailableLanguage] SAL '
				 + 'WHERE SAL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
				 + 'AND NOT EXISTS (SELECT 1 FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblSiteAvailableLanguage] SAL1 '
				 + '				WHERE SAL1.[idLanguage] = SAL.[idLanguage] AND SAL1.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ')'
			
		EXEC(@sql)		

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1.9: Copy Site - Site Available Languages Inserted', 0)

		/*

		RETURN

		*/
		
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_Site_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 1: Copy Site - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_Site_PortalMigrationFailed'

	END CATCH
		
	SET XACT_ABORT OFF

END