SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.Report]
GO


/*

CLONE PORTAL - REPORT DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.Report]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.0: Copy Report - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	  
		-- copy reports
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReport] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idUser, '
	   			 + '	idDataset, '
	   			 + '	title, '
	   			 + '	fields, '
	   			 + '	filter, '
	   			 + '	[order], '
	   			 + '	isPublic, '
	   			 + '	dtCreated, '
	   			 + '	dtModified, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTU.destinationId, '
	   			 + '	R.idDataset, '
	   			 + '	R.title, '
	   			 + '	R.fields, '
	   			 + '	R.filter, '
	   			 + '	R.[order], '
	   			 + '	R.isPublic, '
	   			 + '	R.dtCreated, '
	   			 + '	R.dtModified, '
	   			 + '	R.isDeleted, '
	   			 + '	R.dtDeleted '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReport] R '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = R.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND R.idDataset < 1000'
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.1: Copy Report - Reports Inserted', 0)
	   
		-- insert the mapping for source to destination report ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SR.idReport, '
	   			 + '	DR.idReport, '
	   			 + '	''report'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReport] DR '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReport] SR '
	   			 + 'ON (SR.idSite =' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SR.dtCreated = DR.dtCreated OR (SR.dtCreated IS NULL AND DR.dtCreated IS NULL)) '
	   			 + '	AND (SR.dtModified = DR.dtModified OR (SR.dtModified IS NULL AND DR.dtModified IS NULL)) '
	   			 + '	AND (SR.dtDeleted = DR.dtDeleted OR (SR.dtDeleted IS NULL AND DR.dtDeleted IS NULL))) '
	   			 + 'WHERE DR.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.2: Copy Report - Report Mappings Created', 0)

		-- copy report languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReportLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idReport, '
	   			 + '	idLanguage, '
	   			 + '	title '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	RL.idLanguage, '
	   			 + '	RL.title '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReportLanguage] RL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RL.idReport AND TTR.object = ''report'') '
	   			 + 'WHERE RL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTR.destinationId IS NOT NULL'
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.3: Copy Report - Report Languages Inserted', 0)
		
		-- copy report files
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReportFile] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idReport, '
	   			 + '	idUser, '
	   			 + '	[filename], '
	   			 + '	dtCreated, '
	   			 + '	htmlKb, '
	   			 + '	csvKb, '
	   			 + '	pdfKb '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	RF.[filename], '
	   			 + '	RF.dtCreated, '
	   			 + '	RF.htmlKb, '
	   			 + '	RF.csvKb, '
	   			 + '	RF.pdfKb '	
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReportFile] RF '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RF.idReport AND TTR.object = ''report'') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = RF.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE RF.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTR.destinationId IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.4: Copy Report - Report Files Inserted', 0)
	   
		-- copy report subscriptions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReportSubscription] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idUser, '
	   			 + '	idReport, '
	   			 + '	dtStart, '
	   			 + '	dtNextAction, '
	   			 + '	recurInterval, '
	   			 + '	recurTimeframe, '
	   			 + '	token '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTU.destinationId, '
	   			 + '	TTR.destinationId, '
	   			 + '	RS.dtStart, '
	   			 + '	RS.dtNextAction, '
	   			 + '	RS.recurInterval, '
	   			 + '	RS.recurTimeframe, '
	   			 + '	RS.token '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReportSubscription] RS '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RS.idReport AND TTR.object = ''report'') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = RS.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE RS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTR.destinationId IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.5: Copy Report - Report Subscriptions Inserted', 0)
		
		-- copy report shortcut widget links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblReportToReportShortcutsWidgetLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idReport, '
	   			 + '	idUser '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblReportToReportShortcutsWidgetLink] RRSWL '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RRSWL.idReport AND TTR.object = ''report'') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = RRSWL.idUser AND TTU.object = ''users'') '
	   			 + 'WHERE RRSWL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTR.destinationId IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.6: Copy Report - Report Shortcut Widget Links Inserted', 0)

		/*

		FINAL STEP CLEAR OUT RULESET ENGINE QUEUE, AND ENABLE THE DESTINATION DB SQL JOB - THIS MUST ALWAYS BE THE FINAL STEP

		*/

		-- clear out the ruleset engine queue
		SET @sql = 'DELETE FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetEngineQueue] '
				 + 'WHERE idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.7: FINAL STEP - RULESET ENGINE QUEUE CLEARED', 0)

		-- enable the SQL Agent Job for the destination database
		SET @sql = 'IF EXISTS ('
				 + 'SELECT 1 FROM [' + @destinationDBServer + '].[msdb].[dbo].[sysjobs] '
				 + 'WHERE name = ''Asentia Jobs - ' + @destinationDBName + ''') '
				 + 'BEGIN EXEC [msdb].[dbo].sp_update_job @job_name = ''Asentia Jobs - ' + @destinationDBName + ''', @enabled = 1 END'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15.8: FINAL STEP - SQL AGENT JOB ENABLED', 0)		
				
		EXEC(@sql)

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_PurchaseTransaction_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 15: Copy Report - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_PurchaseTransaction_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END