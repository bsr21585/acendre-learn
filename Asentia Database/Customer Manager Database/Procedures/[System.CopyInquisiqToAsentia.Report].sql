
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Report]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Report]
GO


/*

Copy report data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Report]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

SET NOCOUNT ON
SET XACT_ABORT ON    
    	
/*

Filter Data Structure Transform Mapping

*/
DECLARE @sql NVARCHAR(MAX), @filterTransform xml, @intFilterCondition int
SET @filterTransform = 
'<conditions>
  <condition identifier="is"        operator="=&apos;data&apos;"                                   dataType="String"          input="true"  />
  <condition identifier="nis"       operator="&lt;&gt;&apos;data&apos;"                            dataType="String"          input="true"  />
  <condition identifier="is_date"   operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, &apos;bDate&apos;))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()) - 1, DATEADD(d, 1, DATEDIFF(d, 0, &apos;bDate&apos;)))" dataType="DateTime"        input="true"  />
  <condition identifier="nis_date"  operator="NOT BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, &apos;bDate&apos;))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()) - 1, DATEADD(d, 1, DATEDIFF(d, 0, &apos;bDate&apos;)))" dataType="DateTime"    input="true"  />
  <condition identifier="ct"        operator="LIKE &apos;%data%&apos;"                             dataType="StringContain"   input="true"  />
  <condition identifier="nct"       operator="NOT LIKE &apos;%data%&apos;"                         dataType="StringContain"   input="true"  />
  <condition identifier="sw"        operator="LIKE &apos;data%&apos;"                              dataType="String"          input="true"  />
  <condition identifier="ew"        operator="LIKE &apos;%data&apos;"                              dataType="String"          input="true"  />
  <condition identifier="bf"        operator="&lt;&apos;bDate&apos;"                               dataType="DateTime"        input="true"  />
  <condition identifier="af"        operator="&gt;&apos;bDate&apos;"                               dataType="DateTime"        input="true"  />
  <condition identifier="bw"        operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, &apos;bDate&apos;))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 1, DATEDIFF(d, 0, &apos;eDate&apos;)))"  dataType="DateTime"        input="true"  />
  <condition identifier="nbw"       operator="NOT BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, &apos;bDate&apos;))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 1, DATEDIFF(d, 0, &apos;eDate&apos;)))" dataType="DateTime"        input="true"  />
  <condition identifier="r_d0"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 0, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 1, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime"     input="false" />
  <condition identifier="r_d1"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d,-1, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 0, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime"    input="false" />
  <condition identifier="r_d7"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d,-7, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 0, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime"    input="false" />
  <condition identifier="r_w0"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(wk,DATEDIFF(wk,0,GETDATE()),-1)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(d, 1, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime" input="false" />
  <condition identifier="r_w1"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(wk,DATEDIFF(wk,0,GETDATE())-1,-1)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(wk,DATEDIFF(wk,0,GETDATE()), -1))"   dataType="DateTime" input="false" />
  <condition identifier="r_w2"      operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(wk,DATEDIFF(wk,0,GETDATE())-2,-1)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(wk,DATEDIFF(wk,0,GETDATE()), -1))"   dataType="DateTime" input="false" />
  <condition identifier="r_m0"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,0,GETDATE()), 0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(d, 1, DATEDIFF(d,0,GETDATE())))"     dataType="DateTime" input="false" />
  <condition identifier="r_m1"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,0,GETDATE())-1,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_m3"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,0,GETDATE())-3,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_m6"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,0,GETDATE())-6,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_q0"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))  AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(d, 1, DATEDIFF(d, 0, GETDATE())))"   dataType="DateTime" input="false" />
  <condition identifier="r_q1"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE())-1,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_q2"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE())-2,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_q3"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE())-3,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_q4"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,0,GETDATE())-4,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_y1"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(yy,DATEDIFF(yy,0,GETDATE())-1,0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0))"    dataType="DateTime" input="false" />
  <condition identifier="r_y0"      operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 1, DATEDIFF(d, 0, GETDATE())))"  dataType="DateTime" input="false" />
  <condition identifier="rf_d1"     operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 1, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 2, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime" input="false" />
  <condition identifier="rf_d7"     operator="BETWEEN DATEADD(MINUTE, DATEDIFF(MINUTE, GETDATE(), GETUTCDATE()), DATEADD(d, 1, DATEDIFF(d, 0, GETDATE()))) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())- 1, DATEADD(d, 8, DATEDIFF(d, 0, GETDATE())))" dataType="DateTime" input="false" />
  <condition identifier="rf_w1"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(wk, DATEDIFF(wk,0,GETDATE()),6)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 13))"  dataType="DateTime" input="false" />
  <condition identifier="rf_w2"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(wk, DATEDIFF(wk,0,GETDATE()),6)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 20))"  dataType="DateTime" input="false" />
  <condition identifier="rf_m1"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,DateAdd(mm,2,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_m3"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,DateAdd(mm,4,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_m6"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(mm,DATEDIFF(mm,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(mm,DATEDIFF(mm,0,DateAdd(mm,7,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_q1"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,DateAdd(qq,2,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_q2"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,DateAdd(qq,3,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_q3"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,DateAdd(qq,4,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_q4"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(qq,DATEDIFF(qq,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(qq,DATEDIFF(qq,0,DateAdd(qq,5,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="rf_y1"     operator="BETWEEN DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), DATEADD(yy,DATEDIFF(yy,-1,GETDATE()),0)) AND DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE())-1, DATEADD(yy,DATEDIFF(yy,0,DateAdd(yy,2,GETDATE())),0))" dataType="DateTime" input="false" />
  <condition identifier="eq"        operator="=data"                                               dataType="IntegerEnum"         input="true"  />
  <condition identifier="neq"       operator="&lt;&gt;data"                                        dataType="IntegerEnum"         input="true"  />
  <condition identifier="lt"        operator="&lt;data"                                            dataType="Integer"         input="true"  />
  <condition identifier="leq"       operator="&lt;=data"                                           dataType="Integer"         input="true"  />
  <condition identifier="gt"        operator="&gt;data"                                            dataType="Integer"         input="true"  />
  <condition identifier="geq"       operator="&gt;=data"                                           dataType="Integer"         input="true"  />
  <condition identifier="yes"       operator="=1"                                                  dataType="Bit"             input="false" />
  <condition identifier="no"        operator="=0"                                                  dataType="Bit"             input="false" />
  <condition identifier="Active"    operator="=1"                                                  dataType="Status"          input="false" />
  <condition identifier="Disabled"  operator="=0"                                                  dataType="Status"          input="false" />
  <condition identifier="null"      operator="IS NULL"                                             dataType="StringIntegerDateTimeEnum"   input="false"  />
  <condition identifier="nnull"     operator="IS NOT NULL"                                         dataType="StringIntegerDateTimeEnum"   input="false"  />
</conditions>'

EXEC sp_xml_preparedocument @intFilterCondition OUTPUT, @filterTransform

CREATE TABLE #tblFilterTransform (
	identifier				VARCHAR(50),
	operator				VARCHAR(MAX),
	input					BIT
)

INSERT INTO #tblFilterTransform
SELECT identifier, operator, input
	FROM OPENXML(@intFilterCondition, 'conditions/condition', 1) 
	WITH 
	(	
		identifier [varchar](50) '@identifier',
		operator [varchar](max) '@operator',
		input [varchar](50) '@input' 
	)
EXEC sp_xml_removedocument @intFilterCondition

/*

Column Name Transform Mapping

*/
DECLARE @columnNameTransform xml, @intColumnName int
SET @columnNameTransform = 
'<columns>
  <column inquisiq="_idSite" asentia="_idSite" />
  <column inquisiq="Full Name" asentia="Full Name" />
  <column inquisiq="##name.firstname##" asentia="##firstName##" />
  <column inquisiq="##name.lastname##" asentia="##lastName##" />
  <column inquisiq="##email##" asentia="##email##" />
  <column inquisiq="##username##" asentia="##username##" />
  <column inquisiq="##jobtitle##" asentia="##jobTitle##" />
  <column inquisiq="##jobclass##" asentia="##jobClass##" />
  <column inquisiq="##company##" asentia="##company##" />
  <column inquisiq="##address.street##" asentia="##address##" />
  <column inquisiq="##address.city##" asentia="##city##" />
  <column inquisiq="##address.province##" asentia="##province##" />
  <column inquisiq="##address.postalcode##" asentia="##postalcode##" />
  <column inquisiq="##address.country##" asentia="##country##" />
  <column inquisiq="##phone.primary##" asentia="##phonePrimary##" />
  <column inquisiq="##phone.work##" asentia="##phoneWork##" />
  <column inquisiq="##phone.home##" asentia="##phoneHome##" />
  <column inquisiq="##phone.fax##" asentia="##phoneFax##" />
  <column inquisiq="##phone.mobile##" asentia="##phoneMobile##" />
  <column inquisiq="##phone.pager##" asentia="##phonePager##" />
  <column inquisiq="##phone.other##" asentia="##phoneOther##" />
  <column inquisiq="##division##" asentia="##division##" />
  <column inquisiq="##department##" asentia="##department##" />
  <column inquisiq="##region##" asentia="##region##" />
  <column inquisiq="##employeeid##" asentia="##employeeID##" />
  <column inquisiq="##hiredate##" asentia="##dtHire##" />
  <column inquisiq="##termdate##" asentia="##dtTerm##" />
  <column inquisiq="##gender##" asentia="##gender##" />
  <column inquisiq="##race##" asentia="##race##" />
  <column inquisiq="##dob##" asentia="##dtDOB##" />
  <column inquisiq="##isactive##" asentia="##isActive##" />
  <column inquisiq="##created##" asentia="##dtCreated##" />
  <column inquisiq="##expires##" asentia="##dtExpires##" />
  <column inquisiq="##dtLastLogin##" asentia="##dtLastLogin##" />
  <column inquisiq="##language##" asentia="##idLanguage##" />
  <column inquisiq="##idtimezone##" asentia="##idTimezone##" />
  <column inquisiq="Group" asentia="Group" />
  <column inquisiq="_sbGroup" asentia="_sbGroup"  optionGroup="" />
  <column inquisiq="##supervisor##" asentia="Supervisor" />
  <column inquisiq="##field00##" asentia="##field00##" />
  <column inquisiq="##field01##" asentia="##field01##" />
  <column inquisiq="##field02##" asentia="##field02##" />
  <column inquisiq="##field03##" asentia="##field03##" />
  <column inquisiq="##field04##" asentia="##field04##" />
  <column inquisiq="##field05##" asentia="##field05##" />
  <column inquisiq="##field06##" asentia="##field06##" />
  <column inquisiq="##field07##" asentia="##field07##" />
  <column inquisiq="##field08##" asentia="##field08##" />
  <column inquisiq="##field09##" asentia="##field09##" />
  <column inquisiq="Code" asentia="Code" />
  <column inquisiq="Course" asentia="Course" />
  <column inquisiq="_order_Course" asentia="_order_Course" />
  <column inquisiq="Credits" asentia="Credits" />
  <column inquisiq="Enroll Date" asentia="Enroll Date" />
  <column inquisiq="Course Status" asentia="Course Status" />
  <column inquisiq="_sbCourse Status" asentia="_sbCourse Status" />
  <column inquisiq="Due Date" asentia="Date Due" />
  <column inquisiq="Date Completed" asentia="Date Completed" />
  <column inquisiq="Lesson" asentia="Lesson" />
  <column inquisiq="Session Date/Time" asentia="Session Date/Time" />
  <column inquisiq="Content Resource" asentia="Content Resource" />
  <column inquisiq="Lesson Completion" asentia="Lesson Completion" />
  <column inquisiq="Lesson Success" asentia="Lesson Success" />
  <column inquisiq="Score" asentia="Score" />
  <column inquisiq="Lesson Timestamp" asentia="Lesson Timestamp" />
  <column inquisiq="Lesson Time" asentia="Lesson Time" />
  <column inquisiq="Attempts" asentia="Attempts" />  
  <column inquisiq="Interaction" asentia="Interaction" />
  <column inquisiq="Interaction Description" asentia="Interaction Description" />
  <column inquisiq="Type" asentia="Interaction Type" />
  <column inquisiq="Interaction Timestamp" asentia="Interaction Timestamp" />
  <column inquisiq="Interaction Time" asentia="Interaction Time" />
  <column inquisiq="Learner Response" asentia="Learner Response" />
  <column inquisiq="Result" asentia="Result" />
  <column inquisiq="Objective" asentia="Objective" />
  <column inquisiq="Objective Score" asentia="Objective Score" />
  <column inquisiq="Objective Completion Status" asentia="Objective Completion Status" />
  <column inquisiq="Objective Success Status" asentia="Objective Success Status" />
  <column inquisiq="Catalog Name" asentia="Catalog Name" />
  <column inquisiq="Catalog Cost" asentia="Catalog Cost" />
  <column inquisiq="Course Name" asentia="Course Name" />
  <column inquisiq="_order_Course" asentia="_order_Course" />
  <column inquisiq="Course Code" asentia="Course Code" />
  <column inquisiq="Course Cost" asentia="Course Cost" />
  <column inquisiq="Course Rating" asentia="Course Rating" />
  <column inquisiq="Votes Cast" asentia="Votes Cast" />
  <column inquisiq="Course Credits" asentia="Course Credits" />
  <column inquisiq="Course Published" asentia="Course Published" />
  <column inquisiq="Course Closed" asentia="Course Closed" dataType="Bit" />
  <column inquisiq="Course Locked" asentia="Course Locked" dataType="Bit" />
  <column inquisiq="Lesson Name" asentia="Lesson Name" />
  <column inquisiq="Lesson Type" asentia="Content Type_s" dataType="Contain" />
  <column inquisiq="Content Resource" asentia="Resource Name" />
  <column inquisiq="Session Date/Time" asentia="Session Date/Time" />
  <column inquisiq="Session Seats" asentia="Session Seats" />
  <column inquisiq="Session Waiting Seats" asentia="Session Waiting Seats" />
  <column inquisiq="Session Seats Available" asentia="Session Seats Available" />
  <column inquisiq="Session Instructor" asentia="Session Instructor" />
  <column inquisiq="Session Instructor Email" asentia="Session Instructor Email" />
  <column inquisiq="Session City" asentia="Session City" />
  <column inquisiq="Session State/Province" asentia="Session Province" />
  <column inquisiq="Session Student Name" asentia="Session Student Name" />
  <column inquisiq="Session Student Email" asentia="Session Student Email" />
  <column inquisiq="Session Student Username" asentia="Session Student Username" />
  <column inquisiq="Certificate Name" asentia="Certificate Name" />
  <column inquisiq="Issuing Organization" asentia="Issuing Organization" />
  <column inquisiq="Awarded By" asentia="Awarded By" />
  <column inquisiq="Award Date" asentia="Award Date" />
  <column inquisiq="Expiration Date" asentia="Expiration Date" />
  <column inquisiq="Credits" asentia="Credits" />
  <column inquisiq="Certificate Code" asentia="Code" />
  <column inquisiq="Order Number" asentia="Order Number" />
  <column inquisiq="Order Date" asentia="Order Date" />
  <column inquisiq="Credit Card Digits" asentia="Credit Card Digits" />
  <column inquisiq="Item Type" asentia="Item Type" />
  <column inquisiq="Item Name" asentia="Item Name" />
  <column inquisiq="Item Description" asentia="Item Description" />
  <column inquisiq="Cost" asentia="Cost" />
  <column inquisiq="Paid" asentia="Paid" />
  <column inquisiq="Coupon Code" asentia="Coupon Code" />
</columns>'

EXEC sp_xml_preparedocument @intColumnName OUTPUT, @columnNameTransform

CREATE TABLE #tblColumnMapping (
	inquisiq			VARCHAR(50),
	asentia				VARCHAR(50)
)

INSERT INTO #tblColumnMapping
SELECT inquisiq, asentia
	FROM OPENXML(@intColumnName, 'columns/column', 1) 
	WITH 
	(	
		inquisiq [varchar](50) '@inquisiq',
		asentia [varchar](50) '@asentia'
	)
EXEC sp_xml_removedocument @intColumnName

/*

Start Transfer Process, loop through each report and transform the data structure

*/

DECLARE @idReport int, @instanceData xml, @hDoc int, @fields varchar(max), @filters varchar(max)

CREATE TABLE #TempTable(
	idReport				INT,
	instanceData			VARCHAR(MAX)
)

SET @sql = ' INSERT INTO #TempTable'
		 + ' SELECT idReport, instanceData'
		 + ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReport] R'
		 + ' WHERE R.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
EXEC (@sql)

CREATE TABLE #AsentiaFormatReportTable (
	idReport			INT,
	idSite				INT,
	idUser				INT,
	idDataset			INT,
	isPublic			BIT,
	fields				VARCHAR(MAX),
	filters				VARCHAR(MAX)
)

WHILE EXISTS(SELECT * FROM #TempTable)
BEGIN
	SELECT TOP 1 @idReport = idReport, @instanceData = instanceData From #TempTable

	EXEC sp_xml_preparedocument @hDoc OUTPUT, @instanceData

	CREATE TABLE #ColumnTable (
	columnName			VARCHAR(MAX)
	)

	INSERT INTO #ColumnTable 
	SELECT DISTINCT '[' + CM.asentia + '],'
	FROM OPENXML(@hDoc, 'instanceData/field') 
	WITH 
	(	ColumnName [varchar](50) '@columnName'
	) FN
	LEFT JOIN #tblColumnMapping CM ON CM.inquisiq = FN.ColumnName

	SELECT @fields = STUFF(
		(
			SELECT columnName FROM #ColumnTable FOR XML PATH('')
		)
	, 1, 0, '')

	DROP TABLE #ColumnTable

	CREATE TABLE #FilterTable (
	orAND				VARCHAR(10),
	columnName			VARCHAR(MAX),
	filterValue			VARCHAR(MAX),
	comment				VARCHAR(MAX))

	INSERT INTO #FilterTable
	SELECT
		 CASE WHEN ROW_NUMBER() OVER(ORDER BY ColumnName ASC) = 1
			THEN ''
		 ELSE
			 CASE WHEN matchType= 'any' THEN ' or ' ELSE ' and ' END
		 END,
		'([' + CM.asentia + '] ' + FT.operator + ' ' ,  val, ')'
	FROM OPENXML(@hDoc, 'instanceData/field/criterion', 3) 
	WITH 
	( 
		ColumnName [varchar](50) '../@columnName',
		operator [varchar](20) '@operator',
		val ntext 'text()',
		matchType [varchar](50) '../@matchType' 
	) FR
	LEFT JOIN #tblFilterTransform FT ON FT.identifier = FR.operator
	LEFT JOIN #tblColumnMapping CM ON CM.inquisiq = FR.ColumnName
	WHERE matchType is not null
	
	UPDATE #FilterTable
	SET columnName = REPLACE(columnName, 'data', filterValue) 
	WHERE filterValue IS NOT NULL

	SELECT @filters = STUFF(
		(
			SELECT orAND + columnName + comment FROM #FilterTable FOR XML PATH('')
		)
	, 1, 0, '')

	DROP TABLE #FilterTable
	EXEC sp_xml_removedocument @hDoc

	SET @sql = ' INSERT INTO #AsentiaFormatReportTable'
		 + ' ('
		 + '	idReport,'
		 + '	idSite,'
		 + '	idUser,'
		 + '	idDataset,'
		 + '	isPublic,'
		 + '	fields,'
		 + '	filters'
		 + ' )'
		 + '  SELECT'
		 + '	R.idReport,'
		 + '	R.idSite,'
		 + '	R.idUser,'
		 + '	R.idDataset,'
		 + '	R.isPublic,'
		 + '	Left(''' + @fields + ''',LEN(''' + @fields + ''')-1),'
		 + '	''' + REPLACE(@filters, '''', '''''')  +''''
		 + '	FROM   [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReport] R'
		 + '	WHERE R.idReport = ' + CONVERT(varchar(10), @idReport)
	EXEC (@sql)
	DELETE #TempTable Where idReport = @idReport
END

	DROP TABLE #tblFilterTransform
	DROP TABLE #TempTable

	CREATE TABLE #TempDatasetTranformTable (
		idOldDataset		INT,
		idNewDataset		INT
	)

	INSERT INTO #TempDatasetTranformTable VALUES(1,1)
	INSERT INTO #TempDatasetTranformTable VALUES(2,2)
	INSERT INTO #TempDatasetTranformTable VALUES(3,4)
	INSERT INTO #TempDatasetTranformTable VALUES(4,8)
	INSERT INTO #TempDatasetTranformTable VALUES(5,3)

	-- reports
	SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblReport]'
			+ ' ( '
			+ '		idSite, '
			+ '		idUser, '
			+ '		idDataset, '
			+ '		title, '
			+ '		fields, '
			+ '		filter, '
			+ '		[order], '				
			+ '		isPublic, '
			+ '		dtCreated, '
			+ '		dtModified, '
			+ '		isDeleted, '
			+ '		dtDeleted '
			+ ' ) '
			+ ' SELECT '
			+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite	
			+ '		UM.destinationID,'															-- idUser
			+ '		DST.idNewDataset,'															-- idDataset
			+ '		R.name + '' ##'' + CONVERT(NVARCHAR, R.idReport) + ''##'','					-- title
			+ '		AFR.fields,'																-- fields
			+ '		AFR.filters,'																-- filter
			+ '		NULL,'																		-- order
			+ '		R.isPublic,'																-- isPublic
			+ '		GETDATE(),'																	-- dtCreated
			+ '		GETDATE(),'																	-- dtModified
			+ '		NULL,'																		-- isDeleted
			+ '		NULL'																		-- dtDeleted			
			+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReport] R'
			+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = R.idUser'
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = R.idUser AND UM.object = ''user'''
			+ ' LEFT JOIN #TempDatasetTranformTable DST ON DST.idOldDataset = R.idDataset'
			+ ' LEFT JOIN #AsentiaFormatReportTable AFR ON AFR.idReport = R.idReport'
			+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) 
		
	 EXEC (@sql)
	 DROP TABLE #TempDatasetTranformTable

		-- insert idReport mappings (idReport maps to Inquisiq idReport)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SR.idReport, '
				+ '		DR.idReport,'
				+ '		''report''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblReport] DR '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReport] SR ON SR.name + '' ##'' + CONVERT(NVARCHAR, SR.idReport) + ''##'' = DR.title collate database_default'
				+ ' WHERE DR.idSite IS NOT NULL AND DR.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SR.idReport IS NOT NULL AND DR.idReport IS NOT NULL'		
		EXEC(@sql)

		-- clean up the ##idReport## additions we made to report titles, we did that to uniquely distinguish names
		SET @sql = ' UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblReport] '
				+ '	SET	title = REPLACE(DR.title, '' ##'' + CONVERT(NVARCHAR, SEIM.sourceID) + ''##'', ''''),'
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblReport] DR '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SEIM ON SEIM.destinationID = DR.idReport AND SEIM.object = ''report'''
				+ ' WHERE SEIM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- insert user report subscription
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblReportSubscription]'
			+ ' ( '
			+ '		idSite, '
			+ '		idUser, '
			+ '		idReport, '
			+ '		dtStart, '
			+ '		dtNextAction, '
			+ '		recurInterval, '
			+ '		recurTimeframe, '				
			+ '		token '
			+ ' ) '
			+ ' SELECT '
			+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite	
			+ '		UM.destinationID,'															-- idUser
			+ '		RM.destinationID,'															-- idReport
			+ '		RS.dtStart,'																-- dtStart
			+ '		RS.dtNextAction,'															-- dtNextAction
			+ '		RS.recurInterval,'															-- recurInterval
			+ '		RS.recurTimeframe,'															-- recurTimeframe
			+ '		RS.token'																	-- token		
			+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblReportSubscription] RS'
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = RS.idUser AND UM.object = ''user'''
			+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RM ON RM.sourceID = RS.idReport AND RM.object = ''report'''
			+ ' WHERE UM.destinationID IS NOT NULL AND RM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		DROP TABLE #AsentiaFormatReportTable
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaReport_Success'		
		
	SET XACT_ABORT OFF

END

GO