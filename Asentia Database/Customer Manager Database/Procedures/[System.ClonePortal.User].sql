SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.User]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.User]
GO


/*

CLONE PORTAL - USER DATA
OCCURS FOR FULL COPY ONLY

*/
CREATE PROCEDURE [System.ClonePortal.User]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.0: Copy Users - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	   
		-- insert a row in the mapings table to map user id 1 to user id 1, this is so we don't have to code exceptions for user id 1
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	object '
				 + ') '
				 + 'VALUES '
				 + '( '
				 + '	1, '
				 + '	1, '
				 + '	''users'' '
				 + ')'

		EXEC(@sql)
		
		-- declare two variables for the user insert SQL, the query length exceeds what can be concatenated in one string
		DECLARE @userSql1 NVARCHAR(MAX)
		DECLARE @userSql2 NVARCHAR(MAX)
	   
		-- copy users
		SET @userSql1 = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUser] '
	   				  + '( '
	   				  + '	idSite, '
	   				  + '	firstName, '
	   				  + '	middleName, '
	   				  + '	lastName, '
	   				  + '	displayName, '
	   				  + '	email, '
	   				  + '	username, '
	   				  + '	[password], '
	   				  + '	isDeleted, '
	   				  + '	dtDeleted, '
	   				  + '	idTimezone, '
	   				  + '	isActive, '
	   				  + '	mustchangePassword, '
	   				  + '	dtCreated, '
	   				  + '	dtExpires, '
	   				  + '	idLanguage, '
	   				  + '	dtModified, '
	   				  + '	employeeID, '
	   				  + '	company, '
	   				  + '	[address], '
	   				  + '	city, '
	   				  + '	province, '
	   				  + '	postalcode, '
	   				  + '	country, '
	   				  + '	phonePrimary, '
	   				  + '	phoneWork, '
	   				  + '	phoneFax, '
	   				  + '	phoneHome, '
	   				  + '	phoneMobile, '
	   				  + '	phonePager, '
	   				  + '	phoneOther, '
	   				  + '	department, '
	   				  + '	division, '
	   				  + '	region, '
	   				  + '	jobTitle, '
	   				  + '	jobClass, '
	   				  + '	gender, '
	   				  + '	race, '
	   				  + '	dtDOB, '
	   				  + '	dtHire, '
	   				  + '	dtTerm, '
	   				  + '	avatar, '
	   				  + '	objectGUID, '
	   				  + '	activationGUID, '
	   				  + '	distinguishedName, '
	   				  + '	dtLastLogin, '
	   				  + '	dtSessionExpires, '
	   				  + '	activeSessionId, '
	   				  + '	dtLastPermissionCheck, '
	   				  + '	field00, '
	   				  + '	field01, '
	   				  + '	field02, '
	   				  + '	field03, '
	   				  + '	field04, '
	   				  + '	field05, '
	   				  + '	field06, '
	   				  + '	field07, '
	   				  + '	field08, '
	   				  + '	field09, '
	   				  + '	field10, '
	   				  + '	field11, '
	   				  + '	field12, '
	   				  + '	field13, '
	   				  + '	field14, '
	   				  + '	field15, '
	   				  + '	field16, '
	   				  + '	field17, '
	   				  + '	field18, '
	   				  + '	field19, '
	   				  + '	isRegistrationApproved, '	   				  
	   				  + '	rejectionComments, '
	   				  + '	dtApproved, '
	   				  + '	dtDenied, '
	   				  + '	idApprover, '
	   				  + '	disableLoginFromLoginForm, '
	   				  + '	dtMarkedDisabled, '
	   				  + '	exemptFromIPLoginRestriction, '
					  + '	optOutOfEmailNotifications, '
					  + '	dtUserAgreementAgreed '
	   				  + ') '
	   				  + 'SELECT '
					  +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				  + '	U.firstName, '
	   				  + '	U.middleName, '
	   				  + '	U.lastName, '
	   				  + '	U.displayName, '
	   				  + '	U.email, '
	   				  + '	U.username, '
	   				  + '	U.[password], '
	   				  + '	U.isDeleted, '
	   				  + '	U.dtDeleted, '
	   				  + '	U.idTimezone, '
	   				  + '	U.isActive, '
	   				  + '	U.mustchangePassword, '
	   				  + '	U.dtCreated, '
	   				  + '	U.dtExpires, '
	   				  + '	U.idLanguage, '
	   				  + '	U.dtModified, '
	   				  + '	U.employeeID, '
	   				  + '	U.company, '
	   				  + '	U.[address], '
					  + '	U.city, '
	   				  + '	U.province, '
	   				  + '	U.postalcode, '
	   				  + '	U.country, '
	   				  + '	U.phonePrimary, '
	   				  + '	U.phoneWork, '
	   				  + '	U.phoneFax, '
	   				  + '	U.phoneHome, '
	   				  + '	U.phoneMobile, '
	   				  + '	U.phonePager, '
	   				  + '	U.phoneOther, '
	   				  + '	U.department, '
	   				  + '	U.division, '
	   				  + '	U.region, '
	   				  + '	U.jobTitle, '
	   				  + '	U.jobClass, '
	   				  + '	U.gender, '
	   				  + '	U.race, '
	   				  + '	U.dtDOB, '
	   				  + '	U.dtHire, '
	   				  + '	U.dtTerm, '
	   				  + '	U.avatar, '
	   				  + '	U.objectGUID, '
	   				  + '	U.activationGUID, '
	   				  + '	U.distinguishedName, '
	   				  + '	U.dtLastLogin, '
	   				  + '	U.dtSessionExpires, '
	   				  + '	U.activeSessionId, '
	   				  + '	U.dtLastPermissionCheck, '
		SET @userSql2 = '	U.field00, '
		  			  + '	U.field01, '
		  			  + '	U.field02, '
		  			  + '	U.field03, '
		  			  + '	U.field04, '
		  			  + '	U.field05, '
		  			  + '	U.field06, '
		  			  + '	U.field07, '
		  			  + '	U.field08, '
		  			  + '	U.field09, '
		  			  + '	U.field10, '
		  			  + '	U.field11, '
		  			  + '	U.field12, '
		  			  + '	U.field13, '
		  			  + '	U.field14, '
		  			  + '	U.field15, '
		  			  + '	U.field16, '
		  			  + '	U.field17, '
		  			  + '	U.field18, '
		  			  + '	U.field19, '
		  			  + '	U.isRegistrationApproved, '		  			  
		  			  + '	U.rejectionComments, '
		  			  + '	U.dtApproved, '
		  			  + '	U.dtDenied, '
		  			  + '	U.idApprover, '
		  			  + '	U.disableLoginFromLoginForm, '
		  			  + '	U.dtMarkedDisabled, '
		  			  + '	U.exemptFromIPLoginRestriction, '
					  + '	U.optOutOfEmailNotifications, '
					  + '	U.dtUserAgreementAgreed '
		  			  + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUser] U '
		  			  + 'WHERE U.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
		  			  + 'AND U.idUser <> 1 '

		SET @userSql1 = @userSql1 + @userSql2
	   
		EXEC(@userSql1)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.1: Copy Users - Users Inserted', 0)

		-- insert the mapping for source to destination user ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SU.idUser, '
	   			 + '	DU.idUser, '
	   			 + '	''users'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUser] DU '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUser] SU '
	   			 + 'ON (SU.idSite =' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SU.username = DU.username) '
	   			 + 'WHERE DU.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' '
	   			 + 'AND DU.idUser <> 1'
	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.2: Copy Users - User Mappings Created', 0)
	   
		-- update idApprover column with the appropriate user id
		SET @sql = 'UPDATE DSTU '
				 + '	SET DSTU.idApprover = TEMP.destinationId '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblUser] DSTU '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TEMP '
				 + 'ON TEMP.sourceId = DSTU.idApprover AND TEMP.object = ''users'' '
				 + 'WHERE DSTU.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' '
				 + 'AND DSTU.idApprover IS NOT NULL AND TEMP.destinationId IS NOT NULL'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.3: Copy Users - User ApprovedBy Column Updated', 0)

		-- copy course experts
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseExpert] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + ' FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseExpert] CE '
	   			 + ' LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = CE.idUser AND TTU.object = ''users'') '
	   			 + ' LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = CE.idCourse AND TTC.object = ''courses'') '
	   			 + ' WHERE CE.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 	
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.4: Copy Users - Course Experts Inserted', 0)

		-- copy course enrollment approvers
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseEnrollmentApprover] '
	   			 + ' ( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseEnrollmentApprover] CEA '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = CEA.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = CEA.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CEA.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 	
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.5: Copy Users - Course Enrollment Approvers Inserted', 0)
	   
		-- copy user supervisors
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUserToSupervisorLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idUser, '
	   			 + '	idSupervisor '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTU.destinationId, '
	   			 + '	TTS.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserToSupervisorLink] UTSL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = UTSL.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTS ON (TTS.sourceId = UTSL.idSupervisor AND TTS.object = ''users'') '
	   			 + 'WHERE UTSL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.6: Copy Users - User Supervisors Inserted', 0)

		-- copy activity import records
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblActivityImport] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idUser, '
	   			 + '	[timestamp], '
	   			 + '	idUserImported, '
	   			 + '	importFileName '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTUser.destinationId, '
	   			 + '	AI.[timestamp], '
	   			 + '	TTUserImported.destinationId, '
	   			 + '	AI.importFileName '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblActivityImport] AI '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUser on (TTUser.sourceId = AI.idUser AND TTUser.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUserImported on (TTUserImported.sourceId = AI.idUserImported AND TTUserImported.object = ''users'') '
	   			 + 'WHERE AI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.7: Copy Users - Activity Import Records Inserted', 0)
	   
		-- insert the mapping for source to destination activity import ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idActivityImport, '
	   			 + '	DST.idActivityImport, '
	   			 + '	''activityimport'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblActivityImport] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblActivityImport] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SRC.timestamp = DST.timestamp) '
	   			 + '	AND (SRC.importFileName = DST.importFileName OR (SRC.importFileName IS NULL AND DST.importFileName IS NULL))) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4.8: Copy Users - Activity Import Mappings Created', 0)

		/*

		RETURN

		*/
		
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_User_InsertedSuccessfully'

	END TRY

	BEGIN CATCH
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 4: Copy Users - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_User_PortalMigrationFailed'
			
	END CATCH
			
	SET XACT_ABORT OFF

END