-- =====================================================================
-- PROCEDURE: [DatabaseServer.DoesServerNameExists]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.DoesServerNameExists]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.DoesServerNameExists]
GO

CREATE PROCEDURE [DatabaseServer.DoesServerNameExists]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@serverName				NVARCHAR (255),
	@isServerNameAlredyExists	BIT				OUTPUT
)
AS

	BEGIN
	SET NOCOUNT ON
	
	/*
	
	Determines if the current session server alredy has a server name with same name.
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblDatabaseServer ATDBS
		WHERE (ATDBS.serverName = @serverName 
		 )	
		
		) = 0 
		BEGIN
		SET @isServerNameAlredyExists = 0
		END
	ELSE
		BEGIN
		SET @isServerNameAlredyExists = 1
		END
		
	SET @Return_Code = 0
	SET @Error_Description_Code = ''

	END


GO


