
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal]
GO


/*

Copy source site data to destination site

*/
CREATE PROCEDURE [System.ClonePortal]
(
	@Return_Code			INT	OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@fullCopyFlag			BIT,		-- 1 for full copy and 0 for partial copy
	@isNewHostName			BIT,		-- 1 for same as source, and 0 for new portal name
	@idSiteSource			INT,
	@idSiteDestination		INT,		-- in case of partial copy '@idSiteDestination' value get by CS code, and in case of full copy '@idSiteDestination' update by current inserted idSite.
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@destinationHostName    NVARCHAR(50),
	@idAccount				INT				= NULL,
	@idCaller				INT	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON
    
    BEGIN TRY	
    
	   DECLARE @sql NVARCHAR(MAX)
	   DECLARE @subSql VARCHAR(MAX)					
	   DECLARE @isHostNameExistSql INT	
	   DECLARE @idAccountDestination INT
	   

	 /*
	  
	   Replace double slash with single slash
	 
	 */
	   SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
	   SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')
	   
	   CREATE TABLE #ObjectMapping
	   (
		  [oldId] INT NOT NULL,
		  [newId] INT NOT NULL,
		  objectName NVARCHAR(100)
	   )
	   	

	/*
	
	validate the unique hostname
	
	*/
	 -- validate unique host name in Account database
	 SET @sql = ' SET @isHostNameExistSql = (SELECT COUNT(*) FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSite] 
				WHERE hostname = ' + '''' + @destinationHostName + ''''
			 +')'
  
	EXECUTE SP_EXECUTESQL @sql, N'@isHostNameExistSql INT OUTPUT', @isHostNameExistSql OUTPUT
		
	-- Note: full copy will always occur between two different databases, if we have taken destination hostname same as source host name
	-- if copying site for full copy and host name already exists in account DB's tblSite, return with error 
	IF (@isHostNameExistSql > 0  AND @fullCopyFlag = 1)
	BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'SystemClonePortal_HostNameNotUnique'
		PRINT @Error_Description_Code
		RETURN 1
	END

    /*

	TRANSACTION BEGIN

	*/
	--begin distributed transaction
	BEGIN DISTRIBUTED TRANSACTION

	   
	    -- check condition for the full copy
	IF @fullCopyFlag = 1
	BEGIN
-- =============================================================================
-- LOGIC START -- copy tblSite object for full copy ----------------------------


	   -- insert records from source site to destination table Site
	   SET @sql =' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSite]'
				+' ( '
				+'	  hostname, '
				+'	  password, '
				+'	  isActive, '
				+'	  dtExpires, '
				+'	  title, '
				+'	  company, '
				+'	  contactName, '
				+'	  contactEmail, '
				+'	  userLimit, '
				+'	  kbLimit, '
				+'	  idLanguage, '
				+'	  idTimezone' 
				+' ) '
				+' SELECT ' 
				+'''' +@destinationHostName +''''
				+',	CP.password, '
				+'	CP.isActive, '
				+'	CP.dtExpires, '
				+'	CP.title, '
				+'	CP.company, '
				+'	CP.contactName, '
				+'	CP.contactEmail,' 
				+'	CP.userLimit,' 
				+'	CP.kbLimit, '
				+'	CP.idLanguage,'
				+'	CP.idTimezone '
				+' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSite] CP'
				+' WHERE CP.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC (@sql)
	   
	   -- temporary table to hold the idSite of source and destination site tables
	   CREATE TABLE #SiteIdMappings
			 (
			     oldSourceId INT NOT NULL, 
			     newDestinationId INT NOT NULL  	   
			 )	
	  
	   -- inserting idSite of source Site table and destination Site table inside temporary table for mapping	
	   SET @sql =' INSERT INTO #SiteIdMappings '
				+' ( '
				+'	 oldSourceId,'
				+'	 newDestinationId'
				+' ) '
				+' SELECT '
				+' SS.idSite, '
				+' DS.idSite' 
				+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSite] DS '
				+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSite] SS '
				+' ON (SS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				+' AND (SS.isActive = DS.isActive ) ' 
				+' AND (SS.dtExpires = DS.dtExpires OR (SS.dtExpires IS NULL AND DS.dtExpires IS NULL)) '
				+' AND (SS.title = DS.title ) '
				+' AND (SS.company = DS.company ) '
				+' AND (SS.contactEmail = DS.contactEmail OR (SS.contactEmail IS NULL AND DS.contactEmail IS NULL))'					
				+' )'
				+' WHERE SS.idSite IS NOT NULL AND SS.idSite ='+ CAST(@idSiteSource AS NVARCHAR)
		
	   EXEC(@sql)	
				
		
	   -- SiteLanguage data move logic start
	   
	   -- insert records from source site to destination table SiteLanguage
	   SET @sql =' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSiteLanguage]'
				+' ( '
				+'   idSite, '
				+'   idLanguage, '
				+'   title, '
				+'   company'
				+' ) '
				+' SELECT '
				+'  TTS.newDestinationId,  '
				+'  SL.idLanguage, '
				+'  SL.title, '
				+'  SL.company  '
				+' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSiteLanguage]  SL'
				+' LEFT JOIN #SiteIdMappings TTS on TTS.oldSourceId = SL.idSite'
				+' WHERE SL.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC(@sql)
	   
	   -- get new created destination site id 
	   DECLARE @newIdSite INT

	   SET @sql = ' SET @newIdSite = (SELECT Max(idSite)'
                 +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSite] )'

	  
	  
	   EXECUTE SP_EXECUTESQL @sql, N'@newIdSite INT OUTPUT', @newIdSite OUTPUT
	   
	   -- set new created destination site id 
	   SET @idSiteDestination = @newIdSite

		
	   -- droping temporary table site 
	   DROP TABLE #SiteIdMappings

	   
	   
-- ===============================================================================================
-- LOGIC START -- copy tblAccountToDomainAliasLink object for full copy with new host name ----

-- not taking the fully qualified name because table tblAccountToDomainAliasLink exist inside the local database

	
	   SET @sql = ' INSERT INTO [tblAccountToDomainAliasLink]'
				+' ( '
				+'	idAccount,'  
				+'	hostname, '
				+'	domain '
				+' ) '
				+' SELECT '
				+   CAST(@idAccount AS NVARCHAR) 
				+',' 
				+ '''' + @destinationHostName + ''''
				+', ''''' 
								
	   EXEC(@sql)		   
	   

-- =============================================================================
-- LOGIC START -- copy tblSiteToDomainAliasLink object for full copy -----------


	   -- insert records from source site to destination table SiteToDomainAliasLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSiteToDomainAliasLink]'
	   			 +' ( '
	   			 +'  idSite,'  
	   			 +'  domain '
	   			 +' ) '
	   			 +' SELECT '
	   			 +   CAST(@idSiteDestination AS NVARCHAR)
				 +',' 
	   			 + '''' + @destinationHostName + ''''
						
	   EXEC(@sql)	
	   
    END

------------------------------------------------------------------------------
-- full copy end



-- ===============================================================================
-- LOGIC START -- copy tblSiteParam object for both full copy and partial copy ---

      
	   -- insert records from source site to destination table SiteParam
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSiteParam]'
	   			 +' ( '
	   			 +'   idSite,'
	   			 +'	 param,'
	   			 +'	 value'
	   			 +'  ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , SP.param,'
	   			 +'   SP.value '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSiteParam] SP'
	   			 +' WHERE SP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			 +' AND SP.idSite != 1'    --  idSite = 1, already generated while creating database structure

	   EXEC(@sql)

-- ============================================================================================
-- LOGIC START -- copy tblSiteAvailableLanguage object for both full copy and partial copy ----
		
	   -- insert records from source site to destination table SiteAvailableLanguage
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSiteAvailableLanguage]'
				 +' ( '
				 +'  idSite,'     
				 +'  idLanguage ' 			 
				 +' ) '
				 +'SELECT '
				 +   CAST(@idSiteDestination AS NVARCHAR)
				 +', SAL.idLanguage '
				 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSiteAvailableLanguage] SAL '
				 +' WHERE SAL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
			
	   EXEC(@sql)		

-- =============================================================================
-- LOGIC START -- copy tblCatalog object for both full copy and partial copy ----

	   -- insert records from source site to destination table catalog
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalog]'
				  +' ( '
				  +'  idSite, 
				 	 idParent, 
				 	 title, 
				 	 [order], 
				 	 shortDescription, 
				 	 longDescription, 
				 	 isPrivate, 
				 	 isClosed, 
				 	 dtCreated, 
				 	 dtModified, 
				 	 cost, 
				 	 costType, 
				 	 searchTags'
				 +' ) '
				 +' SELECT '
				 +   CAST(@idSiteDestination AS NVARCHAR)
				 +', NULL, '     ---- this null value of idParent will be updated from temporary CatalogIdMappings table below
				 +'	C.title, 
				 	C.[order], 
				 	C.shortDescription, 
				 	C.longDescription, 
				 	C.isPrivate, 
				 	C.isClosed, 
				 	C.dtCreated, 
				 	C.dtModified, 
				 	C.cost,
				 	C.costType, 
				 	C.searchTags 
				    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalog] C'
				 +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						
	   EXEC(@sql)
	   
	   -- temporary table to hold the idCatalog of source and destination Catalog tables
	   CREATE TABLE #CatalogIdMappings 
	   (
	   	oldSourceId INT NOT NULL, 
	   	newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idCatalog of source Catalog table and destination Catalog table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CatalogIdMappings'
				  +' ( '
				  +'  oldSourceId,
				  	 newDestinationId'
				  +' ) '
				  +' SELECT 
				      SRC.idCatalog,
					 DST.idCatalog '
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalog] DST'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalog] SRC'
				  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SRC.title = DST.title)'
				  +' AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL))'
				  +' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
				  +' )'
				  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)

	   EXEC(@sql)
	   
	   -- update idParent from temp table catalog
	   SET @sql =	   ' UPDATE DSTC'
	   			  +'  SET idParent = TEMP.newDestinationId'
	   			  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalog] SRCC'
	   			  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalog] DSTC'
	   			  +' ON( SRCC.title = DSTC.title'
	   			  +' AND (SRCC.dtCreated = DSTC.dtCreated OR (SRCC.dtCreated IS NULL AND DSTC.dtCreated IS NULL))'
	   			  +' AND (SRCC.dtModified = DSTC.dtModified OR (SRCC.dtModified IS NULL AND DSTC.dtModified IS NULL))'
	   			  +' AND SRCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	   			  +' INNER JOIN  #CatalogIdMappings TEMP'
	   			  +' ON(TEMP.oldSourceId = SRCC.idParent)'
	   			  +' WHERE DSTC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND SRCC.idParent IS NOT NULL'
	   
	   EXEC(@sql)	
	   
	   
	   -- catalog language data move logic start

	   -- insert records from source site to destination table CatalogLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalogLanguage]'
				  +' ( '
				  +'   idSite, 
				  	  idCatalog, 
				  	  idLanguage, 
				  	  title, 
				  	  shortDescription, 
				  	  longDescription, 
				  	  searchTags'
				  +'  ) '
				  +' SELECT '
				  +    CAST(@idSiteDestination AS NVARCHAR)
				  +' , TTC.newDestinationId, 
				  	  CL.idLanguage, 
				  	  CL.title, 
				  	  CL.shortDescription, 
				  	  CL.longDescription, 
				  	  CL.searchTags  
				     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalogLanguage] CL'
				  +' LEFT JOIN #CatalogIdMappings TTC on (TTC.oldSourceId = CL.idCatalog)'
				  +' WHERE CL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
								
	   EXEC(@sql)
	   
	   -- catalog language move data logic end		


-- =============================================================================
-- LOGIC START -- copy tblCourse object for both full copy and partial copy ----

	
	   -- insert records from source site to destination table Course
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourse]'
				  +' ( '
			       +'   idSite, 
                	 	  title, 
                	 	  coursecode, 
                	 	  revcode, 
                	 	  avatar, 
                	 	  cost, 
                	 	  credits, 
                	 	  [minutes], 
                	 	  shortDescription,  
                	 	  longDescription, 
                	 	  objectives, 
                	 	  socialMedia,  
                	 	  isPublished, 
                	 	  isClosed, 
                	 	  isLocked, 
                	 	  isPrerequisiteAny, 
                	 	  isFeedActive, 
                	 	  isFeedModerated, 
                	 	  isFeedOpenSubscription, 
                	 	  dtCreated, 
                	 	  dtModified, 
                	 	  isDeleted, 
                	 	  dtDeleted, 
                	 	  [order], 
                	 	  disallowRating, 
                	 	  forceLessonCompletionInOrder, 
                	 	  selfEnrollmentDueInterval, 
                	 	  selfEnrollmentDueTimeframe, 
                	 	  selfEnrollmentExpiresFromStartInterval, 
                	 	  selfEnrollmentExpiresFromStartTimeframe, 
                	 	  selfEnrollmentExpiresFromFirstLaunchInterval, 
                	 	  selfEnrollmentExpiresFromFirstLaunchTimeframe, 
                	 	  searchTags'
                	 +'  ) '
                	 +' SELECT '
				 +     CAST(@idSiteDestination AS NVARCHAR)
                	 +' ,  C.title, 
                	 	  C.coursecode, 
                	 	  C.revcode, 
                	 	  C.avatar, 
                	 	  C.cost, 
                	 	  C.credits, 
                	 	  C.[minutes], 
                	 	  C.shortDescription,  
                	 	  C.longDescription, 
                	 	  C.objectives, 
                	 	  C.socialMedia,  
                	 	  C.isPublished, 
                	 	  C.isClosed, 
                	 	  C.isLocked, 
                	 	  C.isPrerequisiteAny, 
                	 	  C.isFeedActive, 
                	 	  C.isFeedModerated, 
                	 	  C.isFeedOpenSubscription, 
                	 	  C.dtCreated, 
                	 	  C.dtModified, 
                	 	  C.isDeleted, 
                	 	  C.dtDeleted, 
                	 	  C.[order], 
                	 	  C.disallowRating, 
                	 	  C.forceLessonCompletionInOrder, 
                	 	  C.selfEnrollmentDueInterval, 
                	 	  C.selfEnrollmentDueTimeframe, 
                	 	  C.selfEnrollmentExpiresFromStartInterval, 
                	 	  C.selfEnrollmentExpiresFromStartTimeframe, 
                	 	  C.selfEnrollmentExpiresFromFirstLaunchInterval, 
                	 	  C.selfEnrollmentExpiresFromFirstLaunchTimeframe, 
                	 	  C.searchTags 
                	    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourse] C'
                	 +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
							          										
	   EXEC(@sql)
	   
	   -- temporary table to hold the idCourse of source and destination Course tables
	   CREATE TABLE #CourseIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idCourse of source Course table and destination Course table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CourseIdMappings'
	   		  	  +' ( '
	   		  	  +'	   oldSourceId,
	   		  	  	   newDestinationId'
	   		  	  +' ) '
	   		  	  +' SELECT 
					   SRC.idCourse, 
					   DST.idCourse '
	   		  	  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourse] DST'
	   		  	  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourse] SRC'
	   		  	  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		  	  +' AND (SRC.title = DST.title)'
	   		  	  +' AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL))'
	   		  	  +' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
	   		  	  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   		  	  +' )'
	   		  	  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)

				
	   	
	   EXEC(@sql)	
						
	   -- course language data move logic start
	   
	   -- insert records from source site to destination table CourseLanguage
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseLanguage]'
	   			+ ' ( '
	   			+ '  idSite, 
	   			     idCourse, 
	   				idLanguage, 
	   				title, 
	   				shortDescription, 
	   				longDescription, 
	   				searchTags'
	   			+ ' ) '
	   			+' SELECT '
				+    CAST(@idSiteDestination AS NVARCHAR)
	   			+' , TTC.newDestinationId, 
	   				CL.idLanguage, 
	   				CL.title, 
	   				CL.shortDescription, 
	   				CL.longDescription, 
	   				CL.searchTags 
	   			   FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseLanguage] CL'
	   			+' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CL.idCourse)'
	   			+' WHERE CL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 				
	   			
	   EXEC(@sql)

	   -- course language data move logic end

	   -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'courses' FROM #CourseIdMappings


-- =========================================================================================
-- LOGIC START -- copy tblCourseToCatalogLink object for both full copy and partial copy ---

	   -- insert records from source site to destination table CourseToCatalogLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseToCatalogLink]'
	   			  +'  ( '
	   			  +'	   idSite, 
	   			  	   idCatalog, 
	   			  	   idCourse, 
	   			  	   [order]'
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTCatalog.newDestinationId, 
	   			  	   TTCourse.newDestinationId, 
	   			  	   CTCL.[order] 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseToCatalogLink] CTCL'
	   			  +' LEFT JOIN #CatalogIdMappings TTCatalog on (TTCatalog.oldSourceId = CTCL.idCatalog )'
	   			  +' LEFT JOIN #CourseIdMappings TTCourse on (TTCourse.oldSourceId = CTCL.idCourse )'
	   			  +' WHERE CTCL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblGroup object for both full copy and partial copy-----------

                
	   -- insert records from source site to destination table Group
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroup]'
				  +' ( '
			       +'   idSite, 
				       name,
				       primaryGroupToken,
				       objectGUID,
				       distinguishedName,
				       avatar ,
				       shortDescription,
				       longDescription,
				       isSelfJoinAllowed,
				       isFeedActive,
				       isFeedModerated,
				       membershipIsPublicized,
				       searchTags'
                	  +'  ) '
                	  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
                	  +' , G.name,
				       G.primaryGroupToken,
				       G.objectGUID,
				       G.distinguishedName,
				       G.avatar ,
				       G.shortDescription,
				       G.longDescription,
				       G.isSelfJoinAllowed,
				       G.isFeedActive,
				       G.isFeedModerated,
				       G.membershipIsPublicized,
				       G.searchTags 
                	     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroup] G'
                	  +' WHERE G.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
                										
	   EXEC(@sql)

	   -- temporary table to hold the idGroup of source and destination Group tables
	   CREATE TABLE #GroupIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idGroup of source Group table and destination Group table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #GroupIdMappings '
	   			  +'  ( '
	   			  +'    oldSourceId,
	   			  	   newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					   SRC.idGroup, 
					   DST.idGroup'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroup] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroup] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.name = DST.name)'
	   			  +' AND (SRC.shortDescription = DST.shortDescription OR (SRC.shortDescription IS NULL AND DST.shortDescription IS NULL))'
	   			  +' AND (SRC.longDescription = DST.longDescription OR (SRC.longDescription IS NULL AND DST.longDescription IS NULL))'
	   			  +' AND (SRC.objectGUID = DST.objectGUID OR (SRC.objectGUID IS NULL AND DST.objectGUID IS NULL))'
	   			  +' AND (SRC.primaryGroupToken = DST.primaryGroupToken OR (SRC.primaryGroupToken IS NULL AND DST.primaryGroupToken IS NULL))'
	   			  +' AND (SRC.isSelfJoinAllowed = DST.isSelfJoinAllowed OR (SRC.isSelfJoinAllowed IS NULL AND DST.isSelfJoinAllowed IS NULL))'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)	
	   
	   				
	   -- group language data move logic start
	   
	   -- insert records from source site to destination table GroupLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupLanguage]'
	   			  +'  ( '
	   			  +'   idSite, 
	   			       idGroup, 
	   			  	  idLanguage, 
	   			  	  name, 
	   			  	  shortDescription, 
	   			  	  longDescription, 
	   			  	  searchTags'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTG.newDestinationId, 
	   			  	  GL.idLanguage, 
	   			  	  GL.name, 
	   			  	  GL.shortDescription, 
	   			  	  GL.longDescription, 
	   			  	  GL.searchTags 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupLanguage] GL'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GL.idGroup)'
	   			  +' WHERE GL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   			
	   EXEC(@sql)
	   
	   -- group language data move logic end

	   -- insert temp table data for group in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'groups' FROM #GroupIdMappings	



	   
-- ========================================================================================
-- LOGIC START -- copy tblCatalogAccessToGroupLink object for full copy and partial copy---


	   --insert records from source site to destination table CatalogAccessToGroupLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCatalogAccessToGroupLink]'
	     		  +' ( '
	     		  +'  idSite,
	   		   	 	 idGroup,
	   		   	 	 idCatalog,
	   		   	 	 created'
	     		  +' ) '
	     		  +' SELECT '
	   			  +    CAST(@idSiteDestination AS NVARCHAR)
	     		  +' , TTG.newDestinationId,
	   		   	 	  TTC.newDestinationId,
	   		   	 	  CATGL.created
	     		     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCatalogAccessToGroupLink] CATGL'
	     		  +' LEFT JOIN #CatalogIdMappings TTC on (TTC.oldSourceId = CATGL.idCatalog )'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = CATGL.idGroup )'
	   	
		--EXEC(@sql)
		

-- =============================================================================
-- LOGIC START -- copy tblLesson object for both full copy and partial copy ----

	   -- insert records from source site to destination table Lesson
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLesson]'
	     		+ ' ( '
	     		+ '  idSite, 
	     			idCourse, 
	     			title, 
	     			revcode, 
	     			shortDescription, 
	     			longDescription, 
	     			dtCreated, 
	     			dtModified, 
	     			isDeleted, 
	     			dtDeleted, 
	     			[order], 
	     			searchTags'
	     		+ ' ) '
	     		+' SELECT '
				+    CAST(@idSiteDestination AS NVARCHAR)
	     		+' , TTC.newDestinationId, 
	     			L.title, 
	     			L.revcode, 
	     			L.shortDescription, 
	     			L.longDescription, 
	     			L.dtCreated, 
	     			L.dtModified, 
	     			L.isDeleted, 
	     			L.dtDeleted, 
	     			L.[order], 
	     			L.searchTags 
	     		   FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLesson] L'
	     		+' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = L.idCourse )'
	     		+' WHERE L.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC(@sql)
	   	
	   -- temporary table to hold the idLesson of source and destination Lesson tables
	   CREATE TABLE #LessonIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )
	   
	   -- inserting idLesson of source Lesson table and destination Lesson table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #LessonIdMappings'
				 + ' ( '
				 + '	  oldSourceId,
				 	  newDestinationId'
				 + ' ) '
				 +' SELECT 
				 	  SRC.idLesson, 
				 	  DST.idLesson '
				 +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLesson] DST'
				 +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLesson] SRC'
				 +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				 +' AND (SRC.title = DST.title)'
				 +' AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL))'
				 +' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
				 +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
				 +')'
				 +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
		
	
	   EXEC(@sql)
	   
	   -- lesson language data move logic start

	   -- insert records from source site to destination table LessonLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLessonLanguage]'
              		  +' ( '
              		  +'	   idSite, 
              		  	   idLesson, 
              		  	   idLanguage, 
              		  	   title, 
              		  	   shortDescription, 
              		  	   longDescription, 
              		  	   searchTags'
              		  +' ) '
              		  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
              		  +' ,  TTL.newDestinationId, 
              		  	   LL.idLanguage, 
              		  	   LL.title, 
              		  	   LL.shortDescription, 
              		  	   LL.longDescription, 
              		  	   LL.searchTags 
              		     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLessonLanguage] LL'
              		  +' LEFT JOIN #LessonIdMappings TTL on (TTL.oldSourceId = LL.idLesson )'
              		  +' WHERE LL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	                
	   -- lesson language data move logic end

	   -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'lessons' FROM #LessonIdMappings
	   

------------------------------------------------------------------------------
	   
	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN


-- =============================================================================
-- LOGIC START -- copy tblUser object for full copy ----------------------------

		
	   -- temporary table to hold the idUser of source and destination User tables
	   CREATE TABLE #UserIdMappings 
	   (
	   	  oldSourceId INT NOT NULL,
	   	  newDestinationId INT NOT NULL 	   
	   )
	   
	   -- initially insert row for admin record
	   INSERT INTO #UserIdMappings(
	   	  oldSourceId,
	   	  newDestinationId
	   )
	   VALUES
	     (
	   	  1,
	   	  1
		)
		
	   -- user insert query exceed the length of nvarchar.
	   DECLARE @sqlStrUser VARCHAR(MAX)
	   
	   -- insert records from source site to destination table User
	   SET @sqlStrUser =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUser]'
	   				 +' ( '
	   				 +'    idSite, 
	   					  firstName, 
	   					  middleName,	
	   					  lastName, 
	   					  displayName, 
	   					  email, 
	   					  username, 
	   					  [password], 
	   					  isDeleted, 
	   					  dtDeleted,
	   					  idTimezone, 
	   					  isActive, 
	   					  mustchangePassword,
	   					  dtCreated, 
	   					  dtExpires, 
	   					  idLanguage, 
	   					  dtModified, 
	   					  employeeID, 
	   					  company, 
	   					  [address], 
	   					  city, 
	   					  province, 
	   					  postalcode, 
	   					  country, 
	   					  phonePrimary, 
	   					  phoneWork, 
	   					  phoneFax, 
	   					  phoneHome, 
	   					  phoneMobile, 
	   					  phonePager, 
	   					  phoneOther, 
	   					  department, 
	   					  division, 
	   					  region, 
	   					  jobTitle, 
	   					  jobClass, 
	   					  gender, 
	   					  race, 
	   					  dtDOB, 
	   					  dtHire, 
	   					  dtTerm, 
	   					  avatar,	
	   					  objectGUID,	
	   					  activationGUID,
	   					  distinguishedName,	
	   					  dtLastLogin,	
	   					  dtSessionExpires,	
	   					  activeSessionId,	
	   					  dtLastPermissionCheck,
	   					  field00, 
	   					  field01, 
	   					  field02, 
	   					  field03, 
	   					  field04, 
	   					  field05, 
	   					  field06, 
	   					  field07, 
	   					  field08, 
	   					  field09, 
	   					  field10, 
	   					  field11, 
	   					  field12, 
	   					  field13, 
	   					  field14, 
	   					  field15, 
	   					  field16, 
	   					  field17, 
	   					  field18, 
	   					  field19'
	   			     +' ) '
	   			     +' SELECT '
					+	  CAST(@idSiteDestination AS NVARCHAR)
	   			     +' ,	  U.firstName, 
	   					  U.middleName, 
	   					  U.lastName, 
	   					  U.displayName, 
	   					  U.email, 
	   					  U.username, 
	   					  U.[password],
	   					  U.isDeleted,
	   					  U.dtDeleted, 
	   					  U.idTimezone, 
	   					  U.isActive, 
	   					  U.mustchangePassword, 
	   					  U.dtCreated, 
	   					  U.dtExpires, 
	   					  U.idLanguage, 
	   					  U.dtModified, 
	   					  U.employeeID, 
	   					  U.company, 
	   					  U.[address], 
	   					  U.city, 
	   					  U.province, 
	   					  U.postalcode, 
	   					  U.country, 
	   					  U.phonePrimary, 
	   					  U.phoneWork, 
	   					  U.phoneFax, 
	   					  U.phoneHome, 
	   					  U.phoneMobile, 
	   					  U.phonePager, 
	   					  U.phoneOther, 
	   					  U.department, 
	   					  U.division, 
	   					  U.region, 
	   					  U.jobTitle, 
	   					  U.jobClass, 
	   					  U.gender, 
	   					  U.race, 
	   					  U.dtDOB, 
	   					  U.dtHire, 
	   					  U.dtTerm,
	   					  U.avatar,	
	   					  U.objectGUID,	
	   					  U.activationGUID,
	   					  U.distinguishedName,	
	   					  U.dtLastLogin,	
	   					  U.dtSessionExpires,	
	   					  U.activeSessionId,	
	   					  U.dtLastPermissionCheck, '
	   			
		 
		  SET @subSql = '  U.field00, 
		  				  U.field01, 
		  				  U.field02, 
		  				  U.field03, 
		  				  U.field04, 
		  				  U.field05, 
		  				  U.field06, 
		  				  U.field07, 
		  				  U.field08, 
		  				  U.field09, 
		  				  U.field10, 
		  				  U.field11, 
		  				  U.field12, 
		  				  U.field13, 
		  				  U.field14, 
		  				  U.field15, 
		  				  U.field16, 
		  				  U.field17, 
		  				  U.field18,
		  				  U.field19 
	   			      FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUser] U '
	   			   +' WHERE U.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			   +' AND U.idUser != 1 ' -- idUser 1 is already inserted while creating db structure.
	   
	   SET @sqlStrUser = @sqlStrUser + @subSql
	   
	   EXEC(@sqlStrUser)
	   

	   SET @sql =  ' INSERT INTO #UserIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idUser, 
					   DST.idUser'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUser] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUser] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.firstName = DST.firstName)'
	   			  +' AND (SRC.lastName = DST.lastName)'
	   			  +' AND (SRC.email = DST.email OR (SRC.email IS NULL AND DST.email IS NULL))'
	   			  +' AND (SRC.dtCreated = DST.dtCreated )'
	   			  +' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
	   			  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND DST.idUser != 1' -- idUser 1 is already inserted while creating db structure.
	
	   EXEC(@sql)
	   
	   -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'users' FROM #UserIdMappings


	   
    End

------------------------------------------------------------------------------
-- full copy end


-- =================================================================================================
-- LOGIC START -- copy tblQuizSurvey object for both full copy and partial copy with condition -----

	   -- insert records from source site to destination table QuizSurvey
	   SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblQuizSurvey]'
							+ ' ( '
			                	+ '  idSite,
								[type],
								identifier,	
								guid,
								data,
								isDraft,
								idContentPackage,	
								dtCreated,
								dtModified,
								idAuthor'
                				+ ' ) '
                				+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
                				+'  ,QS.[type],
								QS.identifier,	
								QS.guid,
								QS.data,
								QS.isDraft,
								NULL,	
								QS.dtCreated,
								QS.dtModified,'

		IF(@fullCopyFlag = 1) -- idAuthor is available for full copy only, because tblUser moved only for full copy.
		  BEGIN
			 SET @subSql =' TTU.newDestinationId
							FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblQuizSurvey] QS'
                				+' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = QS.idAuthor)'
							+' WHERE QS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
            END
		ELSE
		  BEGIN -- otherwise 1 is inserted in this field as administrator id
			  SET @subSql = ' 1
							FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblQuizSurvey] QS'
                				+' WHERE QS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		  END
	
		SET @sql = @sql + @subSql
		EXEC(@sql)		
	
	   -- temporary table to hold the idQuizSurvey of source and destination QuizSurvey tables
	   CREATE TABLE #QuizSurveyIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idQuizSurvey of source QuizSurvey table and destination QuizSurvey table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #QuizSurveyIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId '
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idQuizSurvey, 
					   DST.idQuizSurvey '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblQuizSurvey] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblQuizSurvey] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.guid = DST.guid)'
	   			  +' AND (SRC.dtCreated = DST.dtCreated)'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   	
	   EXEC(@sql)

-- ====================================================================================================
-- LOGIC START -- copy tblContentPackage object for both full copy and partial copy with condition ----


	   -- insert records from source site to destination table ContentPackage
	   SET @sql =   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblContentPackage]'
				+' ( '
				+'   idSite,
					name,
					[path],
					kb,
					idContentPackageType,
					idSCORMPackageType,
					manifest,
					dtCreated,
					dtModified,
					isMediaUpload,
					idMediaType,
					contentTitle,
					originalMediaFilename,
					isVideoMedia3rdParty,
					videoMediaEmbedCode,
					enableAutoplay,
					allowRewind,
					allowFastForward,
					allowNavigation,
					allowResume,
					minProgressForCompletion,
					isProcessing,
					isProcessed,
					dtProcessed,
					idQuizSurvey'
				+'  ) '
				+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
				+' , CP.name'
				+' , CONCAT(''/warehouse'',REPLACE(REPLACE(CP.path, SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path),
				     (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-''), 
				     SUBSTRING ( REPLACE(CP.path, SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path), (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-'') ,CHARINDEX(''/'',REPLACE(CP.path, 
				     SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path), (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-''))+1, (CHARINDEX(''-'',REPLACE(CP.path, 
				     SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path), (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-'')) - CHARINDEX(''/'',REPLACE(CP.path, 
				     SUBSTRING ( CP.path ,CHARINDEX(''-'',CP.path), (CHARINDEX(''-'',CP.path,CHARINDEX(''-'',CP.path)+1))-CHARINDEX(''-'',CP.path)+1) , ''-''+ '''+cast(@idSiteDestination as nvarchar)+''' +''-''))-1)) , '''+cast(@idAccount as nvarchar)+''')) '
				+',	CP.kb,
					CP.idContentPackageType,
					CP.idSCORMPackageType,
					CP.manifest,
					CP.dtCreated,
					CP.dtModified,
					CP.isMediaUpload,
					CP.idMediaType,
					CP.contentTitle,
					CP.originalMediaFilename,
					CP.isVideoMedia3rdParty,
					CP.videoMediaEmbedCode,
					CP.enableAutoplay,
					CP.allowRewind,
					CP.allowFastForward,
					CP.allowNavigation,
					CP.allowResume,
					CP.minProgressForCompletion,
					CP.isProcessing,
					CP.isProcessed,
					CP.dtProcessed,
					TTQS.newDestinationId'
				+' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblContentPackage] CP'
				+' LEFT JOIN #QuizSurveyIdMappings TTQS on (TTQS.oldSourceId = CP.idQuizSurvey)'
				+' WHERE CP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		 
	   EXEC(@sql)
		
	   -- temporary table to hold the idContentPackage of source and destination ContentPackage tables
	   CREATE TABLE #ContentPackageIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idContentPackage of source ContentPackage table and destination ContentPackage table inside temporary table for mapping
	   SET @sql =   ' INSERT INTO #ContentPackageIdMappings'
				+' ( '
				+'	 oldSourceId,
					 newDestinationId'
				+' ) '
				+' SELECT 
					 SRC.idContentPackage, 
					 DST.idContentPackage'
				+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblContentPackage] DST'
				+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblContentPackage] SRC'
				+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				+' AND (SRC.name = DST.name)'
				+' AND (SRC.dtCreated = DST.dtCreated )'
				+' AND (SRC.dtModified = DST.dtModified )'
				+' )'
				+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)

	   EXEC(@sql)

	   
	   -- There is cross dependency between tblContentPackage and tblQuizSurvey, So we need to update tblQuizSurvey
	   -- and set idContentPackage after we have moved tblContentPackage data.
	    SET @sql =  ' UPDATE DSTQS'
	    		+ '  SET idContentPackage = TEMP.newDestinationId'
	    		+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblQuizSurvey] SRCQS'
	    		+ ' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblQuizSurvey] DSTQS'
	    		+ ' ON( SRCQS.guid = DSTQS.guid'
	    		+ ' AND (SRCQS.dtCreated = DSTQS.dtCreated)'
	    		+ ' AND SRCQS.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	    		+ ' INNER JOIN  #ContentPackageIdMappings TEMP'
	    		+ ' ON(TEMP.oldSourceId = SRCQS.idContentPackage)'
	    		+'  WHERE DSTQS.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	    		+'  AND SRCQS.idContentPackage IS NOT NULL'

		EXEC(@sql)				
		
	   
	    /*
	   Drop temporary table QuizSurveyIdMappings, temporary table QuizSurvey will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #QuizSurveyIdMappings


-- =================================================================================
-- LOGIC START -- copy tblResourceType object for both full copy and partial copy---
		
	   -- insert records from source site to destination table ResourceType
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceType]'
	   		       +' ( '
	                 +'	  idSite, 
	   		   	       resourceType,
	   		   	       isDeleted,
	   		   	       dtDeleted'
	   		       +' ) '
	   		       +' SELECT '
	   		       +	  CAST(@idSiteDestination AS NVARCHAR)
	   		       +' , RT.resourceType,
	   		   	       RT.isDeleted,
	   		   	       RT.dtDeleted 
	   		          FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceType] RT'
	                 +' WHERE RT.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
                										
	   EXEC(@sql)
	   
	   -- temporary table to hold the idResourceType of source and destination ResourceType tables
	   CREATE TABLE #ResourceTypeIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idResourceType of source ResourceType table and destination ResourceType table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #ResourceTypeIdMappings'
				  +' ( '
				  +'	   oldSourceId,
				  	   newDestinationId'
				  +' ) '
				  +' SELECT 
					   SRC.idResourceType, 
					   DST.idResourceType'
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceType] DST'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceType] SRC'
				  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SRC.resourceType = DST.resourceType)'
				  +' AND (SRC.isDeleted = DST.isDeleted)'
				  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
				  +'	)'
				  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
			
	   EXEC(@sql)					
	   		
	   -- resourceType language data move logic start
	   
	   -- insert records from source site to destination table ResourceTypeLanguage
	   SET @sql =	    ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceTypeLanguage]'
	   			  + ' ( '
	   			  + '  idSite, 
	   			       idResourceType, 
	   			  	  idLanguage, 
	   			  	  resourceType '
	   			  + ' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTRT.newDestinationId, 
	   			  	  RTL.idLanguage, 
	   			       RTL.resourceType 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceTypeLanguage] RTL '
	   			  +' LEFT JOIN #ResourceTypeIdMappings TTRT on (TTRT.oldSourceId = RTL.idResourceType)'
	   			  +' WHERE RTL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   						
	   			
	   EXEC(@sql)

	   -- resourceType language data move logic end

	   
------------------------------------------------------------------------------
    	   
	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN

	   
-- =============================================================================
-- LOGIC START -- copy tblResource object for full copy ------------------------


	   -- insert records from source site to destination table Resource
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResource]'
				  +'  ( '
			       +'  idSite, 
				      name,
				      idParentResource,
				      idOwner,
				      idResourceType,
				      description,
				      identifier,
				      isMoveable,
				      isAvailable,
				      capacity,
				      locationRoom,
				      locationBuilding,
				      locationCity,
				      locationProvince,
				      locationCountry,
				      ownerWithinSystem,
				      ownerName,
				      ownerEmail,
				      ownerContactInformation,
				      isDeleted'
                	  +'  ) '
                	  +' SELECT '
				  +	 CAST(@idSiteDestination AS NVARCHAR)
                	  +' ,R.name, '
				  +'  NULL, '             -- this null value of idParentResource will be updated from temporary ResourceIdMappings table below  
				  +'  TTU.newDestinationId,
				      TTRT.newDestinationId,
				      R.description,
				      R.identifier,
				      R.isMoveable,
				      R.isAvailable,
				      R.capacity,
				      R.locationRoom,
				      R.locationBuilding,
				      R.locationCity,
				      R.locationProvince,
				      R.locationCountry,
				      R.ownerWithinSystem,
				      R.ownerName,
				      R.ownerEmail,
				      R.ownerContactInformation,
				      R.isDeleted 
                	     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResource] R'
                	  +' LEFT JOIN #ResourceTypeIdMappings TTRT on (TTRT.oldSourceId = R.idResourceType)'
				  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = R.idOwner)'
				  +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
     										
	   EXEC(@sql)

	   -- temporary table to hold the idResource of source and destination Resource tables
	   CREATE TABLE #ResourceIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idResource of source Resource table and destination Resource table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #ResourceIdMappings'
	   		  	  +'  ( '
	   		  	  +'	   oldSourceId,
	   		  	  	   newDestinationId'
	   		  	  +'  ) '
	   		  	  +' SELECT 
				  	   SRC.idResource, 
				  	   DST.idResource'
	   		  	  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResource] DST'
	   		  	  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResource] SRC'
	   		  	  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		  	  +' AND (SRC.name = DST.name)'
	   		  	  +' AND (SRC.locationRoom = DST.locationRoom OR (SRC.locationRoom IS NULL AND DST.locationRoom IS NULL))'
	   		  	  +' AND (SRC.locationBuilding = DST.locationBuilding OR (SRC.locationBuilding IS NULL AND DST.locationBuilding IS NULL))'
	   		  	  +' AND (SRC.locationCity = DST.locationCity OR (SRC.locationCity IS NULL AND DST.locationCity IS NULL))'
	   		  	  +' AND (SRC.ownerEmail = DST.ownerEmail OR (SRC.ownerEmail IS NULL AND DST.ownerEmail IS NULL))'
	   		  	  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   		  	  +')'
	   		  	  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	  EXEC(@sql)
	   
	   -- update idParentResource from temp table resource
	   SET @sql =	   ' UPDATE DSTR'
	   		  	  +'  SET idParentResource = TEMP.newDestinationId'
	   		  	  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResource] SRCR'
	   		  	  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResource] DSTR'
	   		  	  +' ON(SRCR.name = DSTR.name AND (SRCR.ownerEmail = DSTR.ownerEmail OR (SRCR.ownerEmail IS NULL AND DSTR.ownerEmail IS NULL)) 
	   		  	     AND (SRCR.locationRoom = DSTR.locationRoom OR (SRCR.locationRoom IS NULL AND DSTR.locationRoom IS NULL))
	   		  	     AND (SRCR.locationBuilding = DSTR.locationBuilding OR (SRCR.locationBuilding IS NULL AND DSTR.locationBuilding IS NULL)) 
	   		  	     AND (SRCR.locationCity = DSTR.locationCity OR (SRCR.locationCity IS NULL AND DSTR.locationCity IS NULL))
	   		  	     AND SRCR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) +')'
	   		  	  +' INNER JOIN  #ResourceIdMappings TEMP'
	   		  	  +' ON(TEMP.oldSourceId = SRCR.idParentResource)'
	   		  	  +' WHERE DSTR.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		  	  +' AND SRCR.idParentResource IS NOT NULL'
	   
	   EXEC(@sql)	
											
						
	   -- resource language data move logic start
	   
	   -- insert records from source site to destination table ResourceLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceLanguage]'
	   			 + ' ( '
	   			 + '	  idSite, 
	   				  idResource, 
	   				  idLanguage, 
	   				  name, 
	   				  description'
	   			 + ' ) '
	   			 +' SELECT '
				 +	  CAST(@idSiteDestination AS NVARCHAR)
	   			 +' ,  TTR.newDestinationId, 
	   			 	  RL.idLanguage, 
	   			 	  RL.name, 
	   			 	  RL.description 
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceLanguage] RL'
	   			 +' LEFT JOIN #ResourceIdMappings TTR on (TTR.oldSourceId = RL.idResource)'
	   			 +' WHERE RL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 			
	   			
	   EXEC(@sql)

	   -- resource language data move logic end


	   END
----------------------------------------------------------------------------------
--full copy end


	   /*
	   Drop temporary table ResourceTypeIdMappings, temporary table ResourceType will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #ResourceTypeIdMappings


-- ====================================================================================
-- LOGIC START -- copy tblStandUpTraining object for both full copy and partial copy ---


	   -- insert records from source site to destination table StandUpTraining
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTraining]'
	   			  +'  ( '
	   			  +'    idSite, 
	   			  	   title,
	   			  	   [description],
	   			  	   isStandaloneEnroll,
	   			  	   isRestrictedEnroll,
	   			  	   isRestrictedDrop,
	   			  	   searchTags, 
					   isDeleted,
					   dtDeleted,
					   avatar '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  ST.title,
	   			  	   ST.[description],
	   			  	   ST.isStandaloneEnroll,
	   			  	   ST.isRestrictedEnroll,
	   			  	   ST.isRestrictedDrop,
					   ST.searchTags, 
					   ST.isDeleted,
					   ST.dtDeleted,
					   ST.avatar '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTraining] ST '
	   			  +' WHERE ST.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   					
			 						
	   -- temporary table to hold the idStandUpTraining of source and destination StandUpTraining tables
	   CREATE TABLE #StandUpTrainingIdMappings 
	   (
		  oldSourceId INT NOT NULL, 
		  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idStandUpTraining of source StandUpTraining table and destination StandUpTraining table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #StandUpTrainingIdMappings'
	   			  +'  ( '
	   			  +'   oldSourceId,
	   			  	  newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					  SRC.idStandUpTraining, 
					  DST.idStandUpTraining'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTraining] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTraining] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.title = DST.title)'
	   			  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   			  +' AND (SRC.description = DST.description OR (SRC.description IS NULL AND DST.description IS NULL))'
	   			  +')'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)
	   
	   	
	   -- standUpTraining language data move logic start
	   				
	   -- insert records from source site to destination table StandUpTrainingLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingLanguage]'
	   		      + ' ( '
	   		      + '   idSite, 
	   			       idStandUpTraining, 
	   			       idLanguage, 
	   			       title, 
	   			       Description, 
	   			       searchTags'
	   		      + ' ) '
	   		      +' SELECT '
				 +	  CAST(@idSiteDestination AS NVARCHAR)
	   		      +' ,  TTST.newDestinationId, 
	   		       	  STL.idLanguage, 
	   		       	  STL.title, 
	   		      	  STL.Description, 
	   		       	  STL.searchTags 
	   		         FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingLanguage] STL '
	   		      +' LEFT JOIN #StandUpTrainingIdMappings TTST on (TTST.oldSourceId = STL.idStandUpTraining )'
	   		      +' WHERE STL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   
	   EXEC(@sql)

	   -- standUpTraining language data move logic end

	   -- insert temporary table StandUpTrainingIdMappings data, in order to return it for moving file system object
		INSERT INTO #ObjectMapping
		([oldId],[newId],objectName)
		SELECT oldSourceId, newDestinationId, 'standuptraining' FROM #StandUpTrainingIdMappings

   
------------------------------------------------------------------------------

   	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN

-- =============================================================================
-- LOGIC START -- copy tblStandUpTrainingInstance object for full copy ---------


	   -- insert records from source site to destination table StandUpTrainingInstance
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingInstance]'
	   			  +'  ( '
	   			  +'	   idSite,
	   			  	   idStandupTraining,
	   			  	   title,
	   			  	   [description],
	   			  	   seats,
	   			  	   waitingSeats,
	   			  	   [type],
	   			  	   urlRegistration,
	   			  	   urlAttend,
	   			  	   city,
	   			  	   province,
	   			  	   locationDescription,
	   			  	   idInstructor,
					   isDeleted,
					   dtDeleted '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTST.newDestinationId,
	   			  	   STI.title,
	   			  	   STI.[description],
	   			  	   STI.seats,
	   			  	   STI.waitingSeats,
	   			  	   STI.[type],
	   			  	   STI.urlRegistration,
	   			  	   STI.urlAttend,
	   			  	   STI.city,
	   			  	   STI.province,
	   			  	   STI.locationDescription,
	   			  	   TTU.newDestinationId, 
					   STI.isDeleted,
					   STI.dtDeleted '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingInstance] STI'
	   			  +' LEFT JOIN #StandUpTrainingIdMappings TTST on (TTST.oldSourceId = STI.idStandUpTraining )'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = STI.idInstructor )'
	   			  +' WHERE STI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   EXEC(@sql)
	   					
	   -- temporary table to hold the idStandUpTrainingInstance of source and destination StandUpTrainingInstance tables
	   CREATE TABLE #StandUpTrainingInstanceIdMappings
	   (
	   	oldSourceId INT NOT NULL, 
	   	newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idStandUpTrainingInstance of source StandUpTrainingInstance table and destination StandUpTrainingInstance table inside temporary table for mapping
	   SET @sql =	     ' INSERT INTO #StandUpTrainingInstanceIdMappings'
				   + ' ( '
				   + '	 oldSourceId,
				   		 newDestinationId'
				   + ' ) '
				   +' SELECT 
				   		 SRC.idStandUpTrainingInstance, 
				   		 DST.idStandUpTrainingInstance '
				   +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingInstance] DST'
				   +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingInstance] SRC'
				   +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				   +' AND (SRC.title = DST.title)'
				   +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
				   +' AND (SRC.description = DST.description OR (SRC.description IS NULL AND DST.description IS NULL))'
				   +' )'
				   +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
				
	   EXEC(@sql)

		-- standUpTrainingInstance language data move logic start
						
		-- insert records from source site to destination table StandUpTrainingInstanceLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingInstanceLanguage]'
				  +' ( '
				  +'    idSite,
				  	   idStandUpTrainingInstance,
				  	   idLanguage,
				  	   title,
				  	   [description],
				  	   locationDescription'
				  +' ) '
				  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
				  +' ,  TTSTI.newDestinationId,
				  	   STIL.idLanguage,
				  	   STIL.title,
				  	   STIL.[description],
				  	   STIL.locationDescription 
				     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingInstanceLanguage] STIL'
				  +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = STIL.idStandUpTrainingInstance )'
				  +' WHERE STIL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				
	   EXEC(@sql)

		-- standUpTrainingInstance language data move logic end

		
-- =============================================================================
-- LOGIC START -- copy tblResourceToObjectLink object for full copy ------------


	   -- insert records from source site to destination table ResourceToObjectLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblResourceToObjectLink]'
	   			  +' ( '
	   			  +'    idSite,
				  	   idResource,
				  	   idObject,
				  	   objectType,
				  	   dtStart,
				  	   dtEnd,
				  	   isOutsideUse,
				  	   isMaintenance'
	   			  +' ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTR.newDestinationId,'
	   			  +'	   CASE RTOL.objectType
				  	     WHEN ''standuptraining'' THEN
				  	   	 TTSTI.newDestinationId
				  	     ELSE
				  	   	 0
					   END'
				  +' ,  RTOL.objectType,
				  	   RTOL.dtStart,
				  	   RTOL.dtEnd,
				  	   RTOL.isOutsideUse,
				  	   RTOL.isMaintenance 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblResourceToObjectLink] RTOL'
	   			  +' LEFT JOIN #ResourceIdMappings TTR on (TTR.oldSourceId = RTOL.idResource)'
	   			  +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = RTOL.idObject)'
				  +' WHERE RTOL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   				
	   EXEC(@sql)


	END	

------------------------------------------------------------------------------
-- full copy end	

	
-- ==========================================================================================
-- LOGIC START -- copy tblLessonToContentLink object for both full copy and partial copy ----


	   -- insert records from source site to destination table LessonToContentLink
	   SET @sql =   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLessonToContentLink]'
				+' ( '
				+'    idSite,
					 idLesson,
					 idContentType,
					 idAssignmentDocumentType,
					 assignmentResourcePath,
					 allowSupervisorsAsProctor,
					 allowCourseExpertsAsProctor,
					 idObject'
				+' ) '
				+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
				+',	 TTL.newDestinationId,
					 LTCL.idContentType,
					 LTCL.idAssignmentDocumentType,
					 LTCL.assignmentResourcePath,
					 LTCL.allowSupervisorsAsProctor,
					 LTCL.allowCourseExpertsAsProctor,
					 CASE LTCL.idContentType
					   WHEN 1 THEN
						  TTCP.newDestinationId
					   WHEN 2 THEN
						  TTST.newDestinationId
					   WHEN 3 THEN 
						  NULL
					   WHEN 4 THEN
						  TTCP.newDestinationId
					   END '
	   			    +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLessonToContentLink] LTCL'
	   			    +' LEFT JOIN #LessonIdMappings TTL on (TTL.oldSourceId = LTCL.idLesson)'
	   			    +' LEFT JOIN #ContentPackageIdMappings TTCP on (TTCP.oldSourceId = LTCL.idObject)'
				    +' LEFT JOIN #StandUpTrainingIdMappings TTST on (TTST.oldSourceId = LTCL.idObject)'
				    +' WHERE LTCL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		  
	   EXEC(@sql)


	   /*
	   Drop temporary table ContentPackageIdMappings, temporary table ContentPackage will be dorp in both conditions full copy and partial copy 
	   */	   
	   DROP TABLE #ContentPackageIdMappings


-- =============================================================================
-- LOGIC START -- copy tblRole object for both full copy and partial copy -------


	   -- insert records from source site to destination table Role
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRole]'
	   			 +' ( '
	   			 +'	 idSite,
	   				 name'
	   			 +' ) '
	   			 +' SELECT '
				 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , R.name
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRole] R'
	   			 +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		      +' AND R.idRole != 1' -- idRole 1 is already inserted while creating db structure.
	   			
	   EXEC(@sql)
	   
	     -- temporary table to hold the idRole of source and destination Role tables
	   	CREATE TABLE #RoleIdMappings
	   	(
	   		oldSourceId INT NOT NULL, 
	   		newDestinationId INT NOT NULL 	   
	   	)
	   
	   	-- initially insert row for temporary table role record
	   	INSERT INTO #RoleIdMappings(
	   			oldSourceId,
	   			newDestinationId
	   		)
	   		VALUES
	   		(
	   			1,
	   			1
	   		)

	   -- inserting idRole of source Role table and destination Role table inside temporary table for mapping
	   SET @sql =	  'INSERT INTO #RoleIdMappings'
	   			 +' ( '
	   			 +'	   oldSourceId,
	   			 	   newDestinationId'
	   			 +' ) '
	   			 +' SELECT 
					   SRC.idRole,
					   DST.idRole '
	   			 +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRole] DST'
	   			 +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRole] SRC'
	   			 +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			 +' AND (SRC.name = DST.name)'
	   			 +'  )'
	   			 +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			 +' AND DST.idRole != 1' -- idRole 1 is already inserted while creating db structure.
	   
	   EXEC(@sql)
	   			

	   -- role language data move logic start
	   
	   -- insert records from source site to destination table RoleLanguage
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRoleLanguage]'
	   			+ ' ( '
	   			+ ' idSite,
	   			    idRole,
	   			    idLanguage,
	   			    name'
	   			+ ' ) '
	   			+' SELECT '
				+    CAST(@idSiteDestination AS NVARCHAR)
	   			+' , TTR.newDestinationId, 
	   				RL.idLanguage, 
	   				RL.name
	   			   FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRoleLanguage] RL'
	   			+' LEFT JOIN #RoleIdMappings TTR on (TTR.oldSourceId = RL.idRole)'
	   			+' WHERE RL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			+' AND RL.idRole != 1' -- idRole 1 is already inserted while creating db structure.
	   						
	   			
	   EXEC(@sql)

	   -- role language data move logic end
	   

-- ==========================================================================================
-- LOGIC START -- copy tblRoleToPermissionLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table RoleToPermissionLink
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRoleToPermissionLink]'
	   			 +' ( '
	   			 +'  idSite,
	   			     idRole,
	   		          idPermission,
	   		          scope'
	   			 +'  ) '
	   			 +' SELECT '
				 +	 CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTR.newDestinationId,
	   		           RTPL.idPermission,
	   		           RTPL.scope
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRoleToPermissionLink] RTPL'
	   			 +' LEFT JOIN #RoleIdMappings TTR on (TTR.oldSourceId = RTPL.idRole )'
	   		      +' WHERE RTPL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		      +' AND RTPL.idRole != 1' -- idRole 1 is already inserted while creating db structure.
	   		
	   EXEC(@sql)
	   


-- =================================================================================
-- LOGIC START -- copy tblRuleset object for both full copy and partial copy -------


	   -- insert records from source site to destination table Ruleset
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleset]'
	     		  +' ( '
	     		  +'	  [idSite],
	   			  	  [isAny],
	   			  	  [label]'
	     		  +'  ) '
	     		  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	     		  +' ,  R.[isAny],
	   			  	   R.[label] 
	     		     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleset] R'
	     		  +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	     
	   EXEC(@sql)
		  						
	   -- temporary table to hold the idRuleset of source and destination Ruleset tables
	   CREATE TABLE #RulesetIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idRuleset of source Ruleset table and destination Ruleset table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #RulesetIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId '
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idRuleset, 
					   DST.idRuleset '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleset] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleset] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.label = DST.label)'
	   			  +' AND (SRC.isAny = DST.isAny)'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)
	   
	   -- ruleset language data move logic start

	   -- insert records from source site to destination table RulesetLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRulesetLanguage]'
	   			  +' ( '
	   			  +'   idSite, 
	   			  	  idRuleset, 
	   			  	  idLanguage, 
	   			  	  label'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTR.newDestinationId, 
	   			  	  RL.idLanguage, 
	   			  	  RL.label 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRulesetLanguage] RL'
	   			  +' LEFT JOIN #RulesetIdMappings TTR on (TTR.oldSourceId = RL.idRuleset )'
	   			  +' WHERE RL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)
		     	              
	   -- ruleset language data move logic end


-- =============================================================================
-- LOGIC START -- copy tblRule object for both full copy and partial copy -------


	   -- insert records from source site to destination table Rule
	   SET @sql =	   'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRule]'
	   			  +' ( '
	   			  +'   idSite ,
	   				  idRuleSet ,
	   				  userField ,
	   				  operator ,
	   				  textValue ,
	   				  dateValue ,
	   				  numValue ,
	   				  bitValue'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTR.newDestinationId, 
	   			  	  R.userField ,
	   				  R.operator ,
	   				  R.textValue ,
	   				  R.dateValue ,
	   				  R.numValue ,
	   				  R.bitValue 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRule] R'
	   			  +' LEFT JOIN #RulesetIdMappings TTR on (TTR.oldSourceId = R.idRuleSet )'
	   			  +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   


-- ========================================================================================
-- LOGIC START -- copy tblRuleSetToGroupLink object for both full copy and partial copy ----


	   -- insert records from source site to destination table RuleSetToGroupLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetToGroupLink]'
	   			  +' ( '
	   			  +'	   idSite,
	   			  	   idRuleSet,
	   			  	   idGroup'
	   			  +' ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTR.newDestinationId, 
	   			  	   TTG.newDestinationId 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetToGroupLink] RTGL'
	   			  +' LEFT JOIN #RuleSetIdMappings TTR on (TTR.oldSourceId = RTGL.idRuleSet )'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = RTGL.idGroup )'
	   			  +' WHERE RTGL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   


-- =======================================================================================
-- LOGIC START -- copy tblRuleSetToRoleLink object for both full copy and partial copy ----

	   -- insert records from source site to destination table RuleSetToRoleLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetToRoleLink]'
	   			  +' ( '
	   			  +'  idSite,
	   				 idRuleSet,
	   				 idRole'
	   			  +' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTR.newDestinationId, 
	   			  	  TTRole.newDestinationId 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetToRoleLink] RTRL'
	   			  +' LEFT JOIN #RuleSetIdMappings TTR on (TTR.oldSourceId = RTRL.idRuleSet )'
	   			  +' LEFT JOIN #RoleIdMappings TTRole on (TTRole.oldSourceId = RTRL.idRole )'
	   			  +' WHERE RTRL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)


------------------------------------------------------------------------------
	   
	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN



	    /*
	   Drop temporary table ResourceIdMappings
	   */
	   DROP TABLE #ResourceIdMappings


-- ===================================================================================
-- LOGIC START -- copy tblStandUpTrainingInstanceMeetingTime object for full copy ----


	   -- insert records from source site to destination table StandUpTrainingInstanceMeetingTime
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandUpTrainingInstanceMeetingTime]'
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idStandUpTrainingInstance,
	   			  	  dtStart,
	   			  	  dtEnd,
	   			  	  [minutes],
	   			  	  idTimezone'
	   			  +' ) '
	   			  +' SELECT '
	   			  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTSTI.newDestinationId,
	   			  	  STIMT.dtStart,
	   			  	  STIMT.dtEnd,
	   			  	  STIMT.[minutes],
	   			  	  STIMT.idTimezone 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandUpTrainingInstanceMeetingTime] STIMT'
	   			  +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = STIMT.idStandUpTrainingInstance )'
	   			  +' WHERE STIMT.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)


-- ==================================================================================
-- LOGIC START -- copy tblStandupTrainingInstanceToUserLink object for full copy ----


	   -- insert records from source site to destination table StandupTrainingInstanceToUserLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblStandupTrainingInstanceToUserLink]'
	   			  +' ( '
	   			  +'   idUser,
	   			  	  idStandupTrainingInstance,
	   			  	  completionStatus,
	   			  	  successStatus,
	   			  	  score,
	   			  	  isWaitingList,
	   			  	  dtCompleted'
	   			  +'  ) '
	   			  +' SELECT '
	   			  +'   TTU.newDestinationId,
	   			  	  TTSTI.newDestinationId,
	   			  	  STITUL.completionStatus,
	   			  	  STITUL.successStatus,
	   			  	  STITUL.score,
	   			  	  STITUL.isWaitingList,
	   			  	  STITUL.dtCompleted 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblStandupTrainingInstanceToUserLink] STITUL'
	   			  +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U'
	   			  +' ON(STITUL.idUser = U.idUser AND U.idSite = '+ CAST(@idSiteSource as nvarchar)+ ')'
	   			  +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = STITUL.idStandUpTrainingInstance)'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = STITUL.idUser)'
	   			  +' WHERE U.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   EXEC(@sql)


-- =============================================================================
-- LOGIC START -- copy tblUserToGroupLink object for full copy -----------------


	   -- insert records from source site to destination table UserToGroupLink
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUserToGroupLink]'
	   			 +' ( '
	   			 +'   idSite,
	   			 	 idUser,
	   			 	 idGroup,
	   			 	 idRuleSet,
	   			 	 created,
	   			 	 selfJoined'
	   			 +'  ) '
	   			 +' SELECT '
				 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTU.newDestinationId,
	   			 	 TTG.newDestinationId,
	   			 	 TTRS.newDestinationId,
	   			 	 UTGL.created,
	   			 	 UTGL.selfJoined
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUserToGroupLink] UTGL'
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = UTGL.idUser )'
	   			 +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = UTGL.idGroup )'
	   			 +' LEFT JOIN #RulesetIdMappings TTRS on (TTRS.oldSourceId = UTGL.idRuleset )'
	   			 +' WHERE UTGL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblUserToRoleLink object for full copy ------------------


	   -- insert records from source site to destination table UserToRoleLink
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUserToRoleLink]'
	   			 +' ( '
	   			 +'   idSite,
	   			      idUser,
	   			      idRole,
	   			      idRuleSet,
	   			      created'
	   			 +'  ) '
	   			 +' SELECT '
				 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTU.newDestinationId,
	   			      TTR.newDestinationId,
	   			      TTRS.newDestinationId,
	   			      UTRL.created
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUserToRoleLink] UTRL'
	   			 +' LEFT JOIN #RoleIdMappings TTR on (TTR.oldSourceId = UTRL.idRole )'
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = UTRL.idUser )'
	   			 +' LEFT JOIN #RulesetIdMappings TTRS on (TTRS.oldSourceId = UTRL.idRuleset )'
	   			 +' WHERE UTRL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblGroupToRoleLink object for full copy -----------------


	   -- insert records from source site to destination table GroupToRoleLink
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupToRoleLink]'
	   			 +' ( '
	   			 +'   idSite,
	   				 idGroup,
	   				 idRole,
	   				 idRuleSet,
	   				 created '
	   			 +' ) '
	   			 +' SELECT '
				 +	  CAST(@idSiteDestination AS NVARCHAR)
	   			 +' ,  TTG.newDestinationId,
	   				  TTR.newDestinationId,
	   				  TTRS.newDestinationId,
	   				  GTRL.created
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupToRoleLink] GTRL'
	   			 +' LEFT JOIN #RoleIdMappings TTR on (TTR.oldSourceId = GTRL.idRole )'
	   			 +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GTRL.idGroup )'
	   			 +' LEFT JOIN #RulesetIdMappings TTRS on (TTRS.oldSourceId = GTRL.idRuleset )'
	   			 +' WHERE GTRL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)

    END

------------------------------------------------------------------------------
-- full copy end


	    /*
	   Drop temporary Table RoleIdMappings, temporary table Role will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #RoleIdMappings

-- ===================================================================================
-- LOGIC START -- copy tblLearningPath object for both full copy and partial copy-----


	   -- insert records from source site to destination table LearningPath
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPath]'
	   			 +' ( '
	   			 +'    idSite,'
	   			 +'	  name,'
	   			 +'	  shortDescription,'
	   			 +'	  longDescription,'
	   			 +'	  cost,'
	   			 +'	  isPublished,'
	   			 +'	  isClosed,'
	   			 +'	  forceCompletionInOrder,'
	   			 +'	  dtCreated,'
	   			 +'	  dtModified,'
	   			 +'	  isDeleted,'
	   			 +'	  dtDeleted,'
	   			 +'	  avatar,'
	   			 +'	  searchTags'
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +'	, LP.name,'
	   			 +'	  LP.shortDescription,'
	   			 +'	  LP.longDescription,'
	   			 +'	  LP.cost,'
	   			 +'	  LP.isPublished,'
	   			 +'	  LP.isClosed,'
	   			 +'	  LP.forceCompletionInOrder,'
	   			 +'	  LP.dtCreated,'
	   			 +'	  LP.dtModified,'
	   			 +'	  LP.isDeleted,'
	   			 +'	  LP.dtDeleted,'
	   			 +'	  LP.avatar,'
	   			 +'	  LP.searchTags '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPath] LP'
	   			 +' WHERE LP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)
	   		
	   -- temporary table to hold the idLearningPath of source and destination LearningPath tables
	   CREATE TABLE #LearningPathIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idLearningPath of source LearningPath table and destination LearningPath table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #LearningPathIdMappings '
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SLP.idLearningPath, 
					   DLP.idLearningPath '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPath] DLP'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPath] SLP'
	   			  +' ON (SLP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SLP.dtCreated = DLP.dtCreated) '
	   			  +' AND (SLP.dtModified = DLP.dtModified OR (SLP.dtModified IS NULL AND DLP.dtModified IS NULL))'					
	   			  +' AND (SLP.dtDeleted = DLP.dtDeleted OR (SLP.dtDeleted IS NULL AND DLP.dtDeleted IS NULL))' 
	   			  +' )'
	   			  +' WHERE DLP.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)	 
	   	    	
	   -- learningPathLanguage data move logic start
	   
	   -- insert records from source site to destination table LearningPathLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathLanguage]'
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idLearningPath,'
	   			  +'	  idLanguage,'
	   			  +'	  name,'
	   			  +'	  shortDescription,'
	   			  +'	  longDescription,'
	   			  +'	  searchTags'
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +'	,  TTLP.newDestinationId, '
	   			  +'	   LPL.idLanguage, '
	   			  +'	   LPL.name, '
	   			  +'	   LPL.shortDescription, '
	   			  +'	   LPL.longDescription, '
	   			  +'	   LPL.searchTags '
				  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathLanguage] LPL '
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = LPL.idLearningPath ) '
	   			  +' WHERE LPL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)
	   
	   -- learningPathLanguage data move logic end

	   -- insert temp table data for group in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'learningPaths' FROM #LearningPathIdMappings	
	   



-- ================================================================================
-- LOGIC START -- copy tblCoupenCode object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCode
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCode]'
	   			 +' ( '
	   			 +'	   idSite,'
	   			 +'	   code,'
	   			 +'	   comments,'
	   			 +'	   usesAllowed,'
	   			 +'	   discount,'
	   			 +'	   discountType,'
	   			 +'	   forCourse,'
	   			 +'	   forCatalog,'
	   			 +'	   forLearningPath,'
	   			 +'	   dtStart,'
	   			 +'	   dtEnd, '
	   			 +'	   isSingleUsePerUser,'
	   			 +'	   isDeleted,'
	   			 +'	   dtDeleted,'
	   			 +'	   forStandupTraining '
	   			 +' ) '
	   			 +' SELECT '
				 +	   CAST(@idSiteDestination AS NVARCHAR)
	   			 +',	   CC.code,'
	   			 +'	   CC.comments,'
	   			 +'	   CC.usesAllowed,'
	   			 +'	   CC.discount,'
	   			 +'	   CC.discountType ,'
	   			 +'	   TTC.newDestinationId,'
	   			 +'	   TTCat.newDestinationId,'
	   			 +'	   TTLP.newDestinationId,'
	   			 +'	   CC.dtStart,'
	   			 +'	   CC.dtEnd,'
	   			 +'	   CC.isSingleUsePerUser,'
	   			 +'	   CC.isDeleted,'
	   			 +'	   CC.dtDeleted, '
	   			 +'	   TTST.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC '
				 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CC.forCourse ) '
				 +' LEFT JOIN #CatalogIdMappings TTCat on (TTCat.oldSourceId = CC.forCatalog ) '
				 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = CC.forLearningPath )'
				 +' LEFT JOIN #StandUpTrainingIdMappings TTST on (TTST.oldSourceId = CC.forStandupTraining )'
	   			 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC(@sql)
	   


	   -- temporary table to hold the idCouponCode of source and destination CouponCode tables
	   CREATE TABLE #CouponCodeIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idCouponCode of source CouponCode table and destination CouponCode table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CouponCodeIdMappings '
				  +' ( '
				  +'	   oldSourceId,
				  	   newDestinationId'
				  +' ) '
				  +' SELECT 
					   SCC.idCouponCode, 
					   DCC.idCouponCode '
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCode] DCC'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] SCC'
				  +' ON (SCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SCC.code = DCC.code )'
				  +' AND (SCC.dtDeleted = DCC.dtDeleted OR (SCC.dtDeleted IS NULL AND DCC.dtDeleted IS NULL))'
				  +' AND (SCC.dtStart = DCC.dtStart OR (SCC.dtStart IS NULL AND DCC.dtStart IS NULL))'
				  +' AND (SCC.dtEnd = DCC.dtEnd OR (SCC.dtEnd IS NULL AND DCC.dtEnd IS NULL))'					
				  +' )'
				  +' WHERE DCC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
	   EXEC(@sql)	


-- =====================================================================================================
-- LOGIC START -- copy tblCouponCodeToStandupTrainingLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCodeToStandupTrainingLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCodeToStandupTrainingLink]'
	   			 +' ( '
	   			 +'	 idCouponCode,' 
	   			 +'	 idStandupTraining '
	   			 +' ) '
	   			 +' SELECT '
	   			 +'   TTCC.newDestinationId,'
	   			 +'   TTST.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCodeToStandupTrainingLink] CCSTL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC ON CC.idCouponCode= CCSTL.idCouponCode '
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC ON (TTCC.oldSourceId = CCSTL.idCouponCode  )'
	   			 +' LEFT JOIN #StandUpTrainingIdMappings TTST ON (TTST.oldSourceId = CCSTL.idStandupTraining )'
	   			 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   	
	   EXEC(@sql)

	     

-- =============================================================================================
-- LOGIC START -- copy tblCouponCodeToCatalogLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCodeToCatalogLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCodeToCatalogLink]'
	   			 +' ( '
	   			 +'	 idCouponCode,' 
	   			 +'	 idCatalog'
	   			 +' ) '
	   			 +' SELECT '
	   			 +'   TTCC.newDestinationId,'
	   			 +'   TTC.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCodeToCatalogLink] CCTCL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC ON CC.idCouponCode= CCTCL.idCouponCode '
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC ON (TTCC.oldSourceId = CCTCL.idCouponCode  )'
	   			 +' LEFT JOIN #CatalogIdMappings TTC ON (TTC.oldSourceId = CCTCL.idCatalog )'
	   			 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)



-- ==================================================================================================
-- LOGIC START -- copy tblCouponCodeToLearningPathLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCodeToLearningPathLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCodeToLearningPathLink]'
	   			 +'  ( '
	   			 +'   idCouponCode,' 
	   			 +'   idLearningPath'
	   			 +'   ) '
	   			 +' SELECT '
	   			 +'   TTCC.newDestinationId,'
	   			 +'	 TTLP.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCodeToLearningPathLink] CCLPL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC ON CC.idCouponCode= CCLPL.idCouponCode '
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC on (TTCC.oldSourceId = CCLPL.idCouponCode)'
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = CCLPL.idLearningPath)'
	   		 	 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- ============================================================================================
-- LOGIC START -- copy tblCouponCodeToCourseLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CouponCodeToCourseLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCouponCodeToCourseLink]'
	   			 +' ( '
	   			 +'	 idCouponCode,' 
	   			 +'	 idCourse '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +'	 TTCC.newDestinationId,'
	   			 +'	 TTC.newDestinationId ' 
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCodeToCourseLink] CCTCL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCouponCode] CC ON CC.idCouponCode= CCTCL.idCouponCode '
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC on (TTCC.oldSourceId = CCTCL.idCouponCode )'
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CCTCL.idCourse )'
	   		 	 +' WHERE CC.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 

	   EXEC(@sql)


	  /*
	   Drop temporary table StandUpTrainingIdMappings, temporary table Catalog will be dorp in both conditions full copy and partial copy
	   */	   
	   DROP TABLE #StandUpTrainingIdMappings 

	   /*
	   Drop temporary table CatalogIdMappings, temporary table Catalog will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #CatalogIdMappings


-- ==============================================================================================
-- LOGIC START -- copy tblLearningPathToCourseLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table LearningPathToCourseLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathToCourseLink]'
	   			 +' ( '
	   		      +'	 idSite,'
	   			 +'	 idLearningPath,'
	   			 +'	 idCourse,'
	   			 +'	 [order]'
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTLP.newDestinationId, '
	   			 +'   TTC.newDestinationId,'	
	   			 +'   LPCL.[order]'									
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathToCourseLink] LPCL '
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = LPCL.idLearningPath )'
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = LPCL.idCourse )'
	   			 +' WHERE LPCL.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)


-- ===================================================================================================
-- LOGIC START -- copy tblRuleSetLearningPathEnrollment object for both full copy and partial copy ---


	   -- insert records from source site to destination table RuleSetLearningPathEnrollment
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollment]'
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idLearningPath,'   
	   			  +'	  idTimezone,'
	   			  +'	  priority,'
	   			  +'	  label,'
	   			  +'	  dtStart,'
	   			  +'	  dtEnd,'
	   			  +'	  delayInterval,'
	   			  +'	  delayTimeframe,'
	   			  +'	  dueInterval,'
	   			  +'	  dueTimeframe'
	   			  +'  ) '
	   			  +' SELECT '
				  +    CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTLP.newDestinationId,'
	   			  +'	  RSLPE.idTimezone,'
	   			  +'	  RSLPE.priority,'
	   			  +'	  RSLPE.label,'
	   			  +'	  RSLPE.dtStart,'
	   			  +'	  RSLPE.dtEnd,'
	   			  +'	  RSLPE.delayInterval,'
	   			  +'	  RSLPE.delayTimeframe,'
	   			  +'	  RSLPE.dueInterval,'
	   			  +'	  RSLPE.dueTimeframe'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollment] RSLPE'
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = RSLPE.idLearningPath )'
	   			  +' WHERE RSLPE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   
	   EXEC(@sql)

	   -- temporary table to hold the idRuleSetLearningPathEnrollment of source and destination RuleSetLearningPathEnrollment tables
	   CREATE TABLE #RuleSetLearningPathEnrollmentIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idRuleSetLearningPathEnrollment of source RuleSetLearningPathEnrollment table and destination RuleSetLearningPathEnrollment table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #RuleSetLearningPathEnrollmentIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					  SCC.idRuleSetLearningPathEnrollment, 
					  DCC.idRuleSetLearningPathEnrollment '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollment] DCC'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollment] SCC'
	   			  +' ON (SCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SCC.label = DCC.label)' 	
	   		       +' AND (SCC.dtStart = DCC.dtStart)'		
	   		       +' AND (SCC.dtEnd = DCC.dtEnd OR (SCC.dtEnd IS NULL AND DCC.dtEnd IS NULL))'		
	   			  +' )'
	   			  +' WHERE DCC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)	
		
	   -- ruleSetLearningPathEnrollmentLanguage data move logic start
	   
	   -- insert records from source site to destination table RuleSetLearningPathEnrollmentLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollmentLanguage]'
	   			  +' ( '
	   			  +'	   idSite,'
	   			  +'	   idRuleSetLearningPathEnrollment,'
	   			  +'	   idLanguage,'
	   			  +'	   label '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTRSLPE.newDestinationId,'
	   			  +'	   RSLPEL.idLanguage,'
	   			  +'	   RSLPEL.label '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetLearningPathEnrollmentLanguage] RSLPEL'
	   			  +' LEFT JOIN #RuleSetLearningPathEnrollmentIdMappings TTRSLPE on (TTRSLPE.oldSourceId = RSLPEL.idRuleSetLearningPathEnrollment )'
	   			  +' WHERE RSLPEL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)
	   
	   -- ruleSetLearningPathEnrollmentLanguage data move logic end

------------------------------------------------------------------------------

	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN		


-- =============================================================================
-- LOGIC START -- copy tblLearningPathEnrollment object for full copy ----------


	   -- insert records from source site to destination table LearningPathEnrollment
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathEnrollment]'
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idLearningPath,'  
	   			  +'	  idUser,'    
	   			  +'	  idRuleSetLearningPathEnrollment,'   				
	   			  +'	  idTimezone,' 
	   			  +'	  title,' 
	   			  +'	  dtStart,' 
	   			  +'	  dtDue,' 
	   			  +'	  dtCompleted,' 
	   			  +'	  dtCreated,' 
	   			  +'	  dueInterval,' 
	   			  +'	  dueTimeframe' 
	   			  +'  ) '
	   			  +' SELECT '
				  +     CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTLP.newDestinationId,'
	   			  +'	   TTU.newDestinationId,'
	   			  +'	   TTRSLPE.newDestinationId,'
	   			  +'	   LPE.idTimezone,'
	   			  +'	   LPE.title,'
	   			  +'	   LPE.dtStart,'
	   			  +'	   LPE.dtDue,'
	   			  +'	   LPE.dtCompleted,'
	   			  +'	   LPE.dtCreated,'
	   			  +'	   LPE.dueInterval,'
	   			  +'	   LPE.dueTimeframe'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathEnrollment] LPE'
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = LPE.idLearningPath )'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = LPE.idUser )'
	   			  +' LEFT JOIN #RuleSetLearningPathEnrollmentIdMappings TTRSLPE on (TTRSLPE.oldSourceId = LPE.idRuleSetLearningPathEnrollment )'
	   			  +' WHERE LPE.idSite ='+ CAST(@idSiteSource AS NVARCHAR)
	   
	   EXEC(@sql)
	   
	   -- temporary table to hold the idLearningPathEnrollment of source and destination LearningPathEnrollment tables
	   CREATE TABLE #LearningPathEnrollmentIdMappings
	   (
	   	oldSourceId INT NOT NULL, 
	   	newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idLearningPathEnrollment of source and destination LearningPathEnrollment tables inside temp table
	   SET @sql =	   ' INSERT INTO #LearningPathEnrollmentIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
				  	  SLPE.idLearningPathEnrollment, 
				  	  DLPE.idLearningPathEnrollment '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathEnrollment] DLPE'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathEnrollment] SLPE'
	   			  +' ON (SLPE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SLPE.dtStart = DLPE.dtStart )'
	   		       +' AND (SLPE.dtCompleted = DLPE.dtCompleted OR (SLPE.dtCompleted IS NULL AND DLPE.dtCompleted IS NULL))'		
	   		       +' AND (SLPE.dtCreated = DLPE.dtCreated )'		
	   			  +' )'
	   			  +' WHERE DLPE.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)	
	   
-- =============================================================================
-- LOGIC START -- copy tblLearningPathEnrollmentApprover object for full copy ----------


	   -- insert records from source site to destination table LearningPathEnrollmentApprover
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblLearningPathEnrollmentApprover]'
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idLearningPath,'  
	   			  +'	  idUser'    
	   			  +'  ) '
	   			  +' SELECT '
				  +     CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTLP.newDestinationId,'
	   			  +'	   TTU.newDestinationId'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPathEnrollmentApprover] LPEA'
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = LPEA.idLearningPath )'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = LPEA.idUser )'
	   			  +' WHERE LPEA.idSite ='+ CAST(@idSiteSource AS NVARCHAR)
	   
	   EXEC(@sql)

  End
---------------------------------------------------------------------------------------------
-- end full copy


-- ================================================================================================================
-- LOGIC START -- copy tblRuleSetToRuleSetLearningPathEnrollmentLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table RuleSetToRuleSetLearningPathEnrollmentLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetToRuleSetLearningPathEnrollmentLink]'
	   			 +' ( '
	   		      +'	 idSite,'
	   			 +'	 idRuleSet,'  
	   			 +'	 idRuleSetLearningPathEnrollment '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTRS.newDestinationId, '
	   			 +'   TTRSLPE.newDestinationId '						
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetToRuleSetLearningPathEnrollmentLink] RSRSLPEL '
	   			 +' LEFT JOIN #RuleSetLearningPathEnrollmentIdMappings TTRSLPE on (TTRSLPE.oldSourceId = RSRSLPEL.idRuleSetLearningPathEnrollment )'
	   			 +' LEFT JOIN #RuleSetIdMappings TTRS on (TTRS.oldSourceId = RSRSLPEL.idRuleSet )'
	   			 +' WHERE RSRSLPEL.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)


	   /*
	   Drop temporary table RuleSetLearningPathEnrollmentIdMappings
	   */
	   DROP TABLE #RuleSetLearningPathEnrollmentIdMappings



-- ================================================================================
-- LOGIC START -- copy tblCertificate object for both full copy and partial copy---

	   -- insert records from source site to destination table Certificate
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificate]'
	   			  +' ( '
	   			  +'   idSite,
	   				  name,
	   				  issuingOrganization,
	   				  [description],
	   				  credits,
	   				  code,
	   				  [filename],
	   				  isActive,
	   				  activationDate,
	   				  expiresInterval,
	   				  expiresTimeframe,
	   				  objectType,
	   				  idObject,
	   				  isDeleted,
	   				  dtDeleted'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , C.name,
	   				  C.issuingOrganization,
	   				  C.[description],
	   				  C.credits,
	   				  C.code,
	   				  C.[filename],
	   				  C.isActive,
	   				  C.activationDate,
	   				  C.expiresInterval,
	   				  C.expiresTimeframe,
	   				  C.objectType,'
	   			  +'   CASE C.objectType
	   					   WHEN 1 THEN
	   					  	 TTC.newDestinationId
	   					   WHEN 2 THEN
	   					  	 TTLP.newDestinationId
	   					   ELSE
	   					  	 NULL
	   					   END'
	   			  +',  C.isDeleted,
	   			       C.dtDeleted					
	   			  	FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificate] C'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = C.idObject)'
	   			  +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = C.idObject)'
	   			  +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)

							
	   -- temporary table to hold the idCertificate of source and destination Certificate tables
	   CREATE TABLE #CertificateIdMappings 
	   (
	   	oldSourceId INT NOT NULL, 
	   	newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idCertificate of source Certificate table and destination Certificate table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CertificateIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idCertificate, 
					   DST.idCertificate '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificate] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificate] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.name = DST.name)'
	   			  +' AND (SRC.activationDate = DST.activationDate OR (SRC.activationDate IS NULL AND DST.activationDate IS NULL))'
	   			  +' AND (SRC.issuingOrganization = DST.issuingOrganization OR (SRC.issuingOrganization IS NULL AND DST.issuingOrganization IS NULL))'
	   			  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)

	   -- certificate language data move logic start

	   -- insert records from source site to destination table CertificateLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateLanguage]'
	   			  +' ( '
	   			  +'   idSite, 
	   			  	  idCertificate, 
	   			  	  idLanguage, 
	   			  	  name, 
	   			  	  [description] '
	   			  +' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTC.newDestinationId, 
	   			  	  CL.idLanguage, 
	   			  	  CL.name, 
	   			  	  CL.[description] 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateLanguage] CL'
	   			  +' LEFT JOIN #CertificateIdMappings TTC on (TTC.oldSourceId = CL.idCertificate )'
	   			  +' WHERE CL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)
              	              
	   -- certificate language data move logic end

	   -- insert temp table data for group in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'certificates' FROM #CertificateIdMappings
	   

------------------------------------------------------------------------------

	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN		


-- =============================================================================
-- LOGIC START -- copy tblCertificateImport object for full copy ---------------


	   -- insert records from source site to destination table CertificateImport
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateImport]'
	   			  +' ( '
	   			  +'	  [idSite],
	   			  	  [idUser],
	   			  	  [timestamp],
	   			  	  [idUserImported],
	   			  	  [importFileName]'
	   			  +' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTUser.newDestinationId,
	   			  	  CI.[timestamp],
	   			  	  TTUserImported.newDestinationId,
	   			  	  CI.[importFileName] 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateImport] CI'
	   			  +' LEFT JOIN #UserIdMappings TTUser on (TTUser.oldSourceId = CI.idUser )'
	   			  +' LEFT JOIN #UserIdMappings TTUserImported on (TTUserImported.oldSourceId = CI.idUserImported )'
	   			  +' WHERE CI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   					
	   -- temporary table to hold the idCertificateImport of source and destination CertificateImport tables
	   CREATE TABLE #CertificateImportIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idCertificateImport of source CertificateImport table and destination CertificateImport table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CertificateImportIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idCertificateImport, 
					   DST.idCertificateImport'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateImport] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateImport] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.[timestamp] = DST.[timestamp])'
	   			  +' AND (SRC.[importFileName] = DST.[importFileName] OR (SRC.[importFileName] IS NULL AND DST.[importFileName] IS NULL))'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblCertificateRecord object for full copy ---------------


	   -- insert records from source site to destination table CertificateRecord
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateRecord]'
              		  +' ( '
              		  +'   idSite,
				  	  idCertificate,
				  	  idUser,
				  	  idAwardedBy,
				  	  [timestamp],
				  	  expires,
				  	  idTimezone,
				  	  code,
				  	  credits,
				  	  awardData,
				  	  idCertificateImport'
              		  +' ) '
              		  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
              		  +' , TTC.newDestinationId, 
              		  	  TTU.newDestinationId, 
              		  	  TTAB.newDestinationId, 
              		  	  CR.[timestamp],
				  	  CR.expires,
				  	  CR.idTimezone,
				  	  CR.code,
				  	  CR.credits,
				  	  CR.awardData,
				  	  TTCI.newDestinationId
              		     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateRecord] CR'
              		  +' LEFT JOIN #CertificateIdMappings TTC on (TTC.oldSourceId = CR.idCertificate )'
				  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CR.idUser )'
				  +' LEFT JOIN #UserIdMappings TTAB on (TTAB.oldSourceId = CR.idAwardedBy )'
				  +' LEFT JOIN #CertificateImportIdMappings TTCI on (TTCI.oldSourceId = CR.idCertificateImport )'
              		  +' WHERE CR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
	   EXEC(@sql)
	   
	   -- temporary table to hold the idCertificateRecord of source and destination CertificateRecord tables
	   CREATE TABLE #CertificateRecordIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idCertificateRecord of source CertificateRecord table and destination CertificateRecord table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CertificateRecordIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					   SRC.idCertificateRecord, 
					   DST.idCertificateRecord'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificateRecord] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificateRecord] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.[timestamp] = DST.[timestamp])'
	   			  +' AND (SRC.code = DST.code OR (SRC.code IS NULL AND DST.code IS NULL))'
	   			  +' AND (SRC.credits = DST.credits OR (SRC.credits IS NULL AND DST.credits IS NULL))'
	   			  +' )'
	   			  +' INNER JOIN #CertificateIdMappings TTC on (TTC.oldSourceId = SRC.idCertificate AND TTC.newDestinationId =  DST.idCertificate)'
	   			  +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = SRC.idUser AND TTU.newDestinationId = DST.idUser)'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)



	   /*
	   Drop temporary Table CertificateImportIdMappings
	   */
	   DROP TABLE #CertificateImportIdMappings


-- =============================================================================
-- LOGIC START -- copy tblCourseExpert object for full copy --------------------


	   -- insert records from source site to destination table CourseExpert
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseExpert]'
	   			  +' ( '
	   			  +'	   idSite,
	   			  	   idCourse,
	   			  	   idUser'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTC.newDestinationId,'
	   			  +'   TTU.newDestinationId '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseExpert] CE'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CE.idUser )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CE.idCourse )'
	   			  +' WHERE CE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   
	   EXEC(@sql)


-- =============================================================================
-- LOGIC START -- copy tblCourseEnrollmentApprover object for full copy --------------------


	   -- insert records from source site to destination table CourseEnrollmentApprover
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseEnrollmentApprover]'
	   			  +' ( '
	   			  +'	   idSite,
	   			  	   idCourse,
	   			  	   idUser'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTC.newDestinationId,'
	   			  +'   TTU.newDestinationId '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseEnrollmentApprover] CEA'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CEA.idUser )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CEA.idCourse )'
	   			  +' WHERE CEA.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 	
	   
	   EXEC(@sql)

	   

-- =============================================================================
-- LOGIC START -- copy tblCourseFeedMessage object for full copy ---------------


	   -- insert records from source site to destination table CourseFeedMessage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseFeedMessage]'
	   			  +' ( '
	   			  +'	   idSite,
	   			  	   idCourse,
	   			  	   idLanguage,
	   			  	   idAuthor,
	   			  	   idParentCourseFeedMessage,
	   			  	   [message] ,
	   			  	   [timestamp],
	   			  	   dtApproved,
	   			  	   idApprover,
	   			  	   IsApproved '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTC.newDestinationId, '
	   			  +'	   CFM.idLanguage,'
	   			  +'	   TTAuthor.newDestinationId,'
	   			  +'	   NULL,'			    -- this null value of idParentCourseFeedMessage will be updated from temporary CourseFeedMessageIdMappings table below
	   			  +'	   CFM.[message],   
	   			  	   CFM.[timestamp],
	   			  	   CFM.dtApproved,
	   			  	   TTApprover.newDestinationId,
	   			  	   CFM.IsApproved '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseFeedMessage] CFM'
	   			  +' LEFT JOIN #UserIdMappings TTAuthor on (TTAuthor.oldSourceId = CFM.idAuthor )'
	   			  +' LEFT JOIN #UserIdMappings TTApprover on (TTApprover.oldSourceId = CFM.idApprover )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CFM.idCourse )'
	   			  +' WHERE CFM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)


	   -- temporary table to hold the idCourseFeedMessage of source and destination CourseFeedMessage tables
	   CREATE TABLE #CourseFeedMessageIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idCourseFeedMessage of source CourseFeedMessage table and destination CourseFeedMessage table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #CourseFeedMessageIdMappings'
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idCourseFeedMessage, 
					   DST.idCourseFeedMessage'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseFeedMessage] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseFeedMessage] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.[timestamp] = DST.[timestamp])'
	   			  +' AND (SRC.message = DST.message)'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)
	   
	   
	   -- update idParentCourseFeedMessage from temp table CourseFeedMessage
	   SET @sql =	  ' UPDATE DSTC'
	   			 +'	 SET idParentCourseFeedMessage = TEMP.newDestinationId'
	   			 +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseFeedMessage] SRCC'
	   			 +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseFeedMessage] DSTC'
	   			 +' ON(SRCC.[timestamp] = DSTC.[timestamp] AND SRCC.message = DSTC.message AND SRCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	   			 +' INNER JOIN  #CourseFeedMessageIdMappings TEMP'
	   			 +' ON(TEMP.oldSourceId = SRCC.idParentCourseFeedMessage)'
	   			 +' WHERE DSTC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			 +' AND SRCC.idParentCourseFeedMessage IS NOT NULL'
	   
	   EXEC(@sql)	
	   
	   /*
	   Drop temporary table TempTableCourseFeedMessage
	   */
	   DROP TABLE #CourseFeedMessageIdMappings



-- =============================================================================
-- LOGIC START -- copy tblCourseFeedModerator object for full copy -------------


	   -- insert records from source site to destination table CourseFeedModerator
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseFeedModerator]'
	   			  +' ( '
	   			  +'    idSite,
	   		 		   idCourse,
	   			  	   idModerator'
	   			  +' ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTC.newDestinationId,
	   			  	   TTU.newDestinationId'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseFeedModerator] CFM'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CFM.idModerator )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CFM.idCourse )'
	   			  +' WHERE CFM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- =============================================================================
-- LOGIC START -- copy tblCourseRating object for full copy --------------------


	   -- insert records from source site to destination table CourseRating
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseRating]'
	   			  +' ( '
	   			  +'	  idSite,
	   			  	  idCourse,
	   			  	  idUser,
	   			  	  rating,
	   			  	  [timestamp]'
	   			  +' ) '
	   			  +' SELECT '
				  +    CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTC.newDestinationId,
	   			  	  TTU.newDestinationId,
	   			  	  CR.rating,
	   			  	  CR.[timestamp]'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseRating] CR'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CR.idUser )'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CR.idCourse )'
	   			  +' WHERE CR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)


-- =============================================================================
-- LOGIC START -- copy tblActivityImport object for full copy ------------------


	   -- insert records from source site to destination table ActivityImport
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblActivityImport]'
	   			  +' ( '
	   			  +'	  [idSite],
	   			  	  [idUser],
	   			  	  [timestamp],
	   			  	  [idUserImported],
	   			  	  [importFileName]'
	   			  +' ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +',  TTUser.newDestinationId,
	   			  	  AI.[timestamp],
	   			  	  TTUserImported.newDestinationId,
	   			  	  AI.[importFileName] 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblActivityImport] AI'
	   			  +' LEFT JOIN #UserIdMappings TTUser on (TTUser.oldSourceId = AI.idUser )'
	   			  +' LEFT JOIN #UserIdMappings TTUserImported on (TTUserImported.oldSourceId = AI.idUserImported )'
	   			  +' WHERE AI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
							
							
	   -- temporary table to hold the idActivityImport of source and destination ActivityImport tables
	   CREATE TABLE #ActivityImportIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )

	   -- inserting idActivityImport of source ActivityImport table and destination ActivityImport table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #ActivityImportIdMappings'
	   			  +' ( '
	   			  +'   oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
				  	 SRC.idActivityImport, 
				  	 DST.idActivityImport '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblActivityImport] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblActivityImport] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.timestamp = DST.timestamp)'
	   			  +' AND (SRC.importFileName = DST.importFileName OR (SRC.importFileName IS NULL AND DST.importFileName IS NULL))'
	   			  +')'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)


    END

------------------------------------------------------------------------------
-- full copy end



-- ==============================================================================================
-- LOGIC START -- copy tblCourseToPrerequisiteLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CourseToPrerequisiteLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseToPrerequisiteLink]'
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idCourse,
	   			  	  idPrerequisite '
	   			  +'  ) '
	   			  +' SELECT TOP 1 '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTCourse.newDestinationId
	   			  	 ,  TTPrerequisite.newDestinationId '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseToPrerequisiteLink] CTPL'
				  +' LEFT JOIN #CourseIdMappings TTCourse on (TTCourse.oldSourceId = CTPL.idCourse )'
	   			  +' LEFT JOIN #CourseIdMappings TTPrerequisite on (TTPrerequisite.oldSourceId = CTPL.idPrerequisite )'
	   			  +' WHERE CTPL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				
	   EXEC(@sql)


-- ==============================================================================================
-- LOGIC START -- copy tblCourseToScreenshotLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table CourseToScreenshotLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCourseToScreenshotLink]'
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idCourse,
	   			  	  filename '
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTCourse.newDestinationId,
	   			  	   CTSL.filename '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCourseToScreenshotLink] CTSL'
	   			  +' LEFT JOIN #CourseIdMappings TTCourse on (TTCourse.oldSourceId = CTSL.idCourse )'
	   			  +' WHERE CTSL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- =====================================================================================
-- LOGIC START -- copy tblGroupEnrollment object for both full copy and partial copy ---


	   -- insert records from source site to destination table GroupEnrollment
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupEnrollment]'
	   			  +' ( '
	             	  +'   idSite,
	   			       idCourse,
	   			       idGroup,
	   			       idTimezone,
	   			       isLockedByPrerequisites,
	   			       dtStart,
	   			       dtCreated,
	   			       dueInterval,
	   			       dueTimeframe,
	   			       expiresFromStartInterval,
	   			       expiresFromStartTimeframe,
	   			       expiresFromFirstLaunchInterval,
	   			       expiresFromFirstLaunchTimeframe '
	   			  +'  ) '
	   			  +' SELECT '
				  +     CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTC.newDestinationId,
	   				   TTG.newDestinationId,
	   				   GE.idTimezone,
	   				   GE.isLockedByPrerequisites,
	   				   GE.dtStart,
	   				   GE.dtCreated,
	   				   GE.dueInterval,
	   				   GE.dueTimeframe,
	   				   GE.expiresFromStartInterval,
	   				   GE.expiresFromStartTimeframe,
	   				   GE.expiresFromFirstLaunchInterval,
	   				   GE.expiresFromFirstLaunchTimeframe 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupEnrollment] GE '
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GE.idGroup)'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = GE.idCourse)'
	   			  +' WHERE GE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   										
	   EXEC(@sql)
	   
	   -- temporary table to hold the idGroupEnrollment of source and destination GroupEnrollment tables
	   CREATE TABLE #GroupEnrollmentIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idGroupEnrollment of source GroupEnrollment table and destination GroupEnrollment table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #GroupEnrollmentIdMappings'
				  +' ( '
				  +'	  oldSourceId,
				  	  newDestinationId'
				  +' ) '
				  +' SELECT 
					   SRC.idGroupEnrollment, 
					   DST.idGroupEnrollment '
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupEnrollment] DST'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupEnrollment] SRC'
				  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SRC.dtStart = DST.dtStart)'
				  +' AND (SRC.dtCreated = DST.dtCreated)'
				  +' )'
				  +' INNER JOIN #GroupIdMappings TTG on (TTG.oldSourceId = SRC.idGroup AND TTG.newDestinationId =  DST.idGroup)'
				  +' INNER JOIN #CourseIdMappings TTC on (TTC.oldSourceId = SRC.idCourse AND TTC.newDestinationId = DST.idCourse)'
				  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
			
	   EXEC(@sql)



-- =======================================================================================
-- LOGIC START -- copy tblRuleSetEnrollment object for both full copy and partial copy ---


	   -- insert records from source site to destination table RuleSetEnrollment
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetEnrollment]'
	   			  +' ( '
	   			  +'   idSite,
	   				  idCourse,
	   				  idTimezone,
	   				  [priority],
	   				  label,
	   				  isLockedByPrerequisites,
	   				  isFixedDate,
	   				  dtStart,
	   				  dtEnd,
	   				  dtCreated,
	   				  delayInterval,
	   				  delayTimeframe,
	   				  dueInterval,
	   				  dueTimeframe,
	   				  recurInterval,
	   				  recurTimeframe,
	   				  expiresFromStartInterval,
	   				  expiresFromStartTimeframe,
	   				  expiresFromFirstLaunchInterval,
	   				  expiresFromFirstLaunchTimeframe,
	   				  forceReassignment'
	   			  + '  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTC.newDestinationId,
	   				   RE.idTimezone,
	   				   RE.[priority],
	   				   RE.label,
	   				   RE.isLockedByPrerequisites,
	   				   RE.isFixedDate,
	   				   RE.dtStart,
	   				   RE.dtEnd,
	   				   RE.dtCreated,
	   				   RE.delayInterval,
	   				   RE.delayTimeframe,
	   				   RE.dueInterval,
	   				   RE.dueTimeframe,
	   				   RE.recurInterval,
	   				   RE.recurTimeframe,
	   				   RE.expiresFromStartInterval,
	   				   RE.expiresFromStartTimeframe,
	   				   RE.expiresFromFirstLaunchInterval,
	   				   RE.expiresFromFirstLaunchTimeframe,
	   				   RE.forceReassignment 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetEnrollment] RE'
	   			  +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = RE.idCourse )'
	   			  +' WHERE RE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	   				
	   -- temporary table to hold the idRuleSetEnrollment of source and destination RuleSetEnrollment tables
	   CREATE TABLE #RuleSetEnrollmentIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )
	   
	   -- inserting idRuleSetEnrollment of source RuleSetEnrollment table and destination RuleSetEnrollment table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #RuleSetEnrollmentIdMappings'
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					   SRC.idRuleSetEnrollment, 
					   DST.idRuleSetEnrollment '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetEnrollment] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetEnrollment] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.label = DST.label)'
	   			  +' AND (SRC.dtStart = DST.dtStart)'
	   			  +' AND (SRC.dtEnd = DST.dtEnd OR (SRC.dtEnd IS NULL AND DST.dtEnd IS NULL))'
	   			  +' AND (SRC.dtCreated = DST.dtCreated)'
	   			  +' )'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   	
	   EXEC(@sql)

	   -- ruleSetEnrollment language data move logic start

	   -- insert records from source site to destination table RuleSetEnrollmentLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetEnrollmentLanguage]'
	   			  +' ( '
	   			  +'   idSite, 
	   			  	  idRuleSetEnrollment, 
	   			  	  idLanguage, 
	   			  	  label '
	   			  +'  ) '
	   			  +' SELECT '
	   			  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTRE.newDestinationId, 
	   			  	  REL.idLanguage, 
	   			  	  REL.label 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetEnrollmentLanguage] REL'
	   			  +' LEFT JOIN #RuleSetEnrollmentIdMappings TTRE on (TTRE.oldSourceId = REL.idRuleSetEnrollment )'
	   			  +' WHERE REL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	           
	   EXEC(@sql)
	                 
	   -- ruleSetEnrollment language data move logic end

	   
-- ====================================================================================================
-- LOGIC START -- copy tblRuleSetToRuleSetEnrollmentLink object for both full copy and partial copy ---


	   -- insert records from source site to destination table RuleSetToRuleSetEnrollmentLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblRuleSetToRuleSetEnrollmentLink]'
	   			  +' ( '
	   			  +'	   idSite,
	   				   idRuleSet,
	   				   idRuleSetEnrollment '
	   			  + ' ) '
	   			  +' SELECT TOP 1 '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTR.newDestinationId, 
	   			  	   TTRE.newDestinationId 
	   			     FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblRuleSetToRuleSetEnrollmentLink] RTREL'
	   			  +' LEFT JOIN #RuleSetIdMappings TTR on (TTR.oldSourceId = RTREL.idRuleSet )'
	   			  +' LEFT JOIN #RuleSetEnrollmentIdMappings TTRE on (TTRE.oldSourceId = RTREL.idRuleSetEnrollment )'
	   			  +' WHERE RTREL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)



-- ==============================================================================================
-- LOGIC START -- copy tblDocumentRepositoryFolder object for both full copy and partial copy ---


	   -- insert records from source site to destination table DocumentRepositoryFolder
	   SET @sql =	  'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryFolder]'
	   			 +' ( '
	   			 +'   idSite,
	   			      idDocumentRepositoryObjectType,
	   			      idObject,
	   			      [Name]'
	   			 + '  ) '
	   			 +' SELECT '
				 +	  CAST(@idSiteDestination AS NVARCHAR)
	   			 +' ,  DRF.idDocumentRepositoryObjectType'
				 +' ,  DRF.idObject'
	   			 --+'    CASE DRF.idDocumentRepositoryObjectType
	   			 --	     WHEN 1 THEN
	   			 --	   	 TTG.newDestinationId
	   			 --	     WHEN 2 THEN
	   			 --	   	 TTC.newDestinationId
	   			 --	     WHEN 3 THEN 
	   			 --	   	 TTLP.newDestinationId
	   			--	  END'
	   			 +' ,  DRF.[Name] 
	   			    FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryFolder] DRF'
	   			 +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = DRF.idObject )'
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = DRF.idObject )'
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = DRF.idObject )'
	   			 +' WHERE DRF.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   				
	   -- temporary table to hold the idDocumentRepositoryFolder of source and destination DocumentRepositoryFolder tables
	   CREATE TABLE #DocumentRepositoryFolderIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL 	   
	   )
	   
	   -- inserting idDocumentRepositoryFolder of source DocumentRepositoryFolder table and destination DocumentRepositoryFolder table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #DocumentRepositoryFolderIdMappings'
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idDocumentRepositoryFolder, 
					   DST.idDocumentRepositoryFolder '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryFolder] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryFolder] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.name = DST.name)'
	   			  +' AND (SRC.idDocumentRepositoryObjectType = DST.idDocumentRepositoryObjectType)'
	   			  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
	   			  +')'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)


-- ==========================================================================================================
-- LOGIC START -- copy tblEventEmailNotification object for both full copy and partial copy with condition---


	   -- insert records from source site to destination table EventEmailNotification
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventEmailNotification]'
	   			 +' ( '
	   			 +'   idSite,'
	   			 +'	 name,'
	   			 +'	 idEventType,'  
	   			 +'	 idEventTypeRecipient,'
	   			 +'	 [from],'
	   			 +'	 isActive,'
	   			 +'	 dtActivation,'
	   			 +'	 interval,'
	   			 +'	 timeframe,'
	   			 +'	 [priority], '
				  +'	 idObject '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +	 CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  EEN.name,'
	   			 +'	 EEN.idEventType,'
	   			 +'	 EEN.idEventTypeRecipient,'
	   			 +'	 EEN.[from],'
	   			 +'	 EEN.isActive,'
	   			 +'	 EEN.dtActivation,'
	   			 +'	 EEN.interval,'
	   			 +'	 EEN.timeframe,'
	   			 +'	 EEN.[priority], '
				

		IF(@fullCopyFlag = 1) 
		  BEGIN
			 SET @subSql = '	 CASE 
								WHEN EEN.idEventType >= 100 AND EEN.idEventType < 200 THEN 
									TTUO.newDestinationId
								WHEN EEN.idEventType >= 200 AND EEN.idEventType < 300 THEN 
									TTCO.newDestinationId
								WHEN EEN.idEventType >= 300 AND EEN.idEventType < 400 THEN 
									TTLO.newDestinationId
								WHEN EEN.idEventType >= 500 AND EEN.idEventType < 600 THEN  
									TTLPO.newDestinationId
								WHEN EEN.idEventType >= 700 AND EEN.idEventType < 800 THEN 
									TTCEO.newDestinationId
	   				   	 	 END '
	   				      +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailNotification] EEN '
	   				   	 +' LEFT JOIN #UserIdMappings TTUO on (TTUO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #CourseIdMappings TTCO on (TTCO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #LessonIdMappings TTLO on (TTLO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #LearningPathIdMappings TTLPO on (TTLPO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #CertificateIdMappings TTCEO on (TTCEO.oldSourceId = EEN.idObject )'
	   				   	 +' WHERE EEN.idSite ='+ CAST(@idSiteSource AS NVARCHAR)
						 +' AND  EEN.idEventType NOT BETWEEN 400 AND 499 '
		  END
		  ELSE		   -- idUser and idCertificate are not move in partial copy. 
		  BEGIN
			  SET @subSql = '  CASE 
	   				   	 	 	 WHEN EEN.idEventType >= 200 AND EEN.idEventType < 300 THEN 
	   				   	 	   		TTCO.newDestinationId
	   				   	 	 	 WHEN EEN.idEventType >= 300 AND EEN.idEventType < 400 THEN 
	   				   	 	 		TTLO.newDestinationId
	   				   	 	 	 WHEN EEN.idEventType >= 500 AND EEN.idEventType < 600 THEN  
	   				   	 	 		TTLPO.newDestinationId
								 WHEN EEN.idEventType >= 700 AND EEN.idEventType < 800 THEN 
									TTCEO.newDestinationId
	   				   	 	 END '
	   				      +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailNotification] EEN '
	   				   	 +' LEFT JOIN #CourseIdMappings TTCO on (TTCO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #LessonIdMappings TTLO on (TTLO.oldSourceId = EEN.idObject ) '
	   				   	 +' LEFT JOIN #LearningPathIdMappings TTLPO on (TTLPO.oldSourceId = EEN.idObject ) '
						 +' LEFT JOIN #CertificateIdMappings TTCEO on (TTCEO.oldSourceId = EEN.idObject )'
	   				   	 +' WHERE EEN.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   				   	 +' AND  EEN.idEventType NOT BETWEEN 100 AND 199 '
	   				   	 +' AND  EEN.idEventType NOT BETWEEN 400 AND 499 '
	   				   	
		  END

	   SET @sql = @sql + @subSql
	   EXEC(@sql)
							
	   -- temporary table to hold the idEventEmailNotification of source and destination EventEmailNotification tables
	   CREATE TABLE #EventEmailNotificationIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idEventEmailNotification of source EventEmailNotification table and destination EventEmailNotification table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #EventEmailNotificationIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
	   				   SEEN.idEventEmailNotification, 
	   				   DEEN.idEventEmailNotification '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventEmailNotification] DEEN'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailNotification] SEEN'
	   			  +' ON (SEEN.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		       +' AND (SEEN.[from] = DEEN.[from] OR (SEEN.[from] IS NULL AND DEEN.[from] IS NULL))'
	   			  +' AND (SEEN.dtActivation = DEEN.dtActivation OR (SEEN.dtActivation IS NULL AND DEEN.dtActivation IS NULL))'					
	   			  +' AND (SEEN.timeframe = DEEN.timeframe OR (SEEN.timeframe IS NULL AND DEEN.timeframe IS NULL))' 
	   			  +' )'
	   			  +' WHERE DEEN.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
			
	   EXEC(@sql)	 
	   		    	
	   -- eventEmailNotificationLanguage data move logic start
	   
	   -- insert records from source site to destination table EventEmailNotificationLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventEmailNotificationLanguage] '
	   			  +' ( '
	   			  +'	  idSite,'
	   			  +'	  idEventEmailNotification,' 
	   			  +'	  idLanguage,'
	   			  +'	  name '
	   			  +' ) '
	   			  +' SELECT '
	   			  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +',  TTEEN.newDestinationId,'
	   			  +'   EENL.idLanguage,'
	   			  +'   EENL.name '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailNotificationLanguage] EENL '
	   			

		  IF(@fullCopyFlag = 1) 
		  BEGIN
			 SET @subSql = ' LEFT JOIN #EventEmailNotificationIdMappings TTEEN on (TTEEN.oldSourceId = EENL.idEventEmailNotification )'
	   				    +' WHERE EENL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 

		  END
		  ELSE
		  BEGIN
			SET @subSql = ' INNER JOIN #EventEmailNotificationIdMappings TTEEN on (TTEEN.oldSourceId = EENL.idEventEmailNotification )'
	   				   +' WHERE EENL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		  END
	   
	   SET @sql = @sql + @subSql 
	    
	   EXEC(@sql)
	   
	   -- eventEmailNotificationLanguage data move logic end


	   -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'emailNotifications' FROM #EventEmailNotificationIdMappings


------------------------------------------------------------------------------
	  

	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN


-- =======================================================================
-- LOGIC START -- copy tblDocumentRepositoryItem object for full copy ---


	   -- insert records from source site to destination table DocumentRepositoryItem
	   SET @sql =	 ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryItem]'
				+' ( '
				+'  idSite,
				    idDocumentRepositoryObjectType,
				    label,
				    [fileName],
				    kb,
				    searchTags,
				    isPrivate,
				    idLanguage,
				    isAllLanguages,
				    dtCreated,
				    isDeleted,
				    dtDeleted,
				    idObject,
				    idDocumentRepositoryFolder,
				    idOwner'
				+' ) '
				+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
				+' ,DRI.idDocumentRepositoryObjectType,'
				+'  DRI.label,
				    DRI.[fileName],
				    DRI.kb,
				    DRI.searchTags,
				    DRI.isPrivate,
				    DRI.idLanguage,
				    DRI.isAllLanguages,
				    DRI.dtCreated, 
				    DRI.isDeleted,
				    DRI.dtDeleted,'
				+ '	 CASE DRI.idDocumentRepositoryObjectType
								WHEN 1 THEN
								    TTG.newDestinationId
								WHEN 2 THEN
								    TTC.newDestinationId
								WHEN 3 THEN 
								    TTLP.newDestinationId
							 END'
				 +', TTDRF.newDestinationId,
					  TTU.newDestinationId '
				 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryItem] DRI'
              		 +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = DRI.idObject )'
				 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = DRI.idObject )'
				 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = DRI.idObject )'
				 +' LEFT JOIN #DocumentRepositoryFolderIdMappings TTDRF on (TTDRF.oldSourceId = DRI.idDocumentRepositoryFolder )'
				 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = DRI.idOwner )'
              		 +' WHERE DRI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 


            EXEC(@sql)

		 
		  -- temporary table to hold the idDocumentRepositoryItem of source and destination DocumentRepositoryItem tables
		  CREATE TABLE #DocumentRepositoryItemIdMappings
		  (
		  	oldSourceId INT NOT NULL, 
		  	newDestinationId INT NOT NULL 	   
		  )
		  
		  -- inserting idDocumentRepositoryItem of source DocumentRepositoryItem table and destination DocumentRepositoryItem table inside temporary table for mapping
		  SET @sql = 'INSERT INTO #DocumentRepositoryItemIdMappings'
				  +' ( '
				  +'  oldSourceId,
				  	newDestinationId'
				  +' ) '
				  +' SELECT 1, DST.idDocumentRepositoryItem' --SRC.idDocumentRepositoryItem, DST.idDocumentRepositoryItem'
				  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryItem] DST'
				  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryItem] SRC'
				  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				  +' AND (SRC.label = DST.label)'
				  +' AND (SRC.dtCreated = DST.dtCreated )'
				  +' AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))'
				  +')'
				  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)


		
				
	   EXEC(@sql)

		 
	   -- DocumentRepositoryItem language data move start
		  
	   -- insert records from source site to destination table DocumentRepositoryItemLanguage
	   SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblDocumentRepositoryItemLanguage]'
				   +' ( '
				   +'  idSite,
				       idDocumentRepositoryItem,
				       idLanguage,
				       label,
				       searchTags'
				   +' ) '
				   +' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
				   +' , TTDRI.newDestinationId, '
				   +'	DRIL.idLanguage, '
				   +'	DRIL.label, '
				   +'	DRIL.searchTags ' 
				   +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblDocumentRepositoryItemLanguage] DRIL '
				   +' LEFT JOIN #DocumentRepositoryItemIdMappings TTDRI on (TTDRI.oldSourceId = DRIL.idDocumentRepositoryItem)'
				   +' WHERE DRIL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		
	   EXEC(@sql)				

	   -- documentRepositoryItem language data move logic end
	   
	   /*
	   Drop temporary table DocumentRepositoryItemIdMappings
	   */
	   DROP TABLE #DocumentRepositoryItemIdMappings


-- =============================================================================
-- LOGIC START -- copy tblEnrollment object for full copy ----------------------


	   -- insert records from source site to destination table Enrollment
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEnrollment]'
	   			 +' ( '
	   			 +'   idSite,' 
	   			 +'   idCourse,'      
	   			 +'   idUser,'
	   			 +'   idGroupEnrollment,'
	   			 +'   idRuleSetEnrollment,'
	   			 +'   idLearningPathEnrollment,'
	   			 +'   idTimezone,'
	   			 +'   isLockedByPrerequisites, '
	   			 +'   code, '
	   			 +'   revcode, '
	   			 +'   title, '
	   			 +'   dtStart, '
	   			 +'   dtDue, '
	   			 +'   dtExpiresFromStart, '
	   			 +'   dtExpiresFromFirstLaunch, '
	   			 +'   dtFirstLaunch, '
	   			 +'   dtCompleted, '
	   			 +'   dtCreated, '
	   			 +'   dtLastSynchronized, '
	   			 +'   dueInterval, '
	   			 +'   dueTimeframe, '
	   			 +'   expiresFromStartInterval, '
	   			 +'   expiresFromStartTimeframe, '
	   			 +'   expiresFromFirstLaunchInterval, '
	   			 +'   expiresFromFirstLaunchTimeframe, '
	   			 +'   idActivityImport, '
	   			 +'   credits '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTC.newDestinationId,'
	   			 +'	 TTU.newDestinationId,'
	   			 +'	 TTGE.newDestinationId,'
	   			 +'	 TTRSE.newDestinationId,'
	   			 +'	 TTLPE.newDestinationId,'
	   			 +'	 E.idTimezone,'
	   			 +'	 E.isLockedByPrerequisites,'
	   			 +'	 E.code,'
	   			 +'	 E.revcode,'
	   			 +'	 E.title,'
	   			 +'	 E.dtStart,'
	   			 +'	 E.dtDue,'
	   			 +'	 E.dtExpiresFromStart,'
	   			 +'	 E.dtExpiresFromFirstLaunch,'
	   			 +'	 E.dtFirstLaunch,'
	   			 +'	 E.dtCompleted,'
	   			 +'	 E.dtCreated,'
	   			 +'	 E.dtLastSynchronized,'
	   			 +'	 E.dueInterval,'
	   			 +'	 E.dueTimeframe,'
	   			 +'	 E.expiresFromStartInterval,'
	   			 +'	 E.expiresFromStartTimeframe,'
	   			 +'	 E.expiresFromFirstLaunchInterval,'
	   			 +'	 E.expiresFromFirstLaunchTimeframe,'
	   			 +'	 TTAI.newDestinationId,'
	   			 +'	 E.credits '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEnrollment] E '
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = E.idCourse ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = E.idUser ) '
	   			 +' LEFT JOIN #GroupEnrollmentIdMappings TTGE on (TTGE.oldSourceId = E.idGroupEnrollment ) '
	   			 +' LEFT JOIN #RuleSetEnrollmentIdMappings TTRSE on (TTRSE.oldSourceId = E.idRuleSetEnrollment ) '
	   			 +' LEFT JOIN #LearningPathEnrollmentIdMappings TTLPE on (TTLPE.oldSourceId = E.idLearningPathEnrollment ) '
	   			 +' LEFT JOIN #ActivityImportIdMappings TTAI on (TTAI.oldSourceId = E.idActivityImport ) '
	   			 +' WHERE E.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)				
	   		

	   -- temporary table to hold the idRuleSetEnrollment of source and destination RuleSetEnrollment tables
	   CREATE TABLE #EnrollmentIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idRuleSetEnrollment of source RuleSetEnrollment table and destination RuleSetEnrollment table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #EnrollmentIdMappings '
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT '
	   			  +'	   SE.idEnrollment,'
	   			  +'	   DE.idEnrollment'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEnrollment] DE'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEnrollment] SE'
	   			  +' ON (SE.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SE.dtStart = DE.dtStart )'
	   			  +' AND (SE.dtCompleted = DE.dtCompleted OR (SE.dtCompleted IS NULL AND DE.dtCompleted IS NULL))'	
	   			  +' AND (SE.dtCreated = DE.dtCreated )'			
	   			  +'  )'
	   			  +' INNER JOIN #CourseIdMappings TTC on (TTC.oldSourceId = SE.idCourse AND TTC.newDestinationId =  DE.idCourse)'
	   			  +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = SE.idUser AND TTU.newDestinationId = DE.idUser)'
	   			  +' WHERE DE.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)
	   	 
	   

-- =============================================================================
-- LOGIC START -- copy tblEnrollmentRequest object for full copy ----------------------


	   -- insert records from source site to destination table Enrollment Request
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEnrollmentRequest]'
	   			 +' ( '
	   			 +'   idSite,' 
	   			 +'   idCourse,'      
	   			 +'   idUser,'
	   			 +'   timestamp,'
	   			 +'   dtApproved,'
	   			 +'   dtDenied,'
	   			 +'   idApprover'
	   			 +'  ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' , TTC.newDestinationId,'
	   			 +'	 TTU.newDestinationId,'
	   			 +'	 ER.timestamp,'
	   			 +'	 ER.dtApproved,'
	   			 +'	 ER.dtDenied,'
	   			 +'	 TTUA.newDestinationId'
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEnrollmentRequest] ER '
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = ER.idCourse ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = ER.idUser ) '
				 +' LEFT JOIN #UserIdMappings TTUA on (TTU.oldSourceId = ER.idApprover ) '
	   			 +' WHERE ER.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)				


-- =============================================================================
-- LOGIC START -- copy tblData-Lesson object for full copy ---------------------


	   -- insert records from source site to destination table Data-Lesson
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-Lesson]'
	   			 +' ( '
	   			 +'   idSite,'     
	   			 +'   idEnrollment,'   							
	   			 +'   idLesson,'
	   			 +'   idTimezone,'
	   			 +'   title,'
	   			 +'   revcode,'
	   			 +'   [order],'
	   			 +'   contentTypeCommittedTo,'
	   			 +'   dtCommittedToContentType,'
	   			 +'   dtCompleted,'
	   			 +'   resetForContentChange,'
	   			 +'   preventPostCompletionLaunchForContentChange '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTE.newDestinationId,'
	   			 +'	 TTL.newDestinationId,'
	   			 +'	 DL.idTimezone,'
	   			 +'	 DL.title,'
	   			 +'	 DL.revcode,'
	   			 +'	 DL.[order],'
	   			 +'	 DL.contentTypeCommittedTo,'
	   			 +'	 DL.dtCommittedToContentType,'
	   			 +'	 DL.dtCompleted,'
	   			 +'	 DL.resetForContentChange,'
	   			 +'	 DL.preventPostCompletionLaunchForContentChange '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-Lesson] DL '
	   			 +' LEFT JOIN #EnrollmentIdMappings TTE on (TTE.oldSourceId = DL.idEnrollment ) '
	   			 +' LEFT JOIN #LessonIdMappings TTL on (TTL.oldSourceId = DL.idLesson ) '
	   			 +' WHERE DL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)					
	   
	   -- temporary table to hold the idData-Lesson of source and destination Data-Lesson tables
	   CREATE TABLE #DataLessonIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idData-Lesson of source Data-Lesson table and destination Data-Lesson table inside temporary table for mapping	
	   SET @sql =	   ' INSERT INTO #DataLessonIdMappings '
	   			  +' ( '
	   			  +'	   oldSourceId,
	   			  	   newDestinationId'
	   			  +' ) '
	   			  +' SELECT '
	   			  +'	   SDL.[idData-Lesson],'
	   			  +'	   DDL.[idData-Lesson]'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-Lesson] DDL'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-Lesson] SDL'
	   			  +' ON (SDL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SDL.title = DDL.title)'
	   			  +' AND (SDL.[order] = DDL.[order])'
	   			  +' AND (SDL.revcode = DDL.revcode OR (SDL.revcode IS NULL AND DDL.revcode IS NULL))'
	   			  +' AND (SDL.dtCommittedToContentType = DDL.dtCommittedToContentType OR (SDL.dtCommittedToContentType IS NULL AND DDL.dtCommittedToContentType IS NULL))'
	   			  +' AND (SDL.dtCompleted = DDL.dtCompleted OR (SDL.dtCompleted IS NULL AND DDL.dtCompleted IS NULL))'
	   			  +' )'
	   			  +' INNER JOIN #EnrollmentIdMappings TTE on (TTE.oldSourceId = SDL.idEnrollment AND TTE.newDestinationId =  DDL.idEnrollment)'
	   			  +' INNER JOIN #LessonIdMappings TTL on ((TTL.oldSourceId = SDL.idLesson AND TTL.newDestinationId =  DDL.idLesson))'
	   			  +' WHERE DDL.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND DDL.idLesson IS NOT NULL'
	   			  +' UNION '
	   			  +' SELECT '
	   			  +'	   SDL.[idData-Lesson],'
	   			  +'	   DDL.[idData-Lesson]'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-Lesson] DDL'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-Lesson] SDL'
	   			  +' ON (SDL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SDL.title = DDL.title )'
	   			  +' AND (SDL.[order] = DDL.[order])'
	   			  +' AND (SDL.revcode = DDL.revcode OR (SDL.revcode IS NULL AND DDL.revcode IS NULL))'
	   			  +' AND (SDL.dtCommittedToContentType = DDL.dtCommittedToContentType OR (SDL.dtCommittedToContentType IS NULL AND DDL.dtCommittedToContentType IS NULL))'
	   			  +' AND (SDL.dtCompleted = DDL.dtCompleted OR (SDL.dtCompleted IS NULL AND DDL.dtCompleted IS NULL))'
	   			  +' )'
	   			  +' INNER JOIN #EnrollmentIdMappings TTE on (TTE.oldSourceId = SDL.idEnrollment AND TTE.newDestinationId =  DDL.idEnrollment)'
	   			  +' WHERE DDL.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND DDL.idLesson IS NULL'
	   
	   EXEC(@sql)	


	     -- insert temp table data for user in order to return it for moving file system object
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT oldSourceId, newDestinationId, 'dataLesson' FROM #DataLessonIdMappings
	   

-- =============================================================================
-- LOGIC START -- copy tblEventLog object for full copy ------------------------


	   -- insert records from source site to destination table EventLog
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventLog]'
	   			 +' ( '
	   			 +'  idSite,'
	   			 +'  idEventType,'
	   			 +'  timestamp,'
	   			 +'  eventDate,'
	   			 +'  idObject,'
	   			 +'  idObjectRelated,'
	   			 +'  idObjectUser,'
	   			 +'  idExecutingUser'
	   			 +' ) '
	   			 +'SELECT '
	   			 +   CAST(@idSiteDestination AS NVARCHAR)
	   			 +', EL.idEventType,'
	   			 +'  EL.timestamp,'
	   			 +'  EL.eventDate,'
	   			 +'	CASE 
	   					WHEN EL.idEventType >= 100 AND EL.idEventType < 200 THEN 
	   		  				TTUO.newDestinationId
	   					WHEN EL.idEventType >= 200 AND EL.idEventType < 300 THEN 
	   		  				TTCO.newDestinationId
	   					WHEN EL.idEventType >= 300 AND EL.idEventType < 400 THEN 
	   						TTLO.newDestinationId
	   					When EL.idEventType >= 400 AND EL.idEventType < 500 THEN 
	   						TTSTIO.newDestinationId
	   					WHEN EL.idEventType >= 500 AND EL.idEventType < 600 THEN  
	   						TTLPO.newDestinationId
	   					WHEN EL.idEventType >= 700 AND EL.idEventType < 800 THEN 
	   						TTCEO.newDestinationId
	   				
	   				END ,'
	   		
	   			 +'  CASE 
	   					WHEN EL.idEventType >= 100 AND EL.idEventType < 200 THEN 
	   		  				TTUO.newDestinationId
	   					WHEN EL.idEventType >= 200 AND EL.idEventType < 300 THEN 
	   		  				TTEOR.newDestinationId
	   					WHEN EL.idEventType >= 300 AND EL.idEventType < 400 THEN 
	   						TTDLOR.newDestinationId
	   					When EL.idEventType >= 400 AND EL.idEventType < 500 THEN 
	   						TTSTIO.newDestinationId
	   					WHEN EL.idEventType >= 500 AND EL.idEventType < 600 THEN  
	   						TTLPEOR.newDestinationId
	   					WHEN EL.idEventType >= 700 AND EL.idEventType < 800 THEN 
	   						TTCROR.newDestinationId
	   				   						
	   			     END ,'
	   			 +'  TTOU.newDestinationId, '
	   			 +'  TTUE.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventLog] EL'
	   			 +' LEFT JOIN #UserIdMappings TTUO on (TTUO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #UserIdMappings TTOU on (TTOU.oldSourceId = EL.idObjectUser ) ' 
	   			 +' LEFT JOIN #CourseIdMappings TTCO on (TTCO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #LessonIdMappings TTLO on (TTLO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTIO on (TTSTIO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #LearningPathIdMappings TTLPO on (TTLPO.oldSourceId = EL.idObject )'
	   			 +' LEFT JOIN #CertificateIdMappings TTCEO on (TTCEO.oldSourceId = EL.idObject )'	

	   			 +' LEFT JOIN #EnrollmentIdMappings TTEOR on (TTEOR.oldSourceId = EL.idObjectRelated ) '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDLOR on (TTDLOR.oldSourceId = EL.idObjectRelated ) '
	   			 +' LEFT JOIN #LearningPathEnrollmentIdMappings TTLPEOR on (TTLPEOR.oldSourceId = EL.idObjectRelated ) '
	   			 +' LEFT JOIN #CertificateRecordIdMappings TTCROR on (TTCROR.oldSourceId = EL.idObjectRelated ) '  
	   			 +' LEFT JOIN #UserIdMappings TTUE on (TTUE.oldSourceId = EL.idExecutingUser )'
	   			 +' WHERE EL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 


	   EXEC(@sql)	

	   -- temporary table to hold the idEventLog of source and destination EventLog tables
	   CREATE TABLE #EventLogIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idEventLog of source EventLog table and destination EventLog table inside temporary table for mapping	
	   SET @sql =	   ' INSERT INTO #EventLogIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId '
	   			  +' ) '
	   			  +' SELECT 
				  	   SEL.idEventLog, 
				  	   DEL.idEventLog '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventLog] DEL'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventLog] SEL'
	   			  +' ON (SEL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SEL.eventDate = DEL.eventDate )  OR (SEL.eventDate IS NULL AND DEL.eventDate IS NULL))'	
	   			  +' AND (SEL.idEventType = DEL.idEventType) '					
	   			  +' AND (SEL.timestamp = DEL.timestamp) '					
	   			  +' WHERE DEL.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		
	   EXEC(@sql)	
	   
	   
-- =============================================================================
-- LOGIC START -- copy tblEventEmailQueue object for full copy -----------------

	   -- insert records from source site to destination table EventEmailQueue
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblEventEmailQueue]'
	   			 +' ( '
	   			 +'   idSite,'   
	   			 +'	 idEventLog,'
	   			 +'	 idEventType ,'  
	   			 +'	 idEventEmailNotification,'
	   			 +'	 idEventTypeRecipient,'
	   			 +'	 idObject,'
	   			 +'	 idObjectRelated,'
	   			 +'	 objectType,'
	   			 +'	 idObjectUser,'
	   			 +'	 objectUserFullName,'
	   			 +'	 objectUserFirstName,'
	   			 +'	 objectUserLogin,'
	   			 +'	 objectUserEmail,'
	   			 +'	 idRecipient,'
	   			 +'	 recipientLangString,'
	   			 +'	 recipientFullName,'
	   			 +'	 recipientFirstName,'
	   			 +'	 recipientLogin,'
	   			 +'	 recipientEmail,'
	   			 +'	 recipientTimezone,'
	   			 +'	 recipientTimezoneDotNetName,'
	   			 +'	 [from],'
	   			 +'	 [priority],'
	   			 +'	 dtEvent,'
	   			 +'	 dtAction,'
	   			 +'	 dtActivation,'
	   			 +'	 dtSent,'
	   			 +'	 statusDescription '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +	 CAST(@idSiteDestination AS NVARCHAR)
	   			 +',	 TTELog.newDestinationId,'
	   			 +'	 EEQ.idEventType ,'  
	   			 +'	 TTEEN.newDestinationId,'
	   			 +'	 EEQ.idEventTypeRecipient,'
	   			 +'	 CASE EEQ.objectType
	   			 		 WHEN ''user'' THEN 
	   			   			TTU.newDestinationId
	   			 		 WHEN ''courseenrollment'' THEN 
	   			   			TTC.newDestinationId
	   			 		 WHEN ''lesson'' THEN 
	   			 			TTL.newDestinationId
	   			 		 When ''standuptraininginstance'' THEN 
	   			 			TTSTI.newDestinationId
	   			 		 WHEN ''learningpathenrollment'' THEN  
	   			 			TTLP.newDestinationId
	   			 		 WHEN ''certificate'' THEN 
	   			 			TTCE.newDestinationId
	   			 		 ELSE  
	   			 			TTELog.newDestinationId
	   			 	 END ,'
	   			 
	   			 +'	 CASE EEQ.objectType
	   			 		 WHEN ''user'' THEN 
	   			   			TTU.newDestinationId
	   			 		 WHEN ''courseenrollment'' THEN 
	   			   			TTEOR.newDestinationId
	   			 		 WHEN ''lesson'' THEN 
	   			 			TTDLOR.newDestinationId
	   			 		 When ''standuptraininginstance'' THEN 
	   			 			TTSTI.newDestinationId
	   			 		 WHEN ''learningpathenrollment'' THEN  
	   			 			TTLPEOR.newDestinationId
	   			 		 WHEN ''certificate'' THEN 
	   			 			TTCROR.newDestinationId
	   			 		 ELSE  
	   			 			TTELog.newDestinationId
	   			 	 END ,'
	   			 +'	 EEQ.objectType,'
	   			 +'	 TTOU.newDestinationId,'
	   			 +'	 EEQ.objectUserFullName,'
	   			 +'	 EEQ.objectUserFirstName,'
	   			 +'	 EEQ.objectUserLogin,'
	   			 +'	 EEQ.objectUserEmail,'
	   			 +'	 TTUR.newDestinationId,'
	   			 +'	 EEQ.recipientLangString,'
	   			 +'	 EEQ.recipientFullName,'
	   			 +'	 EEQ.recipientFirstName,'
	   			 +'	 EEQ.recipientLogin,'
	   			 +'	 EEQ.recipientEmail,'
	   			 +'	 EEQ.recipientTimezone,'
	   			 +'	 EEQ.recipientTimezoneDotNetName,'
	   			 +'	 EEQ.[from],'
	   			 +'	 EEQ.[priority],'
	   			 +'	 EEQ.dtEvent,'
	   			 +'	 EEQ.dtAction,'
	   			 +'	 EEQ.dtActivation,'
	   			 +'	 EEQ.dtSent,'
	   			 +'	 EEQ.statusDescription '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblEventEmailQueue] EEQ '
	   			 +' LEFT JOIN #EventLogIdMappings TTELog on (TTELog.oldSourceId = EEQ.idEventLog ) '
	   			 +' LEFT JOIN #EventEmailNotificationIdMappings TTEEN on (TTEEN.oldSourceId = EEQ.idEventEmailNotification ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = EEQ.idObject ) ' 
	   			 +' LEFT JOIN #UserIdMappings TTOU on (TTOU.oldSourceId = EEQ.idObjectUser ) ' 
	   			 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = EEQ.idObject )'
	   			 +' LEFT JOIN #LessonIdMappings TTL on (TTL.oldSourceId = EEQ.idObject ) '
	   			 +' LEFT JOIN #StandUpTrainingInstanceIdMappings TTSTI on (TTSTI.oldSourceId = EEQ.idObject ) '
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = EEQ.idObject ) '
	   			 +' LEFT JOIN #CertificateIdMappings TTCE on (TTCE.oldSourceId = EEQ.idObject ) '
	   			 +' LEFT JOIN #EnrollmentIdMappings TTEOR on (TTEOR.oldSourceId = EEQ.idObjectRelated ) '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDLOR on (TTDLOR.oldSourceId = EEQ.idObjectRelated )'
	   			 +' LEFT JOIN #LearningPathEnrollmentIdMappings TTLPEOR on (TTLPEOR.oldSourceId = EEQ.idObjectRelated ) '
	   			 +' LEFT JOIN #CertificateRecordIdMappings TTCROR on (TTCROR.oldSourceId = EEQ.idObjectRelated ) '
	   			 +' LEFT JOIN #UserIdMappings TTUR on (TTUR.oldSourceId = EEQ.idRecipient ) ' 
	   			 +' WHERE EEQ.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)

	   

	   /*
	   DROP temporary table EventEmailNotificationIdMappings
	   */
	   DROP TABLE #EventEmailNotificationIdMappings
	   
	   /*
	   Drop temporary table EventLogIdMappings
	   */
	   DROP TABLE #EventLogIdMappings
	    
	   /* 
	   Drop temporary table StandUpTrainingInstanceIdMappings
	   */
	   DROP TABLE #StandUpTrainingInstanceIdMappings
	     
	   /* 
	   Drop temporary table CertificateIdMappings
	   */
	   DROP TABLE #CertificateIdMappings
	   
	   /*  
	   Drop temporary table CertificateRecordIdMappings
	   */
	   DROP TABLE #CertificateRecordIdMappings
	   
	   /*
	   Drop temporary table ActivityImportIdMappings
	   */
	   DROP TABLE #ActivityImportIdMappings
	   

-- =============================================================================
-- LOGIC START -- copy tblUserToLearningPathLink object for full copy ----------


	   -- insert records from source site to destination table UserToLearningPathLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUserToLearningPathLink]'
	   			 +' ( '
	   			 +'	 idSite,' 
	   			 +'	 idUser,' 
	   			 +'	 idLearningPath,' 
	   			 +'	 idRuleSet,' 
	   			 +'	 created'  
	   			 +' ) '
	   			 +' SELECT '
	   		  	 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTU.newDestinationId,' 
	   			 +'   TTLP.newDestinationId,' 
	   			 +'	 TTRS.newDestinationId,' 
	   			 +'	 ULPL.created '  
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUserToLearningPathLink] ULPL'
	   			 +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblLearningPath] LP ON LP.idLearningPath= ULPL.idLearningPath '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = ULPL.idUser)'
	   			 +' LEFT JOIN #LearningPathIdMappings TTLP on (TTLP.oldSourceId = ULPL.idLearningPath)'
	   			 +' LEFT JOIN #RuleSetIdMappings TTRS on (TTRS.oldSourceId = ULPL.idRuleSet)'
	   			 +' WHERE LP.idSite = '+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)


	   /*
	   Drop temporary table LearningPathEnrollmentIdMappings
	   */
	   DROP TABLE #LearningPathEnrollmentIdMappings

	   
    END

------------------------------------------------------------------------------
-- full copy end


	     /*
	   Drop temporary table DocumentRepositoryFolderIdMappings, temporary table DocumentRepositoryFolder will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #DocumentRepositoryFolderIdMappings

	   /*
	   Drop temporary table RuleSetEnrollmentIdMappings, temporary table RuleSetEnrollment will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #RuleSetEnrollmentIdMappings

	   /*
	   Drop temporary tableGroupEnrollmentIdMappings, temporary table GroupEnrollment will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #GroupEnrollmentIdMappings
	   
	   /*
	   Drop temporary table RulesetIdMappings, temporary table Ruleset will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #RulesetIdMappings

    	   /*   
	   Drop temporary table LearningPathIdMappings, temporary table LearningPath will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #LearningPathIdMappings
	   
	   /*  
	   Drop temporary table LessonIdMappings, temporary table Lesson will be dorp in both conditions full copy and partial copy 
	   */
	   DROP TABLE #LessonIdMappings

------------------------------------------------------------------------------

	   
	   -- check condition for the full copy
	   IF @fullCopyFlag = 1
	   BEGIN



-- =============================================================================
-- LOGIC START -- copy tblData-SCO object for full copy ------------------------


	   -- insert records from source site to destination table Data-SCO
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-SCO]'
	   			 +' ( '
	   			 +'   idSite,'  
	   			 +'   [idData-Lesson],'   							
	   			 +'   idTimezone,'
	   			 +'   manifestIdentifier,'
	   			 +'   completionStatus,'
	   			 +'   successStatus,'
	   			 +'   scoreScaled,'
	   			 +'   totalTime,'
	   			 +'   [timestamp],'
	   			 +'   actualAttemptCount,'
	   			 +'   effectiveAttemptCount,'
	   			 +'   proctoringUser '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTDL.newDestinationId,'
	   			 +'	 DS.idTimezone,'
	   			 +'	 DS.manifestIdentifier,'
	   			 +'	 DS.completionStatus,'
	   			 +'	 DS.successStatus,'
	   			 +'	 DS.scoreScaled,'
	   			 +'	 DS.totalTime,'
	   			 +'	 DS.[timestamp],'
	   			 +'	 DS.actualAttemptCount,'
	   			 +'	 DS.effectiveAttemptCount,'
	   			 +'	 TTU.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-SCO] DS '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDL on (TTDL.oldSourceId = DS.[idData-Lesson] ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = DS.[proctoringUser] ) '
	   			 +' WHERE DS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)					
	   
	   -- temporary table to hold the idData-SCO of source and destination Data-SCO tables
	   CREATE TABLE #DataSCOIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	
	   -- inserting idData-SCO of source Data-SCO table and destination Data-SCO table inside temporary table for mapping	
	   SET @sql =	   ' INSERT INTO #DataSCOIdMappings '
	   			  +' ( '
	   			  +'	 oldSourceId,
	   			  	 newDestinationId'
	   			  +'  ) '
	   			  +' SELECT '
	   			  +'	 SDS.[idData-SCO],'
	   			  +'	 DDS.[idData-SCO]'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-SCO] DDS'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-SCO] SDS'
	   			  +' ON (SDS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SDS.completionStatus = DDS.completionStatus )'
	   			  +' AND (SDS.successStatus = DDS.successStatus )'
	   			  +' AND (SDS.scoreScaled = DDS.scoreScaled OR (SDS.scoreScaled IS NULL AND DDS.scoreScaled IS NULL))'
	   			  +' AND (SDS.totalTime = DDS.totalTime OR (SDS.totalTime IS NULL AND DDS.totalTime IS NULL))'
	   			  +' AND (SDS.[timestamp] = DDS.[timestamp] OR (SDS.[timestamp] IS NULL AND DDS.[timestamp] IS NULL))'
	   			  +'  )'
	   			  +' WHERE DDS.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)
	   


-- =============================================================================
-- LOGIC START -- copy tblData-HomeworkAssignment object for full copy ---------


	   -- insert records from source site to destination table Data-HomeworkAssignment
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-HomeworkAssignment]'
	   			 +' ( '
	   			 +'	 idSite,'     
	   			 +'	 [idData-Lesson],' 							
	   			 +'	 uploadedAssignmentFilename,'
	   			 +'	 dtUploaded,'
	   			 +'	 completionStatus,'
	   			 +'	 successStatus,'
	   			 +'	 score,'
	   			 +'	 [timestamp],'
	   			 +'	 proctoringUser'
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTDL.newDestinationId,'
	   			 +'	 DHA.uploadedAssignmentFilename,'
	   			 +'	 DHA.dtUploaded,'
	   			 +'	 DHA.completionStatus,'
	   			 +'	 DHA.successStatus,'
	   			 +'	 DHA.score,'
	   			 +'	 DHA.[timestamp],'
	   			 +'	 TTU.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-HomeworkAssignment] DHA '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDL on (TTDL.oldSourceId = DHA.[idData-Lesson] ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = DHA.[proctoringUser] ) '
	   			 +' WHERE DHA.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)	



-- =============================================================================
-- LOGIC START -- copy tblxAPIoAuthConsumer object for full copy ---------------


	   -- insert records from source site to destination table xAPIoAuthConsumer
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer]'
	   			 +' ( '
	   			 +'   idSite,'  
	   			 +'   label,'
	   			 +'   oAuthKey,'
	   			 +'   oAuthSecret,'
	   			 +'   isActive '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  XAPIAC.label,'
	   			 +'   XAPIAC.oAuthKey,'
	   			 +'   XAPIAC.oAuthSecret,'
	   			 +'   XAPIAC.isActive '		
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer] XAPIAC'
	   			 +' WHERE XAPIAC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   								
	   EXEC(@sql)	
			
	   -- temporary table to hold the idxAPIoAuthConsumer of source and destination xAPIoAuthConsumer tables
	   CREATE TABLE #xAPIoAuthConsumerIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	

	   -- inserting idxAPIoAuthConsumer of source xAPIoAuthConsumer table and destination xAPIoAuthConsumer table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #xAPIoAuthConsumerIdMappings '
	   			  +' ( '
	   			  +'	 oldSourceId,
	   			  	 newDestinationId'
	   			  +'  ) '
	   			  +' SELECT 
					   SP.idxAPIoAuthConsumer, 
					   DP.idxAPIoAuthConsumer '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer] DP'
	   			  +' INNER JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer] SP'
	   			  +' ON (SP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SP.label = DP.label OR (SP.label IS NULL AND DP.label IS NULL))' 
	   			  +' AND (SP.oAuthKey = DP.oAuthKey OR (SP.oAuthKey IS NULL AND DP.oAuthKey IS NULL))' 
	   			  +' AND (SP.oAuthSecret = DP.oAuthSecret OR (SP.oAuthSecret IS NULL AND DP.oAuthSecret IS NULL))'					
	   			  +' )'
	   			  +' WHERE DP.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)	



-- =============================================================================
-- LOGIC START -- copy tblData-TinCan object for full copy ---------------------


	   -- insert records from source site to destination table Data-TinCan
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCan]'
	   			 +' ( '
	   			 +'  [idData-Lesson], '
	   		   	 +'  [idData-TinCanParent] , '
	   		   	 +'  idSite,  '
	   		   	 +'	idEndpoint, '
	   		   	 +'	isInternalAPI, '
	   		   	 +'	statementId, '
	   		   	 +'	actor, '
	   		   	 +'	verbId, '
	   		   	 +'	verb, '
	   		   	 +'	activityId, '
	   		   	 +'	mboxObject, '
	   		      +'  mboxSha1SumObject, '
	   		   	 +'	openIdObject, '
	   		   	 +'	accountObject, '
	   		   	 +'	mboxActor, '
	   		   	 +'	mboxSha1SumActor, '
	   		   	 +'	openIdActor, '
	   		   	 +'	accountActor, '
	   		   	 +'	mboxAuthority, '
	   		   	 +'	mboxSha1SumAuthority, '
	   		      +'  openIdAuthority, '
	   		   	 +'	accountAuthority, '
	   		   	 +'	mboxTeam, '
	   		   	 +'	mboxSha1SumTeam, '
	   		   	 +'	openIdTeam, '
	   		   	 +'	accountTeam, '
	   		   	 +'	mboxInstructor, '
	   		   	 +'	mboxSha1SumInstructor, '
	   		   	 +'	openIdInstructor, '
	   		      +'  accountInstructor, '
	   		   	 +'	[object], '
	   		   	 +'	registration, '
	   		   	 +'	[statement], '
	   		   	 +'	isVoidingStatement, '
	   		   	 +'	isStatementVoided, '
	   		   	 +'	dtStored, '
	   		   	 +'	scoreScaled '
	   			 +' ) '
	   			 +' SELECT '
	   			 +'  TTDL.newDestinationId, '
	   		   	 +'  NULL , '			     -- this null value of [idData-TinCanParent] will be updated from temporary DataTinCanIdMappings table below 
	   		   	 +   CAST(@idSiteDestination AS NVARCHAR)
	   		   	 +',	TTXAAC.newDestinationId, '
	   		   	 +'	DTC.isInternalAPI, '
	   		   	 +'	DTC.statementId, '
	   		   	 +'	DTC.actor, '
	   		   	 +'	DTC.verbId, '
	   		   	 +'	DTC.verb, '
	   		   	 +'	DTC.activityId, '
	   		   	 +'	DTC.mboxObject, '
	   		      +'  DTC.mboxSha1SumObject, '
	   		   	 +'	DTC.openIdObject, '
	   		   	 +'	DTC.accountObject, '
	   		   	 +'	DTC.mboxActor, '
	   		   	 +'	DTC.mboxSha1SumActor, '
	   		   	 +'	DTC.openIdActor, '
	   		   	 +'	DTC.accountActor, '
	   		   	 +'	DTC.mboxAuthority, '
	   		   	 +'	DTC.mboxSha1SumAuthority, '
	   		      +'  DTC.openIdAuthority, '
	   		   	 +'	DTC.accountAuthority, '
	   		   	 +'	DTC.mboxTeam, '
	   		   	 +'	DTC.mboxSha1SumTeam, '
	   		   	 +'	DTC.openIdTeam, '
	   		   	 +'	DTC.accountTeam, '
	   		   	 +'	DTC.mboxInstructor, '
	   		   	 +'	DTC.mboxSha1SumInstructor, '
	   		   	 +'	DTC.openIdInstructor, '
	   		      +'  DTC.accountInstructor, '
	   		   	 +'	DTC.[object], '
	   		   	 +'	DTC.registration, '
	   		   	 +'	DTC.[statement], '
	   		   	 +'	DTC.isVoidingStatement, '
	   		   	 +'	DTC.isStatementVoided, '
	   		   	 +'	DTC.dtStored, '
	   		   	 +'	DTC.scoreScaled '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCan] DTC '
	   			 +' LEFT JOIN #DataLessonIdMappings TTDL on TTDL.oldSourceId = DTC.[idData-Lesson] '
	   			 +' LEFT JOIN #xAPIoAuthConsumerIdMappings TTXAAC on TTXAAC.oldSourceId = DTC.[idEndpoint] '
	   			 +' WHERE DTC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)					
	   
	   -- temporary table to hold the idData-TinCan of source and destination Data-TinCan tables
	   CREATE TABLE #DataTinCanIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idData-TinCan of source Data-TinCan table and destination Data-TinCan table inside temporary table for mapping	
	   SET @sql =	   ' INSERT INTO #DataTinCanIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT '
	   			  +'	  SDS.[idData-TinCan],'
	   			  +'	  DDS.[idData-TinCan]'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCan] DDS'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCan] SDS'
	   			  +' ON (SDS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SDS.statementId = DDS.statementId OR (SDS.statementId IS NULL AND DDS.statementId IS NULL))'
	   			  +' AND (SDS.dtStored = DDS.dtStored )'
	   			  +' )'
	   			  +' WHERE DDS.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)
	   
	   -- update [idData-TinCanParent] from temp table DataTinCan
	   SET @sql =	  ' UPDATE DDTC'
	   			 +'  SET [idData-TinCanParent] = TEMP.newDestinationId'
	   			 +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCan] SDTC'
	   			 +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCan] DDTC'
	   			 +' ON( SDTC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			 +' AND (SDTC.statementId = DDTC.statementId OR (SDTC.statementId IS NULL AND DDTC.statementId IS NULL))'
	   			 +' AND (SDTC.dtStored = DDTC.dtStored OR (SDTC.dtStored IS NULL AND DDTC.dtStored IS NULL))'
	   			 +' )'
	   			 +' INNER JOIN  #DataTinCanIdMappings TEMP'
	   			 +' ON(TEMP.oldSourceId = SDTC.[idData-TinCanParent])'
	   			 +' WHERE DDTC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			 +' AND SDTC.[idData-TinCanParent] IS NOT NULL'
	   
	   EXEC(@sql)	



-- =============================================================================
-- LOGIC START -- copy tblxAPIoAuthNonce object for full copy ------------------


	   -- insert records from source site to destination table xAPIoAuthNonce
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblxAPIoAuthNonce]'
	   			 +' ( '			   
	   		   	 +'   idxAPIoAuthConsumer,  '   
	   		   	 +'	 nonce '   					 
	   			 +' ) '
	   			 +' SELECT '
	   		   	 +'	 TTXAAC.newDestinationId, '
	   		   	 +'	 xAPIAN.nonce '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblxAPIoAuthNonce] xAPIAN '  
	   			 +' INNER JOIN  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblxAPIoAuthConsumer] xAPIAC  ON  xAPIAN.idxAPIoAuthConsumer= xAPIAC.idxAPIoAuthConsumer ' 
	   			 +' LEFT JOIN #xAPIoAuthConsumerIdMappings TTXAAC on TTXAAC.oldSourceId = xAPIAN.[idxAPIoAuthConsumer] '
	   			 +' WHERE xAPIAC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		
	   
	   EXEC(@sql)	



-- =============================================================================
-- LOGIC START -- copy tblData-TinCanProfile object for full copy --------------


	   -- insert records from source site to destination table Data-TinCanProfile
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCanProfile]'
	   			 +' ( '			   
	   		   	 +'   idSite,  '   
	   		   	 +'	 idEndpoint, '
	   		   	 +'	 profileId, '
	   		   	 +'	 activityId, '
	   		   	 +'	 agent, '
	   		   	 +'	 contentType, '
	   		   	 +'	 docContents, '
	   		   	 +'	 dtUpdated '
	   			 +' ) '
	   			 +' SELECT '
	   		   	 +    CAST(@idSiteDestination AS NVARCHAR)
	   		   	 +',	 TTXAAC.newDestinationId, '
	   		   	 +'	 DTCP.profileId, '
	   		   	 +'	 DTCP.activityId, '
	   		   	 +'	 DTCP.agent, '
	   		   	 +'	 DTCP.contentType, '
	   		   	 +'	 DTCP.docContents, '
	   		   	 +'	 DTCP.dtUpdated  '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCanProfile] DTCP '
	   			 +' LEFT JOIN #xAPIoAuthConsumerIdMappings TTXAAC on TTXAAC.oldSourceId = DTCP.[idEndpoint] '
	   			 +' WHERE DTCP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)		



-- =============================================================================
-- LOGIC START -- copy tblData-TinCanState object for full copy ----------------


	   -- insert records from source site to destination table Data-TinCanState
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCanState]'
	   			 +' ( '			   
	   		   	 +'   idSite,  '   
	   		   	 +'	 idEndpoint, '   					 
	   		   	 +'	 stateId, '
	   		   	 +'	 agent, '
	   		   	 +'	 activityId, '
	   		   	 +'	 registration, '
	   		   	 +'	 contentType, '
	   		   	 +'	 docContents, '
	   		   	 +'	 dtUpdated '
	   			 +' ) '
	   			 +' SELECT '
	   		   	 +    CAST(@idSiteDestination AS NVARCHAR)
	   		   	 +',	 TTXAAC.newDestinationId, '
	   		   	 +'	 DTCS.stateId, '
	   		   	 +'	 DTCS.agent, '
	   		   	 +'	 DTCS.activityId, '
	   		   	 +'	 DTCS.registration, '
	   		   	 +'	 DTCS.contentType, '
	   		   	 +'	 DTCS.docContents,  '
	   		   	 +'	 DTCS.dtUpdated  '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCanState] DTCS '
	   			 +' LEFT JOIN #xAPIoAuthConsumerIdMappings TTXAAC on TTXAAC.oldSourceId = DTCS.[idEndpoint] '
	   			 +' WHERE DTCS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)	

	
	   /*
	   Drop temporary table xAPIoAuthConsumerIdMappings
	   */
	   DROP TABLE #xAPIoAuthConsumerIdMappings
	   
	   /*
	   Drop temporary table DataLessonIdMappings
	   */
	   DROP TABLE #DataLessonIdMappings



-- =============================================================================
-- LOGIC START -- copy tblData-TinCanContextActivities object for full copy ----


	   -- insert records from source site to destination table Data-TinCanContextActivities
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-TinCanContextActivities]'
	   			 +' ( '
	   			 +'   [idData-TinCan],'          
	   			 +'   idSite,'          
	   			 +'   activityId,' 							
	   			 +'   activityType,'
	   			 +'   objectType '
	   			 +' ) '
	   			 +' SELECT '
	   			 +'   TTDTC.newDestinationId,'
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  DTCA.activityId,'
	   			 +'	 DTCA.activityType,'
	   			 +'	 DTCA.objectType '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-TinCanContextActivities] DTCA '
	   			 +' LEFT JOIN #DataTinCanIdMappings TTDTC on (TTDTC.oldSourceId = DTCA.[idData-TinCan] ) '
	   			 +' WHERE DTCA.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)	


	   /*
	   Drop temporary table DataTinCanIdMappings
	   */
	   DROP TABLE #DataTinCanIdMappings



-- =============================================================================
-- LOGIC START -- copy tblData-SCOInt object for full copy ---------------------


	   -- insert records from source site to destination table Data-SCOInt
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-SCOInt]'
	   			 +' ( '
	   			 +'   idSite,'        
	   			 +'   [idData-SCO],' 							
	   			 +'   [identifier],'
	   			 +'   [timestamp], '
	   			 +'   [result], '
	   			 +'   [latency], '
	   			 +'   [scoIdentifier] '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTDS.newDestinationId,'
	   			 +'   DSI.identifier,'
	   			 +'	 DSI.[timestamp],'
	   			 +'	 DSI.result, '
	   			 +'	 DSI.latency, '
	   			 +'	 DSI.scoIdentifier '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-SCOInt] DSI '
	   			 +' LEFT JOIN #DataSCOIdMappings TTDS on (TTDS.oldSourceId = DSI.[idData-SCO] ) '
	   			 +' WHERE DSI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   
	   EXEC(@sql)		


-- =============================================================================
-- LOGIC START -- copy tblData-SCOObj object for full copy ---------------------


	   -- insert records from source site to destination table Data-SCOObj
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-SCOObj]'
	   			 +' ( '
	   			 +'   idSite,'        
	   			 +'   [idData-SCO],' 			 
	   			 +'   [identifier],'
	   			 +'   [scoreScaled], '
	   			 +'   [completionStatus], '
	   			 +'   [successStatus] '
	   			 +' ) '
	   			 +' SELECT '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TTDS.newDestinationId,'
	   			 +'   DSO.identifier,'
	   			 +'	 DSO.scoreScaled,'
	   			 +'	 DSO.completionStatus, '
	   			 +'	 DSO.successStatus '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-SCOObj] DSO '
	   			 +' LEFT JOIN #DataSCOIdMappings TTDS on (TTDS.oldSourceId = DSO.[idData-SCO] ) '
	   			 +' WHERE DSO.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)				


	   /*
	   Drop temporary table DataSCOIdMappings
	   */
	   DROP TABLE #DataSCOIdMappings



-- =============================================================================
-- LOGIC START -- copy tblPurchase object for full copy ------------------------


	   -- insert records from source site to destination table Purchase
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblPurchase]'
	   			 +' ( '
	   			 +'   idUser,'
	   			 +'   orderNumber,'
	   			 +'   timeStamp,'
	   			 +'   ccnum,'
	   			 +'   currency,'
	   			 +'   idSite,'
	   			 +'   dtPending,'
	   			 +'   failed,'
	   			 +'   transactionId '
	   			 +' ) '
	   			 +' SELECT '
	   			 +'   TTU.newDestinationId,'
	   			 +'   TP.orderNumber,'
	   			 +'   TP.timeStamp,'
	   			 +'   TP.ccnum,'
	   			 +'   TP.currency,'
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +',  TP.dtPending,'
	   			 +'   TP.failed, '
	   			 +'	 TP.transactionId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblPurchase] TP '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = TP.idUser ) '
	   			 +' WHERE TP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   								
	   EXEC(@sql)	
		
	   -- temporary table to hold the idPurchase of source and destination Purchase tables
	   CREATE TABLE #PurchaseIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idPurchase of source Purchase table and destination Purchase table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #PurchaseIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SP.idPurchase, 
					   DP.idPurchase '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblPurchase] DP'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblPurchase] SP'
	   			  +' ON (SP.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SP.orderNumber = DP.orderNumber )' 
	   			  +' AND (SP.timeStamp = DP.timeStamp OR (SP.timeStamp IS NULL AND DP.timeStamp IS NULL))' 
	   			  +' AND (SP.dtPending = DP.dtPending OR (SP.dtPending IS NULL AND DP.dtPending IS NULL))'					
	   			  +' )'
	   			  +' WHERE DP.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)	

	  

-- =============================================================================
-- LOGIC START -- copy tblTransactionItem object for full copy -----------------


	   -- insert records from source site to destination table TransactionItem
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblTransactionItem]'
	   			 +' ( '
	   			 +'   idParentTransactionItem,'
	   			 +'   idPurchase,'
	   			 +'   idEnrollment,'
	   			 +'   idUser,'
	   			 +'   userName,'
	   			 +'   idAssigner,'
	   			 +'   assignerName,'
	   			 +'   itemId,'
	   			 +'   itemName,'
	   			 +'   itemType,'
	   			 +'   description,'
	   			 +'   cost,'
	   			 +'   paid,'
	   			 +'   idCouponCode,'
	   			 +'   couponCode,'
	   			 +'   dtAdded,'
	   			 +'   confirmed,'
	   			 +'   idSite'
	   			 +'  ) '
	   			 +' SELECT '
	   			 +'   NULL ,'     -- this null value of idParentTransactionItem will be updated from temporary TransactionItemIdMappings table below
	   			 +'   TTP.newDestinationId,'  
	   			 +'   TTE.newDestinationId,'
	   			 +'   TTU.newDestinationId,'
	   			 +'   TI.userName,'
	   			 +'   TTUA.newDestinationId,'
	   			 +'   TI.assignerName,'
	   			 +'   TI.itemId,'
	   			 +'   TI.itemName,'
	   			 +'   TI.itemType,'
	   			 +'   TI.description,'
	   			 +'   TI.cost,'
	   			 +'   TI.paid,'
	   			 +'   TTCC.newDestinationId,'
	   			 +'   TI.couponCode,'
	   			 +'   TI.dtAdded,'
	   			 +'   TI.confirmed , '
	   			 +    CAST(@idSiteDestination AS NVARCHAR)
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblTransactionItem] TI '
	   			 +' LEFT JOIN #PurchaseIdMappings TTP on (TTP.oldSourceId = TI.idPurchase ) '
	   			 +' LEFT JOIN #EnrollmentIdMappings TTE on (TTE.oldSourceId = TI.idEnrollment ) '
	   			 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = TI.idUser ) '
	   			 +' LEFT JOIN #UserIdMappings TTUA on (TTUA.oldSourceId = TI.idAssigner )'
	   			 +' LEFT JOIN #CouponCodeIdMappings TTCC on (TTCC.oldSourceId = TI.idCouponCode ) '
	   			 +' WHERE TI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	  
	   -- temporary table to hold the idTransactionItem of source and destination TransactionItem tables
	   CREATE TABLE #TransactionItemIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idTransactionItem of source TransactionItem table and destination TransactionItem table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #TransactionItemIdMappings '
	   			  +' ( '
	   			  +'	   oldSourceId,
	   				   newDestinationId'
	   			  +' ) '
	   			  +' SELECT '
	   			  +'	   STI.[idTransactionItem],'
	   			  +'	   DTI.[idTransactionItem] '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblTransactionItem] DTI'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblTransactionItem] STI'
	   			  +' ON (STI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (STI.itemName = DTI.itemName OR (STI.itemName IS NULL AND DTI.itemName IS NULL))'
	   			  +' AND (STI.userName = DTI.userName OR (STI.userName IS NULL AND DTI.userName IS NULL))'
	   			  +' AND (STI.dtAdded = DTI.dtAdded OR (STI.dtAdded IS NULL AND DTI.dtAdded IS NULL))'
	   			  +' AND (STI.confirmed = DTI.confirmed OR (STI.confirmed IS NULL AND DTI.confirmed IS NULL))'
	   			  +' )'
	   			  +' WHERE DTI.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			
	   EXEC(@sql)


	   -- update idParentTransactionItem from Temp Table TransactionItem
	   SET @sql =	   ' UPDATE DTI '
	   			  +'  SET idParentTransactionItem = TEMP.newDestinationId'
	   			  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblTransactionItem] STI'
	   			  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblTransactionItem] DTI'
	   			  +' ON( STI.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (STI.itemName = DTI.itemName OR (STI.itemName IS NULL AND DTI.itemName IS NULL))'
	   			  +' AND (STI.userName = DTI.userName OR (STI.userName IS NULL AND DTI.userName IS NULL))'
	   			  +' AND (STI.dtAdded = DTI.dtAdded OR (STI.dtAdded IS NULL AND DTI.dtAdded IS NULL))'
	   			  +' AND (STI.confirmed = DTI.confirmed OR (STI.confirmed IS NULL AND DTI.confirmed IS NULL))'
	   			  +'  )'
	   			  +' INNER JOIN  #TransactionItemIdMappings TEMP'
	   			  +' ON(TEMP.oldSourceId = STI.idParentTransactionItem)'
	   			  +' WHERE DTI.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND STI.idParentTransactionItem IS NOT NULL'
	   
	   EXEC(@sql)	

	   
	   /*
	   Drop temporary table TransactionItemIdMappings
	   */
	   DROP TABLE #TransactionItemIdMappings

	   /*
	   Drop temporary table PurchaseIdMappings
	   */
	   DROP TABLE #PurchaseIdMappings
	    
	   /*   
	   Drop temporary table EnrollmentIdMappings
	   */
	   DROP TABLE #EnrollmentIdMappings
	   
	   /*
	   Drop temporary table CouponCodeIdMappings
	   */
	   DROP TABLE #CouponCodeIdMappings


    END

---------------------------------------------------------------------------------------
-- full copy end


-- =====================================================================================
--LOGIC START -- copy tblCertificationType object both full copy and partial copy ------

			-- insert records from source site to destination table CertificationType
	          SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationType]'
              		+ ' ( '
              		+ '   idSite,
					 title,
					 shortDescription,
					 isDeleted,
					 dtDeleted'
              		+ ' ) '
              		+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              		+' ,  CT.title,
					 CT.shortDescription,
				      CT.isDeleted,
				      CT.dtDeleted					
              		 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationType] CT'
				+' WHERE CT.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
				
              
              EXEC(@sql)
						
				-- temporary table to hold the idCertificationType of source and destination CertificationType tables
				CREATE TABLE #CertificationTypeIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)
				
				-- inserting idCertificationType of source CertificationType table and destination CertificationType table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationTypeIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationType, DST.idCertificationType'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationType] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationType] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.title = DST.title)'
						+' AND (SRC.isDeleted = DST.isDeleted)'
						+' AND (SRC.shortDescription = DST.shortDescription OR (SRC.shortDescription IS NULL AND DST.shortDescription IS NULL))'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
			
				EXEC(@sql)


-- ============================================================================================
--LOGIC START -- copy tblCertification object both full copy and partial copy with condition---

	 -- insert records from source site to destination table Certification
	 SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertification]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationType, 
						 title,
						 shortDescription,
						 searchTags,
						 dtCreated,
						 dtModified,
						 isDeleted,
						 dtDeleted,
						 initialAwardExpiresInterval,
						 initialAwardExpiresTimeframe,
						 renewalExpiresInterval,
						 renewalExpiresTimeframe,
						 accreditingOrganization,
						 certificationContactName,
						 certificationContactEmail,
						 certificationContactPhoneNumber,
						 isPublished,
						 isClosed,
						 isAnyModule,
						 idCertificationContact'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCT.newDestinationId, 
						 C.title,
						 C.shortDescription,
						 C.searchTags,
						 C.dtCreated,
						 C.dtModified,
						 C.isDeleted,
						 C.dtDeleted,
						 C.initialAwardExpiresInterval,
						 C.initialAwardExpiresTimeframe,
						 C.renewalExpiresInterval,
						 C.renewalExpiresTimeframe,
						 C.accreditingOrganization,
						 C.certificationContactName,
						 C.certificationContactEmail,
						 C.certificationContactPhoneNumber,
						 C.isPublished,
						 C.isClosed,
						 C.isAnyModule,'

		IF(@fullCopyFlag = 1) -- idCertificationContact is available for full copy only, because tblUser moved only for full copy.
		  BEGIN
			 SET @subSql =' TTU.newDestinationId '				
              			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertification] C'
					 +' LEFT JOIN #CertificationTypeIdMappings TTCT on (TTCT.oldSourceId = C.idCertificationType)'
					 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = C.idCertificationContact)'
					 +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
            END
		ELSE				  -- idCertificationContact is available for full copy only, so in partial copy we send Admin user id "1" .
		  BEGIN				 
			  SET @subSql = ' 1 '	
			               +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertification] C'
					     +' LEFT JOIN #CertificationTypeIdMappings TTCT on (TTCT.oldSourceId = C.idCertificationType)'
					     +' WHERE C.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
		  END
		  
		SET @sql = @sql + @subSql
		EXEC(@sql)		
                       
							
				-- temporary table to hold the idCertification of source and destination Certification tables
				CREATE TABLE #CertificationIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)
				
				-- inserting idCertification of source Certification table and destination Certification table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertification, DST.idCertification'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertification] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertification] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.title = DST.title)'
						+' AND (SRC.dtCreated = DST.dtCreated)'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

				

              	--insert tamplate record into table CertificationLanguage
              	SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationLanguage]'
              			+ ' ( '
              			+ '  idSite, 
              				idCertification, 
              				idLanguage, 
              				title, 
              				shortDescription,
						searchTags'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' , TTC.newDestinationId, 
              				CL.idLanguage, 
              				CL.title, 
              				CL.shortDescription,
						CL.searchTags 
              			FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationLanguage] CL'
              			+' LEFT JOIN #CertificationIdMappings TTC on (TTC.oldSourceId = CL.idCertification )'
              			+' WHERE CL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
                            
              	
              	EXEC(@sql)
              	              
	   /*    
	   Drop temporary table CertificationTypeIdMappings, temporary table CertificationType will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationTypeIdMappings

-- =============================================================================
 --LOGIC START -- copy tblCertificationModule object both full copy and partial copy ----------------

		   -- insert records from source site to destination table CertificationModule
		   SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModule]'
              			+ ' ( '
              			+ '   idSite,
						 idCertification,
						 title,
						 shortDescription,
						 isAnyRequirementSet,
						 dtCreated,
						 dtModified,
						 isDeleted,
						 dtDeleted'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTC.newDestinationId,
						 CM.title,
						 CM.shortDescription,
						 CM.isAnyRequirementSet,
						 CM.dtCreated,
						 CM.dtModified,
						 CM.isDeleted,
						 CM.dtDeleted					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModule] CM'
					 +' LEFT JOIN #CertificationIdMappings TTC on (TTC.oldSourceId = CM.idCertification)'
					+' WHERE CM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              			
              
              EXEC(@sql)
					
				-- temporary table to hold the idCertificationModule of source and destination CertificationModule tables
				CREATE TABLE #CertificationModuleIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)
				
				-- inserting idCertificationModule of source CertificationModule table and destination CertificationModule table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationModuleIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationModule, DST.idCertificationModule'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModule] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModule] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.title = DST.title)'
						+' AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL))'
						+' AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

				
              	--insert tamplate record into table CertificationModuleLanguage
              	SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleLanguage]'
              			+ ' ( '
              			+ '  idSite, 
              				idCertificationModule, 
              				idLanguage, 
              				title, 
              				shortDescription'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' , TTCM.newDestinationId, 
              				CML.idLanguage, 
              				CML.title, 
              				CML.shortDescription
              			FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleLanguage] CML'
              			+' LEFT JOIN #CertificationModuleIdMappings TTCM on (TTCM.oldSourceId = CML.idCertificationModule )'
              			+' WHERE CML.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
              	EXEC(@sql)
				
-- ====================================================================================================
 --LOGIC START -- copy tblCertificationModuleRequirementSet object both full copy and partial copy -----

		  -- insert records from source site to destination table CertificationModuleRequirementSet
		  SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSet]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationModule,
						 isAny,
						 label'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCM.newDestinationId,
						 CMRS.isAny,
						 CMRS.label					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSet] CMRS'
					 +' LEFT JOIN #CertificationModuleIdMappings TTCM on (TTCM.oldSourceId = CMRS.idCertificationModule)'
					+' WHERE CMRS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              			
              
              EXEC(@sql)
				
				-- temporary table to hold the idCertificationModuleRequirementSet of source and destination CertificationModuleRequirementSet tables
				CREATE TABLE #CertificationModuleRequirementSetIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)
				
				-- inserting idCertificationModuleRequirementSet of source CertificationModuleRequirementSet table and destination CertificationModuleRequirementSet table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationModuleRequirementSetIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationModuleRequirementSet, DST.idCertificationModuleRequirementSet'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSet] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSet] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.label = DST.label)'
						+' AND (SRC.isAny = DST.isAny)'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

              	--insert tamplate record into table CertificationModuleRequirementSetLanguage
              	SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSetLanguage]'
              			+ ' ( '
              			+ '  idSite, 
              				idCertificationModuleRequirementSet, 
              				idLanguage, 
              				label'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' , TTCMRS.newDestinationId, 
              				CMRSL.idLanguage, 
              				CMRSL.label
              			FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementSetLanguage] CMRSL'
              			+' LEFT JOIN #CertificationModuleRequirementSetIdMappings TTCMRS on (TTCMRS.oldSourceId = CMRSL.idCertificationModuleRequirementSet )'
              			+' WHERE CMRSL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
              	
              	EXEC(@sql)
				
	   /*    
	   Drop temporary table CertificationModuleIdMappings, temporary table Course will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationModuleIdMappings


-- =============================================================================
  --LOGIC START -- copy tblCertificationModuleRequirement object both full copy and partial copy ----------------
   
		 -- insert records from source site to destination table CertificationModuleRequirement
		  SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirement]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationModuleRequirementSet,
						 label,
						 shortDescription,
						 requirementType,
						 courseCompletionIsAny,
						 forceCourseCompletionInOrder,
						 numberCreditsRequired,
						 documentUploadFileType,
						 documentationApprovalRequired, 
						 allowSupervisorsToApproveDocumentation,
						 allowExpertsToApproveDocumentation,
						 courseCreditEligibleCoursesIsAll '
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCMRS.newDestinationId,
						 CMR.label,
						 CMR.shortDescription,
						 CMR.requirementType,
						 CMR.courseCompletionIsAny,
						 CMR.forceCourseCompletionInOrder,
						 CMR.numberCreditsRequired,
						 CMR.documentUploadFileType,
						 CMR.documentationApprovalRequired, 
						 CMR.allowSupervisorsToApproveDocumentation,
						 CMR.allowExpertsToApproveDocumentation,
						 CMR.courseCreditEligibleCoursesIsAll '					
              			+' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirement] CMR'
					+' LEFT JOIN #CertificationModuleRequirementSetIdMappings TTCMRS on (TTCMRS.oldSourceId = CMR.idCertificationModuleRequirementSet)'
					+' WHERE CMR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              EXEC(@sql)
							
				-- temporary table to hold the idCertificationModuleRequirement of source and destination CertificationModuleRequirement tables
				CREATE TABLE #CertificationModuleRequirementIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)

				-- inserting idCertificationModuleRequirement of source CertificationModuleRequirement table and destination CertificationModuleRequirement table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationModuleRequirementIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationModuleRequirement, DST.idCertificationModuleRequirement'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirement] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirement] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.label = DST.label)'
						+' AND (SRC.shortDescription = DST.shortDescription OR (SRC.shortDescription IS NULL AND DST.shortDescription IS NULL))'
						+' AND (SRC.requirementType = DST.requirementType)'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

			-- insert records from source site to destination table CertificationModuleRequirementLanguage	
              	SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementLanguage]'
              			+ ' ( '
              			+ '  idSite, 
              				idCertificationModuleRequirement, 
              				idLanguage, 
              				label, 
              				shortDescription'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' , TTCMR.newDestinationId, 
              				CMRL.idLanguage, 
              				CMRL.label, 
              				CMRL.shortDescription
              			FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementLanguage] CMRL'
              			+' LEFT JOIN #CertificationModuleRequirementIdMappings TTCMR on (TTCMR.oldSourceId = CMRL.idCertificationModuleRequirement )'
              			+' WHERE CMRL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
              			
              	EXEC(@sql)

	   /*    
	   Drop temporary table CertificationModuleRequirementSetIdMappings, temporary table Course will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationModuleRequirementSetIdMappings


-- =============================================================================
  --LOGIC START -- copy tblCertificationModuleRequirementToCourseLink object both full copy and partial copy ----------------

              -- insert records from source site to destination table CertificationModuleRequirementToCourseLink
		    SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationModuleRequirementToCourseLink]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationModuleRequirement,
						 idCourse,
						 [order]'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCMR.newDestinationId,
						 TTC.newDestinationId,
						 CMRTCL.[order]					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationModuleRequirementToCourseLink] CMRTCL'
					 +' LEFT JOIN #CertificationModuleRequirementIdMappings TTCMR on (TTCMR.oldSourceId = CMRTCL.idCertificationModuleRequirement)'
					 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CMRTCL.idCourse)'
					+' WHERE CMRTCL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              			
              
              EXEC(@sql)

-- =============================================================================
  --LOGIC START -- copy tblCertificationToCourseCreditLink object both full copy and partial copy ----------------

		  -- insert records from source site to destination table CertificationToCourseCreditLink
		  SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationToCourseCreditLink]'
              			+ ' ( '
              			+ '   idSite,
						 idCertification,
						 idCourse,
						 credits'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCM.newDestinationId,
						 TTC.newDestinationId,
						 CTCCL.credits					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationToCourseCreditLink] CTCCL'
					 +' LEFT JOIN #CertificationIdMappings TTCM on (TTCM.oldSourceId = CTCCL.idCertification)'
					 +' LEFT JOIN #CourseIdMappings TTC on (TTC.oldSourceId = CTCCL.idCourse)'
					+' WHERE CTCCL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              EXEC(@sql)
		
	    
	   /*    
	   Drop temporary table CourseIdMappings, temporary table Course will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CourseIdMappings

------------------------------------------------------------------------------


	-- check condition for the full copy
	IF @fullCopyFlag = 1
	BEGIN


-- =============================================================================
-- LOGIC START -- copy tblCertificationToUserLink object for full copy ----------------

		  -- insert records from source site to destination table  CertificationToUserLink
			 SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationToUserLink]'
              			+ ' ( '
              			+ '   idSite,
						 idCertification,
						 idUser,
						 initialAwardDate,
						 certificationTrack,
						 currentExpirationDate,
						 currentExpirationInterval,
						 currentExpirationTimeframe,
						 certificationId'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTC.newDestinationId,
						 TTU.newDestinationId,
						 CTUL.initialAwardDate,
						 CTUL.certificationTrack,
						 CTUL.currentExpirationDate,
						 CTUL.currentExpirationInterval,
						 CTUL.currentExpirationTimeframe,
						 CTUL.certificationId					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationToUserLink] CTUL'
					 +' LEFT JOIN #CertificationIdMappings TTC on (TTC.oldSourceId = CTUL.idCertification)'
					 +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = CTUL.idUser)'
					+' WHERE CTUL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              
              EXEC(@sql)


				-- temporary table to hold the idCertificationToUserLink of source and destination CertificationToUserLink tables
				CREATE TABLE #CertificationToUserLinkIdMappings
				(
					oldSourceId INT NOT NULL, 
					newDestinationId INT NOT NULL 	   
				)

				-- inserting idCertificationToUserLink of source CertificationToUserLink table and destination CertificationToUserLink table inside temporary table for mapping
				SET @sql =  ' INSERT INTO #CertificationToUserLinkIdMappings'
						+ ' ( '
						+ '  oldSourceId,
							newDestinationId'
						+ ' ) '
						+' SELECT SRC.idCertificationToUserLink, DST.idCertificationToUserLink'
						+' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblCertificationToUserLink] DST'
						+' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblCertificationToUserLink] SRC'
						+' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
						+' AND (SRC.initialAwardDate = DST.initialAwardDate OR (SRC.initialAwardDate IS NULL AND DST.initialAwardDate IS NULL))'
						+' AND (SRC.currentExpirationDate = DST.currentExpirationDate OR (SRC.currentExpirationDate IS NULL AND DST.currentExpirationDate IS NULL))'
						+' AND (SRC.certificationTrack = DST.certificationTrack)'
						+')'
						+' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
						
				EXEC(@sql)

-- ======================================================================================
-- LOGIC START -- copy [tblData-CertificationModuleRequirement] object for full copy ----

			 -- insert records from source site to destination table Data-CertificationModuleRequirement
			 SET @sql =  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblData-CertificationModuleRequirement]'
              			+ ' ( '
              			+ '   idSite,
						 idCertificationToUserLink,
						 idCertificationModuleRequirement,
						 label,
						 revcode,
						 dtCompleted,
						 completionDocumentationFilePath'
              			+ ' ) '
              			+' SELECT '+ CAST(@idSiteDestination AS NVARCHAR)
              			+' ,  TTCTUL.newDestinationId,
						 TTCMR.newDestinationId,
						 DCMR.label,
						 DCMR.revcode,
						 DCMR.dtCompleted,
						 DCMR.completionDocumentationFilePath					
              			 FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblData-CertificationModuleRequirement] DCMR'
					 +' LEFT JOIN #CertificationToUserLinkIdMappings TTCTUL on (TTCTUL.oldSourceId = DCMR.idCertificationToUserLink)'
					 +' LEFT JOIN #CertificationModuleRequirementIdMappings TTCMR on (TTCMR.oldSourceId = DCMR.idCertificationModuleRequirement)'
					 +' WHERE DCMR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
					
              
              EXEC(@sql)
			  
	   /*
	   DROP temporary table CertificationToUserLinkIdMappings
	   */
	   DROP TABLE #CertificationToUserLinkIdMappings

-- =============================================================================
-- LOGIC START -- copy tblGroupFeedMessage object for full copy ----------------


	   -- insert records from source site to destination table GroupFeedMessage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupFeedMessage] '
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idGroup,
	   			  	  idLanguage,
	   			  	  idAuthor,
	   			  	  idParentGroupFeedMessage,
	   			  	  [message] ,
	   			  	  [timestamp],
	   			  	  dtApproved,
	   			  	  idApprover,
	   			  	  IsApproved'
	   			  +'  ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTG.newDestinationId, '
	   			  +'	   GFM.idLanguage, '
	   			  +'	   TTAuthor.newDestinationId, '
	   			  +'	   NULL, '				 -- this null value of idParentGroupFeedMessage will be updated from temporary GroupFeedMessageIdMappings table below
	   			  +'	   GFM.[message], 
	   			  	   GFM.[timestamp],
	   			  	   GFM.dtApproved,
	   			  	   TTApprover.newDestinationId,
	   			  	   GFM.IsApproved'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupFeedMessage] GFM'
	   			  +' LEFT JOIN #UserIdMappings TTAuthor on (TTAuthor.oldSourceId = GFM.idAuthor )'
	   			  +' LEFT JOIN #UserIdMappings TTApprover on (TTApprover.oldSourceId = GFM.idApprover )'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GFM.idGroup )'
	   			  +' WHERE GFM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		
	   EXEC(@sql)
	   
	   -- temporary table to hold the idGroupFeedMessage of source and destination GroupFeedMessage tables
	   CREATE TABLE #GroupFeedMessageIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )
	   
	   -- inserting idGroupFeedMessage of source GroupFeedMessage table and destination GroupFeedMessage table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #GroupFeedMessageIdMappings'
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SRC.idGroupFeedMessage, 
					   DST.idGroupFeedMessage'
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupFeedMessage] DST'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupFeedMessage] SRC'
	   			  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			  +' AND (SRC.[timestamp] = DST.[timestamp])'
	   			  +' AND (SRC.message = DST.message)'
	   			  +')'
	   			  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)
	   
	    
	   -- update idParentGroupFeedMessage from Temp Table GroupFeedMessage
	   SET @sql =	   ' UPDATE DSTC'
	   			  +'  SET idParentGroupFeedMessage = TEMP.newDestinationId'
	   			  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupFeedMessage] SRCC'
	   			  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupFeedMessage] DSTC'
	   			  +' ON(SRCC.[timestamp] = DSTC.[timestamp] AND SRCC.message = DSTC.message AND SRCC.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	   			  +' INNER JOIN  #GroupFeedMessageIdMappings TEMP'
	   			  +' ON(TEMP.oldSourceId = SRCC.idParentGroupFeedMessage)'
	   			  +' WHERE DSTC.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   			  +' AND SRCC.idParentGroupFeedMessage IS NOT NULL'
	   
	   EXEC(@sql)	
	   
	   /*
	   DROP temporary table GroupFeedMessageIdMappings
	   */
	   DROP TABLE #GroupFeedMessageIdMappings

              

-- =============================================================================
-- LOGIC START -- copy tblGroupFeedModerator object for full copy --------------


	   -- insert records from source site to destination table GroupFeedModerator
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblGroupFeedModerator]'
	   			  +' ( '
	   			  +'	  idSite,
	   				  idGroup,
	   				  idModerator'
	   			  +' ) '
	   			  +' SELECT '
				  +	   CAST(@idSiteDestination AS NVARCHAR)
	   			  +' ,  TTG.newDestinationId,
	   			  	   TTU.newDestinationId'
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblGroupFeedModerator] GFM'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = GFM.idModerator )'
	   			  +' LEFT JOIN #GroupIdMappings TTG on (TTG.oldSourceId = GFM.idGroup )'
	   			  +' WHERE GFM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)


-- =============================================================================
-- LOGIC START -- copy tblUserToSupervisorLink object for full copy ------------


	   -- insert records from source site to destination table UserToSupervisorLink
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblUserToSupervisorLink]'
	   			  +'  ( '
	   			  +'	   idSite,
	   			  	   idUser,
	   			  	   idSupervisor'
	   			  +' ) '
	   			  +' SELECT '
				  +    CAST(@idSiteDestination AS NVARCHAR)
	   			  +' , TTU.newDestinationId '
	   			  +' , TTS.newDestinationId '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblUserToSupervisorLink] UTSL'
	   			  +' LEFT JOIN #UserIdMappings TTU on (TTU.oldSourceId = UTSL.idUser )'
	   			  +' LEFT JOIN #UserIdMappings TTS on (TTS.oldSourceId = UTSL.idSupervisor )'
	   			  +' WHERE UTSL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   			
	   EXEC(@sql)
	   
-- =============================================================================
-- LOGIC START -- copy tblInboxMessage object for full copy --------------------


	   -- insert records from source site to destination table InboxMessage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblInboxMessage] '
	   			  +' ( '
	   			  +'   idSite,
	   			  	  idParentInboxMessage,
	   			  	  idRecipient,
	   			  	  idSender,
	   			  	  subject,
	   			  	  message,
	   			  	  isDraft,
	   			  	  isSent,
	   			  	  dtSent,
	   			  	  isRead,
	   			  	  dtRead,
	   			  	  isRecipientDeleted,
	   			  	  dtRecipientDeleted,
	   			  	  isSenderDeleted,	
	   			  	  dtSenderDeleted,
	   			  	  dtCreated'
	   			  +'  ) '
	   			  +' SELECT '
				  +	  CAST(@idSiteDestination AS NVARCHAR) 
	   			  +' , NULL ,'			 -- this null value of idParentInboxMessage will be updated from temporary InboxMessageIdMappings table below 
	   			  +'	  TTR.newDestinationId,  
	   			  	  TTS.newDestinationId,
	   			  	  IM.subject,
	   			  	  IM.message,
	   			  	  IM.isDraft,
	   			  	  IM.isSent,
	   			  	  IM.dtSent,
	   			  	  IM.isRead,
	   			  	  IM.dtRead,
	   			  	  IM.isRecipientDeleted,
	   			  	  IM.dtRecipientDeleted,
	   			  	  IM.isSenderDeleted,	
	   			  	  IM.dtSenderDeleted,
	   			  	  IM.dtCreated 
	   			  	FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblInboxMessage] IM'
	   			  +' LEFT JOIN #UserIdMappings TTR on (TTR.oldSourceId = IM.idRecipient )'
	   			  +' LEFT JOIN #UserIdMappings TTS on (TTS.oldSourceId = IM.idSender )'
	   			  +' WHERE IM.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   							
	   EXEC(@sql)

	   -- temporary table to hold the idInboxMessage of source and destination InboxMessage tables
	   CREATE TABLE #InboxMessageIdMappings 
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )

	   -- inserting idInboxMessage of source InboxMessage table and destination InboxMessage table inside temporary table for mapping
	   SET @sql =	   ' INSERT INTO #InboxMessageIdMappings'
	   		   	  +' ( '
	   		   	  +'   oldSourceId,
	   		   	  	  newDestinationId'
	   		   	  +' ) '
	   		   	  +' SELECT 
					   SRC.idInboxMessage, 
					   DST.idInboxMessage '
	   		   	  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblInboxMessage] DST'
	   		   	  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblInboxMessage] SRC'
	   		   	  +' ON (SRC.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		   	  +' AND (SRC.message = DST.message)'
	   		   	  +' AND (SRC.dtCreated = DST.dtCreated)'
	   		   	  +' AND (SRC.dtSent = DST.dtSent OR (SRC.dtSent IS NULL AND DST.dtSent IS NULL))'
	   		   	  +' )'
	   		   	  +' WHERE DST.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   
	   EXEC(@sql)
	   
			
	   -- update idParentInboxMessage from Temp Table InboxMessage
	   SET @sql =	   ' UPDATE DSTIM'
	   		   	  +'  SET idParentInboxMessage = TEMP.newDestinationId'
	   		   	  +' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblInboxMessage] SRCIM'
	   		   	  +' INNER JOIN  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblInboxMessage] DSTIM'
	   		   	  +' ON(SRCIM.dtCreated = DSTIM.dtCreated AND SRCIM.[message] = DSTIM.[message] AND SRCIM.idSite ='+ CAST(@idSiteSource AS NVARCHAR)+ ')'
	   		   	  +' INNER JOIN  #InboxMessageIdMappings TEMP'
	   		   	  +' ON(TEMP.oldSourceId = SRCIM.idParentInboxMessage)'
	   		   	  +' WHERE DSTIM.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   		   	  +' AND SRCIM.idParentInboxMessage IS NOT NULL'
	   
	   EXEC(@sql)	
	   

	   /*
	   Drop temporary table InboxMessageIdMappings
	   */
	   DROP TABLE #InboxMessageIdMappings
	   


-- =============================================================================
-- LOGIC START -- copy tblReport object for full copy --------------------------


	   -- insert records from source site to destination table Report
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReport]'
	   			 +' ( '
	   			 +'   idSite,'  
	   			 +'	 idUser,'
	   			 +'	 idDataset,'
	   			 +'	 title,'
	   			 +'	 fields,'
	   			 +'	 filter,'
	   			 +'	 [order],'
	   			 +'	 isPublic,'
	   			 +'	 dtCreated,'
	   			 +'	 dtModified,'
	   			 +'	 isDeleted,'
	   			 +'	 dtDeleted '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +',   TTU.newDestinationId,'
	   			 +'	  R.idDataset,'
	   			 +'	  R.title,'
	   			 +'	  R.fields,'
	   			 +'	  R.filter,'
	   			 +'	  R.[order],'
	   			 +'	  R.isPublic,'
	   			 +'	  R.dtCreated,'
	   			 +'	  R.dtModified,'
	   			 +'	  R.isDeleted,'
	   			 +'	  R.dtDeleted '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReport] R'
	   			 +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = R.idUser ) '
	   			 +' WHERE R.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   	
	   EXEC(@sql)
	   		
	   -- temporary table to hold the idReport of source and destination Report tables
	   CREATE TABLE #ReportIdMappings
	   (
	   	  oldSourceId INT NOT NULL, 
	   	  newDestinationId INT NOT NULL  	   
	   )	
	   
	   -- inserting idReport of source Report table and destination Report table inside temporary table for mapping		
	   SET @sql =	   ' INSERT INTO #ReportIdMappings '
	   			  +' ( '
	   			  +'	  oldSourceId,
	   			  	  newDestinationId'
	   			  +' ) '
	   			  +' SELECT 
					   SR.idReport, 
					   DR.idReport '
	   			  +' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReport] DR'
	   			  +' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReport] SR'
	   			  +' ON (SR.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   		       +' AND (SR.dtCreated = DR.dtCreated OR (SR.dtCreated IS NULL AND DR.dtCreated IS NULL))'
	   			  +' AND (SR.dtModified = DR.dtModified OR (SR.dtModified IS NULL AND DR.dtModified IS NULL))'					
	   			  +' AND (SR.dtDeleted = DR.dtDeleted OR (SR.dtDeleted IS NULL AND DR.dtDeleted IS NULL))' 
	   			  +' )'
	   			  +' WHERE DR.idSite ='+ CAST(@idSiteDestination AS NVARCHAR)
	   				
	   EXEC(@sql)	 
	     	
	   -- reportLanguage data move logic start

	   -- insert records from source site to destination table ReportLanguage
	   SET @sql =	   ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReportLanguage]'
	   			  +' ( '
	   			  +'	  idSite,' 
	   			  +'	  idReport,'
	   			  +'	  idLanguage,'
	   			  +'	  title '
	   			  +' )  '
	   			  +' SELECT '
	   			  +	  CAST(@idSiteDestination AS NVARCHAR)
	   			  +',  TTR.newDestinationId,'
	   			  +'	  RL.idLanguage,'
	   			  +'	  RL.title '
	   			  +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReportLanguage] RL'
	   			  +' LEFT JOIN #ReportIdMappings TTR on (TTR.oldSourceId = RL.idReport )'
	   			  +' WHERE RL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	   
	   -- reportLanguage data move logic end



-- =============================================================================
-- LOGIC START -- copy tblReportFile object for full copy ----------------------

		
	   -- insert records from source site to destination table Report file
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReportFile]'
	   			 +' ( '
	   			 +'   idSite,'                 
	   			 +'	 idReport,'
	   			 +'	 idUser,'
	   			 +'	 [filename],'
	   			 +'	 dtCreated,'
	   			 +'	 htmlKb,'
	   			 +'	 csvKb,'
	   			 +'	 pdfKb '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +',   TTR.newDestinationId,'
	   			 +'    TTU.newDestinationId,'
	   			 +'	  RF.[filename],'
	   			 +'	  RF.dtCreated,'
	   			 +'	  RF.htmlKb,'
	   			 +'	  RF.csvKb,'
	   			 +'	  RF.pdfKb '	
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReportFile] RF'
	   			 +' INNER JOIN #ReportIdMappings TTR on (TTR.oldSourceId = RF.idReport ) '
	   			 +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = RF.idUser ) '
	   			 +' WHERE RF.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	   


-- =============================================================================
-- LOGIC START -- copy tblReportSubscription object for full copy --------------

	
	   -- insert records from source site to destination table ReportSubscription
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReportSubscription]'
	   			 +' ( '
	   			 +'   idSite,'    
	   			 +'	 idUser,'
	   			 +'	 idReport,'
	   			 +'	 dtStart,'
	   			 +'	 dtNextAction,'
	   			 +'	 recurInterval,'
	   			 +'	 recurTimeframe,'
	   			 +'	 token '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +' ,  TTU.newDestinationId,'
	   			 +'    TTR.newDestinationId,'
	   			 +'	  RS.dtStart,'
	   			 +'	  RS.dtNextAction,'
	   			 +'	  RS.recurInterval,'
	   			 +'	  RS.recurTimeframe,'
	   			 +'	  RS.token '	
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReportSubscription] RS'
	   			 +' INNER JOIN #ReportIdMappings TTR on (TTR.oldSourceId = RS.idReport ) '
	   			 +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = RS.idUser ) '
	   			 +' WHERE RS.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)



-- =================================================================================
-- LOGIC START -- copy tblReportToReportShortcutsWidgetLink object for full copy ---

		
	   -- insert records from source site to destination table ReportToReportShortcutsWidgetLink
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblReportToReportShortcutsWidgetLink]'
	   			 +' ( '
	   			 +'   idSite,'                
	   			 +'	 idReport,'
	   			 +'	 idUser '
	   			 +'  ) '
	   			 +' SELECT '
	   			 +     CAST(@idSiteDestination AS NVARCHAR)
	   			 +',   TTR.newDestinationId,'
	   			 +'    TTU.newDestinationId '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblReportToReportShortcutsWidgetLink] RRSWL'
	   			 +' INNER JOIN #ReportIdMappings TTR on (TTR.oldSourceId = RRSWL.idReport ) '
	   			 +' INNER JOIN #UserIdMappings TTU on (TTU.oldSourceId = RRSWL.idUser ) '
	   			 +' WHERE RRSWL.idSite ='+ CAST(@idSiteSource AS NVARCHAR) 
	   
	   EXEC(@sql)
	   


-- =============================================================================
-- LOGIC START -- copy tblSystem object for full copy --------------------------
	   --insert records from source site to destination table tblSystem
	   SET @sql =	  ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].'+ '[tblSystem]'
	   			 +' ( '
	   			 +'    lastEventEmailQueueCascade '
	   			 +' ) '
	   			 
	   			 +' SELECT '
	   			 +'    lastEventEmailQueueCascade '
	   			 +' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].'+ '[tblSystem] '
	   		
	   --EXEC(@sql)
	   
-- =============================================================================

	   /*
	   Drop temporary table ReportIdMappings
	   */
	   DROP TABLE #ReportIdMappings
	   
	   /*
	   Drop temporary table UserIdMappings
	   */
	   DROP TABLE #UserIdMappings

    END

------------------------------------------------------------------------------
  -- full copy end

  	   /*
	   Drop temporary table GroupIdMappings, temporary table will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #GroupIdMappings

	   /*    
	   Drop temporary table CertificationModuleRequirementIdMappings, temporary table will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationModuleRequirementIdMappings

	   /*    
	   Drop temporary table CourseIdMappings, temporary table Course will be dorp in both conditions full copy and partial copy
	   */
	   DROP TABLE #CertificationIdMappings


	   -- insert new idSite into the objectMapping that will be used on moving file object.
	   INSERT INTO #ObjectMapping
	   ([oldId],[newId],objectName)
	   SELECT @idSiteSource, @idSiteDestination, 'site'

	  
		 /* LOGIC END */

			 SET @Return_Code = 0
			 SET @Error_Description_Code	= 'SystemClonePortal_InsertedSuccessfully'
			 SELECT * FROM #ObjectMapping
			 DROP TABLE #ObjectMapping 

	    COMMIT TRANSACTION
	   
	   --TRANSACTION END

	END TRY
		--END TRY

	BEGIN CATCH
		
			
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_PortalMigrationFailed_RollbackOccured'
		
		
		IF @@TRANCOUNT > 0
			BEGIN 				
				ROLLBACK TRANSACTION
			END	     
			--INSERT INTO Clone_Portal_Error_Log ([Timestamp], ErrorMessage) VALUES (GETUTCDATE(), ERROR_MESSAGE())
	END CATCH
		
	SET XACT_ABORT OFF		
END 
--main begin end
