SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.RoleRule]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.RoleRule]
GO


/*

CLONE PORTAL - ROLE / RULE / RULESET DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.RoleRule]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,			-- needed for user links because users are only copied in full copy
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.0: Copy RoleRule - Initialized', 0)
    
	BEGIN TRY
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		ROLES

		*/	  

		-- copy roles
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRole] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	name '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	R.name '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRole] R '
	   			 + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.1: Copy RoleRule - Roles Inserted', 0)	   	

		-- insert the mapping for source to destination role ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idRole, '
	   			 + '	DST.idRole, '
	   			 + '	''role'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRole] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRole] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.name = DST.name)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)	   			 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.2: Copy RoleRule - Roles Mappings Created', 0)	   
	   
		-- copy role languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRoleLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRole, '
	   			 + '	idLanguage, '
	   			 + '	name '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	RL.idLanguage, '
	   			 + '	RL.name '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRoleLanguage] RL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RL.idRole AND TTR.object = ''role'') '
	   			 + 'WHERE RL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.3: Copy RoleRule - Role Languages Inserted', 0)

		-- copy role to permission links -- ONLY NON SCOPED PERMISSIONS
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRoleToPermissionLink] '
	   			+ '( '
	   			+ '	idSite, '
	   			+ '	idRole, '
	   			+ '	idPermission, '
	   			+ '	scope '
	   			+ ') '
	   			+ 'SELECT '
				+	CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			+ '	TTR.destinationId, '
	   			+ '	RTPL.idPermission, '
	   			+ '	RTPL.scope '
	   			+ 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRoleToPermissionLink] RTPL '
	   			+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTPL.idRole AND TTR.object = ''role'' ) '
	   			+ 'WHERE RTPL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND RTPL.scope IS NULL'
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.4: Copy RoleRule - Role to Permission Links Inserted', 0)

		/*

		RULESETS

		*/

		-- copy rulesets
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSet] '
	     		 + '( '
	     		 + '	[idSite], '
	     		 + '	[isAny], '
	     		 + '	[label] '
	     		 + ') '
	     		 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	     		 + '	R.[isAny], '
	     		 + '	R.[label]  + '' ##'' + CAST(R.idRuleSet AS NVARCHAR) + ''##'' '
	     		 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSet] R '
	     		 + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	     
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.5: Copy RoleRule - Rulesets Inserted', 0)

		-- insert the mapping for source to destination ruleset ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idRuleSet, '
	   			 + '	DST.idRuleSet, '
	   			 + '	''ruleset'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSet] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSet] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.[label] + '' ##'' + CAST(SRC.idRuleSet AS NVARCHAR) + ''##'' = DST.[label]) ' 
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.6: Copy RoleRule - Ruleset Mappings Created', 0)

		-- clean up the ##idRuleSet## additions we made to labels, we did that to uniquely distinguish rulesets
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleSet] SET '
				 + '	label = REPLACE(DRS.label, '' ##'' + CAST(SRS.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleSet] DRS '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SRS ON SRS.destinationID = DRS.idRuleSet AND SRS.object = ''ruleset'' '
				 + 'WHERE DRS.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SRS.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.7: Copy RoleRule - Ruleset Titles Cleaned Up', 0)

		-- copy ruleset languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idLanguage, '
	   			 + '	label '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	RL.idLanguage, '
	   			 + '	RL.label '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetLanguage] RL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RL.idRuleset AND TTR.object = ''ruleset'' ) '
	   			 + 'WHERE RL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.8: Copy RoleRule - Ruleset Languages Inserted', 0)

		/*

		RULES

		*/

		-- copy rules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRule] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	userField, '
	   			 + '	operator, '
	   			 + '	textValue, '
	   			 + '	dateValue, '
	   			 + '	numValue, '
	   			 + '	bitValue '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	R.userField, '
	   			 + '	R.operator, '
	   			 + '	R.textValue, '
	   			 + '	R.dateValue, '
	   			 + '	R.numValue, '
	   			 + '	R.bitValue '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRule] R '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = R.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'WHERE R.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.9: Copy RoleRule - Rules Inserted', 0)

		/*

		RULESET TO OBJECT LINKS

		*/

		-- copy ruleset to group links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToGroupLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idGroup '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTG.destinationId '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToGroupLink] RTGL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTGL.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = RTGL.idGroup AND TTG.object = ''groups'') '
	   			 + 'WHERE RTGL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.10: Copy RoleRule - Ruleset to Group Links Inserted', 0)

		-- copy ruleset to role links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToRoleLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idRole '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTRole.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToRoleLink] RTRL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTRL.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRole on (TTRole.sourceId = RTRL.idRole AND TTRole.object = ''role'') '
	   			 + 'WHERE RTRL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.11: Copy RoleRule - Ruleset to Role Links Inserted', 0)

		-- copy ruleset to learning path enrollment links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToRuleSetLearningPathEnrollmentLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, ' 
	   			 + '	idRuleSetLearningPathEnrollment '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTRS.destinationId, '
	   			 + '	TTRSLPE.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToRuleSetLearningPathEnrollmentLink] RSRSLPEL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRSLPE on (TTRSLPE.sourceId = RSRSLPEL.idRuleSetLearningPathEnrollment AND TTRSLPE.object = ''rulesetlearningpathenrollment'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = RSRSLPEL.idRuleSet AND TTRS.object = ''ruleset'') '
	   			 + 'WHERE RSRSLPEL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.12: Copy RoleRule - Ruleset to Ruleset Learning Path Enrollment Links Inserted', 0)

		-- copy ruleset enrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetEnrollment] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idTimezone, '
	   			 + '	[priority], '
	   			 + '	label, '
	   			 + '	isLockedByPrerequisites, '
	   			 + '	isFixedDate, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	dtCreated, '
	   			 + '	delayInterval, '
	   			 + '	delayTimeframe, '
	   			 + '	dueInterval, '
	   			 + '	dueTimeframe, '
	   			 + '	recurInterval, '
	   			 + '	recurTimeframe, '
	   			 + '	expiresFromStartInterval, '
	   			 + '	expiresFromStartTimeframe, '
	   			 + '	expiresFromFirstLaunchInterval, '
	   			 + '	expiresFromFirstLaunchTimeframe, '
	   			 + '	forceReassignment '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	RE.idTimezone, '
	   			 + '	RE.[priority], '
	   			 + '	RE.label, '
	   			 + '	RE.isLockedByPrerequisites, '
	   			 + '	RE.isFixedDate, '
	   			 + '	RE.dtStart, '
	   			 + '	RE.dtEnd, '
	   			 + '	RE.dtCreated, '
	   			 + '	RE.delayInterval, '
	   			 + '	RE.delayTimeframe, '
	   			 + '	RE.dueInterval, '
	   			 + '	RE.dueTimeframe, '
	   			 + '	RE.recurInterval, '
	   			 + '	RE.recurTimeframe, '
	   			 + '	RE.expiresFromStartInterval, '
	   			 + '	RE.expiresFromStartTimeframe, '
	   			 + '	RE.expiresFromFirstLaunchInterval, '
	   			 + '	RE.expiresFromFirstLaunchTimeframe, '
	   			 + '	RE.forceReassignment '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetEnrollment] RE '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = RE.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE RE.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.13: Copy RoleRule - Ruleset Enrollments Inserted', 0)
	   
		-- insert the mapping for source to destination ruleset enrollment id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, ' 
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idRuleSetEnrollment, '
	   			 + '	DST.idRuleSetEnrollment, '
	   			 + '	''rulesetenrollment'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetEnrollment] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetEnrollment] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.label = DST.label) '
	   			 + '	AND (SRC.dtStart = DST.dtStart) '	   			 
	   			 + '	AND (SRC.dtCreated = DST.dtCreated)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.14: Copy RoleRule - Ruleset Enrollment Mappings Created', 0)

		-- copy ruleset enrollment languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetEnrollmentLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSetEnrollment, '
	   			 + '	idLanguage, '
	   			 + '	label '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTRE.destinationId, '
	   			 + '	REL.idLanguage, '
	   			 + '	REL.label '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetEnrollmentLanguage] REL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRE on (TTRE.sourceId = REL.idRuleSetEnrollment AND TTRE.object = ''rulesetenrollment'') '
	   			 + 'WHERE REL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	           
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.15: Copy RoleRule - Ruleset Enrollment Languages Inserted', 0)

		-- copy ruleset to enrollment links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToRuleSetEnrollmentLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idRuleSetEnrollment '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTRE.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToRuleSetEnrollmentLink] RTREL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTREL.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRE on (TTRE.sourceId = RTREL.idRuleSetEnrollment AND TTRE.object = ''rulesetenrollment'') '
	   			 + 'WHERE RTREL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.16: Copy RoleRule - Ruleset to Ruleset Enrollment Links Inserted', 0)

		/*

		GROUP TO ROLE LINKS

		*/

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupToRoleLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idGroup, '
	   			 + '	idRole, '
	   			 + '	idRuleSet, '
	   			 + '	created '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTG.destinationId, '
	   			 + '	TTR.destinationId, '
	   			 + '	TTRS.destinationId, '
	   			 + '	GTRL.created '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupToRoleLink] GTRL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = GTRL.idRole AND TTR.object = ''role'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GTRL.idGroup AND TTG.object = ''groups'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = GTRL.idRuleset AND TTRS.object = ''ruleset'') '
	   			 + 'WHERE GTRL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.17: Copy RoleRule - Group to Role Links Inserted', 0)

		/*

		USER TO OBJECT LINKS - FULL COPY ONLY

		*/
			   
		IF @fullCopyFlag = 1
			BEGIN

			-- copy user to group links
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUserToGroupLink] '
	   				 + '( '
	   				 + '	idSite, '
	   				 + '	idUser, '
	   				 + '	idGroup, '
	   				 + '	idRuleSet, '
	   				 + '	created, '
	   				 + '	selfJoined '
	   				 + ') '
	   				 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				 + '	TTU.destinationId, '
	   				 + '	TTG.destinationId, '
	   				 + '	TTRS.destinationId, '
	   				 + '	UTGL.created, '
	   				 + '	UTGL.selfJoined '
	   				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserToGroupLink] UTGL '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = UTGL.idUser AND TTU.object = ''users'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = UTGL.idGroup AND TTG.object = ''groups'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = UTGL.idRuleset AND TTRS.object = ''ruleset'') '
	   				 + 'WHERE UTGL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.18: Copy RoleRule - User to Group Links Inserted', 0)

			-- copy user to role links
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblUserToRoleLink] '
	   				 + '( '
	   				 + '	idSite, '
	   				 + '	idUser, '
	   				 + '	idRole, '
	   				 + '	idRuleSet, '
	   				 + '	created '
	   				 + ') '
	   				 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				 + '	TTU.destinationId, '
	   				 + '	CASE WHEN UTRL.idRole < 100 THEN UTRL.idRole ELSE TTR.destinationId END, '
	   				 + '	TTRS.destinationId, '
	   				 + '	UTRL.created '
	   				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserToRoleLink] UTRL '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = UTRL.idRole AND TTR.object = ''role'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = UTRL.idUser AND TTU.object = ''users'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = UTRL.idRuleset AND TTRS.object = ''ruleset'') '
	   				 + 'WHERE UTRL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7.19: Copy RoleRule - User to Role Links Inserted', 0)
	   
			END

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_RoleRule_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 7: Copy RoleRule - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_RoleRule_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END