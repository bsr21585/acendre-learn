
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.User]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.User]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.User]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	  
	/************************************************ USER ******************************************************/
		-- tblUser
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUser]'
				+ ' ( '
				+ '		firstName, '
				+ '		middleName, '
				+ '		lastName, '
				+ '		displayName, '
				+ '		email, '
				+ '		username, '
				+ '		password, '
				+ '		idSite, '
				+ '		isDeleted, '
				+ '		idTimezone, '
				+ '		objectGUID, '
				+ '		activationGUID, '
				+ '		distinguishedName, '
				+ '		isActive, '
				+ '		mustchangePassword, '  
				+ '		dtCreated, '
				+ '		dtExpires, '
				+ '		idLanguage, '
				+ '		dtModified, '
				+ '		employeeID, '
				+ '		company, '
				+ '		address, '
				+ '		city, '
				+ '		province, '
				+ '		postalcode, '
				+ '		country, '
				+ '		phonePrimary, '
				+ '		phoneWork, '
				+ '		phoneFax, '
				+ '		phoneHome, '
				+ '		phoneMobile, '
				+ '		phonePager, '
				+ '		phoneOther, '
				+ '		department, '
				+ '		division, '
				+ '		region, '
				+ '		jobTitle, '
				+ '		jobClass, '
				+ '		gender, '
				+ '		race, '
				+ '		dtDOB, '
				+ '		dtHire, '
				+ '		dtTerm, '
				+ '		field00, '
				+ '		field01, '
				+ '		field02, '
				+ '		field03, '
				+ '		field04, '
				+ '		field05, '
				+ '		field06, '
				+ '		field07, '
				+ '		field08, '
				+ '		field09, '
				+ '		field10, '
				+ '		field11, '
				+ '		field12, '
				+ '		field13, '
				+ '		field14, '
				+ '		field15, '
				+ '		field16, '
				+ '		field17, '
				+ '		field18, '
				+ '		field19, '
				+ '		avatar, '
				+ '		dtLastLogin, '
				+ '		dtDeleted, '
				+ '		dtSessionExpires, '
				+ '		activeSessionId, '
				+ '		dtLastPermissionCheck, '
				+ '		isRegistrationApproved, '
				+ '		dtApproved, '
				+ '		dtDenied, '				
				+ '		idApprover, '
				+ '		rejectionComments, '
				+ '		excludeFromLeaderboards '
				+ ' ) '
				+ ' SELECT ' 
				+ '		U.firstName,'									-- first name
				+ '		U.middleName,'									-- middle name
				+ '		U.lastName,'									-- last name
				+ '		U.lastName + '', '' + U.firstName,'				-- display name
				+ '		U.email,'										-- email
				+ '		U.username,'									-- username
				+ '		U.password,'									-- password (this is an MD5 value)
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- site ID
				+ '		0,'												-- is deleted? 
				+ '		ISNULL(TZ.destinationID, 15) AS idTimezone,'	-- timezone id (default to Eastern timezone if the value is NULL)
				+ '		U.objectGUID,'									-- object GUID
				+ '		NULL,'											-- activation GUID
				+ '		U.distinguishedName,'							-- distinguished name
				+ '		U.isActive,'									-- is active?
				+ '		1,'												-- must change password? (since Asentia uses SHA1, we need to force all users to change their password)
				+ '		U.dtCreated,'									-- date created
				+ '		U.dtExpires,'									-- date expires
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ','	-- language id, use Asentia default
				+ '		U.dtModified,'									-- date modified
				+ '		UP.employeeID,'									-- employeed ID
				+ '		UP.company,'									-- company
				+ '		UP.address,'									-- street address
				+ '		UP.city,'										-- city
				+ '		UP.province,'									-- state/province
				+ '		UP.postalcode,'									-- postal code
				+ '		UP.country,'									-- country
				+ '		UP.phonePrimary,'								-- primary phone
				+ '		UP.phoneWork,'									-- work phone
				+ '		UP.phoneFax,'									-- fax
				+ '		UP.phoneHome,'									-- home phone
				+ '		UP.phoneMobile,'								-- mobile phone
				+ '		UP.phonePager,'									-- pager
				+ '		UP.phoneOther,'									-- other phone
				+ '		UP.department,'									-- department
				+ '		UP.division,'									-- division
				+ '		UP.region,'										-- region
				+ '		UP.jobTitle,'									-- job title
				+ '		UP.jobClass,'									-- job class
				+ '		UP.gender,'										-- gender
				+ '		UP.race,'										-- race
				+ '		UP.dtDOB,'										-- date of birth
				+ '		UP.dtHire,'										-- hire date
				+ '		UP.dtTerm,'										-- term date
				+ '		UDUFV.field00,'									-- user defined field00
				+ '		UDUFV.field01,'									-- user defined field01
				+ '		UDUFV.field02,'									-- user defined field02
				+ '		UDUFV.field03,'									-- user defined field03
				+ '		UDUFV.field04,'									-- user defined field04
				+ '		UDUFV.field05,'									-- user defined field05
				+ '		UDUFV.field06,'									-- user defined field06
				+ '		UDUFV.field07,'									-- user defined field07
				+ '		UDUFV.field08,'									-- user defined field08
				+ '		UDUFV.field09,'									-- user defined field09
				+ '		UDUFV.field10,'									-- user defined field10
				+ '		NULL,'											-- user defined field11
				+ '		NULL,'											-- user defined field12
				+ '		NULL,'											-- user defined field13
				+ '		NULL,'											-- user defined field14
				+ '		NULL,'											-- user defined field15
				+ '		NULL,'											-- user defined field16
				+ '		NULL,'											-- user defined field17
				+ '		NULL,'											-- user defined field18
				+ '		NULL,'											-- user defined field19
				+ '		NULL,'											-- avatar
				+ '		NULL,'											-- date last login (set to null because user has never logged into Asentia)
				+ '		NULL,'											-- date deleted
				+ '		NULL,'											-- date session expires
				+ '		NULL,'											-- active session ID
				+ '		NULL,'											-- date last permission check
				+ '		NULL,'											-- is registration approved?
				+ '		NULL,'											-- date approved
				+ '		NULL,'											-- date denied
				+ '		NULL,'											-- approver id
				+ '		NULL,'											-- rejection comments
				+ '		U.excludeFromLeaderboards'						-- exclude from leaderboards?
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].' + '[tblUser] U'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserProperties] UP ON UP.idUser = U.idUser'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUserDefinedUserFieldValue] UDUFV ON UDUFV.idUser = U.idUser'
				+ ' LEFT JOIN [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = U.idTimezone'
				+ ' WHERE U.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)
	  
		-- insert mapping for idUser 1 (admin)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		1, '
				+ '		1, '
				+ '		''user'''
				+ ' ) ' 

		EXEC(@sql)

		-- insert idUser mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SU.idUser, '
				+ '		DU.idUser,'
				+ '		''user''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblUser] DU '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] SU ON SU.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SU.username = DU.username collate database_default AND SU.dtCreated = DU.dtCreated'
				+ ' WHERE DU.idSite IS NOT NULL AND DU.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SU.idUser IS NOT NULL AND DU.idUser IS NOT NULL'
		
		EXEC(@sql)

	/*****************************************************GROUP********************************************************/
		-- tblGroup
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblGroup]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		primaryGroupToken, '
				+ '		objectGUID, '
				+ '		distinguishedName, '
				+ '		avatar, '
				+ '		isSelfJoinAllowed, '
				+ '		shortDescription, '
				+ '		longDescription, '
				+ '		isFeedActive, '
				+ '		isFeedModerated, '
				+ '		membershipIsPublicized,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		G.name,'										-- name
				+ '		G.primaryGroupToken,'							-- primary group token
				+ '		G.objectGUID,'									-- object GUID
				+ '		G.distinguishedName,'							-- distinguished name
				+ '		NULL,'											-- avatar
				+ '		0,'												-- is self joined allowed?
				+ '		NULL,'											-- short description
				+ '		NULL,'											-- long description
				+ '		0,'												-- is feed active?
				+ '		0,'												-- is feed moderated?
				+ '		0,'												-- membership is publicized?
				+ '		NULL'											-- search tags
				+ '		FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G'
				+ ' WHERE G.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idGroup mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SG.idGroup, '
				+ '		DG.idGroup,'
				+ '		''group''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblGroup] DG '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] SG ON SG.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SG.name = DG.name collate database_default'
				+ ' WHERE DG.idSite IS NOT NULL AND DG.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SG.idGroup IS NOT NULL AND DG.idGroup IS NOT NULL'
						
		EXEC(@sql)

		-- tblGroupLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroupLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idGroup,'
				+ '		idLanguage,'
				+ '		name,'
				+ '		shortDescription,'
				+ '		longDescription,'
				+ '		searchTags'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		G.idGroup, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		G.name, '
				+ '		G.shortDescription, '
				+ '		G.longDescription, '
				+ '		G.searchTags '
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblGroup] G'
				+ ' WHERE G.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		/***************************************************ROLES / PERMISSIONS********************************************************/
		
		-- create Asentia roles based on Inquisiq Permissions and put them in mapping table

		/***************************************************** 
			Inquisiq Permission: System Manager (6)
				Asentia Permissions:
					System_AccountSettings (1)
					System_Configuration (2)
					System_Ecommerce (3)
					System_CouponCodes (4)
					System_TrackingCodes (5)
					System_EmailNotifications (6)
					System_API (7)
					System_xAPIEndpoints (8)
					System_Logs (9)
					System_WebMeetingIntegration (10)
		********************************************************/

		DECLARE @idRole int

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''System Manager'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		1'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		2'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		3'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		4'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		5'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		6'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		7'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		8'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		9'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		10'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		6, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: Designer (7)
				Asentia Permissions:
					InterfaceAndLayout_HomePage (201)
					InterfaceAndLayout_Masthead (202)
					InterfaceAndLayout_Footer (203)
					InterfaceAndLayout_CSSEditor (204)
					InterfaceAndLayout_ImageEditor (205)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''Designer'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		201'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		202'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		203'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		204'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		205'
				+ ' ) '
				
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		7, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Creator (1)
				Asentia Permissions:
					UsersAndGroups_UserCreator (102)
					UsersAndGroups_UserEditor (104)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Creator'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		102'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		104'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		1, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Deleter (11)
				Asentia Permissions:
					UsersAndGroups_UserDeleter (103)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Deleter'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		103'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		11, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Editor (10)
				Asentia Permissions:
					UsersAndGroups_UserEditor (104)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Editor'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		104'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		10, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Manager (2)
				Asentia Permissions:
					UsersAndGroups_UserManager (105)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Manager'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		105'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		2, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: User Impersonator (13)
				Asentia Permissions:
					UsersAndGroups_UserImpersonator (106)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''User Impersonator'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		106'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		13, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: Group Creator (9)
				Asentia Permissions:
					UsersAndGroups_GroupManager (107)
					UsersAndGroups_GroupCreator (112)
					UsersAndGroups_GroupDeleter (113)
					UsersAndGroups_GroupEditor (114)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''Group Creator'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		107'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		112'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		113'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		114'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		9, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: Content Manager (8)
				Asentia Permissions:
					LearningAssets_CourseCatalog (301)
					LearningAssets_CourseContentManager (302)
					LearningAssets_LearningPathContentManager (303)
					LearningAssets_ContentPackageManager (304)
					LearningAssets_CertificateTemplateManager (305)
					LearningAssets_QuizAndSurveyManager (309)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''Content Manager'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		301'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		302'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		303'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		304'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		305'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		309'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		8, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		/***************************************************** 
			Inquisiq Permission: Reporter (3, 12)
				Asentia Permissions:
					Reporting_Reporter_UserDemographics (401)
					Reporting_Reporter_UserCourseTranscripts (402)
					Reporting_Reporter_CatalogAndCourseInformation (403)
					Reporting_Reporter_Certificates (404)
					Reporting_Reporter_XAPI (405)
					Reporting_Reporter_UserLearningPathTranscripts (406)
					Reporting_Reporter_UserInstructorLedTrainingTranscripts (407)
					Reporting_Reporter_Purchases (408)
					Reporting_Reporter_UserCertificateTranscripts (409)
		********************************************************/
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRole]'
				+ ' ( '
				+ '		idSite,'
				+ '		name'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''Reporter'' '
				+ ' ) '
		EXEC(@sql)

		SET @idRole = @@IDENTITY

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRoleToPermissionLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idRole,'
				+ '		idPermission'
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		401'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		402'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		403'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		404'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		405'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		406'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		407'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		408'
				+ ' ), '
				+ ' ( '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		409'
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		3, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
				+ ' ( '
				+ '		sourceID, '
				+ '		destinationID, '
				+ '		object '
				+ ' ) '
				+ ' VALUES '
				+ ' ( '
				+ '		12, '
				+ '		' + CONVERT(NVARCHAR, @idRole) + ', '
				+ '		''role'' '
				+ ' ) '
		EXEC(@sql)

		-- map user rights to roles
	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblUserToRoleLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idUser,'
				+ '		idRole,'
				+ '		created'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		UM.destinationID, '		-- user id					
				+ '		RM.destinationID, '		-- role id
				+ '		GETDATE() '
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUserRight] UR'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = UR.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RM ON RM.sourceID = UR.idUserRight AND RM.object = ''role'''
				+ ' WHERE UM.destinationID IS NOT NULL AND RM.destinationID IS NOT NULL'
				
		EXEC(@sql)

		-- map group rights to roles (assign the roles to individual users in the groups)

		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblUserToRoleLink]'
				+ ' ( '
				+ '		idSite,'
				+ '		idUser,'
				+ '		idRole,'
				+ '		created'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		UM.destinationID, '		-- user id					
				+ '		RM.destinationID, '		-- role id
				+ '		GETDATE() '
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupRight] GR'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblUserGroupLink] UGL ON UGL.idGroup = GR.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = UGL.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RM ON RM.sourceID = GR.idGroupRight AND RM.object = ''role'''
				+ ' WHERE UM.destinationID IS NOT NULL AND RM.destinationID IS NOT NULL'
				
		EXEC(@sql)
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaUser_Success'
		
	SET XACT_ABORT OFF

END
GO