SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.CouponCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.CouponCode]
GO


/*

CLONE PORTAL - COUPON CODES
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.CouponCode]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
    SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.0: Copy CouponCode - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		COUPON CODES

		*/

		-- copy coupon codes
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCode] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	code, '
	   			 + '	comments, '
	   			 + '	usesAllowed, '
	   			 + '	discount, '
	   			 + '	discountType, '
	   			 + '	forCourse, '
	   			 + '	forCatalog, '
	   			 + '	forLearningPath, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	isSingleUsePerUser, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	forStandupTraining '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	CC.code, '
	   			 + '	CC.comments, '
	   			 + '	CC.usesAllowed, '
	   			 + '	CC.discount, '
	   			 + '	CC.discountType, '
	   			 + '	CC.forCourse, '
	   			 + '	CC.forCatalog, '
	   			 + '	CC.forLearningPath, '
	   			 + '	CC.dtStart, '
	   			 + '	CC.dtEnd, '
	   			 + '	CC.isSingleUsePerUser, '
	   			 + '	CC.isDeleted, '
	   			 + '	CC.dtDeleted, '
	   			 + '	CC.forStandupTraining '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC '				 
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.1: Copy CouponCode - Coupon Codes Inserted', 0)

		-- insert the mapping for source to destination coupon code ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SCC.idCouponCode, '
				 + '	DCC.idCouponCode, '
				 + '	''couponcode'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCode] DCC '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] SCC '
				 + 'ON (SCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SCC.code = DCC.code) '
				 + '	AND (SCC.dtDeleted = DCC.dtDeleted OR (SCC.dtDeleted IS NULL AND DCC.dtDeleted IS NULL)) '
				 + '	AND (SCC.dtStart = DCC.dtStart OR (SCC.dtStart IS NULL AND DCC.dtStart IS NULL)) '
				 + '	AND (SCC.dtEnd = DCC.dtEnd OR (SCC.dtEnd IS NULL AND DCC.dtEnd IS NULL))) '
				 + 'WHERE DCC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.1: Copy CouponCode - Coupon Code Mappings Created', 0)

		/*

		COUPON CODE TO OBJECT LINKS

		*/

		-- copy coupon code to ilt links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCodeToStandupTrainingLink] '
	   			 + '( '
	   			 + '	idCouponCode, ' 
	   			 + '	idStandupTraining '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTCC.destinationId, '
	   			 + '	TTST.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCodeToStandupTrainingLink] CCSTL '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC ON CC.idCouponCode = CCSTL.idCouponCode '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC ON (TTCC.sourceId = CCSTL.idCouponCode  AND TTCC.object = ''couponcode'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST ON (TTST.sourceId = CCSTL.idStandupTraining AND TTST.object = ''standuptraining'') '
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTCC.destinationId IS NOT NULL AND TTST.destinationId IS NOT NULL'
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.2: Copy CouponCode - Coupon Code to ILT Links Inserted', 0)

		-- copy coupon code to catalog links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCodeToCatalogLink] '
	   			 + '( '
	   			 + '	idCouponCode, '
	   			 + '	idCatalog '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTCC.destinationId, '
	   			 + '	TTC.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCodeToCatalogLink] CCTCL '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC ON CC.idCouponCode = CCTCL.idCouponCode '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC ON (TTCC.sourceId = CCTCL.idCouponCode AND TTCC.object = ''couponcode'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = CCTCL.idCatalog AND TTC.object = ''catalog'') '
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTCC.destinationId IS NOT NULL AND TTC.destinationId IS NOT NULL'
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.3: Copy CouponCode - Coupon Code to Catalog Links Inserted', 0)

		-- copy coupon code to learning path links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCodeToLearningPathLink] '
	   			 + '( '
	   			 + '	idCouponCode, '
	   			 + '	idLearningPath '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTCC.destinationId, '
	   			 + '	TTLP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCodeToLearningPathLink] CCLPL '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC ON CC.idCouponCode = CCLPL.idCouponCode '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC on (TTCC.sourceId = CCLPL.idCouponCode AND TTCC.object = ''couponcode'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = CCLPL.idLearningPath AND TTLP.object = ''learningPaths'') '
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTCC.destinationId IS NOT NULL AND TTLP.destinationId IS NOT NULL'
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.4: Copy CouponCode - Coupon Code to Learning Path Links Inserted', 0)

		-- copy coupon to course links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCouponCodeToCourseLink] '
	   			 + '( '
	   			 + '	idCouponCode, '
	   			 + '	idCourse '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTCC.destinationId, '
	   			 + '	TTC.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCodeToCourseLink] CCTCL '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCouponCode] CC ON CC.idCouponCode = CCTCL.idCouponCode '	   			 
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCC on (TTCC.sourceId = CCTCL.idCouponCode AND TTCC.object = ''couponcode'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CCTCL.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTCC.destinationId IS NOT NULL AND TTC.destinationId IS NOT NULL'

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8.5: Copy CouponCode - Coupon Code to Course Links Inserted', 0)

		/*

		RETURN

		*/
			   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_CouponCode_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 8: Copy CouponCode - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_CouponCode_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END