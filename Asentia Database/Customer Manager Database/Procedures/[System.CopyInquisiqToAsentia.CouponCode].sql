
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.CouponCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.CouponCode]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.CouponCode]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	

	/*****************************************************COUPON CODE***********************************************************************/		
		-- tblCouponCode
		SET @sql = ' INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCode]'
				+ ' ( '
				+ '		idSite, '
				+ '		usesAllowed, '
				+ '		discount, '
				+ '		discountType, '
				+ '		code, '
				+ '		isDeleted, '
				+ '		comments, '
				+ '		forCourse, '
				+ '		forCatalog, '
				+ '		dtStart, '
				+ '		dtEnd, '
				+ '		isSingleUsePerUser, '
				+ '		dtDeleted, '
				+ '		forLearningPath, '
				+ '		forStandupTraining ' 
				+ ' ) '
				+ ' SELECT DISTINCT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) +','						-- idSite
				+ '		CC.usesAllowed,'													-- uses allowed
				+ '		CASE WHEN CC.value < 0 THEN CC.value * -1'							-- discount
				+ '			 WHEN CC.value >= 0.1 AND CC.value <= 0.9 THEN CC.value * 100'
				+ '		ELSE CC.value END,'
				+ '		CASE WHEN CC.value = 0 THEN 1'										-- discount type
				+ '			 WHEN CC.value >= 0.1 AND CC.value <= 0.9 THEN 4'
				+ '			 WHEN CC.value < 0 THEN 3'
				+ '		ELSE 2 END,'
				+ '		CC.code,'															-- code
				+ '		CC.isDeleted,'														-- is deleted?
				+ '		CC.comments,'														-- comments
				+ '		CASE WHEN CC.isCourse = 0 THEN 1 '									-- for course
				+ '			 WHEN CC.isCourse = 1 AND courseLink.idCourse IS NULL THEN 2 '
				+ '		ELSE 3 END,'														
				+ '		CASE WHEN CC.isCatalog = 0 THEN 1 '									-- for catalog
				+ '			 WHEN CC.isCatalog = 1 AND catalogLink.idCatalog IS NULL THEN 2 '
				+ '		ELSE 3 END,'														
				+ '		CC.dtStart,'														-- date start
				+ '		CC.dtEnd,'															-- date end
				+ '		CC.isSingleUsePerUser, '											-- is single use per user?
				+ '		CASE WHEN CC.isDeleted = 1 THEN GETUTCDATE() ELSE NULL END, '		-- date deleted
				+ '		1, '																-- for learning path
				+ '		1 '																	-- for standup training	
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCode] CC'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCourseLink] courseLink ON courseLink.idCouponCode = CC.idCouponCode'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCodeCatalogLink] catalogLink ON catalogLink.idCouponCode = CC.idCouponCode'
				+ ' WHERE CC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)

		EXEC (@sql)

		-- insert idCouponCode mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SCC.idCouponCode, '
				+ '		DCC.idCouponCode,'
				+ '		''couponcode''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblCouponCode] DCC '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCouponCode] SCC ON SCC.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SCC.code = DCC.code AND SCC.usesAllowed = DCC.usesAllowed'
				+ ' WHERE DCC.idSite IS NOT NULL AND DCC.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SCC.idCouponCode IS NOT NULL AND DCC.idCouponCode IS NOT NULL'
		
		EXEC(@sql)

		
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaCouponCode_Success'

		
		
	SET XACT_ABORT OFF

END

GO