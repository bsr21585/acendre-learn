-- =====================================================================
-- PROCEDURE: [AccountUser.ResetPassword]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[AccountUser.ResetPassword]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.ResetPassword]
GO

/*

Resets the user's password.

*/
CREATE PROCEDURE [AccountUser.ResetPassword]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@userFirstName			NVARCHAR(255)	OUTPUT,
	@userLastName			NVARCHAR(255)	OUTPUT,
	@userEmail				NVARCHAR(255)	OUTPUT,
	@userCulture			NVARCHAR(20)	OUTPUT,
	
	@username				NVARCHAR(512),
	@newPassword			NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @count INT
	SET @count = 0
	
	DECLARE @idAccountUser INT
	DECLARE @idAccount INT
	
	/*
	
	administrator account cannot be reset
	
	*/
	
	IF @username = 'admin' OR @username = 'administrator'
		BEGIN
		SET @Return_Code = 4 -- failed login
		SET @Error_Description_Code = 'AccountUserResetPassword_CannotResetAdmin'   
		RETURN 1
		END
		
	/*
	
	attempt to reset account user
	
	*/
		
	SELECT @count = COUNT(1)
	FROM tblAccountUser AU
	WHERE AU.username = @username
	
	IF (@count) = 0 
		BEGIN
		SET @Return_Code = 1 -- not found
		SET @Error_Description_Code = 'AccountUserResetPassword_UsernameNotFound'   
		END
		
	IF (@count) > 1
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'AccountUserResetPassword_DuplicateAccount'    
		RETURN 1
		END
		
	IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccountUser = AU.idAccountUser,
			@userFirstName = AU.firstName,
			@userLastName = AU.lastName,
			@userEmail = AU.email
			-- TODO: userCulture
		FROM tblAccountUser AU
		WHERE AU.username = @username
		
		IF (@userEmail IS NULL)
			BEGIN
			SET @Return_Code = 1 -- no email address
			SET @Error_Description_Code = 'AccountUserResetPassword_NoEmailAddress'   
			RETURN 1	
			END
	
		-- reset the password
		UPDATE tblAccountUser SET
			[password] = dbo.GetHashedString('SHA1', @newPassword)
		WHERE idAccountUser = @idAccountUser
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
	
	------------------------------------------------------
	/*
	
	attempt to reset account administrator
	
	*/
		
	SELECT @count = COUNT(1)
	FROM tblAccount A
	WHERE A.username = @username
	
	IF (@count) = 0 
		BEGIN
		SET @Return_Code = 1 -- not found
		SET @Error_Description_Code = 'AccountUserResetPassword_UsernameNotFound'        
		END
		
	IF (@count) > 1
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'AccountUserResetPassword_DuplicateAccount'         
		RETURN 1
		END
		
	IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccount = A.idAccount,
			@userFirstName = A.contactFirstName,
			@userLastName = A.contactLastName,
			@userEmail = A.contactEmail
			-- TODO: userCulture
		FROM tblAccount A
		WHERE A.username = @username
		
		IF (@userEmail IS NULL)
			BEGIN
			SET @Return_Code = 1 -- no email address
			SET @Error_Description_Code = 'AccountUserResetPassword_NoEmailAddress'       
			RETURN 1	
			END
	
		-- reset the password
		UPDATE tblAccount SET
			[password] = dbo.GetHashedString('SHA1', @newPassword)
		WHERE idAccount = @idAccount
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
	
	END
GO