
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetContentPackagePathsForFileSystemCleanup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetContentPackagePathsForFileSystemCleanup]
GO


/*

Get Object IDs of the objects in the database

*/
CREATE PROCEDURE [dbo].[System.GetContentPackagePathsForFileSystemCleanup]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@DBServerName			NVARCHAR(50),
	@DatabaseName			NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	  
		SET @sql = 'SELECT '
				 + '	[path] '
				 + 'FROM [' + @DBServerName + '].[' + @DatabaseName + '].[dbo].[tblContentPackage]'
						
		
		EXEC(@sql)
		
		
		
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemGetContentPackagePathsForFileSystemCleanup_Success'
		
	SET XACT_ABORT OFF

END
GO