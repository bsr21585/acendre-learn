-- =====================================================================
-- PROCEDURE: [ExceptionLog.GetGrid]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[ExceptionLog.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ExceptionLog.GetGrid]
GO

/*
Gets a listing from the exception log for one account or from all accounts.
*/
CREATE PROCEDURE [ExceptionLog.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,

	@searchParam			NVARCHAR(255),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT,

	@idAccount				INT				= 0
)
AS
	BEGIN
		SET NOCOUNT ON

		-- temp table to hold the list of exceptions we want (either all exceptions, or just one account)
		CREATE TABLE #ExceptionLog
	   (
		  idExceptionLog INT NOT NULL IDENTITY(1, 1),
		  idAccount INT NOT NULL,
		  company NVARCHAR(255) NOT NULL, 
		  idSite INT NULL,
		  idUser INT NULL,
		  username NVARCHAR(512) NULL,
		  displayName NVARCHAR(768) NULL,
		  [timestamp] DATETIME NOT NULL,
		  [page] NVARCHAR(512) NOT NULL,
		  exceptionType NVARCHAR(512) NOT NULL,
		  exceptionMessage NVARCHAR(MAX) NOT NULL,
		  exceptionStackTrace NVARCHAR(MAX) NOT NULL
	   )

	   DECLARE @databaseServer NVARCHAR(100)
	   DECLARE @databaseName NVARCHAR(100)
	   DECLARE @sql NVARCHAR(MAX)

	   IF(@idAccount = 0)
			BEGIN
			
			SELECT @idAccount = MIN(idAccount) FROM tblAccount

			-- iterate through all accounts and add the exceptions for each database to the temp table
			WHILE @idAccount IS NOT NULL
			BEGIN
			
				-- set database server and database name
				SELECT
					@databaseServer = D.serverName,
					@databaseName = A.databaseName
				FROM tblAccount A
				LEFT JOIN tblDatabaseServer D ON D.idDatabaseServer = A.idDatabaseServer
				WHERE A.idAccount = @idAccount

				-- if the database exists add the exceptions to the temp exception log
				IF (EXISTS (SELECT name 
				FROM master.dbo.sysdatabases 
				WHERE ('[' + name + ']' = @databaseName 
				OR name = @databaseName)))
				BEGIN
					-- get exceptions for one account
					SET @sql =' INSERT INTO #ExceptionLog '
						+' ( '
						+'	 idAccount,'
						+'	 company,'
						+'	 idSite,'
						+'	 idUser,'
						+'	 username,'
						+'	 displayName,'
						+'	 [timestamp],'
						+'	 [page],'
						+'	 exceptionType,'
						+'	 exceptionMessage,'
						+'	 exceptionStackTrace'
						+' ) '
						+' SELECT '
						+' A.idAccount, '
						+' A.company, '
						+' E.idSite, '
						+' E.idUser, '
						+' U.username, '
						+' U.displayName, '
						+' E.[timestamp], '
						+' E.[page], '
						+' E.exceptionType, '
						+' E.exceptionMessage, '
						+' E.exceptionStackTrace ' 
						+' FROM [dbo].'+ '[tblAccount] A, '
						+' [' + @databaseServer + '].['+ @databaseName + '].[dbo].'+ '[tblExceptionLog] E '
						+' LEFT JOIN [' + @databaseServer + '].[' + @databaseName + '].[dbo].' + '[tblUser] U '
						+' ON U.idUser = E.idUser ' 
						+' WHERE A.idAccount ='+ CAST(@idAccount AS NVARCHAR) + ' AND A.isDeleted = 0'
		
					EXEC(@sql)		
				END

				SELECT @idAccount = MIN(idAccount) FROM tblAccount WHERE idAccount > @idAccount
			END
			
			END
		ELSE
			BEGIN
			-- set database server and database name
			SELECT
				@databaseServer = D.serverName,
				@databaseName = A.databaseName
			FROM tblAccount A
			LEFT JOIN tblDatabaseServer D ON D.idDatabaseServer = A.idDatabaseServer
			WHERE A.idAccount = @idAccount

			-- get exceptions for one account
			SET @sql =' INSERT INTO #ExceptionLog '
				+' ( '
				+'	 idAccount,'
				+'	 company,'
				+'	 idSite,'
				+'	 idUser,'
				+'	 username,'
				+'	 displayName,'
				+'	 [timestamp],'
				+'	 [page],'
				+'	 exceptionType,'
				+'	 exceptionMessage,'
				+'	 exceptionStackTrace'
				+' ) '
				+' SELECT '
				+' A.idAccount, '
				+' A.company, '
				+' E.idSite, '
				+' E.idUser, '
				+' U.username, '
				+' U.displayName, '
				+' E.[timestamp], '
				+' E.[page], '
				+' E.exceptionType, '
				+' E.exceptionMessage, '
				+' E.exceptionStackTrace ' 
				+' FROM [dbo].'+ '[tblAccount] A, '
				+' [' + @databaseServer + '].['+ @databaseName + '].[dbo].'+ '[tblExceptionLog] E '
				+' LEFT JOIN [' + @databaseServer + '].[' + @databaseName + '].[dbo].' + '[tblUser] U '
				+' ON U.idUser = E.idUser ' 
				+' WHERE A.idAccount ='+ CAST(@idAccount AS NVARCHAR) + ' AND A.isDeleted = 0'
		
			EXEC(@sql)		
			END
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
	
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #ExceptionLog
			
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idExceptionLog,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'company' THEN company END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'company' THEN company END) END
						)
						AS [row_number]
					FROM #ExceptionLog
					
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idExceptionLog, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				EXL.idExceptionLog,
				EXL.company, 
				CONVERT(VARCHAR(12), EXL.[timestamp], 107) AS [timestamp],
				LTRIM(right(CONVERT(VARCHAR(25), EXL.[timestamp], 100), 7)) AS [time],		
				CASE WHEN EXL.idUser IS NULL THEN '[N/A]' ELSE EXL.displayName + '(' + EXL.username + ')' END AS [user],
				EXL.page,
				EXL.exceptionType,
				EXL.exceptionMessage,
				EXL.exceptionStackTrace,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #ExceptionLog EXL ON EXL.idExceptionLog = SelectedKeys.idExceptionLog
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM #ExceptionLog
			INNER JOIN CONTAINSTABLE(tblExceptionLog, *, @searchParam) K ON K.[key] = tblExceptionLog.idExceptionLog
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idExceptionLog,
						ROW_NUMBER() OVER (ORDER BY
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END DESC,
							CASE WHEN @orderAsc = 0 THEN (CASE WHEN @orderColumn = 'company' THEN company END) END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionMessage' THEN exceptionMessage END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'exceptionType' THEN exceptionType END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'user' THEN user END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'timestamp' THEN [timestamp] END) END,
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN (CASE WHEN @orderColumn = 'company' THEN company END) END
						)
						AS [row_number]
					FROM #ExceptionLog
					INNER JOIN CONTAINSTABLE(tblExceptionLog, *, @searchParam) K ON K.[key] = tblExceptionLog.idExceptionLog
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idExceptionLog, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				EXL.idExceptionLog, 
				EXL.company,
				EXL.[timestamp],					
				CASE WHEN EXL.idUser IS NULL THEN '[N/A]' ELSE EXL.displayName + ' (' + EXL.username + ')' END AS [user],
				EXL.page,
				EXL.exceptionType,
				EXL.exceptionMessage,
				EXL.exceptionStackTrace,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN #ExceptionLog EXL ON EXL.idExceptionLog = SelectedKeys.idExceptionLog
			ORDER BY SelectedKeys.[row_number]
			
			END

			DROP TABLE #ExceptionLog
	END		
			
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO