-- =====================================================================
-- PROCEDURE: [AccountUser.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[AccountUser.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.Save]
GO

CREATE PROCEDURE [AccountUser.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, -- default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0, -- will fail if not specified
	
	@idAccountUser			INT				OUTPUT,
	@idAccount				INT,
	@firstName				NVARCHAR(255),
	@lastName				NVARCHAR(255),
	@email					NVARCHAR(255),
	@username				NVARCHAR(512),
	@password				NVARCHAR(512),
	@idRole					INT,
	@isActive				BIT
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/* validate caller permission */
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller
	
	DECLARE @isCallerEditingSelf BIT
	DECLARE @isCallerSystemAdmin BIT
	DECLARE @isCallerSystemUser BIT
	DECLARE @isCallerAccountAdmin BIT
	DECLARE @isCallerAccountUser BIT
	
	SELECT @isCallerEditingSelf =	CASE WHEN @idAccountUser = @idCaller THEN 1 ELSE 0 END
	
	SELECT @isCallerSystemAdmin =	CASE WHEN @idCallerAccount = 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
								  
	SELECT @isCallerSystemUser =	CASE WHEN @idCallerAccount = 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountAdmin =	CASE WHEN @idCallerAccount > 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountUser =	CASE WHEN @idCallerAccount > 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	-- check permissions only if caller is not editing their own account
	
	IF @isCallerEditingSelf = 0
		BEGIN
		IF -- system users cannot create/edit other system users
		   (@isCallerSystemUser = 1 AND @idAccount = 1)
		   OR
		   -- account administrators cannot create/edit users outside of their account
		   (@isCallerAccountAdmin = 1 AND @idAccount <> @idCallerAccount)
		   OR
		   -- account users cannot create/edit other users
		   (@isCallerAccountUser = 1)
			BEGIN
			SELECT @Return_Code = 3 -- caller permission error
		    SET @Error_Description_Code = 'AccountUserSave_PermissionError'
			RETURN 1
			END
		END
	
	/*
	
	validate role
	
	*/
	
	-- system users can only belong to idAccount 1
	
	IF (@idRole = 1 AND @idAccount <> 1)
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'AccountUserSave_UserNotUnique'
		RETURN 0
		END
		 
	/*
	
	validate uniqueness across all possible login names
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccountUser
		WHERE (idAccountUser <> @idAccountUser AND username = @username)		
		) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'AccountUserSave_UserNotUnique'
		RETURN 0 
		END
		
	/*
	
	save the data
	
	*/
	
	IF (@idAccountUser = 0 OR @idAccountUser is null)
		
		BEGIN
		
		-- insert the new object
		
		INSERT INTO tblAccountUser (
			idAccount,
			firstName,
			lastName,
			displayName,
			email,
			username,
			[password],
			idRole,
			isActive
		)			
		VALUES (
			@idAccount,
			@firstName,
			@lastName,
			@lastName + ', ' + @firstName,
			@email,
			@username,
			dbo.GetHashedString('SHA1', @password),
			@idRole,
			@isActive
		)
		
		-- get the new object id and return successfully
		
		SELECT @idAccountUser = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the object id exists
		
		IF (SELECT COUNT(1) FROM tblAccountUser WHERE idAccountUser = @idAccountUser) = 0
			
			BEGIN
				SELECT @idAccountUser = @idAccountUser
				SET @Return_Code = 1 --(1 is 'details not found')
				SET @Error_Description_Code = 'AccountUserSave_NoRecordFound'
				RETURN 0
			END
			
		-- update existing object's properties

		UPDATE tblAccountUser SET
			idAccount = @idAccount,
			firstName = @firstName,
			lastName = @lastName,
			displayName = @lastName + ', ' + @firstName,
			email = @email,
			username = @username,
			[password] = (CASE WHEN @password IS NULL THEN [password] ELSE dbo.GetHashedString('SHA1', @password) END),
			idRole = @idRole,
			isActive = @isActive
		WHERE idAccountUser = @idAccountUser
		
		-- get the id and return successfully
		
		SELECT @idAccountUser = @idAccountUser
				
		END
	
	SELECT @Return_Code = 0
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO