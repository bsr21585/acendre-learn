-- =====================================================================
-- PROCEDURE: [AccountUser.Details]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[AccountUser.Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [AccountUser.Details]
GO

/*
Return all the properties for a given object id.
*/
CREATE PROCEDURE [AccountUser.Details]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		INT				OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,
	
	@idAccountUser				INT				OUTPUT,
	@idAccount					INT				OUTPUT,
	@firstName					NVARCHAR(255)	OUTPUT,
	@lastName					NVARCHAR(255)	OUTPUT,
	@displayName				NVARCHAR(512)	OUTPUT,
	@email						NVARCHAR(255)	OUTPUT,
	@username					NVARCHAR(512)	OUTPUT,
	@password					NVARCHAR(512)	OUTPUT,
	@idRole						INT				OUTPUT,
	@roleName					NVARCHAR(255)	OUTPUT,
	@isActive					BIT				OUTPUT
)
AS
	BEGIN
	SET NOCOUNT ON
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller
	
	DECLARE @isCallerViewingSelf BIT
	DECLARE @isCallerSystemAdmin BIT
	DECLARE @isCallerSystemUser BIT
	DECLARE @isCallerAccountAdmin BIT
	DECLARE @isCallerAccountUser BIT
	
	SELECT @isCallerViewingSelf =	CASE WHEN @idAccountUser = @idCaller THEN 1 ELSE 0 END
	
	SELECT @isCallerSystemAdmin =	CASE WHEN @idCallerAccount = 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
								  
	SELECT @isCallerSystemUser =	CASE WHEN @idCallerAccount = 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 1 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountAdmin =	CASE WHEN @idCallerAccount > 1 
											  AND @idCaller = 1 
											  AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	SELECT @isCallerAccountUser =	CASE WHEN @idCallerAccount > 1 
										      AND @idCaller > 1 
										      AND @idCallerRole = 2 
									THEN 1 
									ELSE 0 END
									
	-- if caller is not viewing self, make sure the
	-- caller is not an account user
	
	IF @isCallerViewingSelf = 0
		BEGIN
		IF -- account users cannot view any other users
		   (@isCallerAccountUser = 1)
			BEGIN
			SET @Return_Code = 3 -- sort of (caller not member of specified site).
			SET @Error_Description_Code = 'AccountUserDetails_PermissionError'
			RETURN 1
			END
		END
		
	/*

	validate that the object exists
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccountUser
		WHERE idAccountUser = @idAccountUser
		) = 0 
		BEGIN
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'AccountUserDetails_NoRecordFound'
		RETURN 1
		END
	
	/*
	
	validate caller permission
	
	*/
	
	IF @isCallerSystemAdmin <> 1 OR @isCallerViewingSelf <> 1
	
		BEGIN
		
		IF (
			SELECT COUNT(1)
			FROM tblAccountUser AU 
			WHERE AU.idAccountUser = @idAccountUser
			AND (
					-- system users cannot view other system users
					(@isCallerSystemUser = 1 AND AU.idAccount = 1)
					OR
					-- account administrators cannot view users outside of their account
					(@isCallerAccountAdmin = 1 AND AU.idAccount <> @idCallerAccount)
				)
			) > 0 
			
			BEGIN
			SET @Return_Code = 3 -- sort of (caller not member of specified site).
			SET @Error_Description_Code = 'AccountUserDetails_PermissionError'
			RETURN 1
			END
				
		END
	
	/*
	
	get the data 
	
	*/
	
	SELECT
		@idAccountUser = AU.idAccountUser,
		@idAccount = AU.idAccount,
		@firstName = AU.firstName,
		@lastName = AU.lastName,
		@displayName = AU.displayName,
		@email = AU.email,
		@username = AU.username,
		@password = AU.[password],
		@idRole = AU.idRole,
		@roleName = R.roleName,
		@isActive = AU.isActive
	FROM tblAccountUser AU
	LEFT JOIN tblRole R ON R.idRole = AU.idRole
	WHERE AU.idAccountUser = @idAccountUser
	
	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO