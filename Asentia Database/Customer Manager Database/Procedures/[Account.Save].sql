-- =====================================================================
-- PROCEDURE: [Account.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.Save]
GO

CREATE PROCEDURE [Account.Save]
(
	@Return_Code				INT					OUTPUT,
	@Error_Description_Code		NVARCHAR(50)		OUTPUT,
	@idCallerAccount			INT					= 0, -- default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT					= 0, -- will fail if not specified
	
	@idAccount					INT					OUTPUT,
	@idDatabaseServer			INT					OUTPUT, 
	@company					NVARCHAR(255),
	@contactFirstName			NVARCHAR(255),
	@contactLastName			NVARCHAR(255),
	@contactEmail				NVARCHAR(255),
	@username					NVARCHAR(255),
	@password					NVARCHAR(512),
	@isActive					BIT,
	@isDeleted					BIT,
	@databaseName				NVARCHAR(512)
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/* validate caller permission */
		
	DECLARE @idCallerRole INT
	SELECT @idCallerRole = 0
	SELECT @idCallerRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller		
	
	IF @idCallerRole <> 1 AND @idCallerAccount <> @idAccount
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
			 
	/*
	
	validate uniqueness (if required)
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM tblAccount
		WHERE (idAccount <> @idAccount AND username = @username)
		AND (isDeleted IS NULL OR isDeleted = 0)
		
		) > 0
		BEGIN
		SELECT @Return_Code = 2
		SELECT @Error_Description_Code  = 'AccountSave_UserNameNotUnique'
		RETURN 0 
		END
		
	/*
	
	save the data
	
	*/
	
	IF (@idAccount = 0 OR @idAccount is null)
		
		BEGIN

			IF (
					SELECT COUNT(1) 
					FROM tblAccount 
					WHERE (databaseName=@databaseName AND idDatabaseServer=@idDatabaseServer)
					AND (isDeleted IS NULL OR isDeleted = 0)
				) > 0
				BEGIN
			    SELECT @Return_Code = 2
				SELECT @Error_Description_Code  = 'AccountSave_DatabaseNameNotUnique'
				RETURN 0 
				END
		-- insert the new object
		
		INSERT INTO tblAccount (
			idDatabaseServer,
			company,
			contactFirstName,
			contactLastName,
			contactDisplayName,
			contactEmail,
			username,
			[password],
			isActive,
			isDeleted,
			dtCreated,
			databaseName
		)			
		VALUES (
			@idDatabaseServer,
			@company,
			@contactFirstName,
			@contactLastName,
			@contactLastName + ', ' + @contactFirstName,
			@contactEmail,
			@username,
			dbo.GetHashedString('SHA1', @password),
			@isActive,
			@isDeleted,
			GETUTCDATE(),
			@databaseName
		)
		
		-- get the new object id and return successfully
		
		SELECT @idAccount = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the object id exists
		
		IF (SELECT COUNT(1) FROM tblAccount WHERE idAccount = @idAccount) = 0
			
			BEGIN
				SELECT @idAccount = @idAccount
				SELECT @Return_Code = 1
				RETURN 0
			END
			
		-- update existing object's properties
		
		UPDATE tblAccount SET
			company = @company,
			contactFirstName = @contactFirstName,
			contactLastName = @contactLastName,
			contactDisplayName = @contactLastName + ', ' + @contactFirstName,
			contactEmail = @contactEmail,
			username = @username,
			[password] = dbo.GetHashedString('SHA1', @password),
			isActive = @isActive,
			isDeleted = @isDeleted
		WHERE idAccount = @idAccount 
		
		-- get the id and return successfully
		
		SELECT @idAccount = @idAccount
				
		END
	
	SELECT @Return_Code = 0
		
	END
		
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO