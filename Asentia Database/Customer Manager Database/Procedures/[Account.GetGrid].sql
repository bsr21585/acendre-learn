-- =====================================================================
-- PROCEDURE: [Account.GetGrid]

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[Account.GetGrid]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.GetGrid]
GO

CREATE PROCEDURE [Account.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		/* get caller permission */
		
		DECLARE @idUserRole INT
		SELECT @idUserRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller		
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblAccount
			WHERE @idUserRole = 1
			AND idAccount <> 1
			AND (isDeleted IS NULL OR isDeleted = 0)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idAccount,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN company END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN company END
						)
						AS [row_number]
					FROM tblAccount
					WHERE @idUserRole = 1
					AND idAccount <> 1
					AND (isDeleted IS NULL OR isDeleted = 0)

				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idAccount, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idAccount, 
				A.company, 
				A.contactDisplayName,
				A.contactEmail,
				A.isActive,
				CONVERT(BIT, 1) AS isLogonOn,
				CONVERT(BIT, 1) AS isModifyOn,
				SelectedKeys.[row_number]		
			FROM SelectedKeys
			JOIN tblAccount A ON A.idAccount = SelectedKeys.idAccount
			WHERE (A.isDeleted IS NULL OR A.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblAccount
			INNER JOIN CONTAINSTABLE(tblAccount, *, @searchParam) K ON K.[key] = tblAccount.idAccount
			WHERE @idUserRole = 1
			AND idAccount <> 1
			AND (isDeleted IS NULL OR isDeleted = 0)
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idAccount,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN idAccount END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN idAccount END
						)
						AS [row_number]
					FROM tblAccount
					INNER JOIN CONTAINSTABLE(tblAccount, *, @searchParam) K ON K.[key] = tblAccount.idAccount
					WHERE @idUserRole = 1
					AND idAccount <> 1
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idAccount, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idAccount, 
				A.company, 
				A.contactDisplayName,
				A.contactEmail,
				A.isActive,
				CONVERT(BIT, 1) AS isLogonOn,
				CONVERT(BIT, 1) AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblAccount A ON A.idAccount = SelectedKeys.idAccount
			WHERE (A.isDeleted IS NULL OR A.isDeleted = 0)
			ORDER BY SelectedKeys.[row_number]
			
			END
		
	END
GO


