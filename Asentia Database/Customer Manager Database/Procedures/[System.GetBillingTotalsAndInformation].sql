-- =====================================================================
-- PROCEDURE: [System.GetBillingTotalsAndInformation]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.GetBillingTotalsAndInformation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.GetBillingTotalsAndInformation]
GO

/*

Returns the information and totals needed for billing Asentia clients. 

*/

CREATE PROCEDURE [System.GetBillingTotalsAndInformation]
AS
	BEGIN
	SET NOCOUNT ON
	
	/*

	Get all account contracts and place them into a temporary table.

	*/

	DECLARE @AccountContractInfoAndTotals TABLE (
		idAccount						INT				NOT NULL,
		idAccountContract				INT				NOT NULL,
		serverName						NVARCHAR(255)	NOT NULL,
		databaseName					NVARCHAR(255)	NOT NULL,
		company							NVARCHAR(255)	NOT NULL,
		isReseller						BIT				NULL,
		allowedUsers					INT				NULL,
		numUsersLoggedIn				INT				NULL,
		disabledUserRatio				FLOAT			NULL,
		disabledUserThresholdInterval	INT				NULL,
		disabledUserThresholdTimeframe	NVARCHAR(4)		NULL,
		diskSpaceLimitKB				INT				NULL,
		portalIdList					NVARCHAR(MAX)	NOT NULL,
		portalNameList					NVARCHAR(MAX)	NOT NULL,
		contractNotes					NVARCHAR(MAX)	NULL,
		activeUsers						INT				NULL,
		disabledUsers					INT				NULL,
		totalUsers						INT				NULL,
		totalDiskSpaceKB				INT				NULL
	)

	INSERT INTO @AccountContractInfoAndTotals (
		idAccount,
		idAccountContract,
		serverName,
		databaseName,
		company,
		--isReseller, -- not yet implemented
		allowedUsers,		
		disabledUserRatio,
		disabledUserThresholdInterval,
		disabledUserThresholdTimeframe,
		diskSpaceLimitKB,
		portalIdList,
		portalNameList,
		contractNotes	
	)
	SELECT
		A.idAccount,
		AC.idAccountContract,
		DS.serverName,
		A.databaseName,
		A.company,
		--A.isReseller -- not yet implemented
		AC.allowedUsers,
		AC.disabledUserRatio,
		AC.disabledUserThresholdInterval,
		AC.disabledUserThresholdTimeframe,
		AC.diskSpaceLimitKB,
		STUFF((
			SELECT ',' + CONVERT(NVARCHAR, ACPLS.idPortal)
			FROM tblAccountContractToPortalLink ACPLS
			WHERE ACPLS.idAccountContract = AC.idAccountContract
			ORDER BY ACPLS.idPortal
			FOR XML PATH('')
		), 1, 1, '') AS portalIdList,
		STUFF((
			SELECT ', ' + ACPLS.portalName
			FROM tblAccountContractToPortalLink ACPLS
			WHERE ACPLS.idAccountContract = AC.idAccountContract
			ORDER BY ACPLS.portalName
			FOR XML PATH('')
		), 1, 2, '') AS portalNameList,
		AC.contractNotes	
	FROM tblAccountContract AC
	LEFT JOIN tblAccount A ON A.idAccount = AC.idAccount
	LEFT JOIN tblDatabaseServer DS ON DS.idDatabaseServer = A.idDatabaseServer
	WHERE A.isActive = 1 AND A.isDeleted = 0

	/*

	Cursor through all account contracts, and get the user counts and disk space used for each.

	*/

	-- gets the following counts across all portals in the contract
	--		- active users: defined as currently active users or disabled users who have been disabled 
	--		  for less than the contract's disabled threshold
	--
	--		- disabled users: defined as disabled users who have been disabled for longer than the
	--		  contract's disabled threshold, or active users who have been expired for longer than
	--		  the contract's disabled threshold
	--
	--		- total users: the total number of users in the site, used as a reference for accuracy
	--		  of the other counts, i.e. active + disabled = total
	--
	--		- total disk space in kb: the total of all disk space used by objects in the system

	DECLARE @idAccountContract				INT
	DECLARE @databaseServer					NVARCHAR(255)
	DECLARE @databaseName					NVARCHAR(255)
	DECLARE @idSiteList						NVARCHAR(MAX)
	DECLARE @disabledUserRatio				FLOAT
	DECLARE @disabledUserThresholdInterval	INT
	DECLARE @disabledUserThresholdTimeframe	NVARCHAR(4)

	DECLARE @params							NVARCHAR(MAX)
	DECLARE @sql							NVARCHAR(MAX)
	DECLARE @activeUsers					INT
	DECLARE @disabledUsers					INT
	DECLARE @totalUsers						INT
	DECLARE @totalDiskSpaceKB				INT
	DECLARE @numUsersLoggedIn				INT

	SET @params = '@activeUsers INT OUTPUT, '
				+ '@disabledUsers INT OUTPUT, '
				+ '@totalUsers INT OUTPUT, '
				+ '@totalDiskSpaceKB INT OUTPUT, '
				+ '@numUsersLoggedIn INT OUTPUT '
	
	-- declare cursor
	DECLARE accountContractsCursor CURSOR LOCAL FOR
		SELECT 
			idAccountContract,
			serverName,
			databaseName,
			portalIdList,
			disabledUserThresholdInterval,
			disabledUserThresholdTimeframe
		FROM @AccountContractInfoAndTotals

	OPEN accountContractsCursor

	FETCH NEXT FROM accountContractsCursor INTO 
		@idAccountContract,
		@databaseServer,
		@databaseName,
		@idSiteList,
		@disabledUserThresholdInterval,
		@disabledUserThresholdTimeframe

	-- loop through cursor, getting the counts from the account's Asentia database, and updating in the temp table
	WHILE @@FETCH_STATUS = 0
		BEGIN

		-- get the counts
		SET @sql = 'SELECT '
				 + ' '
				 + '	@activeUsers = (SELECT COUNT(1) '
				 + '	 FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblUser U '
				 + '	 WHERE U.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') '
				 + '	 AND (U.isDeleted IS NULL OR U.isDeleted = 0) '
				 + '	 AND ( '
				 + '		  (U.isActive = 1 AND (dtExpires IS NULL OR dtExpires > DATEADD(' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdTimeframe, 'yyyy')) + ', -' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdInterval, 0)) + ', GETUTCDATE()))) '
				 + '		  OR '
				 + '		  (U.isActive = 0 AND  U.dtMarkedDisabled > DATEADD(' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdTimeframe, 'yyyy')) + ', -' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdInterval, 0)) + ', GETUTCDATE())) '
				 + '		 ) '
				 + '	 ), '
				 + ' '
				 + '	@disabledUsers = (SELECT COUNT(1) '
				 + '	 FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblUser U '
				 + '	 WHERE U.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') '
				 + '	 AND (U.isDeleted IS NULL OR U.isDeleted = 0) '
				 + '	 AND ( '
				 + '		  (U.isActive = 0 AND U.dtMarkedDisabled <= DATEADD(' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdTimeframe, 'yyyy')) + ', -' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdInterval, 0)) + ', GETUTCDATE())) '
				 + '		  OR '
				 + '		  (U.isActive = 1 AND dtExpires IS NOT NULL AND dtExpires <= DATEADD(' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdTimeframe, 'yyyy')) + ', -' + CONVERT(NVARCHAR, ISNULL(@disabledUserThresholdInterval, 0)) + ', GETUTCDATE())) '
				 + '		 ) '
				 + '	), '
				 + ' '
				 + '	@totalUsers = (SELECT COUNT(1) '
				 + '	 FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblUser U '
				 + '	 WHERE U.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') '
				 + '	 AND (U.isDeleted IS NULL OR U.isDeleted = 0) '
				 + '	), '
				 + ' '
				 + '	@totalDiskSpaceKB = (SELECT '
				 + '		(SELECT ISNULL(SUM(kb), 0) FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblContentPackage CP WHERE CP.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ')) '
				 + '		+ '
				 + '		(SELECT ISNULL(SUM(kb), 0) FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblDocumentRepositoryItem DRI WHERE DRI.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') AND (DRI.isDeleted IS NULL OR DRI.isDeleted = 0)) '
				 + '		+ '
				 + '		(SELECT ISNULL(SUM(htmlKb), 0) + ISNULL(SUM(csvKb), 0) + ISNULL(SUM(pdfKb), 0) FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblReportFile RF WHERE RF.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ')) '
				 + '	), '
				 + ' '
				 + '	@numUsersLoggedIn = (SELECT COUNT(1) '
				 + '	 FROM [' + @databaseServer + '].[' + @databaseName + '].dbo.tblUser U '
				 + '	 WHERE U.idSite IN (' + CONVERT(NVARCHAR, @idSiteList) + ') '
				 + '	 AND (U.isDeleted IS NULL OR U.isDeleted = 0) AND U.dtLastLogin IS NOT NULL AND U.dtLastLogin >= DATEADD(MONTH, -1, GETUTCDATE()) '
				 + '	) '

		EXECUTE sp_executesql @sql, @params, @activeUsers OUTPUT, @disabledUsers OUTPUT, @totalUsers OUTPUT, @totalDiskSpaceKB OUTPUT, @numUsersLoggedIn OUTPUT

		-- update temp table
		UPDATE @AccountContractInfoAndTotals SET
			activeUsers = @activeUsers,
			disabledUsers = @disabledUsers,
			totalUsers = @totalUsers,
			totalDiskSpaceKB = @totalDiskSpaceKB,
			numUsersLoggedIn = @numUsersLoggedIn
		WHERE idAccountContract = @idAccountContract

		-- get the next row in the cursor
		FETCH NEXT FROM accountContractsCursor INTO 
			@idAccountContract,
			@databaseServer,
			@databaseName,
			@idSiteList,
			@disabledUserThresholdInterval,
			@disabledUserThresholdTimeframe

		END

	-- kill the cursor
	CLOSE accountContractsCursor
	DEALLOCATE accountContractsCursor

	-- return the data
	SELECT
		company AS [Client/Contract],
		--CASE WHEN isReseller = 1 THEN 'YES' ELSE 'NO' END AS [Is Reseller], -- not yet implemented
		activeUsers + CEILING(disabledUsers * ISNULL(disabledUserRatio, 1)) AS [Total Users],
		CASE WHEN allowedUsers IS NOT NULL THEN CONVERT(NVARCHAR, allowedUsers) ELSE 'NO LIMIT' END AS [Allowed Users],
		CASE WHEN numUsersLoggedIn IS NOT NULL THEN numUsersLoggedIn ELSE 0 END AS [# Users Logged In],
		CASE WHEN allowedUsers IS NOT NULL THEN CONVERT(NVARCHAR, ROUND(((activeUsers + CEILING(disabledUsers * ISNULL(disabledUserRatio, 1))) / allowedUsers) * 100, 2)) ELSE 'N/A' END AS [% of User Limit Used],		
		activeUsers AS [Active Users],
		disabledUsers AS [Disabled Users],
		CASE WHEN disabledUserRatio IS NOT NULL THEN disabledUserRatio ELSE 1 END AS [Disabled User Ratio],
		CASE WHEN totalDiskSpaceKB > 0 THEN dbo.[RawKBToFriendlyFormat](totalDiskSpaceKB) ELSE 'NONE' END AS [Total Disk Used],
		CASE WHEN diskSpaceLimitKB IS NOT NULL THEN dbo.[RawKBToFriendlyFormat](diskSpaceLimitKB) ELSE 'NO LIMIT' END AS [Total Disk Allowed],
		CASE WHEN diskSpaceLimitKB IS NOT NULL THEN CONVERT(NVARCHAR, ROUND((CONVERT(FLOAT, totalDiskSpaceKB) / CONVERT(FLOAT, diskSpaceLimitKB)) * 100, 2)) ELSE 'N/A' END AS [% of Disk Limit Used],
		portalNameList AS [Included Portals],
		contractNotes AS [Notes]
	FROM @AccountContractInfoAndTotals

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO