
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Enrollment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Enrollment]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Enrollment]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)


	/**********************************************ENROLLMENTS (WHERE WE DO NOT HAVE TO ONE-OFF)**********************************************/
		
		/**********ACTIVITY IMPORTED ENROLLMENTS***********/

		-- enrollments (instances only) that belong to an activity import
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','								-- idSite
				+ '		CM.destinationID,'															-- idCourse				
				+ '		UM.destinationID,'															-- idUser
				+ '		NULL,'																		-- idGroupEnrollment
				+ '		NULL,'																		-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'			-- idTimezone
				+ '		0,'																			-- isLockedByPrerequisites
				+ '		EI.code,'																	-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','	-- title
				+ '		EI.dtStart,'																-- dtStart
				+ '		EI.dtDue,'																	-- dtDue
				+ '		EI.dtEnd,'																	-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'															-- dtFirstLaunch
				+ '		EI.dtCompleted,'															-- dtCompleted
				+ '		EI.dtStart,'																-- dtCreated
				+ '		EI.dtLastSynchronized,'														-- dtLastSynchronized
				+ '		NULL,'																		-- dueInterval
				+ '		NULL,'																		-- dueTimeframe
				+ '		NULL,'																		-- expiresFromStartInterval
				+ '		NULL,'																		-- expiresFromStartTimeframe
				+ '		AIM.destinationID,'															-- idActivityImport
				+ '		EI.credits '																-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] AIM ON AIM.sourceID = EI.idActivityImport AND AIM.object = ''activityimport'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NOT NULL AND UM.destinationID IS NOT NULL AND AIM.destinationID IS NOT NULL'
		
		EXEC (@sql)				

		/**********MANUAL SINGLE ENROLLMENTS**********/

		-- manual (self-assigned or admin-assigned) enrollments - recurrence will be taken away 		
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		NULL,'																							-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		/**********BUILD RULESET ENROLLMENTS FROM GROUP ENROLLMENTS **********/

		-- add ruleset enrollments using group enrollments from Inquisiq
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idTimezone, '
				+ '		priority, '
				+ '		label, '				
				+ '		isLockedByPrerequisites, '
				+ '		isFixedDate, '
				+ '		dtStart, '
				+ '		dtEnd, '
				+ '		dtCreated, '				
				+ '		delayInterval, '
				+ '		delayTimeframe, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		recurInterval, '
				+ '		recurTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		expiresFromFirstLaunchInterval, '
				+ '		expiresFromFirstLaunchTimeframe, '
				+ '		forceReassignment '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','																-- idSite
				+ '		CM.destinationID,'																							-- idCourse				
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'											-- idTimezone
				+ '		ROW_NUMBER() OVER(PARTITION BY CM.destinationID ORDER BY CM.destinationID),'								-- priority
				+ '		''START: '' + CONVERT(NVARCHAR, GE.dtStart, 101)'															-- label
				+ '		+ CASE WHEN GE.dueInterval IS NOT NULL THEN '' DUE: '' + CONVERT(NVARCHAR, GE.dueInterval) + GE.dueTimeframe ELSE '''' END'
				+ '		+ CASE WHEN GE.recurInterval IS NOT NULL THEN '' RECUR: '' + CONVERT(NVARCHAR, GE.recurInterval) + GE.recurTimeframe ELSE '''' END'
				+ '		+ CASE WHEN GE.accessInterval IS NOT NULL THEN '' ACCESS: '' + CONVERT(NVARCHAR, GE.accessInterval) + GE.accessTimeframe ELSE '''' END,'
				+ '		CASE WHEN GE.isLockedForPrerequisites IS NOT NULL THEN GE.isLockedForPrerequisites ELSE 0 END,'				-- isLockedByPrerequisites
				+ '		GE.useFixedDates,'																							-- isFixedDate
				+ '		GE.dtStart,'																								-- dtStart
				+ '		GE.dtEnd,'																									-- dtEnd
				+ '		GE.dtStart,'																								-- dtCreated
				+ '		GE.delayInterval,'																							-- delayInterval
				+ '		GE.delayTimeframe,'																							-- delayTimeframe				
				+ '		GE.dueInterval,'																							-- dueInterval
				+ '		GE.dueTimeframe,'																							-- dueTimeframe
				+ '		GE.recurInterval,'																							-- recurInterval
				+ '		GE.recurTimeframe,'																							-- recurTimeframe
				+ '		GE.accessInterval,'																							-- expiresFromStartInterval
				+ '		GE.accessTimeframe,'																						-- expiresFromStartTimeframe
				+ '		GE.idGroupEnrollment,'																						-- expiresFromFirstLaunchInterval (use as placeholder for mapping, put idGroupEnrollment in it)
				+ '		NULL, '																										-- expiresFromFirstLaunchTimeframe
				+ '		0 '																											-- forceReassignment
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblCourse] C ON C.idCourse = GE.idCourse'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = GE.idCourse AND CM.object = ''course'''						
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = GE.idTimezone '
				+ ' WHERE C.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND CM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idRulesetEnrollment mappings (idRulesetEnrollment maps to Inquisiq idGroupEnrollment)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SGE.idGroupEnrollment, '
				+ '		DRSE.idRulesetEnrollment,'
				+ '		''rulesetenrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] DRSE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] SGE ON SGE.idGroupEnrollment = DRSE.expiresFromFirstLaunchInterval'				
				+ ' WHERE DRSE.idSite IS NOT NULL AND DRSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SGE.idGroupEnrollment IS NOT NULL AND DRSE.idRulesetEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the idGroupEnrollment additions we made to tblRulesetEnrollment, we did that to allow it to be mapped
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollment] SET '
				+ '		expiresFromFirstLaunchInterval = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] DRSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SGEM ON SGEM.destinationID = DRSE.[idRulesetEnrollment] AND SGEM.object = ''rulesetenrollment'''
				+ ' WHERE SGEM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- create rulesets for the rulesetenrollment to group mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] '
				+ ' ( '
				+ '		idSite,'
				+ '		isAny,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		1,'
				+ '		''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'''
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- create rules for the rulesetenrollment to group mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRule] '
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		userField,'
				+ '		operator,'
				+ '		textValue'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		RS.idRuleset,'
				+ '		''group'','
				+ '		''eq'','
				+ '		G.name'				
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS ON RS.label = ''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'' collate database_default'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL AND RS.idRuleset IS NOT NULL'
		
		EXEC(@sql)

		-- link rulesets to rulesetenrollments
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetToRulesetEnrollmentLink] '
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		idRulesetEnrollment'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		RS.idRuleset,'
				+ '		RSE.idRuleSetEnrollment'
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRulesetEnrollment] RSE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] GEM ON GEM.destinationID = RSE.[idRulesetEnrollment] AND GEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroupEnrollment] GE ON GE.idGroupEnrollment = GEM.sourceID'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblGroup] G ON G.idGroup = GE.idGroup'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS ON RS.label = ''Member of Group: '' + G.name + '' ##'' + CONVERT(NVARCHAR, GE.idGroupEnrollment) + ''##'' collate database_default'
				+ ' WHERE RSE.idSite IS NOT NULL AND RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND GE.idGroupEnrollment IS NOT NULL AND RSE.idRulesetEnrollment IS NOT NULL AND RS.idRuleset IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idGroupEnrollment## additions we made to ruleset labels, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] SET '
				+ '		label = REPLACE(DRS.label, '' ##'' + CONVERT(NVARCHAR, RSEM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblRuleset] DRS '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetToRulesetEnrollmentLink] RSEL ON RSEL.idRuleset = DRS.idRuleset'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RSEM ON RSEM.destinationID = RSEL.idRulesetEnrollment AND RSEM.object = ''rulesetenrollment'''
				+ ' WHERE RSEM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblRulesetEnrollmentLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollmentLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idRulesetEnrollment,'
				+ '		idLanguage,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		RSE.idRulesetEnrollment, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		RSE.label '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetEnrollmentLanguage] RSE'
				+ ' WHERE RSE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- tblRulesetLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRulesetLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idRuleset,'
				+ '		idLanguage,'
				+ '		label'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		RS.idRuleset, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		RS.label '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblRuleset] RS'
				+ ' WHERE RS.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		/**********RULESET ENROLLMENTS CASCADED**********/

		-- group enrollments from Inquisiq that were cascaded get mapped as cascaded ruleset enrollments in Asentia
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		RSEM.destinationID,'																			-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] RSEM ON RSEM.sourceID = E.idGroupEnrollment AND RSEM.object = ''rulesetenrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NOT NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL AND RSEM.destinationID IS NOT NULL'
		
		EXEC (@sql)
		
		/**********LESSON DATA AND SCORM INTERACTIONS ASSOCIATED WITH ENROLLMENTS**********/

		-- insert idEnrollment mappings (idEnrollment maps to Inquisiq idEnrollmentInstance)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEI.idEnrollmentInstance, '
				+ '		DE.idEnrollment,'
				+ '		''enrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] SEI ON SEI.name + '' ##'' + CONVERT(NVARCHAR, SEI.idEnrollmentInstance) + ''##'' = DE.title collate database_default'
				+ ' WHERE DE.idSite IS NOT NULL AND DE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEI.idEnrollmentInstance IS NOT NULL AND DE.idEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEnrollmentInstance## additions we made to enrollment titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] SET '
				+ '		title = REPLACE(DE.title, '' ##'' + CONVERT(NVARCHAR, SEIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SEIM ON SEIM.destinationID = DE.idEnrollment AND SEIM.object = ''enrollment'''
				+ ' WHERE SEIM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idEnrollment, '
				+ '		idLesson, '
				+ '		title, '
				+ '		revcode, '
				+ '		[order], '
				+ '		dtCompleted, '
				+ '		idTimezone, '
				+ '		contentTypeCommittedTo, '
				+ '		dtCommittedToContentType, '
				+ '		resetForContentChange, '
				+ '		preventPostCompletionLaunchForContentChange '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		EM.destinationID,'																		-- idEnrollment				
				+ '		LM.destinationID,'																		-- idLesson
				+ '		LD.lessonName + '' ##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','					-- title
				+ '		NULL,'																					-- revcode
				+ '		LD.[order],'																			-- order
				+ '		CASE WHEN LD.completionStatus = ''completed'' OR LD.successStatus = ''passed'' THEN'	-- dtCompleted
				+ '			LD.[timestamp] '
				+ '		ELSE NULL END,'
				+ '		E.idTimezone,'																			-- idTimezone
				+ '		NULL,'																					-- contentTypeCommittedTo
				+ '		NULL,'																					-- dtCommittedToContentType
				+ '		NULL,'																					-- resetForContentChange
				+ '		NULL'																					-- preventPostCompletionLaunchForContentChange						
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EM ON EM.sourceID = EI.idEnrollmentInstance AND EM.object = ''enrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = LD.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EM.destinationID'				
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EM.destinationID IS NOT NULL AND E.idEnrollment IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-Lesson mappings (idData-Lesson maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDL.[idData-Lesson],'
				+ '		''lessondata''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] SLD ON SLD.lessonName + '' ##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDL.title collate database_default'				
				+ ' WHERE DDL.idSite IS NOT NULL AND DDL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDL.[idData-Lesson] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to lesson data titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] SET '
				+ '		title = REPLACE(DDL.title, '' ##'' + CONVERT(NVARCHAR, SLDM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDL.[idData-Lesson] AND SLDM.object = ''lessondata'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data sco
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-Lesson], '
				+ '		manifestIdentifier, '
				+ '		completionStatus, '
				+ '		successStatus, '
				+ '		scoreScaled, '
				+ '		totalTime, '
				+ '		[timestamp], '
				+ '		idTimezone, '
				+ '		actualAttemptCount, '
				+ '		effectiveAttemptCount, '
				+ '		proctoringUser '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		LDM.destinationID,'																-- [idData-Lesson]
				+ '		''##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','							-- use manifest identifier as a mapping plceholder
				+ '		CASE WHEN CS.idSCORMVocabulary IS NULL THEN 0 ELSE CS.idSCORMVocabulary END,'	-- completionStatus
				+ '		CASE WHEN SS.idSCORMVocabulary IS NULL THEN 0 ELSE SS.idSCORMVocabulary END,'	-- successStatus
				+ '		LD.scoreScaled,'																-- scoreScaled
				+ '		LD.totalTime,'																	-- totalTime
				+ '		LD.[timestamp],'																-- timestamp
				+ '		DL.idTimezone,'																	-- idTimezone
				+ '		LD.actualAttempts,'																-- actualAttemptCount
				+ '		LD.effectiveAttempts,'															-- effectiveAttemptCount
				+ '		NULL'																			-- proctoringUser
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LDM ON LDM.sourceID = LD.idLessonData AND LDM.object = ''lessondata'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] DL ON DL.[idData-Lesson] = LDM.destinationID'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] CS ON CS.value = LD.completionStatus'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] SS ON SS.value = LD.successStatus'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND LDM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-SCO mappings (idData-SCO maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDSCO.[idData-SCO],'
				+ '		''lessondatasco''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] SLD ON ''##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDSCO.manifestIdentifier collate database_default'
				+ ' WHERE DDSCO.idSite IS NOT NULL AND DDSCO.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDSCO.[idData-SCO] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to data sco manifestIdentifier, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO] SET '
				+ '		manifestIdentifier = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDSCO.[idData-Lesson] AND SLDM.object = ''lessondatasco'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- scorm interactions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCOInt]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-SCO], '
				+ '		identifier, '
				+ '		timestamp, '
				+ '		result, '
				+ '		latency, '
				+ '		scoIdentifier '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		DSCOM.destinationID,'																	-- [idData-SCO]
				+ '		SI.identifier,'																			-- identifier
				+ '		SI.timestamp,'																			-- timestamp
				+ '		CASE WHEN SV.idSCORMVocabulary IS NOT NULL THEN SV.idSCORMVocabulary ELSE NULL END,'	-- result
				+ '		SI.latency,'																			-- latency
				+ '		NULL'																					-- scoIdentifier
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblScormInt] SI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblLessonData] LD ON LD.idLessonData = SI.idLessonData'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] DSCOM ON DSCOM.sourceID = SI.idLessonData AND DSCOM.object = ''lessondatasco'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSCORMVocabulary] SV ON SV.value = SI.result'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND DSCOM.destinationID IS NOT NULL AND DSCOM.destinationID IN (SELECT [idData-SCO] FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO])'
		
		EXEC (@sql)

	/***************************************************ARCHIVED ACTIVITY DATA*****************************************************************/

	-- archived enrollments
	SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment]'
				+ ' ( '
				+ '		idSite, '
				+ '		idCourse, '
				+ '		idUser, '
				+ '		idGroupEnrollment, '
				+ '		idRuleSetEnrollment, '				
				+ '		idTimezone, '
				+ '		isLockedByPrerequisites, '
				+ '		code, '
				+ '		title, '
				+ '		dtStart, '
				+ '		dtDue, '
				+ '		dtExpiresFromStart, '				
				+ '		dtFirstLaunch, '
				+ '		dtCompleted, '
				+ '		dtCreated, '
				+ '		dtLastSynchronized, '
				+ '		dueInterval, '
				+ '		dueTimeframe, '
				+ '		expiresFromStartInterval, '
				+ '		expiresFromStartTimeframe, '				
				+ '		idActivityImport, '
				+ '		credits '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','													-- idSite
				+ '		CM.destinationID,'																				-- idCourse				
				+ '		UM.destinationID,'																				-- idUser
				+ '		NULL,'																							-- idGroupEnrollment
				+ '		NULL,'																							-- idRulesetEnrollment
				+ '		CASE WHEN TZ.destinationID > 0 THEN TZ.destinationID ELSE 15 END,'								-- idTimezone
				+ '		CASE WHEN E.isLockedForPrerequisites IS NOT NULL THEN E.isLockedForPrerequisites ELSE 0 END,'	-- isLockedByPrerequisites
				+ '		EI.code,'																						-- code
				+ '		EI.name + '' ##'' + CONVERT(NVARCHAR, EI.idEnrollmentInstance) + ''##'','						-- title
				+ '		EI.dtStart,'																					-- dtStart
				+ '		EI.dtDue,'																						-- dtDue
				+ '		EI.dtEnd,'																						-- dtExpiresFromStart
				+ '		EI.dtFirstLaunch,'																				-- dtFirstLaunch
				+ '		EI.dtCompleted,'																				-- dtCompleted
				+ '		EI.dtStart,'																					-- dtCreated
				+ '		EI.dtLastSynchronized,'																			-- dtLastSynchronized
				+ '		E.dueInterval,'																					-- dueInterval
				+ '		E.dueTimeframe,'																				-- dueTimeframe
				+ '		E.accessInterval,'																				-- expiresFromStartInterval
				+ '		E.accessTimeframe,'																				-- expiresFromStartTimeframe
				+ '		NULL,'																							-- idActivityImport
				+ '		EI.credits '																					-- credits				
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] EI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EI.idEnrollment'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] CM ON CM.sourceID = EI.idCourse AND CM.object = ''course'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = EI.idUser AND UM.object = ''user'''				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] TZ ON TZ.sourceID = EI.idTimezone '
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EI.idActivityImport IS NULL AND E.idGroupEnrollment IS NULL '
				+ ' AND CM.destinationID IS NOT NULL AND UM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idEnrollment mappings (idEnrollment maps to Inquisiq idEnrollmentInstance)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEI.idEnrollmentInstance, '
				+ '		DE.idEnrollment,'
				+ '		''enrollment''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] SEI ON SEI.name + '' ##'' + CONVERT(NVARCHAR, SEI.idEnrollmentInstance) + ''##'' = DE.title collate database_default'
				+ ' WHERE DE.idSite IS NOT NULL AND DE.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEI.idEnrollmentInstance IS NOT NULL AND DE.idEnrollment IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEnrollmentInstance## additions we made to enrollment titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] SET '
				+ '		title = REPLACE(DE.title, '' ##'' + CONVERT(NVARCHAR, SEIM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEnrollment] DE '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SEIM ON SEIM.destinationID = DE.idEnrollment AND SEIM.object = ''enrollment'''
				+ ' WHERE SEIM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson]'
				+ ' ( '
				+ '		idSite, '
				+ '		idEnrollment, '
				+ '		idLesson, '
				+ '		title, '
				+ '		revcode, '
				+ '		[order], '
				+ '		dtCompleted, '
				+ '		idTimezone, '
				+ '		contentTypeCommittedTo, '
				+ '		dtCommittedToContentType, '
				+ '		resetForContentChange, '
				+ '		preventPostCompletionLaunchForContentChange '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		EM.destinationID,'																		-- idEnrollment				
				+ '		LM.destinationID,'																		-- idLesson
				+ '		LD.lessonName + '' ##'' + CONVERT(NVARCHAR, LD.idARCHIVELessonData) + ''##'','					-- title
				+ '		NULL,'																					-- revcode
				+ '		LD.[order],'																			-- order
				+ '		CASE WHEN LD.completionStatus = ''completed'' OR LD.successStatus = ''passed'' THEN'	-- dtCompleted
				+ '			LD.[timestamp] '
				+ '		ELSE NULL END,'
				+ '		E.idTimezone,'																			-- idTimezone
				+ '		NULL,'																					-- contentTypeCommittedTo
				+ '		NULL,'																					-- dtCommittedToContentType
				+ '		NULL,'																					-- resetForContentChange
				+ '		NULL'																					-- preventPostCompletionLaunchForContentChange						
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EM ON EM.sourceID = EI.idEnrollmentInstance AND EM.object = ''enrollment'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LM ON LM.sourceID = LD.idLesson AND LM.object = ''lesson'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEnrollment] E ON E.idEnrollment = EM.destinationID'				
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND EM.destinationID IS NOT NULL AND E.idEnrollment IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-Lesson mappings (idData-Lesson maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDL.[idData-Lesson],'
				+ '		''lessondata''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] SLD ON SLD.lessonName + '' ##'' + CONVERT(NVARCHAR, SLD.idARCHIVELessonData) + ''##'' = DDL.title collate database_default'				
				+ ' WHERE DDL.idSite IS NOT NULL AND DDL.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDL.[idData-Lesson] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to lesson data titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] SET '
				+ '		title = REPLACE(DDL.title, '' ##'' + CONVERT(NVARCHAR, SLDM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-Lesson] DDL '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDL.[idData-Lesson] AND SLDM.object = ''lessondata'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- lesson data sco
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-Lesson], '
				+ '		manifestIdentifier, '
				+ '		completionStatus, '
				+ '		successStatus, '
				+ '		scoreScaled, '
				+ '		totalTime, '
				+ '		[timestamp], '
				+ '		idTimezone, '
				+ '		actualAttemptCount, '
				+ '		effectiveAttemptCount, '
				+ '		proctoringUser '
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','									-- idSite
				+ '		LDM.destinationID,'																-- [idData-Lesson]
				+ '		''##'' + CONVERT(NVARCHAR, LD.idLessonData) + ''##'','							-- use manifest identifier as a mapping plceholder
				+ '		CASE WHEN CS.idSCORMVocabulary IS NULL THEN 0 ELSE CS.idSCORMVocabulary END,'	-- completionStatus
				+ '		CASE WHEN SS.idSCORMVocabulary IS NULL THEN 0 ELSE SS.idSCORMVocabulary END,'	-- successStatus
				+ '		LD.scoreScaled,'																-- scoreScaled
				+ '		LD.totalTime,'																	-- totalTime
				+ '		LD.[timestamp],'																-- timestamp
				+ '		DL.idTimezone,'																	-- idTimezone
				+ '		LD.actualAttempts,'																-- actualAttemptCount
				+ '		LD.effectiveAttempts,'															-- effectiveAttemptCount
				+ '		NULL'																			-- proctoringUser
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] LD'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] LDM ON LDM.sourceID = LD.idLessonData AND LDM.object = ''lessondata'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-Lesson] DL ON DL.[idData-Lesson] = LDM.destinationID'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] CS ON CS.value = LD.completionStatus'
				+ ' LEFT JOIN [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSCORMVocabulary] SS ON SS.value = LD.successStatus'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND LDM.destinationID IS NOT NULL'
		
		EXEC (@sql)

		-- insert idData-SCO mappings (idData-SCO maps to Inquisiq idLessonData)
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SLD.idLessonData, '
				+ '		DDSCO.[idData-SCO],'
				+ '		''lessondatasco''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] SLD ON ''##'' + CONVERT(NVARCHAR, SLD.idLessonData) + ''##'' = DDSCO.manifestIdentifier collate database_default'
				+ ' WHERE DDSCO.idSite IS NOT NULL AND DDSCO.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SLD.idLessonData IS NOT NULL AND DDSCO.[idData-SCO] IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idLessonData## additions we made to data sco manifestIdentifier, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO] SET '
				+ '		manifestIdentifier = NULL '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCO] DDSCO '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] SLDM ON SLDM.destinationID = DDSCO.[idData-Lesson] AND SLDM.object = ''lessondatasco'''
				+ ' WHERE SLDM.sourceID IS NOT NULL'

		EXEC (@sql)
		
		-- scorm interactions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblData-SCOInt]'
				+ ' ( '
				+ '		idSite, '
				+ '		[idData-SCO], '
				+ '		identifier, '
				+ '		timestamp, '
				+ '		result, '
				+ '		latency, '
				+ '		scoIdentifier '				
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','											-- idSite
				+ '		DSCOM.destinationID,'																	-- [idData-SCO]
				+ '		SI.identifier,'																			-- identifier
				+ '		SI.timestamp,'																			-- timestamp
				+ '		CASE WHEN SV.idSCORMVocabulary IS NOT NULL THEN SV.idSCORMVocabulary ELSE NULL END,'	-- result
				+ '		SI.latency,'																			-- latency
				+ '		NULL'																					-- scoIdentifier
				+ ' FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEScormInt] SI'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVELessonData] LD ON LD.idLessonData = SI.idLessonData'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblARCHIVEEnrollmentInstance] EI ON EI.idEnrollmentInstance = LD.idEnrollmentInstance'
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblUser] U ON U.idUser = EI.idUser'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] DSCOM ON DSCOM.sourceID = SI.idLessonData AND DSCOM.object = ''lessondatasco'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSCORMVocabulary] SV ON SV.value = SI.result'
				+ ' WHERE U.idSite = ' + CONVERT(NVARCHAR, @idSiteSource)  + ' AND DSCOM.destinationID IS NOT NULL AND DSCOM.destinationID IN (SELECT [idData-SCO] FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblData-SCO])'
		
		EXEC (@sql)
		
	/****************************************************INSERT HOSTNAME INTO ACCOUNT DB******************************************************/

		SET @sql = 'INSERT INTO [tblAccountToDomainAliasLink]'
				+ ' ( '
				+ '		idAccount, '
				+ '		hostname '			
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idAccount) + ','	-- idAccount
				+ '		''' + @destinationHostname + ''''		-- hostname
		
		EXEC (@sql)

		
		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaEnrollment_Success'

		
		
	SET XACT_ABORT OFF

END

GO