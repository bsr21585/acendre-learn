SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.DocumentRepositoryEventEmail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.DocumentRepositoryEventEmail]
GO


/*

CLONE PORTAL - DOCUMENT REPOSITORY / EVENT EMAIL DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.DocumentRepositoryEventEmail]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.0: Copy DocumentRepositoryEventEmail - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql	NVARCHAR(MAX)
		DECLARE @subSql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		DOCUMENT REPOSITORY

		*/

		-- copy document repository folders
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idDocumentRepositoryObjectType, '
	   			 + '	idObject, '
	   			 + '	name, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	DRF.idDocumentRepositoryObjectType, '
	   			 + '	CASE DRF.idDocumentRepositoryObjectType '
	   			 + '		WHEN 1 THEN '
	   			 + '			TTG.destinationId '
	   			 + '		WHEN 2 THEN '
	   			 + '			TTC.destinationId '
	   			 + '		WHEN 3 THEN '
	   			 + '			TTLP.destinationId '

		IF @fullCopyFlag = 1 -- only copy over profile file data if this is a full copy
		
		BEGIN
		SET @subSql = '		WHEN 4 THEN '
	   				+ '			TTU.destinationId '
	   				+ '		END, '
	   				+ '		DRF.name, '
	   				+ '		DRF.isDeleted, '
	   				+ '		DRF.dtDeleted '
	   				+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] DRF '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = DRF.idObject AND TTG.object = ''groups'') '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = DRF.idObject AND TTC.object = ''courses'') '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = DRF.idObject AND TTLP.object = ''learningPaths'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DRF.idObject AND TTU.object = ''users'') '
	   				+ 'WHERE DRF.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		END

		ELSE

		BEGIN
		SET @subSql = '		END, '
	   				+ '		DRF.name, '
	   				+ '		DRF.isDeleted, '
	   				+ '		DRF.dtDeleted '
	   				+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] DRF '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = DRF.idObject AND TTG.object = ''groups'') '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = DRF.idObject AND TTC.object = ''courses'') '
	   				+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = DRF.idObject AND TTLP.object = ''learningPaths'') '
	   				+ 'WHERE DRF.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		END

		SET @sql = @sql + @subSql

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.1: Copy DocumentRepositoryEventEmail - Document Repository Folders Inserted', 0)
	   
		-- insert the mapping for source to destination document repository folder ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idDocumentRepositoryFolder, '
	   			 + '	DST.idDocumentRepositoryFolder, '
	   			 + '	''documentrepositoryfolder'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryFolder] SRC '
	   			 + 'ON (SRC.idSite =' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SRC.name = DST.name) '
	   			 + '	AND (SRC.idDocumentRepositoryObjectType = DST.idDocumentRepositoryObjectType) '
	   			 + '	AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.2: Copy DocumentRepositoryEventEmail - Document Repository Folder Mappings Created', 0)

		-- copy document repository items
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryItem] '
				 + '( '
				 + '	idSite, '
				 + '	idDocumentRepositoryObjectType, '
				 + '	label, '
				 + '	[fileName], '
				 + '	kb, '
				 + '	searchTags, '
				 + '	isPrivate, '
				 + '	idLanguage, '
				 + '	isAllLanguages, '
				 + '	dtCreated, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	idObject, '
				 + '	idDocumentRepositoryFolder, '
				 + '	idOwner, '
				 + '	isVisibleToUser, '
				 + '	isUploadedByUser '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	DRI.idDocumentRepositoryObjectType, '
				 + '	DRI.label, '
				 + '	DRI.[fileName], '
				 + '	DRI.kb, '
				 + '	DRI.searchTags, '
				 + '	DRI.isPrivate, '
				 + '	DRI.idLanguage, '
				 + '	DRI.isAllLanguages, '
				 + '	DRI.dtCreated, '
				 + '	DRI.isDeleted, '
				 + '	DRI.dtDeleted, '

		IF @fullCopyFlag = 1 -- only copy over profile file data if this is a full copy
		
		BEGIN
		SET @subSql = '	CASE DRI.idDocumentRepositoryObjectType '
					+ '		WHEN 1 THEN '
					+ '			TTG.destinationId '
					+ '		WHEN 2 THEN '
					+ '			TTC.destinationId '
					+ '		WHEN 3 THEN '
					+ '			TTLP.destinationId '
					+ '		WHEN 4 THEN '
					+ '			TTUT.destinationId '
					+ '	END AS idObject, '
					+ '	TTDRF.destinationId, '
					+ '	TTU.destinationId, '
					+ '	DRI.isVisibleToUser, '
					+ '	DRI.isUploadedByUser '
					+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryItem] DRI '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = DRI.idObject AND TTG.object = ''groups'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = DRI.idObject AND TTC.object = ''courses'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = DRI.idObject AND TTLP.object = ''learningPaths'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDRF on (TTDRF.sourceId = DRI.idDocumentRepositoryFolder AND TTDRF.object = ''documentrepositoryfolder'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DRI.idOwner AND TTU.object = ''users'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUT on (TTUT.sourceId = DRI.idObject AND TTUT.object = ''users'') '
					+ 'WHERE DRI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
					+ 'AND ((TTG.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 1) '
					+ 'OR (TTC.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 2) '
					+ 'OR (TTLP.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 3) '
					+ 'OR (TTUT.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 4))'
		END

		ELSE

		BEGIN
		SET @subSql = '	CASE DRI.idDocumentRepositoryObjectType '
					+ '		WHEN 1 THEN '
					+ '			TTG.destinationId '
					+ '		WHEN 2 THEN '
					+ '			TTC.destinationId '
					+ '		WHEN 3 THEN '
					+ '			TTLP.destinationId '					
					+ '	END AS idObject, '
					+ '	TTDRF.destinationId, '
					+ '	1, '
					+ '	DRI.isVisibleToUser, '
					+ '	DRI.isUploadedByUser '
					+ 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryItem] DRI '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = DRI.idObject AND TTG.object = ''groups'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = DRI.idObject AND TTC.object = ''courses'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = DRI.idObject AND TTLP.object = ''learningPaths'') '
					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDRF on (TTDRF.sourceId = DRI.idDocumentRepositoryFolder AND TTDRF.object = ''documentrepositoryfolder'') '					
					+ 'WHERE DRI.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
					+ 'AND ((TTG.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 1) '
					+ 'OR (TTC.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 2) '
					+ 'OR (TTLP.destinationId IS NOT NULL AND DRI.idDocumentRepositoryObjectType = 3)) '					
		END

		SET @sql = @sql + @subSql

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.3: Copy DocumentRepositoryEventEmail - Document Repository Items Inserted', 0)
		  
		-- insert the mapping for source to destination document repository item ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idDocumentRepositoryItem, '
				 + '	DST.idDocumentRepositoryItem, '
				 + '	''documentrepositoryitem'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryItem] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryItem] SRC '
				 + 'ON (SRC.idSite =' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND (SRC.label = DST.label) '
	   			 + '	AND (SRC.idDocumentRepositoryObjectType = DST.idDocumentRepositoryObjectType) '
				 + '	AND (SRC.dtCreated = DST.dtCreated) '
				 + '	AND (SRC.dtDeleted = DST.dtDeleted OR (SRC.dtDeleted IS NULL AND DST.dtDeleted IS NULL))) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.4: Copy DocumentRepositoryEventEmail - Document Repository Item Mappings Created', 0)
		  
		-- copy document repository item languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblDocumentRepositoryItemLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idDocumentRepositoryItem, '
				 + '	idLanguage, '
				 + '	label, '
				 + '	searchTags '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTDRI.destinationId, '
				 + '	DRIL.idLanguage, '
				 + '	DRIL.label, '
				 + '	DRIL.searchTags '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblDocumentRepositoryItemLanguage] DRIL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDRI ON (TTDRI.sourceId = DRIL.idDocumentRepositoryItem AND TTDRI.object = ''documentrepositoryitem'') '
				 + 'WHERE DRIL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTDRI.destinationId IS NOT NULL'
		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.5: Copy DocumentRepositoryEventEmail - Document Repository Item Languages Inserted', 0)

		/*

		EMAIL NOTIFICATIONS

		*/

		-- copy email notifications
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEventEmailNotification] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	name, '
	   			 + '	idEventType, '
	   			 + '	idEventTypeRecipient, '
	   			 + '	[from], '
	   			 + '	isActive, '
	   			 + '	dtActivation, '
	   			 + '	interval, '
	   			 + '	timeframe, '
	   			 + '	[priority], '
	   			 + '	isDeleted, '
	   			 + '	idObject, '
				 + '	isHTMLBased, '
				 + '	attachmentType, '
				 + '	copyTo, '
				 + '	specificEmailAddress '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '	   			 
				 + '	EEN.name  + '' ##'' + CAST(EEN.idEventEmailNotification AS NVARCHAR) + ''##'', '
	   			 + '	EEN.idEventType, '
	   			 + '	EEN.idEventTypeRecipient, '
	   			 + '	EEN.[from], '
	   			 + '	EEN.isActive, '
	   			 + '	EEN.dtActivation, '
	   			 + '	EEN.interval, '
	   			 + '	EEN.timeframe, '
	   			 + '	EEN.[priority], '
	   			 + '	EEN.isDeleted, '
				 + '	CASE '
				 + '	WHEN EEN.idEventType >= 200 AND EEN.idEventType < 400 THEN ' -- course enrollment (courses and lessons)
				 + '		TTC.destinationId '				 
				 + '	WHEN EEN.idEventType >= 400 AND EEN.idEventType < 500 THEN ' -- ilt session
				 + '		TTILT.destinationId '
				 + '	WHEN EEN.idEventType >= 500 AND EEN.idEventType < 600 THEN ' -- learning path
				 + '		TTLP.destinationId '
				 + '	WHEN EEN.idEventType >= 600 AND EEN.idEventType < 700 THEN ' -- certification
				 + '		TTCRT.destinationId '
				 + '	WHEN EEN.idEventType >= 700 AND EEN.idEventType < 800 THEN ' -- certificate
				 + '		TTCER.destinationId '
				 + '	ELSE '														 -- all other events that are not object-specific
				 + '		NULL '
				 + '	END, '
				 + '	EEN.isHTMLBased, '
				 + '	EEN.attachmentType, '
				 + '	EEN.copyTo, '
				 + '	EEN.specificEmailAddress '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEventEmailNotification] EEN '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = EEN.idObject AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTILT ON (TTILT.sourceId = EEN.idObject AND TTILT.object = ''standuptraining'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP ON (TTLP.sourceId = EEN.idObject AND TTLP.object = ''learningPaths'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCRT ON (TTCRT.sourceId = EEN.idObject AND TTCRT.object = ''certification'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCER ON (TTCER.sourceId = EEN.idObject AND TTCER.object = ''certificates'') '
	   			 + 'WHERE EEN.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.6: Copy DocumentRepositoryEventEmail - Event Email Notifications Inserted', 0)

		-- insert the mapping for source to destination email notification ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SEEN.idEventEmailNotification, '
	   			 + '	DEEN.idEventEmailNotification, '
	   			 + '	''emailNotifications'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEventEmailNotification] DEEN '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEventEmailNotification] SEEN '
	   			 + 'ON (SEEN.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SEEN.name + '' ##'' + CAST(SEEN.idEventEmailNotification AS NVARCHAR) + ''##'' = DEEN.name) '	   			 
	   			 + 'WHERE DEEN.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.7: Copy DocumentRepositoryEventEmail - Event Email Notification Mappings Created', 0)

		-- clean up the ##idEventEmailNotification## additions we made to email notification names, we did that to uniquely distinguish email notifications
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] SET '
				 + '	name = REPLACE(DEEN.name, '' ##'' + CAST(SEEN.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SEEN ON SEEN.destinationID = DEEN.idEventEmailNotification AND SEEN.object = ''emailNotifications'' '
				 + 'WHERE DEEN.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SEEN.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.8: Copy DocumentRepositoryEventEmail - Event Email Notification Names Cleaned Up', 0)
	   
		-- copy event email notification languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEventEmailNotificationLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEventEmailNotification, '
	   			 + '	idLanguage, '
	   			 + '	name '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTEEN.destinationId, '
	   			 + '	EENL.idLanguage, '
	   			 + '	EENL.name '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEventEmailNotificationLanguage] EENL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTEEN on (TTEEN.sourceId = EENL.idEventEmailNotification AND TTEEN.object = ''emailNotifications'') '
				 + 'WHERE EENL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' AND TTEEN.destinationId IS NOT NULL'
	    
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.9: Copy DocumentRepositoryEventEmail - Event Email Notification Languages Inserted', 0)	   
		
		/*
		
		EVENT LOGS & INBOX MESSAGES - FULL COPY ONLY
		
		*/	  

		IF @fullCopyFlag = 1
		BEGIN

		-- copy the event log
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblEventLog] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idEventType, '
	   			 + '	timestamp, '
	   			 + '	eventDate, '
	   			 + '	idObject, '
	   			 + '	idObjectRelated, '
	   			 + '	idObjectUser, '
	   			 + '	idExecutingUser '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	EL.idEventType, '
	   			 + '	EL.timestamp, '
	   			 + '	EL.eventDate, '
	   			 + '	CASE '
	   			 + '	WHEN EL.idEventType >= 100 AND EL.idEventType < 200 THEN ' -- user events
	   			 + '		TTU.destinationId '
	   			 + '	WHEN EL.idEventType >= 200 AND EL.idEventType < 300 THEN ' -- course enrollment events
	   			 + '		TTC.destinationId '
	   			 + '	WHEN EL.idEventType >= 300 AND EL.idEventType < 400 THEN ' -- lesson events
	   			 + '		TTL.destinationId '
	   			 + '	WHEN EL.idEventType >= 400 AND EL.idEventType < 500 THEN ' -- session events
	   			 + '		TTST.destinationId '
	   			 + '	WHEN EL.idEventType >= 500 AND EL.idEventType < 600 THEN ' -- learning path enrollment events
	   			 + '		TTLP.destinationId '
				 + '	WHEN EL.idEventType >= 600 AND EL.idEventType < 700 THEN ' -- certification events
	   			 + '		TTCRT.destinationId '
	   			 + '	WHEN EL.idEventType >= 700 AND EL.idEventType < 800 THEN ' -- certificate events
	   			 + '		TTCER.destinationId '	   				
	   			 + '	END, '
	   			 + '	CASE '
	   			 + '	WHEN EL.idEventType >= 100 AND EL.idEventType < 200 THEN ' -- user events
	   			 + '		TTU.destinationId '
	   			 + '	WHEN EL.idEventType >= 200 AND EL.idEventType < 300 THEN ' -- course enrollment events
	   			 + '		TTEN.destinationId '
	   			 + '	WHEN EL.idEventType >= 300 AND EL.idEventType < 400 THEN ' -- lesson events
	   			 + '		TTDL.destinationId '
	   			 + '	WHEN EL.idEventType >= 400 AND EL.idEventType < 500 THEN ' -- session events
	   			 + '		TTSTI.destinationId '
	   			 + '	WHEN EL.idEventType >= 500 AND EL.idEventType < 600 THEN ' -- learning path enrollment events
	   			 + '		TTLPEN.destinationId '
				 + '	WHEN EL.idEventType >= 600 AND EL.idEventType < 700 THEN ' -- certification events
	   			 + '		TTCRTUL.destinationId '
	   			 + '	WHEN EL.idEventType >= 700 AND EL.idEventType < 800 THEN ' -- certificate events
	   			 + '		TTCERR.destinationId '
	   			 + '	END, '
	   			 + '	TTOU.destinationId, '
	   			 + '	TTEU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblEventLog] EL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = EL.idObject AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC ON (TTC.sourceId = EL.idObject AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTEN on (TTEN.sourceId = EL.idObjectRelated AND TTEN.object = ''enrollment'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTL on (TTL.sourceId = EL.idObject AND TTL.object = ''lesson'') '
	   	SET @sql = @sql + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTDL on (TTDL.sourceId = EL.idObjectRelated AND TTDL.object = ''datalesson'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST on (TTST.sourceId = EL.idObject AND TTST.object = ''standuptraining'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI on (TTSTI.sourceId = EL.idObject AND TTSTI.object = ''session'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = EL.idObject AND TTLP.object = ''learningPaths'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLPEN on (TTLPEN.sourceId = EL.idObjectRelated AND TTLPEN.object = ''learningpathenrollment'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCRT on (TTCRT.sourceId = EL.idObject AND TTCRT.object = ''certification'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCRTUL on (TTCRTUL.sourceId = EL.idObject AND TTCRTUL.object = ''certificationtouserlink'') '				 
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCER on (TTCER.sourceId = EL.idObject AND TTCER.object = ''certificates'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCERR on (TTCERR.sourceId = EL.idObjectRelated AND TTCERR.object = ''certificaterecord'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTOU ON (TTOU.sourceId = EL.idObjectUser AND TTOU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTEU ON (TTEU.sourceId = EL.idExecutingUser AND TTEU.object = ''users'') '
	   			 + 'WHERE EL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ' '
				 + 'AND ((TTU.destinationId IS NOT NULL AND EL.idEventType >= 100 AND EL.idEventType < 200) '
	   			 + 'OR (TTC.destinationId IS NOT NULL AND TTEN.destinationId IS NOT NULL AND EL.idEventType >= 200 AND EL.idEventType < 300) '
	   			 + 'OR (TTL.destinationId IS NOT NULL AND TTDL.destinationId IS NOT NULL AND EL.idEventType >= 300 AND EL.idEventType < 400) '
	   			 + 'OR (TTST.destinationId IS NOT NULL AND TTSTI.destinationId IS NOT NULL AND EL.idEventType >= 400 AND EL.idEventType < 500) '
	   			 + 'OR (TTLP.destinationId IS NOT NULL AND TTLPEN.destinationId IS NOT NULL AND EL.idEventType >= 500 AND EL.idEventType < 600) '
				 + 'OR (TTCRT.destinationId IS NOT NULL AND TTCRTUL.destinationId IS NOT NULL AND EL.idEventType >= 600 AND EL.idEventType < 700) '
	   			 + 'OR (TTCER.destinationId IS NOT NULL AND TTCERR.destinationId IS NOT NULL AND EL.idEventType >= 700 AND EL.idEventType < 800))'
					
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.10: Copy DocumentRepositoryEventEmail - Event Log Inserted', 0)
	   
		-- DO NOT CREATE EVENT LOG MAPPINGS

		-- DO NOT COPY EVENT EMAIL QUEUE

		-- copy inbox messages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblInboxMessage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idParentInboxMessage, '
	   			 + '	idRecipient, '
	   			 + '	idSender, '
	   			 + '	[subject], '
	   			 + '	[message], '
	   			 + '	isDraft, '
	   			 + '	isSent, '
	   			 + '	dtSent, '
	   			 + '	isRead, '
	   			 + '	dtRead, '
	   			 + '	isRecipientDeleted, '
	   			 + '	dtRecipientDeleted, '
	   			 + '	isSenderDeleted, '
	   			 + '	dtSenderDeleted, '
	   			 + '	dtCreated '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	NULL, '			 -- this null value of idParentInboxMessage will be updated from temporary InboxMessageIdMappings table below 
	   			 + '	TTR.destinationId, '
	   			 + '	TTS.destinationId, '
	   			 + '	IM.[subject], '
	   			 + '	IM.[message], '
	   			 + '	IM.isDraft, '
	   			 + '	IM.isSent, '
	   			 + '	IM.dtSent, '
	   			 + '	IM.isRead, '
	   			 + '	IM.dtRead, ' 
	   			 + '	IM.isRecipientDeleted, '
	   			 + '	IM.dtRecipientDeleted, '
	   			 + '	IM.isSenderDeleted, '
	   			 + '	IM.dtSenderDeleted, '
	   			 + '	IM.dtCreated '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblInboxMessage] IM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = IM.idRecipient AND TTR.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTS on (TTS.sourceId = IM.idSender AND TTS.object = ''users'') '
	   			 + 'WHERE IM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   							
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.11: Copy DocumentRepositoryEventEmail - Inbox Messages Inserted', 0)

		-- insert the mapping for source to destination inbox message ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idInboxMessage, '
	   			 + '	DST.idInboxMessage, '
	   			 + '	''inboxmessage'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblInboxMessage] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblInboxMessage] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.message = DST.message) '
	   			 + '	AND (SRC.dtCreated = DST.dtCreated) '
	   			 + '	AND (SRC.dtSent = DST.dtSent OR (SRC.dtSent IS NULL AND DST.dtSent IS NULL)) '
	   			 + ') '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.12: Copy DocumentRepositoryEventEmail - Inbox Message Mappings Created', 0)
			   			
		-- update idParentInboxMessage mappings
		SET @sql = 'UPDATE DSTIM '
	   			 + '	SET idParentInboxMessage = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblInboxMessage] SRCIM '
	   			 + 'INNER JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblInboxMessage] DSTIM '
	   			 + 'ON (SRCIM.dtCreated = DSTIM.dtCreated AND SRCIM.[message] = DSTIM.[message] AND SRCIM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) + ') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCIM.idParentInboxMessage AND TEMP.object = ''inboxmessage'') '
	   			 + 'WHERE DSTIM.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			 + 'AND SRCIM.idParentInboxMessage IS NOT NULL '
	   
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12.13: Copy DocumentRepositoryEventEmail - Inbox Message Parent Ids Updated', 0)
	   
		END

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_DocumentRepositoryEventEmail_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 12: Copy DocumentRepositoryEventEmail - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)	
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_DocumentRepositoryEventEmail_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END