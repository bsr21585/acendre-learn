-- =====================================================================
-- PROCEDURE: [DatabaseServer.GetSitesFromDatabase]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[DatabaseServer.GetSitesFromDatabase]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.GetSitesFromDatabase]
GO

CREATE PROCEDURE [dbo].[DatabaseServer.GetSitesFromDatabase]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@DBServerName			NVARCHAR(50),
	@DatabaseName			NVARCHAR(50)
	
)
AS
	BEGIN
	   SET NOCOUNT ON
		
	   DECLARE @sql NVARCHAR(MAX)
	   DECLARE @isDatabaseExists INT
	   

	BEGIN TRY

	BEGIN

	  -- check to validate exitence of passed database on server
	  SET @SQL = 'SET @isDatabaseExists = (SELECT  COUNT(1) FROM ['+@DBServerName+'].master.sys.databases 
		   WHERE name ='''+ @DatabaseName +''')'

	  EXECUTE SP_EXECUTESQL @SQL, N'@isDatabaseExists INT OUTPUT', @isDatabaseExists OUTPUT
	

	  IF @isDatabaseExists = 1     
	      BEGIN
				-- getting site information from link server database
				 SET @sql =' SELECT '
	   			 		+'  idSite,'
	   			 		+'  hostname  '
	   			 		+' FROM  [' + @DBServerName + '].['+ @DatabaseName + '].[dbo].'+ '[tblSite]'
				 		+' WHERE idSite <> 1  '  
				 		+' ORDER BY hostname  '  
				 EXEC(@sql)

				 SELECT @Return_Code = 0	
				 SELECT @Error_Description_Code=''

		  END
	   ELSE
		  BEGIN
			 SELECT @Return_Code = 1	
			 SELECT @Error_Description_Code='DatabaseServerGetSitesFromDatabase_DatabaseNotFoundOnLinkServer'
		  END

	   END
  END TRY
  BEGIN CATCH
			    SELECT @Return_Code = 1	
			    SELECT @Error_Description_Code='DatabaseServerGetSitesFromDatabase_DatabaseNotFoundOnLinkServer'
  END CATCH
  END