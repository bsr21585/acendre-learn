-- =====================================================================
-- PROCEDURE: [DatabaseServer.Delete]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.Delete]
GO


CREATE PROCEDURE [DatabaseServer.Delete]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR (10)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,	 
	@DatabaseServer				  IDTable			READONLY	
)
AS
	
	BEGIN
	
	/*
		check to see if there is anything to do
	*/
	
	IF (SELECT COUNT(1) FROM @DatabaseServer) = 0
		BEGIN
		SET @Return_Code = 1 --(1 is 'details not found')
		SET @Error_Description_Code = 'DataBaseServerDelete_NoRecordFound'
		RETURN 1
		END
	
	
		
	/*
	
	Delete the data base(s).
	
	*/
	
	DELETE FROM tblDatabaseServer
	WHERE EXISTS (
		SELECT 1
		FROM @DatabaseServer VDBS
		WHERE tblDatabaseServer.idDatabaseServer = VDBS.id
	)
	
	SELECT @Return_Code = 0
	
	SET NOCOUNT ON
	END
	

GO


