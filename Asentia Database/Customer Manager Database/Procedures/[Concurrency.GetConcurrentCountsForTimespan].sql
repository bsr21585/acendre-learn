-- =====================================================================
-- PROCEDURE: [Concurrency.GetConcurrentCountsForTimespan]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Concurrency.GetConcurrentCountsForTimespan]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Concurrency.GetConcurrentCountsForTimespan]
GO

/*

Returns concurrency history for a specified timespan, or the most current concurrency 
count when the "current" parameter is set.

Note, NULL timespans will return everything before, everything after, or just everything.

*/

CREATE PROCEDURE [Concurrency.GetConcurrentCountsForTimespan]
(
	@dtTimespanStart	DATETIME	= NULL,
	@dtTimeSpanEnd		DATETIME	= NULL,
	@getLatestCount		BIT			= 0
)
AS
	
	BEGIN
	SET NOCOUNT ON
		
	IF (@getLatestCount = 1) -- this will get the latest (current) count
		BEGIN

		SELECT 
			SUM(concurrentUsers), 
			[timestamp]
		FROM tblConcurrency
		WHERE [timestamp] = (SELECT MAX([timestamp]) FROM tblConcurrency)
		GROUP BY [timestamp]
		ORDER BY [timestamp]

		END

	ELSE -- this will get all counts within a timespan
		BEGIN

		/* 
		
		if the timespan is not logical, i.e. start not before end, 
		just null out the timespan start and end

		*/

		IF (@dtTimespanStart IS NOT NULL AND @dtTimeSpanEnd IS NOT NULL AND @dtTimeSpanEnd < @dtTimespanStart)
			BEGIN
			SET @dtTimespanStart = NULL
			SET @dtTimeSpanEnd = NULL
			END

		/*

		get the counts within timespan
		 - if both start and end are null, the entire history is returned
		 - if start specified but no end, everything after start is returned
		 - if end but no start, everything before end is returned
		 - if both specified, everything in between is returned
		 - all counts are inclusive (___ than or equal to)

		*/

		IF (@dtTimespanStart IS NULL AND @dtTimeSpanEnd IS NULL) -- both null
			BEGIN

			SELECT 
				SUM(concurrentUsers), 
				[timestamp] 
			FROM tblConcurrency 
			GROUP BY [timestamp]
			ORDER BY [timestamp]

			END

		IF (@dtTimespanStart IS NOT NULL AND @dtTimeSpanEnd IS NULL) -- only start
			BEGIN

			SELECT 
				SUM(concurrentUsers), 
				[timestamp] 
			FROM tblConcurrency 
			WHERE [timestamp] >= @dtTimespanStart
			GROUP BY [timestamp]
			ORDER BY [timestamp]

			END

		IF (@dtTimespanEnd IS NOT NULL AND @dtTimeSpanStart IS NULL) -- only end
			BEGIN

			SELECT 
				SUM(concurrentUsers), 
				[timestamp] 
			FROM tblConcurrency 
			WHERE [timestamp] <= @dtTimespanEnd
			GROUP BY [timestamp]
			ORDER BY [timestamp]

			END

		IF (@dtTimespanEnd IS NOT NULL AND @dtTimeSpanStart IS NOT NULL) -- both
			BEGIN

			SELECT 
				SUM(concurrentUsers), 
				[timestamp] 
			FROM tblConcurrency 
			WHERE [timestamp] >= @dtTimespanStart
			AND [timestamp] <= @dtTimespanEnd
			GROUP BY [timestamp]
			ORDER BY [timestamp]

			END

		END
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
