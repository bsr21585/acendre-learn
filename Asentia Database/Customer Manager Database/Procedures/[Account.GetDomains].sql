-- =====================================================================
-- PROCEDURE: [Account.GetDomains]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.GetDomains]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.GetDomains]
GO

/*

Returns a recordset of  ids and names that are domains of a hostname or account.

*/

CREATE PROCEDURE [Account.GetDomains]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@idAccount              INT
)
AS

	BEGIN
	SET NOCOUNT ON

		/*
		Gets the list of domain aaliases 
		*/
		
	
		SELECT
			DISTINCT
			ATDAL.idAccountToDomainAliasLink AS id, 
			ATDAL.domain AS displayName
		FROM tblAccountToDomainAliasLink ATDAL
		WHERE ATDAL.idAccount = @idAccount
		AND (ATDAL.hostname IS NULL OR ATDAL.hostname=' ')
		
		ORDER BY displayName

		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	