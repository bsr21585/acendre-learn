SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.InstructorLedTraining]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.InstructorLedTraining]
GO


/*

CLONE PORTAL - INSTRUCTOR LED TRAINING DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.InstructorLedTraining]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,			-- needed for ILT Sessions
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.0: Copy ILT - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql	NVARCHAR(MAX)
		DECLARE @subSql	NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		ILT MODULES

		*/

		-- copy ilt modules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTraining] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	title, '
	   			 + '	[description], '
	   			 + '	isStandaloneEnroll, '
	   			 + '	isRestrictedEnroll, '
	   			 + '	isRestrictedDrop, '
	   			 + '	searchTags, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	avatar, '
	   			 + '	cost, '
	   			 + '	objectives, '
	   			 + '	shortcode, '
				 + '	dtCreated, '
				 + '	dtModified '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	ST.title, '
	   			 + '	ST.[description], '
	   			 + '	ST.isStandaloneEnroll, '
	   			 + '	ST.isRestrictedEnroll, '
	   			 + '	ST.isRestrictedDrop, '
	   			 + '	ST.searchTags, '
	   			 + '	ST.isDeleted, '
	   			 + '	ST.dtDeleted, '
	   			 + '	ST.avatar, '
	   			 + '	ST.cost, '
	   			 + '	ST.objectives, '
	   			 + '	ST.shortcode, '
				 + '	ST.dtCreated, '
				 + '	ST.dtModified '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTraining] ST '
	   			 + 'WHERE ST.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.1: Copy ILT - ILT Modules Inserted', 0)
	   
		-- insert the mapping for source to destination ilt module ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idStandUpTraining, '
	   			 + '	DST.idStandUpTraining, '
	   			 + '	''standuptraining'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTraining] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTraining] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SRC.title = DST.title) '
	   			 + '	AND (SRC.dtCreated = DST.dtCreated) '
	   			 + '	AND (SRC.dtModified = DST.dtModified)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.2: Copy ILT - ILT Module Mappings Created', 0)
	   				
		-- copy ilt module languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idStandUpTraining, '
	   			 + '	idLanguage, '
	   			 + '	title, '
	   			 + '	[description], '
	   			 + '	searchTags, '
	   			 + '	objectives '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTST.destinationId, '
	   			 + '	STL.idLanguage, '
	   			 + '	STL.title, '
	   			 + '	STL.[description], '
	   			 + '	STL.searchTags, '
	   			 + '	STL.objectives '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingLanguage] STL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST ON (TTST.sourceId = STL.idStandUpTraining AND TTST.object = ''standuptraining'') '
	   			 + 'WHERE STL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.3: Copy ILT - ILT Module Languages Inserted', 0)	      

		/*

		ILT SESSIONS

		*/

		-- copy web meeting organizers - only for full copy
		IF(@fullCopyFlag = 1)
			BEGIN

			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblWebMeetingOrganizer] '
					 + '( '
					 + '	idSite, '
					 + '	idUser, '
					 + '	organizerType, '
					 + '	username, '
					 + '	[password] '
					 + ') '
					 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
					 + '	TTU.destinationId, '
					 + '	WMO.organizerType, '
					 + '	WMO.username, '
					 + '	WMO.[password] '
					 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblWebMeetingOrganizer] WMO '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = WMO.idUser AND TTU.object = ''users'') '
	   				 + 'WHERE WMO.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.4: Copy ILT - Web Meeting Organizers Inserted', 0)

			-- insert the mapping for source to destination web meeting organizer ids
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   				 + '( '
	   				 + '	sourceId, '
	   				 + '	destinationId, '
	   				 + '	[object] '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	SRC.idWebMeetingOrganizer, '
	   				 + '	DST.idWebMeetingOrganizer, '
	   				 + '	''webmeetingorganizer'' '
	   				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblWebMeetingOrganizer] DST '
	   				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblWebMeetingOrganizer] SRC '
	   				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   				 + '	AND (SRC.username = DST.username) '
	   				 + '	AND (SRC.[password] = DST.[password])) '	   				 
	   				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.5: Copy ILT - Web Meeting Organizer Mappings Created', 0)

			END

		-- copy ilt sessions
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingInstance] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idStandupTraining, '
	   			 + '	title, '
	   			 + '	[description], '
	   			 + '	seats, '
	   			 + '	waitingSeats, '
	   			 + '	[type], '
	   			 + '	urlRegistration, '
	   			 + '	urlAttend, '
	   			 + '	city, '
	   			 + '	province, '
	   			 + '	locationDescription, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	integratedObjectKey, '
	   			 + '	hostUrl, '
	   			 + '	genericJoinUrl, '
	   			 + '	meetingPassword, '
	   			 + '	idWebMeetingOrganizer ' -- maps to a web meeting organizer, or null if not full copy
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTST.destinationId, '
	   			 + '	STI.title  + '' ##'' + CONVERT(NVARCHAR, STI.idStandUpTrainingInstance) + ''##'', '
	   			 + '	STI.[description], '
	   			 + '	STI.seats, '
	   			 + '	STI.waitingSeats, '
	   			 + '	STI.[type], '
	   			 + '	STI.urlRegistration, '
	   			 + '	STI.urlAttend, '
	   			 + '	STI.city, '
	   			 + '	STI.province, '
	   			 + '	STI.locationDescription, '
	   			 + '	STI.isDeleted, '
	   			 + '	STI.dtDeleted, '
	   			 + '	STI.integratedObjectKey, '
	   			 + '	STI.hostUrl, '
	   			 + '	STI.genericJoinUrl, '
	   			 + '	STI.meetingPassword, '

		IF(@fullCopyFlag = 1)
			BEGIN

			SET @sql = @sql + '	TTU.destinationId '

			END
		
		ELSE
			
			BEGIN

			SET @sql = @sql + '	NULL '

			END

		SET @sql = @sql + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingInstance] STI '
	   					+ 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST ON (TTST.sourceId = STI.idStandUpTraining AND TTST.object = ''standuptraining'') '

		IF(@fullCopyFlag = 1)
			BEGIN

			SET @sql = @sql + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = STI.idWebMeetingOrganizer AND TTU.object = ''users'' ) '

			END

		SET @sql = @sql + 'WHERE STI.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.6: Copy ILT - ILT Sessions Inserted', 0)

		-- insert the mapping for source to destination ilt session ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idStandUpTrainingInstance, '
				 + '	DST.idStandUpTrainingInstance, '
				 + '	''session'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingInstance] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingInstance] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SRC.title + '' ##'' + CONVERT(NVARCHAR, SRC.idStandUpTrainingInstance) + ''##'' = DST.title) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.7: Copy ILT - ILT Session Mappings Created', 0)

		-- clean up the ##idStandUpTrainingInstance## additions we made to titles, we did that to uniquely distinguish sessions
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstance] SET '
				 + '	title = REPLACE(DSTI.title, '' ##'' + CONVERT(NVARCHAR, SSTI.sourceID) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblStandUpTrainingInstance] DSTI '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SSTI ON SSTI.destinationID = DSTI.idStandUpTrainingInstance AND SSTI.object = ''session'' '
				 + 'WHERE SSTI.sourceID IS NOT NULL AND DSTI.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.8: Copy ILT - ILT Session Titles Cleaned Up', 0)		
						
		-- copy ilt session languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingInstanceLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idStandUpTrainingInstance, '
				 + '	idLanguage, '
				 + '	title, '
				 + '	[description], '
				 + '	locationDescription '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTSTI.destinationId, '
				 + '	STIL.idLanguage, '
				 + '	STIL.title, '
				 + '	STIL.[description], '
				 + '	STIL.locationDescription '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingInstanceLanguage] STIL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI ON (TTSTI.sourceId = STIL.idStandUpTrainingInstance AND TTSTI.object = ''session'') '
				 + 'WHERE STIL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.9: Copy ILT - ILT Session Languages Inserted', 0)

		-- copy resource to object links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblResourceToObjectLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idResource, '
	   			 + '	idObject, '
	   			 + '	objectType, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	isOutsideUse, '
	   			 + '	isMaintenance '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	CASE RTOL.objectType '
	   			 + '	WHEN ''standuptraining'' THEN '
	   			 + '		TTSTI.destinationId '
	   			 + '	ELSE '
	   			 + '		0 '
	   			 + '	END, '
	   			 + '	RTOL.objectType, '
	   			 + '	RTOL.dtStart, '
	   			 + '	RTOL.dtEnd, '
	   			 + '	RTOL.isOutsideUse, '
	   			 + '	RTOL.isMaintenance '
	   			 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblResourceToObjectLink] RTOL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR ON (TTR.sourceId = RTOL.idResource AND TTR.object = ''resource'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI ON (TTSTI.sourceId = RTOL.idObject AND TTSTI.object = ''session'') '
	   			 + 'WHERE RTOL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.10: Copy ILT - ILT Session To Resource Links Inserted', 0)

		-- copy ilt session meeting times
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandUpTrainingInstanceMeetingTime] '
	   			 + '( '
	   			 + '	idStandupTrainingInstance, '
	   			 + '	idSite, '
	   			 + '	dtStart, '
	   			 + '	dtEnd, '
	   			 + '	[minutes], '
	   			 + '	idTimezone '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	TTSTI.destinationId, '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	STIMT.dtStart, '
	   			 + '	STIMT.dtEnd, '
	   			 + '	STIMT.[minutes], '
	   			 + '	STIMT.idTimezone '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandUpTrainingInstanceMeetingTime] STIMT '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI on (TTSTI.sourceId = STIMT.idStandUpTrainingInstance AND TTSTI.object = ''session'') '
	   			 + 'WHERE STIMT.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.11: Copy ILT - ILT Session To Meeting Time Links Inserted', 0)

		-- insert instructor links and roster links if full copy
		IF(@fullCopyFlag = 1)

			BEGIN

			-- copy instructor links
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandupTrainingInstanceToInstructorLink] '
	   				 + '( '
	   				 + '	idInstructor, '
	   				 + '	idStandupTrainingInstance, '
	   				 + '	idSite '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	TTU.destinationId, '
	   				 + '	TTSTI.destinationId, '
	   				 +		CAST(@idSiteDestination AS NVARCHAR) + ' '
	   				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandupTrainingInstanceToInstructorLink] STITUL '	   				 
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI on (TTSTI.sourceId = STITUL.idStandUpTrainingInstance AND TTSTI.object = ''session'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = STITUL.idInstructor AND TTU.object = ''users'') '
	   				 + 'WHERE STITUL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 

			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.12: Copy ILT - ILT Session To Instructor Links Inserted', 0)

			-- copy session roster
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblStandupTrainingInstanceToUserLink] '
	   				 + '( '
	   				 + '	idUser, '
	   				 + '	idStandupTrainingInstance, '
	   				 + '	completionStatus, '
	   				 + '	successStatus, '
	   				 + '	score, '
	   				 + '	isWaitingList, '
	   				 + '	dtCompleted, '
	   				 + '	registrantKey, '
	   				 + '	registrantEmail, '
	   				 + '	joinUrl, '
	   				 + '	waitlistOrder '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	TTU.destinationId, '
	   				 + '	TTSTI.destinationId, '
	   				 + '	STITUL.completionStatus, '
	   				 + '	STITUL.successStatus, '
	   				 + '	STITUL.score, '
	   				 + '	STITUL.isWaitingList, '
	   				 + '	STITUL.dtCompleted, '
	   				 + '	STITUL.registrantKey, '
	   				 + '	STITUL.registrantEmail, '
	   				 + '	STITUL.joinUrl, '
	   				 + '	STITUL.waitlistOrder '
	   				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblStandupTrainingInstanceToUserLink] STITUL '
					 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblUser] U ON U.idUser = STITUL.idUser '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTSTI ON (TTSTI.sourceId = STITUL.idStandUpTrainingInstance AND TTSTI.object = ''session'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU ON (TTU.sourceId = STITUL.idUser AND TTU.object = ''users'') '
	   				 + 'WHERE U.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
					 	   
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.13: Copy ILT - ILT Session Roster Links Inserted', 0)	   

			END	

		/*

		LESSON TO CONTENT LINKS

		*/

		-- copy lesson to content links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblLessonToContentLink] '
				 + '( '
				 + '	idSite, '
				 + '	idLesson, '
				 + '	idContentType, '
				 + '	idAssignmentDocumentType, '
				 + '	assignmentResourcePath, '
				 + '	allowSupervisorsAsProctor, '
				 + '	allowCourseExpertsAsProctor, '
				 + '	idObject '
				 + ') '
				 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTL.destinationId, '
				 + '	LTCL.idContentType, '
				 + '	LTCL.idAssignmentDocumentType, '
				 + '	LTCL.assignmentResourcePath, '
				 + '	LTCL.allowSupervisorsAsProctor, '
				 + '	LTCL.allowCourseExpertsAsProctor, '
				 + '	CASE LTCL.idContentType '
				 + '	WHEN 1 THEN '
				 + '		TTCP.destinationId '
				 + '	WHEN 2 THEN '
				 + '		TTST.destinationId '
				 + '	WHEN 3 THEN '
				 + '		NULL '
				 + '	WHEN 4 THEN '
				 + '		TTCP.destinationId '
				 + '	END '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblLessonToContentLink] LTCL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTL ON (TTL.sourceId = LTCL.idLesson AND TTL.object = ''lesson'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCP ON (TTCP.sourceId = LTCL.idObject AND TTCP.object = ''contentpackage'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTST ON (TTST.sourceId = LTCL.idObject AND TTST.object = ''standuptraining'') '
				 + 'WHERE LTCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		  
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6.14: Copy ILT - Lesson to Content Links Inserted', 0)	   

		/*

		RETURN

		*/
	   
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_InstructorLedTraining_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 6: Copy ILT - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_InstructorLedTraining_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END