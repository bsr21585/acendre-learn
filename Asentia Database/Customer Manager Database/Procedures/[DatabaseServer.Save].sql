-- =====================================================================
-- PROCEDURE: [DatabaseServer.Save]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.Save]
GO

CREATE PROCEDURE [DatabaseServer.Save]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,	
	@idDatabaseServer		INT				OUTPUT, 
	@serverName				NVARCHAR(255),
	@networkName			NVARCHAR(255),
	@username				NVARCHAR(512),
	@password				NVARCHAR(512),
	@isTrusted				BIT
	
)
AS
	
	BEGIN
	SET NOCOUNT ON

	/*
	
	validate the unique hostname
	
	*/
	
	IF (
		SELECT COUNT(1)
		FROM  tblDatabaseServer
		WHERE serverName = @serverName and networkName=@networkName
		AND (
			@idDatabaseServer IS NULL
			OR idDatabaseServer <> @idDatabaseServer
			)
		) > 0
		
		BEGIN
		SELECT @Return_Code = 2
		SET @Error_Description_Code = 'DataBaseServerSave_ServerNameNotUnique'
		RETURN 1
		END
	
	/*
	
	save the data
	
	*/
	
	IF (@idDatabaseServer= 0 OR @idDatabaseServer is null)
		
		BEGIN
		
		-- insert the new object
		
		INSERT INTO tblDatabaseServer (
			serverName,
			networkName,
			username,
			[password],
			isTrusted
			
		)			
		VALUES (
			@serverName,	
			@networkName,
			@username,			
			@password,
			@isTrusted	
		)
		
		-- get the new object's id and return successfully
		
		SELECT @idDatabaseServer = SCOPE_IDENTITY()
		
		END
		
	ELSE
		
		BEGIN
		
		-- check that the database server id exists
		
		IF (SELECT COUNT(1) FROM tblDatabaseServer WHERE idDatabaseServer = @idDatabaseServer) < 1
			
			BEGIN
				SELECT @idDatabaseServer = @idDatabaseServer
				SELECT @Return_Code = 1
				SET @Error_Description_Code = 'DataBaseServerSave_RecordNotFound'
				RETURN 1
			END
			
		-- update existing object's properties
		
		UPDATE tblDatabaseServer SET
			serverName = @serverName,
			networkName =@networkName,
			username = @username,
			[password] = @password,
			isTrusted=@isTrusted			
		WHERE idDatabaseServer = @idDatabaseServer		
		
		-- get the id and return successfully
		
		SELECT @idDatabaseServer = @idDatabaseServer
				
		END	
	
	SELECT @Return_Code = 0
		
	END
		

GO


