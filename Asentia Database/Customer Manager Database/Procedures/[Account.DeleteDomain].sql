-- =====================================================================
-- PROCEDURE: [Account.DeleteDomain]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Account.DeleteDomain]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Account.DeleteDomain]
GO

/*

Delete the selected domain alias of a specific site

*/

CREATE PROCEDURE [Account.DeleteDomain]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	
	@domainAliass			NVARCHAR(255)
)
AS

	BEGIN
	SET NOCOUNT ON


	/*
	 DELETE the site to domain alias link

	*/

	DELETE FROM tblAccountToDomainAliasLink
	WHERE domain	=	@domainAliass 
	AND (hostname IS NULL OR hostname = '')
	AND idAccount = @idCallerAccount
		
		
		SET @Return_Code = 0
		SET @Error_Description_Code = ''

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
	