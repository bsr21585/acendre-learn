-- =====================================================================
-- PROCEDURE: [PowerPointJobProcessor.SetProcessingFlagForAccount]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[PowerPointJobProcessor.SetProcessingFlagForAccount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [PowerPointJobProcessor.SetProcessingFlagForAccount]
GO

CREATE PROCEDURE [PowerPointJobProcessor.SetProcessingFlagForAccount]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT,

	@idAccount					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* 
		validate caller, only caller id/account id 1 is valid here
		the job processor should be the only process calling this
		and it will run as id 1/1; other than that, only the system
		admin will be able to run this
	*/
	
	IF @idCaller <> 1 AND @idCallerAccount <> 1
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
	
	/*
	
	set the processing flag and set dtLastPowerPointJobProcessStart
	
	*/
	
	UPDATE tblAccount SET
		isPowerPointJobProcessing = 1,
		dtLastPowerPointJobProcessStart = GETUTCDATE()
	WHERE idAccount = @idAccount

	SELECT @Return_Code = 0
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO