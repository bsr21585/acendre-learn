SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.DiscussionFeed]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.DiscussionFeed]
GO


/*

CLONE PORTAL - COURSE FEED / GROUP FEED DATA
OCCURS FOR FULL COPY

*/
CREATE PROCEDURE [System.ClonePortal.DiscussionFeed]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.0: Copy DiscussionFeed - Initialized', 0)
    
	BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		COURSE FEEDS

		*/
			  
		-- copy course feed messages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseFeedMessage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idLanguage, '
	   			 + '	idAuthor, '
	   			 + '	idParentCourseFeedMessage, '
	   			 + '	[message], '
	   			 + '	[timestamp], '
	   			 + '	dtApproved, '
	   			 + '	idApprover, '
	   			 + '	isApproved '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	CFM.idLanguage, '
	   			 + '	TTAuthor.destinationId, '
	   			 + '	NULL, '						-- this null value of idParentCourseFeedMessage will be updated from temporary CourseFeedMessageIdMappings table below
	   			 + '	CFM.[message], '
	   			 + '	CFM.[timestamp], '
	   			 + '	CFM.dtApproved, '
	   			 + '	TTApprover.destinationId, '
	   			 + '	CFM.IsApproved '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseFeedMessage] CFM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTAuthor on (TTAuthor.sourceId = CFM.idAuthor AND TTAuthor.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTApprover on (TTApprover.sourceId = CFM.idApprover AND TTApprover.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CFM.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CFM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.1: Copy DiscussionFeed - Course Feed Messages Inserted', 0)

		-- insert the mapping for source to destination course feed message ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + ' SELECT '
	   			 + '	SRC.idCourseFeedMessage, '
	   			 + '	DST.idCourseFeedMessage, '
	   			 + '	''coursefeedmessage'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseFeedMessage] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseFeedMessage] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			 + '	AND (SRC.[timestamp] = DST.[timestamp]) '
	   			 + '	AND (SRC.message = DST.message)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.2: Copy DiscussionFeed - Course Feed Message Mappings Created', 0)
	   
		-- update idParentCourseFeedMessage from temp table CourseFeedMessage
		SET @sql = 'UPDATE DSTC '
	   			 + '	SET idParentCourseFeedMessage = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseFeedMessage] SRCC '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseFeedMessage] DSTC '
	   			 + 'ON(SRCC.[timestamp] = DSTC.[timestamp] AND SRCC.message = DSTC.message AND SRCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)+ ') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCC.idParentCourseFeedMessage AND TEMP.object = ''coursefeedmessage'') '
	   			 + 'WHERE DSTC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			 + 'AND SRCC.idParentCourseFeedMessage IS NOT NULL'
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.3: Copy DiscussionFeed - Course Feed Message Parent Ids Updated', 0)

		-- copy course feed moderators
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseFeedModerator] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idModerator '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseFeedModerator] CFM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = CFM.idModerator AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CFM.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CFM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.4: Copy DiscussionFeed - Course Feed Moderators Inserted', 0)

		-- copy course ratings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCourseRating] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCourse, '
	   			 + '	idUser, '
	   			 + '	rating, '
	   			 + '	[timestamp] '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	TTU.destinationId, '
	   			 + '	CR.rating, '
	   			 + '	CR.[timestamp] '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCourseRating] CR '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = CR.idUser AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CR.idCourse AND TTC.object = ''courses'') '
	   			 + 'WHERE CR.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.5: Copy DiscussionFeed - Course Ratings Inserted', 0)

		/*

		GROUP FEEDS

		*/

		-- copy group feed messages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupFeedMessage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idGroup, '
	   			 + '	idLanguage, '
	   			 + '	idAuthor, '
	   			 + '	idParentGroupFeedMessage, '
	   			 + '	[message], '
	   			 + '	[timestamp], '
	   			 + '	dtApproved, '
	   			 + '	idApprover, '
	   			 + '	isApproved '
	   			 + ') '
	   			 + 'SELECT '
	   			 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTG.destinationId, '
	   			 + '	GFM.idLanguage, '
	   			 + '	TTAuthor.destinationId, '
	   			 + '	NULL, '					-- this null value of idParentGroupFeedMessage will be updated from temporary GroupFeedMessageIdMappings table below
	   			 + '	GFM.[message], '
	   			 + '	GFM.[timestamp], '
	   			 + '	GFM.dtApproved, '
	   			 + '	TTApprover.destinationId, '
	   			 + '	GFM.IsApproved '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupFeedMessage] GFM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTAuthor on (TTAuthor.sourceId = GFM.idAuthor AND TTAuthor.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTApprover on (TTApprover.sourceId = GFM.idApprover AND TTApprover.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GFM.idGroup AND TTG.object = ''groups'') '
	   			 + 'WHERE GFM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.6: Copy DiscussionFeed - Group Feed Messages Inserted', 0)
	   	   
		-- insert the mapping for source to destination group feed message ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idGroupFeedMessage, '
	   			 + '	DST.idGroupFeedMessage, '
	   			 + '	''groupfeedmessage'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupFeedMessage] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupFeedMessage] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			 + '	AND (SRC.[timestamp] = DST.[timestamp]) '
	   			 + '	AND (SRC.message = DST.message)) '
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   
		EXEC(@sql)
	   
	    INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.7: Copy DiscussionFeed - Group Feed Message Mappings Created', 0)
	   
		-- update idParentGroupFeedMessage from temp table GroupFeedMessage
		SET @sql = 'UPDATE DSTC '
	   			 + '	SET idParentGroupFeedMessage = TEMP.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupFeedMessage] SRCC '
	   			 + 'INNER JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupFeedMessage] DSTC '
	   			 + 'ON (SRCC.[timestamp] = DSTC.[timestamp] AND SRCC.message = DSTC.message AND SRCC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)+ ') '
	   			 + 'INNER JOIN [dbo].[ClonePortal_IDMappings] TEMP '
	   			 + 'ON (TEMP.sourceId = SRCC.idParentGroupFeedMessage AND TEMP.object = ''groupfeedmessage'') '
	   			 + 'WHERE DSTC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   			 + 'AND SRCC.idParentGroupFeedMessage IS NOT NULL'
	   
		EXEC(@sql)	
        
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.8: Copy DiscussionFeed - Group Feed Message Parent Ids Updated', 0)

		-- copy course feed moderators
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblGroupFeedModerator] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idGroup, '
	   			 + '	idModerator '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTG.destinationId, '
	   			 + '	TTU.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblGroupFeedModerator] GFM '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = GFM.idModerator AND TTU.object = ''users'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTG on (TTG.sourceId = GFM.idGroup AND TTG.object = ''groups'') '
	   			 + 'WHERE GFM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10.9: Copy DiscussionFeed - Group Feed Moderators Inserted', 0)

		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_DiscussionFeed_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
			
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 10: Copy DiscussionFeed - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_DiscussionFeed_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF

END