
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.ActivityImportEmail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.ActivityImportEmail]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.ActivityImportEmail]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)

	/*************************************************ACTIVITY IMPORT****************************************************/
		-- tblActivityImport
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblActivityImport]'
				+ ' ( '
				+ '		idSite, '
				+ '		idUser, '
				+ '		timestamp, '
				+ '		idUserImported, '
				+ '		importFileName '
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','	-- idSite
				+ '		UM.destinationID,'								-- user id
				+ '		AI.timestamp,'									-- timestamp
				+ '		UIM.destinationID,'								-- user imported id
				+ '		AI.importFileName'								-- import filename
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblActivityImport] AI'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UM ON UM.sourceID = AI.idUser AND UM.object = ''user'''
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] UIM ON UIM.sourceID = AI.idUserImported AND UIM.object = ''user'''
				+ ' WHERE idSite = ' + CONVERT(NVARCHAR, @idSiteSource)
		
		EXEC (@sql)

		-- insert idActivityImport mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SAI.idActivityImport, '
				+ '		DAI.idActivityImport,'
				+ '		''activityimport''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblActivityImport] DAI '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblActivityImport] SAI ON SAI.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SAI.timestamp = DAI.timestamp'
				+ ' WHERE DAI.idSite IS NOT NULL AND DAI.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SAI.idActivityImport IS NOT NULL AND DAI.idActivityImport IS NOT NULL'
		
		EXEC(@sql)

	/**************************************************EVENT EMAIL NOTIFICATION******************************************/
		-- tblEventEmailNotification
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification]'
				+ ' ( '
				+ '		idSite, '
				+ '		name, '
				+ '		idEventType, '
				+ '		idEventTypeRecipient, '
				+ '		idObject, '
				+ '		[from], '
				+ '		isActive, '
				+ '		dtActivation, '
				+ '		interval,'
				+ '		timeframe,'
				+ '		priority'
				+ ' ) '
				+ ' SELECT ' 
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','										-- idSite
				+ '		EEN.name + '' ##'' + CONVERT(NVARCHAR, EEN.idEventEmailNotification) + ''##'','		-- name
				+ '		ETM.destinationID,'																	-- idEventType
				+ '		ETRM.destinationID,'																-- idEventTypeRecipient
				+ '		NULL,'																				-- idObject
				+ '		EEN.[from],'																		-- from
				+ '		EEN.isActive,'																		-- is active?
				+ '		CASE WHEN EEN.isActive = 1 THEN GETUTCDATE() ELSE NULL END,'						-- activation date
				+ '		EEN.interval,'																		-- interval
				+ '		EEN.timeframe,'																		-- timeframe
				+ '		EEN.priority'																		-- priority
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] EEN'				
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings] ETM ON ETM.sourceID = EEN.idEventType'
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings] ETRM ON ETRM.sourceID = EEN.idEventTypeRecipient'
				+ ' WHERE EEN.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND EEN.idEventType <> 4'	-- exclude "Purchase Confirmed"
		
		EXEC (@sql)

		-- insert idEventEmailNotification mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		SEEN.idEventEmailNotification, '
				+ '		DEEN.idEventEmailNotification,'
				+ '		''emailnotification''' 
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] SEEN ON SEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteSource) + ' AND SEEN.name + '' ##'' + CONVERT(NVARCHAR, SEEN.idEventEmailNotification) + ''##'' = DEEN.name collate database_default'
				+ ' WHERE DEEN.idSite IS NOT NULL AND DEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination) + ' AND SEEN.idEventEmailNotification IS NOT NULL AND DEEN.idEventEmailNotification IS NOT NULL'
		
		EXEC(@sql)

		-- clean up the ##idEventEmailNotification## additions we made to email notification titles, we did that to uniquely distinguish names
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] SET '
				+ '		name = REPLACE(DEEN.name, '' ##'' + CONVERT(NVARCHAR, EENM.sourceID) + ''##'', '''') '
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EENM ON EENM.destinationID = DEEN.idEventEmailNotification AND EENM.object = ''emailnotification'''
				+ ' WHERE EENM.sourceID IS NOT NULL'

		EXEC (@sql)

		-- tblEventEmailNotificationLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotificationLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idEventEmailNotification,'
				+ '		idLanguage,'
				+ '		name'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		EEN.idEventEmailNotification, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ', '
				+ '		EEN.name '				
				+ ' FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblEventEmailNotification] EEN'
				+ ' WHERE EEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)

		-- create a table, and get email notification data mappings so email notification config XML files can be written		
		SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EmailNotificationDataMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings]'
			+ '('
			+ '    [destinationID] INT NOT NULL,'
			+ '    [data] NVARCHAR(MAX) NOT NULL'
			+ ')'
			+ ' END '
			+ ' ELSE '
		SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings]'

		EXEC(@sql)
		
		-- get the email notification data		
		SET @sql = ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EmailNotificationDataMappings] '
				+ ' ( '
				+ '		destinationID,'
				+ '		data'
				+ ' ) '
				+ ' SELECT '
				+ '		DEEN.idEventEmailNotification, '
				+ '		  ''<?xml version="1.0" encoding="utf-8"?>'' '
				+ '		+ ''<emailNotification>'' '
				+ '		+ ''	<language code="default">'' '
				+ '		+ ''		<subject><![CDATA['' + SEEN.[subject] + '']]></subject>'' '
				+ '		+ ''		<body><![CDATA['' + SEEN.[bodyTemplate] + '']]></body>'' '
				+ '		+ ''	</language>'' '
				+ '		+ ''	<language code="en-US">'' '
				+ '		+ ''		<subject><![CDATA['' + SEEN.[subject] + '']]></subject>'' '
				+ '		+ ''		<body><![CDATA['' + SEEN.[bodyTemplate] + '']]></body>'' '
				+ '		+ ''	</language>'' '
				+ '		+ ''</emailNotification>'' '				
				+ ' FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblEventEmailNotification] DEEN '
				+ ' LEFT JOIN  [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] EENM ON EENM.destinationID = DEEN.idEventEmailNotification AND EENM.object = ''emailnotification'''
				+ ' LEFT JOIN [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblEventEmailNotification] SEEN ON SEEN.idEventEmailNotification = EENM.sourceID'				
				+ ' WHERE DEEN.idSite IS NOT NULL AND DEEN.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
				+ ' AND SEEN.idSite IS NOT NULL AND SEEN.idSite = '+ CONVERT(NVARCHAR, @idSiteSource)
	
		EXEC (@sql)

		/*********************************************************COMMIT THE TRANSACTION**********************************************************/

	SET @Return_Code = 0
	SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaActivityImportEmail_Success'

		
	SET XACT_ABORT OFF

END
GO