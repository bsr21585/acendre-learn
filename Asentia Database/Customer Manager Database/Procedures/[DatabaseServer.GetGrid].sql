
-- =====================================================================
-- PROCEDURE: [DatabaseServer.GetGrid]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[DatabaseServer.GetGrid]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [DatabaseServer.GetGrid]
GO


CREATE PROCEDURE [DatabaseServer.GetGrid]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idCallerAccount		INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT,
	
	@searchParam			NVARCHAR(4000),
	@pageNum				INT, 
	@pageSize				INT, 
	@orderColumn			NVARCHAR(255),
	@orderAsc				BIT
)
AS
	BEGIN
		SET NOCOUNT ON
		
		/* get caller permission */
		
		DECLARE @idUserRole INT
		SELECT @idUserRole = idRole FROM tblAccountUser WHERE idAccountUser = @idCaller		
		
		IF @searchParam = '' OR @searchParam = '*'
			BEGIN
			SET @searchParam = null
			END

		IF @searchParam IS NULL
			
			BEGIN
			
			-- return the rowcount select * from tblDataBaseServer
			
			SELECT COUNT(1) AS row_count 
			FROM tblDatabaseServer
			WHERE
			idDatabaseServer <> 1 or  @idUserRole = 1
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idDatabaseServer,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN serverName END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN serverName END
						)
						AS [row_number]
					FROM tblDatabaseServer
					WHERE
					 idDatabaseServer <> 1 or  @idUserRole = 1
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDatabaseServer, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idDatabaseServer, 
				A.serverName, 
				A.networkName,
				A.username,
				A.password,
				A.isTrusted,
				CONVERT(BIT, 1) AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblDatabaseServer A ON A.idDatabaseServer = SelectedKeys.idDatabaseServer
			
			ORDER BY SelectedKeys.[row_number]
			
			END
			
		ELSE
			
			BEGIN
			
			-- return the rowcount
			
			SELECT COUNT(1) AS row_count 
			FROM tblDatabaseServer
			INNER JOIN CONTAINSTABLE(tblDatabaseServer, *, @searchParam) K ON K.[key] = tblDatabaseServer.idDatabaseServer
			WHERE 
			 idDatabaseServer <> 1 or @idUserRole = 1
			
			;WITH 
				Keys AS (
					SELECT TOP (@pageNum * @pageSize) 
						idDatabaseServer,
						ROW_NUMBER() OVER (ORDER BY 
							-- FIRST ORDER DESC
							CASE WHEN @orderAsc = 0 THEN serverName END DESC,
							
							-- FIRST ORDER ASC
							CASE WHEN @orderAsc IS NULL OR @orderAsc = 1 THEN serverName END
						)
						AS [row_number]
					FROM tblDatabaseServer
					INNER JOIN CONTAINSTABLE(tblDatabaseServer, *, @searchParam) K ON K.[key] = tblDataBaseServer.idDatabaseServer
					WHERE @idUserRole = 1
					AND idDatabaseServer <> 1
				), 
				
				SelectedKeys AS (
					SELECT -- TOP (@pageSize) ; this is unnecessary but lets leave it for now.
						idDatabaseServer, 
						[row_number]
					FROM Keys
					WHERE Keys.[row_number] > ((@pageNum - 1) * @pageSize)
				)
			SELECT 
				A.idDatabaseServer, 
				A.serverName, 
				A.networkName,
				A.username,
				A.password,
				A.isTrusted,
				CONVERT(BIT, 1) AS isModifyOn,
				SelectedKeys.[row_number]
			FROM SelectedKeys
			JOIN tblDatabaseServer A ON A.idDatabaseServer = SelectedKeys.idDatabaseServer
			
			ORDER BY SelectedKeys.[row_number]
			
			END
		
	END


GO


