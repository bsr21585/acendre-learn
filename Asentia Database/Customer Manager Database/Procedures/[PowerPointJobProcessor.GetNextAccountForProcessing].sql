-- =====================================================================
-- PROCEDURE: [PowerPointJobProcessor.GetNextAccountForProcessing]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[PowerPointJobProcessor.GetNextAccountForProcessing]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [PowerPointJobProcessor.GetNextAccountForProcessing]
GO

CREATE PROCEDURE [PowerPointJobProcessor.GetNextAccountForProcessing]
(
	@Return_Code				INT				OUTPUT,
	@Error_Description_Code		NVARCHAR(50)	OUTPUT,
	@idCallerAccount			INT				= 0, --default if not specified
	@callerLangString			NVARCHAR (10),
	@idCaller					INT
)
AS
	BEGIN
	SET NOCOUNT ON
	
	/* 
		validate caller, only caller id/account id 1 is valid here
		the job processor should be the only process calling this
		and it will run as id 1/1; other than that, only the system
		admin will be able to run this
	*/
	
	IF @idCaller <> 1 AND @idCallerAccount <> 1
		BEGIN
		SELECT @Return_Code = 3 -- caller permission error
		RETURN 1
		END
	
	/*
	
	get top 1 account id from accounts that are not currently processing and
	have not just finished processing within the last second ordered by the
	number of milliseconds descending since last process and account id
	
	*/
	
	SELECT TOP 1
		idAccount,
		DATEDIFF(SECOND, ISNULL(dtLastPowerPointJobProcessEnd,'1975-01-01 01:23:45.678'), GETUTCDATE()) AS timeDifferenceInSeconds
	FROM tblAccount A
	WHERE (
			A.isPowerPointJobProcessing IS NULL 
			OR 
			A.isPowerPointJobProcessing = 0
		  )
		  --AND (
				--DATEDIFF(n, A.dtLastPowerPointJobProcessEnd, GETUTCDATE()) IS NULL
				--OR
				--DATEDIFF(ss, A.dtLastPowerPointJobProcessEnd, GETUTCDATE()) > 1
			  --)
		  AND DATEDIFF(SECOND, ISNULL(dtLastPowerPointJobProcessEnd,'1975-01-01 01:23:45.678'), GETUTCDATE()) > 60
		  AND A.isActive = 1
		  AND (
				A.isDeleted IS NULL 
				OR 
				A.isDeleted = 0
			  )
		  AND A.idAccount > 1
	ORDER BY 
	timeDifferenceInSeconds DESC,
	idAccount ASC

	SET @Return_Code = 0
	SET @Error_Description_Code = ''
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO