SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.GetIDMappings]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.GetIDMappings]
GO


/*

Gets all records from the portal clone id mapping table.

*/
CREATE PROCEDURE [System.ClonePortal.GetIDMappings]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50),
	@forFolder				NVARCHAR(50)	= NULL
)
AS

BEGIN

    SET NOCOUNT ON

	IF (@forFolder IS NULL OR @forFolder = '')
		BEGIN

		SELECT * FROM [dbo].[ClonePortal_IDMappings]

		END
	
	IF (@forFolder = '_config')
		BEGIN

		SELECT * 
		FROM [dbo].[ClonePortal_IDMappings] 
		WHERE [object] IN ('catalog',
						   'certificates',
						   'certificationmodulerequirement',
						   'courses',
						   'datalesson',
						   'emailNotifications',
						   'groups',
						   'learningPaths',
						   'standuptraining',
						   'users')

		END

	IF (@forFolder = '_log')
		BEGIN

		SELECT * 
		FROM [dbo].[ClonePortal_IDMappings] 
		WHERE [object] IN ('datalesson',						   
						   'users')

		END

	IF (@forFolder = 'warehouse')
		BEGIN

		SELECT * 
		FROM [dbo].[ClonePortal_IDMappings] 
		WHERE [object] IN ('courses',
						   'groups',
						   'learningPaths',
						   'lesson',
						   'site',
						   'users')

		END


	SET @Return_Code = 0
	SET @Error_Description_Code	= 'SystemClonePortal_Site_InsertedSuccessfully'

END