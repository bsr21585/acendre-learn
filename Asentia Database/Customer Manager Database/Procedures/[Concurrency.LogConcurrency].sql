-- =====================================================================
-- PROCEDURE: [Concurrency.LogConcurrency]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[Concurrency.LogConcurrency]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [Concurrency.LogConcurrency]
GO

/*

Logs concurrent user counts for all account databases found in customer manager
that are active and not deleted.

*/

CREATE PROCEDURE [Concurrency.LogConcurrency]
AS
	
	BEGIN
	SET NOCOUNT ON

	DECLARE @idAccount		INT
	DECLARE @serverName		NVARCHAR(255)
	DECLARE @databaseName	NVARCHAR(255)
	DECLARE @SQL			NVARCHAR(MAX)
	DECLARE @utcNow			DATETIME
	DECLARE @utcNowString	NVARCHAR(50)

	SET @utcNow = GETUTCDATE()
	SET @utcNowString = CONVERT(NVARCHAR(10), DATEPART(year, @utcNow)) + '-' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(month, @utcNow)), 2) + '-' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(day, @utcNow)), 2) + ' ' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(hour, @utcNow)), 2) + ':' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(minute, @utcNow)), 2) + ':' +
					RIGHT('0' + CONVERT(NVARCHAR(10), DATEPART(second, @utcNow)), 2)

	DECLARE C CURSOR FOR 
	
		SELECT
			A.idAccount,
			LOWER(DBS.networkName),
			A.databaseName
		FROM tblAccount A
		LEFT JOIN tblDatabaseServer DBS ON DBS.idDatabaseServer = A.idDatabaseServer
		WHERE A.idAccount > 1
		AND A.isActive = 1
		AND (A.isDeleted IS NULL OR A.isDeleted = 0)

	OPEN C

	FETCH NEXT FROM C INTO @idAccount, @serverName, @databaseName

	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		IF (@serverName = 'asentia-sql01')	-- this is the database that the customer manager resides on, and therefore local
											-- so we do not need (and cannot have) a database server reference in the query
			BEGIN

			SET @SQL = '
				INSERT INTO tblConcurrency (
					idAccount,
					concurrentUsers,
					[timestamp]
				)
				SELECT 
					' + CONVERT(NVARCHAR, @idAccount) + ',
					COUNT(1),
					''' + @utcNowString + '''
				FROM 
				[' + @databaseName + '].[dbo].[tblUser] WHERE dtSessionExpires > GETUTCDATE()'

			END

		ELSE
			BEGIN

			SET @SQL = '
				INSERT INTO tblConcurrency (
					idAccount,
					concurrentUsers,
					[timestamp]
				)
				SELECT 
					' + CONVERT(NVARCHAR, @idAccount) + ',
					COUNT(1),
					''' + @utcNowString + '''
				FROM 
				[' + @serverName + '].[' + @databaseName + '].[dbo].[tblUser] WHERE dtSessionExpires > GETUTCDATE()'

			END
	
		EXECUTE sp_executesql @SQL

		FETCH NEXT FROM C INTO @idAccount, @serverName, @databaseName

	END

	CLOSE C
	DEALLOCATE C
		
	END
	
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
