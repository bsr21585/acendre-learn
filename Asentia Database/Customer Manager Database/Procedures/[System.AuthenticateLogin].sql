-- =====================================================================
-- PROCEDURE: [System.AuthenticateLogin]

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF exists (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.AuthenticateLogin]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.AuthenticateLogin]
GO

/*

Authenticates credentials for login to the system.

*/
CREATE PROCEDURE [dbo].[System.AuthenticateLogin]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,
	@idAccount				INT				OUTPUT,
	@idAccountUser			INT				OUTPUT,
	@userFirstName			NVARCHAR(255)	OUTPUT,
	@userLastName			NVARCHAR(255)	OUTPUT,
	@userCulture			NVARCHAR(20)	OUTPUT,
	
	@username				NVARCHAR(512),
	@password				NVARCHAR(512)
)
AS
	BEGIN
	SET NOCOUNT ON
	
	DECLARE @count INT
	SET @count = 0
	
	/* 
	
	attempt to authenticate the system administrator user, this
	is the only account that should be named "administrator"
	
	*/
	
	IF @username = 'admin' OR @username = 'administrator'
		BEGIN
		
		SELECT @count = COUNT(1)
		FROM tblAccountUser AU
		WHERE AU.username = 'administrator'
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount = 1
		
		IF (@count) = 0 
		BEGIN
		SET @Return_Code = 3 -- failed login
		SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'   
		RETURN 1
		END
		
		IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 "administrator" account (very BAD)
		SET @Error_Description_Code = 'SystemAuthenticateLogin_DuplicateAccount'    
		RETURN 1
		END
	
		SELECT TOP 1
			@idAccount = 1,
			@idAccountUser = AU.idAccountUser,
			@userFirstName = AU.firstName,
			@userLastName = AU.lastName,
			@userCulture = 'en-US' --L.code
		FROM tblAccountUser AU
		WHERE AU.username = 'administrator'
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount = 1
			
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		
		END
		
	ELSE
		BEGIN
		
		/*
		
		attempt to authenticate as an account user
		
		*/
		
		SELECT @count = COUNT(1)
		FROM tblAccountUser AU
		WHERE AU.username = @username
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount > 1
		
		IF (@count) = 0 
		BEGIN
		SET @Return_Code = 3 -- failed login
		SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'
		END
		
		IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'SystemAuthenticateLogin_DuplicateAccount' 
		RETURN 1
		END
		
		IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccount = AU.idAccount,
			@idAccountUser = AU.idAccountUser,
			@userFirstName = AU.firstName,
			@userLastName = AU.lastName,
			@userCulture = 'en-US' --L.code
		FROM tblAccountUser AU
		WHERE AU.username = @username
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount > 1
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
		
		/*
		
		attempt to authenticate as an account administrator
		
		*/
		
		SELECT @count = COUNT(1)
		FROM tblAccount A
		WHERE A.username = @username
		AND A.[password] = dbo.GetHashedString('SHA1', @password)
		
		IF (@count) = 0 
		BEGIN
		SET @Return_Code = 3 -- failed login
		SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'
		END
		
		IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'SystemAuthenticateLogin_DuplicateAccount'
		RETURN 1
		END
		
		IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccount = A.idAccount,
			@idAccountUser = 1,
			@userFirstName = A.contactFirstName,
			@userLastName = A.contactLastName,
			@userCulture = 'en-US' --L.code
		FROM tblAccount A
		WHERE A.username = @username
		AND A.[password] = dbo.GetHashedString('SHA1', @password)
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
		
		/*
		
		attempt to authenticate as an system user
		
		*/
		
		SELECT @count = COUNT(1)
		FROM tblAccountUser AU
		WHERE AU.username = @username
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount = 1
		
		IF (@count) = 0 
		BEGIN
		SET @Return_Code = 3 -- failed login
		SET @Error_Description_Code = 'SystemAuthenticateLogin_Failed'
		END
		
		IF (@count) > 1 
		BEGIN
		SET @Return_Code = 2 -- more than 1 account (very BAD)
		SET @Error_Description_Code = 'SystemAuthenticateLogin_DuplicateAccount'
		RETURN 1
		END
		
		IF (@count) = 1
		BEGIN
		SELECT TOP 1
			@idAccount = 1,
			@idAccountUser = AU.idAccountUser,
			@userFirstName = AU.firstName,
			@userLastName = AU.lastName,
			@userCulture = 'en-US' --L.code
		FROM tblAccountUser AU
		WHERE AU.username = @username
		AND AU.[password] = dbo.GetHashedString('SHA1', @password)
		AND AU.idAccount = 1
		
		SELECT @Return_Code = 0 --(0 is 'success')
		RETURN 1
		END
		
		END
		
		RETURN 1
		
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO