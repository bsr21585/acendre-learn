SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.ClonePortal.CertificateCertification]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.ClonePortal.CertificateCertification]
GO


/*

CLONE PORTAL - CERTIFICATE / CERTIFICATION DATA
OCCURS FOR FULL AND PARTIAL COPY

*/
CREATE PROCEDURE [System.ClonePortal.CertificateCertification]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,

	@fullCopyFlag			BIT,
	@idSiteSource			INT,
	@idSiteDestination		INT,
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)
)
AS

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.0: Copy CertificateCertification - Initialized', 0)
    
    BEGIN TRY	
    
		DECLARE @sql NVARCHAR(MAX)

		/*
	  
		replace double slash with single slash. WHY DO WE DO THIS?!? Need to find out!
	 
		*/

		SET @sourceDBServer = REPLACE(@sourceDBServer, '\\', '\')
		SET @destinationDBServer = REPLACE(@destinationDBServer, '\\', '\')

		/*

		CERTIFICATES

		*/	  

		-- copy certificates
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificate] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	name, '
	   			 + '	issuingOrganization, '
	   			 + '	[description], '
	   			 + '	credits, '
	   			 + '	code, '
	   			 + '	[filename], '
	   			 + '	isActive, '
	   			 + '	activationDate, '
	   			 + '	expiresInterval, '
	   			 + '	expiresTimeframe, '
	   			 + '	objectType, '
	   			 + '	idObject, '
	   			 + '	isDeleted, '
	   			 + '	dtDeleted, '
	   			 + '	reissueBasedOnMostRecentCompletion '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '	   			 
				 + '	C.name  + '' ##'' + CAST(C.idCertificate AS NVARCHAR) + ''##'', '
	   			 + '	C.issuingOrganization, '
	   			 + '	C.[description], '
	   			 + '	C.credits, '
	   			 + '	C.code, '
	   			 + '	C.[filename], '
	   			 + '	C.isActive, '
	   			 + '	C.activationDate, '
	   			 + '	C.expiresInterval, '
	   			 + '	C.expiresTimeframe, '
	   			 + '	C.objectType, '
	   			 + '	CASE C.objectType '
	   			 + '		WHEN 1 THEN '
	   			 + '			TTC.destinationId '
	   			 + '		WHEN 2 THEN '
	   			 + '			TTLP.destinationId '
	   			 + '	ELSE '
	   			 + '		NULL '
	   			 + '	END, '
	   			 + '	C.isDeleted, '
	   			 + '	C.dtDeleted, '
	   			 + '	C.reissueBasedOnMostRecentCompletion '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificate] C '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = C.idObject AND TTC.object = ''courses'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTLP on (TTLP.sourceId = C.idObject AND TTLP.object = ''learningPaths'') '
	   			 + 'WHERE C.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.1: Copy CertificateCertification - Certificates Inserted', 0)

		-- insert the mapping for source to destination certificate ids
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   			 + '( '
	   			 + '	sourceId, '
	   			 + '	destinationId, '
	   			 + '	[object] '
	   			 + ') '
	   			 + 'SELECT '
	   			 + '	SRC.idCertificate, '
	   			 + '	DST.idCertificate, '
	   			 + '	''certificates'' '
	   			 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificate] DST '
	   			 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificate] SRC '
	   			 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.name + '' ##'' + CAST(SRC.idCertificate AS NVARCHAR) + ''##'' = DST.name) '	   			 
	   			 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.2: Copy CertificateCertification - Certificate Mappings Created', 0)

		-- clean up the ##idCertificate## additions we made to certificate names, we did that to uniquely distinguish courses
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificate] SET '
				 + '	name = REPLACE(DC.name, '' ##'' + CAST(SC.sourceID AS NVARCHAR) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertificate] DC '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SC ON SC.destinationID = DC.idCertificate AND SC.object = ''certificates'' '
				 + 'WHERE DC.idSite = ' + CAST(@idSiteDestination AS NVARCHAR) + ' AND SC.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.3: Copy CertificateCertification - Certificate Names Cleaned Up', 0)

		-- copy certificate languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateLanguage] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idCertificate, '
	   			 + '	idLanguage, '
	   			 + '	name, '
	   			 + '	[description], '
				 + '	[filename] '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTC.destinationId, '
	   			 + '	CL.idLanguage, '
	   			 + '	CL.name, '
	   			 + '	CL.[description], '
				 + '	CL.[filename] '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateLanguage] CL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CL.idCertificate AND TTC.object = ''certificates'') '
	   			 + 'WHERE CL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   		
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.4: Copy CertificateCertification - Certificate Languages Inserted', 0)

		/*

		CERTIFICATE RECORDS - FULL COPY ONLY

		*/
			   
		IF @fullCopyFlag = 1
			BEGIN

			-- copy certificate import records
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateImport] '
	   				 + '( '
	   				 + '	[idSite], '
	   				 + '	[idUser], '
	   				 + '	[timestamp], '
	   				 + '	[idUserImported], '
	   				 + '	[importFileName] '
	   				 + ') '
	   				 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   				 + '	TTUser.destinationId, '
	   				 + '	CI.[timestamp], '
	   				 + '	TTUserImported.destinationId, '
	   				 + '	CI.[importFileName] '
	   				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateImport] CI '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUser on (TTUser.sourceId = CI.idUser AND TTUser.object = ''users'') '
	   				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTUserImported on (TTUserImported.sourceId = CI.idUserImported AND TTUserImported.object = ''users'') '
	   				 + 'WHERE CI.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
	   			
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.5: Copy CertificateCertification - Certificate Import Records Inserted', 0)

			-- insert the mapping for source to destination certificate import id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   				 + '( '
	   				 + '	sourceId, '
	   				 + '	destinationId, '
	   				 + '	[object] '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	SRC.idCertificateImport, '
	   				 + '	DST.idCertificateImport, '
	   				 + '	''certificateimport'' '
	   				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateImport] DST '
	   				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateImport] SRC '
	   				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   				 + '	AND (SRC.[timestamp] = DST.[timestamp]) '
	   				 + '	AND (SRC.[importFileName] = DST.[importFileName] OR (SRC.[importFileName] IS NULL AND DST.[importFileName] IS NULL))) '
	   				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
	   		
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.6: Copy CertificateCertification - Certificate Import Record Mappings Created', 0)

			-- copy certificate records
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateRecord] '
              		 + '( '
              		 + '	idSite, '
              		 + '	idCertificate, '
              		 + '	idUser, '
              		 + '	idAwardedBy, '
              		 + '	[timestamp], '
              		 + '	expires, '
              		 + '	idTimezone, '
              		 + '	code, '
              		 + '	credits, '
              		 + '	awardData, '
              		 + '	idCertificateImport '
              		 + ') '
              		 + 'SELECT '
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
              		 + '	TTC.destinationId, '
              		 + '	TTU.destinationId, '
              		 + '	TTAB.destinationId, '
              		 + '	CR.[timestamp], '
              		 + '	CR.expires, '
              		 + '	CR.idTimezone, '
              		 + '	CR.code, '
              		 + '	CR.credits, '
              		 + '	CR.awardData, '
              		 + '	TTCI.destinationId '
              		 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateRecord] CR '
              		 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CR.idCertificate AND TTC.object = ''certificates'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = CR.idUser AND TTU.object = ''users'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTAB on (TTAB.sourceId = CR.idAwardedBy AND TTAB.object = ''users'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCI on (TTCI.sourceId = CR.idCertificateImport AND TTCI.object = ''certificateimport'') '
              		 + 'WHERE CR.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
              			
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.7: Copy CertificateCertification - Certificate Records Inserted', 0)

			-- insert the mapping for source to destination certificate record id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
	   				 + '( '
	   				 + '	sourceId, '
	   				 + '	destinationId, '
	   				 + '	[object] '
	   				 + ') '
	   				 + 'SELECT '
	   				 + '	SRC.idCertificateRecord, '
	   				 + '	DST.idCertificateRecord, '
	   				 + '	''certificaterecord'' '
	   				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificateRecord] DST '
	   				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificateRecord] SRC '
	   				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   				 + '	AND (SRC.[timestamp] = DST.[timestamp]) '
	   				 + '	AND (SRC.code = DST.code OR (SRC.code IS NULL AND DST.code IS NULL)) '
	   				 + '	AND (SRC.credits = DST.credits OR (SRC.credits IS NULL AND DST.credits IS NULL))) '
	   				 + ' INNER JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = SRC.idCertificate AND TTC.destinationId =  DST.idCertificate AND TTC.object = ''certificates'') '
	   				 + ' INNER JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = SRC.idUser AND TTU.destinationId = DST.idUser AND TTU.object = ''users'') '
	   				 + ' WHERE DST.idSite =' + CAST(@idSiteDestination AS NVARCHAR)
	   		
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.8: Copy CertificateCertification - Certificate Record Mappings Created', 0)

			END

		/*

		CERTIFICATIONS

		*/

		-- copy certifications
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertification] '
				 + '( '
				 + '	idSite, '
				 + '	title, '
				 + '	shortDescription, '
				 + '	searchTags, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	initialAwardExpiresInterval, '
				 + '	initialAwardExpiresTimeframe, '
				 + '	renewalExpiresInterval, '
				 + '	renewalExpiresTimeframe, '
				 + '	accreditingOrganization, '
				 + '	certificationContactName, '
				 + '	certificationContactEmail, '
				 + '	certificationContactPhoneNumber, '
				 + '	isPublished, '
				 + '	isClosed, '
				 + '	isAnyModule, '
				 + '	isRenewalAnyModule '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	C.title + '' ##'' + CAST(C.idCertification AS NVARCHAR) + ''##'', '
				 + '	C.shortDescription, '
				 + '	C.searchTags, '
				 + '	C.dtCreated, '
				 + '	C.dtModified, '
				 + '	C.isDeleted, '
				 + '	C.dtDeleted, '
				 + '	C.initialAwardExpiresInterval, '
				 + '	C.initialAwardExpiresTimeframe, '
				 + '	C.renewalExpiresInterval, '
				 + '	C.renewalExpiresTimeframe, '
				 + '	C.accreditingOrganization, '
				 + '	C.certificationContactName, '
				 + '	C.certificationContactEmail, '
				 + '	C.certificationContactPhoneNumber, '
				 + '	C.isPublished, '
				 + '	C.isClosed, '
				 + '	C.isAnyModule, '
				 + '	C.isRenewalAnyModule '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertification] C '
				 + 'WHERE C.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
		
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.9: Copy CertificateCertification - Certifications Inserted', 0)
				
		-- insert the mapping for source to destination certification id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCertification, '
				 + '	DST.idCertification, '
				 + '	''certification'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertification] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertification] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SRC.title + '' ##'' + CONVERT(NVARCHAR, SRC.idCertification) + ''##'' = DST.title) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.10: Copy CertificateCertification - Certification Mappings Created', 0)

		-- clean up the ##idCertification## additions we made to titles, we did that to uniquely distinguish certifications
		SET @sql = 'UPDATE [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertification] SET '
				 + '	title = REPLACE(DC.title, '' ##'' + CONVERT(NVARCHAR, SC.sourceID) + ''##'', '''') '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblCertification] DC '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] SC ON SC.destinationID = DC.idCertification AND SC.object = ''certification'' '
				 + 'WHERE SC.sourceID IS NOT NULL'

		EXEC (@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.11: Copy CertificateCertification - Certification Titles Cleaned Up', 0)

		-- copy certification languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCertification, '
				 + '	idLanguage, '
				 + '	title, '
				 + '	shortDescription, '
				 + '	searchTags '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	CL.idLanguage, '
				 + '	CL.title, '
				 + '	CL.shortDescription, '
				 + '	CL.searchTags '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationLanguage] CL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CL.idCertification AND TTC.object = ''certification'') '
				 + 'WHERE CL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				              	
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.12: Copy CertificateCertification - Certification Languages Inserted', 0)

		-- copy certification modules
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModule] '
				 + '( '
				 + '	idSite, '
				 + '	idCertification, '
				 + '	title, '
				 + '	shortDescription, '
				 + '	isAnyRequirementSet, '
				 + '	dtCreated, '
				 + '	dtModified, '
				 + '	isDeleted, '
				 + '	dtDeleted, '
				 + '	isInitialRequirement '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTC.destinationId, '
				 + '	CM.title, '
				 + '	CM.shortDescription, '
				 + '	CM.isAnyRequirementSet, '
				 + '	CM.dtCreated, '
				 + '	CM.dtModified, '
				 + '	CM.isDeleted, '
				 + '	CM.dtDeleted, '
				 + '	CM.isInitialRequirement '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModule] CM '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CM.idCertification AND TTC.object = ''certification'') '
				 + 'WHERE CM.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
					
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.13: Copy CertificateCertification - Certification Modules Inserted', 0)

		-- insert the mapping for source to destination certification module id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCertificationModule, '
				 + '	DST.idCertificationModule, '
				 + '	''certificationmodule'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModule] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModule] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND (SRC.title = DST.title) '
				 + '	AND (SRC.dtCreated = DST.dtCreated OR (SRC.dtCreated IS NULL AND DST.dtCreated IS NULL)) '
				 + '	AND (SRC.dtModified = DST.dtModified OR (SRC.dtModified IS NULL AND DST.dtModified IS NULL))) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.14: Copy CertificateCertification - Certification Module Mappings Created', 0)
				
		-- copy certification module mappings
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModule, '
				 + '	idLanguage, '
				 + '	title, '
				 + '	shortDescription '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCM.destinationId, '
				 + '	CML.idLanguage, '
				 + '	CML.title, '
				 + '	CML.shortDescription '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleLanguage] CML '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCM on (TTCM.sourceId = CML.idCertificationModule AND TTCM.object = ''certificationmodule'') '
				 + 'WHERE CML.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
              			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.15: Copy CertificateCertification - Certification Module Languages Inserted', 0)

		-- copy certification module requirement sets
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementSet] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModule, '
				 + '	isAny, '
				 + '	label '				 
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCM.destinationId, '
				 + '	CMRS.isAny, '
				 + '	CMRS.label  + '' ##'' + CAST(CMRS.idCertificationModuleRequirementSet AS NVARCHAR) + ''##'' '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementSet] CMRS '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCM on (TTCM.sourceId = CMRS.idCertificationModule AND TTCM.object = ''certificationmodule'') '
				 + 'WHERE CMRS.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
					
		EXEC(@sql)
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.16: Copy CertificateCertification - Certification Module Requirement Sets Inserted', 0)
				
		-- insert the mapping for source to destination certification module requirement set id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCertificationModuleRequirementSet, '
				 + '	DST.idCertificationModuleRequirementSet, '
				 + '	''certificationmodulerequirementset'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementSet] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementSet] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 + '	AND SRC.label + '' ##'' + CAST(SRC.idCertificationModuleRequirementSet AS NVARCHAR) + ''##'' = DST.label) '
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.17: Copy CertificateCertification - Certification Module Requirement Set Mappings Created', 0)

		-- copy certification module requirement set languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementSetLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModuleRequirementSet, '
				 + '	idLanguage, '
				 + '	label '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCMRS.destinationId, '
				 + '	CMRSL.idLanguage, '
				 + '	CMRSL.label '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementSetLanguage] CMRSL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMRS on (TTCMRS.sourceId = CMRSL.idCertificationModuleRequirementSet AND TTCMRS.object = ''certificationmodulerequirementset'') '
				 + 'WHERE CMRSL.idSite = ' + CAST(@idSiteSource AS NVARCHAR)

		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.18: Copy CertificateCertification - Certification Module Requirement Set Languages Inserted', 0)
   
		-- copy certification module requirements
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirement] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModuleRequirementSet, '
				 + '	label, '
				 + '	shortDescription, '
				 + '	requirementType, '
				 + '	courseCompletionIsAny, '
				 + '	forceCourseCompletionInOrder, '
				 + '	numberCreditsRequired, '
				 + '	documentUploadFileType, '
				 + '	documentationApprovalRequired, '
				 + '	allowSupervisorsToApproveDocumentation, '
				 + '	allowExpertsToApproveDocumentation, '
				 + '	courseCreditEligibleCoursesIsAll '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCMRS.destinationId, '				 
				 + '	CMR.label  + '' ##'' + CAST(CMR.idCertificationModuleRequirement AS NVARCHAR) + ''##'', '
				 + '	CMR.shortDescription, '
				 + '	CMR.requirementType, '
				 + '	CMR.courseCompletionIsAny, '
				 + '	CMR.forceCourseCompletionInOrder, '
				 + '	CMR.numberCreditsRequired, '
				 + '	CMR.documentUploadFileType, '
				 + '	CMR.documentationApprovalRequired, '
				 + '	CMR.allowSupervisorsToApproveDocumentation, '
				 + '	CMR.allowExpertsToApproveDocumentation, '
				 + '	CMR.courseCreditEligibleCoursesIsAll '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirement] CMR '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMRS on (TTCMRS.sourceId = CMR.idCertificationModuleRequirementSet AND TTCMRS.object = ''certificationmodulerequirementset'') '
				 + 'WHERE CMR.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
					
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.19: Copy CertificateCertification - Certification Module Requirements Inserted', 0)

		-- insert the mapping for source to destination certification module requirement id
		SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
				 + '( '
				 + '	sourceId, '
				 + '	destinationId, '
				 + '	[object] '
				 + ') '
				 + 'SELECT '
				 + '	SRC.idCertificationModuleRequirement, '
				 + '	DST.idCertificationModuleRequirement, '
				 + '	''certificationmodulerequirement'' '
				 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirement] DST '
				 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirement] SRC '
				 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR)
				 + '	AND SRC.label + '' ##'' + CAST(SRC.idCertificationModuleRequirement AS NVARCHAR) + ''##'' = DST.label) '				 
				 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.20: Copy CertificateCertification - Certification Module Requirement Mappings Created', 0)

		-- copy certification module requirement languages
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementLanguage] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModuleRequirement, '
				 + '	idLanguage, '
				 + '	label, '
				 + '	shortDescription '
				 + ') '
				 + ' SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCMR.destinationId, '
				 + '	CMRL.idLanguage, '
				 + '	CMRL.label, '
				 + '	CMRL.shortDescription '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementLanguage] CMRL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMR on (TTCMR.sourceId = CMRL.idCertificationModuleRequirement AND TTCMR.object = ''certificationmodulerequirement'') '
				 + 'WHERE CMRL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
              			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.21: Copy CertificateCertification - Certification Module Requirement Languages Inserted', 0)

		-- copy certification module requirement to course links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationModuleRequirementToCourseLink] '
				 + '( '
				 + '	idSite, '
				 + '	idCertificationModuleRequirement, '
				 + '	idCourse, '
				 + '	[order] '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCMR.destinationId, '
				 + '	TTC.destinationId, '
				 + '	CMRTCL.[order] '
				 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationModuleRequirementToCourseLink] CMRTCL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMR on (TTCMR.sourceId = CMRTCL.idCertificationModuleRequirement AND TTCMR.object = ''certificationmodulerequirement'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CMRTCL.idCourse AND TTC.object = ''courses'') '
				 + 'WHERE CMRTCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
				 				
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.22: Copy CertificateCertification - Certification Module Requirement to Course Links Inserted', 0)

		-- copy certification to course credit links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationToCourseCreditLink] '
				 + '( '
				 + '	idSite, '
				 + '	idCertification, '
				 + '	idCourse, '
				 + '	credits '
				 + ') '
				 + 'SELECT ' 
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
				 + '	TTCM.destinationId, '
				 + '	TTC.destinationId, '
				 + '	CTCCL.credits '
				 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationToCourseCreditLink] CTCCL '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCM on (TTCM.sourceId = CTCCL.idCertification AND TTCM.object = ''certification'') '
				 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CTCCL.idCourse AND TTC.object = ''courses'') '
				 + 'WHERE CTCCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
					
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.23: Copy CertificateCertification - Certification to Course Credit Links Inserted', 0)

		-- copy ruleset to certification links
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblRuleSetToCertificationLink] '
	   			 + '( '
	   			 + '	idSite, '
	   			 + '	idRuleSet, '
	   			 + '	idCertification '
	   			 + ') '
	   			 + 'SELECT '
				 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
	   			 + '	TTR.destinationId, '
	   			 + '	TTCertification.destinationId '
	   			 + 'FROM [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblRuleSetToCertificationLink] RTCL '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTR on (TTR.sourceId = RTCL.idRuleSet AND TTR.object = ''ruleset'') '
	   			 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCertification on (TTCertification.sourceId = RTCL.idCertification AND TTCertification.object = ''certification'') '
	   			 + 'WHERE RTCL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
	   			
		EXEC(@sql)

		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.24: Copy CertificateCertification - Ruleset to Certification Links Inserted', 0)

		/*

		CERTIFICATION TO USER LINKS - FULL COPY ONLY

		*/

		IF @fullCopyFlag = 1
			BEGIN

			-- copy certification to user links
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationToUserLink] '
					 + '( '
					 + '	idSite, '
					 + '	idCertification, '
					 + '	idUser, '
					 + '	initialAwardDate, '
					 + '	certificationTrack, '
					 + '	currentExpirationDate, '
					 + '	currentExpirationInterval, '
					 + '	currentExpirationTimeframe, '
					 + '	certificationId, '
					 + '	lastExpirationDate, '
					 + '	idRuleSet '
					 + ') '
					 + 'SELECT ' 
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
					 + '	TTC.destinationId, '
					 + '	TTU.destinationId, '
					 + '	CTUL.initialAwardDate, '
					 + '	CTUL.certificationTrack, '
					 + '	CTUL.currentExpirationDate, '
					 + '	CTUL.currentExpirationInterval, '
					 + '	CTUL.currentExpirationTimeframe, '
					 + '	CTUL.certificationId, '
					 + '	CTUL.lastExpirationDate, '
					 + '	TTRS.destinationId '
					 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationToUserLink] CTUL '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTC on (TTC.sourceId = CTUL.idCertification AND TTC.object = ''certification'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = CTUL.idUser AND TTU.object = ''users'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTRS on (TTRS.sourceId = CTUL.idRuleSet AND TTRS.object = ''ruleset'') '
					 + 'WHERE CTUL.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
              
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.25: Copy CertificateCertification - Certification to User Links Inserted', 0)

			-- insert the mapping for source to destination certification to user link id
			SET @sql = 'INSERT INTO [dbo].[ClonePortal_IDMappings] '
					 + '( '
					 + '	sourceId, '
					 + '	destinationId, '
					 + '	[object] '
					 + ') '
					 + 'SELECT '
					 + '	SRC.idCertificationToUserLink, '
					 + '	DST.idCertificationToUserLink, '
					 + '	''certificationtouserlink'' '
					 + 'FROM [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblCertificationToUserLink] DST '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.destinationId = DST.idUser AND TTU.object = ''users'') '
					 + 'LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblCertificationToUserLink] SRC '
					 + 'ON (SRC.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
					 + '	AND (SRC.idUser = TTU.sourceId) '
					 + '	AND (SRC.initialAwardDate = DST.initialAwardDate OR (SRC.initialAwardDate IS NULL AND DST.initialAwardDate IS NULL)) '
					 + '	AND (SRC.currentExpirationDate = DST.currentExpirationDate OR (SRC.currentExpirationDate IS NULL AND DST.currentExpirationDate IS NULL)) '
					 + '	AND (SRC.certificationTrack = DST.certificationTrack)) '
					 + 'WHERE DST.idSite = ' + CAST(@idSiteDestination AS NVARCHAR)
						
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.26: Copy CertificateCertification - Certification to User Link Mappings Created', 0)

			-- insert records from source site to destination table Data-CertificationModuleRequirement
			SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].' + '[tblData-CertificationModuleRequirement] '
					 + '( '
					 + '	idSite, '
					 + '	idCertificationToUserLink, '
					 + '	idCertificationModuleRequirement, '
					 + '	label, '
					 + '	dtCompleted, '
					 + '	completionDocumentationFilePath, '
					 + '	dtSubmitted, '
					 + '	isApproved, '
					 + '	dtApproved, '
					 + '	idApprover '
					 + ') '
					 + 'SELECT ' 
					 +		CAST(@idSiteDestination AS NVARCHAR) + ', '
					 + '	TTCTUL.destinationId, '
					 + '	TTCMR.destinationId, '
					 + '	DCMR.label, '
					 + '	DCMR.dtCompleted, '
					 + '	DCMR.completionDocumentationFilePath, '
					 + '	DCMR.dtSubmitted, '
					 + '	DCMR.isApproved, '
					 + '	DCMR.dtApproved, '
					 + '	TTU.destinationId '
					 + 'FROM  [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].' + '[tblData-CertificationModuleRequirement] DCMR '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCTUL on (TTCTUL.sourceId = DCMR.idCertificationToUserLink AND TTCTUL.object = ''certificationtouserlink'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTCMR on (TTCMR.sourceId = DCMR.idCertificationModuleRequirement AND TTCMR.object = ''certificationmodulerequirement'') '
					 + 'LEFT JOIN [dbo].[ClonePortal_IDMappings] TTU on (TTU.sourceId = DCMR.idApprover AND TTU.object = ''users'') '
					 + 'WHERE DCMR.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
									
			EXEC(@sql)

			INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9.27: Copy CertificateCertification - User Certification Module Requirement Data Inserted', 0)

			END			  
	   
		/*

		RETURN

		*/

		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemClonePortal_CertificateCertification_InsertedSuccessfully'

	END TRY

	/*

	CATCH AND LOG ERRORS

	*/

	BEGIN CATCH
		
		INSERT INTO [tblPortalCloneLog] ([timestamp], [message], [isError]) VALUES (GETUTCDATE(), 'STEP 9: Copy CertificateCertification - ERROR - ' + CAST(ERROR_LINE() AS NVARCHAR) + ' | ' + CAST(ERROR_MESSAGE() AS NVARCHAR(MAX)), 1)	
		SET @Return_Code = 1
		SET @Error_Description_Code	= 'SystemClonePortal_CertificateCertification_PortalMigrationFailed'
		
	END CATCH
		
	SET XACT_ABORT OFF		

END