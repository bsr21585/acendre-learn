-- =====================================================================
-- PROCEDURE: [ClonePortalSite.Delete]   Script Date: 3/9/2017 7:40:19 PM ******/

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[ClonePortalSite.Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [ClonePortalSite.Delete]
GO


/*

 Delete site from Account database and customer manage database 

*/
CREATE PROCEDURE [dbo].[ClonePortalSite.Delete]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idCallerSite			INT				= 0, --default if not specified
	@callerLangString		NVARCHAR (10),
	@idCaller				INT				= 0,
	@idSite				INT,

	@sourceDBServer		NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idAccountOld			INT	= 0,
	@hostNameOld			NVARCHAR(50)
)
AS
	
BEGIN
SET NOCOUNT ON 

		DECLARE @sqlStr NVARCHAR(MAX)
		DECLARE @isIdSiteExists INT

	 /*
	validate that the object exists
	*/
	SET @sqlStr = 'SET @isIdSiteExists = (SELECT  COUNT(1)	FROM ['+@sourceDBServer+'].['+@sourceDBName+'].[dbo].[tblSite] 
				WHERE idSite ='+ cast(@idSite AS NVARCHAR)
			 +')'

	EXECUTE SP_EXECUTESQL @sqlStr, N'@isIdSiteExists INT OUTPUT', @isIdSiteExists OUTPUT

	IF ( @isIdSiteExists = 0 )
		BEGIN
		SET @Return_Code = 1
		RETURN 1
		END

		-- calling intermediary procedure [Site.DeleteSingular] to delete site from Account database on link server.

	     SET @sqlStr = 'Exec [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[Site.DeleteSingular]'
	     +'  @idCallerSite=' + CAST(@idCallerSite AS NVARCHAR)
	     +', @callerLangString=' + ''''+ @callerLangString + ''''
	     +', @idCaller=' + CAST(@idCaller AS NVARCHAR)
	     +', @idSite =' + CAST(@idSite AS NVARCHAR)
	     +', @Return_Code = NULL'
	     +', @Error_Description_Code = NULL'
		  
		EXECUTE SP_EXECUTESQL @sqlStr

		  
		  -- delete old host name after copying the same host name as source on diffrent database 
		
	     DELETE 
	     FROM tblAccountToDomainAliasLink
	     WHERE idAccount=@idAccountOld 
	     AND hostname= @hostNameOld 
	     AND (hostname IS NOT NULL AND  hostname <> '')

	   
	   SET @Return_Code = 0
	   SET @Error_Description_Code = ''
	
END 


	