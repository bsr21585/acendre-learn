
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[System.CopyInquisiqToAsentia.Site]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [System.CopyInquisiqToAsentia.Site]
GO


/*

Copy source site data from Inquisiq to destination site in Asentia

*/
CREATE PROCEDURE [dbo].[System.CopyInquisiqToAsentia.Site]
(
	@Return_Code			INT				OUTPUT,
	@Error_Description_Code	NVARCHAR(50)	OUTPUT,	
	@idAccount				INT,
	@idCaller				INT				= 1,
	@idSiteDefaultLang		INT				= 57,		-- English is ID 57	
	@siteHostname			NVARCHAR(255),
	@idSiteDestination		INT				OUTPUT,
	@destinationHostname	NVARCHAR(255),
	@sourceDBServer			NVARCHAR(50),
	@sourceDBName			NVARCHAR(50),
	@idSiteSource			INT				OUTPUT,
	@destinationDBServer	NVARCHAR(50),
	@destinationDBName 		NVARCHAR(50)	
)
AS

BEGIN

    SET NOCOUNT ON
    SET XACT_ABORT ON    
    	
	DECLARE @sql NVARCHAR(MAX)
	  
	/*
	  
	create id mappings table or clear out existing id mappings table

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_IDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL,'
			+ '    [object] NVARCHAR(255)'
			+ ')'
			+ ' END '
			+ ' ELSE '
	SET @sql = @sql + ' DELETE FROM [' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings]'

	EXEC(@sql)
	  
	/*
	  
	create timezone mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_TimezoneIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_TimezoneIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '
			+ ' (1, 1), '
			+ ' (2, 102), '
			+ ' (3, 3), '
			+ ' (4, 4), '
			+ ' (5, 6), '
			+ ' (6, 5), '
			+ ' (7, 7), '
			+ ' (8, 8), '
			+ ' (9, 8), '
			+ ' (10, 9), '
			+ ' (11, 10), '
			+ ' (12, 11), '
			+ ' (13, 12), '
			+ ' (14, 12), '
			+ ' (15, 13), '
			+ ' (16, 14), '
			+ ' (17, 15), '
			+ ' (18, 16), '
			+ ' (19, 17), '
			+ ' (20, 19), '
			+ ' (21, 21), '
			+ ' (22, 21), '
			+ ' (23, 22), '
			+ ' (24, 23), '
			+ ' (25, 24), '
			+ ' (26, 25), '
			+ ' (27, 27), '
			+ ' (28, 28), '
			+ ' (29, 31), '
			+ ' (30, 32), '
			+ ' (31, 33), '
			+ ' (32, 34), '
			+ ' (33, 36), '
			+ ' (34, 38), '
			+ ' (35, 39), '
			+ ' (36, 40), '
			+ ' (37, 41), '
			+ ' (38, 43), '
			+ ' (39, 54), '
			+ ' (40, 45), '
			+ ' (41, 46), '
			+ ' (42, 47), '
			+ ' (43, 50), '
			+ ' (44, 51), '
			+ ' (45, 53), '
			+ ' (46, 56), '
			+ ' (47, 44), '
			+ ' (48, 55), '
			+ ' (49, 57), '
			+ ' (50, 62), '
			+ ' (51, 58), '
			+ ' (52, 64), '
			+ ' (53, 59), '
			+ ' (54, 60), '
			+ ' (55, 61), '
			+ ' (56, 65), '
			+ ' (57, 65), '
			+ ' (58, 66), '
			+ ' (59, 74), '
			+ ' (60, 68), '
			+ ' (61, 69), '
			+ ' (62, 70), '
			+ ' (63, 71), '
			+ ' (64, 77), '
			+ ' (65, 72), '
			+ ' (66, 75), '
			+ ' (67, 76), '
			+ ' (68, 79), '
			+ ' (69, 78), '
			+ ' (70, 83), '
			+ ' (71, 80), '
			+ ' (72, 81), '
			+ ' (73, 82), '
			+ ' (74, 85), '
			+ ' (75, 86), '
			+ ' (76, 93), '
			+ ' (77, 87), '
			+ ' (78, 88), '
			+ ' (79, 89), '
			+ ' (80, 90), '
			+ ' (81, 91), '
			+ ' (82, 92), '
			+ ' (83, 95), '
			+ ' (84, 99), '
			+ ' (85, 96), '
			+ ' (86, 98), '
			+ ' (87, 101) '
			+ ' END '		

	EXEC(@sql)

	/*
	  
	create event type mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EventTypeIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '			
			+ ' (1, 702), '		-- Certificate Awarded
			+ ' (2, 701), '		-- Certificate Earned
			+ ' (3, 703), '		-- Certificate Revoked
			+ ' (4, 0), '		-- Purchase Confirmed - NOT IN ASENTIA
			+ ' (5, 206), '		-- Enrollment Start
			+ ' (6, 205), '		-- Enrollment End
			+ ' (7, 203), '		-- Enrollment Revoked
			+ ' (8, 204), '		-- Enrollment Due
			+ ' (9, 301), '		-- Lesson Passed
			+ ' (10, 302), '	-- Lesson Failed
			+ ' (11, 303), '	-- Lesson Completed
			+ ' (14, 101), '	-- User Created
			+ ' (15, 102), '	-- User Deleted
			+ ' (16, 402), '	-- Session Instructor Assigned or Changed
			+ ' (17, 403), '	-- Session Instructor Removed
			+ ' (18, 404), '	-- Learner Joined to Session
			+ ' (19, 405), '	-- Learner Dropped From Session
			+ ' (20, 406), '	-- Session Time or Location Changed
			+ ' (21, 205), '	-- Enrollment Completed
			+ ' (22, 103), '	-- User Account Expires
			+ ' (24, 401), '	-- Session Meets
			+ ' (25, 704) '		-- Certificate Expires
			+ ' END '		

	EXEC(@sql)

	/*
	  
	create event type recipient mappings table, if it doesnt already exist

	*/

	SET @sql = 'IF NOT EXISTS (SELECT * FROM [' + @destinationDBName + '].dbo.sysobjects WHERE name = N''InquisiqMigration_EventTypeRecipientIDMappings'' AND xtype = ''U'')'
			+ ' BEGIN '
			+ 'CREATE TABLE [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings]'
			+ '('
			+ '    [sourceID] INT NOT NULL,'
			+ '    [destinationID] INT NOT NULL'
			+ ')'
			+ ' '
			+ ' INSERT INTO [' + @destinationDBName + '].[dbo].[InquisiqMigration_EventTypeRecipientIDMappings] (sourceID, destinationID) '
			+ ' VALUES '
			+ ' (0, 0), '
			+ ' (1, 1), '
			+ ' (3, 2), '
			+ ' (4, 1), '
			+ ' (5, 2), '
			+ ' (6, 1), '
			+ ' (7, 2), '
			+ ' (8, 1), '
			+ ' (9, 3), '
			+ ' (10, 1), '
			+ ' (11, 2), '
			+ ' (12, 1), '
			+ ' (13, 2), '
			+ ' (14, 1), '
			+ ' (15, 2), '
			+ ' (16, 1), '
			+ ' (17, 2), '
			+ ' (18, 1), '
			+ ' (19, 2), '
			+ ' (20, 1), '
			+ ' (21, 2), '
			+ ' (22, 1), '
			+ ' (23, 2), '
			+ ' (24, 1), '
			+ ' (25, 2), '
			+ ' (28, 1), '
			+ ' (29, 3), '
			+ ' (30, 1), '
			+ ' (32, 1), '
			+ ' (33, 5), '
			+ ' (34, 6), '
			+ ' (35, 1), '
			+ ' (36, 5), '
			+ ' (38, 1), '
			+ ' (39, 5), '
			+ ' (40, 2), '
			+ ' (41, 1), '
			+ ' (42, 5), '
			+ ' (43, 2), '
			+ ' (44, 1), '
			+ ' (45, 5), '
			+ ' (46, 6), '
			+ ' (47, 1), '
			+ ' (48, 2), '
			+ ' (49, 8), '
			+ ' (50, 8), '
			+ ' (51, 8), '
			+ ' (52, 8), '
			+ ' (53, 8), '
			+ ' (54, 8), '
			+ ' (55, 8), '
			+ ' (56, 8), '
			+ ' (57, 8), '
			+ ' (58, 8), '
			+ ' (59, 8), '
			+ ' (60, 8), '
			+ ' (61, 8), '
			+ ' (62, 1), '
			+ ' (63, 3), '
			+ ' (64, 1), '
			+ ' (65, 5), '
			+ ' (66, 6), '
			+ ' (67, 1), '
			+ ' (68, 2), '
			+ ' (69, 8) '
			+ ' END '		

	EXEC(@sql)

	/*
	
	validate unique host name in Account database -- this runs in scope of "Customer Manager" so it can be executed here instead of as dynamic SQL
	
	*/
	
	IF (SELECT COUNT(1) FROM tblAccountToDomainAliasLink WHERE hostname = @destinationHostname) > 0
		BEGIN
		SET @Return_Code = 2
		SET @Error_Description_Code = 'SystemCopyInquisiqToAsentia_HostnameNotUnique'
		RETURN 1
		END

	/*

	get the id of the source (Inquisiq) site

	*/

	SET @sql = 'SELECT @idSiteSource = idSite FROM [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSite] WHERE hostname = ''' + @siteHostname + ''''

	EXECUTE SP_EXECUTESQL @sql, N'@idSiteSource INT OUTPUT', @idSiteSource OUTPUT
	
    /*

	TRANSACTION BEGIN

	*/


	-- =============================================================================
	-- LOGIC START

	/************************************************SITE*************************************************************/
		-- tblSite
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite]'
				+ ' ( '
				+ '		hostname, '
				+ '		password, '
				+ '		isActive, '
				+ '		dtExpires, '
				+ '		title, '
				+ '		company, '
				+ '		contactName, '
				+ '		contactEmail, '
				+ '		userLimit, '
				+ '		kbLimit, '
				+ '		idLanguage, '
				+ '		idTimezone' 
				+ ' ) '
				+ ' SELECT ' 
				+ '		''' + @destinationHostname + ''','							-- hostname
				+ '		S.password,'												-- password (this is an MD5 value)
				+ '		S.isActive,'												-- is active?
				+ '		S.dtExpires,'												-- date expires
				+ '		S.name,'													-- title
				+ '		SC.company,'												-- company
				+ '		SC.firstName + '' '' + SC.lastName,'						-- contact name
				+ '		SC.email,'													-- contact email
				+ '		CASE WHEN S.userLimit > 0 THEN S.userLimit ELSE NULL END,'	-- user limit
				+ '		CASE WHEN S.kbLimit > 0 THEN S.kbLimit ELSE NULL END,'		-- kb limit
				+ '		' + CAST(@idSiteDefaultLang AS NVARCHAR) + ','				-- language ID
				+ '		15'															-- timezone ID				
				+ ' FROM  [' + @sourceDBServer + '].['+ @sourceDBName + '].[dbo].[tblSite] S'
				+ ' LEFT JOIN [' + @sourceDBServer + '].[' + @sourceDBName + '].[dbo].[tblSiteContact] SC ON SC.idSite = S.idSite'
				+ ' WHERE S.idSite = ' + CAST(@idSiteSource AS NVARCHAR) 
		
		EXEC (@sql)

		-- get the newly inserted site id as @idSiteDestination
		SET @sql = 'SELECT @idSiteDestination = idSite FROM [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite] WHERE hostname = ''' + @destinationHostname + ''''

		EXECUTE SP_EXECUTESQL @sql, N'@idSiteDestination INT OUTPUT', @idSiteDestination OUTPUT
	  
		-- insert idSite of source and destination site tables inside temporary table for mapping	
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[InquisiqMigration_IDMappings] '
				+ ' ( '
				+ '		sourceID,'
				+ '		destinationID,'
				+ '		object'
				+ ' ) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteSource) + ', '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ', '
				+ '		''site'' ' 				
		
		EXEC(@sql)		   	   

		-- tblSiteLanguage
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSiteLanguage]' 
				+ ' ( '
				+ '		idSite,'
				+ '		idLanguage,'
				+ '		title,'
				+ '		company'
				+ '	) '
				+ ' SELECT '
				+ '		S.idSite, '
				+ '		' + CONVERT(NVARCHAR, @idSiteDefaultLang) + ','
				+ '		S.title, '
				+ '		S.company'
				+ ' FROM  [' + @destinationDBServer + '].['+ @destinationDBName + '].[dbo].[tblSite] S'
				+ ' WHERE S.idSite = ' + CONVERT(NVARCHAR, @idSiteDestination)
		
		EXEC(@sql)	

		-- tblSiteToDomainAliasLink
		SET @sql = 'INSERT INTO [' + @destinationDBServer + '].[' + @destinationDBName + '].[dbo].[tblSiteToDomainAliasLink]' 
				+ ' ( '
				+ '		idSite,'
				+ '		domain'				
				+ '	) '
				+ ' SELECT '
				+ '		' + CONVERT(NVARCHAR, @idSiteDestination) + ','
				+ '		''' + @destinationHostname + ''''				
		
		EXEC(@sql)

		  
		SET @Return_Code = 0
		SET @Error_Description_Code	= 'SystemCopyInquisiqToAsentiaSite_Success'
		
	SET XACT_ABORT OFF

END
GO